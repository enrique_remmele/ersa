﻿Imports System.Drawing
Imports System.Windows.Forms

Module MVariablesGlobales

    'CONEXION
    Public vgConexiones() As String = {}
    Public vgIDConexion As String = 0
    Public vgNombreEmpresa As String = "Express"
    Public vgNombreServidor As String = "192.168.0.141"
    Public vgNombreBaseDatos As String = "EXPRESS"
    Public vgNombreUsuarioBD As String = "sa"
    Public vgPasswordBD As String = "Abc1234"
    Public VGCadenaConexion As String = "Data Source=localhost;Initial Catalog=EXPRESS;User Id=sa;Password=Abc1234"

    'Public vgNombreServidor As String = "sql2003.shared-servers.com,1088"
    'Public vgNombreBaseDatos As String = "EXPRESS"
    'Public vgNombreUsuarioBD As String = "saexpress"
    'Public vgPasswordBD As String = "Abc1234"
    'Public VGCadenaConexion As String = "Data Source=localhost;Initial Catalog=EXPRESS;User Id=sa;Password=Abc1234"

    'Public vgNombreServidor As String = "201.217.57.82"
    'Public vgNombreBaseDatos As String = "EXPRESS"
    'Public vgNombreUsuarioBD As String = "sa"
    'Public vgPasswordBD As String = "Abc1234"
    'Public VGCadenaConexion As String = "Data Source=localhost;Initial Catalog=EXPRESS;User Id=sa;Password=Abc1234"

    'PATH
    Public VGCarpetaAplicacion As String = My.Computer.FileSystem.SpecialDirectories.ProgramFiles & "\" & My.Application.Info.ProductName
    Public VGCarpetaError As String = VGCarpetaAplicacion & "\Error\"
    Public VGCarpetaTemporal As String = VGCarpetaAplicacion & "\Temporales\"
    Public VGPathEjecutable As String = My.Application.Info.DirectoryPath & "\"

    'INFORMACION DE APLICACION
    Public VGSoftwareNombre As String = My.Application.Info.ProductName
    Public VGSoftwareVersion As String = My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Build & "." & My.Application.Info.Version.Revision
    Public VGSoftwareUltimaActualizacion As String = "04-11-13"
    Public VGSoftwareLicenciaOtorgadaA As String = "EXPRESS ALIMENTOS"
    Public VGSoftwareDesarrollador As String = "EXPRESS SYSTEMS S.A. :: Tel: 202-202 - www.expressystem.com.py - info@expressystem.com.py"
    Public VGSoftwareInfo As String = VGSoftwareNombre & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.expressystem.com.py"

    'CONFIGURACIONES
    Public VGArchivoINI As String = VGCarpetaAplicacion & "\" & My.Application.Info.ProductName & ".INI"
    Public VGFechaHoraSistema As Date
    Public vgConfiguraciones As DataRow

    'IDENTIFICACIONES
    Public vgIDSucursal As Integer = 1
    Public vgIDDeposito As Integer = 1
    Public vgIDTerminal As Integer = 1
    Public vgIDUsuario As Integer = 1
    Public vgIDPerfil As Integer = 1
    Public vgSucursal As String = "ASUNCION"
    Public vgSucursalCodigo As String = "ASU"
    Public vgReferenciaSucursal As String = "001"
    Public vgDeposito As String = "CENTRAL"
    Public vgTerminal As String = "DESARROLLO"
    Public vgUsuario As String = "carlos"
    Public vgUsuarioIdentificador As String = "CAR"
    Public vgPerfil As String = "ADMIN"
    Public vgOwnerDBFunction As String = "dbo"

    'EMPRESA
    Public vgEmpresa As String = "EXPRESS ALIMENTOS S.R.L."
    Public vgEmpresaRUC As String = "800055448"
    Public vgEmpresaDireccion As String = "Acahay 931 c/ Tte. Villalba"
    Public vgEmpresaTelefono As String = "(59521) 284-033 / (59521) 284-035 / (59521) 282-474"
    Public vgEmpresaWeb As String = "http://www.expressalimentos.com.py"

    'LICENCIA
    Public vgLicencia As String = ""
    Public vgLicenciaPath As String = My.Computer.FileSystem.SpecialDirectories.ProgramFiles & "\" & My.Application.Info.ProductName & "\" & My.Application.Info.ProductName & ".lic"
    Public vgLicenciaOtorgado As String
    Public vgLicenciaOtorgadoRazonSocial As String
    Public vgLicenciaOtorgadoRUC As String
    Public vgLicenciaOtorgadoDireccion As String
    Public vgLicenciaOtorgadoTelefono As String
    Public vgLicenciaOtorgadoEmail As String
    Public vgLicenciaOtorgadoWEB As String
    Public vgLicenciaOtorgadoFechaInicio As String
    Public vgLicenciaOtorgadoFechaFin As String
    Public vgLicenciaOtorgadoSucursales As String = "0"
    Public vgLicenciaOtorgadoDepositos As String = "0"
    Public vgLicenciaOtorgadoTerminales As String = "0"
    Public vgLicenciaOtorgadoAdmin As String
    Public vgLicenciaOtorgadoPassword As String

    'APARIENCIA
    Public vgColorFocus As String = Color.AliceBlue.ToArgb.ToString
    Public vgColorSoloLectura As String = Color.WhiteSmoke.ToArgb.ToString
    Public vgColorBackColor As String = Color.WhiteSmoke.ToArgb.ToString
    Public vgColorSeleccionado As String = Color.LightGreen.ToArgb.ToString
    Public vgColorFormulario As String = Color.FromKnownColor(KnownColor.Control).ToArgb
    Public vgColorFormulario2 As String = Color.FromKnownColor(KnownColor.Control).ToArgb
    Public vgColorTextos As String = Color.FromKnownColor(KnownColor.ControlText).ToArgb
    Public vgColorBoton As String = Color.FromKnownColor(KnownColor.Control).ToArgb
    Public vgColorTextosBoton As String = Color.FromKnownColor(KnownColor.ControlText).ToArgb
    Public vgColorDataGridAlternancia As String = Color.FromKnownColor(KnownColor.WhiteSmoke).ToArgb

    'DATA
    Public vgData As New DataSet

    'TECLAS DE ACCESO RAPIDO
    Public vgKeyConsultar As Integer = Keys.F1
    Public vgKeyEditar As Integer = Keys.F2
    Public vgKeyVerInformacionDetallada As Integer = Keys.F3
    Public vgKeyActualizarTabla As Integer = Keys.F4
    Public vgKeyInicializar As Integer = Keys.F5

    'Transacciones
    Public vgKeyProcesar As Integer = Keys.F8
    Public vgKeyCerrar As Integer = Keys.Escape
    Public vgKeyNuevoRegistro As Integer = Keys.Add

    'Otros
    'Facturacion
    Public vgImpresionFacturaPath As String
    Public vgImpresionFacturaConversionPath As String
    Public vgImpresionFacturaImpresora As String

    'Nota Credito/Debito
    Public vgImpresionNotaCreditoPath As String
    Public vgImpresionNotaDebitoPath As String

    'Retenciones
    Public vgImpresionRetencionPath As String

End Module

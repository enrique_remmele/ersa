﻿Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Windows
Imports System.Drawing
Imports SAINLib

Namespace Reporte

    Public Class CReporte

        Protected CSistema As New CSistema
        Protected CData As New CData

        Private frmMainValue As Form
        Public Property frmMain() As Form
            Get
                Return frmMainValue
            End Get
            Set(ByVal value As Form)
                frmMainValue = value
            End Set
        End Property

        Private frmReporteValue As frmReporte
        Public Property frmReporte() As frmReporte
            Get
                Return frmReporteValue
            End Get
            Set(ByVal value As frmReporte)
                frmReporteValue = value
            End Set
        End Property

        Protected Sub EstablecerConexion(ByVal report As ReportDocument)

            Dim CRTLI As CrystalDecisions.Shared.TableLogOnInfo
            For Each CRTable In report.Database.Tables
                CRTLI = CRTable.LogOnInfo
                With CRTLI.ConnectionInfo
                    .ServerName = vgNombreServidor
                    .UserID = vgNombreUsuarioBD
                    .Password = vgPasswordBD
                    .DatabaseName = vgNombreBaseDatos
                End With
                CRTable.ApplyLogOnInfo(CRTLI)
            Next CRTable

        End Sub

        Protected Sub LoadReport(ByRef rpt As ReportClass, ByVal Name As String)

            rpt = CreateObject(Name)

        End Sub

    End Class

    Public Class CReporteVentas
        Inherits CReporte

        Sub ListadoFacturasEmitidas(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VVenta")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoFacturasEmitidas")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo de Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoFacturasEmitidasDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleVenta " & WhereDetalle, conn)
            Dim aTable3 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosadoGravado ", conn)
            Dim aTable4 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosado ", conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VVenta")
                DSDatos.Tables.Add("VDetalleVenta")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                aTable2.Fill(DSDatos.Tables("VDetalleVenta"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoFacturasEmitidasDetallada")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo de Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = True
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasProductoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal SubTitulo As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleVenta " & WhereDetalle & " Order By TipoProducto", conn)

            Try

                DSDatos.Tables.Add("VVenta")
                DSDatos.Tables.Add("VDetalleVenta")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                aTable2.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasProductoCliente")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'SubTitulo
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSubTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = SubTitulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuarioIdentificador & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = VGSoftwareInfo

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = True
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasTotalesProducto(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal SubTitulo As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleVenta " & WhereDetalle, conn)

            Try

                DSDatos.Tables.Add("VDetalleVenta")
                conn.Open()
                aTable2.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasTotalesProducto")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'SubTitulo
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSubTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = SubTitulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuarioIdentificador & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = VGSoftwareInfo

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasTotalesTipoProducto(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String, ByVal Venta As String, ByVal Moneda As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleVenta " & WhereDetalle, conn)

            Try

                DSDatos.Tables.Add("VVenta")
                DSDatos.Tables.Add("VDetalleVenta")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                aTable2.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasTotalesTipoProducto")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Rango de Fechas
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Tipo Venta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtVenta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Venta

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda


                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = True
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasTotalesCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String, ByVal Venta As String, ByVal Moneda As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,Vendedor,'TotalBruto'=SUM(TotalBruto),'TotalDescuento'=SUM(TotalDescuento),'Total'=SUM(Total),'Fec'=Max(Fec) From VVenta " & Where & " Group By Cliente,Vendedor" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasTotalesCliente")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Rango de Fechas
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Tipo Venta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtVenta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Venta

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda


                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasTotalesTipoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String, ByVal Venta As String, ByVal Moneda As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " TipoCliente,Cliente,Vendedor,'TotalBruto'=SUM(TotalBruto),'TotalDescuento'=SUM(TotalDescuento),'Total'=SUM(Total),'Fec'=Max(Fec) From VVenta " & Where & " Group By Cliente,Vendedor,TipoCliente" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasTotalesTipoCliente")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Rango de Fechas
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Tipo Venta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtVenta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Venta

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda

                'TipoInforme
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = True
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasTotalesProductoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String, ByVal Venta As String, ByVal Moneda As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,Producto,'Bruto'=SUM(Bruto),'TotalDescuento'=SUM(TotalDescuento),'Total'=SUM(Total),'FechaEmision'=MAX(FechaEmision) from VDetalleVenta" & Where & " group by Cliente,Producto,FechaEmision " & OrderBy, conn)
            Try

                DSDatos.Tables.Add("VDetalleVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentaTotalesProductoCliente")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Rango de Fechas
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Tipo Venta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtVenta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Venta

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = True
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDetalleVentasProductoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String, ByVal Venta As String, ByVal Moneda As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleVenta " & WhereDetalle, conn)

            Try

                DSDatos.Tables.Add("VVenta")
                DSDatos.Tables.Add("VDetalleVenta")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                aTable2.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoDetalleVentaProductoCliente")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Rango de Fechas
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Tipo Venta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtVenta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Venta

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda


                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = True
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasAnualTotalCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String, ByVal Venta As String, ByVal Moneda As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,'Enero'=SUM(Enero),'Febrero'=sum(Febrero),'Marzo'=SUM(Marzo),'Abril'=SUM(Abril),'Mayo'=SUM(Mayo),'Junio'=SUM(Junio),'Julio'=SUM(Julio),'Agosto'=SUM(Agosto),'Setiembre'=SUM(Setiembre),'Octubre'=SUM(Octubre),'Noviembre'=SUM(Noviembre),'Diciembre'=SUM(Diciembre) From VVentaTotalAnualCliente " & Where & " group by Cliente " & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            Try

                DSDatos.Tables.Add("VVentaTotalAnualCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentaTotalAnualCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasAnualesTotalesCliente")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Rango de Fechas
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Tipo Venta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtVenta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Venta

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda


                ' ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasAnualTotalTipoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String, ByVal Venta As String, ByVal Moneda As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " TipoCliente,'Enero'=SUM(Enero),'Febrero'=sum(Febrero),'Marzo'=SUM(Marzo),'Abril'=SUM(Abril),'Mayo'=SUM(Mayo),'Junio'=SUM(Junio),'Julio'=SUM(Julio),'Agosto'=SUM(Agosto),'Setiembre'=SUM(Setiembre),'Octubre'=SUM(Octubre),'Noviembre'=SUM(Noviembre),'Diciembre'=SUM(Diciembre)  From VVentaTotalAnualTipoCliente " & Where & " group by TipoCliente" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVentaTotalAnualTipoCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentaTotalAnualTipoCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasAnualesTotalesTipoCliente")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Rango de Fechas
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Tipo Venta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtVenta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Venta

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda


                ' ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasAnualTotalClienteProducto(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String, ByVal Venta As String, ByVal Moneda As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,Producto,Referencia,ReferenciaProducto,'Enero'=SUM(Enero),'Febrero'=sum(Febrero),'Marzo'=SUM(Marzo),'Abril'=SUM(Abril),'Mayo'=SUM(Mayo),'Junio'=SUM(Junio),'Julio'=SUM(Julio),'Agosto'=SUM(Agosto),'Setiembre'=SUM(Setiembre),'Octubre'=SUM(Octubre),'Noviembre'=SUM(Noviembre),'Diciembre'=SUM(Diciembre)  From VVentaTotalAnualProductoCliente " & Where & " group by Cliente,Producto,Referencia,ReferenciaProducto" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVentaTotalAnualProductoCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentaTotalAnualProductoCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoTotalAnualClienteProducto")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Rango de Fechas
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'Report.ReportDefinition.Sections(2).ReportObjects

                txtField.Text = Del
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Tipo Venta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtVenta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Venta

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda


                ' ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasAnualTotalProductoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String, ByVal Venta As String, ByVal Moneda As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,Producto,Referencia,ReferenciaProducto,'Enero'=SUM(Enero),'Febrero'=sum(Febrero),'Marzo'=SUM(Marzo),'Abril'=SUM(Abril),'Mayo'=SUM(Mayo),'Junio'=SUM(Junio),'Julio'=SUM(Julio),'Agosto'=SUM(Agosto),'Setiembre'=SUM(Setiembre),'Octubre'=SUM(Octubre),'Noviembre'=SUM(Noviembre),'Diciembre'=SUM(Diciembre)  From VVentaTotalAnualProductoCliente " & Where & " group by Cliente,Producto,Referencia,ReferenciaProducto" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVentaTotalAnualProductoCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentaTotalAnualProductoCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoTotalAnualProductoCliente")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Rango de Fechas
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Tipo Venta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtVenta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Venta

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda


                ' ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasAnualCantidadCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String, ByVal Venta As String, ByVal Moneda As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,'Enero'=SUM(Enero),'Febrero'=sum(Febrero),'Marzo'=SUM(Marzo),'Abril'=SUM(Abril),'Mayo'=SUM(Mayo),'Junio'=SUM(Junio),'Julio'=SUM(Julio),'Agosto'=SUM(Agosto),'Setiembre'=SUM(Setiembre),'Octubre'=SUM(Octubre),'Noviembre'=SUM(Noviembre),'Diciembre'=SUM(Diciembre) From VVentaCantidadAnualCliente " & Where & " group by Cliente " & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            Try

                DSDatos.Tables.Add("VVentaCantidadAnualCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentaCantidadAnualCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentaAnualCantidadCliente")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Rango de Fechas
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Tipo Venta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtVenta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Venta

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda


                ' ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub GraficoVentaMesTotal(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal Año As Integer)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select Mes,'Total'=Sum(Total) From VMesTotal Where Year(FechaEmision) = " & Where, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VMesTotal")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VMesTotal"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptGraficoVentaMesTotal")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Año
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAño"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Año


                ' ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub GraficoRankingProductoRentable(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal Top As String, ByVal Del As String, ByVal Al As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VProductoUtilidad Where " & Where, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VProductoUtilidad")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProductoUtilidad"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptGraficoProductoRentable")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Del
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del

                'Al
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al


                ' ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub GraficoRankingClienteVentas(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal Top As String, ByVal Del As String, ByVal Al As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,'Total' =(sum(Total)) From VVenta Where " & Where & " And Anulado = 'False' Group By Cliente Order By Total Desc", conn)

            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptGraficoRankingClientesVenta")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Del
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del

                'Al
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al


                ' ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub GraficoRankingProductoMasVendido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal Top As String, ByVal Del As String, ByVal Al As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Producto,'Cantidad'=Sum(Cantidad) From VProductoMasVendido  Where " & Where & " Group By Producto Order By Cantidad Desc", conn)

            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VProductoMasVendido")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProductoMasVendido"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptGraficoProductoMasVendido")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Del
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del

                'Al
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al


                ' ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirFactura(ByRef frmReporte As frmReporte, ByRef Where As String, ByRef PathReporte As String, ByVal Impresora As String, ByVal NumeroALetra As String, ByVal Total As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VVenta " & Where & "  ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleVenta " & Where & "  Order By ID", conn)
            Dim aTable3 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosado " & Where & "  ", conn)
            Dim aTable4 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosadoGravado " & Where & "  ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VVenta")
                DSDatos.Tables.Add("VDetalleVenta")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                aTable2.Fill(DSDatos.Tables("VDetalleVenta"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                conn.Close()

                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                    MsgBox("El archivo no existe!")
                    Exit Sub
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & PathReporte
                EstablecerConexion(Report)
                Report.SetDataSource(DSDatos)

                '' Total Letras 
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = NumeroALetra & ".-"
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Total

                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)

                ''Configuracion del Reporte
                'frmReporte.CrystalReportViewer1.ReportSource = Report
                'frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                'frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                'frmReporte.CrystalReportViewer1.AutoSize = True
                'frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                'frmReporte.StartPosition = FormStartPosition.CenterScreen

                'frmReporte.Show()

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ListadoVentasAnuladas(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal TipoInforme As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " *  From VVenta " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentaAnulada")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing


                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'TipoInforme
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme


                ' ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteCompras
        Inherits CReporte

        Sub ListadoFacturasComprasEmitidas(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VCompra " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCompra")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCompra"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoFacturaCompraEmitida")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo de Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoFacturasComprasEmitidasDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VCompra " & Where & " " & OrderBy, conn)
            'Dim aTable1 As New SqlDataAdapter("Select * From VVenta ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleCompra " & WhereDetalle, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCompra")
                DSDatos.Tables.Add("VDetalleCompra")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCompra"))
                aTable2.Fill(DSDatos.Tables("VDetalleCompra"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoFacturaCompraDetallada")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo de Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = VGSoftwareInfo

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = True
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteNotaCredito
        Inherits CReporte

        Sub ImprimirNotaCredito(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCredito " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCredito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaCredito")
                Report.SetDataSource(DSDatos)

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirNotaCreditoAplicar(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String)

            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCredito " & Where & " " & OrderBy, conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCredito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListadoNotaCreditoAplicar")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirNotaCreditoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            ' Dim aTable1 As New SqlDataAdapter("Select  * From VNotaCredito ", conn)
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCredito " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotacredito ", conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos


            Try

                DSDatos.Tables.Add("VNotaCredito")
                DSDatos.Tables.Add(" VDetalleNotaCredito")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))
                aTable2.Fill(DSDatos.Tables(" VDetalleNotaCredito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpNotaCreditoDetalle")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & vgUsuarioIdentificador & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                'Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImpresionNotaCredito(ByRef frmReporte As frmReporte, ByRef Where As String, ByRef PathReporte As String, ByVal Impresora As String, ByVal NumeroALetra As String, ByVal Total As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VNotaCredito " & Where & "  ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotaCredito " & Where & "  Order By ID", conn)
            Dim aTable3 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosado " & Where & "  ", conn)
            Dim aTable4 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosadoGravado " & Where & "  ", conn)
            Dim aTable5 As New SqlDataAdapter("Select * From VDetalleImpuesto " & Where & "  ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCredito")
                DSDatos.Tables.Add("VDetalleNotaCredito")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")
                DSDatos.Tables.Add("VDetalleImpuesto")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))
                aTable2.Fill(DSDatos.Tables("VDetalleNotaCredito"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                aTable5.Fill(DSDatos.Tables("VDetalleImpuesto"))
                conn.Close()

                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                    MsgBox("El archivo no existe!")
                    Exit Sub
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & PathReporte
                Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)

                '' Total Letras 
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = NumeroALetra & ".-"
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Total

                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

    End Class

    Public Class CReporteNotaDebito
        Inherits CReporte

        Sub ImprimirNotaDebito(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaDebito " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaDebito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaDebito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaDebito")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirNotaDebitoAplicar(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            'Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCredito  " & Where & " " & OrderBy, conn)
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaDebito " & Where & " " & OrderBy, conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaDebito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaDebito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaDebitoAplicar")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"


                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirNotaDebitoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            ' Dim aTable1 As New SqlDataAdapter("Select  * From VNotaCredito ", conn)
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaDebito " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotaDebito ", conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos


            Try

                DSDatos.Tables.Add("VNotaDebito")
                DSDatos.Tables.Add(" VDetalleNotaDebito")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaDebito"))
                aTable2.Fill(DSDatos.Tables(" VDetalleNotaDebito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListadoNotaDebitoDetalle")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & vgUsuarioIdentificador & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                'Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImpresionNotaDebito(ByRef frmReporte As frmReporte, ByRef Where As String, ByRef PathReporte As String, ByVal Impresora As String, ByVal NumeroALetra As String, ByVal Total As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VNotaDebito " & Where & "  ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotaDebito " & Where & "  Order By ID", conn)
            Dim aTable3 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosado " & Where & "  ", conn)
            Dim aTable4 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosadoGravado " & Where & "  ", conn)
            Dim aTable5 As New SqlDataAdapter("Select * From VDetalleImpuesto " & Where & "  ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaDebito")
                DSDatos.Tables.Add("VDetalleNotaDebito")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")
                DSDatos.Tables.Add("VDetalleImpuesto")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaDebito"))
                aTable2.Fill(DSDatos.Tables("VDetalleNotaDebito"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                aTable5.Fill(DSDatos.Tables("VDetalleImpuesto"))
                conn.Close()

                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                    MsgBox("El archivo no existe!")
                    Exit Sub
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & PathReporte
                Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)

                '' Total Letras 
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = NumeroALetra & ".-"
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Total

                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

    End Class

    Public Class CReporteStock
        Inherits CReporte

        Sub ExistenciaValorizadaDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VExistenciaDeposito " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VExistenciaDeposito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExistenciaDeposito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExistenciaValorizadaDetallado")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo de Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ExistenciaValorizadaResumen(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " TipoProducto,'Existencia'=SUM(Existencia),'Costo'=SUM(Costo),'CostoPromedio'=sum(CostoPromedio) From VExistenciaDeposito " & Where & " group by TipoProducto " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VExistenciaDeposito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExistenciaDeposito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExistenciaValorizadaResumen")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo de Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub MovimientoProductoResumen(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Existencia As String, ByVal Del As String, ByVal Al As String, ByVal Producto As String, ByVal Subtitulo As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable2 As New SqlDataAdapter("Select " & Top & "  Movimiento, Referencia,'Entrada'=sum(Entrada),'Salida'=sum(Salida) From VExtractoMovimientoProducto " & Where & " group by Producto,Movimiento, Referencia " & OrderBy, conn)
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " T.Movimiento,'Salida'=IsNull((Select SUM(Salida) From VExtractoMovimientoProducto E " & Where & " And E.Movimiento=T.Movimiento ),0),'Entrada'=IsNull((Select SUM(Entrada) From VExtractoMovimientoProducto E " & Where & " And E.Movimiento=T.Movimiento),0) From VTipoExtractoMovimientoProducto T " & OrderBy, conn)

            'Dim Existencia As Double = CSistema.ExecuteScalar("Select 'Cantidad'=" & vgOwnerDBFunction & ".FExtractoMovimientoProducto(" & Condicion)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VExtractoMovimientoProducto")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExtractoMovimientoProducto"))

                conn.Close()


                Dim Entradas As Decimal = CSistema.dtSumColumn(DSDatos.Tables("VExtractoMovimientoProducto"), "Entrada")
                Dim Salidas As Decimal = CSistema.dtSumColumn(DSDatos.Tables("VExtractoMovimientoProducto"), "Salida")
                Dim ExitenciaFinal As Decimal = (Existencia + Entradas) - Salidas

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptMovimientoProductoResumen")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Existencia Inicial
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtExistenciaInicial"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(Existencia)

                'Existencia Final
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtExistenciaFinal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(ExitenciaFinal)

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSubtitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Subtitulo

                'Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Desde
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Del

                ''Hasta
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Al

                'Producto
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtProducto"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Producto

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub MovimientoProductoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Existencia As String, ByVal Del As String, ByVal Al As String, ByVal Subtitulo As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * from VExtractoMovimientoProductoDetalle " & Where & " " & OrderBy, conn)
            'Dim Existencia As Decimal = CSistema.ExecuteScalar("Select 'Cantidad'=dbo.FExtractoMovimientoProducto(" & CondicionGeneral)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VExtractoMovimientoProductoDetalle")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExtractoMovimientoProductoDetalle"))
                conn.Close()

                Dim dt As DataTable = DSDatos.Tables("VExtractoMovimientoProductoDetalle")
                Dim i As Integer = 0
                Dim SaldoAnterior As Decimal = 0

                For Each oRow As DataRow In dt.Rows

                    If i = 0 Then
                        SaldoAnterior = Existencia
                    End If

                    oRow("Saldo") = (SaldoAnterior + oRow("Entrada")) - oRow("Salida")
                    SaldoAnterior = oRow("Saldo")

                    i = 1

                Next

                'Calculo para los totales
                Dim Compras As Decimal
                Dim Entrada As Decimal
                Dim Venta As Decimal
                Dim salida As Decimal

                For Each oRow As DataRow In dt.Rows
                    'Suma Compras + Entradas
                    If oRow("Tipo") = "COMPRA" Then
                        Compras = Compras + oRow("Entrada")
                    End If

                    If oRow("Tipo") = "ENTRADA" Then
                        Entrada = Entrada + oRow("Entrada")
                    End If



                    'Suma Venta + salida
                    If oRow("Tipo") = "VENTA" Then
                        Venta = Venta + oRow("Salida")
                    End If
                    If oRow("Tipo") = "SALIDA" Then
                        salida = salida + oRow("Salida")
                    End If




                Next


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptMovimientoProductoDetalle")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Existencia Inicial
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtExistenciaInicial"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Existencia

                ''Existencia Final
                'txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtExistenciaFinal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = ExitenciaFinal

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSubtitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Subtitulo

                ''Existencia Anterior
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtExistenciaAnterior"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Existencia)

                ''Existencia Final
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtExitFinal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(SaldoAnterior)

                ''Existencia Anterior
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtExInicial"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Existencia)

                ' ''Desde
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Del

                ' ''Hasta
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Al

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalcompras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(CInt(Math.Round(Compras, 0)))

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalEntradas"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(CInt(Math.Round(Entrada, 0)))

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalVentas"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(CInt(Math.Round(Venta, 0)))

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalSalida"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(CInt(Math.Round(salida, 0)))


                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                Report.SetDataSource(DSDatos)
                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


        Sub PlanillaTomaInventarioFisico(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal TipoFiltro As String, ByVal Filtro As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VProducto " & Where & " " & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VProducto")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProducto"))
                conn.Close()


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanillaTomaInventario")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'TipoFiltro
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txt1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoFiltro

                'Filtro
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txt2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Filtro

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                Report.SetDataSource(DSDatos)
                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ExistenciaBajoMinimoSobreMaximo(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VExistenciaDeposito " & Where & " " & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VExistenciaDeposito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExistenciaDeposito"))
                conn.Close()


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExistenciaBajoStockMinimo")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                Report.SetDataSource(DSDatos)
                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ExistenciaMovimientoCalculado(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VExistenciaMovimientoCalculado " & Where & " " & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VExistenciaMovimientoCalculado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExistenciaMovimientoCalculado"))
                conn.Close()


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExistenciaMovimientoCalculado")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                Report.SetDataSource(DSDatos)
                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoProducto(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal OrderBy As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VProducto " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VProducto")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProducto"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoProducto")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub AjusteControlInventario(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VControlInventario Where IDTransaccion =" & Where, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VExistenciaDepositoInventario Where IDTransaccion =" & Where, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VControlInventario")
                DSDatos.Tables.Add("VExistenciaDepositoInventario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VControlInventario"))
                aTable2.Fill(DSDatos.Tables("VExistenciaDepositoInventario"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptAjusteControlInventario")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoMovimientoProducto(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VMovimiento " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VMovimiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VMovimiento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoMovimientoProducto")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoMovimientoProductoDetallado(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VMovimiento " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleMovimiento " & WhereDetalle & " ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VMovimiento")
                DSDatos.Tables.Add("VDetalleMovimiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VMovimiento"))
                aTable2.Fill(DSDatos.Tables("VDetalleMovimiento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoMovimientoProductoDetallado")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoCargaMercaderia(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VCargaMercaderia " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCargaMercaderia")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCargaMercaderia"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCargaMercaderia")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoCargaMercaderiaDetallado(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VCargaMercaderia " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleCargaMercaderia " & WhereDetalle & " ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCargaMercaderia")
                DSDatos.Tables.Add("VDetalleCargaMercaderia")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCargaMercaderia"))
                aTable2.Fill(DSDatos.Tables("VDetalleCargaMercaderia"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCargaMercaderiaDetallado")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub CargaMercaderia(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal IDTransaccion As Integer, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VCargaMercaderia Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleCargaMercaderia Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCargaMercaderia")
                DSDatos.Tables.Add("VDetalleCargaMercaderia")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCargaMercaderia"))
                aTable2.Fill(DSDatos.Tables("VDetalleCargaMercaderia"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptCargaMercaderia")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub PlanillaInventario(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal IDTransaccion As Integer, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VControlInventario Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VExistenciaDepositoInventario Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VControlInventario")
                DSDatos.Tables.Add("VExistenciaDepositoInventario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VControlInventario"))
                aTable2.Fill(DSDatos.Tables("VExistenciaDepositoInventario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanillaInventario")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub TomaInventario(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal IDTransaccion As Integer, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VControlInventario Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VExistenciaDepositoInventario Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VControlInventario")
                DSDatos.Tables.Add("VExistenciaDepositoInventario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VControlInventario"))
                aTable2.Fill(DSDatos.Tables("VExistenciaDepositoInventario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptTomaInventario")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ComparativoInventarioEquipo(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal IDTransaccion As Integer, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VControlInventario Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VExistenciaDepositoInventario Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VControlInventario")
                DSDatos.Tables.Add("VExistenciaDepositoInventario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VControlInventario"))
                aTable2.Fill(DSDatos.Tables("VExistenciaDepositoInventario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComparativoInventarioEquipo")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ComparativoInventarioSistema(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal IDTransaccion As Integer, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VControlInventario Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VExistenciaDepositoInventario Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VControlInventario")
                DSDatos.Tables.Add("VExistenciaDepositoInventario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VControlInventario"))
                aTable2.Fill(DSDatos.Tables("VExistenciaDepositoInventario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComparativoInventarioSistema")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteLotes
        Inherits CReporte

        Sub ListadoLotesEmitidos(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoLoteEmitido")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo de Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoLotesEmitidosDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " " & OrderBy, conn)
            'Dim aTable1 As New SqlDataAdapter("Select * From VVenta ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VVentaLoteDistribucion " & WhereDetalle, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VVentaLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                aTable2.Fill(DSDatos.Tables("VVentaLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoLoteEmitidoDetalle")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo de Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ' ''Responsable
                'txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ' ''Sistema
                'txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDevolucionLotes(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " " & OrderBy, conn)
            'Dim aTable1 As New SqlDataAdapter("Select * From VVenta ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleLoteDistribucion " & WhereDetalle, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VVentaLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                aTable2.Fill(DSDatos.Tables("VVentaLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoLoteDetalleDevolucion")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Tipo de Informe
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Rango de Fechas
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al


                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoComprobantesSinLote(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " and IDTransaccion not in(select IDTransaccion from VLoteDistribucion) " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoComprobanteSinLote")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo de Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDetalleComprobantesSinLote(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " and IDTransaccion not in(select IDTransaccion from VLoteDistribucion)" & OrderBy, conn)
            'Dim aTable1 As New SqlDataAdapter("Select * From VVenta ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VVentaLoteDistribucion " & WhereDetalle, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VVentaLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                aTable2.Fill(DSDatos.Tables("VVentaLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoDetalleCompranteSinLote")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo de Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ' ''Responsable
                'txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ' ''Sistema
                'txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub HojaRuta(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " " & OrderBy, conn)
            'Dim aTable1 As New SqlDataAdapter("Select * From VVenta ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VVentaLoteDistribucion " & WhereDetalle, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VVentaLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                aTable2.Fill(DSDatos.Tables("VVentaLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptHojaRuta")
                Report.SetDataSource(DSDatos)

                'Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Tipo de Informe
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = TipoInforme

                ' ''Responsable
                'txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ' ''Sistema
                'txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub CargaPlanilla(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " " & OrderBy, conn)
            'Dim aTable1 As New SqlDataAdapter("Select * From VVenta ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleLoteDistribucion " & WhereDetalle, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VDetalleLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                aTable2.Fill(DSDatos.Tables("VDetalleLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptCargaPlanilla")
                Report.SetDataSource(DSDatos)

                'Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Tipo de Informe
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = TipoInforme

                ' ''Responsable
                'txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ' ''Sistema
                'txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDevolucionLote(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VDevolucionLote " & Where, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleDevolucionLote" & Where, conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDevolucionLote")
                DSDatos.Tables.Add("VDetalleDevolucionLote")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDevolucionLote"))
                aTable2.Fill(DSDatos.Tables("VDetalleDevolucionLote"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDevolucionLotes")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Fecha Titulo
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ' ''Fecha hasta
                'txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoRendicionCobranza(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VRendicionLote " & Where, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VRendicionLote")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VRendicionLote"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptRendicionCobranza")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Fecha Titulo
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ' ''Fecha hasta
                'txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ComposiciondeLotesparaDistribucion(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From vVentaLoteDistribucion " & Where & " " & OrderBy, conn)



            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vVentaLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vVentaLoteDistribucion"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComposicionLotesparaDistribucion")
                Report.SetDataSource(DSDatos)

                'Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Tipo de Informe
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteCheque
        Inherits CReporte

        Sub ImprimirCheque(ByRef frmReporte As frmReporte, ByVal IDTransaccion As Integer, ByVal IDCuentaBancaria As Integer, ByVal Diferido As Boolean, ByVal Impresora As String, ByVal ImporteLetras As String, ByVal OP As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VCheque Where IDTransaccion=" & IDTransaccion, conn)


            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCheque")
                conn.Open()

                aTable1.Fill(DSDatos.Tables("VCheque"))
                conn.Close()

                Dim PathReporte As String = ""
                Dim dtCuentaContable As DataTable = CData.GetTable("VCuentaBancaria", " ID= " & IDCuentaBancaria).Copy

                If dtCuentaContable Is Nothing Then
                    'Mostrar error

                End If

                If dtCuentaContable.Rows.Count = 0 Then
                    'Mostrar error

                End If

                Dim oRow As DataRow = dtCuentaContable.Rows(0)

                If Diferido = True Then
                    PathReporte = oRow("FormatoDiferido").ToString
                Else
                    PathReporte = oRow("FormatoAlDia").ToString
                End If

                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                    'Mostrar error

                End If



                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & PathReporte
                Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)

                '' Total Letras 
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtImporte"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = ImporteLetras

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtOP"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = OP

                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)

                'Configuracion del Reporte

                'frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                'frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                'frmReporte.CrystalReportViewer1.AutoSize = True
                'frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                'frmReporte.StartPosition = FormStartPosition.CenterScreen



                'frmReporte.Show()
            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ListarChequeCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Tipo As String, ByVal Estado As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From Vchequecliente " & Where & "" & Tipo & "" & Estado & "" & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VChequeCliente")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VChequeCliente"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListadoCheque")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Sucursal
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"


                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirListadoCanje(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal SubTitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VPagoChequeCliente " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetallePagoChequeCliente " & WhereDetalle, conn)
            Dim aTable3 As New SqlDataAdapter("Select * From VFormaPago ", conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Debug.Print(aTable2.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VPagoChequeCliente")
                DSDatos.Tables.Add("VDetallePagoChequeCliente")
                DSDatos.Tables.Add("VFormaPago")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPagoChequeCliente"))
                aTable2.Fill(DSDatos.Tables("VDetallePagoChequeCliente"))
                aTable3.Fill(DSDatos.Tables("VFormaPago"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListadoCanjes")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Tipo de Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSubTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = SubTitulo

                'Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListarChequesDiferidosDepositados(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Condicion As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal SubTitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VChequesDiferidosDepositados " & Where & Condicion & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VChequesDiferidosDepositados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VChequesDiferidosDepositados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpChequesDiferidosDepositados")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Tipo de Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSubTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = SubTitulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"


                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListarChequeClienteDiferido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Condicion As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal SubTitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VchequeClienteVenta " & Where & " And Diferido='True' And Cartera='True'" & Condicion & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VChequeClienteVenta")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VChequeClienteVenta"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptIngresoChequesDiferidos")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Tipo de Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSubTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = SubTitulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"


                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub SaldoChequesDiferidos(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Condicion As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal ContCred As String, ByVal Sucursal As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From vChequeClienteDiferidoMovimiento" & Where & Condicion & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vChequeClienteDiferidoMovimiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vChequeClienteDiferidoMovimiento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpSaldoChequesDiferido")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                ''Condicion
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtContadoCredito"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = ContCred

                ''Sucursal
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSucursal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Sucursal

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DescuentoChequeDiferido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Responsable As String, ByVal Titulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VDetalleDescuentoCheque " & Where, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleDescuentoCheque")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleDescuentoCheque"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDescuentoCheque")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Sucursal
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirEfectivizacion(ByVal frmReporte As frmReporte, ByVal IDTransaccion As Integer, ByVal Responsable As String, ByVal Titulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VEfectivizacion Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleEfectivizacion Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VEfectivizacion")
                DSDatos.Tables.Add("VDetalleEfectivizacion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VEfectivizacion"))
                aTable2.Fill(DSDatos.Tables("VDetalleEfectivizacion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptEfectivizacion")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteCuentasACobrar
        Inherits CReporte

        Sub InventarioDocumentoPendiente(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VInventarioDocumentosPendientes")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendiente")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"


                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InventarioDocumentoPendienteVencimientoCliente(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VInventarioDocumentosPendientes")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendienteVencimientoCliente")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"


                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InventarioDocumentoPendienteCobradorFechaVencimiento(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VInventarioDocumentosPendientes")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendienteCobradorFechaVencimiento")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"


                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InventarioDocumentoPendienteVendedorFechaVencimiento(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VInventarioDocumentosPendientes")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendienteVendedorFechaVencimiento")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"


                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InventarioDocumentoPendientePorComprobante(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VInventarioDocumentosPendientesLote  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VInventarioDocumentosPendientesLote")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientesLote"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendientePorComprobante")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"


                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InventarioDocumentoPendienteDireccionAlternativa(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VInventarioDocumentosPendientes")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendienteDireccionAlternativa")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"


                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DistribucionSaldo(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Cond As String, ByVal Responsable As String, ByVal Fecha1 As String, ByVal Fecha2 As String, ByVal Fecha3 As String, ByVal Fecha4 As String, ByVal Fecha5 As String, ByVal Fecha6 As String, ByVal Fecha7 As String, ByVal Fecha8 As String, ByVal Fecha9 As String, ByVal Dia1 As String, ByVal Dia2 As String, ByVal Dia3 As String, ByVal Dia4 As String, ByVal Dia5 As String, ByVal Dia6 As String, ByVal Dia7 As String, ByVal Dia8 As String, ByVal Dia9 As String, ByVal Refencia As String, ByVal Moneda As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDistribucionSaldo  " & Where & "  " & Cond & "" & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDistribucionSaldo")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDistribucionSaldo"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDistribucionSaldo")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha1

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha3

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha4

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha5

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha6
                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha7"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha7

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha8"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha8

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha9"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha9

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia1

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia2

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia3

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia4

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia5

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia6

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia7"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia7

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia8"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia8

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia9"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia9

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txt10"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia9


                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ResumenMovimientoSaldo(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Fecha1 As String, ByVal Fecha2 As String, ByVal TipoInforme As String, Optional ByVal ASC As Boolean = True)

            'Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewResumenMovimientosSaldoCobro " & Where)

            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, ASC)

            'Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptResumendeMovimientoSaldo")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Fecha
                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ResumenMovimientoSaldoDetallado(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Fecha1 As String, ByVal Fecha2 As String, ByVal TipoInforme As String, Optional ByVal ASC As Boolean = True)

            'Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewResumeMovimientosSaldoCobroDetallado " & Where)

            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, ASC)

            'Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpResumenMovimientoSaldoDetallado")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Fecha
                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DistribucionSaldoCobro(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Cond As String, ByVal Responsable As String, ByVal Fecha1 As String, ByVal Fecha2 As String, ByVal Fecha3 As String, ByVal Fecha4 As String, ByVal Fecha5 As String, ByVal Fecha6 As String, ByVal Fecha7 As String, ByVal Fecha8 As String, ByVal Fecha9 As String, ByVal Dia1 As String, ByVal Dia2 As String, ByVal Dia3 As String, ByVal Dia4 As String, ByVal Dia5 As String, ByVal Dia6 As String, ByVal Dia7 As String, ByVal Dia8 As String, ByVal Dia9 As String, ByVal Referencia As String, ByVal Moneda As String, Optional ByVal ASC As Boolean = True)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            'Dim aTable1 As New SqlDataAdapter("Execute SPDistribucionPago " & Where, conn)
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewDistribucionSaldoCobro " & Where)

            CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, ASC)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                'DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDistribucionSaldo")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha1

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha3

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha4

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha5

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha6
                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha7"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha7

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha8"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha8

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha9"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha9



                'Dia
                Dia1 = Dia1 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia1

                'Dia
                Dia2 = Dia2 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia2

                'Dia
                Dia3 = Dia3 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia3

                'Dia
                Dia4 = Dia4 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia4

                'Dia
                Dia5 = Dia5 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia5

                'Dia
                Dia6 = Dia6 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia6

                'Dia
                Dia7 = Dia7 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia7"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia7

                'Dia
                Dia8 = Dia8 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia8"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia8

                'Dia
                Dia9 = Dia9 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia9"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia9

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


    End Class

    Public Class CReporteCuentasAPagar
        Inherits CReporte

        Sub InventarioDocumentoPendientePago(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Cond As String, ByVal Responsable As String, ByVal Titulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VInventarioDocumentosPendientesPago  " & Where & "  " & Cond & "" & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VInventarioDocumentosPendientesPago")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientesPago"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendientePago")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DistribucionSaldosPago(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Fecha1 As Date, ByVal Fecha2 As Date, ByVal Fecha3 As Date, ByVal Fecha4 As Date, ByVal Fecha5 As Date, ByVal Fecha6 As Date, ByVal Dia1 As String, ByVal Dia2 As String, ByVal Dia3 As String, ByVal Dia4 As String, ByVal Dia5 As String, ByVal Dia6 As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, Optional ByVal ASC As Boolean = True)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            'Dim aTable1 As New SqlDataAdapter("Execute SPDistribucionPago " & Where, conn)
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewDistribucionSaldoPago " & Where)

            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, ASC)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                'DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDistribucionSaldoPago")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Dia1 y Fecha1
                Dia1 = Dia1 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia1

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha1

                'Dia2 y Fecha2
                Dia2 = Dia2 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia2

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2

                'Dia3 y Fecha3
                Dia3 = Dia3 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia3

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha3

                'Dia4 y Fecha4
                Dia4 = Dia4 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia4

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha4

                'Dia5 y Fecha5
                Dia5 = Dia5 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia5

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha5

                'Dia6 y Fecha6
                Dia6 = Dia6 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia6

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha6

                'Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'TipoInforme
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                'Report.SetParameterValue("@Fecha1", Fecha1)
                'Report.SetParameterValue("@Fecha2", Fecha2)
                'Report.SetParameterValue("@Fecha3", Fecha3)
                'Report.SetParameterValue("@Fecha4", Fecha4)
                'Report.SetParameterValue("@Fecha5", Fecha5)
                'Report.SetParameterValue("@Fecha6", Fecha6)
                'Report.SetParameterValue("@Condicion", Condicion)
                'Report.SetParameterValue("@FechaReferencia", FechaReferencia)
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DocumentoPagado(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As Date, ByVal Al As Date)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDocumentoPagado" & Where & "" & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDocumentoPagado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDocumentoPagado"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDocumentoPagado")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Fecha Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del

                'Fecha Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub MovimientoSaldoPagoResumen(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal WhereDetalle As String, ByVal Fecha As Date, ByVal Moneda As String, ByVal OrdernadoPor As String, ByVal OrderBy As String, ByVal Top As String, ByVal Orden As Boolean)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewResumenMovimientosSaldosPago " & Where)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, Orden)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptMovimientoSaldoProveedor")
                Report.SetDataSource(DSDatos)

                'Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Fecha Vencido
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda

                'Ordenado por 
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtOrdenado"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = OrdernadoPor

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                'Report.SetParameterValue("@Fecha1", Fecha1)
                'Report.SetParameterValue("@Fecha2", Fecha2)
                'Report.SetParameterValue("@Fecha3", Fecha3)
                'Report.SetParameterValue("@Fecha4", Fecha4)
                'Report.SetParameterValue("@Fecha5", Fecha5)
                'Report.SetParameterValue("@Fecha6", Fecha6)
                'Report.SetParameterValue("@Condicion", Condicion)
                'Report.SetParameterValue("@FechaReferencia", FechaReferencia)
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub MovimientoSaldoPagoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal WhereDetalle As String, ByVal Fecha1 As Date, ByVal Fecha2 As Date, ByVal Moneda As String, ByVal OrdernadoPor As String, ByVal OrderBy As String, ByVal Top As String, ByVal Orden As Boolean)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewResumenMovimientosSaldosPagoDetalle " & Where)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, Orden)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptMovimientoSaldoProveedorDetalle")
                Report.SetDataSource(DSDatos)

                'Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Fecha Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha1

                'Fecha Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda

                'Ordenado por 
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtOrdenadoPor"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = OrdernadoPor

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                'Report.SetParameterValue("@Fecha1", Fecha1)
                'Report.SetParameterValue("@Fecha2", Fecha2)
                'Report.SetParameterValue("@Fecha3", Fecha3)
                'Report.SetParameterValue("@Fecha4", Fecha4)
                'Report.SetParameterValue("@Fecha5", Fecha5)
                'Report.SetParameterValue("@Fecha6", Fecha6)
                'Report.SetParameterValue("@Condicion", Condicion)
                'Report.SetParameterValue("@FechaReferencia", FechaReferencia)
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteFondoFijo
        Inherits CReporte

        Sub Rendicion(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal Responsable As String, ByVal Reponer As Integer, ByVal Saldo As Integer, ByVal Tope As Integer, ByVal Fecha As Date)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                conn.Open()
                dt.TableName = "VDetalleFondoFijo"
                DSDatos.Tables.Add(dt)
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComprobanteReponer")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Reponer
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtReponer"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Reponer)

                'Saldo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSaldo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Saldo)

                'Tope
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTope"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Tope)


                'Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVales(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VVale " & Where, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VVale")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVale"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVale")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteBanco
        Inherits CReporte

        Sub ExtractoMovimientoBancario(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Fecha1 As String, ByVal Fecha2 As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VExtractoMovimientoBancarioSaldo " & Where & "  " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VExtractoMovimientoBancarioSaldo")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExtractoMovimientoBancarioSaldo"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpExtractoMovimientoBancario")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                ''Fecha

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha1

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2


                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub SaldoBanco()

            'Variable 
            Dim ID As Integer
            Dim IDTransaccion As Integer
            Dim Where As String = ""

            Dim param(-1) As SqlClient.SqlParameter

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)

            CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "", ParameterDirection.Output, 18)

            Dim MensajeRetorno As String = ""

            'Insertar Registro
            CSistema.ExecuteStoreProcedure(param, "SpViewSaldoBancario", False, False, MensajeRetorno, IDTransaccion)

            ID = IDTransaccion

            Where = "Where ID=" & ID

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From SaldoBanco " & Where & "", conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("SaldoBanco")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("SaldoBanco"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptSaldoBanco")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"


                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDepositoBancario(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDepositoBancario " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDepositoBancario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDepositoBancario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoDepositoBancario")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDebitoCreditoBancario(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDebitoCreditoBancario " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDebitoCreditoBancario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDebitoCreditoBancario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoDebitoCreditoBancario")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


    End Class

    Public Class CCliente
        Inherits CReporte
        Sub ExtractoMovimientoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal Moneda As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewExtractoMovimientoCliente " & Where)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExtractoMovimientoCliente")
                Report.SetDataSource(DSDatos)

                'Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Fecha Inicio
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFechaInicio"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = FechaInicio

                'Fecha Del
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = FechaInicio

                'Fecha Al
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = FechaFin

                'Fecha Al2
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = FechaFin

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                'Report.SetParameterValue("@Fecha1", Fecha1)
                'Report.SetParameterValue("@Fecha2", Fecha2)
                'Report.SetParameterValue("@Fecha3", Fecha3)
                'Report.SetParameterValue("@Fecha4", Fecha4)
                'Report.SetParameterValue("@Fecha5", Fecha5)
                'Report.SetParameterValue("@Fecha6", Fecha6)
                'Report.SetParameterValue("@Condicion", Condicion)
                'Report.SetParameterValue("@FechaReferencia", FechaReferencia)
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal OrderBy As String, ByVal Orden As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VCliente " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCliente")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Fecha Inicio
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtOrdenadoPor"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Orden

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoClienteNoCompra(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal OrderBy As String, ByVal Orden As String, ByVal Responsable As String, ByVal Desde As Date, ByVal Hasta As Date)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VCliente Where UltimaCompra not in(Select UltimaCompra From VCliente " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoClienteNoCompra")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Fecha Inicio
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtOrdenadoPor"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Orden

                'Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                'Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CProveedor
        Inherits CReporte

        Sub ExtractoMovimientoProveedor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal FechaInicio As String, ByVal FechaFin As String, ByVal Moneda As String, ByVal Responsable As String, ByVal FechaDesdeHasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewExtractoMovimientoProveedor " & Where)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExtractoMovimientoProveedor")
                Report.SetDataSource(DSDatos)

                'Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Fecha Inicio
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFechaInicio"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = FechaInicio

                'FechaDesdeHasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFechaDesdeHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = FechaDesdeHasta

                ''Fecha Del
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = FechaInicio

                ''Fecha Al
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = FechaFin

                'Fecha Al2
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = FechaFin

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                'Report.SetParameterValue("@Fecha1", Fecha1)
                'Report.SetParameterValue("@Fecha2", Fecha2)
                'Report.SetParameterValue("@Fecha3", Fecha3)
                'Report.SetParameterValue("@Fecha4", Fecha4)
                'Report.SetParameterValue("@Fecha5", Fecha5)
                'Report.SetParameterValue("@Fecha6", Fecha6)
                'Report.SetParameterValue("@Condicion", Condicion)
                'Report.SetParameterValue("@FechaReferencia", FechaReferencia)
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ListadoProveedor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal OrderBy As String, ByVal Orden As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VProveedor " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VProveedor")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProveedor"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoProveedor")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Fecha Inicio
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtOrdenadoPor"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Orden

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteAsiento
        Inherits CReporte
        Sub AsientoDiario(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim SQL As String
            SQL = "Select  * From VDetalleAsientoContable " & Where & " Order By Orden   "
            Dim aTable1 As New SqlDataAdapter(SQL, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleAsientoContable")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleAsientoContable"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptAsientoDiario")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
    End Class

    Public Class CReportePedido
        Inherits CReporte

        Sub ImprimirPedido(ByRef frmReporte As frmReporte, ByRef Where As String, ByRef Impresora As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VPedido " & Where & "  ", conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VPedido")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedido"))

                conn.Close()
                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos

                'If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                '    MsgBox("El archivo no existe!")
                '    Exit Sub
                'End If

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpPedidos")
                Report.SetDataSource(DSDatos)

                'Dim Report As New ReportClass
                'Report.FileName = rptPedidos
                'Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub
    End Class

    Public Class CReporteOrdenPago
        Inherits CReporte

        Sub ImprimirOrdenPago(ByVal frmReporte As frmReporte, ByVal IDTransaccion As Integer, ByVal Responsable As String, ByVal MontoLetra As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VOrdenPago Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VCheque Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable3 As New SqlDataAdapter("Select  * From VOrdenPagoEgreso Where IDTransaccionOrdenPago=" & IDTransaccion, conn)
            Dim aTable4 As New SqlDataAdapter("Select  * From VDetalleAsiento Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable5 As New SqlDataAdapter("Select SUM(Importe)as Importe from VOrdenPagoEfectivo Where IDTransaccionOrdenPago=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VOrdenPago")
                DSDatos.Tables.Add("VCheque")
                DSDatos.Tables.Add("VOrdenPagoEgreso")
                DSDatos.Tables.Add("VDetalleAsiento")
                DSDatos.Tables.Add("VOrdenPagoEfectivo")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VOrdenPago"))
                aTable2.Fill(DSDatos.Tables("VCheque"))
                aTable3.Fill(DSDatos.Tables("VOrdenPagoEgreso"))
                aTable4.Fill(DSDatos.Tables("VDetalleAsiento"))
                aTable5.Fill(DSDatos.Tables("VOrdenPagoEfectivo"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptOrdenPago")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Responsable2
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Monto Letra
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMontoLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = MontoLetra

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirRetencionIVA(ByVal frmReporte As frmReporte, ByVal IDTransaccion As Integer, ByVal IDTransaccionOrdenPago As Integer, ByVal Responsable As String, ByVal MontoLetra As String, ByRef PathReporte As String, ByVal Impresora As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VRetencionIVA Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select  * From VComprobanteRetencion Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VRetencionIVA")
                DSDatos.Tables.Add("VComprobanteRetencion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VRetencionIVA"))
                aTable2.Fill(DSDatos.Tables("VComprobanteRetencion"))
                conn.Close()

                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                    MsgBox("El archivo no existe!")
                    Exit Sub
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & PathReporte
                Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)

                '' Total Letras 
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtMontoLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = MontoLetra


                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)


                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoOrdenPago(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VOrdenPago " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VOrdenPago")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VOrdenPago"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoOrdenPago")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteContabilidad
        Inherits CReporte

        Sub ImprimirPlanCuenta(ByVal frmReporte As frmReporte, ByVal Responsable As String, ByVal IDPlanCuenta As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VCuentaContableReporte Where IDPlanCuenta=" & IDPlanCuenta & " Order By Codigo", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCuentaContable")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCuentaContable"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanCuenta")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirLibroIVA(ByVal frmReporte As frmReporte, ByVal Responsable As String, ByVal WhereDetalle As String, ByVal Where As String, ByVal Titulo As String, ByVal Del As String, ByVal Al As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal Resumido As Boolean = False)

            Dim SQL As String = ""

            If Resumido = False Then
                SQL = "Select " & Top & " * From VLibroIVA " & WhereDetalle & Where & " " & OrderBy
            End If

            If Resumido = True Then
                SQL = "Select " & Top & " * From VLibroIVA " & WhereDetalle & Where & " " & OrderBy
            End If

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter(SQL, conn)



            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VLibroIVA")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLibroIVA"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New Object

                If Resumido = False Then
                    LoadReport(Report, "rptLibroIVA3")
                Else
                    LoadReport(Report, "rptLibroIVAAgrupado")
                End If

                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirLibroMayor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal SubTitulo As String, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewLibroMayor " & Where)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            'Calcular el saldo
            Dim SaldoAnterior As Decimal = 0
            Dim SaldoActual As Decimal = 0
            Dim Debito As Decimal = 0
            Dim Credito As Decimal = 0
            Dim Prefijo As String = 0

            'For Each orow As DataRow In dt.Rows
            '    Debito = orow("Debito")
            '    Credito = orow("Credito")
            '    Prefijo = orow("Codigo").ToString.Substring(0, 1)

            '    If Prefijo = "1" Or Prefijo = "5" Then
            '        SaldoActual = (SaldoAnterior + Debito) - Credito
            '    End If

            '    If Prefijo = "2" Or Prefijo = "3" Or Prefijo = "4" Then
            '        SaldoActual = (SaldoAnterior + Credito) - Debito
            '    End If

            '    orow("Saldo") = SaldoActual

            '    SaldoAnterior = SaldoActual

            'Next

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptLibroMayor")
                Report.SetDataSource(DSDatos)

                'Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''SubTitulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSubTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = SubTitulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                'Report.SetParameterValue("@Fecha1", Fecha1)
                'Report.SetParameterValue("@Fecha2", Fecha2)
                'Report.SetParameterValue("@Fecha3", Fecha3)
                'Report.SetParameterValue("@Fecha4", Fecha4)
                'Report.SetParameterValue("@Fecha5", Fecha5)
                'Report.SetParameterValue("@Fecha6", Fecha6)
                'Report.SetParameterValue("@Condicion", Condicion)
                'Report.SetParameterValue("@FechaReferencia", FechaReferencia)
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirLibroMayorSucursal(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal SubTitulo As String, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal Responsable As String, ByVal IDSucursal As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewLibroMayorSucursal " & Where & "," & IDSucursal)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptLibroMayor")
                Report.SetDataSource(DSDatos)

                'Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''SubTitulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSubTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = SubTitulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                'Report.SetParameterValue("@Fecha1", Fecha1)
                'Report.SetParameterValue("@Fecha2", Fecha2)
                'Report.SetParameterValue("@Fecha3", Fecha3)
                'Report.SetParameterValue("@Fecha4", Fecha4)
                'Report.SetParameterValue("@Fecha5", Fecha5)
                'Report.SetParameterValue("@Fecha6", Fecha6)
                'Report.SetParameterValue("@Condicion", Condicion)
                'Report.SetParameterValue("@FechaReferencia", FechaReferencia)
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirBalanceGeneral(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Responsable As String)

            'Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try


                'Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptBalanceGeneral")
                Report.SetDataSource(dt)

                'Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSubTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = SubTitulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirBalanceComprobacion4(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Responsable As String)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptBalanceComprobacion4")
                Report.SetDataSource(dt)

                'Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'SubTitulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSubTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = SubTitulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirBalanceComprobacion3(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Responsable As String)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptBalanceComprobacion3")
                Report.SetDataSource(dt)

                'Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'SubTitulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSubTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = SubTitulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirComprobanteNoEmitido(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Responsable As String)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComprobantesNoEmitidos")
                Report.SetDataSource(dt)

                'Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'SubTitulo
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSubTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = SubTitulo

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CPedido
        Inherits CReporte
        Sub ListadoPedido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VPedido " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VPedido")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedido"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListaPedido")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Fecha Desde
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Fecha hasta
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoPedidoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VPedido " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetallePedido", conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VPedido")
                DSDatos.Tables.Add("VDetallePedido")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedido"))
                aTable2.Fill(DSDatos.Tables("VDetallePedido"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoPedidoDetalle")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Fecha Desde
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Fecha hasta
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Responsable & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString

                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"

                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteNotaCreditoProveedor
        Inherits CReporte

        Sub ImprimirNotaCreditoProveedor(ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String)

            Dim frmReporte As New frmReporte

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCreditoProveedor " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCreditoProveedor")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCreditoProveedor"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaCreditoProveedor")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"


                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen

                EstablecerConexion(Report)

                frmReporte.Show()

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteNotaDebitoProveedor
        Inherits CReporte
        Sub ImprimirNotaDebitoProveedor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaDebitoProveedor " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaDebitoProveedor")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaDebitoProveedor"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaDebitoProveedor")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteCobranza
        Inherits CReporte

        Sub ListadoCobranzaCredito(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VCobranzaCredito " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCobranzaCredito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCobranzaCredito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCobranzaCredito")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoCobranzaContado(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VCobranzaContado " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCobranzaContado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCobranzaContado"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCobranzaContado")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DetalleDocumentosCobrados(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleDocumentosCobrados " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleDocumentosCobrados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleDocumentosCobrados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDetalleDocumentoCobrado")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''TipoInforme
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ResumenCobranzaDiaria(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal WhereDetalle As String, Optional ByVal ASC As Boolean = True)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt1 As DataTable = CSistema.ExecuteToDataTable("Select * From VResumenCobranzaDiaria2 " & Where)
            Dim dt2 As DataTable = CSistema.ExecuteToDataTable("Select * From VResumenCobranzaDiariaFormaPago " & Where)


            If dt1 Is Nothing Then
                Exit Sub
            End If

            dt1.TableName = "VResumenCobranzaDiaria2"
            dt2.TableName = "VResumenCobranzaDiariaFormaPago"

            DSDatos.Tables.Add(dt1)
            DSDatos.Tables.Add(dt2)


            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptResumenCobranzaDiaria2")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''TipoInforme
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DetalleValoresCobrados(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VDetalleValoresCobrados " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleValoresCobrados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleValoresCobrados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDetalleValoresCobrados")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''TipoInforme
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DetalleValoresCobradosPorComprobante(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VDetalleValoresCobrados " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleValoresCobrados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleValoresCobrados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDetalleValoresCobradosPorComprobante")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''TipoInforme
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteGasto
        Inherits CReporte
        Sub ListadoGasto(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VGasto " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VGasto")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VGasto"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoGasto")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
    End Class

    Public Class CReporteComisionVendedor
        Inherits CReporte
        Sub ListadoComision(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal Vendedor As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VComisionVendedor " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VComisionVendedor")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VComisionVendedor"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpComisionVendedor")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing


                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtVendedor"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Vendedor

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoComisionDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal Vendedor As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VComisionVendedor " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VComisionVendedor")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VComisionVendedor"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComisionVendedorDetalle")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing


                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtVendedor"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Vendedor

                ''Responsable
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtResponsable"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString


                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtSistema"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = "ERP Software" & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.erp.com.py"



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True
                frmReporte.Size = New Size(frmMain.ClientSize.Width - 75, frmMain.ClientSize.Height - 75)
                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
    End Class

End Namespace

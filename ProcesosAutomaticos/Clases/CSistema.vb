﻿Imports System.Data.SqlClient
Imports System.Data.Odbc
Imports System.Management
Imports Scripting
Imports System.IO


Public Class CSistema
    Inherits CError


#Region "PROPIEDADES"

#End Region

#Region "ESTRUCTURAS, TIPOS DE DATOS, ENUMERADORES"

    ''' <summary>
    ''' Enumeracion para definir el comportamiento de los botonos ABM
    ''' </summary>
    ''' <remarks></remarks>
    Enum NUMHabilitacionBotonesABM

        INICIO = 0
        NUEVO = 1
        EDITAR = 2
        CANCELAR = 3
        GUARDAR = 4
        ELIMINAR = 5
        EDITANDO = 6

    End Enum

    ''' <summary>
    ''' Enumeracion para definir el comportamiento de los botonos de REGISTROS
    ''' </summary>
    ''' <remarks></remarks>
    Enum NUMHabilitacionBotonesRegistros

        INICIO = 0
        NUEVO = 1
        GUARDAR = 2
        CANCELAR = 3
        ANULAR_ELIMINAR = 4
        IMPRIMIR = 6
        BUSQUEDA = 7
        MODIFICAR = 10

    End Enum

    ''' <summary>
    ''' Enumeracion para definir las operaciones posibles en la base de datos Insertar, Actualizar o Eliminar
    ''' </summary>
    ''' <remarks></remarks>
    Enum NUMOperacionesABM

        INS = 1
        UPD = 2
        DEL = 3

    End Enum

    ''' <summary>
    ''' Enumeracion para definir las operaciones posibles en la base de datos Agregar, Anular, Eliminar
    ''' </summary>
    ''' <remarks></remarks>
    Enum NUMOperacionesRegistro

        INS = 1
        DEL = 3
        ANULAR = 4
        UPD = 2

        DEL1 = 5 'JGR 20140820 Agregue para más una opcion de eliminacion. SpVentaLoteDistribucion (frmLoteDistribucion)

    End Enum

    ''' <summary>
    ''' Enumeracion para definir las operaciones posibles en la base de datos Agregar, Anular, Eliminar
    ''' </summary>
    ''' <remarks></remarks>
    Enum NUMFechaPreestablecida

        HOY = 1
        AYER = 2
        ESTA_SEMANA = 3
        ESTE_MES = 5
        ESTE_AÑO = 6

    End Enum

    ''' <summary>
    ''' Tipos de Operaciones realizadas por el usuario, se utiliza especialmente por auditoria
    ''' </summary>
    ''' <remarks></remarks>
    Enum NUMTipoOperaciones

        ''' <summary>
        ''' Ingreso a formulario
        ''' </summary>
        ''' <remarks></remarks>
        ING = 1
        ''' <summary>
        ''' Salida de Formulario
        ''' </summary>
        ''' <remarks></remarks>
        SAL = 2

        ''' <summary>
        ''' Insertar registro
        ''' </summary>
        ''' <remarks></remarks>
        INS = 10
        ''' <summary>
        ''' Actualizar Registro
        ''' </summary>
        ''' <remarks></remarks>
        ACT = 11
        ''' <summary>
        ''' Eliminar registro
        ''' </summary>
        ''' <remarks></remarks>
        ELI = 12
        ''' <summary>
        ''' Anular registro
        ''' </summary>
        ''' <remarks></remarks>
        ANU = 13

        ''' <summary>
        ''' Imprimir documento
        ''' </summary>
        ''' <remarks></remarks>
        IMP = 20

        ''' <summary>
        ''' Ingreso de Sistema
        ''' </summary>
        ''' <remarks></remarks>
        ISI = 30
        ''' <summary>
        ''' Salida de Sistema
        ''' </summary>
        ''' <remarks></remarks>
        SSI = 31

    End Enum


    ''' <summary>
    ''' Enumeracion para definir el comportamiento de los botonos ABM
    ''' </summary>
    ''' <remarks></remarks>
    Enum ENUMPosicionesNavegar

        PRIMERO = 0
        ANTERIOR = 1
        SIGUIENTE = 2
        ULTIMO = 3

    End Enum

#End Region

#Region "CONEXION CON BASE DE DATOS"

    ''' <summary>
    ''' Ejecuta un Procedimiento Almacenado en la Base de Datos, retorna el mensaje de la ejecucion.
    ''' </summary>
    ''' <param name="Parametros">Vector de tipo SqlParameter, esta coleccion guarda los parametros del Procedimiento Almacenado como: Nombre, Tipo de Dato, Ent/Sal, etc</param>
    ''' <param name="StoreProcedure">Indica como se llama el Procedimiento Almacenado en la Base de Datos</param>
    ''' <param name="MostrarMensaje">Opcional: TRUE si se quiere mostrar un mensaje si se proceso correctamente</param>
    ''' <param name="MostrarMensajeError">Opcional: TRUE si se quiere mostrar un mensaje si ocurrio un problema en el Procedimiento Almacenado</param>
    ''' <param name="Mensaje">Opcional: Informa de lo ocurrido en el la Base de Datos</param>
    ''' <param name="IDTransaccionSalida">Opcional: Retorna el IDTransaccion creada para el registro</param>
    ''' <returns>Retorna el mensaje del proceso</returns>
    ''' <remarks></remarks>
    Public Function ExecuteStoreProcedure(ByVal Parametros() As SqlParameter, ByVal StoreProcedure As String, Optional ByVal MostrarMensaje As Boolean = True, Optional ByVal MostrarMensajeError As Boolean = True, Optional ByRef Mensaje As String = "", Optional ByRef IDTransaccionSalida As String = "", Optional ByVal Auditoria As Boolean = False, Optional ByVal vConexion As String = "") As Boolean

        ExecuteStoreProcedure = False

        If vConexion = "" Then
            vConexion = VGCadenaConexion
        End If

        Try
            Dim Capturador As String = "Exec " & StoreProcedure & vbCrLf
            Dim rowCount As Integer
            Dim cmd As New SqlCommand
            Dim conn As New SqlConnection(vConexion)

            Dim previousConnectionState As ConnectionState
            previousConnectionState = conn.State
            Try
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If

                'Establecemos las propiedades preliminares
                cmd.Connection = conn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = StoreProcedure

                'Agregar la auditoria
                If Auditoria = True Then
                    cmd.Parameters.AddWithValue("@IDUsuario", 0)
                    cmd.Parameters.AddWithValue("@IDTerminal", 0)
                End If

                'Establecemos los parametros
                'Entrada
                For i = 0 To Parametros.GetLength(0) - 1
                    cmd.Parameters.Add(Parametros(i))
                    If i = 0 Then
                        Capturador = Capturador & Parametros(i).ParameterName & "='" & Parametros(i).Value & "'" & vbCrLf
                    Else
                        Capturador = Capturador & "," & Parametros(i).ParameterName & "='" & Parametros(i).Value & "'" & vbCrLf
                    End If
                Next

                'Ejecutamos el sp y evaluamos la cantidad de filas insertadas
                rowCount = cmd.ExecuteNonQuery()

                If rowCount > 0 Then
                    If MostrarMensaje = True Then
                        MessageBox.Show(CStr(cmd.Parameters("@Mensaje").Value.ToString), "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else

                        If cmd.Parameters.IndexOf("@Mensaje") >= 0 Then
                            Mensaje = CStr(cmd.Parameters("@Mensaje").SqlValue.ToString)
                        End If

                        If IDTransaccionSalida <> "" Then
                            IDTransaccionSalida = CStr(cmd.Parameters("@IDTransaccionSalida").SqlValue.ToString)
                        End If

                    End If

                    Return True

                Else
                    If MostrarMensajeError = True Then
                        MessageBox.Show(CStr(cmd.Parameters("@Mensaje").Value.ToString), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Else
                        Mensaje = CStr(cmd.Parameters("@Mensaje").Value.ToString)
                    End If

                    Return False

                End If
            Catch ex As Exception
                CargarError(ex, "CSistema", "ExecuteStoreProcedure", cmd.Parameters.ToString, "Nombre del SP: " & StoreProcedure, MostrarMensajeError)
                Mensaje = ex.Message
                ExecuteStoreProcedure = False
            Finally
                If previousConnectionState = ConnectionState.Closed Then
                    conn.Close()
                End If
            End Try
        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteStoreProcedure", "", "Nombre del SP: " & StoreProcedure)
            Mensaje = ex.Message
            Return False
        End Try

    End Function
    Public Function ExecuteStoreProcedure2(ByVal Parametros() As SqlParameter, ByVal StoreProcedure As String, Optional ByVal MostrarMensaje As Boolean = True, Optional ByVal MostrarMensajeError As Boolean = True, Optional ByRef Mensaje As String = "", Optional ByRef IDTransaccionSalida As String = "", Optional ByVal Auditoria As Boolean = False, Optional ByVal vConexion As String = "") As Boolean

        ExecuteStoreProcedure2 = False


        If vConexion = "" Then
            vConexion = VGCadenaConexion
        End If

        Try
            Dim Capturador As String = "Exec " & StoreProcedure & vbCrLf
            Dim rowCount As Integer
            Dim cmd As New SqlCommand
            Dim conn As New SqlConnection(vConexion)

            Dim previousConnectionState As ConnectionState
            previousConnectionState = conn.State
            Try
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If

                'Establecemos las propiedades preliminares
                cmd.Connection = conn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = StoreProcedure

                'Agregar la auditoria
                If Auditoria = True Then
                    cmd.Parameters.AddWithValue("@IDUsuario", 0)
                    cmd.Parameters.AddWithValue("@IDTerminal", 0)
                End If

                'Establecemos los parametros
                'Entrada
                For i = 0 To Parametros.GetLength(0) - 1
                    cmd.Parameters.Add(Parametros(i))
                    If i = 0 Then
                        Capturador = Capturador & Parametros(i).ParameterName & "='" & Parametros(i).Value & "'" & vbCrLf
                    Else
                        Capturador = Capturador & "," & Parametros(i).ParameterName & "='" & Parametros(i).ToString & "'" & vbCrLf
                    End If
                Next

                'Ejecutamos el sp y evaluamos la cantidad de filas insertadas
                rowCount = cmd.ExecuteNonQuery()

                If rowCount > 0 Then
                    If MostrarMensaje = True Then
                        MessageBox.Show(CStr(cmd.Parameters("@Mensaje").Value.ToString), "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else

                        If cmd.Parameters.IndexOf("@Mensaje") >= 0 Then
                            Mensaje = CStr(cmd.Parameters("@Mensaje").SqlValue.ToString)
                        End If

                        If IDTransaccionSalida <> "" Then
                            IDTransaccionSalida = CStr(cmd.Parameters("@IDTransaccionSalida").SqlValue.ToString)
                        End If

                    End If

                    Return True

                Else
                    If MostrarMensajeError = True Then
                        MessageBox.Show(CStr(cmd.Parameters("@Mensaje").Value.ToString), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Else
                        Mensaje = CStr(cmd.Parameters("@Mensaje").Value.ToString)
                    End If

                    Return False

                End If
            Catch ex As Exception
                CargarError(ex, "CSistema", "ExecuteStoreProcedure", cmd.Parameters.ToString, "Nombre del SP: " & StoreProcedure, MostrarMensajeError)
                Mensaje = ex.Message
                ExecuteStoreProcedure2 = False
            Finally
                If previousConnectionState = ConnectionState.Closed Then
                    conn.Close()
                End If
            End Try
        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteStoreProcedure", "", "Nombre del SP: " & StoreProcedure)
            Mensaje = ex.Message
            Return False
        End Try

    End Function

    ''' <summary>
    ''' Retorna un DataTable segun la consulta pasada por parametro
    ''' </summary>
    ''' <param name="consulta">Cualquier consulta tipo TSQL</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExecuteToDataTable(ByVal consulta As String, Optional ByVal vConexion As String = "", Optional ByVal Timeout As Integer = 60) As DataTable

        ExecuteToDataTable = Nothing

        Try

            If vConexion = "" Then
                vConexion = VGCadenaConexion
            End If

            Using Adapter As New SqlDataAdapter(consulta, vConexion)
                Dim dt As New DataTable
                'Adapter.ContinueUpdateOnError = True
                Adapter.SelectCommand.CommandTimeout = Timeout
                Adapter.Fill(dt)
                Adapter.SelectCommand.Connection.Close()
                Adapter.Dispose()
                Return (dt)
            End Using

        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteToDataTable", "", consulta)
            Return Nothing

        End Try


    End Function

    Public Function ExecuteToDataTableDBF(ByVal consulta As String, ByVal Path As String, Optional ByVal vConexion As String = "") As DataTable

        ExecuteToDataTableDBF = New DataTable

        Try


            If vConexion = "" Then
                vConexion = VGCadenaConexion
            End If

            Dim sConn As String = "Provider=vfpoledb.1;Data Source=" & Path & ";Collating Sequence=general;"
            Dim dbConn As New System.Data.OleDb.OleDbConnection(sConn)

            dbConn.Open()

            Dim Adapter As New System.Data.OleDb.OleDbDataAdapter(consulta, dbConn)
            Dim dt As New DataTable

            Adapter.Fill(dt)

            dbConn.Close()

            Return dt

        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteToDataTable", "", consulta)
            Return Nothing
        End Try


    End Function

    Public Function ExecuteToDataTableXLS(ByVal consulta As String, ByVal Archivo As String, Optional ByVal vConexion As String = "") As DataTable

        ExecuteToDataTableXLS = Nothing
        Try

            If vConexion = "" Then
                vConexion = VGCadenaConexion
            End If

            Dim MiConexion As New System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Archivo & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'")
            Dim MiAdaptador As New System.Data.OleDb.OleDbDataAdapter(consulta, MiConexion)
            Dim MiDataSet As New DataSet()
            Dim MiEnlazador As New BindingSource

            Dim commandbuilder As New OleDb.OleDbCommandBuilder(MiAdaptador)
            MiConexion.Open()
            MiAdaptador.Fill(MiDataSet)
            Return MiDataSet.Tables(0)

        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteToDataTable", "", consulta)
            Return Nothing
        End Try


    End Function

    Public Function ExecuteToDataTableTXT(ByVal path As String, Optional ByVal vConexion As String = "") As DataTable
        ExecuteToDataTableTXT = Nothing

        Try

            If vConexion = "" Then
                vConexion = VGCadenaConexion
            End If

            Dim dt As New DataTable
            Dim myStream As System.IO.StreamReader = New System.IO.StreamReader(path)
            Dim Linea As String
            Dim Separador As String = vbTab
            Dim Cabecera As Boolean = True
            Do
                Linea = myStream.ReadLine()
                If Linea Is Nothing Then
                    Exit Do
                End If

                Dim Vector As String() = Split(Linea, Separador)

                If Cabecera = True Then

                    For i As Integer = 0 To Vector.GetLength(0) - 1
                        dt.Columns.Add(Vector(i).ToString)
                    Next

                    Cabecera = False

                Else

                    'Cargamos los registros
                    Dim NewRow As DataRow = dt.NewRow
                    For i As Integer = 0 To Vector.GetLength(0) - 1
                        NewRow(i) = Vector(i).ToString
                    Next

                    dt.Rows.Add(NewRow)

                End If

            Loop

            myStream.Close()

            Return dt

        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteToDataTableTXT", "", path)
            Return Nothing
        End Try



    End Function


    Public Function DataTableToTXT(vdt As DataTable, Path As String) As Boolean

        DataTableToTXT = False

        If IO.File.Exists(Path) = True Then
            Try
                IO.File.Delete(Path)
            Catch ex As Exception
                MessageBox.Show("No se pudo Generar", "Atención", MessageBoxButtons.OK)
                Return False
            End Try
        End If

        Dim sw As New System.IO.StreamWriter(Path, True, System.Text.Encoding.Unicode)

        Try
            'CABECERA
            Dim cadena As String = ""
            For c As Integer = 0 To vdt.Columns.Count - 1
                If c = vdt.Columns.Count - 1 Then
                    cadena = cadena & vdt.Columns(c).ColumnName
                Else
                    cadena = cadena & vdt.Columns(c).ColumnName & vbTab
                End If
            Next

            sw.WriteLine(cadena)

            'DETALLE
            For Each oRow As DataRow In vdt.Rows
                cadena = ""
                For c As Integer = 0 To vdt.Columns.Count - 1
                    If c = vdt.Columns.Count - 1 Then
                        cadena = cadena & oRow(c).ToString
                    Else
                        cadena = cadena & oRow(c).ToString & vbTab
                    End If
                Next

                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            sw.Close()
            Return False
        End Try


        Return True

    End Function

    ''' <summary>
    ''' Retorna un SqlDataReader segun la consulta pasada por parametro
    ''' </summary>
    ''' <param name="consulta">Cualquier consulta tipo TSQL</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 
    Public Function ExecuteReader(ByVal consulta As String, Optional ByVal vConexion As String = "") As SqlDataReader


        ExecuteReader = Nothing

        If vConexion = "" Then
            vConexion = VGCadenaConexion
        End If

        Dim cmd As New SqlCommand
        Dim conn As New SqlConnection(vConexion)
        Dim reader As SqlDataReader
        Dim previousConnectionState As ConnectionState = conn.State
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            reader = cmd.ExecuteReader()
            Using reader
                While reader.Read
                    ' Process SprocResults datareader here.
                    Console.WriteLine(reader.GetValue(0))
                End While
            End Using
        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteReader", "", consulta)
        Finally
            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
        End Try



    End Function

    ''' <summary>
    ''' Sirve para extraer alguna informacion especifica de la base de datos. No confundir con ExecuteReader
    ''' </summary>
    ''' <param name="Consulta">Consulta TSQL, debe ser especifica, o sea, se debe solicitar un unico valor.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExecuteScalar(ByVal Consulta As String, Optional ByVal vConexion As String = "", Optional ByVal Timeout As Integer = 10) As Object

        ExecuteScalar = Nothing
        Dim cadena As String = Nothing

        If vConexion = "" Then
            vConexion = VGCadenaConexion
        End If
        cadena = vConexion
        If Not cadena.Contains("Timeout") Then
            cadena = cadena & ";Connection Timeout=" & Timeout.ToString
        End If

        Dim conn As New SqlConnection(cadena)
        Dim cmd As New SqlCommand(Consulta, conn)
        Dim result As Object
        Dim previousConnectionState As ConnectionState = conn.State
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            result = cmd.ExecuteScalar()
            Return result
        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteScalar", "", Consulta)
        Finally
            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
        End Try

    End Function

    Public Function ExecuteNonQuery(ByVal Consulta As String, Optional ByVal vConexion As String = "", Optional ByVal Timeout As Integer = 30) As Integer

        ExecuteNonQuery = 0

        If vConexion = "" Then
            vConexion = VGCadenaConexion
        End If

        Dim conn As New SqlConnection(vConexion)
        Dim cmd As New SqlCommand(Consulta, conn)
        cmd.CommandTimeout = Timeout
        Dim result As Integer
        Dim previousConnectionState As ConnectionState = conn.State
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            result = cmd.ExecuteNonQuery
            Return result
        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteNonQuery", "", Consulta)
            Debug.Print(ex.Message)
        Finally
            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
        End Try

    End Function

    Public Function CerrarConexion(Optional ByVal vConexion As String = "") As Boolean

        CerrarConexion = 0

        If vConexion = "" Then
            vConexion = VGCadenaConexion
        End If

        Dim conn As New SqlConnection(vConexion)
        Dim previousConnectionState As ConnectionState = conn.State
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            Return True
        Catch ex As Exception
            CargarError(ex, "CSistema", "CerrarConexion", "", "")
        Finally
            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
            SqlConnection.ClearAllPools()
        End Try

    End Function

    Public Function SPToString(ByVal parametros() As SqlParameter, ByVal StoreProcedure As String, Optional ByVal DeclararVariablesSalida As Boolean = True) As String

        SPToString = ""

        Dim Variables As String = ""
        Dim Datos As String = ""
        Dim Salida As String = ""

        'Variables
        If DeclararVariablesSalida = True Then
            For i = 0 To parametros.GetLength(0) - 1
                If parametros(i).Direction = ParameterDirection.Output Then
                    If Variables = "" Then
                        Variables = "Declare " & parametros(i).ParameterName & " " & parametros(i).SqlDbType.ToString
                    Else
                        Variables = Variables & ", " & parametros(i).ParameterName & " " & parametros(i).SqlDbType.ToString
                    End If
                End If
            Next
        End If

        'Datos
        For i = 0 To parametros.GetLength(0) - 1
            If parametros(i).Direction = ParameterDirection.Input Then
                If Datos = "" Then
                    Datos = " " & parametros(i).ParameterName & " = '" & parametros(i).Value & "'"
                Else
                    Datos = Datos & ", " & parametros(i).ParameterName & " = '" & parametros(i).Value & "'"
                End If
            End If
        Next

        'Datos
        For i = 0 To parametros.GetLength(0) - 1
            If parametros(i).Direction = ParameterDirection.Output Then
                If Salida = "" Then
                    Salida = ", " & parametros(i).ParameterName & " = " & parametros(i).ParameterName
                Else
                    Salida = Salida & ", " & parametros(i).ParameterName & " = " & parametros(i).ParameterName
                End If
            End If
        Next

        SPToString = Variables & " Execute " & StoreProcedure & " " & Datos & " " & Salida

    End Function

    Public Function InsertSQL(ByVal dt As DataTable, ByVal tabla As String) As String

        InsertSQL = "INSERT INTO " & tabla
        Dim Campos As String = ""
        Dim Valores As String = ""

        'Generar los campos
        For Each oRow As DataRow In dt.Rows
            If Campos = "" Then
                Campos = "(" & oRow("Campo")
            Else
                Campos = Campos & "," & oRow("Campo")
            End If
        Next

        Campos = Campos & ")"

        'Generar los valores
        For Each oRow As DataRow In dt.Rows
            If Valores = "" Then
                Valores = " VALUES('" & oRow("Valor") & "'"
            Else
                Valores = Valores & ",'" & oRow("Valor") & "'"
            End If
        Next

        Valores = Valores & ")"

        InsertSQL = InsertSQL & Campos & Valores

    End Function

    Public Function FuncionPermitida(ByVal IDPerfil As Integer, ByVal frm As Form, ByVal Funcion As String) As Boolean
        Dim Retorno As Boolean = False

        Retorno = ExecuteScalar("Select Habilitado from vaccesoespecificoperfil where IDPerfil = " & IDPerfil & "and Tag= '" & frm.Name & "' and NombreFuncion = '" & Funcion & "'")
        If IDPerfil = 1 Then
            Retorno = True
        End If

        Return Retorno

    End Function


    Function SetSQLParameter(ByVal NombreParametro As String, ByVal Valor As String, ByVal Tipo As ParameterDirection, Optional ByVal Tamaño As Integer = 0, Optional ByVal TipoDato As SqlDbType = SqlDbType.VarChar) As SqlParameter
        SetSQLParameter = Nothing

        Dim Retorno As New SqlParameter
        Retorno.ParameterName = NombreParametro
        Retorno.Value = Valor
        Retorno.Direction = Tipo
        Retorno.SqlDbType = TipoDato

        If Tamaño > 0 Then
            Retorno.Size = Tamaño
        End If

        Return Retorno

    End Function

    Sub SetSQLParameter(ByRef SQLParametros() As SqlParameter, ByVal NombreParametro As String, ByVal valor As Byte(), ByVal Tipo As ParameterDirection, Optional ByVal Tamaño As Integer = 0, Optional ByVal TipoDato As SqlDbType = SqlDbType.Image)

        Dim Retorno As New SqlParameter
        Retorno.ParameterName = NombreParametro

        If IsNothing(valor) = False Then
            Retorno.Value = valor
        End If

        Retorno.Direction = Tipo
        Retorno.SqlDbType = TipoDato

        If Tamaño > 0 Then
            Retorno.Size = Tamaño
        End If

        Dim i As Integer = SQLParametros.GetLength(0)

        ReDim Preserve SQLParametros(i)

        SQLParametros(i) = Retorno

    End Sub

    Sub SetSQLParameter(ByRef SQLParametros As DataTable, ByVal Campo As String, ByVal Valor As String)

        If SQLParametros Is Nothing Then
            SQLParametros = New DataTable
            SQLParametros.Columns.Add("Campo")
            SQLParametros.Columns.Add("Valor")
        End If

        If SQLParametros.Columns.Count = 0 Then
            SQLParametros.Columns.Add("Campo")
            SQLParametros.Columns.Add("Valor")
        End If

        Dim NewRow As DataRow = SQLParametros.NewRow
        NewRow("Campo") = Campo
        NewRow("Valor") = Valor
        SQLParametros.Rows.Add(NewRow)

    End Sub

    Sub SetSQLParameter(ByRef SQLParametros() As SqlParameter, ByVal NombreParametro As String, ByVal cbx As ComboBox, ByVal Tipo As ParameterDirection, Optional ByVal Tamaño As Integer = 0, Optional ByVal TipoDato As SqlDbType = SqlDbType.VarChar)
        Dim Retorno As New SqlParameter
        Retorno.ParameterName = NombreParametro

        If cbx.Text.Trim <> "" Then
            If IsNothing(cbx.SelectedValue) = False Then
                Retorno.Value = cbx.SelectedValue
            End If
        End If

        Retorno.Direction = Tipo
        Retorno.SqlDbType = TipoDato

        If Tamaño > 0 Then
            Retorno.Size = Tamaño
        End If

        Dim i As Integer = SQLParametros.GetLength(0)

        ReDim Preserve SQLParametros(i)

        SQLParametros(i) = Retorno

    End Sub

    Sub SetSQLParameter(ByRef SQLParametros() As SqlParameter, ByVal NombreParametro As String, ByVal Valor As String, ByVal Tipo As ParameterDirection, Optional ByVal Tamaño As Integer = 0, Optional ByVal TipoDato As SqlDbType = SqlDbType.VarChar)

        Dim Retorno As New SqlParameter
        Retorno.ParameterName = NombreParametro

        Valor = Valor.Trim

        If IsNothing(Valor) = False Then
            Retorno.Value = Valor
        End If

        Retorno.Direction = Tipo
        Retorno.SqlDbType = TipoDato

        If Tamaño > 0 Then
            Retorno.Size = Tamaño
        End If

        Dim i As Integer = SQLParametros.GetLength(0)

        ReDim Preserve SQLParametros(i)

        SQLParametros(i) = Retorno

    End Sub

    Sub ConcatenarParametro(ByRef SQL As String, ByVal Campo As String, ByVal Valor As String, Optional ByVal AgregarComa As Boolean = False)

        'Buscamos si existe una "," coma
        If SQL.IndexOf("@") > 0 Then
            SQL = SQL & ", " & Campo & "='" & Valor & "' "
        Else
            SQL = SQL & " " & Campo & "='" & Valor & "' "
        End If


        If AgregarComa = True Then
            SQL = SQL & ","
        End If

    End Sub

#End Region

#Region "CONTROLES WINFORM Y BASES DE DATOS"

    ''' <summary>
    ''' Carga cualquier tipo de consulta TSQL en el LsitView
    ''' </summary>
    ''' <param name="lv">Control Winform tipo ListView</param>
    ''' <param name="consulta">Consulta tipo TSQL</param>
    ''' <remarks></remarks>
    Public Sub SqlToLv(ByVal lv As ListView, ByVal consulta As String, Optional ByVal vConexion As String = "")
        Try

            If vConexion = "" Then
                vConexion = VGCadenaConexion
            End If

            Dim Adapter As New SqlDataAdapter(consulta, vConexion)
            Dim ds As New DataSet
            Adapter.Fill(ds, "Resultado")
            Dim objListItem As New ListViewItem
            lv.FullRowSelect = True
            lv.GridLines = True
            lv.AllowColumnReorder = True
            lv.Columns.Clear()
            lv.Scrollable = True
            lv.View = View.Details
            lv.HideSelection = False

            'Determinamos la cantidad de resultados
            Dim i As Integer

            For i = 0 To ds.Tables(0).Columns.Count - 1
                lv.Columns.Add(ds.Tables(0).Columns(i).ToString, ds.Tables(0).Columns(i).ToString)
            Next

            lv.Items.Clear()
            For Each drw As DataRow In ds.Tables("Resultado").Rows
                objListItem = lv.Items.Add(drw.Item(0).ToString, 0)
                If ds.Tables(0).Columns.Count > 1 Then
                    For i = 1 To ds.Tables(0).Columns.Count - 1
                        objListItem.SubItems.Add(drw.Item(i).ToString)
                    Next
                End If
            Next

            For i = 0 To ds.Tables(0).Columns.Count - 1
                lv.Columns(i).AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize)
            Next

        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToLv", "", consulta)
        End Try


    End Sub

    Public Sub dtToGrid(ByVal grid As DataGridView, ByVal dt As DataTable)
        Try
            grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(Color.White.ToArgb)
            grid.BackgroundColor = Color.White
            grid.RowHeadersVisible = False
            grid.AllowUserToAddRows = False
            grid.AllowUserToDeleteRows = False
            grid.StandardTab = True
            grid.MultiSelect = False
            grid.EditMode = DataGridViewEditMode.EditOnKeystrokeOrF2
            grid.Cursor = Cursors.Hand
            grid.DataSource = dt

            Try
                grid.Columns("ControlarExistencia").Visible = False
            Catch
            End Try

            grid.AllowUserToAddRows = False
            grid.AllowUserToDeleteRows = False

            'Nombres de Columnas y vision
            For c As Integer = 0 To grid.ColumnCount - 1
                grid.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                grid.Columns(c).Name = dt.Columns(c).ColumnName
            Next


        Catch ex As Exception
            CargarError(ex, "CSistema", "dtTogrid", "", "")
        End Try



    End Sub

    Public Sub SqlToDataGrid(ByVal dg As DataGridView, ByVal sql As String, Optional ByVal vConexion As String = "", Optional TimeOut As Integer = 30)
        Try
            Dim dt As DataTable = ExecuteToDataTable(sql, vConexion, TimeOut)
            dtToGrid(dg, dt)

        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToDataGrid", "", "")
        End Try


    End Sub

    Public Sub dtToLv(ByVal lv As ListView, ByVal dt As DataTable)
        Try

            Dim objListItem As New ListViewItem
            lv.FullRowSelect = True
            lv.GridLines = True
            lv.AllowColumnReorder = True
            lv.Columns.Clear()
            lv.Scrollable = True
            lv.View = View.Details
            lv.HideSelection = False

            'Determinamos la cantidad de resultados
            Dim i As Integer

            For i = 0 To dt.Columns.Count - 1
                lv.Columns.Add(dt.Columns(i).ToString, dt.Columns(i).ToString)
            Next

            lv.Items.Clear()
            For Each drw As DataRow In dt.Rows
                objListItem = lv.Items.Add(drw.Item(0).ToString)
                If dt.Columns.Count > 1 Then
                    For i = 1 To dt.Columns.Count - 1
                        'objListItem.SubItems.Add(drw.Item(i).ToString)
                        Dim subItem As New ListViewItem.ListViewSubItem
                        subItem.Name = dt.Columns(i).ColumnName
                        subItem.Text = drw.Item(i).ToString
                        objListItem.SubItems.Add(subItem)
                    Next
                End If
            Next

            For i = 0 To dt.Columns.Count - 1
                lv.Columns(i).AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize)
            Next

        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToLv", "", "")
        End Try


    End Sub

    Public Sub dtToLv(ByVal lv As ListView, ByVal dt As DataTable, ByVal Campos() As String, Optional ByVal OrderBy As String = "")
        Try

            'Copiamos la estructura y datos
            Dim dttemp As DataTable = dt.Copy

            'Ahora recorremos los campos y eliminamos los que no estan definidos
            For i As Integer = 0 To dt.Columns.Count - 1

                Dim Existe As Integer = Array.IndexOf(Campos, dt.Columns(i).ColumnName)

                If Existe < 0 Then
                    dttemp.Columns.RemoveAt(i)
                End If

            Next

            'Ordenar segun campos
            For i As Integer = 0 To Campos.GetLength(0) - 1
                dttemp.Columns(Campos(i)).SetOrdinal(i)
            Next

            OrderDataTable(dttemp, OrderBy)

            dtToLv(lv, dttemp)

        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToLv", "", "")
        End Try


    End Sub

    Public Sub OrderDataTable(ByRef dt As DataTable, ByVal OrderBy As String, Optional ByVal Asc As Boolean = True)

        If dt Is Nothing Then
            Exit Sub
        End If


        If OrderBy = "" Then
            Exit Sub
        End If

        Dim Tipo As String
        If Asc = True Then
            Tipo = " ASC "
        Else
            Tipo = " DESC "
        End If

        Dim dtv As DataView = dt.DefaultView
        dtv.Sort = OrderBy & Tipo

        dt = dtv.ToTable().Copy

    End Sub

    ''' <summary>
    ''' Carga la consulta SQL en el ComboBox pasado por parametro.
    ''' Esta funcion sirve para posteriormente obtener el ID de un registro.
    ''' Ej: Consulta="Select ID, RazonSocial From Cliente" donde el ID se obtiene mediante cbx.SelectedValue
    ''' </summary>
    ''' <param name="cbx"></param>
    ''' <param name="consulta"></param>
    ''' <remarks></remarks>
    Sub SqlToComboBox(ByRef cbx As ComboBox, ByVal consulta As String, Optional ByVal CadenaConexion As String = "")

        Try

            If CadenaConexion = "" Then
                CadenaConexion = VGCadenaConexion
            End If

            Using conn As New SqlConnection(CadenaConexion)
                Using Adapter As New SqlDataAdapter(consulta, conn)
                    Dim table As New DataTable
                    Adapter.Fill(table)
                    cbx.ValueMember = table.Columns(0).ToString
                    cbx.DisplayMember = table.Columns(1).ToString
                    cbx.DataSource = table
                End Using

            End Using

        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToComboBox", "", consulta)
        End Try

    End Sub

    Sub SqlToComboBox(ByVal cbx As DataGridViewComboBoxColumn, ByVal consulta As String)

        Try
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim Adapter As New SqlDataAdapter(consulta, conn)
            Dim table As New DataTable
            Adapter.Fill(table)
            cbx.ValueMember = table.Columns(0).ToString
            cbx.DisplayMember = table.Columns(1).ToString
            cbx.DataSource = table
        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToComboBox", "", consulta)
        End Try

    End Sub

    ''' <summary>
    ''' Carga la consulta SQL en el ComboBox pasado por parametro.
    ''' Esta funcion sirve para posteriormente obtener el ID de un registro.
    ''' Ej: Consulta="Select ID, RazonSocial From Cliente" donde el ID se obtiene mediante cbx.SelectedValue
    ''' </summary>
    ''' <param name="cbx"></param>
    ''' <param name="consulta"></param>
    ''' <remarks></remarks>


    Sub SqlToComboBox(ByVal cbx As ComboBox, ByVal dt As DataTable)

        Try
            cbx.ValueMember = dt.Columns(0).ToString
            cbx.DisplayMember = dt.Columns(1).ToString
            cbx.DataSource = dt
        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToComboBox", "", dt.TableName)
        End Try

    End Sub

    Sub SqlToComboBox(ByVal cbx As ComboBox, ByVal dt As DataTable, ByVal ColumnValue As String, ByVal ColumnDisplay As String)

        Try
            cbx.ValueMember = dt.Columns(ColumnValue).ToString
            cbx.DisplayMember = dt.Columns(ColumnDisplay).ToString
            cbx.DataSource = dt
        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToComboBox", "", dt.TableName)
        End Try

    End Sub

    Sub SqlToComboBox(ByVal cbx As DataGridViewComboBoxColumn, ByVal dt As DataTable, ByVal ColumnValue As String, ByVal ColumnDisplay As String)

        Try
            cbx.ValueMember = dt.Columns(ColumnValue).ToString
            cbx.DisplayMember = dt.Columns(ColumnDisplay).ToString
            cbx.DataSource = dt
        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToComboBox", "", dt.TableName)
        End Try

    End Sub

    Sub SqlToListBox(ByVal lbx As ListBox, ByVal dt As DataTable, ByVal ColumnValue As String, ByVal ColumnDisplay As String)

        Try
            lbx.ValueMember = dt.Columns(ColumnValue).ToString
            lbx.DisplayMember = dt.Columns(ColumnDisplay).ToString
            lbx.DataSource = dt
        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToListBox", "", dt.TableName)
        End Try

    End Sub

    ''' <summary>
    ''' Devuelve un DataTable Filtrado
    ''' </summary>
    ''' <param name="dt">DataTable con informacion que se quiere filtrar</param>
    ''' <param name="Where">Los parametros de filtro</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Function FiltrarDataTable(ByVal dt As DataTable, ByVal Where As String) As DataTable

        FiltrarDataTable = Nothing

        If dt Is Nothing Then
            Return Nothing
        End If

        Try

            Dim dttemp As DataTable = dt.Copy
            Dim dtv As DataView = dttemp.DefaultView
            dtv.RowFilter = Where
            FiltrarDataTable = dtv.ToTable


            Return FiltrarDataTable

        Catch ex As Exception
            CargarError(ex, "CSistema", "FiltrarDataTable", "", dt.TableName)
        End Try


    End Function

    Function ObtenerIDOperacion(ByVal FormName As String, ByVal Descripcion As String, ByVal Codigo As String, Optional ByVal vCadenaConexion As String = "", Optional PrimeraVez As Boolean = False) As Integer

        ObtenerIDOperacion = 0
        Dim Retorno As Integer = CType(ExecuteScalar("Select IsNull((Select Top(1) ID From Operacion Where FormName = '" & FormName & "'), -1)", vCadenaConexion), Integer)

        If Retorno > 0 Then
            PrimeraVez = False
            Return Retorno
            'Else
            '    MessageBox.Show("Se produjo un error interno. El sistema no puede generar el codigo de Operacion, sin este codigo no se puede procesar.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            '    Return -1
        End If

        'Se comenta porque inserta operaciones repetidas
        If Retorno > 0 Then
            PrimeraVez = False
            Return Retorno
        End If

        If Retorno < 0 Then
            MessageBox.Show("Se produjo un error interno. El sistema no puede generar el codigo de Operacion, sin este codigo no se puede procesar.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return -1
        End If

        ''Es por primera vez
        'PrimeraVez = True

        ''Agregar Operacion
        Dim IDOperacion As Integer = CType(ExecuteScalar("Select IsNull((Select MAX(ID)+1 From Operacion), 1)", vCadenaConexion), Integer)
        Dim Consulta As String = "Insert Into Operacion(ID, Descripcion, Codigo, AsientoCredito, AsientoDebito, FormName) values('" & IDOperacion & "', '" & Descripcion & "', '" & Codigo & "', NULL, NULL, '" & FormName & "')"

        If ExecuteNonQuery(Consulta, vCadenaConexion) > 0 Then
            Return IDOperacion
        Else
            MessageBox.Show("Se produjo un error interno. El sistema no puede generar el codigo de Operacion, sin este codigo no se puede procesar.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return 0
        End If

    End Function

    Function ObtenerIDImportacionDatos(ByVal Descripcion As String, ByVal Scrip As String) As Integer

        ObtenerIDImportacionDatos = 0
        Dim Retorno As Integer = CType(ExecuteScalar("Select IsNull((Select ID From ImportarDatos Where Descripcion = '" & Descripcion & "'), 0)"), Integer)

        If Retorno > 0 Then
            Return Retorno
        End If

        'Agregar Operacion
        Dim IDOperacion As Integer = CType(ExecuteScalar("Select IsNull((Select MAX(ID)+1 From ImportarDatos), 1)"), Integer)
        Dim Consulta As String = "Insert Into ImportarDatos(ID, Descripcion, Scrip) values('" & IDOperacion & "', '" & Descripcion & "', '" & Scrip & "')"

        If ExecuteNonQuery(Consulta) > 0 Then
            Return IDOperacion
        Else
            MessageBox.Show("Se produjo un error interno. El sistema no puede generar el codigo de Operacion, sin este codigo no se puede procesar.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return 0
        End If

    End Function

    Function ObtenerProximoNroComprobante(ByVal IDTipoComprobante As Integer, ByVal Tabla As String, ByVal Campo As String, ByVal IDSucursal As Integer, Optional ByVal VCadenaConexion As String = "") As String

        ObtenerProximoNroComprobante = ""

        Dim dtTemp As DataTable = ExecuteToDataTable("Select 'Autonumerico'=(Select Autonumerico From TipoComprobante Where ID=" & IDTipoComprobante & "), 'MaxNroComprobante'=(Select IsNull((Select " & Campo & " From " & Tabla & " Where IDTransaccion=(Select Max(IDTransaccion) From " & Tabla & " Where IDSucursal=" & IDSucursal & ")), 0))", VCadenaConexion).Copy

        For Each oRow As DataRow In dtTemp.Rows

            If IsDBNull(oRow("Autonumerico")) Then
                Return ""
            End If

            If oRow("Autonumerico") = False Then
                Return ""
            End If

            Dim NroComprobante As String = oRow("MaxNroComprobante")

            If IsNumeric(NroComprobante) = True Then
                NroComprobante = CInt(NroComprobante) + 1
                Return NroComprobante
            End If

        Next

    End Function

#End Region

#Region "UTILITARIOS"

    Sub ControlBotonesABM(ByVal Proceso As CSistema.NUMHabilitacionBotonesABM, ByVal btnNuevo As Button, ByVal btnEditar As Button, ByVal btnCancelar As Button, ByVal btnGuardar As Button, ByVal btnEliminar As Button, ByRef ctrl() As Object, Optional ByVal btnImprimir As Button = Nothing)

        Dim HabilitarControles As Boolean = False

        Try

            If btnImprimir Is Nothing Then
                btnImprimir = New Button
            End If

            If ctrl Is Nothing Then
                ReDim ctrl(-1)
            End If

            Select Case Proceso

                Case CSistema.NUMHabilitacionBotonesABM.INICIO
                    btnNuevo.Enabled = True
                    btnEditar.Enabled = False
                    btnCancelar.Enabled = False
                    btnGuardar.Enabled = False
                    btnEliminar.Enabled = False
                    btnImprimir.Enabled = False
                Case CSistema.NUMHabilitacionBotonesABM.NUEVO
                    btnNuevo.Enabled = False
                    btnEditar.Enabled = False
                    btnCancelar.Enabled = True
                    btnGuardar.Enabled = True
                    btnEliminar.Enabled = False
                    HabilitarControles = True
                    btnImprimir.Enabled = False
                Case CSistema.NUMHabilitacionBotonesABM.EDITAR
                    btnNuevo.Enabled = True
                    btnEditar.Enabled = True
                    btnCancelar.Enabled = False
                    btnGuardar.Enabled = False
                    btnEliminar.Enabled = True
                    btnImprimir.Enabled = False
                Case CSistema.NUMHabilitacionBotonesABM.CANCELAR
                    btnNuevo.Enabled = True
                    btnEditar.Enabled = False
                    btnCancelar.Enabled = False
                    btnGuardar.Enabled = False
                    btnEliminar.Enabled = False
                    btnImprimir.Enabled = True
                Case CSistema.NUMHabilitacionBotonesABM.GUARDAR
                    btnNuevo.Enabled = True
                    btnEditar.Enabled = False
                    btnCancelar.Enabled = False
                    btnGuardar.Enabled = False
                    btnEliminar.Enabled = True
                    btnImprimir.Enabled = True
                Case CSistema.NUMHabilitacionBotonesABM.ELIMINAR
                    btnNuevo.Enabled = True
                    btnEditar.Enabled = True
                    btnCancelar.Enabled = False
                    btnGuardar.Enabled = False
                    btnEliminar.Enabled = True
                    btnImprimir.Enabled = False
                Case CSistema.NUMHabilitacionBotonesABM.EDITANDO
                    btnNuevo.Enabled = True
                    btnEditar.Enabled = False
                    btnCancelar.Enabled = True
                    btnGuardar.Enabled = True
                    btnEliminar.Enabled = True
                    HabilitarControles = True
                    btnImprimir.Enabled = False
            End Select


            For i = 0 To ctrl.GetLength(0) - 1
                Select Case ctrl(i).GetType.Name
                    Case "TextBox"
                        ctrl(i).readonly = Not (HabilitarControles)
                    Case "ocxTXTString"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTNumeric"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTDate"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ComboBox"
                        ctrl(i).enabled = HabilitarControles
                    Case "ocxCBX"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ListView"
                        'ctrl(i).enabled = HabilitarControles
                    Case "ocxTXTProveedor"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTCliente"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTProducto"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxFormaPago"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxFormaPagoLote"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxDesgloseBilletes"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxCargaCheque"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxCHK"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxCotizacion"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTCuentaContable"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case Else
                        ctrl(i).Enabled = HabilitarControles
                End Select

            Next

            'Botones
            FormatoBoton(btnNuevo)
            FormatoBoton(btnEditar)
            FormatoBoton(btnCancelar)
            FormatoBoton(btnGuardar)
            FormatoBoton(btnEliminar)

            'Formato
            btnNuevo.Cursor = Cursors.Hand
            btnEditar.Cursor = Cursors.Hand
            btnCancelar.Cursor = Cursors.Hand
            btnGuardar.Cursor = Cursors.Hand
            btnEliminar.Cursor = Cursors.Hand

        Catch ex As Exception

        End Try

    End Sub

    Private Sub FormatoBoton(ByVal btn As Button)

        If btn.Enabled = False Then
            btn.Font = New Font(btn.Font.FontFamily, btn.Font.Size, FontStyle.Regular)
        Else
            btn.Font = New Font(btn.Font.FontFamily, btn.Font.Size, FontStyle.Bold)
        End If

    End Sub

    Sub ControlBotonesRegistro(ByVal Proceso As CSistema.NUMHabilitacionBotonesRegistros, ByVal btnNuevo As Button, ByVal btnGuardar As Button, ByVal btnCancelar As Button, ByVal btnAnular As Button, ByVal btnImprimir As Button, ByVal btnBuscar As Button, ByVal btnAsiento As Button, ByRef ctrl() As Object, Optional ByVal btnModificar As Button = Nothing, Optional ByVal btnEliminar As Button = Nothing, Optional ControlarFuncionNuevo As Boolean = True)

        'Control la funcion nuevo
        Try
            If ControlarFuncionNuevo = False Then
                Exit Try
            End If

            If Proceso <> NUMHabilitacionBotonesRegistros.NUEVO Then
                Exit Try
            End If

            If btnNuevo Is Nothing Then
                Exit Try
            End If

            If btnNuevo.Visible = True Then
                Exit Try
            End If

            'Si llegamos hasta aqui, no se tiene permisos y debemos salir
            'MessageBox.Show("No tiene privilegios para realizar esta operacion!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub

        Catch ex As Exception

        End Try

        Dim HabilitarControles As Boolean = False

        If btnModificar Is Nothing Then
            btnModificar = New Button
        End If

        If btnEliminar Is Nothing Then
            btnEliminar = New Button
        End If

        Try

            Select Case Proceso

                Case CSistema.NUMHabilitacionBotonesRegistros.INICIO
                    btnAnular.Enabled = False
                    btnImprimir.Enabled = False
                    btnBuscar.Enabled = True
                    btnAsiento.Enabled = False
                    btnNuevo.Enabled = True
                    btnGuardar.Enabled = False
                    btnCancelar.Enabled = False
                    btnModificar.Enabled = False
                    btnEliminar.Enabled = False
                Case CSistema.NUMHabilitacionBotonesRegistros.NUEVO
                    btnAnular.Enabled = False
                    btnImprimir.Enabled = False
                    btnBuscar.Enabled = False
                    btnAsiento.Enabled = True
                    btnNuevo.Enabled = False
                    btnGuardar.Enabled = True
                    btnCancelar.Enabled = True
                    HabilitarControles = True
                    btnModificar.Enabled = False
                    btnEliminar.Enabled = False
                Case CSistema.NUMHabilitacionBotonesRegistros.CANCELAR
                    btnAnular.Enabled = True
                    btnImprimir.Enabled = True
                    btnBuscar.Enabled = True
                    btnAsiento.Enabled = True
                    btnNuevo.Enabled = True
                    btnGuardar.Enabled = False
                    btnCancelar.Enabled = False
                    btnModificar.Enabled = True
                    btnEliminar.Enabled = True
                Case CSistema.NUMHabilitacionBotonesRegistros.GUARDAR
                    btnAnular.Enabled = True
                    btnImprimir.Enabled = True
                    btnBuscar.Enabled = True
                    btnAsiento.Enabled = False
                    btnNuevo.Enabled = True
                    btnGuardar.Enabled = False
                    btnCancelar.Enabled = False
                    btnModificar.Enabled = True
                    btnEliminar.Enabled = True
                Case CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA
                    btnAnular.Enabled = True
                    btnImprimir.Enabled = True
                    btnBuscar.Enabled = True
                    btnAsiento.Enabled = True
                    btnNuevo.Enabled = True
                    btnGuardar.Enabled = False
                    btnCancelar.Enabled = False
                    btnModificar.Enabled = True
                    btnEliminar.Enabled = True
                Case CSistema.NUMHabilitacionBotonesRegistros.MODIFICAR
                    btnAnular.Enabled = False
                    btnImprimir.Enabled = False
                    btnBuscar.Enabled = False
                    btnAsiento.Enabled = True
                    btnNuevo.Enabled = False
                    btnGuardar.Enabled = True
                    btnCancelar.Enabled = True
                    btnModificar.Enabled = False
                    HabilitarControles = False
                    'Habilitar manualmente los campos que se quieran modificar!!!!
                    btnEliminar.Enabled = False
            End Select


            For i = 0 To ctrl.GetLength(0) - 1
                Select Case ctrl(i).GetType.Name
                    Case "TextBox"
                        ctrl(i).readonly = Not (HabilitarControles)
                    Case "ocxTXTString"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTNumeric"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTDate"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ComboBox"
                        ctrl(i).enabled = HabilitarControles
                    Case "ocxCBX"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ListView"
                        ctrl(i).CheckBoxes = HabilitarControles
                    Case "ocxTXTProveedor"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTCliente"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTProducto"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxFormaPago"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxFormaPagoLote"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxDesgloseBilletes"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxCargaCheque"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxCHK"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxCotizacion"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "TreeView"
                        ctrl(i).CheckBoxes = HabilitarControles
                    Case Else
                        ctrl(i).Enabled = HabilitarControles
                End Select

            Next

        Catch ex As Exception

        End Try

    End Sub

    Sub InicializaControles(ByVal Control As Control)

        For Each ctr As Object In Control.Controls

            'TextBox
            If ctr.GetType.Name = "TextBox" Then
                ctr.Text = ""
            End If

            'ComboBox
            If ctr.GetType.Name = "ComboBox" Then
                ctr.Text = ""
            End If

            'ListView
            If ctr.GetType.Name = "ListView" Then
                ctr.ITEMS.CLEAR()
            End If

            'ocxTXTString
            If ctr.GetType.Name = "ocxTXTString" Then
                ctr.texto = ""
            End If

            'ocxTXTNumeric
            If ctr.GetType.Name = "ocxTXTNumeric" Then
                ctr.texto = "0"
            End If

            'ocxTXTDate
            If ctr.GetType.Name = "ocxTXTDate" Then
                ctr.setvalue(Now.ToShortDateString)
            End If

            'ocxCBX
            If ctr.GetType.Name = "ocxCBX" Then
                ctr.texto = ""
            End If

            'ocxTXTCuentaContable
            If ctr.GetType.Name = "ocxTXTCuentaContable" Then
                ctr.clear()
            End If

            'StatusStrip
            If ctr.GetType.Name = "StatusStrip" Then
                ctr.text = ""
            End If

        Next

    End Sub

    Sub InicializaControles(ByVal ctrls() As Control)

        For Each ctr As Object In ctrls

            'TextBox
            If ctr.GetType.Name = "TextBox" Then
                ctr.Text = ""
            End If

            'ComboBox
            If ctr.GetType.Name = "ComboBox" Then
                ctr.Text = ""
            End If

            'ListView
            If ctr.GetType.Name = "ListView" Then
                ctr.clear()
            End If

            'ocxTXTString
            If ctr.GetType.Name = "ocxTXTString" Then
                ctr.texto = ""
            End If

            'ocxTXTNumeric
            If ctr.GetType.Name = "ocxTXTNumeric" Then
                ctr.texto = "0"
            End If

            'ocxTXTDate
            If ctr.GetType.Name = "ocxTXTDate" Then
                ctr.setvalue(Now.ToShortDateString)
            End If

            'ocxCBX
            If ctr.GetType.Name = "ocxCBX" Then
                ctr.texto = ""
            End If

            'ocxCHK
            If ctr.GetType.Name = "ocxCHK" Then
                ctr.valor = False
            End If

        Next

    End Sub

    Function CargaControles(ByVal control As Control) As Control()

        Dim i As Integer = 0
        Dim vControl(i)

        For Each ctr As Object In control.Controls

            vControl(i) = ctr

        Next

        Return vControl

    End Function

    Sub CargaControl(ByRef ctrls() As Control, ByVal ctr As Control)

        Dim i As Integer = ctrls.GetLength(0)

        ReDim Preserve ctrls(i)

        ctrls(i) = ctr


    End Sub

    Sub CargarRowEnControl(ByVal oRow As DataRow, ByVal index As String, ByRef ctr As Object, Optional ByVal ocxCBXSelectedValue As Boolean = False)

        Try
            Select Case ctr.GetType.Name

                'TextBox
                Case "TextBox"
                    ctr.Text = oRow(index).ToString

                    'ComboBox
                Case "ComboBox"
                    ctr.selectedvalue = oRow(index).ToString

                Case "ListView"

                    'ocxTXTString
                Case "ocxTXTString"
                    ctr.texto = oRow(index).ToString

                    'ocxTXTNumeric
                Case "ocxTXTNumeric"
                    ctr.txt.text = oRow(index).ToString

                    'ocxTXTDate
                Case "ocxTXTDate"
                    ctr.txt.text = oRow(index).ToString

                    'ocxTXTCuentaContable
                Case "ocxTXTCuentaContable"
                    ctr.SetValue(oRow(index).ToString)

                    'ocxCBX
                Case "ocxCBX"
                    If ocxCBXSelectedValue = True Then
                        ctr.SelectedValue(oRow(index).ToString)
                    Else
                        ctr.texto = oRow(index).ToString
                    End If


                    'RadioButton
                Case "RadioButton"
                    ctr.checked = oRow(index).ToString

            End Select


        Catch ex As Exception

        End Try


    End Sub

    Public Shared Function Image2Bytes(ByVal img As Bitmap) As Byte()

        Dim sTemp As String = IO.Path.GetTempFileName()
        Dim fs As New IO.FileStream(sTemp, IO.FileMode.OpenOrCreate, IO.FileAccess.ReadWrite)
        img.Save(fs, System.Drawing.Imaging.ImageFormat.Png)
        fs.Position = 0

        Dim imgLength As Integer = CInt(fs.Length)
        Dim bytes(0 To imgLength - 1) As Byte
        fs.Read(bytes, 0, imgLength)
        fs.Close()
        Return bytes

    End Function
    'Convierte un tipo de Arrary de Byte() a imagen
    Public Shared Function Bytes2Image(ByVal bytes() As Byte) As Image

        If bytes Is Nothing Then Return Nothing
        '
        Dim ms As New MemoryStream(bytes)
        Dim bm As Bitmap = Nothing
        Try
            bm = New Bitmap(ms)
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.Message)
        End Try
        Return bm

    End Function


    Function FormatoMoneda(ByVal valor As String, Optional ByVal decimales As Boolean = False) As String

        FormatoMoneda = "0"

        Try
            If IsNumeric(valor) = False Then
                valor = "0"
                Exit Function
            End If

            If CDec(valor) = 0 Then
                valor = "0"
                Exit Function
            End If

            If decimales = True Then
                If CDec(valor) > 1 Then
                    valor = CStr(Format(CDec(valor), "###,###,###.##"))
                Else
                    'valor = CDec(valor).ToString
                    valor = CStr(Format(CDec(valor), "0.##"))
                End If

            Else
                valor = CStr(Format(CDec(valor), "###,###,###"))
            End If

        Catch ex As Exception

        End Try

        FormatoMoneda = valor

    End Function

    Function FormatoMoneda(ByVal valor As String, ByVal decimales As Boolean, Optional ByVal CantidadDecimales As Integer = 2) As String

        FormatoMoneda = "0"

        Try
            If IsNumeric(valor) = False Then
                valor = "0"
                Exit Function
            End If

            If CDec(valor) = 0 Then
                valor = "0"
                Exit Function
            End If

            If decimales = True Then
                Dim vDecimales As String = ""

                For i As Integer = 1 To CantidadDecimales
                    vDecimales = vDecimales & "#"
                Next

                If CDec(valor) > 1 Then
                    valor = CStr(Format(CDec(valor), "###,###,###." & vDecimales))
                Else
                    valor = CStr(Format(CDec(valor), "0." & vDecimales))
                End If

            Else
                valor = CStr(Format(CDec(valor), "###,###,###"))
            End If



        Catch ex As Exception

        End Try

        FormatoMoneda = valor

    End Function

    Function FormatoMoneda3Decimales(ByVal valor As String, Optional ByVal decimales As Boolean = False) As String

        FormatoMoneda3Decimales = "0"

        Try
            If IsNumeric(valor) = False Then
                valor = "0"
                Exit Function
            End If

            If CDec(valor) = 0 Then
                valor = "0"
                Exit Function
            End If

            If decimales = True Then
                If CDec(valor) > 1 Then
                    valor = CStr(Format(CDec(valor), "###,###,###.###"))
                Else
                    'valor = CDec(valor).ToString
                    valor = CStr(Format(CDec(valor), "0.###"))
                End If

            Else
                valor = CStr(Format(CDec(valor), "###,###,###"))
            End If

        Catch ex As Exception

        End Try

        FormatoMoneda3Decimales = valor

    End Function

    Sub FormatoNumero(ByVal lv As ListView, ByVal Columna As Integer, Optional ByVal decimales As Boolean = False)

        Try


            For Each item As ListViewItem In lv.Items
                item.SubItems(Columna).Text = FormatoNumero(item.SubItems(Columna).Text, decimales)
            Next

            'Formato
            lv.Columns(Columna).TextAlign = HorizontalAlignment.Right

        Catch ex As Exception

        End Try

    End Sub

    Function FormatoNumero(ByVal valor As String, Optional ByVal decimales As Boolean = False) As String

        FormatoNumero = "0"

        Try
            If IsNumeric(valor) = False Then
                valor = "0"
                Exit Function
            End If

            If CDec(valor) = 0 Then
                valor = "0"
                Exit Function
            End If

            If decimales = True Then
                If CDec(valor) > 0 Then
                    valor = CStr(Format(CDec(valor), "###,###,###.##"))
                Else
                    'valor = CDec(valor).ToString
                    valor = CStr(Format(CDec(valor), "0.##"))
                End If

            Else
                valor = CStr(Format(CDec(valor), "###,###,###"))
            End If

        Catch ex As Exception

        End Try

        FormatoNumero = valor

    End Function

    Function ExtraerDecimal(ByVal valor As String) As Integer

        Dim ParteEntera As Long
        Dim ParteDecimal As Double
        Dim ImporteFinal As Integer

        ParteEntera = CInt(valor)
        ParteDecimal = valor - ParteEntera

        If ParteDecimal < 0 Then
            ParteDecimal = ParteDecimal * -1
        End If
        ImporteFinal = valor - ParteDecimal

        Return ImporteFinal

    End Function

    Function FormatoMonedaBaseDatos(ByVal valor As String, Optional ByVal decimales As Boolean = False) As String

        FormatoMonedaBaseDatos = "0"

        Try
            If IsNumeric(valor) = False Then
                valor = "0"
                Exit Function
            End If

            If CDec(valor) = 0 Then
                valor = "0"
                Exit Function
            End If

            If decimales = True Then
                If CDec(valor) > 1 Then
                    valor = CStr(Format(CDec(valor), "###.####"))
                Else
                    'valor = CDec(valor).ToString
                    valor = CStr(Format(CDec(valor), "0.####"))
                End If

            Else
                valor = CStr(Format(CDec(valor), "###"))
            End If

            valor = valor.Replace(".", "")
            valor = valor.Replace(",", ".")

        Catch ex As Exception

        End Try

        FormatoMonedaBaseDatos = valor

    End Function

    Function FormatoNumeroBaseDatos(ByVal valor As String, Optional ByVal decimales As Boolean = False) As String

        FormatoNumeroBaseDatos = "0"

        Try
            If IsNumeric(valor) = False Then
                valor = "0"
                Exit Function
            End If

            If CDec(valor) = 0 Then
                valor = "0"
                Exit Function
            End If

            If decimales = True Then
                If CDec(valor) > 1 Then
                    valor = CStr(Format(CDec(valor), "###.####"))
                Else
                    valor = CStr(Format(CDec(valor), "0.####"))
                End If

            Else
                valor = CStr(Format(CDec(valor), "###"))
            End If

            valor = valor.Replace(".", "")
            valor = valor.Replace(",", ".")

        Catch ex As Exception

        End Try

        FormatoNumeroBaseDatos = valor

    End Function

    Function FormatoFechaBaseDatos(ByVal dtp As DateTimePicker, ByVal Fecha As Boolean, ByVal Hora As Boolean) As String

        Dim Retorno As String = ""

        Retorno = FormatoFechaBaseDatos(dtp.Value, True, False)

        Return Retorno

    End Function

    Function FormatoFechaBaseDatos(ByVal value As Date, ByVal Fecha As Boolean, ByVal Hora As Boolean) As String

        Dim Retorno As String = ""

        If Fecha = True Then
            Retorno = value.Year & FormatDobleDigito(value.Month) & FormatDobleDigito(value.Day)
        End If

        If Hora = True Then
            If Retorno = "" Then
                Retorno = value.Hour & ":" & value.Minute & ":" & value.Second
            Else
                Retorno = Retorno & " " & value.Hour & ":" & value.Minute & ":" & value.Second
            End If
        End If

        Return Retorno

    End Function

    Function FormatoFechaDesdeHastaBaseDatos(ByVal dtp As DateTimePicker, Optional ByVal Desde As Boolean = True, Optional ByVal Hasta As Boolean = False) As String

        Dim Retorno As String = ""

        Retorno = FormatoFechaBaseDatos(dtp.Value, True, False)

        If Desde = True Then
            Retorno = Retorno & " 00:00:00.000"
        End If

        If Hasta = True Then
            Retorno = Retorno & " 23:59:59.999"
        End If

        Return Retorno

    End Function

    Function FormatoFechaDesdeHastaBaseDatos(ByVal value As Date, Optional ByVal Desde As Boolean = True, Optional ByVal Hasta As Boolean = False) As String

        Dim Retorno As String = ""

        Retorno = FormatoFechaBaseDatos(value, True, False)

        If Desde = True Then
            Retorno = Retorno & " 00:00:00.000"
        End If

        If Hasta = True Then
            Retorno = Retorno & " 23:59:59.999"
        End If

        Return Retorno

    End Function

    Function FormatoFechaBaseDatos(ByVal valor As String, ByVal Fecha As Boolean, ByVal Hora As Boolean) As String

        Dim Retorno As String = ""
        Dim dtp As DateTime

        If IsDate(valor) = False Then
            dtp = Date.Now
        Else
            dtp = CDate(valor)
        End If

        Retorno = FormatoFechaBaseDatos(dtp, Fecha, Hora)

        Return Retorno

    End Function

    Public Function EstablecerFechaPredeterminada(ByVal Rango As NUMFechaPreestablecida) As Date
        EstablecerFechaPredeterminada = Now

        Select Case Rango
            Case NUMFechaPreestablecida.HOY
                EstablecerFechaPredeterminada = EstablecerFechaPredeterminada.Day & "/" & EstablecerFechaPredeterminada.Month & "/" & EstablecerFechaPredeterminada.Year & " 00:00:00"
            Case NUMFechaPreestablecida.AYER
                EstablecerFechaPredeterminada = EstablecerFechaPredeterminada.Day & "/" & EstablecerFechaPredeterminada.Month & "/" & EstablecerFechaPredeterminada.Year & " 00:00:00"
                EstablecerFechaPredeterminada = EstablecerFechaPredeterminada.AddDays(-1)
            Case NUMFechaPreestablecida.ESTA_SEMANA
                EstablecerFechaPredeterminada = EstablecerFechaPredeterminada.Day & "/" & EstablecerFechaPredeterminada.Month & "/" & EstablecerFechaPredeterminada.Year & " 00:00:00"
                Dim Dia As Integer = EstablecerFechaPredeterminada.DayOfWeek - 1
                EstablecerFechaPredeterminada = EstablecerFechaPredeterminada.AddDays(-Dia)
            Case NUMFechaPreestablecida.ESTE_MES
                EstablecerFechaPredeterminada = "01/" & EstablecerFechaPredeterminada.Month & "/" & EstablecerFechaPredeterminada.Year & " 00:00:00"
            Case NUMFechaPreestablecida.ESTE_AÑO
                EstablecerFechaPredeterminada = "01/01" & "/" & EstablecerFechaPredeterminada.Year & " 00:00:00"
        End Select

    End Function

    Sub FormatoMoneda(ByVal lv As ListView, ByVal Columna As Integer, Optional ByVal ColumnaDecimal As Integer = 0)

        Try


            For Each item As ListViewItem In lv.Items
                If ColumnaDecimal = 0 Then
                    item.SubItems(Columna).Text = FormatoMoneda(item.SubItems(Columna).Text, False)
                Else
                    item.SubItems(Columna).Text = FormatoMoneda(item.SubItems(Columna).Text, item.SubItems(ColumnaDecimal).Text)
                End If

            Next

            'Formato
            lv.Columns(Columna).TextAlign = HorizontalAlignment.Right
            If ColumnaDecimal <> 0 Then
                lv.Columns(ColumnaDecimal).Width = 0
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub FormatoMoneda(ByVal lv As ListView, ByVal Columna As String, Optional ByVal ColumnaDecimal As Integer = 0)

        Try


            For Each item As ListViewItem In lv.Items
                If ColumnaDecimal = 0 Then
                    item.SubItems(lv.Columns(Columna).Index).Text = FormatoMoneda(item.SubItems(lv.Columns(Columna).Index).Text, False)
                Else
                    item.SubItems(lv.Columns(Columna).Index).Text = FormatoMoneda(item.SubItems(lv.Columns(Columna).Index).Text, item.SubItems(lv.Columns(Columna).Index).Text)
                End If

            Next

            'Formato
            lv.Columns(Columna).TextAlign = HorizontalAlignment.Right
            If ColumnaDecimal <> 0 Then
                lv.Columns(ColumnaDecimal).Width = 0
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub TotalesLv(ByVal lv As ListView, ByVal txt As TextBox, ByVal Columna As Integer, Optional ByVal Moneda As Boolean = False)

        Dim Total As Decimal = 0

        For Each item As ListViewItem In lv.Items

            Dim value As String
            value = item.SubItems(Columna).Text

            If IsNumeric(value) = False Then
                value = "0"
            End If

            Total = Total + CDec(value)

        Next

        If Moneda = False Then
            txt.Text = FormatoNumero(CStr(Total))
        Else
            txt.Text = FormatoMoneda(CStr(Total))
        End If

    End Sub

    Sub TotalesLv(ByVal lv As ListView, ByVal txt As TextBox, ByVal Columna As String, Optional ByVal Moneda As Boolean = False)

        Dim Total As Decimal = 0

        For Each item As ListViewItem In lv.Items

            Dim value As String
            value = lv.Items(item.Index).SubItems(lv.Columns(Columna).Index).Text

            If IsNumeric(value) = False Then
                value = "0"
            End If

            Total = Total + CDec(value)

        Next

        If Moneda = False Then
            txt.Text = FormatoNumero(CStr(Total))
        Else
            txt.Text = FormatoMoneda(CStr(Total))
        End If

    End Sub

    Sub TotalesGrid(ByVal dg As DataGridView, ByVal txt As TextBox, ByVal Columna As Integer, Optional ByVal Moneda As Boolean = False)

        Dim Total As Decimal = 0

        For i As Integer = 0 To dg.Rows.Count - 1

            Dim value As String
            value = dg.Rows(i).Cells(Columna).Value.ToString


            If IsNumeric(value) = False Then
                value = "0"
            End If

            Total = Total + CDec(value)

        Next

        If Moneda = False Then
            txt.Text = FormatoNumero(CStr(Total))
        Else
            txt.Text = FormatoMoneda(CStr(Total))
        End If

    End Sub

    Function TotalesDataTable(ByVal dt As DataTable, ByVal Columna As String, Optional ByVal Moneda As Boolean = False) As String

        Dim Total As Decimal = 0

        For Each oRow As DataRow In dt.Rows

            Dim value As String
            value = oRow(Columna).ToString

            If IsNumeric(value) = False Then
                value = "0"
            End If

            Total = Total + CDec(value)

        Next

        If Moneda = False Then
            Return FormatoNumero(CStr(Total))
        Else
            Return FormatoMoneda(CStr(Total))
        End If

    End Function

    Function TotalesDataTable(ByVal dt As DataView, ByVal Columna As String, Optional ByVal Moneda As Boolean = False, Optional ByVal Decimales As Boolean = False) As String

        Dim Total As Decimal = 0

        For Each oRow As DataRowView In dt

            Dim value As String
            value = oRow(Columna).ToString

            If IsNumeric(value) = False Then
                value = "0"
            End If

            Total = Total + CDec(value)

        Next

        If Moneda = False Then
            Return FormatoNumero(CStr(Total), Decimales)
        Else
            Return FormatoMoneda(CStr(Total), Decimales)
        End If

    End Function

    Sub ShowForm(ByVal frmClient As Form, ByVal frmParent As Form)

        Dim frm As New Form
        frm = frmClient

        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog(frmParent)

    End Sub

    Sub LvPintarAnulados(ByVal lv As ListView, ByVal colum As Integer, ByVal Filtro As String)
        Try
            If colum > lv.Columns.Count - 1 Then
                Exit Sub
            End If

            For Each item As ListViewItem In lv.Items

                Dim valor As String = item.SubItems(colum).Text
                If valor.Contains(Filtro) = True Then
                    item.BackColor = Color.LightYellow
                End If

            Next

        Catch ex As Exception

        End Try



    End Sub

    Function GetDateTimeString() As String
        GetDateTimeString = Date.Now.Year & Date.Now.Month & Date.Now.Day & "_" & Date.Now.Hour & Date.Now.Minute & Date.Now.Second
    End Function

    Function GetDateTimeFormatString(Optional ByVal value As String = "") As String

        If value = "" Then
            GetDateTimeFormatString = Date.Now.Day & "-" & Date.Now.Month & "-" & Date.Now.Year & " " & Date.Now.Hour & ":" & Date.Now.Minute
        Else
            If IsDate(value) = False Then
                GetDateTimeFormatString = Date.Now.Day & "-" & Date.Now.Month & "-" & Date.Now.Year & " " & Date.Now.Hour & ":" & Date.Now.Minute
            Else
                Dim d As Date = CDate(value)
                GetDateTimeFormatString = FormatDobleDigito(d.Day.ToString) & "-" & FormatDobleDigito(d.Month.ToString) & "-" & FormatDobleDigito(d.Year.ToString) & " " & FormatDobleDigito(d.Hour.ToString) & ":" & FormatDobleDigito(d.Minute.ToString)
            End If
        End If

    End Function

    Function FormatDobleDigito(ByVal value As String) As String

        FormatDobleDigito = "00"

        If IsNumeric(value) = False Then
            Exit Function
        End If

        If CInt(value) < 10 Then
            FormatDobleDigito = "0" & value
        Else
            FormatDobleDigito = value
        End If

    End Function


    Function CalcularPorcentaje(ByVal Importe As Decimal, ByVal Porcentaje As Decimal, ByVal Decimales As Boolean, ByVal Redondear As Boolean) As Decimal

        CalcularPorcentaje = 0
        Porcentaje = Porcentaje / 100
        Dim Descuento As Decimal = Importe * Porcentaje

        CalcularPorcentaje = Descuento

        If Decimales = False Then
            Redondear = True
        End If

        If Redondear = True Then
            CalcularPorcentaje = CInt(Math.Round(CalcularPorcentaje, 0))
        End If

    End Function

    Function CalcularSinIVA(ByVal IDImpuesto As Integer, ByVal Monto As Decimal, ByVal Decimales As Boolean, ByVal Redondear As Boolean) As Decimal

        CalcularSinIVA = 0

        'Obtenermos valores de la Base de Datos
        Dim dt As DataTable = ExecuteToDataTable("Select FactorDiscriminado, FactorImpuesto From Impuesto Where ID=" & IDImpuesto)

        If dt Is Nothing Then
            Return 0
        End If

        If dt.Rows.Count = 0 Then
            Return 0
        End If

        Dim oRow As DataRow = dt.Rows(0)

        Dim FactorDiscrimindado As Decimal = CDec(oRow("FactorDiscriminado").ToString)

        CalcularSinIVA = Monto / FactorDiscrimindado

        If Decimales = False Then
            Redondear = True
        End If

        If Redondear = True Then

            CalcularSinIVA = CInt(Math.Round(CalcularSinIVA, 0))

        End If

    End Function

    'Calcula Archivo
    Function CalculaTamaño(ByVal valor As Decimal, ByRef Unidad As String) As Decimal

        CalculaTamaño = 0
        Dim Resultado As Decimal = valor
        Resultado = Resultado / 1024

        Select Case Unidad
            Case "Bytes"
                Unidad = "KBytes"
            Case "KBytes"
                Unidad = "MBytes"
            Case "MBytes"
                Unidad = "GBytes"
            Case "GBytes"
                Unidad = "TBytes"
        End Select

        If Resultado >= 1000 Then
            Resultado = CalculaTamaño(Resultado, Unidad)
        End If

        Return Resultado

    End Function
    Function dtSumColumn(ByVal dt As DataTable, ByVal column As String) As Decimal

        dtSumColumn = 0

        Dim temp As Decimal = 0

        Try

            For Each oRow As DataRow In dt.Rows

                If IsNumeric(oRow(column).ToString) = False Then
                    GoTo siguiente
                End If

                temp = temp + CDec(oRow(column).ToString)

siguiente:

            Next

            Return temp

        Catch ex As Exception

        End Try

    End Function

    Function dtCountDistinct(ByVal dt As DataTable, ByVal column As String) As Integer

        dtCountDistinct = 0

        Dim temp As Integer = 0
        Dim i As Integer = 0
        Dim Vector(-1) As String

        Try

            For Each oRow As DataRow In dt.Rows

                If Vector.Contains(oRow(column).ToString) = False Then

                    ReDim Preserve Vector(i)

                    Vector(i) = oRow(column).ToString

                    i = i + 1

                End If
siguiente:

            Next

            temp = Vector.GetLength(0)

            Return temp

        Catch ex As Exception

        End Try

    End Function

    Function dtSumColumn(ByVal dt As DataTable, ByVal column As String, ByVal Condicion As String) As Decimal

        dtSumColumn = 0

        Dim temp As Decimal = 0

        Try

            For Each oRow As DataRow In dt.Select(Condicion)

                If IsNumeric(oRow(column).ToString) = False Then
                    GoTo siguiente
                End If

                temp = temp + CDec(oRow(column).ToString)

siguiente:

            Next

            Return temp

        Catch ex As Exception

        End Try

    End Function

    Function dtSumColumn(ByVal dt As DataTable, ByVal column As String, ByVal Condicion As String, ByVal ColumnaCondicion As String) As Decimal

        dtSumColumn = 0

        Dim temp As Decimal = 0

        Try

            For Each oRow As DataRow In dt.Rows

                If Condicion <> oRow(ColumnaCondicion).ToString Then
                    GoTo siguiente
                End If

                If IsNumeric(oRow(column).ToString) = False Then
                    GoTo siguiente
                End If

                temp = temp + CDec(oRow(column).ToString)

siguiente:

            Next

            Return temp

        Catch ex As Exception

        End Try

    End Function

    Function gridSumColumn(ByVal dg As DataGridView, ByVal column As String) As Decimal

        gridSumColumn = 0

        Dim temp As Decimal = 0

        Try

            For Each dgv As DataGridViewRow In dg.Rows

                If IsNumeric(dgv.Cells(column).Value) = False Then
                    GoTo siguiente
                End If

                temp = temp + CDec(dgv.Cells(column).Value)

siguiente:

            Next

            Return temp

        Catch ex As Exception

        End Try

    End Function

    Function gridSumColumn(ByVal dg As DataGridView, ByVal column As String, ByVal ColumnaCondicion As String, ByVal Valor As String) As Decimal

        gridSumColumn = 0

        Dim temp As Decimal = 0

        Try

            For Each dgv As DataGridViewRow In dg.Rows

                If dgv.Cells(ColumnaCondicion).Value = Valor Then

                    If IsNumeric(dgv.Cells(column).Value) = False Then
                        GoTo siguiente
                    End If

                    temp = temp + CDec(dgv.Cells(column).Value)

                End If

siguiente:

            Next

            Return temp

        Catch ex As Exception

        End Try

    End Function

    Function gridCountRows(ByVal dg As DataGridView, ByVal ColumnaCondicion As String, ByVal Valor As String) As Decimal

        gridCountRows = 0

        Dim temp As Decimal = 0

        Try

            For Each dgv As DataGridViewRow In dg.Rows

                If dgv.Cells(ColumnaCondicion).Value = Valor Then

                    temp = temp + 1

                End If

siguiente:

            Next

            Return temp

        Catch ex As Exception

        End Try

    End Function


    Function gridSumColumn(ByVal dg As DataGridView, ByVal column As String, ByVal ColumnaCondicion As String, ByVal Operacion As String, ByVal Valor As String) As Decimal

        gridSumColumn = 0

        Dim temp As Decimal = 0

        Try

            For Each dgv As DataGridViewRow In dg.Rows

                If IsNumeric(dgv.Cells(column).Value) = False Then
                    GoTo siguiente
                End If

                'Igual
                Select Case Operacion

                    Case "="
                        If dgv.Cells(ColumnaCondicion).Value = Valor Then
                            temp = temp + CDec(dgv.Cells(column).Value)
                        End If

                    Case "<"
                        If dgv.Cells(ColumnaCondicion).Value < Valor Then
                            temp = temp + CDec(dgv.Cells(column).Value)
                        End If
                    Case ">"
                        If dgv.Cells(ColumnaCondicion).Value > Valor Then
                            temp = temp + CDec(dgv.Cells(column).Value)
                        End If
                    Case "<>"
                        If dgv.Cells(ColumnaCondicion).Value <> Valor Then
                            temp = temp + CDec(dgv.Cells(column).Value)
                        End If
                End Select

siguiente:

            Next

            Return temp

        Catch ex As Exception

        End Try

    End Function

    Public Function RetornarValorBoolean(ByVal valor As String) As Boolean

        RetornarValorBoolean = False

        Try
            If valor Is Nothing Then
                Return False
            End If

            If valor.Length = 0 Then
                Return False
            End If

            If valor.ToUpper <> "FALSE" Then
                If valor.ToUpper <> "TRUE" Then
                    Return False
                End If
            End If

            Return CBool(valor)

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function RetornarValorInteger(ByVal valor As String) As Integer

        RetornarValorInteger = 0

        Try
            If valor Is Nothing Then
                Return 0
            End If

            If valor.Length = 0 Then
                Return 0
            End If

            If IsNumeric(valor) = False Then
                Return 0
            End If

            Return CDec(valor)

        Catch ex As Exception
            Return 0
        End Try

    End Function

    Public Function RetornarValorString(ByVal valor As String) As String

        RetornarValorString = ""

        Try
            If valor Is Nothing Then
                Return ""
            End If

            If valor.Length = 0 Then
                Return ""
            End If

            Return valor

        Catch ex As Exception
            Return ""
        End Try

    End Function

    Public Function CotizacionDelDiaFecha(IDMoneda As Decimal, IDOperacion As Integer, Fecha As String) As Integer

        CotizacionDelDiaFecha = 1

        If IDMoneda = 1 Then
            Return 1
        End If

        CotizacionDelDiaFecha = ExecuteScalar("Select max(Cotizacion) from Cotizacion where IDMoneda = " & IDMoneda & " and cast(fecha as date) =" & Fecha)

    End Function

    Public Sub SelectNextControl(ByVal Control As Control)

        'Si el control es especial, no cambiar, lo hace solo
        '(ocxImpuestos, ocxCuentaContable, ocxTXTCliente, ocxTXTProveedor)

        Dim tipo As String = Control.GetType.ToString
        Dim ctr As Object = Nothing

        Select Case tipo
            Case "ocxImpuesto"
                Exit Sub
            Case "ocxCuentaContable"
                Exit Sub
            Case "ocxTXTProveedor"
                Exit Sub
            Case "ocxTXTProducto"
                Exit Sub
            Case "ocxTXTCliente"
                ctr = Control
                If ctr.SoloLectura = False Then
                    Exit Sub
                End If
            Case "Button"
                Exit Sub
            Case "ocxSeleccionarEfectivo"
                Exit Sub
            Case "ocxCotizacion"
                If CObj(Control).Saltar = False Then
                    Exit Sub
                End If
        End Select

        Control.SelectNextControl(Control, True, True, True, True)


    End Sub

    Public Sub LvToExcel(ByVal lv As ListView)

        Try

            'DECLARACIONES
            Dim xlApp As Excel.Application
            Dim xlLibro As Excel.Workbook
            Dim xlHoja As Excel.Worksheet

            Dim i, f, cant, j As Integer
            xlApp = CType(CreateObject("Excel.application"), Excel.Application)
            xlLibro = CType(xlApp.Workbooks.Add, Excel.Workbook)
            xlHoja = CType(xlLibro.Worksheets(1), Excel.Worksheet)
            xlHoja.Name = "RESULTADO"
            f = 2
            cant = 0
            j = 2

            For i = 0 To lv.Columns.Count - 1
                xlHoja.Cells(f, i + 2) = lv.Columns(i).Text
                xlHoja.Cells(f, i + 2).Interior.ColorIndex = 15
                xlHoja.Cells(f, i + 2).Cells.HorizontalAlignment = 3
                xlHoja.Cells(f, i + 2).Font.Bold = True
                xlHoja.Cells(f, i + 2).Borders.LineStyle = 1
                xlHoja.Cells(f, i + 2).Columns.AutoFit()
            Next

            f = 3
            Dim item As ListViewItem
            For Each item In lv.Items
                For i = 0 To lv.Columns.Count - 1
                    xlHoja.Cells(f, i + 2) = item.SubItems(i).Text
                    xlHoja.Cells(f, i + 2).Borders.LineStyle = 1
                    xlHoja.Cells(f, i + 2).Columns.AutoFit()
                Next
                f += 1
            Next

            ''total
            'xlHoja.Cells(f, lv.Columns.Count).Interior.ColorIndex = 15
            'xlHoja.Cells(f, lv.Columns.Count).Cells.HorizontalAlignment = 3
            'xlHoja.Cells(f, lv.Columns.Count).Font.Bold = True
            'xlHoja.Cells(f, lv.Columns.Count).Borders.LineStyle = 1
            'xlHoja.Cells(f, lv.Columns.Count).Value = "Total"
            'xlHoja.Cells(f, lv.Columns.Count + 1).Value = cajasuma.Text
            'xlHoja.Cells(f, lv.Columns.Count + 12).Borders.LineStyle = 1


            'Mostrar Hoja de Trabajo
            xlHoja.Application.Visible = True
            xlHoja = Nothing

            'Prueba
            'xlHoja.PageSetup.PaperSize = Excel.XlPaperSize.xlPaperA4
            'xlHoja.PrintOut()
            'xlHoja = Nothing

        Catch ex As Exception
            Try
                'LvToOO(lv)
            Catch
                MessageBox.Show("No se encuentra ningun programa de calculo para exportar! Se requiere Microsoft Excel o Open Office para continuar", "ERP", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End Try
        End Try


    End Sub

    Public Sub dtToExcel(ByVal dt As DataTable)

        Try

            'DECLARACIONES
            Dim xlApp As New Excel.Application
            Dim xlLibro As Excel.Workbook
            Dim xlHoja As New Excel.Worksheet

            Dim i, f, cant, j As Integer
            xlApp = CType(CreateObject("Excel.application"), Excel.Application)
            xlLibro = CType(xlApp.Workbooks.Add, Excel.Workbook)
            xlHoja = CType(xlLibro.Worksheets(1), Excel.Worksheet)
            xlHoja.Name = "RESULTADO"
            f = 2
            cant = 0
            j = 2

            For i = 0 To dt.Columns.Count - 1
                xlHoja.Cells(f, i + 2) = dt.Columns(i).ColumnName
                xlHoja.Cells(f, i + 2).Interior.ColorIndex = 15
                xlHoja.Cells(f, i + 2).Cells.HorizontalAlignment = 3
                xlHoja.Cells(f, i + 2).Font.Bold = True
                xlHoja.Cells(f, i + 2).Borders.LineStyle = 1
                xlHoja.Cells(f, i + 2).Columns.AutoFit()
            Next

            f = 3
            For Each oRow As DataRow In dt.Rows
                For i = 0 To dt.Columns.Count - 1
                    xlHoja.Cells(f, i + 2) = oRow(i).ToString
                    xlHoja.Cells(f, i + 2).Borders.LineStyle = 1
                    xlHoja.Cells(f, i + 2).Columns.AutoFit()
                Next
                f += 1
            Next

            xlHoja = Nothing

            'Mostrar Hoja de Trabajo
            xlHoja.Application.Visible = True

        Catch ex As Exception
            Try
                dtToOO(dt)
            Catch
                MessageBox.Show("No se encuentra ningun programa de calculo para exportar! Se requiere Microsoft Excel o Open Office para continuar", "ERP", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End Try
        End Try


    End Sub

    Function GridAExcel(ByVal ElGrid As DataGridView) As Boolean

        'Creamos las variables
        'Dim exApp As New Microsoft.Office.Interop.Excel.Application
        'Dim exLibro As Microsoft.Office.Interop.Excel.Workbook
        'Dim exHoja As Microsoft.Office.Interop.Excel.Worksheet

        Dim exApp As New Excel.Application
        Dim exLibro As Excel.Workbook
        Dim exHoja As Excel.Worksheet

        Try
            'Añadimos el Libro al programa, y la hoja al libro
            exLibro = exApp.Workbooks.Add
            exHoja = exLibro.Worksheets.Add()

            ' ¿Cuantas columnas y cuantas filas?
            Dim NCol As Integer = ElGrid.ColumnCount
            Dim NRow As Integer = ElGrid.RowCount

            'Aqui recorremos todas las filas, y por cada fila todas las columnas y vamos escribiendo.
            For i As Integer = 1 To NCol
                exHoja.Cells.Item(1, i) = ElGrid.Columns(i - 1).Name.ToString
                'exHoja.Cells.Item(1, i).HorizontalAlignment = 3
            Next

            For Fila As Integer = 0 To NRow - 1
                For Col As Integer = 0 To NCol - 1
                    exHoja.Cells.Item(Fila + 2, Col + 1) = ElGrid.Rows(Fila).Cells(Col).Value
                Next
            Next
            'Titulo en negrita, Alineado al centro y que el tamaño de la columna se ajuste al texto
            exHoja.Rows.Item(1).Font.Bold = 1
            exHoja.Rows.Item(1).HorizontalAlignment = 3
            exHoja.Columns.AutoFit()



            'Aplicación visible
            exApp.Application.Visible = True

            exHoja = Nothing
            exLibro = Nothing
            exApp = Nothing

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al exportar a Excel")

            Return False
        End Try

        Return True

    End Function


    Public Sub dtToExcel(ByVal ds As DataSet, ByVal strFileName As String)

        Try

            Dim _excel As Excel.Application
            Dim wBook As Excel.Workbook
            Dim wSheet As Excel.Worksheet

            _excel = CType(CreateObject("Excel.application"), Excel.Application)
            wBook = CType(_excel.Workbooks.Add, Excel.Workbook)
            wSheet = CType(wBook.Worksheets(1), Excel.Worksheet)

            For Each dttemp As DataTable In ds.Tables

                Dim dt As System.Data.DataTable = dttemp
                Dim dc As System.Data.DataColumn
                Dim dr As System.Data.DataRow
                Dim colIndex As Integer = 0
                Dim rowIndex As Integer = 0

                For Each dc In dt.Columns
                    colIndex = colIndex + 1
                    _excel.Cells(1, colIndex) = dc.ColumnName
                Next

                For Each dr In dt.Rows
                    rowIndex = rowIndex + 1
                    colIndex = 0
                    For Each dc In dt.Columns
                        colIndex = colIndex + 1
                        _excel.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)
                    Next
                Next

                wSheet.Columns.AutoFit()

                If System.IO.File.Exists(strFileName) Then
                    Try
                        System.IO.File.Delete(strFileName)
                    Catch ex As Exception

                    End Try
                End If

            Next

            Dim savefile As New SaveFileDialog
            savefile.AddExtension = False
            savefile.Filter = "Excel|*.xls"
            savefile.FileName = strFileName
            savefile.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

            If savefile.ShowDialog = DialogResult.OK Then
                wBook.SaveAs(savefile.FileName)
            End If

            wBook.Close()
            _excel.Quit()

        Catch ex As Exception

            Try
                'dtToOO(dtTemp)
            Catch
                MessageBox.Show("No se encuentra ningun programa de calculo para exportar! Se requiere Microsoft Excel o Open Office para continuar", "ERP", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End Try

        End Try

    End Sub

    Public Sub dgvToPlanilla(dgv As DataGridView, nombre As String)

        Dim fic As String = "Temp" & "\" & nombre

        'Vendedor
        Try

            If IO.Directory.Exists("Temp") = False Then

                'Eliminar
                IO.Directory.CreateDirectory("Temp")

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, False)

            Dim cadena As String = ""

            cadena = ""
            For c As Integer = 0 To dgv.Columns.Count - 1

                If c = 0 Then
                    cadena = cadena & dgv.Columns(c).HeaderText
                Else
                    cadena = cadena & vbTab & dgv.Columns(c).HeaderText
                End If

            Next

            sw.WriteLine(cadena)

            For Each oRow As DataGridViewRow In dgv.Rows
                cadena = ""
                For c As Integer = 0 To dgv.Columns.Count - 1

                    If c = 0 Then
                        cadena = cadena & oRow.Cells(c).Value.ToString
                    Else
                        cadena = cadena & vbTab & oRow.Cells(c).Value.ToString
                    End If

                Next

                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        Dim savefile As New FolderBrowserDialog
        savefile.SelectedPath = My.Computer.FileSystem.SpecialDirectories.Desktop

        Try
            If savefile.ShowDialog = DialogResult.OK Then
                FileCopy(fic, savefile.SelectedPath & "\" & nombre & ".xls")
            End If

        Catch ex As Exception

        End Try

    End Sub

    Public Sub dtToOO(ByVal dt As DataTable)

        Dim COpenOffice As New ERP.COpenOffice

        Try
            COpenOffice.ConnectOpenOffice()

            Dim i, f, cant, j As Integer
            f = 2
            cant = 0
            j = 2

            Dim myDoc As Object, firstSheet As Object, aRange As Object
            Dim fields As Object, unoWrap As Object, sortDx As Object
            myDoc = COpenOffice.StarDesktop.loadComponentFromURL("private:factory/scalc", "_blank", 0, COpenOffice.dummyArray)
            firstSheet = myDoc.Sheets.getByIndex(0)

            'columnas
            Dim Columna = 2
            For i = 0 To dt.Columns.Count - 1
                firstSheet.getCellByPosition(Columna, f).String = dt.Columns(i).ColumnName
                Columna += 1
            Next

            f = 3
            Columna = 2

            For Each oRow As DataRow In dt.Rows
                For i = 0 To dt.Columns.Count - 1
                    If IsNumeric(oRow(i).ToString) = True Then
                        Dim NumeroTemp As String = oRow(i).ToString.Replace(".", "")
                        firstSheet.getCellByPosition(Columna, f).value = NumeroTemp
                    Else
                        firstSheet.getCellByPosition(Columna, f).String = oRow(i).ToString.Replace(".", "")
                    End If

                    Columna += 1

                Next

                Columna = 2
                f += 1

            Next


            'MsgBox(OOoMess107)
            aRange = firstSheet.getCellRangeByName("A1:B16")
            fields = COpenOffice.CreateUnoStruct("com.sun.star.table.TableSortField", 0)
            fields(0).Field = 1
            fields(0).IsAscending = True
            fields(0).IsCaseSensitive = True

            ' il faut prciser quel type de squence est transmis la proprit SortFields
            ' you must specify which type of sequence is transmitted to SortFields property
            unoWrap = COpenOffice.OpenOffice.Bridge_GetValueObject
            unoWrap.set("[]com.sun.star.table.TableSortField", fields)

            ' remplissage de SortDescriptor : proprits ayant des valeurs autres que dfaut
            ' filling of SortDescriptor : properties with non-default values 
            sortDx = COpenOffice.CreateProperties("ContainsHeader", True, "SortFields", unoWrap)
            aRange.sort(sortDx)

            ' MsgBox(OOoMess108)

            ' myDoc.close(True)
            COpenOffice.DisconnectOpenOffice()

        Catch ex As Exception
            MessageBox.Show("Se produjo el siguiente error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Public Function NumeroALetra(ByVal value As Double) As String

        value = CDec(value)

        Select Case value
            Case 0 : NumeroALetra = "CERO"
            Case 1 : NumeroALetra = "UN"
            Case 2 : NumeroALetra = "DOS"
            Case 3 : NumeroALetra = "TRES"
            Case 4 : NumeroALetra = "CUATRO"
            Case 5 : NumeroALetra = "CINCO"
            Case 6 : NumeroALetra = "SEIS"
            Case 7 : NumeroALetra = "SIETE"
            Case 8 : NumeroALetra = "OCHO"
            Case 9 : NumeroALetra = "NUEVE"
            Case 10 : NumeroALetra = "DIEZ"
            Case 11 : NumeroALetra = "ONCE"
            Case 12 : NumeroALetra = "DOCE"
            Case 13 : NumeroALetra = "TRECE"
            Case 14 : NumeroALetra = "CATORCE"
            Case 15 : NumeroALetra = "QUINCE"
            Case Is < 20 : NumeroALetra = "DIECI" & NumeroALetra(value - 10)
            Case 20 : NumeroALetra = "VEINTE"
            Case Is < 30 : NumeroALetra = "VEINTI" & NumeroALetra(value - 20)
            Case 30 : NumeroALetra = "TREINTA"
            Case 40 : NumeroALetra = "CUARENTA"
            Case 50 : NumeroALetra = "CINCUENTA"
            Case 60 : NumeroALetra = "SESENTA"
            Case 70 : NumeroALetra = "SETENTA"
            Case 80 : NumeroALetra = "OCHENTA"
            Case 90 : NumeroALetra = "NOVENTA"
            Case Is < 100 : NumeroALetra = NumeroALetra(Int(value \ 10) * 10) & " Y " & NumeroALetra(value Mod 10)
            Case 100 : NumeroALetra = "CIEN"
            Case Is < 200 : NumeroALetra = "CIENTO " & NumeroALetra(value - 100)
            Case 200, 300, 400, 600, 800 : NumeroALetra = NumeroALetra(Int(value \ 100)) & "CIENTOS"
            Case 500 : NumeroALetra = "QUINIENTOS"
            Case 700 : NumeroALetra = "SETECIENTOS"
            Case 900 : NumeroALetra = "NOVECIENTOS"
            Case Is < 1000 : NumeroALetra = NumeroALetra(Int(value \ 100) * 100) & " " & NumeroALetra(value Mod 100)
            Case 1000 : NumeroALetra = "MIL"
            Case Is < 2000 : NumeroALetra = "MIL " & NumeroALetra(value Mod 1000)
            Case Is < 1000000 : NumeroALetra = NumeroALetra(Int(value \ 1000)) & " MIL"
                If value Mod 1000 Then NumeroALetra = NumeroALetra & " " & NumeroALetra(value Mod 1000)
            Case 1000000 : NumeroALetra = "UN MILLON"
            Case Is < 2000000 : NumeroALetra = "UN MILLON " & NumeroALetra(value Mod 1000000)
            Case Is < 1000000000000.0# : NumeroALetra = NumeroALetra(Int(value / 1000000)) & " MILLONES "
                If (value - Int(value / 1000000) * 1000000) Then NumeroALetra = NumeroALetra & " " & NumeroALetra(value - Int(value / 1000000) * 1000000)
            Case 1000000000000.0# : NumeroALetra = "UN BILLON"
            Case Is < 2000000000000.0# : NumeroALetra = "UN BILLON " & NumeroALetra(value - Int(value / 1000000000000.0#) * 1000000000000.0#)
            Case Else : NumeroALetra = NumeroALetra(Int(value / 1000000000000.0#)) & " BILLONES"
                If (value - Int(value / 1000000000000.0#) * 1000000000000.0#) Then NumeroALetra = NumeroALetra & " " & NumeroALetra(value - Int(value / 1000000000000.0#) * 1000000000000.0#)
        End Select
    End Function

     Public Function NumeroALetra2(ByVal numero As String) As String
        Dim palabras, entero, dec, flag As String

        entero = ""
        dec = ""

        Dim num, x, y As Integer

        flag = "N"

        If Mid(numero, 1, 1) = "-" Then
            numero = Mid(numero, 2, numero.ToString.Length - 1).ToString
            palabras = "menos "
        End If

        For x = 1 To numero.ToString.Length
            If Mid(numero, 1, 1) = "0" Then
                numero = Trim(Mid(numero, 2, numero.ToString.Length).ToString)
                If Trim(numero.ToString.Length) = 0 Then palabras = ""
            Else
                Exit For
            End If
        Next

        For y = 1 To Len(numero)
            'If Mid(numero, y, 1) = "." Then
            If Mid(numero, y, 1) = "," Then
                flag = "S"
            Else
                If flag = "N" Then
                    entero = entero + Mid(numero, y, 1)
                Else
                    dec = dec + Mid(numero, y, 1)
                End If
            End If
        Next y

        If Len(dec) = 1 Then dec = dec & "0"

        flag = "N"

        If Val(numero) <= 999999999 Then
            For y = Len(entero) To 1 Step -1
                num = Len(entero) - (y - 1)
                Select Case y
                    Case 3, 6, 9

                        Select Case Mid(entero, num, 1)
                            Case "1"
                                If Mid(entero, num + 1, 1) = "0" And Mid(entero, num + 2, 1) = "0" Then
                                    palabras = palabras & "CIEN "
                                Else
                                    palabras = palabras & "CIENTO "
                                End If
                            Case "2"
                                palabras = palabras & "DOSCIENTOS "
                            Case "3"
                                palabras = palabras & "TRESCIENTOS "
                            Case "4"
                                palabras = palabras & "CUATROCIENTOS "
                            Case "5"
                                palabras = palabras & "QUINIENTOS "
                            Case "6"
                                palabras = palabras & "SEISCIENTOS "
                            Case "7"
                                palabras = palabras & "SETECIENTOS "
                            Case "8"
                                palabras = palabras & "OCHOCIENTOS "
                            Case "9"
                                palabras = palabras & "NOVECIENTOS "
                        End Select
                    Case 2, 5, 8

                        Select Case Mid(entero, num, 1)
                            Case "1"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    flag = "S"
                                    palabras = palabras & "DIEZ "
                                End If
                                If Mid(entero, num + 1, 1) = "1" Then
                                    flag = "S"
                                    palabras = palabras & "ONCE "
                                End If
                                If Mid(entero, num + 1, 1) = "2" Then
                                    flag = "S"
                                    palabras = palabras & "DOCE "
                                End If
                                If Mid(entero, num + 1, 1) = "3" Then
                                    flag = "S"
                                    palabras = palabras & "TRECE "
                                End If
                                If Mid(entero, num + 1, 1) = "4" Then
                                    flag = "S"
                                    palabras = palabras & "CATORCE "
                                End If
                                If Mid(entero, num + 1, 1) = "5" Then
                                    flag = "S"
                                    palabras = palabras & "QUINCE "
                                End If
                                If Mid(entero, num + 1, 1) > "5" Then
                                    flag = "N"
                                    palabras = palabras & "DIECI"
                                End If
                            Case "2"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "VEINTE "
                                    flag = "S"
                                Else
                                    palabras = palabras & "VEINTI"
                                    flag = "N"
                                End If
                            Case "3"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "TREINTA "
                                    flag = "S"
                                Else
                                    palabras = palabras & "TREINTA Y "
                                    flag = "N"
                                End If
                            Case "4"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "CUARENTA"
                                    flag = "S"
                                Else
                                    palabras = palabras & "CUARENTA Y "
                                    flag = "N"
                                End If
                            Case "5"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "CINCUENTA "
                                    flag = "S"
                                Else
                                    palabras = palabras & "CINCUENTA Y "
                                    flag = "N"
                                End If
                            Case "6"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "SESENTA "
                                    flag = "S"
                                Else
                                    palabras = palabras & "SESENTA Y "
                                    flag = "N"
                                End If
                            Case "7"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "SETENTA "
                                    flag = "S"
                                Else
                                    palabras = palabras & "SETENTA Y "
                                    flag = "N"
                                End If
                            Case "8"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "OCHENTA "
                                    flag = "S"
                                Else
                                    palabras = palabras & "OCHENTA Y "
                                    flag = "N"
                                End If
                            Case "9"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "NOVENTA "
                                    flag = "S"
                                Else
                                    palabras = palabras & "NOVENTA Y "
                                    flag = "N"
                                End If
                        End Select
                    Case 1, 4, 7

                        Select Case Mid(entero, num, 1)
                            Case "1"
                                If flag = "N" Then
                                    If y = 1 Then
                                        palabras = palabras & "UNO "
                                    Else
                                        palabras = palabras & "UN "
                                    End If
                                End If
                            Case "2"
                                If flag = "N" Then palabras = palabras & "DOS "
                            Case "3"
                                If flag = "N" Then palabras = palabras & "TRES "
                            Case "4"
                                If flag = "N" Then palabras = palabras & "CUATRO "
                            Case "5"
                                If flag = "N" Then palabras = palabras & "CINCO "
                            Case "6"
                                If flag = "N" Then palabras = palabras & "SEIS "
                            Case "7"
                                If flag = "N" Then palabras = palabras & "SIETE "
                            Case "8"
                                If flag = "N" Then palabras = palabras & "OCHO "
                            Case "9"
                                If flag = "N" Then palabras = palabras & "NUEVE "
                        End Select
                End Select


                If y = 4 Then
                    If numero = 1000 Then palabras = "MIL"
                    If numero < 2000 Then palabras = "MIL " & palabras(numero Mod 1000)
                    If 1000000 Then palabras = palabras & "MIL "
                End If

                If y = 7 Then
                    If Len(entero) = 7 And Mid(entero, 1, 1) = "1" Then
                        palabras = palabras & "MILLON "
                    Else
                        palabras = palabras & "MILLONES "
                    End If
                End If
            Next y


            If dec <> "" Then
                NumeroALetra2 = palabras & "con " & dec
            Else
                NumeroALetra2 = palabras
            End If
        Else
            NumeroALetra2 = ""
        End If

    End Function

    Function ValidarDigitoVerificador(ByVal RUC As String, Optional ByVal MostrarMensaje As Boolean = True, Optional ByVal Validar As Boolean = False) As Boolean

        ValidarDigitoVerificador = False

        'Validar el RUC
        'Si no tiene guion (-) Retornar TRUE
        If RUC.Contains("-") = False Then
            If IsNumeric(RUC) = True And Validar = False Then
                Return True
            Else
                Return False
            End If
        End If

        If RUC.Contains(" ") = True And Validar = True Then
            Return False
        End If


        Dim basemax As Integer = 11
        Dim codigo As Long
        Dim DigitoVerificador As String = ""
        Dim numero_al As String = ""
        Dim numero As String = ""

        numero = RUC.Split("-")(0)
        DigitoVerificador = RUC.Split("-")(1)

        Dim i
        For i = 1 To Len(numero)
            Dim c
            c = Mid$(numero, i, 1)
            codigo = Asc(UCase(c))
            If Not (codigo >= 48 And codigo <= 57) Then
                numero_al = numero_al & codigo
            Else
                numero_al = numero_al & c
            End If
        Next

        Dim k : Dim total
        k = 2
        total = 0

        For i = Len(numero_al) To 1 Step -1
            If (k > basemax) Then k = 2
            Dim numero_aux
            numero_aux = Val(Mid(numero_al, i, 1))
            total = total + (numero_aux * k)
            k = k + 1
        Next


        Dim resto : Dim digito
        resto = total Mod 11
        If (resto > 1) Then
            digito = 11 - resto
        Else
            digito = 0
        End If

        If digito.ToString = DigitoVerificador Then
            Return True
        Else
            If MostrarMensaje = True Then
                MessageBox.Show("El digito verificador no es correcto! Deberia ser: " & digito.ToString, "Digito Verificador", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Return False
            End If
        End If

    End Function

    Public Function ControlarNroDocumento(ByVal NroDocumento As String) As Boolean

        If NroDocumento Is Nothing Then
            MessageBox.Show("El nro no es correcto!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If NroDocumento.Length < 9 Then
            MessageBox.Show("El nro de Factura debe tener como minimo 9 digitos!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If NroDocumento.Contains(" ") = True Then
            MessageBox.Show("El numero de documento no debe contener espacios!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If NroDocumento.Contains("-") = False Then
            MessageBox.Show("El numero de documento no esta separado por guiones!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        Dim Documento() As String = NroDocumento.Split("-")

        If Documento.GetLength(0) <> 3 Then
            MessageBox.Show("El nro do decumento no tiene 2 guiones!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If IsNumeric(Documento(0)) = False Then
            MessageBox.Show("La referencia a la sucursal no es valida!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If Documento(0).Length <> 3 Then
            MessageBox.Show("La referencia a la sucursal debe tener 3 digitos!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If IsNumeric(Documento(1)) = False Then
            MessageBox.Show("La referencia al punto de expedicion no es valida!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If Documento(1).Length <> 3 Then
            MessageBox.Show("La referencia al punto de expedicion debe tener 3 digitos!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If IsNumeric(Documento(2)) = False Then
            MessageBox.Show("El nro de documento no es valida!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If Documento(2).Substring(0, 1) = "0" Then
            MessageBox.Show("El nro de documento debe ser sin ceros a la izquierda!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        For I As Integer = 0 To Documento(2).Length - 1
            Dim caracter = Documento(2).Substring(I, 1)
            Select Case caracter
                Case "-"
                    MessageBox.Show("El nro de documento debe ser numerico solo, sin simbolos y sin ceros a la izquierda!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Return False
                Case ","
                    MessageBox.Show("El nro de documento debe ser numerico solo, sin simbolos y sin ceros a la izquierda!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Return False
                Case "."
                    MessageBox.Show("El nro de documento debe ser numerico solo, sin simbolos y sin ceros a la izquierda!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Return False
            End Select
        Next


        If NroDocumento.Length > 15 Then
            MessageBox.Show("El nro no puede ser mayor a 15 digitos!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        Return True

    End Function

    Public Function ControlarTimbrado(ByVal Timbrado As String) As Boolean

        If Timbrado Is Nothing Then
            Return False
        End If

        If IsNumeric(Timbrado) = False Then
            MessageBox.Show("El timbrado tiene q ser solo numerico!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If Timbrado.Length < 8 Then
            MessageBox.Show("El Timbrado debe tener como minimo 8 digitos!", "Formato Incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        Return True

    End Function

    Public Shared Function CheckForInternetConnection(Optional ByVal vMostrarError As Boolean = False) As Boolean

        CheckForInternetConnection = False

        If My.Computer.Network.IsAvailable() Then
            Try
                If My.Computer.Network.Ping("www.google.com", 1000) Then
                    CheckForInternetConnection = True
                Else
                    CheckForInternetConnection = False
                End If
            Catch exint As System.Net.NetworkInformation.PingException
                If vMostrarError = True Then
                    MsgBox("Servicio de Internet No Responde ")
                End If
            Finally
            End Try
        Else
            If vMostrarError = True Then
                MsgBox("No Hay Conexión de Red")
            End If
            CheckForInternetConnection = False
        End If

        Return CheckForInternetConnection

    End Function

    Public Sub DataGridColumnasNumericas(DG As DataGridView, ByRef ColumnasNumericas() As String, Optional Decimales As Boolean = False)

        Dim CantidadDecimal As Integer = 0

        If Decimales Then
            CantidadDecimal = 2
        End If


        For c As Integer = 0 To ColumnasNumericas.Length - 1
            If DG.RowCount > 0 Then
                DG.Columns(ColumnasNumericas(c)).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                DG.Columns(ColumnasNumericas(c)).DefaultCellStyle.Format = "N" & CantidadDecimal.ToString
            End If

        Next

    End Sub

    Public Sub DataGridColumnasCentradas(DG As DataGridView, ByRef ColumnasCentradas() As String)
        For c As Integer = 0 To ColumnasCentradas.Length - 1
            DG.Columns(ColumnasCentradas(c)).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        Next
    End Sub

    Public Sub DataGridColumnasVisibles(DG As DataGridView, ByRef ColumnasVisible() As String)
        For Each c As DataGridViewColumn In DG.Columns
            If ColumnasVisible.Contains(c.Name) = True Then
                c.Visible = True
            Else
                c.Visible = False
            End If
        Next
    End Sub


#End Region

End Class

Public Class CFormaPago

    Dim CSistema As New CSistema

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Sub Inicializar()

        'CARGAR ESTRUCTURA DEL DETALLE IMPUESTO
        dt = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleImpuesto").Clone

    End Sub

    Sub EstablecerImpuestosDetalle(ByVal dtDetalle As DataTable)

        dt.Rows.Clear()

        For Each oRow As DataRow In dtDetalle.Rows

            'Verificar si existe el impuesto en el detalle
            If dt.Select(" IDImpuesto=" & oRow("IDImpuesto").ToString).Length = 0 Then

                Dim NewRow As DataRow = dt.NewRow

                NewRow("IDTransaccion") = 0
                NewRow("Impuesto") = oRow("Impuesto").ToString
                NewRow("IDImpuesto") = oRow("IDImpuesto").ToString
                NewRow("Total") = oRow("Total").ToString
                NewRow("TotalImpuesto") = oRow("TotalImpuesto").ToString
                NewRow("TotalDiscriminado") = oRow("TotalDiscriminado").ToString

                dt.Rows.Add(NewRow)

            Else

                For Each CurrentRow As DataRow In dt.Select(" IDImpuesto=" & oRow("IDImpuesto").ToString)

                    Dim Total As Decimal = Math.Round(CDec(oRow("Total").ToString), 0)
                    Dim TotalImpuesto As Decimal = CDec(oRow("TotalImpuesto").ToString)
                    Dim TotalDiscriminado As Decimal = CDec(oRow("TotalDiscriminado").ToString)

                    CurrentRow("Total") = CDec(CurrentRow("Total")) + Total
                    CurrentRow("TotalImpuesto") = CDec(CurrentRow("TotalImpuesto")) + TotalImpuesto
                    CurrentRow("TotalDiscriminado") = CDec(CurrentRow("TotalDiscriminado")) + TotalDiscriminado

                Next

            End If

        Next

    End Sub

    Function Guardar(ByVal IDTransaccion As Integer) As Boolean

        Guardar = True

        For Each oRow As DataRow In dt.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDImpuesto", oRow("IDImpuesto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Total", oRow("Total").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalImpuesto", oRow("TotalImpuesto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalDiscriminado", oRow("TotalDiscriminado").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpDetalleImpuesto", False, False, MensajeRetorno) = False Then
                Return False
            End If

        Next

        Return True

    End Function

End Class

﻿Public Class frmOperaciones

    Public Property Procesado As Boolean
    Public Property vNuevo As Boolean
    Public Property vEditar As Boolean
    Public Property ID As Integer

    Dim CSistema As New CSistema
    Dim SQL As String = ""
    Dim dtSucursal As DataTable

    Sub Inicializar()
        CargarInformacion()
    End Sub

    Sub CargarInformacion()

        Dim dtTipoProducto As New DataTable
        Dim dtProducto As New DataTable

        dtpHora.Text = Now.ToLongTimeString

        CSistema.SqlToComboBox(cbxTipoOperacion, "Select ID,Descripcion from TipoOperacionAutomatica")
        CSistema.SqlToComboBox(cbxTipoKardex, "Select ID,Descripcion from TipoRecalculoKardex")

        CSistema.SqlToComboBox(cbxTipoProducto, "Select ID,Descripcion from TipoProducto")
        CSistema.SqlToComboBox(cbxProducto, "Select ID, Concat(Referencia, '-', Descripcion) from Producto where estado = 1")

        'Dia de la semana
        cbxDiaSemana.DisplayMember = "DIA"
        cbxDiaSemana.ValueMember = "ID"
        Dim tb As New DataTable
        tb.Columns.Add("DIA", GetType(String))
        tb.Columns.Add("ID", GetType(Integer))
        tb.Rows.Add("DOMINGO", 1)
        tb.Rows.Add("LUNES", 2)
        tb.Rows.Add("MARTES", 3)
        tb.Rows.Add("MIERCOLES", 4)
        tb.Rows.Add("JUEVES", 5)
        tb.Rows.Add("VIERNES", 6)
        tb.Rows.Add("SABADO", 7)

        cbxDiaSemana.DataSource = tb

        If vNuevo = False And vEditar = False Then
            btnGuardar.Enabled = False
        Else
            btnGuardar.Enabled = True
        End If


        '''''''''''''''''''''''''''''Regenerar Asientoas
        chkGenerarTodo.Checked = False

        'Sucursal
        dtSucursal = CSistema.ExecuteToDataTable("Select ID, Descripcion from Sucursal")
        CSistema.SqlToComboBox(cbxSucursal, dtSucursal)
        cbxSucursal.SelectedValue = 1

        'Documentos
        cbxDocumento.Items.Add("VENTA")
        cbxDocumento.Items.Add("NOTA DE CREDITO")
        cbxDocumento.Items.Add("NOTA DE DEBITO")
        cbxDocumento.Items.Add("COBRANZA")
        cbxDocumento.Items.Add("MOVIMIENTO")
        cbxDocumento.Items.Add("TICKET DE BASCULA")
        cbxDocumento.Items.Add("COMPRA MERCADERIAS")
        cbxDocumento.Items.Add("DEPOSITO BANCARIO")
        ''''''''''''''''''''''''''''

        ''''''''''''''''''''''''''''Recalcular Saldo
        'Año
        nudAño.Minimum = Date.Now.Year - 10
        nudAño.Maximum = Date.Now.Year + 10
        nudAño.Value = Date.Now.Year

        'Mes
        cbxMes.SelectedIndex = Date.Now.Month - 1
        ''''''''''''''''''''''''''''

        If vNuevo = False Then
            CargarOperacion()
        End If

    End Sub

    Sub CargarOperacion()
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * from vOperacionesAutomaticas where id = " & ID)
        Dim oRow As DataRow = dt.Rows(0)
        cbxDB.Text = oRow("DB").ToString
        cbxTipoOperacion.Text = oRow("TipoOperacion").ToString
        dtpHora.Text = oRow("HoraOperacion").ToString
        cbxTipoKardex.Text = oRow("TipoRecalculoKardex").ToString
        cbxTipoProducto.Text = oRow("TipoProducto").ToString
        cbxProducto.Text = oRow("Producto").ToString
        txtFechaInicioKardex.Text = CDate(oRow("FechaInicioKardex").ToString).ToShortDateString
        txtTitulo.Text = oRow("Titulo").ToString
        txtDescripcion.Text = oRow("Descripcion").ToString
        cbxRecurrencia.Text = oRow("Recurrencia").ToString
        txtFechaOperacion.Text = oRow("FechaOperacion").ToString
        cbxDiaSemana.SelectedValue = oRow("DiaSemana")
        txtDiaMes.Text = oRow("DiaMes")
        txtSQL.Text = oRow("SQL").ToString.Replace("´", "'")

    End Sub


    Public Sub Guardar(ByVal Operacion As String)

        If ValidarOperacion() = False Then
            Exit Sub
        End If

        Dim INS As String = txtSQL.Text


        Dim param(-1) As SqlClient.SqlParameter
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DB", cbxDB.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Titulo", txtTitulo.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoOperacion", cbxTipoOperacion.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@HoraOperacion", CSistema.FormatoFechaBaseDatos(dtpHora.Text, False, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Recurrencia", cbxRecurrencia.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaOperacion", CSistema.FormatoFechaBaseDatos(txtFechaOperacion.Text, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DiaSemana", cbxDiaSemana.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DiaMes", txtDiaMes.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoRecalculoKardex", cbxTipoKardex.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoProducto", cbxTipoProducto.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProducto", cbxProducto.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaInicioKardex", CSistema.FormatoFechaBaseDatos(txtFechaInicioKardex.Text, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@SQL", INS.Replace("'", "´"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion, ParameterDirection.Input)
        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpOperacionAutomatica", False, False, MensajeRetorno, "", False) = True Then
            MessageBox.Show(MensajeRetorno, "Informe", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            Procesado = True
            Me.Close()
        Else
            MessageBox.Show(MensajeRetorno, "Informe", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Procesado = False
        End If

    End Sub

    Function ValidarOperacion() As Boolean

        Dim retorno As Boolean = True

        If cbxDB.SelectedItem Is Nothing Then
            MessageBox.Show("Debe seleccionar una base de datos!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            cbxDB.Focus()
            retorno = False
            GoTo salir
        End If

        If cbxDB.SelectedItem.ToString() = "" Then
            MessageBox.Show("Debe seleccionar una base de datos!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            cbxDB.Focus()
            retorno = False
            GoTo salir
        End If

        If cbxTipoOperacion.Text = "KARDEX" Then
            If cbxTipoKardex.Text = "" Then
                MessageBox.Show("Debe seleccionar un tipo de operacion de Kardex!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                cbxTipoKardex.Focus()
                retorno = False
                GoTo salir
            End If


            Select Case cbxTipoKardex.Text.ToUpper
                Case "PRODUCTO"
                    If cbxProducto.Text = "" Then
                        MessageBox.Show("Debe seleccionar un producto!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        cbxProducto.Focus()
                        retorno = False
                        GoTo salir
                    End If

                Case "TIPOPRODUCTO"
                    If cbxTipoProducto.Text = "" Then
                        MessageBox.Show("Debe seleccionar un producto!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        cbxProducto.Focus()
                        retorno = False
                        GoTo salir
                    End If


                Case "TODOS"

            End Select

        Else

            If cbxTipoOperacion.Text = "REGENERAR ASIENTO" Then

                If StoreProcedureRegenerarAsiento() = "" Then
                    MessageBox.Show("Seleccione un documento para continuar!", "Asiento", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    retorno = False
                    GoTo salir
                End If

            End If

            If txtSQL.Text = "" Then
                MessageBox.Show("Debe redactar un script SQL correcto!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                cbxTipoKardex.Focus()
                retorno = False
                GoTo salir
            End If

        End If

salir:

        Return retorno
    End Function

    Sub GenerarSQL()

        'kardex
        cbxProducto.Enabled = False
        cbxTipoProducto.Enabled = False
        cbxTipoKardex.Enabled = False
        txtFechaInicioKardex.Enabled = False
        'Regenerar Asiento
        cbxSucursal.Enabled = False
        cbxDocumento.Enabled = False
        txtDesde.Enabled = False
        txtHasta.Enabled = False
        chkGenerarTodo.Enabled = False
        chkSoloConDiferencias.Enabled = False
        'Recalcular Saldos
        nudAño.Enabled = False
        cbxMes.Enabled = False
        rbAño.Enabled = False
        rbMes.Enabled = False

        Select Case cbxTipoOperacion.Text.ToUpper

            Case "MANUAL"
                txtSQL.Focus()
                txtSQL.Text = ""
                txtSQL.ReadOnly = False

                SQL = ""
                txtSQL.Text = SQL

            Case "KARDEX"

                txtSQL.ReadOnly = True
                SQL = "Exec SpRecalcularKardex "
                txtSQL.Text = SQL
                cbxTipoKardex.Enabled = True
                txtFechaInicioKardex.Enabled = True

                Select Case cbxTipoKardex.Text.ToUpper

                    Case "PRODUCTO"

                        SQL = SQL & " @Operacion = 'PRODUCTO', "
                        txtSQL.Text = SQL
                        cbxProducto.Enabled = True
                        cbxTipoProducto.Enabled = False

                        SQL = SQL & " @ID = " & cbxProducto.SelectedValue & ", "
                        txtSQL.Text = SQL

                    Case "TIPOPRODUCTO"

                        SQL = SQL & " @Operacion = 'TIPOPRODUCTO', "
                        txtSQL.Text = SQL
                        cbxProducto.Enabled = False
                        cbxTipoProducto.Enabled = True

                        SQL = SQL & " @ID = " & cbxTipoProducto.SelectedValue & ", "
                        txtSQL.Text = SQL

                    Case "TODOS"

                        SQL = SQL & " @Operacion = 'TODOS', "
                        txtSQL.Text = SQL
                        cbxProducto.Enabled = False
                        cbxTipoProducto.Enabled = False

                        SQL = SQL & " @ID = 1, "
                        txtSQL.Text = SQL

                End Select

                txtSQL.Text = SQL & " @FechaInicio='" & CSistema.FormatoFechaBaseDatos(txtFechaInicioKardex.Text, True, False) & "'"

            Case "REGENERAR ASIENTO"

                txtSQL.ReadOnly = True
                cbxSucursal.Enabled = True
                cbxDocumento.Enabled = True
                txtDesde.Enabled = True
                txtHasta.Enabled = True
                chkGenerarTodo.Enabled = True
                chkSoloConDiferencias.Enabled = True
                SQL = ""
                txtSQL.Text = SQL


                If chkGenerarTodo.Checked = False Then

                    SQL = " Exec " & StoreProcedureRegenerarAsiento()
                    CSistema.ConcatenarParametro(SQL, "@IDSucursal", cbxSucursal.SelectedValue)
                    CSistema.ConcatenarParametro(SQL, "@Todos", chkGenerarTodo.Checked.ToString)
                    CSistema.ConcatenarParametro(SQL, "@SoloConDiferencias", chkSoloConDiferencias.Checked.ToString)
                    CSistema.ConcatenarParametro(SQL, "@Desde", CSistema.FormatoFechaBaseDatos(txtDesde.GetValue, True, False))
                    CSistema.ConcatenarParametro(SQL, "@Hasta", CSistema.FormatoFechaBaseDatos(txtHasta.GetValue, True, False))

                    txtSQL.Text = SQL

                Else

                    For Each oRow As DataRow In dtSucursal.Rows

                        SQL = SQL & " Exec " & StoreProcedureRegenerarAsiento()
                        CSistema.ConcatenarParametro(SQL, "@IDSucursal", oRow("ID"))
                        CSistema.ConcatenarParametro(SQL, "@Todos", chkGenerarTodo.Checked.ToString)
                        CSistema.ConcatenarParametro(SQL, "@SoloConDiferencias", chkSoloConDiferencias.Checked.ToString)
                        CSistema.ConcatenarParametro(SQL, "@Desde", CSistema.FormatoFechaBaseDatos(txtDesde.GetValue, True, False))
                        CSistema.ConcatenarParametro(SQL, "@Hasta", CSistema.FormatoFechaBaseDatos(txtHasta.GetValue, True, False))
                        SQL = SQL & "; "
                    Next
                    txtSQL.Text = SQL
                End If

            Case "RECALCULAR SALDO"

                nudAño.Enabled = True
                cbxMes.Enabled = True
                rbAño.Enabled = True
                rbMes.Enabled = True
                SQL = ""
                txtSQL.Text = SQL

                If rbMes.Checked Then

                    SQL = "Exec SpRecalcularPlanCuentaSaldo @Año='" & nudAño.Value & "', @Mes='" & cbxMes.SelectedIndex + 1 & "' "

                Else

                    For i As Integer = 1 To 12

                        SQL = SQL & "Exec SpRecalcularPlanCuentaSaldo @Año='" & nudAño.Value & "', @Mes='" & i & "'; "
                    Next

                End If

                txtSQL.Text = SQL

        End Select
    End Sub


    Function StoreProcedureRegenerarAsiento() As String

        StoreProcedureRegenerarAsiento = ""

        Select Case cbxDocumento.SelectedIndex
            Case 0
                StoreProcedureRegenerarAsiento = "SpRegenerarAsientoVenta"
            Case 1
                StoreProcedureRegenerarAsiento = "SpRegenerarAsientoNotaCredito"
            Case 2
                StoreProcedureRegenerarAsiento = "SpRegenerarAsientoNotaDebito"
            Case 3
                StoreProcedureRegenerarAsiento = "SpRegenerarAsientoCobranza"
            Case 4
                StoreProcedureRegenerarAsiento = "SpRegenerarAsientoMovimiento"
            Case 5
                StoreProcedureRegenerarAsiento = "SpRegenerarAsientoTicketBascula"
            Case 6
                StoreProcedureRegenerarAsiento = "SpRegenerarAsientoCompra"
            Case 7
                StoreProcedureRegenerarAsiento = "SpRegenerarAsientoDepositoBancario"
        End Select

    End Function

    Private Sub frmOperaciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub MaskedTextBox2_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtDiaMes.Validating
        e.Cancel = CInt(txtDiaMes.Text) > 31
    End Sub

    Private Sub cbxRecurrencia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxRecurrencia.SelectedIndexChanged
        Select Case cbxRecurrencia.Text.ToUpper

            Case "DIARIO"
                txtDiaMes.Enabled = False
                txtFechaOperacion.Enabled = False
                cbxDiaSemana.Enabled = False
            Case "UNICA VEZ"
                txtFechaOperacion.Enabled = True
                txtDiaMes.Enabled = False
                cbxDiaSemana.Enabled = False
            Case "SEMANAL"
                cbxDiaSemana.Enabled = True
                txtFechaOperacion.Enabled = False
                txtDiaMes.Enabled = False
            Case "MENSUAL"
                txtDiaMes.Enabled = True
                txtFechaOperacion.Enabled = False
                cbxDiaSemana.Enabled = False
        End Select
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Procesado = False
        Me.Close()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If vNuevo Then
            Guardar("INS")
        End If
        If vEditar Then
            Guardar("UPD")
        End If
    End Sub

    Private Sub cbxTipoOperacion_PropertyChanged(sender As Object, e As EventArgs) Handles cbxTipoOperacion.SelectedValueChanged
        GenerarSQL()
    End Sub

    Private Sub cbxTipoKardex_PropertyChanged(sender As Object, e As EventArgs) Handles cbxTipoKardex.SelectedValueChanged
        GenerarSQL()
    End Sub

    Private Sub cbxTipoProducto_PropertyChanged(sender As Object, e As EventArgs) Handles cbxTipoProducto.SelectedValueChanged
        GenerarSQL()
    End Sub

    Private Sub cbxProducto_PropertyChanged(sender As Object, e As EventArgs) Handles cbxProducto.SelectedValueChanged
        GenerarSQL()
    End Sub

    Private Sub txtFechaInicioKardex_Validated(sender As Object, e As EventArgs) Handles txtFechaInicioKardex.Validated
        GenerarSQL()
    End Sub

    Private Sub cbxSucursal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSucursal.SelectedIndexChanged
        GenerarSQL()
    End Sub

    Private Sub cbxDocumento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxDocumento.SelectedIndexChanged
        GenerarSQL()
    End Sub

    Private Sub txtDesde_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtDesde.TeclaPrecionada
        GenerarSQL()
    End Sub

    Private Sub txtHasta_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtHasta.TeclaPrecionada
        GenerarSQL()
    End Sub

    Private Sub chkGenerarTodo_CheckedChanged(sender As Object, e As EventArgs) Handles chkGenerarTodo.CheckedChanged
        GenerarSQL()
    End Sub

    Private Sub chkSoloConDiferencias_CheckedChanged(sender As Object, e As EventArgs) Handles chkSoloConDiferencias.CheckedChanged
        GenerarSQL()
    End Sub

    Private Sub nudAño_ValueChanged(sender As Object, e As EventArgs) Handles nudAño.ValueChanged
        GenerarSQL()
    End Sub

    Private Sub cbxMes_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbxMes.SelectedValueChanged
        GenerarSQL()
    End Sub

    Private Sub rbAño_CheckedChanged(sender As Object, e As EventArgs) Handles rbAño.CheckedChanged
        GenerarSQL()
    End Sub

    Private Sub rbMes_CheckedChanged(sender As Object, e As EventArgs) Handles rbMes.CheckedChanged
        GenerarSQL()
    End Sub
End Class
﻿Imports System.Threading

Public Class Form1

    Dim CSistema As New CSistema

    Dim dtOperaciones As New DataTable
    Dim dtOperacionesCorriendo As New DataTable
    Dim dtOperacionesRealizadas As New DataTable
    Dim RowSiguiente As DataRow
    Dim RowProcesar As DataRow
    Dim tProcesar As Thread

    Sub Inicializar()

        Listar()

    End Sub

    Sub Listar()

        dtOperacionesCorriendo = CSistema.ExecuteToDataTable("Select * from vOperacionesAutomaticas where ejecutando = 1 order by SiguienteEjecucion asc, HoraOperacion asc")
        dtOperaciones = CSistema.ExecuteToDataTable("Select * from vOperacionesAutomaticas where pendiente=1 order by SiguienteEjecucion asc, HoraOperacion asc")
        CSistema.dtToGrid(dgvOperacionesAutomaticas, dtOperaciones)

        CSistema.DataGridColumnasVisibles(dgvOperacionesAutomaticas, {"ID", "Activo", "Ejecutando", "DB", "Titulo", "Descripcion", "RecurrenciaObs", "SiguienteEjecucion", "HoraOperacion"})

        dtOperacionesRealizadas = CSistema.ExecuteToDataTable("Select * from OperacionAutomaticaRealizada order by FechaFin asc, FechaInicio Desc")
        CSistema.dtToGrid(dgvOperacionesRealizadas, dtOperacionesRealizadas)

        CSistema.DataGridColumnasVisibles(dgvOperacionesRealizadas, {"ID", "Titulo", "Descripcion", "FechaInicio", "HoraInicio", "FechaFin", "HoraFin"})
        Verificar()

    End Sub

    Sub Verificar()

        If dtOperaciones.Rows.Count = 0 Then
            Exit Sub
        End If
        Dim PrimeraFecha As Date

        Try
            PrimeraFecha = dtOperaciones.Select("Ejecutando = 'False'")(0)("SiguienteEjecucion")
        Catch ex As Exception
            Exit Sub
        End Try
        Dim dtOperacionesCercanas As DataTable = dtOperaciones.Clone()
        For Each oRow As DataRow In dtOperaciones.Rows
            Dim Row As DataRow = dtOperacionesCercanas.NewRow()
            If (CDate(oRow("SiguienteEjecucion")) < PrimeraFecha) And (CBool(oRow("Ejecutando")) = False) Then
                dtOperacionesCercanas.Clear()
                PrimeraFecha = CDate(oRow("SiguienteEjecucion"))
                For i = 0 To dtOperaciones.Columns.Count - 1
                    Row(i) = oRow(i)
                Next
                dtOperacionesCercanas.Rows.Add(Row)
            Else
                If CDate(oRow("SiguienteEjecucion")) = PrimeraFecha Then
                    For i = 0 To dtOperaciones.Columns.Count - 1
                        Row(i) = oRow(i)
                    Next
                    dtOperacionesCercanas.Rows.Add(Row)
                End If
            End If
        Next
        Dim PrimeraHora As TimeSpan = CType(dtOperacionesCercanas.Rows(0)("HoraOperacion"), TimeSpan)
        Dim IDPrimeraOperacion As Integer = dtOperacionesCercanas.Rows(0)("ID")
        RowSiguiente = dtOperacionesCercanas.Rows(0)
        For Each dRow As DataRow In dtOperacionesCercanas.Rows

            If CType(dRow("HoraOperacion"), TimeSpan) < PrimeraHora Then
                PrimeraHora = CType(dRow("HoraOperacion"), TimeSpan)
                IDPrimeraOperacion = dRow("ID")
                RowSiguiente = dRow
            End If

        Next

        dtpHora.Text = PrimeraHora.ToString
        dtpFecha.Text = PrimeraFecha.ToShortDateString
    End Sub

    Sub Procesar(ByVal Row As DataRow)

        RowProcesar = Row

        If dtOperacionesCorriendo.Rows.Count > 0 Then

            Dim Indice As List(Of String) = BuildIndex(dtOperacionesCorriendo, "ID")

            If Indice.BinarySearch(Row("ID").ToString) >= 0 Then
                Exit Sub
            End If
        End If

        tProcesar = New Thread(AddressOf ProcesarAsync)
        tProcesar.Start()
        Listar()

    End Sub

    Sub ProcesarAsync()
        If RowProcesar Is Nothing Then
            Exit Sub
        End If

        Dim Ejecutar As Boolean = CBool(CSistema.ExecuteScalar("Select [dbo].FEjecutarOperacionAutomatica(" & RowProcesar("ID") & ")", VGCadenaConexion))
        If Not Ejecutar Then
            Exit Sub
        End If

        Dim SQL As String = RowProcesar("SQL").ToString.Replace("´", "'")
        Dim con As String = VGCadenaConexion.Replace("Remmele", RowProcesar("DB").ToString)
        CSistema.ExecuteNonQuery("Exec SpOperacionAutomatica @ID= " & RowProcesar("ID") & ", @Operacion='INICIO' ")
        CSistema.ExecuteNonQuery(SQL, con, 1296000)
        CSistema.ExecuteNonQuery("Exec SpOperacionAutomatica @ID= " & RowProcesar("ID") & ", @Operacion='FIN' ")

    End Sub

    Sub Eliminar()

    End Sub

    Sub PausarReanudar()

    End Sub

    Private Function BuildIndex(table As DataTable, keyColumnIndex As String) As List(Of String)
        Dim index As New List(Of String)(table.Rows.Count)
        For Each row As DataRow In table.Rows
            index.Add(row(keyColumnIndex))
        Next
        index.Sort()
        Return index
    End Function

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick

        lblHoraActual.Text = Date.Now.ToString("HH:mm:ss")
        If Now.ToShortDateString = dtpFecha.Value.ToShortDateString Then
            If lblHoraActual.Text >= dtpHora.Value.ToString("HH:mm:ss") Then
                Procesar(RowSiguiente)
            End If
        End If
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Dim frm As New frmOperaciones

        frm.vNuevo = True
        frm.ShowDialog()

        If frm.Procesado Then
            Listar()
        End If

    End Sub

    Private Sub btnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click

        Dim frm As New frmOperaciones
        frm.vEditar = True
        frm.vNuevo = False
        frm.ID = dgvOperacionesAutomaticas.Rows(dgvOperacionesAutomaticas.CurrentRow.Index).Cells("ID").Value
        frm.ShowDialog()

        If frm.Procesado Then
            Listar()
        End If

    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

    Private Sub btnPausar_Click(sender As Object, e As EventArgs) Handles btnPausar.Click
        PausarReanudar()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        Listar()
    End Sub
End Class

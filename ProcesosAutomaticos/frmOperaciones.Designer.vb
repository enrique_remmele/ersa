﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOperaciones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtTitulo = New System.Windows.Forms.TextBox()
        Me.cbxProducto = New System.Windows.Forms.ComboBox()
        Me.cbxTipoProducto = New System.Windows.Forms.ComboBox()
        Me.cbxTipoKardex = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cbxDB = New System.Windows.Forms.ComboBox()
        Me.cbxTipoOperacion = New System.Windows.Forms.ComboBox()
        Me.txtDiaMes = New System.Windows.Forms.MaskedTextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbxDiaSemana = New System.Windows.Forms.ComboBox()
        Me.dtpHora = New System.Windows.Forms.DateTimePicker()
        Me.txtFechaOperacion = New System.Windows.Forms.MaskedTextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cbxRecurrencia = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtFechaInicioKardex = New System.Windows.Forms.MaskedTextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.txtSQL = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkSoloConDiferencias = New System.Windows.Forms.CheckBox()
        Me.chkGenerarTodo = New System.Windows.Forms.CheckBox()
        Me.cbxDocumento = New System.Windows.Forms.ComboBox()
        Me.lblDocumento = New System.Windows.Forms.Label()
        Me.cbxSucursal = New System.Windows.Forms.ComboBox()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblHasta = New System.Windows.Forms.Label()
        Me.lblDesde = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.nudAño = New System.Windows.Forms.NumericUpDown()
        Me.cbxMes = New System.Windows.Forms.ComboBox()
        Me.lblAño = New System.Windows.Forms.Label()
        Me.rbAño = New System.Windows.Forms.RadioButton()
        Me.rbMes = New System.Windows.Forms.RadioButton()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.nudAño, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.GroupBox3)
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.txtDescripcion)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.txtTitulo)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.cbxDB)
        Me.Panel1.Controls.Add(Me.cbxTipoOperacion)
        Me.Panel1.Controls.Add(Me.txtDiaMes)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.cbxDiaSemana)
        Me.Panel1.Controls.Add(Me.dtpHora)
        Me.Panel1.Controls.Add(Me.txtFechaOperacion)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.cbxRecurrencia)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.btnCancelar)
        Me.Panel1.Controls.Add(Me.btnGuardar)
        Me.Panel1.Controls.Add(Me.txtSQL)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(823, 434)
        Me.Panel1.TabIndex = 0
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(9, 127)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(66, 13)
        Me.Label13.TabIndex = 45
        Me.Label13.Text = "Descripcion:"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(117, 125)
        Me.txtDescripcion.Multiline = True
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(306, 44)
        Me.txtDescripcion.TabIndex = 44
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(9, 96)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(36, 13)
        Me.Label12.TabIndex = 43
        Me.Label12.Text = "Titulo:"
        '
        'txtTitulo
        '
        Me.txtTitulo.Location = New System.Drawing.Point(117, 94)
        Me.txtTitulo.Name = "txtTitulo"
        Me.txtTitulo.Size = New System.Drawing.Size(306, 20)
        Me.txtTitulo.TabIndex = 42
        '
        'cbxProducto
        '
        Me.cbxProducto.Enabled = False
        Me.cbxProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbxProducto.FormattingEnabled = True
        Me.cbxProducto.Location = New System.Drawing.Point(103, 72)
        Me.cbxProducto.Name = "cbxProducto"
        Me.cbxProducto.Size = New System.Drawing.Size(212, 21)
        Me.cbxProducto.TabIndex = 41
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbxTipoProducto.FormattingEnabled = True
        Me.cbxTipoProducto.Location = New System.Drawing.Point(103, 46)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.Size = New System.Drawing.Size(212, 21)
        Me.cbxTipoProducto.TabIndex = 40
        '
        'cbxTipoKardex
        '
        Me.cbxTipoKardex.Enabled = False
        Me.cbxTipoKardex.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbxTipoKardex.FormattingEnabled = True
        Me.cbxTipoKardex.Location = New System.Drawing.Point(103, 19)
        Me.cbxTipoKardex.Name = "cbxTipoKardex"
        Me.cbxTipoKardex.Size = New System.Drawing.Size(212, 21)
        Me.cbxTipoKardex.TabIndex = 39
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(9, 15)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(80, 13)
        Me.Label11.TabIndex = 38
        Me.Label11.Text = "Base de Datos:"
        '
        'cbxDB
        '
        Me.cbxDB.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbxDB.FormattingEnabled = True
        Me.cbxDB.Items.AddRange(New Object() {"Remmele", "GRANERSA", "ALTOHONDO2016"})
        Me.cbxDB.Location = New System.Drawing.Point(117, 12)
        Me.cbxDB.Name = "cbxDB"
        Me.cbxDB.Size = New System.Drawing.Size(214, 21)
        Me.cbxDB.TabIndex = 37
        '
        'cbxTipoOperacion
        '
        Me.cbxTipoOperacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbxTipoOperacion.FormattingEnabled = True
        Me.cbxTipoOperacion.Items.AddRange(New Object() {"DIARIO", "UNICA VEZ", "SEMANAL", "MENSUAL"})
        Me.cbxTipoOperacion.Location = New System.Drawing.Point(117, 41)
        Me.cbxTipoOperacion.Name = "cbxTipoOperacion"
        Me.cbxTipoOperacion.Size = New System.Drawing.Size(212, 21)
        Me.cbxTipoOperacion.TabIndex = 36
        '
        'txtDiaMes
        '
        Me.txtDiaMes.Enabled = False
        Me.txtDiaMes.Location = New System.Drawing.Point(117, 259)
        Me.txtDiaMes.Mask = "99"
        Me.txtDiaMes.Name = "txtDiaMes"
        Me.txtDiaMes.Size = New System.Drawing.Size(32, 20)
        Me.txtDiaMes.TabIndex = 30
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(9, 262)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(66, 13)
        Me.Label10.TabIndex = 29
        Me.Label10.Text = "Dia del Mes:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(9, 235)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(94, 13)
        Me.Label9.TabIndex = 27
        Me.Label9.Text = "Dia de la Semana:"
        '
        'cbxDiaSemana
        '
        Me.cbxDiaSemana.Enabled = False
        Me.cbxDiaSemana.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbxDiaSemana.FormattingEnabled = True
        Me.cbxDiaSemana.Location = New System.Drawing.Point(117, 232)
        Me.cbxDiaSemana.Name = "cbxDiaSemana"
        Me.cbxDiaSemana.Size = New System.Drawing.Size(100, 21)
        Me.cbxDiaSemana.TabIndex = 26
        '
        'dtpHora
        '
        Me.dtpHora.CustomFormat = "HH:mm:ss"
        Me.dtpHora.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpHora.Location = New System.Drawing.Point(117, 68)
        Me.dtpHora.Name = "dtpHora"
        Me.dtpHora.Size = New System.Drawing.Size(105, 20)
        Me.dtpHora.TabIndex = 25
        Me.dtpHora.Value = New Date(2019, 3, 29, 23, 47, 0, 0)
        '
        'txtFechaOperacion
        '
        Me.txtFechaOperacion.Enabled = False
        Me.txtFechaOperacion.Location = New System.Drawing.Point(117, 206)
        Me.txtFechaOperacion.Mask = "00/00/0000"
        Me.txtFechaOperacion.Name = "txtFechaOperacion"
        Me.txtFechaOperacion.Size = New System.Drawing.Size(100, 20)
        Me.txtFechaOperacion.TabIndex = 24
        Me.txtFechaOperacion.ValidatingType = GetType(Date)
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(9, 206)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(92, 13)
        Me.Label8.TabIndex = 23
        Me.Label8.Text = "Fecha Operacion:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(9, 182)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 13)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "Recurrencia:"
        '
        'cbxRecurrencia
        '
        Me.cbxRecurrencia.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbxRecurrencia.FormattingEnabled = True
        Me.cbxRecurrencia.Items.AddRange(New Object() {"DIARIO", "UNICA VEZ", "SEMANAL", "MENSUAL"})
        Me.cbxRecurrencia.Location = New System.Drawing.Point(117, 179)
        Me.cbxRecurrencia.Name = "cbxRecurrencia"
        Me.cbxRecurrencia.Size = New System.Drawing.Size(212, 21)
        Me.cbxRecurrencia.TabIndex = 21
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 68)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(85, 13)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Hora Operacion:"
        '
        'txtFechaInicioKardex
        '
        Me.txtFechaInicioKardex.Enabled = False
        Me.txtFechaInicioKardex.Location = New System.Drawing.Point(103, 100)
        Me.txtFechaInicioKardex.Mask = "00/00/0000"
        Me.txtFechaInicioKardex.Name = "txtFechaInicioKardex"
        Me.txtFechaInicioKardex.Size = New System.Drawing.Size(100, 20)
        Me.txtFechaInicioKardex.TabIndex = 14
        Me.txtFechaInicioKardex.ValidatingType = GetType(Date)
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 100)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(71, 13)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Inicio Kardex:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 76)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Producto:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 49)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(92, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Tipo de Producto:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Tipo Kardex:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 44)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Tipo de Operacion:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(348, 281)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(267, 281)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 1
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'txtSQL
        '
        Me.txtSQL.Location = New System.Drawing.Point(12, 310)
        Me.txtSQL.MaxLength = 1000
        Me.txtSQL.Multiline = True
        Me.txtSQL.Name = "txtSQL"
        Me.txtSQL.Size = New System.Drawing.Size(411, 108)
        Me.txtSQL.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbxTipoKardex)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.cbxProducto)
        Me.GroupBox1.Controls.Add(Me.txtFechaInicioKardex)
        Me.GroupBox1.Controls.Add(Me.cbxTipoProducto)
        Me.GroupBox1.Location = New System.Drawing.Point(458, 15)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(342, 133)
        Me.GroupBox1.TabIndex = 46
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Kardex"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkSoloConDiferencias)
        Me.GroupBox2.Controls.Add(Me.chkGenerarTodo)
        Me.GroupBox2.Controls.Add(Me.cbxDocumento)
        Me.GroupBox2.Controls.Add(Me.lblDocumento)
        Me.GroupBox2.Controls.Add(Me.cbxSucursal)
        Me.GroupBox2.Controls.Add(Me.lblSucursal)
        Me.GroupBox2.Controls.Add(Me.lblHasta)
        Me.GroupBox2.Controls.Add(Me.lblDesde)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(458, 162)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(342, 145)
        Me.GroupBox2.TabIndex = 47
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Regenerar Asientos"
        '
        'chkSoloConDiferencias
        '
        Me.chkSoloConDiferencias.AutoSize = True
        Me.chkSoloConDiferencias.Enabled = False
        Me.chkSoloConDiferencias.Location = New System.Drawing.Point(103, 123)
        Me.chkSoloConDiferencias.Name = "chkSoloConDiferencias"
        Me.chkSoloConDiferencias.Size = New System.Drawing.Size(164, 17)
        Me.chkSoloConDiferencias.TabIndex = 19
        Me.chkSoloConDiferencias.Text = "Solo asientos con diferencias"
        Me.chkSoloConDiferencias.UseVisualStyleBackColor = True
        '
        'chkGenerarTodo
        '
        Me.chkGenerarTodo.AutoSize = True
        Me.chkGenerarTodo.Enabled = False
        Me.chkGenerarTodo.Location = New System.Drawing.Point(103, 100)
        Me.chkGenerarTodo.Name = "chkGenerarTodo"
        Me.chkGenerarTodo.Size = New System.Drawing.Size(162, 17)
        Me.chkGenerarTodo.TabIndex = 18
        Me.chkGenerarTodo.Text = "Generar todas las sucursales"
        Me.chkGenerarTodo.UseVisualStyleBackColor = True
        '
        'cbxDocumento
        '
        Me.cbxDocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxDocumento.Enabled = False
        Me.cbxDocumento.FormattingEnabled = True
        Me.cbxDocumento.Location = New System.Drawing.Point(103, 46)
        Me.cbxDocumento.Name = "cbxDocumento"
        Me.cbxDocumento.Size = New System.Drawing.Size(220, 21)
        Me.cbxDocumento.TabIndex = 13
        '
        'lblDocumento
        '
        Me.lblDocumento.AutoSize = True
        Me.lblDocumento.Location = New System.Drawing.Point(9, 50)
        Me.lblDocumento.Name = "lblDocumento"
        Me.lblDocumento.Size = New System.Drawing.Size(65, 13)
        Me.lblDocumento.TabIndex = 12
        Me.lblDocumento.Text = "Documento:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormattingEnabled = True
        Me.cbxSucursal.Location = New System.Drawing.Point(103, 19)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.Size = New System.Drawing.Size(220, 21)
        Me.cbxSucursal.TabIndex = 11
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(9, 23)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 10
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblHasta
        '
        Me.lblHasta.AutoSize = True
        Me.lblHasta.Location = New System.Drawing.Point(194, 77)
        Me.lblHasta.Name = "lblHasta"
        Me.lblHasta.Size = New System.Drawing.Size(38, 13)
        Me.lblHasta.TabIndex = 16
        Me.lblHasta.Text = "Hasta:"
        '
        'lblDesde
        '
        Me.lblDesde.AutoSize = True
        Me.lblDesde.Location = New System.Drawing.Point(9, 77)
        Me.lblDesde.Name = "lblDesde"
        Me.lblDesde.Size = New System.Drawing.Size(41, 13)
        Me.lblDesde.TabIndex = 14
        Me.lblDesde.Text = "Desde:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.rbMes)
        Me.GroupBox3.Controls.Add(Me.rbAño)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.nudAño)
        Me.GroupBox3.Controls.Add(Me.cbxMes)
        Me.GroupBox3.Controls.Add(Me.lblAño)
        Me.GroupBox3.Location = New System.Drawing.Point(458, 324)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(342, 94)
        Me.GroupBox3.TabIndex = 48
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Recalcular Saldos"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(9, 52)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(30, 13)
        Me.Label14.TabIndex = 10
        Me.Label14.Text = "Mes:"
        '
        'nudAño
        '
        Me.nudAño.Enabled = False
        Me.nudAño.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudAño.Location = New System.Drawing.Point(103, 20)
        Me.nudAño.Name = "nudAño"
        Me.nudAño.Size = New System.Drawing.Size(78, 26)
        Me.nudAño.TabIndex = 9
        '
        'cbxMes
        '
        Me.cbxMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxMes.Enabled = False
        Me.cbxMes.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.cbxMes.FormattingEnabled = True
        Me.cbxMes.Items.AddRange(New Object() {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SETIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"})
        Me.cbxMes.Location = New System.Drawing.Point(103, 52)
        Me.cbxMes.Name = "cbxMes"
        Me.cbxMes.Size = New System.Drawing.Size(121, 28)
        Me.cbxMes.TabIndex = 11
        '
        'lblAño
        '
        Me.lblAño.AutoSize = True
        Me.lblAño.Location = New System.Drawing.Point(9, 27)
        Me.lblAño.Name = "lblAño"
        Me.lblAño.Size = New System.Drawing.Size(29, 13)
        Me.lblAño.TabIndex = 8
        Me.lblAño.Text = "Año:"
        '
        'rbAño
        '
        Me.rbAño.AutoSize = True
        Me.rbAño.Enabled = False
        Me.rbAño.Location = New System.Drawing.Point(230, 20)
        Me.rbAño.Name = "rbAño"
        Me.rbAño.Size = New System.Drawing.Size(98, 17)
        Me.rbAño.TabIndex = 12
        Me.rbAño.TabStop = True
        Me.rbAño.Text = "Recalcular Año"
        Me.rbAño.UseVisualStyleBackColor = True
        '
        'rbMes
        '
        Me.rbMes.AutoSize = True
        Me.rbMes.Enabled = False
        Me.rbMes.Location = New System.Drawing.Point(230, 52)
        Me.rbMes.Name = "rbMes"
        Me.rbMes.Size = New System.Drawing.Size(99, 17)
        Me.rbMes.TabIndex = 13
        Me.rbMes.TabStop = True
        Me.rbMes.Text = "Recalcular Mes"
        Me.rbMes.UseVisualStyleBackColor = True
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Enabled = False
        Me.txtHasta.Fecha = New Date(2013, 5, 29, 13, 3, 23, 343)
        Me.txtHasta.Location = New System.Drawing.Point(232, 73)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(91, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 17
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Enabled = False
        Me.txtDesde.Fecha = New Date(2013, 5, 29, 13, 3, 8, 859)
        Me.txtDesde.Location = New System.Drawing.Point(103, 73)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(91, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 15
        '
        'frmOperaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(823, 434)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmOperaciones"
        Me.Text = "frmOperaciones"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.nudAño, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents txtSQL As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtFechaInicioKardex As MaskedTextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents cbxRecurrencia As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtFechaOperacion As MaskedTextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtDiaMes As MaskedTextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents cbxDiaSemana As ComboBox
    Friend WithEvents dtpHora As DateTimePicker
    Friend WithEvents cbxTipoOperacion As ComboBox
    Friend WithEvents Label11 As Label
    Friend WithEvents cbxDB As ComboBox
    Friend WithEvents cbxTipoKardex As ComboBox
    Friend WithEvents cbxProducto As ComboBox
    Friend WithEvents cbxTipoProducto As ComboBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txtDescripcion As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtTitulo As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents chkSoloConDiferencias As CheckBox
    Friend WithEvents chkGenerarTodo As CheckBox
    Friend WithEvents cbxDocumento As ComboBox
    Friend WithEvents lblDocumento As Label
    Friend WithEvents cbxSucursal As ComboBox
    Friend WithEvents lblSucursal As Label
    Friend WithEvents lblHasta As Label
    Friend WithEvents lblDesde As Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents rbMes As RadioButton
    Friend WithEvents rbAño As RadioButton
    Friend WithEvents Label14 As Label
    Friend WithEvents nudAño As NumericUpDown
    Friend WithEvents cbxMes As ComboBox
    Friend WithEvents lblAño As Label
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccesos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAccesos))
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.NuevoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExpandirNodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExpandirTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ContraerNodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContraerTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImageList101 = New System.Windows.Forms.ImageList(Me.components)
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.tvAccesos = New System.Windows.Forms.TreeView()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.gbxOperacionesEspeciales = New System.Windows.Forms.GroupBox()
        Me.lvOtrasOperaciones = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ContextMenuStrip2 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.HabilitarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DenegarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImageList201 = New System.Windows.Forms.ImageList(Me.components)
        Me.gbxOperacionesBasicas = New System.Windows.Forms.GroupBox()
        Me.chkImprimir = New ERP.ocxCHK()
        Me.chkAnular = New ERP.ocxCHK()
        Me.chkEliminar = New ERP.ocxCHK()
        Me.chkModificar = New ERP.ocxCHK()
        Me.chkAgregar = New ERP.ocxCHK()
        Me.chkVisualizar = New ERP.ocxCHK()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.lblPlanCuenta = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbxPerfil = New ERP.ocxCBX()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.gbxOperacionesEspeciales.SuspendLayout()
        Me.ContextMenuStrip2.SuspendLayout()
        Me.gbxOperacionesBasicas.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevoToolStripMenuItem, Me.EditarToolStripMenuItem, Me.EliminarToolStripMenuItem, Me.ToolStripSeparator2, Me.ExpandirNodoToolStripMenuItem, Me.ExpandirTodoToolStripMenuItem, Me.ToolStripSeparator3, Me.ContraerNodoToolStripMenuItem, Me.ContraerTodoToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(152, 170)
        '
        'NuevoToolStripMenuItem
        '
        Me.NuevoToolStripMenuItem.Name = "NuevoToolStripMenuItem"
        Me.NuevoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.NuevoToolStripMenuItem.Text = "Nuevo"
        '
        'EditarToolStripMenuItem
        '
        Me.EditarToolStripMenuItem.Name = "EditarToolStripMenuItem"
        Me.EditarToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.EditarToolStripMenuItem.Text = "Editar"
        '
        'EliminarToolStripMenuItem
        '
        Me.EliminarToolStripMenuItem.Name = "EliminarToolStripMenuItem"
        Me.EliminarToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.EliminarToolStripMenuItem.Text = "Eliminar"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(148, 6)
        '
        'ExpandirNodoToolStripMenuItem
        '
        Me.ExpandirNodoToolStripMenuItem.Name = "ExpandirNodoToolStripMenuItem"
        Me.ExpandirNodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ExpandirNodoToolStripMenuItem.Text = "Expandir nodo"
        '
        'ExpandirTodoToolStripMenuItem
        '
        Me.ExpandirTodoToolStripMenuItem.Name = "ExpandirTodoToolStripMenuItem"
        Me.ExpandirTodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ExpandirTodoToolStripMenuItem.Text = "Expandir todo"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(148, 6)
        '
        'ContraerNodoToolStripMenuItem
        '
        Me.ContraerNodoToolStripMenuItem.Name = "ContraerNodoToolStripMenuItem"
        Me.ContraerNodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ContraerNodoToolStripMenuItem.Text = "Contraer nodo"
        '
        'ContraerTodoToolStripMenuItem
        '
        Me.ContraerTodoToolStripMenuItem.Name = "ContraerTodoToolStripMenuItem"
        Me.ContraerTodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ContraerTodoToolStripMenuItem.Text = "Contraer todo"
        '
        'ImageList101
        '
        Me.ImageList101.ImageStream = CType(resources.GetObject("ImageList101.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList101.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList101.Images.SetKeyName(0, "icon.folder.close.gif")
        Me.ImageList101.Images.SetKeyName(1, "icon.folder.open.gif")
        Me.ImageList101.Images.SetKeyName(2, "form.png")
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 42)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.tvAccesos)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnImprimir)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnCancelar)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnModificar)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnGuardar)
        Me.SplitContainer1.Panel2.Controls.Add(Me.StatusStrip1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.gbxOperacionesEspeciales)
        Me.SplitContainer1.Panel2.Controls.Add(Me.gbxOperacionesBasicas)
        Me.SplitContainer1.Size = New System.Drawing.Size(648, 547)
        Me.SplitContainer1.SplitterDistance = 324
        Me.SplitContainer1.TabIndex = 1
        '
        'tvAccesos
        '
        Me.tvAccesos.AllowDrop = True
        Me.tvAccesos.CheckBoxes = True
        Me.tvAccesos.ContextMenuStrip = Me.ContextMenuStrip1
        Me.tvAccesos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvAccesos.FullRowSelect = True
        Me.tvAccesos.HideSelection = False
        Me.tvAccesos.ImageIndex = 0
        Me.tvAccesos.ImageList = Me.ImageList101
        Me.tvAccesos.Location = New System.Drawing.Point(0, 0)
        Me.tvAccesos.Name = "tvAccesos"
        Me.tvAccesos.SelectedImageIndex = 0
        Me.tvAccesos.Size = New System.Drawing.Size(320, 543)
        Me.tvAccesos.TabIndex = 0
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(235, 254)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 6
        Me.btnImprimir.Text = "&Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(158, 254)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 4
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(4, 254)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 2
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(82, 254)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 3
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 521)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(316, 22)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'gbxOperacionesEspeciales
        '
        Me.gbxOperacionesEspeciales.Controls.Add(Me.lvOtrasOperaciones)
        Me.gbxOperacionesEspeciales.Location = New System.Drawing.Point(12, 118)
        Me.gbxOperacionesEspeciales.Name = "gbxOperacionesEspeciales"
        Me.gbxOperacionesEspeciales.Size = New System.Drawing.Size(240, 109)
        Me.gbxOperacionesEspeciales.TabIndex = 1
        Me.gbxOperacionesEspeciales.TabStop = False
        Me.gbxOperacionesEspeciales.Text = "Operaciones Especiales:"
        '
        'lvOtrasOperaciones
        '
        Me.lvOtrasOperaciones.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lvOtrasOperaciones.ContextMenuStrip = Me.ContextMenuStrip2
        Me.lvOtrasOperaciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvOtrasOperaciones.FullRowSelect = True
        Me.lvOtrasOperaciones.GridLines = True
        Me.lvOtrasOperaciones.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvOtrasOperaciones.HideSelection = False
        Me.lvOtrasOperaciones.LargeImageList = Me.ImageList201
        Me.lvOtrasOperaciones.Location = New System.Drawing.Point(3, 16)
        Me.lvOtrasOperaciones.MultiSelect = False
        Me.lvOtrasOperaciones.Name = "lvOtrasOperaciones"
        Me.lvOtrasOperaciones.Size = New System.Drawing.Size(234, 90)
        Me.lvOtrasOperaciones.SmallImageList = Me.ImageList201
        Me.lvOtrasOperaciones.TabIndex = 0
        Me.lvOtrasOperaciones.UseCompatibleStateImageBehavior = False
        Me.lvOtrasOperaciones.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "ID"
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Acceso"
        Me.ColumnHeader2.Width = 165
        '
        'ContextMenuStrip2
        '
        Me.ContextMenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HabilitarToolStripMenuItem, Me.DenegarToolStripMenuItem})
        Me.ContextMenuStrip2.Name = "ContextMenuStrip2"
        Me.ContextMenuStrip2.Size = New System.Drawing.Size(120, 48)
        '
        'HabilitarToolStripMenuItem
        '
        Me.HabilitarToolStripMenuItem.Image = CType(resources.GetObject("HabilitarToolStripMenuItem.Image"), System.Drawing.Image)
        Me.HabilitarToolStripMenuItem.Name = "HabilitarToolStripMenuItem"
        Me.HabilitarToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.HabilitarToolStripMenuItem.Text = "Habilitar"
        '
        'DenegarToolStripMenuItem
        '
        Me.DenegarToolStripMenuItem.Image = CType(resources.GetObject("DenegarToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DenegarToolStripMenuItem.Name = "DenegarToolStripMenuItem"
        Me.DenegarToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.DenegarToolStripMenuItem.Text = "Denegar"
        '
        'ImageList201
        '
        Me.ImageList201.ImageStream = CType(resources.GetObject("ImageList201.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList201.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList201.Images.SetKeyName(0, "ok.png")
        Me.ImageList201.Images.SetKeyName(1, "delete.png")
        '
        'gbxOperacionesBasicas
        '
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkImprimir)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkAnular)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkEliminar)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkModificar)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkAgregar)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkVisualizar)
        Me.gbxOperacionesBasicas.Location = New System.Drawing.Point(12, 3)
        Me.gbxOperacionesBasicas.Name = "gbxOperacionesBasicas"
        Me.gbxOperacionesBasicas.Size = New System.Drawing.Size(240, 109)
        Me.gbxOperacionesBasicas.TabIndex = 0
        Me.gbxOperacionesBasicas.TabStop = False
        Me.gbxOperacionesBasicas.Text = "Operaciones Basicas"
        '
        'chkImprimir
        '
        Me.chkImprimir.BackColor = System.Drawing.Color.Transparent
        Me.chkImprimir.Color = System.Drawing.Color.Empty
        Me.chkImprimir.Location = New System.Drawing.Point(115, 74)
        Me.chkImprimir.Margin = New System.Windows.Forms.Padding(4)
        Me.chkImprimir.Name = "chkImprimir"
        Me.chkImprimir.Size = New System.Drawing.Size(104, 21)
        Me.chkImprimir.SoloLectura = False
        Me.chkImprimir.TabIndex = 5
        Me.chkImprimir.Texto = "Imprimir"
        Me.chkImprimir.Valor = False
        '
        'chkAnular
        '
        Me.chkAnular.BackColor = System.Drawing.Color.Transparent
        Me.chkAnular.Color = System.Drawing.Color.Empty
        Me.chkAnular.Location = New System.Drawing.Point(115, 51)
        Me.chkAnular.Margin = New System.Windows.Forms.Padding(4)
        Me.chkAnular.Name = "chkAnular"
        Me.chkAnular.Size = New System.Drawing.Size(104, 21)
        Me.chkAnular.SoloLectura = False
        Me.chkAnular.TabIndex = 4
        Me.chkAnular.Texto = "Anular"
        Me.chkAnular.Valor = False
        '
        'chkEliminar
        '
        Me.chkEliminar.BackColor = System.Drawing.Color.Transparent
        Me.chkEliminar.Color = System.Drawing.Color.Empty
        Me.chkEliminar.Location = New System.Drawing.Point(115, 28)
        Me.chkEliminar.Margin = New System.Windows.Forms.Padding(4)
        Me.chkEliminar.Name = "chkEliminar"
        Me.chkEliminar.Size = New System.Drawing.Size(104, 21)
        Me.chkEliminar.SoloLectura = False
        Me.chkEliminar.TabIndex = 3
        Me.chkEliminar.Texto = "Eliminar"
        Me.chkEliminar.Valor = False
        '
        'chkModificar
        '
        Me.chkModificar.BackColor = System.Drawing.Color.Transparent
        Me.chkModificar.Color = System.Drawing.Color.Empty
        Me.chkModificar.Location = New System.Drawing.Point(6, 74)
        Me.chkModificar.Margin = New System.Windows.Forms.Padding(4)
        Me.chkModificar.Name = "chkModificar"
        Me.chkModificar.Size = New System.Drawing.Size(104, 21)
        Me.chkModificar.SoloLectura = False
        Me.chkModificar.TabIndex = 2
        Me.chkModificar.Texto = "Modificar"
        Me.chkModificar.Valor = False
        '
        'chkAgregar
        '
        Me.chkAgregar.BackColor = System.Drawing.Color.Transparent
        Me.chkAgregar.Color = System.Drawing.Color.Empty
        Me.chkAgregar.Location = New System.Drawing.Point(6, 51)
        Me.chkAgregar.Margin = New System.Windows.Forms.Padding(4)
        Me.chkAgregar.Name = "chkAgregar"
        Me.chkAgregar.Size = New System.Drawing.Size(104, 21)
        Me.chkAgregar.SoloLectura = False
        Me.chkAgregar.TabIndex = 1
        Me.chkAgregar.Texto = "Agregar"
        Me.chkAgregar.Valor = False
        '
        'chkVisualizar
        '
        Me.chkVisualizar.BackColor = System.Drawing.Color.Transparent
        Me.chkVisualizar.Color = System.Drawing.Color.Empty
        Me.chkVisualizar.Location = New System.Drawing.Point(6, 28)
        Me.chkVisualizar.Margin = New System.Windows.Forms.Padding(4)
        Me.chkVisualizar.Name = "chkVisualizar"
        Me.chkVisualizar.Size = New System.Drawing.Size(104, 21)
        Me.chkVisualizar.SoloLectura = False
        Me.chkVisualizar.TabIndex = 0
        Me.chkVisualizar.Texto = "Vizualizar"
        Me.chkVisualizar.Valor = False
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel1.Location = New System.Drawing.Point(248, 10)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(122, 12)
        Me.LinkLabel1.TabIndex = 2
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Clonar permisos a otro perfil."
        '
        'lblPlanCuenta
        '
        Me.lblPlanCuenta.AutoSize = True
        Me.lblPlanCuenta.Location = New System.Drawing.Point(12, 10)
        Me.lblPlanCuenta.Name = "lblPlanCuenta"
        Me.lblPlanCuenta.Size = New System.Drawing.Size(33, 13)
        Me.lblPlanCuenta.TabIndex = 0
        Me.lblPlanCuenta.Text = "Perfil:"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.SplitContainer1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(654, 592)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.LinkLabel1)
        Me.Panel1.Controls.Add(Me.lblPlanCuenta)
        Me.Panel1.Controls.Add(Me.cbxPerfil)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(648, 33)
        Me.Panel1.TabIndex = 0
        '
        'cbxPerfil
        '
        Me.cbxPerfil.CampoWhere = Nothing
        Me.cbxPerfil.CargarUnaSolaVez = False
        Me.cbxPerfil.DataDisplayMember = Nothing
        Me.cbxPerfil.DataFilter = Nothing
        Me.cbxPerfil.DataOrderBy = Nothing
        Me.cbxPerfil.DataSource = Nothing
        Me.cbxPerfil.DataValueMember = Nothing
        Me.cbxPerfil.dtSeleccionado = Nothing
        Me.cbxPerfil.FormABM = Nothing
        Me.cbxPerfil.Indicaciones = Nothing
        Me.cbxPerfil.Location = New System.Drawing.Point(51, 6)
        Me.cbxPerfil.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxPerfil.Name = "cbxPerfil"
        Me.cbxPerfil.SeleccionMultiple = False
        Me.cbxPerfil.SeleccionObligatoria = False
        Me.cbxPerfil.Size = New System.Drawing.Size(191, 21)
        Me.cbxPerfil.SoloLectura = False
        Me.cbxPerfil.TabIndex = 1
        Me.cbxPerfil.Texto = ""
        '
        'frmAccesos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(654, 592)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmAccesos"
        Me.Tag = "ACCESOS"
        Me.Text = "frmAccesos"
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.gbxOperacionesEspeciales.ResumeLayout(False)
        Me.ContextMenuStrip2.ResumeLayout(False)
        Me.gbxOperacionesBasicas.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents cbxPerfil As ERP.ocxCBX
    Friend WithEvents lblPlanCuenta As System.Windows.Forms.Label
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents NuevoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExpandirNodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExpandirTodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ContraerNodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContraerTodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImageList101 As System.Windows.Forms.ImageList
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents gbxOperacionesBasicas As System.Windows.Forms.GroupBox
    Friend WithEvents gbxOperacionesEspeciales As System.Windows.Forms.GroupBox
    Friend WithEvents lvOtrasOperaciones As System.Windows.Forms.ListView
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tvAccesos As System.Windows.Forms.TreeView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents chkImprimir As ERP.ocxCHK
    Friend WithEvents chkAnular As ERP.ocxCHK
    Friend WithEvents chkEliminar As ERP.ocxCHK
    Friend WithEvents chkModificar As ERP.ocxCHK
    Friend WithEvents chkAgregar As ERP.ocxCHK
    Friend WithEvents chkVisualizar As ERP.ocxCHK
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ImageList201 As System.Windows.Forms.ImageList
    Friend WithEvents ContextMenuStrip2 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents HabilitarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DenegarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
End Class

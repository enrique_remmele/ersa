﻿Public Class frmAccesos

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CData As New CData

    'PROPIEDADES
    Private BloquearValue As Boolean
    Public Property Bloquear() As Boolean
        Get
            Return BloquearValue
        End Get
        Set(ByVal value As Boolean)
            BloquearValue = value
        End Set
    End Property

    'EVENTOS

    'VARIABLES
    Dim dt As New DataTable
    Dim vTabla As String = "VMenu"
    Dim vIDPerfil As Integer = 0
    Dim vControles() As Control
    Dim vModificando As Boolean
    Dim vBloquearCheck As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Controles

        'Propiedades

        'Otros
        vModificando = False

        'Funciones
        CargarInformacion()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        'Foco

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'Cabecera
        CSistema.CargaControl(vControles, chkVisualizar)
        CSistema.CargaControl(vControles, chkAgregar)
        CSistema.CargaControl(vControles, chkModificar)
        CSistema.CargaControl(vControles, chkEliminar)
        CSistema.CargaControl(vControles, chkAnular)
        CSistema.CargaControl(vControles, chkImprimir)
        CSistema.CargaControl(vControles, tvAccesos)

        'Cargamos 
        'dt = CData.GetTable(vTabla).Copy
        If vgSAIN.Tables.Count = 0 Then
            dt = CData.GetTable(vTabla).Copy
        End If


        dt.Columns.Add("Habilitar")
        dt.Columns.Add("Visualizar")
        dt.Columns.Add("Agregar")
        dt.Columns.Add("Modificar")
        dt.Columns.Add("Eliminar")
        dt.Columns.Add("Anular")
        dt.Columns.Add("Imprimir")

        'Registros
        CData.CreateTable("VAccesoPerfil", "Select * From VAccesoPerfil")

        'Perfiles
        CSistema.SqlToComboBox(cbxPerfil.cbx, "Select ID, Descripcion From VPerfil order by 2")

    End Sub

    Sub GuardarInformacion()

    End Sub

    Sub SeleccionarRegistro()

        If vModificando = True Then
            SeleccionarRegistroModificando()
        End If

        If vModificando = False Then
            SeleccionarRegistroConsulta()
        End If

        ListarAccesosEspecificos()

    End Sub

    Sub SeleccionarRegistroConsulta()

        'EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)
        vBloquearCheck = True

        chkVisualizar.chk.Checked = False
        chkAgregar.chk.Checked = False
        chkModificar.chk.Checked = False
        chkEliminar.chk.Checked = False
        chkAnular.chk.Checked = False
        chkImprimir.chk.Checked = False

        If vModificando = True Then
            If tvAccesos.SelectedNode.Checked = False Then
                GoTo continuar
            End If
        End If

        For Each oRow As DataRow In CData.GetTable("VAccesoPerfil", "IDPerfil=" & cbxPerfil.GetValue & " And Codigo = " & tvAccesos.SelectedNode.Name).Rows

            'Aca me quede, hay que agregar estos campos en VMenu!!!
            chkVisualizar.Valor = CSistema.RetornarValorBoolean(oRow("Visualizar").ToString)
            chkAgregar.Valor = CSistema.RetornarValorBoolean(oRow("Agregar").ToString)
            chkModificar.Valor = CSistema.RetornarValorBoolean(oRow("Modificar").ToString)
            chkEliminar.Valor = CSistema.RetornarValorBoolean(oRow("Eliminar").ToString)
            chkAnular.Valor = CSistema.RetornarValorBoolean(oRow("Anular").ToString)
            chkImprimir.Valor = CSistema.RetornarValorBoolean(oRow("Imprimir").ToString)

        Next


continuar:

        chkVisualizar.SoloLectura = True
        chkAgregar.SoloLectura = True
        chkModificar.SoloLectura = True
        chkEliminar.SoloLectura = True
        chkAnular.SoloLectura = True
        chkImprimir.SoloLectura = True

        vBloquearCheck = False

    End Sub

    Sub SeleccionarRegistroModificando()

        'EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)
        vBloquearCheck = True

        chkVisualizar.chk.Checked = False
        chkAgregar.chk.Checked = False
        chkModificar.chk.Checked = False
        chkEliminar.chk.Checked = False
        chkAnular.chk.Checked = False
        chkImprimir.chk.Checked = False

        If vModificando = True Then
            If tvAccesos.SelectedNode.Checked = False Then
                GoTo continuar
            End If
        End If

        For Each oRow As DataRow In dt.Select(" Codigo = " & tvAccesos.SelectedNode.Name)

            'Aca me quede, hay que agregar estos campos en VMenu!!!
            chkVisualizar.Valor = CSistema.RetornarValorBoolean(oRow("Visualizar").ToString)
            chkAgregar.Valor = CSistema.RetornarValorBoolean(oRow("Agregar").ToString)
            chkModificar.Valor = CSistema.RetornarValorBoolean(oRow("Modificar").ToString)
            chkEliminar.Valor = CSistema.RetornarValorBoolean(oRow("Eliminar").ToString)
            chkAnular.Valor = CSistema.RetornarValorBoolean(oRow("Anular").ToString)
            chkImprimir.Valor = CSistema.RetornarValorBoolean(oRow("Imprimir").ToString)

        Next

        chkVisualizar.SoloLectura = False
        chkAgregar.SoloLectura = False
        chkModificar.SoloLectura = False
        chkEliminar.SoloLectura = False
        chkAnular.SoloLectura = False
        chkImprimir.SoloLectura = False

continuar:

        vBloquearCheck = False

    End Sub

    Sub ListarAccesosEspecificos()

        lvOtrasOperaciones.Items.Clear()

        'Listar los Accesos
        Dim dtAccesoEspecifico As DataTable = CData.GetTable("VAccesoEspecifico").Copy
        Dim dtAccesoEspecificoPerfil As DataTable = CData.GetTable("VAccesoEspecificoPerfil").Copy
        Dim dt As New DataTable

        dtAccesoEspecifico = CData.FiltrarDataTable(dtAccesoEspecifico, " IDMenu = " & tvAccesos.SelectedNode.Name).Copy
        dtAccesoEspecificoPerfil = CData.FiltrarDataTable(dtAccesoEspecificoPerfil, " IDMenu = " & tvAccesos.SelectedNode.Name & " And IDPerfil=" & cbxPerfil.GetValue).Copy

        dt.Columns.Add("ID")
        dt.Columns.Add("Acceso")
        dt.Columns.Add("Habilitado")

        For Each oRow As DataRow In dtAccesoEspecifico.Rows
            Dim NewRow As DataRow = dt.NewRow
            NewRow("ID") = oRow("ID").ToString
            NewRow("Acceso") = oRow("Descripcion").ToString
            NewRow("Habilitado") = "False"

            'Verificar si esta habilitado
            If dtAccesoEspecificoPerfil.Select(" IDAccesoEspecifico = " & oRow("ID").ToString).Count > 0 Then
                NewRow("Habilitado") = dtAccesoEspecificoPerfil.Select(" IDAccesoEspecifico = " & oRow("ID").ToString)(0)("HAbilitado")
            End If

            dt.Rows.Add(NewRow)

        Next

        For Each oRow As DataRow In dt.Rows
            Dim item As New ListViewItem(oRow("ID").ToString)
            item.SubItems.Add(oRow("Acceso").ToString)
            If oRow("Habilitado") = True Then
                item.ImageIndex = 0
            Else
                item.ImageIndex = 1
            End If

            lvOtrasOperaciones.Items.Add(item)

        Next

    End Sub

    Sub ActualizarPermisos()

        For Each oRow As DataRow In dt.Select(" Codigo = " & tvAccesos.SelectedNode.Name)
            oRow("Visualizar") = chkVisualizar.Valor
            oRow("Agregar") = chkAgregar.Valor
            oRow("Modificar") = chkModificar.Valor
            oRow("Eliminar") = chkEliminar.Valor
            oRow("Anular") = chkAnular.Valor
            oRow("Imprimir") = chkImprimir.Valor
        Next

    End Sub

    Sub ActualizarAccesosEspecificos(ByVal Habilitar As Boolean)

        Dim param(-1) As SqlClient.SqlParameter
        Dim ID As Integer

        ID = lvOtrasOperaciones.SelectedItems(0).Text

        CSistema.SetSQLParameter(param, "@IDPerfil", cbxPerfil.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDAccesoEspecifico", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMenu", tvAccesos.SelectedNode.Name, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Habilitar", habilitar, ParameterDirection.Input)

        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpAccesoEspecificoPerfil", False, False, MensajeRetorno, "", False) = True Then

            'Actualizar
            CData.Actualizar(" IDPerfil=" & cbxPerfil.GetValue & " And IDAccesoEspecifico=" & ID & " And IDMenu=" & tvAccesos.SelectedNode.Name & " ", "VAccesoEspecificoPerfil")

        End If

        If Habilitar = True Then
            lvOtrasOperaciones.SelectedItems(0).ImageIndex = 0
        Else
            lvOtrasOperaciones.SelectedItems(0).ImageIndex = 1
        End If

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, New Button, btnGuardar, btnCancelar, New Button, btnImprimir, New Button, New Button, vControles, btnModificar)

    End Sub

    Sub CargarOperacion()

        vBloquearCheck = True

        chkVisualizar.Valor = False
        chkAgregar.Valor = False
        chkModificar.Valor = False
        chkEliminar.Valor = False
        chkAnular.Valor = False
        chkImprimir.Valor = False

        vBloquearCheck = False

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        EstablecerPermisos()
        ListarAccesos(True)

    End Sub

    Sub EstablecerPermisos()

        'Recorremos el menu
        For Each MenuRow As DataRow In dt.Rows

            'Verificamos si el menu esta habilitado para este perfil
            If CData.GetTable("VAccesoPerfil", " IDPerfil=" & cbxPerfil.GetValue & " And Codigo=" & MenuRow("Codigo") & "").Rows.Count > 0 Then
                MenuRow("Habilitar") = True
            Else
                MenuRow("Habilitar") = False
            End If
        Next

    End Sub

    Sub ListarAccesos(ByVal ListarTodo As Boolean, Optional ByVal nodo As TreeNode = Nothing)

        Dim Filtro As String = ""

        If nodo Is Nothing Then

            'Ordenar
            CData.OrderDataTable(dt, "Codigo, Nivel, CodigoPadre")

            'Limpiamos todo
            tvAccesos.Nodes.Clear()
            Filtro = "Nivel = 1"

            If ListarTodo = False Then
                Filtro = Filtro & " And Habilitar = 'True' "
            End If

            For Each orow As DataRow In dt.Select(Filtro)
                Dim nodo2 As New TreeNode
                nodo2.Name = orow("Codigo")
                nodo2.Text = orow("Descripcion")

                If ListarTodo = True Then
                    nodo2.Checked = orow("Habilitar")

                    If orow("Habilitar") = True Then
                        nodo2.ForeColor = Color.Black
                    Else
                        nodo2.ForeColor = Color.LightGray
                    End If

                End If

                tvAccesos.Nodes.Add(nodo2)

                ListarAccesos(ListarTodo, nodo2)

            Next

            Exit Sub

        End If

        Filtro = "CodigoPadre = '" & nodo.Name & "' "

        If ListarTodo = False Then
            Filtro = Filtro & " And Habilitar = 'True' "
        End If

        If dt.Select(Filtro).Count > 0 Then

            nodo.ImageIndex = 0
            nodo.SelectedImageIndex = 1

            For Each orow As DataRow In dt.Select(Filtro)

                Dim nodo2 As New TreeNode
                nodo2.Name = orow("Codigo")
                nodo2.Text = orow("Descripcion")

                If ListarTodo = True Then
                    nodo2.Checked = orow("Habilitar")

                    If orow("Habilitar") = True Then
                        nodo2.ForeColor = Color.Black
                    Else
                        nodo2.ForeColor = Color.LightGray
                    End If

                End If

                nodo.Nodes.Add(nodo2)

                ListarAccesos(ListarTodo, nodo2)

            Next

        Else
            nodo.ImageIndex = 2
            nodo.SelectedImageIndex = 2
        End If

    End Sub
    Sub Imprimir()

        Dim CMovimiento As New Reporte.CReporteAccesoPerfil
        CMovimiento.AccesoPerfil(" where IDPerfil = " & cbxPerfil.GetValue)

    End Sub

    Sub Modificar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.MODIFICAR)
        cbxPerfil.SoloLectura = True

        gbxOperacionesBasicas.Enabled = True

        tvAccesos.CheckBoxes = True
        ListarAccesos(True)

        'Actualizar permisos
        For Each MenuRow As DataRow In CData.GetTable("VAccesoPerfil", "IDPerfil=" & cbxPerfil.GetValue).Rows
            For Each oRow As DataRow In dt.Select(" Codigo = " & MenuRow("Codigo"))

                'Aca me quede, hay que agregar estos campos en VMenu!!!
                oRow("Visualizar") = CSistema.RetornarValorBoolean(MenuRow("Visualizar").ToString)
                oRow("Agregar") = CSistema.RetornarValorBoolean(MenuRow("Agregar").ToString)
                oRow("Modificar") = CSistema.RetornarValorBoolean(MenuRow("Modificar").ToString)
                oRow("Eliminar") = CSistema.RetornarValorBoolean(MenuRow("Eliminar").ToString)
                oRow("Anular") = CSistema.RetornarValorBoolean(MenuRow("Anular").ToString)
                oRow("Imprimir") = CSistema.RetornarValorBoolean(MenuRow("Imprimir").ToString)

            Next
        Next

        'Limpiar accesos especiales
        lvOtrasOperaciones.Items.Clear()

        vModificando = True


    End Sub

    Sub Cancelar()

        vModificando = False
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)
        cbxPerfil.SoloLectura = False

        ListarAccesos(False)

        'Limpiar accesos especiales
        lvOtrasOperaciones.Items.Clear()

        gbxOperacionesBasicas.Enabled = True

        tvAccesos.CheckBoxes = False

    End Sub

    Sub Guardar()


        'Validar
        Dim Sql As String = ""

        'Accesos
        SQLString(Sql)

        If CSistema.ExecuteNonQuery(Sql) = 0 Then
            Exit Sub
        End If

        'Actualizar accesos en memoria
        CData.Actualizar(" IDPerfil = " & cbxPerfil.GetValue, "VAccesoPerfil")
        vModificando = False

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        cbxPerfil.SoloLectura = False
        gbxOperacionesBasicas.Enabled = True

        dt = CData.GetTable(vTabla).Copy
        dt.Columns.Add("Habilitar")
        dt.Columns.Add("Visualizar")
        dt.Columns.Add("Agregar")
        dt.Columns.Add("Modificar")
        dt.Columns.Add("Eliminar")
        dt.Columns.Add("Anular")
        dt.Columns.Add("Imprimir")

        EstablecerPermisos()
        ListarAccesos(False)
        tvAccesos.ExpandAll()

    End Sub

    Sub Clonar()

        'Validar
        If cbxPerfil.Validar("Seleccione correctamente un perfil para continuar!", New ErrorProvider, cbxPerfil, New ToolStripStatusLabel) = False Then
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Esta seguro/a de clonar los permidos de " & cbxPerfil.cbx.Text.ToUpper & " a otro perfil?", "Clonar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim frm As New frmClonarAcceso
        frm.IDPerfil = cbxPerfil.GetValue
        frm.Text = "Clonar " & cbxPerfil.cbx.Text & " a:"
        frm.ShowDialog(Me)

        If frm.Seleccionado = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDPerfil", frm.IDPerfil, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDPerfilClonar", frm.IDPerfilClonar, ParameterDirection.Input)

        'Auditoria
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpClonarAccesoPerfil", False, False, MensajeRetorno) = False Then
            CSistema.MostrarError("Error: " & MensajeRetorno, ctrError, LinkLabel1, tsslEstado, )
            Exit Sub
        End If


        'Actualizar los datos
        CData.Actualizar(" IDPerfil = " & frm.IDPerfilClonar, "VAccesoPerfil")


    End Sub

    Sub SQLString(ByRef SQL As String, Optional ByVal nodo1 As TreeNode = Nothing)

        Dim Sentencia As String = ""

        For Each oRow As DataRow In dt.Rows

            Sentencia = "Exec SpAccesoPerfil "

            CSistema.ConcatenarParametro(Sentencia, "@IDPerfil", cbxPerfil.GetValue)
            CSistema.ConcatenarParametro(Sentencia, "@IDMenu", oRow("Codigo"))
            CSistema.ConcatenarParametro(Sentencia, "@NombreControl", oRow("NombreControl"))
            CSistema.ConcatenarParametro(Sentencia, "@Habilitar", oRow("Habilitar"))

            'Accesos
            CSistema.ConcatenarParametro(Sentencia, "@Visualizar", CSistema.RetornarValorBoolean(oRow("Visualizar").ToString))
            CSistema.ConcatenarParametro(Sentencia, "@Agregar", CSistema.RetornarValorBoolean(oRow("Agregar").ToString))
            CSistema.ConcatenarParametro(Sentencia, "@Modificar", CSistema.RetornarValorBoolean(oRow("Modificar").ToString))
            CSistema.ConcatenarParametro(Sentencia, "@Eliminar", CSistema.RetornarValorBoolean(oRow("Eliminar").ToString))
            CSistema.ConcatenarParametro(Sentencia, "@Anular", CSistema.RetornarValorBoolean(oRow("Anular").ToString))
            CSistema.ConcatenarParametro(Sentencia, "@Imprimir", CSistema.RetornarValorBoolean(oRow("Imprimir").ToString))

            'Identificadores
            CSistema.ConcatenarParametro(Sentencia, "@IDUsuario", vgIDUsuario)
            CSistema.ConcatenarParametro(Sentencia, "@IDSucursal", vgIDSucursal)
            CSistema.ConcatenarParametro(Sentencia, "@IDDeposito", vgIDDeposito)
            CSistema.ConcatenarParametro(Sentencia, "@IDTerminal", vgIDTerminal)

            SQL = SQL & Sentencia & vbCrLf

        Next


    End Sub

    Sub CheckMenu(ByVal nodo As TreeNode)

        SeleccionarHaciaArriba(nodo, nodo.Checked)
        QuitarSeleccionHaciaAbajo(nodo)

        'Haiblitar o Deshabilitar gbx 
        If vModificando = True Then
            gbxOperacionesBasicas.Enabled = nodo.Checked
            tvAccesos.SelectedNode = nodo
            For Each oRow As DataRow In dt.Select("Codigo = " & nodo.Name)
                oRow("Habilitar") = nodo.Checked
            Next

        End If

        'Accesos predeterminados
        If vModificando = True Then
            If nodo.Checked = True Then
                For Each oRow As DataRow In CData.GetTable("VPerfil", " ID = " & cbxPerfil.GetValue).Copy.Rows
                    chkVisualizar.Valor = oRow("Visualizar")
                    chkAgregar.Valor = oRow("Agregar")
                    chkModificar.Valor = oRow("Modificar")
                    chkEliminar.Valor = oRow("Eliminar")
                    chkAnular.Valor = oRow("Anular")
                    chkImprimir.Valor = oRow("Imprimir")
                Next
            End If
        End If
    End Sub

    Private Sub SeleccionarHaciaArriba(ByVal Nodo1 As TreeNode, ByVal Seleccionar As Boolean)

        If Bloquear = True Then
            Exit Sub
        End If

        'Si el nivel es 0, simpelemte establecemos la seleccion y salimos del sistema
        If Nodo1.Level = 0 Then
            If Nodo1.Checked <> Seleccionar Then
                Nodo1.Checked = Seleccionar
            End If

            If Seleccionar = True Then
                Nodo1.ForeColor = Color.Black
            Else
                Nodo1.ForeColor = Color.DarkGray
            End If

            Exit Sub
        End If


        'Si seleccion es True
        If Seleccionar = True Then

            Nodo1.ForeColor = Color.Black

            'Establecemos True al padre
            Nodo1.Parent.Checked = Seleccionar

            'Vamos a un nivel mas arriba
            'SeleccionarHaciaArriba(Nodo1.Parent, Seleccionar)

        Else

            Nodo1.ForeColor = Color.DarkGray

            'Si es false, verificar si hay nodos del nivel marcados
            For Each nodo As TreeNode In Nodo1.Parent.Nodes

                'Si se trata del mismo nodo que genero, vamos a siguiente.
                If nodo.Name = Nodo1.Name And nodo.Level = Nodo1.Level Then
                    GoTo siguiente
                End If

                'Si hay un nodo marcado True no hacer nada
                If nodo.Checked = True Then
                    Exit Sub
                End If
siguiente:

            Next

            'Vamos a un nivel mas arriba
            SeleccionarHaciaArriba(Nodo1.Parent, Seleccionar)

        End If


    End Sub

    Private Sub SeleccionarHaciaAbajo(Optional ByVal Nodo1 As TreeNode = Nothing)

        If Bloquear = True Then
            Exit Sub
        End If

        If Nodo1 Is Nothing Then
            Nodo1 = tvAccesos.SelectedNode
        End If

        Nodo1.Checked = True

        For Each nodo2 As TreeNode In Nodo1.Nodes
            nodo2.Checked = True

            If nodo2.Nodes.Count > 0 Then
                SeleccionarHaciaAbajo(nodo2)
            End If
        Next


    End Sub

    Sub QuitarSeleccionHaciaAbajo(ByVal nodo As TreeNode)

        If tvAccesos.CheckBoxes = False Then
            Exit Sub
        End If

        If nodo.Checked = True Then
            Exit Sub
        End If

        'RaiseEvent CheckedItem()

        Bloquear = True

        nodo.ForeColor = Color.DarkGray

        For Each nodo1 As TreeNode In nodo.Nodes
            nodo1.Checked = False
            If nodo1.Nodes.Count > 0 Then
                QuitarSeleccionHaciaAbajo(nodo1)
            End If
        Next

        Bloquear = False

    End Sub

    Private Sub frmCuentaContable_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmCuentaContable_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub ExpandirNodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExpandirNodoToolStripMenuItem.Click

        Try
            tvAccesos.SelectedNode.Expand()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ExpandirTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExpandirTodoToolStripMenuItem.Click
        tvAccesos.ExpandAll()
    End Sub

    Private Sub ContraerNodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContraerNodoToolStripMenuItem.Click
        tvAccesos.SelectedNode.Collapse()
    End Sub

    Private Sub ContraerTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContraerTodoToolStripMenuItem.Click
        tvAccesos.CollapseAll()
    End Sub

    Private Sub tvCuentas_AfterCollapse(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvAccesos.AfterCollapse

        Try
            If e.Node.IsExpanded = False Then
                e.Node.SelectedImageIndex = 0
                e.Node.ImageIndex = 0
            Else
                e.Node.SelectedImageIndex = 1
                e.Node.ImageIndex = 1
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub tvCuentas_AfterExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvAccesos.AfterExpand
        Try
            If e.Node.IsExpanded = False Then
                e.Node.SelectedImageIndex = 0
                e.Node.ImageIndex = 0
            Else
                e.Node.SelectedImageIndex = 1
                e.Node.ImageIndex = 1
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub tvCuentas_BeforeExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles tvAccesos.BeforeExpand
        ' le asigno la imagen con la carpeta abierta
        'tvAccesos.SelectedImageIndex = 1
    End Sub

    Private Sub cbxPerfil_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxPerfil.PropertyChanged
        CargarOperacion()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub tv_AfterCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvAccesos.AfterCheck
        CheckMenu(e.Node)
    End Sub

    Private Sub tv_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tvAccesos.NodeMouseClick

        If e.Button = Windows.Forms.MouseButtons.Right Then

            ' Referenciamos el control
            Dim tv As Windows.Forms.TreeView = DirectCast(sender, Windows.Forms.TreeView)

            ' Seleccionamos el nodo
            tv.SelectedNode = e.Node
        
        End If

    End Sub

    Private Sub tvAccesos_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvAccesos.AfterSelect

        SeleccionarRegistro()

        'Haiblitar o Deshabilitar gbx
        If vModificando = True Then
            gbxOperacionesBasicas.Enabled = e.Node.Checked
        End If

    End Sub

    Private Sub tvAccesos_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvAccesos.GotFocus

    End Sub

    Private Sub tvAccesos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvAccesos.Click

    End Sub

    Private Sub chkBasicos_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkVisualizar.PropertyChanged, chkAgregar.PropertyChanged, chkModificar.PropertyChanged, chkEliminar.PropertyChanged, chkAnular.PropertyChanged, chkImprimir.PropertyChanged

        If vBloquearCheck = True Then
            Exit Sub
        End If

        vBloquearCheck = True
        ActualizarPermisos()
        vBloquearCheck = False

    End Sub

    Private Sub HabilitarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HabilitarToolStripMenuItem.Click
        ActualizarAccesosEspecificos(True)
    End Sub

    Private Sub DenegarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DenegarToolStripMenuItem.Click
        ActualizarAccesosEspecificos(False)
    End Sub

    Private Sub ContextMenuStrip2_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ContextMenuStrip2.Opening
        ContextMenuStrip2.Enabled = True

        If vModificando = False Then
            ContextMenuStrip2.Enabled = False
        End If

    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Clonar()
    End Sub

    Private Sub btnImprimir_Click(sender As System.Object, e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub FrmAccesos_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class FrmAccesosSegunFormulario

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CData As New CData

    'EVENTOS

    'VARIABLES
    Dim dt As New DataTable
    Dim vTabla As String = "VMenu"
    Dim vTag As String

    'FUNCIONES
    Sub Inicializar()
        CargarInformacion()
        CargarOperacion()
        'Controles

        'Propiedades

    End Sub

    Sub CargarInformacion()

        'Cargamos 
        'dt = CData.GetTable(vTabla).Copy
        If vgSAIN.Tables.Count = 0 Then
            dt = CData.GetTable(vTabla).Copy
        End If


        dt.Columns.Add("Habilitar")
        dt.Columns.Add("Visualizar")
        dt.Columns.Add("Agregar")
        dt.Columns.Add("Modificar")
        dt.Columns.Add("Eliminar")
        dt.Columns.Add("Anular")
        dt.Columns.Add("Imprimir")

        'Registros
        CData.CreateTable("VAccesoPerfil", "Select * From VAccesoPerfil")

    End Sub

    Sub GuardarInformacion()

    End Sub

    Sub SeleccionarRegistro()

        SeleccionarRegistroConsulta()


        ListarAccesosEspecificos()

    End Sub

    Sub SeleccionarRegistroConsulta()

        'EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        chkVisualizar.chk.Checked = False
        chkAgregar.chk.Checked = False
        chkModificar.chk.Checked = False
        chkEliminar.chk.Checked = False
        chkAnular.chk.Checked = False
        chkImprimir.chk.Checked = False

        vTag = dt.Select("codigo = " & tvAccesosSegunFormulario.SelectedNode.Name)(0)("Tag")


        ListarUsuario()

    End Sub


    Sub ListarUsuario()

        Dim dtusuario As DataTable

        dtusuario = CSistema.ExecuteToDataTable("Select U.ID, U.Nombre, U.Perfil From vUsuario U Join VAccesoPerfil AP on AP.IDPerfil = U.IDPerfil where AP.TAG='" & vTag & "'")
        CSistema.dtToGrid(dgv, dtusuario)

    End Sub

    Sub ListarAccesosEspecificos()

        lvOtrasOperaciones.Items.Clear()

        'Listar los Accesos
        Dim dtAccesoEspecifico As DataTable = CData.GetTable("VAccesoEspecifico").Copy
        Dim dtAccesoEspecificoPerfil As DataTable = CData.GetTable("VAccesoEspecificoPerfil").Copy
        Dim dt As New DataTable

        'dtAccesoEspecifico = CData.FiltrarDataTable(dtAccesoEspecifico, " TAG = " & tvAccesosSegunFormulario.SelectedNode.Name).Copy
        'dtAccesoEspecificoPerfil = CData.FiltrarDataTable(dtAccesoEspecificoPerfil, " TAG = " & tvAccesosSegunFormulario.SelectedNode.Name & " And IDPerfil=" & dgv.SelectedRows(0).Cells("IDPerfil").Value).Copy

        dt.Columns.Add("ID")
        dt.Columns.Add("Nombre")
        dt.Columns.Add("Perfil")

        For Each oRow As DataRow In dtAccesoEspecifico.Rows
            Dim NewRow As DataRow = dt.NewRow
            NewRow("ID") = oRow("ID").ToString
            NewRow("Acceso") = oRow("Descripcion").ToString
            NewRow("Habilitado") = "False"

            'Verificar si esta habilitado
            If dtAccesoEspecificoPerfil.Select(" IDAccesoEspecifico = " & oRow("ID").ToString).Count > 0 Then
                NewRow("Habilitado") = dtAccesoEspecificoPerfil.Select(" IDAccesoEspecifico = " & oRow("ID").ToString)(0)("HAbilitado")
            End If

            dt.Rows.Add(NewRow)

        Next

        For Each oRow As DataRow In dt.Rows
            Dim item As New ListViewItem(oRow("ID").ToString)
            item.SubItems.Add(oRow("Acceso").ToString)
            If oRow("Habilitado") = True Then
                item.ImageIndex = 0
            Else
                item.ImageIndex = 1
            End If

            lvOtrasOperaciones.Items.Add(item)

        Next

        dgv.Columns("Nombres").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub ActualizarPermisos()

        For Each oRow As DataRow In dt.Select(" Codigo = " & tvAccesosSegunFormulario.SelectedNode.Name)
            oRow("Visualizar") = chkVisualizar.Valor
            oRow("Agregar") = chkAgregar.Valor
            oRow("Modificar") = chkModificar.Valor
            oRow("Eliminar") = chkEliminar.Valor
            oRow("Anular") = chkAnular.Valor
            oRow("Imprimir") = chkImprimir.Valor
        Next

    End Sub

    'Sub ActualizarAccesosEspecificos(ByVal Habilitar As Boolean)

    '    Dim param(-1) As SqlClient.SqlParameter
    '    Dim ID As Integer

    '    ID = lvOtrasOperaciones.SelectedItems(0).Text

    '    CSistema.SetSQLParameter(param, "@IDPerfil", cbxPerfil.GetValue, ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@IDAccesoEspecifico", ID, ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@IDMenu", tvAccesosSegunFormulario.SelectedNode.Name, ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@Habilitar", Habilitar, ParameterDirection.Input)

    '    Dim MensajeRetorno As String = ""

    '    'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
    '    If CSistema.ExecuteStoreProcedure(param, "SpAccesoEspecificoPerfil", False, False, MensajeRetorno, "", False) = True Then

    '        'Actualizar
    '        CData.Actualizar(" IDPerfil=" & cbxPerfil.GetValue & " And IDAccesoEspecifico=" & ID & " And IDMenu=" & tvAccesosSegunFormulario.SelectedNode.Name & " ", "VAccesoEspecificoPerfil")

    '    End If

    '    If Habilitar = True Then
    '        lvOtrasOperaciones.SelectedItems(0).ImageIndex = 0
    '    Else
    '        lvOtrasOperaciones.SelectedItems(0).ImageIndex = 1
    '    End If

    'End Sub

    Sub CargarOperacion()

        chkVisualizar.Valor = False
        chkAgregar.Valor = False
        chkModificar.Valor = False
        chkEliminar.Valor = False
        chkAnular.Valor = False
        chkImprimir.Valor = False

        EstablecerPermisos()
        ListarAccesos(True)

    End Sub

    Sub EstablecerPermisos()

        'Recorremos el menu
        For Each MenuRow As DataRow In dt.Rows
            'Verificamos si el menu esta habilitado para este perfil
            MenuRow("Habilitar") = True
        Next

    End Sub

    Sub ListarAccesos(ByVal ListarTodo As Boolean, Optional ByVal nodo As TreeNode = Nothing)

        Dim Filtro As String = ""

        If nodo Is Nothing Then

            'Ordenar
            CData.OrderDataTable(dt, "Codigo, Nivel, CodigoPadre")

            'Limpiamos todo
            tvAccesosSegunFormulario.Nodes.Clear()
            Filtro = "Nivel = 1"

            If ListarTodo = False Then
                Filtro = Filtro & " And Habilitar = 'True' "
            End If

            For Each orow As DataRow In dt.Select(Filtro)
                Dim nodo2 As New TreeNode
                nodo2.Name = orow("Codigo")
                nodo2.Text = orow("Descripcion")

                If ListarTodo = True Then
                    nodo2.Checked = orow("Habilitar")

                    If orow("Habilitar") = True Then
                        nodo2.ForeColor = Color.Black
                    Else
                        nodo2.ForeColor = Color.LightGray
                    End If

                End If

                tvAccesosSegunFormulario.Nodes.Add(nodo2)

                ListarAccesos(ListarTodo, nodo2)

            Next

            Exit Sub

        End If

        Filtro = "CodigoPadre = '" & nodo.Name & "' "

        If ListarTodo = False Then
            Filtro = Filtro & " And Habilitar = 'True' "
        End If

        If dt.Select(Filtro).Count > 0 Then

            nodo.ImageIndex = 0
            nodo.SelectedImageIndex = 1

            For Each orow As DataRow In dt.Select(Filtro)

                Dim nodo2 As New TreeNode
                nodo2.Name = orow("Codigo")
                nodo2.Text = orow("Descripcion")

                If ListarTodo = True Then
                    nodo2.Checked = orow("Habilitar")

                    If orow("Habilitar") = True Then
                        nodo2.ForeColor = Color.Black
                    Else
                        nodo2.ForeColor = Color.LightGray
                    End If

                End If

                nodo.Nodes.Add(nodo2)

                ListarAccesos(ListarTodo, nodo2)

            Next

        Else
            nodo.ImageIndex = 2
            nodo.SelectedImageIndex = 2
        End If

    End Sub



    'Sub SQLString(ByRef SQL As String, Optional ByVal nodo1 As TreeNode = Nothing)

    '    Dim Sentencia As String = ""

    '    For Each oRow As DataRow In dt.Rows

    '        Sentencia = "Exec SpAccesoPerfil "

    '        CSistema.ConcatenarParametro(Sentencia, "@IDPerfil", cbxPerfil.GetValue)
    '        CSistema.ConcatenarParametro(Sentencia, "@IDMenu", oRow("Codigo"))
    '        CSistema.ConcatenarParametro(Sentencia, "@NombreControl", oRow("NombreControl"))
    '        CSistema.ConcatenarParametro(Sentencia, "@Habilitar", oRow("Habilitar"))

    '        'Accesos
    '        CSistema.ConcatenarParametro(Sentencia, "@Visualizar", CSistema.RetornarValorBoolean(oRow("Visualizar").ToString))
    '        CSistema.ConcatenarParametro(Sentencia, "@Agregar", CSistema.RetornarValorBoolean(oRow("Agregar").ToString))
    '        CSistema.ConcatenarParametro(Sentencia, "@Modificar", CSistema.RetornarValorBoolean(oRow("Modificar").ToString))
    '        CSistema.ConcatenarParametro(Sentencia, "@Eliminar", CSistema.RetornarValorBoolean(oRow("Eliminar").ToString))
    '        CSistema.ConcatenarParametro(Sentencia, "@Anular", CSistema.RetornarValorBoolean(oRow("Anular").ToString))
    '        CSistema.ConcatenarParametro(Sentencia, "@Imprimir", CSistema.RetornarValorBoolean(oRow("Imprimir").ToString))

    '        'Identificadores
    '        CSistema.ConcatenarParametro(Sentencia, "@IDUsuario", vgIDUsuario)
    '        CSistema.ConcatenarParametro(Sentencia, "@IDSucursal", vgIDSucursal)
    '        CSistema.ConcatenarParametro(Sentencia, "@IDDeposito", vgIDDeposito)
    '        CSistema.ConcatenarParametro(Sentencia, "@IDTerminal", vgIDTerminal)

    '        SQL = SQL & Sentencia & vbCrLf

    '    Next


    'End Sub

    'Sub CheckMenu(ByVal nodo As TreeNode)

    '    SeleccionarHaciaArriba(nodo, nodo.Checked)
    '    QuitarSeleccionHaciaAbajo(nodo)

    '    'Haiblitar o Deshabilitar gbx 
    '    If vModificando = True Then
    '        gbxOperacionesBasicas.Enabled = nodo.Checked
    '        tvAccesosSegunFormulario.SelectedNode = nodo
    '        For Each oRow As DataRow In dt.Select("Codigo = " & nodo.Name)
    '            oRow("Habilitar") = nodo.Checked
    '        Next

    '    End If

    '    'Accesos predeterminados
    '    If vModificando = True Then
    '        If nodo.Checked = True Then
    '            For Each oRow As DataRow In CData.GetTable("VPerfil", " ID = " & cbxPerfil.GetValue).Copy.Rows
    '                chkVisualizar.Valor = oRow("Visualizar")
    '                chkAgregar.Valor = oRow("Agregar")
    '                chkModificar.Valor = oRow("Modificar")
    '                chkEliminar.Valor = oRow("Eliminar")
    '                chkAnular.Valor = oRow("Anular")
    '                chkImprimir.Valor = oRow("Imprimir")
    '            Next
    '        End If
    '    End If
    'End Sub

    Private Sub SeleccionarHaciaArriba(ByVal Nodo1 As TreeNode, ByVal Seleccionar As Boolean)

        'Si el nivel es 0, simpelemte establecemos la seleccion y salimos del sistema
        If Nodo1.Level = 0 Then
            If Nodo1.Checked <> Seleccionar Then
                Nodo1.Checked = Seleccionar
            End If

            If Seleccionar = True Then
                Nodo1.ForeColor = Color.Black
            Else
                Nodo1.ForeColor = Color.DarkGray
            End If

            Exit Sub
        End If


        'Si seleccion es True
        If Seleccionar = True Then

            Nodo1.ForeColor = Color.Black

            'Establecemos True al padre
            Nodo1.Parent.Checked = Seleccionar

            'Vamos a un nivel mas arriba
            'SeleccionarHaciaArriba(Nodo1.Parent, Seleccionar)

        Else

            Nodo1.ForeColor = Color.DarkGray

            'Si es false, verificar si hay nodos del nivel marcados
            For Each nodo As TreeNode In Nodo1.Parent.Nodes

                'Si se trata del mismo nodo que genero, vamos a siguiente.
                If nodo.Name = Nodo1.Name And nodo.Level = Nodo1.Level Then
                    GoTo siguiente
                End If

                'Si hay un nodo marcado True no hacer nada
                If nodo.Checked = True Then
                    Exit Sub
                End If
siguiente:

            Next

            'Vamos a un nivel mas arriba
            SeleccionarHaciaArriba(Nodo1.Parent, Seleccionar)

        End If


    End Sub

    Private Sub SeleccionarHaciaAbajo(Optional ByVal Nodo1 As TreeNode = Nothing)

        If Nodo1 Is Nothing Then
            Nodo1 = tvAccesosSegunFormulario.SelectedNode
        End If

        Nodo1.Checked = True

        For Each nodo2 As TreeNode In Nodo1.Nodes
            nodo2.Checked = True

            If nodo2.Nodes.Count > 0 Then
                SeleccionarHaciaAbajo(nodo2)
            End If
        Next


    End Sub

    Sub QuitarSeleccionHaciaAbajo(ByVal nodo As TreeNode)

        If tvAccesosSegunFormulario.CheckBoxes = False Then
            Exit Sub
        End If

        If nodo.Checked = True Then
            Exit Sub
        End If

        'RaiseEvent CheckedItem()

        nodo.ForeColor = Color.DarkGray

        For Each nodo1 As TreeNode In nodo.Nodes
            nodo1.Checked = False
            If nodo1.Nodes.Count > 0 Then
                QuitarSeleccionHaciaAbajo(nodo1)
            End If
        Next

    End Sub

    Private Sub frmCuentaContable_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmCuentaContable_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub ExpandirNodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExpandirNodoToolStripMenuItem.Click

        Try
            tvAccesosSegunFormulario.SelectedNode.Expand()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ExpandirTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExpandirTodoToolStripMenuItem.Click
        tvAccesosSegunFormulario.ExpandAll()
    End Sub

    Private Sub ContraerNodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContraerNodoToolStripMenuItem.Click
        tvAccesosSegunFormulario.SelectedNode.Collapse()
    End Sub

    Private Sub ContraerTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContraerTodoToolStripMenuItem.Click
        tvAccesosSegunFormulario.CollapseAll()
    End Sub

    Private Sub tvCuentas_AfterCollapse(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvAccesosSegunFormulario.AfterCollapse

        Try
            If e.Node.IsExpanded = False Then
                e.Node.SelectedImageIndex = 0
                e.Node.ImageIndex = 0
            Else
                e.Node.SelectedImageIndex = 1
                e.Node.ImageIndex = 1
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub tvCuentas_AfterExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvAccesosSegunFormulario.AfterExpand
        Try
            If e.Node.IsExpanded = False Then
                e.Node.SelectedImageIndex = 0
                e.Node.ImageIndex = 0
            Else
                e.Node.SelectedImageIndex = 1
                e.Node.ImageIndex = 1
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub tvCuentas_BeforeExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles tvAccesosSegunFormulario.BeforeExpand
        ' le asigno la imagen con la carpeta abierta
        'tvAccesos.SelectedImageIndex = 1
    End Sub

    'Private Sub cbxPerfil_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxPerfil.PropertyChanged
    '    CargarOperacion()
    'End Sub

    Private Sub tv_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tvAccesosSegunFormulario.NodeMouseClick

        If e.Button = Windows.Forms.MouseButtons.Right Then

            ' Referenciamos el control
            Dim tv As Windows.Forms.TreeView = DirectCast(sender, Windows.Forms.TreeView)

            ' Seleccionamos el nodo
            tv.SelectedNode = e.Node

        End If

    End Sub

    Private Sub tvAccesos_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvAccesosSegunFormulario.AfterSelect

        SeleccionarRegistro()

        'Haiblitar o Deshabilitar gbx
        'If vModificando = True Then
        '    gbxOperacionesBasicas.Enabled = e.Node.Checked
        'End If


    End Sub

    Private Sub tvAccesos_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvAccesosSegunFormulario.GotFocus

    End Sub

    Private Sub tvAccesos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvAccesosSegunFormulario.Click

    End Sub

    Private Sub chkBasicos_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkVisualizar.PropertyChanged, chkAgregar.PropertyChanged, chkModificar.PropertyChanged, chkEliminar.PropertyChanged, chkAnular.PropertyChanged, chkImprimir.PropertyChanged

        'If vBloquearCheck = True Then
        '    Exit Sub
        'End If

        'vBloquearCheck = True
        'ActualizarPermisos()
        'vBloquearCheck = False

    End Sub

    Private Sub ContextMenuStrip2_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ContextMenuStrip2.Opening
        ContextMenuStrip2.Enabled = True

        'If vModificando = False Then
        '    ContextMenuStrip2.Enabled = False
        'End If

    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub FrmAccesosSegunFormulario_Activate()
        Me.Refresh()
    End Sub
End Class

﻿Public Class frmClonarAcceso

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private IDPerfilValue As Integer
    Public Property IDPerfil() As Integer
        Get
            Return IDPerfilValue
        End Get
        Set(ByVal value As Integer)
            IDPerfilValue = value
        End Set
    End Property

    Private IDPerfilClonarValue As Integer
    Public Property IDPerfilClonar() As Integer
        Get
            Return IDPerfilClonarValue
        End Get
        Set(ByVal value As Integer)
            IDPerfilClonarValue = value
        End Set
    End Property

    Private SeleccionadoValue As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionadoValue = value
        End Set
    End Property

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        'Cargar perfiles que no sean el seleccionado
        Dim dt As DataTable = CData.GetTable("VPerfil", " ID <> " & IDPerfil).Copy
        CSistema.SqlToListBox(lbxPerfiles, dt, "ID", "Descripcion")


    End Sub

    Sub Seleccionar()

        If IsNumeric(lbxPerfiles.SelectedValue) = False Then
            CSistema.MostrarError("Seleccione correctamente un perfil", New ErrorProvider, btnAceptar, New ToolStripStatusLabel)
            Exit Sub
        End If

        IDPerfilClonar = lbxPerfiles.SelectedValue
        Seleccionado = True

        Me.Close()

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Seleccionado = False
        Me.Close()
    End Sub

    Private Sub frmClonarAcceso_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmClonarAccesos_Activate()
        Me.Refresh()
    End Sub

End Class
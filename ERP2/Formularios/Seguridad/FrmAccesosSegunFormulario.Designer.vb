﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAccesosSegunFormulario
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmAccesosSegunFormulario))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.tvAccesosSegunFormulario = New System.Windows.Forms.TreeView()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.NuevoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExpandirNodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExpandirTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ContraerNodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContraerTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImageList101 = New System.Windows.Forms.ImageList(Me.components)
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.gbxOperacionesEspeciales = New System.Windows.Forms.GroupBox()
        Me.lvOtrasOperaciones = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ContextMenuStrip2 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.HabilitarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DenegarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImageList201 = New System.Windows.Forms.ImageList(Me.components)
        Me.gbxOperacionesBasicas = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.chkImprimir = New ERP.ocxCHK()
        Me.chkAnular = New ERP.ocxCHK()
        Me.chkEliminar = New ERP.ocxCHK()
        Me.chkModificar = New ERP.ocxCHK()
        Me.chkAgregar = New ERP.ocxCHK()
        Me.chkVisualizar = New ERP.ocxCHK()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.gbxOperacionesEspeciales.SuspendLayout()
        Me.ContextMenuStrip2.SuspendLayout()
        Me.gbxOperacionesBasicas.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.SplitContainer1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(883, 441)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 42)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.tvAccesosSegunFormulario)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.dgv)
        Me.SplitContainer1.Panel2.Controls.Add(Me.StatusStrip1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.gbxOperacionesEspeciales)
        Me.SplitContainer1.Panel2.Controls.Add(Me.gbxOperacionesBasicas)
        Me.SplitContainer1.Size = New System.Drawing.Size(877, 396)
        Me.SplitContainer1.SplitterDistance = 249
        Me.SplitContainer1.TabIndex = 1
        '
        'tvAccesosSegunFormulario
        '
        Me.tvAccesosSegunFormulario.AllowDrop = True
        Me.tvAccesosSegunFormulario.CheckBoxes = True
        Me.tvAccesosSegunFormulario.ContextMenuStrip = Me.ContextMenuStrip1
        Me.tvAccesosSegunFormulario.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvAccesosSegunFormulario.FullRowSelect = True
        Me.tvAccesosSegunFormulario.HideSelection = False
        Me.tvAccesosSegunFormulario.ImageIndex = 0
        Me.tvAccesosSegunFormulario.ImageList = Me.ImageList101
        Me.tvAccesosSegunFormulario.Location = New System.Drawing.Point(0, 0)
        Me.tvAccesosSegunFormulario.Name = "tvAccesosSegunFormulario"
        Me.tvAccesosSegunFormulario.SelectedImageIndex = 0
        Me.tvAccesosSegunFormulario.Size = New System.Drawing.Size(245, 392)
        Me.tvAccesosSegunFormulario.TabIndex = 0
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevoToolStripMenuItem, Me.EditarToolStripMenuItem, Me.EliminarToolStripMenuItem, Me.ToolStripSeparator2, Me.ExpandirNodoToolStripMenuItem, Me.ExpandirTodoToolStripMenuItem, Me.ToolStripSeparator3, Me.ContraerNodoToolStripMenuItem, Me.ContraerTodoToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(152, 170)
        '
        'NuevoToolStripMenuItem
        '
        Me.NuevoToolStripMenuItem.Name = "NuevoToolStripMenuItem"
        Me.NuevoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.NuevoToolStripMenuItem.Text = "Nuevo"
        '
        'EditarToolStripMenuItem
        '
        Me.EditarToolStripMenuItem.Name = "EditarToolStripMenuItem"
        Me.EditarToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.EditarToolStripMenuItem.Text = "Editar"
        '
        'EliminarToolStripMenuItem
        '
        Me.EliminarToolStripMenuItem.Name = "EliminarToolStripMenuItem"
        Me.EliminarToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.EliminarToolStripMenuItem.Text = "Eliminar"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(148, 6)
        '
        'ExpandirNodoToolStripMenuItem
        '
        Me.ExpandirNodoToolStripMenuItem.Name = "ExpandirNodoToolStripMenuItem"
        Me.ExpandirNodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ExpandirNodoToolStripMenuItem.Text = "Expandir nodo"
        '
        'ExpandirTodoToolStripMenuItem
        '
        Me.ExpandirTodoToolStripMenuItem.Name = "ExpandirTodoToolStripMenuItem"
        Me.ExpandirTodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ExpandirTodoToolStripMenuItem.Text = "Expandir todo"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(148, 6)
        '
        'ContraerNodoToolStripMenuItem
        '
        Me.ContraerNodoToolStripMenuItem.Name = "ContraerNodoToolStripMenuItem"
        Me.ContraerNodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ContraerNodoToolStripMenuItem.Text = "Contraer nodo"
        '
        'ContraerTodoToolStripMenuItem
        '
        Me.ContraerTodoToolStripMenuItem.Name = "ContraerTodoToolStripMenuItem"
        Me.ContraerTodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ContraerTodoToolStripMenuItem.Text = "Contraer todo"
        '
        'ImageList101
        '
        Me.ImageList101.ImageStream = CType(resources.GetObject("ImageList101.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList101.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList101.Images.SetKeyName(0, "icon.folder.close.gif")
        Me.ImageList101.Images.SetKeyName(1, "icon.folder.open.gif")
        Me.ImageList101.Images.SetKeyName(2, "form.png")
        '
        'dgv
        '
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(17, 133)
        Me.dgv.Name = "dgv"
        Me.dgv.Size = New System.Drawing.Size(543, 219)
        Me.dgv.TabIndex = 1
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 370)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(620, 22)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'gbxOperacionesEspeciales
        '
        Me.gbxOperacionesEspeciales.Controls.Add(Me.lvOtrasOperaciones)
        Me.gbxOperacionesEspeciales.Location = New System.Drawing.Point(323, 3)
        Me.gbxOperacionesEspeciales.Name = "gbxOperacionesEspeciales"
        Me.gbxOperacionesEspeciales.Size = New System.Drawing.Size(240, 109)
        Me.gbxOperacionesEspeciales.TabIndex = 1
        Me.gbxOperacionesEspeciales.TabStop = False
        Me.gbxOperacionesEspeciales.Text = "Operaciones Especiales:"
        '
        'lvOtrasOperaciones
        '
        Me.lvOtrasOperaciones.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lvOtrasOperaciones.ContextMenuStrip = Me.ContextMenuStrip2
        Me.lvOtrasOperaciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvOtrasOperaciones.FullRowSelect = True
        Me.lvOtrasOperaciones.GridLines = True
        Me.lvOtrasOperaciones.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvOtrasOperaciones.HideSelection = False
        Me.lvOtrasOperaciones.LargeImageList = Me.ImageList201
        Me.lvOtrasOperaciones.Location = New System.Drawing.Point(3, 16)
        Me.lvOtrasOperaciones.MultiSelect = False
        Me.lvOtrasOperaciones.Name = "lvOtrasOperaciones"
        Me.lvOtrasOperaciones.Size = New System.Drawing.Size(234, 90)
        Me.lvOtrasOperaciones.SmallImageList = Me.ImageList201
        Me.lvOtrasOperaciones.TabIndex = 0
        Me.lvOtrasOperaciones.UseCompatibleStateImageBehavior = False
        Me.lvOtrasOperaciones.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "ID"
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Acceso"
        Me.ColumnHeader2.Width = 165
        '
        'ContextMenuStrip2
        '
        Me.ContextMenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HabilitarToolStripMenuItem, Me.DenegarToolStripMenuItem})
        Me.ContextMenuStrip2.Name = "ContextMenuStrip2"
        Me.ContextMenuStrip2.Size = New System.Drawing.Size(120, 48)
        '
        'HabilitarToolStripMenuItem
        '
        Me.HabilitarToolStripMenuItem.Image = CType(resources.GetObject("HabilitarToolStripMenuItem.Image"), System.Drawing.Image)
        Me.HabilitarToolStripMenuItem.Name = "HabilitarToolStripMenuItem"
        Me.HabilitarToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.HabilitarToolStripMenuItem.Text = "Habilitar"
        '
        'DenegarToolStripMenuItem
        '
        Me.DenegarToolStripMenuItem.Image = CType(resources.GetObject("DenegarToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DenegarToolStripMenuItem.Name = "DenegarToolStripMenuItem"
        Me.DenegarToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.DenegarToolStripMenuItem.Text = "Denegar"
        '
        'ImageList201
        '
        Me.ImageList201.ImageStream = CType(resources.GetObject("ImageList201.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList201.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList201.Images.SetKeyName(0, "ok.png")
        Me.ImageList201.Images.SetKeyName(1, "delete.png")
        '
        'gbxOperacionesBasicas
        '
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkImprimir)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkAnular)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkEliminar)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkModificar)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkAgregar)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkVisualizar)
        Me.gbxOperacionesBasicas.Location = New System.Drawing.Point(17, 3)
        Me.gbxOperacionesBasicas.Name = "gbxOperacionesBasicas"
        Me.gbxOperacionesBasicas.Size = New System.Drawing.Size(240, 109)
        Me.gbxOperacionesBasicas.TabIndex = 0
        Me.gbxOperacionesBasicas.TabStop = False
        Me.gbxOperacionesBasicas.Text = "Operaciones Basicas"
        '
        'Panel1
        '
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(877, 33)
        Me.Panel1.TabIndex = 0
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'chkImprimir
        '
        Me.chkImprimir.BackColor = System.Drawing.Color.Transparent
        Me.chkImprimir.Color = System.Drawing.Color.Empty
        Me.chkImprimir.Location = New System.Drawing.Point(120, 74)
        Me.chkImprimir.Margin = New System.Windows.Forms.Padding(4)
        Me.chkImprimir.Name = "chkImprimir"
        Me.chkImprimir.Size = New System.Drawing.Size(104, 21)
        Me.chkImprimir.SoloLectura = False
        Me.chkImprimir.TabIndex = 5
        Me.chkImprimir.Texto = "Imprimir"
        Me.chkImprimir.Valor = False
        '
        'chkAnular
        '
        Me.chkAnular.BackColor = System.Drawing.Color.Transparent
        Me.chkAnular.Color = System.Drawing.Color.Empty
        Me.chkAnular.Location = New System.Drawing.Point(120, 51)
        Me.chkAnular.Margin = New System.Windows.Forms.Padding(4)
        Me.chkAnular.Name = "chkAnular"
        Me.chkAnular.Size = New System.Drawing.Size(104, 21)
        Me.chkAnular.SoloLectura = False
        Me.chkAnular.TabIndex = 4
        Me.chkAnular.Texto = "Anular"
        Me.chkAnular.Valor = False
        '
        'chkEliminar
        '
        Me.chkEliminar.BackColor = System.Drawing.Color.Transparent
        Me.chkEliminar.Color = System.Drawing.Color.Empty
        Me.chkEliminar.Location = New System.Drawing.Point(120, 28)
        Me.chkEliminar.Margin = New System.Windows.Forms.Padding(4)
        Me.chkEliminar.Name = "chkEliminar"
        Me.chkEliminar.Size = New System.Drawing.Size(104, 21)
        Me.chkEliminar.SoloLectura = False
        Me.chkEliminar.TabIndex = 3
        Me.chkEliminar.Texto = "Eliminar"
        Me.chkEliminar.Valor = False
        '
        'chkModificar
        '
        Me.chkModificar.BackColor = System.Drawing.Color.Transparent
        Me.chkModificar.Color = System.Drawing.Color.Empty
        Me.chkModificar.Location = New System.Drawing.Point(11, 74)
        Me.chkModificar.Margin = New System.Windows.Forms.Padding(4)
        Me.chkModificar.Name = "chkModificar"
        Me.chkModificar.Size = New System.Drawing.Size(104, 21)
        Me.chkModificar.SoloLectura = False
        Me.chkModificar.TabIndex = 2
        Me.chkModificar.Texto = "Modificar"
        Me.chkModificar.Valor = False
        '
        'chkAgregar
        '
        Me.chkAgregar.BackColor = System.Drawing.Color.Transparent
        Me.chkAgregar.Color = System.Drawing.Color.Empty
        Me.chkAgregar.Location = New System.Drawing.Point(11, 51)
        Me.chkAgregar.Margin = New System.Windows.Forms.Padding(4)
        Me.chkAgregar.Name = "chkAgregar"
        Me.chkAgregar.Size = New System.Drawing.Size(104, 21)
        Me.chkAgregar.SoloLectura = False
        Me.chkAgregar.TabIndex = 1
        Me.chkAgregar.Texto = "Agregar"
        Me.chkAgregar.Valor = False
        '
        'chkVisualizar
        '
        Me.chkVisualizar.BackColor = System.Drawing.Color.Transparent
        Me.chkVisualizar.Color = System.Drawing.Color.Empty
        Me.chkVisualizar.Location = New System.Drawing.Point(11, 28)
        Me.chkVisualizar.Margin = New System.Windows.Forms.Padding(4)
        Me.chkVisualizar.Name = "chkVisualizar"
        Me.chkVisualizar.Size = New System.Drawing.Size(104, 21)
        Me.chkVisualizar.SoloLectura = False
        Me.chkVisualizar.TabIndex = 0
        Me.chkVisualizar.Texto = "Vizualizar"
        Me.chkVisualizar.Valor = False
        '
        'FrmAccesosSegunFormulario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(883, 441)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "FrmAccesosSegunFormulario"
        Me.Text = "FrmAccesosSegunFormulario"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.gbxOperacionesEspeciales.ResumeLayout(False)
        Me.ContextMenuStrip2.ResumeLayout(False)
        Me.gbxOperacionesBasicas.ResumeLayout(False)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents tvAccesosSegunFormulario As TreeView
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents NuevoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EditarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EliminarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents ExpandirNodoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExpandirTodoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents ContraerNodoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ContraerTodoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ImageList101 As ImageList
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents gbxOperacionesEspeciales As GroupBox
    Friend WithEvents lvOtrasOperaciones As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ContextMenuStrip2 As ContextMenuStrip
    Friend WithEvents HabilitarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DenegarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ImageList201 As ImageList
    Friend WithEvents gbxOperacionesBasicas As GroupBox
    Friend WithEvents chkImprimir As ocxCHK
    Friend WithEvents chkAnular As ocxCHK
    Friend WithEvents chkEliminar As ocxCHK
    Friend WithEvents chkModificar As ocxCHK
    Friend WithEvents chkAgregar As ocxCHK
    Friend WithEvents chkVisualizar As ocxCHK
    Friend WithEvents Panel1 As Panel
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents dgv As DataGridView
End Class

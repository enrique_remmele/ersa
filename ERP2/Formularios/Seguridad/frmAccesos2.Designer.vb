﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccesos2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAccesos2))
        Me.chkImprimir = New ERP.ocxCHK()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.NuevoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExpandirNodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExpandirTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ContraerNodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContraerTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lvOtrasOperaciones = New System.Windows.Forms.ListView()
        Me.chkAnular = New ERP.ocxCHK()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.chkEliminar = New ERP.ocxCHK()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.chkModificar = New ERP.ocxCHK()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.lblPlanCuenta = New System.Windows.Forms.Label()
        Me.cbxPerfil = New ERP.ocxCBX()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.tvAccesos = New System.Windows.Forms.TreeView()
        Me.ImageList101 = New System.Windows.Forms.ImageList(Me.components)
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.gbxOperacionesEspeciales = New System.Windows.Forms.GroupBox()
        Me.gbxOperacionesBasicas = New System.Windows.Forms.GroupBox()
        Me.chkAgregar = New ERP.ocxCHK()
        Me.chkVisualizar = New ERP.ocxCHK()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ContextMenuStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.gbxOperacionesEspeciales.SuspendLayout()
        Me.gbxOperacionesBasicas.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'chkImprimir
        '
        Me.chkImprimir.BackColor = System.Drawing.Color.Transparent
        Me.chkImprimir.Color = System.Drawing.Color.Empty
        Me.chkImprimir.Location = New System.Drawing.Point(153, 91)
        Me.chkImprimir.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.chkImprimir.Name = "chkImprimir"
        Me.chkImprimir.Size = New System.Drawing.Size(139, 26)
        Me.chkImprimir.SoloLectura = False
        Me.chkImprimir.TabIndex = 5
        Me.chkImprimir.Texto = "Imprimir"
        Me.chkImprimir.Valor = False
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevoToolStripMenuItem, Me.EditarToolStripMenuItem, Me.EliminarToolStripMenuItem, Me.ToolStripSeparator2, Me.ExpandirNodoToolStripMenuItem, Me.ExpandirTodoToolStripMenuItem, Me.ToolStripSeparator3, Me.ContraerNodoToolStripMenuItem, Me.ContraerTodoToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(176, 184)
        '
        'NuevoToolStripMenuItem
        '
        Me.NuevoToolStripMenuItem.Name = "NuevoToolStripMenuItem"
        Me.NuevoToolStripMenuItem.Size = New System.Drawing.Size(175, 24)
        Me.NuevoToolStripMenuItem.Text = "Nuevo"
        '
        'EditarToolStripMenuItem
        '
        Me.EditarToolStripMenuItem.Name = "EditarToolStripMenuItem"
        Me.EditarToolStripMenuItem.Size = New System.Drawing.Size(175, 24)
        Me.EditarToolStripMenuItem.Text = "Editar"
        '
        'EliminarToolStripMenuItem
        '
        Me.EliminarToolStripMenuItem.Name = "EliminarToolStripMenuItem"
        Me.EliminarToolStripMenuItem.Size = New System.Drawing.Size(175, 24)
        Me.EliminarToolStripMenuItem.Text = "Eliminar"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(172, 6)
        '
        'ExpandirNodoToolStripMenuItem
        '
        Me.ExpandirNodoToolStripMenuItem.Name = "ExpandirNodoToolStripMenuItem"
        Me.ExpandirNodoToolStripMenuItem.Size = New System.Drawing.Size(175, 24)
        Me.ExpandirNodoToolStripMenuItem.Text = "Expandir nodo"
        '
        'ExpandirTodoToolStripMenuItem
        '
        Me.ExpandirTodoToolStripMenuItem.Name = "ExpandirTodoToolStripMenuItem"
        Me.ExpandirTodoToolStripMenuItem.Size = New System.Drawing.Size(175, 24)
        Me.ExpandirTodoToolStripMenuItem.Text = "Expandir todo"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(172, 6)
        '
        'ContraerNodoToolStripMenuItem
        '
        Me.ContraerNodoToolStripMenuItem.Name = "ContraerNodoToolStripMenuItem"
        Me.ContraerNodoToolStripMenuItem.Size = New System.Drawing.Size(175, 24)
        Me.ContraerNodoToolStripMenuItem.Text = "Contraer nodo"
        '
        'ContraerTodoToolStripMenuItem
        '
        Me.ContraerTodoToolStripMenuItem.Name = "ContraerTodoToolStripMenuItem"
        Me.ContraerTodoToolStripMenuItem.Size = New System.Drawing.Size(175, 24)
        Me.ContraerTodoToolStripMenuItem.Text = "Contraer todo"
        '
        'lvOtrasOperaciones
        '
        Me.lvOtrasOperaciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvOtrasOperaciones.Location = New System.Drawing.Point(4, 19)
        Me.lvOtrasOperaciones.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.lvOtrasOperaciones.Name = "lvOtrasOperaciones"
        Me.lvOtrasOperaciones.Size = New System.Drawing.Size(312, 111)
        Me.lvOtrasOperaciones.TabIndex = 0
        Me.lvOtrasOperaciones.UseCompatibleStateImageBehavior = False
        '
        'chkAnular
        '
        Me.chkAnular.BackColor = System.Drawing.Color.Transparent
        Me.chkAnular.Color = System.Drawing.Color.Empty
        Me.chkAnular.Location = New System.Drawing.Point(153, 63)
        Me.chkAnular.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.chkAnular.Name = "chkAnular"
        Me.chkAnular.Size = New System.Drawing.Size(139, 26)
        Me.chkAnular.SoloLectura = False
        Me.chkAnular.TabIndex = 4
        Me.chkAnular.Texto = "Anular"
        Me.chkAnular.Valor = False
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(236, 313)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(100, 28)
        Me.btnCancelar.TabIndex = 4
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(57, 20)
        Me.tsslEstado.Text = "Estado:"
        '
        'chkEliminar
        '
        Me.chkEliminar.BackColor = System.Drawing.Color.Transparent
        Me.chkEliminar.Color = System.Drawing.Color.Empty
        Me.chkEliminar.Location = New System.Drawing.Point(153, 34)
        Me.chkEliminar.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.chkEliminar.Name = "chkEliminar"
        Me.chkEliminar.Size = New System.Drawing.Size(139, 26)
        Me.chkEliminar.SoloLectura = False
        Me.chkEliminar.TabIndex = 3
        Me.chkEliminar.Texto = "Eliminar"
        Me.chkEliminar.Valor = False
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(128, 313)
        Me.btnGuardar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(100, 28)
        Me.btnGuardar.TabIndex = 3
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'chkModificar
        '
        Me.chkModificar.BackColor = System.Drawing.Color.Transparent
        Me.chkModificar.Color = System.Drawing.Color.Empty
        Me.chkModificar.Location = New System.Drawing.Point(8, 91)
        Me.chkModificar.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.chkModificar.Name = "chkModificar"
        Me.chkModificar.Size = New System.Drawing.Size(139, 26)
        Me.chkModificar.SoloLectura = False
        Me.chkModificar.TabIndex = 2
        Me.chkModificar.Texto = "Modificar"
        Me.chkModificar.Valor = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.LinkLabel1)
        Me.Panel1.Controls.Add(Me.lblPlanCuenta)
        Me.Panel1.Controls.Add(Me.cbxPerfil)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(4, 4)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(811, 40)
        Me.Panel1.TabIndex = 0
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel1.Location = New System.Drawing.Point(331, 12)
        Me.LinkLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(164, 15)
        Me.LinkLabel1.TabIndex = 2
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Clonar permisos a otro perfil."
        '
        'lblPlanCuenta
        '
        Me.lblPlanCuenta.AutoSize = True
        Me.lblPlanCuenta.Location = New System.Drawing.Point(16, 12)
        Me.lblPlanCuenta.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblPlanCuenta.Name = "lblPlanCuenta"
        Me.lblPlanCuenta.Size = New System.Drawing.Size(44, 17)
        Me.lblPlanCuenta.TabIndex = 0
        Me.lblPlanCuenta.Text = "Perfil:"
        '
        'cbxPerfil
        '
        Me.cbxPerfil.CampoWhere = Nothing
        Me.cbxPerfil.CargarUnaSolaVez = False
        Me.cbxPerfil.DataDisplayMember = Nothing
        Me.cbxPerfil.DataFilter = Nothing
        Me.cbxPerfil.DataOrderBy = Nothing
        Me.cbxPerfil.DataSource = Nothing
        Me.cbxPerfil.DataValueMember = Nothing
        Me.cbxPerfil.FormABM = Nothing
        Me.cbxPerfil.Indicaciones = Nothing
        Me.cbxPerfil.Location = New System.Drawing.Point(68, 7)
        Me.cbxPerfil.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.cbxPerfil.Name = "cbxPerfil"
        Me.cbxPerfil.SeleccionObligatoria = False
        Me.cbxPerfil.Size = New System.Drawing.Size(255, 26)
        Me.cbxPerfil.SoloLectura = False
        Me.cbxPerfil.TabIndex = 1
        Me.cbxPerfil.Texto = ""
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.SplitContainer1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(819, 587)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.Location = New System.Drawing.Point(4, 52)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.tvAccesos)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnCancelar)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnModificar)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnGuardar)
        Me.SplitContainer1.Panel2.Controls.Add(Me.StatusStrip1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.gbxOperacionesEspeciales)
        Me.SplitContainer1.Panel2.Controls.Add(Me.gbxOperacionesBasicas)
        Me.SplitContainer1.Size = New System.Drawing.Size(811, 531)
        Me.SplitContainer1.SplitterDistance = 324
        Me.SplitContainer1.SplitterWidth = 5
        Me.SplitContainer1.TabIndex = 1
        '
        'tvAccesos
        '
        Me.tvAccesos.AllowDrop = True
        Me.tvAccesos.CheckBoxes = True
        Me.tvAccesos.ContextMenuStrip = Me.ContextMenuStrip1
        Me.tvAccesos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvAccesos.FullRowSelect = True
        Me.tvAccesos.HideSelection = False
        Me.tvAccesos.ImageIndex = 0
        Me.tvAccesos.ImageList = Me.ImageList101
        Me.tvAccesos.Location = New System.Drawing.Point(0, 0)
        Me.tvAccesos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tvAccesos.Name = "tvAccesos"
        Me.tvAccesos.SelectedImageIndex = 0
        Me.tvAccesos.Size = New System.Drawing.Size(320, 527)
        Me.tvAccesos.TabIndex = 0
        '
        'ImageList101
        '
        Me.ImageList101.ImageStream = CType(resources.GetObject("ImageList101.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList101.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList101.Images.SetKeyName(0, "icon.folder.close.gif")
        Me.ImageList101.Images.SetKeyName(1, "icon.folder.open.gif")
        Me.ImageList101.Images.SetKeyName(2, "form.png")
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(20, 313)
        Me.btnModificar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(100, 28)
        Me.btnModificar.TabIndex = 2
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 502)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(478, 25)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'gbxOperacionesEspeciales
        '
        Me.gbxOperacionesEspeciales.Controls.Add(Me.lvOtrasOperaciones)
        Me.gbxOperacionesEspeciales.Location = New System.Drawing.Point(16, 145)
        Me.gbxOperacionesEspeciales.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxOperacionesEspeciales.Name = "gbxOperacionesEspeciales"
        Me.gbxOperacionesEspeciales.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxOperacionesEspeciales.Size = New System.Drawing.Size(320, 134)
        Me.gbxOperacionesEspeciales.TabIndex = 1
        Me.gbxOperacionesEspeciales.TabStop = False
        Me.gbxOperacionesEspeciales.Text = "Operaciones Especiales:"
        '
        'gbxOperacionesBasicas
        '
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkImprimir)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkAnular)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkEliminar)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkModificar)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkAgregar)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkVisualizar)
        Me.gbxOperacionesBasicas.Location = New System.Drawing.Point(16, 4)
        Me.gbxOperacionesBasicas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxOperacionesBasicas.Name = "gbxOperacionesBasicas"
        Me.gbxOperacionesBasicas.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxOperacionesBasicas.Size = New System.Drawing.Size(320, 134)
        Me.gbxOperacionesBasicas.TabIndex = 0
        Me.gbxOperacionesBasicas.TabStop = False
        Me.gbxOperacionesBasicas.Text = "Operaciones Basicas"
        '
        'chkAgregar
        '
        Me.chkAgregar.BackColor = System.Drawing.Color.Transparent
        Me.chkAgregar.Color = System.Drawing.Color.Empty
        Me.chkAgregar.Location = New System.Drawing.Point(8, 63)
        Me.chkAgregar.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.chkAgregar.Name = "chkAgregar"
        Me.chkAgregar.Size = New System.Drawing.Size(139, 26)
        Me.chkAgregar.SoloLectura = False
        Me.chkAgregar.TabIndex = 1
        Me.chkAgregar.Texto = "Agregar"
        Me.chkAgregar.Valor = False
        '
        'chkVisualizar
        '
        Me.chkVisualizar.BackColor = System.Drawing.Color.Transparent
        Me.chkVisualizar.Color = System.Drawing.Color.Empty
        Me.chkVisualizar.Location = New System.Drawing.Point(8, 34)
        Me.chkVisualizar.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.chkVisualizar.Name = "chkVisualizar"
        Me.chkVisualizar.Size = New System.Drawing.Size(139, 26)
        Me.chkVisualizar.SoloLectura = False
        Me.chkVisualizar.TabIndex = 0
        Me.chkVisualizar.Texto = "Vizualizar"
        Me.chkVisualizar.Valor = False
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'frmAccesos2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(819, 587)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "frmAccesos2"
        Me.Tag = "ACCESOS"
        Me.Text = "frmAccesos2"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.gbxOperacionesEspeciales.ResumeLayout(False)
        Me.gbxOperacionesBasicas.ResumeLayout(False)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkImprimir As ERP.ocxCHK
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents NuevoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExpandirNodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExpandirTodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ContraerNodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContraerTodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lvOtrasOperaciones As System.Windows.Forms.ListView
    Friend WithEvents chkAnular As ERP.ocxCHK
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents chkEliminar As ERP.ocxCHK
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents chkModificar As ERP.ocxCHK
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents lblPlanCuenta As System.Windows.Forms.Label
    Friend WithEvents cbxPerfil As ERP.ocxCBX
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents tvAccesos As System.Windows.Forms.TreeView
    Friend WithEvents ImageList101 As System.Windows.Forms.ImageList
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents gbxOperacionesEspeciales As System.Windows.Forms.GroupBox
    Friend WithEvents gbxOperacionesBasicas As System.Windows.Forms.GroupBox
    Friend WithEvents chkAgregar As ERP.ocxCHK
    Friend WithEvents chkVisualizar As ERP.ocxCHK
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
End Class

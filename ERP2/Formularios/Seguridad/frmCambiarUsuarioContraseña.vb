﻿Public Class frmCambiarUsuarioContraseña

    'CLASES
    Dim CSistema As New CSistema

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button

        'Usuario
        txtUsuario.SetValue(vgUsuario)
        txtIdentificador.SetValue(vgUsuarioIdentificador)

        'TextBox
        txtContraseñaActual.txt.UseSystemPasswordChar = True
        txtContraseñaActual.txt.PasswordChar = "*"
        txtContraseñaNueva.txt.UseSystemPasswordChar = True
        txtContraseñaNueva.txt.PasswordChar = "*"
        txtRepetirContraseña.txt.UseSystemPasswordChar = True
        txtRepetirContraseña.txt.PasswordChar = "*"

        txtContraseñaActual.txt.CharacterCasing = CharacterCasing.Normal
        txtContraseñaNueva.txt.CharacterCasing = CharacterCasing.Normal
        txtRepetirContraseña.txt.CharacterCasing = CharacterCasing.Normal

        'Foco
        txtContraseñaActual.txt.Focus()
        txtContraseñaActual.txt.SelectAll()

    End Sub

    Sub Cambiar()

        'Validar
        If txtContraseñaNueva.txt.Text <> txtRepetirContraseña.txt.Text Then
            MessageBox.Show("La contraseña nueva no concuerda!", "Usuario", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtContraseñaNueva.txt.Focus()
            txtContraseñaNueva.txt.SelectAll()
            Exit Sub
        End If

        'Procesar
        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDPerfil", vgIDPerfil, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ContraseñaActual", txtContraseñaActual.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ContraseñaNueva", txtContraseñaNueva.txt.Text.Trim, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpCambiarContraseña", False, False, MensajeRetorno) = False Then
            MessageBox.Show(MensajeRetorno, "Usuario", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Me.Close()

    End Sub

    Private Sub frmCambiarUsuarioContraseña_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmCambiarUsuarioContraseña_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Cambiar()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmCambiarUsuarioContraseña_Activate()
        Me.Refresh()
    End Sub

End Class
﻿Public Class frmLogAccesos

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'FUNCIONES
    Sub Inicializar()

        'Form

        'Fecha
        dtpDesde.Value = VGFechaHoraSistema.Day & "/" & VGFechaHoraSistema.Month & "/" & VGFechaHoraSistema.Year & " 00:00:00"

        'ComboBox
        cbxGrupos.SelectedIndex = 0
        cbxOrden.SelectedIndex = 0

        'Funciones
        CargarDatos()

    End Sub

    Sub CargarDatos()

        tvLista.Nodes.Clear()

        If cbxGrupos.SelectedIndex < 0 Then
            Exit Sub
        End If

        'Cargar Arbol
        Dim Tablas() As String = {"VTablaLog", "VUsuario", "VTipoOperacionLog", "VSucursal", "VTerminal"}
        Dim Campos() As String = {"Descripcion", "Usuario", "Descripcion", "Descripcion", "Descripcion"}

        If cbxGrupos.SelectedIndex > 0 Then

            Select Case cbxGrupos.SelectedIndex
                Case 1
                    tvLista.Nodes.Add("Tablas")
                Case 2
                    tvLista.Nodes.Add("Usuarios")
                Case 3
                    tvLista.Nodes.Add("Operaciones")
                Case 4
                    tvLista.Nodes.Add("Sucursales")
                Case 5
                    tvLista.Nodes.Add("Terminales")
            End Select

            Dim i As Integer = cbxGrupos.SelectedIndex - 1
            Dim Tabla As String = Tablas(i)
            Dim dt As DataTable = CData.GetTable(Tabla).Copy
            CData.OrderDataTable(dt, Campos(i))
            Dim nodo As TreeNode = tvLista.Nodes(0)
            For Each oRow As DataRow In dt.Rows
                Dim nodo1 As New TreeNode(oRow(Campos(i)))
                nodo1.SelectedImageIndex = i + 1
                nodo1.ImageIndex = i + 1
                nodo.Nodes.Add(nodo1)
            Next
        Else

            tvLista.Nodes.Add("Tablas")
            tvLista.Nodes.Add("Usuarios")
            tvLista.Nodes.Add("Operaciones")
            tvLista.Nodes.Add("Sucursales")
            tvLista.Nodes.Add("Terminales")

            For i As Integer = 0 To Tablas.Length - 1
                Dim Tabla As String = Tablas(i)
                Dim dt As DataTable = CData.GetTable(Tabla).Copy
                CData.OrderDataTable(dt, Campos(i))
                Dim nodo As TreeNode = tvLista.Nodes(i)
                For Each oRow As DataRow In dt.Rows
                    Dim nodo1 As New TreeNode(oRow(Campos(i)))
                    nodo1.SelectedImageIndex = i + 1
                    nodo1.ImageIndex = i + 1
                    nodo.Nodes.Add(nodo1)
                Next
            Next
        End If

        

    End Sub

    Sub Listar()

        'Limpiar lista
        lvLista.Items.Clear()

        Dim Consulta As String = "Select * From VLogSucesos "
        Dim Where As String = " Where (Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, True) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, True) & "') "
        Dim OrderBy As String = " Order By " & cbxOrden.Text

        'Tabla
        If tvLista.Nodes.Count = 1 Then
            Where = Where & ConfigurarWhere()
        Else
            Where = Where & ConfigurarWhere(0, "Tabla")
            Where = Where & ConfigurarWhere(1, "Usuario")
            Where = Where & ConfigurarWhere(2, "Operacion")
            Where = Where & ConfigurarWhere(3, "Sucursal")
            Where = Where & ConfigurarWhere(4, "Terminal")
        End If


        Dim dt As DataTable = CSistema.ExecuteToDataTable(Consulta & Where & OrderBy)

        If dt Is Nothing Then
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each oRow As DataRow In dt.Rows
            Dim item As New ListViewItem(oRow("Fec").ToString)
            item.SubItems.Add(oRow("Hora").ToString)
            item.SubItems.Add(oRow("Tabla").ToString)
            item.SubItems.Add(oRow("Operacion").ToString)
            item.SubItems.Add(oRow("Usuario").ToString)
            item.SubItems.Add(oRow("Terminal").ToString)
            item.SubItems.Add(oRow("Sucursal").ToString)

            lvLista.Items.Add(item)

        Next

        lblCantidad.Text = "Cantidad:  " & lvLista.Items.Count

    End Sub

    Private Function ConfigurarWhere(ByVal Indice As Integer, ByVal campo As String) As String

        ConfigurarWhere = ""
        Dim Where As String = ""

        Dim Nodo As TreeNode
        If tvLista.Nodes.Count = 1 Then
            Nodo = tvLista.Nodes(0)
        Else
            Nodo = tvLista.Nodes(Indice)
        End If

        For Each n As TreeNode In Nodo.Nodes

            If n.Checked = True Then
                If Where = "" Then
                    Where = Where & " And (" & campo & " = '" & n.Text & "' "
                Else
                    Where = Where & " Or " & campo & " = '" & n.Text & "' "
                End If
            End If
        Next

        If Where <> "" Then
            Where = Where & " ) "
        End If

        ConfigurarWhere = Where

    End Function

    Private Function ConfigurarWhere() As String

        ConfigurarWhere = ""
        Dim Where As String = ""

        Select Case cbxGrupos.SelectedIndex

            Case 1
                Where = ConfigurarWhere(0, "Tabla")
            Case 2
                Where = Where & ConfigurarWhere(1, "Usuario")
            Case 3
                Where = Where & ConfigurarWhere(2, "Operacion")
            Case 4
                Where = Where & ConfigurarWhere(3, "Sucursal")
            Case 5
                Where = Where & ConfigurarWhere(4, "Terminal")
        End Select

        ConfigurarWhere = Where

    End Function

    Private Sub EstablecerFecha(ByVal Rango As CSistema.NUMFechaPreestablecida)

        dtpDesde.Value = CSistema.EstablecerFechaPredeterminada(Rango)

    End Sub

    Private Sub frmLogAccesos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub cbxGrupos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxGrupos.Click

    End Sub

    Private Sub cbxGrupos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxGrupos.SelectedIndexChanged
        CargarDatos()
    End Sub

    Private Sub btnActualizarPedidos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizarPedidos.Click
        Listar()
    End Sub

    Private Sub btnActualizarGrupo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizarGrupo.Click
        Listar()
    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        EstablecerFecha(ERP.CSistema.NUMFechaPreestablecida.HOY)
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        EstablecerFecha(ERP.CSistema.NUMFechaPreestablecida.AYER)
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        EstablecerFecha(ERP.CSistema.NUMFechaPreestablecida.ESTA_SEMANA)
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        EstablecerFecha(ERP.CSistema.NUMFechaPreestablecida.ESTE_MES)
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmLogAccesos_Activate()
        Me.Refresh()
    End Sub

End Class
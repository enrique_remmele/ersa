﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPerfil
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.gbxOperacionesBasicas = New System.Windows.Forms.GroupBox()
        Me.chkImprimir = New ERP.ocxCHK()
        Me.chkAnular = New ERP.ocxCHK()
        Me.chkEliminar = New ERP.ocxCHK()
        Me.chkModificar = New ERP.ocxCHK()
        Me.chkAgregar = New ERP.ocxCHK()
        Me.chkVisualizar = New ERP.ocxCHK()
        Me.chkVerCosto = New ERP.ocxCHK()
        Me.cbxDepartamentoEmpresa = New ERP.ocxCBX()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.gbxOperacionesBasicas.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(90, 31)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(201, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(414, 411)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 14
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(151, 87)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 6
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(90, 87)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 5
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(90, 4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 1
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(12, 89)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 4
        Me.lblEstado.Text = "Estado:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(333, 205)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 11
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(414, 205)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 12
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(252, 205)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 10
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(171, 205)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 9
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(90, 205)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 8
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lvLista
        '
        Me.lvLista.Location = New System.Drawing.Point(90, 234)
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(399, 171)
        Me.lvLista.TabIndex = 13
        Me.lvLista.UseCompatibleStateImageBehavior = False
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(12, 35)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Text = "Descripción:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(12, 9)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 444)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(501, 22)
        Me.StatusStrip1.TabIndex = 15
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'gbxOperacionesBasicas
        '
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkImprimir)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkAnular)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkEliminar)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkModificar)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkAgregar)
        Me.gbxOperacionesBasicas.Controls.Add(Me.chkVisualizar)
        Me.gbxOperacionesBasicas.Location = New System.Drawing.Point(90, 122)
        Me.gbxOperacionesBasicas.Name = "gbxOperacionesBasicas"
        Me.gbxOperacionesBasicas.Size = New System.Drawing.Size(318, 77)
        Me.gbxOperacionesBasicas.TabIndex = 7
        Me.gbxOperacionesBasicas.TabStop = False
        Me.gbxOperacionesBasicas.Text = "Operaciones Basicas"
        '
        'chkImprimir
        '
        Me.chkImprimir.BackColor = System.Drawing.Color.Transparent
        Me.chkImprimir.Color = System.Drawing.Color.Empty
        Me.chkImprimir.Location = New System.Drawing.Point(6, 47)
        Me.chkImprimir.Name = "chkImprimir"
        Me.chkImprimir.Size = New System.Drawing.Size(104, 21)
        Me.chkImprimir.SoloLectura = False
        Me.chkImprimir.TabIndex = 5
        Me.chkImprimir.Texto = "Imprimir"
        Me.chkImprimir.Valor = False
        '
        'chkAnular
        '
        Me.chkAnular.BackColor = System.Drawing.Color.Transparent
        Me.chkAnular.Color = System.Drawing.Color.Empty
        Me.chkAnular.Location = New System.Drawing.Point(226, 47)
        Me.chkAnular.Name = "chkAnular"
        Me.chkAnular.Size = New System.Drawing.Size(104, 21)
        Me.chkAnular.SoloLectura = False
        Me.chkAnular.TabIndex = 4
        Me.chkAnular.Texto = "Anular"
        Me.chkAnular.Valor = False
        '
        'chkEliminar
        '
        Me.chkEliminar.BackColor = System.Drawing.Color.Transparent
        Me.chkEliminar.Color = System.Drawing.Color.Empty
        Me.chkEliminar.Location = New System.Drawing.Point(226, 19)
        Me.chkEliminar.Name = "chkEliminar"
        Me.chkEliminar.Size = New System.Drawing.Size(104, 21)
        Me.chkEliminar.SoloLectura = False
        Me.chkEliminar.TabIndex = 3
        Me.chkEliminar.Texto = "Eliminar"
        Me.chkEliminar.Valor = False
        '
        'chkModificar
        '
        Me.chkModificar.BackColor = System.Drawing.Color.Transparent
        Me.chkModificar.Color = System.Drawing.Color.Empty
        Me.chkModificar.Location = New System.Drawing.Point(116, 47)
        Me.chkModificar.Name = "chkModificar"
        Me.chkModificar.Size = New System.Drawing.Size(104, 21)
        Me.chkModificar.SoloLectura = False
        Me.chkModificar.TabIndex = 2
        Me.chkModificar.Texto = "Modificar"
        Me.chkModificar.Valor = False
        '
        'chkAgregar
        '
        Me.chkAgregar.BackColor = System.Drawing.Color.Transparent
        Me.chkAgregar.Color = System.Drawing.Color.Empty
        Me.chkAgregar.Location = New System.Drawing.Point(116, 20)
        Me.chkAgregar.Name = "chkAgregar"
        Me.chkAgregar.Size = New System.Drawing.Size(104, 21)
        Me.chkAgregar.SoloLectura = False
        Me.chkAgregar.TabIndex = 1
        Me.chkAgregar.Texto = "Agregar"
        Me.chkAgregar.Valor = False
        '
        'chkVisualizar
        '
        Me.chkVisualizar.BackColor = System.Drawing.Color.Transparent
        Me.chkVisualizar.Color = System.Drawing.Color.Empty
        Me.chkVisualizar.Location = New System.Drawing.Point(6, 19)
        Me.chkVisualizar.Name = "chkVisualizar"
        Me.chkVisualizar.Size = New System.Drawing.Size(104, 21)
        Me.chkVisualizar.SoloLectura = False
        Me.chkVisualizar.TabIndex = 0
        Me.chkVisualizar.Texto = "Vizualizar"
        Me.chkVisualizar.Valor = False
        '
        'chkVerCosto
        '
        Me.chkVerCosto.BackColor = System.Drawing.Color.Transparent
        Me.chkVerCosto.Color = System.Drawing.Color.Empty
        Me.chkVerCosto.Location = New System.Drawing.Point(190, 5)
        Me.chkVerCosto.Name = "chkVerCosto"
        Me.chkVerCosto.Size = New System.Drawing.Size(175, 21)
        Me.chkVerCosto.SoloLectura = False
        Me.chkVerCosto.TabIndex = 22
        Me.chkVerCosto.Texto = "Puede ver Costos"
        Me.chkVerCosto.Valor = False
        '
        'cbxDepartamentoEmpresa
        '
        Me.cbxDepartamentoEmpresa.CampoWhere = Nothing
        Me.cbxDepartamentoEmpresa.CargarUnaSolaVez = False
        Me.cbxDepartamentoEmpresa.DataDisplayMember = "Departamento"
        Me.cbxDepartamentoEmpresa.DataFilter = Nothing
        Me.cbxDepartamentoEmpresa.DataOrderBy = Nothing
        Me.cbxDepartamentoEmpresa.DataSource = "vDepartamentoEmpresa"
        Me.cbxDepartamentoEmpresa.DataValueMember = "ID"
        Me.cbxDepartamentoEmpresa.dtSeleccionado = Nothing
        Me.cbxDepartamentoEmpresa.FormABM = Nothing
        Me.cbxDepartamentoEmpresa.Indicaciones = Nothing
        Me.cbxDepartamentoEmpresa.Location = New System.Drawing.Point(90, 59)
        Me.cbxDepartamentoEmpresa.Name = "cbxDepartamentoEmpresa"
        Me.cbxDepartamentoEmpresa.SeleccionMultiple = False
        Me.cbxDepartamentoEmpresa.SeleccionObligatoria = False
        Me.cbxDepartamentoEmpresa.Size = New System.Drawing.Size(201, 23)
        Me.cbxDepartamentoEmpresa.SoloLectura = False
        Me.cbxDepartamentoEmpresa.TabIndex = 23
        Me.cbxDepartamentoEmpresa.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 13)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "Departamento:"
        '
        'frmPerfil
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(501, 466)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbxDepartamentoEmpresa)
        Me.Controls.Add(Me.chkVerCosto)
        Me.Controls.Add(Me.gbxOperacionesBasicas)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.rdbDesactivado)
        Me.Controls.Add(Me.rdbActivo)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.lvLista)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.lblID)
        Me.Name = "frmPerfil"
        Me.Tag = "PERFIL"
        Me.Text = "frmPerfil"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.gbxOperacionesBasicas.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents rdbDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbActivo As System.Windows.Forms.RadioButton
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lvLista As System.Windows.Forms.ListView
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents gbxOperacionesBasicas As System.Windows.Forms.GroupBox
    Friend WithEvents chkImprimir As ERP.ocxCHK
    Friend WithEvents chkAnular As ERP.ocxCHK
    Friend WithEvents chkEliminar As ERP.ocxCHK
    Friend WithEvents chkModificar As ERP.ocxCHK
    Friend WithEvents chkAgregar As ERP.ocxCHK
    Friend WithEvents chkVisualizar As ERP.ocxCHK
    Friend WithEvents chkVerCosto As ERP.ocxCHK
    Friend WithEvents Label1 As Label
    Friend WithEvents cbxDepartamentoEmpresa As ocxCBX
End Class

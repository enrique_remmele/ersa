﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsuario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUsuario))
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtBuscar = New ERP.ocxTXTString()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.dgvLista = New System.Windows.Forms.DataGridView()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkChofer = New ERP.ocxCHK()
        Me.cbxChofer = New ERP.ocxCBX()
        Me.txtEmail = New ERP.ocxTXTString()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.lblID = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.lklReestablecerContraseña = New System.Windows.Forms.LinkLabel()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.txtUsuario = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblUsuario = New System.Windows.Forms.Label()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.cbxPerfil = New ERP.ocxCBX()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.lblPerfil = New System.Windows.Forms.Label()
        Me.txtNombre = New ERP.ocxTXTString()
        Me.txtIdentificador = New ERP.ocxTXTString()
        Me.lblIdentificador = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 449)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(835, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 334.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvLista, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 1, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(835, 449)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.txtBuscar)
        Me.FlowLayoutPanel3.Controls.Add(Me.Button1)
        Me.FlowLayoutPanel3.Controls.Add(Me.Label2)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtCantidad)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(495, 24)
        Me.FlowLayoutPanel3.TabIndex = 0
        '
        'txtBuscar
        '
        Me.txtBuscar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBuscar.Color = System.Drawing.Color.Empty
        Me.txtBuscar.Indicaciones = Nothing
        Me.txtBuscar.Location = New System.Drawing.Point(3, 3)
        Me.txtBuscar.Multilinea = False
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(205, 21)
        Me.txtBuscar.SoloLectura = False
        Me.txtBuscar.TabIndex = 0
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBuscar.Texto = ""
        '
        'Button1
        '
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.ImageIndex = 6
        Me.Button1.ImageList = Me.ImageList1
        Me.Button1.Location = New System.Drawing.Point(214, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(20, 21)
        Me.Button1.TabIndex = 1
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "Agregar.png")
        Me.ImageList1.Images.SetKeyName(1, "Editar.png")
        Me.ImageList1.Images.SetKeyName(2, "Eliminar.png")
        Me.ImageList1.Images.SetKeyName(3, "Guardar.png")
        Me.ImageList1.Images.SetKeyName(4, "Cancelar.png")
        Me.ImageList1.Images.SetKeyName(5, "Actualizar.png")
        Me.ImageList1.Images.SetKeyName(6, "Buscar.png")
        Me.ImageList1.Images.SetKeyName(7, "Cargar.png")
        Me.ImageList1.Images.SetKeyName(8, "Imprimir.png")
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(240, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 24)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Cant.:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = Nothing
        Me.txtCantidad.Location = New System.Drawing.Point(281, 3)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(63, 20)
        Me.txtCantidad.SoloLectura = True
        Me.txtCantidad.TabIndex = 3
        Me.txtCantidad.TabStop = False
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLista.Location = New System.Drawing.Point(3, 33)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.Size = New System.Drawing.Size(495, 376)
        Me.dgvLista.TabIndex = 1
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnNuevo)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnEditar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnEliminar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnImprimir)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 415)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(495, 31)
        Me.FlowLayoutPanel1.TabIndex = 2
        '
        'btnNuevo
        '
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.ImageIndex = 0
        Me.btnNuevo.ImageList = Me.ImageList1
        Me.btnNuevo.Location = New System.Drawing.Point(3, 3)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(82, 23)
        Me.btnNuevo.TabIndex = 0
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEditar.ImageIndex = 1
        Me.btnEditar.ImageList = Me.ImageList1
        Me.btnEditar.Location = New System.Drawing.Point(91, 3)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 1
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.ImageIndex = 2
        Me.btnEliminar.ImageList = Me.ImageList1
        Me.btnEliminar.Location = New System.Drawing.Point(172, 3)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(83, 23)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImprimir.ImageKey = "Imprimir.png"
        Me.btnImprimir.ImageList = Me.ImageList1
        Me.btnImprimir.Location = New System.Drawing.Point(261, 3)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(87, 23)
        Me.btnImprimir.TabIndex = 3
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkChofer)
        Me.GroupBox1.Controls.Add(Me.cbxChofer)
        Me.GroupBox1.Controls.Add(Me.txtEmail)
        Me.GroupBox1.Controls.Add(Me.lblEmail)
        Me.GroupBox1.Controls.Add(Me.chkVendedor)
        Me.GroupBox1.Controls.Add(Me.cbxVendedor)
        Me.GroupBox1.Controls.Add(Me.lblID)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.lblNombre)
        Me.GroupBox1.Controls.Add(Me.lklReestablecerContraseña)
        Me.GroupBox1.Controls.Add(Me.lblEstado)
        Me.GroupBox1.Controls.Add(Me.txtUsuario)
        Me.GroupBox1.Controls.Add(Me.txtID)
        Me.GroupBox1.Controls.Add(Me.lblUsuario)
        Me.GroupBox1.Controls.Add(Me.rdbActivo)
        Me.GroupBox1.Controls.Add(Me.cbxPerfil)
        Me.GroupBox1.Controls.Add(Me.rdbDesactivado)
        Me.GroupBox1.Controls.Add(Me.lblPerfil)
        Me.GroupBox1.Controls.Add(Me.txtNombre)
        Me.GroupBox1.Controls.Add(Me.txtIdentificador)
        Me.GroupBox1.Controls.Add(Me.lblIdentificador)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(504, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.TableLayoutPanel1.SetRowSpan(Me.GroupBox1, 2)
        Me.GroupBox1.Size = New System.Drawing.Size(328, 406)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos"
        '
        'chkChofer
        '
        Me.chkChofer.BackColor = System.Drawing.Color.Transparent
        Me.chkChofer.Color = System.Drawing.Color.Empty
        Me.chkChofer.Location = New System.Drawing.Point(6, 154)
        Me.chkChofer.Name = "chkChofer"
        Me.chkChofer.Size = New System.Drawing.Size(68, 21)
        Me.chkChofer.SoloLectura = False
        Me.chkChofer.TabIndex = 12
        Me.chkChofer.Texto = "Chofer:"
        Me.chkChofer.Valor = False
        '
        'cbxChofer
        '
        Me.cbxChofer.CampoWhere = Nothing
        Me.cbxChofer.CargarUnaSolaVez = False
        Me.cbxChofer.DataDisplayMember = "Nombres"
        Me.cbxChofer.DataFilter = Nothing
        Me.cbxChofer.DataOrderBy = "Nombres"
        Me.cbxChofer.DataSource = "VChofer"
        Me.cbxChofer.DataValueMember = "ID"
        Me.cbxChofer.FormABM = Nothing
        Me.cbxChofer.Indicaciones = Nothing
        Me.cbxChofer.Location = New System.Drawing.Point(83, 154)
        Me.cbxChofer.Name = "cbxChofer"
        Me.cbxChofer.SeleccionObligatoria = True
        Me.cbxChofer.Size = New System.Drawing.Size(237, 21)
        Me.cbxChofer.SoloLectura = False
        Me.cbxChofer.TabIndex = 13
        Me.cbxChofer.Texto = ""
        '
        'txtEmail
        '
        Me.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtEmail.Color = System.Drawing.Color.Empty
        Me.txtEmail.Indicaciones = Nothing
        Me.txtEmail.Location = New System.Drawing.Point(83, 181)
        Me.txtEmail.Multilinea = False
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(236, 21)
        Me.txtEmail.SoloLectura = False
        Me.txtEmail.TabIndex = 15
        Me.txtEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEmail.Texto = ""
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Location = New System.Drawing.Point(6, 185)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(35, 13)
        Me.lblEmail.TabIndex = 14
        Me.lblEmail.Text = "Email:"
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(6, 127)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(68, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 10
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = Nothing
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "VVendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.FormABM = Nothing
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(83, 127)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionObligatoria = True
        Me.cbxVendedor.Size = New System.Drawing.Size(237, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 11
        Me.cbxVendedor.Texto = ""
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(6, 24)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(190, 256)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(129, 12)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "- Contraseña temporal (12345)"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(6, 50)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(47, 13)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Text = "Nombre:"
        '
        'lklReestablecerContraseña
        '
        Me.lklReestablecerContraseña.AutoSize = True
        Me.lklReestablecerContraseña.Location = New System.Drawing.Point(190, 243)
        Me.lklReestablecerContraseña.Name = "lklReestablecerContraseña"
        Me.lklReestablecerContraseña.Size = New System.Drawing.Size(127, 13)
        Me.lklReestablecerContraseña.TabIndex = 19
        Me.lklReestablecerContraseña.TabStop = True
        Me.lklReestablecerContraseña.Text = "Reestablecer Contraseña"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(6, 210)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 16
        Me.lblEstado.Text = "Estado:"
        '
        'txtUsuario
        '
        Me.txtUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUsuario.Color = System.Drawing.Color.Empty
        Me.txtUsuario.Indicaciones = Nothing
        Me.txtUsuario.Location = New System.Drawing.Point(84, 73)
        Me.txtUsuario.Multilinea = False
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(112, 21)
        Me.txtUsuario.SoloLectura = False
        Me.txtUsuario.TabIndex = 5
        Me.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUsuario.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(84, 19)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 1
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblUsuario
        '
        Me.lblUsuario.AutoSize = True
        Me.lblUsuario.Location = New System.Drawing.Point(6, 77)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuario.TabIndex = 4
        Me.lblUsuario.Text = "Usuario:"
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(83, 208)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 17
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'cbxPerfil
        '
        Me.cbxPerfil.CampoWhere = Nothing
        Me.cbxPerfil.CargarUnaSolaVez = False
        Me.cbxPerfil.DataDisplayMember = "Descripcion"
        Me.cbxPerfil.DataFilter = Nothing
        Me.cbxPerfil.DataOrderBy = "Descripcion"
        Me.cbxPerfil.DataSource = "VPerfil"
        Me.cbxPerfil.DataValueMember = "ID"
        Me.cbxPerfil.FormABM = Nothing
        Me.cbxPerfil.Indicaciones = Nothing
        Me.cbxPerfil.Location = New System.Drawing.Point(84, 100)
        Me.cbxPerfil.Name = "cbxPerfil"
        Me.cbxPerfil.SeleccionObligatoria = True
        Me.cbxPerfil.Size = New System.Drawing.Size(237, 21)
        Me.cbxPerfil.SoloLectura = False
        Me.cbxPerfil.TabIndex = 9
        Me.cbxPerfil.Texto = ""
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(144, 208)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 18
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'lblPerfil
        '
        Me.lblPerfil.AutoSize = True
        Me.lblPerfil.Location = New System.Drawing.Point(6, 104)
        Me.lblPerfil.Name = "lblPerfil"
        Me.lblPerfil.Size = New System.Drawing.Size(33, 13)
        Me.lblPerfil.TabIndex = 8
        Me.lblPerfil.Text = "Perfil:"
        '
        'txtNombre
        '
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.Color = System.Drawing.Color.Empty
        Me.txtNombre.Indicaciones = Nothing
        Me.txtNombre.Location = New System.Drawing.Point(84, 46)
        Me.txtNombre.Multilinea = False
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(237, 21)
        Me.txtNombre.SoloLectura = False
        Me.txtNombre.TabIndex = 3
        Me.txtNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNombre.Texto = ""
        '
        'txtIdentificador
        '
        Me.txtIdentificador.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdentificador.Color = System.Drawing.Color.Empty
        Me.txtIdentificador.Indicaciones = Nothing
        Me.txtIdentificador.Location = New System.Drawing.Point(285, 73)
        Me.txtIdentificador.Multilinea = False
        Me.txtIdentificador.Name = "txtIdentificador"
        Me.txtIdentificador.Size = New System.Drawing.Size(36, 21)
        Me.txtIdentificador.SoloLectura = False
        Me.txtIdentificador.TabIndex = 7
        Me.txtIdentificador.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtIdentificador.Texto = ""
        '
        'lblIdentificador
        '
        Me.lblIdentificador.AutoSize = True
        Me.lblIdentificador.Location = New System.Drawing.Point(207, 77)
        Me.lblIdentificador.Name = "lblIdentificador"
        Me.lblIdentificador.Size = New System.Drawing.Size(68, 13)
        Me.lblIdentificador.TabIndex = 6
        Me.lblIdentificador.Text = "Identificador:"
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.btnSalir)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnGuardar)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(504, 415)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(328, 31)
        Me.FlowLayoutPanel2.TabIndex = 4
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Location = New System.Drawing.Point(250, 3)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 2
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.ImageIndex = 4
        Me.btnCancelar.ImageList = Me.ImageList1
        Me.btnCancelar.Location = New System.Drawing.Point(154, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(90, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGuardar.ImageIndex = 3
        Me.btnGuardar.ImageList = Me.ImageList1
        Me.btnGuardar.Location = New System.Drawing.Point(58, 3)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(90, 23)
        Me.btnGuardar.TabIndex = 0
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'frmUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(835, 471)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmUsuario"
        Me.Tag = "USUARIO"
        Me.Text = "frmUsuario"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtBuscar As ERP.ocxTXTString
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As ERP.ocxTXTNumeric
    Friend WithEvents dgvLista As System.Windows.Forms.DataGridView
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents lklReestablecerContraseña As System.Windows.Forms.LinkLabel
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents txtUsuario As ERP.ocxTXTString
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblUsuario As System.Windows.Forms.Label
    Friend WithEvents rdbActivo As System.Windows.Forms.RadioButton
    Friend WithEvents cbxPerfil As ERP.ocxCBX
    Friend WithEvents rdbDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents lblPerfil As System.Windows.Forms.Label
    Friend WithEvents txtNombre As ERP.ocxTXTString
    Friend WithEvents txtIdentificador As ERP.ocxTXTString
    Friend WithEvents lblIdentificador As System.Windows.Forms.Label
    Friend WithEvents txtEmail As ERP.ocxTXTString
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents chkChofer As ERP.ocxCHK
    Friend WithEvents cbxChofer As ERP.ocxCBX
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCambiarUsuarioContraseña
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblUsuario = New System.Windows.Forms.Label()
        Me.lblContraseñaActual = New System.Windows.Forms.Label()
        Me.txtUsuario = New ERP.ocxTXTString()
        Me.txtIdentificador = New ERP.ocxTXTString()
        Me.txtContraseñaActual = New ERP.ocxTXTString()
        Me.txtContraseñaNueva = New ERP.ocxTXTString()
        Me.lblContraseñaNueva = New System.Windows.Forms.Label()
        Me.txtRepetirContraseña = New ERP.ocxTXTString()
        Me.lblRepetirContraseña = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblUsuario
        '
        Me.lblUsuario.AutoSize = True
        Me.lblUsuario.Location = New System.Drawing.Point(20, 21)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuario.TabIndex = 0
        Me.lblUsuario.Text = "Usuario:"
        '
        'lblContraseñaActual
        '
        Me.lblContraseñaActual.AutoSize = True
        Me.lblContraseñaActual.Location = New System.Drawing.Point(20, 48)
        Me.lblContraseñaActual.Name = "lblContraseñaActual"
        Me.lblContraseñaActual.Size = New System.Drawing.Size(97, 13)
        Me.lblContraseñaActual.TabIndex = 3
        Me.lblContraseñaActual.Text = "Contraseña Actual:"
        '
        'txtUsuario
        '
        Me.txtUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUsuario.Color = System.Drawing.Color.Empty
        Me.txtUsuario.Indicaciones = Nothing
        Me.txtUsuario.Location = New System.Drawing.Point(123, 17)
        Me.txtUsuario.Multilinea = False
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(122, 21)
        Me.txtUsuario.SoloLectura = True
        Me.txtUsuario.TabIndex = 1
        Me.txtUsuario.TabStop = False
        Me.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUsuario.Texto = ""
        '
        'txtIdentificador
        '
        Me.txtIdentificador.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdentificador.Color = System.Drawing.Color.Empty
        Me.txtIdentificador.Indicaciones = Nothing
        Me.txtIdentificador.Location = New System.Drawing.Point(245, 17)
        Me.txtIdentificador.Multilinea = False
        Me.txtIdentificador.Name = "txtIdentificador"
        Me.txtIdentificador.Size = New System.Drawing.Size(43, 21)
        Me.txtIdentificador.SoloLectura = True
        Me.txtIdentificador.TabIndex = 2
        Me.txtIdentificador.TabStop = False
        Me.txtIdentificador.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtIdentificador.Texto = ""
        '
        'txtContraseñaActual
        '
        Me.txtContraseñaActual.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtContraseñaActual.Color = System.Drawing.Color.Empty
        Me.txtContraseñaActual.Indicaciones = Nothing
        Me.txtContraseñaActual.Location = New System.Drawing.Point(123, 44)
        Me.txtContraseñaActual.Multilinea = False
        Me.txtContraseñaActual.Name = "txtContraseñaActual"
        Me.txtContraseñaActual.Size = New System.Drawing.Size(165, 21)
        Me.txtContraseñaActual.SoloLectura = False
        Me.txtContraseñaActual.TabIndex = 4
        Me.txtContraseñaActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtContraseñaActual.Texto = ""
        '
        'txtContraseñaNueva
        '
        Me.txtContraseñaNueva.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtContraseñaNueva.Color = System.Drawing.Color.Empty
        Me.txtContraseñaNueva.Indicaciones = Nothing
        Me.txtContraseñaNueva.Location = New System.Drawing.Point(123, 71)
        Me.txtContraseñaNueva.Multilinea = False
        Me.txtContraseñaNueva.Name = "txtContraseñaNueva"
        Me.txtContraseñaNueva.Size = New System.Drawing.Size(165, 21)
        Me.txtContraseñaNueva.SoloLectura = False
        Me.txtContraseñaNueva.TabIndex = 6
        Me.txtContraseñaNueva.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtContraseñaNueva.Texto = ""
        '
        'lblContraseñaNueva
        '
        Me.lblContraseñaNueva.AutoSize = True
        Me.lblContraseñaNueva.Location = New System.Drawing.Point(20, 75)
        Me.lblContraseñaNueva.Name = "lblContraseñaNueva"
        Me.lblContraseñaNueva.Size = New System.Drawing.Size(99, 13)
        Me.lblContraseñaNueva.TabIndex = 5
        Me.lblContraseñaNueva.Text = "Contraseña Nueva:"
        '
        'txtRepetirContraseña
        '
        Me.txtRepetirContraseña.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRepetirContraseña.Color = System.Drawing.Color.Empty
        Me.txtRepetirContraseña.Indicaciones = Nothing
        Me.txtRepetirContraseña.Location = New System.Drawing.Point(123, 98)
        Me.txtRepetirContraseña.Multilinea = False
        Me.txtRepetirContraseña.Name = "txtRepetirContraseña"
        Me.txtRepetirContraseña.Size = New System.Drawing.Size(165, 21)
        Me.txtRepetirContraseña.SoloLectura = False
        Me.txtRepetirContraseña.TabIndex = 8
        Me.txtRepetirContraseña.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRepetirContraseña.Texto = ""
        '
        'lblRepetirContraseña
        '
        Me.lblRepetirContraseña.AutoSize = True
        Me.lblRepetirContraseña.Location = New System.Drawing.Point(20, 102)
        Me.lblRepetirContraseña.Name = "lblRepetirContraseña"
        Me.lblRepetirContraseña.Size = New System.Drawing.Size(100, 13)
        Me.lblRepetirContraseña.TabIndex = 7
        Me.lblRepetirContraseña.Text = "Repetir contraseña:"
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(123, 134)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 9
        Me.btnGuardar.Text = "Cambiar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(213, 134)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 10
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'frmCambiarUsuarioContraseña
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(309, 170)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.txtRepetirContraseña)
        Me.Controls.Add(Me.lblRepetirContraseña)
        Me.Controls.Add(Me.txtContraseñaNueva)
        Me.Controls.Add(Me.lblContraseñaNueva)
        Me.Controls.Add(Me.txtContraseñaActual)
        Me.Controls.Add(Me.txtIdentificador)
        Me.Controls.Add(Me.txtUsuario)
        Me.Controls.Add(Me.lblContraseñaActual)
        Me.Controls.Add(Me.lblUsuario)
        Me.KeyPreview = True
        Me.Name = "frmCambiarUsuarioContraseña"
        Me.Tag = "CARMBIAR CONTRASEÑA"
        Me.Text = "frmCambiarUsuarioContraseña"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblUsuario As System.Windows.Forms.Label
    Friend WithEvents lblContraseñaActual As System.Windows.Forms.Label
    Friend WithEvents txtUsuario As ERP.ocxTXTString
    Friend WithEvents txtIdentificador As ERP.ocxTXTString
    Friend WithEvents txtContraseñaActual As ERP.ocxTXTString
    Friend WithEvents txtContraseñaNueva As ERP.ocxTXTString
    Friend WithEvents lblContraseñaNueva As System.Windows.Forms.Label
    Friend WithEvents txtRepetirContraseña As ERP.ocxTXTString
    Friend WithEvents lblRepetirContraseña As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccesoTerminal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxLicencia = New System.Windows.Forms.GroupBox()
        Me.txtCantidadActual = New ERP.ocxTXTString()
        Me.lblCantidadActual = New System.Windows.Forms.Label()
        Me.txtCantidadMaxima = New ERP.ocxTXTString()
        Me.lblCantidadMaxima = New System.Windows.Forms.Label()
        Me.txtRUC = New ERP.ocxTXTString()
        Me.lblRUC = New System.Windows.Forms.Label()
        Me.txtEmpresa = New ERP.ocxTXTString()
        Me.lblEmpresa = New System.Windows.Forms.Label()
        Me.gbxTerminal = New System.Windows.Forms.GroupBox()
        Me.lklAdministrarDeposito = New System.Windows.Forms.LinkLabel()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.lblDeposito = New System.Windows.Forms.Label()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.txtKEY = New ERP.ocxTXTString()
        Me.lblKEY = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTString()
        Me.lblID = New System.Windows.Forms.Label()
        Me.gbxUsuario = New System.Windows.Forms.GroupBox()
        Me.txtContraseña = New ERP.ocxTXTString()
        Me.lblContraseña = New System.Windows.Forms.Label()
        Me.txtUsuario = New ERP.ocxTXTString()
        Me.lblUsuario = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.gbxLicencia.SuspendLayout()
        Me.gbxTerminal.SuspendLayout()
        Me.gbxUsuario.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxLicencia
        '
        Me.gbxLicencia.Controls.Add(Me.txtCantidadActual)
        Me.gbxLicencia.Controls.Add(Me.lblCantidadActual)
        Me.gbxLicencia.Controls.Add(Me.txtCantidadMaxima)
        Me.gbxLicencia.Controls.Add(Me.lblCantidadMaxima)
        Me.gbxLicencia.Controls.Add(Me.txtRUC)
        Me.gbxLicencia.Controls.Add(Me.lblRUC)
        Me.gbxLicencia.Controls.Add(Me.txtEmpresa)
        Me.gbxLicencia.Controls.Add(Me.lblEmpresa)
        Me.gbxLicencia.Location = New System.Drawing.Point(12, 12)
        Me.gbxLicencia.Name = "gbxLicencia"
        Me.gbxLicencia.Size = New System.Drawing.Size(367, 105)
        Me.gbxLicencia.TabIndex = 0
        Me.gbxLicencia.TabStop = False
        Me.gbxLicencia.Text = "LICENCIA"
        '
        'txtCantidadActual
        '
        Me.txtCantidadActual.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCantidadActual.Color = System.Drawing.Color.Empty
        Me.txtCantidadActual.Indicaciones = Nothing
        Me.txtCantidadActual.Location = New System.Drawing.Point(224, 71)
        Me.txtCantidadActual.Multilinea = False
        Me.txtCantidadActual.Name = "txtCantidadActual"
        Me.txtCantidadActual.Size = New System.Drawing.Size(51, 21)
        Me.txtCantidadActual.SoloLectura = True
        Me.txtCantidadActual.TabIndex = 7
        Me.txtCantidadActual.TabStop = False
        Me.txtCantidadActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCantidadActual.Texto = ""
        '
        'lblCantidadActual
        '
        Me.lblCantidadActual.AutoSize = True
        Me.lblCantidadActual.Location = New System.Drawing.Point(150, 75)
        Me.lblCantidadActual.Name = "lblCantidadActual"
        Me.lblCantidadActual.Size = New System.Drawing.Size(68, 13)
        Me.lblCantidadActual.TabIndex = 6
        Me.lblCantidadActual.Text = "Cant. Actual:"
        '
        'txtCantidadMaxima
        '
        Me.txtCantidadMaxima.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCantidadMaxima.Color = System.Drawing.Color.Empty
        Me.txtCantidadMaxima.Indicaciones = Nothing
        Me.txtCantidadMaxima.Location = New System.Drawing.Point(93, 71)
        Me.txtCantidadMaxima.Multilinea = False
        Me.txtCantidadMaxima.Name = "txtCantidadMaxima"
        Me.txtCantidadMaxima.Size = New System.Drawing.Size(51, 21)
        Me.txtCantidadMaxima.SoloLectura = True
        Me.txtCantidadMaxima.TabIndex = 5
        Me.txtCantidadMaxima.TabStop = False
        Me.txtCantidadMaxima.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCantidadMaxima.Texto = ""
        '
        'lblCantidadMaxima
        '
        Me.lblCantidadMaxima.AutoSize = True
        Me.lblCantidadMaxima.Location = New System.Drawing.Point(27, 75)
        Me.lblCantidadMaxima.Name = "lblCantidadMaxima"
        Me.lblCantidadMaxima.Size = New System.Drawing.Size(58, 13)
        Me.lblCantidadMaxima.TabIndex = 4
        Me.lblCantidadMaxima.Text = "Cant. Max:"
        '
        'txtRUC
        '
        Me.txtRUC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRUC.Color = System.Drawing.Color.Empty
        Me.txtRUC.Indicaciones = Nothing
        Me.txtRUC.Location = New System.Drawing.Point(93, 46)
        Me.txtRUC.Multilinea = False
        Me.txtRUC.Name = "txtRUC"
        Me.txtRUC.Size = New System.Drawing.Size(182, 21)
        Me.txtRUC.SoloLectura = True
        Me.txtRUC.TabIndex = 3
        Me.txtRUC.TabStop = False
        Me.txtRUC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRUC.Texto = ""
        '
        'lblRUC
        '
        Me.lblRUC.AutoSize = True
        Me.lblRUC.Location = New System.Drawing.Point(52, 50)
        Me.lblRUC.Name = "lblRUC"
        Me.lblRUC.Size = New System.Drawing.Size(33, 13)
        Me.lblRUC.TabIndex = 2
        Me.lblRUC.Text = "RUC:"
        '
        'txtEmpresa
        '
        Me.txtEmpresa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmpresa.Color = System.Drawing.Color.Empty
        Me.txtEmpresa.Indicaciones = Nothing
        Me.txtEmpresa.Location = New System.Drawing.Point(93, 19)
        Me.txtEmpresa.Multilinea = False
        Me.txtEmpresa.Name = "txtEmpresa"
        Me.txtEmpresa.Size = New System.Drawing.Size(263, 21)
        Me.txtEmpresa.SoloLectura = True
        Me.txtEmpresa.TabIndex = 1
        Me.txtEmpresa.TabStop = False
        Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEmpresa.Texto = ""
        '
        'lblEmpresa
        '
        Me.lblEmpresa.AutoSize = True
        Me.lblEmpresa.Location = New System.Drawing.Point(34, 23)
        Me.lblEmpresa.Name = "lblEmpresa"
        Me.lblEmpresa.Size = New System.Drawing.Size(51, 13)
        Me.lblEmpresa.TabIndex = 0
        Me.lblEmpresa.Text = "Empresa:"
        '
        'gbxTerminal
        '
        Me.gbxTerminal.Controls.Add(Me.lklAdministrarDeposito)
        Me.gbxTerminal.Controls.Add(Me.cbxDeposito)
        Me.gbxTerminal.Controls.Add(Me.lblDeposito)
        Me.gbxTerminal.Controls.Add(Me.txtDescripcion)
        Me.gbxTerminal.Controls.Add(Me.lblDescripcion)
        Me.gbxTerminal.Controls.Add(Me.txtKEY)
        Me.gbxTerminal.Controls.Add(Me.lblKEY)
        Me.gbxTerminal.Controls.Add(Me.txtID)
        Me.gbxTerminal.Controls.Add(Me.lblID)
        Me.gbxTerminal.Location = New System.Drawing.Point(12, 136)
        Me.gbxTerminal.Name = "gbxTerminal"
        Me.gbxTerminal.Size = New System.Drawing.Size(367, 132)
        Me.gbxTerminal.TabIndex = 1
        Me.gbxTerminal.TabStop = False
        Me.gbxTerminal.Text = "TERMINAL"
        '
        'lklAdministrarDeposito
        '
        Me.lklAdministrarDeposito.AutoSize = True
        Me.lklAdministrarDeposito.Location = New System.Drawing.Point(289, 102)
        Me.lklAdministrarDeposito.Name = "lklAdministrarDeposito"
        Me.lklAdministrarDeposito.Size = New System.Drawing.Size(67, 13)
        Me.lklAdministrarDeposito.TabIndex = 8
        Me.lklAdministrarDeposito.TabStop = True
        Me.lklAdministrarDeposito.Text = "+ Administrar"
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = Nothing
        Me.cbxDeposito.DataDisplayMember = Nothing
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = Nothing
        Me.cbxDeposito.DataSource = Nothing
        Me.cbxDeposito.DataValueMember = Nothing
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(93, 98)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(190, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 7
        Me.cbxDeposito.Texto = ""
        '
        'lblDeposito
        '
        Me.lblDeposito.AutoSize = True
        Me.lblDeposito.Location = New System.Drawing.Point(27, 102)
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(58, 13)
        Me.lblDeposito.TabIndex = 6
        Me.lblDeposito.Text = "Suc - Dep:"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(93, 71)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(263, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 5
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(19, 75)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 4
        Me.lblDescripcion.Text = "Descripcion:"
        '
        'txtKEY
        '
        Me.txtKEY.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKEY.Color = System.Drawing.Color.Empty
        Me.txtKEY.Indicaciones = Nothing
        Me.txtKEY.Location = New System.Drawing.Point(93, 46)
        Me.txtKEY.Multilinea = False
        Me.txtKEY.Name = "txtKEY"
        Me.txtKEY.Size = New System.Drawing.Size(263, 21)
        Me.txtKEY.SoloLectura = True
        Me.txtKEY.TabIndex = 3
        Me.txtKEY.TabStop = False
        Me.txtKEY.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtKEY.Texto = ""
        '
        'lblKEY
        '
        Me.lblKEY.AutoSize = True
        Me.lblKEY.Location = New System.Drawing.Point(54, 50)
        Me.lblKEY.Name = "lblKEY"
        Me.lblKEY.Size = New System.Drawing.Size(31, 13)
        Me.lblKEY.TabIndex = 2
        Me.lblKEY.Text = "KEY:"
        '
        'txtID
        '
        Me.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(93, 19)
        Me.txtID.Multilinea = False
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(51, 21)
        Me.txtID.SoloLectura = True
        Me.txtID.TabIndex = 1
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtID.Texto = ""
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(64, 23)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'gbxUsuario
        '
        Me.gbxUsuario.Controls.Add(Me.txtContraseña)
        Me.gbxUsuario.Controls.Add(Me.lblContraseña)
        Me.gbxUsuario.Controls.Add(Me.txtUsuario)
        Me.gbxUsuario.Controls.Add(Me.lblUsuario)
        Me.gbxUsuario.Location = New System.Drawing.Point(12, 274)
        Me.gbxUsuario.Name = "gbxUsuario"
        Me.gbxUsuario.Size = New System.Drawing.Size(367, 84)
        Me.gbxUsuario.TabIndex = 2
        Me.gbxUsuario.TabStop = False
        Me.gbxUsuario.Text = "USUARIO"
        '
        'txtContraseña
        '
        Me.txtContraseña.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtContraseña.Color = System.Drawing.Color.Empty
        Me.txtContraseña.Indicaciones = Nothing
        Me.txtContraseña.Location = New System.Drawing.Point(93, 50)
        Me.txtContraseña.Multilinea = False
        Me.txtContraseña.Name = "txtContraseña"
        Me.txtContraseña.Size = New System.Drawing.Size(263, 21)
        Me.txtContraseña.SoloLectura = False
        Me.txtContraseña.TabIndex = 3
        Me.txtContraseña.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtContraseña.Texto = ""
        '
        'lblContraseña
        '
        Me.lblContraseña.AutoSize = True
        Me.lblContraseña.Location = New System.Drawing.Point(21, 54)
        Me.lblContraseña.Name = "lblContraseña"
        Me.lblContraseña.Size = New System.Drawing.Size(64, 13)
        Me.lblContraseña.TabIndex = 2
        Me.lblContraseña.Text = "Contraseña:"
        '
        'txtUsuario
        '
        Me.txtUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUsuario.Color = System.Drawing.Color.Empty
        Me.txtUsuario.Indicaciones = Nothing
        Me.txtUsuario.Location = New System.Drawing.Point(93, 23)
        Me.txtUsuario.Multilinea = False
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(263, 21)
        Me.txtUsuario.SoloLectura = False
        Me.txtUsuario.TabIndex = 1
        Me.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUsuario.Texto = ""
        '
        'lblUsuario
        '
        Me.lblUsuario.AutoSize = True
        Me.lblUsuario.Location = New System.Drawing.Point(39, 27)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuario.TabIndex = 0
        Me.lblUsuario.Text = "Usuario:"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(203, 368)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 3
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(293, 368)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 4
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'frmAccesoTerminal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(389, 406)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.gbxUsuario)
        Me.Controls.Add(Me.gbxTerminal)
        Me.Controls.Add(Me.gbxLicencia)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmAccesoTerminal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmAccesoTerminal"
        Me.gbxLicencia.ResumeLayout(False)
        Me.gbxLicencia.PerformLayout()
        Me.gbxTerminal.ResumeLayout(False)
        Me.gbxTerminal.PerformLayout()
        Me.gbxUsuario.ResumeLayout(False)
        Me.gbxUsuario.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxLicencia As System.Windows.Forms.GroupBox
    Friend WithEvents txtCantidadActual As ERP.ocxTXTString
    Friend WithEvents lblCantidadActual As System.Windows.Forms.Label
    Friend WithEvents txtCantidadMaxima As ERP.ocxTXTString
    Friend WithEvents lblCantidadMaxima As System.Windows.Forms.Label
    Friend WithEvents txtRUC As ERP.ocxTXTString
    Friend WithEvents lblRUC As System.Windows.Forms.Label
    Friend WithEvents txtEmpresa As ERP.ocxTXTString
    Friend WithEvents lblEmpresa As System.Windows.Forms.Label
    Friend WithEvents gbxTerminal As System.Windows.Forms.GroupBox
    Friend WithEvents lklAdministrarDeposito As System.Windows.Forms.LinkLabel
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents lblDeposito As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtKEY As ERP.ocxTXTString
    Friend WithEvents lblKEY As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTString
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents gbxUsuario As System.Windows.Forms.GroupBox
    Friend WithEvents txtContraseña As ERP.ocxTXTString
    Friend WithEvents lblContraseña As System.Windows.Forms.Label
    Friend WithEvents txtUsuario As ERP.ocxTXTString
    Friend WithEvents lblUsuario As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
End Class

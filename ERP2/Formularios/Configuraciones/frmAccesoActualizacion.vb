﻿Public Class frmAccesoActualizacion
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDecode As New ERP2ED.CDecode

    'FUNCIONES
    Sub Inicializar()

        Me.KeyPreview = True
        Me.AcceptButton = New Button

        txtContraseña.txt.UseSystemPasswordChar = True
        txtContraseña.txt.PasswordChar = "*"

        CargarDatos()

    End Sub

    Sub CargarDatos()

        'Actualizacion
        vgActualizacionPath = CArchivoInicio.IniGet(VGArchivoINI, "ACTUALIZACION", "PATH", "ftp://sain.expressalimentos.com.py")
        vgActualizacionUsuario = CArchivoInicio.IniGet(VGArchivoINI, "ACTUALIZACION", "USUARIO", "sain@expressalimentos.com.py")
        CDecode.InClearText = CArchivoInicio.IniGet(VGArchivoINI, "ACTUALIZACION", "PASSWORD", "")

        CDecode.Decrypt()
        vgActualizacionPassword = CDecode.CryptedText

        txtPath.SetValue(vgActualizacionPath)
        txtUsuario.SetValue(vgActualizacionUsuario)
        txtContraseña.SetValue(vgActualizacionPassword)

    End Sub

    Sub Guardar()

        vgActualizacionPassword = txtContraseña.GetValue
        vgActualizacionPath = txtPath.Texto
        vgActualizacionUsuario = txtUsuario.Texto

        CArchivoInicio.IniWrite(VGArchivoINI, "ACTUALIZACION", "PATH", vgActualizacionPath)
        CArchivoInicio.IniWrite(VGArchivoINI, "ACTUALIZACION", "USUARIO", vgActualizacionUsuario)

        CDecode.InClearText = vgActualizacionPassword
        CDecode.Encrypt()
        CArchivoInicio.IniWrite(VGArchivoINI, "ACTUALIZACION", "PASSWORD", CDecode.CryptedText)

        Me.Close()

    End Sub

    Private Sub frmAccesoActualizacion_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmAccesoActualizacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

    'FA 28/05/2021
    Sub frmAccesoActualizacion_Activate()
        Me.Refresh()
    End Sub

End Class
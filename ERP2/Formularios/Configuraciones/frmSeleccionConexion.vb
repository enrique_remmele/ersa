﻿Public Class frmSeleccionConexion

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Private ConexionValue As String
    Public Property Conexion() As String
        Get
            Return ConexionValue
        End Get
        Set(ByVal value As String)
            ConexionValue = value
        End Set
    End Property

    'FUNCONES
    Sub Inicializar()

        'Form
        Me.Text = "Base de Datos"
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        DataGridView1.DataSource = vgSAIN.Tables("Conexion")
        DataGridView1.RowHeadersVisible = False

        For c As Integer = 0 To DataGridView1.ColumnCount - 1
            DataGridView1.Columns(c).Visible = False
        Next

        DataGridView1.Columns("Descripcion").Visible = True
        DataGridView1.Columns("Servidor").Visible = True

        DataGridView1.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        DataGridView1.Columns("Servidor").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub Seleccionar()

        'Validar
        If DataGridView1.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        CArchivoInicio.IniWrite(VGArchivoINI, "CONEXION", "TIPO", DataGridView1.SelectedRows(0).Cells("Descripcion").Value)
        Conexion = DataGridView1.SelectedRows(0).Cells("Descripcion").Value
        Procesado = True

        Me.Close()

    End Sub

    Private Sub frmSeleccionConexion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

    End Sub

    Private Sub frmSeleccionConexion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        Seleccionar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Procesado = False
        Me.Close()
    End Sub

    'FA 28/05/2021
    Sub frmSeleccionConexion_Activate()
        Me.Refresh()
    End Sub

End Class
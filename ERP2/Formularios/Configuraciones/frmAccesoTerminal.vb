﻿Public Class frmAccesoTerminal

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CInfoHardware As New CInfoHardware
    Dim CDecode As New ERP2ED.CDecode

    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Private EsconderAdministrarSucursalValue As Boolean
    Public Property EsconderAdministrarSucursal() As Boolean
        Get
            Return EsconderAdministrarSucursalValue
        End Get
        Set(ByVal value As Boolean)
            EsconderAdministrarSucursalValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        Dim Licencia As New CLicencia

        Licencia.InfoLicencia()

        txtUsuario.txt.CharacterCasing = CharacterCasing.Normal
        txtContraseña.txt.CharacterCasing = CharacterCasing.Normal
        txtContraseña.txt.UseSystemPasswordChar = True
        txtContraseña.txt.PasswordChar = "*"

        'TextBox
        'Informacion de la Licencia
        Me.txtCantidadActual.txt.Text = CType(CSistema.ExecuteScalar("Select Count(*) From VTerminal", CadenaConexion), Integer)
        Me.txtCantidadMaxima.txt.Text = vgLicenciaOtorgadoTerminales
        Me.txtEmpresa.txt.Text = vgLicenciaOtorgadoRazonSocial
        Me.txtRUC.txt.Text = vgLicenciaOtorgadoRUC

        'Informacion de la terminal
        Me.txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select Max(ID)+1 From VTerminal), '1')", CadenaConexion), Integer)
        Me.txtDescripcion.txt.Text = CInfoHardware.NAME
        Me.txtKEY.txt.Text = CInfoHardware.CODIGO

        'Inicio de Sesion
        Me.txtContraseña.txt.Clear()
        Me.txtUsuario.txt.Clear()

        If EsconderAdministrarSucursal = True Then
            lklAdministrarDeposito.Visible = False
        End If

        CargarSucDep()
    End Sub

    Private Sub CargarSucDep()
        Try
            CSistema.SqlToComboBox(cbxDeposito, "Select ID, 'Deposito'=[Suc-Dep] From VDeposito Order By 2", CadenaConexion)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    'Grabar en Licencia
    Function GrabarEnLicencia() As Boolean

        'Validar
        If CInt(txtCantidadMaxima.txt.Text) > 0 Then
            'Cantidad de Terminales Permitidas
            If CInt(txtCantidadActual.txt.Text) >= CInt(txtCantidadMaxima.txt.Text) Then
                MessageBox.Show("No se puede agregar mas terminales! El cupo de terminales habilitadas en la licencia ha sido superada.", "ADCON", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Me.Close()
            End If
        End If

        'Hardware Key
        If txtKEY.txt.Text.Trim.Length = 0 Then
            MessageBox.Show("El sistema no puede leer la informacion de la PC!", "KEY", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Me.Close()
        End If

        'Descripcion
        If txtDescripcion.txt.Text.Trim.Length = 0 Then
            MessageBox.Show("Introduzca una descripcion para la terminal!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        'Seleccion de deposito
        If cbxDeposito.Validar = False Then
            MessageBox.Show("Seleccione correctamente el deposito!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        'Inicio de Sesion
        If (txtUsuario.txt.Text.Trim <> vgLicenciaOtorgadoAdmin) Or (txtContraseña.txt.Text.Trim <> vgLicenciaOtorgadoPassword) Then
            MessageBox.Show("El inicio de sesion es incorrecto!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        'Cargar
        Dim CLicencia As New CLicencia
        CLicencia.InfoLicencia()

        Dim sContent As String = vbNullString
        sContent = My.Computer.FileSystem.ReadAllText(vgLicenciaPath)
        CDecode.InClearText = sContent
        CDecode.Decrypt()

        sContent = CDecode.CryptedText

        Dim values As String = CLicencia.dtInformacion.Rows(0)("Terminales")
        Dim TERMINALES() As String = values.Split("#")

        'Verificar que no exista
        Dim PrimeraTerminal As Boolean = True

        'Vverificar que la terminal no este dentro de la licencia
        For i As Integer = 0 To TERMINALES.GetLength(0) - 1
            If TERMINALES(i) = CInfoHardware.CODIGO Then
                Return True
            End If

            PrimeraTerminal = False

        Next

        'Anexar
        If PrimeraTerminal = True Then
            sContent = sContent & CInfoHardware.CODIGO
        Else
            sContent = sContent & "#" & CInfoHardware.CODIGO
        End If


        'Volver a Encriptar todo
        CDecode.InClearText = sContent
        CDecode.Encrypt()
        My.Computer.FileSystem.WriteAllText(vgLicenciaPath, CDecode.CryptedText, False)

        Return True

    End Function

    'Insertar en Base de Datos
    Sub Procesar()

        If GrabarEnLicencia() = False Then
            Exit Sub
        End If

        'Consultar
        If CType(CSistema.ExecuteScalar("Select IsNull((Select CodigoActivacion From VTerminal Where CodigoActivacion='" & txtKEY.txt.Text.Trim & "'), '')", CadenaConexion), String) = txtKEY.txt.Text.Trim Then
            MessageBox.Show("Registro procesado!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()
            Exit Sub
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", cbxDeposito.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Impresora", "", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CodigoActivacion", txtKEY.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpTerminal", False, True, MensajeRetorno, "", False, CadenaConexion) = True Then
            Procesado = True
            Me.Close()
        End If


    End Sub

    Private Sub frmAccesoTerminal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Procesar()
    End Sub

    Private Sub lklAdministrarDeposito_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklAdministrarDeposito.LinkClicked
        Dim frm As New frmSucursalDeposito
        frm.tbcGeneral.TabPages(0).Hide()
        frm.ShowDialog()
        'CSistema.SqlToComboBox(cbxDeposito, "Select ID, 'Deposito'=[Suc-Dep] From VDeposito Order By 2", CadenaConexion)
        CargarSucDep()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    'FA 28/05/2021
    Sub frmAccesoTerminal_Activate()
        Me.Refresh()
    End Sub

End Class
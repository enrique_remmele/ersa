﻿Public Class frmAdministrarCaja

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Property IDOperacion As Integer

    'VARIABLES
    Dim vdtDocumentos As New DataTable

    'FUNCIONES
    Sub Inicializar()

        'Funciones
        CargarInformacion()
        IDOperacion = CSistema.ObtenerIDOperacion(frmAperturaCaja.Name, "CAJA", "CAJ")

    End Sub

    Sub CargarInformacion()

         'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Codigo   From Sucursal Order By 2")

        Try
            cbxSucursal.SelectedValue(vgIDSucursal)

        Catch ex As Exception

        End Try

    End Sub

    Sub GuardarInformacion()

    End Sub

    Sub ListarComprobantes(Optional ByVal NroCaja As Integer = 0, Optional ByVal IDTipoComprobante As Integer = 0, Optional ByVal IDSucursal As Integer = 0)

        Dim Consulta As String = "Select IDTransaccion, 'Suc'=CodigoSucursal, Numero, Fecha, 'Estado'=Case When (Habilitado) = 'False' Then 'CERRADO' Else 'HABILITADO' End, 'Cierre'=FechaCierre, 'Usuario'=UsuarioCierre, Observacion From VCaja "
        Dim Where As String = ""
        Dim OrderBy As String = " Order By 2"

        'Si es por comprobante
        If NroCaja > 0 Then

            Where = " Where IDSucursal=" & cbxSucursal.GetValue & " And Numero = '" & NroCaja & "' "

        End If

        'Si es normal
        If Where = "" Then

            Where = " Where IDSucursal=" & cbxSucursal.GetValue & " And Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' "

        End If

        'Listar 
        vdtDocumentos = CSistema.ExecuteToDataTable(Consulta & " " & Where & " " & OrderBy)
        DataGridView1.DataSource = vdtDocumentos

        If DataGridView1.ColumnCount = 0 Then
            Exit Sub
        End If

        'Formato
        DataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(vgColorDataGridAlternancia)
        'DataGridView1.Columns("Anulado").Visible = False
        DataGridView1.Columns("IDTransaccion").Visible = False


        For c As Integer = 0 To DataGridView1.ColumnCount - 1
            DataGridView1.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Next

        DataGridView1.Columns("Suc").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns("Fecha").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns("Estado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns("Observacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        txtCantidad.SetValue(DataGridView1.Rows.Count)

    End Sub

    Sub AperturaCaja()

        Dim frm As New frmAperturaCaja
        FGMostrarFormulario(Me, frm, "Apertura", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Sub CerrarCaja()

        'Validar
        If DataGridView1.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        If MessageBox.Show("Desea realizar el cierre de la caja?", "Cerrar caja", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        If Me.DataGridView1.SelectedRows(0).Cells("Estado").Value = "CERRADO" Then
            MessageBox.Show("El registro ya esta cerrado! Seleccione otro registro.", "Cerrar caja", MessageBoxButtons.OK, MessageBoxIcon.Question)
            Exit Sub
        End If

        Dim NroCaja As Integer = Me.DataGridView1.SelectedRows(0).Cells("Numero").Value


        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@Numero", NroCaja, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "CERRAR", ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpCaja", False, False, MensajeRetorno) = False Then
            MessageBox.Show("Atencion: " & MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        Else
            MessageBox.Show(MensajeRetorno, "Caja", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ListarComprobantes()
            Exit Sub
        End If

    End Sub

    Sub AbrirCaja()

        'Validar
        If DataGridView1.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        If MessageBox.Show("Desea volver realizar la apertura de la caja?", "Apertura de caja", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        If Me.DataGridView1.SelectedRows(0).Cells("Estado").Value = "HABILITADO" Then
            MessageBox.Show("El registro ya esta abierto! Seleccione otro registro.", "Apertura de caja", MessageBoxButtons.OK, MessageBoxIcon.Question)
            Exit Sub
        End If

        Dim NroCaja As Integer = Me.DataGridView1.SelectedRows(0).Cells("Numero").Value


        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@Numero", NroCaja, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "ABRIR", ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpCaja", False, False, MensajeRetorno) = False Then
            MessageBox.Show("Atencion: " & MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        Else
            MessageBox.Show(MensajeRetorno, "Caja", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ListarComprobantes()
            Exit Sub
        End If

    End Sub

    Sub VerDetalle()

        If DataGridView1.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione un registro!"
            ctrError.SetError(btnVerDetalle, mensaje)
            ctrError.SetIconAlignment(btnVerDetalle, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim IDTransaccion As Integer
        IDTransaccion = DataGridView1.SelectedRows(0).Cells("Numero").Value

        'Dim frm As New frmVizualizarVenta
        'frm.Inicializar()
        'frm.CargarOperacion(IDTransaccion)
        'frm.ShowDialog()

    End Sub

    Sub VerAsiento()

        If DataGridView1.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione un registro!"
            ctrError.SetError(btnAsiento, mensaje)
            ctrError.SetIconAlignment(btnAsiento, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim Comprobante As String
        Dim Cliente As String
        Dim IDTransaccion As Integer

        IDTransaccion = DataGridView1.SelectedRows(0).Cells(0).Value
        Comprobante = DataGridView1.SelectedRows(0).Cells(2).Value
        Cliente = DataGridView1.SelectedRows(0).Cells(3).Value

        Dim frm As New frmVisualizarAsiento
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.Text = "Venta: " & Comprobante & "  -  " & Cliente
        frm.IDTransaccion = IDTransaccion

        'Mostramos
        frm.ShowDialog(Me)

    End Sub

    Sub Imprimir()

        If DataGridView1.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim CReporte As New ERP.Reporte.CReporteConfiguracion
        Dim Where As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar IDTransaccion
        Where = " Where Resumido='False' And IDTransaccionCaja =" & DataGridView1.SelectedRows(0).Cells(0).Value

        Dim Path As String = vgImpresionFacturaPath

        CReporte.ImprimirCaja(frm, Where)

    End Sub

    Sub Anular()

        'Seleccion
        If DataGridView1.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione un registro!"
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim IDTransaccion As Integer
        IDTransaccion = DataGridView1.SelectedRows(0).Cells(0).Value

        Dim frm As New frmAgregarMotivoAnulacion
        frm.IDTransaccion = IDTransaccion
        frm.Text = " Motivo Anulacion "
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.ShowDialog(Me)

        ListarComprobantes()

    End Sub

    Private Sub frmAdministrarVenta_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmAdministrarVenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnVerDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerDetalle.Click
        VerDetalle()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VerAsiento()
    End Sub

    Private Sub btnNuevaCaja_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevaCaja.Click
        AperturaCaja()
    End Sub

    Private Sub btnCerrarCaja_Click(sender As System.Object, e As System.EventArgs) Handles btnCerrarCaja.Click
        CerrarCaja()

    End Sub

    Private Sub btnListar_Click(sender As System.Object, e As System.EventArgs) Handles btnListar.Click
        ListarComprobantes()
    End Sub

    Private Sub txtNroCaja_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtNroCaja.KeyDown, txtNroCaja.KeyUp
        If e.KeyCode = Keys.Enter Then

            If IsNumeric(txtNroCaja.Text) = False Then
                Exit Sub
            End If

            ListarComprobantes(CInt(txtNroCaja.Text))
        End If
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        AbrirCaja()
    End Sub

    'FA 28/05/2021
    Sub frmAdministrarCaja_Activate()
        Me.Refresh()
    End Sub

End Class
﻿Public Class frmAperturaCaja

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Public Property IDOperacion As Integer
    Public Property IDTransaccion As Integer
    Public Property Procesado As Boolean


    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        Procesado = False

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "CAJA", "CAJ")

        ObtenerSiguienteNumero()

        'Foco
        cbxSucursal.cbx.Focus()

    End Sub

    Sub ObtenerSiguienteNumero()

        If IsNumeric(cbxSucursal.GetValue) = False Then
            MessageBox.Show("Seleccione correctamente la sucursal!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select 'Numero'=IsNull(MAX(Numero) + 1,1), 'Fecha'=GETDATE() From Caja Where IDSucursal=" & cbxSucursal.GetValue)

        If dt Is Nothing Then
            MessageBox.Show("No se pudo obtener la informacion!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            MessageBox.Show("No se pudo obtener la informacion!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim Registro As DataRow = dt.Rows(0)

        txtNroCaja.SetValue(Registro("Numero"))
        txtFecha.SetValue(Registro("Fecha"))

    End Sub

    Sub Guardar()

        If MessageBox.Show("Desea realizar la apertura de la caja?", "Abrir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@Numero", txtNroCaja.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpCaja", False, False, MensajeRetorno) = False Then
            MessageBox.Show("Atencion: " & MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        If CBool(param(param.Length - 1).Value) = True Then
            Me.Close()
            Procesado = True
            Exit Sub
        End If

    End Sub

    Sub Cancelar()

        Procesado = False
        Me.Close()

    End Sub

    Private Sub frmAperturaCaja_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Enter Then
            CSistema.SelectNextControl(Me, e.KeyCode)
        End If
    End Sub

    Private Sub frmAperturaCaja_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub cbxSucursal_LostFocus(sender As Object, e As System.EventArgs) Handles cbxSucursal.LostFocus
        ObtenerSiguienteNumero()
    End Sub

    Private Sub txtNroCaja_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtNroCaja.TeclaPrecionada
        If e.KeyCode = Keys.Add Then
            ObtenerSiguienteNumero()
        End If
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub cbxSucursal_Leave(sender As System.Object, e As System.EventArgs) Handles cbxSucursal.Leave
        ObtenerSiguienteNumero()
    End Sub

    'FA 28/05/2021
    Sub frmAperturaCaja_Activate()
        Me.Refresh()
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImpresionImpresora
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.lblFormulario = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.txtTerminal = New ERP.ocxTXTString()
        Me.cbxFormularioImpresion = New ERP.ocxCBX()
        Me.cbxImpresora = New ERP.ocxCBX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(414, 298)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 13
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 332)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(500, 22)
        Me.StatusStrip1.TabIndex = 14
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(90, 5)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(38, 21)
        Me.txtID.SoloLectura = True
        Me.txtID.TabIndex = 1
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(333, 92)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 10
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(414, 92)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 11
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(252, 92)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 9
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(171, 92)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 8
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(90, 92)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 7
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lvLista
        '
        Me.lvLista.Location = New System.Drawing.Point(90, 121)
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(399, 171)
        Me.lvLista.TabIndex = 12
        Me.lvLista.UseCompatibleStateImageBehavior = False
        '
        'lblFormulario
        '
        Me.lblFormulario.AutoSize = True
        Me.lblFormulario.Location = New System.Drawing.Point(12, 36)
        Me.lblFormulario.Name = "lblFormulario"
        Me.lblFormulario.Size = New System.Drawing.Size(58, 13)
        Me.lblFormulario.TabIndex = 3
        Me.lblFormulario.Text = "Formulario:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(12, 9)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'txtTerminal
        '
        Me.txtTerminal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTerminal.Color = System.Drawing.Color.Empty
        Me.txtTerminal.Indicaciones = Nothing
        Me.txtTerminal.Location = New System.Drawing.Point(134, 5)
        Me.txtTerminal.Multilinea = False
        Me.txtTerminal.Name = "txtTerminal"
        Me.txtTerminal.Size = New System.Drawing.Size(237, 21)
        Me.txtTerminal.SoloLectura = True
        Me.txtTerminal.TabIndex = 2
        Me.txtTerminal.TabStop = False
        Me.txtTerminal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTerminal.Texto = ""
        '
        'cbxFormularioImpresion
        '
        Me.cbxFormularioImpresion.CampoWhere = Nothing
        Me.cbxFormularioImpresion.CargarUnaSolaVez = False
        Me.cbxFormularioImpresion.DataDisplayMember = "Descripcion"
        Me.cbxFormularioImpresion.DataFilter = Nothing
        Me.cbxFormularioImpresion.DataOrderBy = Nothing
        Me.cbxFormularioImpresion.DataSource = "vFormularioImpresion"
        Me.cbxFormularioImpresion.DataValueMember = "IDFormularioImpresion"
        Me.cbxFormularioImpresion.FormABM = Nothing
        Me.cbxFormularioImpresion.Indicaciones = Nothing
        Me.cbxFormularioImpresion.Location = New System.Drawing.Point(90, 32)
        Me.cbxFormularioImpresion.Name = "cbxFormularioImpresion"
        Me.cbxFormularioImpresion.SeleccionObligatoria = False
        Me.cbxFormularioImpresion.Size = New System.Drawing.Size(281, 21)
        Me.cbxFormularioImpresion.SoloLectura = False
        Me.cbxFormularioImpresion.TabIndex = 4
        Me.cbxFormularioImpresion.Texto = ""
        '
        'cbxImpresora
        '
        Me.cbxImpresora.CampoWhere = Nothing
        Me.cbxImpresora.CargarUnaSolaVez = False
        Me.cbxImpresora.DataDisplayMember = Nothing
        Me.cbxImpresora.DataFilter = Nothing
        Me.cbxImpresora.DataOrderBy = Nothing
        Me.cbxImpresora.DataSource = Nothing
        Me.cbxImpresora.DataValueMember = Nothing
        Me.cbxImpresora.FormABM = Nothing
        Me.cbxImpresora.Indicaciones = Nothing
        Me.cbxImpresora.Location = New System.Drawing.Point(90, 59)
        Me.cbxImpresora.Name = "cbxImpresora"
        Me.cbxImpresora.SeleccionObligatoria = False
        Me.cbxImpresora.Size = New System.Drawing.Size(281, 21)
        Me.cbxImpresora.SoloLectura = False
        Me.cbxImpresora.TabIndex = 6
        Me.cbxImpresora.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Impresora:"
        '
        'frmImpresionImpresora
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(500, 354)
        Me.Controls.Add(Me.cbxImpresora)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbxFormularioImpresion)
        Me.Controls.Add(Me.txtTerminal)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.lvLista)
        Me.Controls.Add(Me.lblFormulario)
        Me.Controls.Add(Me.lblID)
        Me.Name = "frmImpresionImpresora"
        Me.Text = "frmImpresionImpresora"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lvLista As System.Windows.Forms.ListView
    Friend WithEvents lblFormulario As System.Windows.Forms.Label
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents txtTerminal As ERP.ocxTXTString
    Friend WithEvents cbxFormularioImpresion As ERP.ocxCBX
    Friend WithEvents cbxImpresora As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class

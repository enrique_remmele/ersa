﻿Public Class frmImpresionCoordenadasCopias

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Property IDFormularioImpresion As Integer

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button

        'Funciones
        Listar()

    End Sub

    Sub Listar()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VFormularioImpresionCopia Where IDFormularioImpresion=" & IDFormularioImpresion & " Order By Descripcion")
        CSistema.dtToGrid(dgv, dt)

        'Formato
        dgv.Columns("IDFormularioImpresion").Visible = False
        dgv.Columns("ID").Visible = False
        dgv.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("Descripcion").ReadOnly = True
        dgv.Columns("PosicionX").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv.Columns("PosicionY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv.Columns("PosicionX").DefaultCellStyle.Format = "N0"
        dgv.Columns("PosicionY").DefaultCellStyle.Format = "N0"

    End Sub

    Sub Modificar()

        'Validar
        If dgv.CurrentRow Is Nothing Then
            MessageBox.Show("Seleccione un campo para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim IDFormularioImpresionCopia As Integer = dgv.CurrentRow.Cells("ID").Value
        Dim PosicionX As String = dgv.CurrentRow.Cells("PosicionX").Value
        Dim PosicionY As String = dgv.CurrentRow.Cells("PosicionY").Value

        Dim SQL As String = "Update FormularioImpresionCopia Set PosicionX=" & PosicionX & ", PosicionY=" & PosicionY & " Where IDFormularioImpresion=" & IDFormularioImpresion & " And ID=" & IDFormularioImpresionCopia & ""

        Dim Procesado As Integer = CSistema.ExecuteNonQuery(SQL)

        If Procesado = 0 Then
            MessageBox.Show("Se produjo un error mientras se intentaba guardar los cambios! Intentelo nuevamente.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

    End Sub

    Sub Cerrar()
        Me.Close()
    End Sub

    Private Sub frmImpresionCoordenadasCopias_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub dgv_CellEndEdit(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellEndEdit
        Modificar()
    End Sub

    Private Sub btnCerrar_Click(sender As System.Object, e As System.EventArgs) Handles btnCerrar.Click
        Cerrar()
    End Sub

    'FA 28/05/2021
    Sub frmImpresionCoordenadasCopias_Activate()
        Me.Refresh()
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClonarFormulario
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxDocumento = New ERP.ocxCBX()
        Me.cbxDestino = New ERP.ocxCBX()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(145, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Seleccione Formulario Origen"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 78)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(150, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Seleccione Formulario Destino"
        '
        'cbxDocumento
        '
        Me.cbxDocumento.CampoWhere = Nothing
        Me.cbxDocumento.CargarUnaSolaVez = False
        Me.cbxDocumento.DataDisplayMember = Nothing
        Me.cbxDocumento.DataFilter = Nothing
        Me.cbxDocumento.DataOrderBy = Nothing
        Me.cbxDocumento.DataSource = Nothing
        Me.cbxDocumento.DataValueMember = Nothing
        Me.cbxDocumento.dtSeleccionado = Nothing
        Me.cbxDocumento.FormABM = Nothing
        Me.cbxDocumento.Indicaciones = Nothing
        Me.cbxDocumento.Location = New System.Drawing.Point(12, 43)
        Me.cbxDocumento.Name = "cbxDocumento"
        Me.cbxDocumento.SeleccionMultiple = False
        Me.cbxDocumento.SeleccionObligatoria = True
        Me.cbxDocumento.Size = New System.Drawing.Size(290, 23)
        Me.cbxDocumento.SoloLectura = False
        Me.cbxDocumento.TabIndex = 2
        Me.cbxDocumento.Texto = ""
        '
        'cbxDestino
        '
        Me.cbxDestino.CampoWhere = Nothing
        Me.cbxDestino.CargarUnaSolaVez = False
        Me.cbxDestino.DataDisplayMember = Nothing
        Me.cbxDestino.DataFilter = Nothing
        Me.cbxDestino.DataOrderBy = Nothing
        Me.cbxDestino.DataSource = Nothing
        Me.cbxDestino.DataValueMember = Nothing
        Me.cbxDestino.dtSeleccionado = Nothing
        Me.cbxDestino.FormABM = Nothing
        Me.cbxDestino.Indicaciones = Nothing
        Me.cbxDestino.Location = New System.Drawing.Point(16, 94)
        Me.cbxDestino.Name = "cbxDestino"
        Me.cbxDestino.SeleccionMultiple = False
        Me.cbxDestino.SeleccionObligatoria = True
        Me.cbxDestino.Size = New System.Drawing.Size(290, 23)
        Me.cbxDestino.SoloLectura = False
        Me.cbxDestino.TabIndex = 3
        Me.cbxDestino.Texto = ""
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(235, 123)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Clonar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmClonarFormulario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(322, 154)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.cbxDestino)
        Me.Controls.Add(Me.cbxDocumento)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmClonarFormulario"
        Me.Text = "frmClonarFormulario"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents cbxDocumento As ocxCBX
    Friend WithEvents cbxDestino As ocxCBX
    Friend WithEvents Button1 As Button
End Class

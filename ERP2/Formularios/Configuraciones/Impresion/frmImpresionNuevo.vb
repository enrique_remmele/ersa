﻿Public Class frmImpresionNuevo

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDAES
    Property Procesado As Boolean = False
    Property dtImpresion As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Controles
        CSistema.InicializaControles(Me)

        'Funciones
        CargarDocumento()

        'Focus
        cbxDocumento.Focus()

    End Sub

    Sub CargarDocumento()

        Dim SQL As String = "Exec SpImpresion "
        dtImpresion = CSistema.ExecuteToDataTable(SQL)

        If dtImpresion Is Nothing Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxDocumento.cbx, dtImpresion, "IDImpresion", "Descripcion")

    End Sub

    Sub Guardar()

        'Validar
        If txtDescripcion.GetValue = "" Then
            MessageBox.Show("Ingrese una descripcion para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim SQL As String = "Exec SpFormularioImpresion "

        'Entidad
       CSistema.ConcatenarParametro(SQL, "@IDImpresion", cbxDocumento.GetValue)
        CSistema.ConcatenarParametro(SQL, "@Descripcion", txtDescripcion.GetValue)
        CSistema.ConcatenarParametro(SQL, "@Operacion", CSistema.NUMOperacionesABM.INS.ToString)

        'Ejecutar
        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        'Verificar si se produzco un error
        If dt Is Nothing Then
            MessageBox.Show("Error con la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Verificar si se produzco un error
        If dt.Rows.Count = 0 Then
            MessageBox.Show("Error con la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Mensaje
        If dt.Rows(0)("Procesado") = True Then
            MessageBox.Show(dt.Rows(0)("Mensaje"), "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Procesado = True
            Me.Close()
        Else
            MessageBox.Show(dt.Rows(0)("Mensaje"), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Procesado = False
            Exit Sub
        End If

    End Sub

    Sub MostrarObservacion()

        txtObservacion.Clear()
        txtObservacion.Text = dtImpresion.Select("IDImpresion=" & cbxDocumento.GetValue)(0)("Observacion").ToString

    End Sub

    Private Sub frmImpresionNuevo_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Enter Then
            CSistema.SelectNextControl(Me, e.KeyCode)
        End If
    End Sub

    Private Sub frmImpresionNuevo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Procesado = False
        Me.Close()
    End Sub

    Private Sub cbxDocumento_Leave(sender As Object, e As System.EventArgs) Handles cbxDocumento.Leave
        MostrarObservacion()
    End Sub

    Private Sub cbxDocumento_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxDocumento.PropertyChanged
        MostrarObservacion()
    End Sub

    'FA 28/05/2021
    Sub frmImpresionNuevo_Activate()
        Me.Refresh()
    End Sub

End Class
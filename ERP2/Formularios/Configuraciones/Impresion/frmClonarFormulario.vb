﻿Public Class frmClonarFormulario

    Dim CSistema As New CSistema

    Sub Inicializar()

        CSistema.SqlToComboBox(cbxDocumento, "Select IDFormularioImpresion, Descripcion From VFormularioImpresion ")
        CSistema.SqlToComboBox(cbxDestino, "Select IDFormularioImpresion, Descripcion From VFormularioImpresion ")


    End Sub

    Sub CLonar()




        Dim SQL As String = "Exec SpClonarFormularioImpresion "

        'Entidad
        CSistema.ConcatenarParametro(SQL, "@IDFormularioOrigen", cbxDocumento.GetValue)
        CSistema.ConcatenarParametro(SQL, "@IDFormularioDestino", cbxDestino.GetValue)

        'Ejecutar
        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        'Verificar si se produzco un error
        If dt Is Nothing Then
            MessageBox.Show("Error con la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Verificar si se produzco un error
        If dt.Rows.Count = 0 Then
            MessageBox.Show("Error con la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        MessageBox.Show(dt.Rows(0)("Mensaje"), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
        'Mensaje
        If dt.Rows(0)("Procesado") = True Then
            Me.Close()
        End If


    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Clonar()
    End Sub

    Private Sub frmClonarFormulario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    'FA 28/05/2021
    Sub frmClonarFormulario_Activate()
        Me.Refresh()
    End Sub

End Class
﻿Public Class frmImpresion

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CImpresion As New CImpresion

    'PROPIEDADES
    Property dtFormularioImpresion As DataTable
    Property dtDetalle As DataTable

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()


        'Funciones
        CargarFuentes()
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'FormularioImpresionS
        CSistema.SqlToComboBox(cbxDocumento, "Select IDFormularioImpresion, Descripcion From VFormularioImpresion ")

    End Sub

    Sub NuevoDocumento()

        Dim frm As New frmImpresionNuevo
        FGMostrarFormulario(Me, frm, "Nuevo Documento", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

    End Sub

    Sub CargarDocumento()

        'Cargar datos
        dtFormularioImpresion = CSistema.ExecuteToDataTable("Select * From VFormularioImpresion Where IDFormularioImpresion=" & cbxDocumento.GetValue)
        dtDetalle = CSistema.ExecuteToDataTable("Select * From VDetalleFormularioImpresion Where IDFormularioImpresion=" & cbxDocumento.GetValue)

        'Estado
        If dtFormularioImpresion.Rows(0)("Estado").ToString = "True" Then
            btnActivarDesactivar.Text = "Desactivar"
        Else
            btnActivarDesactivar.Text = "Activar"
        End If

        'Listar cabecera
        ListarCampos(dgvCabecera, False)

        'Listar detalle
        ListarCampos(dgvDetalle, True)

        'Papel
        If dtFormularioImpresion.Rows.Count > 0 Then

            txtTamañoPapel.Text = dtFormularioImpresion.Rows(0)("TamañoPapel").ToString
            nudTamañoPapelX.Value = CSistema.RetornarValorInteger(dtFormularioImpresion.Rows(0)("TamañoPapelX").ToString)
            nudTamañoPapelY.Value = CSistema.RetornarValorInteger(dtFormularioImpresion.Rows(0)("TamañoPapelY").ToString)
            nudMargenIzquierda.Value = CSistema.RetornarValorInteger(dtFormularioImpresion.Rows(0)("MargenIzquierdo").ToString)
            nudMargenDerecha.Value = CSistema.RetornarValorInteger(dtFormularioImpresion.Rows(0)("MargenDerecho").ToString)
            nudMargenArriba.Value = CSistema.RetornarValorInteger(dtFormularioImpresion.Rows(0)("MargenArriba").ToString)
            nudMargenAbajo.Value = CSistema.RetornarValorInteger(dtFormularioImpresion.Rows(0)("MargenAbajo").ToString)

            If CSistema.RetornarValorBoolean(dtFormularioImpresion.Rows(0)("Horizontal").ToString) Then
                rdbHorizontal.Checked = True
            Else
                rdbVertical.Checked = True
            End If

        End If

        If dtFormularioImpresion Is Nothing Then
            Exit Sub
        End If

        If dtFormularioImpresion.Rows.Count = 0 Then
            Exit Sub
        End If

        'Opciones
        chkDetalle.Valor = CSistema.RetornarValorBoolean(dtFormularioImpresion.Rows(0)("Detalle").ToString)
        nudCantidadDetalle.Value = CSistema.RetornarValorInteger(dtFormularioImpresion.Rows(0)("CantidadDetalle").ToString)
        nudCopia.Value = CSistema.RetornarValorInteger(dtFormularioImpresion.Rows(0)("Copias").ToString)

        'Impresora
        CargarImpresora()

    End Sub

    Sub CargarImpresora()

        txtImpresora.Clear()

        Dim vdt As DataTable = CSistema.ExecuteToDataTable("Select * From VFormularioImpresionTerminal Where IDTerminal=" & vgIDTerminal & " And IDFormularioImpresion=" & cbxDocumento.GetValue & " ")
        If vdt Is Nothing Then
            Exit Sub
        End If

        If vdt.Rows.Count = 0 Then
            Exit Sub
        End If

        txtImpresora.Text = vdt.Rows(0)("Impresora").ToString
        CImpresion.Impresora = txtImpresora.Text

    End Sub

    Sub ListarCampos(ByRef dgv As DataGridView, Detalle As Boolean)

        Dim dttemp As DataTable = CData.FiltrarDataTable(dtDetalle, " Detalle = '" & Detalle.ToString & "' ")
        CSistema.dtToGrid(dgv, dttemp)

        'Formato
        dgv.SelectionMode = DataGridViewSelectionMode.CellSelect

        'Ocultar todas las columnas
        For c As Integer = 0 To dgv.ColumnCount - 1
            dgv.Columns(c).Visible = False
        Next

        'Mostrar
        dgv.Columns("Campo").Visible = True
        dgv.Columns("PosicionX").Visible = True
        dgv.Columns("PosicionY").Visible = True
        dgv.Columns("Campo").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("PosicionX").HeaderText = "Pos. X"
        dgv.Columns("PosicionY").HeaderText = "Pos. Y"
        dgv.Columns("PosicionX").DefaultCellStyle.Format = "N0"
        dgv.Columns("PosicionY").DefaultCellStyle.Format = "N0"
        dgv.Columns("PosicionX").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv.Columns("PosicionY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

    End Sub

    Sub AgregarCampo(ByRef dgv As DataGridView, Detalle As Boolean)

        Dim frm As New frmImpresionAgregarCampo

        'Obtenemos la vista
        If Detalle Then
            frm.Vista = dtFormularioImpresion.Select("IDFormularioImpresion=" & cbxDocumento.GetValue)(0)("VistaDetalle").ToString
        Else
            frm.Vista = dtFormularioImpresion.Select("IDFormularioImpresion=" & cbxDocumento.GetValue)(0)("Vista").ToString
        End If

        frm.Detalle = Detalle
        frm.IDFormularioImpresion = cbxDocumento.GetValue
        FGMostrarFormulario(Me, frm, "Agregar campo", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Seleccionado = False Then
            Exit Sub
        End If

        'Volvemos a listar
        dtDetalle = CSistema.ExecuteToDataTable("Select * From VDetalleFormularioImpresion Where IDFormularioImpresion=" & cbxDocumento.GetValue)
        ListarCampos(dgv, Detalle)

    End Sub

    Sub ModificarCampo(ByRef dgv As DataGridView)

        'Validar
        If dgv.CurrentRow Is Nothing Then
            MessageBox.Show("Seleccione un campo para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim SQL As String = "Exec SpDetalleFormularioImpresion "
        Dim Campo As String = dgv.CurrentRow.Cells("NombreCampo").Value
        Dim IDDetalleFormularioImpresion As Integer = dgv.CurrentRow.Cells("IDDetalleFormularioImpresion").Value
        Dim PosicionX As String = dgv.CurrentRow.Cells("PosicionX").Value
        Dim PosicionY As String = dgv.CurrentRow.Cells("PosicionY").Value

        'Entidad
        CSistema.ConcatenarParametro(SQL, "@IDFormularioImpresion", cbxDocumento.GetValue)
        CSistema.ConcatenarParametro(SQL, "@IDDetalleFormularioImpresion", IDDetalleFormularioImpresion)
        CSistema.ConcatenarParametro(SQL, "@NombreCampo", Campo)
        CSistema.ConcatenarParametro(SQL, "@Campo", Campo)
        CSistema.ConcatenarParametro(SQL, "@PosicionX", PosicionX)
        CSistema.ConcatenarParametro(SQL, "@PosicionY", PosicionY)

        'Formato
        CSistema.ConcatenarParametro(SQL, "@TipoLetra", cbxFuente.Text)
        CSistema.ConcatenarParametro(SQL, "@TamañoLetra", nudTamaño.Value)

        Dim Formato As String = "Normal"
        If rdbNegrita.Checked Then
            Formato = "Negrita"
        End If

        If rdbCursiva.Checked Then
            Formato = "Cursiva"
        End If

        CSistema.ConcatenarParametro(SQL, "@Formato", Formato)

        'Tipo
        CSistema.ConcatenarParametro(SQL, "@Numerico", chkNumerico.Checked.ToString)
        CSistema.ConcatenarParametro(SQL, "@Alineacion", cbxAlineacion.Text)
        CSistema.ConcatenarParametro(SQL, "@Largo", nudLargo.Value)
        CSistema.ConcatenarParametro(SQL, "@PuedeCrecer", chkPuedeCrecer.Checked.ToString)
        CSistema.ConcatenarParametro(SQL, "@Lineas", nudLineas.Value)
        CSistema.ConcatenarParametro(SQL, "@MargenPrimeraLinea", nudMargenPrimeraLinea.Value)
        CSistema.ConcatenarParametro(SQL, "@Interlineado", nudInterlineado.Value)

        CSistema.ConcatenarParametro(SQL, "@Operacion", "UPD")

        'Ejecutar
        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        'Verificar si se produzco un error
        If dt Is Nothing Then
            MessageBox.Show("Error con la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Verificar si se produzco un error
        If dt.Rows.Count = 0 Then
            MessageBox.Show("Error con la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Mensaje
        If dt.Rows(0)("Procesado") = False Then
            MessageBox.Show(dt.Rows(0)("Mensaje"), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        'Si todo es correcto, actualizamos tambien la tabla en memoria dtDetalleFormularioImpresion
        For Each oRow As DataRow In dtDetalle.Select("IDDetalleFormularioImpresion=" & IDDetalleFormularioImpresion)
            oRow("PosicionX") = PosicionX
            oRow("PosicionY") = PosicionY
            oRow("TipoLetra") = cbxFuente.Text
            oRow("TamañoLetra") = nudTamaño.Value
            oRow("Formato") = Formato

            oRow("Alineacion") = cbxAlineacion.Text
            oRow("Numerico") = chkNumerico.Checked
            oRow("Largo") = nudLargo.Value
            oRow("PuedeCrecer") = chkPuedeCrecer.Checked
            oRow("Lineas") = nudLineas.Value
            oRow("MargenPrimeraLinea") = nudMargenPrimeraLinea.Value
            oRow("Interlineado") = nudInterlineado.Value

        Next

    End Sub

    Sub EliminarCampo(dgv As DataGridView, Detalle As Boolean)

        'Validar
        If dgv.CurrentCell Is Nothing Then
            MessageBox.Show("Seleccione un campo para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If MessageBox.Show("Desea eliminar el campo?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim SQL As String = "Exec SpDetalleFormularioImpresion "
        Dim IDDetalleFormularioImpresion As Integer = dgv.Rows(dgv.CurrentCell.RowIndex).Cells("IDDetalleFormularioImpresion").Value
        Dim IDFormularioImpresion As Integer = cbxDocumento.GetValue

        'Entidad
        CSistema.ConcatenarParametro(SQL, "@IDFormularioImpresion", IDFormularioImpresion)
        CSistema.ConcatenarParametro(SQL, "@IDDetalleFormularioImpresion", IDDetalleFormularioImpresion)
        CSistema.ConcatenarParametro(SQL, "@Operacion", "DEL")

        'Ejecutar
        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        'Verificar si se produzco un error
        If dt Is Nothing Then
            MessageBox.Show("Error con la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Verificar si se produzco un error
        If dt.Rows.Count = 0 Then
            MessageBox.Show("Error con la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Mensaje
        If dt.Rows(0)("Procesado") = False Then
            MessageBox.Show(dt.Rows(0)("Mensaje"), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        CargarDocumento()

    End Sub

    Sub SeleccionCampo(ID As Integer)

        For Each oRow As DataRow In dtDetalle.Select("IDDetalleFormularioImpresion=" & ID)

            cbxFuente.Text = oRow("TipoLetra").ToString
            nudTamaño.Value = oRow("TamañoLetra").ToString

            Select Case oRow("Formato").ToString
                Case "Normal"
                    rdbNormal.Checked = True
                Case "Negrita"
                    rdbNegrita.Checked = True
                Case "Cursiva"
                    rdbCursiva.Checked = True
                Case Else
                    rdbNormal.Checked = True
            End Select

            cbxAlineacion.Text = oRow("Alineacion").ToString
            chkNumerico.Checked = oRow("Numerico").ToString
            nudLargo.Value = oRow("Largo").ToString
            chkPuedeCrecer.Checked = oRow("PuedeCrecer").ToString
            nudLineas.Value = oRow("Lineas").ToString
            nudMargenPrimeraLinea.Value = oRow("MargenPrimeraLinea").ToString
            nudInterlineado.Value = oRow("Interlineado").ToString
        Next

    End Sub

    Sub CargarFuentes()

        Dim f As System.Drawing.Text.InstalledFontCollection = New System.Drawing.Text.InstalledFontCollection
        Dim fFamily As FontFamily

        If cbxFuente.Items.Count > 0 Then
            cbxFuente.Items.Clear()
        End If

        For Each fFamily In f.Families
            cbxFuente.Items.Add(fFamily.Name)
        Next

    End Sub

    Sub Previsualizar(Optional Impresora As Boolean = False)

        CImpresion.Test = True
        CImpresion.Inicializar()
        CImpresion.PrintPreviewControl1 = ppd
        CImpresion.Zoom = nudZoom.Value
        CImpresion.DibujarCuadricula = chkDibujarCuadricula.Valor
        CImpresion.IDFormularioImpresion = cbxDocumento.GetValue
        CImpresion.PageSettings.PaperSize = New System.Drawing.Printing.PaperSize(txtTamañoPapel.Text, nudTamañoPapelX.Value, nudTamañoPapelY.Value)
        CImpresion.PageSettings.Margins = New System.Drawing.Printing.Margins(nudMargenIzquierda.Value, nudMargenDerecha.Value, nudMargenArriba.Value, nudMargenAbajo.Value)
        CImpresion.PageSettings.Landscape = rdbHorizontal.Checked
        CImpresion.Impresora = txtImpresora.Text

        If Impresora Then
            CImpresion.Imprimir()
        Else
            CImpresion.ImprimirTest()
        End If


    End Sub

    Sub EstablecerPapel()

        PageSetupDialog1.Document = New System.Drawing.Printing.PrintDocument

        Try

            If PageSetupDialog1.PageSettings.PaperSize.Kind <> Printing.PaperKind.Custom Then
                PageSetupDialog1.PageSettings.PaperSize.PaperName = txtTamañoPapel.Text
                PageSetupDialog1.PageSettings.PaperSize = New System.Drawing.Printing.PaperSize(txtTamañoPapel.Text, nudTamañoPapelX.Value, nudTamañoPapelY.Value)
            End If

            PageSetupDialog1.PageSettings.Margins = New System.Drawing.Printing.Margins(CImpresion.ConvInchToMm(nudMargenIzquierda.Value), CImpresion.ConvInchToMm(nudMargenDerecha.Value), CImpresion.ConvInchToMm(nudMargenArriba.Value), CImpresion.ConvInchToMm(nudMargenAbajo.Value))
            PageSetupDialog1.PageSettings.Landscape = rdbHorizontal.Checked

        Catch ex As Exception

        End Try

        PageSetupDialog1.PrinterSettings.PrinterName = txtImpresora.Text

        If PageSetupDialog1.ShowDialog(Me) <> Windows.Forms.DialogResult.OK Then
            Exit Sub
        End If

        Try

            txtTamañoPapel.Text = PageSetupDialog1.PageSettings.PaperSize.PaperName
            nudTamañoPapelX.Value = PageSetupDialog1.PageSettings.PaperSize.Width
            nudTamañoPapelY.Value = PageSetupDialog1.PageSettings.PaperSize.Height
            nudMargenIzquierda.Value = CImpresion.ConvToMmInch(PageSetupDialog1.PageSettings.Margins.Left)
            nudMargenDerecha.Value = CImpresion.ConvToMmInch(PageSetupDialog1.PageSettings.Margins.Right)
            nudMargenArriba.Value = CImpresion.ConvToMmInch(PageSetupDialog1.PageSettings.Margins.Top)
            nudMargenAbajo.Value = CImpresion.ConvToMmInch(PageSetupDialog1.PageSettings.Margins.Bottom)

            If PageSetupDialog1.PageSettings.Landscape Then
                rdbHorizontal.Checked = True
            Else
                rdbVertical.Checked = True
            End If

        Catch ex As Exception

        End Try

        ModificarPapel()

    End Sub

    Sub EstablecerCoordenadasCopias()

        Dim frm As New frmImpresionCoordenadasCopias
        frm.IDFormularioImpresion = cbxDocumento.GetValue
        FGMostrarFormulario(Me, frm, "Coordenadas de Copias", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False, False)

    End Sub

    Sub ModificarPapel()

        'Validar
        If cbxDocumento.GetValue = 0 Then
            MessageBox.Show("Seleccione correctamente un FormularioImpresion para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim SQL As String = "Exec SpFormularioImpresion "

        'Entidad
        CSistema.ConcatenarParametro(SQL, "@IDFormularioImpresion", cbxDocumento.GetValue)
        CSistema.ConcatenarParametro(SQL, "@TamañoPapel", txtTamañoPapel.Text)
        CSistema.ConcatenarParametro(SQL, "@TamañoPapelX", nudTamañoPapelX.Value)
        CSistema.ConcatenarParametro(SQL, "@TamañoPapelY", nudTamañoPapelY.Value)
        CSistema.ConcatenarParametro(SQL, "@Horizontal", rdbHorizontal.Checked)
        CSistema.ConcatenarParametro(SQL, "@MargenIzquierdo", nudMargenIzquierda.Value)
        CSistema.ConcatenarParametro(SQL, "@MargenDerecho", nudMargenDerecha.Value)
        CSistema.ConcatenarParametro(SQL, "@MargenArriba", nudMargenArriba.Value)
        CSistema.ConcatenarParametro(SQL, "@MargenAbajo", nudMargenAbajo.Value)

        CSistema.ConcatenarParametro(SQL, "@Operacion", "PAPEL")

        'Ejecutar
        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        'Verificar si se produzco un error
        If dt Is Nothing Then
            MessageBox.Show("Error con la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Verificar si se produzco un error
        If dt.Rows.Count = 0 Then
            MessageBox.Show("Error con la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Mensaje
        If dt.Rows(0)("Procesado") = False Then
            MessageBox.Show(dt.Rows(0)("Mensaje"), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

    End Sub

    Sub ModificarOpciones()

        'Validar
        If cbxDocumento.GetValue = 0 Then
            MessageBox.Show("Seleccione correctamente un FormularioImpresion para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim SQL As String = "Exec SpFormularioImpresion "

        'Entidad
        CSistema.ConcatenarParametro(SQL, "@IDFormularioImpresion", cbxDocumento.GetValue)
        CSistema.ConcatenarParametro(SQL, "@Detalle", chkDetalle.Valor)
        CSistema.ConcatenarParametro(SQL, "@CantidadDetalle", nudCantidadDetalle.Value)
        CSistema.ConcatenarParametro(SQL, "@Copias", nudCopia.Value)

        CSistema.ConcatenarParametro(SQL, "@Operacion", "OPCIONES")

        'Ejecutar
        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        'Verificar si se produzco un error
        If dt Is Nothing Then
            MessageBox.Show("Error con la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Verificar si se produzco un error
        If dt.Rows.Count = 0 Then
            MessageBox.Show("Error con la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Mensaje
        If dt.Rows(0)("Procesado") = False Then
            MessageBox.Show(dt.Rows(0)("Mensaje"), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

    End Sub

    Sub SeleccionarImpresora()

        Dim frm As New frmImpresionImpresora
        FGMostrarFormulario(Me, frm, "Impresoras", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True)
        CargarImpresora()

    End Sub

    Sub ActivarDesactivar()
        Dim Estado As Integer = 0
        If btnActivarDesactivar.Text = "Activar" Then
            Estado = 1
        End If

        CSistema.ExecuteNonQuery("Update FormularioImpresion set Estado = " & Estado & " Where IDFormularioImpresion = " & cbxDocumento.GetValue)
        CargarDocumento()
    End Sub

    Private Sub frmImpresion_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub ToolStripButton7_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton7.Click
        NuevoDocumento()
    End Sub

    Private Sub cbxDocumento_Leave(sender As Object, e As System.EventArgs) Handles cbxDocumento.Leave
        CargarDocumento()
    End Sub

    Private Sub cbxDocumento_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxDocumento.PropertyChanged
        CargarDocumento()
    End Sub

    Private Sub dgvCabecera_SelectionChanged(sender As Object, e As System.EventArgs) Handles dgvCabecera.SelectionChanged
        If dgvCabecera.CurrentRow Is Nothing Then
            Exit Sub
        End If

        SeleccionCampo(dgvCabecera.CurrentRow.Cells("IDDetalleFormularioImpresion").Value)
    End Sub

    Private Sub ToolStripButton1_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton1.Click
        AgregarCampo(dgvCabecera, False)
    End Sub

    Private Sub dgvCabecera_CellEndEdit(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCabecera.CellEndEdit
        ModificarCampo(dgvCabecera)
    End Sub

    Private Sub btnAplicar_Click(sender As System.Object, e As System.EventArgs) Handles btnAplicar.Click

        Select Case tcFormulario.SelectedTab.Name

            Case "TabPage1"
                ModificarCampo(dgvCabecera)
            Case "TabPage2"
                ModificarCampo(dgvDetalle)
        End Select


    End Sub

    Private Sub ToolStripButton8_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton8.Click
        CargarDocumento()
    End Sub

    Private Sub btnPrevisualizar_Click(sender As System.Object, e As System.EventArgs) Handles btnPrevisualizar.Click
        Previsualizar()
    End Sub

    Private Sub btnImprimir_Click(sender As System.Object, e As System.EventArgs) Handles btnImprimir.Click
        Previsualizar(True)
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles btnEstablecerPapel.Click
        EstablecerPapel()
    End Sub

    Private Sub btnAplicarConfiguracion_Click(sender As System.Object, e As System.EventArgs) Handles btnAplicarConfiguracion.Click
        ModificarOpciones()
    End Sub

    Private Sub nudZoom_ValueChanged(sender As System.Object, e As System.EventArgs) Handles nudZoom.ValueChanged
        ppd.Zoom = nudZoom.Value
    End Sub

    Private Sub btnAplicarTipo_Click(sender As System.Object, e As System.EventArgs) Handles btnAplicarTipo.Click
        Select Case tcFormulario.SelectedTab.Name
            Case "TabPage1"
                ModificarCampo(dgvCabecera)
            Case "TabPage2"
                ModificarCampo(dgvDetalle)
        End Select
    End Sub

    Private Sub ToolStripButton4_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton4.Click
        AgregarCampo(dgvDetalle, True)
    End Sub

    Private Sub dgvDetalle_CellEndEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellEndEdit
        ModificarCampo(dgvDetalle)
    End Sub

    Private Sub dgvDetalle_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles dgvDetalle.SelectionChanged
        SeleccionCampo(dgvDetalle.CurrentRow.Cells("IDDetalleFormularioImpresion").Value)
    End Sub

    Private Sub btnEliminarCabecera_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminarCabecera.Click
        EliminarCampo(dgvCabecera, False)
    End Sub

    Private Sub btnEliminarCampoDetalle_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminarCampoDetalle.Click
        EliminarCampo(dgvDetalle, True)
    End Sub

    Private Sub lklCorrdenadasCopias_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklCorrdenadasCopias.LinkClicked
        EstablecerCoordenadasCopias()
    End Sub

    Private Sub lklSeleccionarImpresora_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklSeleccionarImpresora.LinkClicked
        SeleccionarImpresora()
    End Sub

    Private Sub ToolStripButton9_Click(sender As Object, e As EventArgs) Handles btnActivarDesactivar.Click
        ActivarDesactivar()
    End Sub

    Private Sub ToolStripButton2_Click(sender As Object, e As EventArgs) Handles ToolStripButton2.Click
        Dim frm As New frmClonarFormulario
        frm.ShowDialog()
        Inicializar()
    End Sub

    'FA 28/05/2021
    Sub frmImpresion_Activate()
        Me.Refresh()
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmImpresion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImpresion))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.gbxDocumento = New System.Windows.Forms.GroupBox()
        Me.tsDocumento = New System.Windows.Forms.ToolStrip()
        Me.ToolStripButton7 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton8 = New System.Windows.Forms.ToolStripButton()
        Me.btnActivarDesactivar = New System.Windows.Forms.ToolStripButton()
        Me.cbxDocumento = New ERP.ocxCBX()
        Me.tcFormulario = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.dgvCabecera = New System.Windows.Forms.DataGridView()
        Me.tsCabecera = New System.Windows.Forms.ToolStrip()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.btnEliminarCabecera = New System.Windows.Forms.ToolStripButton()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.tsDetalle = New System.Windows.Forms.ToolStrip()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.btnEliminarCampoDetalle = New System.Windows.Forms.ToolStripButton()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.lklCorrdenadasCopias = New System.Windows.Forms.LinkLabel()
        Me.btnAplicarConfiguracion = New System.Windows.Forms.Button()
        Me.lblCantidadDetalle = New System.Windows.Forms.Label()
        Me.lblCopias = New System.Windows.Forms.Label()
        Me.nudCopia = New System.Windows.Forms.NumericUpDown()
        Me.nudCantidadDetalle = New System.Windows.Forms.NumericUpDown()
        Me.chkDetalle = New ERP.ocxCHK()
        Me.ppd = New System.Windows.Forms.PrintPreviewControl()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.nudZoom = New System.Windows.Forms.NumericUpDown()
        Me.lblZoom = New System.Windows.Forms.Label()
        Me.chkDibujarCuadricula = New ERP.ocxCHK()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnPrevisualizar = New System.Windows.Forms.Button()
        Me.gbxConfiguracionPapel = New System.Windows.Forms.GroupBox()
        Me.nudTamañoPapelY = New System.Windows.Forms.NumericUpDown()
        Me.nudTamañoPapelX = New System.Windows.Forms.NumericUpDown()
        Me.txtTamañoPapel = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.rdbHorizontal = New System.Windows.Forms.RadioButton()
        Me.rdbVertical = New System.Windows.Forms.RadioButton()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.nudMargenAbajo = New System.Windows.Forms.NumericUpDown()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.nudMargenArriba = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.nudMargenDerecha = New System.Windows.Forms.NumericUpDown()
        Me.lblMargenIzquierda = New System.Windows.Forms.Label()
        Me.nudMargenIzquierda = New System.Windows.Forms.NumericUpDown()
        Me.btnEstablecerPapel = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.lklAplicarATodos = New System.Windows.Forms.LinkLabel()
        Me.lblFuente = New System.Windows.Forms.Label()
        Me.lblTipoLetra = New System.Windows.Forms.Label()
        Me.cbxFuente = New System.Windows.Forms.ComboBox()
        Me.btnAplicar = New System.Windows.Forms.Button()
        Me.nudTamaño = New System.Windows.Forms.NumericUpDown()
        Me.pnTipoLetra = New System.Windows.Forms.Panel()
        Me.rdbCursiva = New System.Windows.Forms.RadioButton()
        Me.rdbNegrita = New System.Windows.Forms.RadioButton()
        Me.rdbNormal = New System.Windows.Forms.RadioButton()
        Me.lblTamaño = New System.Windows.Forms.Label()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.nudInterlineado = New System.Windows.Forms.NumericUpDown()
        Me.lblInterlineado = New System.Windows.Forms.Label()
        Me.nudMargenPrimeraLinea = New System.Windows.Forms.NumericUpDown()
        Me.lblMargenPrimeraLinea = New System.Windows.Forms.Label()
        Me.nudLineas = New System.Windows.Forms.NumericUpDown()
        Me.lblLineas = New System.Windows.Forms.Label()
        Me.chkPuedeCrecer = New System.Windows.Forms.CheckBox()
        Me.lblSinLimite = New System.Windows.Forms.Label()
        Me.nudLargo = New System.Windows.Forms.NumericUpDown()
        Me.lblLargo = New System.Windows.Forms.Label()
        Me.chkNumerico = New System.Windows.Forms.CheckBox()
        Me.lblAlineacion = New System.Windows.Forms.Label()
        Me.cbxAlineacion = New System.Windows.Forms.ComboBox()
        Me.btnAplicarTipo = New System.Windows.Forms.Button()
        Me.gbxImpresora = New System.Windows.Forms.GroupBox()
        Me.txtImpresora = New System.Windows.Forms.TextBox()
        Me.lklSeleccionarImpresora = New System.Windows.Forms.LinkLabel()
        Me.PageSetupDialog1 = New System.Windows.Forms.PageSetupDialog()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.gbxDocumento.SuspendLayout()
        Me.tsDocumento.SuspendLayout()
        Me.tcFormulario.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tsCabecera.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tsDetalle.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.nudCopia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudCantidadDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.nudZoom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxConfiguracionPapel.SuspendLayout()
        CType(Me.nudTamañoPapelY, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudTamañoPapelX, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMargenAbajo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMargenArriba, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMargenDerecha, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMargenIzquierda, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        CType(Me.nudTamaño, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnTipoLetra.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        CType(Me.nudInterlineado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMargenPrimeraLinea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudLineas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudLargo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxImpresora.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 306.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 199.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.gbxDocumento, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.tcFormulario, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.ppd, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox4, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxConfiguracionPapel, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TabControl1, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxImpresora, 2, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 165.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1004, 564)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'gbxDocumento
        '
        Me.gbxDocumento.Controls.Add(Me.tsDocumento)
        Me.gbxDocumento.Controls.Add(Me.cbxDocumento)
        Me.gbxDocumento.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxDocumento.Location = New System.Drawing.Point(3, 3)
        Me.gbxDocumento.Name = "gbxDocumento"
        Me.TableLayoutPanel1.SetRowSpan(Me.gbxDocumento, 2)
        Me.gbxDocumento.Size = New System.Drawing.Size(300, 83)
        Me.gbxDocumento.TabIndex = 0
        Me.gbxDocumento.TabStop = False
        Me.gbxDocumento.Text = "Documento"
        '
        'tsDocumento
        '
        Me.tsDocumento.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton7, Me.ToolStripButton8, Me.btnActivarDesactivar, Me.ToolStripButton2})
        Me.tsDocumento.Location = New System.Drawing.Point(3, 16)
        Me.tsDocumento.Name = "tsDocumento"
        Me.tsDocumento.Size = New System.Drawing.Size(294, 25)
        Me.tsDocumento.TabIndex = 1
        Me.tsDocumento.Text = "ToolStrip3"
        '
        'ToolStripButton7
        '
        Me.ToolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton7.Image = CType(resources.GetObject("ToolStripButton7.Image"), System.Drawing.Image)
        Me.ToolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton7.Name = "ToolStripButton7"
        Me.ToolStripButton7.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton7.Text = "ToolStripButton1"
        '
        'ToolStripButton8
        '
        Me.ToolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton8.Image = CType(resources.GetObject("ToolStripButton8.Image"), System.Drawing.Image)
        Me.ToolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton8.Name = "ToolStripButton8"
        Me.ToolStripButton8.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton8.Text = "ToolStripButton2"
        '
        'btnActivarDesactivar
        '
        Me.btnActivarDesactivar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.btnActivarDesactivar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnActivarDesactivar.Name = "btnActivarDesactivar"
        Me.btnActivarDesactivar.Size = New System.Drawing.Size(65, 22)
        Me.btnActivarDesactivar.Text = "Desactivar"
        '
        'cbxDocumento
        '
        Me.cbxDocumento.CampoWhere = Nothing
        Me.cbxDocumento.CargarUnaSolaVez = False
        Me.cbxDocumento.DataDisplayMember = Nothing
        Me.cbxDocumento.DataFilter = Nothing
        Me.cbxDocumento.DataOrderBy = Nothing
        Me.cbxDocumento.DataSource = Nothing
        Me.cbxDocumento.DataValueMember = Nothing
        Me.cbxDocumento.dtSeleccionado = Nothing
        Me.cbxDocumento.FormABM = Nothing
        Me.cbxDocumento.Indicaciones = Nothing
        Me.cbxDocumento.Location = New System.Drawing.Point(6, 51)
        Me.cbxDocumento.Name = "cbxDocumento"
        Me.cbxDocumento.SeleccionMultiple = False
        Me.cbxDocumento.SeleccionObligatoria = True
        Me.cbxDocumento.Size = New System.Drawing.Size(290, 23)
        Me.cbxDocumento.SoloLectura = False
        Me.cbxDocumento.TabIndex = 0
        Me.cbxDocumento.Texto = ""
        '
        'tcFormulario
        '
        Me.tcFormulario.Controls.Add(Me.TabPage1)
        Me.tcFormulario.Controls.Add(Me.TabPage2)
        Me.tcFormulario.Controls.Add(Me.TabPage3)
        Me.tcFormulario.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcFormulario.Location = New System.Drawing.Point(0, 89)
        Me.tcFormulario.Margin = New System.Windows.Forms.Padding(0)
        Me.tcFormulario.Name = "tcFormulario"
        Me.tcFormulario.SelectedIndex = 0
        Me.tcFormulario.Size = New System.Drawing.Size(306, 292)
        Me.tcFormulario.TabIndex = 2
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.dgvCabecera)
        Me.TabPage1.Controls.Add(Me.tsCabecera)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(298, 266)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Cabecera"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'dgvCabecera
        '
        Me.dgvCabecera.AllowUserToAddRows = False
        Me.dgvCabecera.AllowUserToDeleteRows = False
        Me.dgvCabecera.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCabecera.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvCabecera.Location = New System.Drawing.Point(3, 28)
        Me.dgvCabecera.Name = "dgvCabecera"
        Me.dgvCabecera.Size = New System.Drawing.Size(292, 235)
        Me.dgvCabecera.TabIndex = 1
        '
        'tsCabecera
        '
        Me.tsCabecera.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.btnEliminarCabecera})
        Me.tsCabecera.Location = New System.Drawing.Point(3, 3)
        Me.tsCabecera.Name = "tsCabecera"
        Me.tsCabecera.Size = New System.Drawing.Size(292, 25)
        Me.tsCabecera.TabIndex = 0
        Me.tsCabecera.Text = "ToolStrip1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "ToolStripButton1"
        '
        'btnEliminarCabecera
        '
        Me.btnEliminarCabecera.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnEliminarCabecera.Image = CType(resources.GetObject("btnEliminarCabecera.Image"), System.Drawing.Image)
        Me.btnEliminarCabecera.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEliminarCabecera.Name = "btnEliminarCabecera"
        Me.btnEliminarCabecera.Size = New System.Drawing.Size(23, 22)
        Me.btnEliminarCabecera.Text = "ToolStripButton3"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.dgvDetalle)
        Me.TabPage2.Controls.Add(Me.tsDetalle)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(298, 266)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Detalle"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDetalle.Location = New System.Drawing.Point(3, 28)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.Size = New System.Drawing.Size(292, 235)
        Me.dgvDetalle.TabIndex = 2
        '
        'tsDetalle
        '
        Me.tsDetalle.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton4, Me.btnEliminarCampoDetalle})
        Me.tsDetalle.Location = New System.Drawing.Point(3, 3)
        Me.tsDetalle.Name = "tsDetalle"
        Me.tsDetalle.Size = New System.Drawing.Size(292, 25)
        Me.tsDetalle.TabIndex = 1
        Me.tsDetalle.Text = "ToolStrip2"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton4.Text = "ToolStripButton1"
        '
        'btnEliminarCampoDetalle
        '
        Me.btnEliminarCampoDetalle.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnEliminarCampoDetalle.Image = CType(resources.GetObject("btnEliminarCampoDetalle.Image"), System.Drawing.Image)
        Me.btnEliminarCampoDetalle.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEliminarCampoDetalle.Name = "btnEliminarCampoDetalle"
        Me.btnEliminarCampoDetalle.Size = New System.Drawing.Size(23, 22)
        Me.btnEliminarCampoDetalle.Text = "ToolStripButton3"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.lklCorrdenadasCopias)
        Me.TabPage3.Controls.Add(Me.btnAplicarConfiguracion)
        Me.TabPage3.Controls.Add(Me.lblCantidadDetalle)
        Me.TabPage3.Controls.Add(Me.lblCopias)
        Me.TabPage3.Controls.Add(Me.nudCopia)
        Me.TabPage3.Controls.Add(Me.nudCantidadDetalle)
        Me.TabPage3.Controls.Add(Me.chkDetalle)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(298, 266)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Opciones de Formulario"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'lklCorrdenadasCopias
        '
        Me.lklCorrdenadasCopias.AutoSize = True
        Me.lklCorrdenadasCopias.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklCorrdenadasCopias.Location = New System.Drawing.Point(90, 78)
        Me.lklCorrdenadasCopias.Name = "lklCorrdenadasCopias"
        Me.lklCorrdenadasCopias.Size = New System.Drawing.Size(131, 13)
        Me.lklCorrdenadasCopias.TabIndex = 5
        Me.lklCorrdenadasCopias.TabStop = True
        Me.lklCorrdenadasCopias.Text = "Coordenadas para copias."
        '
        'btnAplicarConfiguracion
        '
        Me.btnAplicarConfiguracion.Location = New System.Drawing.Point(93, 176)
        Me.btnAplicarConfiguracion.Name = "btnAplicarConfiguracion"
        Me.btnAplicarConfiguracion.Size = New System.Drawing.Size(188, 30)
        Me.btnAplicarConfiguracion.TabIndex = 6
        Me.btnAplicarConfiguracion.Text = "Aplicar"
        Me.btnAplicarConfiguracion.UseVisualStyleBackColor = True
        '
        'lblCantidadDetalle
        '
        Me.lblCantidadDetalle.AutoSize = True
        Me.lblCantidadDetalle.Location = New System.Drawing.Point(35, 33)
        Me.lblCantidadDetalle.Name = "lblCantidadDetalle"
        Me.lblCantidadDetalle.Size = New System.Drawing.Size(58, 13)
        Me.lblCantidadDetalle.TabIndex = 1
        Me.lblCantidadDetalle.Text = "- Cantidad:"
        '
        'lblCopias
        '
        Me.lblCopias.AutoSize = True
        Me.lblCopias.Location = New System.Drawing.Point(10, 59)
        Me.lblCopias.Name = "lblCopias"
        Me.lblCopias.Size = New System.Drawing.Size(81, 13)
        Me.lblCopias.TabIndex = 3
        Me.lblCopias.Text = "- Copias x Hoja:"
        '
        'nudCopia
        '
        Me.nudCopia.Location = New System.Drawing.Point(93, 55)
        Me.nudCopia.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudCopia.Name = "nudCopia"
        Me.nudCopia.Size = New System.Drawing.Size(45, 20)
        Me.nudCopia.TabIndex = 4
        Me.nudCopia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'nudCantidadDetalle
        '
        Me.nudCantidadDetalle.Location = New System.Drawing.Point(93, 29)
        Me.nudCantidadDetalle.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudCantidadDetalle.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudCantidadDetalle.Name = "nudCantidadDetalle"
        Me.nudCantidadDetalle.Size = New System.Drawing.Size(45, 20)
        Me.nudCantidadDetalle.TabIndex = 2
        Me.nudCantidadDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudCantidadDetalle.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'chkDetalle
        '
        Me.chkDetalle.BackColor = System.Drawing.SystemColors.Control
        Me.chkDetalle.Color = System.Drawing.Color.Empty
        Me.chkDetalle.Location = New System.Drawing.Point(11, 6)
        Me.chkDetalle.Name = "chkDetalle"
        Me.chkDetalle.Size = New System.Drawing.Size(134, 15)
        Me.chkDetalle.SoloLectura = False
        Me.chkDetalle.TabIndex = 0
        Me.chkDetalle.Texto = "Documento con detalle"
        Me.chkDetalle.Valor = False
        '
        'ppd
        '
        Me.ppd.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ppd.Location = New System.Drawing.Point(309, 63)
        Me.ppd.Name = "ppd"
        Me.TableLayoutPanel1.SetRowSpan(Me.ppd, 3)
        Me.ppd.Size = New System.Drawing.Size(493, 480)
        Me.ppd.TabIndex = 0
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.nudZoom)
        Me.GroupBox4.Controls.Add(Me.lblZoom)
        Me.GroupBox4.Controls.Add(Me.chkDibujarCuadricula)
        Me.GroupBox4.Controls.Add(Me.btnImprimir)
        Me.GroupBox4.Controls.Add(Me.btnPrevisualizar)
        Me.GroupBox4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox4.Location = New System.Drawing.Point(309, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(493, 54)
        Me.GroupBox4.TabIndex = 5
        Me.GroupBox4.TabStop = False
        '
        'nudZoom
        '
        Me.nudZoom.DecimalPlaces = 2
        Me.nudZoom.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.nudZoom.Location = New System.Drawing.Point(421, 19)
        Me.nudZoom.Maximum = New Decimal(New Integer() {20, 0, 0, 0})
        Me.nudZoom.Minimum = New Decimal(New Integer() {1, 0, 0, 131072})
        Me.nudZoom.Name = "nudZoom"
        Me.nudZoom.Size = New System.Drawing.Size(45, 20)
        Me.nudZoom.TabIndex = 4
        Me.nudZoom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudZoom.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblZoom
        '
        Me.lblZoom.AutoSize = True
        Me.lblZoom.Location = New System.Drawing.Point(373, 23)
        Me.lblZoom.Name = "lblZoom"
        Me.lblZoom.Size = New System.Drawing.Size(42, 13)
        Me.lblZoom.TabIndex = 3
        Me.lblZoom.Text = "ZOOM:"
        '
        'chkDibujarCuadricula
        '
        Me.chkDibujarCuadricula.BackColor = System.Drawing.Color.Transparent
        Me.chkDibujarCuadricula.Color = System.Drawing.Color.Empty
        Me.chkDibujarCuadricula.Location = New System.Drawing.Point(258, 22)
        Me.chkDibujarCuadricula.Name = "chkDibujarCuadricula"
        Me.chkDibujarCuadricula.Size = New System.Drawing.Size(112, 15)
        Me.chkDibujarCuadricula.SoloLectura = False
        Me.chkDibujarCuadricula.TabIndex = 2
        Me.chkDibujarCuadricula.Texto = "Dibujar cuadricula"
        Me.chkDibujarCuadricula.Valor = False
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(127, 15)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(115, 29)
        Me.btnImprimir.TabIndex = 1
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnPrevisualizar
        '
        Me.btnPrevisualizar.Location = New System.Drawing.Point(6, 15)
        Me.btnPrevisualizar.Name = "btnPrevisualizar"
        Me.btnPrevisualizar.Size = New System.Drawing.Size(115, 29)
        Me.btnPrevisualizar.TabIndex = 0
        Me.btnPrevisualizar.Text = "PREVISUALIZAR"
        Me.btnPrevisualizar.UseVisualStyleBackColor = True
        '
        'gbxConfiguracionPapel
        '
        Me.gbxConfiguracionPapel.Controls.Add(Me.nudTamañoPapelY)
        Me.gbxConfiguracionPapel.Controls.Add(Me.nudTamañoPapelX)
        Me.gbxConfiguracionPapel.Controls.Add(Me.txtTamañoPapel)
        Me.gbxConfiguracionPapel.Controls.Add(Me.Label6)
        Me.gbxConfiguracionPapel.Controls.Add(Me.rdbHorizontal)
        Me.gbxConfiguracionPapel.Controls.Add(Me.rdbVertical)
        Me.gbxConfiguracionPapel.Controls.Add(Me.Label5)
        Me.gbxConfiguracionPapel.Controls.Add(Me.Label4)
        Me.gbxConfiguracionPapel.Controls.Add(Me.Label3)
        Me.gbxConfiguracionPapel.Controls.Add(Me.nudMargenAbajo)
        Me.gbxConfiguracionPapel.Controls.Add(Me.Label2)
        Me.gbxConfiguracionPapel.Controls.Add(Me.nudMargenArriba)
        Me.gbxConfiguracionPapel.Controls.Add(Me.Label1)
        Me.gbxConfiguracionPapel.Controls.Add(Me.nudMargenDerecha)
        Me.gbxConfiguracionPapel.Controls.Add(Me.lblMargenIzquierda)
        Me.gbxConfiguracionPapel.Controls.Add(Me.nudMargenIzquierda)
        Me.gbxConfiguracionPapel.Controls.Add(Me.btnEstablecerPapel)
        Me.gbxConfiguracionPapel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxConfiguracionPapel.Location = New System.Drawing.Point(808, 63)
        Me.gbxConfiguracionPapel.Name = "gbxConfiguracionPapel"
        Me.TableLayoutPanel1.SetRowSpan(Me.gbxConfiguracionPapel, 3)
        Me.gbxConfiguracionPapel.Size = New System.Drawing.Size(193, 480)
        Me.gbxConfiguracionPapel.TabIndex = 6
        Me.gbxConfiguracionPapel.TabStop = False
        Me.gbxConfiguracionPapel.Text = "Configuracion de Papel"
        '
        'nudTamañoPapelY
        '
        Me.nudTamañoPapelY.Location = New System.Drawing.Point(84, 109)
        Me.nudTamañoPapelY.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
        Me.nudTamañoPapelY.Name = "nudTamañoPapelY"
        Me.nudTamañoPapelY.ReadOnly = True
        Me.nudTamañoPapelY.Size = New System.Drawing.Size(71, 20)
        Me.nudTamañoPapelY.TabIndex = 16
        Me.nudTamañoPapelY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudTamañoPapelY.Value = New Decimal(New Integer() {800, 0, 0, 0})
        '
        'nudTamañoPapelX
        '
        Me.nudTamañoPapelX.Location = New System.Drawing.Point(7, 109)
        Me.nudTamañoPapelX.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.nudTamañoPapelX.Name = "nudTamañoPapelX"
        Me.nudTamañoPapelX.ReadOnly = True
        Me.nudTamañoPapelX.Size = New System.Drawing.Size(71, 20)
        Me.nudTamañoPapelX.TabIndex = 15
        Me.nudTamañoPapelX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudTamañoPapelX.Value = New Decimal(New Integer() {600, 0, 0, 0})
        '
        'txtTamañoPapel
        '
        Me.txtTamañoPapel.Location = New System.Drawing.Point(6, 83)
        Me.txtTamañoPapel.Name = "txtTamañoPapel"
        Me.txtTamañoPapel.ReadOnly = True
        Me.txtTamañoPapel.Size = New System.Drawing.Size(181, 20)
        Me.txtTamañoPapel.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 67)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(110, 13)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Tamaño de Papel:"
        '
        'rdbHorizontal
        '
        Me.rdbHorizontal.AutoSize = True
        Me.rdbHorizontal.Enabled = False
        Me.rdbHorizontal.Location = New System.Drawing.Point(6, 193)
        Me.rdbHorizontal.Name = "rdbHorizontal"
        Me.rdbHorizontal.Size = New System.Drawing.Size(72, 17)
        Me.rdbHorizontal.TabIndex = 5
        Me.rdbHorizontal.TabStop = True
        Me.rdbHorizontal.Text = "Horizontal"
        Me.rdbHorizontal.UseVisualStyleBackColor = True
        '
        'rdbVertical
        '
        Me.rdbVertical.AutoSize = True
        Me.rdbVertical.Enabled = False
        Me.rdbVertical.Location = New System.Drawing.Point(6, 170)
        Me.rdbVertical.Name = "rdbVertical"
        Me.rdbVertical.Size = New System.Drawing.Size(60, 17)
        Me.rdbVertical.TabIndex = 4
        Me.rdbVertical.TabStop = True
        Me.rdbVertical.Text = "Vertical"
        Me.rdbVertical.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 154)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 13)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Orientacion:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 225)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(66, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Margenes:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(60, 290)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 13)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Abajo:"
        '
        'nudMargenAbajo
        '
        Me.nudMargenAbajo.Location = New System.Drawing.Point(63, 306)
        Me.nudMargenAbajo.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.nudMargenAbajo.Name = "nudMargenAbajo"
        Me.nudMargenAbajo.ReadOnly = True
        Me.nudMargenAbajo.Size = New System.Drawing.Size(50, 20)
        Me.nudMargenAbajo.TabIndex = 14
        Me.nudMargenAbajo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudMargenAbajo.Value = New Decimal(New Integer() {7, 0, 0, 0})
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 290)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Arriba:"
        '
        'nudMargenArriba
        '
        Me.nudMargenArriba.Location = New System.Drawing.Point(6, 306)
        Me.nudMargenArriba.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.nudMargenArriba.Name = "nudMargenArriba"
        Me.nudMargenArriba.ReadOnly = True
        Me.nudMargenArriba.Size = New System.Drawing.Size(50, 20)
        Me.nudMargenArriba.TabIndex = 13
        Me.nudMargenArriba.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudMargenArriba.Value = New Decimal(New Integer() {7, 0, 0, 0})
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(62, 248)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Derecha:"
        '
        'nudMargenDerecha
        '
        Me.nudMargenDerecha.Location = New System.Drawing.Point(65, 264)
        Me.nudMargenDerecha.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.nudMargenDerecha.Name = "nudMargenDerecha"
        Me.nudMargenDerecha.ReadOnly = True
        Me.nudMargenDerecha.Size = New System.Drawing.Size(50, 20)
        Me.nudMargenDerecha.TabIndex = 10
        Me.nudMargenDerecha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudMargenDerecha.Value = New Decimal(New Integer() {7, 0, 0, 0})
        '
        'lblMargenIzquierda
        '
        Me.lblMargenIzquierda.AutoSize = True
        Me.lblMargenIzquierda.Location = New System.Drawing.Point(6, 248)
        Me.lblMargenIzquierda.Name = "lblMargenIzquierda"
        Me.lblMargenIzquierda.Size = New System.Drawing.Size(53, 13)
        Me.lblMargenIzquierda.TabIndex = 7
        Me.lblMargenIzquierda.Text = "Izquierda:"
        '
        'nudMargenIzquierda
        '
        Me.nudMargenIzquierda.Location = New System.Drawing.Point(6, 264)
        Me.nudMargenIzquierda.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.nudMargenIzquierda.Name = "nudMargenIzquierda"
        Me.nudMargenIzquierda.ReadOnly = True
        Me.nudMargenIzquierda.Size = New System.Drawing.Size(50, 20)
        Me.nudMargenIzquierda.TabIndex = 9
        Me.nudMargenIzquierda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudMargenIzquierda.Value = New Decimal(New Integer() {7, 0, 0, 0})
        '
        'btnEstablecerPapel
        '
        Me.btnEstablecerPapel.Location = New System.Drawing.Point(6, 30)
        Me.btnEstablecerPapel.Name = "btnEstablecerPapel"
        Me.btnEstablecerPapel.Size = New System.Drawing.Size(181, 29)
        Me.btnEstablecerPapel.TabIndex = 0
        Me.btnEstablecerPapel.Text = "Establecer"
        Me.btnEstablecerPapel.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(3, 384)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(300, 159)
        Me.TabControl1.TabIndex = 7
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.lklAplicarATodos)
        Me.TabPage4.Controls.Add(Me.lblFuente)
        Me.TabPage4.Controls.Add(Me.lblTipoLetra)
        Me.TabPage4.Controls.Add(Me.cbxFuente)
        Me.TabPage4.Controls.Add(Me.btnAplicar)
        Me.TabPage4.Controls.Add(Me.nudTamaño)
        Me.TabPage4.Controls.Add(Me.pnTipoLetra)
        Me.TabPage4.Controls.Add(Me.lblTamaño)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(292, 133)
        Me.TabPage4.TabIndex = 0
        Me.TabPage4.Text = "Formato"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'lklAplicarATodos
        '
        Me.lklAplicarATodos.AutoSize = True
        Me.lklAplicarATodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklAplicarATodos.Location = New System.Drawing.Point(10, 94)
        Me.lklAplicarATodos.Name = "lklAplicarATodos"
        Me.lklAplicarATodos.Size = New System.Drawing.Size(230, 13)
        Me.lklAplicarATodos.TabIndex = 7
        Me.lklAplicarATodos.TabStop = True
        Me.lklAplicarATodos.Text = "* Aplicar esta configuracion a todos los campos"
        '
        'lblFuente
        '
        Me.lblFuente.AutoSize = True
        Me.lblFuente.Location = New System.Drawing.Point(6, 13)
        Me.lblFuente.Name = "lblFuente"
        Me.lblFuente.Size = New System.Drawing.Size(43, 13)
        Me.lblFuente.TabIndex = 0
        Me.lblFuente.Text = "Fuente:"
        '
        'lblTipoLetra
        '
        Me.lblTipoLetra.AutoSize = True
        Me.lblTipoLetra.Location = New System.Drawing.Point(6, 53)
        Me.lblTipoLetra.Name = "lblTipoLetra"
        Me.lblTipoLetra.Size = New System.Drawing.Size(31, 13)
        Me.lblTipoLetra.TabIndex = 4
        Me.lblTipoLetra.Text = "Tipo:"
        '
        'cbxFuente
        '
        Me.cbxFuente.FormattingEnabled = True
        Me.cbxFuente.Location = New System.Drawing.Point(9, 29)
        Me.cbxFuente.Name = "cbxFuente"
        Me.cbxFuente.Size = New System.Drawing.Size(194, 21)
        Me.cbxFuente.TabIndex = 1
        '
        'btnAplicar
        '
        Me.btnAplicar.Location = New System.Drawing.Point(209, 69)
        Me.btnAplicar.Name = "btnAplicar"
        Me.btnAplicar.Size = New System.Drawing.Size(70, 23)
        Me.btnAplicar.TabIndex = 6
        Me.btnAplicar.Text = "Aplicar"
        Me.btnAplicar.UseVisualStyleBackColor = True
        '
        'nudTamaño
        '
        Me.nudTamaño.Location = New System.Drawing.Point(209, 29)
        Me.nudTamaño.Maximum = New Decimal(New Integer() {36, 0, 0, 0})
        Me.nudTamaño.Minimum = New Decimal(New Integer() {6, 0, 0, 0})
        Me.nudTamaño.Name = "nudTamaño"
        Me.nudTamaño.Size = New System.Drawing.Size(68, 20)
        Me.nudTamaño.TabIndex = 3
        Me.nudTamaño.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudTamaño.Value = New Decimal(New Integer() {7, 0, 0, 0})
        '
        'pnTipoLetra
        '
        Me.pnTipoLetra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnTipoLetra.Controls.Add(Me.rdbCursiva)
        Me.pnTipoLetra.Controls.Add(Me.rdbNegrita)
        Me.pnTipoLetra.Controls.Add(Me.rdbNormal)
        Me.pnTipoLetra.Location = New System.Drawing.Point(9, 69)
        Me.pnTipoLetra.Name = "pnTipoLetra"
        Me.pnTipoLetra.Size = New System.Drawing.Size(194, 22)
        Me.pnTipoLetra.TabIndex = 5
        '
        'rdbCursiva
        '
        Me.rdbCursiva.AutoSize = True
        Me.rdbCursiva.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbCursiva.Location = New System.Drawing.Point(122, 3)
        Me.rdbCursiva.Name = "rdbCursiva"
        Me.rdbCursiva.Size = New System.Drawing.Size(56, 16)
        Me.rdbCursiva.TabIndex = 2
        Me.rdbCursiva.TabStop = True
        Me.rdbCursiva.Text = "Cursiva"
        Me.rdbCursiva.UseVisualStyleBackColor = True
        '
        'rdbNegrita
        '
        Me.rdbNegrita.AutoSize = True
        Me.rdbNegrita.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbNegrita.Location = New System.Drawing.Point(62, 3)
        Me.rdbNegrita.Name = "rdbNegrita"
        Me.rdbNegrita.Size = New System.Drawing.Size(60, 16)
        Me.rdbNegrita.TabIndex = 1
        Me.rdbNegrita.TabStop = True
        Me.rdbNegrita.Text = "Negrita"
        Me.rdbNegrita.UseVisualStyleBackColor = True
        '
        'rdbNormal
        '
        Me.rdbNormal.AutoSize = True
        Me.rdbNormal.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbNormal.Location = New System.Drawing.Point(3, 3)
        Me.rdbNormal.Name = "rdbNormal"
        Me.rdbNormal.Size = New System.Drawing.Size(53, 16)
        Me.rdbNormal.TabIndex = 0
        Me.rdbNormal.TabStop = True
        Me.rdbNormal.Text = "Normal"
        Me.rdbNormal.UseVisualStyleBackColor = True
        '
        'lblTamaño
        '
        Me.lblTamaño.AutoSize = True
        Me.lblTamaño.Location = New System.Drawing.Point(206, 13)
        Me.lblTamaño.Name = "lblTamaño"
        Me.lblTamaño.Size = New System.Drawing.Size(49, 13)
        Me.lblTamaño.TabIndex = 2
        Me.lblTamaño.Text = "Tamaño:"
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.nudInterlineado)
        Me.TabPage5.Controls.Add(Me.lblInterlineado)
        Me.TabPage5.Controls.Add(Me.nudMargenPrimeraLinea)
        Me.TabPage5.Controls.Add(Me.lblMargenPrimeraLinea)
        Me.TabPage5.Controls.Add(Me.nudLineas)
        Me.TabPage5.Controls.Add(Me.lblLineas)
        Me.TabPage5.Controls.Add(Me.chkPuedeCrecer)
        Me.TabPage5.Controls.Add(Me.lblSinLimite)
        Me.TabPage5.Controls.Add(Me.nudLargo)
        Me.TabPage5.Controls.Add(Me.lblLargo)
        Me.TabPage5.Controls.Add(Me.chkNumerico)
        Me.TabPage5.Controls.Add(Me.lblAlineacion)
        Me.TabPage5.Controls.Add(Me.cbxAlineacion)
        Me.TabPage5.Controls.Add(Me.btnAplicarTipo)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(292, 133)
        Me.TabPage5.TabIndex = 1
        Me.TabPage5.Text = "Tipo"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'nudInterlineado
        '
        Me.nudInterlineado.Location = New System.Drawing.Point(154, 108)
        Me.nudInterlineado.Maximum = New Decimal(New Integer() {200, 0, 0, 0})
        Me.nudInterlineado.Name = "nudInterlineado"
        Me.nudInterlineado.Size = New System.Drawing.Size(43, 20)
        Me.nudInterlineado.TabIndex = 12
        Me.nudInterlineado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblInterlineado
        '
        Me.lblInterlineado.AutoSize = True
        Me.lblInterlineado.Location = New System.Drawing.Point(151, 92)
        Me.lblInterlineado.Name = "lblInterlineado"
        Me.lblInterlineado.Size = New System.Drawing.Size(65, 13)
        Me.lblInterlineado.TabIndex = 11
        Me.lblInterlineado.Text = "Interlineado:"
        '
        'nudMargenPrimeraLinea
        '
        Me.nudMargenPrimeraLinea.Location = New System.Drawing.Point(9, 108)
        Me.nudMargenPrimeraLinea.Maximum = New Decimal(New Integer() {200, 0, 0, 0})
        Me.nudMargenPrimeraLinea.Name = "nudMargenPrimeraLinea"
        Me.nudMargenPrimeraLinea.Size = New System.Drawing.Size(43, 20)
        Me.nudMargenPrimeraLinea.TabIndex = 10
        Me.nudMargenPrimeraLinea.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMargenPrimeraLinea
        '
        Me.lblMargenPrimeraLinea.AutoSize = True
        Me.lblMargenPrimeraLinea.Location = New System.Drawing.Point(6, 92)
        Me.lblMargenPrimeraLinea.Name = "lblMargenPrimeraLinea"
        Me.lblMargenPrimeraLinea.Size = New System.Drawing.Size(96, 13)
        Me.lblMargenPrimeraLinea.TabIndex = 9
        Me.lblMargenPrimeraLinea.Text = "Margen 1ra. Linea:"
        '
        'nudLineas
        '
        Me.nudLineas.Location = New System.Drawing.Point(154, 67)
        Me.nudLineas.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudLineas.Name = "nudLineas"
        Me.nudLineas.Size = New System.Drawing.Size(43, 20)
        Me.nudLineas.TabIndex = 8
        Me.nudLineas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLineas
        '
        Me.lblLineas.AutoSize = True
        Me.lblLineas.Location = New System.Drawing.Point(151, 51)
        Me.lblLineas.Name = "lblLineas"
        Me.lblLineas.Size = New System.Drawing.Size(41, 13)
        Me.lblLineas.TabIndex = 5
        Me.lblLineas.Text = "Lineas:"
        '
        'chkPuedeCrecer
        '
        Me.chkPuedeCrecer.AutoSize = True
        Me.chkPuedeCrecer.Location = New System.Drawing.Point(58, 70)
        Me.chkPuedeCrecer.Name = "chkPuedeCrecer"
        Me.chkPuedeCrecer.Size = New System.Drawing.Size(90, 17)
        Me.chkPuedeCrecer.TabIndex = 7
        Me.chkPuedeCrecer.Text = "Puede crecer"
        Me.chkPuedeCrecer.UseVisualStyleBackColor = True
        '
        'lblSinLimite
        '
        Me.lblSinLimite.AutoSize = True
        Me.lblSinLimite.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSinLimite.Location = New System.Drawing.Point(41, 54)
        Me.lblSinLimite.Name = "lblSinLimite"
        Me.lblSinLimite.Size = New System.Drawing.Size(107, 12)
        Me.lblSinLimite.TabIndex = 4
        Me.lblSinLimite.Text = "* Establezca 0 sin limites"
        '
        'nudLargo
        '
        Me.nudLargo.Location = New System.Drawing.Point(9, 69)
        Me.nudLargo.Maximum = New Decimal(New Integer() {200, 0, 0, 0})
        Me.nudLargo.Name = "nudLargo"
        Me.nudLargo.Size = New System.Drawing.Size(43, 20)
        Me.nudLargo.TabIndex = 6
        Me.nudLargo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLargo
        '
        Me.lblLargo.AutoSize = True
        Me.lblLargo.Location = New System.Drawing.Point(6, 53)
        Me.lblLargo.Name = "lblLargo"
        Me.lblLargo.Size = New System.Drawing.Size(37, 13)
        Me.lblLargo.TabIndex = 3
        Me.lblLargo.Text = "Largo:"
        '
        'chkNumerico
        '
        Me.chkNumerico.AutoSize = True
        Me.chkNumerico.Location = New System.Drawing.Point(208, 31)
        Me.chkNumerico.Name = "chkNumerico"
        Me.chkNumerico.Size = New System.Drawing.Size(71, 17)
        Me.chkNumerico.TabIndex = 2
        Me.chkNumerico.Text = "Numerico"
        Me.chkNumerico.UseVisualStyleBackColor = True
        '
        'lblAlineacion
        '
        Me.lblAlineacion.AutoSize = True
        Me.lblAlineacion.Location = New System.Drawing.Point(6, 13)
        Me.lblAlineacion.Name = "lblAlineacion"
        Me.lblAlineacion.Size = New System.Drawing.Size(59, 13)
        Me.lblAlineacion.TabIndex = 0
        Me.lblAlineacion.Text = "Alineacion:"
        '
        'cbxAlineacion
        '
        Me.cbxAlineacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxAlineacion.FormattingEnabled = True
        Me.cbxAlineacion.Items.AddRange(New Object() {"Izquierda", "Derecha", "Centrada "})
        Me.cbxAlineacion.Location = New System.Drawing.Point(9, 29)
        Me.cbxAlineacion.Name = "cbxAlineacion"
        Me.cbxAlineacion.Size = New System.Drawing.Size(193, 21)
        Me.cbxAlineacion.TabIndex = 1
        '
        'btnAplicarTipo
        '
        Me.btnAplicarTipo.Location = New System.Drawing.Point(216, 105)
        Me.btnAplicarTipo.Name = "btnAplicarTipo"
        Me.btnAplicarTipo.Size = New System.Drawing.Size(70, 23)
        Me.btnAplicarTipo.TabIndex = 13
        Me.btnAplicarTipo.Text = "Aplicar"
        Me.btnAplicarTipo.UseVisualStyleBackColor = True
        '
        'gbxImpresora
        '
        Me.gbxImpresora.Controls.Add(Me.txtImpresora)
        Me.gbxImpresora.Controls.Add(Me.lklSeleccionarImpresora)
        Me.gbxImpresora.Location = New System.Drawing.Point(808, 3)
        Me.gbxImpresora.Name = "gbxImpresora"
        Me.gbxImpresora.Size = New System.Drawing.Size(193, 54)
        Me.gbxImpresora.TabIndex = 8
        Me.gbxImpresora.TabStop = False
        Me.gbxImpresora.Text = "Impresora"
        '
        'txtImpresora
        '
        Me.txtImpresora.Location = New System.Drawing.Point(6, 17)
        Me.txtImpresora.Name = "txtImpresora"
        Me.txtImpresora.ReadOnly = True
        Me.txtImpresora.Size = New System.Drawing.Size(181, 20)
        Me.txtImpresora.TabIndex = 0
        '
        'lklSeleccionarImpresora
        '
        Me.lklSeleccionarImpresora.AutoSize = True
        Me.lklSeleccionarImpresora.Location = New System.Drawing.Point(75, 38)
        Me.lklSeleccionarImpresora.Name = "lklSeleccionarImpresora"
        Me.lklSeleccionarImpresora.Size = New System.Drawing.Size(111, 13)
        Me.lklSeleccionarImpresora.TabIndex = 1
        Me.lklSeleccionarImpresora.TabStop = True
        Me.lklSeleccionarImpresora.Text = "Seleccionar impresora"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.ToolStripButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(64, 22)
        Me.ToolStripButton2.Text = "CLONAR"
        Me.ToolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay
        '
        'frmImpresion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1004, 564)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmImpresion"
        Me.Text = "frmImpresion"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.gbxDocumento.ResumeLayout(False)
        Me.gbxDocumento.PerformLayout()
        Me.tsDocumento.ResumeLayout(False)
        Me.tsDocumento.PerformLayout()
        Me.tcFormulario.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dgvCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tsCabecera.ResumeLayout(False)
        Me.tsCabecera.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tsDetalle.ResumeLayout(False)
        Me.tsDetalle.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.nudCopia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudCantidadDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.nudZoom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxConfiguracionPapel.ResumeLayout(False)
        Me.gbxConfiguracionPapel.PerformLayout()
        CType(Me.nudTamañoPapelY, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudTamañoPapelX, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMargenAbajo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMargenArriba, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMargenDerecha, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMargenIzquierda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.nudTamaño, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnTipoLetra.ResumeLayout(False)
        Me.pnTipoLetra.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        CType(Me.nudInterlineado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMargenPrimeraLinea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudLineas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudLargo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxImpresora.ResumeLayout(False)
        Me.gbxImpresora.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents gbxDocumento As System.Windows.Forms.GroupBox
    Friend WithEvents cbxDocumento As ERP.ocxCBX
    Friend WithEvents tcFormulario As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents tsCabecera As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnEliminarCabecera As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsDetalle As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnEliminarCampoDetalle As System.Windows.Forms.ToolStripButton
    Friend WithEvents dgvCabecera As System.Windows.Forms.DataGridView
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents lblTipoLetra As System.Windows.Forms.Label
    Friend WithEvents btnAplicar As System.Windows.Forms.Button
    Friend WithEvents pnTipoLetra As System.Windows.Forms.Panel
    Friend WithEvents rdbCursiva As System.Windows.Forms.RadioButton
    Friend WithEvents rdbNegrita As System.Windows.Forms.RadioButton
    Friend WithEvents rdbNormal As System.Windows.Forms.RadioButton
    Friend WithEvents lblTamaño As System.Windows.Forms.Label
    Friend WithEvents nudTamaño As System.Windows.Forms.NumericUpDown
    Friend WithEvents cbxFuente As System.Windows.Forms.ComboBox
    Friend WithEvents lblFuente As System.Windows.Forms.Label
    Friend WithEvents ppd As System.Windows.Forms.PrintPreviewControl
    Friend WithEvents lklAplicarATodos As System.Windows.Forms.LinkLabel
    Friend WithEvents btnAplicarConfiguracion As System.Windows.Forms.Button
    Friend WithEvents nudCopia As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblCopias As System.Windows.Forms.Label
    Friend WithEvents nudCantidadDetalle As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblCantidadDetalle As System.Windows.Forms.Label
    Friend WithEvents chkDetalle As ERP.ocxCHK
    Friend WithEvents lklCorrdenadasCopias As System.Windows.Forms.LinkLabel
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents nudZoom As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblZoom As System.Windows.Forms.Label
    Friend WithEvents chkDibujarCuadricula As ERP.ocxCHK
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnPrevisualizar As System.Windows.Forms.Button
    Friend WithEvents tsDocumento As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton7 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton8 As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnActivarDesactivar As System.Windows.Forms.ToolStripButton
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents gbxConfiguracionPapel As System.Windows.Forms.GroupBox
    Friend WithEvents btnEstablecerPapel As System.Windows.Forms.Button
    Friend WithEvents PageSetupDialog1 As System.Windows.Forms.PageSetupDialog
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents nudMargenAbajo As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents nudMargenArriba As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents nudMargenDerecha As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMargenIzquierda As System.Windows.Forms.Label
    Friend WithEvents nudMargenIzquierda As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents rdbHorizontal As System.Windows.Forms.RadioButton
    Friend WithEvents rdbVertical As System.Windows.Forms.RadioButton
    Friend WithEvents txtTamañoPapel As System.Windows.Forms.TextBox
    Friend WithEvents nudTamañoPapelY As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudTamañoPapelX As System.Windows.Forms.NumericUpDown
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents lblAlineacion As System.Windows.Forms.Label
    Friend WithEvents cbxAlineacion As System.Windows.Forms.ComboBox
    Friend WithEvents btnAplicarTipo As System.Windows.Forms.Button
    Friend WithEvents nudLargo As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblLargo As System.Windows.Forms.Label
    Friend WithEvents chkNumerico As System.Windows.Forms.CheckBox
    Friend WithEvents lblSinLimite As System.Windows.Forms.Label
    Friend WithEvents nudLineas As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblLineas As System.Windows.Forms.Label
    Friend WithEvents chkPuedeCrecer As System.Windows.Forms.CheckBox
    Friend WithEvents nudMargenPrimeraLinea As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMargenPrimeraLinea As System.Windows.Forms.Label
    Friend WithEvents nudInterlineado As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblInterlineado As System.Windows.Forms.Label
    Friend WithEvents gbxImpresora As System.Windows.Forms.GroupBox
    Friend WithEvents txtImpresora As System.Windows.Forms.TextBox
    Friend WithEvents lklSeleccionarImpresora As System.Windows.Forms.LinkLabel
    Friend WithEvents ToolStripButton2 As ToolStripButton
End Class

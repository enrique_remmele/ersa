﻿Public Class frmImpresionAgregarCampo

    'CLASES
    Dim Csistema As New CSistema

    'PROPIEDADES
    Property Seleccionado As Boolean = False
    Property Vista As String = ""
    Property IDFormularioImpresion As Integer = 0
    Property Detalle As Boolean = False

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Funciones
        Listar()

        dgv.Focus()

    End Sub

    Sub Listar()

        Dim SQL As String = "Select Top(0) * From " & Vista
        Dim dt As DataTable = Csistema.ExecuteToDataTable(SQL)

        For c As Integer = 0 To dt.Columns.Count - 1
            dgv.Rows.Add(dt.Columns(c).ColumnName)
        Next

    End Sub

    Sub Procesar()

        'Validar
        If dgv.SelectedRows.Count = 0 Then
            MessageBox.Show("Seleccione un campo para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim SQL As String = "Exec SpDetalleFormularioImpresion "
        Dim Campo As String = dgv.SelectedRows(0).Cells("Campo").Value

        'Entidad
        Csistema.ConcatenarParametro(SQL, "@IDFormularioImpresion", IDFormularioImpresion)
        Csistema.ConcatenarParametro(SQL, "@NombreCampo", Campo)
        Csistema.ConcatenarParametro(SQL, "@Campo", Campo)
        Csistema.ConcatenarParametro(SQL, "@Valor", txtEjemplo.Text.Trim)
        Csistema.ConcatenarParametro(SQL, "@Detalle", Detalle.ToString)
        Csistema.ConcatenarParametro(SQL, "@Operacion", "INS")

        'Ejecutar
        Dim dt As DataTable = Csistema.ExecuteToDataTable(SQL)

        'Verificar si se produzco un error
        If dt Is Nothing Then
            MessageBox.Show("Error con la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Verificar si se produzco un error
        If dt.Rows.Count = 0 Then
            MessageBox.Show("Error con la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Mensaje
        If dt.Rows(0)("Procesado") = True Then
            MessageBox.Show(dt.Rows(0)("Mensaje"), "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Seleccionado = True
            Me.Close()
        Else
            MessageBox.Show(dt.Rows(0)("Mensaje"), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Seleccionado = False
            Exit Sub
        End If

    End Sub

    Sub Cancelar()

    End Sub

    Private Sub frmImpresionAgregarCabecera_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Procesar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    'FA 28/05/2021
    Sub frmImpresionAgregarCampo_Activate()
        Me.Refresh()
    End Sub

End Class
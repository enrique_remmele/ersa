﻿Public Class frmConsultarTransacciones
    Dim CSistema As New CSistema
    Dim dt As New DataTable()

    Sub Inicializar()
        CSistema.SqlToComboBox(cbxUsuario, "Select ID, Nombre from Usuario")
        CSistema.SqlToComboBox(cbxOperacion, "Select ID, Descripcion from Operacion")
    End Sub

    Sub Listar()
        Dim where As String = ""
        If IsDate(txtDesde.Text) = False Then
            MessageBox.Show("Seleccione correctamente una fecha desde", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtDesde.Focus()
        End If

        If IsDate(txtHasta.Text) = False Then
            MessageBox.Show("Seleccione correctamente una fecha desde", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtHasta.Focus()
        End If

        If chkOperacion.Checked Then
            where = " and IDOperacion = " & cbxOperacion.SelectedValue
        End If

        If chkUsuario.Checked Then
            where = " and IDUsuario = " & cbxUsuario.SelectedValue
        End If


        dt = CSistema.ExecuteToDataTable("Select * from vtransaccion where fecha between '" & CSistema.FormatoFechaBaseDatos(txtDesde.Text, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHasta.Text, True, False) & "'")

    End Sub

    Sub Exportar()
        CSistema.dtToExcel2(dt, "Listado de Operaciones")
    End Sub

    Private Sub frmConsultarTransacciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        Exportar()
    End Sub

    'FA 28/05/2021
    Sub frmConsultarTransacciones_Activate()
        Me.Refresh()
    End Sub

End Class
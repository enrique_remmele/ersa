﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccesoActualizacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.gbxUsuario = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblContraseña = New System.Windows.Forms.Label()
        Me.lblUsuario = New System.Windows.Forms.Label()
        Me.txtPath = New ERP.ocxTXTString()
        Me.txtContraseña = New ERP.ocxTXTString()
        Me.txtUsuario = New ERP.ocxTXTString()
        Me.gbxUsuario.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(293, 129)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 2
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(203, 129)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 1
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'gbxUsuario
        '
        Me.gbxUsuario.Controls.Add(Me.txtPath)
        Me.gbxUsuario.Controls.Add(Me.Label1)
        Me.gbxUsuario.Controls.Add(Me.txtContraseña)
        Me.gbxUsuario.Controls.Add(Me.lblContraseña)
        Me.gbxUsuario.Controls.Add(Me.txtUsuario)
        Me.gbxUsuario.Controls.Add(Me.lblUsuario)
        Me.gbxUsuario.Location = New System.Drawing.Point(12, 12)
        Me.gbxUsuario.Name = "gbxUsuario"
        Me.gbxUsuario.Size = New System.Drawing.Size(363, 111)
        Me.gbxUsuario.TabIndex = 0
        Me.gbxUsuario.TabStop = False
        Me.gbxUsuario.Text = "CUENTA DE ACCESO"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(53, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Path:"
        '
        'lblContraseña
        '
        Me.lblContraseña.AutoSize = True
        Me.lblContraseña.Location = New System.Drawing.Point(21, 82)
        Me.lblContraseña.Name = "lblContraseña"
        Me.lblContraseña.Size = New System.Drawing.Size(64, 13)
        Me.lblContraseña.TabIndex = 4
        Me.lblContraseña.Text = "Contraseña:"
        '
        'lblUsuario
        '
        Me.lblUsuario.AutoSize = True
        Me.lblUsuario.Location = New System.Drawing.Point(39, 55)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuario.TabIndex = 2
        Me.lblUsuario.Text = "Usuario:"
        '
        'txtPath
        '
        Me.txtPath.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPath.Color = System.Drawing.Color.Empty
        Me.txtPath.Indicaciones = Nothing
        Me.txtPath.Location = New System.Drawing.Point(93, 24)
        Me.txtPath.Multilinea = False
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(263, 21)
        Me.txtPath.SoloLectura = False
        Me.txtPath.TabIndex = 1
        Me.txtPath.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPath.Texto = ""
        '
        'txtContraseña
        '
        Me.txtContraseña.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtContraseña.Color = System.Drawing.Color.Empty
        Me.txtContraseña.Indicaciones = Nothing
        Me.txtContraseña.Location = New System.Drawing.Point(93, 78)
        Me.txtContraseña.Multilinea = False
        Me.txtContraseña.Name = "txtContraseña"
        Me.txtContraseña.Size = New System.Drawing.Size(263, 21)
        Me.txtContraseña.SoloLectura = False
        Me.txtContraseña.TabIndex = 5
        Me.txtContraseña.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtContraseña.Texto = ""
        '
        'txtUsuario
        '
        Me.txtUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtUsuario.Color = System.Drawing.Color.Empty
        Me.txtUsuario.Indicaciones = Nothing
        Me.txtUsuario.Location = New System.Drawing.Point(93, 51)
        Me.txtUsuario.Multilinea = False
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(263, 21)
        Me.txtUsuario.SoloLectura = False
        Me.txtUsuario.TabIndex = 3
        Me.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUsuario.Texto = ""
        '
        'frmAccesoActualizacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(387, 158)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.gbxUsuario)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmAccesoActualizacion"
        Me.Text = "Acceso a las actualizaciones"
        Me.gbxUsuario.ResumeLayout(False)
        Me.gbxUsuario.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents gbxUsuario As System.Windows.Forms.GroupBox
    Friend WithEvents txtPath As ERP.ocxTXTString
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtContraseña As ERP.ocxTXTString
    Friend WithEvents lblContraseña As System.Windows.Forms.Label
    Friend WithEvents txtUsuario As ERP.ocxTXTString
    Friend WithEvents lblUsuario As System.Windows.Forms.Label
End Class

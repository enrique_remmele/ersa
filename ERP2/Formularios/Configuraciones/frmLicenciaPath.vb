﻿Public Class frmLicenciaPath

    'Clases
    Dim CArchivoInicio As New CArchivoInicio

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        OpenFileDialog1.ShowDialog()
        txtArchivo.txt.CharacterCasing = CharacterCasing.Normal
        txtArchivo.txt.Text = OpenFileDialog1.FileName
        txtArchivo.txt.SelectAll()

    End Sub

    Private Sub btnAplicar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAplicar.Click

        'Corroboramos que exista el archivo
        If My.Computer.FileSystem.FileExists(txtArchivo.GetValue) = False Then
            MessageBox.Show("El archivo no existe!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        'Hacemos una copia ne nuestro disco
        My.Computer.FileSystem.CopyFile(txtArchivo.GetValue, vgLicenciaPath, True)

        'Volvemos a corroborar
        If My.Computer.FileSystem.FileExists(txtArchivo.GetValue) = False Then
            MessageBox.Show("El sistema no tiene acceso a la carpera del sistema <" & VGCarpetaAplicacion & "> Asignar los permisos correspondientes para continuar! ", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End
        End If

        Me.Close()

    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    'FA 28/05/2021
    Sub frmLicenciaPath_Activate()
        Me.Refresh()
    End Sub

End Class
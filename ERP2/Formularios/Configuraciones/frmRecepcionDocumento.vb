﻿Public Class frmRecepcionDocumento

    Dim CSistema As New CSistema
    Dim dtRecepcionDocumento As New DataTable
    Public Property IDTransaccion As Integer
    Public Property Comprobante As String

    Sub Inicializar()
        Cargar()
    End Sub

    Sub Cargar()

        dtRecepcionDocumento = CSistema.ExecuteToDataTable("Select * from vRecepcionDocumento where IDTransaccion = " & IDTransaccion)

        txtDocumentoRelacionado.Text = Comprobante

        If dtRecepcionDocumento Is Nothing Then
            Exit Sub
        End If

        If dtRecepcionDocumento.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim Row As DataRow = dtRecepcionDocumento(0)

        txtNombreRecibido.Text = Row("NombreRecibido")
        txtFechaRecibido.Text = Row("FechaRecibido")
        txtObservacionRecibido.Text = Row("Observacion")
        lblUsuarioRegistro.Text = Row("Usuario")
        lblFechaRegistro.Text = Row("FechaRegistro")


        'Verificar Permisos
        Dim SoloLectura As Boolean = CSistema.ExecuteScalar("Select ISnull((Select 1 from UsuarioRecepcionDocumento where idusuario = " & vgIDUsuario & "), 0)")
        If SoloLectura Then
            btnGuardar.Enabled = True
        Else
            btnGuardar.Enabled = False
        End If

    End Sub

    Sub Guardar()
        Dim Operacion As String = ""

        If dtRecepcionDocumento Is Nothing Then
            Operacion = "INS"
        Else
            If dtRecepcionDocumento.Rows.Count = 0 Then
                Operacion = "INS"
            Else
                Operacion = "UPD"
            End If
        End If

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@NombreRecibido", txtNombreRecibido.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaRecibido", txtFechaRecibido.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacionRecibido.Text.Trim, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpRecepcionDocumento", False, False, MensajeRetorno) = False Then
            MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            Exit Sub

        End If

    End Sub

    Private Sub frmRecepcionDocumento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    'FA 28/05/2021
    Sub frmRecepcionDocumento_Activate()
        Me.Refresh()
    End Sub

End Class
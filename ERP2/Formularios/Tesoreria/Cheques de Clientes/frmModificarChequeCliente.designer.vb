﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificarChequeCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblDocumentosPagados = New System.Windows.Forms.Label()
        Me.lblDiferido = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.lblSaldo = New System.Windows.Forms.Label()
        Me.chkChequeTercero = New System.Windows.Forms.CheckBox()
        Me.chkDiferido = New System.Windows.Forms.CheckBox()
        Me.lblVencimiento = New System.Windows.Forms.Label()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.gbxDatos = New System.Windows.Forms.GroupBox()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.txtEstado = New ERP.ocxTXTString()
        Me.cbxDocumentosPagados = New ERP.ocxCBX()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.txtLiberador = New ERP.ocxTXTString()
        Me.txtVencimiento = New ERP.ocxTXTDate()
        Me.txtImporteMoneda = New ERP.ocxTXTNumeric()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.txtNroCheque = New ERP.ocxTXTString()
        Me.lblNroCheque = New System.Windows.Forms.Label()
        Me.cbxNroCuentaBancaria = New ERP.ocxCBX()
        Me.lblNroCuentaBancaria = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.cbxBanco = New ERP.ocxCBX()
        Me.lblBanco = New System.Windows.Forms.Label()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.txtTitular = New ERP.ocxTXTString()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnmodificar = New System.Windows.Forms.Button()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDatos.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 225)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(647, 22)
        Me.StatusStrip1.TabIndex = 6
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(4, 191)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(126, 23)
        Me.btnBuscar.TabIndex = 2
        Me.btnBuscar.Text = "&Busqueda Avanzada"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(568, 191)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 5
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(300, 103)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Cambio:"
        '
        'lblDocumentosPagados
        '
        Me.lblDocumentosPagados.AutoSize = True
        Me.lblDocumentosPagados.Location = New System.Drawing.Point(392, 155)
        Me.lblDocumentosPagados.Name = "lblDocumentosPagados"
        Me.lblDocumentosPagados.Size = New System.Drawing.Size(72, 13)
        Me.lblDocumentosPagados.TabIndex = 32
        Me.lblDocumentosPagados.Text = "Doc. pagado:"
        '
        'lblDiferido
        '
        Me.lblDiferido.AutoSize = True
        Me.lblDiferido.Location = New System.Drawing.Point(30, 129)
        Me.lblDiferido.Name = "lblDiferido"
        Me.lblDiferido.Size = New System.Drawing.Size(46, 13)
        Me.lblDiferido.TabIndex = 22
        Me.lblDiferido.Text = "Diferido:"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(198, 155)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 30
        Me.lblEstado.Text = "Estado:"
        '
        'lblSaldo
        '
        Me.lblSaldo.AutoSize = True
        Me.lblSaldo.Location = New System.Drawing.Point(39, 155)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(37, 13)
        Me.lblSaldo.TabIndex = 28
        Me.lblSaldo.Text = "Saldo:"
        '
        'chkChequeTercero
        '
        Me.chkChequeTercero.AutoSize = True
        Me.chkChequeTercero.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkChequeTercero.Location = New System.Drawing.Point(222, 127)
        Me.chkChequeTercero.Name = "chkChequeTercero"
        Me.chkChequeTercero.Size = New System.Drawing.Size(121, 17)
        Me.chkChequeTercero.TabIndex = 26
        Me.chkChequeTercero.Text = "Cheque de Tercero:"
        Me.chkChequeTercero.UseVisualStyleBackColor = True
        '
        'chkDiferido
        '
        Me.chkDiferido.AutoSize = True
        Me.chkDiferido.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkDiferido.Location = New System.Drawing.Point(80, 128)
        Me.chkDiferido.Name = "chkDiferido"
        Me.chkDiferido.Size = New System.Drawing.Size(15, 14)
        Me.chkDiferido.TabIndex = 23
        Me.chkDiferido.UseVisualStyleBackColor = True
        '
        'lblVencimiento
        '
        Me.lblVencimiento.AutoSize = True
        Me.lblVencimiento.Location = New System.Drawing.Point(97, 129)
        Me.lblVencimiento.Name = "lblVencimiento"
        Me.lblVencimiento.Size = New System.Drawing.Size(38, 13)
        Me.lblVencimiento.TabIndex = 24
        Me.lblVencimiento.Text = "Venc.:"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(390, 103)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(45, 13)
        Me.lblImporte.TabIndex = 19
        Me.lblImporte.Text = "Importe:"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(198, 103)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 15
        Me.lblMoneda.Text = "Moneda:"
        '
        'gbxDatos
        '
        Me.gbxDatos.Controls.Add(Me.txtCliente)
        Me.gbxDatos.Controls.Add(Me.Label2)
        Me.gbxDatos.Controls.Add(Me.txtFecha)
        Me.gbxDatos.Controls.Add(Me.Label3)
        Me.gbxDatos.Controls.Add(Me.cbxSucursal)
        Me.gbxDatos.Controls.Add(Me.txtImporte)
        Me.gbxDatos.Controls.Add(Me.Label1)
        Me.gbxDatos.Controls.Add(Me.txtEstado)
        Me.gbxDatos.Controls.Add(Me.cbxDocumentosPagados)
        Me.gbxDatos.Controls.Add(Me.lblDocumentosPagados)
        Me.gbxDatos.Controls.Add(Me.lblDiferido)
        Me.gbxDatos.Controls.Add(Me.lblEstado)
        Me.gbxDatos.Controls.Add(Me.txtSaldo)
        Me.gbxDatos.Controls.Add(Me.lblSaldo)
        Me.gbxDatos.Controls.Add(Me.txtLiberador)
        Me.gbxDatos.Controls.Add(Me.chkChequeTercero)
        Me.gbxDatos.Controls.Add(Me.chkDiferido)
        Me.gbxDatos.Controls.Add(Me.txtVencimiento)
        Me.gbxDatos.Controls.Add(Me.lblVencimiento)
        Me.gbxDatos.Controls.Add(Me.txtImporteMoneda)
        Me.gbxDatos.Controls.Add(Me.lblImporte)
        Me.gbxDatos.Controls.Add(Me.txtCotizacion)
        Me.gbxDatos.Controls.Add(Me.cbxMoneda)
        Me.gbxDatos.Controls.Add(Me.lblMoneda)
        Me.gbxDatos.Controls.Add(Me.txtNroCheque)
        Me.gbxDatos.Controls.Add(Me.lblNroCheque)
        Me.gbxDatos.Controls.Add(Me.cbxNroCuentaBancaria)
        Me.gbxDatos.Controls.Add(Me.lblNroCuentaBancaria)
        Me.gbxDatos.Controls.Add(Me.txtID)
        Me.gbxDatos.Controls.Add(Me.lblOperacion)
        Me.gbxDatos.Controls.Add(Me.cbxBanco)
        Me.gbxDatos.Controls.Add(Me.lblBanco)
        Me.gbxDatos.Controls.Add(Me.lblCliente)
        Me.gbxDatos.Controls.Add(Me.txtTitular)
        Me.gbxDatos.Location = New System.Drawing.Point(4, 1)
        Me.gbxDatos.Name = "gbxDatos"
        Me.gbxDatos.Size = New System.Drawing.Size(639, 182)
        Me.gbxDatos.TabIndex = 0
        Me.gbxDatos.TabStop = False
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 140
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(259, 18)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(374, 23)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(36, 49)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Titular:"
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 6, 26, 11, 37, 29, 740)
        Me.txtFecha.Location = New System.Drawing.Point(564, 47)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(67, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(524, 51)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Fecha:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(80, 19)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(58, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = False
        Me.txtImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporte.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporte.Location = New System.Drawing.Point(541, 99)
        Me.txtImporte.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(90, 21)
        Me.txtImporte.SoloLectura = True
        Me.txtImporte.TabIndex = 21
        Me.txtImporte.TabStop = False
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'txtEstado
        '
        Me.txtEstado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstado.Color = System.Drawing.Color.Empty
        Me.txtEstado.Indicaciones = Nothing
        Me.txtEstado.Location = New System.Drawing.Point(241, 151)
        Me.txtEstado.Multilinea = False
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.Size = New System.Drawing.Size(145, 21)
        Me.txtEstado.SoloLectura = True
        Me.txtEstado.TabIndex = 31
        Me.txtEstado.TabStop = False
        Me.txtEstado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEstado.Texto = ""
        '
        'cbxDocumentosPagados
        '
        Me.cbxDocumentosPagados.CampoWhere = Nothing
        Me.cbxDocumentosPagados.DataDisplayMember = Nothing
        Me.cbxDocumentosPagados.DataFilter = Nothing
        Me.cbxDocumentosPagados.DataOrderBy = Nothing
        Me.cbxDocumentosPagados.DataSource = Nothing
        Me.cbxDocumentosPagados.DataValueMember = Nothing
        Me.cbxDocumentosPagados.FormABM = Nothing
        Me.cbxDocumentosPagados.Indicaciones = Nothing
        Me.cbxDocumentosPagados.Location = New System.Drawing.Point(469, 151)
        Me.cbxDocumentosPagados.Name = "cbxDocumentosPagados"
        Me.cbxDocumentosPagados.SeleccionObligatoria = True
        Me.cbxDocumentosPagados.Size = New System.Drawing.Size(162, 21)
        Me.cbxDocumentosPagados.SoloLectura = False
        Me.cbxDocumentosPagados.TabIndex = 33
        Me.cbxDocumentosPagados.Texto = ""
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = False
        Me.txtSaldo.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtSaldo.Location = New System.Drawing.Point(80, 151)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(109, 21)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 29
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'txtLiberador
        '
        Me.txtLiberador.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLiberador.Color = System.Drawing.Color.Empty
        Me.txtLiberador.Indicaciones = Nothing
        Me.txtLiberador.Location = New System.Drawing.Point(349, 125)
        Me.txtLiberador.Multilinea = False
        Me.txtLiberador.Name = "txtLiberador"
        Me.txtLiberador.Size = New System.Drawing.Size(282, 21)
        Me.txtLiberador.SoloLectura = False
        Me.txtLiberador.TabIndex = 27
        Me.txtLiberador.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtLiberador.Texto = ""
        '
        'txtVencimiento
        '
        Me.txtVencimiento.Color = System.Drawing.Color.Empty
        Me.txtVencimiento.Fecha = New Date(2013, 6, 26, 11, 51, 34, 222)
        Me.txtVencimiento.Location = New System.Drawing.Point(133, 125)
        Me.txtVencimiento.Name = "txtVencimiento"
        Me.txtVencimiento.PermitirNulo = False
        Me.txtVencimiento.Size = New System.Drawing.Size(83, 20)
        Me.txtVencimiento.SoloLectura = False
        Me.txtVencimiento.TabIndex = 25
        '
        'txtImporteMoneda
        '
        Me.txtImporteMoneda.Color = System.Drawing.Color.Empty
        Me.txtImporteMoneda.Decimales = False
        Me.txtImporteMoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteMoneda.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporteMoneda.Location = New System.Drawing.Point(435, 99)
        Me.txtImporteMoneda.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtImporteMoneda.Name = "txtImporteMoneda"
        Me.txtImporteMoneda.Size = New System.Drawing.Size(98, 21)
        Me.txtImporteMoneda.SoloLectura = False
        Me.txtImporteMoneda.TabIndex = 20
        Me.txtImporteMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteMoneda.Texto = "0"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(345, 99)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(39, 21)
        Me.txtCotizacion.SoloLectura = False
        Me.txtCotizacion.TabIndex = 18
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "0"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = Nothing
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(247, 99)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(53, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 16
        Me.cbxMoneda.Texto = ""
        '
        'txtNroCheque
        '
        Me.txtNroCheque.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroCheque.Color = System.Drawing.Color.Empty
        Me.txtNroCheque.Indicaciones = Nothing
        Me.txtNroCheque.Location = New System.Drawing.Point(80, 99)
        Me.txtNroCheque.Multilinea = False
        Me.txtNroCheque.Name = "txtNroCheque"
        Me.txtNroCheque.Size = New System.Drawing.Size(109, 21)
        Me.txtNroCheque.SoloLectura = False
        Me.txtNroCheque.TabIndex = 14
        Me.txtNroCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroCheque.Texto = ""
        '
        'lblNroCheque
        '
        Me.lblNroCheque.AutoSize = True
        Me.lblNroCheque.Location = New System.Drawing.Point(6, 103)
        Me.lblNroCheque.Name = "lblNroCheque"
        Me.lblNroCheque.Size = New System.Drawing.Size(70, 13)
        Me.lblNroCheque.TabIndex = 13
        Me.lblNroCheque.Text = "Nro. Cheque:"
        '
        'cbxNroCuentaBancaria
        '
        Me.cbxNroCuentaBancaria.CampoWhere = Nothing
        Me.cbxNroCuentaBancaria.DataDisplayMember = Nothing
        Me.cbxNroCuentaBancaria.DataFilter = Nothing
        Me.cbxNroCuentaBancaria.DataOrderBy = Nothing
        Me.cbxNroCuentaBancaria.DataSource = Nothing
        Me.cbxNroCuentaBancaria.DataValueMember = Nothing
        Me.cbxNroCuentaBancaria.FormABM = Nothing
        Me.cbxNroCuentaBancaria.Indicaciones = Nothing
        Me.cbxNroCuentaBancaria.Location = New System.Drawing.Point(386, 74)
        Me.cbxNroCuentaBancaria.Name = "cbxNroCuentaBancaria"
        Me.cbxNroCuentaBancaria.SeleccionObligatoria = True
        Me.cbxNroCuentaBancaria.Size = New System.Drawing.Size(138, 21)
        Me.cbxNroCuentaBancaria.SoloLectura = False
        Me.cbxNroCuentaBancaria.TabIndex = 12
        Me.cbxNroCuentaBancaria.Texto = ""
        '
        'lblNroCuentaBancaria
        '
        Me.lblNroCuentaBancaria.AutoSize = True
        Me.lblNroCuentaBancaria.Location = New System.Drawing.Point(274, 78)
        Me.lblNroCuentaBancaria.Name = "lblNroCuentaBancaria"
        Me.lblNroCuentaBancaria.Size = New System.Drawing.Size(112, 13)
        Me.lblNroCuentaBancaria.TabIndex = 11
        Me.lblNroCuentaBancaria.Text = "Nro. Cuenta Bancaria:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(143, 19)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(74, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(17, 23)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'cbxBanco
        '
        Me.cbxBanco.CampoWhere = Nothing
        Me.cbxBanco.DataDisplayMember = Nothing
        Me.cbxBanco.DataFilter = Nothing
        Me.cbxBanco.DataOrderBy = Nothing
        Me.cbxBanco.DataSource = Nothing
        Me.cbxBanco.DataValueMember = Nothing
        Me.cbxBanco.FormABM = Nothing
        Me.cbxBanco.Indicaciones = Nothing
        Me.cbxBanco.Location = New System.Drawing.Point(80, 74)
        Me.cbxBanco.Name = "cbxBanco"
        Me.cbxBanco.SeleccionObligatoria = True
        Me.cbxBanco.Size = New System.Drawing.Size(188, 21)
        Me.cbxBanco.SoloLectura = False
        Me.cbxBanco.TabIndex = 10
        Me.cbxBanco.Texto = ""
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(35, 78)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(41, 13)
        Me.lblBanco.TabIndex = 9
        Me.lblBanco.Text = "Banco:"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(217, 23)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(42, 13)
        Me.lblCliente.TabIndex = 3
        Me.lblCliente.Text = "Cliente:"
        '
        'txtTitular
        '
        Me.txtTitular.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTitular.Color = System.Drawing.Color.Empty
        Me.txtTitular.Indicaciones = Nothing
        Me.txtTitular.Location = New System.Drawing.Point(80, 46)
        Me.txtTitular.Multilinea = False
        Me.txtTitular.Name = "txtTitular"
        Me.txtTitular.Size = New System.Drawing.Size(438, 21)
        Me.txtTitular.SoloLectura = False
        Me.txtTitular.TabIndex = 6
        Me.txtTitular.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTitular.Texto = ""
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(487, 191)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 4
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(406, 191)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 1
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnmodificar
        '
        Me.btnmodificar.Location = New System.Drawing.Point(325, 191)
        Me.btnmodificar.Name = "btnmodificar"
        Me.btnmodificar.Size = New System.Drawing.Size(75, 23)
        Me.btnmodificar.TabIndex = 3
        Me.btnmodificar.Text = "&Modificar"
        Me.btnmodificar.UseVisualStyleBackColor = True
        '
        'frmModificarChequeCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(647, 247)
        Me.Controls.Add(Me.btnmodificar)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.gbxDatos)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Name = "frmModificarChequeCliente"
        Me.Text = "frmModificarChequeCliente"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDatos.ResumeLayout(False)
        Me.gbxDatos.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtImporte As ERP.ocxTXTNumeric
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents txtEstado As ERP.ocxTXTString
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents cbxDocumentosPagados As ERP.ocxCBX
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents txtSaldo As ERP.ocxTXTNumeric
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents gbxDatos As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblDocumentosPagados As System.Windows.Forms.Label
    Friend WithEvents lblDiferido As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents txtLiberador As ERP.ocxTXTString
    Friend WithEvents chkChequeTercero As System.Windows.Forms.CheckBox
    Friend WithEvents chkDiferido As System.Windows.Forms.CheckBox
    Friend WithEvents txtVencimiento As ERP.ocxTXTDate
    Friend WithEvents lblVencimiento As System.Windows.Forms.Label
    Friend WithEvents txtImporteMoneda As ERP.ocxTXTNumeric
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents txtNroCheque As ERP.ocxTXTString
    Friend WithEvents lblNroCheque As System.Windows.Forms.Label
    Friend WithEvents cbxNroCuentaBancaria As ERP.ocxCBX
    Friend WithEvents lblNroCuentaBancaria As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents cbxBanco As ERP.ocxCBX
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnmodificar As System.Windows.Forms.Button
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtTitular As ERP.ocxTXTString
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class

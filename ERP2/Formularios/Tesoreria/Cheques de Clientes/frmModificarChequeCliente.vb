﻿Public Class frmModificarChequeCliente
   
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property
    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property


    'EVENTOS

    'VARIABLES
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim dtCuentaBancaria As DataTable

    'FUNCIONES
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        txtCliente.Conectar()

        'Propiedades
        IDTransaccion = 0
        vNuevo = True

        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "CHEQUE CLIENTE", "CHQC")

        'Funciones
        CargarInformacion()

        Dim ID As Integer
        ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VChequeCliente Where IDsucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)

        txtID.txt.Text = ID
        txtID.txt.SelectAll()

        CargarOperacion()

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'Combrobante
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, cbxBanco)
        CSistema.CargaControl(vControles, cbxNroCuentaBancaria)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtNroCheque)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, txtCotizacion)
        CSistema.CargaControl(vControles, txtImporteMoneda)
        CSistema.CargaControl(vControles, chkDiferido)
        CSistema.CargaControl(vControles, txtVencimiento)
        CSistema.CargaControl(vControles, chkChequeTercero)
        CSistema.CargaControl(vControles, txtLiberador)
        CSistema.CargaControl(vControles, txtTitular)

        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select Distinct ID, CodigoCiudad  From VSucursal Order By 2")

        'Banco
        CSistema.SqlToComboBox(cbxBanco.cbx, "Select ID, Descripcion  From Banco Order By 2")

        'Cuentas Bancarias
        dtCuentaBancaria = CSistema.ExecuteToDataTable("Select * From ClienteCuentaBancaria")

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Referencia From Moneda")
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'BAnco
        cbxBanco.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "BANCO", "")

        'Moneda
        cbxMoneda.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MONEDA", "")

        ''ID
        'txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From ChequeCliente),1)"), Integer)

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxSucursal.cbx.Text)

        'Banco
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "BANCO", cbxBanco.cbx.Text)

        'Moneda
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", cbxMoneda.cbx.Text)

    End Sub

    Sub Modificar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        BloquearImporte()

        txtID.txt.ReadOnly = False
        txtCliente.Focus()


    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnmodificar, btnGuardar, btnCancelar, New Button, New Button, btnBuscar, New Button, vControles, New Button)

    End Sub
    Function ValidarDocumento() As Boolean

        ValidarDocumento = False

        'Validar

        'Sucursal
        If cbxSucursal.cbx.SelectedValue = Nothing Then
            CSistema.MostrarError("Seleccione correctamente la sucursal!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Cliente
        If txtCliente.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Banco
        If cbxBanco.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente el banco!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Nro de Cheque
        If txtNroCheque.txt.Text.Trim = "" Then
            Dim mensaje As String = "El Nro del Cheque no es valido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Importe
        If CDec(txtImporteMoneda.ObtenerValor) <= 0 Then
            Dim mensaje As String = "El importe del documento no es valido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        Return True

    End Function

    Sub Guardar()

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento() = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim vIDTransaccion As Integer = 0
        Dim IndiceOperacion As Integer = 0

        If txtCotizacion.ObtenerValor = 0 Then
            txtCotizacion.SetValue(1)
        End If

        'Entrada

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalDocumento", cbxSucursal.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDBanco", cbxBanco.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaBancaria", cbxNroCuentaBancaria.cbx.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", txtFecha.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroCheque", txtNroCheque.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", txtCotizacion.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Importe", txtImporte.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ImporteMoneda", txtImporteMoneda.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Diferido", chkDiferido.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Titular", txtTitular.GetValue, ParameterDirection.Input)

        If chkDiferido.Checked = True Then
            CSistema.SetSQLParameter(param, "@FechaVencimiento", txtVencimiento.GetValue, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@ChequeTercero", chkChequeTercero.Checked.ToString, ParameterDirection.Input)

        If chkChequeTercero.Checked = True Then
            CSistema.SetSQLParameter(param, "@Librador", txtLiberador.txt.Text.Trim, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Operacion", ERP.CSistema.NUMOperacionesABM.UPD.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpChequeCliente", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            Exit Sub

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False


    End Sub

    Sub Eliminar()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para eliminar!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.DEL.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Eliminar
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpChequeCliente", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

    End Sub

    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'Otros
        Dim frm As New frmConsultaChequeCliente
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog(Me)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub ListarCuentaBancaria()

        'Seleccion de Cliente


        Try
            Dim IDCliente As String = txtCliente.Registro("ID").ToString
            Dim IDBanco As String = cbxBanco.cbx.SelectedValue

            cbxNroCuentaBancaria.cbx.Items.Clear()
            cbxNroCuentaBancaria.cbx.Text = ""

            If IsNumeric(IDCliente) = False Then
                Exit Sub
            End If

            If IsNumeric(IDBanco) = False Then
                Exit Sub
            End If

            For Each oRow As DataRow In dtCuentaBancaria.Select("IDCliente=" & IDCliente & " And IDBanco=" & IDBanco & " ")
                cbxNroCuentaBancaria.cbx.Items.Add(oRow("CuentaBancaria").ToString)
            Next


        Catch ex As Exception

        End Try

    End Sub

    Sub BloquearImporte()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VChequeCliente Where IDTransaccion=" & IDTransaccion)

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        If (oRow("Depositado") = True) Or (cbxDocumentosPagados.cbx.Items.Count > 0) Then
            txtImporteMoneda.SoloLectura = True
        Else
            txtImporteMoneda.SoloLectura = False
        End If

    End Sub

    Sub ListarDocumentosPagados(ByVal IDTransaccion As Integer)

        cbxDocumentosPagados.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        Dim sql As String = "Select IDTransaccion, Comprobante From VChequeClienteDocumentosPagados Where IDTransaccion=" & IDTransaccion
        CSistema.SqlToComboBox(cbxDocumentosPagados.cbx, sql)
        ' BloquearImporte()

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From ChequeCliente Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.MODIFICAR)

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VChequeCliente Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        cbxSucursal.cbx.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        txtCliente.SetValue(oRow("IDCliente").ToString)
        cbxBanco.cbx.Text = oRow("Banco").ToString
        cbxNroCuentaBancaria.cbx.Text = oRow("CuentaBancaria").ToString
        txtFecha.SetValue(CDate(oRow("Fecha").ToString))
        txtNroCheque.txt.Text = oRow("NroCheque").ToString
        cbxMoneda.cbx.Text = oRow("Moneda").ToString
        txtCotizacion.txt.Text = oRow("Cotizacion").ToString
        txtImporteMoneda.txt.Text = oRow("ImporteMoneda").ToString
        txtImporte.txt.Text = oRow("Importe").ToString
        txtTitular.SetValue(oRow("Titular").ToString)

        chkDiferido.Checked = CBool(oRow("Diferido").ToString)

        If chkDiferido.Checked = True Then
            txtVencimiento.SetValue(CDate(oRow("FechaVencimiento").ToString))
        Else
            txtVencimiento.txt.Clear()
        End If

        chkChequeTercero.Checked = CBool(oRow("ChequeTercero").ToString)

        If chkChequeTercero.Checked = True Then
            txtLiberador.txt.Text = oRow("Librador").ToString
        Else
            txtLiberador.txt.Clear()
        End If

        txtSaldo.SetValue(oRow("Saldo").ToString)
        txtEstado.txt.Text = oRow("Estado").ToString

        '  BloquearImporte()

        'Cargar Documentos Asociados
        ListarDocumentosPagados(IDTransaccion)

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VChequeCliente"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VChequeCliente"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

    End Sub
    Private Sub frmChequeCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmModificarChequeCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmModificarChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Buscar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado

        txtTitular.txt.Focus()
        ListarCuentaBancaria()

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub cbxBanco_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxBanco.PropertyChanged
        ListarCuentaBancaria()
    End Sub

    Private Sub txtImporte_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtImporteMoneda.TeclaPrecionada
        txtImporte.SetValue(CSistema.Cotizador(txtCotizacion.ObtenerValor, txtImporteMoneda.ObtenerValor, cbxMoneda.cbx.SelectedValue))
    End Sub

    Private Sub btnmodificar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnmodificar.Click
        Modificar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

﻿Public Class frmConsultaChequeCliente

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim dtBanco As DataTable
    Dim dtCliente As DataTable

    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'Form

        'TextBox
        txtCantidadCheque.txt.ResetText()
        txtCantidadDocumentos.txt.ResetText()
        txtNroCheque.txt.ResetText()
        txtOperacion.txt.ResetText()
        txtTotalCheques.txt.ResetText()
        txtTotalSaldo.txt.ResetText()

        'CheckBox
        chkBanco.Checked = False
        chkBancoSucursal.Checked = False
        chkCliente.Checked = False
        chkClienteSucursal.Checked = False
        chkFecha.Checked = False
        chkFechaSucursal.Checked = False
        chkTipo.Checked = False
        chkTipoSucursal.Checked = False

        'ComboBox
        cbxBanco.Enabled = False
        cbxBancoSucursal.Enabled = False
        cbxCliente.Enabled = False
        cbxClienteSucursal.Enabled = False
        cbxTipo.Enabled = False
        cbxTipoSucursal.Enabled = False

        'ListView
        lvDocumentos.Items.Clear()
        lvOperacion.Items.Clear()

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now
        dtpDesdeSucursal.Value = Date.Now
        dtpHastaSucursal.Value = Date.Now

        'Funciones
        CargarInformacion()

        'Foco
        txtOperacion.txt.Focus()
        txtOperacion.txt.SelectAll()
    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'Banco
        dtBanco = CSistema.ExecuteToDataTable("Select ID, Descripcion From Banco Order By 2").Copy
        CSistema.SqlToComboBox(cbxBanco, dtBanco.Copy, "ID", "Descripcion")
        CSistema.SqlToComboBox(cbxBancoSucursal, dtBanco.Copy, "ID", "Descripcion")

        'Cliente
        dtCliente = CSistema.ExecuteToDataTable("Select ID, RazonSocial From Cliente Order By 2").Copy
        CSistema.SqlToComboBox(cbxCliente, dtCliente.Copy, "ID", "RazonSocial")
        CSistema.SqlToComboBox(cbxClienteSucursal, dtCliente.Copy, "ID", "RazonSocial")

        'Tipo
        cbxTipo.Items.Add("AL DIA")
        cbxTipo.Items.Add("DIFERIDO")
        cbxTipo.DropDownStyle = ComboBoxStyle.DropDownList

        cbxTipoSucursal.Items.Add("AL DIA")
        cbxTipoSucursal.Items.Add("DIFERIDO")
        cbxTipoSucursal.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

        'Cliente
        chkCliente.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO", "False")
        chkClienteSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO SUCRUSAL", "False")
        cbxCliente.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE", "")
        cbxClienteSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE SUCURSAL", "")

        'Banco
        chkBanco.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO", "False")
        chkBancoSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO SUCURSAL", "False")
        cbxBanco.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO", "")
        cbxBancoSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO SUCURSAL", "")

    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Cliente
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO", chkCliente.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO SUCURSAL", chkClienteSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE", cbxCliente.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE SUCURSAL", cbxClienteSucursal.Text)

        'Banco
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO", chkBanco.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO SUCURSAL", chkBancoSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO", cbxBanco.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO SUCURSAL", cbxBancoSucursal.Text)

        'Tipo
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO ACTIVO", chkTipo.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO ACTIVO SUCURSAL", chkTipoSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO", cbxTipo.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO SUCURSAL", cbxTipoSucursal.Text)


    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Operaciones
    Sub ListarOperaciones(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "", Optional ByVal Sucursal As Boolean = False)

        ctrError.Clear()

        Consulta = "Select 'ID'=Numero, Cliente, Banco, 'Fecha'=Fec, 'Nro'=NroCheque, Importe, Saldo, Estado From VChequeCliente  "

        Where = Condicion

        'Solo por numero
        If Numero > 0 Then
            Where = " Where Numero = " & Numero & ""
        End If

        If IsNumeric(txtNroCheque.txt.Text) > 0 Then
            Where = "Where NroCheque = " & txtNroCheque.txt.Text
        End If

        If Sucursal = True Then
            If Where = "" Then
                Where = " Where (IDSucursal=" & cbxSucursal.SelectedValue & ") "
            Else
                Where = Where & " And (IDSucursal=" & cbxSucursal.SelectedValue & ") "
            End If
        End If

        'Cliente
        If Sucursal = True Then
            If EstablecerCondicion(cbxClienteSucursal, chkClienteSucursal, "IDCliente", "Seleccione correctamente el cliente!") = False Then
                Exit Sub
            End If
        Else
            If EstablecerCondicion(cbxCliente, chkCliente, "IDCliente", "Seleccione correctamente el cliente!") = False Then
                Exit Sub
            End If
        End If

        'Banco
        If Sucursal = True Then
            If EstablecerCondicion(cbxBancoSucursal, chkBancoSucursal, "IDBanco", "Seleccione correctamente el banco!") = False Then
                Exit Sub
            End If
        Else
            If EstablecerCondicion(cbxBanco, chkBanco, "IDBanco", "Seleccione correctamente el banco!") = False Then
                Exit Sub
            End If
        End If

        'Tipo
        If Sucursal = True Then
            If chkTipoSucursal.Checked = True Then
                If Where = "" Then
                    Where = " Where (Tipo='" & cbxTipoSucursal.Text & "') "
                Else
                    Where = Where & " And (Tipo='" & cbxTipoSucursal.Text & "')  "
                End If
            End If
        Else

            If chkTipo.Checked = True Then
                If Where = "" Then
                    Where = " Where (Tipo='" & cbxTipo.Text & "') "
                Else
                    Where = Where & " And (Tipo='" & cbxTipo.Text & "')  "
                End If
            End If

        End If

        'Fecha
        If Sucursal = True Then
            If chkFechaSucursal.Checked = True Then
                If Where = "" Then
                    Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesdeSucursal, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHastaSucursal, True, False) & "' ) "
                Else
                    Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesdeSucursal, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHastaSucursal, True, False) & "' )  "
                End If
            End If

        Else

            If chkFecha.Checked = True Then
                If Where = "" Then
                    Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' ) "
                Else
                    Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' )  "
                End If
            End If

        End If

        lvOperacion.Items.Clear()
        lvDocumentos.Items.Clear()

        CSistema.SqlToLv(lvOperacion, Consulta & " " & Where & " Order By Fecha")

        'Formato

        'Totales
        CSistema.FormatoMoneda(lvOperacion, 5)
        CSistema.FormatoMoneda(lvOperacion, 6)

        CSistema.TotalesLv(lvOperacion, txtTotalCheques.txt, 5)
        CSistema.TotalesLv(lvOperacion, txtTotalSaldo.txt, 6)

        txtCantidadCheque.txt.Text = lvOperacion.Items.Count

    End Sub

    'Listar Documentos
    Sub ListarDocumentos()

        ctrError.Clear()

        'Validar
        If lvOperacion.SelectedItems.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(lvOperacion.SelectedItems(0).SubItems(0).Text) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Limpiar ListView
        lvDocumentos.Items.Clear()

        Dim Numero As Integer = lvOperacion.SelectedItems(0).SubItems(0).Text
        Dim sql As String = "Select TipoComprobante, Comprobante, Fec, 'Importe'=ImporteCheque From VChequeClienteDocumentosPagados Where Numero=" & Numero
        CSistema.SqlToLv(lvDocumentos, sql)

        CSistema.FormatoMoneda(lvDocumentos, 3)

    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        If lvOperacion.SelectedItems.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(lvOperacion.SelectedItems(0).SubItems(13).Text) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = CInt(lvOperacion.SelectedItems(0).SubItems(13).Text)

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub frmConsultaChequeCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn1.Click
        ListarOperaciones(0, " Where (Estado='CARTERA') ", False)
    End Sub

    Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn2.Click
        ListarOperaciones(0, " Where (Cancelado='False') ", False)
    End Sub

    Private Sub btn3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn3.Click
        ListarOperaciones(0, " Where (Estado='DEPOSITADO') ", False)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        ListarOperaciones(0, " Where (Estado='RECHAZADO') ", False)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        ListarOperaciones(0, "", False)
    End Sub

    Private Sub chkCliente_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCliente.CheckedChanged
        HabilitarControles(chkCliente, cbxCliente)
    End Sub

    Private Sub chkBanco_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBanco.CheckedChanged
        HabilitarControles(chkBanco, cbxBanco)
    End Sub

    Private Sub chkTipo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipo.CheckedChanged
        HabilitarControles(chkTipo, cbxTipo)
    End Sub

    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged
        HabilitarControles(chkFecha, dtpDesde)
        HabilitarControles(chkFecha, dtpHasta)
    End Sub

    Private Sub chkClienteSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkClienteSucursal.CheckedChanged
        HabilitarControles(chkClienteSucursal, cbxClienteSucursal)
    End Sub

    Private Sub chkBancoSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBancoSucursal.CheckedChanged
        HabilitarControles(chkBancoSucursal, cbxBancoSucursal)
    End Sub

    Private Sub chkTipoSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipoSucursal.CheckedChanged
        HabilitarControles(chkTipoSucursal, cbxTipoSucursal)
    End Sub

    Private Sub chkFechaSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFechaSucursal.CheckedChanged
        HabilitarControles(chkFechaSucursal, dtpDesdeSucursal)
        HabilitarControles(chkFechaSucursal, dtpHastaSucursal)
    End Sub

    Private Sub btn10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn10.Click
        ListarOperaciones(0, " Where (Estado='CARTERA') ", True)
    End Sub

    Private Sub btn11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn11.Click
        ListarOperaciones(0, " Where (Cancelado='False') ", True)
    End Sub

    Private Sub btn12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn12.Click
        ListarOperaciones(0, " Where (Estado='DEPOSITADO') ", True)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ListarOperaciones(0, " Where (Estado='RECHAZADO') ", True)
    End Sub

    Private Sub btn13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn13.Click
        ListarOperaciones(0, "", True)
    End Sub

    Private Sub lvOperacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvOperacion.SelectedIndexChanged
        ListarDocumentos()
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarOperaciones(txtOperacion.ObtenerValor)
            txtOperacion.txt.Focus()
            txtOperacion.txt.SelectAll()
        End If
    End Sub

    Private Sub txtNroCheque_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroCheque.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarOperaciones(txtNroCheque.ObtenerValor)
            txtOperacion.txt.Focus()
            txtOperacion.txt.SelectAll()
        End If
    End Sub
End Class
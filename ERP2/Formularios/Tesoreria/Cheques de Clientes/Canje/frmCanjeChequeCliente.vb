﻿Public Class frmCanjeChequeCliente



    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CAsientoContableCobranzaCredito As New CAsientoContableCobranzaCredito

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'EVENTOS

    'VARIABLES
    Dim dtDetalleCobranzaVenta As New DataTable
    Dim dtCheque As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Otros
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "PAGO CHEQUE CLIENTE", "PCC")
        vNuevo = True

        'Funciones
        CargarInformacion()

        'Clases
        CAsientoContableCobranzaCredito.Inicializar()

        'Controles
        OcxFormaPago1.Inicializar()
        OcxFormaPago1.Tipo = "CONTADO"
        OcxFormaPago1.Form = Me

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)
        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)
    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtObservacion)

        'Ventas

        CSistema.CargaControl(vControles, lklEliminarVenta)
        CSistema.CargaControl(vControles, btnCheques)
        'Forma de Pago
        CSistema.CargaControl(vControles, OcxFormaPago1)

        'CARGAR ESTRUCTURA DEL DETALLE VENTA

        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Limpiar detalle
        dtDetalleCobranzaVenta.Rows.Clear()
        ListarComprobantes()

        'Cargamos lotes pendientes
        dtCheque = CSistema.ExecuteToDataTable("Select *, 'Sel'='False', 'Cancelar'='False' from VChequeCliente Where Rechazado = 'True' And SaldoACuenta > 0 ").Copy

        'Forma de Pago
        OcxFormaPago1.dtCheques.Rows.Clear()
        OcxFormaPago1.dtChequesTercero.Rows.Clear()
        OcxFormaPago1.dtEfectivo.Rows.Clear()
        OcxFormaPago1.dtFormaPago.Rows.Clear()
        OcxFormaPago1.ListarFormaPago()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        CAsientoContableCobranzaCredito.Limpiar()
        txtTotal.txt.Text = ""
        txtSaldoTotal.txt.Text = ""

        vNuevo = True

        'Cabecera
        txtFecha.txt.Text = ""
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()

        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From PagoChequeCliente Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)
        txtComprobante.txt.Text = CSistema.ObtenerProximoNroComprobante(cbxTipoComprobante.cbx.SelectedValue, "PagoChequeCliente", "NroComprobante", cbxSucursal.cbx.SelectedValue)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Limpiar lvVentas
        lvVentas.Items.Clear()

        'Poner el foco en el proveedor
        cbxTipoComprobante.cbx.Focus()

    End Sub


    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If

        End If

        'Validar
        'Ciudad
        If cbxCiudad.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente la ciudad!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Tipo Comprobante
        If cbxTipoComprobante.cbx.Text.Trim = "" Then
            Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
            Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
            ctrError.SetError(cbxTipoComprobante, mensaje)
            ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Sucursal
        If cbxSucursal.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If cbxSucursal.cbx.Text.Trim = "" Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        ''Lote
        'If cbxLote.Validar("Seleccione correctamente la sucursal!", ctrError, btnGuardar, tsslEstado) = False Then
        '    Exit Function
        '    'End If

        'Venta
        If lvVentas.Items.Count = 0 Then
            Dim mensaje As String = "El documento tiene que tener comprobantes de venta!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If txtTotal.ObtenerValor <= 0 Then
            Dim mensaje As String = "El importe de los comprobantes de venta no es valido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Forma de Pago

        ''Saldo
        'If txtSaldoTotal.ObtenerValor <> 0 Then
        '    Dim mensaje As String = "El importe de los comprobantes de venta y la forma de pago no concuerdan!"
        '    ctrError.SetError(btnGuardar, mensaje)
        '    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
        '    tsslEstado.Text = mensaje
        '    Exit Function
        'End If

        'Asiento
        'Dim SaldoAsiento As Decimal = CDec(CAsientoContableCobranzaCredito.CAsiento.dtAsiento(0)("Saldo").ToString)

        'If SaldoAsiento <> 0 Then
        '    Dim mensaje As String = "El saldo del asiento no es correcto!"
        '    ctrError.SetError(btnGuardar, mensaje)
        '    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
        '    tsslEstado.Text = mensaje
        '    Exit Function
        'End If

        Return True


    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDLote", cbxLote.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(txtTotal.txt.Text), ParameterDirection.Input)

        'Totales

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpPagoChequeCliente", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From CobranzaContado Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpPagoChequeCliente", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        Dim Procesar As Boolean = True


        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
            CargarOperacion(IDTransaccion)

            txtID.SoloLectura = False
        End If

        If IDTransaccion > 0 Then

            'Cargamos el Detalle de Ventas
            Procesar = InsertarDetalle(IDTransaccion)

            'Cargamos la Forma de Pago
            Procesar = InsertarFormaPago(IDTransaccion)

        End If

        'Aplicar el recibo
        Try

            ReDim param(-1)

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)

            If Procesar = True Then
                CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)
            Else
                CSistema.SetSQLParameter(param, "@Operacion", "DEL", ParameterDirection.Input)
            End If

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            'Aplicar Pago Cheque Cliente
            If CSistema.ExecuteStoreProcedure(param, "SpPagoChequeClienteProcesar", False, True, MensajeRetorno) = False Then

            End If

            'Cargamos el Asiento
            'CAsientoContableCobranzaCredito.CAsiento.IDTransaccion = IDTransaccion
            'GenerarAsiento()

            'CAsientoContableCobranzaCredito.CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        Catch ex As Exception

        End Try

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

    End Sub

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)


        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpPagoChequeCliente", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From PagoChequeCliente Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then

            End If

            Exit Sub

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False


    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalle = True

        For Each oRow As DataRow In dtCheque.Select(" Sel = 'True' ")

            Dim sql As String = "Insert Into DetallePagoChequeCliente(IDTransaccionChequeCliente, IDTransaccionPagoChequeCliente,Importe) Values(" & oRow("IDTransaccion").ToString & "," & IDTransaccion & "," & oRow("Importe").ToString & ")"

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

        Next

    End Function

    Function InsertarFormaPago(ByVal IDTransaccion As Integer) As Boolean

        InsertarFormaPago = True

        Dim dtEfectivo As DataTable
        Dim dtCheque As DataTable
        Dim dtDocumento As DataTable

        Dim ID As Integer = 1

        dtEfectivo = OcxFormaPago1.dtEfectivo.Copy
        dtCheque = OcxFormaPago1.dtCheques.Copy
        dtDocumento = OcxFormaPago1.dtDocumento.Copy

        'Cargar el Efectivo
        For Each oRow As DataRow In dtEfectivo.Rows

            Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe) values(" & IDTransaccion & ", " & ID & ", 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", 'False', 0, NULL, NULL, 'False', 0, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ")"
            Dim sqlEfectivo As String = "Insert Into Efectivo(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, VentasCredito, VentasContado, Gastos, IDMoneda, ImporteMoneda, Cotizacion, Observacion, Depositado, Cancelado) values(" & IDTransaccion & ", " & ID & ", " & cbxSucursal.cbx.SelectedValue & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("Comprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True) & "', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", 0, 0, " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString) & ", '" & oRow("Observacion").ToString & "', 0, 'False')"

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            If CSistema.ExecuteNonQuery(sqlEfectivo) = 0 Then
                Return False
            End If

            ID = ID + 1

        Next

        'Cargar el Documento
        For Each oRow As DataRow In dtDocumento.Rows

            Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, 0, 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ")"
            Dim sqlDocumento As String = "Insert Into FormaPagoDocumento(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, IDMoneda, ImporteMoneda, Cotizacion, Importe, Observacion) values(" & IDTransaccion & ", " & ID & ", " & cbxSucursal.cbx.SelectedValue & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("Comprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True) & "', " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", '" & oRow("Observacion").ToString & "')"

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            If CSistema.ExecuteNonQuery(sqlDocumento) = 0 Then
                Return False
            End If

            ID = ID + 1

        Next

        'Cargar el Cheque
        For Each oRow As DataRow In dtCheque.Rows

            If CBool(oRow("Sel").ToString) = True Then

                Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, CancelarCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & oRow("IDTransaccion").ToString & ", NULL, '" & oRow("Cancelar").ToString & "' ,'False', NULL, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ")"

                If CSistema.ExecuteNonQuery(sql) = 0 Then
                    Return False
                End If

                ID = ID + 1

            End If

        Next

        Dim dtChequeTercero As DataTable
        dtChequeTercero = OcxFormaPago1.dtChequesTercero.Copy

        'Cargar el Cheque
        For Each oRow As DataRow In dtChequeTercero.Rows

            If CBool(oRow("Sel").ToString) = True Then

                Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Importe) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & oRow("IDTransaccion").ToString & ", NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ")"

                If CSistema.ExecuteNonQuery(sql) = 0 Then
                    Return False
                End If

                ID = ID + 1

            End If

        Next

    End Function

    'Sub ObtenerComprobantesLote()

    '    If cbxLote.cbx.SelectedValue Is Nothing Then
    '        Exit Sub
    '    End If

    '    dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Select * From VVentasPendientesParaLote Where IDTransaccionLote=" & cbxLote.cbx.SelectedValue).Copy
    '    ListarVentas()

    'End Sub

    Sub CargarCheques(ByVal IDTransaccion As Integer)

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra las ventas asociadas!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        'Limpiar ventas seleccionadas
        dtCheque.Rows.Clear()
        dtCheque = CSistema.ExecuteToDataTable("Select *, 'Sel'='True' From VDetallePagoChequeCliente Where IDTransaccionPagoChequeCliente=" & IDTransaccion).Copy
        ListarComprobantes()

    End Sub

    Sub ListarComprobantes()

        lvVentas.Items.Clear()

        Dim Total As Decimal = 0

        For Each oRow As DataRow In dtCheque.Rows
            If oRow("Sel") = True Then

                Dim item As ListViewItem = New ListViewItem(oRow("IDTransaccion").ToString)
                item.SubItems.Add(oRow("CodigoTipo").ToString)
                item.SubItems.Add(oRow("Banco").ToString)
                item.SubItems.Add(oRow("NroCheque").ToString)
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Importe").ToString))
                item.SubItems.Add(oRow("Cliente").ToString)
                item.SubItems.Add(oRow("CuentaBancaria").ToString)

                Total = Total + CDec(oRow("Importe").ToString)
                txtTotal.txt.Text = Total
                lvVentas.Items.Add(item)

            End If
        Next

        CalcularTotales()

    End Sub



    Sub EliminarVenta()

        For Each item As ListViewItem In lvVentas.SelectedItems

            Dim IDTransaccion As String = item.SubItems(0).Text

            'buscamos en CHeque
            For Each oRow As DataRow In dtCheque.Select(" IDTransaccion = " & IDTransaccion & "")


                oRow("Sel") = False
                Exit For

            Next
        Next
        ListarComprobantes()

    End Sub

    Sub CalcularTotales()

        Dim TotalVentas As Decimal
        Dim TotalFormaPago As Decimal
        Dim SaldoTotal As Decimal

        TotalVentas = txtTotal.ObtenerValor
        TotalFormaPago = OcxFormaPago1.Total

        'Calcular
        SaldoTotal = TotalVentas - TotalFormaPago

        txtSaldoTotal.SetValue(SaldoTotal)
        OcxFormaPago1.Saldo = SaldoTotal

    End Sub

    Sub VisualizarAsiento()

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "Cobranza Credito: " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text & "  -  "

            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VCobranzaCredito Where Numero=" & txtID.ObtenerValor & "), 0)")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else

            'Validar
            If cbxCiudad.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la ciudad de operacion!"
                ctrError.SetError(cbxCiudad, mensaje)
                ctrError.SetIconAlignment(cbxCiudad, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(cbxTipoComprobante, mensaje)
                ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento2(IDOperacion)
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "Cobranza Contado: " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text & "  -  "

            GenerarAsiento()

            frm.CAsiento = CAsientoContableCobranzaCredito.CAsiento
            CAsientoContableCobranzaCredito.CAsiento.ListarDetalle(frm.lvLista)
            frm.CalcularTotales()

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsientoContableCobranzaCredito.CAsiento = frm.CAsiento

        End If

    End Sub

    Sub GenerarAsiento()

        'Establecer Cabecera
        Dim oRow As DataRow = CAsientoContableCobranzaCredito.CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = cbxCiudad.cbx.SelectedValue
        oRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
        oRow("Fecha") = txtFecha.GetValue
        oRow("IDMoneda") = 1
        oRow("Cotizacion") = 1
        oRow("IDTipoComprobante") = cbxTipoComprobante.cbx.SelectedValue
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = txtComprobante.txt.Text
        oRow("Comprobante") = txtComprobante.txt.Text
        oRow("Detalle") = txtObservacion.txt.Text
        oRow("Total") = txtTotal.ObtenerValor

        CAsientoContableCobranzaCredito.CAsiento.dtAsiento.Rows.Clear()
        CAsientoContableCobranzaCredito.CAsiento.dtAsiento.Rows.Add(oRow)

    End Sub

    Sub Imprimir()

    End Sub

    Sub Buscar()

        Dim frm As New frmConsultaCanjeChequeCliente
        frm.ShowDialog()
        CargarOperacion(frm.IDTransaccion)
    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From PagoChequeCliente Where Numero=" & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VPagoChequeCliente Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Suc").ToString
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtTotal.txt.Text = oRow("Total").ToString
        'txtDistribuidor.txt.Text = oRow("Distribuidor").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("UsuarioIdentificador").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos el detalle de Venta
        CargarCheques(IDTransaccion)

        'Cargar Forma de Pago
        OcxFormaPago1.ListarFormaPago(IDTransaccion)

        'Calcular Totales
        CalcularTotales()

        'Inicializamos el Asiento
        CAsientoContableCobranzaCredito.Limpiar()

    End Sub

    Sub CargarCheque()

        Dim frm As New frmSeleccionarCanjeChequeCliente
        frm.Text = "Seleccion de Cheque"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtCheque
        frm.Inicializar()
        frm.ShowDialog(Me)
        dtCheque = frm.dt

        ListarComprobantes()

    End Sub

    Sub RecalcularAsiento()

        'For Each oRow As DataRow In CAsientoContableCobranzaCredito.CAsiento.dtDetalleAsiento.Select("Credito>0")
        '    CAsientoContableCobranzaCredito.CAsiento.dtDetalleAsiento.Rows.Remove(oRow)
        'Next

        For i As Integer = CAsientoContableCobranzaCredito.CAsiento.dtDetalleAsiento.Rows.Count - 1 To 0 Step -1
            Dim oRow As DataRow = CAsientoContableCobranzaCredito.CAsiento.dtDetalleAsiento.Rows(i)

            If oRow("Credito") > 0 Then
                CAsientoContableCobranzaCredito.CAsiento.dtDetalleAsiento.Rows.RemoveAt(i)
            End If

        Next

        'Volvemos a cargar
        For Each oRow As DataRow In dtDetalleCobranzaVenta.Rows

            If CBool(oRow("Sel").ToString) = True Then
                'Cargamos en el asiento
                CAsientoContableCobranzaCredito.InsertarVenta(oRow("Importe").ToString, oRow("IDTipoComprobante").ToString, oRow("IDMoneda").ToString, oRow("Credito").ToString)
            End If
        Next

    End Sub

    Sub CargarNuevoCheque()

    End Sub

    Sub AplicarOrdenCompra()

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VPagoChequeCliente"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VPagoChequeCliente"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub ObtenerSucursal()

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

    End Sub

    Private Sub frmCobranzaCredito_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs)
        GuardarInformacion()
    End Sub

    Private Sub frmCobranzaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Buscar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Imprimir()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        VisualizarAsiento()
    End Sub

    Private Sub lklEliminarVenta_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarVenta.LinkClicked
        EliminarVenta()
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged

        ObtenerSucursal()

    End Sub

    Private Sub txtComprobante_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtComprobante.TeclaPrecionada
        OcxFormaPago1.Comprobante = txtComprobante.txt.Text
    End Sub

    Private Sub txtFecha_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtFecha.TeclaPrecionada
        OcxFormaPago1.Fecha = txtFecha.GetValue
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub OcxFormaPago1_ImporteModificado(ByVal sender As Object, ByVal e As System.EventArgs) Handles OcxFormaPago1.ImporteModificado
        CalcularTotales()
    End Sub

    Private Sub OcxFormaPago1_ListarCheques(ByVal sender As Object, ByVal e As System.EventArgs) Handles OcxFormaPago1.ListarCheques

        'Cheques y Forma de Pago
        OcxFormaPago1.Reset()

    End Sub

    Private Sub OcxFormaPago1_RegistroEliminado(ByVal sender As Object, ByVal e As System.EventArgs, ByVal oRow As System.Data.DataRow, ByVal Tipo As ocxFormaPago.ENUMFormaPago) Handles OcxFormaPago1.RegistroEliminado

        Select Case Tipo
            Case ocxFormaPago.ENUMFormaPago.Efectivo
                CAsientoContableCobranzaCredito.EliminarEfectivo(oRow("Importe").ToString, oRow("IDTipoComprobante").ToString, oRow("IDMoneda").ToString)
            Case ocxFormaPago.ENUMFormaPago.Cheque
                CAsientoContableCobranzaCredito.EliminarCheque(oRow("Importe").ToString, oRow("Diferido").ToString, oRow("IDMoneda").ToString)
            Case ocxFormaPago.ENUMFormaPago.ChequeTercero
                CAsientoContableCobranzaCredito.EliminarCheque(oRow("Importe").ToString, oRow("Diferido").ToString, oRow("IDMoneda").ToString)
            Case ocxFormaPago.ENUMFormaPago.Documento
                CAsientoContableCobranzaCredito.EliminarDocumento(oRow("Importe").ToString, oRow("IDTipoComprobante").ToString, oRow("IDMoneda").ToString)
        End Select

        CalcularTotales()

    End Sub

    Private Sub OcxFormaPago1_RegistroInsertado(ByVal sender As Object, ByVal e As System.EventArgs, ByVal oRow As System.Data.DataRow, ByVal Tipo As ocxFormaPago.ENUMFormaPago) Handles OcxFormaPago1.RegistroInsertado
        Select Case Tipo
            Case ocxFormaPago.ENUMFormaPago.Efectivo
                CAsientoContableCobranzaCredito.InsertarEfectivo(oRow("Importe").ToString, oRow("IDTipoComprobante").ToString, oRow("IDMoneda").ToString)
            Case ocxFormaPago.ENUMFormaPago.Cheque
                CAsientoContableCobranzaCredito.InsertarCheque(oRow("Importe").ToString, oRow("Diferido").ToString, oRow("IDMoneda").ToString)
            Case ocxFormaPago.ENUMFormaPago.ChequeTercero
                CAsientoContableCobranzaCredito.InsertarCheque(oRow("Importe").ToString, oRow("Diferido").ToString, oRow("IDMoneda").ToString)
            Case ocxFormaPago.ENUMFormaPago.Documento
                CAsientoContableCobranzaCredito.InsertarDocumento(oRow("Importe").ToString, oRow("IDTipoComprobante").ToString, oRow("IDMoneda").ToString)
        End Select
    End Sub

    'Private Sub cbxLote_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxLote.Leave
    '    ObtenerComprobantesLote()
    'End Sub

    'Private Sub cbxLote_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxLote.TeclaPrecionada
    '    If e.KeyCode = Keys.Enter Then
    '        ObtenerComprobantesLote()
    '    End If
    'End Sub

    Private Sub OcxFormaPago1_RegistroEliminado(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal oRow As System.Data.DataRow, ByVal Tipo As ERP.ocxFormaPagoLote.ENUMFormaPago) Handles OcxFormaPago1.RegistroEliminado

    End Sub
    Private Sub OcxFormaPago1_RegistroInsertado(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal oRow As System.Data.DataRow, ByVal Tipo As ERP.ocxFormaPagoLote.ENUMFormaPago) Handles OcxFormaPago1.RegistroInsertado

    End Sub

    Private Sub btnCheques_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheques.Click
        CargarCheque()
    End Sub

    Private Sub btnNuevo_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub frmPagoChequeCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmCanjeChequeCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmPagoChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub


    Private Sub btnAsiento_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub btnBusquedaAvanzada_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub


    Private Sub btnCancelar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnSalir_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub
End Class
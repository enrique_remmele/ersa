﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaChequeCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.btn4 = New System.Windows.Forms.Button()
        Me.btn3 = New System.Windows.Forms.Button()
        Me.btn2 = New System.Windows.Forms.Button()
        Me.chkFecha = New System.Windows.Forms.CheckBox()
        Me.chkTipo = New System.Windows.Forms.CheckBox()
        Me.cbxTipo = New System.Windows.Forms.ComboBox()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.btn1 = New System.Windows.Forms.Button()
        Me.chkBanco = New System.Windows.Forms.CheckBox()
        Me.cbxBanco = New System.Windows.Forms.ComboBox()
        Me.chkCliente = New System.Windows.Forms.CheckBox()
        Me.cbxCliente = New System.Windows.Forms.ComboBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btn13 = New System.Windows.Forms.Button()
        Me.btn12 = New System.Windows.Forms.Button()
        Me.btn11 = New System.Windows.Forms.Button()
        Me.chkFechaSucursal = New System.Windows.Forms.CheckBox()
        Me.chkTipoSucursal = New System.Windows.Forms.CheckBox()
        Me.cbxTipoSucursal = New System.Windows.Forms.ComboBox()
        Me.dtpHastaSucursal = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesdeSucursal = New System.Windows.Forms.DateTimePicker()
        Me.btn10 = New System.Windows.Forms.Button()
        Me.chkBancoSucursal = New System.Windows.Forms.CheckBox()
        Me.cbxBancoSucursal = New System.Windows.Forms.ComboBox()
        Me.chkClienteSucursal = New System.Windows.Forms.CheckBox()
        Me.cbxClienteSucursal = New System.Windows.Forms.ComboBox()
        Me.cbxSucursal = New System.Windows.Forms.ComboBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblCantidadDocumentos = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.txtCantidadDocumentos = New ERP.ocxTXTNumeric()
        Me.lvDocumentos = New System.Windows.Forms.ListView()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblCantidadCheque = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtNroCheque = New ERP.ocxTXTNumeric()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblNroCheque = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.txtOperacion = New ERP.ocxTXTNumeric()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtCantidadCheque = New ERP.ocxTXTNumeric()
        Me.lvOperacion = New System.Windows.Forms.ListView()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalSaldo = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTotalCheques = New ERP.ocxTXTNumeric()
        Me.Label2 = New System.Windows.Forms.Label()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 237.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(876, 515)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TabControl1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(642, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(231, 509)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'TabControl1
        '
        Me.TabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(3, 16)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(225, 490)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Button2)
        Me.TabPage1.Controls.Add(Me.btn4)
        Me.TabPage1.Controls.Add(Me.btn3)
        Me.TabPage1.Controls.Add(Me.btn2)
        Me.TabPage1.Controls.Add(Me.chkFecha)
        Me.TabPage1.Controls.Add(Me.chkTipo)
        Me.TabPage1.Controls.Add(Me.cbxTipo)
        Me.TabPage1.Controls.Add(Me.dtpHasta)
        Me.TabPage1.Controls.Add(Me.dtpDesde)
        Me.TabPage1.Controls.Add(Me.btn1)
        Me.TabPage1.Controls.Add(Me.chkBanco)
        Me.TabPage1.Controls.Add(Me.cbxBanco)
        Me.TabPage1.Controls.Add(Me.chkCliente)
        Me.TabPage1.Controls.Add(Me.cbxCliente)
        Me.TabPage1.Location = New System.Drawing.Point(4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(217, 464)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(6, 309)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(203, 24)
        Me.Button2.TabIndex = 12
        Me.Button2.Text = "Cheques Rechazados"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.UseVisualStyleBackColor = True
        '
        'btn4
        '
        Me.btn4.Location = New System.Drawing.Point(6, 339)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(203, 24)
        Me.btn4.TabIndex = 13
        Me.btn4.Text = "Todos"
        Me.btn4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn4.UseVisualStyleBackColor = True
        '
        'btn3
        '
        Me.btn3.Location = New System.Drawing.Point(6, 279)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(203, 24)
        Me.btn3.TabIndex = 11
        Me.btn3.Text = "Cheques Depositados"
        Me.btn3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn3.UseVisualStyleBackColor = True
        '
        'btn2
        '
        Me.btn2.Location = New System.Drawing.Point(6, 249)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(203, 24)
        Me.btn2.TabIndex = 10
        Me.btn2.Text = "Cheques no Cancelados"
        Me.btn2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn2.UseVisualStyleBackColor = True
        '
        'chkFecha
        '
        Me.chkFecha.AutoSize = True
        Me.chkFecha.Location = New System.Drawing.Point(6, 137)
        Me.chkFecha.Name = "chkFecha"
        Me.chkFecha.Size = New System.Drawing.Size(59, 17)
        Me.chkFecha.TabIndex = 6
        Me.chkFecha.Text = "Fecha:"
        Me.chkFecha.UseVisualStyleBackColor = True
        '
        'chkTipo
        '
        Me.chkTipo.AutoSize = True
        Me.chkTipo.Location = New System.Drawing.Point(6, 93)
        Me.chkTipo.Name = "chkTipo"
        Me.chkTipo.Size = New System.Drawing.Size(50, 17)
        Me.chkTipo.TabIndex = 4
        Me.chkTipo.Text = "Tipo:"
        Me.chkTipo.UseVisualStyleBackColor = True
        '
        'cbxTipo
        '
        Me.cbxTipo.Enabled = False
        Me.cbxTipo.FormattingEnabled = True
        Me.cbxTipo.Location = New System.Drawing.Point(6, 110)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.Size = New System.Drawing.Size(203, 21)
        Me.cbxTipo.TabIndex = 5
        '
        'dtpHasta
        '
        Me.dtpHasta.Enabled = False
        Me.dtpHasta.Location = New System.Drawing.Point(6, 180)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(203, 20)
        Me.dtpHasta.TabIndex = 8
        '
        'dtpDesde
        '
        Me.dtpDesde.Enabled = False
        Me.dtpDesde.Location = New System.Drawing.Point(6, 154)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(203, 20)
        Me.dtpDesde.TabIndex = 7
        '
        'btn1
        '
        Me.btn1.Location = New System.Drawing.Point(6, 219)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(203, 24)
        Me.btn1.TabIndex = 9
        Me.btn1.Text = "Cheques en Cartera"
        Me.btn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn1.UseVisualStyleBackColor = True
        '
        'chkBanco
        '
        Me.chkBanco.AutoSize = True
        Me.chkBanco.Location = New System.Drawing.Point(6, 49)
        Me.chkBanco.Name = "chkBanco"
        Me.chkBanco.Size = New System.Drawing.Size(60, 17)
        Me.chkBanco.TabIndex = 2
        Me.chkBanco.Text = "Banco:"
        Me.chkBanco.UseVisualStyleBackColor = True
        '
        'cbxBanco
        '
        Me.cbxBanco.Enabled = False
        Me.cbxBanco.FormattingEnabled = True
        Me.cbxBanco.Location = New System.Drawing.Point(6, 66)
        Me.cbxBanco.Name = "cbxBanco"
        Me.cbxBanco.Size = New System.Drawing.Size(203, 21)
        Me.cbxBanco.TabIndex = 3
        '
        'chkCliente
        '
        Me.chkCliente.AutoSize = True
        Me.chkCliente.Location = New System.Drawing.Point(6, 6)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(61, 17)
        Me.chkCliente.TabIndex = 0
        Me.chkCliente.Text = "Cliente:"
        Me.chkCliente.UseVisualStyleBackColor = True
        '
        'cbxCliente
        '
        Me.cbxCliente.Enabled = False
        Me.cbxCliente.FormattingEnabled = True
        Me.cbxCliente.Location = New System.Drawing.Point(6, 22)
        Me.cbxCliente.Name = "cbxCliente"
        Me.cbxCliente.Size = New System.Drawing.Size(203, 21)
        Me.cbxCliente.TabIndex = 1
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Button1)
        Me.TabPage2.Controls.Add(Me.btn13)
        Me.TabPage2.Controls.Add(Me.btn12)
        Me.TabPage2.Controls.Add(Me.btn11)
        Me.TabPage2.Controls.Add(Me.chkFechaSucursal)
        Me.TabPage2.Controls.Add(Me.chkTipoSucursal)
        Me.TabPage2.Controls.Add(Me.cbxTipoSucursal)
        Me.TabPage2.Controls.Add(Me.dtpHastaSucursal)
        Me.TabPage2.Controls.Add(Me.dtpDesdeSucursal)
        Me.TabPage2.Controls.Add(Me.btn10)
        Me.TabPage2.Controls.Add(Me.chkBancoSucursal)
        Me.TabPage2.Controls.Add(Me.cbxBancoSucursal)
        Me.TabPage2.Controls.Add(Me.chkClienteSucursal)
        Me.TabPage2.Controls.Add(Me.cbxClienteSucursal)
        Me.TabPage2.Controls.Add(Me.cbxSucursal)
        Me.TabPage2.Location = New System.Drawing.Point(4, 4)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(217, 464)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Sucursal"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(6, 336)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(203, 24)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "Cheques Rechazados"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btn13
        '
        Me.btn13.Location = New System.Drawing.Point(6, 366)
        Me.btn13.Name = "btn13"
        Me.btn13.Size = New System.Drawing.Size(203, 24)
        Me.btn13.TabIndex = 14
        Me.btn13.Text = "Todos"
        Me.btn13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn13.UseVisualStyleBackColor = True
        '
        'btn12
        '
        Me.btn12.Location = New System.Drawing.Point(6, 306)
        Me.btn12.Name = "btn12"
        Me.btn12.Size = New System.Drawing.Size(203, 24)
        Me.btn12.TabIndex = 12
        Me.btn12.Text = "Cheques Depositados"
        Me.btn12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn12.UseVisualStyleBackColor = True
        '
        'btn11
        '
        Me.btn11.Location = New System.Drawing.Point(6, 276)
        Me.btn11.Name = "btn11"
        Me.btn11.Size = New System.Drawing.Size(203, 24)
        Me.btn11.TabIndex = 11
        Me.btn11.Text = "Cheques no Cancelados"
        Me.btn11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn11.UseVisualStyleBackColor = True
        '
        'chkFechaSucursal
        '
        Me.chkFechaSucursal.AutoSize = True
        Me.chkFechaSucursal.Location = New System.Drawing.Point(6, 164)
        Me.chkFechaSucursal.Name = "chkFechaSucursal"
        Me.chkFechaSucursal.Size = New System.Drawing.Size(59, 17)
        Me.chkFechaSucursal.TabIndex = 7
        Me.chkFechaSucursal.Text = "Fecha:"
        Me.chkFechaSucursal.UseVisualStyleBackColor = True
        '
        'chkTipoSucursal
        '
        Me.chkTipoSucursal.AutoSize = True
        Me.chkTipoSucursal.Location = New System.Drawing.Point(6, 120)
        Me.chkTipoSucursal.Name = "chkTipoSucursal"
        Me.chkTipoSucursal.Size = New System.Drawing.Size(50, 17)
        Me.chkTipoSucursal.TabIndex = 5
        Me.chkTipoSucursal.Text = "Tipo:"
        Me.chkTipoSucursal.UseVisualStyleBackColor = True
        '
        'cbxTipoSucursal
        '
        Me.cbxTipoSucursal.Enabled = False
        Me.cbxTipoSucursal.FormattingEnabled = True
        Me.cbxTipoSucursal.Location = New System.Drawing.Point(6, 137)
        Me.cbxTipoSucursal.Name = "cbxTipoSucursal"
        Me.cbxTipoSucursal.Size = New System.Drawing.Size(203, 21)
        Me.cbxTipoSucursal.TabIndex = 6
        '
        'dtpHastaSucursal
        '
        Me.dtpHastaSucursal.Enabled = False
        Me.dtpHastaSucursal.Location = New System.Drawing.Point(6, 207)
        Me.dtpHastaSucursal.Name = "dtpHastaSucursal"
        Me.dtpHastaSucursal.Size = New System.Drawing.Size(203, 20)
        Me.dtpHastaSucursal.TabIndex = 9
        '
        'dtpDesdeSucursal
        '
        Me.dtpDesdeSucursal.Enabled = False
        Me.dtpDesdeSucursal.Location = New System.Drawing.Point(6, 181)
        Me.dtpDesdeSucursal.Name = "dtpDesdeSucursal"
        Me.dtpDesdeSucursal.Size = New System.Drawing.Size(203, 20)
        Me.dtpDesdeSucursal.TabIndex = 8
        '
        'btn10
        '
        Me.btn10.Location = New System.Drawing.Point(6, 246)
        Me.btn10.Name = "btn10"
        Me.btn10.Size = New System.Drawing.Size(203, 24)
        Me.btn10.TabIndex = 10
        Me.btn10.Text = "Cheques en Cartera"
        Me.btn10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn10.UseVisualStyleBackColor = True
        '
        'chkBancoSucursal
        '
        Me.chkBancoSucursal.AutoSize = True
        Me.chkBancoSucursal.Location = New System.Drawing.Point(6, 76)
        Me.chkBancoSucursal.Name = "chkBancoSucursal"
        Me.chkBancoSucursal.Size = New System.Drawing.Size(60, 17)
        Me.chkBancoSucursal.TabIndex = 3
        Me.chkBancoSucursal.Text = "Banco:"
        Me.chkBancoSucursal.UseVisualStyleBackColor = True
        '
        'cbxBancoSucursal
        '
        Me.cbxBancoSucursal.Enabled = False
        Me.cbxBancoSucursal.FormattingEnabled = True
        Me.cbxBancoSucursal.Location = New System.Drawing.Point(6, 93)
        Me.cbxBancoSucursal.Name = "cbxBancoSucursal"
        Me.cbxBancoSucursal.Size = New System.Drawing.Size(203, 21)
        Me.cbxBancoSucursal.TabIndex = 4
        '
        'chkClienteSucursal
        '
        Me.chkClienteSucursal.AutoSize = True
        Me.chkClienteSucursal.Location = New System.Drawing.Point(6, 33)
        Me.chkClienteSucursal.Name = "chkClienteSucursal"
        Me.chkClienteSucursal.Size = New System.Drawing.Size(61, 17)
        Me.chkClienteSucursal.TabIndex = 1
        Me.chkClienteSucursal.Text = "Cliente:"
        Me.chkClienteSucursal.UseVisualStyleBackColor = True
        '
        'cbxClienteSucursal
        '
        Me.cbxClienteSucursal.Enabled = False
        Me.cbxClienteSucursal.FormattingEnabled = True
        Me.cbxClienteSucursal.Location = New System.Drawing.Point(6, 49)
        Me.cbxClienteSucursal.Name = "cbxClienteSucursal"
        Me.cbxClienteSucursal.Size = New System.Drawing.Size(203, 21)
        Me.cbxClienteSucursal.TabIndex = 2
        '
        'cbxSucursal
        '
        Me.cbxSucursal.FormattingEnabled = True
        Me.cbxSucursal.Location = New System.Drawing.Point(6, 6)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.Size = New System.Drawing.Size(203, 21)
        Me.cbxSucursal.TabIndex = 0
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel4, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.lvDocumentos, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.lvOperacion, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1, 0, 2)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 4
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(633, 509)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 4
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.01285!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.98715!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 59.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Panel5, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel1, 2, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel6, 3, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(4, 275)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(625, 28)
        Me.TableLayoutPanel4.TabIndex = 4
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Label5)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(3, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(404, 22)
        Me.Panel5.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(274, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Lista de Documentos asociados al cheque seleccionado"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblCantidadDocumentos)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(502, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(60, 22)
        Me.Panel1.TabIndex = 1
        '
        'lblCantidadDocumentos
        '
        Me.lblCantidadDocumentos.AutoSize = True
        Me.lblCantidadDocumentos.Location = New System.Drawing.Point(3, 8)
        Me.lblCantidadDocumentos.Name = "lblCantidadDocumentos"
        Me.lblCantidadDocumentos.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadDocumentos.TabIndex = 0
        Me.lblCantidadDocumentos.Text = "Cantidad:"
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.txtCantidadDocumentos)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(568, 3)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(54, 22)
        Me.Panel6.TabIndex = 2
        '
        'txtCantidadDocumentos
        '
        Me.txtCantidadDocumentos.Color = System.Drawing.Color.Empty
        Me.txtCantidadDocumentos.Decimales = True
        Me.txtCantidadDocumentos.Indicaciones = Nothing
        Me.txtCantidadDocumentos.Location = New System.Drawing.Point(3, 1)
        Me.txtCantidadDocumentos.Name = "txtCantidadDocumentos"
        Me.txtCantidadDocumentos.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadDocumentos.SoloLectura = True
        Me.txtCantidadDocumentos.TabIndex = 0
        Me.txtCantidadDocumentos.TabStop = False
        Me.txtCantidadDocumentos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadDocumentos.Texto = "0"
        '
        'lvDocumentos
        '
        Me.lvDocumentos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvDocumentos.Location = New System.Drawing.Point(4, 310)
        Me.lvDocumentos.Name = "lvDocumentos"
        Me.lvDocumentos.Size = New System.Drawing.Size(625, 195)
        Me.lvDocumentos.TabIndex = 0
        Me.lvDocumentos.UseCompatibleStateImageBehavior = False
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 3
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 58.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Panel2, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel3, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel4, 2, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(4, 4)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(625, 30)
        Me.TableLayoutPanel3.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblCantidadCheque)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(504, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(60, 24)
        Me.Panel2.TabIndex = 2
        '
        'lblCantidadCheque
        '
        Me.lblCantidadCheque.AutoSize = True
        Me.lblCantidadCheque.Location = New System.Drawing.Point(3, 4)
        Me.lblCantidadCheque.Name = "lblCantidadCheque"
        Me.lblCantidadCheque.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadCheque.TabIndex = 0
        Me.lblCantidadCheque.Text = "Cantidad:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.txtNroCheque)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.lblNroCheque)
        Me.Panel3.Controls.Add(Me.lblOperacion)
        Me.Panel3.Controls.Add(Me.txtOperacion)
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(495, 24)
        Me.Panel3.TabIndex = 1
        '
        'txtNroCheque
        '
        Me.txtNroCheque.Color = System.Drawing.Color.Empty
        Me.txtNroCheque.Decimales = True
        Me.txtNroCheque.Indicaciones = Nothing
        Me.txtNroCheque.Location = New System.Drawing.Point(362, 1)
        Me.txtNroCheque.Name = "txtNroCheque"
        Me.txtNroCheque.Size = New System.Drawing.Size(130, 22)
        Me.txtNroCheque.SoloLectura = False
        Me.txtNroCheque.TabIndex = 6
        Me.txtNroCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNroCheque.Texto = "0"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(9, 1)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 20)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Cheque Cliente:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNroCheque
        '
        Me.lblNroCheque.AutoSize = True
        Me.lblNroCheque.Location = New System.Drawing.Point(276, 4)
        Me.lblNroCheque.Name = "lblNroCheque"
        Me.lblNroCheque.Size = New System.Drawing.Size(85, 13)
        Me.lblNroCheque.TabIndex = 2
        Me.lblNroCheque.Text = "Nro. de Cheque:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(144, 4)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'txtOperacion
        '
        Me.txtOperacion.Color = System.Drawing.Color.Empty
        Me.txtOperacion.Decimales = True
        Me.txtOperacion.Indicaciones = Nothing
        Me.txtOperacion.Location = New System.Drawing.Point(205, 1)
        Me.txtOperacion.Name = "txtOperacion"
        Me.txtOperacion.Size = New System.Drawing.Size(68, 22)
        Me.txtOperacion.SoloLectura = False
        Me.txtOperacion.TabIndex = 1
        Me.txtOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOperacion.Texto = "0"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.txtCantidadCheque)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(570, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(52, 24)
        Me.Panel4.TabIndex = 0
        '
        'txtCantidadCheque
        '
        Me.txtCantidadCheque.Color = System.Drawing.Color.Empty
        Me.txtCantidadCheque.Decimales = True
        Me.txtCantidadCheque.Indicaciones = Nothing
        Me.txtCantidadCheque.Location = New System.Drawing.Point(3, 1)
        Me.txtCantidadCheque.Name = "txtCantidadCheque"
        Me.txtCantidadCheque.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadCheque.SoloLectura = True
        Me.txtCantidadCheque.TabIndex = 0
        Me.txtCantidadCheque.TabStop = False
        Me.txtCantidadCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadCheque.Texto = "0"
        '
        'lvOperacion
        '
        Me.lvOperacion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvOperacion.Location = New System.Drawing.Point(4, 41)
        Me.lvOperacion.Name = "lvOperacion"
        Me.lvOperacion.Size = New System.Drawing.Size(625, 194)
        Me.lvOperacion.TabIndex = 2
        Me.lvOperacion.UseCompatibleStateImageBehavior = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalSaldo)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalCheques)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label2)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(4, 242)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(625, 26)
        Me.FlowLayoutPanel1.TabIndex = 3
        '
        'txtTotalSaldo
        '
        Me.txtTotalSaldo.Color = System.Drawing.Color.Empty
        Me.txtTotalSaldo.Decimales = True
        Me.txtTotalSaldo.Indicaciones = Nothing
        Me.txtTotalSaldo.Location = New System.Drawing.Point(519, 3)
        Me.txtTotalSaldo.Name = "txtTotalSaldo"
        Me.txtTotalSaldo.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalSaldo.SoloLectura = True
        Me.txtTotalSaldo.TabIndex = 3
        Me.txtTotalSaldo.TabStop = False
        Me.txtTotalSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalSaldo.Texto = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(476, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Saldo:"
        '
        'txtTotalCheques
        '
        Me.txtTotalCheques.Color = System.Drawing.Color.Empty
        Me.txtTotalCheques.Decimales = True
        Me.txtTotalCheques.Indicaciones = Nothing
        Me.txtTotalCheques.Location = New System.Drawing.Point(367, 3)
        Me.txtTotalCheques.Name = "txtTotalCheques"
        Me.txtTotalCheques.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalCheques.SoloLectura = True
        Me.txtTotalCheques.TabIndex = 1
        Me.txtTotalCheques.TabStop = False
        Me.txtTotalCheques.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCheques.Texto = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(327, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Total:"
        '
        'frmConsultaChequeCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(876, 515)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmConsultaChequeCliente"
        Me.Text = "frmConsultaChequeCliente"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents chkBanco As System.Windows.Forms.CheckBox
    Friend WithEvents cbxBanco As System.Windows.Forms.ComboBox
    Friend WithEvents chkCliente As System.Windows.Forms.CheckBox
    Friend WithEvents cbxCliente As System.Windows.Forms.ComboBox
    Friend WithEvents btn1 As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidadCheque As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtOperacion As ERP.ocxTXTNumeric
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidadCheque As ERP.ocxTXTNumeric
    Friend WithEvents lvOperacion As System.Windows.Forms.ListView
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotalSaldo As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTotalCheques As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtpHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblNroCheque As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents chkFecha As System.Windows.Forms.CheckBox
    Friend WithEvents chkTipo As System.Windows.Forms.CheckBox
    Friend WithEvents cbxTipo As System.Windows.Forms.ComboBox
    Friend WithEvents btn4 As System.Windows.Forms.Button
    Friend WithEvents btn3 As System.Windows.Forms.Button
    Friend WithEvents btn2 As System.Windows.Forms.Button
    Friend WithEvents btn13 As System.Windows.Forms.Button
    Friend WithEvents btn12 As System.Windows.Forms.Button
    Friend WithEvents btn11 As System.Windows.Forms.Button
    Friend WithEvents chkFechaSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents chkTipoSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents cbxTipoSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents dtpHastaSucursal As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDesdeSucursal As System.Windows.Forms.DateTimePicker
    Friend WithEvents btn10 As System.Windows.Forms.Button
    Friend WithEvents chkBancoSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents cbxBancoSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents chkClienteSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents cbxClienteSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidadDocumentos As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidadDocumentos As ERP.ocxTXTNumeric
    Friend WithEvents lvDocumentos As System.Windows.Forms.ListView
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNroCheque As ERP.ocxTXTNumeric
End Class

﻿Public Class frmSeleccionarChequeRechazar
    Dim CSistema As New CSistema


    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property
    'ID Cuenta Bancaria
    Private IDCuentaBancariaValue As Integer
    Public Property IDCuentaBancaria() As Integer
        Get
            Return IDCuentaBancariaValue
        End Get
        Set(ByVal value As Integer)
            IDCuentaBancariaValue = value
        End Set
    End Property

    Private SeleccionadoValue As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionadoValue = value
        End Set
    End Property

    

    Sub Inicializar()



        'Listar lvListar
        Listar()

    End Sub

    Sub Listar()

        'Select * From VDetalleDepositoBancarioCheque Where Depositado='True And IDCuentaBancariaDeposito=IDCuentaBancaria'

        Dim sql As String = "Select IDTransaccion, CodigoTipo, NroCheque, Importe, Cliente, Estado From VDetalleDepositoBancarioCheque Where Depositado='True' And IDCuentaBancariaDeposito= " & IDCuentaBancaria & " And NroCheque like '%" & txtNroCheque.txt.Text & "%'"
        CSistema.SqlToLv(LvComprobantes, sql)
        CSistema.FormatoMoneda(LvComprobantes, 3)
        LvComprobantes.Columns(0).Width = 0

    End Sub

    Sub seleccionar()

        If LvComprobantes.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        'For Each item As ListViewItem In LvListar.SelectedItems
        '    IDTransaccion = item.SubItems(7).Text()
        'Next

        IDTransaccion = LvComprobantes.SelectedItems(0).SubItems(0).Text
        Seleccionado = True

        Me.Close()

    End Sub

    Private Sub frmSeleccionarNotaCreditoAplicacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub BtnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancelar.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        seleccionar()
    End Sub

    Private Sub LvListar_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles LvComprobantes.KeyUp
        If e.KeyCode = Keys.Enter Then
            seleccionar()
        End If
    End Sub

    Private Sub txtNroCheque_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroCheque.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            Listar()
        End If
    End Sub
End Class
﻿Public Class frmMotivoRechazo

    'CLASES
    Dim Csistema As New CSistema

    'PROPIEDADES
    Private IDMotivoValue As Integer
    Public Property IDMotivo() As Integer
        Get
            Return IDMotivoValue
        End Get
        Set(ByVal value As Integer)
            IDMotivoValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        Procesado = False
        IDMotivo = 0

        'Funciones
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Motivo de Rechazo
        Csistema.SqlToComboBox(cbxMotivoRechazo, "Select Id, Descripcion From MotivoRechazoCheque ")

    End Sub

    Sub Procesar()

        IDMotivo = cbxMotivoRechazo.SelectedValue
        Procesado = True
        Me.Close()

    End Sub

    Sub Cancelar()

        Procesado = False
        Me.Close()

    End Sub

    Private Sub frmMotivoRechazo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Procesar()
    End Sub

End Class
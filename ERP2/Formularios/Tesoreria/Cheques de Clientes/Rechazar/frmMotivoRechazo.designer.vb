﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMotivoRechazo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbxMotivoRechazo = New System.Windows.Forms.ComboBox()
        Me.lblMotivoRechazo = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cbxMotivoRechazo
        '
        Me.cbxMotivoRechazo.FormattingEnabled = True
        Me.cbxMotivoRechazo.Location = New System.Drawing.Point(15, 36)
        Me.cbxMotivoRechazo.Name = "cbxMotivoRechazo"
        Me.cbxMotivoRechazo.Size = New System.Drawing.Size(183, 21)
        Me.cbxMotivoRechazo.TabIndex = 0
        '
        'lblMotivoRechazo
        '
        Me.lblMotivoRechazo.AutoSize = True
        Me.lblMotivoRechazo.Location = New System.Drawing.Point(12, 20)
        Me.lblMotivoRechazo.Name = "lblMotivoRechazo"
        Me.lblMotivoRechazo.Size = New System.Drawing.Size(109, 13)
        Me.lblMotivoRechazo.TabIndex = 1
        Me.lblMotivoRechazo.Text = "Seleccione el Motivo:"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(42, 63)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 2
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(123, 63)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 3
        Me.btnSalir.Text = "&Cancelar"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'frmMotivoRechazo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(209, 99)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.lblMotivoRechazo)
        Me.Controls.Add(Me.cbxMotivoRechazo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmMotivoRechazo"
        Me.Text = "MotivoRechazo"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxMotivoRechazo As System.Windows.Forms.ComboBox
    Friend WithEvents lblMotivoRechazo As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
End Class

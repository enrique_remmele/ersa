﻿Public Class frmDescuentodeChequeSeleccionarCheque

    'CLASES
    Dim CSistema As New CSistema

    'EVENTOS
    Public Event VentasSeleccionadas(ByVal sender As Object, ByVal e As EventArgs)

    'PROPIEDADES

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Funciones
        Listar()
        'Cargar()

        'Foco
        dgw.Focus()

    End Sub

    Sub Listar()

        'Limpiar la grilla
        dgw.Rows.Clear()
        Dim TotalDeuda As Decimal = 0

        For Each oRow As DataRow In dt.Rows
            Dim oRow1(16) As String

            oRow1(0) = oRow("IDTransaccion").ToString
            oRow1(1) = CBool(oRow("Sel").ToString)
            oRow1(2) = oRow("NroCheque").ToString
            oRow1(3) = oRow("Ciudad").ToString
            oRow1(4) = oRow("Banco").ToString
            oRow1(5) = oRow("CuentaBancaria").ToString
            oRow1(6) = oRow("Librador").ToString
            oRow1(7) = oRow("Cliente").ToString
            oRow1(8) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            oRow1(9) = oRow("Tipo").ToString
            oRow1(10) = oRow("Fec").ToString
            oRow1(11) = oRow("Vencimiento").ToString
            'oRow1(16) = CBool(oRow("Cancelar").ToString)


            'Sumar el total de la deuda
            ' TotalDeuda = TotalDeuda + CDec(oRow("Saldo").ToString)

            dgw.Rows.Add(oRow1)

        Next
        CalcularTotales()


    End Sub

    Sub Seleccionar()

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells("colIDTransaccion").Value

            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Sel") = oRow.Cells("colSeleccionar").Value
                oRow1("Importe") = oRow.Cells("colImporte").Value
                oRow1("Cancelar") = oRow.Cells("colCancelar").Value
            Next

        Next

        Me.Close()

    End Sub

    Sub Cargar()

        For Each oRow As DataRow In dt.Rows

            Dim oRow1(8) As String

            oRow1(0) = "0"
            oRow1(1) = CBool(oRow("Sel").ToString)
            oRow1(2) = oRow("NroCheque").ToString
            oRow1(3) = oRow("CodigoTipo").ToString
            oRow1(4) = oRow("Fec").ToString
            oRow1(5) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            oRow1(6) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            oRow1(7) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            oRow1(8) = CBool(oRow("Cancelar").ToString)

            dgw.Rows.Add(oRow1)

        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim TotalComprobantes As Decimal = 0
        Dim TotalSeleccionado As Decimal = 0
        Dim CantidadComprobantes As Integer = 0
        Dim CantidadSeleccionado As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows

            If oRow.Cells("colSeleccionar").Value = True Then
                TotalSeleccionado = TotalSeleccionado + oRow.Cells("colImporte").Value
                CantidadSeleccionado = CantidadSeleccionado + 1
            End If

            TotalComprobantes = TotalComprobantes + oRow.Cells("colImporte").Value
            CantidadComprobantes = CantidadComprobantes + 1
        Next

        txtTotalComprobantes.SetValue(TotalComprobantes)
        txtCantidadComprobantes.SetValue(CantidadComprobantes)

        txtTotalSelecionado.SetValue(TotalSeleccionado)
        txtCantidadSeleccionado.SetValue(CantidadSeleccionado)

    End Sub

    Sub PintarCelda()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(1).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next

        If dgw.CurrentRow Is Nothing Then
            Exit Sub
        End If

        dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Columns(e.ColumnIndex).Name = "colImporte" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value

            Else
                dgw.CurrentCell.Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells("colImporte").Value)
            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Space Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSeleccionar").Value = False Then
                        oRow.Cells("colSeleccionar").Value = True
                        oRow.Cells("colSeleccionar").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colCancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("colImporte")
                    Else
                        oRow.Cells("colSeleccionar").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colSeleccionar").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colImporte")
            Else
                btnAceptar.Focus()
            End If

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If

    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Index = e.RowIndex Then
                If oRow.Cells("colSeleccionar").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                End If

                oRow.DefaultCellStyle.Font = f1

                oRow.Cells("colImporte").Selected = True

            Else

                oRow.DefaultCellStyle.Font = f2

                If oRow.Cells("colSeleccionar").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.White
                Else
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                End If
            End If
        Next
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells("colImporte")
        End If

    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = True
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = False
        Next

        PintarCelda()
        CalcularTotales()
    End Sub

    Private Sub TableLayoutPanel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles TableLayoutPanel1.Paint

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub frmDescuentodeChequeSeleccionarCheque_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class
﻿Public Class frmConsultaDescuentoCheque

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim IDOperacion As Integer
    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'Form

        'TextBox
        txtCantidadCobranza.txt.ResetText()
        txtCantidadComprobantes.txt.ResetText()
        txtNroComprobante.txt.ResetText()
        txtOperacion.txt.ResetText()
        txtTotalCobranza.txt.ResetText()

        'CheckBox
        chkTipoComprobante.Checked = False
        chkCuentaBancaria.Checked = False
        chkFecha.Checked = False


        'ComboBox
        cbxTipoComprobante.Enabled = False
        cbxCuentaBancaria.Enabled = False

        'ListView
        lvComprobantes.Items.Clear()
        lvOperacion.Items.Clear()

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion(frmDescuentodeCheques.Name, "DESCUENTO CHEQUE", "DES")
        CargarInformacion()

        'Foco

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Cuenta Bancaria
        CSistema.SqlToComboBox(cbxCuentaBancaria, "Select  ID, CuentaBancaria From CuentaBancaria Order By 2")

        'TipoComprobante
        CSistema.SqlToComboBox(cbxTipoComprobante, "Select ID, Descripcion From VTipoComprobante Where IDOperacion=" & IDOperacion & " Order By 2")


        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Cuenta Bancaria
        chkCuentaBancaria.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA ACTIVO", "False")
        cbxCuentaBancaria.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA", "")

        'Tipo de Comprobante
        chkTipoComprobante.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO", "False")
        cbxTipoComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", "")



        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Cuenta Bancaria
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA ACTIVO", chkCuentaBancaria.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA", cbxCuentaBancaria.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO", chkTipoComprobante.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", cbxTipoComprobante.Text)



    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Deposito
    Sub ListarDeposito(Optional ByVal Condicion As String = "")

        ctrError.Clear()

        'Inicializr el Where
        Where = ""

        Consulta = "Select IDtransaccion, Cuenta, Banco,'T.Comp'= DescripcionTipoComprobante, Fec,Observacion,TotalDescontado,TotalAcreditado From VDescuentoCheque  "

        'Cuenta Bancaria
        If EstablecerCondicion(cbxCuentaBancaria, chkCuentaBancaria, "IDCuentaBancaria", "Seleccione correctamente el cliente!") = False Then
            Exit Sub
        End If

        'Comprobante
        If EstablecerCondicion(cbxTipoComprobante, chkTipoComprobante, "IDTipoComprobante", "Seleccione correctamente el tipo de comprobante!") = False Then
            Exit Sub
        End If

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la sucursal!") = False Then
            Exit Sub
        End If

        'Fecha
        If chkFecha.Checked = True Then
            If Where = "" Then
                Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' ) "
            Else
                Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' )  "
            End If
        End If

        lvOperacion.Items.Clear()
        lvComprobantes.Items.Clear()

        'Buscar por Operacion
        If Condicion <> "" Then
            Where = Condicion
        End If

        'buscar por Comprobantes

        If Condicion <> "" Then
            Where = Condicion
        End If



        CSistema.SqlToLv(lvOperacion, Consulta & " " & Where & " Order By Fecha")

        'Formato

        'Totales
        CSistema.FormatoMoneda(lvOperacion, 6)
        CSistema.TotalesLv(lvOperacion, txtTotalCobranza.txt, 6)
        txtCantidadCobranza.txt.Text = lvOperacion.Items.Count
        lvOperacion.Columns(0).Width = 0

    End Sub

    'Listar Comprobantes
    Sub ListarComprobantes(ByVal vIDTransaccion As Integer)

        'Limpiar ListView
        lvComprobantes.Items.Clear()

        Dim sql As String = "Select Ciudad,Cuenta,Banco, Observacion,TotalDescontado,TotalAcreditado From VDescuentoCheque Where IDTransaccion =" & vIDTransaccion

        CSistema.SqlToLv(lvComprobantes, sql)
        CSistema.FormatoMoneda(lvComprobantes, 4)
        'CSistema.FormatoMoneda(lvComprobantes, 5)

        CSistema.TotalesLv(lvComprobantes, txtTotalComprobantes.txt, 4)
        txtCantidadComprobantes.txt.Text = lvComprobantes.Items.Count

    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        If lvOperacion.SelectedItems.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(lvOperacion.SelectedItems(0).SubItems(0).Text) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = lvOperacion.SelectedItems(0).SubItems(0).Text

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub frmConsultaChequeCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkCliente_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCuentaBancaria.CheckedChanged
        HabilitarControles(chkCuentaBancaria, cbxCuentaBancaria)
    End Sub

    Private Sub chkBanco_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipoComprobante.CheckedChanged
        HabilitarControles(chkTipoComprobante, cbxTipoComprobante)
    End Sub

    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged
        HabilitarControles(chkFecha, dtpDesde)
        HabilitarControles(chkFecha, dtpHasta)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        ListarDeposito()
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub

    Private Sub lvOperacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvOperacion.SelectedIndexChanged

        ctrError.Clear()

        'Validar
        If lvOperacion.SelectedItems.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(lvOperacion.SelectedItems(0).SubItems(0).Text) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        Dim vIDTransaccion As Integer

        vIDTransaccion = lvOperacion.SelectedItems(0).SubItems(0).Text

        ListarComprobantes(vIDTransaccion)

    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        SeleccionarRegistro()
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada

        If e.KeyCode = Keys.Enter Then
            Dim Condicion As String = " Where Numero = " & txtOperacion.ObtenerValor
            ListarDeposito(Condicion)
        End If


    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada

        If e.KeyCode = Keys.Enter Then

            If IsNumeric(txtNroComprobante.txt.Text) = False Then
                Exit Sub
            End If

            Dim Condicion1 As String = " Where Comprobante = " & txtNroComprobante.txt.Text
            ListarDeposito(Condicion1)

        End If

    End Sub

End Class
﻿Imports ERP.Reporte
Public Class frmDescuentodeCheques

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CAsiento As New CAsientoDescuentoCheque
    Dim CReporte As New CReporteCheque

    'EVENTOS
    Public Event RegistroInsertado(ByVal sender As Object, ByVal e As EventArgs, ByVal oRow As DataRow)

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private dtOtrosChequesValue As DataTable

    'EVENTOS

    'VARIABLES
    Dim dtCheque As New DataTable
    Dim dtOtrosCheques As New DataTable
    Dim dtCuentaBancaria As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Otros

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "DESCUENTO CHEQUE", "DES")
        vNuevo = False

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        ManejarTecla(New KeyEventArgs(Keys.End))

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtfecha)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, cbxCuenta)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, txtCambio)

        CSistema.CargaControl(vControles, txtComprobante)

        'Totales
        CSistema.CargaControl(vControles, txtTotalAcreditado)

        'Efectivo, Cheques, Otros Cheques
        CSistema.CargaControl(vControles, btnAgregarCheques)
        CSistema.CargaControl(vControles, lklEliminarVenta)

        'Ocultar registrado por 
        flpRegistradoPor.Visible = True

        'CARGAR ESTRUCTURA DEL DETALLE VENTA

        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select Distinct ID, Descripcion  From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        dtCuentaBancaria = CSistema.ExecuteToDataTable("Select * From VCuentaBancaria").Copy
        CSistema.SqlToComboBox(cbxCuenta.cbx, dtCuentaBancaria, "ID", "CuentaBancaria")

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "OPERACION", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "OPERACION", cbxCiudad.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnEliminar, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles, btnModificar)

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        vNuevo = False

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        ManejarTecla(New KeyEventArgs(Keys.End))

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If

        End If

        'Validar
        'Comprobante
        If IsNumeric(txtComprobante.txt.Text) = False Then
            CSistema.MostrarError("Debe Ingresar Solo Numeros", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.BottomRight)
            Exit Function
        End If
        'Validar Detalle
        If lvLista.Items.Count = 0 Then
            CSistema.MostrarError("Dede Ingrsar los Detalles", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.BottomRight)
        End If

        'Ciudad
        If cbxCiudad.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente la ciudad!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Tipo Comprobante
        If cbxTipoComprobante.cbx.Text.Trim = "" Then
            Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
            Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
            ctrError.SetError(cbxTipoComprobante, mensaje)
            ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Sucursal
        If cbxSucursal.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If cbxSucursal.cbx.Text.Trim = "" Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Lote
        ' If cbxLote.Validar("Seleccione correctamente la sucursal!", ctrError, btnGuardar, tsslEstado) = False Then
        't Function
        ' End If

        'Venta
        If lvLista.Items.Count = 0 Then
            'Dim mensaje As String = "El documento tiene que tener comprobantes de venta!"
            'ctrError.SetError(btnGuardar, mensaje)
            'ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            'tsslEstado.Text = mensaje
            'Exit Function
        End If

        'If txtTotalVenta.ObtenerValor <= 0 Then
        '    Dim mensaje As String = "El importe de los comprobantes de venta no es valido!"
        '    ctrError.SetError(btnGuardar, mensaje)
        '    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
        '    tsslEstado.Text = mensaje
        '    Exit Function
        'End If

        'Forma de Pago

        ''Saldo
        'If txtSaldoTotal.ObtenerValor <> 0 Then
        '    Dim mensaje As String = "El importe de los comprobantes de venta y la forma de pago no concuerdan!"
        '    ctrError.SetError(btnGuardar, mensaje)
        '    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
        '    tsslEstado.Text = mensaje
        '    Exit Function
        'End If

        'Asiento
        'Dim SaldoAsiento As Decimal = CDec(CAsientoContableCobranzaCredito.CAsiento.dtAsiento(0)("Saldo").ToString)

        'If SaldoAsiento <> 0 Then
        '    Dim mensaje As String = "El saldo del asiento no es correcto!"
        '    ctrError.SetError(btnGuardar, mensaje)
        '    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
        '    tsslEstado.Text = mensaje
        '    Exit Function
        'End If

        Return True


    End Function

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Limpiar detalle
        ' ListarComprobantes()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        CAsiento.Limpiar()

        vNuevo = True

        'Cabecera
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()
        'detalle
        lvLista.Items.Clear()
        txtTotalDescontado.txt.Clear()
        txtTotalAcreditado.txt.Clear()
        txtGastoBancario.txt.Clear()
        txtCantidadDepositado.txt.Clear()

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From VDescuentoCheque Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)

        dtCheque = CSistema.ExecuteToDataTable("Select *, 'Sel'='False', 'Cancelar'='False' from VChequeCliente Where Cartera = 'True' And Diferido='True' Order By Vencimiento Desc").Copy

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Poner el foco en el proveedor
        cbxTipoComprobante.cbx.Focus()

        'Ocultar Registro
        flpRegistradoPor.Visible = False

        'Limpiar lvcomprobantes
        lvComprobantes.Items.Clear()



    End Sub

    Sub Modificar()

        'Validar
        If txtTotalAcreditado.ObtenerValor > 0 Then
            MessageBox.Show("El registro no se puede volver a modificar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.MODIFICAR)

        txtTotalAcreditado.SoloLectura = False
        txtObservacion.SoloLectura = False

        txtTotalAcreditado.Focus()

    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtfecha.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCuentaBancaria", cbxCuenta.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(txtMoneda.txt.Text), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalAcreditado", txtTotalAcreditado.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDescontado", txtTotalDescontado.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@GastoBancario", txtGastoBancario.ObtenerValor, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpDescuentoCheque", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From CobranzaContado Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpDescuentoCheque", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        Dim Procesar As Boolean = True


        'Anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
            CargarOperacion(IDTransaccion)

            txtID.SoloLectura = False
        End If

        'Modificar
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.UPD Then
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
            CargarOperacion(IDTransaccion)
            txtID.SoloLectura = False
            GoTo CargarAsiento
        End If

        'Guardar detalle
        If IDTransaccion > 0 Then

            'Guardar Efectivo
            Procesar = InsertarDetalle(IDTransaccion)


        End If

        'Aplicar el Deposito Bancario
        '
        Try

            ReDim param(-1)

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            'Aplicar
            If CSistema.ExecuteStoreProcedure(param, "SpDescuentoChequeProcesar", False, False, MensajeRetorno) = False Then

            End If

            GoTo CargarAsiento

        Catch ex As Exception

        End Try

CargarAsiento:

        'Cargamos el asiento
        CAsiento.IDTransaccion = IDTransaccion
        GenerarAsiento()
        CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False

    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "DESCUENTO DE CHEQUES DIFERIDOS - " & txtID.ObtenerValor

            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From DescuentoCheque Where Numero=" & txtID.ObtenerValor & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "DESCUENTO DE CHEQUES DIFERIDOS - " & txtID.ObtenerValor

            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            CAsiento.ListarDetalle(frm.dgv)
            frm.CalcularTotales()

            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento
            frm.CAsiento.NoAgrupar = False

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                CAsiento.dtAsiento.Clear()
                CAsiento.dtDetalleAsiento.Clear()
                VisualizarAsiento()
            End If

        End If


    End Sub

    Sub GenerarAsiento()

        'Establecer Cabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDDeposito") = 1
        oRow("IDSucursal") = cbxSucursal.GetValue
        oRow("IDCiudad") = cbxCiudad.GetValue
        oRow("Fecha") = txtFecha.GetValue
        oRow("IDMoneda") = 1
        oRow("Cotizacion") = txtCambio.ObtenerValor
        oRow("IDTipoComprobante") = cbxTipoComprobante.GetValue
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = txtComprobante.GetValue
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.GetValue
        oRow("Detalle") = txtObservacion.txt.Text
        oRow("Total") = txtTotalDescontado.ObtenerValor

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)

        CAsiento.dtCheque = dtCheque
        CAsiento.Total = txtTotalDescontado.ObtenerValor
        CAsiento.TotalAcreditado = txtTotalAcreditado.ObtenerValor
        CAsiento.GastoBancario = txtGastoBancario.ObtenerValor

        CAsiento.IDCuentaBancaria = cbxCuenta.GetValue
        CAsiento.IDSucursal = cbxSucursal.GetValue

        CAsiento.Generar()

    End Sub

    Sub Eliminar()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnEliminar, mensaje)
            ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.DEL.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

       'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Eliminar
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpDescuentoCheque", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnEliminar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.ANULAR_ELIMINAR)

    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalle = True

        ' Dim id As Integer = 0

        For Each oRow As DataRow In dtCheque.Select(" Sel = 'True' ")

            Dim sql As String = "Insert Into DetalleDescuentoCheque(IDTransaccionDescuentoCheque, IDTransaccionChequeCliente) Values(" & IDTransaccion & "," & oRow("IDTransaccion").ToString & ")"
            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            ' id = id + 1

        Next

    End Function

    Sub ListarComprobantes()

        lvLista.Items.Clear()

        Dim Total As Decimal = 0

        For Each oRow As DataRow In dtCheque.Rows
            If oRow("Sel") = True Then

                Dim item As ListViewItem = New ListViewItem(oRow("IDTransaccion").ToString)
                item.SubItems.Add(oRow("CodigoTipo").ToString)
                item.SubItems.Add(oRow("Banco").ToString)
                item.SubItems.Add(oRow("NroCheque").ToString)
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Importe").ToString))
                item.SubItems.Add(oRow("Cliente").ToString)
                item.SubItems.Add(oRow("CuentaBancaria").ToString)

                Total = Total + CDec(oRow("Importe").ToString)

                lvLista.Items.Add(item)

            End If
        Next

        CalcularTotales()

        ListarComprobante()


    End Sub

    Sub ObtenerCuenta()

        If cbxCuenta.cbx.SelectedValue Is Nothing Then
            Exit Sub
        End If

        txtBanco.txt.Clear()
        txtMoneda.txt.Clear()

        For Each oRow As DataRow In dtCuentaBancaria.Select(" ID=" & cbxCuenta.cbx.SelectedValue)

            txtBanco.txt.Text = oRow("Banco").ToString
            txtMoneda.txt.Text = oRow("Mon").ToString

        Next

    End Sub

    Sub CargarCheque()

        Dim frm As New frmDescuentodeChequeSeleccionarCheque
        frm.Text = "Seleccion de Cheque"
        frm.WindowState = FormWindowState.Maximized
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtCheque
        frm.Inicializar()
        frm.ShowDialog(Me)
        dtCheque = frm.dt

        ListarComprobantes()

        txtTotalAcreditado.txt.SelectAll()
        txtTotalAcreditado.Focus()

    End Sub

    Sub CalcularTotales()

        txtCantidadDepositado.SetValue(lvLista.Items.Count)

        Dim TotalCheques As Decimal = 0

        'Calcula Total Cheque
        TotalCheques = CSistema.dtSumColumn(dtCheque, "Importe", " Sel = 'True'  ")
        'TotalCheques = TotalCheques + CSistema.dtSumColumn(dtOtrosCheques, "Importe", " Sel = 'True' And Banco = '" & txtBanco.txt.Text & "' ")

        txtTotalDescontado.txt.Text = TotalCheques

    End Sub

    Sub EliminarComprobante()

        For Each item As ListViewItem In lvLista.SelectedItems

            Dim IDTransaccion As String = item.SubItems(0).Text

            'buscamos en Cheque
            For Each oRow As DataRow In dtCheque.Select(" IDTransaccion = " & IDTransaccion & "")

                oRow("Sel") = False
                Exit For
            Next

        Next

        ListarComprobantes()
    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VDescuentoCheque Where Numero=" & txtID.ObtenerValor & " and IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),0)")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VDescuentoCheque Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtfecha.SetValueFromString(oRow("Fecha").ToString)
        cbxCuenta.cbx.Text = oRow("Cuenta").ToString
        txtCambio.txt.Text = oRow("Cotizacion")
        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtTotalAcreditado.SetValue(oRow("TotalAcreditado").ToString)
        txtTotalDescontado.SetValue(oRow("TotalDescontado").ToString)
        txtGastoBancario.SetValue(oRow("GastoBancario").ToString)

        'Cargamos el detalle de Comprobantes
        'dtEfectivo = CSistema.ExecuteToDataTable("Select * From VDetalleDepositoBancarioEfectivo Where IDTransaccionDepositoBancario = " & IDTransaccion).Copy
        dtCheque = CSistema.ExecuteToDataTable("Select * From VDetalleDescuentoCheque Where IDTransaccionDepositoBancario = " & IDTransaccion).Copy

        'No hace falta, dtCheque ya trae los otros cheques
        dtOtrosCheques.Rows.Clear()

        ListarComprobantes()

        'Calcular Totales
        CalcularTotales()
        CalcularGasto()


        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        'Inicializamos el Asiento
        CAsiento.Limpiar()

    End Sub

    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    Sub Buscar()

        Dim frm As New frmConsultaDescuentoCheque
        frm.ShowDialog()
        CargarOperacion(frm.IDTransaccion)

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
    End Sub

    Sub ListarComprobante()

        'Limpiar ListView
        lvComprobantes.Items.Clear()

        Dim sql As String = ""

        For Each oRow As DataRow In dtCheque.Rows

            If sql = "" Then
                sql = sql & "Select Numero, Comprobante, Fec, Importe From VChequeClienteDocumentosPagados Where IDTransaccion=" & oRow("IDTransaccion").ToString & " "
            Else
                sql = vbCrLf & sql & " Union All Select Numero, Comprobante, Fec, Importe From VChequeClienteDocumentosPagados Where IDTransaccion=" & oRow("IDTransaccion").ToString & " "
            End If

        Next

        If sql = "" Then
            Exit Sub
        End If

        CSistema.SqlToLv(lvComprobantes, sql)
        CSistema.FormatoMoneda(lvComprobantes, 3)

        'lvComprobantes.Columns(0).Width = 0

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If vNuevo = True Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VDescuentoCheque Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VDescuentoCheque Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = MVariablesGlobales.vgKeyNuevoRegistro Then
            Nuevo()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub CalcularGasto()

        txtGastoBancario.SetValue(CDec(txtTotalDescontado.ObtenerValor) - CDec(txtTotalAcreditado.ObtenerValor))

    End Sub

    Private Sub frmDescuentodeCheques_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmDescuentodeCheques_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmDescuentodeCheques_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub cbxCuenta_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCuenta.PropertyChanged
        ObtenerCuenta()
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

    End Sub

    Private Sub btnAgregarCheques_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarCheques.Click
        CargarCheque()
    End Sub

    Private Sub lklEliminarVenta_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarVenta.LinkClicked
        EliminarComprobante()
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada

        ManejarTecla(e)


    End Sub

    Sub Imprimir()

        Dim Titulo As String = "DESCUENTO DE CHEQUES DIFERIDOS  -  " & txtfecha.txt.Text
        Dim Where As String = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = "Where IDTransaccionDescuentoCheque = " & IDTransaccion
        CReporte.DescuentoChequeDiferido(frm, Where, vgUsuarioIdentificador, Titulo)

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
        Else
            Guardar(ERP.CSistema.NUMOperacionesRegistro.UPD)
        End If
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub lvLista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvLista.SelectedIndexChanged
        'ListarComprobante()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    Private Sub txtTotalAcreditado_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtTotalAcreditado.TeclaPrecionada
        CalcularGasto()
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

End Class
﻿Public Class frmConsultaEfectivo


    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim dtBanco As DataTable
    Dim dtCliente As DataTable

    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'Form

        'TextBox
        txtCantidad.txt.ResetText()
        txtOperacion.txt.ResetText()
        txtTotal.txt.ResetText()
        txtSaldo.txt.ResetText()

        'CheckBox
        chkFecha.Checked = False
        chkFechaSucursal.Checked = False
        chkTipo.Checked = False
        chkTipoSucursal.Checked = False

        'ComboBox
        cbxTipo.Enabled = False
        cbxTipoSucursal.Enabled = False

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now
        dtpDesdeSucursal.Value = Date.Now
        dtpHastaSucursal.Value = Date.Now

        'Funciones
        CargarInformacion()

        'Foco

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'Tipo
        cbxTipo.Items.Add("VENTA CREDITO")
        cbxTipo.Items.Add("VENTA CONTADO")
        cbxTipo.Items.Add("GASTO")
        cbxTipo.Items.Add("---")
        cbxTipo.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

        'Tipo
        chkTipo.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO ACTIVO", "False")
        chkTipoSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO ACTIVO SUCURSAL", "False")
        cbxTipo.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO", "")
        cbxTipoSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO SUCURSAL", "")

    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Tipo
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO ACTIVO", chkTipo.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO ACTIVO SUCURSAL", chkTipoSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO", cbxTipo.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO SUCURSAL", cbxTipoSucursal.Text)

    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Operaciones
    Sub ListarOperaciones(Optional ByVal Numero As String = "", Optional ByVal Condicion As String = "", Optional ByVal Sucursal As Boolean = False)

        ctrError.Clear()

        Consulta = "Select 'T. Comp.'=CodigoComprobante, Comprobante, Fec, Tipo, Moneda, Cotizacion, Importe, Depositado, Saldo, Estado, Cliente, Cobranza From VEfectivo "

        Where = Condicion

        If Sucursal = True Then
            If Where = "" Then
                Where = " Where (IDSucursal=" & cbxSucursal.SelectedValue & ") "
            Else
                Where = Where & " And (IDSucursal=" & cbxSucursal.SelectedValue & ") "
            End If
        End If

        'Solo por numero
        If Numero > 0 Then
            Where = " Where Comprobante = " & Numero & ""
        End If

        'Tipo
        If Sucursal = True Then
            If chkTipoSucursal.Checked = True Then
                If Where = "" Then
                    Where = " Where (Tipo='" & cbxTipoSucursal.Text & "') "
                Else
                    Where = Where & " And (Tipo='" & cbxTipoSucursal.Text & "')  "
                End If
            End If
        Else

            If chkTipo.Checked = True Then
                If Where = "" Then
                    Where = " Where (Tipo='" & cbxTipo.Text & "') "
                Else
                    Where = Where & " And (Tipo='" & cbxTipo.Text & "')  "
                End If
            End If

        End If

        'Fecha
        If Sucursal = True Then
            If chkFechaSucursal.Checked = True Then
                If Where = "" Then
                    Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesdeSucursal, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHastaSucursal, True, False) & "' ) "
                Else
                    Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesdeSucursal, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHastaSucursal, True, False) & "' )  "
                End If
            End If

        Else

            If chkFecha.Checked = True Then
                If Where = "" Then
                    Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' ) "
                Else
                    Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' )  "
                End If
            End If

        End If

        CSistema.SqlToDataGrid(dgvLista, Consulta & " " & Where & " Order By Fecha")

        'Formato
        
        If dgvLista.ColumnCount = 0 Then
            Exit Sub
        End If

        dgvLista.Columns("Comprobante").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvLista.Columns("Fec").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvLista.Columns("Moneda").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvLista.Columns("Cotizacion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Importe").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Depositado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Saldo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Cotizacion").DefaultCellStyle.Format = "N0"
        dgvLista.Columns("Importe").DefaultCellStyle.Format = "N0"
        dgvLista.Columns("Depositado").DefaultCellStyle.Format = "N0"
        dgvLista.Columns("Saldo").DefaultCellStyle.Format = "N0"
        dgvLista.Columns("Cobranza").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        dgvLista.Columns("Cliente").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft


        'Totales
        txtTotal.txt.Text = CSistema.gridSumColumn(dgvLista, "Importe")
        txtSaldo.txt.Text = CSistema.gridSumColumn(dgvLista, "Saldo")

        txtCantidad.txt.Text = dgvLista.Rows.Count

    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    Private Sub frmConsultaChequeCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn2.Click
        ListarOperaciones(0, " Where (Cancelado='False') ", False)
    End Sub

    Private Sub btn3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn3.Click
        ListarOperaciones(0, " Where (Cancelado='True') ", False)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        ListarOperaciones(0, "", False)
    End Sub

    Private Sub chkTipo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipo.CheckedChanged
        HabilitarControles(chkTipo, cbxTipo)
    End Sub

    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged
        HabilitarControles(chkFecha, dtpDesde)
        HabilitarControles(chkFecha, dtpHasta)
    End Sub

    Private Sub chkTipoSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipoSucursal.CheckedChanged
        HabilitarControles(chkTipoSucursal, cbxTipoSucursal)
    End Sub

    Private Sub chkFechaSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFechaSucursal.CheckedChanged
        HabilitarControles(chkFechaSucursal, dtpDesdeSucursal)
        HabilitarControles(chkFechaSucursal, dtpHastaSucursal)
    End Sub

    Private Sub btn11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn11.Click
        ListarOperaciones(0, " Where (Cancelado='False') ", True)
    End Sub

    Private Sub btn12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn12.Click
        ListarOperaciones(0, " Where (Cancelado='True') ", True)
    End Sub

    Private Sub btn13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn13.Click
        ListarOperaciones(0, "", True)
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarOperaciones("'" & txtOperacion.ObtenerValor & "'")
            txtOperacion.txt.Focus()
            txtOperacion.txt.SelectAll()
        End If
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmConsultaEfectivo_Activate()
        Me.Refresh()
    End Sub

End Class
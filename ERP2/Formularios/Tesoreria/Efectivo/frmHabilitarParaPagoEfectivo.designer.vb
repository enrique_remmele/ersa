﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHabilitarParaPagoEfectivo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.QuitarTodaSeleccionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SeleccionarTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtCantidadSeleccionado = New ERP.ocxTXTNumeric()
        Me.txtTotalSelecionado = New ERP.ocxTXTNumeric()
        Me.lblTotalSaldo = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtCantidadComprobantes = New ERP.ocxTXTNumeric()
        Me.txtTotalComprobantes = New ERP.ocxTXTNumeric()
        Me.lblTotalEfectivo = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.dgw2 = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colGenerado2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colComprobante2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFecha2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colHabilitado2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colID2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTNumeric()
        Me.txtMonto = New ERP.ocxTXTNumeric()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chkFecha = New ERP.ocxCHK()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSeleccionar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colGenerado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTipoComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColHabilitado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.dgw2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        Me.dgw.AllowUserToResizeColumns = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colSeleccionar, Me.colGenerado, Me.colComprobante, Me.colTipoComprobante, Me.colFecha, Me.colTotal, Me.colSaldo, Me.colImporte, Me.ColHabilitado, Me.colID})
        Me.dgw.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgw.Location = New System.Drawing.Point(3, 43)
        Me.dgw.Name = "dgw"
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgw.Size = New System.Drawing.Size(831, 240)
        Me.dgw.TabIndex = 0
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(678, 6)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 2
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'QuitarTodaSeleccionToolStripMenuItem
        '
        Me.QuitarTodaSeleccionToolStripMenuItem.Name = "QuitarTodaSeleccionToolStripMenuItem"
        Me.QuitarTodaSeleccionToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.QuitarTodaSeleccionToolStripMenuItem.Text = "Quitar toda seleccion"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 545)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(837, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SeleccionarTodoToolStripMenuItem, Me.QuitarTodaSeleccionToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(187, 48)
        '
        'SeleccionarTodoToolStripMenuItem
        '
        Me.SeleccionarTodoToolStripMenuItem.Name = "SeleccionarTodoToolStripMenuItem"
        Me.SeleccionarTodoToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.SeleccionarTodoToolStripMenuItem.Text = "Seleccionar todo"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtCantidadSeleccionado)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalSelecionado)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblTotalSaldo)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(328, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(253, 27)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'txtCantidadSeleccionado
        '
        Me.txtCantidadSeleccionado.Color = System.Drawing.Color.Empty
        Me.txtCantidadSeleccionado.Decimales = True
        Me.txtCantidadSeleccionado.Indicaciones = Nothing
        Me.txtCantidadSeleccionado.Location = New System.Drawing.Point(205, 3)
        Me.txtCantidadSeleccionado.Name = "txtCantidadSeleccionado"
        Me.txtCantidadSeleccionado.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadSeleccionado.SoloLectura = False
        Me.txtCantidadSeleccionado.TabIndex = 2
        Me.txtCantidadSeleccionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadSeleccionado.Texto = "0"
        '
        'txtTotalSelecionado
        '
        Me.txtTotalSelecionado.Color = System.Drawing.Color.Empty
        Me.txtTotalSelecionado.Decimales = True
        Me.txtTotalSelecionado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalSelecionado.Indicaciones = Nothing
        Me.txtTotalSelecionado.Location = New System.Drawing.Point(116, 3)
        Me.txtTotalSelecionado.Name = "txtTotalSelecionado"
        Me.txtTotalSelecionado.Size = New System.Drawing.Size(83, 22)
        Me.txtTotalSelecionado.SoloLectura = True
        Me.txtTotalSelecionado.TabIndex = 1
        Me.txtTotalSelecionado.TabStop = False
        Me.txtTotalSelecionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalSelecionado.Texto = "0"
        '
        'lblTotalSaldo
        '
        Me.lblTotalSaldo.AutoSize = True
        Me.lblTotalSaldo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblTotalSaldo.Location = New System.Drawing.Point(8, 0)
        Me.lblTotalSaldo.Name = "lblTotalSaldo"
        Me.lblTotalSaldo.Size = New System.Drawing.Size(102, 28)
        Me.lblTotalSaldo.TabIndex = 0
        Me.lblTotalSaldo.Text = "Total Seleccionado:"
        Me.lblTotalSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.txtCantidadComprobantes)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtTotalComprobantes)
        Me.FlowLayoutPanel2.Controls.Add(Me.lblTotalEfectivo)
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(9, 3)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(255, 27)
        Me.FlowLayoutPanel2.TabIndex = 0
        '
        'txtCantidadComprobantes
        '
        Me.txtCantidadComprobantes.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobantes.Decimales = True
        Me.txtCantidadComprobantes.Indicaciones = Nothing
        Me.txtCantidadComprobantes.Location = New System.Drawing.Point(207, 3)
        Me.txtCantidadComprobantes.Name = "txtCantidadComprobantes"
        Me.txtCantidadComprobantes.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadComprobantes.SoloLectura = False
        Me.txtCantidadComprobantes.TabIndex = 2
        Me.txtCantidadComprobantes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobantes.Texto = "0"
        '
        'txtTotalComprobantes
        '
        Me.txtTotalComprobantes.Color = System.Drawing.Color.Empty
        Me.txtTotalComprobantes.Decimales = True
        Me.txtTotalComprobantes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalComprobantes.Indicaciones = Nothing
        Me.txtTotalComprobantes.Location = New System.Drawing.Point(118, 3)
        Me.txtTotalComprobantes.Name = "txtTotalComprobantes"
        Me.txtTotalComprobantes.Size = New System.Drawing.Size(83, 25)
        Me.txtTotalComprobantes.SoloLectura = True
        Me.txtTotalComprobantes.TabIndex = 1
        Me.txtTotalComprobantes.TabStop = False
        Me.txtTotalComprobantes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalComprobantes.Texto = "0"
        '
        'lblTotalEfectivo
        '
        Me.lblTotalEfectivo.AutoSize = True
        Me.lblTotalEfectivo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblTotalEfectivo.Location = New System.Drawing.Point(7, 0)
        Me.lblTotalEfectivo.Name = "lblTotalEfectivo"
        Me.lblTotalEfectivo.Size = New System.Drawing.Size(105, 31)
        Me.lblTotalEfectivo.TabIndex = 0
        Me.lblTotalEfectivo.Text = "Total Comprobantes:"
        Me.lblTotalEfectivo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.dgw2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.dgw, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel4, 0, 3)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 246.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 218.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(837, 567)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'dgw2
        '
        Me.dgw2.AllowUserToAddRows = False
        Me.dgw2.AllowUserToDeleteRows = False
        Me.dgw2.AllowUserToOrderColumns = True
        Me.dgw2.AllowUserToResizeColumns = False
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw2.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle7
        Me.dgw2.BackgroundColor = System.Drawing.Color.White
        Me.dgw2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion2, Me.colSel, Me.colGenerado2, Me.colComprobante2, Me.colFecha2, Me.colTotal2, Me.colSaldo2, Me.colImporte2, Me.colHabilitado2, Me.colID2})
        Me.dgw2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgw2.Location = New System.Drawing.Point(3, 289)
        Me.dgw2.Name = "dgw2"
        Me.dgw2.ReadOnly = True
        Me.dgw2.RowHeadersVisible = False
        Me.dgw2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw2.Size = New System.Drawing.Size(831, 212)
        Me.dgw2.TabIndex = 2
        '
        'colIDTransaccion2
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "C2"
        DataGridViewCellStyle8.NullValue = "0"
        Me.colIDTransaccion2.DefaultCellStyle = DataGridViewCellStyle8
        Me.colIDTransaccion2.HeaderText = "IDTransaccion"
        Me.colIDTransaccion2.Name = "colIDTransaccion2"
        Me.colIDTransaccion2.ReadOnly = True
        Me.colIDTransaccion2.Visible = False
        '
        'colSel
        '
        Me.colSel.HeaderText = "Sel."
        Me.colSel.Name = "colSel"
        Me.colSel.ReadOnly = True
        Me.colSel.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colSel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colSel.Width = 35
        '
        'colGenerado2
        '
        Me.colGenerado2.HeaderText = "Generado"
        Me.colGenerado2.Name = "colGenerado2"
        Me.colGenerado2.ReadOnly = True
        Me.colGenerado2.Width = 160
        '
        'colComprobante2
        '
        Me.colComprobante2.HeaderText = "Comprobante"
        Me.colComprobante2.Name = "colComprobante2"
        Me.colComprobante2.ReadOnly = True
        Me.colComprobante2.Width = 75
        '
        'colFecha2
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colFecha2.DefaultCellStyle = DataGridViewCellStyle9
        Me.colFecha2.HeaderText = "Fecha"
        Me.colFecha2.Name = "colFecha2"
        Me.colFecha2.ReadOnly = True
        Me.colFecha2.Width = 70
        '
        'colTotal2
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        DataGridViewCellStyle10.NullValue = "0"
        Me.colTotal2.DefaultCellStyle = DataGridViewCellStyle10
        Me.colTotal2.HeaderText = "Total"
        Me.colTotal2.Name = "colTotal2"
        Me.colTotal2.ReadOnly = True
        Me.colTotal2.Width = 80
        '
        'colSaldo2
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = "0"
        Me.colSaldo2.DefaultCellStyle = DataGridViewCellStyle11
        Me.colSaldo2.HeaderText = "Saldo"
        Me.colSaldo2.Name = "colSaldo2"
        Me.colSaldo2.ReadOnly = True
        Me.colSaldo2.Width = 80
        '
        'colImporte2
        '
        Me.colImporte2.HeaderText = "Importe Habilitado"
        Me.colImporte2.Name = "colImporte2"
        Me.colImporte2.ReadOnly = True
        Me.colImporte2.Width = 120
        '
        'colHabilitado2
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colHabilitado2.DefaultCellStyle = DataGridViewCellStyle12
        Me.colHabilitado2.HeaderText = "Habilitado"
        Me.colHabilitado2.Name = "colHabilitado2"
        Me.colHabilitado2.ReadOnly = True
        '
        'colID2
        '
        Me.colID2.HeaderText = "ID"
        Me.colID2.Name = "colID2"
        Me.colID2.ReadOnly = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(831, 34)
        Me.Panel1.TabIndex = 3
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblSucursal)
        Me.Panel2.Controls.Add(Me.cbxSucursal)
        Me.Panel2.Controls.Add(Me.txtComprobante)
        Me.Panel2.Controls.Add(Me.txtMonto)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.chkFecha)
        Me.Panel2.Controls.Add(Me.txtHasta)
        Me.Panel2.Controls.Add(Me.btnListar)
        Me.Panel2.Controls.Add(Me.txtDesde)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(831, 34)
        Me.Panel2.TabIndex = 0
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(11, 11)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(32, 13)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Text = "Suc.:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Codigo"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = "Codigo"
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(43, 7)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(63, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Decimales = True
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(357, 7)
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(91, 20)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 5
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtComprobante.Texto = ""
        '
        'txtMonto
        '
        Me.txtMonto.Color = System.Drawing.Color.Empty
        Me.txtMonto.Decimales = True
        Me.txtMonto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMonto.Indicaciones = Nothing
        Me.txtMonto.Location = New System.Drawing.Point(201, 6)
        Me.txtMonto.Name = "txtMonto"
        Me.txtMonto.Size = New System.Drawing.Size(83, 22)
        Me.txtMonto.SoloLectura = False
        Me.txtMonto.TabIndex = 3
        Me.txtMonto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMonto.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(106, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Importe a Habilitar:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkFecha
        '
        Me.chkFecha.BackColor = System.Drawing.Color.Transparent
        Me.chkFecha.Color = System.Drawing.Color.Empty
        Me.chkFecha.Location = New System.Drawing.Point(452, 7)
        Me.chkFecha.Name = "chkFecha"
        Me.chkFecha.Size = New System.Drawing.Size(104, 21)
        Me.chkFecha.SoloLectura = False
        Me.chkFecha.TabIndex = 6
        Me.chkFecha.Texto = "Fecha Cobranza"
        Me.chkFecha.Valor = False
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Enabled = False
        Me.txtHasta.Fecha = New Date(CType(0, Long))
        Me.txtHasta.Location = New System.Drawing.Point(640, 7)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(77, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 8
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(725, 6)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(85, 23)
        Me.btnListar.TabIndex = 9
        Me.btnListar.Text = "&Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Enabled = False
        Me.txtDesde.Fecha = New Date(CType(0, Long))
        Me.txtDesde.Location = New System.Drawing.Point(558, 7)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(77, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(284, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Comprobante:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.FlowLayoutPanel2)
        Me.Panel4.Controls.Add(Me.btnSalir)
        Me.Panel4.Controls.Add(Me.FlowLayoutPanel1)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(3, 507)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(831, 57)
        Me.Panel4.TabIndex = 3
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'colIDTransaccion
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = "0"
        Me.colIDTransaccion.DefaultCellStyle = DataGridViewCellStyle2
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colSeleccionar
        '
        Me.colSeleccionar.HeaderText = "Sel."
        Me.colSeleccionar.Name = "colSeleccionar"
        Me.colSeleccionar.Width = 35
        '
        'colGenerado
        '
        Me.colGenerado.HeaderText = "Generado"
        Me.colGenerado.Name = "colGenerado"
        Me.colGenerado.ReadOnly = True
        Me.colGenerado.Width = 160
        '
        'colComprobante
        '
        Me.colComprobante.HeaderText = "Comprobante"
        Me.colComprobante.Name = "colComprobante"
        Me.colComprobante.ReadOnly = True
        Me.colComprobante.Width = 75
        '
        'colTipoComprobante
        '
        Me.colTipoComprobante.HeaderText = "T.Comp"
        Me.colTipoComprobante.Name = "colTipoComprobante"
        '
        'colFecha
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colFecha.DefaultCellStyle = DataGridViewCellStyle3
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.ReadOnly = True
        Me.colFecha.Width = 70
        '
        'colTotal
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = "0"
        Me.colTotal.DefaultCellStyle = DataGridViewCellStyle4
        Me.colTotal.HeaderText = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.ReadOnly = True
        Me.colTotal.Width = 80
        '
        'colSaldo
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = "0"
        Me.colSaldo.DefaultCellStyle = DataGridViewCellStyle5
        Me.colSaldo.HeaderText = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.ReadOnly = True
        Me.colSaldo.Width = 80
        '
        'colImporte
        '
        Me.colImporte.HeaderText = "Importe Habilitado"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        Me.colImporte.Width = 120
        '
        'ColHabilitado
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.ColHabilitado.DefaultCellStyle = DataGridViewCellStyle6
        Me.ColHabilitado.HeaderText = "Habilitado"
        Me.ColHabilitado.Name = "ColHabilitado"
        Me.ColHabilitado.ReadOnly = True
        '
        'colID
        '
        Me.colID.HeaderText = "ID"
        Me.colID.Name = "colID"
        '
        'frmHabilitarParaPagoEfectivo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(837, 567)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmHabilitarParaPagoEfectivo"
        Me.Text = "frmHabilitarParaPagoEfectivo"
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.dgw2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents QuitarTodaSeleccionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SeleccionarTodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents txtCantidadSeleccionado As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalSelecionado As ERP.ocxTXTNumeric
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblTotalSaldo As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtCantidadComprobantes As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalComprobantes As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalEfectivo As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnListar As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents chkFecha As ERP.ocxCHK
    Friend WithEvents txtMonto As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgw2 As System.Windows.Forms.DataGridView
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents colIDTransaccion2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSel As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colGenerado2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colComprobante2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFecha2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotal2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSaldo2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colHabilitado2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colID2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtComprobante As ERP.ocxTXTNumeric
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents colIDTransaccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSeleccionar As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colGenerado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTipoComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSaldo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColHabilitado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colID As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿Public Class frmHabilitarParaPagoEfectivo

    'CLASES
    Dim CSistema As New CSistema
    Dim MontoHabilitar As Decimal
    Dim dtComprobantes As DataTable
    Dim Habilitado As Boolean
    Dim Filtro As String = ""
    Dim dt As DataTable

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property
    Private ImporteHabilitadoValue As Decimal
    Public Property ImporteHabilitado() As Decimal
        Get
            Return ImporteHabilitadoValue
        End Get
        Set(ByVal value As Decimal)
            ImporteHabilitadoValue = value
        End Set
    End Property


    'EVENTOS
    Public Event VentasSeleccionadas(ByVal sender As Object, ByVal e As EventArgs)

    'Variable

    Dim dtEfectivo As New DataTable

    'FUNCIONES
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Codigo From VSucursal Order By 2")

        'listar
        Listar()

        If txtTotalSelecionado.ObtenerValor <> 0 Then
            txtMonto.txt.Text = txtTotalSelecionado.ObtenerValor
        End If

        ''Foco
        dgw.Focus()

    End Sub

    Sub Listar()

        'Cargar
        dtEfectivo = CSistema.ExecuteToDataTable("Select *,'Cancelar'='False' From VEfectivo Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And Saldo>0 and Cancelado = 'False' Order By Fecha").Copy

        dt = CSistema.ExecuteToDataTable("Select Top(0)*,'Cancelar'='False','Sel'='True' From VEfectivo Where Saldo>0 and Cancelado = 'False' Order By Fecha")

        'Limpiar la grilla
        dgw.Rows.Clear()
        Dim TotalDeuda As Decimal = 0

        For Each oRow As DataRow In dtEfectivo.Select(Filtro)

            Dim oRow1(10) As String
            If oRow("Habilitado Pago") = "SI" Then
                oRow("Select") = True
            Else
                oRow("Select") = False
            End If
            oRow1(0) = oRow("IDTransaccion").ToString
            oRow1(1) = CBool(oRow("Select").ToString)
            oRow1(2) = oRow("Generado").ToString()
            oRow1(3) = oRow("Comprobante").ToString
            oRow1(4) = oRow("CodigoComprobante").ToString
            oRow1(5) = oRow("Fec").ToString
            oRow1(6) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            oRow1(7) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            oRow1(8) = CSistema.FormatoMoneda(oRow("ImporteHabilitado").ToString)
            oRow1(9) = oRow("Habilitado Pago").ToString
            oRow1(10) = oRow("ID").ToString



            'Sumar el total de la deuda
            TotalDeuda = TotalDeuda + CDec(oRow("Saldo").ToString)

            dgw.Rows.Add(oRow1)

        Next

        CargarTabla()

        CalcularTotales()


    End Sub

    Sub CargarComprobantes()
        dgw2.Rows.Clear()
        For Each oRow As DataRow In dt.Rows
            Dim Registro(9) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Sel").ToString
            Registro(2) = oRow("Generado").ToString
            Registro(3) = oRow("Comprobante").ToString
            Registro(4) = oRow("Fecha").ToString
            Registro(5) = oRow("Total").ToString
            Registro(6) = oRow("Saldo").ToString
            Registro(7) = oRow("ImporteHabilitado").ToString
            Registro(8) = oRow("Habilitado Pago").ToString
            Registro(9) = oRow("ID").ToString

            dgw2.Rows.Add(Registro)

        Next
    End Sub

    Sub CargarTabla()

        dt.Rows.Clear()

        For Each oRow As DataGridViewRow In dgw.Rows
            Dim oRow1 As DataRow = dt.NewRow
            If oRow.Cells("colSeleccionar").Value = True Then
                oRow1("IDTransaccion") = oRow.Cells("colIDTransaccion").Value
                oRow1("Sel") = oRow.Cells("colSeleccionar").Value
                oRow1("Generado") = oRow.Cells("colGenerado").Value
                oRow1("Comprobante") = oRow.Cells("colComprobante").Value
                oRow1("Fecha") = oRow.Cells("colFecha").Value
                oRow1("Total") = oRow.Cells("colTotal").Value
                oRow1("Saldo") = oRow.Cells("colSaldo").Value
                oRow1("ImporteHabilitado") = oRow.Cells("colImporte").Value
                oRow1("Habilitado Pago") = (oRow.Cells("ColHabilitado").Value)
                oRow1("ID") = oRow.Cells("colID").Value

                dt.Rows.Add(oRow1)

            End If
        Next
      

        CargarComprobantes()


    End Sub


    Sub Procesar()

        tsslEstado.Text = ""
        ctrError.Clear()


        For Each oRow As DataRow In dt.Select("IDTransaccion= " & IDTransaccion & " And Sel='" & True & "'")

            If dgw2.Rows.Count = 0 Then
                Exit Sub
            End If

            Dim param(-1) As SqlClient.SqlParameter
            Dim IndiceOperacion As Integer = 0

            'Entrada
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            If Habilitado = True Then
                CSistema.SetSQLParameter(param, "@ImporteHabilitado", CSistema.FormatoMonedaBaseDatos(oRow("ImporteHabilitado").ToString), ParameterDirection.Input)
            Else
                CSistema.SetSQLParameter(param, "@ImporteHabilitado", CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString), ParameterDirection.Input)
            End If

            CSistema.SetSQLParameter(param, "@Habilitado", Habilitado, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", oRow("ID").ToString, ParameterDirection.Input)
            IndiceOperacion = param.GetLength(0) - 1

            'Transaccion
            CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)


            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpActualizarEfectivo", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(dgw2, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(dgw2, ErrorIconAlignment.TopRight)

                Exit Sub

            End If

        Next
        Listar()
        PintarCelda()
    End Sub

    Sub Cargar()

        For Each oRow As DataRow In dtEfectivo.Rows

            Dim oRow1(8) As String

            oRow1(0) = "0"
            oRow1(1) = CBool(oRow("Select").ToString)
            oRow1(2) = oRow("CodigoComprobante").ToString
            oRow1(3) = oRow("TipoComprobante").ToString
            oRow1(4) = oRow("Fec").ToString
            oRow1(5) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            oRow1(6) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            oRow1(7) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            oRow1(8) = CBool(oRow("Cancelar").ToString)

            dgw.Rows.Add(oRow1)

        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim TotalComprobantes As Decimal = 0
        Dim TotalSeleccionado As Decimal = 0
        Dim CantidadComprobantes As Integer = 0
        Dim CantidadSeleccionado As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows

            If oRow.Cells("colSeleccionar").Value = True Then
                TotalSeleccionado = TotalSeleccionado + oRow.Cells("colImporte").Value
                CantidadSeleccionado = CantidadSeleccionado + 1
            End If

            TotalComprobantes = TotalComprobantes + oRow.Cells("colTotal").Value
            CantidadComprobantes = CantidadComprobantes + 1
        Next

        txtTotalComprobantes.SetValue(TotalComprobantes)
        txtCantidadComprobantes.SetValue(CantidadComprobantes)

        txtTotalSelecionado.SetValue(TotalSeleccionado)
        txtCantidadSeleccionado.SetValue(CantidadSeleccionado)

    End Sub

    Sub PintarCelda()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(1).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next



        If dgw.CurrentRow Is Nothing Then
            Exit Sub
        End If

        dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Columns(e.ColumnIndex).Name = "colImporte" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colImporte").Value

            Else
                dgw.CurrentCell.Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells("colImporte").Value)
            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        'If e.KeyCode = Keys.Enter Then

        '    ctrError.Clear()
        '    tsslEstado.Text = ""


        '    If dgw.SelectedCells.Count = 0 Then
        '        Exit Sub
        '    End If

        '    If txtMonto.ObtenerValor = 0 Then
        '        Exit Sub
        '    End If

        '    Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


        '    For Each oRow As DataGridViewRow In dgw.Rows

        '        If oRow.Index = RowIndex Then

        '            If oRow.Cells("colSeleccionar").Value = False Then
        '                oRow.Cells("colSeleccionar").Value = True
        '                oRow.Cells("colSeleccionar").ReadOnly = False
        '                oRow.Cells("colImporte").ReadOnly = False

        '                MontoHabilitar = txtMonto.ObtenerValor
        '                Dim TotalSeleccionado As Decimal = CSistema.gridSumColumn(dgw, "colImporte", "colSeleccionar", "True")
        '                Dim TotalActual As Decimal = oRow.Cells("colImporte").Value
        '                Dim Resultante As Decimal = 0
        '                If MontoHabilitar > TotalSeleccionado Then
        '                    oRow.Cells("colImporte").Value = TotalActual
        '                Else
        '                    oRow.Cells("colImporte").Value = MontoHabilitar - (TotalSeleccionado - TotalActual)
        '                End If

        '                oRow.Cells("colImporte").Value = CSistema.FormatoMoneda(oRow.Cells("colImporte").Value)
        '                oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

        '                CargarComprobantes()
        '                CalcularTotales()
        '                'Habilitado = True
        '                'Procesar()

        '            Else
        '                oRow.Cells("colSeleccionar").ReadOnly = True
        '                oRow.Cells("colImporte").ReadOnly = True
        '                oRow.Cells("colSeleccionar").Value = False
        '                oRow.Cells("colImporte").Value = CSistema.FormatoMoneda(oRow.Cells("colSaldo").Value)
        '                oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

        '                'Habilitado = False
        '                'Procesar()
        '                CargarComprobantes()
        '                CalcularTotales()
        '            End If

        '            Exit For

        '        End If

        '    Next

        '    If RowIndex < dgw.Rows.Count - 1 Then
        '        dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colSeleccionar")
        '    End If

        '    'CalcularTotales()

        '    dgw.Update()

        '    ' Your code here
        '    e.SuppressKeyPress = True

        'End If
    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dgw.Rows

            If oRow.Index = e.RowIndex Then

                If oRow.Cells("colSeleccionar").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                End If

                oRow.DefaultCellStyle.Font = f1

                oRow.Cells("colImporte").Selected = True

            Else

                oRow.DefaultCellStyle.Font = f2

                If oRow.Cells("colSeleccionar").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.White
                Else
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                End If
            End If

        Next
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
        End If
    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = True
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = False
        Next

        PintarCelda()
        CalcularTotales()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmHabilitarParaPagoEfectivo_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
          CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmHabilitarParaPagoEfectivo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        If txtMonto.ObtenerValor = 0 Then
            Exit Sub
        End If


        If e.ColumnIndex = dgw.Columns.Item("colSeleccionar").Index Then

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSeleccionar").Value = False Then
                        oRow.Cells("colSeleccionar").Value = True
                        oRow.Cells("colSeleccionar").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = False

                        MontoHabilitar = txtMonto.ObtenerValor
                        Dim TotalSeleccionado As Decimal = CSistema.gridSumColumn(dgw, "colImporte", "colSeleccionar", "True")
                        Dim TotalActual As Decimal = oRow.Cells("colImporte").Value
                        Dim Resultante As Decimal = 0
                        If MontoHabilitar > TotalSeleccionado Then
                            oRow.Cells("colImporte").Value = TotalActual
                        Else
                            oRow.Cells("colImporte").Value = MontoHabilitar - (TotalSeleccionado - TotalActual)
                        End If

                        oRow.Cells("colImporte").Value = CSistema.FormatoMoneda(oRow.Cells("colImporte").Value)
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        IDTransaccion = oRow.Cells("colIDTransaccion").Value

                        CargarTabla()
                        CalcularTotales()
                        Habilitado = True
                        Procesar()

                    Else
                        oRow.Cells("colSeleccionar").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colSeleccionar").Value = False
                        oRow.Cells("colImporte").Value = CSistema.FormatoMoneda(oRow.Cells("colSaldo").Value)
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

                        IDTransaccion = oRow.Cells("colIDTransaccion").Value

                        Habilitado = False
                        Procesar()
                        CargarTabla()
                        CalcularTotales()
                    End If

                    Exit For

                End If

            Next


            dgw.Update()

            'CargarComprobantes()

        End If
    End Sub


    Private Sub dgw_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgw.EditingControlShowing

        If (dgw.CurrentCell.ColumnIndex = 7) Then

            Dim oTexbox As TextBox = CType(e.Control, TextBox)
            AddHandler oTexbox.KeyPress, AddressOf dgwTextBox_Keypress

        End If

    End Sub

    Private Sub dgwTextBox_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim columna As Integer = dgw.CurrentCell.ColumnIndex

        If columna = 7 Then

            If (Char.IsNumber(e.KeyChar) Or e.KeyChar = Microsoft.VisualBasic.ChrW(46) Or e.KeyChar = Microsoft.VisualBasic.ChrW(127) Or e.KeyChar = Microsoft.VisualBasic.ChrW(8)) Then
                e.Handled = False

                If Asc(e.KeyChar) = Keys.Enter Then
                    If CSistema.FormatoNumero(dgw.Columns.Item("colImporte").ToString) > CSistema.FormatoNumero(dgw.Columns.Item("colSaldo").ToString) Then
                        Dim mensaje As String = "El Importe debe ser menor al saldo!"
                        ctrError.SetError(dgw, mensaje)
                        ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                        tsslEstado.Text = mensaje
                    End If
                End If


            Else
                e.Handled = True
            End If

        End If

    End Sub

    Private Sub dgw_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgw.KeyPress

        If Asc(e.KeyChar) = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSeleccionar").Value = False Then
                        oRow.Cells("colSeleccionar").Value = True
                        oRow.Cells("colSeleccionar").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = False
                        MontoHabilitar = txtMonto.ObtenerValor
                        Dim TotalSeleccionado As Decimal = CSistema.gridSumColumn(dgw, "colImporte", "colSeleccionar", "True")
                        Dim TotalActual As Decimal = oRow.Cells("colImporte").Value
                        Dim Resultante As Decimal = 0
                        If MontoHabilitar > TotalSeleccionado Then
                            oRow.Cells("colImporte").Value = TotalActual
                        Else
                            oRow.Cells("colImporte").Value = MontoHabilitar - (TotalSeleccionado - TotalActual)
                        End If

                        oRow.Cells("colImporte").Value = CSistema.FormatoMoneda(oRow.Cells("colImporte").Value)
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                    Else
                        oRow.Cells("colSeleccionar").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colSeleccionar").Value = False
                        oRow.Cells("colImporte").Value = oRow.Cells("colSaldo").Value
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

                    End If


                    Exit For

                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colSeleccionar")
            End If

            CalcularTotales()

            dgw.Update()

        End If

    End Sub

    Private Sub txtMonto_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtMonto.TeclaPrecionada
          MontoHabilitar = txtMonto.ObtenerValor
    End Sub

    Private Sub chkFecha_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkFecha.PropertyChanged
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub

    Private Sub btnListar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListar.Click
        If chkFecha.Valor = True Then
            Filtro = " Comprobante Like '%" & txtComprobante.ObtenerValor & "%' And (Fecha >= '" & txtDesde.GetValueString & "' And Fecha <= '" & txtHasta.GetValueString & "')"
        Else
            Filtro = " Comprobante = '" & txtComprobante.ObtenerValor & "'"
        End If
        Listar()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmHabilitarParaPagoEfectivo_Activate()
        Me.Refresh()
    End Sub

End Class

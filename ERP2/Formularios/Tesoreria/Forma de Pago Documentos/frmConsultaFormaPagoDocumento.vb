﻿Public Class frmConsultaFormaPagoDocumento


    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim dtBanco As DataTable
    Dim dtCliente As DataTable

    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'Form
        IDOperacion = CSistema.ObtenerIDOperacion(frmSeleccionFormaPagoDocumento.Name, "PAGOS Y COBROS CON DOCUMENTOS", "DOC")

        'TextBox
        txtCantidad.txt.ResetText()
        txtNroCheque.txt.ResetText()
        txtTotal.txt.ResetText()

        'CheckBox
        chkFecha.Checked = False
        chkFechaSucursal.Checked = False
        chkTipo.Checked = False
        chkTipoSucursal.Checked = False

        'ComboBox
        cbxTipoComprobante.Enabled = False
        cbxTipoComprobanteSucursal.Enabled = False

        'ListView
        lvOperacion.Items.Clear()

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now
        dtpDesdeSucursal.Value = Date.Now
        dtpHastaSucursal.Value = Date.Now

        'Funciones
        CargarInformacion()

        'Foco

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)
        CSistema.SqlToComboBox(cbxTipoComprobanteSucursal, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

        'Tipo
        chkTipo.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO", "False")
        chkTipoSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO SUCURSAL", "False")
        cbxTipoComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", "")
        cbxTipoComprobanteSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE SUCURSAL", "")

    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Tipo
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO", chkTipo.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO SUCURSAL", chkTipoSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", cbxTipoComprobante.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE SUCURSAL", cbxTipoComprobanteSucursal.Text)

    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Operaciones
    Sub ListarOperaciones(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "", Optional ByVal Sucursal As Boolean = False)

        ctrError.Clear()

        Consulta = "Select TipoComprobante, Comprobante, Fec, Moneda, ImporteMoneda, Cotizacion, Importe, Observacion From VFormaPagoDocumento "

        Where = Condicion

        If Sucursal = True Then
            If Where = "" Then
                Where = " Where (IDSucursal=" & cbxSucursal.SelectedValue & ") "
            Else
                Where = Where & " And (IDSucursal=" & cbxSucursal.SelectedValue & ") "
            End If
        End If

        'Tipo
        If Sucursal = True Then
            If chkTipoSucursal.Checked = True Then
                If Where = "" Then
                    Where = " Where (IDTipoComprobante='" & cbxTipoComprobanteSucursal.SelectedValue & "') "
                Else
                    Where = Where & " And (IDTipoComprobante='" & cbxTipoComprobanteSucursal.SelectedValue & "')  "
                End If
            End If
        Else

            If chkTipo.Checked = True Then
                If Where = "" Then
                    Where = " Where (IDTipoComprobante='" & cbxTipoComprobante.SelectedValue & "') "
                Else
                    Where = Where & " And (IDTipoComprobante='" & cbxTipoComprobante.SelectedValue & "')  "
                End If
            End If

        End If

        'Fecha
        If Sucursal = True Then
            If chkFechaSucursal.Checked = True Then
                If Where = "" Then
                    Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesdeSucursal, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHastaSucursal, True, False) & "' ) "
                Else
                    Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesdeSucursal, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHastaSucursal, True, False) & "' )  "
                End If
            End If

        Else

            If chkFecha.Checked = True Then
                If Where = "" Then
                    Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' ) "
                Else
                    Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' )  "
                End If
            End If

        End If

        lvOperacion.Items.Clear()

        CSistema.SqlToLv(lvOperacion, Consulta & " " & Where & " Order By Fecha")

        'Formato

        'Totales
        CSistema.FormatoMoneda(lvOperacion, 4)
        CSistema.FormatoMoneda(lvOperacion, 5)
        CSistema.FormatoMoneda(lvOperacion, 6)

        CSistema.TotalesLv(lvOperacion, txtTotal.txt, 6)


    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    Private Sub frmConsultaChequeCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn2.Click
        ListarOperaciones(0, "", False)
    End Sub

    Private Sub chkTipo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipo.CheckedChanged
        HabilitarControles(chkTipo, cbxTipoComprobante)
    End Sub

    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged
        HabilitarControles(chkFecha, dtpDesde)
        HabilitarControles(chkFecha, dtpHasta)
    End Sub

    Private Sub chkTipoSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipoSucursal.CheckedChanged
        HabilitarControles(chkTipoSucursal, cbxTipoComprobanteSucursal)
    End Sub

    Private Sub chkFechaSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFechaSucursal.CheckedChanged
        HabilitarControles(chkFechaSucursal, dtpDesdeSucursal)
        HabilitarControles(chkFechaSucursal, dtpHastaSucursal)
    End Sub

    Private Sub btn11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn11.Click
        ListarOperaciones(0, "", True)
    End Sub

    Private Sub txtNroCheque_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroCheque.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarOperaciones(0, " Where Comprobante Like '%" & txtNroCheque.txt.Text.Trim & "%'", False)
        End If
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmConsultaFormaPagoDocumento_Activate()
        Me.Refresh()
    End Sub
End Class
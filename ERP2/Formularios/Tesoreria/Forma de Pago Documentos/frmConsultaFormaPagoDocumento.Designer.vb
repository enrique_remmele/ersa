﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaFormaPagoDocumento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtNroCheque = New ERP.ocxTXTString()
        Me.lblNroCheque = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.lvOperacion = New System.Windows.Forms.ListView()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotal = New ERP.ocxTXTNumeric()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btn2 = New System.Windows.Forms.Button()
        Me.cbxTipoComprobante = New System.Windows.Forms.ComboBox()
        Me.chkFecha = New System.Windows.Forms.CheckBox()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.chkTipo = New System.Windows.Forms.CheckBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btn11 = New System.Windows.Forms.Button()
        Me.chkFechaSucursal = New System.Windows.Forms.CheckBox()
        Me.chkTipoSucursal = New System.Windows.Forms.CheckBox()
        Me.cbxTipoComprobanteSucursal = New System.Windows.Forms.ComboBox()
        Me.dtpHastaSucursal = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesdeSucursal = New System.Windows.Forms.DateTimePicker()
        Me.cbxSucursal = New System.Windows.Forms.ComboBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.lvOperacion, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1, 0, 2)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(636, 451)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 4
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.01285!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.98715!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 59.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Panel2, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel3, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel4, 3, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(4, 4)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(628, 30)
        Me.TableLayoutPanel3.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblCantidad)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(505, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(60, 24)
        Me.Panel2.TabIndex = 2
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(3, 8)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidad.TabIndex = 0
        Me.lblCantidad.Text = "Cantidad:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.txtNroCheque)
        Me.Panel3.Controls.Add(Me.lblNroCheque)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(406, 24)
        Me.Panel3.TabIndex = 1
        '
        'txtNroCheque
        '
        Me.txtNroCheque.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroCheque.Color = System.Drawing.Color.Empty
        Me.txtNroCheque.Indicaciones = Nothing
        Me.txtNroCheque.Location = New System.Drawing.Point(113, 3)
        Me.txtNroCheque.Name = "txtNroCheque"
        Me.txtNroCheque.Size = New System.Drawing.Size(125, 21)
        Me.txtNroCheque.SoloLectura = False
        Me.txtNroCheque.TabIndex = 3
        Me.txtNroCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroCheque.Texto = ""
        '
        'lblNroCheque
        '
        Me.lblNroCheque.AutoSize = True
        Me.lblNroCheque.Location = New System.Drawing.Point(3, 7)
        Me.lblNroCheque.Name = "lblNroCheque"
        Me.lblNroCheque.Size = New System.Drawing.Size(111, 13)
        Me.lblNroCheque.TabIndex = 2
        Me.lblNroCheque.Text = "Nro. de Comprobante:"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.txtCantidad)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(571, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(54, 24)
        Me.Panel4.TabIndex = 0
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = Nothing
        Me.txtCantidad.Location = New System.Drawing.Point(3, 1)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidad.SoloLectura = False
        Me.txtCantidad.TabIndex = 0
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'lvOperacion
        '
        Me.lvOperacion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvOperacion.Location = New System.Drawing.Point(4, 41)
        Me.lvOperacion.Name = "lvOperacion"
        Me.lvOperacion.Size = New System.Drawing.Size(628, 373)
        Me.lvOperacion.TabIndex = 2
        Me.lvOperacion.UseCompatibleStateImageBehavior = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotal)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label2)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(4, 421)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(628, 26)
        Me.FlowLayoutPanel1.TabIndex = 3
        '
        'txtTotal
        '
        Me.txtTotal.Color = System.Drawing.Color.Empty
        Me.txtTotal.Decimales = True
        Me.txtTotal.Indicaciones = Nothing
        Me.txtTotal.Location = New System.Drawing.Point(522, 3)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(103, 22)
        Me.txtTotal.SoloLectura = False
        Me.txtTotal.TabIndex = 1
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Texto = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(482, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Total:"
        '
        'btn2
        '
        Me.btn2.Location = New System.Drawing.Point(6, 128)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(203, 24)
        Me.btn2.TabIndex = 10
        Me.btn2.Text = "Emitir Informe"
        Me.btn2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn2.UseVisualStyleBackColor = True
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormattingEnabled = True
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(6, 23)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(203, 21)
        Me.cbxTipoComprobante.TabIndex = 5
        '
        'chkFecha
        '
        Me.chkFecha.AutoSize = True
        Me.chkFecha.Location = New System.Drawing.Point(6, 50)
        Me.chkFecha.Name = "chkFecha"
        Me.chkFecha.Size = New System.Drawing.Size(59, 17)
        Me.chkFecha.TabIndex = 6
        Me.chkFecha.Text = "Fecha:"
        Me.chkFecha.UseVisualStyleBackColor = True
        '
        'dtpDesde
        '
        Me.dtpDesde.Enabled = False
        Me.dtpDesde.Location = New System.Drawing.Point(6, 67)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(203, 20)
        Me.dtpDesde.TabIndex = 7
        '
        'dtpHasta
        '
        Me.dtpHasta.Enabled = False
        Me.dtpHasta.Location = New System.Drawing.Point(6, 93)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(203, 20)
        Me.dtpHasta.TabIndex = 8
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btn2)
        Me.TabPage1.Controls.Add(Me.chkFecha)
        Me.TabPage1.Controls.Add(Me.chkTipo)
        Me.TabPage1.Controls.Add(Me.cbxTipoComprobante)
        Me.TabPage1.Controls.Add(Me.dtpHasta)
        Me.TabPage1.Controls.Add(Me.dtpDesde)
        Me.TabPage1.Location = New System.Drawing.Point(4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(217, 406)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'chkTipo
        '
        Me.chkTipo.AutoSize = True
        Me.chkTipo.Location = New System.Drawing.Point(6, 6)
        Me.chkTipo.Name = "chkTipo"
        Me.chkTipo.Size = New System.Drawing.Size(50, 17)
        Me.chkTipo.TabIndex = 4
        Me.chkTipo.Text = "Tipo:"
        Me.chkTipo.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(3, 16)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(225, 432)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btn11)
        Me.TabPage2.Controls.Add(Me.chkFechaSucursal)
        Me.TabPage2.Controls.Add(Me.chkTipoSucursal)
        Me.TabPage2.Controls.Add(Me.cbxTipoComprobanteSucursal)
        Me.TabPage2.Controls.Add(Me.dtpHastaSucursal)
        Me.TabPage2.Controls.Add(Me.dtpDesdeSucursal)
        Me.TabPage2.Controls.Add(Me.cbxSucursal)
        Me.TabPage2.Location = New System.Drawing.Point(4, 4)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(217, 406)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Sucursal"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btn11
        '
        Me.btn11.Location = New System.Drawing.Point(6, 146)
        Me.btn11.Name = "btn11"
        Me.btn11.Size = New System.Drawing.Size(203, 24)
        Me.btn11.TabIndex = 11
        Me.btn11.Text = "Emitir Informe"
        Me.btn11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn11.UseVisualStyleBackColor = True
        '
        'chkFechaSucursal
        '
        Me.chkFechaSucursal.AutoSize = True
        Me.chkFechaSucursal.Location = New System.Drawing.Point(6, 77)
        Me.chkFechaSucursal.Name = "chkFechaSucursal"
        Me.chkFechaSucursal.Size = New System.Drawing.Size(59, 17)
        Me.chkFechaSucursal.TabIndex = 7
        Me.chkFechaSucursal.Text = "Fecha:"
        Me.chkFechaSucursal.UseVisualStyleBackColor = True
        '
        'chkTipoSucursal
        '
        Me.chkTipoSucursal.AutoSize = True
        Me.chkTipoSucursal.Location = New System.Drawing.Point(6, 33)
        Me.chkTipoSucursal.Name = "chkTipoSucursal"
        Me.chkTipoSucursal.Size = New System.Drawing.Size(50, 17)
        Me.chkTipoSucursal.TabIndex = 5
        Me.chkTipoSucursal.Text = "Tipo:"
        Me.chkTipoSucursal.UseVisualStyleBackColor = True
        '
        'cbxTipoComprobanteSucursal
        '
        Me.cbxTipoComprobanteSucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxTipoComprobanteSucursal.Enabled = False
        Me.cbxTipoComprobanteSucursal.FormattingEnabled = True
        Me.cbxTipoComprobanteSucursal.Location = New System.Drawing.Point(6, 50)
        Me.cbxTipoComprobanteSucursal.Name = "cbxTipoComprobanteSucursal"
        Me.cbxTipoComprobanteSucursal.Size = New System.Drawing.Size(203, 21)
        Me.cbxTipoComprobanteSucursal.TabIndex = 6
        '
        'dtpHastaSucursal
        '
        Me.dtpHastaSucursal.Enabled = False
        Me.dtpHastaSucursal.Location = New System.Drawing.Point(6, 120)
        Me.dtpHastaSucursal.Name = "dtpHastaSucursal"
        Me.dtpHastaSucursal.Size = New System.Drawing.Size(203, 20)
        Me.dtpHastaSucursal.TabIndex = 9
        '
        'dtpDesdeSucursal
        '
        Me.dtpDesdeSucursal.Enabled = False
        Me.dtpDesdeSucursal.Location = New System.Drawing.Point(6, 94)
        Me.dtpDesdeSucursal.Name = "dtpDesdeSucursal"
        Me.dtpDesdeSucursal.Size = New System.Drawing.Size(203, 20)
        Me.dtpDesdeSucursal.TabIndex = 8
        '
        'cbxSucursal
        '
        Me.cbxSucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSucursal.FormattingEnabled = True
        Me.cbxSucursal.Location = New System.Drawing.Point(6, 6)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.Size = New System.Drawing.Size(203, 21)
        Me.cbxSucursal.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TabControl1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(645, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(231, 451)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 237.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(879, 457)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'frmConsultaFormaPagoDocumento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(879, 457)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmConsultaFormaPagoDocumento"
        Me.Text = "frmConusultaFormaPagoDocumento"
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtNroCheque As ERP.ocxTXTString
    Friend WithEvents lblNroCheque As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidad As ERP.ocxTXTNumeric
    Friend WithEvents lvOperacion As System.Windows.Forms.ListView
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotal As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btn2 As System.Windows.Forms.Button
    Friend WithEvents cbxTipoComprobante As System.Windows.Forms.ComboBox
    Friend WithEvents chkFecha As System.Windows.Forms.CheckBox
    Friend WithEvents dtpDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents chkTipo As System.Windows.Forms.CheckBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents btn11 As System.Windows.Forms.Button
    Friend WithEvents chkFechaSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents chkTipoSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents cbxTipoComprobanteSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents dtpHastaSucursal As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDesdeSucursal As System.Windows.Forms.DateTimePicker
    Friend WithEvents cbxSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
End Class

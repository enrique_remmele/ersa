﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrestamoBancario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.gbxDetalle = New System.Windows.Forms.GroupBox()
        Me.txtTotalInteres = New ERP.ocxTXTNumeric()
        Me.Interes = New System.Windows.Forms.Label()
        Me.txtTotalAmortizacionInteres = New ERP.ocxTXTNumeric()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtSaldoInteres = New ERP.ocxTXTNumeric()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lklGenerarCreditoBancario = New System.Windows.Forms.LinkLabel()
        Me.lklGenerarCreditoBancarioParcial = New System.Windows.Forms.LinkLabel()
        Me.lklEstablecerPago = New System.Windows.Forms.LinkLabel()
        Me.lklEliminarCreditosBancarios = New System.Windows.Forms.LinkLabel()
        Me.txtTotalCapital = New ERP.ocxTXTNumeric()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtTotalAmortizacion = New ERP.ocxTXTNumeric()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtSaldoCapital = New ERP.ocxTXTNumeric()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtCuota = New ERP.ocxTXTNumeric()
        Me.lblCuota = New System.Windows.Forms.Label()
        Me.txtImpuesto = New ERP.ocxTXTNumeric()
        Me.lblImpuesto = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtFechaVencimiento = New ERP.ocxTXTDate()
        Me.txtInteres = New ERP.ocxTXTNumeric()
        Me.lblCosto = New System.Windows.Forms.Label()
        Me.txtAmortizacion = New ERP.ocxTXTNumeric()
        Me.lblAmortizacion = New System.Windows.Forms.Label()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.GenerarCreditoBancarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PagoTotalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PagoParcialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarCreditoBancarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstablecerPagoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.cbxTipoGarantia = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnSeleccionarDebitoBancario = New System.Windows.Forms.Button()
        Me.btnGenerarDebitoBancario = New System.Windows.Forms.Button()
        Me.txtDebitoBancario = New ERP.ocxTXTString()
        Me.lblDebitoBancario = New System.Windows.Forms.Label()
        Me.txtCambio = New ERP.ocxTXTNumeric()
        Me.lblCambio = New System.Windows.Forms.Label()
        Me.txtPorcentajeInteres = New ERP.ocxTXTNumeric()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtNeto = New ERP.ocxTXTNumeric()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPlazoDias = New ERP.ocxTXTNumeric()
        Me.txtGastos = New ERP.ocxTXTNumeric()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtImporteTotalInteres = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCapital = New ERP.ocxTXTNumeric()
        Me.txtMoneda = New ERP.ocxTXTString()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.cbxCuenta = New ERP.ocxCBX()
        Me.txtBanco = New ERP.ocxTXTString()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.lblCapital = New System.Windows.Forms.Label()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.StatusStrip1.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDetalle.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.gbxCabecera.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 581)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(770, 22)
        Me.StatusStrip1.TabIndex = 10
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(14, 524)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(277, 20)
        Me.flpRegistradoPor.TabIndex = 18
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(171, 551)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(126, 23)
        Me.btnBusquedaAvanzada.TabIndex = 4
        Me.btnBusquedaAvanzada.Text = "&Busqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(17, 15)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(90, 551)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 3
        Me.btnImprimir.Text = "&Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        Me.lblAnulado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(687, 551)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 9
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(297, 524)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(310, 20)
        Me.flpAnuladoPor.TabIndex = 19
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(577, 551)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 8
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(9, 551)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 2
        Me.btnAnular.Text = "Anu&lar"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(496, 551)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 7
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(415, 551)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 6
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'gbxDetalle
        '
        Me.gbxDetalle.Controls.Add(Me.txtTotalInteres)
        Me.gbxDetalle.Controls.Add(Me.Interes)
        Me.gbxDetalle.Controls.Add(Me.txtTotalAmortizacionInteres)
        Me.gbxDetalle.Controls.Add(Me.Label11)
        Me.gbxDetalle.Controls.Add(Me.txtSaldoInteres)
        Me.gbxDetalle.Controls.Add(Me.Label12)
        Me.gbxDetalle.Controls.Add(Me.lklGenerarCreditoBancario)
        Me.gbxDetalle.Controls.Add(Me.lklGenerarCreditoBancarioParcial)
        Me.gbxDetalle.Controls.Add(Me.lklEstablecerPago)
        Me.gbxDetalle.Controls.Add(Me.lklEliminarCreditosBancarios)
        Me.gbxDetalle.Controls.Add(Me.txtTotalCapital)
        Me.gbxDetalle.Controls.Add(Me.Label10)
        Me.gbxDetalle.Controls.Add(Me.txtTotalAmortizacion)
        Me.gbxDetalle.Controls.Add(Me.Label9)
        Me.gbxDetalle.Controls.Add(Me.txtSaldoCapital)
        Me.gbxDetalle.Controls.Add(Me.Label8)
        Me.gbxDetalle.Controls.Add(Me.txtCuota)
        Me.gbxDetalle.Controls.Add(Me.lblCuota)
        Me.gbxDetalle.Controls.Add(Me.txtImpuesto)
        Me.gbxDetalle.Controls.Add(Me.lblImpuesto)
        Me.gbxDetalle.Controls.Add(Me.Label6)
        Me.gbxDetalle.Controls.Add(Me.txtFechaVencimiento)
        Me.gbxDetalle.Controls.Add(Me.txtInteres)
        Me.gbxDetalle.Controls.Add(Me.lblCosto)
        Me.gbxDetalle.Controls.Add(Me.txtAmortizacion)
        Me.gbxDetalle.Controls.Add(Me.lblAmortizacion)
        Me.gbxDetalle.Controls.Add(Me.dgw)
        Me.gbxDetalle.Location = New System.Drawing.Point(9, 148)
        Me.gbxDetalle.Name = "gbxDetalle"
        Me.gbxDetalle.Size = New System.Drawing.Size(753, 370)
        Me.gbxDetalle.TabIndex = 1
        Me.gbxDetalle.TabStop = False
        '
        'txtTotalInteres
        '
        Me.txtTotalInteres.Color = System.Drawing.Color.Empty
        Me.txtTotalInteres.Decimales = False
        Me.txtTotalInteres.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalInteres.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotalInteres.Location = New System.Drawing.Point(53, 337)
        Me.txtTotalInteres.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalInteres.Name = "txtTotalInteres"
        Me.txtTotalInteres.Size = New System.Drawing.Size(150, 21)
        Me.txtTotalInteres.SoloLectura = True
        Me.txtTotalInteres.TabIndex = 21
        Me.txtTotalInteres.TabStop = False
        Me.txtTotalInteres.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalInteres.Texto = "0"
        '
        'Interes
        '
        Me.Interes.AutoSize = True
        Me.Interes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Interes.Location = New System.Drawing.Point(4, 340)
        Me.Interes.Name = "Interes"
        Me.Interes.Size = New System.Drawing.Size(50, 13)
        Me.Interes.TabIndex = 20
        Me.Interes.Text = "Interes:"
        '
        'txtTotalAmortizacionInteres
        '
        Me.txtTotalAmortizacionInteres.Color = System.Drawing.Color.Empty
        Me.txtTotalAmortizacionInteres.Decimales = False
        Me.txtTotalAmortizacionInteres.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalAmortizacionInteres.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotalAmortizacionInteres.Location = New System.Drawing.Point(349, 337)
        Me.txtTotalAmortizacionInteres.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalAmortizacionInteres.Name = "txtTotalAmortizacionInteres"
        Me.txtTotalAmortizacionInteres.Size = New System.Drawing.Size(150, 21)
        Me.txtTotalAmortizacionInteres.SoloLectura = True
        Me.txtTotalAmortizacionInteres.TabIndex = 23
        Me.txtTotalAmortizacionInteres.TabStop = False
        Me.txtTotalAmortizacionInteres.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalAmortizacionInteres.Texto = "0"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(208, 340)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(144, 13)
        Me.Label11.TabIndex = 22
        Me.Label11.Text = "Amortización de Interes:"
        '
        'txtSaldoInteres
        '
        Me.txtSaldoInteres.Color = System.Drawing.Color.Empty
        Me.txtSaldoInteres.Decimales = False
        Me.txtSaldoInteres.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldoInteres.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtSaldoInteres.Location = New System.Drawing.Point(597, 337)
        Me.txtSaldoInteres.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSaldoInteres.Name = "txtSaldoInteres"
        Me.txtSaldoInteres.Size = New System.Drawing.Size(150, 21)
        Me.txtSaldoInteres.SoloLectura = True
        Me.txtSaldoInteres.TabIndex = 25
        Me.txtSaldoInteres.TabStop = False
        Me.txtSaldoInteres.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoInteres.Texto = "0"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(512, 340)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(86, 13)
        Me.Label12.TabIndex = 24
        Me.Label12.Text = "Saldo Interes:"
        '
        'lklGenerarCreditoBancario
        '
        Me.lklGenerarCreditoBancario.AutoSize = True
        Me.lklGenerarCreditoBancario.Location = New System.Drawing.Point(108, 287)
        Me.lklGenerarCreditoBancario.Name = "lklGenerarCreditoBancario"
        Me.lklGenerarCreditoBancario.Size = New System.Drawing.Size(153, 13)
        Me.lklGenerarCreditoBancario.TabIndex = 19
        Me.lklGenerarCreditoBancario.TabStop = True
        Me.lklGenerarCreditoBancario.Text = "- Generar Credito Bancario (F2)"
        '
        'lklGenerarCreditoBancarioParcial
        '
        Me.lklGenerarCreditoBancarioParcial.AutoSize = True
        Me.lklGenerarCreditoBancarioParcial.Location = New System.Drawing.Point(269, 287)
        Me.lklGenerarCreditoBancarioParcial.Name = "lklGenerarCreditoBancarioParcial"
        Me.lklGenerarCreditoBancarioParcial.Size = New System.Drawing.Size(188, 13)
        Me.lklGenerarCreditoBancarioParcial.TabIndex = 11
        Me.lklGenerarCreditoBancarioParcial.TabStop = True
        Me.lklGenerarCreditoBancarioParcial.Text = "- Generar Credito Bancario Parcial (F3)"
        '
        'lklEstablecerPago
        '
        Me.lklEstablecerPago.AutoSize = True
        Me.lklEstablecerPago.Location = New System.Drawing.Point(634, 287)
        Me.lklEstablecerPago.Name = "lklEstablecerPago"
        Me.lklEstablecerPago.Size = New System.Drawing.Size(112, 13)
        Me.lklEstablecerPago.TabIndex = 13
        Me.lklEstablecerPago.TabStop = True
        Me.lklEstablecerPago.Text = "- Establecer Pago (F5)"
        '
        'lklEliminarCreditosBancarios
        '
        Me.lklEliminarCreditosBancarios.AutoSize = True
        Me.lklEliminarCreditosBancarios.Location = New System.Drawing.Point(466, 287)
        Me.lklEliminarCreditosBancarios.Name = "lklEliminarCreditosBancarios"
        Me.lklEliminarCreditosBancarios.Size = New System.Drawing.Size(161, 13)
        Me.lklEliminarCreditosBancarios.TabIndex = 12
        Me.lklEliminarCreditosBancarios.TabStop = True
        Me.lklEliminarCreditosBancarios.Text = "- Eliminar Creditos Bancarios (F4)"
        '
        'txtTotalCapital
        '
        Me.txtTotalCapital.Color = System.Drawing.Color.Empty
        Me.txtTotalCapital.Decimales = False
        Me.txtTotalCapital.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalCapital.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotalCapital.Location = New System.Drawing.Point(53, 308)
        Me.txtTotalCapital.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalCapital.Name = "txtTotalCapital"
        Me.txtTotalCapital.Size = New System.Drawing.Size(150, 21)
        Me.txtTotalCapital.SoloLectura = True
        Me.txtTotalCapital.TabIndex = 14
        Me.txtTotalCapital.TabStop = False
        Me.txtTotalCapital.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCapital.Texto = "0"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(4, 311)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(50, 13)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "Capital:"
        '
        'txtTotalAmortizacion
        '
        Me.txtTotalAmortizacion.Color = System.Drawing.Color.Empty
        Me.txtTotalAmortizacion.Decimales = False
        Me.txtTotalAmortizacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalAmortizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotalAmortizacion.Location = New System.Drawing.Point(349, 308)
        Me.txtTotalAmortizacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalAmortizacion.Name = "txtTotalAmortizacion"
        Me.txtTotalAmortizacion.Size = New System.Drawing.Size(150, 21)
        Me.txtTotalAmortizacion.SoloLectura = True
        Me.txtTotalAmortizacion.TabIndex = 16
        Me.txtTotalAmortizacion.TabStop = False
        Me.txtTotalAmortizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalAmortizacion.Texto = "0"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(208, 311)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(144, 13)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "Amortización de Capital:"
        '
        'txtSaldoCapital
        '
        Me.txtSaldoCapital.Color = System.Drawing.Color.Empty
        Me.txtSaldoCapital.Decimales = False
        Me.txtSaldoCapital.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldoCapital.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtSaldoCapital.Location = New System.Drawing.Point(597, 308)
        Me.txtSaldoCapital.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSaldoCapital.Name = "txtSaldoCapital"
        Me.txtSaldoCapital.Size = New System.Drawing.Size(150, 21)
        Me.txtSaldoCapital.SoloLectura = True
        Me.txtSaldoCapital.TabIndex = 18
        Me.txtSaldoCapital.TabStop = False
        Me.txtSaldoCapital.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoCapital.Texto = "0"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(512, 311)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(86, 13)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Saldo Capital:"
        '
        'txtCuota
        '
        Me.txtCuota.Color = System.Drawing.Color.Empty
        Me.txtCuota.Decimales = False
        Me.txtCuota.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCuota.Location = New System.Drawing.Point(593, 29)
        Me.txtCuota.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCuota.Name = "txtCuota"
        Me.txtCuota.Size = New System.Drawing.Size(153, 21)
        Me.txtCuota.SoloLectura = False
        Me.txtCuota.TabIndex = 9
        Me.txtCuota.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCuota.Texto = "0"
        '
        'lblCuota
        '
        Me.lblCuota.AutoSize = True
        Me.lblCuota.Location = New System.Drawing.Point(595, 13)
        Me.lblCuota.Name = "lblCuota"
        Me.lblCuota.Size = New System.Drawing.Size(38, 13)
        Me.lblCuota.TabIndex = 8
        Me.lblCuota.Text = "Cuota:"
        '
        'txtImpuesto
        '
        Me.txtImpuesto.Color = System.Drawing.Color.Empty
        Me.txtImpuesto.Decimales = False
        Me.txtImpuesto.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImpuesto.Location = New System.Drawing.Point(442, 29)
        Me.txtImpuesto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtImpuesto.Name = "txtImpuesto"
        Me.txtImpuesto.Size = New System.Drawing.Size(145, 21)
        Me.txtImpuesto.SoloLectura = False
        Me.txtImpuesto.TabIndex = 7
        Me.txtImpuesto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImpuesto.Texto = "0"
        '
        'lblImpuesto
        '
        Me.lblImpuesto.AutoSize = True
        Me.lblImpuesto.Location = New System.Drawing.Point(444, 13)
        Me.lblImpuesto.Name = "lblImpuesto"
        Me.lblImpuesto.Size = New System.Drawing.Size(53, 13)
        Me.lblImpuesto.TabIndex = 6
        Me.lblImpuesto.Text = "Impuesto:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 13)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Fecha Vencimiento:"
        '
        'txtFechaVencimiento
        '
        Me.txtFechaVencimiento.AñoFecha = 0
        Me.txtFechaVencimiento.Color = System.Drawing.Color.Empty
        Me.txtFechaVencimiento.Fecha = New Date(CType(0, Long))
        Me.txtFechaVencimiento.Location = New System.Drawing.Point(5, 29)
        Me.txtFechaVencimiento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFechaVencimiento.MesFecha = 0
        Me.txtFechaVencimiento.Name = "txtFechaVencimiento"
        Me.txtFechaVencimiento.PermitirNulo = False
        Me.txtFechaVencimiento.Size = New System.Drawing.Size(102, 21)
        Me.txtFechaVencimiento.SoloLectura = False
        Me.txtFechaVencimiento.TabIndex = 1
        '
        'txtInteres
        '
        Me.txtInteres.Color = System.Drawing.Color.Empty
        Me.txtInteres.Decimales = False
        Me.txtInteres.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtInteres.Location = New System.Drawing.Point(283, 29)
        Me.txtInteres.Margin = New System.Windows.Forms.Padding(4)
        Me.txtInteres.Name = "txtInteres"
        Me.txtInteres.Size = New System.Drawing.Size(153, 21)
        Me.txtInteres.SoloLectura = False
        Me.txtInteres.TabIndex = 5
        Me.txtInteres.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtInteres.Texto = "0"
        '
        'lblCosto
        '
        Me.lblCosto.AutoSize = True
        Me.lblCosto.Location = New System.Drawing.Point(285, 13)
        Me.lblCosto.Name = "lblCosto"
        Me.lblCosto.Size = New System.Drawing.Size(42, 13)
        Me.lblCosto.TabIndex = 4
        Me.lblCosto.Text = "Interés:"
        '
        'txtAmortizacion
        '
        Me.txtAmortizacion.Color = System.Drawing.Color.Empty
        Me.txtAmortizacion.Decimales = False
        Me.txtAmortizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtAmortizacion.Location = New System.Drawing.Point(113, 29)
        Me.txtAmortizacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAmortizacion.Name = "txtAmortizacion"
        Me.txtAmortizacion.Size = New System.Drawing.Size(163, 21)
        Me.txtAmortizacion.SoloLectura = False
        Me.txtAmortizacion.TabIndex = 3
        Me.txtAmortizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAmortizacion.Texto = "0"
        '
        'lblAmortizacion
        '
        Me.lblAmortizacion.AutoSize = True
        Me.lblAmortizacion.Location = New System.Drawing.Point(115, 13)
        Me.lblAmortizacion.Name = "lblAmortizacion"
        Me.lblAmortizacion.Size = New System.Drawing.Size(70, 13)
        Me.lblAmortizacion.TabIndex = 2
        Me.lblAmortizacion.Text = "Amortización:"
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.ContextMenuStrip = Me.ContextMenuStrip1
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgw.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgw.Location = New System.Drawing.Point(5, 56)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgw.Size = New System.Drawing.Size(742, 228)
        Me.dgw.TabIndex = 10
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GenerarCreditoBancarioToolStripMenuItem, Me.EliminarCreditoBancarioToolStripMenuItem, Me.EstablecerPagoToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(219, 70)
        '
        'GenerarCreditoBancarioToolStripMenuItem
        '
        Me.GenerarCreditoBancarioToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PagoTotalToolStripMenuItem, Me.PagoParcialToolStripMenuItem})
        Me.GenerarCreditoBancarioToolStripMenuItem.Name = "GenerarCreditoBancarioToolStripMenuItem"
        Me.GenerarCreditoBancarioToolStripMenuItem.Size = New System.Drawing.Size(218, 22)
        Me.GenerarCreditoBancarioToolStripMenuItem.Text = "Generar Credito Bancario"
        '
        'PagoTotalToolStripMenuItem
        '
        Me.PagoTotalToolStripMenuItem.Name = "PagoTotalToolStripMenuItem"
        Me.PagoTotalToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.PagoTotalToolStripMenuItem.Text = "Pago Total"
        '
        'PagoParcialToolStripMenuItem
        '
        Me.PagoParcialToolStripMenuItem.Name = "PagoParcialToolStripMenuItem"
        Me.PagoParcialToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.PagoParcialToolStripMenuItem.Text = "Pago Parcial"
        '
        'EliminarCreditoBancarioToolStripMenuItem
        '
        Me.EliminarCreditoBancarioToolStripMenuItem.Name = "EliminarCreditoBancarioToolStripMenuItem"
        Me.EliminarCreditoBancarioToolStripMenuItem.Size = New System.Drawing.Size(218, 22)
        Me.EliminarCreditoBancarioToolStripMenuItem.Text = "Eliminar Creditos Bancarios"
        '
        'EstablecerPagoToolStripMenuItem
        '
        Me.EstablecerPagoToolStripMenuItem.Name = "EstablecerPagoToolStripMenuItem"
        Me.EstablecerPagoToolStripMenuItem.Size = New System.Drawing.Size(218, 22)
        Me.EstablecerPagoToolStripMenuItem.Text = "Establecer Pago"
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.cbxTipoGarantia)
        Me.gbxCabecera.Controls.Add(Me.Label7)
        Me.gbxCabecera.Controls.Add(Me.btnSeleccionarDebitoBancario)
        Me.gbxCabecera.Controls.Add(Me.btnGenerarDebitoBancario)
        Me.gbxCabecera.Controls.Add(Me.txtDebitoBancario)
        Me.gbxCabecera.Controls.Add(Me.lblDebitoBancario)
        Me.gbxCabecera.Controls.Add(Me.txtCambio)
        Me.gbxCabecera.Controls.Add(Me.lblCambio)
        Me.gbxCabecera.Controls.Add(Me.txtPorcentajeInteres)
        Me.gbxCabecera.Controls.Add(Me.Label5)
        Me.gbxCabecera.Controls.Add(Me.txtNeto)
        Me.gbxCabecera.Controls.Add(Me.Label3)
        Me.gbxCabecera.Controls.Add(Me.txtPlazoDias)
        Me.gbxCabecera.Controls.Add(Me.txtGastos)
        Me.gbxCabecera.Controls.Add(Me.Label2)
        Me.gbxCabecera.Controls.Add(Me.Label4)
        Me.gbxCabecera.Controls.Add(Me.txtImporteTotalInteres)
        Me.gbxCabecera.Controls.Add(Me.Label1)
        Me.gbxCabecera.Controls.Add(Me.txtCapital)
        Me.gbxCabecera.Controls.Add(Me.txtMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblCuenta)
        Me.gbxCabecera.Controls.Add(Me.cbxCuenta)
        Me.gbxCabecera.Controls.Add(Me.txtBanco)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Controls.Add(Me.lblCapital)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Location = New System.Drawing.Point(9, 0)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(753, 146)
        Me.gbxCabecera.TabIndex = 0
        Me.gbxCabecera.TabStop = False
        '
        'cbxTipoGarantia
        '
        Me.cbxTipoGarantia.FormattingEnabled = True
        Me.cbxTipoGarantia.Items.AddRange(New Object() {"A SOLA FIRMA", "HIPOTECARIA", "PRENDARIA", "GARANTIA DE TERCEROS", "WARRANTS", "REVOLVING", "CASH COLLATERAL", "LEASING"})
        Me.cbxTipoGarantia.Location = New System.Drawing.Point(83, 118)
        Me.cbxTipoGarantia.Margin = New System.Windows.Forms.Padding(2)
        Me.cbxTipoGarantia.Name = "cbxTipoGarantia"
        Me.cbxTipoGarantia.Size = New System.Drawing.Size(193, 21)
        Me.cbxTipoGarantia.TabIndex = 33
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(26, 122)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 13)
        Me.Label7.TabIndex = 32
        Me.Label7.Text = "Garantia:"
        '
        'btnSeleccionarDebitoBancario
        '
        Me.btnSeleccionarDebitoBancario.Location = New System.Drawing.Point(671, 117)
        Me.btnSeleccionarDebitoBancario.Name = "btnSeleccionarDebitoBancario"
        Me.btnSeleccionarDebitoBancario.Size = New System.Drawing.Size(75, 23)
        Me.btnSeleccionarDebitoBancario.TabIndex = 31
        Me.btnSeleccionarDebitoBancario.Text = "Seleccionar"
        Me.btnSeleccionarDebitoBancario.UseVisualStyleBackColor = True
        '
        'btnGenerarDebitoBancario
        '
        Me.btnGenerarDebitoBancario.Location = New System.Drawing.Point(590, 117)
        Me.btnGenerarDebitoBancario.Name = "btnGenerarDebitoBancario"
        Me.btnGenerarDebitoBancario.Size = New System.Drawing.Size(75, 23)
        Me.btnGenerarDebitoBancario.TabIndex = 30
        Me.btnGenerarDebitoBancario.Text = "Generar"
        Me.btnGenerarDebitoBancario.UseVisualStyleBackColor = True
        '
        'txtDebitoBancario
        '
        Me.txtDebitoBancario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDebitoBancario.Color = System.Drawing.Color.Empty
        Me.txtDebitoBancario.Indicaciones = Nothing
        Me.txtDebitoBancario.Location = New System.Drawing.Point(356, 118)
        Me.txtDebitoBancario.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDebitoBancario.Multilinea = False
        Me.txtDebitoBancario.Name = "txtDebitoBancario"
        Me.txtDebitoBancario.Size = New System.Drawing.Size(229, 21)
        Me.txtDebitoBancario.SoloLectura = True
        Me.txtDebitoBancario.TabIndex = 29
        Me.txtDebitoBancario.TabStop = False
        Me.txtDebitoBancario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDebitoBancario.Texto = ""
        '
        'lblDebitoBancario
        '
        Me.lblDebitoBancario.AutoSize = True
        Me.lblDebitoBancario.Location = New System.Drawing.Point(284, 122)
        Me.lblDebitoBancario.Name = "lblDebitoBancario"
        Me.lblDebitoBancario.Size = New System.Drawing.Size(64, 13)
        Me.lblDebitoBancario.TabIndex = 28
        Me.lblDebitoBancario.Text = "Deb. Banc.:"
        '
        'txtCambio
        '
        Me.txtCambio.Color = System.Drawing.Color.Empty
        Me.txtCambio.Decimales = False
        Me.txtCambio.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCambio.Location = New System.Drawing.Point(629, 38)
        Me.txtCambio.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCambio.Name = "txtCambio"
        Me.txtCambio.Size = New System.Drawing.Size(117, 21)
        Me.txtCambio.SoloLectura = False
        Me.txtCambio.TabIndex = 13
        Me.txtCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCambio.Texto = ""
        '
        'lblCambio
        '
        Me.lblCambio.AutoSize = True
        Me.lblCambio.Location = New System.Drawing.Point(580, 42)
        Me.lblCambio.Name = "lblCambio"
        Me.lblCambio.Size = New System.Drawing.Size(45, 13)
        Me.lblCambio.TabIndex = 12
        Me.lblCambio.Text = "Cambio:"
        '
        'txtPorcentajeInteres
        '
        Me.txtPorcentajeInteres.Color = System.Drawing.Color.Empty
        Me.txtPorcentajeInteres.Decimales = True
        Me.txtPorcentajeInteres.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtPorcentajeInteres.Location = New System.Drawing.Point(629, 65)
        Me.txtPorcentajeInteres.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPorcentajeInteres.Name = "txtPorcentajeInteres"
        Me.txtPorcentajeInteres.Size = New System.Drawing.Size(117, 21)
        Me.txtPorcentajeInteres.SoloLectura = False
        Me.txtPorcentajeInteres.TabIndex = 19
        Me.txtPorcentajeInteres.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajeInteres.Texto = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(566, 69)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 13)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "Interés (%):"
        '
        'txtNeto
        '
        Me.txtNeto.Color = System.Drawing.Color.Empty
        Me.txtNeto.Decimales = False
        Me.txtNeto.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtNeto.Location = New System.Drawing.Point(629, 92)
        Me.txtNeto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNeto.Name = "txtNeto"
        Me.txtNeto.Size = New System.Drawing.Size(117, 21)
        Me.txtNeto.SoloLectura = False
        Me.txtNeto.TabIndex = 27
        Me.txtNeto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNeto.Texto = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(565, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 13)
        Me.Label3.TabIndex = 26
        Me.Label3.Text = "Total Neto:"
        '
        'txtPlazoDias
        '
        Me.txtPlazoDias.Color = System.Drawing.Color.Empty
        Me.txtPlazoDias.Decimales = False
        Me.txtPlazoDias.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtPlazoDias.Location = New System.Drawing.Point(484, 65)
        Me.txtPlazoDias.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPlazoDias.Name = "txtPlazoDias"
        Me.txtPlazoDias.Size = New System.Drawing.Size(78, 21)
        Me.txtPlazoDias.SoloLectura = False
        Me.txtPlazoDias.TabIndex = 17
        Me.txtPlazoDias.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPlazoDias.Texto = "0"
        '
        'txtGastos
        '
        Me.txtGastos.Color = System.Drawing.Color.Empty
        Me.txtGastos.Decimales = False
        Me.txtGastos.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtGastos.Location = New System.Drawing.Point(461, 92)
        Me.txtGastos.Margin = New System.Windows.Forms.Padding(4)
        Me.txtGastos.Name = "txtGastos"
        Me.txtGastos.Size = New System.Drawing.Size(101, 21)
        Me.txtGastos.SoloLectura = False
        Me.txtGastos.TabIndex = 25
        Me.txtGastos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtGastos.Texto = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(416, 96)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Gastos:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(425, 69)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 13)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Plazo días:"
        '
        'txtImporteTotalInteres
        '
        Me.txtImporteTotalInteres.Color = System.Drawing.Color.Empty
        Me.txtImporteTotalInteres.Decimales = False
        Me.txtImporteTotalInteres.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporteTotalInteres.Location = New System.Drawing.Point(276, 92)
        Me.txtImporteTotalInteres.Margin = New System.Windows.Forms.Padding(4)
        Me.txtImporteTotalInteres.Name = "txtImporteTotalInteres"
        Me.txtImporteTotalInteres.Size = New System.Drawing.Size(129, 21)
        Me.txtImporteTotalInteres.SoloLectura = False
        Me.txtImporteTotalInteres.TabIndex = 23
        Me.txtImporteTotalInteres.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteTotalInteres.Texto = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(223, 96)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Intereses:"
        '
        'txtCapital
        '
        Me.txtCapital.Color = System.Drawing.Color.Empty
        Me.txtCapital.Decimales = False
        Me.txtCapital.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCapital.Location = New System.Drawing.Point(83, 92)
        Me.txtCapital.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCapital.Name = "txtCapital"
        Me.txtCapital.Size = New System.Drawing.Size(134, 21)
        Me.txtCapital.SoloLectura = False
        Me.txtCapital.TabIndex = 21
        Me.txtCapital.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCapital.Texto = "0"
        '
        'txtMoneda
        '
        Me.txtMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMoneda.Color = System.Drawing.Color.Empty
        Me.txtMoneda.Indicaciones = Nothing
        Me.txtMoneda.Location = New System.Drawing.Point(506, 38)
        Me.txtMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.txtMoneda.Multilinea = False
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.Size = New System.Drawing.Size(56, 21)
        Me.txtMoneda.SoloLectura = True
        Me.txtMoneda.TabIndex = 11
        Me.txtMoneda.TabStop = False
        Me.txtMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMoneda.Texto = ""
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(32, 42)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 8
        Me.lblCuenta.Text = "Cuenta:"
        '
        'cbxCuenta
        '
        Me.cbxCuenta.CampoWhere = Nothing
        Me.cbxCuenta.CargarUnaSolaVez = False
        Me.cbxCuenta.DataDisplayMember = Nothing
        Me.cbxCuenta.DataFilter = Nothing
        Me.cbxCuenta.DataOrderBy = Nothing
        Me.cbxCuenta.DataSource = Nothing
        Me.cbxCuenta.DataValueMember = Nothing
        Me.cbxCuenta.dtSeleccionado = Nothing
        Me.cbxCuenta.FormABM = Nothing
        Me.cbxCuenta.Indicaciones = Nothing
        Me.cbxCuenta.Location = New System.Drawing.Point(83, 38)
        Me.cbxCuenta.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCuenta.Name = "cbxCuenta"
        Me.cbxCuenta.SeleccionMultiple = False
        Me.cbxCuenta.SeleccionObligatoria = False
        Me.cbxCuenta.Size = New System.Drawing.Size(194, 21)
        Me.cbxCuenta.SoloLectura = False
        Me.cbxCuenta.TabIndex = 9
        Me.cbxCuenta.Texto = ""
        '
        'txtBanco
        '
        Me.txtBanco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBanco.Color = System.Drawing.Color.Empty
        Me.txtBanco.Indicaciones = Nothing
        Me.txtBanco.Location = New System.Drawing.Point(278, 38)
        Me.txtBanco.Margin = New System.Windows.Forms.Padding(4)
        Me.txtBanco.Multilinea = False
        Me.txtBanco.Name = "txtBanco"
        Me.txtBanco.Size = New System.Drawing.Size(229, 21)
        Me.txtBanco.SoloLectura = True
        Me.txtBanco.TabIndex = 10
        Me.txtBanco.TabStop = False
        Me.txtBanco.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBanco.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(83, 11)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(76, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(83, 65)
        Me.txtObservacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(322, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 15
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(405, 10)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(127, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 6
        Me.cbxTipoComprobante.Texto = ""
        '
        'lblCapital
        '
        Me.lblCapital.AutoSize = True
        Me.lblCapital.Location = New System.Drawing.Point(34, 96)
        Me.lblCapital.Name = "lblCapital"
        Me.lblCapital.Size = New System.Drawing.Size(42, 13)
        Me.lblCapital.TabIndex = 20
        Me.lblCapital.Text = "Capital:"
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 4, 8, 9, 22, 55, 62)
        Me.txtFecha.Location = New System.Drawing.Point(254, 11)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 4
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(160, 11)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(52, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(6, 69)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 14
        Me.lblObservacion.Text = "Observacion:"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(215, 15)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 3
        Me.lblFecha.Text = "Fecha:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(330, 14)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 5
        Me.lblComprobante.Text = "Comprobante:"
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(533, 10)
        Me.txtComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(213, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 7
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(334, 551)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 5
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        Me.btnAsiento.Visible = False
        '
        'frmPrestamoBancario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(770, 603)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.gbxDetalle)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.flpAnuladoPor)
        Me.Name = "frmPrestamoBancario"
        Me.Tag = "frmPrestamoBancario"
        Me.Text = "PRESTAMO BANCARIO"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDetalle.ResumeLayout(False)
        Me.gbxDetalle.PerformLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents gbxDetalle As System.Windows.Forms.GroupBox
    Friend WithEvents txtInteres As ERP.ocxTXTNumeric
    Friend WithEvents lblCosto As System.Windows.Forms.Label
    Friend WithEvents txtAmortizacion As ERP.ocxTXTNumeric
    Friend WithEvents lblAmortizacion As System.Windows.Forms.Label
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblCapital As System.Windows.Forms.Label
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents txtCapital As ERP.ocxTXTNumeric
    Friend WithEvents txtMoneda As ERP.ocxTXTString
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents cbxCuenta As ERP.ocxCBX
    Friend WithEvents txtBanco As ERP.ocxTXTString
    Friend WithEvents txtPorcentajeInteres As ERP.ocxTXTNumeric
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtPlazoDias As ERP.ocxTXTNumeric
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNeto As ERP.ocxTXTNumeric
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtGastos As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtImporteTotalInteres As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblCambio As System.Windows.Forms.Label
    Friend WithEvents txtCambio As ERP.ocxTXTNumeric
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtFechaVencimiento As ERP.ocxTXTDate
    Friend WithEvents txtCuota As ERP.ocxTXTNumeric
    Friend WithEvents lblCuota As System.Windows.Forms.Label
    Friend WithEvents txtImpuesto As ERP.ocxTXTNumeric
    Friend WithEvents lblImpuesto As System.Windows.Forms.Label
    Friend WithEvents txtSaldoCapital As ERP.ocxTXTNumeric
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtTotalAmortizacion As ERP.ocxTXTNumeric
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtTotalCapital As ERP.ocxTXTNumeric
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnSeleccionarDebitoBancario As System.Windows.Forms.Button
    Friend WithEvents btnGenerarDebitoBancario As System.Windows.Forms.Button
    Friend WithEvents txtDebitoBancario As ERP.ocxTXTString
    Friend WithEvents lblDebitoBancario As System.Windows.Forms.Label
    Friend WithEvents lklEstablecerPago As System.Windows.Forms.LinkLabel
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents EliminarCreditoBancarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstablecerPagoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lklGenerarCreditoBancarioParcial As System.Windows.Forms.LinkLabel
    Friend WithEvents GenerarCreditoBancarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lklGenerarCreditoBancario As System.Windows.Forms.LinkLabel
    Friend WithEvents PagoTotalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PagoParcialToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtTotalInteres As ERP.ocxTXTNumeric
    Friend WithEvents Interes As System.Windows.Forms.Label
    Friend WithEvents txtTotalAmortizacionInteres As ERP.ocxTXTNumeric
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtSaldoInteres As ERP.ocxTXTNumeric
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lklEliminarCreditosBancarios As System.Windows.Forms.LinkLabel
    Friend WithEvents cbxTipoGarantia As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class

﻿Imports ERP.CSistema

Public Class frmSeleccionEliminacionCreditoBancario

    'Propiedades
    Public Property IDTransaccionPrestamoBancario As Integer
    Public Property NroCuota As Integer
    Public Property Moneda As String

    'Clases
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim dtDetalle As New DataTable

    Sub Inicializar()

        dtDetalle.Clear()
        dtDetalle = CSistema.ExecuteToDataTable("Select * From VDetalleCuotaPrestamoBancario Where IDTransaccionPrestamoBancario=" & IDTransaccionPrestamoBancario & " And NroCuota=" & NroCuota & " Order By Fecha").Copy

        ListarDetalle()

    End Sub

    Sub ListarDetalle()

        'Ordenamos el datatable
        CData.OrderDataTable(dtDetalle, "Fecha", True)

        'Cargamos registro por registro
        CSistema.dtToGrid(dgw, dtDetalle)

        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).Visible = False
        Next

        dgw.Columns("Fecha").Visible = True
        dgw.Columns("NroPrestamo").Visible = True
        dgw.Columns("NroCreditoBancario").Visible = True
        dgw.Columns("NroCuota").Visible = True
        dgw.Columns("AmortizacionCapital").Visible = True
        dgw.Columns("Interes").Visible = True
        dgw.Columns("Impuesto").Visible = True
        dgw.Columns("PagoTotal").Visible = True
        
        dgw.Columns("NroCuota").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
        dgw.Columns("NroPrestamo").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        Dim Decimales As Boolean = CSistema.RetornarValorBoolean(CData.GetRow("Referencia='" & Moneda & "'", "VMoneda")("Decimales").ToString)
        CSistema.DataGridColumnasNumericas(dgw, {"AmortizacionCapital", "Interes", "Impuesto", "PagoTotal"}, Decimales)

    End Sub

    Sub Eliminar()

        'Actualizamos el registro
        Dim SQL As String = "Delete DetalleCuotaPrestamoBancario Where IDTransaccionPrestamoBancario=" & dgw.SelectedRows(0).Cells("IDTransaccionPrestamoBancario").Value & " And IDTransaccionDebitoCreditoBancario=" & dgw.SelectedRows(0).Cells("IDTransaccionDebitoCreditoBancario").Value & " And NroCuota=" & dgw.SelectedRows(0).Cells("NroCuota").Value & ""
        Dim Procesado As Integer = CSistema.ExecuteNonQuery(SQL)

    End Sub

    Private Sub frmSeleccionEliminacionCreditoBancario_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Inicializar()

    End Sub

    Private Sub btnEliminar_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminar.Click

        If MessageBox.Show("Está seguro de que desea eliminar el registro?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
            Eliminar()
            Me.Close()
        End If

    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmSeleccionEliminacionCreditoBancario_Activate()
        Me.Refresh()
    End Sub

End Class
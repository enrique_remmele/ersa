﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrestamoBancarioCargarPago
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtNroCuota = New ERP.ocxTXTString()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.txtPagosVarios = New ERP.ocxTXTNumeric()
        Me.txtMontoPagado = New ERP.ocxTXTNumeric()
        Me.txtFechaPago = New ERP.ocxTXTDate()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.txtPrestamo = New ERP.ocxTXTString()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chkCreditoBancario = New ERP.ocxCHK()
        Me.btnDebitoCredito = New System.Windows.Forms.Button()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(257, 44)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(72, 13)
        Me.Label9.TabIndex = 37
        Me.Label9.Text = "Pagos Varios:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(122, 44)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(76, 13)
        Me.Label8.TabIndex = 35
        Me.Label8.Text = "Importe Cuota:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(13, 45)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 13)
        Me.Label7.TabIndex = 34
        Me.Label7.Text = "Fecha Pago:"
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(301, 160)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 46
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(201, 160)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 44
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 84)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 13)
        Me.Label1.TabIndex = 48
        Me.Label1.Text = "Observacion:"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(9, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 16)
        Me.Label3.TabIndex = 50
        Me.Label3.Text = "Cuota Nro:"
        '
        'txtNroCuota
        '
        Me.txtNroCuota.BackColor = System.Drawing.Color.Transparent
        Me.txtNroCuota.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroCuota.Color = System.Drawing.Color.Empty
        Me.txtNroCuota.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNroCuota.Indicaciones = Nothing
        Me.txtNroCuota.Location = New System.Drawing.Point(77, 9)
        Me.txtNroCuota.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtNroCuota.Multilinea = False
        Me.txtNroCuota.Name = "txtNroCuota"
        Me.txtNroCuota.Size = New System.Drawing.Size(65, 26)
        Me.txtNroCuota.SoloLectura = True
        Me.txtNroCuota.TabIndex = 51
        Me.txtNroCuota.TabStop = False
        Me.txtNroCuota.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroCuota.Texto = ""
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(12, 100)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(364, 27)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 47
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'txtPagosVarios
        '
        Me.txtPagosVarios.Color = System.Drawing.Color.Empty
        Me.txtPagosVarios.Decimales = True
        Me.txtPagosVarios.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtPagosVarios.Location = New System.Drawing.Point(255, 60)
        Me.txtPagosVarios.Name = "txtPagosVarios"
        Me.txtPagosVarios.Size = New System.Drawing.Size(121, 21)
        Me.txtPagosVarios.SoloLectura = False
        Me.txtPagosVarios.TabIndex = 38
        Me.txtPagosVarios.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPagosVarios.Texto = "0"
        '
        'txtMontoPagado
        '
        Me.txtMontoPagado.Color = System.Drawing.Color.Empty
        Me.txtMontoPagado.Decimales = True
        Me.txtMontoPagado.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtMontoPagado.Location = New System.Drawing.Point(120, 60)
        Me.txtMontoPagado.Name = "txtMontoPagado"
        Me.txtMontoPagado.Size = New System.Drawing.Size(129, 21)
        Me.txtMontoPagado.SoloLectura = False
        Me.txtMontoPagado.TabIndex = 36
        Me.txtMontoPagado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMontoPagado.Texto = "0"
        '
        'txtFechaPago
        '
        Me.txtFechaPago.Color = System.Drawing.Color.Empty
        Me.txtFechaPago.Fecha = New Date(CType(0, Long))
        Me.txtFechaPago.Location = New System.Drawing.Point(12, 60)
        Me.txtFechaPago.Name = "txtFechaPago"
        Me.txtFechaPago.PermitirNulo = False
        Me.txtFechaPago.Size = New System.Drawing.Size(102, 21)
        Me.txtFechaPago.SoloLectura = False
        Me.txtFechaPago.TabIndex = 33
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 189)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(384, 22)
        Me.StatusStrip1.TabIndex = 52
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'txtPrestamo
        '
        Me.txtPrestamo.BackColor = System.Drawing.Color.Transparent
        Me.txtPrestamo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPrestamo.Color = System.Drawing.Color.Empty
        Me.txtPrestamo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrestamo.Indicaciones = Nothing
        Me.txtPrestamo.Location = New System.Drawing.Point(212, 9)
        Me.txtPrestamo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtPrestamo.Multilinea = False
        Me.txtPrestamo.Name = "txtPrestamo"
        Me.txtPrestamo.Size = New System.Drawing.Size(164, 23)
        Me.txtPrestamo.SoloLectura = True
        Me.txtPrestamo.TabIndex = 54
        Me.txtPrestamo.TabStop = False
        Me.txtPrestamo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPrestamo.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(145, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 16)
        Me.Label2.TabIndex = 53
        Me.Label2.Text = "Prestamo:"
        '
        'chkCreditoBancario
        '
        Me.chkCreditoBancario.BackColor = System.Drawing.Color.Transparent
        Me.chkCreditoBancario.Color = System.Drawing.Color.Empty
        Me.chkCreditoBancario.Location = New System.Drawing.Point(11, 133)
        Me.chkCreditoBancario.Name = "chkCreditoBancario"
        Me.chkCreditoBancario.Size = New System.Drawing.Size(102, 21)
        Me.chkCreditoBancario.SoloLectura = False
        Me.chkCreditoBancario.TabIndex = 55
        Me.chkCreditoBancario.Texto = "Crédito Bancario"
        Me.chkCreditoBancario.Valor = False
        '
        'btnDebitoCredito
        '
        Me.btnDebitoCredito.Location = New System.Drawing.Point(11, 160)
        Me.btnDebitoCredito.Name = "btnDebitoCredito"
        Me.btnDebitoCredito.Size = New System.Drawing.Size(103, 23)
        Me.btnDebitoCredito.TabIndex = 56
        Me.btnDebitoCredito.Text = "&Debitar de Banco"
        Me.btnDebitoCredito.UseVisualStyleBackColor = True
        '
        'frmPrestamoBancarioCargarPago
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 211)
        Me.Controls.Add(Me.btnDebitoCredito)
        Me.Controls.Add(Me.chkCreditoBancario)
        Me.Controls.Add(Me.txtPrestamo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.txtNroCuota)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtObservacion)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.txtPagosVarios)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtMontoPagado)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtFechaPago)
        Me.Name = "frmPrestamoBancarioCargarPago"
        Me.Tag = "frmPrestamoBancarioCargarPago"
        Me.Text = "Cargar Pago Cuota Prestamo Bancario"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtPagosVarios As ERP.ocxTXTNumeric
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtMontoPagado As ERP.ocxTXTNumeric
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtFechaPago As ERP.ocxTXTDate
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNroCuota As ERP.ocxTXTString
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtPrestamo As ERP.ocxTXTString
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnDebitoCredito As System.Windows.Forms.Button
    Friend WithEvents chkCreditoBancario As ERP.ocxCHK
End Class

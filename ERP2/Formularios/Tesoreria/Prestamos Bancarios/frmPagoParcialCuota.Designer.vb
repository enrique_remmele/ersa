﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPagoParcialCuota
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblImpuesto = New System.Windows.Forms.Label()
        Me.lblCosto = New System.Windows.Forms.Label()
        Me.lblAmortizacion = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtFechaPago = New ERP.ocxTXTDate()
        Me.txtNroCuota = New ERP.ocxTXTString()
        Me.txtTotal = New ERP.ocxTXTNumeric()
        Me.txtImpuesto = New ERP.ocxTXTNumeric()
        Me.txtInteres = New ERP.ocxTXTNumeric()
        Me.txtAmortizacion = New ERP.ocxTXTNumeric()
        Me.SuspendLayout()
        '
        'lblImpuesto
        '
        Me.lblImpuesto.AutoSize = True
        Me.lblImpuesto.Location = New System.Drawing.Point(46, 135)
        Me.lblImpuesto.Name = "lblImpuesto"
        Me.lblImpuesto.Size = New System.Drawing.Size(53, 13)
        Me.lblImpuesto.TabIndex = 4
        Me.lblImpuesto.Text = "Impuesto:"
        '
        'lblCosto
        '
        Me.lblCosto.AutoSize = True
        Me.lblCosto.Location = New System.Drawing.Point(56, 110)
        Me.lblCosto.Name = "lblCosto"
        Me.lblCosto.Size = New System.Drawing.Size(42, 13)
        Me.lblCosto.TabIndex = 2
        Me.lblCosto.Text = "Interés:"
        '
        'lblAmortizacion
        '
        Me.lblAmortizacion.AutoSize = True
        Me.lblAmortizacion.Location = New System.Drawing.Point(56, 81)
        Me.lblAmortizacion.Name = "lblAmortizacion"
        Me.lblAmortizacion.Size = New System.Drawing.Size(42, 13)
        Me.lblAmortizacion.TabIndex = 0
        Me.lblAmortizacion.Text = "Capital:"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(50, 212)
        Me.btnAceptar.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(92, 28)
        Me.btnAceptar.TabIndex = 8
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(64, 164)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Total:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(171, 212)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(92, 28)
        Me.btnCancelar.TabIndex = 9
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(30, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 16)
        Me.Label3.TabIndex = 52
        Me.Label3.Text = "Cuota Nro:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(32, 51)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 13)
        Me.Label7.TabIndex = 55
        Me.Label7.Text = "Fecha Pago:"
        '
        'txtFechaPago
        '
        Me.txtFechaPago.Color = System.Drawing.Color.Empty
        Me.txtFechaPago.Fecha = New Date(CType(0, Long))
        Me.txtFechaPago.Location = New System.Drawing.Point(100, 47)
        Me.txtFechaPago.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtFechaPago.Name = "txtFechaPago"
        Me.txtFechaPago.PermitirNulo = False
        Me.txtFechaPago.Size = New System.Drawing.Size(102, 21)
        Me.txtFechaPago.SoloLectura = False
        Me.txtFechaPago.TabIndex = 54
        '
        'txtNroCuota
        '
        Me.txtNroCuota.BackColor = System.Drawing.Color.Transparent
        Me.txtNroCuota.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroCuota.Color = System.Drawing.Color.Empty
        Me.txtNroCuota.Enabled = False
        Me.txtNroCuota.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNroCuota.Indicaciones = Nothing
        Me.txtNroCuota.Location = New System.Drawing.Point(100, 16)
        Me.txtNroCuota.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtNroCuota.Multilinea = False
        Me.txtNroCuota.Name = "txtNroCuota"
        Me.txtNroCuota.Size = New System.Drawing.Size(65, 26)
        Me.txtNroCuota.SoloLectura = True
        Me.txtNroCuota.TabIndex = 53
        Me.txtNroCuota.TabStop = False
        Me.txtNroCuota.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroCuota.Texto = ""
        '
        'txtTotal
        '
        Me.txtTotal.Color = System.Drawing.Color.Empty
        Me.txtTotal.Decimales = True
        Me.txtTotal.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotal.Location = New System.Drawing.Point(100, 160)
        Me.txtTotal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(163, 21)
        Me.txtTotal.SoloLectura = True
        Me.txtTotal.TabIndex = 7
        Me.txtTotal.TabStop = False
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Texto = "0"
        '
        'txtImpuesto
        '
        Me.txtImpuesto.Color = System.Drawing.Color.Empty
        Me.txtImpuesto.Decimales = True
        Me.txtImpuesto.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImpuesto.Location = New System.Drawing.Point(100, 131)
        Me.txtImpuesto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtImpuesto.Name = "txtImpuesto"
        Me.txtImpuesto.Size = New System.Drawing.Size(163, 21)
        Me.txtImpuesto.SoloLectura = False
        Me.txtImpuesto.TabIndex = 5
        Me.txtImpuesto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImpuesto.Texto = "0"
        '
        'txtInteres
        '
        Me.txtInteres.Color = System.Drawing.Color.Empty
        Me.txtInteres.Decimales = True
        Me.txtInteres.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtInteres.Location = New System.Drawing.Point(100, 106)
        Me.txtInteres.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtInteres.Name = "txtInteres"
        Me.txtInteres.Size = New System.Drawing.Size(163, 21)
        Me.txtInteres.SoloLectura = False
        Me.txtInteres.TabIndex = 3
        Me.txtInteres.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtInteres.Texto = "0"
        '
        'txtAmortizacion
        '
        Me.txtAmortizacion.Color = System.Drawing.Color.Empty
        Me.txtAmortizacion.Decimales = True
        Me.txtAmortizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtAmortizacion.Location = New System.Drawing.Point(100, 76)
        Me.txtAmortizacion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtAmortizacion.Name = "txtAmortizacion"
        Me.txtAmortizacion.Size = New System.Drawing.Size(163, 21)
        Me.txtAmortizacion.SoloLectura = False
        Me.txtAmortizacion.TabIndex = 1
        Me.txtAmortizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAmortizacion.Texto = "0"
        '
        'frmPagoParcialCuota
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(291, 255)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtFechaPago)
        Me.Controls.Add(Me.txtNroCuota)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.txtImpuesto)
        Me.Controls.Add(Me.lblImpuesto)
        Me.Controls.Add(Me.txtInteres)
        Me.Controls.Add(Me.lblCosto)
        Me.Controls.Add(Me.txtAmortizacion)
        Me.Controls.Add(Me.lblAmortizacion)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "frmPagoParcialCuota"
        Me.Text = "frmPagoParcialCuota"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtImpuesto As ERP.ocxTXTNumeric
    Friend WithEvents lblImpuesto As System.Windows.Forms.Label
    Friend WithEvents txtInteres As ERP.ocxTXTNumeric
    Friend WithEvents lblCosto As System.Windows.Forms.Label
    Friend WithEvents txtAmortizacion As ERP.ocxTXTNumeric
    Friend WithEvents lblAmortizacion As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents txtTotal As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents txtNroCuota As ERP.ocxTXTString
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtFechaPago As ERP.ocxTXTDate
End Class

﻿Public Class frmPrestamoBancarioCargarPago

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CAsiento As New CAsientoMovimiento
    Dim CData As New CData

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue

        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private NroCuotaValue As Integer
    Public Property NroCuota() As Integer
        Get
            Return NroCuotaValue

        End Get
        Set(ByVal value As Integer)
            NroCuotaValue = value
        End Set
    End Property

    Public Property FechaPago As Date
    Public Property ImporteCuota As Decimal

    'Variables
    Dim dt As DataTable

    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True


        CargarControles()

    End Sub

    Sub CargarControles()

        dt = CSistema.ExecuteToDataTable("Select IDTransaccion, NroComprobante, Cuota, FechaPago, ImporteAPagar, PagosVarios, ObservacionPago, Cancelado From VDetallePrestamoBancario Where IDTransaccion=" & IDTransaccion & " And NroCuota=" & NroCuota & "").Copy

        'Cargamos
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnicos."
            MessageBox.Show(mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        Dim FechaPago As Date = CDate(oRow("FechaPago"))

        txtMontoPagado.txt.Text = oRow("ImporteAPagar").ToString
        If txtFechaPago Is Nothing Then
            txtFechaPago.txt.Text = Date.Now
        Else
            txtFechaPago.SetValueFromString(CDate(oRow("FechaPago").ToString))
        End If
        txtPagosVarios.txt.Text = oRow("PagosVarios").ToString
        txtObservacion.txt.Text = oRow("ObservacionPago").ToString

        'Nro Cuota
        txtNroCuota.Texto = NroCuota
        txtPrestamo.txt.Text = oRow("NroComprobante").ToString

    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        'Validar
        'Monto pagado
        If IsNumeric(txtMontoPagado.ObtenerValor) = False Then
            Dim mensaje As String = "El monto pagado no es correcto!"
            ctrError.SetError(txtMontoPagado, mensaje)
            ctrError.SetIconAlignment(txtMontoPagado, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Pagos varios
        If IsNumeric(txtPagosVarios.ObtenerValor) = False Then
            Dim mensaje As String = "La amortización no es correcta!"
            ctrError.SetError(txtPagosVarios, mensaje)
            ctrError.SetIconAlignment(txtPagosVarios, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Guardar
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroCuota", NroCuota, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaPago", CSistema.FormatoFechaBaseDatos(txtFechaPago.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ImporteAPagar", txtMontoPagado.ObtenerValor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PagosVarios", txtPagosVarios.ObtenerValor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ObservacionPago", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpDetallePrestamoBancarioDeclararPago", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        Me.Close()

    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.UPD)
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmPrestamoBancarioCargarPago_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmPrestamoBancarioCargarPago_Activate()
        Me.Refresh()
    End Sub

End Class
﻿Imports ERP.Reporte
Imports System.ComponentModel

Public Class frmPrestamoBancario

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CAsiento As New CAsientoMovimiento
    Dim CData As New CData
    Dim CReporte As New CReporteBanco

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue

        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dtCuentaBancaria As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles

        'Otros
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Propiedades
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "PRESTAMOBANCARIO", "PRB")
        IDTransaccion = 0
        vNuevo = True

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()

        'Botones

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, cbxCuenta)
        CSistema.CargaControl(vControles, txtCambio)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtPlazoDias)
        CSistema.CargaControl(vControles, txtPorcentajeInteres)
        CSistema.CargaControl(vControles, txtCapital)
        CSistema.CargaControl(vControles, txtImporteTotalInteres)
        CSistema.CargaControl(vControles, txtGastos)
        CSistema.CargaControl(vControles, txtNeto)
        CSistema.CargaControl(vControles, txtFechaVencimiento)
        CSistema.CargaControl(vControles, txtAmortizacion)
        CSistema.CargaControl(vControles, txtInteres)
        CSistema.CargaControl(vControles, txtImpuesto)
        CSistema.CargaControl(vControles, txtCuota)
        CSistema.CargaControl(vControles, cbxTipoGarantia)

        'CARGAR ESTRUCTURA DEL DETALLE
        dtDetalle = CSistema.ExecuteToDataTable("Select Top(0) * From VDetallePrestamoBancario").Clone

        'CARGAR CONTROLES
        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Cuenta Bancaria
        dtCuentaBancaria = CSistema.ExecuteToDataTable("Select * From VCuentaBancaria").Copy
        CSistema.SqlToComboBox(cbxCuenta.cbx, dtCuentaBancaria, "ID", "CuentaBancaria")

    End Sub

    Sub ListarDetalle()

        'Ordenamos el datatable por fechavencimiento para reenumerar las cuotas
        CData.OrderDataTable(dtDetalle, "FechaVencimiento", True)
        
        'Volver a enumerar las cuotas porque cuando se elimina se salta la cuota
        Dim Index As Integer = 1
        For Each oRow As DataRow In dtDetalle.Rows
            oRow("Cuota") = Index
            Index = Index + 1
        Next

        'Cargamos registro por registro
        CSistema.dtToGrid(dgw, dtDetalle)

        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).Visible = False
        Next

        dgw.Columns("Cuota").Visible = True
        dgw.Columns("FechaVencimiento").Visible = True
        dgw.Columns("Amortizacion").Visible = True
        dgw.Columns("Interes").Visible = True
        dgw.Columns("Impuesto").Visible = True
        dgw.Columns("ImporteCuota").Visible = True
        dgw.Columns("Pagar").Visible = True
        dgw.Columns("TotalPagado").Visible = True
        dgw.Columns("TotalPagado").HeaderText = "Total Pagado"
        dgw.Columns("Saldo").Visible = True

        dgw.Columns("Cuota").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
        dgw.Columns("TotalPagado").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        Dim Decimales As Boolean = CSistema.RetornarValorBoolean(CData.GetRow("Referencia='" & txtMoneda.GetValue & "'", "VMoneda")("Decimales").ToString)
        CSistema.DataGridColumnasNumericas(dgw, {"ImporteCuota", "Amortizacion", "Impuesto", "Interes", "TotalPagado", "Saldo"}, Decimales)

    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Cuenta Bancaria
        dtCuentaBancaria = CSistema.ExecuteToDataTable("Select * From VCuentaBancaria where Estado = 'True'").Copy
        CSistema.SqlToComboBox(cbxCuenta.cbx, dtCuentaBancaria, "ID", "CuentaBancaria")

        'Limpiar
        txtComprobante.Texto = ""
        txtObservacion.Texto = ""
        txtPlazoDias.Texto = 0
        txtPorcentajeInteres.Texto = 0
        txtCapital.Texto = 0
        txtImporteTotalInteres.Texto = 0
        txtGastos.Texto = 0
        txtNeto.Texto = 0
        txtAmortizacion.Texto = 0
        txtInteres.Texto = 0
        txtImpuesto.Texto = 0
        txtCuota.Texto = 0
        txtFecha.txt.Text = Date.Now
        txtTotalAmortizacion.txt.Text = 0
        txtSaldoCapital.txt.Text = 0
        cbxTipoComprobante.cbx.Text = ""
        txtTotalCapital.txt.Text = 0
        txtDebitoBancario.Clear()
        txtTotalInteres.Texto = 0
        txtTotalAmortizacionInteres.Texto = 0
        txtSaldoInteres.Texto = 0
        cbxTipoGarantia.SelectedIndex = 0

        'Limpiar detalle
        dtDetalle.Rows.Clear()
        ListarDetalle()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True

        CAsiento.Limpiar()
        CAsiento.Inicializar()

        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) FROM VPrestamoBancario Where IDSucursal = " & cbxSucursal.GetValue & "),1)"), Integer)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Poner el foco en la fecha
        txtFecha.txt.Focus()

        'Bloquear Botones de Debito Bancario
        btnGenerarDebitoBancario.Visible = False
        btnSeleccionarDebitoBancario.Visible = False

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VPrestamoBancario Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And Numero=" & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select Top(1) *  From VPrestamoBancario Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select * From VDetallePrestamoBancario Where IDTransaccion=" & IDTransaccion & " Order By FechaVencimiento").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Surgio un inconveniente y debe cerrarse."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        txtID.txt.Text = oRow("Numero").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        cbxTipoComprobante.cbx.Text = oRow("Cod.").ToString
        txtComprobante.txt.Text = oRow("NroComprobante").ToString

        'cbxSucursal.cbx.Text = oRow("Sucursal").ToString
        cbxCuenta.cbx.Text = oRow("CuentaBancaria").ToString
        txtBanco.txt.Text = oRow("Banco").ToString
        txtMoneda.txt.Text = oRow("Moneda").ToString
        txtCambio.txt.Text = oRow("Cotizacion").ToString
        ObtenerCuenta()

        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtPlazoDias.txt.Text = oRow("PlazoDias").ToString
        txtPorcentajeInteres.txt.Text = oRow("PorcentajeInteres").ToString
        txtCapital.txt.Text = oRow("Capital").ToString
        txtTotalCapital.txt.Text = oRow("Capital").ToString
        txtImporteTotalInteres.txt.Text = oRow("Interes").ToString
        txtTotalInteres.txt.Text = oRow("Interes").ToString
        txtGastos.txt.Text = oRow("Gastos").ToString
        txtNeto.txt.Text = oRow("Neto").ToString
        txtDebitoBancario.SetValue(oRow("DebitoBancario").ToString)
        cbxTipoGarantia.Text = (oRow("TipoGarantia").ToString)
        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("UsuarioIdentificador").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            btnGenerarDebitoBancario.Visible = False
            btnSeleccionarDebitoBancario.Visible = False
        Else
            flpAnuladoPor.Visible = False
            If txtDebitoBancario.GetValue.Trim.Length > 0 Then
                btnGenerarDebitoBancario.Visible = False
            Else
                btnGenerarDebitoBancario.Visible = True
            End If
            btnSeleccionarDebitoBancario.Visible = True
        End If

        Dim TotalAmortizacion As Decimal
        Dim TotalInteresPagado As Decimal

        'Total Amortizacion Cancelada
        TotalAmortizacion = (CSistema.ExecuteScalar("Select IsNull(Sum(AmortizacionCapital),0) From DetalleCuotaPrestamoBancario Where IDTransaccionPrestamoBancario=" & IDTransaccion & " "))

        txtTotalAmortizacion.txt.Text = TotalAmortizacion
        txtSaldoCapital.txt.Text = txtTotalCapital.ObtenerValor - txtTotalAmortizacion.ObtenerValor

        'Total Interes
        TotalInteresPagado = (CSistema.ExecuteScalar("Select IsNull(Sum(Interes),0) From DetalleCuotaPrestamoBancario Where IDTransaccionPrestamoBancario=" & IDTransaccion & " "))

        txtTotalAmortizacionInteres.txt.Text = TotalInteresPagado
        txtSaldoInteres.txt.Text = txtTotalInteres.ObtenerValor - txtTotalAmortizacionInteres.ObtenerValor

        'Cargamos el detalle
        ListarDetalle()

        'Inicializamos el Asiento
        CAsiento.Limpiar()

    End Sub

    Sub CargarProducto()

        'Validar                        

        'Cantidad
        If IsNumeric(txtAmortizacion.ObtenerValor) = False Then
            Dim mensaje As String = "La amortización no es correcta!"
            ctrError.SetError(txtAmortizacion, mensaje)
            ctrError.SetIconAlignment(txtAmortizacion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtAmortizacion.ObtenerValor) < 0 Then
            Dim mensaje As String = "La amortización no puede ser menor a cero!"
            ctrError.SetError(txtAmortizacion, mensaje)
            ctrError.SetIconAlignment(txtAmortizacion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cuota
        If IsNumeric(txtCuota.ObtenerValor) = False Then
            Dim mensaje As String = "La cuota no es correcta!"
            ctrError.SetError(txtCuota, mensaje)
            ctrError.SetIconAlignment(txtCuota, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtCuota.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cuota no es correcta!"
            ctrError.SetError(txtCuota, mensaje)
            ctrError.SetIconAlignment(txtCuota, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If (CDec(txtAmortizacion.ObtenerValor) + CDec(txtTotalAmortizacion.ObtenerValor)) > CDec(txtCapital.ObtenerValor) Then
            Dim mensaje As String = "El total de amortizacion es mayor que el total capital!"
            ctrError.SetError(txtCuota, mensaje)
            ctrError.SetIconAlignment(txtCuota, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cargamos el registro en el detalle
        Dim dRow As DataRow = dtDetalle.NewRow()

        'Obtener Valores
        dRow("FechaVencimiento") = txtFechaVencimiento.GetValue
        dRow("Amortizacion") = txtAmortizacion.ObtenerValor
        dRow("Cuota") = dtDetalle.Rows.Count + 1
        dRow("Interes") = txtInteres.ObtenerValor
        dRow("Impuesto") = txtImpuesto.ObtenerValor
        dRow("ImporteCuota") = txtCuota.ObtenerValor

        'Agregamos al detalle
        dtDetalle.Rows.Add(dRow)

        ListarDetalle()

        'Suma de Amortizacion
        txtTotalCapital.Texto = txtCapital.ObtenerValor
        ''txtTotalAmortizacion.Texto = txtTotalAmortizacion.ObtenerValor + txtAmortizacion.ObtenerValor
        txtSaldoCapital.Texto = txtTotalCapital.ObtenerValor - txtTotalAmortizacion.ObtenerValor

        'Inicializamos los valores
        txtFechaVencimiento.txt.Clear()
        txtAmortizacion.Texto = 0
        txtImpuesto.Texto = 0
        txtInteres.Texto = 0
        txtCuota.Texto = 0

        'Retorno de Foco
        txtFechaVencimiento.txt.SelectAll()
        txtFechaVencimiento.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Sub EliminarCuota()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If dgw.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgw, mensaje)
            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If dtDetalle.Rows.Count > 0 Then
            dtDetalle.Rows(dgw.SelectedRows(0).Index).Delete()
        End If

        'Volver a enumerar los ID
        Dim Index As Integer = 1
        For Each oRow As DataRow In dtDetalle.Rows
            oRow("Cuota") = Index
            Index = Index + 1
        Next

        'If dtDetalle.Rows.Count = 0 Then
        '    txtTotalAmortizacion.Texto = 0
        'Else
        '    txtTotalAmortizacion.Texto = dtDetalle.Compute("Sum(Amortizacion)", "")
        'End If
        'txtSaldoCapital.Texto = txtCapital.ObtenerValor - txtAmortizacion.ObtenerValor

        ListarDetalle()

        'CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)
        'CalcularTotales()

    End Sub

    Sub CalcularTotales()

        txtTotalAmortizacion.txt.Text = CSistema.FormatoMoneda(CSistema.gridSumColumn(dgw, "TotalPagado"), True)
        txtTotalCapital.txt.Text = CSistema.FormatoMoneda(CSistema.gridSumColumn(dgw, "colTotal"), True)

    End Sub


    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Detalle
        If dgw.Rows.Count = 0 Then
            Dim mensaje As String = "El registro no tiene ningun detalle!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Capital
        If txtCapital.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese el importe de capital!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Plazo Dias
        If txtPlazoDias.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese el pazo en dias del prestamo!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Porcentaje de interes
        If txtPorcentajeInteres.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese el porcentaje de interes del prestamo!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If txtPorcentajeInteres.ObtenerValor > 99.99 Then
            Dim mensaje As String = "El porcentaje de interes no puede ser mayor a 99,99% !"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Neto
        If txtNeto.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese el importe Neto del prestamo!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        'Capital
        If txtCapital.ObtenerValor <> txtTotalCapital.ObtenerValor Then
            Dim mensaje As String = "La suma del capital declarado en las cuotas no es igual al total de capital del préstamo !"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        ''Si se genero Debito Credito Bancario
        'If txtDebitoBancario.Texto = "" Then
        '    Dim mensaje As String = "Debe generar una operacion de Debito/Credito Bancario. Haga click en el boton Generar!"
        '    ctrError.SetError(btnGuardar, mensaje)
        '    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
        '    tsslEstado.Text = mensaje
        '    Exit Function
        'End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IDTransaccion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Numero", CInt(txtID.ObtenerValor), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCuentaBancaria", cbxCuenta.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(txtCambio.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PlazoDias", txtPlazoDias.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PorcentajeInteres", CSistema.FormatoNumeroBaseDatos(txtPorcentajeInteres.ObtenerValor, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Capital", CSistema.FormatoMonedaBaseDatos(txtCapital.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Interes", CSistema.FormatoNumeroBaseDatos(txtImporteTotalInteres.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ImpuestoInteres", CSistema.FormatoNumeroBaseDatos(txtImporteTotalInteres.txt.Text * 0.1, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Gastos", CSistema.FormatoNumeroBaseDatos(txtGastos.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Neto", CSistema.FormatoNumeroBaseDatos(txtNeto.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TipoGarantia", cbxTipoGarantia.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input) 'JGR 20140816
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SPPrestamoBancario", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro

            Exit Sub

        End If

        If IDTransaccion > 0 Then

            'Insertamos el Detalle
            If InsertarDetalle(IDTransaccion, ERP.CSistema.NUMOperacionesRegistro.INS) = False Then

                'Eliminar Registro

                Exit Sub

            End If

            'Cargamos el asiento
            'CAsiento.IDTransaccion = IDTransaccion
            'GenerarAsiento()
            'CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False

        GenerarDebitoBancario()

    End Sub


    Function InsertarDetalle(ByVal IDTransaccion As Integer, ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        InsertarDetalle = True

        For Each oRow As DataRow In dtDetalle.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroCuota", CSistema.FormatoNumeroBaseDatos(oRow("Cuota").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@FechaVencimiento", CSistema.FormatoFechaBaseDatos(oRow("FechaVencimiento").ToString, True, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Amortizacion", CSistema.FormatoNumeroBaseDatos(oRow("Amortizacion").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Interes", CSistema.FormatoNumeroBaseDatos(oRow("Interes").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Impuesto", CSistema.FormatoNumeroBaseDatos(oRow("Impuesto").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ImporteCuota", CSistema.FormatoNumeroBaseDatos(oRow("ImporteCuota").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Pagar", CSistema.FormatoNumeroBaseDatos(oRow("Pagar").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@FechaPago", oRow("FechaPago").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ImporteAPagar", CSistema.FormatoNumeroBaseDatos(oRow("ImporteAPagar").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@PagosVarios", CSistema.FormatoNumeroBaseDatos(oRow("PagosVarios").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ObservacionPago", oRow("ObservacionPago").ToString, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpDetallePrestamoBancario", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

                'Eliminar el Registro

                Return False

            End If

        Next

    End Function

    Sub ObtenerCuenta()

        Dim IDMoneda As Integer

        If cbxCuenta.cbx.SelectedValue Is Nothing Then
            Exit Sub
        End If

        txtBanco.txt.Clear()
        txtMoneda.txt.Clear()

        For Each oRow As DataRow In dtCuentaBancaria.Select(" ID=" & cbxCuenta.cbx.SelectedValue)

            txtBanco.txt.Text = oRow("Banco").ToString
            txtMoneda.txt.Text = oRow("Mon").ToString
            IDMoneda = oRow("IDMoneda").ToString

        Next

        MostrarDecimales(IDMoneda)

    End Sub

    Sub MostrarDecimales(ByVal IDMoneda As Integer)

        If dgw.Columns.Count = 0 Then
            Exit Sub
        End If

        If IDMoneda = 1 Then
            txtCapital.Decimales = False
            txtImporteTotalInteres.Decimales = False
            txtGastos.Decimales = False
            txtNeto.Decimales = False
            txtAmortizacion.Decimales = False
            txtInteres.Decimales = False
            txtImpuesto.Decimales = False
            txtCuota.Decimales = False
            txtTotalAmortizacion.Decimales = False
            txtSaldoCapital.Decimales = False
            txtTotalCapital.Decimales = False
            dgw.Columns("Amortizacion").DefaultCellStyle.Format = "N0"
            dgw.Columns("Interes").DefaultCellStyle.Format = "N0"
            dgw.Columns("Impuesto").DefaultCellStyle.Format = "N0"
            dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N0"
            dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N0"
        Else
            txtCapital.Decimales = True
            txtImporteTotalInteres.Decimales = True
            txtGastos.Decimales = True
            txtNeto.Decimales = True
            txtAmortizacion.Decimales = True
            txtInteres.Decimales = True
            txtImpuesto.Decimales = True
            txtCuota.Decimales = True
            txtTotalAmortizacion.Decimales = True
            txtSaldoCapital.Decimales = True
            txtTotalCapital.Decimales = True
            dgw.Columns("Amortizacion").DefaultCellStyle.Format = "N2"
            dgw.Columns("Interes").DefaultCellStyle.Format = "N2"
            dgw.Columns("Impuesto").DefaultCellStyle.Format = "N2"
            dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N2"
            dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N2"
        End If

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) FROM VPrestamoBancario Where IDSucursal = " & cbxSucursal.GetValue & "),1)"), Integer)

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        CargarOperacion()

    End Sub

    Sub Anular()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto anulara permanentemente el registro y se revertiran las asociaciones de Debitos y Créditos Bancarios. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.ANULAR.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Anular
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpPrestamoBancario", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)
            Exit Sub
        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

    End Sub

    Sub PagarCuota()

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        'Si esta anulado
        If lblAnulado.Visible = True Then
            MessageBox.Show("El registro se encuentra anulado. No se podra realizar la operacion!", "Prestamo Bancario", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim NroCuota As Integer = dgw.SelectedRows(0).Cells("Cuota").Value
        Dim FechaVencimiento As Date = dgw.SelectedRows(0).Cells("FechaVencimiento").Value
        Dim ImporteCuota As Decimal = dgw.SelectedRows(0).Cells("ImporteCuota").Value

        Dim frm As New frmPrestamoBancarioCargarPago
        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent

        frm.IDTransaccion = IDTransaccion
        frm.NroCuota = NroCuota
        frm.FechaPago = FechaVencimiento
        frm.ImporteCuota = ImporteCuota

        FGMostrarFormulario(Me, frm, "Pago de Cuota: " & NroCuota.ToString, Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        txtID.Focus()

    End Sub

    Sub GenerarCreditoBancarioCuota(ByVal Parcial As Boolean)

        If vNuevo = True Then
            MessageBox.Show("No se puede generar la operacion hasta que se guarde el prestamo!", "Credito Bancario", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        'Si esta anulado
        If lblAnulado.Visible = True Then
            MessageBox.Show("El registro se encuentra anulado. No se podra realizar la operacion!", "Prestamo Bancario", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        'Variables
        Dim Total As Decimal
        Dim frmPagoParcialCuota As New frmPagoParcialCuota
        Dim NroCuota As Integer = dgw.SelectedRows(0).Cells("NroCuota").Value
        frmPagoParcialCuota.Decimales = CSistema.RetornarValorBoolean(CData.GetRow("Referencia='" & txtMoneda.GetValue & "'", "VMoneda")("Decimales").ToString)

        If Parcial = False Then
            If CSistema.ExecuteScalar("Select Count(*) from DetalleCuotaPrestamoBancario Where IDTransaccionPrestamoBancario=" & IDTransaccion & " And NroCuota=" & NroCuota & "") > 0 Then
                MessageBox.Show("El registro ya cuenta con un pago. Ingresará al formulario de pago parcial para continuar!", "Prestamo Bancario", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Parcial = True
            End If
        End If


        If Parcial = True Then
            frmPagoParcialCuota.IDTransaccionPrestamoBancario = IDTransaccion
            frmPagoParcialCuota.CapitalTotalCuota = dgw.SelectedRows(0).Cells("Amortizacion").Value
            frmPagoParcialCuota.NroCuota = NroCuota
            frmPagoParcialCuota.Parcial = True
            FGMostrarFormulario(Me, frmPagoParcialCuota, "Pago Parcial de Cuota", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
            Total = frmPagoParcialCuota.Total
        End If

        If Parcial = False Then
            frmPagoParcialCuota.IDTransaccionPrestamoBancario = IDTransaccion
            frmPagoParcialCuota.CapitalTotalCuota = dgw.SelectedRows(0).Cells("Amortizacion").Value
            frmPagoParcialCuota.NroCuota = NroCuota
            frmPagoParcialCuota.Interes = dgw.SelectedRows(0).Cells("Interes").Value
            frmPagoParcialCuota.Impuesto = dgw.SelectedRows(0).Cells("Impuesto").Value
            frmPagoParcialCuota.Parcial = False
            FGMostrarFormulario(Me, frmPagoParcialCuota, "Pago Total de Cuota", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
            Total = frmPagoParcialCuota.Total
        End If

        Dim frm As New frmDebitoCreditoBancarioPrestamo
        frm.IDCuentaBancaria = cbxCuenta.GetValue
        frm.Fecha = dgw.SelectedRows(0).Cells("FechaVencimiento").Value
        frm.Cotizacion = txtCambio.ObtenerValor
        frm.IDSucursal = cbxSucursal.GetValue
        frm.TipoOperacion = frmDebitoCreditoBancarioPrestamo.ENUMTipoOperacion.Credito
        frm.Observacion = txtObservacion.GetValue
        If Parcial = False Then
            'frm.Total = dgw.SelectedRows(0).Cells("Saldo").Value
            frm.Total = frmPagoParcialCuota.AmortizacionCapital 'Va amortizacion de capital porque el interes + impuesto deben hacer en Debito cred bancario por separado segun luis  
        Else
            frm.Total = frmPagoParcialCuota.AmortizacionCapital
        End If

        frm.Inicializar()

        FGMostrarFormulario(Me, frm, "Debito/Credito Bancario", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        'Actualizamos el registro
        Dim SQL As String = "Insert InTo DetalleCuotaPrestamoBancario (IDTransaccionPrestamoBancario, IDTransaccionDebitoCreditoBancario, NroCuota, Fecha, DebitoAutomatico, AmortizacionCapital, Interes, Impuesto, PagoTotal) Values (" & IDTransaccion & ", " & frm.IDTransaccion & " ," & NroCuota & ",'" & CSistema.FormatoFechaBaseDatos(frmPagoParcialCuota.Fecha, True, False) & "', 'True', " & CSistema.FormatoMonedaBaseDatos(frmPagoParcialCuota.AmortizacionCapital, True) & " , " & CSistema.FormatoMonedaBaseDatos(frmPagoParcialCuota.Interes, True) & " , " & CSistema.FormatoMonedaBaseDatos(frmPagoParcialCuota.Impuesto, True) & " ," & CSistema.FormatoNumeroBaseDatos(Total, True) & ")"
        Dim Procesado As Integer = CSistema.ExecuteNonQuery(SQL)

        If Procesado = 0 Then
            MessageBox.Show("No se pudo asociar el registro! Debe seleccionar o anular el registro generado recientemente.", "Debito Bancario", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If


        CargarOperacion(IDTransaccion)

    End Sub

    Sub SeleccionarCreditoBancarioCuota()

        If vNuevo = True Then
            MessageBox.Show("No se puede generar la operacion hasta que se guarde el prestamo!", "Debito Bancario", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        ''Si ya tiene un credito no se puede volver a generar
        'If dgw.SelectedRows(0).Cells("CreditoBancarioAsociado").Value = True Then
        '    MessageBox.Show("Atencion! El registro ya tiene un Credito Bancario Asociado!", "Credito Bancario", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        'End If

        Dim frm As New frmConsultaDebitoCreditoBancario
        frm.WindowState = FormWindowState.Maximized
        FGMostrarFormulario(Me, frm, "Seleccionar el debito de asociacion", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        If frm.IDTransaccion = 0 Then
            Exit Sub
        End If

        Dim TotalPagado As Decimal = frm.Total

        'Actualizamos el registro
        Dim NroCuota As Integer = dgw.SelectedRows(0).Cells("NroCuota").Value
        'Dim SQL As String = "Update DetallePrestamoBancario Set IDTransaccionDebitoCreditoBancario=" & frm.IDTransaccion & " , DebitoAutomatico='True', Cancelado='True' Where IDTransaccion=" & IDTransaccion & " And NroCuota=" & NroCuota & " "
        Dim SQL As String = "Insert InTo DetalleCuotaPrestamoBancario (IDTransaccionPrestamoBancario, IDTransaccionDebitoCreditoBancario, NroCuota, DebitoAutomatico, PagoTotal) Values (" & IDTransaccion & ", " & frm.IDTransaccion & " ," & NroCuota & " , 'True', " & CSistema.FormatoNumeroBaseDatos(TotalPagado, True) & ")"
        Dim Procesado As Integer = CSistema.ExecuteNonQuery(SQL)

        If Procesado = 0 Then
            MessageBox.Show("No se pudo asociar el registro! Debe seleccionar o anular el registro generado recientemente.", "Debito Bancario", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        CargarOperacion(IDTransaccion)

    End Sub

    Sub EliminarCreditosBancarios()
        Dim frm As New frmSeleccionEliminacionCreditoBancario
        frm.IDTransaccionPrestamoBancario = IDTransaccion
        frm.Moneda = txtMoneda.GetValue
        frm.NroCuota = dgw.SelectedRows(0).Cells("NroCuota").Value
        FGMostrarFormulario(Me, frm, "Seleccion Credito Bancario", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
    End Sub

    Sub GenerarDebitoBancario()

        If vNuevo = True Then
            MessageBox.Show("No se puede generar la operacion hasta que se guarde el prestamo!", "Debito Bancario", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim frm As New frmDebitoCreditoBancarioPrestamo
        frm.btnCancelar.Visible = False
        frm.btnSalir.Visible = False
        frm.IDCuentaBancaria = cbxCuenta.GetValue
        frm.Fecha = txtFecha.GetValue
        frm.Cotizacion = txtCambio.ObtenerValor
        frm.IDSucursal = cbxSucursal.GetValue
        frm.TipoOperacion = frmDebitoCreditoBancarioPrestamo.ENUMTipoOperacion.Debito
        frm.Observacion = txtObservacion.GetValue
        frm.Total = txtNeto.ObtenerValor
        frm.Inicializar()

        FGMostrarFormulario(Me, frm, "Debito/Credito Bancario", Windows.Forms.FormBorderStyle.None, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        'Actualizamos el registro
        Dim SQL As String = "Update PrestamoBancario Set IDTransaccionDebitoCreditoBancario=" & frm.IDTransaccion & " Where IDTransaccion=" & IDTransaccion & " "
        Dim Procesado As Integer = CSistema.ExecuteNonQuery(SQL)

        If Procesado = 0 Then
            MessageBox.Show("No se pudo asociar el registro! Debe seleccionar o anular el registro generado recientemente.", "Debito Bancario", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        CargarOperacion(IDTransaccion)

    End Sub

    Sub SeleccionarDebitoBancario()

        If vNuevo = True Then
            MessageBox.Show("No se puede generar la operacion hasta que se guarde el prestamo!", "Debito Bancario", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim frm As New frmConsultaDebitoCreditoBancario
        frm.WindowState = FormWindowState.Maximized
        FGMostrarFormulario(Me, frm, "Seleccionar el debito de asociacion", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        If frm.IDTransaccion = 0 Then
            Exit Sub
        End If

        Dim TotalPagado As Decimal = frm.Total

        'Actualizamos el registro
        Dim NroCuota As Integer = dgw.SelectedRows(0).Cells("NroCuota").Value
        'Dim SQL As String = "Update PrestamoBancario Set IDTransaccionDebitoCreditoBancario=" & frm.IDTransaccion & " Where IDTransaccion=" & IDTransaccion & " "
        Dim SQL As String = "Insert InTo DetalleCuotaPrestamoBancario (IDTransaccionPrestamoBancario, IDTransaccionDebitoCreditoBancario, NroCuota, DebitoAutomatico, PagoTotal) Values (" & IDTransaccion & ", " & frm.IDTransaccion & " ," & NroCuota & " , 'True', " & CSistema.FormatoNumeroBaseDatos(TotalPagado, True) & ")"
        Dim Procesado As Integer = CSistema.ExecuteNonQuery(Sql)

        If Procesado = 0 Then
            MessageBox.Show("No se pudo asociar el registro, ya se utilizó en otra operación! Seleccione otro registro o genere uno nuevo.", "Debito Bancario", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        CargarOperacion(IDTransaccion)

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)
        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)
    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        Dim Comprobante As String = txtDebitoBancario.GetValue

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante
            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccionDebitoCreditoBancario From VPrestamoBancario Where Numero=" & txtID.ObtenerValor & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)

        End If

    End Sub

    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub frmPrestamoBancario_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If txtID.txt.Focused Then
            Exit Sub
        End If

        If txtCuota.txt.Focused Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmPrestamoBancario_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub cbxCuenta_Leave(sender As Object, e As System.EventArgs) Handles cbxCuenta.Leave
        ObtenerCuenta()
    End Sub

    Private Sub cbxCuenta_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCuenta.PropertyChanged
        ObtenerCuenta()
    End Sub

    Private Sub txtCuota_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCuota.TeclaPrecionada

        'txtCuota.Texto = txtAmortizacion.ObtenerValor + txtInteres.ObtenerValor + txtImpuesto.ObtenerValor

        If e.KeyCode = Keys.Add Then
            txtCuota.Texto = txtAmortizacion.ObtenerValor + txtInteres.ObtenerValor + txtImpuesto.ObtenerValor
        End If

        If txtCuota.ObtenerValor <> 0 Then
            If txtCuota.ObtenerValor <> (txtAmortizacion.ObtenerValor + txtInteres.ObtenerValor + txtImpuesto.ObtenerValor) Then
                tsslEstado.Text = "Atencion: El importe de la cuota no es correcto"
                Exit Sub
            End If
        End If

        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If
    End Sub

    Private Sub txtImpuesto_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtImpuesto.TeclaPrecionada
        If e.KeyCode = Keys.Add Then
            txtImpuesto.Texto = txtInteres.ObtenerValor * 0.1
        End If
    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Num), 1) From VPrestamoBancario Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Num), 1) From VPrestamoBancario Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            ' Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If
    End Sub

    Private Sub Imprimir()
        Dim Where As String = " where IDTransaccion =" & IDTransaccion
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim Banco As String = ""
        Dim Titulo As String = ""
        Dim TipoInforme As String = ""
        Dim frm As New frmReporte
        Dim Informe As String = "Detallado"
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        CReporte.ListadoPrestamoBancarioDetalle(frm, Titulo, Where, OrderBy, Top, vgUsuarioIdentificador, "", "", TipoInforme, Informe)
    End Sub

    Private Sub txtID_TeclaPrecionada(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub cbxTipoComprobante_Editar(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxTipoComprobante.Editar

        Dim frm As New frmTipoComprobante
        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        cbxTipoComprobante.cbx.DataSource = Nothing
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=20")

    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        Select Case e.KeyCode
            Case Keys.F2
                GenerarCreditoBancarioCuota(False)
            Case Keys.F3
                GenerarCreditoBancarioCuota(True)
            Case Keys.F4
                EliminarCreditosBancarios()
            Case Keys.F5
                PagarCuota()
            Case Keys.Delete
                EliminarCuota()
        End Select
    End Sub

    Sub txtID_Enter(sender As Object, e As System.EventArgs) Handles txtID.Enter

        ' Pulsamos la tecla Enter
        SendKeys.Send("{ENTER}")

    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnAnular_Click(sender As Object, e As System.EventArgs) Handles btnAnular.Click
        Anular()
    End Sub

    Private Sub btnGenerarDebitoBancario_Click(sender As System.Object, e As System.EventArgs) Handles btnGenerarDebitoBancario.Click
        GenerarDebitoBancario()
    End Sub

    Private Sub btnSeleccionarDebitoBancario_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionarDebitoBancario.Click
        SeleccionarDebitoBancario()
    End Sub

    Private Sub btnAsiento_Click(sender As System.Object, e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    'Private Sub lklAsignarCredito_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklAsignarCredito.LinkClicked
    '    SeleccionarCreditoBancarioCuota()
    'End Sub

    Private Sub lklEstablecerPago_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEstablecerPago.LinkClicked
        PagarCuota()
    End Sub

    'Private Sub AsignarCreditoBancarioToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AsignarCreditoBancarioToolStripMenuItem.Click
    '    SeleccionarCreditoBancarioCuota()
    'End Sub

    Private Sub EstablecerPagoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles EstablecerPagoToolStripMenuItem.Click
        PagarCuota()
    End Sub

    Private Sub lklGenerarCreditoBancarioParcial_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklGenerarCreditoBancarioParcial.LinkClicked
        GenerarCreditoBancarioCuota(True)
    End Sub

    Private Sub btnImprimir_Click(sender As System.Object, e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Close()
    End Sub

    Private Sub lklGenerarCreditoBancario_LinkClicked(sender As Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklGenerarCreditoBancario.LinkClicked
        GenerarCreditoBancarioCuota(False)
    End Sub

    Private Sub PagoTotalToolStripMenuItem_Click(sender As Object, e As System.EventArgs) Handles PagoTotalToolStripMenuItem.Click
        GenerarCreditoBancarioCuota(False)
    End Sub

    Private Sub PagoParcialToolStripMenuItem_Click(sender As Object, e As System.EventArgs) Handles PagoParcialToolStripMenuItem.Click
        GenerarCreditoBancarioCuota(True)
    End Sub

    Private Sub lklEliminarCreditosBancarios_LinkClicked(sender As Object, e As System.EventArgs) Handles lklEliminarCreditosBancarios.LinkClicked
        EliminarCreditosBancarios()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmPrestamoBancario_Activate()
        Me.Refresh()
    End Sub
End Class

﻿Imports ERP.CSistema

Public Class frmPagoParcialCuota
    'CLASES
    Dim CSistema As New CSistema

    'Propiedades
    Public Property IDTransaccionPrestamoBancario As Integer
    Public Property CapitalTotalCuota As Decimal
    Public Property NroCuota As Integer
    Public Property Total As Decimal
    Public Property AmortizacionCapital As Decimal
    Public Property Interes As Decimal
    Public Property Impuesto As Decimal
    Public Property Fecha As Date
    Public Property Parcial As Boolean
    Public Property Decimales As Boolean

    Sub Limpiar()
        txtAmortizacion.Texto = 0
        txtInteres.Texto = 0
        txtImpuesto.Texto = 0
        txtTotal.Texto = 0
        txtNroCuota.Texto = ""
    End Sub

    Sub CalcularTotal()
        txtTotal.Texto = ""
        Total = txtAmortizacion.ObtenerValor + txtInteres.ObtenerValor + txtImpuesto.ObtenerValor
        txtTotal.Texto = Total
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As System.EventArgs) Handles btnAceptar.Click

        If Validar() = False Then
            Exit Sub
        End If

        AmortizacionCapital = txtAmortizacion.ObtenerValor
        Interes = txtInteres.ObtenerValor
        Impuesto = txtImpuesto.ObtenerValor
        Fecha = txtFechaPago.txt.Text

        Me.Close()

    End Sub

    'Validar
    Function Validar() As Boolean

        Validar = False

        Dim TotalAmortizacion As Decimal = CSistema.ExecuteScalar("Select IsNull(Sum(AmortizacionCapital),0) From DetalleCuotaPrestamoBancario Where IDTransaccionPrestamoBancario = " & IDTransaccionPrestamoBancario & " And NroCuota = " & NroCuota & "")
        TotalAmortizacion = TotalAmortizacion + txtAmortizacion.ObtenerValor
        If TotalAmortizacion > CapitalTotalCuota Then
            MessageBox.Show("La amortizacion de capital declarado supera el total de capital de la cuota, Verifique!!.", "Debito Bancario", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            txtAmortizacion.Texto = 0
            Exit Function
        End If

        Validar = True

    End Function

    Private Sub txtImpuesto_Leave(sender As Object, e As System.EventArgs) Handles txtImpuesto.Leave
        CalcularTotal()
    End Sub

    Private Sub txtAmortizacion_Leave(sender As Object, e As System.EventArgs) Handles txtAmortizacion.Leave
        CalcularTotal()
    End Sub

    Private Sub txtInteres_Leave(sender As Object, e As System.EventArgs) Handles txtInteres.Leave
        CalcularTotal()
    End Sub

    Private Sub frmPagoParcialCuota_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Limpiar()

        txtFechaPago.txt.Text = Date.Now
        txtNroCuota.txt.Text = NroCuota

        If Parcial = False Then
            txtAmortizacion.txt.Text = CapitalTotalCuota
            txtImpuesto.txt.Text = Impuesto
            txtInteres.txt.Text = Interes
            CalcularTotal()
            txtAmortizacion.Enabled = False
            txtImpuesto.Enabled = False
            txtInteres.Enabled = False
        End If

        If Parcial = True Then
            txtAmortizacion.Enabled = True
            txtImpuesto.Enabled = True
            txtInteres.Enabled = True
        End If

    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmPagoParcialCuota_Activate()
        Me.Refresh()
    End Sub
End Class
﻿''' <summary>
''' JGR 09/04/2014
''' Este formulario se realizo con POO (Programacion Orientada a Objetos)
''' Utiliza Clases, Listas y Binding de objetos.
''' </summary>
''' <remarks></remarks>
Public Class frmTipoTarjeta
    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES

    Dim vControles() As Control

    'OBJETOS Y LISTAS
    Private obj As TipoTarjeta
    Private lista As List(Of TipoTarjeta)

#Region "Funciones y Procedimientos"
    'FUNCIONES
    Sub Inicializar()

        'Funciones
        CargarComboTipoTarjeta()

        'Botones
        ReDim vControles(-1)
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Cargamos los registos en la lista
        Actualizar()

    End Sub

    Private Sub CargarComboTipoTarjeta()
        Dim l As New List(Of Dato)
        Dim o As Dato
        o = New Dato
        o.idString = "C"
        o.descripcion = "CRÉDITO"
        l.Add(o)
        o = New Dato
        o.idString = "D"
        o.descripcion = "DÉBITO"
        l.Add(o)
        DatoBindingSource.DataSource = l
    End Sub

    ''' <summary>
    ''' Actualiza los datos consultando a la base de datos.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Actualizar()
        Try
            Cursor = Cursors.WaitCursor
            Using objDAO As New TipoTarjetaDAO
                lista = objDAO.GetAll()
                TipoTarjetaBindingSource.DataSource = lista
            End Using

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary>
    ''' Elimina un registro de la base de datos y remueve de la colección.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Eliminar()
        If DataGridView1.SelectedRows.Count = 0 Then
            Return
        End If
        Try
            obj = CType(TipoTarjetaBindingSource.Current, TipoTarjeta)
            If obj IsNot Nothing AndAlso _
                 MessageBox.Show("¡Atención! Esto eliminará permanentemente el registro. ¿Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Cursor = Cursors.WaitCursor
                Using objDAO As New TipoTarjetaDAO
                    objDAO.Delete(obj)
                End Using
                TipoTarjetaBindingSource.Remove(obj)
            End If
        Catch ex As Exception
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            txtDescripcion.ReadOnly = True
            ComboBox1.Enabled = True
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Modificacion del registro seleccionado.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Modificar(Optional ByVal item As TipoTarjeta = Nothing)
        If DataGridView1.SelectedRows.Count = 0 Then
            Return
        End If
        Try
            If obj Is Nothing Then
                obj = CType(TipoTarjetaBindingSource.Current, TipoTarjeta)
            End If

            If obj IsNot Nothing Then
                'Establecemos los botones a Editando
                CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

                DataGridView1.Enabled = False
                txtDescripcion.ReadOnly = False
                ComboBox1.Enabled = True

                obj.IsNew = False

                'Foco
                txtDescripcion.Focus()
                txtDescripcion.SelectAll()

            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    ''' <summary>
    ''' Crea un nuevo registro.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Nuevo()
        Try
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            txtDescripcion.ReadOnly = False
            ComboBox1.Enabled = True

            obj = New TipoTarjeta
            obj.IsNew = True
            obj.ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From VTipoTarjeta"), Integer)
            If obj.IsNew Then

                TipoTarjetaBindingSource.Add(obj)
                TipoTarjetaBindingSource.MoveLast()
                DataGridView1.Enabled = False
                txtDescripcion.Focus()
            End If
            ctrError.Clear()


        Catch ex As Exception
            If obj Is Nothing Then
                obj = CType(TipoTarjetaBindingSource.Current, TipoTarjeta)
                TipoTarjetaBindingSource.Remove(obj)
            End If
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            txtDescripcion.ReadOnly = True
            ComboBox1.Enabled = True
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region

#Region "Evento CLICK"
    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Modificar()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        If obj IsNot Nothing Then
            If obj.IsNew Then
                TipoTarjetaBindingSource.Remove(obj)
            End If

        End If
        TipoTarjetaBindingSource.CancelEdit()
        DataGridView1.Enabled = True
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        ctrError.Clear()

        txtDescripcion.ReadOnly = True
        ComboBox1.Enabled = False
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Using objDAO As New TipoTarjetaDAO
            objDAO.Save(obj)
        End Using
        DataGridView1.Enabled = True
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        ctrError.Clear()

        txtDescripcion.ReadOnly = True
        ComboBox1.Enabled = False

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        'Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
        Eliminar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
#End Region



    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        ''Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        'If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
        '    If lvLista.SelectedItems.Count = 0 Then
        '        Dim mensaje As String = "¡Seleccione un registro!"
        '        ctrError.SetError(lvLista, mensaje)
        '        ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopRight)
        '        tsslEstado.Text = mensaje
        '        Exit Sub
        '    End If
        'End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("¡Atención! Esto eliminará permanentemente el registro. ¿Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text.Trim, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@Tipo", IIf(rdbActivo.Checked, "C", "D"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpTipoTarjeta", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            'Listar(txtID.txt.Text)
        Else
            tsslEstado.Text = "Atención: " & MensajeRetorno.Substring(0, 90)

            ctrError.SetError(btnGuardar, "Atención: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub


    Private Sub frmTipoTarjeta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub


    '09-06-2021 - SC - Actualiza datos
    Sub frmTipoTarjeta_Activate()
        Me.Refresh()
    End Sub


End Class
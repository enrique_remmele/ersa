﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRetencionIVA
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.chkOP = New System.Windows.Forms.CheckBox()
        Me.btnCargar = New System.Windows.Forms.Button()
        Me.cbxProveedor = New ERP.ocxCBX()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtDireccion = New ERP.ocxTXTString()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtRUC = New ERP.ocxTXTString()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.txtOP = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.gbxComprobante = New System.Windows.Forms.GroupBox()
        Me.txtIDTimbrado = New ERP.ocxTXTString()
        Me.txtTimbrado = New ERP.ocxTXTString()
        Me.lklTimbrado = New System.Windows.Forms.LinkLabel()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtRestoTimbrado = New ERP.ocxTXTNumeric()
        Me.lblRestoTimbrado = New System.Windows.Forms.Label()
        Me.lblCiudad = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.txtVencimientoTimbrado = New ERP.ocxTXTDate()
        Me.lblVenimientoTimbrado = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.txtNroComprobante = New ERP.ocxTXTString()
        Me.txtTalonario = New ERP.ocxTXTString()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.lblTalonario = New System.Windows.Forms.Label()
        Me.txtReferenciaSucursal = New ERP.ocxTXTString()
        Me.txtReferenciaTerminal = New ERP.ocxTXTString()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colGravado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPorcentajeRetencion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRetencionIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPorcentajeRenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtTotalImpuesto = New ERP.ocxTXTNumeric()
        Me.txtTotalRetencion = New ERP.ocxTXTNumeric()
        Me.txtTotalRetencionIVA = New ERP.ocxTXTNumeric()
        Me.txtTotalretencionRenta = New ERP.ocxTXTNumeric()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblTotalRetencionIVA = New System.Windows.Forms.Label()
        Me.lblTotalImpuesto = New System.Windows.Forms.Label()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.flpRegistradoPor.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.gbxCabecera.SuspendLayout()
        Me.gbxComprobante.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(3, 29)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(289, 20)
        Me.flpRegistradoPor.TabIndex = 6
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 3)
        Me.lblRegistradoPor.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 3)
        Me.lblUsuarioRegistro.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 3)
        Me.lblFechaRegistro.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(295, 28)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(315, 20)
        Me.flpAnuladoPor.TabIndex = 7
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 3)
        Me.lblAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 3)
        Me.lblUsuarioAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 3)
        Me.lblFechaAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.chkOP)
        Me.gbxCabecera.Controls.Add(Me.btnCargar)
        Me.gbxCabecera.Controls.Add(Me.cbxProveedor)
        Me.gbxCabecera.Controls.Add(Me.Label8)
        Me.gbxCabecera.Controls.Add(Me.txtDireccion)
        Me.gbxCabecera.Controls.Add(Me.Label7)
        Me.gbxCabecera.Controls.Add(Me.txtRUC)
        Me.gbxCabecera.Controls.Add(Me.Label6)
        Me.gbxCabecera.Controls.Add(Me.btnListar)
        Me.gbxCabecera.Controls.Add(Me.txtOP)
        Me.gbxCabecera.Controls.Add(Me.Label1)
        Me.gbxCabecera.Controls.Add(Me.cbxMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.txtCotizacion)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.Label2)
        Me.gbxCabecera.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxCabecera.Location = New System.Drawing.Point(0, 69)
        Me.gbxCabecera.Margin = New System.Windows.Forms.Padding(0)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(734, 128)
        Me.gbxCabecera.TabIndex = 1
        Me.gbxCabecera.TabStop = False
        '
        'chkOP
        '
        Me.chkOP.AutoSize = True
        Me.chkOP.Location = New System.Drawing.Point(29, 19)
        Me.chkOP.Name = "chkOP"
        Me.chkOP.Size = New System.Drawing.Size(15, 14)
        Me.chkOP.TabIndex = 0
        Me.chkOP.UseVisualStyleBackColor = True
        '
        'btnCargar
        '
        Me.btnCargar.Location = New System.Drawing.Point(171, 100)
        Me.btnCargar.Name = "btnCargar"
        Me.btnCargar.Size = New System.Drawing.Size(101, 21)
        Me.btnCargar.TabIndex = 17
        Me.btnCargar.Text = "Cargar"
        Me.btnCargar.UseVisualStyleBackColor = True
        Me.btnCargar.Visible = False
        '
        'cbxProveedor
        '
        Me.cbxProveedor.CampoWhere = Nothing
        Me.cbxProveedor.CargarUnaSolaVez = False
        Me.cbxProveedor.DataDisplayMember = Nothing
        Me.cbxProveedor.DataFilter = Nothing
        Me.cbxProveedor.DataOrderBy = Nothing
        Me.cbxProveedor.DataSource = Nothing
        Me.cbxProveedor.DataValueMember = Nothing
        Me.cbxProveedor.dtSeleccionado = Nothing
        Me.cbxProveedor.FormABM = Nothing
        Me.cbxProveedor.Indicaciones = Nothing
        Me.cbxProveedor.Location = New System.Drawing.Point(226, 16)
        Me.cbxProveedor.Name = "cbxProveedor"
        Me.cbxProveedor.SeleccionMultiple = False
        Me.cbxProveedor.SeleccionObligatoria = False
        Me.cbxProveedor.Size = New System.Drawing.Size(322, 21)
        Me.cbxProveedor.SoloLectura = False
        Me.cbxProveedor.TabIndex = 4
        Me.cbxProveedor.TabStop = False
        Me.cbxProveedor.Texto = ""
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(165, 48)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(55, 13)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Dirección:"
        '
        'txtDireccion
        '
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Color = System.Drawing.Color.Empty
        Me.txtDireccion.Indicaciones = Nothing
        Me.txtDireccion.Location = New System.Drawing.Point(226, 44)
        Me.txtDireccion.Multilinea = False
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(322, 21)
        Me.txtDireccion.SoloLectura = True
        Me.txtDireccion.TabIndex = 10
        Me.txtDireccion.TabStop = False
        Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccion.Texto = ""
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(39, 48)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(33, 13)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "RUC:"
        '
        'txtRUC
        '
        Me.txtRUC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRUC.Color = System.Drawing.Color.Empty
        Me.txtRUC.Indicaciones = Nothing
        Me.txtRUC.Location = New System.Drawing.Point(75, 44)
        Me.txtRUC.Multilinea = False
        Me.txtRUC.Name = "txtRUC"
        Me.txtRUC.Size = New System.Drawing.Size(79, 21)
        Me.txtRUC.SoloLectura = True
        Me.txtRUC.TabIndex = 8
        Me.txtRUC.TabStop = False
        Me.txtRUC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRUC.Texto = ""
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(161, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 13)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Proveedor:"
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(75, 100)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(91, 21)
        Me.btnListar.TabIndex = 16
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'txtOP
        '
        Me.txtOP.Color = System.Drawing.Color.Empty
        Me.txtOP.Decimales = False
        Me.txtOP.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtOP.Location = New System.Drawing.Point(75, 16)
        Me.txtOP.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOP.Name = "txtOP"
        Me.txtOP.Size = New System.Drawing.Size(79, 21)
        Me.txtOP.SoloLectura = False
        Me.txtOP.TabIndex = 2
        Me.txtOP.TabStop = False
        Me.txtOP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOP.Texto = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(48, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(25, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "OP:"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Referencia"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(605, 44)
        Me.cbxMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(53, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 12
        Me.cbxMoneda.TabStop = False
        Me.cbxMoneda.Texto = ""
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(555, 48)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 11
        Me.lblMoneda.Text = "Moneda:"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(658, 44)
        Me.txtCotizacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(57, 21)
        Me.txtCotizacion.SoloLectura = False
        Me.txtCotizacion.TabIndex = 13
        Me.txtCotizacion.TabStop = False
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "1"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(75, 73)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(640, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 15
        Me.txtObservacion.TabStop = False
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 6, 7, 8, 24, 40, 825)
        Me.txtFecha.Location = New System.Drawing.Point(605, 16)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(110, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 6
        Me.txtFecha.TabStop = False
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(564, 20)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 5
        Me.lblFecha.Text = "Fecha:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 13)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Observacion:"
        '
        'gbxComprobante
        '
        Me.gbxComprobante.Controls.Add(Me.txtIDTimbrado)
        Me.gbxComprobante.Controls.Add(Me.txtTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lklTimbrado)
        Me.gbxComprobante.Controls.Add(Me.cbxSucursal)
        Me.gbxComprobante.Controls.Add(Me.cbxCiudad)
        Me.gbxComprobante.Controls.Add(Me.txtRestoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lblRestoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lblCiudad)
        Me.gbxComprobante.Controls.Add(Me.lblSucursal)
        Me.gbxComprobante.Controls.Add(Me.txtVencimientoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lblVenimientoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lblComprobante)
        Me.gbxComprobante.Controls.Add(Me.txtNroComprobante)
        Me.gbxComprobante.Controls.Add(Me.txtTalonario)
        Me.gbxComprobante.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxComprobante.Controls.Add(Me.lblTalonario)
        Me.gbxComprobante.Controls.Add(Me.txtReferenciaSucursal)
        Me.gbxComprobante.Controls.Add(Me.txtReferenciaTerminal)
        Me.gbxComprobante.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxComprobante.Location = New System.Drawing.Point(0, 0)
        Me.gbxComprobante.Margin = New System.Windows.Forms.Padding(0)
        Me.gbxComprobante.Name = "gbxComprobante"
        Me.gbxComprobante.Size = New System.Drawing.Size(734, 69)
        Me.gbxComprobante.TabIndex = 0
        Me.gbxComprobante.TabStop = False
        '
        'txtIDTimbrado
        '
        Me.txtIDTimbrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIDTimbrado.Color = System.Drawing.Color.Beige
        Me.txtIDTimbrado.Indicaciones = Nothing
        Me.txtIDTimbrado.Location = New System.Drawing.Point(68, 12)
        Me.txtIDTimbrado.Multilinea = False
        Me.txtIDTimbrado.Name = "txtIDTimbrado"
        Me.txtIDTimbrado.Size = New System.Drawing.Size(36, 21)
        Me.txtIDTimbrado.SoloLectura = True
        Me.txtIDTimbrado.TabIndex = 18
        Me.txtIDTimbrado.TabStop = False
        Me.txtIDTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtIDTimbrado.Texto = ""
        Me.txtIDTimbrado.Visible = False
        '
        'txtTimbrado
        '
        Me.txtTimbrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTimbrado.Color = System.Drawing.Color.Beige
        Me.txtTimbrado.Indicaciones = Nothing
        Me.txtTimbrado.Location = New System.Drawing.Point(69, 12)
        Me.txtTimbrado.Multilinea = False
        Me.txtTimbrado.Name = "txtTimbrado"
        Me.txtTimbrado.Size = New System.Drawing.Size(176, 21)
        Me.txtTimbrado.SoloLectura = True
        Me.txtTimbrado.TabIndex = 1
        Me.txtTimbrado.TabStop = False
        Me.txtTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTimbrado.Texto = ""
        '
        'lklTimbrado
        '
        Me.lklTimbrado.AutoSize = True
        Me.lklTimbrado.Location = New System.Drawing.Point(9, 14)
        Me.lklTimbrado.Name = "lklTimbrado"
        Me.lklTimbrado.Size = New System.Drawing.Size(54, 13)
        Me.lklTimbrado.TabIndex = 0
        Me.lklTimbrado.TabStop = True
        Me.lklTimbrado.Text = "Timbrado:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Codigo"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(375, 10)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(117, 21)
        Me.cbxSucursal.SoloLectura = True
        Me.cbxSucursal.TabIndex = 5
        Me.cbxSucursal.TabStop = False
        Me.cbxSucursal.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = "Codigo"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = ""
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(291, 11)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(54, 21)
        Me.cbxCiudad.SoloLectura = True
        Me.cbxCiudad.TabIndex = 3
        Me.cbxCiudad.TabStop = False
        Me.cbxCiudad.Texto = ""
        '
        'txtRestoTimbrado
        '
        Me.txtRestoTimbrado.Color = System.Drawing.Color.Beige
        Me.txtRestoTimbrado.Decimales = True
        Me.txtRestoTimbrado.Indicaciones = Nothing
        Me.txtRestoTimbrado.Location = New System.Drawing.Point(375, 34)
        Me.txtRestoTimbrado.Name = "txtRestoTimbrado"
        Me.txtRestoTimbrado.Size = New System.Drawing.Size(117, 22)
        Me.txtRestoTimbrado.SoloLectura = True
        Me.txtRestoTimbrado.TabIndex = 16
        Me.txtRestoTimbrado.TabStop = False
        Me.txtRestoTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRestoTimbrado.Texto = "0"
        '
        'lblRestoTimbrado
        '
        Me.lblRestoTimbrado.AutoSize = True
        Me.lblRestoTimbrado.Location = New System.Drawing.Point(239, 39)
        Me.lblRestoTimbrado.Name = "lblRestoTimbrado"
        Me.lblRestoTimbrado.Size = New System.Drawing.Size(138, 13)
        Me.lblRestoTimbrado.TabIndex = 15
        Me.lblRestoTimbrado.Text = "Nro. restantes en Talonario:"
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(264, 15)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(28, 13)
        Me.lblCiudad.TabIndex = 2
        Me.lblCiudad.Text = "Ciu.:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(346, 15)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(32, 13)
        Me.lblSucursal.TabIndex = 4
        Me.lblSucursal.Text = "Suc.:"
        '
        'txtVencimientoTimbrado
        '
        Me.txtVencimientoTimbrado.AñoFecha = 0
        Me.txtVencimientoTimbrado.Color = System.Drawing.Color.Beige
        Me.txtVencimientoTimbrado.Fecha = New Date(2013, 6, 7, 8, 24, 40, 700)
        Me.txtVencimientoTimbrado.Location = New System.Drawing.Point(165, 35)
        Me.txtVencimientoTimbrado.MesFecha = 0
        Me.txtVencimientoTimbrado.Name = "txtVencimientoTimbrado"
        Me.txtVencimientoTimbrado.PermitirNulo = False
        Me.txtVencimientoTimbrado.Size = New System.Drawing.Size(74, 20)
        Me.txtVencimientoTimbrado.SoloLectura = True
        Me.txtVencimientoTimbrado.TabIndex = 14
        Me.txtVencimientoTimbrado.TabStop = False
        '
        'lblVenimientoTimbrado
        '
        Me.lblVenimientoTimbrado.AutoSize = True
        Me.lblVenimientoTimbrado.Location = New System.Drawing.Point(98, 39)
        Me.lblVenimientoTimbrado.Name = "lblVenimientoTimbrado"
        Me.lblVenimientoTimbrado.Size = New System.Drawing.Size(68, 13)
        Me.lblVenimientoTimbrado.TabIndex = 13
        Me.lblVenimientoTimbrado.Text = "Vencimiento:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(493, 15)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(55, 13)
        Me.lblComprobante.TabIndex = 6
        Me.lblComprobante.Text = "Comprob.:"
        '
        'txtNroComprobante
        '
        Me.txtNroComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroComprobante.Color = System.Drawing.Color.Empty
        Me.txtNroComprobante.Indicaciones = Nothing
        Me.txtNroComprobante.Location = New System.Drawing.Point(661, 11)
        Me.txtNroComprobante.Multilinea = False
        Me.txtNroComprobante.Name = "txtNroComprobante"
        Me.txtNroComprobante.Size = New System.Drawing.Size(68, 21)
        Me.txtNroComprobante.SoloLectura = False
        Me.txtNroComprobante.TabIndex = 10
        Me.txtNroComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNroComprobante.Texto = ""
        '
        'txtTalonario
        '
        Me.txtTalonario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTalonario.Color = System.Drawing.Color.Beige
        Me.txtTalonario.Indicaciones = Nothing
        Me.txtTalonario.Location = New System.Drawing.Point(68, 35)
        Me.txtTalonario.Multilinea = False
        Me.txtTalonario.Name = "txtTalonario"
        Me.txtTalonario.Size = New System.Drawing.Size(30, 21)
        Me.txtTalonario.SoloLectura = True
        Me.txtTalonario.TabIndex = 12
        Me.txtTalonario.TabStop = False
        Me.txtTalonario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTalonario.Texto = ""
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(550, 11)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(55, 21)
        Me.cbxTipoComprobante.SoloLectura = True
        Me.cbxTipoComprobante.TabIndex = 7
        Me.cbxTipoComprobante.TabStop = False
        Me.cbxTipoComprobante.Texto = ""
        '
        'lblTalonario
        '
        Me.lblTalonario.AutoSize = True
        Me.lblTalonario.Location = New System.Drawing.Point(9, 39)
        Me.lblTalonario.Name = "lblTalonario"
        Me.lblTalonario.Size = New System.Drawing.Size(54, 13)
        Me.lblTalonario.TabIndex = 11
        Me.lblTalonario.Text = "Talonario:"
        '
        'txtReferenciaSucursal
        '
        Me.txtReferenciaSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferenciaSucursal.Color = System.Drawing.Color.Empty
        Me.txtReferenciaSucursal.Indicaciones = Nothing
        Me.txtReferenciaSucursal.Location = New System.Drawing.Point(605, 11)
        Me.txtReferenciaSucursal.Multilinea = False
        Me.txtReferenciaSucursal.Name = "txtReferenciaSucursal"
        Me.txtReferenciaSucursal.Size = New System.Drawing.Size(28, 21)
        Me.txtReferenciaSucursal.SoloLectura = True
        Me.txtReferenciaSucursal.TabIndex = 8
        Me.txtReferenciaSucursal.TabStop = False
        Me.txtReferenciaSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferenciaSucursal.Texto = ""
        '
        'txtReferenciaTerminal
        '
        Me.txtReferenciaTerminal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferenciaTerminal.Color = System.Drawing.Color.Empty
        Me.txtReferenciaTerminal.Indicaciones = Nothing
        Me.txtReferenciaTerminal.Location = New System.Drawing.Point(633, 11)
        Me.txtReferenciaTerminal.Multilinea = False
        Me.txtReferenciaTerminal.Name = "txtReferenciaTerminal"
        Me.txtReferenciaTerminal.Size = New System.Drawing.Size(28, 21)
        Me.txtReferenciaTerminal.SoloLectura = True
        Me.txtReferenciaTerminal.TabIndex = 9
        Me.txtReferenciaTerminal.TabStop = False
        Me.txtReferenciaTerminal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferenciaTerminal.Texto = ""
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.Panel1, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.gbxComprobante, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.gbxCabecera, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.dgw, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel2, 0, 3)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 5
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 128.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 58.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(734, 430)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnAnular)
        Me.Panel1.Controls.Add(Me.btnEliminar)
        Me.Panel1.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Panel1.Controls.Add(Me.btnImprimir)
        Me.Panel1.Controls.Add(Me.btnNuevo)
        Me.Panel1.Controls.Add(Me.btnSalir)
        Me.Panel1.Controls.Add(Me.btnGuardar)
        Me.Panel1.Controls.Add(Me.btnCancelar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 393)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(728, 34)
        Me.Panel1.TabIndex = 4
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(204, 4)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 2
        Me.btnAnular.TabStop = False
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(204, 4)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        Me.btnEliminar.Visible = False
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(3, 4)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(120, 23)
        Me.btnBusquedaAvanzada.TabIndex = 0
        Me.btnBusquedaAvanzada.TabStop = False
        Me.btnBusquedaAvanzada.Text = "&Busqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(126, 4)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 1
        Me.btnImprimir.TabStop = False
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(376, 5)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 3
        Me.btnNuevo.TabStop = False
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(638, 5)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 6
        Me.btnSalir.TabStop = False
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(463, 5)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 4
        Me.btnGuardar.TabStop = False
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(544, 5)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 5
        Me.btnCancelar.TabStop = False
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colComprobante, Me.colFecha, Me.colGravado, Me.colIVA, Me.colTotal, Me.colPorcentajeRetencion, Me.colRetencionIVA, Me.colPorcentajeRenta, Me.colRenta})
        Me.dgw.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgw.Location = New System.Drawing.Point(3, 200)
        Me.dgw.Name = "dgw"
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(728, 129)
        Me.dgw.TabIndex = 2
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.Visible = False
        '
        'colComprobante
        '
        Me.colComprobante.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colComprobante.DefaultCellStyle = DataGridViewCellStyle2
        Me.colComprobante.HeaderText = "Tipo y Nro. Comprobante"
        Me.colComprobante.Name = "colComprobante"
        '
        'colFecha
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.NullValue = "---"
        Me.colFecha.DefaultCellStyle = DataGridViewCellStyle3
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.Width = 78
        '
        'colGravado
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.NullValue = "---"
        Me.colGravado.DefaultCellStyle = DataGridViewCellStyle4
        Me.colGravado.HeaderText = "Gravado"
        Me.colGravado.Name = "colGravado"
        Me.colGravado.Width = 90
        '
        'colIVA
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = "0"
        Me.colIVA.DefaultCellStyle = DataGridViewCellStyle5
        Me.colIVA.HeaderText = "IVA"
        Me.colIVA.Name = "colIVA"
        Me.colIVA.Width = 90
        '
        'colTotal
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N0"
        DataGridViewCellStyle6.NullValue = "0"
        Me.colTotal.DefaultCellStyle = DataGridViewCellStyle6
        Me.colTotal.HeaderText = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.Width = 90
        '
        'colPorcentajeRetencion
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colPorcentajeRetencion.DefaultCellStyle = DataGridViewCellStyle7
        Me.colPorcentajeRetencion.HeaderText = "Porc. Ret%"
        Me.colPorcentajeRetencion.Name = "colPorcentajeRetencion"
        Me.colPorcentajeRetencion.Width = 40
        '
        'colRetencionIVA
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colRetencionIVA.DefaultCellStyle = DataGridViewCellStyle8
        Me.colRetencionIVA.HeaderText = "Retencion IVA"
        Me.colRetencionIVA.Name = "colRetencionIVA"
        Me.colRetencionIVA.Width = 90
        '
        'colPorcentajeRenta
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colPorcentajeRenta.DefaultCellStyle = DataGridViewCellStyle9
        Me.colPorcentajeRenta.HeaderText = "Porc. Renta%"
        Me.colPorcentajeRenta.Name = "colPorcentajeRenta"
        Me.colPorcentajeRenta.Width = 50
        '
        'colRenta
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colRenta.DefaultCellStyle = DataGridViewCellStyle10
        Me.colRenta.HeaderText = "RENTA"
        Me.colRenta.Name = "colRenta"
        Me.colRenta.Width = 90
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txtTotalImpuesto)
        Me.Panel2.Controls.Add(Me.flpAnuladoPor)
        Me.Panel2.Controls.Add(Me.flpRegistradoPor)
        Me.Panel2.Controls.Add(Me.txtTotalRetencion)
        Me.Panel2.Controls.Add(Me.txtTotalRetencionIVA)
        Me.Panel2.Controls.Add(Me.txtTotalretencionRenta)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.lblTotalRetencionIVA)
        Me.Panel2.Controls.Add(Me.lblTotalImpuesto)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 335)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(728, 52)
        Me.Panel2.TabIndex = 3
        '
        'txtTotalImpuesto
        '
        Me.txtTotalImpuesto.Color = System.Drawing.Color.Empty
        Me.txtTotalImpuesto.Decimales = True
        Me.txtTotalImpuesto.Indicaciones = Nothing
        Me.txtTotalImpuesto.Location = New System.Drawing.Point(268, 5)
        Me.txtTotalImpuesto.Name = "txtTotalImpuesto"
        Me.txtTotalImpuesto.Size = New System.Drawing.Size(76, 20)
        Me.txtTotalImpuesto.SoloLectura = True
        Me.txtTotalImpuesto.TabIndex = 1
        Me.txtTotalImpuesto.TabStop = False
        Me.txtTotalImpuesto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalImpuesto.Texto = "0"
        '
        'txtTotalRetencion
        '
        Me.txtTotalRetencion.Color = System.Drawing.Color.Empty
        Me.txtTotalRetencion.Decimales = True
        Me.txtTotalRetencion.Indicaciones = Nothing
        Me.txtTotalRetencion.Location = New System.Drawing.Point(644, 29)
        Me.txtTotalRetencion.Name = "txtTotalRetencion"
        Me.txtTotalRetencion.Size = New System.Drawing.Size(76, 20)
        Me.txtTotalRetencion.SoloLectura = True
        Me.txtTotalRetencion.TabIndex = 9
        Me.txtTotalRetencion.TabStop = False
        Me.txtTotalRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalRetencion.Texto = "0"
        '
        'txtTotalRetencionIVA
        '
        Me.txtTotalRetencionIVA.Color = System.Drawing.Color.Empty
        Me.txtTotalRetencionIVA.Decimales = True
        Me.txtTotalRetencionIVA.Indicaciones = Nothing
        Me.txtTotalRetencionIVA.Location = New System.Drawing.Point(450, 5)
        Me.txtTotalRetencionIVA.Name = "txtTotalRetencionIVA"
        Me.txtTotalRetencionIVA.Size = New System.Drawing.Size(76, 20)
        Me.txtTotalRetencionIVA.SoloLectura = True
        Me.txtTotalRetencionIVA.TabIndex = 3
        Me.txtTotalRetencionIVA.TabStop = False
        Me.txtTotalRetencionIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalRetencionIVA.Texto = "0"
        '
        'txtTotalretencionRenta
        '
        Me.txtTotalretencionRenta.Color = System.Drawing.Color.Empty
        Me.txtTotalretencionRenta.Decimales = True
        Me.txtTotalretencionRenta.Indicaciones = Nothing
        Me.txtTotalretencionRenta.Location = New System.Drawing.Point(644, 5)
        Me.txtTotalretencionRenta.Name = "txtTotalretencionRenta"
        Me.txtTotalretencionRenta.Size = New System.Drawing.Size(76, 20)
        Me.txtTotalretencionRenta.SoloLectura = True
        Me.txtTotalretencionRenta.TabIndex = 5
        Me.txtTotalretencionRenta.TabStop = False
        Me.txtTotalretencionRenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalretencionRenta.Texto = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(608, 31)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(34, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Total:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(526, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(118, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Total Retencion Renta:"
        '
        'lblTotalRetencionIVA
        '
        Me.lblTotalRetencionIVA.AutoSize = True
        Me.lblTotalRetencionIVA.Location = New System.Drawing.Point(344, 9)
        Me.lblTotalRetencionIVA.Name = "lblTotalRetencionIVA"
        Me.lblTotalRetencionIVA.Size = New System.Drawing.Size(106, 13)
        Me.lblTotalRetencionIVA.TabIndex = 2
        Me.lblTotalRetencionIVA.Text = "Total Retencion IVA:"
        '
        'lblTotalImpuesto
        '
        Me.lblTotalImpuesto.AutoSize = True
        Me.lblTotalImpuesto.Location = New System.Drawing.Point(188, 9)
        Me.lblTotalImpuesto.Name = "lblTotalImpuesto"
        Me.lblTotalImpuesto.Size = New System.Drawing.Size(80, 13)
        Me.lblTotalImpuesto.TabIndex = 0
        Me.lblTotalImpuesto.Text = "Total Impuesto:"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(0, 17)
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado, Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 430)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(734, 22)
        Me.StatusStrip1.TabIndex = 0
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'frmRetencionIVA
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(734, 452)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmRetencionIVA"
        Me.Text = "frmRetencionIVA"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.gbxComprobante.ResumeLayout(False)
        Me.gbxComprobante.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents gbxComprobante As System.Windows.Forms.GroupBox
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents txtRestoTimbrado As ERP.ocxTXTNumeric
    Friend WithEvents lblRestoTimbrado As System.Windows.Forms.Label
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents txtVencimientoTimbrado As ERP.ocxTXTDate
    Friend WithEvents lblVenimientoTimbrado As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents txtNroComprobante As ERP.ocxTXTString
    Friend WithEvents txtTalonario As ERP.ocxTXTString
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents lblTalonario As System.Windows.Forms.Label
    Friend WithEvents txtReferenciaSucursal As ERP.ocxTXTString
    Friend WithEvents txtReferenciaTerminal As ERP.ocxTXTString
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents txtOP As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents btnListar As System.Windows.Forms.Button
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtTotalRetencion As ERP.ocxTXTNumeric
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblTotalRetencionIVA As System.Windows.Forms.Label
    Friend WithEvents txtTotalRetencionIVA As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalretencionRenta As ERP.ocxTXTNumeric
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As ERP.ocxTXTString
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtRUC As ERP.ocxTXTString
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cbxProveedor As ERP.ocxCBX
    Friend WithEvents btnCargar As System.Windows.Forms.Button
    Friend WithEvents chkOP As System.Windows.Forms.CheckBox
    Friend WithEvents txtIDTimbrado As ERP.ocxTXTString
    Friend WithEvents txtTimbrado As ERP.ocxTXTString
    Friend WithEvents lklTimbrado As System.Windows.Forms.LinkLabel
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents txtTotalImpuesto As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalImpuesto As System.Windows.Forms.Label
    Friend WithEvents colIDTransaccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colGravado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIVA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPorcentajeRetencion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRetencionIVA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPorcentajeRenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRenta As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

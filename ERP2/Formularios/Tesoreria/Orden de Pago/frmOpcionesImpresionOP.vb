﻿Imports ERP.Reporte
Public Class frmOpcionesImpresionOP

    Public Property MontoLetras As String
    Public Property IDTransaccion As Integer

    Dim CReporteOrdenPago As New CReporteOrdenPago

    Sub Imprimir()

        Dim frm As New frmReporte

        If chkImprimirObservacion.Checked Then
            If chkSeccionFacturasSeparadas.Checked Then
                CReporteOrdenPago.ImprimirOrdenPagoObservacion(frm, IDTransaccion, vgUsuarioIdentificador, MontoLetras)
            Else
                CReporteOrdenPago.ImprimirOrdenPagoObservacion(frm, IDTransaccion, vgUsuarioIdentificador, MontoLetras, True)
            End If

        Else
            If chkSeccionFacturasSeparadas.Checked Then
                CReporteOrdenPago.ImprimirOrdenPago(frm, IDTransaccion, vgUsuarioIdentificador, MontoLetras)
            Else
                CReporteOrdenPago.ImprimirOrdenPago(frm, IDTransaccion, vgUsuarioIdentificador, MontoLetras, True)
            End If
        End If


    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Imprimir()
        Me.Close()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmOpcionesImpresionOP_Activate()
        Me.Refresh()
    End Sub
End Class
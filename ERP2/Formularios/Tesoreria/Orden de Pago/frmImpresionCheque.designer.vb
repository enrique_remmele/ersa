﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImpresionCheque
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnDetalle = New System.Windows.Forms.Button()
        Me.lblSeleccionOP = New System.Windows.Forms.Label()
        Me.gbxSeleccion = New System.Windows.Forms.GroupBox()
        Me.rdbFechaCheque = New System.Windows.Forms.RadioButton()
        Me.rdbNroOrdenPago = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblFechaCheque = New System.Windows.Forms.Label()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.lblFormulario = New System.Windows.Forms.Label()
        Me.lblPrimerCheque = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblCiudad = New System.Windows.Forms.Label()
        Me.btnCargar = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxImpresora = New ERP.ocxCBX()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtPrimerCheque = New ERP.ocxTXTNumeric()
        Me.cbxFormulario = New ERP.ocxCBX()
        Me.txtBanco = New ERP.ocxTXTString()
        Me.cbxCuenta = New ERP.ocxCBX()
        Me.txtFechaHasta = New ERP.ocxTXTDate()
        Me.txtFechaDesde = New ERP.ocxTXTDate()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtOPFinal = New ERP.ocxTXTNumeric()
        Me.txtOPInicial = New ERP.ocxTXTNumeric()
        Me.lblConfiguracion = New System.Windows.Forms.Label()
        Me.cbxConfiguracion = New ERP.ocxCBX()
        Me.chkContinuo = New System.Windows.Forms.CheckBox()
        Me.gbxSeleccion.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(251, 324)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 27
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(170, 324)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 26
        Me.btnImprimir.Text = "&Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnDetalle
        '
        Me.btnDetalle.Location = New System.Drawing.Point(89, 324)
        Me.btnDetalle.Name = "btnDetalle"
        Me.btnDetalle.Size = New System.Drawing.Size(75, 23)
        Me.btnDetalle.TabIndex = 25
        Me.btnDetalle.Text = "&Detalle"
        Me.btnDetalle.UseVisualStyleBackColor = True
        '
        'lblSeleccionOP
        '
        Me.lblSeleccionOP.AutoSize = True
        Me.lblSeleccionOP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSeleccionOP.Location = New System.Drawing.Point(12, 26)
        Me.lblSeleccionOP.Name = "lblSeleccionOP"
        Me.lblSeleccionOP.Size = New System.Drawing.Size(94, 15)
        Me.lblSeleccionOP.TabIndex = 0
        Me.lblSeleccionOP.Text = "Selección de OPs"
        '
        'gbxSeleccion
        '
        Me.gbxSeleccion.Controls.Add(Me.rdbFechaCheque)
        Me.gbxSeleccion.Controls.Add(Me.rdbNroOrdenPago)
        Me.gbxSeleccion.Location = New System.Drawing.Point(111, 8)
        Me.gbxSeleccion.Name = "gbxSeleccion"
        Me.gbxSeleccion.Size = New System.Drawing.Size(135, 63)
        Me.gbxSeleccion.TabIndex = 1
        Me.gbxSeleccion.TabStop = False
        '
        'rdbFechaCheque
        '
        Me.rdbFechaCheque.AutoSize = True
        Me.rdbFechaCheque.Location = New System.Drawing.Point(6, 40)
        Me.rdbFechaCheque.Name = "rdbFechaCheque"
        Me.rdbFechaCheque.Size = New System.Drawing.Size(112, 17)
        Me.rdbFechaCheque.TabIndex = 1
        Me.rdbFechaCheque.TabStop = True
        Me.rdbFechaCheque.Text = "Fecha del Cheque"
        Me.rdbFechaCheque.UseVisualStyleBackColor = True
        '
        'rdbNroOrdenPago
        '
        Me.rdbNroOrdenPago.AutoSize = True
        Me.rdbNroOrdenPago.Location = New System.Drawing.Point(6, 17)
        Me.rdbNroOrdenPago.Name = "rdbNroOrdenPago"
        Me.rdbNroOrdenPago.Size = New System.Drawing.Size(117, 17)
        Me.rdbNroOrdenPago.TabIndex = 0
        Me.rdbNroOrdenPago.TabStop = True
        Me.rdbNroOrdenPago.Text = "Nro Orden de Pago"
        Me.rdbNroOrdenPago.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(19, 106)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "&OP inicial / final:"
        '
        'lblFechaCheque
        '
        Me.lblFechaCheque.AutoSize = True
        Me.lblFechaCheque.Location = New System.Drawing.Point(23, 136)
        Me.lblFechaCheque.Name = "lblFechaCheque"
        Me.lblFechaCheque.Size = New System.Drawing.Size(80, 13)
        Me.lblFechaCheque.TabIndex = 9
        Me.lblFechaCheque.Text = "Fecha Cheque:"
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(59, 162)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 12
        Me.lblCuenta.Text = "&Cuenta:"
        '
        'lblFormulario
        '
        Me.lblFormulario.AutoSize = True
        Me.lblFormulario.Location = New System.Drawing.Point(31, 188)
        Me.lblFormulario.Name = "lblFormulario"
        Me.lblFormulario.Size = New System.Drawing.Size(71, 13)
        Me.lblFormulario.TabIndex = 15
        Me.lblFormulario.Text = "Tipo Cheque:"
        '
        'lblPrimerCheque
        '
        Me.lblPrimerCheque.AutoSize = True
        Me.lblPrimerCheque.Location = New System.Drawing.Point(23, 215)
        Me.lblPrimerCheque.Name = "lblPrimerCheque"
        Me.lblPrimerCheque.Size = New System.Drawing.Size(79, 13)
        Me.lblPrimerCheque.TabIndex = 17
        Me.lblPrimerCheque.Text = "Primer Cheque:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(214, 81)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(29, 13)
        Me.lblSucursal.TabIndex = 4
        Me.lblSucursal.Text = "Suc:"
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(60, 81)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(43, 13)
        Me.lblCiudad.TabIndex = 2
        Me.lblCiudad.Text = "Ciudad:"
        '
        'btnCargar
        '
        Me.btnCargar.Location = New System.Drawing.Point(8, 324)
        Me.btnCargar.Name = "btnCargar"
        Me.btnCargar.Size = New System.Drawing.Size(75, 23)
        Me.btnCargar.TabIndex = 24
        Me.btnCargar.Text = "&Cargar"
        Me.btnCargar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 364)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(348, 22)
        Me.StatusStrip1.TabIndex = 28
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(46, 243)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Impresora:"
        '
        'cbxImpresora
        '
        Me.cbxImpresora.CampoWhere = Nothing
        Me.cbxImpresora.CargarUnaSolaVez = False
        Me.cbxImpresora.DataDisplayMember = Nothing
        Me.cbxImpresora.DataFilter = Nothing
        Me.cbxImpresora.DataOrderBy = Nothing
        Me.cbxImpresora.DataSource = Nothing
        Me.cbxImpresora.DataValueMember = Nothing
        Me.cbxImpresora.FormABM = Nothing
        Me.cbxImpresora.Indicaciones = Nothing
        Me.cbxImpresora.Location = New System.Drawing.Point(110, 239)
        Me.cbxImpresora.Name = "cbxImpresora"
        Me.cbxImpresora.SeleccionObligatoria = True
        Me.cbxImpresora.Size = New System.Drawing.Size(213, 21)
        Me.cbxImpresora.SoloLectura = False
        Me.cbxImpresora.TabIndex = 20
        Me.cbxImpresora.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(245, 77)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(78, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 5
        Me.cbxSucursal.Texto = ""
        '
        'txtPrimerCheque
        '
        Me.txtPrimerCheque.Color = System.Drawing.Color.Empty
        Me.txtPrimerCheque.Decimales = True
        Me.txtPrimerCheque.Indicaciones = Nothing
        Me.txtPrimerCheque.Location = New System.Drawing.Point(110, 212)
        Me.txtPrimerCheque.Name = "txtPrimerCheque"
        Me.txtPrimerCheque.Size = New System.Drawing.Size(103, 21)
        Me.txtPrimerCheque.SoloLectura = False
        Me.txtPrimerCheque.TabIndex = 18
        Me.txtPrimerCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrimerCheque.Texto = "0"
        '
        'cbxFormulario
        '
        Me.cbxFormulario.CampoWhere = Nothing
        Me.cbxFormulario.CargarUnaSolaVez = False
        Me.cbxFormulario.DataDisplayMember = Nothing
        Me.cbxFormulario.DataFilter = Nothing
        Me.cbxFormulario.DataOrderBy = Nothing
        Me.cbxFormulario.DataSource = Nothing
        Me.cbxFormulario.DataValueMember = Nothing
        Me.cbxFormulario.FormABM = Nothing
        Me.cbxFormulario.Indicaciones = Nothing
        Me.cbxFormulario.Location = New System.Drawing.Point(110, 185)
        Me.cbxFormulario.Name = "cbxFormulario"
        Me.cbxFormulario.SeleccionObligatoria = False
        Me.cbxFormulario.Size = New System.Drawing.Size(216, 21)
        Me.cbxFormulario.SoloLectura = False
        Me.cbxFormulario.TabIndex = 16
        Me.cbxFormulario.Texto = ""
        '
        'txtBanco
        '
        Me.txtBanco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBanco.Color = System.Drawing.Color.Empty
        Me.txtBanco.Indicaciones = Nothing
        Me.txtBanco.Location = New System.Drawing.Point(246, 158)
        Me.txtBanco.Multilinea = False
        Me.txtBanco.Name = "txtBanco"
        Me.txtBanco.Size = New System.Drawing.Size(80, 21)
        Me.txtBanco.SoloLectura = False
        Me.txtBanco.TabIndex = 14
        Me.txtBanco.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBanco.Texto = ""
        '
        'cbxCuenta
        '
        Me.cbxCuenta.CampoWhere = Nothing
        Me.cbxCuenta.CargarUnaSolaVez = False
        Me.cbxCuenta.DataDisplayMember = Nothing
        Me.cbxCuenta.DataFilter = Nothing
        Me.cbxCuenta.DataOrderBy = Nothing
        Me.cbxCuenta.DataSource = Nothing
        Me.cbxCuenta.DataValueMember = Nothing
        Me.cbxCuenta.FormABM = Nothing
        Me.cbxCuenta.Indicaciones = Nothing
        Me.cbxCuenta.Location = New System.Drawing.Point(110, 158)
        Me.cbxCuenta.Name = "cbxCuenta"
        Me.cbxCuenta.SeleccionObligatoria = False
        Me.cbxCuenta.Size = New System.Drawing.Size(136, 21)
        Me.cbxCuenta.SoloLectura = False
        Me.cbxCuenta.TabIndex = 13
        Me.cbxCuenta.Texto = ""
        '
        'txtFechaHasta
        '
        Me.txtFechaHasta.Color = System.Drawing.Color.Empty
        Me.txtFechaHasta.Fecha = New Date(2013, 3, 25, 9, 33, 17, 281)
        Me.txtFechaHasta.Location = New System.Drawing.Point(223, 132)
        Me.txtFechaHasta.Name = "txtFechaHasta"
        Me.txtFechaHasta.PermitirNulo = False
        Me.txtFechaHasta.Size = New System.Drawing.Size(103, 20)
        Me.txtFechaHasta.SoloLectura = False
        Me.txtFechaHasta.TabIndex = 11
        '
        'txtFechaDesde
        '
        Me.txtFechaDesde.Color = System.Drawing.Color.Empty
        Me.txtFechaDesde.Fecha = New Date(2013, 3, 25, 9, 33, 17, 281)
        Me.txtFechaDesde.Location = New System.Drawing.Point(111, 132)
        Me.txtFechaDesde.Name = "txtFechaDesde"
        Me.txtFechaDesde.PermitirNulo = False
        Me.txtFechaDesde.Size = New System.Drawing.Size(103, 20)
        Me.txtFechaDesde.SoloLectura = False
        Me.txtFechaDesde.TabIndex = 10
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(111, 77)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(102, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 3
        Me.cbxCiudad.Texto = ""
        '
        'txtOPFinal
        '
        Me.txtOPFinal.Color = System.Drawing.Color.Empty
        Me.txtOPFinal.Decimales = True
        Me.txtOPFinal.Indicaciones = Nothing
        Me.txtOPFinal.Location = New System.Drawing.Point(220, 104)
        Me.txtOPFinal.Name = "txtOPFinal"
        Me.txtOPFinal.Size = New System.Drawing.Size(103, 22)
        Me.txtOPFinal.SoloLectura = False
        Me.txtOPFinal.TabIndex = 8
        Me.txtOPFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOPFinal.Texto = "0"
        '
        'txtOPInicial
        '
        Me.txtOPInicial.Color = System.Drawing.Color.Empty
        Me.txtOPInicial.Decimales = True
        Me.txtOPInicial.Indicaciones = Nothing
        Me.txtOPInicial.Location = New System.Drawing.Point(111, 104)
        Me.txtOPInicial.Name = "txtOPInicial"
        Me.txtOPInicial.Size = New System.Drawing.Size(103, 22)
        Me.txtOPInicial.SoloLectura = False
        Me.txtOPInicial.TabIndex = 7
        Me.txtOPInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOPInicial.Texto = "0"
        '
        'lblConfiguracion
        '
        Me.lblConfiguracion.AutoSize = True
        Me.lblConfiguracion.Location = New System.Drawing.Point(27, 270)
        Me.lblConfiguracion.Name = "lblConfiguracion"
        Me.lblConfiguracion.Size = New System.Drawing.Size(75, 13)
        Me.lblConfiguracion.TabIndex = 21
        Me.lblConfiguracion.Text = "Configuracion:"
        '
        'cbxConfiguracion
        '
        Me.cbxConfiguracion.CampoWhere = Nothing
        Me.cbxConfiguracion.CargarUnaSolaVez = False
        Me.cbxConfiguracion.DataDisplayMember = Nothing
        Me.cbxConfiguracion.DataFilter = Nothing
        Me.cbxConfiguracion.DataOrderBy = Nothing
        Me.cbxConfiguracion.DataSource = Nothing
        Me.cbxConfiguracion.DataValueMember = Nothing
        Me.cbxConfiguracion.FormABM = Nothing
        Me.cbxConfiguracion.Indicaciones = Nothing
        Me.cbxConfiguracion.Location = New System.Drawing.Point(110, 266)
        Me.cbxConfiguracion.Name = "cbxConfiguracion"
        Me.cbxConfiguracion.SeleccionObligatoria = True
        Me.cbxConfiguracion.Size = New System.Drawing.Size(213, 21)
        Me.cbxConfiguracion.SoloLectura = False
        Me.cbxConfiguracion.TabIndex = 22
        Me.cbxConfiguracion.Texto = ""
        '
        'chkContinuo
        '
        Me.chkContinuo.AutoSize = True
        Me.chkContinuo.Location = New System.Drawing.Point(110, 295)
        Me.chkContinuo.Name = "chkContinuo"
        Me.chkContinuo.Size = New System.Drawing.Size(178, 17)
        Me.chkContinuo.TabIndex = 23
        Me.chkContinuo.Text = "Impresion en formulario continuo"
        Me.chkContinuo.UseVisualStyleBackColor = True
        '
        'frmImpresionCheque
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(348, 386)
        Me.Controls.Add(Me.chkContinuo)
        Me.Controls.Add(Me.lblConfiguracion)
        Me.Controls.Add(Me.cbxConfiguracion)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbxImpresora)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnCargar)
        Me.Controls.Add(Me.lblCiudad)
        Me.Controls.Add(Me.cbxSucursal)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.txtPrimerCheque)
        Me.Controls.Add(Me.lblPrimerCheque)
        Me.Controls.Add(Me.cbxFormulario)
        Me.Controls.Add(Me.lblFormulario)
        Me.Controls.Add(Me.txtBanco)
        Me.Controls.Add(Me.cbxCuenta)
        Me.Controls.Add(Me.lblCuenta)
        Me.Controls.Add(Me.txtFechaHasta)
        Me.Controls.Add(Me.txtFechaDesde)
        Me.Controls.Add(Me.lblFechaCheque)
        Me.Controls.Add(Me.cbxCiudad)
        Me.Controls.Add(Me.txtOPFinal)
        Me.Controls.Add(Me.txtOPInicial)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.gbxSeleccion)
        Me.Controls.Add(Me.lblSeleccionOP)
        Me.Controls.Add(Me.btnDetalle)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnSalir)
        Me.Name = "frmImpresionCheque"
        Me.Text = "Impresión de Cheques"
        Me.gbxSeleccion.ResumeLayout(False)
        Me.gbxSeleccion.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnDetalle As System.Windows.Forms.Button
    Friend WithEvents lblSeleccionOP As System.Windows.Forms.Label
    Friend WithEvents gbxSeleccion As System.Windows.Forms.GroupBox
    Friend WithEvents rdbFechaCheque As System.Windows.Forms.RadioButton
    Friend WithEvents rdbNroOrdenPago As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtOPInicial As ERP.ocxTXTNumeric
    Friend WithEvents txtOPFinal As ERP.ocxTXTNumeric
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents lblFechaCheque As System.Windows.Forms.Label
    Friend WithEvents txtFechaDesde As ERP.ocxTXTDate
    Friend WithEvents txtFechaHasta As ERP.ocxTXTDate
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents cbxCuenta As ERP.ocxCBX
    Friend WithEvents txtBanco As ERP.ocxTXTString
    Friend WithEvents lblFormulario As System.Windows.Forms.Label
    Friend WithEvents cbxFormulario As ERP.ocxCBX
    Friend WithEvents lblPrimerCheque As System.Windows.Forms.Label
    Friend WithEvents txtPrimerCheque As ERP.ocxTXTNumeric
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents btnCargar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cbxImpresora As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblConfiguracion As System.Windows.Forms.Label
    Friend WithEvents cbxConfiguracion As ERP.ocxCBX
    Friend WithEvents chkContinuo As System.Windows.Forms.CheckBox
End Class

﻿Public Class frmCargarComprobantesSinOP

    'CLASE
    Dim CSistema As New CSistema

    'PRIPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Public Property IDMoneda As Integer
    Public Property IDProveedor As Integer
    Public Property IDTransaccion As Integer

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        Dim Decimales As Boolean = CSistema.MonedaDecimal(IDMoneda)
        txtTotal5.Decimales = Decimales
        txtIVA5.Decimales = Decimales
        txtTotal10.Decimales = Decimales
        txtIVA10.Decimales = Decimales
        txtTotalGravado.Decimales = Decimales
        txtRetencionIVA.Decimales = Decimales
        txtRenta.Decimales = Decimales

        ObtenerPorcentajeRetencion()

        ListarComprobantes()


    End Sub

    Sub ObtenerPorcentajeRetencion()

        txtPorcRetencion.txt.Text = CSistema.ExecuteScalar("Select Top 1 CompraPorcentajeRetencion  From Configuraciones")

    End Sub

    Sub ListarComprobantes()

        'Limpiar la grilla
        dgw.Rows.Clear()

        Dim Decimales As Boolean = CSistema.MonedaDecimal(IDMoneda)

        For Each oRow As DataRow In dt.Rows

            Dim Registro(10) As String
            Registro(0) = oRow("TipoyComprobante").ToString
            Registro(1) = oRow("Fecha").ToString
            Registro(2) = CSistema.FormatoMoneda(oRow("Total5").ToString, Decimales)
            Registro(3) = CSistema.FormatoMoneda(oRow("IVA5").ToString, Decimales)
            Registro(4) = CSistema.FormatoMoneda(oRow("Total10").ToString, Decimales)
            Registro(5) = CSistema.FormatoMoneda(oRow("IVA10").ToString, Decimales)
            Registro(6) = CSistema.FormatoMoneda(oRow("TotalGravado").ToString, Decimales)
            Registro(7) = oRow("PorcRetencion").ToString
            Registro(8) = CSistema.FormatoMoneda(oRow("RetencionIVA").ToString, Decimales)
            Registro(9) = oRow("PorcRenta").ToString
            Registro(10) = CSistema.FormatoMoneda(oRow("RENTA").ToString, Decimales)

            dgw.Rows.Add(Registro)

        Next


    End Sub

    Sub Limpiar()
        txtComprobante.txt.Clear()
        txtFecha.txt.Clear()
        txtTotal5.txt.Clear()
        txtIVA5.txt.Clear()
        txtIVA10.txt.Clear()
        txtTotal10.txt.Clear()
        txtRetencionIVA.txt.Clear()
        txtPorcRenta.txt.Clear()
        txtRenta.txt.Clear()
        txtTotalGravado.txt.Clear()
        txtComprobante.Focus()
    End Sub

    Sub CargarComprobantes()

        If ValidarDocumento() = False Then
            Exit Sub
        End If

        Dim oRow1 As DataRow = dt.NewRow
        oRow1("IDTransaccionEgreso") = IDTransaccion
        oRow1("TipoyComprobante") = txtComprobante.GetValue
        oRow1("Fecha") = txtFecha.GetValueString
        oRow1("IVA5") = txtIVA5.ObtenerValor
        oRow1("IVA10") = txtIVA10.ObtenerValor
        oRow1("Total5") = CDec(txtTotal5.ObtenerValor)
        oRow1("Total10") = CDec(txtTotal10.ObtenerValor)
        oRow1("Gravado5") = CDec(oRow1("Total5").ToString) - CDec(oRow1("IVA5").ToString)
        oRow1("Gravado10") = CDec(oRow1("Total10").ToString) - CDec(oRow1("IVA10").ToString)
        oRow1("TotalGravado") = CDec(oRow1("Gravado5").ToString) + CDec(oRow1("Gravado10").ToString)
        oRow1("TotalIVA") = CDec(oRow1("IVA5").ToString) + CDec(oRow1("IVA10").ToString)
        oRow1("Total") = CDec(oRow1("Total5").ToString) + CDec(oRow1("Total10").ToString)
        oRow1("PorcRetencion") = txtPorcRetencion.ObtenerValor
        oRow1("RetencionIVA") = txtRetencionIVA.txt.Text
        oRow1("PorcRenta") = txtPorcRenta.ObtenerValor
        oRow1("Renta") = txtRenta.ObtenerValor

        dt.Rows.Add(oRow1)
        dt.AcceptChanges()

        ListarComprobantes()

        Limpiar()

    End Sub

    Sub EliminarComprobante()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each item As DataGridViewRow In dgw.SelectedRows

            Dim Comprobante As String = item.Cells(0).Value

            For Each oRow As DataRow In dt.Select("TipoyComprobante='" & Comprobante & "'")
                dt.Rows.Remove(oRow)
                Exit For
            Next

        Next

        ListarComprobantes()
        Limpiar()

    End Sub

    Sub CalcularIVA10()

        Dim Importe As Decimal = 0
        Dim TotalDiscriminado As Decimal = 0
        Dim TotalImpuesto As Decimal = 0
        Dim RetencionIVA As Decimal = txtRetencionIVA.ObtenerValor
        Dim Decimales As Boolean = CSistema.MonedaDecimal(IDMoneda)
        Dim Redondear As Boolean = True

        If Decimales = True Then
            Redondear = False
        End If

        Importe = txtTotal10.ObtenerValor

        CSistema.CalcularIVA(1, Importe, Decimales, Redondear, TotalDiscriminado, TotalImpuesto)
        txtTotal10.txt.Text = CSistema.FormatoMoneda(Importe.ToString, Decimales)
        txtIVA10.txt.Text = CSistema.FormatoMoneda(TotalImpuesto.ToString, Decimales)
        txtRetencionIVA.txt.Text = txtPorcRetencion.ObtenerValor * txtIVA10.ObtenerValor / 100

        If txtTotal5.txt.Text = "" Then
            Exit Sub
        End If

        If txtTotal5.txt.Text > 0 Then
            txtRetencionIVA.txt.Text = RetencionIVA + CDbl(txtRetencionIVA.ObtenerValor)
        End If

    End Sub

    Sub CalcularIVA5()

        Dim Importe As Decimal = 0
        Dim TotalDiscriminado As Decimal = 0
        Dim TotalImpuesto As Decimal = 0
        Dim Decimales As Boolean = CSistema.MonedaDecimal(IDMoneda)
        Dim Redondear As Boolean = True

        If Decimales = True Then
            Redondear = False
        End If

        Importe = txtTotal5.ObtenerValor

        CSistema.CalcularIVA(2, Importe, Decimales, Redondear, TotalDiscriminado, TotalImpuesto)
        txtTotal5.txt.Text = CSistema.FormatoMoneda(Importe.ToString, Decimales)
        txtIVA5.txt.Text = CSistema.FormatoMoneda(TotalImpuesto.ToString, Decimales)
        txtRetencionIVA.txt.Text = txtPorcRetencion.ObtenerValor * txtIVA5.ObtenerValor / 100

    End Sub

    Sub FormatoMoneda()

        Dim Decimales As Boolean = CSistema.MonedaDecimal(IDMoneda)
        txtIVA10.Decimales = Decimales
        txtIVA5.Decimales = Decimales
        txtRenta.Decimales = Decimales
        txtRetencionIVA.Decimales = Decimales
        txtTotal10.Decimales = Decimales
        txtTotal5.Decimales = Decimales
        txtTotalGravado.Decimales = Decimales

    End Sub

    Function ValidarDocumento() As Boolean

        ValidarDocumento = False

        Dim SQL As String = "Exec SpRetencionObtenerEgresoExistente @IDProveedor=" & IDProveedor & ", @Comprobante='" & txtComprobante.GetValue & "', @Fecha='" & CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False) & "' "
        Dim vdt As DataTable = CSistema.ExecuteToDataTable(SQL)

        If vdt Is Nothing Then
            Exit Function
        End If

        If vdt.Rows.Count = 0 Then
            Exit Function
        End If

        Dim ResultadoRow As DataRow = vdt.Rows(0)

        If ResultadoRow("Procesado") = False Then
            MessageBox.Show(ResultadoRow("Mensaje").ToString, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Function
        End If

        IDTransaccion = ResultadoRow("IDTransaccion")

        ValidarDocumento = True

    End Function

    Private Sub frmCargarComprobantesSinOP_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmSeleccionChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub txtRenta_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtRenta.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarComprobantes()
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub lklEliminarVenta_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarVenta.LinkClicked
        EliminarComprobante()
    End Sub

    Private Sub txtTotal10_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtTotal10.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then

            If txtTotal5.ObtenerValor > 0 Then
                CalcularIVA5()
            End If

            CalcularIVA10()
            CargarComprobantes()

        End If
    End Sub

    Private Sub txtTotal5_Leave(sender As Object, e As System.EventArgs) Handles txtTotal5.Leave
        CalcularIVA5()
    End Sub

    Private Sub txtTotal5_TeclaPrecionada(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtTotal5.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CalcularIVA5()
        End If
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmCargarComprobantesSinOP_Activate()
        Me.Refresh()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmListaComprobantesRet
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colIdTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colTipoyComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIVA10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotalIVA10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIVA5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotalIVA5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRetencionIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNroRetencion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPorcRetencion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCancel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblContado = New System.Windows.Forms.Label()
        Me.txtCantidadRetencion = New ERP.ocxTXTNumeric()
        Me.txtTotalRetencion = New ERP.ocxTXTNumeric()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SeleccionarTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuitarTodaSeleccionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lnkSeleccionar = New System.Windows.Forms.LinkLabel()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgw)
        Me.GroupBox1.Location = New System.Drawing.Point(21, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1022, 275)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle9
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIdTransaccion, Me.colSel, Me.colTipoyComprobante, Me.colFecha, Me.colIVA10, Me.colTotalIVA10, Me.colIVA5, Me.colTotalIVA5, Me.colTotal, Me.colRetencionIVA, Me.colNroRetencion, Me.colPorcRetencion, Me.colCancel})
        Me.dgw.Location = New System.Drawing.Point(6, 9)
        Me.dgw.Name = "dgw"
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(1010, 260)
        Me.dgw.TabIndex = 1
        '
        'colIdTransaccion
        '
        Me.colIdTransaccion.HeaderText = "IdTransaccion"
        Me.colIdTransaccion.Name = "colIdTransaccion"
        Me.colIdTransaccion.Visible = False
        '
        'colSel
        '
        Me.colSel.FillWeight = 50.0!
        Me.colSel.HeaderText = "Sel"
        Me.colSel.Name = "colSel"
        Me.colSel.Width = 50
        '
        'colTipoyComprobante
        '
        Me.colTipoyComprobante.HeaderText = "TipoyComprobante"
        Me.colTipoyComprobante.Name = "colTipoyComprobante"
        '
        'colFecha
        '
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.Name = "colFecha"
        '
        'colIVA10
        '
        DataGridViewCellStyle10.Format = "N2"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.colIVA10.DefaultCellStyle = DataGridViewCellStyle10
        Me.colIVA10.HeaderText = "IVA10"
        Me.colIVA10.Name = "colIVA10"
        '
        'colTotalIVA10
        '
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.colTotalIVA10.DefaultCellStyle = DataGridViewCellStyle11
        Me.colTotalIVA10.HeaderText = "TotalIVA10"
        Me.colTotalIVA10.Name = "colTotalIVA10"
        '
        'colIVA5
        '
        DataGridViewCellStyle12.Format = "N2"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.colIVA5.DefaultCellStyle = DataGridViewCellStyle12
        Me.colIVA5.HeaderText = "IVA5"
        Me.colIVA5.Name = "colIVA5"
        '
        'colTotalIVA5
        '
        DataGridViewCellStyle13.Format = "N2"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.colTotalIVA5.DefaultCellStyle = DataGridViewCellStyle13
        Me.colTotalIVA5.HeaderText = "TotalIVA5"
        Me.colTotalIVA5.Name = "colTotalIVA5"
        '
        'colTotal
        '
        DataGridViewCellStyle14.Format = "N2"
        DataGridViewCellStyle14.NullValue = Nothing
        Me.colTotal.DefaultCellStyle = DataGridViewCellStyle14
        Me.colTotal.HeaderText = "Total"
        Me.colTotal.Name = "colTotal"
        '
        'colRetencionIVA
        '
        DataGridViewCellStyle15.Format = "N2"
        DataGridViewCellStyle15.NullValue = Nothing
        Me.colRetencionIVA.DefaultCellStyle = DataGridViewCellStyle15
        Me.colRetencionIVA.HeaderText = "RetencionIVA"
        Me.colRetencionIVA.Name = "colRetencionIVA"
        '
        'colNroRetencion
        '
        Me.colNroRetencion.HeaderText = "NroRetencion"
        Me.colNroRetencion.Name = "colNroRetencion"
        '
        'colPorcRetencion
        '
        DataGridViewCellStyle16.Format = "N2"
        DataGridViewCellStyle16.NullValue = Nothing
        Me.colPorcRetencion.DefaultCellStyle = DataGridViewCellStyle16
        Me.colPorcRetencion.HeaderText = "PorcRetencion"
        Me.colPorcRetencion.Name = "colPorcRetencion"
        '
        'colCancel
        '
        Me.colCancel.HeaderText = "Canc"
        Me.colCancel.Name = "colCancel"
        Me.colCancel.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colCancel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lnkSeleccionar)
        Me.GroupBox2.Controls.Add(Me.lblContado)
        Me.GroupBox2.Controls.Add(Me.txtCantidadRetencion)
        Me.GroupBox2.Controls.Add(Me.txtTotalRetencion)
        Me.GroupBox2.Location = New System.Drawing.Point(21, 286)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(557, 48)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        '
        'lblContado
        '
        Me.lblContado.Location = New System.Drawing.Point(292, 20)
        Me.lblContado.Name = "lblContado"
        Me.lblContado.Size = New System.Drawing.Size(119, 22)
        Me.lblContado.TabIndex = 18
        Me.lblContado.Text = "Total Retención:"
        Me.lblContado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCantidadRetencion
        '
        Me.txtCantidadRetencion.Color = System.Drawing.Color.Empty
        Me.txtCantidadRetencion.Decimales = True
        Me.txtCantidadRetencion.Indicaciones = Nothing
        Me.txtCantidadRetencion.Location = New System.Drawing.Point(506, 20)
        Me.txtCantidadRetencion.Name = "txtCantidadRetencion"
        Me.txtCantidadRetencion.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadRetencion.SoloLectura = False
        Me.txtCantidadRetencion.TabIndex = 17
        Me.txtCantidadRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadRetencion.Texto = "0"
        '
        'txtTotalRetencion
        '
        Me.txtTotalRetencion.Color = System.Drawing.Color.Empty
        Me.txtTotalRetencion.Decimales = True
        Me.txtTotalRetencion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalRetencion.Indicaciones = Nothing
        Me.txtTotalRetencion.Location = New System.Drawing.Point(417, 20)
        Me.txtTotalRetencion.Name = "txtTotalRetencion"
        Me.txtTotalRetencion.Size = New System.Drawing.Size(83, 22)
        Me.txtTotalRetencion.SoloLectura = True
        Me.txtTotalRetencion.TabIndex = 16
        Me.txtTotalRetencion.TabStop = False
        Me.txtTotalRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalRetencion.Texto = "0"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnCancelar)
        Me.GroupBox3.Controls.Add(Me.btnAceptar)
        Me.GroupBox3.Location = New System.Drawing.Point(584, 287)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(459, 48)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(291, 18)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 15
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(165, 18)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(120, 23)
        Me.btnAceptar.TabIndex = 14
        Me.btnAceptar.Text = "&Aceptar y Guardar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SeleccionarTodoToolStripMenuItem, Me.QuitarTodaSeleccionToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(187, 48)
        '
        'SeleccionarTodoToolStripMenuItem
        '
        Me.SeleccionarTodoToolStripMenuItem.Name = "SeleccionarTodoToolStripMenuItem"
        Me.SeleccionarTodoToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.SeleccionarTodoToolStripMenuItem.Text = "Seleccionar todo"
        '
        'QuitarTodaSeleccionToolStripMenuItem
        '
        Me.QuitarTodaSeleccionToolStripMenuItem.Name = "QuitarTodaSeleccionToolStripMenuItem"
        Me.QuitarTodaSeleccionToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.QuitarTodaSeleccionToolStripMenuItem.Text = "Quitar toda seleccion"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 341)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1055, 22)
        Me.StatusStrip1.TabIndex = 16
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'lnkSeleccionar
        '
        Me.lnkSeleccionar.AutoSize = True
        Me.lnkSeleccionar.Location = New System.Drawing.Point(20, 16)
        Me.lnkSeleccionar.Name = "lnkSeleccionar"
        Me.lnkSeleccionar.Size = New System.Drawing.Size(87, 13)
        Me.lnkSeleccionar.TabIndex = 19
        Me.lnkSeleccionar.TabStop = True
        Me.lnkSeleccionar.Text = "Seleccionar todo"
        '
        'frmListaComprobantesRet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1055, 363)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmListaComprobantesRet"
        Me.Text = "frmListaComprobantesRet"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents txtCantidadRetencion As ocxTXTNumeric
    Friend WithEvents txtTotalRetencion As ocxTXTNumeric
    Friend WithEvents lblContado As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnAceptar As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents SeleccionarTodoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuitarTodaSeleccionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents dgw As DataGridView
    Friend WithEvents colIdTransaccion As DataGridViewTextBoxColumn
    Friend WithEvents colSel As DataGridViewCheckBoxColumn
    Friend WithEvents colTipoyComprobante As DataGridViewTextBoxColumn
    Friend WithEvents colFecha As DataGridViewTextBoxColumn
    Friend WithEvents colIVA10 As DataGridViewTextBoxColumn
    Friend WithEvents colTotalIVA10 As DataGridViewTextBoxColumn
    Friend WithEvents colIVA5 As DataGridViewTextBoxColumn
    Friend WithEvents colTotalIVA5 As DataGridViewTextBoxColumn
    Friend WithEvents colTotal As DataGridViewTextBoxColumn
    Friend WithEvents colRetencionIVA As DataGridViewTextBoxColumn
    Friend WithEvents colNroRetencion As DataGridViewTextBoxColumn
    Friend WithEvents colPorcRetencion As DataGridViewTextBoxColumn
    Friend WithEvents colCancel As DataGridViewCheckBoxColumn
    Friend WithEvents lnkSeleccionar As LinkLabel
End Class

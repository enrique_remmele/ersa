﻿Public Class frmListaComprobantesRet

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim SelTodo As Boolean = False

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Public Property NroComprobante As Integer
    Public Property IDMoneda As Integer
    Public Property Decimales As Boolean

    'VARIABLES
    Dim vCargado As Boolean
    Dim Numero As Integer

    'FUNCIONES
    Sub Inicializar()

        vCargado = False

        'Propiedades
        Decimales = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & IDMoneda, "VMoneda")("Decimales"))
        Numero = NroComprobante
        'Funciones
        Listar()
        Cargar()

        'Foco
        dgw.Focus()

    End Sub

    Sub Listar()

        Dim TotalRetencion As Decimal = 0
        Dim CantidadRetencion As Integer = 0
        Dim NroAuxiliar As Integer = Numero
        vCargado = False


        For Each oRow As DataRow In dt.Rows
            Dim Registro(12) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Seleccionado").ToString
            Registro(2) = oRow("TipoyComprobante").ToString
            Registro(3) = oRow("Fecha").ToString
            Registro(4) = CSistema.FormatoMoneda(oRow("IVA10").ToString, Decimales)
            Registro(5) = CSistema.FormatoMoneda(oRow("TotalIVA10").ToString, Decimales)
            Registro(6) = CSistema.FormatoMoneda(oRow("IVA5").ToString, Decimales)
            Registro(7) = CSistema.FormatoMoneda(oRow("TotalIVA5").ToString, Decimales)
            Registro(8) = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
            Registro(9) = CSistema.FormatoMoneda(oRow("RetencionIVA").ToString, Decimales)
            Registro(10) = NroAuxiliar.ToString
            'Registro(10) = CSistema.FormatoMoneda(oRow("NroComprobante").ToString)
            Registro(11) = CSistema.FormatoMoneda(oRow("PorcRetencion").ToString)

            Registro(12) = oRow("Cancelar").ToString


            'Sumar el total de la deuda
            'TotalRetencion = TotalRetencion + CDec(oRow("RetencionIVA").ToString)


            dgw.Rows.Add(Registro)
            NroAuxiliar = NroAuxiliar + 1
        Next

        'txtTotalRetencion.SetValue(TotalRetencion)
        'txtCantidadRetencion.SetValue(dt.Rows.Count)

    End Sub

    Sub Seleccionar()

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells(0).Value

            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Seleccionado") = oRow.Cells(1).Value
                oRow1("IVA10") = oRow.Cells(4).Value
                oRow1("TotalIVA10") = oRow.Cells(5).Value
                oRow1("IVA5") = oRow.Cells(6).Value
                oRow1("TotalIVA5") = oRow.Cells(7).Value
                oRow1("Total") = oRow.Cells(8).Value
                oRow1("RetencionIVA") = oRow.Cells(9).Value
                oRow1("NroRetencion") = oRow.Cells(10).Value

                oRow1("Cancelar") = oRow.Cells(12).Value

            Next

        Next

        Me.Close()

    End Sub

    Sub Cargar()
        Dim NroAuxiliar As Integer = Numero

        For Each oRow As DataRow In dt.Rows
            For Each oRow1 As DataGridViewRow In dgw.Rows
                If oRow("IDTransaccion") = oRow1.Cells(0).Value Then
                    oRow1.Cells(1).Value = CBool(oRow("Seleccionado").ToString)
                    oRow1.Cells(4).Value = CSistema.FormatoMoneda(oRow("IVA10").ToString, Decimales)
                    oRow1.Cells(5).Value = CSistema.FormatoMoneda(oRow("TotalIVA10").ToString, Decimales)
                    oRow1.Cells(6).Value = CSistema.FormatoMoneda(oRow("IVA5").ToString, Decimales)
                    oRow1.Cells(7).Value = CSistema.FormatoMoneda(oRow("TotalIVA5").ToString, Decimales)
                    oRow1.Cells(8).Value = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
                    oRow1.Cells(9).Value = CSistema.FormatoMoneda(oRow("RetencionIVA").ToString, Decimales)
                    'Nuevo 21062021 -SM

                    oRow1.Cells(10).Value = NroAuxiliar.ToString
                    oRow1.Cells(12).Value = CBool(oRow("Cancelar").ToString)
                    NroAuxiliar = NroAuxiliar + 1
                End If
            Next
        Next

        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim TotalRetencion As Decimal = 0
        Dim CantidadRetencion As Integer = 0


        If dgw.Columns.Count = 0 Then
            Exit Sub
        End If

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Cells(1).Value = True Then
                TotalRetencion = TotalRetencion + oRow.Cells(9).Value
                'TotalRetencion = TotalRetencion + oRow.Cells(10).Value
                CantidadRetencion = CantidadRetencion + 1
            End If
        Next

        txtTotalRetencion.SetValue(CSistema.FormatoMoneda(TotalRetencion, Decimales))
        txtCantidadRetencion.SetValue(CantidadRetencion)

    End Sub

    Sub PintarCelda()

        'If dgw.Rows.Count = 0 Then
        '    Exit Sub
        'End If

        'For Each Row As DataGridViewRow In dgw.Rows
        '    If Row.Cells(1).Value = True Then
        '        Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
        '    Else
        '        Row.DefaultCellStyle.BackColor = Color.White
        '    End If
        'Next

        'dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub

    Private Sub frmListaComprobanteRet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        vCargado = True
    End Sub


    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

        If dgw.Columns(e.ColumnIndex).Name = "Total" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                'dgw.CurrentCell.Value = dgw.CurrentRow.Cells("Saldo").Value

            Else
                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("Total").Value
                dgw.CurrentRow.Cells("RetencionIVA").Value = 0
                dgw.CurrentRow.Cells("IVA10").Value = 0
                dgw.CurrentRow.Cells("IVA5").Value = 0
                Dim mensaje As String = "Debe Ingresar un importe para el IVA y la Retencion!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                MessageBox.Show("Debe Ingresar el Importe de Retencion", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            CalcularTotales()

        End If

        'si se modifica la Retencion
        If dgw.Columns(e.ColumnIndex).Name = "RetencionIVA" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe de Retencion IVA debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = 0

            Else
                ctrError.SetError(dgw, "")
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = ""
            End If

            CalcularTotales()

        End If



        'si se modifica el IVA
        If dgw.Columns(e.ColumnIndex).Name = "IVA10" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe del IVA debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = 0

            Else
                ctrError.SetError(dgw, "")
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = ""
            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Space Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(6).ReadOnly = False
                        oRow.Cells(8).ReadOnly = False
                        oRow.Cells(5).ReadOnly = False
                        oRow.Cells(9).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        'dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("Importe")

                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(6).ReadOnly = True
                        oRow.Cells(8).ReadOnly = True
                        oRow.Cells(5).ReadOnly = True
                        oRow.Cells(9).ReadOnly = True
                        'oRow.Cells("Importe").Value = oRow.Cells("Saldo").Value
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If

        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(6).ReadOnly = False
                        oRow.Cells(8).ReadOnly = False
                        oRow.Cells(5).ReadOnly = False
                        oRow.Cells(9).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(6).ReadOnly = True
                        oRow.Cells(8).ReadOnly = True
                        oRow.Cells(5).ReadOnly = True
                        oRow.Cells(9).ReadOnly = True
                        'oRow.Cells("Importe").Value = oRow.Cells("Saldo").Value
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For


                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells(6)
            End If

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If


    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

        If vCargado = False Then
            Exit Sub
        End If

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)

        If dgw.Rows(e.RowIndex).Cells(1).Value = False Then
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
        End If

        dgw.Rows(e.RowIndex).DefaultCellStyle.Font = f1

    End Sub

    Private Sub dgw_RowLeave(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs)

        If vCargado = False Then
            Exit Sub
        End If

        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        dgw.Rows(e.RowIndex).DefaultCellStyle.Font = f2

        If dgw.Rows(e.RowIndex).Cells(1).Value = False Then
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
        Else
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.PaleTurquoise
        End If

    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            'dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells("Importe")
        End If

    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeleccionarTodoToolStripMenuItem.Click

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuitarTodaSeleccionToolStripMenuItem.Click

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub dgw_CellContentClick_1(sender As Object, e As DataGridViewCellEventArgs) Handles dgw.CellContentClick
        ctrError.Clear()
        tsslEstado.Text = ""

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        If e.ColumnIndex = dgw.Columns(1).Index Then
            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        'oRow.Cells(10).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(6)

                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(6).ReadOnly = True
                        ' oRow.Cells(10).ReadOnly = True
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next
            CalcularTotales()
            dgw.Update()
        End If

    End Sub

    '28-05-2021 - SC - Actualiza datos
    Sub frmListaComprobantesRet_Activate()
        Me.Refresh()
    End Sub

    Private Sub lnkSeleccionar_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkSeleccionar.LinkClicked
        If SelTodo = False Then
            SeleccionarTodo()
        Else
            QuitarTodo()
        End If
    End Sub

    Sub SeleccionarTodo()
        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = True
        Next
        SelTodo = True
        lnkSeleccionar.Text = "Quitar Todo"
        PintarCelda()
        CalcularTotales()
    End Sub

    Sub QuitarTodo()
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = False
        Next
        SelTodo = False
        lnkSeleccionar.Text = "Seleccionar Todo"
        PintarCelda()
        CalcularTotales()
    End Sub
End Class
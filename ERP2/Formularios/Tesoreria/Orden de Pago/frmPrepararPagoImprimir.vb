﻿Public Class frmPrepararPagoImprimir

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CReporte As New Reporte.CReporteOrdenPago

    'PROPIEDADES
    Public Property dt As DataTable
    Public Property Desde As Date
    Public Property Hasta As Date
    Public Property Moneda As String

    'FUNCIONES
    Sub Inicializar()

        txtMoneda.SetValue(Moneda)

    End Sub

    Sub Listar()

        Dim frm As New frmReporte
        Dim Titulo As String = "Listado de Egresos Pendientes de Pago"
        Dim SubTitulo As String = "Todos"

        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Dim vdt As New DataTable

        'Excluir los de Fondo Fijo
        vdt = CData.FiltrarDataTable(dt, " FondoFijo='False' ")

        'Seleccionados
        If rdbDentroRango.Checked Then
            vdt = CData.FiltrarDataTable(vdt, "Pagar='True' And FechaVencimiento >= '" & Desde & "' And FechaVencimiento <= '" & Hasta & "' ")
            Titulo = "Listado de Egresos a Pagar"
            SubTitulo = "Vencimiento entre " & Desde.ToShortDateString & " - " & Hasta.ToShortDateString & " "
        End If

        'Seleccionados
        If rdbTodosPendientesDentroRango.Checked Then
            vdt = CData.FiltrarDataTable(vdt, " FechaVencimiento >= '" & Desde & "' And FechaVencimiento <= '" & Hasta & "' ")
            Titulo = "Listado de Egresos Pendientes de Pago"
            SubTitulo = "Vencimiento entre " & Desde.ToShortDateString & " - " & Hasta.ToShortDateString & " "
        End If

        'Moneda
        If chkIncluirMonedas.Valor = False Then
            vdt = CData.FiltrarDataTable(vdt, " Moneda = '" & Moneda & "' ")
            SubTitulo = SubTitulo & " / " & Moneda
        End If

        'Ordenar
        CData.OrderDataTable(vdt, "CuentaBancaria, Proveedor, FechaVencimiento")
        CReporte.ListadoEgresosAPagar(frm, Titulo, SubTitulo, vdt)

    End Sub

    Private Sub frmPrepararPagoImprimir_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Listar()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmPrepararPagoImprimir_Activate()
        Me.Refresh()
    End Sub

End Class
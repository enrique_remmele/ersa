﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntregaChequeOP
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tpCheque = New System.Windows.Forms.TabPage()
        Me.txtVencimiento = New ERP.ocxTXTDate()
        Me.txtOrden = New ERP.ocxTXTString()
        Me.lblCuentaBancaria = New System.Windows.Forms.Label()
        Me.lblOrden = New System.Windows.Forms.Label()
        Me.cbxCuentaBancaria = New ERP.ocxCBX()
        Me.txtFechaPagoCheque = New ERP.ocxTXTDate()
        Me.lblFechaCheque = New System.Windows.Forms.Label()
        Me.lblFechaPagoCheque = New System.Windows.Forms.Label()
        Me.txtFechaCheque = New ERP.ocxTXTDate()
        Me.txtMoneda = New ERP.ocxTXTString()
        Me.lblNroCheque = New System.Windows.Forms.Label()
        Me.txtBanco = New ERP.ocxTXTString()
        Me.txtNroCheque = New ERP.ocxTXTString()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.lblCotizacionCheque = New System.Windows.Forms.Label()
        Me.lblImporteMoneda = New System.Windows.Forms.Label()
        Me.lblDiferido = New System.Windows.Forms.Label()
        Me.txtImporteMoneda = New ERP.ocxTXTNumeric()
        Me.chkDiferido = New System.Windows.Forms.CheckBox()
        Me.lblVencimiento = New System.Windows.Forms.Label()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.cbxListar = New System.Windows.Forms.ComboBox()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkEntregado = New System.Windows.Forms.CheckBox()
        Me.txtRetiradoPor = New ERP.ocxTXTString()
        Me.txtRecibo = New ERP.ocxTXTString()
        Me.txtFechaEntrega = New ERP.ocxTXTDate()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.txtFechaHoy = New ERP.ocxTXTDate()
        Me.TabControl1.SuspendLayout()
        Me.tpCheque.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxCabecera.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(406, 73)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(74, 13)
        Me.lblImporte.TabIndex = 18
        Me.lblImporte.Text = "Importe Local:"
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(382, 307)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(70, 23)
        Me.btnModificar.TabIndex = 7
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons
        Me.TabControl1.Controls.Add(Me.tpCheque)
        Me.TabControl1.Location = New System.Drawing.Point(9, 100)
        Me.TabControl1.Multiline = True
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(673, 155)
        Me.TabControl1.TabIndex = 1
        '
        'tpCheque
        '
        Me.tpCheque.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.tpCheque.Controls.Add(Me.lblImporte)
        Me.tpCheque.Controls.Add(Me.txtVencimiento)
        Me.tpCheque.Controls.Add(Me.txtOrden)
        Me.tpCheque.Controls.Add(Me.lblCuentaBancaria)
        Me.tpCheque.Controls.Add(Me.lblOrden)
        Me.tpCheque.Controls.Add(Me.cbxCuentaBancaria)
        Me.tpCheque.Controls.Add(Me.txtFechaPagoCheque)
        Me.tpCheque.Controls.Add(Me.lblFechaCheque)
        Me.tpCheque.Controls.Add(Me.lblFechaPagoCheque)
        Me.tpCheque.Controls.Add(Me.txtFechaCheque)
        Me.tpCheque.Controls.Add(Me.txtMoneda)
        Me.tpCheque.Controls.Add(Me.lblNroCheque)
        Me.tpCheque.Controls.Add(Me.txtBanco)
        Me.tpCheque.Controls.Add(Me.txtNroCheque)
        Me.tpCheque.Controls.Add(Me.txtImporte)
        Me.tpCheque.Controls.Add(Me.txtCotizacion)
        Me.tpCheque.Controls.Add(Me.lblCotizacionCheque)
        Me.tpCheque.Controls.Add(Me.lblImporteMoneda)
        Me.tpCheque.Controls.Add(Me.lblDiferido)
        Me.tpCheque.Controls.Add(Me.txtImporteMoneda)
        Me.tpCheque.Controls.Add(Me.chkDiferido)
        Me.tpCheque.Controls.Add(Me.lblVencimiento)
        Me.tpCheque.Location = New System.Drawing.Point(4, 25)
        Me.tpCheque.Name = "tpCheque"
        Me.tpCheque.Padding = New System.Windows.Forms.Padding(3)
        Me.tpCheque.Size = New System.Drawing.Size(665, 126)
        Me.tpCheque.TabIndex = 0
        Me.tpCheque.Text = "Cheque"
        '
        'txtVencimiento
        '
        Me.txtVencimiento.AñoFecha = 0
        Me.txtVencimiento.Color = System.Drawing.Color.Empty
        Me.txtVencimiento.Enabled = False
        Me.txtVencimiento.Fecha = New Date(2013, 5, 30, 8, 29, 32, 578)
        Me.txtVencimiento.Location = New System.Drawing.Point(567, 43)
        Me.txtVencimiento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtVencimiento.MesFecha = 0
        Me.txtVencimiento.Name = "txtVencimiento"
        Me.txtVencimiento.PermitirNulo = False
        Me.txtVencimiento.Size = New System.Drawing.Size(68, 20)
        Me.txtVencimiento.SoloLectura = True
        Me.txtVencimiento.TabIndex = 13
        Me.txtVencimiento.TabStop = False
        '
        'txtOrden
        '
        Me.txtOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOrden.Color = System.Drawing.Color.Empty
        Me.txtOrden.Indicaciones = Nothing
        Me.txtOrden.Location = New System.Drawing.Point(108, 96)
        Me.txtOrden.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOrden.Multilinea = False
        Me.txtOrden.Name = "txtOrden"
        Me.txtOrden.Size = New System.Drawing.Size(527, 21)
        Me.txtOrden.SoloLectura = True
        Me.txtOrden.TabIndex = 21
        Me.txtOrden.TabStop = False
        Me.txtOrden.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtOrden.Texto = ""
        '
        'lblCuentaBancaria
        '
        Me.lblCuentaBancaria.AutoSize = True
        Me.lblCuentaBancaria.Location = New System.Drawing.Point(34, 21)
        Me.lblCuentaBancaria.Name = "lblCuentaBancaria"
        Me.lblCuentaBancaria.Size = New System.Drawing.Size(44, 13)
        Me.lblCuentaBancaria.TabIndex = 0
        Me.lblCuentaBancaria.Text = "Cuenta:"
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Location = New System.Drawing.Point(34, 100)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(75, 13)
        Me.lblOrden.TabIndex = 20
        Me.lblOrden.Text = "A la Orden de:"
        '
        'cbxCuentaBancaria
        '
        Me.cbxCuentaBancaria.CampoWhere = Nothing
        Me.cbxCuentaBancaria.CargarUnaSolaVez = False
        Me.cbxCuentaBancaria.DataDisplayMember = Nothing
        Me.cbxCuentaBancaria.DataFilter = Nothing
        Me.cbxCuentaBancaria.DataOrderBy = Nothing
        Me.cbxCuentaBancaria.DataSource = Nothing
        Me.cbxCuentaBancaria.DataValueMember = Nothing
        Me.cbxCuentaBancaria.dtSeleccionado = Nothing
        Me.cbxCuentaBancaria.FormABM = Nothing
        Me.cbxCuentaBancaria.Indicaciones = Nothing
        Me.cbxCuentaBancaria.Location = New System.Drawing.Point(108, 17)
        Me.cbxCuentaBancaria.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCuentaBancaria.Name = "cbxCuentaBancaria"
        Me.cbxCuentaBancaria.SeleccionMultiple = False
        Me.cbxCuentaBancaria.SeleccionObligatoria = True
        Me.cbxCuentaBancaria.Size = New System.Drawing.Size(138, 21)
        Me.cbxCuentaBancaria.SoloLectura = True
        Me.cbxCuentaBancaria.TabIndex = 1
        Me.cbxCuentaBancaria.TabStop = False
        Me.cbxCuentaBancaria.Texto = ""
        '
        'txtFechaPagoCheque
        '
        Me.txtFechaPagoCheque.AñoFecha = 0
        Me.txtFechaPagoCheque.Color = System.Drawing.Color.Empty
        Me.txtFechaPagoCheque.Fecha = New Date(2013, 5, 30, 8, 29, 32, 578)
        Me.txtFechaPagoCheque.Location = New System.Drawing.Point(268, 43)
        Me.txtFechaPagoCheque.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFechaPagoCheque.MesFecha = 0
        Me.txtFechaPagoCheque.Name = "txtFechaPagoCheque"
        Me.txtFechaPagoCheque.PermitirNulo = False
        Me.txtFechaPagoCheque.Size = New System.Drawing.Size(67, 20)
        Me.txtFechaPagoCheque.SoloLectura = True
        Me.txtFechaPagoCheque.TabIndex = 9
        Me.txtFechaPagoCheque.TabStop = False
        '
        'lblFechaCheque
        '
        Me.lblFechaCheque.AutoSize = True
        Me.lblFechaCheque.Location = New System.Drawing.Point(34, 47)
        Me.lblFechaCheque.Name = "lblFechaCheque"
        Me.lblFechaCheque.Size = New System.Drawing.Size(40, 13)
        Me.lblFechaCheque.TabIndex = 6
        Me.lblFechaCheque.Text = "Fecha:"
        '
        'lblFechaPagoCheque
        '
        Me.lblFechaPagoCheque.AutoSize = True
        Me.lblFechaPagoCheque.Location = New System.Drawing.Point(181, 47)
        Me.lblFechaPagoCheque.Name = "lblFechaPagoCheque"
        Me.lblFechaPagoCheque.Size = New System.Drawing.Size(83, 13)
        Me.lblFechaPagoCheque.TabIndex = 8
        Me.lblFechaPagoCheque.Text = "Fecha de Pago:"
        '
        'txtFechaCheque
        '
        Me.txtFechaCheque.AñoFecha = 0
        Me.txtFechaCheque.Color = System.Drawing.Color.Empty
        Me.txtFechaCheque.Fecha = New Date(2013, 5, 30, 8, 29, 32, 594)
        Me.txtFechaCheque.Location = New System.Drawing.Point(108, 43)
        Me.txtFechaCheque.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFechaCheque.MesFecha = 0
        Me.txtFechaCheque.Name = "txtFechaCheque"
        Me.txtFechaCheque.PermitirNulo = False
        Me.txtFechaCheque.Size = New System.Drawing.Size(67, 20)
        Me.txtFechaCheque.SoloLectura = True
        Me.txtFechaCheque.TabIndex = 7
        Me.txtFechaCheque.TabStop = False
        '
        'txtMoneda
        '
        Me.txtMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMoneda.Color = System.Drawing.Color.Empty
        Me.txtMoneda.Indicaciones = Nothing
        Me.txtMoneda.Location = New System.Drawing.Point(353, 17)
        Me.txtMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.txtMoneda.Multilinea = False
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.Size = New System.Drawing.Size(62, 21)
        Me.txtMoneda.SoloLectura = True
        Me.txtMoneda.TabIndex = 3
        Me.txtMoneda.TabStop = False
        Me.txtMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMoneda.Texto = ""
        '
        'lblNroCheque
        '
        Me.lblNroCheque.AutoSize = True
        Me.lblNroCheque.Location = New System.Drawing.Point(431, 21)
        Me.lblNroCheque.Name = "lblNroCheque"
        Me.lblNroCheque.Size = New System.Drawing.Size(70, 13)
        Me.lblNroCheque.TabIndex = 4
        Me.lblNroCheque.Text = "Nro. Cheque:"
        '
        'txtBanco
        '
        Me.txtBanco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBanco.Color = System.Drawing.Color.Empty
        Me.txtBanco.Indicaciones = Nothing
        Me.txtBanco.Location = New System.Drawing.Point(246, 17)
        Me.txtBanco.Margin = New System.Windows.Forms.Padding(4)
        Me.txtBanco.Multilinea = False
        Me.txtBanco.Name = "txtBanco"
        Me.txtBanco.Size = New System.Drawing.Size(107, 21)
        Me.txtBanco.SoloLectura = True
        Me.txtBanco.TabIndex = 2
        Me.txtBanco.TabStop = False
        Me.txtBanco.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBanco.Texto = ""
        '
        'txtNroCheque
        '
        Me.txtNroCheque.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroCheque.Color = System.Drawing.Color.Empty
        Me.txtNroCheque.Indicaciones = Nothing
        Me.txtNroCheque.Location = New System.Drawing.Point(502, 17)
        Me.txtNroCheque.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNroCheque.Multilinea = False
        Me.txtNroCheque.Name = "txtNroCheque"
        Me.txtNroCheque.Size = New System.Drawing.Size(133, 21)
        Me.txtNroCheque.SoloLectura = True
        Me.txtNroCheque.TabIndex = 5
        Me.txtNroCheque.TabStop = False
        Me.txtNroCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroCheque.Texto = ""
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = False
        Me.txtImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporte.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporte.Location = New System.Drawing.Point(487, 69)
        Me.txtImporte.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(148, 21)
        Me.txtImporte.SoloLectura = True
        Me.txtImporte.TabIndex = 19
        Me.txtImporte.TabStop = False
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(108, 69)
        Me.txtCotizacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(67, 21)
        Me.txtCotizacion.SoloLectura = True
        Me.txtCotizacion.TabIndex = 15
        Me.txtCotizacion.TabStop = False
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "1"
        '
        'lblCotizacionCheque
        '
        Me.lblCotizacionCheque.AutoSize = True
        Me.lblCotizacionCheque.Location = New System.Drawing.Point(34, 73)
        Me.lblCotizacionCheque.Name = "lblCotizacionCheque"
        Me.lblCotizacionCheque.Size = New System.Drawing.Size(59, 13)
        Me.lblCotizacionCheque.TabIndex = 14
        Me.lblCotizacionCheque.Text = "Cotización:"
        '
        'lblImporteMoneda
        '
        Me.lblImporteMoneda.AutoSize = True
        Me.lblImporteMoneda.Location = New System.Drawing.Point(181, 73)
        Me.lblImporteMoneda.Name = "lblImporteMoneda"
        Me.lblImporteMoneda.Size = New System.Drawing.Size(102, 13)
        Me.lblImporteMoneda.TabIndex = 16
        Me.lblImporteMoneda.Text = "Importe de Moneda:"
        '
        'lblDiferido
        '
        Me.lblDiferido.AutoSize = True
        Me.lblDiferido.Location = New System.Drawing.Point(341, 46)
        Me.lblDiferido.Name = "lblDiferido"
        Me.lblDiferido.Size = New System.Drawing.Size(86, 13)
        Me.lblDiferido.TabIndex = 10
        Me.lblDiferido.Text = "Cheque Diferido:"
        '
        'txtImporteMoneda
        '
        Me.txtImporteMoneda.Color = System.Drawing.Color.Empty
        Me.txtImporteMoneda.Decimales = True
        Me.txtImporteMoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteMoneda.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporteMoneda.Location = New System.Drawing.Point(289, 69)
        Me.txtImporteMoneda.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtImporteMoneda.Name = "txtImporteMoneda"
        Me.txtImporteMoneda.Size = New System.Drawing.Size(110, 21)
        Me.txtImporteMoneda.SoloLectura = True
        Me.txtImporteMoneda.TabIndex = 17
        Me.txtImporteMoneda.TabStop = False
        Me.txtImporteMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteMoneda.Texto = "0"
        '
        'chkDiferido
        '
        Me.chkDiferido.AutoSize = True
        Me.chkDiferido.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkDiferido.Enabled = False
        Me.chkDiferido.Location = New System.Drawing.Point(429, 46)
        Me.chkDiferido.Name = "chkDiferido"
        Me.chkDiferido.Size = New System.Drawing.Size(15, 14)
        Me.chkDiferido.TabIndex = 11
        Me.chkDiferido.UseVisualStyleBackColor = True
        '
        'lblVencimiento
        '
        Me.lblVencimiento.AutoSize = True
        Me.lblVencimiento.Location = New System.Drawing.Point(445, 47)
        Me.lblVencimiento.Name = "lblVencimiento"
        Me.lblVencimiento.Size = New System.Drawing.Size(116, 13)
        Me.lblVencimiento.TabIndex = 12
        Me.lblVencimiento.Text = "Fecha de Vencimiento:"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(15, 42)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 9
        Me.lblMoneda.Text = "Moneda:"
        '
        'cbxListar
        '
        Me.cbxListar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxListar.Enabled = False
        Me.cbxListar.FormattingEnabled = True
        Me.cbxListar.Location = New System.Drawing.Point(500, 12)
        Me.cbxListar.Name = "cbxListar"
        Me.cbxListar.Size = New System.Drawing.Size(160, 21)
        Me.cbxListar.TabIndex = 8
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(24, 68)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 13
        Me.lblFecha.Text = "Fecha:"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(141, 42)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(59, 13)
        Me.lblProveedor.TabIndex = 11
        Me.lblProveedor.Text = "Proveedor:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(168, 68)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(32, 13)
        Me.lblObservacion.TabIndex = 15
        Me.lblObservacion.Text = "Obs.:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(194, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(339, 16)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(37, 13)
        Me.lblComprobante.TabIndex = 5
        Me.lblComprobante.Text = "Comp:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(5, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operación:"
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(14, 307)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(66, 23)
        Me.btnAnular.TabIndex = 3
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 356)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(689, 22)
        Me.StatusStrip1.TabIndex = 11
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(82, 307)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(72, 23)
        Me.btnImprimir.TabIndex = 4
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(301, 307)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 6
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(158, 307)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(70, 23)
        Me.btnBusquedaAvanzada.TabIndex = 5
        Me.btnBusquedaAvanzada.Text = "&Busqueda"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(607, 307)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 10
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(530, 307)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 9
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(452, 307)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 8
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.cbxMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.txtProveedor)
        Me.gbxCabecera.Controls.Add(Me.cbxListar)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblProveedor)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Location = New System.Drawing.Point(12, 0)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(669, 94)
        Me.gbxCabecera.TabIndex = 0
        Me.gbxCabecera.TabStop = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Referencia"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(76, 37)
        Me.cbxMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(63, 21)
        Me.cbxMoneda.SoloLectura = True
        Me.cbxMoneda.TabIndex = 10
        Me.cbxMoneda.TabStop = False
        Me.cbxMoneda.Texto = ""
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 100
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(210, 35)
        Me.txtProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(449, 27)
        Me.txtProveedor.SoloLectura = True
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 12
        Me.txtProveedor.TabStop = False
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(210, 64)
        Me.txtObservacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(450, 21)
        Me.txtObservacion.SoloLectura = True
        Me.txtObservacion.TabIndex = 16
        Me.txtObservacion.TabStop = False
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(242, 12)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(94, 21)
        Me.cbxSucursal.SoloLectura = True
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.TabStop = False
        Me.cbxSucursal.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 5, 30, 8, 29, 32, 812)
        Me.txtFecha.Location = New System.Drawing.Point(76, 64)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(69, 20)
        Me.txtFecha.SoloLectura = True
        Me.txtFecha.TabIndex = 14
        Me.txtFecha.TabStop = False
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(373, 12)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(63, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 6
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(438, 12)
        Me.txtComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(59, 21)
        Me.txtComprobante.SoloLectura = True
        Me.txtComprobante.TabIndex = 7
        Me.txtComprobante.TabStop = False
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(76, 12)
        Me.cbxCiudad.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(63, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Introduzca el código y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(139, 12)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(56, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkEntregado)
        Me.GroupBox1.Controls.Add(Me.txtRetiradoPor)
        Me.GroupBox1.Controls.Add(Me.txtRecibo)
        Me.GroupBox1.Controls.Add(Me.txtFechaEntrega)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 250)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(665, 47)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'chkEntregado
        '
        Me.chkEntregado.AutoSize = True
        Me.chkEntregado.Enabled = False
        Me.chkEntregado.Location = New System.Drawing.Point(7, 20)
        Me.chkEntregado.Name = "chkEntregado"
        Me.chkEntregado.Size = New System.Drawing.Size(15, 14)
        Me.chkEntregado.TabIndex = 0
        Me.chkEntregado.UseVisualStyleBackColor = True
        '
        'txtRetiradoPor
        '
        Me.txtRetiradoPor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRetiradoPor.Color = System.Drawing.Color.Empty
        Me.txtRetiradoPor.Indicaciones = Nothing
        Me.txtRetiradoPor.Location = New System.Drawing.Point(394, 17)
        Me.txtRetiradoPor.Margin = New System.Windows.Forms.Padding(4)
        Me.txtRetiradoPor.Multilinea = False
        Me.txtRetiradoPor.Name = "txtRetiradoPor"
        Me.txtRetiradoPor.Size = New System.Drawing.Size(260, 27)
        Me.txtRetiradoPor.SoloLectura = False
        Me.txtRetiradoPor.TabIndex = 6
        Me.txtRetiradoPor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRetiradoPor.Texto = ""
        '
        'txtRecibo
        '
        Me.txtRecibo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRecibo.Color = System.Drawing.Color.Empty
        Me.txtRecibo.Indicaciones = Nothing
        Me.txtRecibo.Location = New System.Drawing.Point(247, 17)
        Me.txtRecibo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtRecibo.Multilinea = False
        Me.txtRecibo.Name = "txtRecibo"
        Me.txtRecibo.Size = New System.Drawing.Size(67, 27)
        Me.txtRecibo.SoloLectura = False
        Me.txtRecibo.TabIndex = 4
        Me.txtRecibo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRecibo.Texto = ""
        '
        'txtFechaEntrega
        '
        Me.txtFechaEntrega.AñoFecha = 0
        Me.txtFechaEntrega.Color = System.Drawing.Color.Empty
        Me.txtFechaEntrega.Fecha = New Date(CType(0, Long))
        Me.txtFechaEntrega.Location = New System.Drawing.Point(112, 17)
        Me.txtFechaEntrega.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFechaEntrega.MesFecha = 0
        Me.txtFechaEntrega.Name = "txtFechaEntrega"
        Me.txtFechaEntrega.PermitirNulo = True
        Me.txtFechaEntrega.Size = New System.Drawing.Size(81, 21)
        Me.txtFechaEntrega.SoloLectura = False
        Me.txtFechaEntrega.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(321, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Retirado Por:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(199, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Recibo:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(29, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Fecha Entrega:"
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(12, 336)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(247, 20)
        Me.flpAnuladoPor.TabIndex = 12
        Me.flpAnuladoPor.Visible = False
        '
        'lblAnulado
        '
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Red
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(89, 20)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(98, 3)
        Me.lblUsuarioAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(150, 3)
        Me.lblFechaAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'txtFechaHoy
        '
        Me.txtFechaHoy.AñoFecha = 0
        Me.txtFechaHoy.Color = System.Drawing.Color.Empty
        Me.txtFechaHoy.Fecha = New Date(2013, 5, 30, 8, 29, 32, 812)
        Me.txtFechaHoy.Location = New System.Drawing.Point(0, 360)
        Me.txtFechaHoy.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFechaHoy.MesFecha = 0
        Me.txtFechaHoy.Name = "txtFechaHoy"
        Me.txtFechaHoy.PermitirNulo = False
        Me.txtFechaHoy.Size = New System.Drawing.Size(69, 20)
        Me.txtFechaHoy.SoloLectura = True
        Me.txtFechaHoy.TabIndex = 15
        Me.txtFechaHoy.TabStop = False
        Me.txtFechaHoy.Visible = False
        '
        'frmEntregaChequeOP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(689, 378)
        Me.Controls.Add(Me.txtFechaHoy)
        Me.Controls.Add(Me.flpAnuladoPor)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Name = "frmEntregaChequeOP"
        Me.Tag = "frmEntregaChequeOP"
        Me.Text = "frmEntregaChequeOP"
        Me.TabControl1.ResumeLayout(False)
        Me.tpCheque.ResumeLayout(False)
        Me.tpCheque.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents txtVencimiento As ERP.ocxTXTDate
    Friend WithEvents txtOrden As ERP.ocxTXTString
    Friend WithEvents cbxCuentaBancaria As ERP.ocxCBX
    Friend WithEvents txtFechaPagoCheque As ERP.ocxTXTDate
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tpCheque As System.Windows.Forms.TabPage
    Friend WithEvents lblCuentaBancaria As System.Windows.Forms.Label
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents lblFechaCheque As System.Windows.Forms.Label
    Friend WithEvents lblFechaPagoCheque As System.Windows.Forms.Label
    Friend WithEvents txtFechaCheque As ERP.ocxTXTDate
    Friend WithEvents txtMoneda As ERP.ocxTXTString
    Friend WithEvents lblNroCheque As System.Windows.Forms.Label
    Friend WithEvents txtBanco As ERP.ocxTXTString
    Friend WithEvents txtNroCheque As ERP.ocxTXTString
    Friend WithEvents txtImporte As ERP.ocxTXTNumeric
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents lblCotizacionCheque As System.Windows.Forms.Label
    Friend WithEvents lblImporteMoneda As System.Windows.Forms.Label
    Friend WithEvents lblDiferido As System.Windows.Forms.Label
    Friend WithEvents txtImporteMoneda As ERP.ocxTXTNumeric
    Friend WithEvents chkDiferido As System.Windows.Forms.CheckBox
    Friend WithEvents lblVencimiento As System.Windows.Forms.Label
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtProveedor As ERP.ocxTXTProveedor
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents cbxListar As System.Windows.Forms.ComboBox
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtRetiradoPor As ERP.ocxTXTString
    Friend WithEvents txtRecibo As ERP.ocxTXTString
    Friend WithEvents txtFechaEntrega As ERP.ocxTXTDate
    Friend WithEvents chkEntregado As System.Windows.Forms.CheckBox
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents txtFechaHoy As ocxTXTDate
End Class

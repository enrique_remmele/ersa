﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrepararPago
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrepararPago))
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkPeriodo = New System.Windows.Forms.CheckBox()
        Me.lklEsteMes = New System.Windows.Forms.LinkLabel()
        Me.lklEstaSemana = New System.Windows.Forms.LinkLabel()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxCuentaBancaria = New ERP.ocxCBX()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.btnSeleccionarTodos = New System.Windows.Forms.ToolStripButton()
        Me.btnQuitarSeleccion = New System.Windows.Forms.ToolStripButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.chkUnidadNegocio = New System.Windows.Forms.CheckBox()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.IDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Proveedor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoDocumento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Comprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cuota = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CantidadCuota = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vencimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cotizacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Saldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RRHH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pagar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CuentaBancaria = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.Observacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Visualizar = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalSaldo = New ERP.ocxTXTNumeric()
        Me.txtCantidadDocumentos = New ERP.ocxTXTNumeric()
        Me.lblTotalSaldo = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalAPagar = New ERP.ocxTXTNumeric()
        Me.txtCantidadAPagar = New ERP.ocxTXTNumeric()
        Me.lblTotalPagar = New System.Windows.Forms.Label()
        Me.dgvSaldosBancarios = New System.Windows.Forms.DataGridView()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblSaldosBancarios = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxSaldosBancariosOrdenar = New System.Windows.Forms.ComboBox()
        Me.chkOrdenadoDescendente = New System.Windows.Forms.CheckBox()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        CType(Me.dgvSaldosBancarios, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 317.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 331.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dgv, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvSaldosBancarios, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel5, 2, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1264, 681)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.FlowLayoutPanel1, 3)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel1)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblMoneda)
        Me.FlowLayoutPanel1.Controls.Add(Me.cbxMoneda)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnListar)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.cbxCuentaBancaria)
        Me.FlowLayoutPanel1.Controls.Add(Me.ToolStrip1)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel2)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1258, 61)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkPeriodo)
        Me.Panel1.Controls.Add(Me.lklEsteMes)
        Me.Panel1.Controls.Add(Me.lklEstaSemana)
        Me.Panel1.Controls.Add(Me.txtDesde)
        Me.Panel1.Controls.Add(Me.txtHasta)
        Me.Panel1.Location = New System.Drawing.Point(3, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(164, 61)
        Me.Panel1.TabIndex = 1
        '
        'chkPeriodo
        '
        Me.chkPeriodo.Checked = True
        Me.chkPeriodo.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPeriodo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkPeriodo.Location = New System.Drawing.Point(2, 1)
        Me.chkPeriodo.Margin = New System.Windows.Forms.Padding(20, 13, 3, 3)
        Me.chkPeriodo.Name = "chkPeriodo"
        Me.chkPeriodo.Size = New System.Drawing.Size(110, 22)
        Me.chkPeriodo.TabIndex = 4
        Me.chkPeriodo.Text = "Periodo de Venc.:"
        Me.chkPeriodo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkPeriodo.UseVisualStyleBackColor = True
        '
        'lklEsteMes
        '
        Me.lklEsteMes.AutoSize = True
        Me.lklEsteMes.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklEsteMes.Location = New System.Drawing.Point(80, 23)
        Me.lklEsteMes.Name = "lklEsteMes"
        Me.lklEsteMes.Size = New System.Drawing.Size(44, 12)
        Me.lklEsteMes.TabIndex = 1
        Me.lklEsteMes.TabStop = True
        Me.lklEsteMes.Text = "Este mes"
        '
        'lklEstaSemana
        '
        Me.lklEstaSemana.AutoSize = True
        Me.lklEstaSemana.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklEstaSemana.Location = New System.Drawing.Point(6, 23)
        Me.lklEstaSemana.Name = "lklEstaSemana"
        Me.lklEstaSemana.Size = New System.Drawing.Size(59, 12)
        Me.lklEstaSemana.TabIndex = 0
        Me.lklEstaSemana.TabStop = True
        Me.lklEstaSemana.Text = "Esta semana"
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(CType(0, Long))
        Me.txtDesde.Location = New System.Drawing.Point(6, 36)
        Me.txtDesde.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = True
        Me.txtDesde.Size = New System.Drawing.Size(69, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 2
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(CType(0, Long))
        Me.txtHasta.Location = New System.Drawing.Point(81, 36)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = True
        Me.txtHasta.Size = New System.Drawing.Size(69, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 3
        '
        'lblMoneda
        '
        Me.lblMoneda.Location = New System.Drawing.Point(173, 13)
        Me.lblMoneda.Margin = New System.Windows.Forms.Padding(3, 13, 3, 0)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(53, 20)
        Me.lblMoneda.TabIndex = 2
        Me.lblMoneda.Text = "Moneda:"
        Me.lblMoneda.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Referencia"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(232, 12)
        Me.cbxMoneda.Margin = New System.Windows.Forms.Padding(3, 12, 3, 3)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(73, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 3
        Me.cbxMoneda.Texto = ""
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(311, 11)
        Me.btnListar.Margin = New System.Windows.Forms.Padding(3, 11, 3, 3)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(75, 23)
        Me.btnListar.TabIndex = 4
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(392, 13)
        Me.Label1.Margin = New System.Windows.Forms.Padding(3, 13, 3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 20)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "C. Banc. pred.:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbxCuentaBancaria
        '
        Me.cbxCuentaBancaria.CampoWhere = Nothing
        Me.cbxCuentaBancaria.CargarUnaSolaVez = False
        Me.cbxCuentaBancaria.DataDisplayMember = "Descripcion"
        Me.cbxCuentaBancaria.DataFilter = Nothing
        Me.cbxCuentaBancaria.DataOrderBy = "ID"
        Me.cbxCuentaBancaria.DataSource = "VCuentaBancaria"
        Me.cbxCuentaBancaria.DataValueMember = "ID"
        Me.cbxCuentaBancaria.dtSeleccionado = Nothing
        Me.cbxCuentaBancaria.FormABM = Nothing
        Me.cbxCuentaBancaria.Indicaciones = Nothing
        Me.cbxCuentaBancaria.Location = New System.Drawing.Point(480, 12)
        Me.cbxCuentaBancaria.Margin = New System.Windows.Forms.Padding(3, 12, 3, 3)
        Me.cbxCuentaBancaria.Name = "cbxCuentaBancaria"
        Me.cbxCuentaBancaria.SeleccionMultiple = False
        Me.cbxCuentaBancaria.SeleccionObligatoria = True
        Me.cbxCuentaBancaria.Size = New System.Drawing.Size(180, 21)
        Me.cbxCuentaBancaria.SoloLectura = False
        Me.cbxCuentaBancaria.TabIndex = 6
        Me.cbxCuentaBancaria.Texto = ""
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnSeleccionarTodos, Me.btnQuitarSeleccion})
        Me.ToolStrip1.Location = New System.Drawing.Point(663, 11)
        Me.ToolStrip1.Margin = New System.Windows.Forms.Padding(0, 11, 0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(238, 25)
        Me.ToolStrip1.TabIndex = 7
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'btnSeleccionarTodos
        '
        Me.btnSeleccionarTodos.Image = CType(resources.GetObject("btnSeleccionarTodos.Image"), System.Drawing.Image)
        Me.btnSeleccionarTodos.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSeleccionarTodos.Name = "btnSeleccionarTodos"
        Me.btnSeleccionarTodos.Size = New System.Drawing.Size(115, 22)
        Me.btnSeleccionarTodos.Text = "Seleccionar todo"
        '
        'btnQuitarSeleccion
        '
        Me.btnQuitarSeleccion.Image = CType(resources.GetObject("btnQuitarSeleccion.Image"), System.Drawing.Image)
        Me.btnQuitarSeleccion.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.btnQuitarSeleccion.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnQuitarSeleccion.Name = "btnQuitarSeleccion"
        Me.btnQuitarSeleccion.Size = New System.Drawing.Size(111, 22)
        Me.btnQuitarSeleccion.Text = "Quitar Seleccion"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.chkUnidadNegocio)
        Me.Panel2.Controls.Add(Me.cbxUnidadNegocio)
        Me.Panel2.Location = New System.Drawing.Point(904, 0)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(299, 43)
        Me.Panel2.TabIndex = 4
        '
        'chkUnidadNegocio
        '
        Me.chkUnidadNegocio.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkUnidadNegocio.Location = New System.Drawing.Point(3, 11)
        Me.chkUnidadNegocio.Margin = New System.Windows.Forms.Padding(20, 13, 3, 3)
        Me.chkUnidadNegocio.Name = "chkUnidadNegocio"
        Me.chkUnidadNegocio.Size = New System.Drawing.Size(120, 22)
        Me.chkUnidadNegocio.TabIndex = 12
        Me.chkUnidadNegocio.Text = "Unidad de Negocio:"
        Me.chkUnidadNegocio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkUnidadNegocio.UseVisualStyleBackColor = True
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = Nothing
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = ""
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = ""
        Me.cbxUnidadNegocio.DataSource = ""
        Me.cbxUnidadNegocio.DataValueMember = ""
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(126, 11)
        Me.cbxUnidadNegocio.Margin = New System.Windows.Forms.Padding(3, 12, 3, 3)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(168, 21)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 11
        Me.cbxUnidadNegocio.Texto = ""
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.BackgroundColor = System.Drawing.Color.White
        Me.dgv.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDTransaccion, Me.Tipo, Me.Codigo, Me.Proveedor, Me.Fecha, Me.TipoDocumento, Me.Comprobante, Me.Cuota, Me.CantidadCuota, Me.Vencimiento, Me.Cotizacion, Me.Saldo, Me.RRHH, Me.Pagar, Me.Importe, Me.CuentaBancaria, Me.Observacion, Me.Visualizar})
        Me.TableLayoutPanel1.SetColumnSpan(Me.dgv, 3)
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv.GridColor = System.Drawing.Color.DimGray
        Me.dgv.Location = New System.Drawing.Point(3, 70)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.RowHeadersVisible = False
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgv.Size = New System.Drawing.Size(1258, 476)
        Me.dgv.StandardTab = True
        Me.dgv.TabIndex = 1
        '
        'IDTransaccion
        '
        Me.IDTransaccion.HeaderText = "IDTransaccion"
        Me.IDTransaccion.Name = "IDTransaccion"
        Me.IDTransaccion.Visible = False
        '
        'Tipo
        '
        Me.Tipo.HeaderText = "Tipo"
        Me.Tipo.Name = "Tipo"
        Me.Tipo.ReadOnly = True
        Me.Tipo.Visible = False
        '
        'Codigo
        '
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        '
        'Proveedor
        '
        Me.Proveedor.HeaderText = "Proveedor"
        Me.Proveedor.Name = "Proveedor"
        Me.Proveedor.ReadOnly = True
        Me.Proveedor.Width = 150
        '
        'Fecha
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Fecha.DefaultCellStyle = DataGridViewCellStyle1
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        Me.Fecha.Width = 70
        '
        'TipoDocumento
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipoDocumento.DefaultCellStyle = DataGridViewCellStyle2
        Me.TipoDocumento.HeaderText = "T. Doc."
        Me.TipoDocumento.Name = "TipoDocumento"
        Me.TipoDocumento.ReadOnly = True
        Me.TipoDocumento.Width = 50
        '
        'Comprobante
        '
        Me.Comprobante.HeaderText = "Comprobante"
        Me.Comprobante.Name = "Comprobante"
        Me.Comprobante.ReadOnly = True
        Me.Comprobante.Width = 130
        '
        'Cuota
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Cuota.DefaultCellStyle = DataGridViewCellStyle3
        Me.Cuota.HeaderText = "Cuota"
        Me.Cuota.Name = "Cuota"
        Me.Cuota.ReadOnly = True
        Me.Cuota.Width = 50
        '
        'CantidadCuota
        '
        Me.CantidadCuota.HeaderText = "Cant. Cuota"
        Me.CantidadCuota.Name = "CantidadCuota"
        Me.CantidadCuota.ReadOnly = True
        Me.CantidadCuota.Visible = False
        '
        'Vencimiento
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Vencimiento.DefaultCellStyle = DataGridViewCellStyle4
        Me.Vencimiento.HeaderText = "F. Venc."
        Me.Vencimiento.Name = "Vencimiento"
        Me.Vencimiento.ReadOnly = True
        Me.Vencimiento.Width = 70
        '
        'Cotizacion
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        Me.Cotizacion.DefaultCellStyle = DataGridViewCellStyle5
        Me.Cotizacion.HeaderText = "Cotiz."
        Me.Cotizacion.Name = "Cotizacion"
        Me.Cotizacion.ReadOnly = True
        Me.Cotizacion.Width = 60
        '
        'Saldo
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.Saldo.DefaultCellStyle = DataGridViewCellStyle6
        Me.Saldo.HeaderText = "Saldo"
        Me.Saldo.Name = "Saldo"
        Me.Saldo.ReadOnly = True
        '
        'RRHH
        '
        Me.RRHH.HeaderText = "RRHH"
        Me.RRHH.Name = "RRHH"
        Me.RRHH.Visible = False
        '
        'Pagar
        '
        Me.Pagar.HeaderText = "Pagar"
        Me.Pagar.Name = "Pagar"
        Me.Pagar.Width = 40
        '
        'Importe
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.Importe.DefaultCellStyle = DataGridViewCellStyle7
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        Me.Importe.ReadOnly = True
        '
        'CuentaBancaria
        '
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CuentaBancaria.DefaultCellStyle = DataGridViewCellStyle8
        Me.CuentaBancaria.HeaderText = "Cuenta"
        Me.CuentaBancaria.Name = "CuentaBancaria"
        '
        'Observacion
        '
        Me.Observacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Observacion.HeaderText = "Observacion"
        Me.Observacion.Name = "Observacion"
        '
        'Visualizar
        '
        Me.Visualizar.HeaderText = "Ver"
        Me.Visualizar.Name = "Visualizar"
        Me.Visualizar.Text = "..."
        Me.Visualizar.ToolTipText = "Visualizar el documento."
        Me.Visualizar.Width = 40
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.txtTotalSaldo)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtCantidadDocumentos)
        Me.FlowLayoutPanel2.Controls.Add(Me.lblTotalSaldo)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 549)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(317, 27)
        Me.FlowLayoutPanel2.TabIndex = 2
        '
        'txtTotalSaldo
        '
        Me.txtTotalSaldo.Color = System.Drawing.Color.Empty
        Me.txtTotalSaldo.Decimales = True
        Me.txtTotalSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalSaldo.Indicaciones = Nothing
        Me.txtTotalSaldo.Location = New System.Drawing.Point(174, 2)
        Me.txtTotalSaldo.Margin = New System.Windows.Forms.Padding(4, 2, 4, 4)
        Me.txtTotalSaldo.Name = "txtTotalSaldo"
        Me.txtTotalSaldo.Size = New System.Drawing.Size(139, 22)
        Me.txtTotalSaldo.SoloLectura = True
        Me.txtTotalSaldo.TabIndex = 2
        Me.txtTotalSaldo.TabStop = False
        Me.txtTotalSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalSaldo.Texto = "0"
        '
        'txtCantidadDocumentos
        '
        Me.txtCantidadDocumentos.Color = System.Drawing.Color.Empty
        Me.txtCantidadDocumentos.Decimales = True
        Me.txtCantidadDocumentos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCantidadDocumentos.Indicaciones = Nothing
        Me.txtCantidadDocumentos.Location = New System.Drawing.Point(131, 2)
        Me.txtCantidadDocumentos.Margin = New System.Windows.Forms.Padding(0, 2, 0, 4)
        Me.txtCantidadDocumentos.Name = "txtCantidadDocumentos"
        Me.txtCantidadDocumentos.Size = New System.Drawing.Size(39, 22)
        Me.txtCantidadDocumentos.SoloLectura = True
        Me.txtCantidadDocumentos.TabIndex = 1
        Me.txtCantidadDocumentos.TabStop = False
        Me.txtCantidadDocumentos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadDocumentos.Texto = "0"
        '
        'lblTotalSaldo
        '
        Me.lblTotalSaldo.Location = New System.Drawing.Point(60, 5)
        Me.lblTotalSaldo.Margin = New System.Windows.Forms.Padding(3, 5, 3, 0)
        Me.lblTotalSaldo.Name = "lblTotalSaldo"
        Me.lblTotalSaldo.Size = New System.Drawing.Size(68, 19)
        Me.lblTotalSaldo.TabIndex = 0
        Me.lblTotalSaldo.Text = "Saldo Total:"
        Me.lblTotalSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.txtTotalAPagar)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtCantidadAPagar)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblTotalPagar)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(0, 576)
        Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(317, 105)
        Me.FlowLayoutPanel3.TabIndex = 7
        '
        'txtTotalAPagar
        '
        Me.txtTotalAPagar.Color = System.Drawing.Color.Empty
        Me.txtTotalAPagar.Decimales = True
        Me.txtTotalAPagar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalAPagar.Indicaciones = Nothing
        Me.txtTotalAPagar.Location = New System.Drawing.Point(174, 2)
        Me.txtTotalAPagar.Margin = New System.Windows.Forms.Padding(4, 2, 4, 4)
        Me.txtTotalAPagar.Name = "txtTotalAPagar"
        Me.txtTotalAPagar.Size = New System.Drawing.Size(139, 22)
        Me.txtTotalAPagar.SoloLectura = True
        Me.txtTotalAPagar.TabIndex = 5
        Me.txtTotalAPagar.TabStop = False
        Me.txtTotalAPagar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalAPagar.Texto = "0"
        '
        'txtCantidadAPagar
        '
        Me.txtCantidadAPagar.Color = System.Drawing.Color.Empty
        Me.txtCantidadAPagar.Decimales = True
        Me.txtCantidadAPagar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCantidadAPagar.Indicaciones = Nothing
        Me.txtCantidadAPagar.Location = New System.Drawing.Point(131, 2)
        Me.txtCantidadAPagar.Margin = New System.Windows.Forms.Padding(0, 2, 0, 4)
        Me.txtCantidadAPagar.Name = "txtCantidadAPagar"
        Me.txtCantidadAPagar.Size = New System.Drawing.Size(39, 22)
        Me.txtCantidadAPagar.SoloLectura = True
        Me.txtCantidadAPagar.TabIndex = 4
        Me.txtCantidadAPagar.TabStop = False
        Me.txtCantidadAPagar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadAPagar.Texto = "0"
        '
        'lblTotalPagar
        '
        Me.lblTotalPagar.Location = New System.Drawing.Point(5, 5)
        Me.lblTotalPagar.Margin = New System.Windows.Forms.Padding(3, 5, 3, 0)
        Me.lblTotalPagar.Name = "lblTotalPagar"
        Me.lblTotalPagar.Size = New System.Drawing.Size(123, 19)
        Me.lblTotalPagar.TabIndex = 3
        Me.lblTotalPagar.Text = "Docum. / Total a Pagar:"
        Me.lblTotalPagar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgvSaldosBancarios
        '
        Me.dgvSaldosBancarios.AllowUserToAddRows = False
        Me.dgvSaldosBancarios.AllowUserToDeleteRows = False
        Me.dgvSaldosBancarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSaldosBancarios.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvSaldosBancarios.Location = New System.Drawing.Point(392, 579)
        Me.dgvSaldosBancarios.Margin = New System.Windows.Forms.Padding(75, 3, 50, 7)
        Me.dgvSaldosBancarios.Name = "dgvSaldosBancarios"
        Me.dgvSaldosBancarios.ReadOnly = True
        Me.dgvSaldosBancarios.Size = New System.Drawing.Size(491, 95)
        Me.dgvSaldosBancarios.TabIndex = 4
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.lblSaldosBancarios)
        Me.FlowLayoutPanel4.Controls.Add(Me.Label2)
        Me.FlowLayoutPanel4.Controls.Add(Me.cbxSaldosBancariosOrdenar)
        Me.FlowLayoutPanel4.Controls.Add(Me.chkOrdenadoDescendente)
        Me.FlowLayoutPanel4.Controls.Add(Me.LinkLabel1)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(392, 549)
        Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(75, 0, 50, 0)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(491, 27)
        Me.FlowLayoutPanel4.TabIndex = 3
        '
        'lblSaldosBancarios
        '
        Me.lblSaldosBancarios.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaldosBancarios.Location = New System.Drawing.Point(3, 3)
        Me.lblSaldosBancarios.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblSaldosBancarios.Name = "lblSaldosBancarios"
        Me.lblSaldosBancarios.Size = New System.Drawing.Size(136, 21)
        Me.lblSaldosBancarios.TabIndex = 0
        Me.lblSaldosBancarios.Text = "SALDOS BANCARIOS"
        Me.lblSaldosBancarios.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(145, 3)
        Me.Label2.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(106, 21)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "-  Ordenado por:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbxSaldosBancariosOrdenar
        '
        Me.cbxSaldosBancariosOrdenar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSaldosBancariosOrdenar.FormattingEnabled = True
        Me.cbxSaldosBancariosOrdenar.Items.AddRange(New Object() {"Disponible", "Utilizado", "Saldo", "Cuenta"})
        Me.cbxSaldosBancariosOrdenar.Location = New System.Drawing.Point(257, 3)
        Me.cbxSaldosBancariosOrdenar.Name = "cbxSaldosBancariosOrdenar"
        Me.cbxSaldosBancariosOrdenar.Size = New System.Drawing.Size(114, 21)
        Me.cbxSaldosBancariosOrdenar.TabIndex = 1
        '
        'chkOrdenadoDescendente
        '
        Me.chkOrdenadoDescendente.CheckAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.chkOrdenadoDescendente.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOrdenadoDescendente.Location = New System.Drawing.Point(377, 3)
        Me.chkOrdenadoDescendente.Name = "chkOrdenadoDescendente"
        Me.chkOrdenadoDescendente.Size = New System.Drawing.Size(50, 21)
        Me.chkOrdenadoDescendente.TabIndex = 2
        Me.chkOrdenadoDescendente.Text = "Desc."
        Me.chkOrdenadoDescendente.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.chkOrdenadoDescendente.UseVisualStyleBackColor = True
        '
        'LinkLabel1
        '
        Me.LinkLabel1.Location = New System.Drawing.Point(3, 27)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(83, 24)
        Me.LinkLabel1.TabIndex = 3
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Vizualizar"
        Me.LinkLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.Controls.Add(Me.btnGuardar)
        Me.FlowLayoutPanel5.Controls.Add(Me.btnImprimir)
        Me.FlowLayoutPanel5.Controls.Add(Me.btnSalir)
        Me.FlowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel5.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(936, 552)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.TableLayoutPanel1.SetRowSpan(Me.FlowLayoutPanel5, 2)
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(325, 126)
        Me.FlowLayoutPanel5.TabIndex = 8
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(39, 3)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(283, 48)
        Me.btnGuardar.TabIndex = 0
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(249, 57)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(73, 24)
        Me.btnImprimir.TabIndex = 1
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(170, 57)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(73, 24)
        Me.btnSalir.TabIndex = 2
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'frmPrepararPago
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(1264, 681)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmPrepararPago"
        Me.Tag = "frmPrepararPago"
        Me.Text = "frmPrepararPago"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        CType(Me.dgvSaldosBancarios, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents btnListar As System.Windows.Forms.Button
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblTotalSaldo As System.Windows.Forms.Label
    Friend WithEvents txtTotalSaldo As ERP.ocxTXTNumeric
    Friend WithEvents txtCantidadDocumentos As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalPagar As System.Windows.Forms.Label
    Friend WithEvents txtCantidadAPagar As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalAPagar As ERP.ocxTXTNumeric
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxCuentaBancaria As ERP.ocxCBX
    Friend WithEvents dgvSaldosBancarios As System.Windows.Forms.DataGridView
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents cbxSaldosBancariosOrdenar As System.Windows.Forms.ComboBox
    Friend WithEvents lblSaldosBancarios As System.Windows.Forms.Label
    Friend WithEvents chkOrdenadoDescendente As System.Windows.Forms.CheckBox
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents btnSeleccionarTodos As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnQuitarSeleccion As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lklEsteMes As System.Windows.Forms.LinkLabel
    Friend WithEvents lklEstaSemana As System.Windows.Forms.LinkLabel
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel5 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents IDTransaccion As DataGridViewTextBoxColumn
    Friend WithEvents Tipo As DataGridViewTextBoxColumn
    Friend WithEvents Codigo As DataGridViewTextBoxColumn
    Friend WithEvents Proveedor As DataGridViewTextBoxColumn
    Friend WithEvents Fecha As DataGridViewTextBoxColumn
    Friend WithEvents TipoDocumento As DataGridViewTextBoxColumn
    Friend WithEvents Comprobante As DataGridViewTextBoxColumn
    Friend WithEvents Cuota As DataGridViewTextBoxColumn
    Friend WithEvents CantidadCuota As DataGridViewTextBoxColumn
    Friend WithEvents Vencimiento As DataGridViewTextBoxColumn
    Friend WithEvents Cotizacion As DataGridViewTextBoxColumn
    Friend WithEvents Saldo As DataGridViewTextBoxColumn
    Friend WithEvents RRHH As DataGridViewTextBoxColumn
    Friend WithEvents Pagar As DataGridViewCheckBoxColumn
    Friend WithEvents Importe As DataGridViewTextBoxColumn
    Friend WithEvents CuentaBancaria As DataGridViewComboBoxColumn
    Friend WithEvents Observacion As DataGridViewTextBoxColumn
    Friend WithEvents Visualizar As DataGridViewButtonColumn
    Friend WithEvents Panel2 As Panel
    Friend WithEvents chkUnidadNegocio As CheckBox
    Friend WithEvents cbxUnidadNegocio As ocxCBX
    Friend WithEvents chkPeriodo As CheckBox
End Class

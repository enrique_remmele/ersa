﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCargarComprobantesSinOP
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.lklEliminarVenta = New System.Windows.Forms.LinkLabel()
        Me.txtPorcRetencion = New ERP.ocxTXTNumeric()
        Me.txtTotalGravado = New ERP.ocxTXTNumeric()
        Me.txtRenta = New ERP.ocxTXTNumeric()
        Me.txtPorcRenta = New ERP.ocxTXTNumeric()
        Me.txtRetencionIVA = New ERP.ocxTXTNumeric()
        Me.txtIVA10 = New ERP.ocxTXTNumeric()
        Me.txtTotal10 = New ERP.ocxTXTNumeric()
        Me.txtIVA5 = New ERP.ocxTXTNumeric()
        Me.txtTotal5 = New ERP.ocxTXTNumeric()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.colComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIVA5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIVA10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotalGravado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPorcentajeRetencion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRetencionIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPorcentajeRenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 350)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(887, 22)
        Me.StatusStrip1.TabIndex = 15
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(795, 316)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 14
        Me.Button2.Text = "&Cancelar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(714, 316)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "&Aceptar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colComprobante, Me.colFecha, Me.colTotal5, Me.colIVA5, Me.colTotal10, Me.colIVA10, Me.colTotalGravado, Me.colPorcentajeRetencion, Me.colRetencionIVA, Me.colPorcentajeRenta, Me.colRenta})
        Me.dgw.Location = New System.Drawing.Point(7, 4)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(873, 286)
        Me.dgw.TabIndex = 0
        Me.dgw.TabStop = False
        '
        'lklEliminarVenta
        '
        Me.lklEliminarVenta.AutoSize = True
        Me.lklEliminarVenta.Location = New System.Drawing.Point(9, 326)
        Me.lklEliminarVenta.Name = "lklEliminarVenta"
        Me.lklEliminarVenta.Size = New System.Drawing.Size(108, 13)
        Me.lklEliminarVenta.TabIndex = 12
        Me.lklEliminarVenta.TabStop = True
        Me.lklEliminarVenta.Text = "Eliminar comprobante"
        '
        'txtPorcRetencion
        '
        Me.txtPorcRetencion.Color = System.Drawing.Color.Empty
        Me.txtPorcRetencion.Decimales = True
        Me.txtPorcRetencion.Indicaciones = Nothing
        Me.txtPorcRetencion.Location = New System.Drawing.Point(627, 290)
        Me.txtPorcRetencion.Name = "txtPorcRetencion"
        Me.txtPorcRetencion.Size = New System.Drawing.Size(52, 20)
        Me.txtPorcRetencion.SoloLectura = True
        Me.txtPorcRetencion.TabIndex = 8
        Me.txtPorcRetencion.TabStop = False
        Me.txtPorcRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcRetencion.Texto = "0"
        '
        'txtTotalGravado
        '
        Me.txtTotalGravado.Color = System.Drawing.Color.Empty
        Me.txtTotalGravado.Decimales = True
        Me.txtTotalGravado.Indicaciones = Nothing
        Me.txtTotalGravado.Location = New System.Drawing.Point(546, 290)
        Me.txtTotalGravado.Name = "txtTotalGravado"
        Me.txtTotalGravado.Size = New System.Drawing.Size(81, 20)
        Me.txtTotalGravado.SoloLectura = True
        Me.txtTotalGravado.TabIndex = 7
        Me.txtTotalGravado.TabStop = False
        Me.txtTotalGravado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalGravado.Texto = "0"
        '
        'txtRenta
        '
        Me.txtRenta.Color = System.Drawing.Color.Empty
        Me.txtRenta.Decimales = True
        Me.txtRenta.Indicaciones = Nothing
        Me.txtRenta.Location = New System.Drawing.Point(795, 290)
        Me.txtRenta.Name = "txtRenta"
        Me.txtRenta.Size = New System.Drawing.Size(85, 20)
        Me.txtRenta.SoloLectura = True
        Me.txtRenta.TabIndex = 11
        Me.txtRenta.TabStop = False
        Me.txtRenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRenta.Texto = "0"
        '
        'txtPorcRenta
        '
        Me.txtPorcRenta.Color = System.Drawing.Color.Empty
        Me.txtPorcRenta.Decimales = True
        Me.txtPorcRenta.Indicaciones = Nothing
        Me.txtPorcRenta.Location = New System.Drawing.Point(758, 290)
        Me.txtPorcRenta.Name = "txtPorcRenta"
        Me.txtPorcRenta.Size = New System.Drawing.Size(37, 20)
        Me.txtPorcRenta.SoloLectura = True
        Me.txtPorcRenta.TabIndex = 10
        Me.txtPorcRenta.TabStop = False
        Me.txtPorcRenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcRenta.Texto = "0"
        '
        'txtRetencionIVA
        '
        Me.txtRetencionIVA.Color = System.Drawing.Color.Empty
        Me.txtRetencionIVA.Decimales = True
        Me.txtRetencionIVA.Indicaciones = Nothing
        Me.txtRetencionIVA.Location = New System.Drawing.Point(679, 290)
        Me.txtRetencionIVA.Name = "txtRetencionIVA"
        Me.txtRetencionIVA.Size = New System.Drawing.Size(79, 20)
        Me.txtRetencionIVA.SoloLectura = True
        Me.txtRetencionIVA.TabIndex = 9
        Me.txtRetencionIVA.TabStop = False
        Me.txtRetencionIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRetencionIVA.Texto = "0"
        '
        'txtIVA10
        '
        Me.txtIVA10.Color = System.Drawing.Color.Empty
        Me.txtIVA10.Decimales = True
        Me.txtIVA10.Indicaciones = Nothing
        Me.txtIVA10.Location = New System.Drawing.Point(454, 290)
        Me.txtIVA10.Name = "txtIVA10"
        Me.txtIVA10.Size = New System.Drawing.Size(92, 20)
        Me.txtIVA10.SoloLectura = True
        Me.txtIVA10.TabIndex = 6
        Me.txtIVA10.TabStop = False
        Me.txtIVA10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIVA10.Texto = "0"
        '
        'txtTotal10
        '
        Me.txtTotal10.Color = System.Drawing.Color.Empty
        Me.txtTotal10.Decimales = True
        Me.txtTotal10.Indicaciones = Nothing
        Me.txtTotal10.Location = New System.Drawing.Point(355, 290)
        Me.txtTotal10.Name = "txtTotal10"
        Me.txtTotal10.Size = New System.Drawing.Size(99, 20)
        Me.txtTotal10.SoloLectura = False
        Me.txtTotal10.TabIndex = 5
        Me.txtTotal10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal10.Texto = "0"
        '
        'txtIVA5
        '
        Me.txtIVA5.Color = System.Drawing.Color.Empty
        Me.txtIVA5.Decimales = True
        Me.txtIVA5.Indicaciones = Nothing
        Me.txtIVA5.Location = New System.Drawing.Point(269, 290)
        Me.txtIVA5.Name = "txtIVA5"
        Me.txtIVA5.Size = New System.Drawing.Size(86, 20)
        Me.txtIVA5.SoloLectura = True
        Me.txtIVA5.TabIndex = 4
        Me.txtIVA5.TabStop = False
        Me.txtIVA5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIVA5.Texto = "0"
        '
        'txtTotal5
        '
        Me.txtTotal5.Color = System.Drawing.Color.Empty
        Me.txtTotal5.Decimales = True
        Me.txtTotal5.Indicaciones = Nothing
        Me.txtTotal5.Location = New System.Drawing.Point(182, 290)
        Me.txtTotal5.Name = "txtTotal5"
        Me.txtTotal5.Size = New System.Drawing.Size(87, 20)
        Me.txtTotal5.SoloLectura = False
        Me.txtTotal5.TabIndex = 3
        Me.txtTotal5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal5.Texto = "0"
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(CType(0, Long))
        Me.txtFecha.Location = New System.Drawing.Point(103, 290)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(79, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 2
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(7, 290)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(96, 20)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 1
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'colComprobante
        '
        Me.colComprobante.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.colComprobante.DefaultCellStyle = DataGridViewCellStyle2
        Me.colComprobante.HeaderText = "Comprobante"
        Me.colComprobante.Name = "colComprobante"
        Me.colComprobante.ReadOnly = True
        Me.colComprobante.ToolTipText = "Tipo y Comprobante "
        '
        'colFecha
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.NullValue = "---"
        Me.colFecha.DefaultCellStyle = DataGridViewCellStyle3
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.ReadOnly = True
        Me.colFecha.Width = 78
        '
        'colTotal5
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.NullValue = "---"
        Me.colTotal5.DefaultCellStyle = DataGridViewCellStyle4
        Me.colTotal5.HeaderText = "Total5%"
        Me.colTotal5.Name = "colTotal5"
        Me.colTotal5.ReadOnly = True
        Me.colTotal5.Width = 85
        '
        'colIVA5
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = "0"
        Me.colIVA5.DefaultCellStyle = DataGridViewCellStyle5
        Me.colIVA5.HeaderText = "IVA5%"
        Me.colIVA5.Name = "colIVA5"
        Me.colIVA5.ReadOnly = True
        Me.colIVA5.Width = 80
        '
        'colTotal10
        '
        Me.colTotal10.HeaderText = "Total10%"
        Me.colTotal10.Name = "colTotal10"
        Me.colTotal10.ReadOnly = True
        '
        'colIVA10
        '
        Me.colIVA10.HeaderText = "IVA10%"
        Me.colIVA10.Name = "colIVA10"
        Me.colIVA10.ReadOnly = True
        '
        'colTotalGravado
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N0"
        DataGridViewCellStyle6.NullValue = "0"
        Me.colTotalGravado.DefaultCellStyle = DataGridViewCellStyle6
        Me.colTotalGravado.HeaderText = "TotalGravado"
        Me.colTotalGravado.Name = "colTotalGravado"
        Me.colTotalGravado.ReadOnly = True
        Me.colTotalGravado.Width = 80
        '
        'colPorcentajeRetencion
        '
        Me.colPorcentajeRetencion.HeaderText = "Porc. Ret%"
        Me.colPorcentajeRetencion.Name = "colPorcentajeRetencion"
        Me.colPorcentajeRetencion.ReadOnly = True
        Me.colPorcentajeRetencion.Width = 40
        '
        'colRetencionIVA
        '
        Me.colRetencionIVA.HeaderText = "Retencion IVA"
        Me.colRetencionIVA.Name = "colRetencionIVA"
        Me.colRetencionIVA.ReadOnly = True
        Me.colRetencionIVA.Width = 80
        '
        'colPorcentajeRenta
        '
        Me.colPorcentajeRenta.HeaderText = "Porc. Renta%"
        Me.colPorcentajeRenta.Name = "colPorcentajeRenta"
        Me.colPorcentajeRenta.ReadOnly = True
        Me.colPorcentajeRenta.Width = 50
        '
        'colRenta
        '
        Me.colRenta.HeaderText = "RENTA"
        Me.colRenta.Name = "colRenta"
        Me.colRenta.ReadOnly = True
        Me.colRenta.Width = 80
        '
        'frmCargarComprobantesSinOP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(887, 372)
        Me.Controls.Add(Me.txtPorcRetencion)
        Me.Controls.Add(Me.txtTotalGravado)
        Me.Controls.Add(Me.lklEliminarVenta)
        Me.Controls.Add(Me.txtRenta)
        Me.Controls.Add(Me.txtPorcRenta)
        Me.Controls.Add(Me.txtRetencionIVA)
        Me.Controls.Add(Me.txtIVA10)
        Me.Controls.Add(Me.txtTotal10)
        Me.Controls.Add(Me.txtIVA5)
        Me.Controls.Add(Me.txtTotal5)
        Me.Controls.Add(Me.txtFecha)
        Me.Controls.Add(Me.txtComprobante)
        Me.Controls.Add(Me.dgw)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmCargarComprobantesSinOP"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmCargarComprobantesSinOP"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents txtRenta As ERP.ocxTXTNumeric
    Friend WithEvents txtPorcRenta As ERP.ocxTXTNumeric
    Friend WithEvents txtRetencionIVA As ERP.ocxTXTNumeric
    Friend WithEvents txtIVA10 As ERP.ocxTXTNumeric
    Friend WithEvents txtTotal10 As ERP.ocxTXTNumeric
    Friend WithEvents txtIVA5 As ERP.ocxTXTNumeric
    Friend WithEvents txtTotal5 As ERP.ocxTXTNumeric
    Friend WithEvents lklEliminarVenta As System.Windows.Forms.LinkLabel
    Friend WithEvents txtPorcRetencion As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalGravado As ERP.ocxTXTNumeric
    Friend WithEvents colComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotal5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIVA5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotal10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIVA10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotalGravado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPorcentajeRetencion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRetencionIVA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPorcentajeRenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRenta As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿Public Class frmPrepararPago

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Public Property dt As DataTable
    Public Property dtSaldoCuentaBancaria As DataTable
    Public Property Decimales As Boolean

    'VARIABLES
    Dim vdtCuentaBancaria As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'CheckBox
        chkPeriodo.Checked = True

        'Fechas
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()

        cbxUnidadNegocio.cbx.Enabled = False

        'Funciones
        CargarInformacion()

        'Focu
        Me.ActiveControl = txtDesde
        txtDesde.Focus()

    End Sub

    Sub CargarInformacion()

        'vdtCuentaBancaria = CSistema.ExecuteToDataTable("select distinct 'Descripcion' = BA.Referencia+' '+CB.CuentaBancaria+' - '+MO.Referencia from chequera CH join CuentaBancaria CB on CH.IDCuentaBancaria = CB.ID join Banco BA ON CH.IDBanco = BA.ID join Moneda MO on CB.IDMoneda = MO.ID")
        'vdtCuentaBancaria = CSistema.ExecuteToDataTable("select distinct 'Descripcion' = BA.Referencia+' '+CB.CuentaBancaria+' - '+MO.Referencia from chequera CH join CuentaBancaria CB on CH.IDCuentaBancaria = CB.ID join Banco BA ON CH.IDBanco = BA.ID join Moneda MO on CB.IDMoneda = MO.ID")
        vdtCuentaBancaria = CSistema.ExecuteToDataTable("Select distinct 'Descripcion' = BA.Referencia+' '+CB.CuentaBancaria+' - '+MO.Referencia from CuentaBancaria CB JOin Banco BA ON CB.IDBanco = BA.ID join Moneda MO on CB.IDMoneda = MO.ID")
        For Each oRow As DataRow In vdtCuentaBancaria.Rows
            CuentaBancaria.Items.Add(oRow("Descripcion").ToString)
        Next

        CuentaBancaria.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
        CuentaBancaria.CellTemplate.Style.BackColor = Color.White

        'Unidad de Negocio
        CSistema.SqlToComboBox(cbxUnidadNegocio, "Select ID, Descripcion From vUnidadNegocio Where Estado= 1 Order By 2")

        'CUENTA BANCARIA PREDETERMINADA
        cbxCuentaBancaria.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CUENTA BANCARIA", "")

        'ORDENADO
        cbxSaldosBancariosOrdenar.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ORDENAR", "SALDO")

        'ORDENADO DESC
        chkOrdenadoDescendente.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ORDENAR DESC", True)

        'SALDO BANCARIO
        'dtSaldoCuentaBancaria = CSistema.ExecuteToDataTable("Select ID, 'Cuenta'=Descripcion, 'Disponible'=dbo.FSaldoBancario(ID), 'Utilizado'=0.00, 'Saldo'=0.00 From VCuentaBancaria Order By 2 Desc")

        '17082021 - Debido a problemas de consulta, modificamos
        'dtSaldoCuentaBancaria = CSistema.ExecuteToDataTable("Select VCuentaBancaria.ID, 'Cuenta'=Descripcion, 'Contable'=dbo.FSaldoBancario(VCuentaBancaria.ID),'A Confirmar'=(Select sum(DepositoAconfirmar) from VSaldoBancoDiario where IDCuentaBancaria = VCuentaBancaria.ID),'Disponible'=(dbo.FSaldoBancario(VCuentaBancaria.ID) - (Select sum(DepositoAconfirmar) from VSaldoBancoDiario where IDCuentaBancaria = VCuentaBancaria.ID)), 'Utilizado'=0.00, 'Saldo'=0.00 From VCuentaBancaria Order By 2 Desc")
        dtSaldoCuentaBancaria = CSistema.ExecuteToDataTable("Select VCuentaBancaria.ID, 'Cuenta'=Descripcion, 'Contable'=(Select 'Saldo'=(IsNull(Sum(Debito), 0)) - (IsNull(Sum(Credito), 0))  From VExtractoMovimientoBancario Where IDCuentaBancaria=VCuentaBancaria.ID),'A Confirmar'=(Select sum(DepositoAconfirmar) from VSaldoBancoDiario where IDCuentaBancaria = VCuentaBancaria.ID),'Disponible'=((Select 'Saldo'=(IsNull(Sum(Debito), 0)) - (IsNull(Sum(Credito), 0))  From VExtractoMovimientoBancario Where IDCuentaBancaria=VCuentaBancaria.ID)-(Select sum(DepositoAconfirmar) from VSaldoBancoDiario where IDCuentaBancaria = VCuentaBancaria.ID)), 'Utilizado'=0.00, 'Saldo'=0.00 From VCuentaBancaria Order By 2 Desc")
        Listar()

        '17082021 - Debido a problemas de consulta, modificamos
        ListarSaldosBancarios()

        CalcularTotales()

    End Sub

    Sub GuardarInformacion()

        'CUENTA BANCARIA PREDETERMINADA
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CUENTA BANCARIA", cbxCuentaBancaria.cbx.Text)

        'ORDENADO
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ORDENAR", cbxSaldosBancariosOrdenar.Text)

        'ORDENADO DESC
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ORDENAR DESC", chkOrdenadoDescendente.Checked.ToString)

    End Sub

    Sub Listar()

        dgv.Rows.Clear()

        'Descargar comprobantes
        dt = CSistema.ExecuteToDataTable("Exec SpViewEgresosParaOrdenPago " & vgIDSucursal & " ")

        'Ordenar
        CData.OrderDataTable(dt, "Proveedor, FechaVencimiento")

        Dim Index As Integer = 0
        'Filtramos por moneda y sacamos los que son fondo fijo
        Dim Filtro As String = " IDMoneda=" & cbxMoneda.GetValue & " And FondoFijo='False' "
        If chkPeriodo.Checked = True Then
            Filtro = Filtro & " And FechaVencimiento >= '" & txtDesde.GetValue & "' And FechaVencimiento <= '" & txtHasta.GetValue & "' "
        End If

        If chkUnidadNegocio.Checked = True Then
            Filtro = Filtro & " And IDUnidadNegocio= " & cbxUnidadNegocio.GetValue
        End If

        Dim vdttemp As DataView

        vdttemp = New DataView(dt, Filtro, "FechaVencimiento", DataViewRowState.CurrentRows)
        'Ordenar
        vdttemp.Sort = "Proveedor, FechaVencimiento"

        If vdttemp.Count > 0 Then
            dgv.Rows.Add(vdttemp.Count)
        End If

        For Each oRow As DataRow In vdttemp.ToTable.Rows
            dgv.Rows(Index).Cells("IDTransaccion").Value = oRow("IDTransaccion")
            dgv.Rows(Index).Cells("Tipo").Value = oRow("Tipo")
            dgv.Rows(Index).Cells("Codigo").Value = oRow("Codigo")
            dgv.Rows(Index).Cells("Proveedor").Value = oRow("Proveedor")
            dgv.Rows(Index).Cells("Fecha").Value = oRow("Fec")
            dgv.Rows(Index).Cells("TipoDocumento").Value = oRow("TipoComprobante")
            dgv.Rows(Index).Cells("Comprobante").Value = oRow("NroComprobante")
            dgv.Rows(Index).Cells("Cuota").Value = oRow("Cuota").ToString & "/" & oRow("CantidadCuota")
            dgv.Rows(Index).Cells("CantidadCuota").Value = oRow("CantidadCuota")
            dgv.Rows(Index).Cells("Vencimiento").Value = oRow("Fec. Venc.")
            dgv.Rows(Index).Cells("Cotizacion").Value = oRow("Cotizacion")
            dgv.Rows(Index).Cells("Saldo").Value = oRow("Saldo")
            'SC: 06/06/2022 - Nuevo campo habilitado para ajustar vista de RRHH
            dgv.Rows(Index).Cells("RRHH").Value = oRow("RRHH")
            dgv.Rows(Index).Cells("Pagar").Value = oRow("Pagar")
            dgv.Rows(Index).Cells("Importe").Value = oRow("ImporteAPagar")
            dgv.Rows(Index).Cells("CuentaBancaria").Value = oRow("CuentaBancaria")
            dgv.Rows(Index).Cells("Observacion").Value = oRow("ObservacionPago")

            Index = Index + 1

        Next

        'Formatos
        Dim Decimales As Boolean = CSistema.RetornarValorBoolean(CData.GetTable("VMoneda", "ID=" & cbxMoneda.GetValue)(0)("Decimales").ToString)
        CSistema.DataGridColumnasNumericas(dgv, {"Saldo", "Cotizacion", "Importe"}, Decimales)
        CSistema.DataGridColumnasCentradas(dgv, {"Fecha", "Vencimiento", "Cuota"})

        'Tamaños
        dgv.Columns("Fecha").Width = 70
        dgv.Columns("Vencimiento").Width = 70
        dgv.Columns("TipoDocumento").Width = 65
        dgv.Columns("Cotizacion").Width = 45
        dgv.Columns("Comprobante").Width = 110
        dgv.Columns("CuentaBancaria").Width = 165
        dgv.Columns("Saldo").Width = 90
        dgv.Columns("Importe").Width = 90

        'Si es que hay resultados, ponemos el foco en la columna de Pagar
        If dgv.Rows.Count > 0 Then
            dgv.CurrentCell = dgv.Rows(0).Cells("Pagar")
        End If

        If dgv.SelectedRows.Count > 0 Then

            'Deseleccionar todo
            dgv.SelectedRows(0).Selected = False

        End If

        'Pintar los seleccionados
        For Each oRow As DataGridViewRow In dgv.Rows
            If oRow.Cells("Pagar").Value = False Then
                oRow.DefaultCellStyle.BackColor = Color.White
            Else
                oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
            End If
        Next

    End Sub

    Sub CalcularTotales()

        Dim Total As Decimal = CSistema.gridSumColumn(dgv, "Saldo")
        Dim CantidadDocumentos As Integer = dgv.Rows.Count
        Dim TotalAPagar As Decimal = CSistema.gridSumColumn(dgv, "Importe", "Pagar", True)
        Dim CantidadAPagar As Integer = CSistema.gridCountRows(dgv, "Pagar", True)

        Dim Decimales As Boolean = CSistema.RetornarValorBoolean(CData.GetTable("VMoneda", "ID=" & cbxMoneda.GetValue)(0)("Decimales").ToString)
        txtCantidadAPagar.Decimales = Decimales
        txtCantidadDocumentos.Decimales = Decimales
        txtTotalAPagar.Decimales = Decimales
        txtTotalSaldo.Decimales = Decimales

        txtCantidadAPagar.SetValue(CantidadAPagar)
        txtCantidadDocumentos.SetValue(CantidadDocumentos)
        txtTotalAPagar.SetValue(TotalAPagar)
        txtTotalSaldo.SetValue(Total)

        'Calcular los saldos bancarios
        '17082021 - Debido a problemas de consulta, modificamos
        CalcularSaldoBancario()

    End Sub

    Sub CalcularSaldoBancario()

        'Cerrar los totales
        For Each oRow As DataRow In dtSaldoCuentaBancaria.Rows
            oRow("Utilizado") = 0
        Next

        For Each orow As DataGridViewRow In dgv.Rows

            If orow.Cells("Pagar").Value = False Then
                GoTo Siguiente
            End If

            Dim CuentaBancaria As String = orow.Cells("CuentaBancaria").Value.ToString

            Dim Total As Decimal = orow.Cells("Importe").Value

            For Each CuentaBancariaRow As DataRow In dtSaldoCuentaBancaria.Select(" Cuenta = '" & CuentaBancaria & "' ")
                CuentaBancariaRow("Utilizado") = CuentaBancariaRow("Utilizado") + Total
                CuentaBancariaRow("Saldo") = CuentaBancariaRow("Disponible") - CuentaBancariaRow("Utilizado")
            Next

Siguiente:

        Next

        dgvSaldosBancarios.CurrentCell = dgvSaldosBancarios.Rows(0).Cells(1)
        dgvSaldosBancarios.ClearSelection()

    End Sub

    Sub Guardar()

        For Each oRow As DataGridViewRow In dgv.Rows
            'SC: 26/04/2022 Se Comenta la linea de abajo por necesidad de guardar datos siempre.
            ' If oRow.Cells("Pagar").Value = "True" Then
            Dim SQL As String = "Exec SpPrepararPago "
            CSistema.ConcatenarParametro(SQL, "@IDTransaccion", oRow.Cells("IDTransaccion").Value)
            CSistema.ConcatenarParametro(SQL, "@Pagar", oRow.Cells("Pagar").Value)
            CSistema.ConcatenarParametro(SQL, "@ImporteAPagar", CSistema.FormatoMonedaBaseDatos(oRow.Cells("Importe").Value, Decimales))
            CSistema.ConcatenarParametro(SQL, "@CuentaBancaria", oRow.Cells("CuentaBancaria").Value.ToString)
            CSistema.ConcatenarParametro(SQL, "@Observacion", oRow.Cells("Observacion").Value.ToString)
            CSistema.ConcatenarParametro(SQL, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(oRow.Cells("Cotizacion").Value))

            'Transaccion
            CSistema.ConcatenarParametro(SQL, "@IDUsuario", vgIDUsuario)


            Dim Cuota As String = oRow.Cells("Cuota").Value.ToString
            If Cuota.Contains("/") Then
                Cuota = Cuota.Split("/")(0)
            Else
                Cuota = "1"
            End If

            CSistema.ConcatenarParametro(SQL, "@Cuota", Cuota)

            'Procesar
            Dim vdt As DataTable = CSistema.ExecuteToDataTable(SQL)

            'Validar
            If vdt Is Nothing Then
                Exit Sub
            End If

            If vdt.Rows.Count = 0 Then
                Exit Sub
            End If

            Dim ResultadoRow As DataRow = vdt.Rows(0)

            If ResultadoRow("Procesado") = False Then
                If MessageBox.Show("El comprobante " & oRow.Cells("Comprobante").Value & " del Proveedor: " & oRow.Cells("Proveedor").Value & " no se guardo correctamente! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                    Exit For
                End If
            End If
            'End If

        Next

        MessageBox.Show("Registros guardados!", "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Listar()

        'Imprimir
        If MessageBox.Show("Desea imprimir?", "Imprimir", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
            Imprimir()
        End If


    End Sub

    Sub Imprimir()

        Dim frm As New frmPrepararPagoImprimir
        frm.dt = dt
        frm.Desde = txtDesde.GetValue
        frm.Hasta = txtHasta.GetValue
        frm.Moneda = cbxMoneda.Texto

        FGMostrarFormulario(Me, frm, "Imprimir Egresos", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Sub CambiarValor()

        'Si estamos posicionados en el campo Pagar
        If dgv.Columns(dgv.CurrentCell.ColumnIndex).Name = "Pagar" Then

            If dgv.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgv.SelectedCells.Item(0).RowIndex

            For Each oRow As DataGridViewRow In dgv.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("Pagar").Value = False Then
                        oRow.Cells("Pagar").Value = True
                        oRow.Cells("Importe").ReadOnly = False
                        oRow.Cells("Observacion").ReadOnly = False
                        oRow.Cells("CuentaBancaria").ReadOnly = False
                        oRow.Cells("Cotizacion").ReadOnly = False
                        oRow.Cells("Importe").Value = oRow.Cells("Saldo").Value
                        oRow.Cells("CuentaBancaria").Value = cbxCuentaBancaria.cbx.Text
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        'If CambiarFila = False Then
                        '    dgv.CurrentCell = dgv.Rows(oRow.Index).Cells("Importe")
                        'End If

                    Else
                        oRow.Cells("Pagar").Value = False
                        oRow.Cells("Importe").ReadOnly = True
                        oRow.Cells("Observacion").ReadOnly = True
                        oRow.Cells("CuentaBancaria").ReadOnly = True
                        oRow.Cells("Cotizacion").ReadOnly = True
                        oRow.Cells("Importe").Value = 0
                        oRow.Cells("CuentaBancaria").Value = ""
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    'If CambiarFila = True Then
                    '    If RowIndex < dgv.Rows.Count - 1 Then
                    '        dgv.CurrentCell = dgv.Rows(RowIndex + 1).Cells("Pagar")
                    '    Else
                    '        btnGuardar.Focus()
                    '    End If
                    'End If

                    Exit For

                End If

            Next

            CalcularTotales()
            dgv.Update()
            Exit Sub

        End If

    End Sub

    Sub VisualizarDocumento()

        If dgv.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        Dim Tipo As String = dgv.Rows(dgv.CurrentCell.RowIndex).Cells("Tipo").Value
        Dim IDTransaccion As Integer = dgv.Rows(dgv.CurrentCell.RowIndex).Cells("IDTransaccion").Value
        'SC: 06/06/2022 - Nuevo campo habilitado para ajustar vista de RRHH
        Dim RRHH As Boolean = dgv.Rows(dgv.CurrentCell.RowIndex).Cells("RRHH").Value


        Select Case Tipo
            Case "COMPRA"
                Dim frm As New frmCompra
                frm.IDTransaccion = IDTransaccion
                FGMostrarFormulario(Me, frm, "Compra de Mercaderias", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)
            Case "GASTO"
                Dim frm As New frmGastos
                frm.IDTransaccion = IDTransaccion
                frm.CajaChica = False
                'SC: 06/06/2022 - Nuevo campo habilitado para ajustar vista de RRHH
                frm.RRHH = RRHH
                FGMostrarFormulario(Me, frm, "Gastos", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)
            Case "FONDO FIJO"
                Dim frm As New frmGastos
                frm.IDTransaccion = IDTransaccion
                frm.CajaChica = True
                FGMostrarFormulario(Me, frm, "Gastos", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)
            Case "VALE"
                Dim frm As New frmVale
                frm.IDTransaccion = IDTransaccion
                FGMostrarFormulario(Me, frm, "Vales", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)
        End Select

    End Sub

    Sub DefinirEstiloGrillaSaldos()

        Dim Fuente As Font = New Font(Me.Font.FontFamily, 7, FontStyle.Regular)

        dgvSaldosBancarios.BackgroundColor = Me.BackColor
        dgvSaldosBancarios.RowHeadersVisible = False
        dgvSaldosBancarios.DefaultCellStyle.Font = Fuente
        dgvSaldosBancarios.DefaultCellStyle.ForeColor = Color.DarkSlateGray
        dgvSaldosBancarios.SelectionMode = DataGridViewSelectionMode.FullRowSelect

        dgvSaldosBancarios.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None
        dgvSaldosBancarios.ColumnHeadersDefaultCellStyle.Font = Fuente
        For c As Integer = 0 To dgvSaldosBancarios.ColumnCount - 1
            dgvSaldosBancarios.Columns(c).DefaultCellStyle.Font = Fuente
            dgvSaldosBancarios.Columns(c).DividerWidth = 0
            dgvSaldosBancarios.Columns(c).ReadOnly = True
        Next

    End Sub

    Sub ListarSaldosBancarios()

        CSistema.dtToGrid(dgvSaldosBancarios, dtSaldoCuentaBancaria)
        DefinirEstiloGrillaSaldos()

        dgvSaldosBancarios.BorderStyle = BorderStyle.None

        'Formato
        dgvSaldosBancarios.Columns("ID").Visible = False
        dgvSaldosBancarios.Columns("Cuenta").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvSaldosBancarios.Columns("Disponible").AutoSizeMode = DataGridViewAutoSizeColumnMode.None
        dgvSaldosBancarios.Columns("Utilizado").AutoSizeMode = DataGridViewAutoSizeColumnMode.None
        dgvSaldosBancarios.Columns("Saldo").AutoSizeMode = DataGridViewAutoSizeColumnMode.None
        dgvSaldosBancarios.Columns("A Confirmar").AutoSizeMode = DataGridViewAutoSizeColumnMode.None
        dgvSaldosBancarios.Columns("Contable").AutoSizeMode = DataGridViewAutoSizeColumnMode.None
        dgvSaldosBancarios.Columns("Disponible").Width = 90
        dgvSaldosBancarios.Columns("Utilizado").Width = 90
        dgvSaldosBancarios.Columns("Saldo").Width = 90
        dgvSaldosBancarios.Columns("A Confirmar").Width = 90
        dgvSaldosBancarios.Columns("Contable").Width = 90
        dgvSaldosBancarios.Columns("Disponible").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvSaldosBancarios.Columns("Utilizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvSaldosBancarios.Columns("Saldo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvSaldosBancarios.Columns("A Confirmar").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvSaldosBancarios.Columns("Contable").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvSaldosBancarios.Columns("Disponible").DefaultCellStyle.Format = "N0"
        dgvSaldosBancarios.Columns("Utilizado").DefaultCellStyle.Format = "N0"
        dgvSaldosBancarios.Columns("Saldo").DefaultCellStyle.Format = "N0"
        dgvSaldosBancarios.Columns("A Confirmar").DefaultCellStyle.Format = "N0"
        dgvSaldosBancarios.Columns("Contable").DefaultCellStyle.Format = "N0"


        dgvSaldosBancarios.CellBorderStyle = DataGridViewCellBorderStyle.None
        dgvSaldosBancarios.ColumnHeadersHeight = 20
        dgvSaldosBancarios.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        dgvSaldosBancarios.ColumnHeadersDefaultCellStyle.BackColor = Me.BackColor
        dgvSaldosBancarios.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None

        'Ordenar
        OrdenarSaldosCuentasBancarias()

    End Sub

    Sub OrdenarSaldosCuentasBancarias()

        'Validar
        If dgvSaldosBancarios.DataSource Is Nothing Then
            Exit Sub
        End If

        If dgvSaldosBancarios.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim Tipo As String = cbxSaldosBancariosOrdenar.Text
        Dim Forma As System.ComponentModel.ListSortDirection = System.ComponentModel.ListSortDirection.Ascending

        If chkOrdenadoDescendente.Checked Then
            Forma = System.ComponentModel.ListSortDirection.Descending
        End If

        dgvSaldosBancarios.Sort(dgvSaldosBancarios.Columns(Tipo), Forma)

    End Sub

    Sub SeleccionarTodos()

        'Validar
        If dgv.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each oRow As DataGridViewRow In dgv.Rows

            'Si ya esta a pagar, no tocar
            If oRow.Cells("Pagar").Value = True Then
                GoTo Seguir
            End If

            oRow.Cells("Pagar").Value = True
            oRow.Cells("Importe").ReadOnly = False
            oRow.Cells("Observacion").ReadOnly = False
            oRow.Cells("CuentaBancaria").ReadOnly = False
            oRow.Cells("Cotizacion").ReadOnly = False
            oRow.Cells("Importe").Value = oRow.Cells("Saldo").Value
            oRow.Cells("CuentaBancaria").Value = cbxCuentaBancaria.cbx.Text
            oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
Seguir:
        Next

        CalcularTotales()

    End Sub

    Sub QuitarSeleccionar()

        'Validar
        If dgv.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each oRow As DataGridViewRow In dgv.Rows
            oRow.Cells("Pagar").Value = False
            oRow.Cells("Importe").ReadOnly = True
            oRow.Cells("Observacion").ReadOnly = True
            oRow.Cells("CuentaBancaria").ReadOnly = True
            oRow.Cells("Cotizacion").ReadOnly = True
            oRow.Cells("Importe").Value = 0
            oRow.Cells("CuentaBancaria").Value = ""
            oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
        Next

        CalcularTotales()

    End Sub

    Private Sub frmPrepararPago_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
        'FA 20/06/2023
        Me.Dispose()
    End Sub

    Private Sub frmPrepararPago_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown, Me.KeyUp

        If e.KeyCode = Keys.Enter Then

            If dgv.Focused Then
                Exit Sub
            End If

            CSistema.SelectNextControl(Me, e.KeyCode)

        End If

    End Sub

    Private Sub frmPrepararPago_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(sender As System.Object, e As System.EventArgs) Handles btnListar.Click
        Listar()
        CalcularTotales()
    End Sub

    Private Sub dgv_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick

        If dgv.Columns(dgv.CurrentCell.ColumnIndex).Name = "Pagar" Then
            CambiarValor()
        End If

        If dgv.Columns(dgv.CurrentCell.ColumnIndex).Name = "Visualizar" Then
            VisualizarDocumento()
        End If

    End Sub

    Private Sub dgv_CellEndEdit(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellEndEdit

        If dgv.Columns(dgv.CurrentCell.ColumnIndex).Name = "Importe" Then

            'Validar
            If IsNumeric(dgv.Rows(e.RowIndex).Cells("Importe").Value) = False Then
                dgv.Rows(e.RowIndex).Cells("Importe").Value = 0
                MessageBox.Show("El importe no es correcto!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

            If CDec(dgv.Rows(e.RowIndex).Cells("Importe").Value) > CDec(dgv.Rows(e.RowIndex).Cells("Saldo").Value) Then
                MessageBox.Show("El importe no puede ser mayor al saldo!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                dgv.Rows(e.RowIndex).Cells("Importe").Value = CSistema.FormatoMoneda(dgv.Rows(e.RowIndex).Cells("Saldo").Value, Decimales)
                Exit Sub
            End If

            dgv.Rows(e.RowIndex).Cells("Importe").Value = CSistema.FormatoMoneda(dgv.Rows(e.RowIndex).Cells("Importe").Value, Decimales)
            'dgv.CurrentCell = dgv.Rows(e.RowIndex).Cells("CuentaBancaria")
            'SendKeys.Send("{up}")
            'SendKeys.Send("{right}")
            CalcularTotales()
            dgv.Update()

            Exit Sub

        End If

        If dgv.Columns(dgv.CurrentCell.ColumnIndex).Name = "CuentaBancaria" Then

            'Solo si se mantiene en el foco
            If Me.ActiveControl.Name = dgv.Name Then
                'SendKeys.Send("{up}")
                'SendKeys.Send("{right}")
                'dgv.CurrentCell = dgv.Rows(e.RowIndex).Cells("Observacion")
                CalcularTotales()
                dgv.Update()
            End If
            

            Exit Sub

        End If

    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnImprimir_Click(sender As System.Object, e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub dgv_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyUp

        If e.KeyCode = Keys.Tab Then
            If dgv.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            dgv.CurrentCell = dgv.Rows(dgv.CurrentRow.Index).Cells("Pagar")
        End If

        If e.KeyCode = Keys.Enter Then

            e.SuppressKeyPress = True


        End If
    End Sub

    Private Sub dgv_RowLeave(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.RowLeave

        Dim f2 As New Font(dgv.DefaultCellStyle.Font.FontFamily.Name, dgv.DefaultCellStyle.Font.Size, FontStyle.Regular)

        dgv.Rows(e.RowIndex).DefaultCellStyle.Font = f2

        If dgv.Rows(e.RowIndex).Cells("Pagar").Value = False Then
            dgv.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
        Else
            dgv.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.PaleTurquoise
        End If

    End Sub

    Private Sub dgv_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.RowEnter

        Dim f1 As New Font(dgv.DefaultCellStyle.Font.Name, dgv.DefaultCellStyle.Font.Size, FontStyle.Regular)

        If dgv.Rows(e.RowIndex).Cells("Pagar").Value = False Then
            dgv.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
        End If

        dgv.Rows(e.RowIndex).DefaultCellStyle.Font = f1

    End Sub

    Private Sub dgv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyDown

        If e.KeyCode = Keys.Space Or e.KeyCode = Keys.Enter Then

            If dgv.CurrentCell Is Nothing Then
                Exit Sub
            End If

            'Dim CambiarFila As Boolean = False
            'If e.KeyCode = Keys.Enter Then
            '    CambiarFila = True
            'End If

            'If dgv.Columns(dgv.CurrentCell.ColumnIndex).Name = "Pagar" Then
            '    CambiarValor(CambiarFila)
            '    Exit Sub
            'End If

            If dgv.Columns(dgv.CurrentCell.ColumnIndex).Name = "Importe" Then

                If e.KeyCode = Keys.Enter Then
                    dgv.CurrentCell = dgv.Rows(dgv.SelectedCells.Item(0).RowIndex).Cells("CuentaBancaria")
                    CuentaBancaria.Frozen = True

                    CalcularTotales()

                    dgv.Update()

                    ' Your code here
                    e.SuppressKeyPress = True

                End If

                Exit Sub

            End If

            If dgv.Columns(dgv.CurrentCell.ColumnIndex).Name = "CuentaBancaria" Then
                If e.KeyCode = Keys.Enter Then
                    dgv.CurrentCell = dgv.Rows(dgv.SelectedCells.Item(0).RowIndex).Cells("Observacion")

                    dgv.Update()

                    ' Your code here
                    e.SuppressKeyPress = True

                End If

               
                Exit Sub

            End If

            If dgv.Columns(dgv.CurrentCell.ColumnIndex).Name = "Observacion" Then
                If e.KeyCode = Keys.Enter Then

                    If dgv.SelectedCells.Item(0).RowIndex < dgv.Rows.Count - 1 Then
                        dgv.CurrentCell = dgv.Rows(dgv.SelectedCells.Item(0).RowIndex + 1).Cells("Pagar")
                    Else
                        btnGuardar.Focus()
                    End If


                    dgv.Update()

                    ' Your code here
                    e.SuppressKeyPress = True

                End If


                Exit Sub

            End If

        End If

    End Sub

    Private Sub btnListar_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles btnListar.KeyUp
        If e.KeyCode = Keys.Enter Then
            cbxCuentaBancaria.Focus()
        End If
    End Sub

    Private Sub dgv_PreviewKeyDown(sender As Object, e As System.Windows.Forms.PreviewKeyDownEventArgs) Handles dgv.PreviewKeyDown

    End Sub

    Private Sub cbxMoneda_Leave(sender As System.Object, e As System.EventArgs) Handles cbxMoneda.Leave
        Decimales = CData.GetRow("ID='" & cbxMoneda.GetValue & "'", "VMoneda")("Decimales")
    End Sub

    Private Sub dgv_CellEnter(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellEnter
        If dgv.Columns(e.ColumnIndex).Name = "CuentaBancaria" Then
            SendKeys.Send("{F4}")
        End If
    End Sub

    Private Sub chkPeriodo_CheckedChanged(sender As System.Object, e As System.EventArgs)
        txtDesde.Enabled = chkPeriodo.Checked
        txtHasta.Enabled = chkPeriodo.Checked
        lklEstaSemana.Enabled = chkPeriodo.Checked
        lklEsteMes.Enabled = chkPeriodo.Checked
    End Sub

    Private Sub dgvSaldosBancarios_SelectionChanged(sender As Object, e As System.EventArgs) Handles dgvSaldosBancarios.SelectionChanged
        dgvSaldosBancarios.ClearSelection()
    End Sub

    Private Sub cbxSaldosBancariosOrdenar_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxSaldosBancariosOrdenar.SelectedIndexChanged
        OrdenarSaldosCuentasBancarias()
    End Sub

    Private Sub chkOrdenadoDescendente_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkOrdenadoDescendente.CheckedChanged
        OrdenarSaldosCuentasBancarias()
    End Sub

    Private Sub btnSeleccionarTodos_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionarTodos.Click
        SeleccionarTodos()
    End Sub

    Private Sub btnQuitarSeleccion_Click(sender As System.Object, e As System.EventArgs) Handles btnQuitarSeleccion.Click
        QuitarSeleccionar()
    End Sub

    Private Sub LinkLabel2_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEstaSemana.LinkClicked
        txtDesde.PrimerDiaSemana()
        txtHasta.UltimoDiaSemana()
        Listar()
    End Sub

    Private Sub LinkLabel3_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEsteMes.LinkClicked
        txtDesde.PrimerDiaMes()
        txtHasta.UltimoDiaMes()
        Listar()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmPrepararPago_Activate()
        Me.Refresh()
    End Sub

    Private Sub chkUnidadNegocio_CheckedChanged(sender As Object, e As EventArgs) Handles chkUnidadNegocio.CheckedChanged
        If chkUnidadNegocio.Checked = True Then
            cbxUnidadNegocio.cbx.Enabled = True
        Else
            cbxUnidadNegocio.cbx.Enabled = False
        End If
    End Sub

End Class
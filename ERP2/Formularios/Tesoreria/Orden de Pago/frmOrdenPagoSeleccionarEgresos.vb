﻿Public Class frmOrdenPagoSeleccionarEgresos

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim SelTodo As Boolean = False
    Dim FondoFijo As Boolean = True

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property

    Public Property IDMoneda As Integer
    Public Property Decimales As Boolean

    'VARIABLES
    Dim vCargado As Boolean
    'Private dt As DataTable

    'FUNCIONES

    Sub Inicializar()

        vCargado = False

        'Propiedades
        Decimales = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & IDMoneda, "VMoneda")("Decimales"))

        'Funciones
        Listar()
        Cargar()

        'Foco
        dgw.Focus()

    End Sub

    Sub Listar()

        Dim TotalDeuda As Decimal = 0
        Dim vdt As DataTable

        vCargado = False

        'Filtramos por moneda, Proveedor 
        If IDProveedor > 0 Then
            'SC: 05-08-2021, Agregar nuevo Filtro de FondoFijo='False' para evitar inconsistencias al Aplicar Pago
            vdt = CData.FiltrarDataTable(dt, " IDProveedor = " & IDProveedor & " And IdMoneda =" & IDMoneda & " And FondoFijo = 'False' ")
        End If

        dgw.DataSource = Nothing
        vdt.Columns("Seleccionado").SetOrdinal(0)
        vdt.Columns("Observacion").SetOrdinal(vdt.Columns.Count - 1)

        CSistema.dtToGrid(dgw, vdt)

        Dim ColumnasVisibles() As String = {"Seleccionado", "Numero", "Comprobante", "Proveedor", "Fecha", "FechaVencimiento", "Total", "Saldo", "Importe", "Observacion", "Cancelar", "IVA", "RetencionIVA"}
        Dim ColumnasNumericas() As String = {"Total", "Saldo", "Importe", "IVA", "RetencionIVA"}
        Dim CantidadDecimal As Integer = 0

        If Decimales = True Then
            CantidadDecimal = 2
        End If

        'Formato
        For Each c As DataGridViewColumn In dgw.Columns

            'Habilitar solo el campo de seleccion
            If c.Name <> "Seleccionado" Then
                c.ReadOnly = True
            End If

            'Ocultar los que no son visibles
            If ColumnasVisibles.Contains(c.Name) = False Then
                c.Visible = False
            End If

            'Formatos numericos
            If ColumnasNumericas.Contains(c.Name) = True Then
                c.DefaultCellStyle.Format = "N" & CantidadDecimal
                c.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End If

            'AutoSize
            Select Case c.Name
                Case "Seleccionado", "Cancelar", "Observacion"
                    c.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
                Case "Importe", "Saldo", "IVA", "RetencionIVA", "Total"
                    c.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                Case "Numero"
                    c.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                Case Else
                    c.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
            End Select

        Next

        'Habilitar celdas de los que ya estan seleccionados
        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Cells("Seleccionado").Value = True Then
                oRow.Cells("Importe").ReadOnly = False
                oRow.Cells("IVA").ReadOnly = False
                oRow.Cells("RetencionIVA").ReadOnly = False
            End If
        Next

        'Ocultar el proveedor si es que este ya se selecciono en la OP
        If IDProveedor <> 0 Then
            dgw.Columns("Proveedor").Visible = False
        End If

        'Nombres de Columnas
        dgw.Columns("Seleccionado").HeaderText = "Sel"
        dgw.Columns("Numero").HeaderText = "NroOper"
        dgw.Columns("FechaVencimiento").HeaderText = "Venc."
        dgw.Columns("Cancelar").HeaderText = "Canc."
        dgw.Columns("RetencionIVA").HeaderText = "Ret. IVA"

        dgw.Columns("Observacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        dgw.SelectionMode = DataGridViewSelectionMode.CellSelect

        TotalDeuda = CSistema.dtSumColumn(vdt, "Saldo")
        txtTotalComprobante.SetValue(TotalDeuda)
        txtCantidadComprobante.SetValue(dt.Rows.Count)

    End Sub

    Sub ListarComprobantes()

        'Limpiar la grilla
        dgw.DataSource = Nothing
        Dim TotalDeuda As Decimal = 0

        Dim vdt As DataTable = CData.FiltrarDataTable(dt, " FondoFijo=" & FondoFijo & " And IDMoneda=" & IDMoneda)
        CSistema.dtToGrid(dgw, vdt)

        'Formato
        TotalDeuda = CSistema.dtSumColumn(vdt, "Saldo")
        txtTotalComprobante.SetValue(TotalDeuda)
        txtCantidadComprobante.SetValue(dt.Rows.Count)


    End Sub

    Sub Seleccionar()

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells("IDTransaccion").Value
            Dim Cuota As Integer = oRow.Cells("Cuota").Value

            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & IDTransaccion & " and Cuota = " & Cuota)
                oRow1("Seleccionado") = oRow.Cells("Seleccionado").Value
                oRow1("Importe") = oRow.Cells("Importe").Value
                oRow1("RetencionIVA") = oRow.Cells("RetencionIVA").Value
                oRow1("IVA") = oRow.Cells("IVA").Value
                oRow1("Cancelar") = oRow.Cells("Cancelar").Value

                If oRow.Cells("RetencionIVA").Value > 0 Then
                    oRow1("Retener") = True
                Else
                    oRow1("Retener") = False
                End If
            Next

        Next

        Me.Close()

    End Sub

    Sub Cargar()

        PintarCelda()
        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim TotalPagado As Decimal = 0
        Dim CantidadPagado As Integer = 0

        If dgw.Columns.Count = 0 Then
            Exit Sub
        End If

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Cells("Seleccionado").Value = True Then
                TotalPagado = TotalPagado + oRow.Cells("Importe").Value
                CantidadPagado = CantidadPagado + 1
            End If
        Next

        txtTotalPagado.SetValue(TotalPagado)
        txtCantidadPagado.SetValue(CantidadPagado)

        txtTotalComprobante.SetValue(CSistema.gridSumColumn(dgw, "Total"))
        txtCantidadComprobante.SetValue(dgw.RowCount)

    End Sub

    Sub PintarCelda()

        'If dgw.Rows.Count = 0 Then
        '    Exit Sub
        'End If

        'For Each Row As DataGridViewRow In dgw.Rows
        '    If Row.Cells(1).Value = True Then
        '        Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
        '    Else
        '        Row.DefaultCellStyle.BackColor = Color.White
        '    End If
        'Next

        'dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub

    Sub SeleccionarTodo()
        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells("Seleccionado").Value = True
        Next
        SelTodo = True
        lnkSeleccionar.Text = "Quitar Todo"
        PintarCelda()
        CalcularTotales()
    End Sub

    Sub QuitarTodo()
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells("Seleccionado").Value = False
        Next
        SelTodo = False
        lnkSeleccionar.Text = "Seleccionar Todo"
        PintarCelda()
        CalcularTotales()
    End Sub

    Sub ObtenerComprobantes()
        If FondoFijo = True Then
            ListarComprobantes()
            FondoFijo = False
            lnkComprobantes.Text = "Comprobantes Sin Fondo Fijo"
        Else
            ListarComprobantes()
            FondoFijo = True
            lnkComprobantes.Text = "Comprobantes Fondo Fijo"
        End If
    End Sub

    Private Sub frmCobranzaCreditoSeleccionarVentas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        vCargado = True
    End Sub

    Private Sub frmOrdenPagoSeleccionarEgresos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        vCargado = True
    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick

        ctrError.Clear()
        tsslEstado.Text = ""

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        If e.ColumnIndex = dgw.Columns("Seleccionado").Index Then

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("Seleccionado").Value = False Then
                        oRow.Cells("Seleccionado").Value = True
                        oRow.Cells("Seleccionado").ReadOnly = False
                        oRow.Cells("Importe").ReadOnly = False
                        oRow.Cells("RetencionIVA").ReadOnly = False
                        oRow.Cells("IVA").ReadOnly = False
                        oRow.Cells("Cancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("Importe")

                    Else
                        oRow.Cells("Seleccionado").ReadOnly = True
                        oRow.Cells("Importe").ReadOnly = True
                        oRow.Cells("RetencionIVA").ReadOnly = True
                        oRow.Cells("IVA").ReadOnly = True
                        oRow.Cells("Cancelar").ReadOnly = True
                        oRow.Cells("Importe").Value = oRow.Cells("Saldo").Value
                        oRow.Cells("Seleccionado").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()
        End If

    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Columns(e.ColumnIndex).Name = "Importe" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("Saldo").Value

            Else
                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("Importe").Value
                dgw.CurrentRow.Cells("RetencionIVA").Value = 0
                dgw.CurrentRow.Cells("IVA").Value = 0
                Dim mensaje As String = "Debe Ingresar un importe para el IVA y la Retencion!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                MessageBox.Show("Debe Ingresar el Importe de Retencion", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            CalcularTotales()

        End If

        'si se modifica la Retencion
        If dgw.Columns(e.ColumnIndex).Name = "RetencionIVA" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe de Retencion IVA debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = 0

            Else
                ctrError.SetError(dgw, "")
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = ""
            End If

            CalcularTotales()

        End If

        'si se modifica el IVA
        If dgw.Columns(e.ColumnIndex).Name = "IVA" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe del IVA debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = 0

            Else
                ctrError.SetError(dgw, "")
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = ""
            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Space Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("Seleccionado").Value = False Then
                        oRow.Cells("Seleccionado").Value = True
                        oRow.Cells("Seleccionado").ReadOnly = False
                        oRow.Cells("Importe").ReadOnly = False
                        oRow.Cells("RetencionIVA").ReadOnly = False
                        oRow.Cells("IVA").ReadOnly = False
                        oRow.Cells("Cancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        'dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("Importe")

                    Else
                        oRow.Cells("Seleccionado").ReadOnly = True
                        oRow.Cells("Importe").ReadOnly = True
                        oRow.Cells("RetencionIVA").ReadOnly = True
                        oRow.Cells("IVA").ReadOnly = True
                        oRow.Cells("Cancelar").ReadOnly = True
                        oRow.Cells("Importe").Value = oRow.Cells("Saldo").Value
                        oRow.Cells("Seleccionado").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If

        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("Seleccionado").Value = False Then
                        oRow.Cells("Seleccionado").Value = True
                        oRow.Cells("Seleccionado").ReadOnly = False
                        oRow.Cells("Importe").ReadOnly = False
                        oRow.Cells("RetencionIVA").ReadOnly = False
                        oRow.Cells("IVA").ReadOnly = False
                        oRow.Cells("Cancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                    Else
                        oRow.Cells("Seleccionado").ReadOnly = True
                        oRow.Cells("Importe").ReadOnly = True
                        oRow.Cells("RetencionIVA").ReadOnly = True
                        oRow.Cells("IVA").ReadOnly = True
                        oRow.Cells("Cancelar").ReadOnly = True
                        oRow.Cells("Importe").Value = oRow.Cells("Saldo").Value
                        oRow.Cells("Seleccionado").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For


                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("Importe")
            End If

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If


    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        If vCargado = False Then
            Exit Sub
        End If

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)

        If dgw.Rows(e.RowIndex).Cells("Seleccionado").Value = False Then
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
        End If

        dgw.Rows(e.RowIndex).DefaultCellStyle.Font = f1

    End Sub

    Private Sub dgw_RowLeave(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowLeave

        If vCargado = False Then
            Exit Sub
        End If

        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        dgw.Rows(e.RowIndex).DefaultCellStyle.Font = f2

        If dgw.Rows(e.RowIndex).Cells("Seleccionado").Value = False Then
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
        Else
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.PaleTurquoise
        End If

    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp

        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            'dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells("Importe")
        End If

    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeleccionarTodoToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = True
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuitarTodaSeleccionToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = False
        Next

        PintarCelda()
        CalcularTotales()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Seleccionar()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSeleccionar.LinkClicked
        If SelTodo = False Then
            SeleccionarTodo()
        Else
            QuitarTodo()
        End If
    End Sub

    Private Sub lnkComprobantes_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkComprobantes.LinkClicked
        ObtenerComprobantes()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmOrdenPagoSeleccionarEgresos_Activate()
        Me.Refresh()
    End Sub

End Class
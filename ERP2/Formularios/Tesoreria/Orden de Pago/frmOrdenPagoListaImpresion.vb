﻿Public Class frmOrdenPagoListaImpresion

    'CLASE
    Dim CSistema As New CSistema

    'VARIABLES
    Public dt As DataTable
    Sub Inicializar()

        'Recorrer la tabla
        For Each oRow As DataRow In dt.Rows

            Dim Registro(8) As String

            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Numero").ToString
            Registro(2) = oRow("Fec").ToString
            Registro(3) = oRow("FechaPago").ToString
            Registro(4) = CSistema.FormatoNumero(oRow("Importe").ToString)
            Registro(5) = oRow("NroCheque").ToString
            Registro(6) = oRow("Banco").ToString
            Registro(7) = oRow("AlaOrden").ToString
            Registro(8) = oRow("Estado").ToString

            dgwOperacion.Rows.Add(Registro)

        Next

    End Sub

    Private Sub frmOrdenPagoListaImpresion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmOrdenPagoListaImpresion_Activate()
        Me.Refresh()
    End Sub
End Class
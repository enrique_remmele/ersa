﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrepararPagoImprimir
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxTipo = New System.Windows.Forms.GroupBox()
        Me.rdbDentroRango = New System.Windows.Forms.RadioButton()
        Me.rdbTodosPendientesDentroRango = New System.Windows.Forms.RadioButton()
        Me.rdbTodos = New System.Windows.Forms.RadioButton()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.chkIncluirMonedas = New ERP.ocxCHK()
        Me.txtMoneda = New ERP.ocxTXTString()
        Me.gbxTipo.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxTipo
        '
        Me.gbxTipo.Controls.Add(Me.txtMoneda)
        Me.gbxTipo.Controls.Add(Me.chkIncluirMonedas)
        Me.gbxTipo.Controls.Add(Me.rdbTodos)
        Me.gbxTipo.Controls.Add(Me.rdbTodosPendientesDentroRango)
        Me.gbxTipo.Controls.Add(Me.rdbDentroRango)
        Me.gbxTipo.Location = New System.Drawing.Point(12, 12)
        Me.gbxTipo.Name = "gbxTipo"
        Me.gbxTipo.Size = New System.Drawing.Size(301, 159)
        Me.gbxTipo.TabIndex = 0
        Me.gbxTipo.TabStop = False
        Me.gbxTipo.Text = "Seleccione el tipo de reporte"
        '
        'rdbDentroRango
        '
        Me.rdbDentroRango.AutoSize = True
        Me.rdbDentroRango.Checked = True
        Me.rdbDentroRango.Location = New System.Drawing.Point(33, 38)
        Me.rdbDentroRango.Name = "rdbDentroRango"
        Me.rdbDentroRango.Size = New System.Drawing.Size(175, 17)
        Me.rdbDentroRango.TabIndex = 0
        Me.rdbDentroRango.TabStop = True
        Me.rdbDentroRango.Text = "Seleccionados dentro del rango"
        Me.rdbDentroRango.UseVisualStyleBackColor = True
        '
        'rdbTodosPendientesDentroRango
        '
        Me.rdbTodosPendientesDentroRango.AutoSize = True
        Me.rdbTodosPendientesDentroRango.Location = New System.Drawing.Point(33, 61)
        Me.rdbTodosPendientesDentroRango.Name = "rdbTodosPendientesDentroRango"
        Me.rdbTodosPendientesDentroRango.Size = New System.Drawing.Size(248, 17)
        Me.rdbTodosPendientesDentroRango.TabIndex = 1
        Me.rdbTodosPendientesDentroRango.Text = "Todos los pendientes de pago dentro del rango"
        Me.rdbTodosPendientesDentroRango.UseVisualStyleBackColor = True
        '
        'rdbTodos
        '
        Me.rdbTodos.AutoSize = True
        Me.rdbTodos.Location = New System.Drawing.Point(33, 84)
        Me.rdbTodos.Name = "rdbTodos"
        Me.rdbTodos.Size = New System.Drawing.Size(126, 17)
        Me.rdbTodos.TabIndex = 2
        Me.rdbTodos.Text = "Todos los pendientes"
        Me.rdbTodos.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(137, 177)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 1
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(218, 177)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 2
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'chkIncluirMonedas
        '
        Me.chkIncluirMonedas.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirMonedas.Color = System.Drawing.Color.Empty
        Me.chkIncluirMonedas.Location = New System.Drawing.Point(33, 119)
        Me.chkIncluirMonedas.Name = "chkIncluirMonedas"
        Me.chkIncluirMonedas.Size = New System.Drawing.Size(144, 21)
        Me.chkIncluirMonedas.SoloLectura = False
        Me.chkIncluirMonedas.TabIndex = 3
        Me.chkIncluirMonedas.Texto = "Incluir todas las monedas"
        Me.chkIncluirMonedas.Valor = False
        '
        'txtMoneda
        '
        Me.txtMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMoneda.Color = System.Drawing.Color.Empty
        Me.txtMoneda.Indicaciones = Nothing
        Me.txtMoneda.Location = New System.Drawing.Point(183, 119)
        Me.txtMoneda.Multilinea = False
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.Size = New System.Drawing.Size(58, 21)
        Me.txtMoneda.SoloLectura = True
        Me.txtMoneda.TabIndex = 4
        Me.txtMoneda.TabStop = False
        Me.txtMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtMoneda.Texto = ""
        '
        'frmPrepararPagoImprimir
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(332, 212)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.gbxTipo)
        Me.Name = "frmPrepararPagoImprimir"
        Me.Text = "frmPrepararPagoImprimir"
        Me.gbxTipo.ResumeLayout(False)
        Me.gbxTipo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxTipo As System.Windows.Forms.GroupBox
    Friend WithEvents rdbTodos As System.Windows.Forms.RadioButton
    Friend WithEvents rdbTodosPendientesDentroRango As System.Windows.Forms.RadioButton
    Friend WithEvents rdbDentroRango As System.Windows.Forms.RadioButton
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents txtMoneda As ERP.ocxTXTString
    Friend WithEvents chkIncluirMonedas As ERP.ocxCHK
End Class

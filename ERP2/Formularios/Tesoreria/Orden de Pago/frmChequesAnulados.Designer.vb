﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChequesAnulados
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.txtNroCheque = New ERP.ocxTXTString()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.cbxNroCuentaBancaria = New ERP.ocxCBX()
        Me.lblNroCuentaBancaria = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.cbxBanco = New ERP.ocxCBX()
        Me.lblBanco = New System.Windows.Forms.Label()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(254, 182)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 22
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(172, 182)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 19
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'txtNroCheque
        '
        Me.txtNroCheque.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroCheque.Color = System.Drawing.Color.Empty
        Me.txtNroCheque.Indicaciones = Nothing
        Me.txtNroCheque.Location = New System.Drawing.Point(105, 108)
        Me.txtNroCheque.Multilinea = False
        Me.txtNroCheque.Name = "txtNroCheque"
        Me.txtNroCheque.Size = New System.Drawing.Size(109, 21)
        Me.txtNroCheque.SoloLectura = False
        Me.txtNroCheque.TabIndex = 29
        Me.txtNroCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroCheque.Texto = ""
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(87, 182)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 21
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'cbxNroCuentaBancaria
        '
        Me.cbxNroCuentaBancaria.CampoWhere = Nothing
        Me.cbxNroCuentaBancaria.CargarUnaSolaVez = False
        Me.cbxNroCuentaBancaria.DataDisplayMember = Nothing
        Me.cbxNroCuentaBancaria.DataFilter = Nothing
        Me.cbxNroCuentaBancaria.DataOrderBy = Nothing
        Me.cbxNroCuentaBancaria.DataSource = Nothing
        Me.cbxNroCuentaBancaria.DataValueMember = Nothing
        Me.cbxNroCuentaBancaria.FormABM = Nothing
        Me.cbxNroCuentaBancaria.Indicaciones = Nothing
        Me.cbxNroCuentaBancaria.Location = New System.Drawing.Point(105, 77)
        Me.cbxNroCuentaBancaria.Name = "cbxNroCuentaBancaria"
        Me.cbxNroCuentaBancaria.SeleccionObligatoria = False
        Me.cbxNroCuentaBancaria.Size = New System.Drawing.Size(202, 21)
        Me.cbxNroCuentaBancaria.SoloLectura = False
        Me.cbxNroCuentaBancaria.TabIndex = 28
        Me.cbxNroCuentaBancaria.Texto = ""
        '
        'lblNroCuentaBancaria
        '
        Me.lblNroCuentaBancaria.AutoSize = True
        Me.lblNroCuentaBancaria.Location = New System.Drawing.Point(12, 81)
        Me.lblNroCuentaBancaria.Name = "lblNroCuentaBancaria"
        Me.lblNroCuentaBancaria.Size = New System.Drawing.Size(89, 13)
        Me.lblNroCuentaBancaria.TabIndex = 27
        Me.lblNroCuentaBancaria.Text = "Cuenta Bancaria:"
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(335, 182)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 23
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(105, 13)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(63, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 20
        Me.cbxSucursal.Texto = ""
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(12, 17)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 18
        Me.lblOperacion.Text = "Operacion:"
        '
        'cbxBanco
        '
        Me.cbxBanco.CampoWhere = Nothing
        Me.cbxBanco.CargarUnaSolaVez = False
        Me.cbxBanco.DataDisplayMember = Nothing
        Me.cbxBanco.DataFilter = Nothing
        Me.cbxBanco.DataOrderBy = Nothing
        Me.cbxBanco.DataSource = Nothing
        Me.cbxBanco.DataValueMember = Nothing
        Me.cbxBanco.FormABM = Nothing
        Me.cbxBanco.Indicaciones = Nothing
        Me.cbxBanco.Location = New System.Drawing.Point(105, 45)
        Me.cbxBanco.Name = "cbxBanco"
        Me.cbxBanco.SeleccionObligatoria = False
        Me.cbxBanco.Size = New System.Drawing.Size(268, 21)
        Me.cbxBanco.SoloLectura = False
        Me.cbxBanco.TabIndex = 26
        Me.cbxBanco.Texto = ""
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(12, 50)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(41, 13)
        Me.lblBanco.TabIndex = 25
        Me.lblBanco.Text = "Banco:"
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 6, 26, 11, 37, 29, 740)
        Me.txtFecha.Location = New System.Drawing.Point(306, 13)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(67, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 31
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(267, 17)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 30
        Me.lblFecha.Text = "Fecha:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 112)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 13)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Nro. Cheque:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(12, 138)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 33
        Me.lblObservacion.Text = "Observacion:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(105, 135)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(306, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 34
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 237)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(423, 22)
        Me.StatusStrip1.TabIndex = 35
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(174, 13)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(74, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 36
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(5, 182)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 37
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'frmChequesAnulados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(423, 259)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.lblObservacion)
        Me.Controls.Add(Me.txtObservacion)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtFecha)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.txtNroCheque)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.cbxNroCuentaBancaria)
        Me.Controls.Add(Me.lblNroCuentaBancaria)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.cbxSucursal)
        Me.Controls.Add(Me.lblOperacion)
        Me.Controls.Add(Me.cbxBanco)
        Me.Controls.Add(Me.lblBanco)
        Me.Name = "frmChequesAnulados"
        Me.Tag = "frmChequesAnulados"
        Me.Text = "frmChequesAnulados"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents txtNroCheque As ERP.ocxTXTString
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents cbxNroCuentaBancaria As ERP.ocxCBX
    Friend WithEvents lblNroCuentaBancaria As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents cbxBanco As ERP.ocxCBX
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
End Class

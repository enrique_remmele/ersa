﻿Imports ERP.Reporte

Public Class frmOrdenPago

    'CLASES
    Public CSistema As New CSistema
    Public CArchivoInicio As New CArchivoInicio
    Public CAsiento As New CAsientoOrdenPago
    Public CReporteOrdenPago As New CReporteOrdenPago
    Public CData As New CData
    Private vCotizacionRetencion As Integer

    'ENUMERACION
    Enum ENUMTipoPago
        PagoAProveedorees = 0
        Anticipo = 1
        EgresosARendir = 2
        Todos = 3
    End Enum

    Enum ENUMFormaPago
        ComprobanteGS_PagoGS = 0
        ComprobanteGS_PagoUS = 1
        ComprobanteUS_PagoUS = 2
        ComprobanteUS_PagoGS = 3
    End Enum

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private ChequeValue As Boolean
    Public Property Cheque As Boolean
        Get
            Return ChequeValue
        End Get
        Set(ByVal value As Boolean)
            ChequeValue = value
        End Set
    End Property

    Private DecimalesValue As Boolean
    Public Property Decimales() As Boolean
        Get
            Return DecimalesValue
        End Get
        Set(ByVal value As Boolean)
            DecimalesValue = value
            txtImporteMoneda.Decimales = value
        End Set
    End Property

    Private CotizacionDelDiaValue As Decimal
    Public Property CotizacionDelDia() As Decimal
        Get
            Return CotizacionDelDiaValue
        End Get
        Set(ByVal value As Decimal)
            CotizacionDelDiaValue = value
            txtCotizacion.SetValue(value)
        End Set
    End Property

    Public Property CargarAlIniciar As Boolean = False
    Public Property CerrarAlGuardar As Boolean = False
    Public Property TipoPago As ENUMTipoPago = ENUMTipoPago.PagoAProveedorees
    Public Property IDMonedaPago As Integer

    'VARIABLES
    Public Property dtEgresos As New DataTable
    Public Property dtAuditoriatEgresos As New DataTable
    Dim dtEfectivo As New DataTable
    Dim dtOrdenPago As New DataTable
    Public vControles() As Control
    Public vNuevo As Boolean = False
    Dim vImporte As Decimal
    Dim vTotalop As Decimal
    Dim Monto As Double = 0
    Dim MontoLetras As String = ""

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion("frmOrdenPago", "ORDEN DE PAGO", "OP")
        vNuevo = False
        'SC
        txtCotizacion.txt.Text = ""

        'Otros
        cbxListar.Items.Add("PAGO A PROVEEDORES")
        cbxListar.Items.Add("ANTICIPO A PROVEEDORES")
        cbxListar.Items.Add("EGRESO A RENDIR")

        cbxListar.SelectedIndex = 0

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()

        'Controles
        'txtProveedor.Conectar()
        'SC:20/04/2022 se reemplaza por linea de abajo para solo listar aquellos proveedores con estado activo
        txtProveedor.Conectar("Select * From VProveedor Where Estado='True' Order By RazonSocial ")

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)
        Dim k As Keys = vgKeyProcesar
        btnGuardar.Text = "&Guardar (" & k.ToString & ")"

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        If CBool(vgConfiguraciones("TesoreriaBloquerFecha").ToString) = True Then
            txtFecha.Enabled = False
        Else
            txtFecha.Enabled = True
        End If
    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, txtProveedor)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtObservacion)

        'Egresos
        CSistema.CargaControl(vControles, btnAgregarComprobante)
        If vNuevo = True Then
            CSistema.CargaControl(vControles, btnAplicarComprobantes)
        Else
            CSistema.CargaControl(vControles, btnEliminarAplicacion)
        End If
        CSistema.CargaControl(vControles, lklEliminarEgreso)
        CSistema.CargaControl(vControles, chkAplicarRetencion)
        'CSistema.CargaControl(vControles, txtTotalRetencion)

        'Cheque
        CSistema.CargaControl(vControles, cbxCuentaBancaria)
        CSistema.CargaControl(vControles, txtNroCheque)
        CSistema.CargaControl(vControles, txtFechaCheque)
        CSistema.CargaControl(vControles, txtFechaPagoCheque)
        CSistema.CargaControl(vControles, txtCotizacion)
        CSistema.CargaControl(vControles, txtImporteMoneda)
        CSistema.CargaControl(vControles, txtImporte)
        CSistema.CargaControl(vControles, chkDiferido)
        'CSistema.CargaControl(vControles, txtVencimiento)
        CSistema.CargaControl(vControles, txtOrden)
        CSistema.CargaControl(vControles, cbxListar)

        'Documentos
        CSistema.CargaControl(vControles, Me.OcxOrdenPagoDocumento1)

        'Tipo de Pago
        EstablecerTipoPago(Me.TipoPago)

        'CARGAR ESTRUCTURA DEL DETALLE VENTA

        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Cuenta Bancaria
        'CSistema.SqlToComboBox(cbxCuentaBancaria.cbx, CData.GetTable("VCuentaBancaria").Copy, "ID", "CuentaBancaria")

        'listar
        cbxListar.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "LISTAR", "")

        'Ultimo Registro
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From OrdenPago Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

        'listar
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "LISTAR", cbxListar.Text)

    End Sub

    Sub EstablecerTipoPago(vTipo As ENUMTipoPago)

        lblTipoPago.Visible = True
        lblTipoPago.BringToFront()
        cbxListar.Visible = False

        Select Case vTipo

            Case ENUMTipoPago.PagoAProveedorees
                cbxListar.SelectedIndex = 0
                lblTipoPago.Text = "PAGO A PROVEEDORES"
            Case ENUMTipoPago.Anticipo
                cbxListar.SelectedIndex = 1
                lblTipoPago.Text = "ANTICIPO A PROVEEDORES"
            Case ENUMTipoPago.EgresosARendir
                cbxListar.SelectedIndex = 2
                lblTipoPago.Text = "EGRESOS A RENDIR"
            Case ENUMTipoPago.Todos
                lblTipoPago.Visible = False
                cbxListar.Visible = True
                cbxListar.BringToFront()
        End Select

        ComportamientoTipoOP()

    End Sub

    Sub Nuevo(Optional ByVal Precargar As Boolean = False)
        'txtCotizacion.txt.Text = ""
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)
        'Clases
        CAsiento.InicializarAsiento()
        'Limpiar detalle
        dtEgresos.Rows.Clear()
        ListarEgresos()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        chkAplicarRetencion.Valor = True

        'Funciones
        LimpiarControles()
        If Not Precargar Then
            'dtEgresos = CSistema.ExecuteToDataTable("Exec SpViewEgresosParaOrdenPago " & cbxSucursal.cbx.SelectedValue & ", @SoloAPagar='True' ")
        End If


        IDTransaccion = 0
        CAsiento.Limpiar()
        InicializarControlesCheque()
        vNuevo = True
        txtID.txt.ReadOnly = True

        'Cotizaciones
        CotizacionDelDia = CSistema.CotizacionDelDia(IDMonedaPago, IDOperacion)

        'Controles
        OcxOrdenPagoDocumento1.Inicializar()

        'Cabecera
        txtFecha.Hoy()
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From OrdenPago Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)
        txtComprobante.txt.Text = CSistema.ObtenerProximoNroComprobante(cbxTipoComprobante.cbx.SelectedValue, "OrdenPago", "NroComprobante", cbxSucursal.cbx.SelectedValue)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Bloquear aplicar 
        btnAplicarComprobantes.Enabled = False
        btnEliminarAplicacion.Enabled = False

        txtComprobante.txt.Text = txtID.ObtenerValor

        'Tipo de Pago
        EstablecerTipoPago(Me.TipoPago)

        'Limpiar Datos Proveedor
        txtProveedor.Clear()

        txtComprobante.Focus()

        'Cargar solo las cuentas que se encuentran activas en la chequera
        CSistema.SqlToComboBox(cbxCuentaBancaria.cbx, "select distinct CB.ID, CuentaBancaria from chequera CH join CuentaBancaria CB on CH.IDCuentaBancaria = CB.ID")
        txtFecha.Hoy()
    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        vNuevo = False
        txtID.txt.Focus()

    End Sub

    Sub LimpiarControles()

        'Limpiar Controles
        dgw.Rows.Clear()
        txtSaldoTotal.txt.Text = ""
        txtTotalComprobante.txt.Text = ""
        txtProveedor.Clear()
        txtFecha.txt.Clear()
        txtObservacion.Clear()
        txtImporteMoneda.txt.Clear()
        txtImporte.txt.Clear()
        txtTotalRetencion.SetValue(0)
        txtOrden.Clear()
        txtNroCheque.txt.Clear()
        txtFechaCheque.txt.Clear()
        txtFechaPagoCheque.txt.Clear()

        'Desaparecer Label
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Validar
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            'Ciudad
            If cbxCiudad.Validar("Seleccione correctamente la ciudad!", ctrError, btnGuardar, tsslEstado) = False Then
                Exit Function
            End If

            'Tipo Comprobante
            If cbxTipoComprobante.Validar("Seleccione correctamente el tipo de comprobante!", ctrError, btnGuardar, tsslEstado) = False Then
                Exit Function
            End If

            'Comprobante
            If txtComprobante.txt.Text.Trim.Length = 0 Then
                txtComprobante.SetValue(txtID.ObtenerValor.ToString)
            End If

            'Sucursal
            If cbxSucursal.Validar("Seleccione correctamente el la sucursal!", ctrError, btnGuardar, tsslEstado) = False Then
                Exit Function
            End If

            'Detalle
            If dgw.Rows.Count = 0 And cbxListar.SelectedIndex = 0 Then
                Dim mensaje As String = "No se ha seleccionado ningun comprobante para el pago!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If
        End If

        'Si es cheque
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR And txtImporte.ObtenerValor <> 0 Then

            'Cuenta Bancaria
            If cbxCuentaBancaria.Validar("Seleccione correctamente la cuenta bancaria!", ctrError, btnGuardar, tsslEstado) = False Then
                Exit Function
            End If

            'A la orden
            If txtOrden.txt.Text.Trim.Length = 0 Then
                Dim mensaje As String = "Se debe especificar para quien es el cheque! (a la orden de)"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If

        End If

        'Saldo
        Select Case FormaPago()

            'No calcular el saldo
            Case ENUMFormaPago.ComprobanteUS_PagoGS
                
            Case Else
                If txtSaldoTotal.ObtenerValor <> 0 And Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR And cbxListar.SelectedIndex = 0 Then
                    Dim mensaje As String = "El importe no corresponde al total de comprobantes!"
                    CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                    Exit Function
                End If
        End Select

        'Si la Operación no es anular entra en el For
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            'Los contado, validar su forma de pago
            If TipoPago = ENUMTipoPago.PagoAProveedorees Then

                If dtEgresos.Rows.Count = 0 Then
                    Exit Function
                End If

                For Each o As DataRow In dtEgresos.Select(" Credito = 'False' And Seleccionado = 'True' ")

                    Try
                        If TabControl1.SelectedIndex = 0 Then 'No controlar en el caso de haber seleccionado documento

                            'Si es por Cheque
                            If o("Cheque") = True And txtImporte.ObtenerValor = 0 Then
                                Dim mensaje As String = "Uno de los Egresos esta marcado que se pago con Cheque pero en la OP no esta marcado!"
                                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                                Exit Function
                            End If

                        End If

                    Catch ex As Exception

                    End Try

                Next

            End If

        End If

        'Asiento
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If cbxListar.Text = "EGRESO A RENDIR" Then
                'Validar el Asiento
                If CAsiento.ObtenerSaldo <> 0 Then
                    CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                    Exit Function
                End If

                If CAsiento.ObtenerTotal = 0 Then
                    CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                    Exit Function
                End If
            End If
            'Total de Pago
            If txtImporte.ObtenerValor + OcxOrdenPagoDocumento1.Total = 0 Then
                Dim mensaje As String = "Debe seleccionar un monto!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If

        End If

        If txtImporte.ObtenerValor = 0 Then

        End If

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If

        End If

        Return True


    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        'Validar moneda utilizada
        If ValidarMoneda() = False Then
            tsslEstado.Text = "Atencion: La moneda del cheque y de los documentos deben ser iguales"
            ctrError.SetError(txtMoneda, "Atencion: La moneda del cheque debe ser la misma que de los documentos")
            ctrError.SetIconAlignment(txtMoneda, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'Validar cotizacion
        If ValidarCotizacion() = False Then
            tsslEstado.Text = "Atencion: La cotizacion del cheque no es igual a la de los documentos"
            ctrError.SetError(txtCotizacion, "Atencion: La moneda debe ser igual a la de los documentos")
            ctrError.SetIconAlignment(txtCotizacion, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'Generar la cabecera del asiento

        GenerarAsiento()

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer
        Dim Decimales As Boolean = False

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)

        If cbxListar.SelectedIndex = 0 Then
            CSistema.SetSQLParameter(param, "@PagoProveedor", True, ParameterDirection.Input)
        End If

        If cbxListar.SelectedIndex = 1 Then
            CSistema.SetSQLParameter(param, "@AnticipoProveedor", True, ParameterDirection.Input)
        End If

        If cbxListar.SelectedIndex = 2 Then
            CSistema.SetSQLParameter(param, "@EgresoRendir", True, ParameterDirection.Input)
        End If

        'Moneda
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.GetValue, ParameterDirection.Input)

        ''Cotizacion
        'If cbxMoneda.GetValue <> 1 Then
        '    txtCotizacion.txt.Text = CotizacionDelDia
        'Else
        '    txtCotizacion.txt.Text = 1
        'End If
        'CSistema.SetSQLParameter(param, "@Cotizacion", txtCotizacion.ObtenerValor, ParameterDirection.Input)

        'Proveedor
        If Not txtProveedor.Registro Is Nothing Then
            CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.Registro("ID").ToString, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@AplicarRetencion", chkAplicarRetencion.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        'Totales
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(txtTotalComprobante.ObtenerValor, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DiferenciaCambio", CSistema.FormatoMonedaBaseDatos(txtDiferenciaCambio.ObtenerValor, 1), ParameterDirection.Input)

        'Cheque
        If txtImporteMoneda.ObtenerValor > 0 = True Then

            CSistema.SetSQLParameter(param, "@Cheque", True, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDCuentaBancaria", cbxCuentaBancaria.cbx, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroCheque", txtNroCheque.txt.Text, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@FechaCheque", txtFechaCheque.GetValueString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@FechaPago", txtFechaPagoCheque.GetValueString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ImporteMoneda", CSistema.FormatoMonedaBaseDatos(txtImporteMoneda.ObtenerValor, txtImporteMoneda.Decimales), ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Cotizacion", txtCotizacion.ObtenerValor, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Importe", txtImporte.ObtenerValor, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Diferido", chkDiferido.Checked.ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@FechaVencimiento", txtVencimiento.GetValueString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ALaOrden", txtOrden.txt.Text, ParameterDirection.Input)

        End If

        'Documento
        If OcxOrdenPagoDocumento1.txtTotalFormaPago.ObtenerValor > 0 Then
            Decimales = OcxOrdenPagoDocumento1.Decimales
            'OcxOrdenPagoDocumento1.dgw.Rows(0).Cells.Item(11).Value
            Dim totalImporteDocumento As Decimal = 0
            Dim totalImporteMonedaDocumento As Decimal = 0
            For Each oRow As DataRow In OcxOrdenPagoDocumento1.dtDocumento.Rows
                totalImporteDocumento += oRow("Importe")
                totalImporteMonedaDocumento += oRow("ImporteMoneda")
            Next
            CSistema.SetSQLParameter(param, "@IDMonedaDocumento", OcxOrdenPagoDocumento1.dgw.Rows(0).Cells.Item(11).Value, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ImporteDocumento", CSistema.FormatoMonedaBaseDatos(totalImporteDocumento, Decimales), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ImporteMonedaDocumento", CSistema.FormatoMonedaBaseDatos(totalImporteMonedaDocumento, Decimales), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@CotizacionDocumento", CSistema.FormatoMonedaBaseDatos(OcxOrdenPagoDocumento1.dgw.Rows(0).Cells.Item(13).Value, Decimales), ParameterDirection.Input)

        End If

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpOrdenPago", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From OrdenPago Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpOrdenPago", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        Dim Procesar As Boolean = True


        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
            CargarOperacion(IDTransaccion)
            txtID.SoloLectura = False
        End If

        If IDTransaccion > 0 Then

            'Cargamos el Detalle de Egresos
            Procesar = InsertarDetalleEgresos(IDTransaccion)

            'Cargamos el Detalle de Efectivo
            Procesar = InsertarDetalleEfectivo(IDTransaccion)

            'Cargamos el Detalle de Documento
            Procesar = InsertarDetalleFormaPago(IDTransaccion)

        End If

        'Aplicar
        Try

            ReDim param(-1)

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            'Aplicar la Cobranza
            If CSistema.ExecuteStoreProcedure(param, "SpOrdenPagoProcesar", False, False, MensajeRetorno) = False Then

            End If
            If cbxListar.Text = "EGRESO A RENDIR" Then
                'Cargamos el asiento
                CAsiento.IDTransaccion = IDTransaccion
                CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
            End If

        Catch ex As Exception

        End Try

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

        If CerrarAlGuardar = True Then
            Me.Close()
        End If

        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False

    End Sub

    Function ValidarMoneda() As Boolean

        Dim idMonedaDocumento As Integer
        If OcxOrdenPagoDocumento1.dgw.Rows.Count > 0 Then
            idMonedaDocumento = OcxOrdenPagoDocumento1.dgw.Rows(0).Cells.Item(11).Value
        Else
            Return True
        End If

        If txtImporte.ObtenerValor <> 0 And (idMonedaDocumento <> IDMonedaPago) Then
            Return False
        End If
        Return True

    End Function

    Function ValidarCotizacion() As Boolean

        Dim cotizacionDocumento As Decimal
        If OcxOrdenPagoDocumento1.dgw.Rows.Count > 0 Then
            cotizacionDocumento = OcxOrdenPagoDocumento1.dgw.Rows(0).Cells.Item(13).Value
        Else
            Return True
        End If

        If txtImporte.ObtenerValor <> 0 And (txtCotizacion.ObtenerValor <> cotizacionDocumento) Then
            Return False
        End If
        Return True

    End Function

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        Dim Comprobante As String = cbxTipoComprobante.cbx.Text & ": " & txtComprobante.txt.Text

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante
            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From vOrdenPago Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else

            If cbxListar.Text <> "EGRESO A RENDIR" Then
                MessageBox.Show("Debe Guardar la Operacion para visualizar el asiento", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If

            'Validar
            If cbxCiudad.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la ciudad de operacion!"
                ctrError.SetError(cbxCiudad, mensaje)
                ctrError.SetIconAlignment(cbxCiudad, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'Validar moneda utilizada
            If ValidarMoneda() = False Then
                tsslEstado.Text = "Atencion: La moneda del cheque y de los documentos deben ser iguales"
                ctrError.SetError(txtMoneda, "Atencion: La moneda del cheque debe ser la misma que de los documentos")
                ctrError.SetIconAlignment(txtMoneda, ErrorIconAlignment.TopRight)
                Exit Sub
            End If

            'Validar cotizacion
            If ValidarCotizacion() = False Then
                tsslEstado.Text = "Atencion: La cotizacion del cheque no es igual a la de los documentos"
                ctrError.SetError(txtCotizacion, "Atencion: La moneda debe ser igual a la de los documentos")
                ctrError.SetIconAlignment(txtCotizacion, ErrorIconAlignment.TopRight)
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante

            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            CAsiento.ListarDetalle(frm.dgv)
            frm.CalcularTotales()

            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
            CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            CAsiento.NroCaja = frm.CAsiento.NroCaja
            CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                CAsiento.dtAsiento.Clear()
                CAsiento.dtDetalleAsiento.Clear()
                VisualizarAsiento()
            End If

        End If

    End Sub

    Sub GenerarAsiento()

        If cbxListar.Text <> "EGRESO A RENDIR" Then
            Exit Sub
        End If

        'Establecemos la moneda de la operacion de acuerdo si fue utilizado un documento, un cheque o ambos
        Dim idMonedaOperacion As Integer
        If OcxOrdenPagoDocumento1.dgw.Rows.Count > 0 Then
            idMonedaOperacion = OcxOrdenPagoDocumento1.dgw.Rows(0).Cells.Item(11).Value
        Else
            idMonedaOperacion = IDMonedaPago
        End If

        'Establecemos la cotizacion de la operacion de acuerdo si fue utilizado un documento, un cheque o ambos
        Dim cotizacionOperacion As Decimal
        If OcxOrdenPagoDocumento1.dgw.Rows.Count > 0 Then
            cotizacionOperacion = OcxOrdenPagoDocumento1.dgw.Rows(0).Cells.Item(13).Value
        Else
            cotizacionOperacion = txtCotizacion.ObtenerValor
        End If


        'EstablecerCabecera
        Dim Total As Decimal
        Dim TotalCheque As Decimal
        Dim TotalEfectivo As Decimal
        Dim TotalRetencion As Decimal
        Dim TotalDocumento As Decimal
        Dim TotalDiferenciaCambio As Decimal

        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = cbxCiudad.cbx.SelectedValue
        oRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
        oRow("Fecha") = txtFecha.GetValue
        oRow("IDMoneda") = idMonedaOperacion
        oRow("Cotizacion") = cotizacionOperacion
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = txtComprobante.txt.Text
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text
        oRow("Detalle") = txtObservacion.txt.Text

        'Si la moneda es diferente al local, pasar convertido 
        Total = ObtenerImporteComprobantes(False, True)
        TotalCheque = ObtenerImporteCheque(True, True)
        TotalRetencion = ObtenerImporteRetencion(True, True)
        TotalDocumento = ObtenerImporteDocumento(False, True, True) + ObtenerImporteDocumento(True, True, True)
        TotalDiferenciaCambio = txtDiferenciaCambio.ObtenerValor

        oRow("Total") = Total

        oRow("GastosMultiples") = False

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)
        CAsiento.Total = Total
        CAsiento.TotalCheque = TotalCheque
        CAsiento.TotalDocumento = TotalDocumento
        CAsiento.TotalEfectivo = TotalEfectivo
        If (TotalDiferenciaCambio >= 0.5) Or (TotalDiferenciaCambio <= -0.5) Then
            CAsiento.TotalDiferenciaCambio = TotalDiferenciaCambio
        Else
            CAsiento.TotalDiferenciaCambio = 0
        End If
        CAsiento.dtDocumento = OcxOrdenPagoDocumento1.dtDocumento

        CAsiento.IDCuentaBancaria = cbxCuentaBancaria.GetValue
        CAsiento.IDMoneda = idMonedaOperacion
        CAsiento.Diferido = chkDiferido.Checked

        If txtProveedor.Seleccionado = True Then
            CAsiento.IDProveedor = txtProveedor.Registro("ID").ToString
        End If

        Dim MontoMinimoRetencion As Decimal = 0

        If IsNumeric(vgConfiguraciones("CompraImporteMinimoRetencion").ToString) = False Then
            MontoMinimoRetencion = 0
        Else
            MontoMinimoRetencion = vgConfiguraciones("CompraImporteMinimoRetencion")
        End If

        If chkAplicarRetencion.Valor = True And Total > MontoMinimoRetencion Then
            CAsiento.TotalRetencion = TotalRetencion
        Else
            CAsiento.TotalRetencion = 0
        End If

        If CAsiento.Generado = True Then
            Exit Sub
        End If

        CAsiento.Generar()

    End Sub

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim frm As New frmMotivoAnulacionOP
        frm.Text = " Motivo Anulacion OP "
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.ShowDialog(Me)

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMotivoAnulacion", frm.ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpOrdenPago", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)

            Exit Sub
        Else

            tsslEstado.Text = MensajeRetorno

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False


    End Sub

    Function InsertarDetalleEgresos(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalleEgresos = True

        For Each oRow As DataRow In dtEgresos.Select("Seleccionado = 'True' ")

            Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)
            Dim sql As String = "Insert Into OrdenPagoEgreso(IDTransaccionOrdenPago, IDTransaccionEgreso, Importe, Saldo, Cuota,Retenido, Retencion, IVA) Values( " & IDTransaccion & "," & oRow("IDTransaccion").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString, Decimales) & ", " & oRow("Cuota").ToString & "," & Convert.ToByte(chkAplicarRetencion.Enabled) & ", " & IIf(chkAplicarRetencion.Enabled, CSistema.FormatoMonedaBaseDatos(oRow("RetencionIVA"), Decimales), 0) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("IVA"), Decimales) & " )"

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

        Next

    End Function

    Function InsertarDetalleFormaPago(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalleFormaPago = True

        Dim index As Integer = 1

        For Each oRow As DataRow In OcxOrdenPagoDocumento1.dtDocumento.Rows

            Dim sql1 As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, Cheque, Documento, Tarjeta, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe) Values( " & IDTransaccion & ", " & oRow("ID").ToString & ",'False', 'False', 'True', 'False', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, True) & ", " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, True) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, True) & ")"
            Dim sql2 As String = "Insert Into FormaPagoDocumento(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, IDMoneda, ImporteMoneda, Cotizacion, Total, Saldo ,Observacion) values(" & IDTransaccion & ", " & index & ", " & cbxSucursal.cbx.SelectedValue & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("Comprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True) & "', " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, True) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, True) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, True) & ", '" & oRow("Observacion").ToString & "')"

            If CSistema.ExecuteNonQuery(sql1) = 0 Then
                Return False
            End If

            If CSistema.ExecuteNonQuery(sql2) = 0 Then
                Return False
            End If

            index = index + 1
        Next

    End Function

    Function InsertarDetalleEfectivo(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalleEfectivo = True

    End Function

    Sub CargarEgreso(ByVal IDTransaccion As Integer)

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra las ventas asociadas!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        'Limpiar ventas seleccionadas
        dtEgresos.Rows.Clear()
        dtEgresos = CSistema.ExecuteToDataTable("Select * From VOrdenPagoEgreso Where IDTransaccionOrdenPago=" & IDTransaccion).Copy
        dtAuditoriatEgresos = CSistema.ExecuteToDataTable("Select * From vSolicitudEliminacionOrdenPagoEgreso Where Aprobado = 'APROBADO' and IDTransaccionOrdenPago =" & IDTransaccion).Copy
        ListarEgresos()
        ListarAuditoria()
    End Sub

    Sub ListarAuditoria()
        Try
            dgvAuditoria.Rows.Clear()
        Catch ex As Exception

        End Try

        CSistema.dtToGrid(dgvAuditoria, dtAuditoriatEgresos)

        dgvAuditoria.Columns("IDUsuarioSolicitud").Visible = False
        dgvAuditoria.Columns("IDUsuarioAprobador").Visible = False
        dgvAuditoria.Columns("IDTransaccionOrdenPago").Visible = False
        dgvAuditoria.Columns("IDTransaccionEgreso").Visible = False
        CSistema.DataGridColumnasNumericas(dgvAuditoria, {"Total"})

    End Sub

    Sub ListarEgresos()

        'chkAplicarRetencion.chk.Checked = True

        dgw.Rows.Clear()

        Dim Total As Decimal = 0
        Dim TotalImporte As Decimal = 0
        Dim TotalRetencion As Decimal = 0
        Dim Retentor As Boolean = False

        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)

        If dtEgresos.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each oRow As DataRow In dtEgresos.Rows
            If CSistema.RetornarValorBoolean(oRow("Seleccionado").ToString) = True Then

                Dim oRow1(8) As String

                oRow1(0) = oRow("IDTransaccion").ToString
                oRow1(1) = oRow("NroComprobante").ToString
                oRow1(2) = oRow("Fec").ToString
                oRow1(3) = oRow("Proveedor").ToString
                oRow1(4) = CSistema.FormatoMoneda(oRow("Cotizacion").ToString, Decimales)
                oRow1(5) = oRow("Moneda").ToString
                oRow1(6) = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
                oRow1(7) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)
                oRow1(8) = CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales)

                Total = Total + CDec(oRow("Importe").ToString)
                TotalRetencion = TotalRetencion + CDec(oRow("RetencionIVA").ToString)

                dgw.Rows.Add(oRow1)

                Retentor = CBool(oRow("Retentor").ToString)

            End If
        Next

        'If vNuevo = False Then
        '    TotalRetencion = dtOrdenPago.Rows(0)("TotalRetencion")
        'End If


        If chkAplicarRetencion.Valor = True Then
            TotalImporte = Total - TotalRetencion

        Else
            'OcxSeleccionarEfectivo1.TotalPago = Total
            TotalImporte = txtImporteMoneda.ObtenerValor
        End If

        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)

        txtTotalComprobante.SetValue(Total)
        'txtSaldoTotal.SetValue(Total)
        txtSaldoTotal.SetValue(TotalImporte)
        txtTotalRetencion.SetValue(CSistema.FormatoMoneda(TotalRetencion, Decimales))

        CalcularTotales()

        If vNuevo = True Then
            If dgw.Rows.Count > 0 Then
                txtProveedor.SoloLectura = True
                cbxMoneda.SoloLectura = True
            Else
                txtProveedor.SoloLectura = False
                cbxMoneda.SoloLectura = False
            End If
        End If


    End Sub

    Sub ObtenerDatosCuentaBancaria()

        txtBanco.txt.Clear()
        txtMoneda.txt.Clear()

        If IsNumeric(cbxCuentaBancaria.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        For Each oRow As DataRow In CData.GetTable("VCuentaBancaria").Select(" ID = " & cbxCuentaBancaria.cbx.SelectedValue)
            txtBanco.txt.Text = oRow("Banco")
            txtMoneda.txt.Text = oRow("Mon")

            If OcxOrdenPagoDocumento1.dgw.Rows.Count > 0 Then
                IDMonedaPago = OcxOrdenPagoDocumento1.dgw.Rows(0).Cells.Item(11).Value
            Else
                IDMonedaPago = oRow("IDMoneda")
            End If

            txtImporteMoneda.Decimales = oRow("Decimales")
        Next

        CambiarTotalEgresoPorPagoOtraMoneda()
        ComportamientoImporteCheque()

        'Si es nuevo, reiniciar valores de importes del cheque
        If vNuevo = True Then
            txtImporte.SetValue(0)
            txtImporteMoneda.SetValue(0)
            txtDiferenciaCambio.SetValue(0)
            CalcularTotales()
        End If

    End Sub

    Sub EliminarEgreso()

        If vNuevo = False Then
            Exit Sub
        End If

        For Each item As DataGridViewRow In dgw.SelectedRows

            For Each oRow As DataRow In dtEgresos.Select(" IDTransaccion = " & item.Cells(0).Value & " ")
                'CAsientoContableOrdenPago.EliminarVenta(oRow("Importe").ToString, oRow("IDTipoComprobante").ToString, oRow("IDMoneda").ToString, oRow("Credito").ToString)
                oRow("Seleccionado") = False
                Exit For
            Next

        Next

        ListarEgresos()

    End Sub

    Sub CalcularTotales()

        'Variables
        Dim TotalEgreso As Decimal = 0
        Dim TotalCheque As Decimal = 0
        Dim TotalDocumento As Decimal = 0
        Dim TotalDocumentoNegativo As Decimal = 0
        Dim TotalEfectivo As Decimal = 0
        Dim SaldoTotal As Decimal = 0
        Dim DiferenciaCambio As Decimal = 0

        Dim DecimalesCobraza As Boolean = False
        Dim DecimalesFormaPago As Boolean = False

        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & IDMonedaPago, "VMoneda")("Decimales").ToString)
        DecimalesFormaPago = Decimales
        DecimalesCobraza = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)

        'Ver si se paga con una moneda diferente a la de la factura para colocar o no decimal en el saldo
        If DecimalesFormaPago <> DecimalesCobraza Then
            Decimales = True
        End If

        TotalEgreso = CSistema.gridSumColumn(dgw, "colImporte")
        TotalEgreso = CSistema.FormatoNumero(TotalEgreso, Decimales)

        'Obtener valores Cheque
        If txtImporte.ObtenerValor > 0 Or txtImporteMoneda.ObtenerValor > 0 Then
            Select Case FormaPago()
                Case ENUMFormaPago.ComprobanteUS_PagoGS
                    TotalEgreso = ObtenerImporteComprobantes(False, True)
                    TotalCheque = ObtenerImporteCheque(True, True)
                    TotalCheque = TotalCheque + ObtenerImporteRetencion(True, False)
                Case ENUMFormaPago.ComprobanteUS_PagoUS
                    TotalEgreso = ObtenerImporteComprobantes(True, False)
                    TotalCheque = ObtenerImporteCheque(True, False)
                Case Else
                    TotalEgreso = ObtenerImporteComprobantes(True, True)
                    TotalCheque = ObtenerImporteCheque(True, True)

                    If CDec(TotalEgreso.ToString) = CDec(TotalCheque) Then
                        If TotalEgreso > TotalCheque Then
                            TotalEgreso = TotalCheque
                        Else
                            TotalCheque = TotalEgreso
                        End If
                    End If

                    TotalEgreso = CSistema.ExtraerDecimal(TotalEgreso)

                    TotalCheque = CSistema.ExtraerDecimal(TotalCheque)


            End Select
        End If
        'Obtener valores Documentos
        If Not OcxOrdenPagoDocumento1.dtDocumento Is Nothing Then
            If OcxOrdenPagoDocumento1.dtDocumento.Rows.Count > 0 Then
                Select Case FormaPagoDocumento()
                    Case ENUMFormaPago.ComprobanteUS_PagoGS
                        'Egresos
                        TotalEgreso = ObtenerImporteComprobantes(False, True)
                        'Documento
                        TotalDocumento = ObtenerImporteDocumento(False, True, True)
                        'Documento Negativo
                        TotalDocumentoNegativo = ObtenerImporteDocumento(True, True, True)
                    Case ENUMFormaPago.ComprobanteUS_PagoUS
                        'Egresos
                        TotalEgreso = ObtenerImporteComprobantes(True, False)
                        'Documentos
                        TotalDocumento = ObtenerImporteDocumento(False, True, False)
                        'Documentos Negativo
                        TotalDocumentoNegativo = ObtenerImporteDocumento(True, True, False)
                    Case Else
                        TotalEgreso = ObtenerImporteComprobantes(True, True)
                        TotalDocumento = ObtenerImporteDocumento(False, True, True)
                        TotalDocumentoNegativo = ObtenerImporteDocumento(True, True, True)
                End Select

            End If

        End If

        DiferenciaCambio = ObtenerDiferenciaCambio()

        If TotalEgreso < 0 Then
            TotalEgreso = TotalEgreso * -1
        End If

        SaldoTotal = (TotalCheque + TotalEfectivo + TotalDocumento) - (TotalEgreso + TotalDocumentoNegativo)

        'Si hay diferencia de cambio
        Dim CalcularDiferenciaCambioEnSaldo As Boolean = False

        If txtImporte.ObtenerValor > 0 Or txtImporteMoneda.ObtenerValor > 0 Then
            Select Case FormaPago()
                Case ENUMFormaPago.ComprobanteUS_PagoGS
                    CalcularDiferenciaCambioEnSaldo = True
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    CalcularDiferenciaCambioEnSaldo = True
                Case Else
                    CalcularDiferenciaCambioEnSaldo = False
            End Select
        Else
            Select Case FormaPagoDocumento()
                Case ENUMFormaPago.ComprobanteUS_PagoGS
                    CalcularDiferenciaCambioEnSaldo = True
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    CalcularDiferenciaCambioEnSaldo = True
                Case Else
                    CalcularDiferenciaCambioEnSaldo = False
            End Select
        End If

        If CalcularDiferenciaCambioEnSaldo = True And DiferenciaCambio <> 0 Then

            If DiferenciaCambio > 0 Then
                If SaldoTotal > 0 Then
                    SaldoTotal = SaldoTotal - DiferenciaCambio
                Else
                    SaldoTotal = SaldoTotal - (DiferenciaCambio * -1)
                End If
            End If

            If DiferenciaCambio < 0 Then
                If SaldoTotal > 0 Then
                    SaldoTotal = SaldoTotal + DiferenciaCambio
                Else
                    SaldoTotal = (SaldoTotal * -1) + DiferenciaCambio
                End If
            End If

        End If

        If OcxOrdenPagoDocumento1.dgw.Rows.Count > 0 Then
            IDMonedaPago = OcxOrdenPagoDocumento1.dgw.Rows(0).Cells.Item(11).Value
        End If

        If chkAplicarRetencion.Valor = True And SaldoTotal <> 0 Then
            'Se coloca esta consulta porque no redondea de la misma forma los TotalEgreso y TotalDocumento
            If SaldoTotal > 1 Or SaldoTotal < -1 Then
                SaldoTotal = SaldoTotal + CSistema.FormatoMoneda(txtTotalRetencion.txt.Text, Decimales)
            End If
        End If

        txtSaldoTotal.SetValue(CSistema.FormatoMoneda(SaldoTotal, Decimales))
        txtCantidadComprobante.SetValue(dgw.Rows.Count)

        'Actualizar saldo para documentos
        OcxOrdenPagoDocumento1.Saldo = SaldoTotal * -1
        OcxOrdenPagoDocumento1.IDMoneda = cbxMoneda.GetValue
        OcxOrdenPagoDocumento1.Decimales = IIf(CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString), 1, 0)

        If TotalCheque > 0 Then
            TabControl1.TabPages(0).Text = "Cheque*"
            'Control fuera del chorizo
            If chkAplicarRetencion.chk.Checked Then
                If txtImporteMoneda.ObtenerValor = (txtTotalComprobante.ObtenerValor - txtTotalRetencion.ObtenerValor) Then
                    txtSaldoTotal.SetValue(0)
                End If
            End If
        Else
            TabControl1.TabPages(0).Text = "Cheque"
        End If

        If TotalDocumento > 0 Or TotalDocumentoNegativo > 0 Or OcxOrdenPagoDocumento1.dgw.Rows.Count > 0 Then
            TabControl1.TabPages(1).Text = "Documento*"
            'Control fuera del chorizo
            If chkAplicarRetencion.chk.Checked Then
                If OcxOrdenPagoDocumento1.Total = (txtTotalComprobante.ObtenerValor - txtTotalRetencion.ObtenerValor) Then
                    txtSaldoTotal.SetValue(0)
                End If
            End If

        Else
            TabControl1.TabPages(1).Text = "Documento"
        End If

    End Sub

    Sub ObtenerEgresoRetencion()

        'txtImporte.txt.Text = txtImporteMoneda.ObtenerValor
        CalcularTotales()

    End Sub

    Sub Imprimir()


        Dim frm As New frmOpcionesImpresionOP

        frm.IDTransaccion = IDTransaccion
        frm.MontoLetras = CSistema.NumeroALetra(Monto)
        frm.ShowDialog()



        'Dim frm As New frmReporte

        'MontoLetras = CSistema.NumeroALetra(Monto)




        'If MessageBox.Show("Imprimir con Observaciones?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.No Then
        '    CReporteOrdenPago.ImprimirOrdenPago(frm, IDTransaccion, vgUsuarioIdentificador, MontoLetras)
        'Else
        '    CReporteOrdenPago.ImprimirOrdenPagoObservacion(frm, IDTransaccion, vgUsuarioIdentificador, MontoLetras)
        'End If



    End Sub

    Sub Buscar()

        Dim frm As New frmConsultaOrdenPago
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog()
        CargarOperacion(frm.IDTransaccion)

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        'Cuenta Bancaria
        CSistema.SqlToComboBox(cbxCuentaBancaria.cbx, CData.GetTable("VCuentaBancaria").Copy, "ID", "CuentaBancaria")

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        dtEgresos.Rows.Clear()
        ListarEgresos()

        'Carga el dt para listar las Facturas
        'OcxSeleccionarEfectivo1.dt = CSistema.ExecuteToDataTable("Select *, 'Sel'='False', 'Cancelar'='False', 'APagar'=Saldo from VEfectivo Where Cancelado = 'False' Order By Fecha").Copy

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From OrdenPago Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
        dtOrdenPago = CSistema.ExecuteToDataTable("Select * From VOrdenPagosaldo Where IDTransaccion=" & IDTransaccion)
        vTotalop = CSistema.dtSumColumn(dtEgresos, "Importe")

        If dtOrdenPago Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dtOrdenPago.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dtOrdenPago.Rows(0)

        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        cbxMoneda.SelectedValue(oRow("IDMonedaComprobante").ToString)
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        txtObservacion.txt.Text = oRow("Observacion").ToString
        chkAplicarRetencion.Valor = oRow("AplicarRetencion").ToString


        vImporte = oRow("Total")

        If (oRow("PagoProveedor")) = True Then
            cbxListar.SelectedIndex = 0
        End If
        If oRow("AnticipoProveedor").ToString = True Then
            cbxListar.SelectedIndex = 1
        End If
        If oRow("EgresoRendir").ToString = True Then
            cbxListar.SelectedIndex = 2
        End If

        'Tipo de Pago
        EstablecerTipoPago(Me.TipoPago)

        'Proveedor
        If oRow("IDProveedor") > 0 Then
            'txtProveedor.SetValue(oRow("IDProveedor"))

            'SC: 19/04/2022 - Carga campo por campo, porque la vista de Proveedor solo trae 'ACTIVO'
            txtProveedor.txtID.txt.Text = oRow("IDProveedor").ToString
            txtProveedor.txtRazonSocial.txt.Text = oRow("Proveedor").ToString
            txtProveedor.txtReferencia.txt.Text = oRow("Referencia").ToString
            txtProveedor.txtRUC.txt.Text = oRow("Ruc").ToString
        Else
            txtProveedor.SetValueString(0, "", "")
        End If

        'Cheque
        InicializarControlesCheque()

        'Efectivo
        'cbxMonedaEfectivo.SelectedValue(Nothing)
        'txtImporteMonedaEfectivo.SetValue(0)
        'txtCotizacionEfectivo.SetValue(1)


        If CBool(oRow("Cheque")) = True Then

            cbxCuentaBancaria.SelectedValue(oRow("IDCuentaBancaria"))
            txtNroCheque.txt.Text = oRow("NroCheque").ToString
            txtFechaCheque.SetValue(oRow("FechaCheque"))
            txtFechaPagoCheque.SetValue(oRow("FechaPago"))
            txtCotizacion.SetValue(oRow("Cotizacion"))
            txtImporteMoneda.SetValue(oRow("ImporteMoneda"))
            txtImporte.SetValue(oRow("Importe"))
            'CalcularImporteCheque()
            chkDiferido.Checked = oRow("Diferido")
            txtVencimiento.SetValue(oRow("FechaVencimiento"))
            txtOrden.txt.Text = oRow("ALaOrden").ToString

        End If

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        'Cargamos el detalle de Egresos
        CargarEgreso(IDTransaccion)

        'Cargamos el Efectivo
        ' OcxSeleccionarEfectivo1.dt = CSistema.ExecuteToDataTable("Select * From VOrdenPagoEfectivo Where IDTransaccionOrdenPago=" & IDTransaccion).Copy

        'Documentos
        OcxOrdenPagoDocumento1.Inicializar()
        OcxOrdenPagoDocumento1.ListarFormaPago(IDTransaccion)

        'Diferencia de Cambio
        Dim vDiferenciaCambio As Decimal = CSistema.RetornarValorInteger(oRow("DiferenciaCambio").ToString)
        If vDiferenciaCambio <> 0 And CBool(oRow("Anulado").ToString) = False Then
            txtDiferenciaCambio.Visible = True
            lblDiferenciaCambio.Visible = True
            txtDiferenciaCambio.SetValue(vDiferenciaCambio)
        Else
            txtDiferenciaCambio.Visible = False
            lblDiferenciaCambio.Visible = False
            txtDiferenciaCambio.SetValue(0)
        End If

        'Calcular Totales
        CalcularTotales()

        If cbxListar.Text <> "PAGO A PROVEEDORES" Then

            btnEliminarAplicacion.Enabled = True

            If txtSaldoTotal.txt.Text > 0 Then
                btnAplicarComprobantes.Enabled = True
            End If
        Else
            btnEliminarAplicacion.Enabled = False
            btnAplicarComprobantes.Enabled = False
        End If

        'Si Saldo es distinto a 0, Listar Egresos a rendir
        'If txtSaldoTotal.ObtenerValor <> 0 Then
        '    CargarEgresos(dtEgresos)
        'End If

        If CBool(oRow("Anulado").ToString) = True Then

            'Visible Label
            lblRegistradoPor.Visible = True
            lblUsuarioAnulado.Visible = True
            lblUsuarioRegistro.Visible = True
            lblFechaRegistro.Visible = True
            lblAnulado.Visible = True
            lblAnulado.Visible = True
            lblFechaAnulado.Visible = True
            btnEliminarAplicacion.Enabled = False
            btnAplicarComprobantes.Enabled = False

            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            'lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            lblUsuarioAnulado.Text = oRow("UsuarioAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Inicializamos el Asiento
        CAsiento.Limpiar()

        Dim dtAsiento As DataTable = CSistema.ExecuteToDataTable("Select 'Credito'=Sum(Credito) From VDetalleAsiento Where IDTransaccion=" & IDTransaccion)

        If dtAsiento.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow2 As DataRow = dtAsiento.Rows(0)
        Monto = CSistema.FormatoNumero(oRow2("Credito").ToString)

        txtSaldoTotal.SetValue(oRow("Saldo"))

    End Sub

    Sub CargarEgresos()
        Dim vCotizacionMoneda As Integer = CType(CSistema.ExecuteScalar("Select IsNull(Max(cotizacion), 1) From Cotizacion Where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' and IDMoneda=" & cbxMoneda.GetValue), Integer)
        If vCotizacionMoneda = 1 And cbxMoneda.GetValue <> 1 Then
            MessageBox.Show("Cargar cotizacion del dia para la moneda.", "Cotizacion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        dtEgresos = CSistema.ExecuteToDataTable("Exec SpViewEgresosParaOrdenPago " & cbxSucursal.cbx.SelectedValue & ", @SoloAPagar='True' ")

        Dim frm As New frmOrdenPagoSeleccionarEgresos
        frm.Text = "Selección de Comprobantes Pendientes de Pago"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.IDMoneda = cbxMoneda.GetValue
        frm.dt = dtEgresos
        If txtProveedor.Seleccionado = True Then
            frm.IDProveedor = txtProveedor.Registro("ID")
        Else
            frm.IDProveedor = 0
        End If

        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccion de Egresos para pago", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        dtEgresos = frm.dt
        'dbs
        Try
            chkAplicarRetencion.Valor = CType(CSistema.ExecuteScalar("Select SujetoRetencion From Proveedor Where ID=" & txtProveedor.Registro("ID")), Boolean)
        Catch ex As Exception
            chkAplicarRetencion.Valor = 0
        End Try


        ListarEgresos()
        CambiarTotalEgresoPorPagoOtraMoneda()
        ObtenerEgresoRetencion()
        AnalizarSiAplicaRetencion()
        cbxCuentaBancaria.Focus()

    End Sub

    Sub CargarAplicar()

        dtEgresos = CSistema.ExecuteToDataTable("Exec SpViewEgresosParaOrdenPago " & cbxSucursal.cbx.SelectedValue & ", 'True', " & IDTransaccion & " ")
        Dim frm As New frmOrdenPagoAplicarComprobante
        frm.Text = "Selección de Comprobantes Pendientes de Pago"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.IDTransaccion = IDTransaccion
        frm.dt = dtEgresos
        frm.IDMoneda = cbxMoneda.GetValue
        frm.Totalop = vTotalop
        frm.Totalop = txtSaldoTotal.ObtenerValor
        frm.Importe = vImporte
        If txtProveedor.txtRazonSocial.txt.Text <> "" Then
            'frm.IDProveedor = txtProveedor.Registro("ID")
            frm.IDProveedor = txtProveedor.txtID.txt.Text
        Else
            frm.IDProveedor = 0
        End If

        frm.Inicializar()
        frm.ShowDialog(Me)
        CargarOperacion()


    End Sub

    Sub EliminarAplicacion()

        Dim frm As New frmOrdenPagoEliminarComprobante
        frm.IDTransaccionOrdenPago = IDTransaccion
        frm.ShowDialog()

    End Sub


    Sub CargarEgresos(ByRef dt As DataTable)

        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpViewEgresosParaOrdenPago " & cbxSucursal.cbx.SelectedValue & " ").Copy

        For Each oRow As DataRow In dttemp.Rows

            Dim NewRow As DataRow = dt.NewRow
            NewRow("IDTransaccion") = oRow("IDTransaccion")
            NewRow("NroComprobante") = oRow("NroComprobante")
            NewRow("T. Comp.") = oRow("T. Comp.")
            NewRow("Proveedor") = oRow("Proveedor")
            NewRow("IDProveedor") = oRow("IDProveedor")
            NewRow("Fecha") = oRow("Fecha")
            NewRow("Fec") = oRow("Fec")
            NewRow("FechaVencimiento") = oRow("FechaVencimiento")
            NewRow("Fec. Venc.") = oRow("Fec. Venc.")
            NewRow("Total") = oRow("Total")
            NewRow("Saldo") = oRow("Saldo")
            NewRow("Importe") = oRow("Importe")
            NewRow("IDMoneda") = oRow("IDMOneda")
            NewRow("Moneda") = oRow("Moneda")
            NewRow("Cotizacion") = oRow("Cotizacion")
            NewRow("Observacion") = oRow("Observacion")
            NewRow("Seleccionado") = oRow("Seleccionado")
            NewRow("Cancelar") = oRow("Cancelar")

            dt.Rows.Add(NewRow)

        Next

    End Sub

    Sub InicializarControlesCheque()

        txtNroCheque.txt.Clear()
        txtCotizacion.SetValue(1)
        txtImporte.SetValue(0)
        txtImporteMoneda.SetValue(0)
        chkDiferido.Checked = 0
        txtVencimiento.txt.Clear()
        txtOrden.txt.Clear()
        txtTotalComprobante.txt.Clear()
        txtTotalRetencion.txt.Clear()

    End Sub

    Sub CalcularImporteCheque()

        If cbxCuentaBancaria.cbx.SelectedValue Is Nothing Then
            Exit Sub
        End If

        Dim IDMonedaComprobante As Integer = cbxMoneda.GetValue
        Dim ImporteMoneda As Decimal
        Dim Cotizacion As Decimal
        Dim ImporteLocal As Decimal

        If txtImporteMoneda.SoloLectura = False Then

            ImporteMoneda = txtImporteMoneda.ObtenerValor
            Cotizacion = txtCotizacion.ObtenerValor
            ImporteLocal = CSistema.Cotizador(Cotizacion, ImporteMoneda, IDMonedaPago)

            txtImporte.SetValue(ImporteLocal)

        End If

        If txtImporte.SoloLectura = False Then

            ImporteLocal = txtImporte.ObtenerValor
            Cotizacion = txtCotizacion.ObtenerValor
            ImporteMoneda = CSistema.Cotizador(Cotizacion, ImporteLocal, IDMonedaPago)

            Select Case FormaPago()
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    ImporteMoneda = CSistema.Cotizador(Cotizacion, ImporteLocal, IDMonedaComprobante, IDMonedaPago)
                Case ENUMFormaPago.ComprobanteUS_PagoGS
                    ImporteMoneda = ImporteLocal
                Case ENUMFormaPago.ComprobanteUS_PagoUS
                    ImporteMoneda = CSistema.Cotizador(Cotizacion, ImporteLocal, IDMonedaPago, IDMonedaComprobante)
            End Select

            txtImporteMoneda.SetValue(ImporteMoneda)

        End If

        CalcularTotales()

    End Sub


    Sub CambiarTotalEgresoPorPagoOtraMoneda()

        Exit Sub

        'Validar
        If dtEgresos Is Nothing Then
            Exit Sub
        End If

        If dtEgresos.Rows.Count = 0 Then
            Exit Sub
        End If

        'Si no se selecciono ningun comprobante, salir
        If dtEgresos.Select("Seleccionado='True'").Count = 0 Then
            Exit Sub
        End If

        'Si las monedas de pago y de los comprobantes son iguales, salir
        Dim IDMonedaComprobantes As Integer
        Dim Total As Decimal = 0
        Dim TotalRetencion As Decimal = 0

        For Each oRow As DataRow In dtEgresos.Select("Seleccionado='True'")
            IDMonedaComprobantes = oRow("IDMoneda")

            Dim Cotizacion As Decimal = oRow("Cotizacion")
            Dim Importe As Decimal = oRow("Importe")
            Dim Retencion As Decimal = oRow("RetencionIVA")

            If IDMonedaPago = IDMonedaComprobantes Then
                Total = Total + Importe
                TotalRetencion = TotalRetencion + Retencion
            Else
                'De GS a US
                If IDMonedaComprobantes = 1 Then

                    'Usamos la cotizacion del dia
                    Cotizacion = CotizacionDelDia
                    Total = Total + (Importe / Cotizacion)
                    TotalRetencion = TotalRetencion + (Retencion / Cotizacion)

                End If

                'De US a GS
                If IDMonedaComprobantes <> 1 Then
                    Total = Total + (Importe * Cotizacion)
                    TotalRetencion = TotalRetencion + (Retencion * Cotizacion)
                End If

            End If

        Next

        txtTotalComprobante.SetValue(Total)
        txtTotalRetencion.SetValue(TotalRetencion)
        CalcularImporteCheque()

        ObtenerDiferenciaCambio()

    End Sub

    Private Function ObtenerDiferenciaCambio() As Decimal


        ObtenerDiferenciaCambio = 0

        lblDiferenciaCambio.Visible = False
        txtDiferenciaCambio.Visible = False

        'Validar
        If vNuevo = False Then

            If txtDiferenciaCambio.ObtenerValor <> 0 Then
                lblDiferenciaCambio.Visible = True
                txtDiferenciaCambio.Visible = True
            End If

            Return txtDiferenciaCambio.ObtenerValor

        End If

        If dtEgresos Is Nothing Then
            Exit Function
        End If

        If dtEgresos.Rows.Count = 0 Then
            Exit Function
        End If

        'Si no se selecciono ningun comprobante, salir
        If dtEgresos.Select("Seleccionado='True'").Count = 0 Then
            Exit Function
        End If

        'Variables
        Dim TotalComprobantes As Decimal = 0
        Dim TotalCheque As Decimal = 0
        Dim TotalRetencion As Decimal = 0
        Dim TotalDocumento As Decimal = 0
        Dim TotalDocumentoNegativo As Decimal = 0

        'Si la moneda de pago es GS y los del comprobante GS, no hay diferencia de cambio, se usa el mismo
        'Obtener valores Cheque
        If txtImporte.ObtenerValor > 0 Or txtImporteMoneda.ObtenerValor > 0 Then
            Select Case FormaPago()
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    TotalComprobantes = ObtenerImporteComprobantes(True, False)
                    TotalCheque = ObtenerImporteCheque(True, True)
                Case ENUMFormaPago.ComprobanteGS_PagoGS
                    txtDiferenciaCambio.SetValue(0)
                    Exit Function
                Case ENUMFormaPago.ComprobanteUS_PagoGS

                    'Transformar en US, hayar la diferencia y volver a convertir en GS
                    TotalComprobantes = ObtenerImporteComprobantes(False, False)
                    'TotalRetencion = ObtenerImporteRetencion(False)
                    TotalRetencion = 0
                    TotalComprobantes = TotalComprobantes - TotalRetencion

                    TotalCheque = TotalComprobantes
                    TotalCheque = TotalCheque * txtCotizacion.ObtenerValor

                    TotalComprobantes = ObtenerImporteComprobantes(False, True)
                    'TotalRetencion = ObtenerImporteRetencion(True, False)
                    TotalRetencion = 0
                    TotalComprobantes = TotalComprobantes - TotalRetencion

                Case ENUMFormaPago.ComprobanteUS_PagoUS
                    TotalComprobantes = ObtenerImporteComprobantes(False, True)
                    TotalComprobantes = TotalComprobantes - ObtenerImporteRetencion(True)
                    TotalCheque = ObtenerImporteCheque(True, True)
            End Select
        End If

        'Obtener valores Documentos
        If OcxOrdenPagoDocumento1.dtDocumento.Rows.Count > 0 Then
            Select Case FormaPagoDocumento()
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    TotalComprobantes = ObtenerImporteComprobantes(True, False)
                    TotalDocumento = ObtenerImporteDocumento(False, True, True)
                    TotalDocumentoNegativo = ObtenerImporteDocumento(True, True, True)
                Case ENUMFormaPago.ComprobanteGS_PagoGS
                    txtDiferenciaCambio.SetValue(0)
                    Exit Function
                Case ENUMFormaPago.ComprobanteUS_PagoGS

                    'Transformar en US, hayar la diferencia y volver a convertir en GS
                    TotalComprobantes = ObtenerImporteComprobantes(False, False)
                    TotalRetencion = 0
                    TotalComprobantes = TotalComprobantes - TotalRetencion

                    'Documento
                    If OcxOrdenPagoDocumento1.ObtenerImporte(True, False) > 0 Then
                        'TotalDocumento = TotalComprobantes
                        TotalDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(True, False)
                        TotalDocumento = TotalDocumento * OcxOrdenPagoDocumento1.dtDocumento.Rows(0)("Cotizacion")
                    Else
                        TotalDocumento = 0
                    End If

                    'Documento Negativo
                    If OcxOrdenPagoDocumento1.ObtenerImporte(True, True) > 0 Then
                        'TotalDocumentoNegativo = TotalComprobantes
                        TotalDocumentoNegativo = OcxOrdenPagoDocumento1.ObtenerImporte(True, True)
                        TotalDocumentoNegativo = TotalDocumento * txtCotizacion.ObtenerValor
                    Else
                        TotalDocumentoNegativo = 0
                    End If

                    TotalComprobantes = ObtenerImporteComprobantes(False, True)
                    TotalRetencion = 0
                    TotalComprobantes = TotalComprobantes - TotalRetencion

                Case ENUMFormaPago.ComprobanteUS_PagoUS
                    TotalComprobantes = ObtenerImporteComprobantes(False, True)
                    TotalComprobantes = TotalComprobantes - ObtenerImporteRetencion(True)
                    TotalDocumento = ObtenerImporteDocumento(False, True, True)
                    TotalDocumentoNegativo = ObtenerImporteDocumento(True, True, True)
            End Select
        End If

        ObtenerDiferenciaCambio = TotalCheque + TotalDocumento - TotalComprobantes
        txtDiferenciaCambio.SetValue(ObtenerDiferenciaCambio)

        lblDiferenciaCambio.Visible = True
        txtDiferenciaCambio.Visible = True

    End Function

    Sub AnalizarSiAplicaRetencion()
        vCotizacionRetencion = CType(CSistema.ExecuteScalar("Select IsNull(Max(cotizacion), 1) From Cotizacion Where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' and IDMoneda=" & cbxMoneda.GetValue), Integer)
        'Validamos la retencion
        Dim MontoMinimoRetencion As Decimal = 0

        If IsNumeric(vgConfiguraciones("CompraImporteMinimoRetencion").ToString) = False Then
            MontoMinimoRetencion = 0
        Else
            MontoMinimoRetencion = vgConfiguraciones("CompraImporteMinimoRetencion")
        End If

        '701560
        Dim TotalComprobanteConRetencion As Decimal = CSistema.dtSumColumn(dtEgresos, "Total", "Retener='True' And Seleccionado='True'")

        'Si la moneda es distinta a la local, tenemos que convertir a local
        If cbxMoneda.GetValue <> 1 Then
            TotalComprobanteConRetencion = TotalComprobanteConRetencion * txtCotizacion.ObtenerValor
        End If

        'Ver cotizacion del documento
        If (TotalComprobanteConRetencion * vCotizacionRetencion) < MontoMinimoRetencion Then
            chkAplicarRetencion.Valor = False
        End If

        'Verificamos que el importe supero el monto minimo
        'Si es una moneda diferente a la principal
        Dim TotalComprobante As Decimal = txtTotalComprobante.ObtenerValor
        If cbxMoneda.GetValue <> 1 Then
            'Volvemos a la moneda local
        End If

    End Sub

    Sub ComportamientoImporteCheque()
        Dim CotizacionDia As Decimal = 1
        txtImporte.SoloLectura = True
        txtImporteMoneda.SoloLectura = True

        'Solo si es nuevo
        If vNuevo = False Then
            Exit Sub
        End If
        'CotizacionDia = CSistema.ExecuteScalar("Select dbo.FCotizacionAlDia(" & cbxMoneda.GetValue & ", 1)")

        'SC:14-10-2021 Cambio necesario para extraer cotizacion de la fecha de operacion
        CotizacionDia = CSistema.ExecuteScalar("Select dbo.FCotizacionAlDiaFecha(" & cbxMoneda.GetValue & ",1,'" & CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, False) & "')")
        If txtCotizacion.txt.Text = "0" Then
            txtCotizacion.txt.Text = CotizacionDia

        End If


        Select Case FormaPago()
            Case ENUMFormaPago.ComprobanteGS_PagoGS
                txtImporte.SoloLectura = True
                txtImporteMoneda.SoloLectura = False
            Case ENUMFormaPago.ComprobanteUS_PagoUS
                txtImporte.SoloLectura = True
                txtImporteMoneda.SoloLectura = False
            Case ENUMFormaPago.ComprobanteUS_PagoGS
                txtImporte.SoloLectura = False
                txtImporteMoneda.SoloLectura = True
            Case ENUMFormaPago.ComprobanteGS_PagoUS
                txtImporte.SoloLectura = False
                txtImporteMoneda.SoloLectura = True
        End Select

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From OrdenPago Where IDSucursal=" & cbxSucursal.GetValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From OrdenPago Where IDSucursal=" & cbxSucursal.GetValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

        'Guardar
        If e.KeyCode = vgKeyProcesar Then
            Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
        End If

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles, btnModificar)

    End Sub

    Sub ObtenerInformacionProveedor()

        Dim oRow As DataRow = txtProveedor.Registro
        Dim oRowSucursal As DataRow = txtProveedor.Sucursal

        'Datos Comunes
        txtProveedor.txtReferencia.txt.Text = oRow("Referencia").ToString
        txtProveedor.txtRazonSocial.txt.Text = oRow("RazonSocial").ToString

    End Sub

    Sub ModificarOrdenPago()
        Dim frm As New frmModificarOrdenPago
        frm.Text = "Modificar Orden Pago"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen

        frm.IDTransaccion = IDTransaccion
        frm.dt = dtOrdenPago
        frm.NroCuenta = cbxCuentaBancaria.cbx.Text
        frm.ShowDialog(Me)

        CargarOperacion(frm.IDTransaccion)
    End Sub

    Sub ComportamientoTipoOP()

        If vNuevo = False Then
            Exit Sub
        End If

        If dgw.Rows.Count > 0 Then
            If MessageBox.Show("Se quitara de la lista de egreso los comprobantes. Desea continuar?", "Orden de Pago", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        If cbxListar.SelectedIndex = 0 Then
            btnAgregarComprobante.Enabled = True
            txtProveedor.Enabled = True
            txtProveedor.SoloLectura = False
            chkAplicarRetencion.chk.Checked = True
        End If

        If cbxListar.SelectedIndex = 1 Then
            dgw.Rows.Clear()
            txtTotalComprobante.txt.Text = ""
            txtCantidadComprobante.txt.Text = ""
            txtSaldoTotal.txt.Text = ""
            btnAgregarComprobante.Enabled = False
            txtProveedor.SoloLectura = False
            txtProveedor.Enabled = True
            chkAplicarRetencion.chk.Checked = True
        End If

        If cbxListar.SelectedIndex = 2 Then
            dgw.Rows.Clear()
            txtTotalComprobante.txt.Text = ""
            txtCantidadComprobante.txt.Text = ""
            txtSaldoTotal.txt.Text = ""
            chkAplicarRetencion.chk.Checked = False
            txtProveedor.Clear()
            btnAgregarComprobante.Enabled = False
            txtProveedor.Enabled = False
            LimpiarControles()
        End If

    End Sub

    Sub VisualizarAsientoComprobante()

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim frm As New frmVisualizarAsiento
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.Text = "Compra : " & dgw.SelectedRows(0).Cells(1).Value

        frm.IDTransaccion = dgw.SelectedRows(0).Cells(0).Value

        'Mostramos
        frm.ShowDialog(Me)

    End Sub

    Sub VisualizarDocumento()

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        Dim IDTransaccion As Integer = dgw.Rows(dgw.CurrentCell.RowIndex).Cells("colIDTransaccion").Value
        Dim vdt As DataTable = CSistema.ExecuteToDataTable("Select * From VTransaccion Where IDTransaccion=" & IDTransaccion)

        If vdt Is Nothing Then
            Exit Sub
        End If

        If vdt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = vdt.Rows(0)

        Dim Tipo As String = oRow("FormName").ToString

        Select Case Tipo
            Case "frmCompra"
                Dim frm As New frmCompra
                frm.IDTransaccion = IDTransaccion

                'Ocultar botones
                frm.btnEliminar.Visible = False
                frm.btnAsiento.Visible = False
                frm.btnBusquedaAvanzada.Visible = False
                frm.btnCancelar.Visible = False
                frm.btnGuardar.Visible = False
                frm.btnNuevo.Visible = False

                FGMostrarFormulario(Me, frm, "Compra de Mercaderias", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)
            Case "frmGastos"
                Dim frm As New frmGastos
                frm.IDTransaccion = IDTransaccion
                Dim CajaChica As Boolean = CSistema.RetornarValorBoolean(CSistema.ExecuteToDataTable("Select Top(1) CajaChica From VGasto Where IDTransaccion=" & IDTransaccion)(0)("CajaChica").ToString)

                'Ocultar botones
                frm.btnAnular.Visible = False
                frm.btnAsiento.Visible = False
                frm.btnBusquedaAvanzada.Visible = False
                frm.btnCancelar.Visible = False
                frm.btnGuardar.Visible = False
                frm.btnNuevo.Visible = False

                'Verificar si es caja chica
                frm.CajaChica = CajaChica
                FGMostrarFormulario(Me, frm, "Gastos", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)
            Case "frmVale"
                Dim frm As New frmVale
                frm.IDTransaccion = IDTransaccion

                'Ocultar botones
                frm.btnAnular.Visible = False
                frm.btnAsiento.Visible = False
                frm.btnBusquedaAvanzada.Visible = False
                frm.btnCancelar.Visible = False
                frm.btnGuardar.Visible = False
                frm.btnNuevo.Visible = False
                frm.btnEditar.Visible = False

                FGMostrarFormulario(Me, frm, "Vales", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)
        End Select

    End Sub

    Private Function FormaPago() As ENUMFormaPago


        FormaPago = ENUMFormaPago.ComprobanteGS_PagoGS

        Dim vIDMonedaFormaPago As Integer = IDMonedaPago
        Dim vIDMonedaComprobantes As Integer = cbxMoneda.GetValue

        'ComprobanteGS_PagoGS
        If vIDMonedaComprobantes = 1 And vIDMonedaFormaPago = 1 Then
            Return ENUMFormaPago.ComprobanteGS_PagoGS
        End If

        'ComprobanteGS_PagoUS
        If vIDMonedaComprobantes = 1 And vIDMonedaFormaPago <> 1 Then
            Return ENUMFormaPago.ComprobanteGS_PagoUS
        End If

        'ComprobanteUS_PagoUS
        If vIDMonedaComprobantes <> 1 And vIDMonedaFormaPago <> 1 Then
            Return ENUMFormaPago.ComprobanteUS_PagoUS
        End If

        'ComprobanteUS_PagoGS
        If vIDMonedaComprobantes <> 1 And vIDMonedaFormaPago = 1 Then
            Return ENUMFormaPago.ComprobanteUS_PagoGS
        End If

    End Function

    Private Function FormaPagoDocumento() As ENUMFormaPago

        'SOLO SE PUEDE PAGAR CON LA MISMA MONEDA!!!!
        'EN EL DETALLE OBLIGAR QUE LA MONEDA SEA LA MISMA PARA TODAS LAS FORMAS DE PAGO

        FormaPagoDocumento = ENUMFormaPago.ComprobanteGS_PagoGS

        If OcxOrdenPagoDocumento1.dtDocumento Is Nothing Then
            Exit Function
        End If

        If OcxOrdenPagoDocumento1.dtDocumento.Rows.Count = 0 Then
            Exit Function
        End If

        Dim vIDMonedaFormaPago As Integer = OcxOrdenPagoDocumento1.dtDocumento.Rows(0)("IDMoneda")
        Dim vIDMonedaComprobantes As Integer = cbxMoneda.GetValue

        'ComprobanteGS_PagoGS
        If vIDMonedaComprobantes = 1 And vIDMonedaFormaPago = 1 Then
            Return ENUMFormaPago.ComprobanteGS_PagoGS
        End If

        'ComprobanteGS_PagoUS
        If vIDMonedaComprobantes = 1 And vIDMonedaFormaPago <> 1 Then
            Return ENUMFormaPago.ComprobanteGS_PagoUS
        End If

        'ComprobanteUS_PagoUS
        If vIDMonedaComprobantes <> 1 And vIDMonedaFormaPago <> 1 Then
            Return ENUMFormaPago.ComprobanteUS_PagoUS
        End If

        'ComprobanteUS_PagoGS
        If vIDMonedaComprobantes <> 1 And vIDMonedaFormaPago = 1 Then
            Return ENUMFormaPago.ComprobanteUS_PagoGS
        End If

    End Function

    Private Function ObtenerImporteComprobantes(Optional RestarRetencion As Boolean = True, Optional EnImporteLocal As Boolean = False) As Decimal

        ObtenerImporteComprobantes = 0

        If dtEgresos Is Nothing Then
            Exit Function
        End If

        If dtEgresos.Rows.Count = 0 Then
            Exit Function
        End If

        Dim TotalComprobante As Decimal = 0
        Dim TotalRetencion As Decimal = 0
        Dim Importe As Decimal = 0
        Dim Retencion As Decimal = 0

        For Each oRow As DataRow In dtEgresos.Select("Seleccionado='True'")

            Select Case FormaPago()

                Case ENUMFormaPago.ComprobanteGS_PagoGS
                    TotalComprobante = TotalComprobante + oRow("Importe")
                    TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    TotalComprobante = TotalComprobante + oRow("Importe")
                    TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                Case ENUMFormaPago.ComprobanteUS_PagoUS
                    If EnImporteLocal Then
                        TotalComprobante = TotalComprobante + (oRow("Importe") * oRow("Cotizacion"))
                        TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * oRow("Cotizacion"))
                    Else
                        TotalComprobante = TotalComprobante + oRow("Importe")
                        TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                    End If
                Case ENUMFormaPago.ComprobanteUS_PagoGS
                    If EnImporteLocal Then
                        TotalComprobante = TotalComprobante + (oRow("Importe") * oRow("Cotizacion"))
                        TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * oRow("Cotizacion"))
                    Else
                        TotalComprobante = TotalComprobante + oRow("Importe")
                        TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                    End If

                Case Else
                    TotalComprobante = TotalComprobante + oRow("Importe")
                    TotalRetencion = TotalRetencion + oRow("RetencionIVA")
            End Select

        Next

        'Sacamos la retencion
        If RestarRetencion = True Then
            If chkAplicarRetencion.Valor Then
                TotalComprobante = TotalComprobante - CSistema.FormatoNumero(TotalRetencion, False)
            End If
        End If

        Return TotalComprobante

    End Function

    Private Function ObtenerImporteRetencion(Optional EnImporteLocal As Boolean = False, Optional EnLaCotizacionDelDia As Boolean = True) As Decimal

        ObtenerImporteRetencion = 0

        If dtEgresos Is Nothing Then
            Exit Function
        End If

        If dtEgresos.Rows.Count = 0 Then
            Exit Function
        End If

        Dim TotalRetencion As Decimal = 0

        'La cotizacion se usa desde el del dia
        For Each oRow As DataRow In dtEgresos.Select("Seleccionado='True'")

            Select Case FormaPago()

                Case ENUMFormaPago.ComprobanteGS_PagoGS
                    TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                Case ENUMFormaPago.ComprobanteUS_PagoUS
                    If EnImporteLocal Then
                        If EnLaCotizacionDelDia = True Then
                            TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * txtCotizacion.ObtenerValor)
                            'TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * oRow("Cotizacion"))
                        Else
                            TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * oRow("Cotizacion"))
                        End If
                    Else
                        TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                    End If
                Case ENUMFormaPago.ComprobanteUS_PagoGS
                    If EnImporteLocal Then
                        If EnLaCotizacionDelDia = True Then
                            TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * txtCotizacion.ObtenerValor)
                            'TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * oRow("Cotizacion"))
                        Else
                            TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * oRow("Cotizacion"))
                        End If
                    Else
                        TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                    End If

                Case Else
                    TotalRetencion = TotalRetencion + oRow("RetencionIVA")
            End Select

        Next

        'Sacamos la retencion
        If chkAplicarRetencion.Valor = False Then
            Return 0
        End If

        Return TotalRetencion

    End Function

    Private Function ObtenerImporteCheque(Optional SinRetencion As Boolean = True, Optional EnImporteLocal As Boolean = False) As Decimal

        ObtenerImporteCheque = 0

        If txtImporteMoneda.ObtenerValor < 0 Then
            txtImporteMoneda.SetValue(txtImporteMoneda.ObtenerValor * -1)
        End If

        Select Case FormaPago()

            Case ENUMFormaPago.ComprobanteGS_PagoGS
                ObtenerImporteCheque = txtImporte.ObtenerValor
            Case ENUMFormaPago.ComprobanteGS_PagoUS
                ObtenerImporteCheque = txtImporteMoneda.ObtenerValor * txtCotizacion.ObtenerValor
            Case ENUMFormaPago.ComprobanteUS_PagoUS
                If EnImporteLocal Then
                    ObtenerImporteCheque = txtImporte.ObtenerValor
                Else
                    ObtenerImporteCheque = txtImporteMoneda.ObtenerValor
                End If
            Case ENUMFormaPago.ComprobanteUS_PagoGS
                If EnImporteLocal Then
                    ObtenerImporteCheque = txtImporte.ObtenerValor
                Else
                    ObtenerImporteCheque = txtImporteMoneda.ObtenerValor
                End If
            Case Else
                ObtenerImporteCheque = txtImporte.ObtenerValor
        End Select

    End Function

    Private Sub frmOrdenPago_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
        'FA 20/06/2023
        LiberarMemoria()
    End Sub

    Private Sub frmOrdenPago_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Enter Then

            If txtObservacion.txt.Focused Then
                Exit Sub
            End If

            CSistema.SelectNextControl(Me, e.KeyCode)

        End If
    End Sub

    Private Sub frmOrdenPago_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If CargarAlIniciar = False Then
            Exit Sub
        End If

        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Function ObtenerImporteDocumento(DocumentoDeResta As Boolean, Optional SinRetencion As Boolean = True, Optional EnImporteLocal As Boolean = False) As Decimal

        'SOLO SE PUEDE PAGAR CON LA MISMA MONEDA!!!!
        'EN EL DETALLE OBLIGAR QUE LA MONEDA SEA LA MISMA PARA TODAS LAS FORMAS DE PAGO

        ObtenerImporteDocumento = 0

        For Each oRow As DataRow In OcxOrdenPagoDocumento1.dtDocumento.Rows

            Select Case FormaPagoDocumento()

                Case ENUMFormaPago.ComprobanteGS_PagoGS
                    ObtenerImporteDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(True, DocumentoDeResta)
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    ObtenerImporteDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(False, DocumentoDeResta) * oRow("Cotizacion")
                Case ENUMFormaPago.ComprobanteUS_PagoUS
                    If EnImporteLocal Then
                        ObtenerImporteDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(True, DocumentoDeResta)
                    Else
                        ObtenerImporteDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(False, DocumentoDeResta)
                    End If
                Case ENUMFormaPago.ComprobanteUS_PagoGS
                    If EnImporteLocal Then
                        ObtenerImporteDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(True, DocumentoDeResta)
                    Else
                        ObtenerImporteDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(False, DocumentoDeResta)
                    End If
                Case Else
                    ObtenerImporteDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(True, DocumentoDeResta)
            End Select

            Exit For

        Next

    End Function

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

        If dtOrdenPago.Rows.Count = 0 Then
            Exit Sub
        End If
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Private Sub txtProveedor_ItemSeleccionado(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProveedor.ItemSeleccionado

        If txtProveedor.SoloLectura = True Then
            Exit Sub
        End If

        If txtProveedor.Seleccionado = True Then
            txtFecha.txt.Focus()
            txtOrden.txt.Text = txtProveedor.Registro("RazonSocial").ToString
            chkAplicarRetencion.Valor = txtProveedor.Registro("SujetoRetencion").ToString
        End If

    End Sub

    Private Sub txtProveedor_ItemMalSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProveedor.ItemMalSeleccionado

        If txtProveedor.SoloLectura = True Then
            Exit Sub
        End If
        Dim oRow As DataRow = MVariablesGlobales.vgConfiguraciones
        Dim ALAOrden As String = CSistema.RetornarValorString(oRow("OrdenPagoALaOrdenPredefinido").ToString)
        txtOrden.txt.Text = ALAOrden

    End Sub

    Private Sub btnAgregarComprobante_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarComprobante.Click
        CargarEgresos()
    End Sub

    Private Sub lklEliminarEgreso_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarEgreso.LinkClicked
        EliminarEgreso()
    End Sub

    Private Sub cbxCuentaBancaria_Leave(sender As Object, e As System.EventArgs) Handles cbxCuentaBancaria.Leave
        ObtenerDatosCuentaBancaria()
    End Sub

    Private Sub cbxCuentaBancaria_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCuentaBancaria.PropertyChanged
        ObtenerDatosCuentaBancaria()
    End Sub

    Private Sub txtImporte_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtImporteMoneda.TeclaPrecionada

        If txtImporteMoneda.SoloLectura = True Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Add Then

            Dim Saldo As Decimal = txtSaldoTotal.ObtenerValor
            If Saldo > 0 Then
                Exit Sub
            End If

            Saldo = Saldo * -1
            txtImporteMoneda.SetValue(Saldo)

        End If

        If vNuevo = True Then

            CalcularImporteCheque()

            Select Case cbxListar.SelectedIndex
                Case Is = 0
                    If txtImporteMoneda.ObtenerValor <> 0 Then
                        ObtenerEgresoRetencion()
                    Else
                        CalcularTotales()
                    End If
            End Select

        End If

    End Sub

    Private Sub TabControl1_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TabControl1.KeyUp


        If e.KeyCode = Keys.Tab Then
            If My.Computer.Keyboard.CtrlKeyDown = True Then

                Dim index As Integer
                index = TabControl1.SelectedIndex

                If index = TabControl1.TabPages.Count - 1 Then
                    index = 0
                Else
                    index = index + 1
                End If

                TabControl1.SelectedIndex = index

            End If
        End If
    End Sub

    Private Sub OcxSeleccionarEfectivo1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Importe As Decimal = 0

    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        VisualizarAsientoComprobante()
    End Sub

    Private Sub cbxListar_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxListar.KeyDown
        ComportamientoTipoOP()
    End Sub

    Private Sub cbxListar_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxListar.KeyUp
        ComportamientoTipoOP()
    End Sub

    Private Sub cbxListar_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxListar.SelectedIndexChanged
        ComportamientoTipoOP()
    End Sub

    Private Sub btnAplicarComprobantes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAplicarComprobantes.Click
        CargarAplicar()
    End Sub

    Private Sub txtComprobante_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtComprobante.KeyPress
        If Asc(e.KeyChar) = Keys.Enter Then
            cbxListar.Focus()
        End If
    End Sub

    Private Sub txtObservacion_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtObservacion.TeclaPrecionada

        If e.KeyCode = Keys.Enter Then

            'Si es una carga normal
            If btnAgregarComprobante.Enabled = True Or btnAgregarComprobante.Visible = True Then
                btnAgregarComprobante.Focus()
            End If

            If btnAgregarComprobante.Enabled = False Or btnAgregarComprobante.Visible = False Then
                txtNroCheque.txt.Focus()
            End If

        End If

    End Sub

    Private Sub chkAplicarRetencion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkAplicarRetencion.PropertyChanged
        ObtenerEgresoRetencion()
    End Sub

    Private Sub chkDiferido_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDiferido.CheckedChanged
        If chkDiferido.Checked = True Then
            txtVencimiento.Enabled = True
        Else
            txtVencimiento.Enabled = False
        End If
    End Sub

    Private Sub txtTotalRetencion_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTotalRetencion.Leave
        CalcularTotales()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If txtNroCheque.txt.Text = "" Then
            ModificarOrdenPago()
        Else
            MessageBox.Show("La OP ya tiene cheque, no se puede modificar.", "OP con Cheque", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
    End Sub

    Private Sub lblCotizacionCheque_Click(sender As System.Object, e As System.EventArgs) Handles lblCotizacionCheque.Click

    End Sub

    Private Sub cbxListar_Leave(sender As System.Object, e As System.EventArgs) Handles cbxListar.Leave

    End Sub

    Private Sub cbxMoneda_Leave(sender As System.Object, e As System.EventArgs) Handles cbxMoneda.Leave
        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)
        ComportamientoImporteCheque()
    End Sub

    Private Sub txtCotizacion_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtCotizacion.TeclaPrecionada
        If vNuevo = True Then
            'CalcularImporteCheque()
            'Cerar los importes
            txtImporte.SetValue(0)
            txtImporteMoneda.SetValue(0)
            CalcularTotales()

        End If
    End Sub

    Private Sub ElimininarDeLaListaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles tsmiEliminarComprobante.Click
        EliminarEgreso()
    End Sub

    Private Sub LinkLabel2_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        VisualizarDocumento()
    End Sub

    Private Sub VisualizarElComprobanteToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles VisualizarElComprobanteToolStripMenuItem.Click
        VisualizarDocumento()
    End Sub

    Private Sub VisualizarAsientoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles VisualizarAsientoToolStripMenuItem.Click
        VisualizarAsientoComprobante()
    End Sub

    Private Sub txtCotizacion_Leave(sender As System.Object, e As System.EventArgs) Handles txtCotizacion.Leave
        ComportamientoImporteCheque()
    End Sub

    Private Sub txtImporte_TeclaPrecionada_1(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtImporte.TeclaPrecionada

        If txtImporte.SoloLectura = True Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Add Then

            Dim Saldo As Decimal = 0
            Dim TotalComprobantes As Decimal = 0
            Dim TotalCheque As Decimal = 0
            Dim TotalRetencion As Decimal = 0

            Select Case FormaPago()
                Case ENUMFormaPago.ComprobanteGS_PagoGS
                    Saldo = txtSaldoTotal.ObtenerValor
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    TotalComprobantes = ObtenerImporteComprobantes(True, True)
                    Saldo = TotalComprobantes * -1
                Case ENUMFormaPago.ComprobanteUS_PagoGS

                    TotalComprobantes = ObtenerImporteComprobantes(False, False)
                    TotalComprobantes = TotalComprobantes * txtCotizacion.ObtenerValor

                    TotalRetencion = ObtenerImporteRetencion(False, False)
                    TotalRetencion = TotalRetencion * txtCotizacion.ObtenerValor

                    TotalComprobantes = TotalComprobantes - TotalRetencion

                    Saldo = TotalComprobantes * -1

                Case ENUMFormaPago.ComprobanteUS_PagoUS
                    Saldo = txtSaldoTotal.ObtenerValor
            End Select

            If Saldo > 0 Then
                Exit Sub
            End If

            Saldo = Saldo * -1
            txtImporte.SetValue(Saldo)

        End If

        If vNuevo = True Then

            CalcularImporteCheque()

            Select Case cbxListar.SelectedIndex
                Case Is = 0
                    If txtImporteMoneda.ObtenerValor <> 0 Then
                        ObtenerEgresoRetencion()
                    Else
                        CalcularTotales()
                    End If
            End Select

        End If
    End Sub

    Private Sub txtComprobante_Leave(sender As System.Object, e As System.EventArgs) Handles txtComprobante.Leave
        If vNuevo = True Then
            OcxOrdenPagoDocumento1.Comprobante = txtComprobante.GetValue
        End If
    End Sub

    Private Sub txtFecha_Leave(sender As System.Object, e As System.EventArgs) Handles txtFecha.Leave
        If vNuevo = True Then
            OcxOrdenPagoDocumento1.Fecha = txtFecha.GetValue
        End If
    End Sub

    Private Sub OcxOrdenPagoDocumento1_RegistroEliminado(sender As System.Object, e As System.EventArgs, oRow As System.Data.DataRow) Handles OcxOrdenPagoDocumento1.RegistroEliminado
        CalcularTotales()
    End Sub

    Private Sub OcxOrdenPagoDocumento1_RegistroInsertado(sender As System.Object, e As System.EventArgs, oRow As System.Data.DataRow) Handles OcxOrdenPagoDocumento1.RegistroInsertado
        CalcularTotales()
    End Sub

    Private Sub cbxMoneda_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxMoneda.TeclaPrecionada
        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)
        ComportamientoImporteCheque()
    End Sub

    Private Sub btnEliminarAplicacion_Click(sender As Object, e As EventArgs) Handles btnEliminarAplicacion.Click
        EliminarAplicacion()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmOrdenPago_Activate()
        Me.Refresh()
    End Sub


End Class
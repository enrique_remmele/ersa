﻿Imports ERP.Reporte

Public Class frmEntregaChequeOP

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    'Dim CAsiento As New CAsientoEntregaChequeOP
    Dim CReporteOrdenPago As New CReporteOrdenPago
    Dim CData As New CData

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private ChequeValue As Boolean
    Public Property Cheque As Boolean
        Get
            Return ChequeValue
        End Get
        Set(ByVal value As Boolean)
            ChequeValue = value
        End Set
    End Property

    Private DecimalesValue As Boolean
    Public Property Decimales() As Boolean
        Get
            Return DecimalesValue
        End Get
        Set(ByVal value As Boolean)
            DecimalesValue = value
            txtImporteMoneda.Decimales = value
        End Set
    End Property

    Private CotizacionDelDiaValue As Decimal

    'VARIABLES
    Dim dtEgresos As New DataTable
    Dim dtEfectivo As New DataTable
    Dim dtOrdenPago As New DataTable
    Dim dtEntregaChequeOP As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean = False
    Dim vImporte As Decimal
    Dim vTotalop As Decimal
    Dim Monto As Double = 0
    Dim MontoLetras As String = ""
    Dim IDTransaccionEntrega As Integer = 0
    Dim vTotalCheque As Decimal
    Dim vTotalProveedor As Decimal
    Dim vCuenaContableProveedor As String
    Dim vDiferenciaCambio As Decimal

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "ENTREGA DE CHEQUE", "ECHQ")
        vNuevo = False

        'Funciones
        CargarInformacion()

        'Clases
        'CAsiento.InicializarAsiento()

        'Otros
        cbxListar.Items.Add("PAGO A PROVEEDORES")
        cbxListar.Items.Add("ANTICIPO A PROVEEDORES")
        cbxListar.Items.Add("EGRESO A RENDIR")
        cbxListar.SelectedIndex = 0

        'Controles
        txtProveedor.Conectar()
        txtFechaHoy.Hoy()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        ''Cabecera
        'CSistema.CargaControl(vControles, cbxSucursal)
        'CSistema.CargaControl(vControles, cbxTipoComprobante)
        'CSistema.CargaControl(vControles, txtComprobante)
        'CSistema.CargaControl(vControles, cbxMoneda)
        'CSistema.CargaControl(vControles, txtProveedor)
        'CSistema.CargaControl(vControles, txtFecha)
        'CSistema.CargaControl(vControles, txtObservacion)

        ''Cheque
        'CSistema.CargaControl(vControles, cbxCuentaBancaria)
        'CSistema.CargaControl(vControles, txtNroCheque)
        'CSistema.CargaControl(vControles, txtFechaCheque)
        'CSistema.CargaControl(vControles, txtFechaPagoCheque)
        'CSistema.CargaControl(vControles, txtCotizacion)
        'CSistema.CargaControl(vControles, txtImporteMoneda)
        'CSistema.CargaControl(vControles, chkDiferido)
        ''CSistema.CargaControl(vControles, txtVencimiento)
        'CSistema.CargaControl(vControles, txtOrden)
        'CSistema.CargaControl(vControles, cbxListar)

        CSistema.CargaControl(vControles, txtFechaEntrega)
        CSistema.CargaControl(vControles, txtRecibo)
        CSistema.CargaControl(vControles, txtRetiradoPor)

        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Tipo de Comprobante
        Dim vIDOperacionOP As Integer = CSistema.ObtenerIDOperacion(frmOrdenPago.Name, "ORDEN DE PAGO", "OP")
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & vIDOperacionOP)

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Cuenta Bancaria
        CSistema.SqlToComboBox(cbxCuentaBancaria.cbx, CData.GetTable("VCuentaBancaria").Copy, "ID", "CuentaBancaria")

        'listar
        cbxListar.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "LISTAR", "")

        'Ultimo Registro
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From OrdenPago Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""


        txtID.txt.Focus()
        txtID.txt.SelectAll()

        dtEgresos.Rows.Clear()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From OrdenPago Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
        dtOrdenPago = CSistema.ExecuteToDataTable("Select * From VOrdenPago Where IDTransaccion=" & IDTransaccion)
        vTotalop = CSistema.dtSumColumn(dtEgresos, "Importe")

        If dtOrdenPago Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dtOrdenPago.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dtOrdenPago.Rows(0)

        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        cbxMoneda.SelectedValue(oRow("IDMoneda").ToString)
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        txtObservacion.txt.Text = oRow("Observacion").ToString

        vImporte = oRow("Total")
        If (oRow("PagoProveedor")) = True Then
            cbxListar.SelectedIndex = 0
        End If
        If oRow("AnticipoProveedor").ToString = True Then
            cbxListar.SelectedIndex = 1
        End If
        If oRow("EgresoRendir").ToString = True Then
            cbxListar.SelectedIndex = 2
        End If

        'Proveedor
        If oRow("IDProveedor") > 0 Then
            txtProveedor.SetValue(oRow("IDProveedor"))
        Else
            txtProveedor.SetValueString(0, "", "")
        End If

        'Cheque
        InicializarControlesCheque()

        If CBool(oRow("Cheque")) = True Then

            cbxCuentaBancaria.SelectedValue(oRow("IDCuentaBancaria"))
            txtNroCheque.txt.Text = oRow("NroCheque").ToString
            txtFechaCheque.SetValue(oRow("FechaCheque"))
            txtFechaPagoCheque.SetValue(oRow("FechaPago"))
            txtCotizacion.SetValue(oRow("Cotizacion"))
            txtImporteMoneda.SetValue(oRow("ImporteMoneda"))
            CalcularImporteCheque()
            chkDiferido.Checked = oRow("Diferido")
            txtVencimiento.SetValue(oRow("FechaVencimiento"))
            txtOrden.txt.Text = oRow("ALaOrden").ToString

        End If

        'ENTREGA DE CHEQUES
        CargarEntregaChequeOP(CBool(oRow("Anulado")), IDTransaccion)

        'Se carga los datos para cuando una OP se encuentra anulada
        lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
        lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString


        'Inicializamos el Asiento
        'CAsiento.Limpiar()

        'OcxSeleccionarEfectivo1.dgw.Enabled = False

        Dim dtAsiento As DataTable = CSistema.ExecuteToDataTable("Select 'Credito'=Sum(Credito) From VDetalleAsiento Where IDTransaccion=" & IDTransaccion)

        If dtAsiento.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow2 As DataRow = dtAsiento.Rows(0)
        Monto = CSistema.FormatoNumero(oRow2("Credito").ToString)

    End Sub

    Sub CargarEntregaChequeOP(ByVal opAnulado As Boolean, Optional ByVal vIDTransaccion As Integer = 0)

        'Obtener IDTransaccion
        IDTransaccionEntrega = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From EntregaChequeOP Where IDTransaccionOP=" & vIDTransaccion & "), 0 )")

        If IDTransaccionEntrega = 0 Then
            txtFechaEntrega.Clear()
            txtRecibo.txt.Text = ""
            txtRetiradoPor.txt.Text = ""
            chkEntregado.Checked = False
            HabilitarAsiento()
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
        dtEntregaChequeOP = CSistema.ExecuteToDataTable("Select * From VEntregaChequeOP Where IDTransaccion=" & IDTransaccionEntrega)
        vTotalop = CSistema.dtSumColumn(dtEgresos, "Importe")

        If dtEntregaChequeOP.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dtEntregaChequeOP.Rows(0)

        txtFechaEntrega.txt.Text = oRow("FechaEntrega").ToString
        txtRecibo.txt.Text = oRow("Recibo").ToString
        txtRetiradoPor.txt.Text = oRow("RetiradoPor").ToString
        chkEntregado.Checked = oRow("ChequeEntregado")

        'Si la entrega esta anulada
        If CBool(oRow("Anulado").ToString) = True And opAnulado Then

            'Visible Label
            lblUsuarioAnulado.Visible = True
            lblAnulado.Visible = True
            lblAnulado.Visible = True
            lblFechaAnulado.Visible = True
            flpAnuladoPor.Visible = True
            btnAnular.Enabled = False
            btnModificar.Enabled = False

        Else
            flpAnuladoPor.Visible = False
            btnAnular.Enabled = True
            btnModificar.Enabled = True
        End If

        HabilitarAsiento()

    End Sub

    Sub HabilitarAsiento()
        If chkEntregado.Checked = True Then
            btnAsiento.Enabled = True
        Else
            btnAsiento.Enabled = False
        End If
    End Sub

    Sub CalcularImporteCheque()

        If cbxCuentaBancaria.cbx.SelectedValue Is Nothing Then
            Exit Sub
        End If

        Dim ImporteMoneda As Decimal = txtImporteMoneda.ObtenerValor
        Dim IDMoneda As Integer = cbxMoneda.GetValue
        Dim Cotizacion As Decimal = txtCotizacion.ObtenerValor
        Dim ImporteLocal As Decimal = CSistema.Cotizador(Cotizacion, ImporteMoneda, IDMoneda)

        txtImporte.SetValue(ImporteLocal)

    End Sub

    Sub InicializarControlesCheque()

        txtNroCheque.txt.Clear()
        txtCotizacion.SetValue(1)
        txtImporte.SetValue(0)
        txtImporteMoneda.SetValue(0)
        chkDiferido.Checked = 0
        txtVencimiento.txt.Clear()
        txtOrden.txt.Clear()

    End Sub

    'Sub GenerarAsiento()

    '    Dim TotalCheque As Decimal

    '    Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

    '    oRow("IDCiudad") = cbxCiudad.cbx.SelectedValue
    '    oRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
    '    oRow("Fecha") = txtFechaEntrega.GetValue
    '    oRow("IDMoneda") = cbxMoneda.GetValue
    '    oRow("Cotizacion") = txtCotizacion.ObtenerValor
    '    oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
    '    oRow("NroComprobante") = txtComprobante.txt.Text
    '    oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text
    '    oRow("Detalle") = txtObservacion.txt.Text

    '    'Si la moneda es diferente al local, pasar convertido 
    '    TotalCheque = txtImporteMoneda.ObtenerValor

    '    'Convertir a Moneda local SI es que el ID <> 1 (Moneda local siempre es ID=1)
    '    If cbxMoneda.GetValue <> 1 Then
    '        TotalCheque = TotalCheque * txtCotizacion.ObtenerValor
    '    End If

    '    CAsiento.dtAsiento.Rows.Clear()
    '    CAsiento.dtAsiento.Rows.Add(oRow)
    '    CAsiento.TotalCheque = vTotalCheque
    '    If chkDiferido.Checked Then
    '        CAsiento.TotalProveedor = vTotalCheque
    '    Else
    '        CAsiento.TotalProveedor = vTotalProveedor - vDiferenciaCambio
    '    End If
    '    CAsiento.CuentaContableProveedor = vCuenaContableProveedor
    '    CAsiento.DiferenciaCambio = vDiferenciaCambio
    '    CAsiento.Diferido = chkDiferido.Checked

    '    If txtProveedor.Seleccionado = True Then
    '        CAsiento.IDProveedor = txtProveedor.Registro("ID").ToString
    '    End If

    '    If CAsiento.Generado = True Then
    '        Exit Sub
    '    End If

    '    CAsiento.Generar()

    'End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Validar
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            'Fecha de entrega
            If txtFechaEntrega.txt.Text = "" Then
                Dim mensaje As String = "Se debe ingresar la fecha de entrega del cheque!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If

            'Recibo
            If txtRecibo.txt.Text = "" Then
                Dim mensaje As String = "Se debe ingresar el recibo correspondiente a la entrega del cheque!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If

            'Retirado por
            If txtRetiradoPor.txt.Text = "" Then
                Dim mensaje As String = "Se debe ingresar el nombre de la persona que retiro el cheque!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If

            If CDate(txtFechaEntrega.txt.Text) < Today Then
                If MessageBox.Show("Fecha de Entrega menor a hoy! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
                    If txtFechaEntrega.Enabled = True Then
                        txtFechaEntrega.Focus()
                        Exit Function
                    Else
                        Cancelar()
                        Exit Function
                    End If
                End If
            End If

            ''Asiento
            'If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            '    'Validar el Asiento
            '    If CAsiento.ObtenerSaldo <> 0 Then
            '        CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            '        Exit Function
            '    End If

            '    If CAsiento.ObtenerTotal = 0 And cbxListar.SelectedIndex = 0 Then
            '        CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            '        Exit Function
            '    End If

            'End If

        End If

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If IDTransaccionEntrega = 0 Then
                CSistema.MostrarError("No puede anular un cheque que todavía no ha sido retirado!", ctrError, txtRetiradoPor, tsslEstado, ErrorIconAlignment.TopRight)
                Return False
            End If


            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If

        End If

        Return True

    End Function

    Sub Modificar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)
        vNuevo = True
        txtFechaEntrega.Hoy()
        'Obtener los importes totales para los asientos
        'vTotalCheque = CSistema.ExecuteScalar("Select IsNull(Sum(DA.Debito)+Sum(DA.Credito),0) From VDetalleAsiento DA Where DA.IDTransaccion=" & IDTransaccion & " And DA.Codigo=(Select Top(1) Codigo From VCFEntregaChequeOP CF Where CF.Cheque='True')")
        vTotalCheque = CSistema.ExecuteScalar("Select IsNull(Sum(DA.Debito)+Sum(DA.Credito),0) From VDetalleAsiento DA Where DA.IDTransaccion=" & IDTransaccion & " And DA.Codigo in(Select Codigo From VCFEntregaChequeOP CF Where CF.Cheque='True')")
        'vTotalProveedor = CSistema.ExecuteScalar("Select IsNull(Sum(DA.Debito)+Sum(DA.Credito),0) From OrdenPago OP Join OrdenPagoEgreso OPE On OP.IDTransaccion=OPE.IDTransaccionOrdenPago Join VDetalleAsiento DA On OPE.IDTransaccionEgreso=DA.IDTransaccion Where OP.IDTransaccion=" & IDTransaccion & " And DA.Codigo=(Select Top(1) Codigo From VCFEntregaChequeOP CF Where CF.Proveedor='True')")
        'vTotalProveedor = CSistema.ExecuteScalar("Select IsNull(Sum(DA.Debito)+Sum(DA.Credito),0) From OrdenPago OP Join OrdenPagoEgreso OPE On OP.IDTransaccion=OPE.IDTransaccionOrdenPago Join VDetalleAsiento DA On OPE.IDTransaccionEgreso=DA.IDTransaccion Where OP.IDTransaccion=" & IDTransaccion & " And DA.Codigo in(Select Codigo From VCFEntregaChequeOP CF Where CF.Proveedor='True')")
        'Trae montos desde OrdenPagoEgreso
        vTotalProveedor = CSistema.ExecuteScalar("Select IsNull(round(sum(OPE.Importe * OP.Cotizacion),2),0) From OrdenPago OP Join OrdenPagoEgreso OPE On OP.IDTransaccion=OPE.IDTransaccionOrdenPago Join VDetalleAsiento DA On OPE.IDTransaccionEgreso=DA.IDTransaccion Where OP.IDTransaccion=" & IDTransaccion & " And DA.Codigo in(Select Codigo From VCFEntregaChequeOP CF Where CF.Proveedor='True')")
        vCuenaContableProveedor = CSistema.ExecuteScalar("Select DA.Codigo From OrdenPago OP Join OrdenPagoEgreso OPE On OP.IDTransaccion=OPE.IDTransaccionOrdenPago Join VDetalleAsiento DA On OPE.IDTransaccionEgreso=DA.IDTransaccion Where OP.IDTransaccion=" & IDTransaccion & " And DA.Codigo in(Select Codigo From VCFEntregaChequeOP CF Where CF.Proveedor='True')")
        vDiferenciaCambio = CSistema.ExecuteScalar("Select top(1)IsNull(round(OP.DiferenciaCambio,0),0) From OrdenPago OP Join OrdenPagoEgreso OPE On OP.IDTransaccion=OPE.IDTransaccionOrdenPago Join VDetalleAsiento DA On OPE.IDTransaccionEgreso=DA.IDTransaccion Where OP.IDTransaccion=" & IDTransaccion & " And DA.Codigo in(Select Codigo From VCFEntregaChequeOP CF Where CF.Proveedor='True')")
    End Sub

    Sub Anular()

        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
        vNuevo = False

    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        '   If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS And cbxListar.SelectedIndex = 0 Then
        ' 'Generar la cabecera del asiento
        'GenerarAsiento()
        ' End If

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer
        Dim Decimales As Boolean = False


        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccionEntrega, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDTransaccionOP", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ChequeEntregado", "True", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaEntrega", CSistema.FormatoFechaBaseDatos(txtFechaEntrega.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Recibo", txtRecibo.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@RetiradoPor", txtRetiradoPor.txt.Text, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then
            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpEntregaChequeOP", False, False, MensajeRetorno, IDTransaccion) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
                ''Eliminar el Registro si es que se registro
                'If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From EntregaChequeOP Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                '    param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                '    CSistema.ExecuteStoreProcedure(param, "SpEntregaChequeOP", False, False, MensajeRetorno, IDTransaccion)
                'End If
                Exit Sub
            End If
        End If
        
        'ver 
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then
            'Cargamos el asiento
            'CAsiento.IDTransaccion = IDTransaccion
            'CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
            CSistema.ExecuteNonQuery("Exec SpAsientoEntregaCheque " & IDTransaccion, , 60)

        End If

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            If CSistema.ExecuteStoreProcedure(param, "SpEntregaChequeOP", False, False, MensajeRetorno, IDTransaccion) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

                Exit Sub
            End If
            CSistema.ExecuteNonQuery("Exec SpAsientoEntregaCheque " & IDTransaccion)
            'CAsiento.IDTransaccion = IDTransaccion
            'CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
        End If
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

    End Sub

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()



        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccionEntrega, ParameterDirection.Input)
        'SC: Se envia FechaEntrega 
        CSistema.SetSQLParameter(param, "@FechaEntrega", CSistema.FormatoFechaBaseDatos(txtFechaHoy.GetValue.ToShortDateString, True, False), ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpEntregaChequeOP", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)

            Exit Sub
        Else

            tsslEstado.Text = MensajeRetorno

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False


    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        Dim Comprobante As String = cbxTipoComprobante.cbx.Text & ": " & txtComprobante.txt.Text

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante
            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From vEntregaChequeOP Where NumeroOP=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else
            MessageBox.Show("Debe Guardar la Operacion para visualizar el asiento", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            ''Validar
            'If cbxCiudad.cbx.SelectedValue Is Nothing Then
            '    Dim mensaje As String = "Seleccione correctamente la ciudad de operacion!"
            '    ctrError.SetError(cbxCiudad, mensaje)
            '    ctrError.SetIconAlignment(cbxCiudad, ErrorIconAlignment.TopLeft)
            '    tsslEstado.Text = mensaje
            '    Exit Sub
            'End If

            'If cbxSucursal.cbx.SelectedValue Is Nothing Then
            '    Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
            '    ctrError.SetError(cbxSucursal, mensaje)
            '    ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
            '    tsslEstado.Text = mensaje
            '    Exit Sub
            'End If

            'Dim frm As New frmAsiento
            'frm.WindowState = FormWindowState.Normal
            'frm.StartPosition = FormStartPosition.CenterScreen
            'frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            'frm.Text = Comprobante

            'GenerarAsiento()

            'frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            'CAsiento.ListarDetalle(frm.dgv)
            'frm.CalcularTotales()

            'frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

            ''Mostramos
            'frm.ShowDialog(Me)

            ''Actualizamos el asiento si es que este tuvo alguna modificacion
            'CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            'CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento

            'If frm.VolverAGenerar = True Then
            '    CAsiento.Generado = False
            '    CAsiento.dtAsiento.Clear()
            '    CAsiento.dtDetalleAsiento.Clear()
            '    VisualizarAsiento()
            'End If

        End If

    End Sub

    Sub Buscar()

        Dim frm As New frmConsultaOrdenPago
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog()
        CargarOperacion(frm.IDTransaccion)

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        vNuevo = False
        txtID.txt.Focus()

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From OrdenPago Where IDSucursal=" & cbxSucursal.GetValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From OrdenPago Where IDSucursal=" & cbxSucursal.GetValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        'If e.KeyCode = vgKeyNuevoRegistro Then
        '    Nuevo()
        'End If

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, New Button, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles, btnModificar)

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

        If dtOrdenPago.Rows.Count = 0 Then
            Exit Sub
        End If
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Private Sub frmEntregaChequeOP_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        If chkEntregado.Checked = True Then
            Guardar(ERP.CSistema.NUMOperacionesRegistro.UPD)
        Else
            Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
        End If
    End Sub

    Private Sub btnModificar_Click(sender As System.Object, e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    Private Sub btnAsiento_Click(sender As Object, e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnAnular_Click(sender As System.Object, e As System.EventArgs) Handles btnAnular.Click
        Anular()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmEntregaChequeOP_Activate()
        Me.Refresh()
    End Sub

End Class
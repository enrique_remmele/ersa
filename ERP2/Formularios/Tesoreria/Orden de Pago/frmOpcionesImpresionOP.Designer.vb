﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOpcionesImpresionOP
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chkImprimirObservacion = New System.Windows.Forms.CheckBox()
        Me.chkSeccionFacturasSeparadas = New System.Windows.Forms.CheckBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'chkImprimirObservacion
        '
        Me.chkImprimirObservacion.AutoSize = True
        Me.chkImprimirObservacion.Location = New System.Drawing.Point(13, 14)
        Me.chkImprimirObservacion.Name = "chkImprimirObservacion"
        Me.chkImprimirObservacion.Size = New System.Drawing.Size(124, 17)
        Me.chkImprimirObservacion.TabIndex = 0
        Me.chkImprimirObservacion.Text = "Imprimir Observacion"
        Me.chkImprimirObservacion.UseVisualStyleBackColor = True
        '
        'chkSeccionFacturasSeparadas
        '
        Me.chkSeccionFacturasSeparadas.AutoSize = True
        Me.chkSeccionFacturasSeparadas.Location = New System.Drawing.Point(13, 47)
        Me.chkSeccionFacturasSeparadas.Name = "chkSeccionFacturasSeparadas"
        Me.chkSeccionFacturasSeparadas.Size = New System.Drawing.Size(173, 17)
        Me.chkSeccionFacturasSeparadas.TabIndex = 1
        Me.chkSeccionFacturasSeparadas.Text = "Seccion de Facturas Separada"
        Me.chkSeccionFacturasSeparadas.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(153, 80)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(99, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Imprimir"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmOpcionesImpresionOP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(268, 115)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.chkSeccionFacturasSeparadas)
        Me.Controls.Add(Me.chkImprimirObservacion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmOpcionesImpresionOP"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmOpcionesImpresionOP"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents chkImprimirObservacion As CheckBox
    Friend WithEvents chkSeccionFacturasSeparadas As CheckBox
    Friend WithEvents Button1 As Button
End Class

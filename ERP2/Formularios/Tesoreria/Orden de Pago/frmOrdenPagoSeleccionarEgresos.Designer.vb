﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOrdenPagoSeleccionarEgresos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.QuitarTodaSeleccionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SeleccionarTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.lnkComprobantes = New System.Windows.Forms.LinkLabel()
        Me.lnkSeleccionar = New System.Windows.Forms.LinkLabel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtCantidadPagado = New ERP.ocxTXTNumeric()
        Me.txtTotalPagado = New ERP.ocxTXTNumeric()
        Me.lblTotalCobrado = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtCantidadComprobante = New ERP.ocxTXTNumeric()
        Me.txtTotalComprobante = New ERP.ocxTXTNumeric()
        Me.lblContado = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel4.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'QuitarTodaSeleccionToolStripMenuItem
        '
        Me.QuitarTodaSeleccionToolStripMenuItem.Name = "QuitarTodaSeleccionToolStripMenuItem"
        Me.QuitarTodaSeleccionToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.QuitarTodaSeleccionToolStripMenuItem.Text = "Quitar toda seleccion"
        '
        'SeleccionarTodoToolStripMenuItem
        '
        Me.SeleccionarTodoToolStripMenuItem.Name = "SeleccionarTodoToolStripMenuItem"
        Me.SeleccionarTodoToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.SeleccionarTodoToolStripMenuItem.Text = "Seleccionar todo"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SeleccionarTodoToolStripMenuItem, Me.QuitarTodaSeleccionToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(187, 48)
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 450)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(904, 22)
        Me.StatusStrip1.TabIndex = 15
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.Button2)
        Me.FlowLayoutPanel4.Controls.Add(Me.Button1)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(3, 419)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(898, 28)
        Me.FlowLayoutPanel4.TabIndex = 2
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(820, 3)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 13
        Me.Button2.Text = "&Cancelar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(729, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(85, 23)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "&Aceptar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Location = New System.Drawing.Point(3, 3)
        Me.dgw.Name = "dgw"
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(892, 244)
        Me.dgw.TabIndex = 0
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 4
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 215.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 211.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 214.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.lnkComprobantes, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.lnkSeleccionar, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.FlowLayoutPanel1, 3, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.FlowLayoutPanel2, 2, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 352)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(898, 61)
        Me.TableLayoutPanel3.TabIndex = 1
        '
        'lnkComprobantes
        '
        Me.lnkComprobantes.AutoSize = True
        Me.lnkComprobantes.Location = New System.Drawing.Point(261, 0)
        Me.lnkComprobantes.Name = "lnkComprobantes"
        Me.lnkComprobantes.Size = New System.Drawing.Size(127, 13)
        Me.lnkComprobantes.TabIndex = 4
        Me.lnkComprobantes.TabStop = True
        Me.lnkComprobantes.Text = "Comprobantes Fondo Fijo"
        '
        'lnkSeleccionar
        '
        Me.lnkSeleccionar.AutoSize = True
        Me.lnkSeleccionar.Location = New System.Drawing.Point(3, 0)
        Me.lnkSeleccionar.Name = "lnkSeleccionar"
        Me.lnkSeleccionar.Size = New System.Drawing.Size(87, 13)
        Me.lnkSeleccionar.TabIndex = 0
        Me.lnkSeleccionar.TabStop = True
        Me.lnkSeleccionar.Text = "Seleccionar todo"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtCantidadPagado)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalPagado)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblTotalCobrado)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(687, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(208, 55)
        Me.FlowLayoutPanel1.TabIndex = 3
        '
        'txtCantidadPagado
        '
        Me.txtCantidadPagado.Color = System.Drawing.Color.Empty
        Me.txtCantidadPagado.Decimales = True
        Me.txtCantidadPagado.Indicaciones = Nothing
        Me.txtCantidadPagado.Location = New System.Drawing.Point(160, 3)
        Me.txtCantidadPagado.Name = "txtCantidadPagado"
        Me.txtCantidadPagado.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadPagado.SoloLectura = False
        Me.txtCantidadPagado.TabIndex = 0
        Me.txtCantidadPagado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadPagado.Texto = "0"
        '
        'txtTotalPagado
        '
        Me.txtTotalPagado.Color = System.Drawing.Color.Empty
        Me.txtTotalPagado.Decimales = True
        Me.txtTotalPagado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalPagado.Indicaciones = Nothing
        Me.txtTotalPagado.Location = New System.Drawing.Point(71, 3)
        Me.txtTotalPagado.Name = "txtTotalPagado"
        Me.txtTotalPagado.Size = New System.Drawing.Size(83, 22)
        Me.txtTotalPagado.SoloLectura = True
        Me.txtTotalPagado.TabIndex = 2
        Me.txtTotalPagado.TabStop = False
        Me.txtTotalPagado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalPagado.Texto = "0"
        '
        'lblTotalCobrado
        '
        Me.lblTotalCobrado.Location = New System.Drawing.Point(4, 0)
        Me.lblTotalCobrado.Name = "lblTotalCobrado"
        Me.lblTotalCobrado.Size = New System.Drawing.Size(61, 22)
        Me.lblTotalCobrado.TabIndex = 1
        Me.lblTotalCobrado.Text = "Pagado:"
        Me.lblTotalCobrado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.txtCantidadComprobante)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtTotalComprobante)
        Me.FlowLayoutPanel2.Controls.Add(Me.lblContado)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(476, 3)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(205, 55)
        Me.FlowLayoutPanel2.TabIndex = 2
        '
        'txtCantidadComprobante
        '
        Me.txtCantidadComprobante.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobante.Decimales = True
        Me.txtCantidadComprobante.Indicaciones = Nothing
        Me.txtCantidadComprobante.Location = New System.Drawing.Point(157, 3)
        Me.txtCantidadComprobante.Name = "txtCantidadComprobante"
        Me.txtCantidadComprobante.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadComprobante.SoloLectura = False
        Me.txtCantidadComprobante.TabIndex = 2
        Me.txtCantidadComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobante.Texto = "0"
        '
        'txtTotalComprobante
        '
        Me.txtTotalComprobante.Color = System.Drawing.Color.Empty
        Me.txtTotalComprobante.Decimales = True
        Me.txtTotalComprobante.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalComprobante.Indicaciones = Nothing
        Me.txtTotalComprobante.Location = New System.Drawing.Point(68, 3)
        Me.txtTotalComprobante.Name = "txtTotalComprobante"
        Me.txtTotalComprobante.Size = New System.Drawing.Size(83, 22)
        Me.txtTotalComprobante.SoloLectura = True
        Me.txtTotalComprobante.TabIndex = 1
        Me.txtTotalComprobante.TabStop = False
        Me.txtTotalComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalComprobante.Texto = "0"
        '
        'lblContado
        '
        Me.lblContado.Location = New System.Drawing.Point(3, 0)
        Me.lblContado.Name = "lblContado"
        Me.lblContado.Size = New System.Drawing.Size(59, 22)
        Me.lblContado.TabIndex = 0
        Me.lblContado.Text = "Total:"
        Me.lblContado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel3, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.dgw, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(904, 450)
        Me.TableLayoutPanel1.TabIndex = 14
        '
        'frmOrdenPagoSeleccionarEgresos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(904, 472)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmOrdenPagoSeleccionarEgresos"
        Me.Text = "frmOrdenPagoSeleccionarEgresos"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents QuitarTodaSeleccionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SeleccionarTodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents lnkComprobantes As LinkLabel
    Friend WithEvents lnkSeleccionar As LinkLabel
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents txtCantidadPagado As ocxTXTNumeric
    Friend WithEvents txtTotalPagado As ocxTXTNumeric
    Friend WithEvents lblTotalCobrado As Label
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents txtCantidadComprobante As ocxTXTNumeric
    Friend WithEvents txtTotalComprobante As ocxTXTNumeric
    Friend WithEvents lblContado As Label
    Friend WithEvents dgw As DataGridView
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents Button2 As Button
    Friend WithEvents Button1 As Button
End Class

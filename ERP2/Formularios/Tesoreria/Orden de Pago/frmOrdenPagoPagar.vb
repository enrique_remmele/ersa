﻿Public Class frmOrdenPagoPagar

    'PROPIEDADES
    Public Property dtEgresosAPagar As DataTable

    'FUNCIONES
    Overloads Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(frmOrdenPago.Name, "ORDEN DE PAGO", "OP")
        vNuevo = False

        'Clases
        CAsiento.InicializarAsiento()

        'Otros
        cbxListar.Items.Add("PAGO A PROVEEDORES")
        cbxListar.Items.Add("ANTICIPO A PROVEEDORES")
        cbxListar.Items.Add("EGRESO A RENDIR")
        cbxListar.SelectedIndex = 0

        'Controles
        txtProveedor.Conectar()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)
        Dim k As Keys = vgKeyProcesar
        btnGuardar.Text = "&Guardar(" & k.ToString & ")"
        btnCancelar.Visible = False
        btnNuevo.Visible = False
        btnModificar.Visible = False
        btnAnular.Visible = False

        'Funciones
        CargarInformacion()
        PreCargar()
        BloquearControles()

        'Foco
        Me.ActiveControl = cbxTipoComprobante
        cbxTipoComprobante.cbx.Focus()

    End Sub

    Overloads Sub CargarInformacion()

        ReDim vControles(-1)

        'Cabecera
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, txtProveedor)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtObservacion)

        'Egresos
        CSistema.CargaControl(vControles, btnAgregarComprobante)
        If vNuevo = True Then
            CSistema.CargaControl(vControles, btnAplicarComprobantes)
        End If
        CSistema.CargaControl(vControles, lklEliminarEgreso)
        CSistema.CargaControl(vControles, chkAplicarRetencion)
        CSistema.CargaControl(vControles, txtTotalRetencion)

        'Cheque
        CSistema.CargaControl(vControles, cbxCuentaBancaria)
        CSistema.CargaControl(vControles, txtNroCheque)
        CSistema.CargaControl(vControles, txtFechaCheque)
        CSistema.CargaControl(vControles, txtFechaPagoCheque)
        CSistema.CargaControl(vControles, txtCotizacion)
        CSistema.CargaControl(vControles, txtImporteMoneda)
        CSistema.CargaControl(vControles, chkDiferido)
        CSistema.CargaControl(vControles, txtOrden)
        CSistema.CargaControl(vControles, cbxListar)

        'Tipo de Pago
        TipoPago = ENUMTipoPago.PagoAProveedorees
        EstablecerTipoPago(Me.TipoPago)

        'CARGAR ESTRUCTURA DEL DETALLE VENTA

        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, frmOrdenPago.Name, "CIUDAD", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, frmOrdenPago.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, frmOrdenPago.Name, "TIPO COMPROBANTE", "")

        'Cuenta Bancaria
        CSistema.SqlToComboBox(cbxCuentaBancaria.cbx, CData.GetTable("VCuentaBancaria").Copy, "ID", "CuentaBancaria")

        'listar
        cbxListar.Text = CArchivoInicio.IniGet(VGArchivoINI, frmOrdenPago.Name, "LISTAR", "")

        'Ultimo Registro
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From OrdenPago Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)

    End Sub

    Sub BloquearControles()

        'CABECERA
        cbxCiudad.SoloLectura = True
        cbxSucursal.SoloLectura = True
        txtID.SoloLectura = True

        cbxListar.Visible = False
        cbxMoneda.SoloLectura = True
        txtProveedor.SoloLectura = True

        'EGRESOS
        btnAgregarComprobante.Visible = False
        btnAplicarComprobantes.Visible = False
        lklEliminarEgreso.Visible = False

        'CHEQUE
        cbxCuentaBancaria.SoloLectura = True
        txtImporteMoneda.SoloLectura = True

        'EFECTIVO

    End Sub

    Public Sub PreCargar()

        'CABECERA
        Nuevo(True)

        Dim oRow As DataRow = dtEgresosAPagar.Rows(0)
        '!!!Conectar la moneda necesariamente, de lo contrario no funciona.....
        cbxMoneda.Conectar()
        cbxMoneda.SelectedValue(oRow("IDMoneda").ToString)
        'IDMonedaPago = oRow("IDMoneda").ToString
        txtFecha.SetValue(Now)
        cbxListar.SelectedIndex = 0
        Decimales = CType(CSistema.ExecuteScalar("Select decimales from Moneda where ID = " & oRow("IDMoneda").ToString), Boolean)

        'PROVEEDOR
        txtProveedor.SetValue(oRow("IDProveedor"))
        If txtProveedor.Seleccionado Then
            txtOrden.txt.Text = txtProveedor.Registro("RazonSocial").ToString
        End If

        'EGRESOS
        CargarEgresoAPagar()
        ListarEgresos()
        ObtenerEgresoRetencion()
        AnalizarSiAplicaRetencion()

        'CHEQUE
        'CSistema.SqlToComboBox(cbxCuentaBancaria.cbx, CData.GetTable("VCuentaBancaria").Copy, "ID", "CuentaBancaria")
        cbxCuentaBancaria.SelectedValue(oRow("IDCuentaBancaria").ToString)
        txtFechaCheque.SetValue(Now)
        txtFechaPagoCheque.SetValue(Now)
        CotizacionDelDia = CSistema.CotizacionDelDia(IDMonedaPago, IDOperacion)
        txtCotizacion.SetValue(CotizacionDelDia)

        'Calcular el importe del cheque
        Dim ImporteCheque As Decimal = CSistema.dtSumColumn(dtEgresosAPagar, "ImporteAPagar")
        If chkAplicarRetencion.Valor Then
            ImporteCheque = ImporteCheque - txtTotalRetencion.ObtenerValor
        End If

        txtImporteMoneda.SetValue(ImporteCheque)

        'Calcular Totales
        CalcularImporteCheque()
        CalcularTotales()

    End Sub

    Sub CargarEgresoAPagar()

        dtEgresos = dtEgresosAPagar.Copy
        For Each SeleccionRow As DataRow In dtEgresos.Rows
            SeleccionRow("Seleccionado") = True
        Next


        'If dtEgresos Is Nothing Then
        '    Exit Sub
        'End If

        'For Each oRow As DataRow In dtEgresosAPagar.Rows

        '    For Each SeleccionRow As DataRow In dtEgresos.Select("IDTransaccion=" & oRow("IDTransaccion").ToString)
        '        SeleccionRow("Seleccionado") = True
        '    Next

        'Next

    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmOrdenPagoPagar_Activate()
        Me.Refresh()
    End Sub
End Class

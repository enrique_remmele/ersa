﻿Public Class frmImpresionOrdenPago

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim COrdenPago As New Reporte.CReporteOrdenPago

    'VARIABLES
    Dim vdtOrdenPago As DataTable
    Dim vdtCuentaBancaria As DataTable
    Dim vdtCiudad As DataTable

    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        CSistema.InicializaControles(Me)
        CargarInformacion()

        'Botones inhabilitados
        btnImprimir.Enabled = False

        'Textbox inhabilitados
        cbxCuenta.Enabled = False
        txtBanco.Enabled = False

    End Sub

    Sub CargarInformacion()

        'Cuentas Bancarias
        vdtCuentaBancaria = CData.GetTable("VCuentaBancaria").Copy
        CSistema.SqlToComboBox(cbxCuenta.cbx, vdtCuentaBancaria, "ID", "Cuenta")

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal"), "ID", "Codigo")

        'Numeros no impresos, MIN y MAX de OP
        Dim vdttemp As DataTable = CSistema.ExecuteToDataTable("Select 'Min'=IsNull(MIN(Numero), 0), 'Max'=IsNull(MAX(Numero), 0) From VOrdenPago Where IDSucursal=" & cbxSucursal.GetValue & " And Impreso is Null or Impreso='False' ")

        If vdttemp Is Nothing Then
            Exit Sub
        End If

        If vdttemp.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = vdttemp.Rows(0)
        txtOPInicial.SetValue(oRow("Min"))
        txtOPFinal.SetValue(oRow("Max"))

        DesplegarImpresoras()

    End Sub

    Sub DesplegarImpresoras()

        cbxImpresora.cbx.Items.Clear()

        For Each strPrinter As String In System.Drawing.Printing.PrinterSettings.InstalledPrinters
            cbxImpresora.cbx.Items.Add(strPrinter)
        Next

    End Sub

    Sub Cargar()

        btnImprimir.Enabled = False

        Dim SQL As String = "select o.*, 'credito'=(Select Sum(Credito) From DetalleAsiento Where IDTransaccion=o.IDTransaccion) from vordenpago o Where o.Numero Between " & txtOPInicial.ObtenerValor & " And " & txtOPFinal.ObtenerValor & " "

        If chkIncluirImpresos.Valor = False Then
            SQL = SQL & " And o.Impreso is Null or o.Impreso='False' "
        End If

        If OcxCHK1.Valor = True Then
            SQL = SQL & " And o.IDCuentaBancaria=" & cbxCuenta.GetValue & " "
        End If

        vdtOrdenPago = CSistema.ExecuteToDataTable(SQL & " Order By o.Numero")

        If vdtOrdenPago Is Nothing Then
            MessageBox.Show("No se encontraron registros", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If vdtOrdenPago.Rows.Count = 0 Then
            MessageBox.Show("No se encontraron registros", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        'Habilitar controles
        btnImprimir.Enabled = True

    End Sub

    Sub Procesar()

        'Validar
        'Si es que MAX y MIN OP no sean 0
        If txtOPInicial.ObtenerValor = 0 Or txtOPFinal.ObtenerValor = 0 Then
            CSistema.MostrarError("Los numeros de OP no concuerdan", ctrError, btnImprimir, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'MIN sea menor a MAX
        If txtOPInicial.ObtenerValor > txtOPFinal.ObtenerValor Then
            CSistema.MostrarError("La OPInicial no puede ser mayor a la OPFinal", ctrError, btnImprimir, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'Elegir una impresora
        If cbxImpresora.cbx.Text = "" Then
            CSistema.MostrarError("Debe elegir una impresora", ctrError, btnImprimir, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        For Each oRow As DataRow In vdtOrdenPago.Rows

            'Impimir
            ' Imprimir(oRow("IDTransaccion"), oRow("TotalImporteMoneda").ToString)
            Imprimir(oRow("IDTransaccion"), oRow("credito").ToString)

            If ActualizarOrdenPago(oRow("IDTransaccion")) = False Then
                'Controlar
            End If

        Next

        Me.Close()

    End Sub

    Function ActualizarOrdenPago(ByVal IDTransaccion As Integer) As Boolean

        ActualizarOrdenPago = False

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        'Insertar Registro
        Dim MensajeRetorno As String = ""
        If CSistema.ExecuteStoreProcedure(param, "SpActualizarImpresoOrdenPago", False, False, MensajeRetorno) = False Then
            Return False
        Else
            Return True
        End If

    End Function

    Sub Imprimir(IDTransaccion As Integer, Total As Decimal)

        Dim MontoLetra As String = CSistema.NumeroALetra(Total)

        If chkObservacion.chk.Checked = True Then
            COrdenPago.ImprimirOrdenPagoConObservacion(New frmReporte, IDTransaccion, vgUsuario, MontoLetra, cbxImpresora.cbx.Text, Not chkSeccionFacturasSeparadas.Checked)
        Else
            COrdenPago.ImprimirOrdenPagoDirecto(New frmReporte, IDTransaccion, vgUsuario, MontoLetra, cbxImpresora.cbx.Text, Not chkSeccionFacturasSeparadas.Checked)
        End If

    End Sub

    Private Sub frmImpresionOrdenPago_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmImpresionOrdenPago_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Procesar()
    End Sub
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        Cargar()
    End Sub

    Private Sub cbxCuenta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxCuenta.PropertyChanged
        For Each oRow As DataRow In vdtCuentaBancaria.Select(" ID = " & cbxCuenta.GetValue)
            txtBanco.txt.Text = oRow("Referencia")
        Next
    End Sub

    Private Sub OcxCHK1_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles OcxCHK1.PropertyChanged
        cbxCuenta.Enabled = OcxCHK1.Valor
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmImpresionOrdenPago_Activate()
        Me.Refresh()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmOrdenPago
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.cbxListar = New System.Windows.Forms.ComboBox()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.lblTipoPago = New System.Windows.Forms.Label()
        Me.gbxComprobantes = New System.Windows.Forms.GroupBox()
        Me.btnEliminarAplicacion = New System.Windows.Forms.Button()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.Documentos = New System.Windows.Forms.TabPage()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBanco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colProveedor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCotizacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmiEliminarComprobante = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.VisualizarAsientoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VisualizarElComprobanteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Auditoria = New System.Windows.Forms.TabPage()
        Me.dgvAuditoria = New System.Windows.Forms.DataGridView()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.chkAplicarRetencion = New ERP.ocxCHK()
        Me.txtTotalRetencion = New ERP.ocxTXTNumeric()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.btnAplicarComprobantes = New System.Windows.Forms.Button()
        Me.txtCantidadComprobante = New ERP.ocxTXTNumeric()
        Me.txtTotalComprobante = New ERP.ocxTXTNumeric()
        Me.lblTotalComprobante = New System.Windows.Forms.Label()
        Me.lklEliminarEgreso = New System.Windows.Forms.LinkLabel()
        Me.btnAgregarComprobante = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.lblOrden = New System.Windows.Forms.Label()
        Me.lblFechaPagoCheque = New System.Windows.Forms.Label()
        Me.lblCotizacionCheque = New System.Windows.Forms.Label()
        Me.lblDiferido = New System.Windows.Forms.Label()
        Me.chkDiferido = New System.Windows.Forms.CheckBox()
        Me.lblVencimiento = New System.Windows.Forms.Label()
        Me.lblImporteMoneda = New System.Windows.Forms.Label()
        Me.lblNroCheque = New System.Windows.Forms.Label()
        Me.lblFechaCheque = New System.Windows.Forms.Label()
        Me.lblCuentaBancaria = New System.Windows.Forms.Label()
        Me.lblSaldoTotal = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tpCheque = New System.Windows.Forms.TabPage()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.txtVencimiento = New ERP.ocxTXTDate()
        Me.txtOrden = New ERP.ocxTXTString()
        Me.cbxCuentaBancaria = New ERP.ocxCBX()
        Me.txtFechaPagoCheque = New ERP.ocxTXTDate()
        Me.txtFechaCheque = New ERP.ocxTXTDate()
        Me.txtMoneda = New ERP.ocxTXTString()
        Me.txtBanco = New ERP.ocxTXTString()
        Me.txtNroCheque = New ERP.ocxTXTString()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.txtImporteMoneda = New ERP.ocxTXTNumeric()
        Me.tpDocumento = New System.Windows.Forms.TabPage()
        Me.OcxOrdenPagoDocumento1 = New ERP.ocxOrdenPagoDocumento()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.lblDiferenciaCambio = New System.Windows.Forms.Label()
        Me.txtDiferenciaCambio = New ERP.ocxTXTNumeric()
        Me.txtSaldoTotal = New ERP.ocxTXTNumeric()
        Me.gbxCabecera.SuspendLayout()
        Me.gbxComprobantes.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.Documentos.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.Auditoria.SuspendLayout()
        CType(Me.dgvAuditoria, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.flpRegistradoPor.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tpCheque.SuspendLayout()
        Me.tpDocumento.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.txtProveedor)
        Me.gbxCabecera.Controls.Add(Me.cbxMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.cbxListar)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblProveedor)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Controls.Add(Me.lblTipoPago)
        Me.gbxCabecera.Location = New System.Drawing.Point(12, -1)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(669, 94)
        Me.gbxCabecera.TabIndex = 0
        Me.gbxCabecera.TabStop = False
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 100
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(210, 35)
        Me.txtProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(449, 27)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 12
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = True
        Me.cbxMoneda.DataDisplayMember = "Referencia"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(76, 38)
        Me.cbxMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(63, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 10
        Me.cbxMoneda.Texto = ""
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(15, 42)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 9
        Me.lblMoneda.Text = "Moneda:"
        '
        'cbxListar
        '
        Me.cbxListar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxListar.FormattingEnabled = True
        Me.cbxListar.Location = New System.Drawing.Point(500, 12)
        Me.cbxListar.Name = "cbxListar"
        Me.cbxListar.Size = New System.Drawing.Size(160, 21)
        Me.cbxListar.TabIndex = 8
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(210, 64)
        Me.txtObservacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(450, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 16
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(242, 12)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(94, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 5, 30, 8, 29, 32, 812)
        Me.txtFecha.Location = New System.Drawing.Point(76, 64)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(69, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 14
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(24, 68)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 13
        Me.lblFecha.Text = "Fecha:"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(373, 12)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(63, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 6
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(438, 12)
        Me.txtComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(59, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 7
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(141, 42)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(59, 13)
        Me.lblProveedor.TabIndex = 11
        Me.lblProveedor.Text = "Proveedor:"
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(76, 12)
        Me.cbxCiudad.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(63, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Introduzca el código y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(139, 12)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(56, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(168, 68)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(32, 13)
        Me.lblObservacion.TabIndex = 15
        Me.lblObservacion.Text = "Obs.:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(194, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(339, 16)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(37, 13)
        Me.lblComprobante.TabIndex = 5
        Me.lblComprobante.Text = "Comp:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(5, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operación:"
        '
        'lblTipoPago
        '
        Me.lblTipoPago.BackColor = System.Drawing.Color.RoyalBlue
        Me.lblTipoPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoPago.ForeColor = System.Drawing.Color.White
        Me.lblTipoPago.Location = New System.Drawing.Point(500, 12)
        Me.lblTipoPago.Name = "lblTipoPago"
        Me.lblTipoPago.Size = New System.Drawing.Size(160, 21)
        Me.lblTipoPago.TabIndex = 17
        Me.lblTipoPago.Tag = "TIPO DE PAGO"
        Me.lblTipoPago.Text = "----"
        Me.lblTipoPago.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'gbxComprobantes
        '
        Me.gbxComprobantes.Controls.Add(Me.btnEliminarAplicacion)
        Me.gbxComprobantes.Controls.Add(Me.TabControl2)
        Me.gbxComprobantes.Controls.Add(Me.LinkLabel2)
        Me.gbxComprobantes.Controls.Add(Me.chkAplicarRetencion)
        Me.gbxComprobantes.Controls.Add(Me.txtTotalRetencion)
        Me.gbxComprobantes.Controls.Add(Me.LinkLabel1)
        Me.gbxComprobantes.Controls.Add(Me.btnAplicarComprobantes)
        Me.gbxComprobantes.Controls.Add(Me.txtCantidadComprobante)
        Me.gbxComprobantes.Controls.Add(Me.txtTotalComprobante)
        Me.gbxComprobantes.Controls.Add(Me.lblTotalComprobante)
        Me.gbxComprobantes.Controls.Add(Me.lklEliminarEgreso)
        Me.gbxComprobantes.Controls.Add(Me.btnAgregarComprobante)
        Me.gbxComprobantes.Location = New System.Drawing.Point(12, 90)
        Me.gbxComprobantes.Name = "gbxComprobantes"
        Me.gbxComprobantes.Size = New System.Drawing.Size(670, 259)
        Me.gbxComprobantes.TabIndex = 1
        Me.gbxComprobantes.TabStop = False
        '
        'btnEliminarAplicacion
        '
        Me.btnEliminarAplicacion.Location = New System.Drawing.Point(534, 12)
        Me.btnEliminarAplicacion.Name = "btnEliminarAplicacion"
        Me.btnEliminarAplicacion.Size = New System.Drawing.Size(130, 21)
        Me.btnEliminarAplicacion.TabIndex = 12
        Me.btnEliminarAplicacion.Text = "Eliminar Aplicacion"
        Me.btnEliminarAplicacion.UseVisualStyleBackColor = True
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.Documentos)
        Me.TabControl2.Controls.Add(Me.Auditoria)
        Me.TabControl2.Location = New System.Drawing.Point(9, 39)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(657, 185)
        Me.TabControl2.TabIndex = 11
        '
        'Documentos
        '
        Me.Documentos.Controls.Add(Me.dgw)
        Me.Documentos.Location = New System.Drawing.Point(4, 22)
        Me.Documentos.Name = "Documentos"
        Me.Documentos.Padding = New System.Windows.Forms.Padding(3)
        Me.Documentos.Size = New System.Drawing.Size(649, 159)
        Me.Documentos.TabIndex = 0
        Me.Documentos.Text = "Documentos"
        Me.Documentos.UseVisualStyleBackColor = True
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colBanco, Me.colFecha, Me.colProveedor, Me.colMoneda, Me.colCotizacion, Me.colTotal, Me.colSaldo, Me.colImporte})
        Me.dgw.ContextMenuStrip = Me.ContextMenuStrip1
        Me.dgw.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgw.Location = New System.Drawing.Point(3, 3)
        Me.dgw.Name = "dgw"
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(643, 153)
        Me.dgw.StandardTab = True
        Me.dgw.TabIndex = 2
        '
        'colIDTransaccion
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = "0"
        Me.colIDTransaccion.DefaultCellStyle = DataGridViewCellStyle2
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colBanco
        '
        Me.colBanco.HeaderText = "Comprobante"
        Me.colBanco.Name = "colBanco"
        '
        'colFecha
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colFecha.DefaultCellStyle = DataGridViewCellStyle3
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.ReadOnly = True
        Me.colFecha.Width = 80
        '
        'colProveedor
        '
        Me.colProveedor.HeaderText = "Proveedor"
        Me.colProveedor.Name = "colProveedor"
        Me.colProveedor.ReadOnly = True
        Me.colProveedor.Width = 170
        '
        'colMoneda
        '
        Me.colMoneda.HeaderText = "Mon"
        Me.colMoneda.Name = "colMoneda"
        Me.colMoneda.ReadOnly = True
        Me.colMoneda.Width = 35
        '
        'colCotizacion
        '
        Me.colCotizacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colCotizacion.HeaderText = "Cot"
        Me.colCotizacion.Name = "colCotizacion"
        '
        'colTotal
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.NullValue = "0"
        Me.colTotal.DefaultCellStyle = DataGridViewCellStyle4
        Me.colTotal.HeaderText = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.ReadOnly = True
        Me.colTotal.Width = 80
        '
        'colSaldo
        '
        DataGridViewCellStyle5.NullValue = Nothing
        Me.colSaldo.DefaultCellStyle = DataGridViewCellStyle5
        Me.colSaldo.HeaderText = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.Width = 75
        '
        'colImporte
        '
        DataGridViewCellStyle6.NullValue = Nothing
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle6
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.Width = 80
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmiEliminarComprobante, Me.ToolStripSeparator1, Me.VisualizarAsientoToolStripMenuItem, Me.VisualizarElComprobanteToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(211, 76)
        '
        'tsmiEliminarComprobante
        '
        Me.tsmiEliminarComprobante.Name = "tsmiEliminarComprobante"
        Me.tsmiEliminarComprobante.Size = New System.Drawing.Size(210, 22)
        Me.tsmiEliminarComprobante.Text = "Elimininar de la lista"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(207, 6)
        '
        'VisualizarAsientoToolStripMenuItem
        '
        Me.VisualizarAsientoToolStripMenuItem.Name = "VisualizarAsientoToolStripMenuItem"
        Me.VisualizarAsientoToolStripMenuItem.Size = New System.Drawing.Size(210, 22)
        Me.VisualizarAsientoToolStripMenuItem.Text = "Visualizar asiento"
        '
        'VisualizarElComprobanteToolStripMenuItem
        '
        Me.VisualizarElComprobanteToolStripMenuItem.Name = "VisualizarElComprobanteToolStripMenuItem"
        Me.VisualizarElComprobanteToolStripMenuItem.Size = New System.Drawing.Size(210, 22)
        Me.VisualizarElComprobanteToolStripMenuItem.Text = "Visualizar el comprobante"
        '
        'Auditoria
        '
        Me.Auditoria.Controls.Add(Me.dgvAuditoria)
        Me.Auditoria.Location = New System.Drawing.Point(4, 22)
        Me.Auditoria.Name = "Auditoria"
        Me.Auditoria.Padding = New System.Windows.Forms.Padding(3)
        Me.Auditoria.Size = New System.Drawing.Size(649, 159)
        Me.Auditoria.TabIndex = 1
        Me.Auditoria.Text = "Auditoria"
        Me.Auditoria.UseVisualStyleBackColor = True
        '
        'dgvAuditoria
        '
        Me.dgvAuditoria.BackgroundColor = System.Drawing.Color.White
        Me.dgvAuditoria.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAuditoria.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAuditoria.Location = New System.Drawing.Point(3, 3)
        Me.dgvAuditoria.Name = "dgvAuditoria"
        Me.dgvAuditoria.Size = New System.Drawing.Size(643, 153)
        Me.dgvAuditoria.TabIndex = 0
        '
        'LinkLabel2
        '
        Me.LinkLabel2.AutoSize = True
        Me.LinkLabel2.Location = New System.Drawing.Point(121, 235)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(88, 13)
        Me.LinkLabel2.TabIndex = 5
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "Ver comprobante"
        '
        'chkAplicarRetencion
        '
        Me.chkAplicarRetencion.BackColor = System.Drawing.Color.Transparent
        Me.chkAplicarRetencion.Color = System.Drawing.Color.Empty
        Me.chkAplicarRetencion.Location = New System.Drawing.Point(287, 231)
        Me.chkAplicarRetencion.Margin = New System.Windows.Forms.Padding(4)
        Me.chkAplicarRetencion.Name = "chkAplicarRetencion"
        Me.chkAplicarRetencion.Size = New System.Drawing.Size(107, 21)
        Me.chkAplicarRetencion.SoloLectura = False
        Me.chkAplicarRetencion.TabIndex = 6
        Me.chkAplicarRetencion.Texto = "Aplicar retención:"
        Me.chkAplicarRetencion.Valor = False
        '
        'txtTotalRetencion
        '
        Me.txtTotalRetencion.Color = System.Drawing.Color.Empty
        Me.txtTotalRetencion.Decimales = True
        Me.txtTotalRetencion.Indicaciones = Nothing
        Me.txtTotalRetencion.Location = New System.Drawing.Point(400, 230)
        Me.txtTotalRetencion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalRetencion.Name = "txtTotalRetencion"
        Me.txtTotalRetencion.Size = New System.Drawing.Size(76, 22)
        Me.txtTotalRetencion.SoloLectura = True
        Me.txtTotalRetencion.TabIndex = 7
        Me.txtTotalRetencion.TabStop = False
        Me.txtTotalRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalRetencion.Texto = "0"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(55, 235)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(60, 13)
        Me.LinkLabel1.TabIndex = 4
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Ver asiento"
        '
        'btnAplicarComprobantes
        '
        Me.btnAplicarComprobantes.Location = New System.Drawing.Point(400, 12)
        Me.btnAplicarComprobantes.Name = "btnAplicarComprobantes"
        Me.btnAplicarComprobantes.Size = New System.Drawing.Size(130, 21)
        Me.btnAplicarComprobantes.TabIndex = 1
        Me.btnAplicarComprobantes.Text = "Aplicar comprobantes"
        Me.btnAplicarComprobantes.UseVisualStyleBackColor = True
        '
        'txtCantidadComprobante
        '
        Me.txtCantidadComprobante.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobante.Decimales = True
        Me.txtCantidadComprobante.Indicaciones = Nothing
        Me.txtCantidadComprobante.Location = New System.Drawing.Point(619, 230)
        Me.txtCantidadComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCantidadComprobante.Name = "txtCantidadComprobante"
        Me.txtCantidadComprobante.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadComprobante.SoloLectura = True
        Me.txtCantidadComprobante.TabIndex = 10
        Me.txtCantidadComprobante.TabStop = False
        Me.txtCantidadComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobante.Texto = "0"
        '
        'txtTotalComprobante
        '
        Me.txtTotalComprobante.Color = System.Drawing.Color.Empty
        Me.txtTotalComprobante.Decimales = True
        Me.txtTotalComprobante.Indicaciones = Nothing
        Me.txtTotalComprobante.Location = New System.Drawing.Point(515, 230)
        Me.txtTotalComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalComprobante.Name = "txtTotalComprobante"
        Me.txtTotalComprobante.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalComprobante.SoloLectura = True
        Me.txtTotalComprobante.TabIndex = 9
        Me.txtTotalComprobante.TabStop = False
        Me.txtTotalComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalComprobante.Texto = "0"
        '
        'lblTotalComprobante
        '
        Me.lblTotalComprobante.AutoSize = True
        Me.lblTotalComprobante.Location = New System.Drawing.Point(480, 235)
        Me.lblTotalComprobante.Name = "lblTotalComprobante"
        Me.lblTotalComprobante.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalComprobante.TabIndex = 8
        Me.lblTotalComprobante.Text = "Total:"
        '
        'lklEliminarEgreso
        '
        Me.lklEliminarEgreso.AutoSize = True
        Me.lklEliminarEgreso.Location = New System.Drawing.Point(6, 235)
        Me.lklEliminarEgreso.Name = "lklEliminarEgreso"
        Me.lklEliminarEgreso.Size = New System.Drawing.Size(43, 13)
        Me.lklEliminarEgreso.TabIndex = 3
        Me.lklEliminarEgreso.TabStop = True
        Me.lklEliminarEgreso.Text = "Eliminar"
        '
        'btnAgregarComprobante
        '
        Me.btnAgregarComprobante.Location = New System.Drawing.Point(9, 12)
        Me.btnAgregarComprobante.Name = "btnAgregarComprobante"
        Me.btnAgregarComprobante.Size = New System.Drawing.Size(62, 21)
        Me.btnAgregarComprobante.TabIndex = 0
        Me.btnAgregarComprobante.Text = "Agregar"
        Me.btnAgregarComprobante.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(4, 536)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(66, 23)
        Me.btnAnular.TabIndex = 8
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 578)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(689, 22)
        Me.StatusStrip1.TabIndex = 16
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(70, 536)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(72, 23)
        Me.btnImprimir.TabIndex = 9
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(292, 536)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 11
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(142, 536)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(70, 23)
        Me.btnBusquedaAvanzada.TabIndex = 10
        Me.btnBusquedaAvanzada.Text = "&Busqueda"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(601, 536)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 15
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(517, 536)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 14
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(367, 536)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 12
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(442, 536)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 13
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Location = New System.Drawing.Point(34, 100)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(75, 13)
        Me.lblOrden.TabIndex = 20
        Me.lblOrden.Text = "A la Orden de:"
        '
        'lblFechaPagoCheque
        '
        Me.lblFechaPagoCheque.AutoSize = True
        Me.lblFechaPagoCheque.Location = New System.Drawing.Point(181, 47)
        Me.lblFechaPagoCheque.Name = "lblFechaPagoCheque"
        Me.lblFechaPagoCheque.Size = New System.Drawing.Size(83, 13)
        Me.lblFechaPagoCheque.TabIndex = 8
        Me.lblFechaPagoCheque.Text = "Fecha de Pago:"
        '
        'lblCotizacionCheque
        '
        Me.lblCotizacionCheque.AutoSize = True
        Me.lblCotizacionCheque.Location = New System.Drawing.Point(34, 73)
        Me.lblCotizacionCheque.Name = "lblCotizacionCheque"
        Me.lblCotizacionCheque.Size = New System.Drawing.Size(59, 13)
        Me.lblCotizacionCheque.TabIndex = 14
        Me.lblCotizacionCheque.Text = "Cotización:"
        '
        'lblDiferido
        '
        Me.lblDiferido.AutoSize = True
        Me.lblDiferido.Location = New System.Drawing.Point(341, 46)
        Me.lblDiferido.Name = "lblDiferido"
        Me.lblDiferido.Size = New System.Drawing.Size(86, 13)
        Me.lblDiferido.TabIndex = 10
        Me.lblDiferido.Text = "Cheque Diferido:"
        '
        'chkDiferido
        '
        Me.chkDiferido.AutoSize = True
        Me.chkDiferido.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkDiferido.Location = New System.Drawing.Point(429, 46)
        Me.chkDiferido.Name = "chkDiferido"
        Me.chkDiferido.Size = New System.Drawing.Size(15, 14)
        Me.chkDiferido.TabIndex = 11
        Me.chkDiferido.UseVisualStyleBackColor = True
        '
        'lblVencimiento
        '
        Me.lblVencimiento.AutoSize = True
        Me.lblVencimiento.Location = New System.Drawing.Point(445, 47)
        Me.lblVencimiento.Name = "lblVencimiento"
        Me.lblVencimiento.Size = New System.Drawing.Size(116, 13)
        Me.lblVencimiento.TabIndex = 12
        Me.lblVencimiento.Text = "Fecha de Vencimiento:"
        '
        'lblImporteMoneda
        '
        Me.lblImporteMoneda.AutoSize = True
        Me.lblImporteMoneda.Location = New System.Drawing.Point(181, 73)
        Me.lblImporteMoneda.Name = "lblImporteMoneda"
        Me.lblImporteMoneda.Size = New System.Drawing.Size(102, 13)
        Me.lblImporteMoneda.TabIndex = 16
        Me.lblImporteMoneda.Text = "Importe de Moneda:"
        '
        'lblNroCheque
        '
        Me.lblNroCheque.AutoSize = True
        Me.lblNroCheque.Location = New System.Drawing.Point(431, 21)
        Me.lblNroCheque.Name = "lblNroCheque"
        Me.lblNroCheque.Size = New System.Drawing.Size(70, 13)
        Me.lblNroCheque.TabIndex = 4
        Me.lblNroCheque.Text = "Nro. Cheque:"
        '
        'lblFechaCheque
        '
        Me.lblFechaCheque.AutoSize = True
        Me.lblFechaCheque.Location = New System.Drawing.Point(34, 47)
        Me.lblFechaCheque.Name = "lblFechaCheque"
        Me.lblFechaCheque.Size = New System.Drawing.Size(40, 13)
        Me.lblFechaCheque.TabIndex = 6
        Me.lblFechaCheque.Text = "Fecha:"
        '
        'lblCuentaBancaria
        '
        Me.lblCuentaBancaria.AutoSize = True
        Me.lblCuentaBancaria.Location = New System.Drawing.Point(34, 21)
        Me.lblCuentaBancaria.Name = "lblCuentaBancaria"
        Me.lblCuentaBancaria.Size = New System.Drawing.Size(44, 13)
        Me.lblCuentaBancaria.TabIndex = 0
        Me.lblCuentaBancaria.Text = "Cuenta:"
        '
        'lblSaldoTotal
        '
        Me.lblSaldoTotal.AutoSize = True
        Me.lblSaldoTotal.Location = New System.Drawing.Point(515, 515)
        Me.lblSaldoTotal.Name = "lblSaldoTotal"
        Me.lblSaldoTotal.Size = New System.Drawing.Size(37, 13)
        Me.lblSaldoTotal.TabIndex = 6
        Me.lblSaldoTotal.Text = "Saldo:"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(14, 511)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(239, 20)
        Me.flpRegistradoPor.TabIndex = 4
        Me.flpRegistradoPor.Visible = False
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 3)
        Me.lblRegistradoPor.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 3)
        Me.lblUsuarioRegistro.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 3)
        Me.lblFechaRegistro.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(259, 511)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(247, 20)
        Me.flpAnuladoPor.TabIndex = 5
        Me.flpAnuladoPor.Visible = False
        '
        'lblAnulado
        '
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Red
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(89, 20)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(98, 3)
        Me.lblUsuarioAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(150, 3)
        Me.lblFechaAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'TabControl1
        '
        Me.TabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons
        Me.TabControl1.Controls.Add(Me.tpCheque)
        Me.TabControl1.Controls.Add(Me.tpDocumento)
        Me.TabControl1.Location = New System.Drawing.Point(12, 355)
        Me.TabControl1.Multiline = True
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(670, 155)
        Me.TabControl1.TabIndex = 2
        '
        'tpCheque
        '
        Me.tpCheque.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.tpCheque.Controls.Add(Me.lblImporte)
        Me.tpCheque.Controls.Add(Me.txtVencimiento)
        Me.tpCheque.Controls.Add(Me.txtOrden)
        Me.tpCheque.Controls.Add(Me.lblCuentaBancaria)
        Me.tpCheque.Controls.Add(Me.lblOrden)
        Me.tpCheque.Controls.Add(Me.cbxCuentaBancaria)
        Me.tpCheque.Controls.Add(Me.txtFechaPagoCheque)
        Me.tpCheque.Controls.Add(Me.lblFechaCheque)
        Me.tpCheque.Controls.Add(Me.lblFechaPagoCheque)
        Me.tpCheque.Controls.Add(Me.txtFechaCheque)
        Me.tpCheque.Controls.Add(Me.txtMoneda)
        Me.tpCheque.Controls.Add(Me.lblNroCheque)
        Me.tpCheque.Controls.Add(Me.txtBanco)
        Me.tpCheque.Controls.Add(Me.txtNroCheque)
        Me.tpCheque.Controls.Add(Me.txtImporte)
        Me.tpCheque.Controls.Add(Me.txtCotizacion)
        Me.tpCheque.Controls.Add(Me.lblCotizacionCheque)
        Me.tpCheque.Controls.Add(Me.lblImporteMoneda)
        Me.tpCheque.Controls.Add(Me.lblDiferido)
        Me.tpCheque.Controls.Add(Me.txtImporteMoneda)
        Me.tpCheque.Controls.Add(Me.chkDiferido)
        Me.tpCheque.Controls.Add(Me.lblVencimiento)
        Me.tpCheque.Location = New System.Drawing.Point(4, 25)
        Me.tpCheque.Name = "tpCheque"
        Me.tpCheque.Padding = New System.Windows.Forms.Padding(3)
        Me.tpCheque.Size = New System.Drawing.Size(662, 126)
        Me.tpCheque.TabIndex = 0
        Me.tpCheque.Text = "Cheque"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(406, 73)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(74, 13)
        Me.lblImporte.TabIndex = 18
        Me.lblImporte.Text = "Importe Local:"
        '
        'txtVencimiento
        '
        Me.txtVencimiento.AñoFecha = 0
        Me.txtVencimiento.Color = System.Drawing.Color.Empty
        Me.txtVencimiento.Enabled = False
        Me.txtVencimiento.Fecha = New Date(2013, 5, 30, 8, 29, 32, 578)
        Me.txtVencimiento.Location = New System.Drawing.Point(567, 43)
        Me.txtVencimiento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtVencimiento.MesFecha = 0
        Me.txtVencimiento.Name = "txtVencimiento"
        Me.txtVencimiento.PermitirNulo = False
        Me.txtVencimiento.Size = New System.Drawing.Size(87, 20)
        Me.txtVencimiento.SoloLectura = False
        Me.txtVencimiento.TabIndex = 13
        '
        'txtOrden
        '
        Me.txtOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOrden.Color = System.Drawing.Color.Empty
        Me.txtOrden.Indicaciones = Nothing
        Me.txtOrden.Location = New System.Drawing.Point(108, 96)
        Me.txtOrden.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOrden.Multilinea = False
        Me.txtOrden.Name = "txtOrden"
        Me.txtOrden.Size = New System.Drawing.Size(546, 21)
        Me.txtOrden.SoloLectura = False
        Me.txtOrden.TabIndex = 21
        Me.txtOrden.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtOrden.Texto = ""
        '
        'cbxCuentaBancaria
        '
        Me.cbxCuentaBancaria.CampoWhere = Nothing
        Me.cbxCuentaBancaria.CargarUnaSolaVez = False
        Me.cbxCuentaBancaria.DataDisplayMember = Nothing
        Me.cbxCuentaBancaria.DataFilter = Nothing
        Me.cbxCuentaBancaria.DataOrderBy = Nothing
        Me.cbxCuentaBancaria.DataSource = Nothing
        Me.cbxCuentaBancaria.DataValueMember = Nothing
        Me.cbxCuentaBancaria.dtSeleccionado = Nothing
        Me.cbxCuentaBancaria.FormABM = Nothing
        Me.cbxCuentaBancaria.Indicaciones = Nothing
        Me.cbxCuentaBancaria.Location = New System.Drawing.Point(108, 17)
        Me.cbxCuentaBancaria.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCuentaBancaria.Name = "cbxCuentaBancaria"
        Me.cbxCuentaBancaria.SeleccionMultiple = False
        Me.cbxCuentaBancaria.SeleccionObligatoria = True
        Me.cbxCuentaBancaria.Size = New System.Drawing.Size(138, 21)
        Me.cbxCuentaBancaria.SoloLectura = False
        Me.cbxCuentaBancaria.TabIndex = 1
        Me.cbxCuentaBancaria.Texto = ""
        '
        'txtFechaPagoCheque
        '
        Me.txtFechaPagoCheque.AñoFecha = 0
        Me.txtFechaPagoCheque.Color = System.Drawing.Color.Empty
        Me.txtFechaPagoCheque.Fecha = New Date(2013, 5, 30, 8, 29, 32, 578)
        Me.txtFechaPagoCheque.Location = New System.Drawing.Point(268, 43)
        Me.txtFechaPagoCheque.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFechaPagoCheque.MesFecha = 0
        Me.txtFechaPagoCheque.Name = "txtFechaPagoCheque"
        Me.txtFechaPagoCheque.PermitirNulo = False
        Me.txtFechaPagoCheque.Size = New System.Drawing.Size(67, 20)
        Me.txtFechaPagoCheque.SoloLectura = False
        Me.txtFechaPagoCheque.TabIndex = 9
        '
        'txtFechaCheque
        '
        Me.txtFechaCheque.AñoFecha = 0
        Me.txtFechaCheque.Color = System.Drawing.Color.Empty
        Me.txtFechaCheque.Fecha = New Date(2013, 5, 30, 8, 29, 32, 594)
        Me.txtFechaCheque.Location = New System.Drawing.Point(108, 43)
        Me.txtFechaCheque.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFechaCheque.MesFecha = 0
        Me.txtFechaCheque.Name = "txtFechaCheque"
        Me.txtFechaCheque.PermitirNulo = False
        Me.txtFechaCheque.Size = New System.Drawing.Size(67, 20)
        Me.txtFechaCheque.SoloLectura = False
        Me.txtFechaCheque.TabIndex = 7
        '
        'txtMoneda
        '
        Me.txtMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMoneda.Color = System.Drawing.Color.Empty
        Me.txtMoneda.Indicaciones = Nothing
        Me.txtMoneda.Location = New System.Drawing.Point(353, 17)
        Me.txtMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.txtMoneda.Multilinea = False
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.Size = New System.Drawing.Size(62, 21)
        Me.txtMoneda.SoloLectura = True
        Me.txtMoneda.TabIndex = 3
        Me.txtMoneda.TabStop = False
        Me.txtMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMoneda.Texto = ""
        '
        'txtBanco
        '
        Me.txtBanco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBanco.Color = System.Drawing.Color.Empty
        Me.txtBanco.Indicaciones = Nothing
        Me.txtBanco.Location = New System.Drawing.Point(246, 17)
        Me.txtBanco.Margin = New System.Windows.Forms.Padding(4)
        Me.txtBanco.Multilinea = False
        Me.txtBanco.Name = "txtBanco"
        Me.txtBanco.Size = New System.Drawing.Size(107, 21)
        Me.txtBanco.SoloLectura = True
        Me.txtBanco.TabIndex = 2
        Me.txtBanco.TabStop = False
        Me.txtBanco.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBanco.Texto = ""
        '
        'txtNroCheque
        '
        Me.txtNroCheque.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroCheque.Color = System.Drawing.Color.Empty
        Me.txtNroCheque.Indicaciones = Nothing
        Me.txtNroCheque.Location = New System.Drawing.Point(502, 17)
        Me.txtNroCheque.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNroCheque.Multilinea = False
        Me.txtNroCheque.Name = "txtNroCheque"
        Me.txtNroCheque.Size = New System.Drawing.Size(152, 21)
        Me.txtNroCheque.SoloLectura = False
        Me.txtNroCheque.TabIndex = 5
        Me.txtNroCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroCheque.Texto = ""
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = False
        Me.txtImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporte.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporte.Location = New System.Drawing.Point(487, 69)
        Me.txtImporte.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(167, 21)
        Me.txtImporte.SoloLectura = False
        Me.txtImporte.TabIndex = 19
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(108, 69)
        Me.txtCotizacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(67, 21)
        Me.txtCotizacion.SoloLectura = False
        Me.txtCotizacion.TabIndex = 15
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "1"
        '
        'txtImporteMoneda
        '
        Me.txtImporteMoneda.Color = System.Drawing.Color.Empty
        Me.txtImporteMoneda.Decimales = False
        Me.txtImporteMoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteMoneda.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporteMoneda.Location = New System.Drawing.Point(289, 69)
        Me.txtImporteMoneda.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtImporteMoneda.Name = "txtImporteMoneda"
        Me.txtImporteMoneda.Size = New System.Drawing.Size(110, 21)
        Me.txtImporteMoneda.SoloLectura = False
        Me.txtImporteMoneda.TabIndex = 17
        Me.txtImporteMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteMoneda.Texto = "0"
        '
        'tpDocumento
        '
        Me.tpDocumento.Controls.Add(Me.OcxOrdenPagoDocumento1)
        Me.tpDocumento.Location = New System.Drawing.Point(4, 25)
        Me.tpDocumento.Margin = New System.Windows.Forms.Padding(2)
        Me.tpDocumento.Name = "tpDocumento"
        Me.tpDocumento.Padding = New System.Windows.Forms.Padding(2)
        Me.tpDocumento.Size = New System.Drawing.Size(662, 126)
        Me.tpDocumento.TabIndex = 2
        Me.tpDocumento.Text = "Documentos"
        Me.tpDocumento.UseVisualStyleBackColor = True
        '
        'OcxOrdenPagoDocumento1
        '
        Me.OcxOrdenPagoDocumento1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.OcxOrdenPagoDocumento1.Comprobante = Nothing
        Me.OcxOrdenPagoDocumento1.Decimales = 0
        Me.OcxOrdenPagoDocumento1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxOrdenPagoDocumento1.dtDocumento = Nothing
        Me.OcxOrdenPagoDocumento1.Fecha = New Date(CType(0, Long))
        Me.OcxOrdenPagoDocumento1.Form = Nothing
        Me.OcxOrdenPagoDocumento1.IDMoneda = 0
        Me.OcxOrdenPagoDocumento1.Location = New System.Drawing.Point(2, 2)
        Me.OcxOrdenPagoDocumento1.Name = "OcxOrdenPagoDocumento1"
        Me.OcxOrdenPagoDocumento1.Saldo = New Decimal(New Integer() {0, 0, 0, 0})
        Me.OcxOrdenPagoDocumento1.Size = New System.Drawing.Size(658, 122)
        Me.OcxOrdenPagoDocumento1.SolocomprobanteCliente = False
        Me.OcxOrdenPagoDocumento1.SoloComprobanteProveedor = False
        Me.OcxOrdenPagoDocumento1.SoloLectura = False
        Me.OcxOrdenPagoDocumento1.TabIndex = 0
        Me.OcxOrdenPagoDocumento1.Tipo = Nothing
        Me.OcxOrdenPagoDocumento1.Total = New Decimal(New Integer() {0, 0, 0, 0})
        Me.OcxOrdenPagoDocumento1.TotalPago = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(206, 355)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(216, 21)
        Me.Label1.TabIndex = 3
        Me.Label1.Tag = "FORMA DE PAGO"
        Me.Label1.Text = "FORMA DE PAGO"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(212, 536)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(70, 23)
        Me.btnModificar.TabIndex = 17
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'lblDiferenciaCambio
        '
        Me.lblDiferenciaCambio.AutoSize = True
        Me.lblDiferenciaCambio.Location = New System.Drawing.Point(300, 514)
        Me.lblDiferenciaCambio.Name = "lblDiferenciaCambio"
        Me.lblDiferenciaCambio.Size = New System.Drawing.Size(96, 13)
        Me.lblDiferenciaCambio.TabIndex = 18
        Me.lblDiferenciaCambio.Text = "Diferencia Cambio:"
        Me.lblDiferenciaCambio.Visible = False
        '
        'txtDiferenciaCambio
        '
        Me.txtDiferenciaCambio.Color = System.Drawing.Color.Empty
        Me.txtDiferenciaCambio.Decimales = True
        Me.txtDiferenciaCambio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiferenciaCambio.Indicaciones = Nothing
        Me.txtDiferenciaCambio.Location = New System.Drawing.Point(399, 510)
        Me.txtDiferenciaCambio.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDiferenciaCambio.Name = "txtDiferenciaCambio"
        Me.txtDiferenciaCambio.Size = New System.Drawing.Size(113, 22)
        Me.txtDiferenciaCambio.SoloLectura = True
        Me.txtDiferenciaCambio.TabIndex = 19
        Me.txtDiferenciaCambio.TabStop = False
        Me.txtDiferenciaCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDiferenciaCambio.Texto = "0"
        Me.txtDiferenciaCambio.Visible = False
        '
        'txtSaldoTotal
        '
        Me.txtSaldoTotal.Color = System.Drawing.Color.Empty
        Me.txtSaldoTotal.Decimales = True
        Me.txtSaldoTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldoTotal.Indicaciones = Nothing
        Me.txtSaldoTotal.Location = New System.Drawing.Point(558, 510)
        Me.txtSaldoTotal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSaldoTotal.Name = "txtSaldoTotal"
        Me.txtSaldoTotal.Size = New System.Drawing.Size(113, 22)
        Me.txtSaldoTotal.SoloLectura = True
        Me.txtSaldoTotal.TabIndex = 7
        Me.txtSaldoTotal.TabStop = False
        Me.txtSaldoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoTotal.Texto = "0"
        '
        'frmOrdenPago
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(689, 600)
        Me.Controls.Add(Me.txtDiferenciaCambio)
        Me.Controls.Add(Me.lblDiferenciaCambio)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.flpAnuladoPor)
        Me.Controls.Add(Me.txtSaldoTotal)
        Me.Controls.Add(Me.lblSaldoTotal)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Controls.Add(Me.gbxComprobantes)
        Me.KeyPreview = True
        Me.Name = "frmOrdenPago"
        Me.Tag = "frmOrdenPago"
        Me.Text = "frmOrdenPago"
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.gbxComprobantes.ResumeLayout(False)
        Me.gbxComprobantes.PerformLayout()
        Me.TabControl2.ResumeLayout(False)
        Me.Documentos.ResumeLayout(False)
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.Auditoria.ResumeLayout(False)
        CType(Me.dgvAuditoria, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.tpCheque.ResumeLayout(False)
        Me.tpCheque.PerformLayout()
        Me.tpDocumento.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents gbxComprobantes As System.Windows.Forms.GroupBox
    Friend WithEvents txtTotalComprobante As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalComprobante As System.Windows.Forms.Label
    Friend WithEvents lklEliminarEgreso As System.Windows.Forms.LinkLabel
    Friend WithEvents btnAgregarComprobante As System.Windows.Forms.Button
    Friend WithEvents txtCantidadComprobante As ERP.ocxTXTNumeric
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents txtOrden As ERP.ocxTXTString
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents txtFechaPagoCheque As ERP.ocxTXTDate
    Friend WithEvents lblFechaPagoCheque As System.Windows.Forms.Label
    Friend WithEvents txtMoneda As ERP.ocxTXTString
    Friend WithEvents txtBanco As ERP.ocxTXTString
    Friend WithEvents txtImporte As ERP.ocxTXTNumeric
    Friend WithEvents lblCotizacionCheque As System.Windows.Forms.Label
    Friend WithEvents lblDiferido As System.Windows.Forms.Label
    Friend WithEvents chkDiferido As System.Windows.Forms.CheckBox
    Friend WithEvents txtVencimiento As ERP.ocxTXTDate
    Friend WithEvents lblVencimiento As System.Windows.Forms.Label
    Friend WithEvents txtImporteMoneda As ERP.ocxTXTNumeric
    Friend WithEvents lblImporteMoneda As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents txtNroCheque As ERP.ocxTXTString
    Friend WithEvents lblNroCheque As System.Windows.Forms.Label
    Friend WithEvents txtFechaCheque As ERP.ocxTXTDate
    Friend WithEvents lblFechaCheque As System.Windows.Forms.Label
    Friend WithEvents cbxCuentaBancaria As ERP.ocxCBX
    Friend WithEvents lblCuentaBancaria As System.Windows.Forms.Label
    Friend WithEvents txtSaldoTotal As ERP.ocxTXTNumeric
    Friend WithEvents lblSaldoTotal As System.Windows.Forms.Label
    Friend WithEvents btnAplicarComprobantes As System.Windows.Forms.Button
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tpCheque As System.Windows.Forms.TabPage
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents cbxListar As System.Windows.Forms.ComboBox
    Friend WithEvents txtProveedor As ERP.ocxTXTProveedor
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalRetencion As ERP.ocxTXTNumeric
    Friend WithEvents chkAplicarRetencion As ERP.ocxCHK
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents lblTipoPago As System.Windows.Forms.Label
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents LinkLabel2 As System.Windows.Forms.LinkLabel
    Friend WithEvents tsmiEliminarComprobante As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents VisualizarAsientoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VisualizarElComprobanteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtDiferenciaCambio As ERP.ocxTXTNumeric
    Friend WithEvents lblDiferenciaCambio As System.Windows.Forms.Label
    Friend WithEvents tpDocumento As System.Windows.Forms.TabPage
    Friend WithEvents OcxOrdenPagoDocumento1 As ERP.ocxOrdenPagoDocumento
    Friend WithEvents TabControl2 As TabControl
    Friend WithEvents Documentos As TabPage
    Friend WithEvents Auditoria As TabPage
    Friend WithEvents dgvAuditoria As DataGridView
    Friend WithEvents btnEliminarAplicacion As Button
    Friend WithEvents colIDTransaccion As DataGridViewTextBoxColumn
    Friend WithEvents colBanco As DataGridViewTextBoxColumn
    Friend WithEvents colFecha As DataGridViewTextBoxColumn
    Friend WithEvents colProveedor As DataGridViewTextBoxColumn
    Friend WithEvents colMoneda As DataGridViewTextBoxColumn
    Friend WithEvents colCotizacion As DataGridViewTextBoxColumn
    Friend WithEvents colTotal As DataGridViewTextBoxColumn
    Friend WithEvents colSaldo As DataGridViewTextBoxColumn
    Friend WithEvents colImporte As DataGridViewTextBoxColumn
End Class

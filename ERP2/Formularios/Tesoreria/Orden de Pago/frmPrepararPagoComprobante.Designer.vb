﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrepararPagoComprobante
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.lblCuentaBancaria = New System.Windows.Forms.Label()
        Me.dgvCuentaBancaria = New System.Windows.Forms.DataGridView()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.gbxPago = New System.Windows.Forms.GroupBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        CType(Me.dgvCuentaBancaria, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxPago.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = True
        Me.txtImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporte.Indicaciones = Nothing
        Me.txtImporte.Location = New System.Drawing.Point(16, 51)
        Me.txtImporte.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(218, 28)
        Me.txtImporte.SoloLectura = False
        Me.txtImporte.TabIndex = 1
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImporte.Location = New System.Drawing.Point(16, 26)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(127, 20)
        Me.lblImporte.TabIndex = 0
        Me.lblImporte.Text = "Importe a Pagar:"
        '
        'lblCuentaBancaria
        '
        Me.lblCuentaBancaria.AutoSize = True
        Me.lblCuentaBancaria.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCuentaBancaria.Location = New System.Drawing.Point(16, 84)
        Me.lblCuentaBancaria.Name = "lblCuentaBancaria"
        Me.lblCuentaBancaria.Size = New System.Drawing.Size(132, 20)
        Me.lblCuentaBancaria.TabIndex = 2
        Me.lblCuentaBancaria.Text = "Cuenta Bancaria:"
        '
        'dgvCuentaBancaria
        '
        Me.dgvCuentaBancaria.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCuentaBancaria.Location = New System.Drawing.Point(16, 107)
        Me.dgvCuentaBancaria.Name = "dgvCuentaBancaria"
        Me.dgvCuentaBancaria.Size = New System.Drawing.Size(222, 98)
        Me.dgvCuentaBancaria.TabIndex = 3
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(16, 224)
        Me.txtObservacion.Multilinea = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(225, 42)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 5
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(16, 208)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 4
        Me.lblObservacion.Text = "Observacion:"
        '
        'gbxPago
        '
        Me.gbxPago.Controls.Add(Me.lblImporte)
        Me.gbxPago.Controls.Add(Me.txtObservacion)
        Me.gbxPago.Controls.Add(Me.txtImporte)
        Me.gbxPago.Controls.Add(Me.lblObservacion)
        Me.gbxPago.Controls.Add(Me.lblCuentaBancaria)
        Me.gbxPago.Controls.Add(Me.dgvCuentaBancaria)
        Me.gbxPago.Location = New System.Drawing.Point(12, 9)
        Me.gbxPago.Name = "gbxPago"
        Me.gbxPago.Size = New System.Drawing.Size(251, 276)
        Me.gbxPago.TabIndex = 0
        Me.gbxPago.TabStop = False
        Me.gbxPago.Text = "Datos para el Pago"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(188, 291)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 32)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(28, 291)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(151, 32)
        Me.btnGuardar.TabIndex = 1
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'frmPrepararPagoComprobante
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(272, 330)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.gbxPago)
        Me.Name = "frmPrepararPagoComprobante"
        Me.Text = "frmPrepararPagoComprobante"
        CType(Me.dgvCuentaBancaria, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxPago.ResumeLayout(False)
        Me.gbxPago.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtImporte As ERP.ocxTXTNumeric
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents lblCuentaBancaria As System.Windows.Forms.Label
    Friend WithEvents dgvCuentaBancaria As System.Windows.Forms.DataGridView
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents gbxPago As System.Windows.Forms.GroupBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
End Class

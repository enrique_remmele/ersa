﻿Public Class frmOrdenPagoAplicarComprobante

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim SelTodo As Boolean = False
    Dim Decimales As Boolean = False

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property

    'Total OP
    Private ImporteValue As Decimal
    Public Property Importe() As Decimal
        Get
            Return ImporteValue
        End Get
        Set(ByVal value As Decimal)
            ImporteValue = value
        End Set
    End Property

    'Suma de Egresos de OP
    Private TotalopValue As Decimal
    Public Property Totalop() As Decimal
        Get
            Return TotalopValue
        End Get
        Set(ByVal value As Decimal)
            TotalopValue = value
        End Set
    End Property

    Public Property IDMoneda As Integer

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        cbxMoneda.cbx.SelectedValue = IDMoneda
        cbxMoneda.Enabled = False

        'Controles
        Decimales = CSistema.MonedaDecimal(IDMoneda)
        txtSaldo.Decimales = Decimales
        txtTotalComprobante.Decimales = Decimales
        txtTotalPagado.Decimales = Decimales

        'Funciones
        Listar()
        Cargar()

        'Foco
        dgw.Focus()

        'Saldo OP = TotalOP - TotalEgresos
        CalcularTotales()
        CSistema.SqlToComboBox(cbxGrupo.cbx, "Select Distinct ID, Descripcion  From VGrupo Order By 2")
        txtDesde.PrimerDiaSemana()
        txtHasta.Hoy()

    End Sub

    Sub Listar()

        'Limpiar la grilla
        dgw.Rows.Clear()
        Dim TotalDeuda As Decimal = 0
        Dim Filtro As String = ""

        Filtro = " IDMoneda =" & IDMoneda

        If IDProveedor > 0 Then
            dt.DefaultView.RowFilter = " IDProveedor = " & IDProveedor
        Else
            dt.DefaultView.RowFilter = ""
        End If

        'For Each oRow As DataRowView In dt.DefaultView
        For Each oRow As DataRow In dt.Select(Filtro)
            Dim Registro(11) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Seleccionado").ToString
            Registro(2) = oRow("NroComprobante").ToString
            Registro(3) = oRow("Proveedor").ToString
            Registro(4) = oRow("Fec").ToString
            Registro(5) = oRow("FechaVencimiento").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
            Registro(7) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)
            Registro(8) = CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales)
            Registro(9) = oRow("Observacion").ToString
            Registro(10) = oRow("Cancelar").ToString

            'Sumar el total de la deuda
            TotalDeuda = TotalDeuda + CDec(oRow("Saldo").ToString)

            dgw.Rows.Add(Registro)

        Next

        ' txtTotalComprobante.SetValue(TotalDeuda)
        'txtCantidadComprobante.SetValue(dt.Rows.Count)

        'Identificar si la moneda tiene decimales VMoneda
        'Si es decimales N2, si no N0
        'CData.GetRow("ID=cbxMoneda.GetValue(), 'VMoneda')("Decimales")"
        If Decimales = True Then
            dgw.Columns("colTotal").DefaultCellStyle.Format = "N2"
            dgw.Columns("colSaldo").DefaultCellStyle.Format = "N2"
            dgw.Columns("colImporte").DefaultCellStyle.Format = "N2"
        Else
            dgw.Columns("colTotal").DefaultCellStyle.Format = "N0"
            dgw.Columns("colSaldo").DefaultCellStyle.Format = "N0"
            dgw.Columns("colImporte").DefaultCellStyle.Format = "N0"
        End If


    End Sub

    Sub ListarFiltro()

        'Limpiar la grilla
        dgw.Rows.Clear()
        Dim TotalDeuda As Decimal = 0
        Dim Filtro As String = " 0=0 "

        'Grupo
        If chkGrupo.Valor = True Then
            Filtro = Filtro & " And IDGrupo=" & cbxGrupo.GetValue
        End If

        'Fecha
        If chkPeriodo.Valor = True Then

            Dim FiltroFecha As String = " FechaTransaccion "
            If rdbFechaDocumento.Checked Then
                FiltroFecha = " Fecha "
            End If

            Filtro = Filtro & " And " & FiltroFecha & " >= '" & txtDesde.GetValue & "' And " & FiltroFecha & " <= '" & txtHasta.GetValue & "' "
        End If

        'Fondo Fijo 
        If rdbFondoFijo.Checked = True Then
            Filtro = Filtro & " And Tipo = 'FONDO FIJO' "
        End If

        'Gastos
        If rdbGasto.Checked = True Then
            Filtro = Filtro & " And Tipo = 'GASTO' "
        End If

        'Moneda
        If Filtro = "" Then
            Filtro = Filtro & " And IDMoneda =" & cbxMoneda.cbx.SelectedIndex + 1
        End If

        If IDProveedor > 0 Then
            dt.DefaultView.RowFilter = " IDProveedor = " & IDProveedor
        Else
            dt.DefaultView.RowFilter = ""
        End If

        For Each oRow As DataRow In dt.Select(Filtro)
            Dim Registro(11) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Seleccionado").ToString
            Registro(2) = oRow("NroComprobante").ToString
            Registro(3) = oRow("Proveedor").ToString
            Registro(4) = oRow("Fec").ToString
            Registro(5) = oRow("FechaVencimiento").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
            Registro(7) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)
            Registro(8) = CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales)
            Registro(9) = oRow("Observacion").ToString
            Registro(10) = oRow("Cancelar").ToString

            'Sumar el total de la deuda
            TotalDeuda = TotalDeuda + CDec(oRow("Saldo").ToString)

            dgw.Rows.Add(Registro)

        Next

        If Decimales = True Then
            dgw.Columns("colTotal").DefaultCellStyle.Format = "N2"
            dgw.Columns("colSaldo").DefaultCellStyle.Format = "N2"
            dgw.Columns("colImporte").DefaultCellStyle.Format = "N2"
        Else
            dgw.Columns("colTotal").DefaultCellStyle.Format = "N0"
            dgw.Columns("colSaldo").DefaultCellStyle.Format = "N0"
            dgw.Columns("colImporte").DefaultCellStyle.Format = "N0"
        End If

    End Sub

    Sub Guardar()

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento() = False Then
            Exit Sub
        End If

        Seleccionar()

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer = 0

        'Entrada
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", ERP.CSistema.NUMOperacionesABM.UPD.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        If IDTransaccion > 0 Then

            'Cargamos el Detalle de Egresos
            InsertarDetalleEgresos(IDTransaccion)

        End If

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpOrdenPagoAplicarProcesar", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            Exit Sub

        End If

        Me.Close()

    End Sub

    Function ValidarDocumento() As Boolean

        ValidarDocumento = False

        'Saldo
        If txtSaldo.ObtenerValor < 0 Then
            Dim mensaje As String = "La cantidad selecionada no puede ser mayor al saldo de la orden de pago!"
            CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
            Exit Function
        End If

        Return True

    End Function

    Function InsertarDetalleEgresos(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalleEgresos = True

        For Each oRow As DataRow In dt.Select("Seleccionado = 'True' ")

            Dim sql As String = "Insert Into OrdenPagoEgreso(IDTransaccionOrdenPago, IDTransaccionEgreso,Cuota, Importe, Saldo) Values( " & IDTransaccion & "," & oRow("IDTransaccion").ToString & "," & oRow("Cuota").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString, Decimales) & ")"

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

        Next

    End Function

    Sub Seleccionar()

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells(0).Value

            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Seleccionado") = oRow.Cells("colSel").Value
                oRow1("Importe") = CDec(oRow.Cells("colImporte").Value)
                oRow1("Cancelar") = oRow.Cells("colCancelar").Value
            Next

        Next

        Me.Close()

    End Sub

    Sub Cargar()
        For Each oRow As DataRow In dt.Rows
            For Each oRow1 As DataGridViewRow In dgw.Rows
                If oRow("IDTransaccion") = oRow1.Cells(0).Value Then
                    oRow1.Cells(1).Value = CBool(oRow("Seleccionado").ToString)
                    oRow1.Cells(8).Value = CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales)
                    oRow1.Cells(10).Value = CBool(oRow("Cancelar").ToString)
                End If
            Next
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Sub CalcularTotales()
        Dim Total As Decimal = 0
        Dim TotalPagado As Decimal = 0
        Dim CantidadPagado As Integer = 0
        Dim Saldo As Decimal = 0

        Total = Totalop
        txtTotalComprobante.SetValue(Total)

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Cells("colSel").Value = True Then
                TotalPagado = TotalPagado + oRow.Cells("colImporte").Value
                CantidadPagado = CantidadPagado + 1
            End If
        Next
        Saldo = Total - TotalPagado
        txtSaldo.SetValue(Saldo)
        txtTotalPagado.SetValue(TotalPagado)
        txtCantidadPagado.SetValue(CantidadPagado)

        '  txtTotalComprobante.SetValue(CSistema.gridSumColumn(dgw, "colTotal"))
        ' txtCantidadComprobante.SetValue(dgw.RowCount)

    End Sub

    Sub PintarCelda()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(1).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next

        'dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub

    Private Sub frmOrdenPagoAplicarComprobante_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Enter Then

            If Me.ActiveControl.Name = dgw.Name Then
                Exit Sub
            End If

            CSistema.SelectNextControl(Me, e.KeyCode)
        End If
    End Sub

    Private Sub frmCobranzaCreditoSeleccionarVentas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick
        ctrError.Clear()
        tsslEstado.Text = ""

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        Dim Importe As Decimal = 0
        Dim Saldo As Decimal = 0

        If e.ColumnIndex = dgw.Columns.Item("colSel").Index Then

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSel").Value = False Then
                        'Si no hay saldo que ya no permita seleccionar
                        If txtSaldo.ObtenerValor = 0 Then
                            Exit Sub
                        End If
                        oRow.Cells("colSel").Value = True
                        oRow.Cells("colSel").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = False
                        oRow.Cells("colCancelar").ReadOnly = False
                        Importe = CDec(oRow.Cells("colImporte").Value)
                        Saldo = txtSaldo.ObtenerValor


                        If Importe > Saldo Then
                            oRow.Cells("colImporte").Value = txtSaldo.ObtenerValor
                        End If

                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("colImporte")

                    Else
                        oRow.Cells("colSel").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colCancelar").ReadOnly = True
                        oRow.Cells("colImporte").Value = oRow.Cells("colSaldo").Value
                        oRow.Cells("colSel").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

        End If


    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Columns(e.ColumnIndex).Name = "colImporte" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value

            Else
                dgw.CurrentCell.Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells("colImporte").Value, Decimales)
            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown
        If e.KeyCode = Keys.Space Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSel").Value = False Then
                        oRow.Cells("colSel").Value = True
                        oRow.Cells("colSel").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = False
                        oRow.Cells("colCancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("colImporte")

                    Else
                        oRow.Cells("colSel").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colCancelar").ReadOnly = True
                        oRow.Cells("colImporte").Value = oRow.Cells("colSaldo").Value
                        oRow.Cells("colSel").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If

        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSel").Value = False Then
                        oRow.Cells("colSel").Value = True
                        oRow.Cells("colSel").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = False
                        oRow.Cells("colCancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                    Else
                        oRow.Cells("colSel").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colCancelar").ReadOnly = True
                        oRow.Cells("colImporte").Value = oRow.Cells("colSaldo").Value
                        oRow.Cells("colSel").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For


                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colImporte")
            End If

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If


    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Index = e.RowIndex Then
                If oRow.Cells("colSel").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                End If

                oRow.DefaultCellStyle.Font = f1

                oRow.Cells("colImporte").Selected = True

            Else

                oRow.DefaultCellStyle.Font = f2

                If oRow.Cells("colSel").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.White
                Else
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                End If
            End If
        Next
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp

        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells("colImporte")
        End If

    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeleccionarTodoToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = True
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuitarTodaSeleccionToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = False
        Next

        PintarCelda()
        CalcularTotales()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub chhGrupo_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkGrupo.PropertyChanged
        cbxGrupo.Enabled = value
    End Sub

    Private Sub btnListar_Click(sender As System.Object, e As System.EventArgs) Handles btnListar.Click
        ListarFiltro()
    End Sub

    Private Sub chkPeriodo_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkPeriodo.PropertyChanged
        Panel2.Enabled = value
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub

    Private Sub cbxMoneda_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxMoneda.TeclaPrecionada
        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)
        txtSaldo.Decimales = Decimales
        txtTotalComprobante.Decimales = Decimales
        txtTotalPagado.Decimales = Decimales
    End Sub

    Private Sub cbxMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxMoneda.PropertyChanged
        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)
        txtSaldo.Decimales = Decimales
        txtTotalComprobante.Decimales = Decimales
        txtTotalPagado.Decimales = Decimales
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LnkSeleccionar.LinkClicked
        If SelTodo = False Then
            SeleccionarTodo()
        Else
            QuitarTodo()
        End If
    End Sub

    Sub SeleccionarTodo()
        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells("colSel").Value = True
        Next
        SelTodo = True
        lnkSeleccionar.Text = "Quitar Todo"
        PintarCelda()
        CalcularTotales()
    End Sub
    Sub QuitarTodo()
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells("colSel").Value = False
        Next
        SelTodo = False
        lnkSeleccionar.Text = "Seleccionar Todo"
        PintarCelda()
        CalcularTotales()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmOrdenPagoAplicarComprobante_Activate()
        Me.Refresh()
    End Sub
End Class


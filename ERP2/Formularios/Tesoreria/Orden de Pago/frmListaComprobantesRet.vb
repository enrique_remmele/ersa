﻿Public Class frmListaComprobantesRet

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim SelTodo As Boolean = False
    Dim FondoFijo As Boolean = True

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Public Property IDMoneda As Integer
    Public Property Decimales As Boolean

    'VARIABLES
    Dim vCargado As Boolean
    'Private dt As DataTable

    'FUNCIONES
    Sub Inicializar()

        vCargado = False

        'Propiedades
        Decimales = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & IDMoneda, "VMoneda")("Decimales"))

        'Funciones
        Listar()
        Cargar()

        'Foco
        dgw.Focus()

    End Sub

    Sub Listar()

        Dim TotalRetencion As Decimal = 0
        Dim vdt As DataTable

        vCargado = False

        'Filtramos por moneda
        'vdt = CData.FiltrarDataTable(dt, " IDMoneda = " & IDMoneda)

        If IDProveedor > 0 Then
            vdt = CData.FiltrarDataTable(dt, " IDProveedor = " & IDProveedor)
        End If

        dgw.DataSource = Nothing
        vdt.Columns("Seleccionado").SetOrdinal(0)
        vdt.Columns("Observacion").SetOrdinal(vdt.Columns.Count - 1)

        CSistema.dtToGrid(dgw, vdt)

        Dim ColumnasVisibles() As String = {"Seleccionado", "Cancelar", "Tipo y Nro. Comprobante", "Fecha", "Gravado", "IVA", "Total", "Porc. Ret%", "Retencion IVA"}
        Dim ColumnasNumericas() As String = {"Gravado", "IVA", "Total", "Porc. Ret%", "RetencionIVA"}
        Dim CantidadDecimal As Integer = 0

        If Decimales = True Then
            CantidadDecimal = 2
        End If

        'Formato
        For Each c As DataGridViewColumn In dgw.Columns

            'Habilitar solo el campo de seleccion
            If c.Name <> "Seleccionado" Then
                c.ReadOnly = True
            End If

            'Ocultar los que no son visibles
            If ColumnasVisibles.Contains(c.Name) = False Then
                c.Visible = False
            End If

            'Formatos numericos
            If ColumnasNumericas.Contains(c.Name) = True Then
                c.DefaultCellStyle.Format = "N" & CantidadDecimal
                c.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End If

            'AutoSize
            Select Case c.Name
                Case "Seleccionado", "Cancelar", "Tipo y Nro. Comprobante"
                    c.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
                Case "Gravado", "IVA", "Total", "RetencionIVA"
                    c.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                    'Case "Numero"
                    '    c.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                Case Else
                    c.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
            End Select

        Next

        'Habilitar celdas de los que ya estan seleccionados
        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Cells("Seleccionado").Value = True Then
                oRow.Cells("Gravado").ReadOnly = False
                oRow.Cells("IVA").ReadOnly = False
                oRow.Cells("RetencionIVA").ReadOnly = False
            End If
        Next

        'Nombres de Columnas
        dgw.Columns("Seleccionado").HeaderText = "Sel"
        dgw.Columns("Cancelar").HeaderText = "Canc."
        dgw.Columns("Tipo y Nro. Comprobante").HeaderText = "Tipo y Nro. Comprobante"
        dgw.Columns("Fecha").HeaderText = "Fecha"
        dgw.Columns("Gravado").HeaderText = "Gravado"
        dgw.Columns("IVA").HeaderText = "IVA"
        dgw.Columns("Total").HeaderText = "Total"
        dgw.Columns("Porc.Ret%").HeaderText = "Porc.Ret%"
        dgw.Columns("RetencionIVA").HeaderText = "Ret. IVA"

        dgw.SelectionMode = DataGridViewSelectionMode.CellSelect

        TotalRetencion = CSistema.dtSumColumn(vdt, "RetencionIVA")
        txtTotalRetencion.SetValue(TotalRetencion)
        txtCantidadRetencion.SetValue(dt.Rows.Count)

    End Sub

    Sub Seleccionar()

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells("IDTransaccion").Value
            Dim Cuota As Integer = oRow.Cells("Cuota").Value

            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Seleccionado") = oRow.Cells("Seleccionado").Value
                oRow1("Gravado") = oRow.Cells("Gravado").Value
                oRow1("IVA") = oRow.Cells("IVA").Value
                oRow1("Total") = oRow.Cells("Total").Value
                oRow1("Porc.Ret%") = oRow.Cells("Porc.Ret%").Value
                oRow1("RetencionIVA") = oRow.Cells("RetencionIVA").Value

                oRow1("Cancelar") = oRow.Cells("Cancelar").Value

                'If oRow.Cells("RetencionIVA").Value > 0 Then
                '    oRow1("Retener") = True
                'Else
                '    oRow1("Retener") = False
                'End If
            Next

        Next

        Me.Close()

    End Sub

    Sub Cargar()

        PintarCelda()
        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim TotalRetencion As Decimal = 0
        Dim CantidadRetencion As Integer = 0

        If dgw.Columns.Count = 0 Then
            Exit Sub
        End If

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Cells("Seleccionado").Value = True Then
                TotalRetencion = TotalRetencion + oRow.Cells("RetencionIVA").Value
                CantidadRetencion = CantidadRetencion + 1
            End If
        Next

        txtTotalRetencion.SetValue(TotalRetencion)
        txtCantidadRetencion.SetValue(CantidadRetencion)

    End Sub

    Sub PintarCelda()

        'If dgw.Rows.Count = 0 Then
        '    Exit Sub
        'End If

        'For Each Row As DataGridViewRow In dgw.Rows
        '    If Row.Cells(1).Value = True Then
        '        Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
        '    Else
        '        Row.DefaultCellStyle.BackColor = Color.White
        '    End If
        'Next

        'dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub

    Private Sub frmListaComprobanteRet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        vCargado = True
    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick

        ctrError.Clear()
        tsslEstado.Text = ""

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        If e.ColumnIndex = dgw.Columns("Seleccionado").Index Then

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("Seleccionado").Value = False Then
                        oRow.Cells("Seleccionado").Value = True
                        oRow.Cells("Seleccionado").ReadOnly = False
                        oRow.Cells("Total").ReadOnly = False
                        oRow.Cells("RetencionIVA").ReadOnly = False
                        oRow.Cells("IVA").ReadOnly = False
                        oRow.Cells("Cancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("Total")

                    Else
                        oRow.Cells("Seleccionado").ReadOnly = True
                        oRow.Cells("Total").ReadOnly = True
                        oRow.Cells("RetencionIVA").ReadOnly = True
                        oRow.Cells("IVA").ReadOnly = True
                        oRow.Cells("Cancelar").ReadOnly = True
                        oRow.Cells("Total").Value = oRow.Cells("Total").Value
                        oRow.Cells("Seleccionado").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()
        End If

    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Columns(e.ColumnIndex).Name = "Total" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                'dgw.CurrentCell.Value = dgw.CurrentRow.Cells("Saldo").Value

            Else
                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("Total").Value
                dgw.CurrentRow.Cells("RetencionIVA").Value = 0
                dgw.CurrentRow.Cells("IVA").Value = 0
                Dim mensaje As String = "Debe Ingresar un importe para el IVA y la Retencion!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                MessageBox.Show("Debe Ingresar el Importe de Retencion", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            CalcularTotales()

        End If

        'si se modifica la Retencion
        If dgw.Columns(e.ColumnIndex).Name = "RetencionIVA" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe de Retencion IVA debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = 0

            Else
                ctrError.SetError(dgw, "")
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = ""
            End If

            CalcularTotales()

        End If

        'si se modifica el IVA
        If dgw.Columns(e.ColumnIndex).Name = "IVA" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe del IVA debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = 0

            Else
                ctrError.SetError(dgw, "")
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = ""
            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Space Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("Seleccionado").Value = False Then
                        oRow.Cells("Seleccionado").Value = True
                        oRow.Cells("Seleccionado").ReadOnly = False
                        oRow.Cells("Total").ReadOnly = False
                        oRow.Cells("RetencionIVA").ReadOnly = False
                        oRow.Cells("IVA").ReadOnly = False
                        oRow.Cells("Cancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        'dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("Importe")

                    Else
                        oRow.Cells("Seleccionado").ReadOnly = True
                        oRow.Cells("Total").ReadOnly = True
                        oRow.Cells("RetencionIVA").ReadOnly = True
                        oRow.Cells("IVA").ReadOnly = True
                        oRow.Cells("Cancelar").ReadOnly = True
                        'oRow.Cells("Importe").Value = oRow.Cells("Saldo").Value
                        oRow.Cells("Seleccionado").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If

        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("Seleccionado").Value = False Then
                        oRow.Cells("Seleccionado").Value = True
                        oRow.Cells("Seleccionado").ReadOnly = False
                        oRow.Cells("Total").ReadOnly = False
                        oRow.Cells("RetencionIVA").ReadOnly = False
                        oRow.Cells("IVA").ReadOnly = False
                        oRow.Cells("Cancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                    Else
                        oRow.Cells("Seleccionado").ReadOnly = True
                        oRow.Cells("Total").ReadOnly = True
                        oRow.Cells("RetencionIVA").ReadOnly = True
                        oRow.Cells("IVA").ReadOnly = True
                        oRow.Cells("Cancelar").ReadOnly = True
                        'oRow.Cells("Importe").Value = oRow.Cells("Saldo").Value
                        oRow.Cells("Seleccionado").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For


                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("Total")
            End If

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If


    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        If vCargado = False Then
            Exit Sub
        End If

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)

        If dgw.Rows(e.RowIndex).Cells("Seleccionado").Value = False Then
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
        End If

        dgw.Rows(e.RowIndex).DefaultCellStyle.Font = f1

    End Sub

    Private Sub dgw_RowLeave(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowLeave

        If vCargado = False Then
            Exit Sub
        End If

        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        dgw.Rows(e.RowIndex).DefaultCellStyle.Font = f2

        If dgw.Rows(e.RowIndex).Cells("Seleccionado").Value = False Then
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
        Else
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.PaleTurquoise
        End If

    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp

        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            'dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells("Importe")
        End If

    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeleccionarTodoToolStripMenuItem.Click

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuitarTodaSeleccionToolStripMenuItem.Click

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub


End Class
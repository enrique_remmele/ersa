﻿Imports System.Threading
Public Class frmSolicitudEliminarComprobanteOrdenPagoEgreso
    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim PorcentajeAsignado As Decimal = 0
    Dim ImporteAsignado As Decimal = 0

    'PROPIEDADES

    'VARIABLES
    Dim tProcesar As Thread
    Dim vProcesar As Boolean = False

    Enum ENUMOperacionPedido
        APROBAR = 1
        RECHAZAR = 2
        RECUPERAR = 3
        ANULAR = 4
    End Enum


    'FUNCIONES
    Sub Inicializar()

        Me.KeyPreview = True

        'Varios
        CheckForIllegalCrossThreadCalls = False
        CargarInformacion()
        cbxGrupos.SelectedIndex = 0
        cbxOrden.SelectedIndex = 0

        'Funciones
        ListarGrupos()

        txtFechaDesde.txt.Text = Today
        txtFechaHasta.txt.Text = Today
        lvGrupo.Items(0).Selected = True
        ListarPedidos()
    End Sub

    Sub CargarInformacion()

        Dim dtOrden As DataTable = CSistema.ExecuteToDataTable("Select Top(0) * From vSolicitudEliminacionOrdenPagoEgreso")

        For index = 1 To dtOrden.Columns.Count - 1
            cbxOrden.Items.Add(dtOrden.Columns(index).ColumnName)
        Next


    End Sub

    Sub ListarGrupos()

        lvGrupo.Items.Clear()
        lvPedidos.Items.Clear()

        Dim INDEX As Integer = cbxGrupos.SelectedIndex

        Dim dtGrupo As New DataTable("Grupos")

        dtGrupo.Columns.Add("Grupo", Type.GetType("System.String"))

        Dim newRow As DataRow = dtGrupo.NewRow()
        newRow(0) = "PENDIENTE"
        dtGrupo.Rows.Add(newRow)
        Dim newRowx As DataRow = dtGrupo.NewRow()
        newRowx(0) = "APROBADOS"
        dtGrupo.Rows.Add(newRowx)
        Dim newRowz As DataRow = dtGrupo.NewRow()
        newRowz(0) = "RECHAZADOS"
        dtGrupo.Rows.Add(newRowz)

        CSistema.dtToLv(lvGrupo, dtGrupo)

        For i = 0 To lvGrupo.Columns.Count - 1
            lvGrupo.Columns(i).AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize)
        Next

    End Sub

    Sub SeleccionarGrupo()

        ListarPedidos()

    End Sub

    Sub ListarPedidos()

        If lvGrupo.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim OrderBy As String = ""
        Dim Campo As String = ""

        Select Case lvGrupo.SelectedItems.Item(0).Text
            Case "PENDIENTE"
                SQL = "Select * From vSolicitudEliminacionOrdenPagoEgreso Where Aprobado = 'PENDIENTE' and cast(FechaSolicitud as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = True
                btnAprobar.Text = "Aprobar"
                btnRechazar.Text = "Rechazar"
            Case "APROBADOS"
                SQL = "Select * From vSolicitudEliminacionOrdenPagoEgreso Where Aprobado = 'APROBADO' and cast(FechaSolicitud as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = False
                btnRechazar.Enabled = False
                btnAprobar.Text = "Aprobar"
            Case "RECHAZADOS"
                SQL = "Select * From vSolicitudEliminacionOrdenPagoEgreso Where Aprobado = 'RECHAZADO' and cast(FechaSolicitud as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = False
                btnAprobar.Text = "Recuperar"
        End Select

        Listar(SQL & " Order By [" & cbxOrden.Text & "]")

    End Sub

    Sub Listar(ByVal SQL As String)

        CSistema.SqlToLv(lvPedidos, SQL)

        If lvPedidos.Items.Count = 0 Then
            Exit Sub
        End If
        lvPedidos.Columns("IDUsuarioSolicitud").Width = 0
        lvPedidos.Columns("IDUsuarioAprobador").Width = 0
        lvPedidos.Columns("IDTransaccionOrdenPago").Width = 0
        lvPedidos.Columns("IDTransaccionEgreso").Width = 0

        lvPedidos.Columns("Total").TextAlign = HorizontalAlignment.Right
        For Each item As ListViewItem In lvPedidos.Items
            'item.Checked = True
            item.SubItems(7).Text = CSistema.FormatoMoneda(item.SubItems(7).Text)

        Next

        txtCantidadPedidos.txt.Text = lvPedidos.Items.Count

    End Sub


    Sub Detener()
        Try
            If vProcesar = False Then
                Exit Sub
            End If

            tProcesar.Abort()
            vProcesar = False
        Catch ex As Exception

        End Try

        Thread.Sleep(1000)

    End Sub

    Sub AprobarRechazarPedido(ByVal Operacion As ENUMOperacionPedido)

        lvGrupo.Enabled = False
        lvPedidos.Enabled = False

        'Si no esta seleccionado ningun grupo, seleccionamos el primero
        If lvGrupo.Items.Count = 0 Then
            GoTo terminar
        End If

        If lvGrupo.SelectedItems.Count = 0 Then
            'GoTo terminar
        End If

        For Each item As ListViewItem In lvPedidos.Items

            If item.Checked = False Then
                GoTo siguiente
            End If

            item.ImageIndex = 1

            'Dim ID As Integer = item.Text
            Dim IDTransaccionOP As Integer = item.SubItems(1).Text
            Dim IDTransaccionEgreso As Integer = item.SubItems(2).Text
            Dim Cuota As Integer = item.SubItems(5).Text
            'Dim Comprobante As String = item.SubItems(2).Text

            Dim param(-1) As SqlClient.SqlParameter

            'Entrada
            CSistema.SetSQLParameter(param, "@IDTransaccionOP", IDTransaccionOP, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccionEgreso", IDTransaccionEgreso, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cuota", Cuota, ParameterDirection.Input)

            Select Case Operacion

                Case ENUMOperacionPedido.APROBAR
                    CSistema.SetSQLParameter(param, "@Operacion", "APROBAR", ParameterDirection.Input)
                Case ENUMOperacionPedido.RECHAZAR
                    CSistema.SetSQLParameter(param, "@Operacion", "RECHAZAR", ParameterDirection.Input)
                Case ENUMOperacionPedido.RECUPERAR
                    CSistema.SetSQLParameter(param, "@Operacion", "RECUPERAR", ParameterDirection.Input)

            End Select

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpOrdenPagoEliminarAplicacion", False, False, MensajeRetorno,, True) = False Then
                MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

siguiente:

        Next

terminar:
        ListarPedidos()
        lvGrupo.Enabled = True
        lvPedidos.Enabled = True
        vProcesar = False

    End Sub

    Sub VerOP()

        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        For Each item As ListViewItem In lvPedidos.SelectedItems
            Dim frm As New frmOrdenPagoEgresosARendir
            item.ImageIndex = 1
            frm.Show()
            frm.Inicializar()
            frm.EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
            frm.CargarOperacion(item.SubItems(1).Text) ' IDTransaccion
        Next
    End Sub


    Sub SeleccionarTodo()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = True
        Next
    End Sub

    Sub QuitarSeleccion()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = False
        Next
    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        Select Case e.KeyCode
            Case Keys.Enter
                CSistema.SelectNextControl(Me, e.KeyCode)
        End Select

    End Sub

    Private Sub frmAprobarPedido_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Detener()
    End Sub

    Private Sub frmAprobarPedido_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        ManejarTecla(e)
    End Sub

    Private Sub frmAprobarPedido_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnActualizarGrupo_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarGrupo.Click
        ListarGrupos()
    End Sub

    Private Sub cbxGrupos_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxGrupos.SelectedIndexChanged
        ListarGrupos()
    End Sub

    Private Sub lvGrupo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lvGrupo.SelectedIndexChanged
        SeleccionarGrupo()
    End Sub

    Private Sub btnProcesar_Click(sender As System.Object, e As System.EventArgs) Handles btnAprobar.Click
        'Procesar()
        Select Case lvGrupo.SelectedItems.Item(0).Text
            Case "RECHAZADOS"
                AprobarRechazarPedido(ENUMOperacionPedido.RECUPERAR)
            Case "APROBADOS"
                AprobarRechazarPedido(ENUMOperacionPedido.RECUPERAR)
            Case Else
                AprobarRechazarPedido(ENUMOperacionPedido.APROBAR)
        End Select

    End Sub

    Private Sub btnSeleccionarTodos_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionarTodos.Click
        SeleccionarTodo()
    End Sub

    Private Sub ToolStripButton1_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton1.Click
        QuitarSeleccion()
    End Sub

    Private Sub btnActualizarPedidos_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarPedidos.Click
        ListarPedidos()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnVerPedido_Click(sender As System.Object, e As System.EventArgs) Handles btnVerOP.Click
        VerOP()
    End Sub

    Private Sub ToolStripButton2_Click(sender As System.Object, e As System.EventArgs) Handles btnRechazar.Click

        AprobarRechazarPedido(ENUMOperacionPedido.RECHAZAR)

    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmSolicitudEliminarComprobanteOrdenPagoEgreso_Activate()
        Me.Refresh()
    End Sub
End Class
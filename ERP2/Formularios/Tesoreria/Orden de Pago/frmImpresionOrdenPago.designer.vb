﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImpresionOrdenPago
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblOP = New System.Windows.Forms.Label()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnSeleccionar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chkObservacion = New ERP.ocxCHK()
        Me.cbxImpresora = New ERP.ocxCBX()
        Me.chkIncluirImpresos = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtBanco = New ERP.ocxTXTString()
        Me.cbxCuenta = New ERP.ocxCBX()
        Me.OcxCHK1 = New ERP.ocxCHK()
        Me.txtOPFinal = New ERP.ocxTXTNumeric()
        Me.txtOPInicial = New ERP.ocxTXTNumeric()
        Me.chkSeccionFacturasSeparadas = New System.Windows.Forms.CheckBox()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblOP
        '
        Me.lblOP.AutoSize = True
        Me.lblOP.Location = New System.Drawing.Point(12, 41)
        Me.lblOP.Name = "lblOP"
        Me.lblOP.Size = New System.Drawing.Size(78, 13)
        Me.lblOP.TabIndex = 2
        Me.lblOP.Text = "OP inicial/final:"
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(27, 76)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(41, 13)
        Me.lblCuenta.TabIndex = 5
        Me.lblCuenta.Text = "Cuenta"
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(174, 220)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 13
        Me.btnImprimir.Text = "&Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnSeleccionar
        '
        Me.btnSeleccionar.Location = New System.Drawing.Point(93, 220)
        Me.btnSeleccionar.Name = "btnSeleccionar"
        Me.btnSeleccionar.Size = New System.Drawing.Size(75, 23)
        Me.btnSeleccionar.TabIndex = 12
        Me.btnSeleccionar.Text = "S&eleccionar"
        Me.btnSeleccionar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(255, 220)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 14
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(39, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Text = "Sucursal:"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 249)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(353, 22)
        Me.StatusStrip1.TabIndex = 15
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(28, 186)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Impresora:"
        '
        'chkObservacion
        '
        Me.chkObservacion.BackColor = System.Drawing.Color.Transparent
        Me.chkObservacion.Color = System.Drawing.Color.Empty
        Me.chkObservacion.Location = New System.Drawing.Point(93, 124)
        Me.chkObservacion.Name = "chkObservacion"
        Me.chkObservacion.Size = New System.Drawing.Size(214, 19)
        Me.chkObservacion.SoloLectura = False
        Me.chkObservacion.TabIndex = 16
        Me.chkObservacion.Texto = "Imprimir con Observación"
        Me.chkObservacion.Valor = False
        '
        'cbxImpresora
        '
        Me.cbxImpresora.CampoWhere = Nothing
        Me.cbxImpresora.CargarUnaSolaVez = False
        Me.cbxImpresora.DataDisplayMember = Nothing
        Me.cbxImpresora.DataFilter = Nothing
        Me.cbxImpresora.DataOrderBy = Nothing
        Me.cbxImpresora.DataSource = Nothing
        Me.cbxImpresora.DataValueMember = Nothing
        Me.cbxImpresora.dtSeleccionado = Nothing
        Me.cbxImpresora.FormABM = Nothing
        Me.cbxImpresora.Indicaciones = Nothing
        Me.cbxImpresora.Location = New System.Drawing.Point(93, 182)
        Me.cbxImpresora.Name = "cbxImpresora"
        Me.cbxImpresora.SeleccionMultiple = False
        Me.cbxImpresora.SeleccionObligatoria = True
        Me.cbxImpresora.Size = New System.Drawing.Size(237, 21)
        Me.cbxImpresora.SoloLectura = False
        Me.cbxImpresora.TabIndex = 11
        Me.cbxImpresora.Texto = ""
        '
        'chkIncluirImpresos
        '
        Me.chkIncluirImpresos.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirImpresos.Color = System.Drawing.Color.Empty
        Me.chkIncluirImpresos.Location = New System.Drawing.Point(93, 99)
        Me.chkIncluirImpresos.Name = "chkIncluirImpresos"
        Me.chkIncluirImpresos.Size = New System.Drawing.Size(214, 19)
        Me.chkIncluirImpresos.SoloLectura = False
        Me.chkIncluirImpresos.TabIndex = 9
        Me.chkIncluirImpresos.Texto = "Incluir OP´s que ya se imprimieron"
        Me.chkIncluirImpresos.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(93, 12)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(110, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'txtBanco
        '
        Me.txtBanco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBanco.Color = System.Drawing.Color.Empty
        Me.txtBanco.Indicaciones = Nothing
        Me.txtBanco.Location = New System.Drawing.Point(235, 72)
        Me.txtBanco.Multilinea = False
        Me.txtBanco.Name = "txtBanco"
        Me.txtBanco.Size = New System.Drawing.Size(95, 21)
        Me.txtBanco.SoloLectura = True
        Me.txtBanco.TabIndex = 8
        Me.txtBanco.TabStop = False
        Me.txtBanco.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBanco.Texto = ""
        '
        'cbxCuenta
        '
        Me.cbxCuenta.CampoWhere = Nothing
        Me.cbxCuenta.CargarUnaSolaVez = False
        Me.cbxCuenta.DataDisplayMember = Nothing
        Me.cbxCuenta.DataFilter = Nothing
        Me.cbxCuenta.DataOrderBy = Nothing
        Me.cbxCuenta.DataSource = Nothing
        Me.cbxCuenta.DataValueMember = Nothing
        Me.cbxCuenta.dtSeleccionado = Nothing
        Me.cbxCuenta.FormABM = Nothing
        Me.cbxCuenta.Indicaciones = Nothing
        Me.cbxCuenta.Location = New System.Drawing.Point(93, 72)
        Me.cbxCuenta.Name = "cbxCuenta"
        Me.cbxCuenta.SeleccionMultiple = False
        Me.cbxCuenta.SeleccionObligatoria = False
        Me.cbxCuenta.Size = New System.Drawing.Size(136, 21)
        Me.cbxCuenta.SoloLectura = False
        Me.cbxCuenta.TabIndex = 7
        Me.cbxCuenta.Texto = ""
        '
        'OcxCHK1
        '
        Me.OcxCHK1.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK1.Color = System.Drawing.Color.Empty
        Me.OcxCHK1.Location = New System.Drawing.Point(74, 77)
        Me.OcxCHK1.Name = "OcxCHK1"
        Me.OcxCHK1.Size = New System.Drawing.Size(13, 14)
        Me.OcxCHK1.SoloLectura = False
        Me.OcxCHK1.TabIndex = 6
        Me.OcxCHK1.Texto = Nothing
        Me.OcxCHK1.Valor = False
        '
        'txtOPFinal
        '
        Me.txtOPFinal.Color = System.Drawing.Color.Empty
        Me.txtOPFinal.Decimales = True
        Me.txtOPFinal.Indicaciones = Nothing
        Me.txtOPFinal.Location = New System.Drawing.Point(217, 39)
        Me.txtOPFinal.Name = "txtOPFinal"
        Me.txtOPFinal.Size = New System.Drawing.Size(113, 22)
        Me.txtOPFinal.SoloLectura = False
        Me.txtOPFinal.TabIndex = 4
        Me.txtOPFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOPFinal.Texto = "0"
        '
        'txtOPInicial
        '
        Me.txtOPInicial.Color = System.Drawing.Color.Empty
        Me.txtOPInicial.Decimales = True
        Me.txtOPInicial.Indicaciones = Nothing
        Me.txtOPInicial.Location = New System.Drawing.Point(93, 39)
        Me.txtOPInicial.Name = "txtOPInicial"
        Me.txtOPInicial.Size = New System.Drawing.Size(110, 22)
        Me.txtOPInicial.SoloLectura = False
        Me.txtOPInicial.TabIndex = 3
        Me.txtOPInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOPInicial.Texto = "0"
        '
        'chkSeccionFacturasSeparadas
        '
        Me.chkSeccionFacturasSeparadas.AutoSize = True
        Me.chkSeccionFacturasSeparadas.Location = New System.Drawing.Point(93, 149)
        Me.chkSeccionFacturasSeparadas.Name = "chkSeccionFacturasSeparadas"
        Me.chkSeccionFacturasSeparadas.Size = New System.Drawing.Size(173, 17)
        Me.chkSeccionFacturasSeparadas.TabIndex = 17
        Me.chkSeccionFacturasSeparadas.Text = "Seccion de Facturas Separada"
        Me.chkSeccionFacturasSeparadas.UseVisualStyleBackColor = True
        '
        'frmImpresionOrdenPago
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(353, 271)
        Me.Controls.Add(Me.chkSeccionFacturasSeparadas)
        Me.Controls.Add(Me.chkObservacion)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbxImpresora)
        Me.Controls.Add(Me.chkIncluirImpresos)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.cbxSucursal)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.txtBanco)
        Me.Controls.Add(Me.cbxCuenta)
        Me.Controls.Add(Me.OcxCHK1)
        Me.Controls.Add(Me.txtOPFinal)
        Me.Controls.Add(Me.txtOPInicial)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnSeleccionar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.lblCuenta)
        Me.Controls.Add(Me.lblOP)
        Me.Name = "frmImpresionOrdenPago"
        Me.Text = "Impresión de órdenes de pago"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblOP As System.Windows.Forms.Label
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnSeleccionar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents txtOPFinal As ERP.ocxTXTNumeric
    Friend WithEvents txtOPInicial As ERP.ocxTXTNumeric
    Friend WithEvents OcxCHK1 As ERP.ocxCHK
    Friend WithEvents txtBanco As ERP.ocxTXTString
    Friend WithEvents cbxCuenta As ERP.ocxCBX
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents chkIncluirImpresos As ERP.ocxCHK
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxImpresora As ERP.ocxCBX
    Friend WithEvents chkObservacion As ocxCHK
    Friend WithEvents chkSeccionFacturasSeparadas As CheckBox
End Class

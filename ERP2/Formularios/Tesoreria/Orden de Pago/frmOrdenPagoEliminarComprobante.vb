﻿Public Class frmOrdenPagoEliminarComprobante

    Dim CSistema As New CSistema
    Public Property IDTransaccionOrdenPago As Integer
    Public Property Procesado As Boolean

    Sub Listar()

        Dim dtDetalle As DataTable = CSistema.ExecuteToDataTable("select 'Sel' = Cast(0 as bit), IDTransaccion, NroComprobante,Cuota,Proveedor,Fecha, Total, Importe,Moneda from vOrdenPagoEgreso where IDTransaccionOrdenPago = " & IDTransaccionOrdenPago)
        Dim dtMotivoEliminacion As DataTable = CSistema.ExecuteToDataTable("Select ID, Descripcion from MotivoEliminarComprobanteAplicado")
        CSistema.dtToGrid(dgv, dtDetalle)

        Dim comboboxColumn As New DataGridViewComboBoxColumn
        comboboxColumn.Name = "Motivo Eliminacion"
        comboboxColumn.DataSource = dtMotivoEliminacion
        comboboxColumn.DisplayMember = "Descripcion"
        comboboxColumn.ValueMember = "ID"
        dgv.Columns.Add(comboboxColumn)

        CSistema.DataGridColumnasVisibles(dgv, {"Sel", "NroComprobante", "Cuota", "Proveedor", "Fecha", "Total", "Importe", "Moneda", "Motivo Eliminacion"})
        CSistema.DataGridColumnasNumericas(dgv, {"Total", "Importe"}, True)
        dgv.Columns("Proveedor").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("Motivo Eliminacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Update()


    End Sub

    Sub Solicitar()

        For i = 0 To dgv.Rows.Count - 1
            If CBool(dgv.Rows(i).Cells("Sel").Value) Then

                Dim param(-1) As SqlClient.SqlParameter

                'Entrada
                CSistema.SetSQLParameter(param, "@IDTransaccionOP", IDTransaccionOrdenPago, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDTransaccionEgreso", dgv.Rows(i).Cells("IDTransaccion").Value, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Cuota", dgv.Rows(i).Cells("Cuota").Value, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Operacion", "SOLICITAR", ParameterDirection.Input)

                'Informacion de Salida
                CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
                CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

                Dim MensajeRetorno As String = ""

                'Insertar Registro
                If CSistema.ExecuteStoreProcedure(param, "SpOrdenPagoEliminarAplicacion", False, False, MensajeRetorno,, True) = False Then
                    MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        Next

    End Sub

    Private Sub frmOrdenPagoEliminarComprobante_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Listar()
    End Sub

    Private Sub btnEnviarSolicitud_Click(sender As Object, e As EventArgs) Handles btnEnviarSolicitud.Click
        Solicitar()
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Procesado = False
        Me.Close()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmOrdenPagoEliminarComprobante_Activate()
        Me.Refresh()
    End Sub
End Class
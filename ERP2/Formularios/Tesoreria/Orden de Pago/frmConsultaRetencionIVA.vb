﻿Public Class frmConsultaRetencionIVA

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim dtRetencion As DataTable
    Dim dtComprobante As DataTable

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim IDOperacion As Integer
    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'TextBox
        txtCantidadOrdenPago.txt.ResetText()
        txtCantidadComprobantes.txt.ResetText()
        txtOperacion.txt.ResetText()
        txtTotalOrdenPago.txt.ResetText()

        'CheckBox
        chkTipoComprobante.Checked = False
        chkProveedor.Checked = False
        chkFecha.Checked = False

        'ComboBox
        cbxTipoComprobante.Enabled = False
        cbxProveedor.Enabled = False

        'DataGridView
        dgwOperacion.Rows.Clear()
        dgwComprobante.Rows.Clear()

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion(frmAplicacionProveedoresAnticipo.Name, "ORDEN PAGO", "OP")
        CargarInformacion()

        'Foco

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Cliente
        CSistema.SqlToComboBox(cbxProveedor, "Select Distinct IDProveedor, Proveedor From VOrdenPago Order By 2")

        'TipoComprobante
        CSistema.SqlToComboBox(cbxTipoComprobante, "Select ID, Descripcion From VTipoComprobante Where IDOperacion=" & IDOperacion & " Order By 2")


        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Cliente
        chkProveedor.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "PROVEEDOR ACTIVO", "False")
        cbxProveedor.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "PROVEEDOR", "")

        'Tipo de Comprobante
        chkTipoComprobante.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO", "False")
        cbxTipoComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", "")

        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

    End Sub

    'Guardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Cliente
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "PROVEEDOR ACTIVO", chkProveedor.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "PROVEEDOR", cbxProveedor.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO", chkTipoComprobante.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", cbxTipoComprobante.Text)


    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Cobranza
    Sub ListarRetencionIVA(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "")

        ctrError.Clear()


        Where = Condicion

        'Proveedor
        If EstablecerCondicion(cbxProveedor, chkProveedor, "IDProveedor", "Seleccione correctamente el !") = False Then
            Exit Sub
        End If

        'Comprobante
        If EstablecerCondicion(cbxTipoComprobante, chkTipoComprobante, "IDTipoComprobante", "Seleccione correctamente el tipo de comprobante!") = False Then
            Exit Sub
        End If

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la sucursal!") = False Then
            Exit Sub
        End If

        'Fecha
        If chkFecha.Checked = True Then
            If Where = "" Then
                Where = " Where (Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' ) "
            Else
                Where = Where & " And (Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' )  "
            End If
        End If

        dgwOperacion.Rows.Clear()
        dgwComprobante.Rows.Clear()

        'Solo por numero
        If Numero > 0 Then
            Where = " Where NroComprobante = " & Numero
        End If

        Dim sql As String = "Select IDTransaccion,Comprobante,Proveedor,Fecha,Sucursal,EstadoAnulado,Total,TotalIVA,TotalRenta From VRetencionIVA " & Where

        dtRetencion = CSistema.ExecuteToDataTable(sql)

        Dim TotalRetencion As Decimal = 0
        Dim CantidadRetencion As Decimal = 0

        For Each oRow As DataRow In dtRetencion.Rows
            Dim Registro(8) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Comprobante").ToString
            Registro(2) = oRow("Proveedor").ToString
            Registro(3) = oRow("Fecha").ToString
            Registro(4) = oRow("Sucursal").ToString
            Registro(5) = oRow("EstadoAnulado").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(7) = CSistema.FormatoMoneda(oRow("TotalIVA").ToString)
            Registro(8) = CSistema.FormatoMoneda(oRow("TotalRenta").ToString)

            'Sumar el total del saldo
            TotalRetencion = TotalRetencion + CDec(oRow("TotalIVA").ToString)
            CantidadRetencion = CantidadRetencion + 1

            dgwOperacion.Rows.Add(Registro)

        Next

        'Totales
        txtTotalOrdenPago.SetValue(TotalRetencion)
        txtCantidadOrdenPago.SetValue(CantidadRetencion)


    End Sub

    'Listar Comprobantes
    Sub ListarComprobantes(ByVal vIDTransaccion As Integer)

        'Limpiar ListView
        dgwComprobante.Rows.Clear()

        Dim TotalComprobante As Decimal = 0
        Dim CantidadComprobante As Decimal = 0

        Dim sql As String = "Select TipoyComprobante,Total,TotalGravado,TotalIVA,PorcRetencion,RetencionIVA,'Por.Renta %'='','Renta'='' From VComprobanteRetencion Where IDTransaccion=" & vIDTransaccion

        dtComprobante = CSistema.ExecuteToDataTable(sql)

        For Each oRow As DataRow In dtComprobante.Rows
            Dim Registro(7) As String
            Registro(0) = oRow("TipoyComprobante").ToString
            Registro(1) = CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(2) = CSistema.FormatoMoneda(oRow("TotalGravado").ToString)
            Registro(3) = CSistema.FormatoMoneda(oRow("TotalIVA").ToString)
            Registro(4) = oRow("PorcRetencion").ToString
            Registro(5) = CSistema.FormatoMoneda(oRow("RetencionIVA").ToString)
            Registro(6) = oRow("Por.Renta %").ToString
            Registro(7) = oRow("Renta").ToString
          

            'Sumar el total del saldo
            TotalComprobante = TotalComprobante + CDec(oRow("Total").ToString)
            CantidadComprobante = CantidadComprobante + 1

            dgwComprobante.Rows.Add(Registro)

        Next

        'Totales
        txtTotalComprobantes.SetValue(TotalComprobante)
        txtCantidadComprobantes.SetValue(CantidadComprobante)


    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = dgwOperacion.SelectedRows(0).Cells("colIDTransaccion").Value

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub frmConsultaChequeCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkCliente_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkProveedor.CheckedChanged
        HabilitarControles(chkProveedor, cbxProveedor)
    End Sub

    Private Sub chkBanco_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipoComprobante.CheckedChanged
        HabilitarControles(chkTipoComprobante, cbxTipoComprobante)
    End Sub


    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged
        HabilitarControles(chkFecha, dtpDesde)
        HabilitarControles(chkFecha, dtpHasta)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        ListarRetencionIVA()
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        SeleccionarRegistro()
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            txtTotalOrdenPago.txt.Clear()
            txtTotalComprobantes.txt.Clear()
            ListarRetencionIVA(txtOperacion.ObtenerValor)
            txtOperacion.txt.Focus()
            txtOperacion.txt.SelectAll()
        End If
    End Sub

    Private Sub dtpDesde_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpDesde.ValueChanged
        dtpHasta.Value = dtpDesde.Text
    End Sub

    Private Sub dgwOperacion_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgwOperacion.CellClick
        ctrError.Clear()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        Dim vIDTransaccion As Integer

        vIDTransaccion = dgwOperacion.SelectedRows(0).Cells("colIDTransaccion").Value

        ListarComprobantes(vIDTransaccion)
    End Sub

    Private Sub dgwOperacion_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgwOperacion.CellDoubleClick
        SeleccionarRegistro()
    End Sub
End Class
﻿Imports System.Threading

Public Class frmPagoEgreso

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Public Property dtEgreso As DataTable

    'VARIABLES
    Dim tProcesar As Thread
    Dim vProcesar As Boolean = False

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button
        Me.Size = New Size(Me.ParentForm.Size.Width - 50, Me.Size.Height)
        Me.Location = New Point(20, Me.Location.Y)

        'Varios
        CheckForIllegalCrossThreadCalls = False

        cbxGrupos.SelectedIndex = 0

        'Funciones
        ListarGrupos()

    End Sub

    Sub CargarInformacion()

    End Sub

    Sub GuardarInformacion()

    End Sub

    Sub CargarEgresos()

        dtEgreso = CSistema.ExecuteToDataTable("Exec SpViewEgresosParaOrdenPAgo @IDSucursal=" & vgIDSucursal & ", @SoloAPagar='True' ")

        'Excluir los de fondo fijo
        dtEgreso = CData.FiltrarDataTable(dtEgreso, "FondoFijo='False' ")

    End Sub

    Sub ListarGrupos()

        lvGrupo.Items.Clear()
        lvEgresos.Items.Clear()

        CargarEgresos()

        Dim INDEX As Integer = cbxGrupos.SelectedIndex
        Dim vdtGrupos As New DataTable

        Select Case INDEX
            Case 0 'TODOS
                vdtGrupos = AgruparDataTable(dtEgreso, "Todos", "ID", False)
            Case 1 'CUENTA BANCARIA
                vdtGrupos = AgruparDataTable(dtEgreso, "CuentaBancaria", "IDCuentaBancaria", False)
            Case 2 'PROVEEDOR
                vdtGrupos = AgruparDataTable(dtEgreso, "Proveedor", "IDProveedor", False)
            Case 3 'TIPO
                vdtGrupos = AgruparDataTable(dtEgreso, "Tipo", "Tipo", False)
            Case 4 'TIPO PROVEEDOR
                vdtGrupos = AgruparDataTable(dtEgreso, "TipoProveedor", "TipoProveedor", False)
            Case Else 'NINGUNO

        End Select

        CSistema.dtToLv(lvGrupo, vdtGrupos)

        'Imagen
        For Each item As ListViewItem In lvGrupo.Items
            item.ImageIndex = 5
        Next

        'Ocultar ID
        lvGrupo.Columns("ID").Width = 0

        'Ancho de columna
        lvGrupo.Columns("Descripcion").Width = 184
        lvGrupo.Columns("Cantidad").Width = 42
        lvGrupo.Columns("Cantidad").Text = "Cant."

    End Sub

    Function AgruparDataTable(vdt As DataTable, Columna As String, IDColumna As String, Ordenar As Boolean) As DataTable

        Dim vdtGrupos As New DataTable

        vdtGrupos.Columns.Add("Descripcion")
        vdtGrupos.Columns.Add("Cantidad")
        vdtGrupos.Columns.Add("ID")

        Dim NewRow As DataRow = vdtGrupos.NewRow

        'Si es todos, cargamos solo un registro y salimos
        If Columna = "Todos" Then
            NewRow("ID") = "1"
            NewRow("Descripcion") = "LISTAR TODOS"
            NewRow("Cantidad") = dtEgreso.Rows.Count
            vdtGrupos.Rows.Add(NewRow)
            GoTo Salir
        End If

        For Each oRow As DataRow In dtEgreso.Rows

            'Buscamos
            If vdtGrupos.Select(" Descripcion = '" & oRow(Columna).ToString & "' ").Count = 0 Then
                Dim NewRow1 As DataRow = vdtGrupos.NewRow
                NewRow1("ID") = oRow(IDColumna).ToString
                NewRow1("Descripcion") = oRow(Columna).ToString
                NewRow1("Cantidad") = 1
                vdtGrupos.Rows.Add(NewRow1)
            Else
                Dim UpdRow As DataRow = vdtGrupos.Select(" Descripcion = '" & oRow(Columna).ToString & "' ")(0)
                UpdRow("Cantidad") = UpdRow("Cantidad") + 1
            End If
        Next

Salir:

        Return vdtGrupos

    End Function

    Sub ListarEgresos()

        If lvGrupo.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim ID As String = lvGrupo.SelectedItems(0).SubItems("ID").Text
        Dim OrderBy As String = ""

        Dim INDEX As Integer = cbxGrupos.SelectedIndex
        Dim vEgresos As New DataTable

        Select Case INDEX
            Case 0 'TODOS
                vEgresos = CData.FiltrarDataTable(dtEgreso, "Pagar='True' ")
            Case 1 'CUENTA BANCARIA
                vEgresos = CData.FiltrarDataTable(dtEgreso, " IDCuentaBancaria='" & ID & "' ")
            Case 2 'PROVEEDOR
                vEgresos = CData.FiltrarDataTable(dtEgreso, " IDProveedor='" & ID & "' ")
            Case 3 'TIPO COMPROBANTE
                vEgresos = CData.FiltrarDataTable(dtEgreso, " Tipo='" & ID & "' ")
            Case 4 'TIPO PROVEEDOR
                vEgresos = CData.FiltrarDataTable(dtEgreso, " TipoProveedor='" & ID & "' ")
            Case Else 'NINGUNO

        End Select


        'Ordenado
        CData.OrderDataTable(vEgresos, "Proveedor, CuentaBancaria, IDMoneda")

        Listar(vEgresos)

    End Sub

    Sub Listar(vdt As DataTable)

        'Ocultamos la lista hasta que se termine
        lvEgresos.Visible = False

        'Listamos
        CSistema.dtToLv(lvEgresos, vdt)

        'Seleccionar todos
        For Each item As ListViewItem In lvEgresos.Items
            item.Checked = True

            'Verificar la moneda
            Dim vDecimales As Boolean = CSistema.RetornarValorBoolean(CData.GetRow(" ID=" & item.SubItems("IDMoneda").Text, "VMoneda")("Decimales").ToString)

            item.SubItems("Total").Text = CSistema.FormatoMoneda(item.SubItems("Total").Text, vDecimales)
            item.SubItems("ImporteAPagar").Text = CSistema.FormatoMoneda(item.SubItems("ImporteAPagar").Text, vDecimales)
            item.SubItems("Cotiz.").Text = CSistema.FormatoMoneda(item.SubItems("Cotiz.").Text, False)

            item.ImageIndex = 3

        Next

        'Columnas Centralizadas
        Dim ColumnasCentralizadas() As String = {"Fec", "Fec. Venc.", "Moneda", "Pago"}
        For Each c As String In ColumnasCentralizadas
            lvEgresos.Columns(c).TextAlign = HorizontalAlignment.Center
        Next

        'Columnas Numericas
        Dim ColumnasNumericas() As String = {"Total", "ImporteAPagar", "Cotiz."}
        For Each c As String In ColumnasNumericas
            lvEgresos.Columns(c).TextAlign = HorizontalAlignment.Right
        Next

        'Ocultar columnas
        Dim ColumnasOcultar() As String = {"IDTransaccion", "Numero", "TipoComprobante", "Comprobante", "NumeroTC", "IDProveedor", "Retentor", "TipoProveedor", "IDCuentaContable", "CodigoCuenta", "CuentaContable", "Alias", "Fecha", "FechaVencimiento", "Saldo", "Importe", "IVA", "RetencionIVA", "RetencionRenta", "Retener", "TotalImpuesto", "TotalDiscriminado", "IDMoneda", "Observacion", "Credito", "Seleccionado", "Cancelar", "Efectivo", "Cheque", "FondoFijo", "Pagar", "IDCuentaBancaria", "CantidadCuota", "Cuota", "Cotizacion"}
        For Each c As String In ColumnasOcultar
            lvEgresos.Columns(c).Width = 0
        Next

        'Nombres de Columnas
        lvEgresos.Columns("NroComprobante").Text = "Comprobante"
        lvEgresos.Columns("ImporteAPagar").Text = "Importe"
        lvEgresos.Columns("ObservacionPago").Text = "Observacion"

        'tamaños fijos
        lvEgresos.Columns("Proveedor").Width = 180
        lvEgresos.Columns("ImporteAPagar").Width = 83
        lvEgresos.Columns("Total").Width = 83

        lvEgresos.Visible = True

        txtCantidadEgresos.txt.Text = lvEgresos.Items.Count

    End Sub

    Sub Procesar()

        If lvEgresos.CheckedItems.Count = 0 Then
            Exit Sub
        End If

        If vProcesar = True Then
            Exit Sub
        End If

        vProcesar = True
        tProcesar = New Thread(AddressOf PagarEgreso)
        tProcesar.Start()

    End Sub

    Sub Detener()

        Try
            If vProcesar = False Then
                Exit Sub
            End If

            vProcesar = False
            tProcesar.Abort()

        Catch ex As Exception

        End Try

        lvGrupo.Enabled = True
        lvEgresos.Enabled = True

        Thread.Sleep(1000)

    End Sub

    Sub PagarEgreso()

        lvGrupo.Enabled = False
        lvEgresos.Enabled = False

        'Si no esta seleccionado ningun grupo, seleccionamos el primero
        If lvGrupo.Items.Count = 0 Then
            GoTo terminar
        End If

        If lvGrupo.SelectedItems.Count = 0 Then
            GoTo terminar
        End If

        For Each item As ListViewItem In lvEgresos.CheckedItems

            If vProcesar = False Then
                GoTo terminar
            End If

            If item.Checked = False Then
                GoTo siguiente
            End If

            If item.SubItems("EstadoProceso").Text.ToUpper <> "PENDIENTE" Then
                GoTo siguiente
            End If

            item.ImageIndex = 1

            'Verificamos si existen mas de un comprobante del mismo proveedor, cuenta y moneda de comprobante
            If dtEgreso.Select("IDProveedor=" & item.SubItems("IDProveedor").Text & " And IDCuentaContable=" & item.SubItems("IDCuentaContable").Text & " " & "And IDMoneda=" & item.SubItems("IDMoneda").Text & " ").Count > 1 Then
                CargarEgresosAgrupados(item)
                GoTo Siguiente
            Else
                Dim vdtTemp As DataTable = dtEgreso.Clone

                'Copiar la linea a la tabla temporal
                vdtTemp.Rows.Add(dtEgreso.Select(" IDTransaccion=" & item.SubItems("IDTransaccion").Text)(0).ItemArray)
                Dim vCotizacionMoneda As Integer = CType(CSistema.ExecuteScalar("Select IsNull(Max(cotizacion), 1) From Cotizacion Where cast(fecha as date) = cast(getdate() as date) and IDMoneda=" & item.SubItems("IDMoneda").Text), Integer)
                If vCotizacionMoneda = 1 And item.SubItems("IDMoneda").Text <> 1 Then
                    MessageBox.Show("Cargar cotizacion del dia para la moneda.", "Cotizacion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If
                Pagar(vdtTemp)
                VerificarEstado({item})

                GoTo Siguiente

            End If

            

Siguiente:

        Next

terminar:

        lvGrupo.Enabled = True
        lvEgresos.Enabled = True
        vProcesar = False

    End Sub

    Sub CargarEgresosAgrupados(ItemSeleccionado As ListViewItem)

        Dim IDTransaccion As Integer = ItemSeleccionado.SubItems("IDTransaccion").Text
        Dim IDProveedor As Integer = ItemSeleccionado.SubItems("IDProveedor").Text
        Dim IDCuentaBancaria As Integer = ItemSeleccionado.SubItems("IDCuentaBancaria").Text
        Dim IDMoneda As Integer = ItemSeleccionado.SubItems("IDMoneda").Text

        Dim vdtTemp As DataTable = dtEgreso.Clone
        Dim itemAgrupado(-1) As ListViewItem
        Dim Indice As Integer = 0

        For Each item As ListViewItem In lvEgresos.CheckedItems

            If item.Checked = False Then
                GoTo siguiente
            End If

            If item.SubItems("EstadoProceso").Text.ToUpper <> "PENDIENTE" Then
                GoTo siguiente
            End If

            If item.SubItems("IDProveedor").Text <> IDProveedor Or item.SubItems("IDCuentaBancaria").Text <> IDCuentaBancaria Or item.SubItems("IDMoneda").Text <> IDMoneda Then
                GoTo siguiente
            End If

            item.ImageIndex = 1

            ReDim Preserve itemAgrupado(Indice)
            itemAgrupado(Indice) = item
            Indice = itemAgrupado.Length

            'Copiar la linea a la tabla temporal
            vdtTemp.Rows.Add(dtEgreso.Select(" IDTransaccion=" & item.SubItems("IDTransaccion").Text)(0).ItemArray)

siguiente:

        Next

        Pagar(vdtTemp)
        VerificarEstado(itemAgrupado)

    End Sub

    Sub VerificarEstado(item() As ListViewItem)

        'Actualizar el item
        Dim Valido As Boolean = True

        For i As Integer = 0 To item.Length - 1
            Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpEgresoEstado @IDTransaccion = " & item(i).SubItems("IDTransaccion").Text & ", @IDSucursal=" & vgIDSucursal & ", @Cuota=" & item(i).SubItems("Cuota").Text).Copy
            Dim oRow As DataRow = dttemp.Rows(0)

            If oRow("Procesado") = False Then
                item(i).SubItems("EstadoProceso").Text = oRow("Mensaje")
                item(i).ImageIndex = 0
                Valido = False
            Else
                item(i).SubItems("EstadoProceso").Text = "Procesado"
                item(i).ImageIndex = 2
            End If

          
        Next

        If Valido = False Then

            Dim frmDialog As New frmDialog
            frmDialog.Mensaje = "Desea continuar?"
            frmDialog.Titulo = "Ordenes de Pago"

            If frmDialog.ShowDialog(Me) = Windows.Forms.DialogResult.No Then
                lvGrupo.Enabled = True
                lvEgresos.Enabled = True
                Detener()
            End If

        End If
        
    End Sub

    Sub PagarSeleccionado()

        If lvEgresos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        For Each item As ListViewItem In lvEgresos.SelectedItems

            If item.SubItems("EstadoProceso").Text.ToUpper <> "PENDIENTE" Then
                Exit For
            End If

            Dim vdtTemp As DataTable = dtEgreso.Clone

            'Copiar la linea a la tabla temporal
            vdtTemp.Rows.Add(dtEgreso.Select(" IDTransaccion=" & item.SubItems("IDTransaccion").Text)(0).ItemArray)
            Dim vCotizacionMoneda As Integer = CType(CSistema.ExecuteScalar("Select IsNull(Max(cotizacion), 1) From Cotizacion Where cast(fecha as date) = cast(getdate() as date) and IDMoneda=" & item.SubItems("IDMoneda").Text), Integer)
            If vCotizacionMoneda = 1 And item.SubItems("IDMoneda").Text <> 1 Then
                MessageBox.Show("Cargar cotizacion del dia para la moneda.", "Cotizacion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
            Pagar(vdtTemp)

            item.ImageIndex = 1


        Next

    End Sub

    Sub Pagar(vdtEgreso As DataTable)

        Dim frm As New frmOrdenPagoPagar
        frm.dtEgresosAPagar = vdtEgreso
        frm.CerrarAlGuardar = True
        frm.Inicializar()

        FGMostrarFormulario(Me, frm, "Orden de Pago", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)


    End Sub

    Sub SeleccionarTodo()
        For Each item As ListViewItem In lvEgresos.Items
            item.Checked = True
            item.ImageIndex = 3
        Next
    End Sub

    Sub QuitarSeleccion()
        For Each item As ListViewItem In lvEgresos.Items
            item.Checked = False
            item.ImageIndex = 5
        Next
    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        Select Case e.KeyCode
            Case Keys.Enter
                CSistema.SelectNextControl(Me, e.KeyCode)
        End Select

    End Sub

    Sub EliminarEgreso()

        If lvEgresos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        lvEgresos.SelectedItems(0).Remove()

    End Sub

    Private Sub frmFacturarEgresos_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs)
        Detener()
        GuardarInformacion()
    End Sub

    Private Sub frmFacturarEgresos_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        ManejarTecla(e)
    End Sub

    Private Sub btnActualizarGrupo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizarGrupo.Click
        ListarGrupos()
    End Sub

    Private Sub cbxGrupos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxGrupos.SelectedIndexChanged
        ListarGrupos()
    End Sub

    Private Sub lvGrupo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvGrupo.SelectedIndexChanged
        ListarEgresos()

        For Each item As ListViewItem In lvGrupo.Items
            item.ImageIndex = 5
        Next

        If lvGrupo.SelectedItems.Count > 0 Then
            lvGrupo.SelectedItems(0).ImageIndex = 2
        End If

    End Sub

    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

    Private Sub btnDetener_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnSeleccionarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionarTodos.Click
        SeleccionarTodo()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        QuitarSeleccion()
    End Sub

    Private Sub FacturarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FacturarToolStripMenuItem.Click
        PagarSeleccionado()
    End Sub

    Private Sub btnActualizarEgresos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ListarEgresos()
    End Sub

    Private Sub EliminarDeLaListaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarDeLaListaToolStripMenuItem.Click
        EliminarEgreso()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmPagoEgreso_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub lvEgresos_ItemCheck(sender As System.Object, e As System.Windows.Forms.ItemCheckEventArgs) Handles lvEgresos.ItemCheck
        If e.NewValue = CheckState.Checked Then
            lvEgresos.Items(e.Index).ImageIndex = 3
        Else
            lvEgresos.Items(e.Index).ImageIndex = 4
        End If
    End Sub

    Private Sub lvEgresos_ItemChecked(sender As Object, e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEgresos.ItemChecked

    End Sub

    Private Sub btnActualizarEgresos_Click_1(sender As System.Object, e As System.EventArgs) Handles btnActualizarEgresos.Click
        ListarEgresos()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmPagoEgreso_Activate()
        Me.Refresh()
    End Sub

End Class
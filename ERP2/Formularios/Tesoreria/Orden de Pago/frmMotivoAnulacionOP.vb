﻿Public Class frmMotivoAnulacionOP
    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(value As Integer)
            IDValue = value
        End Set
    End Property

    Dim CSistema As New CSistema

    Sub Inicializar()
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID,Descripcion from MotivoAnulacionOP where Estado = 1")
        cbxMotivo.cbx.SelectedIndex = 0
    End Sub

    Sub Aceptar()
        If cbxMotivo.cbx.SelectedValue Is Nothing Then
            tsslEstado.Text = "Atencion: Motivo invalido"
            ctrError.SetError(cbxMotivo, "Atencion: Motivo invalido")
            ctrError.SetIconAlignment(cbxMotivo, ErrorIconAlignment.TopRight)
            Exit Sub
        End If
        IDValue = cbxMotivo.GetValue
        Me.Close()
    End Sub

    Private Sub frmMotivoAnulacionOP_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Aceptar()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmMotivoAnulacionOP_Activate()
        Me.Refresh()
    End Sub
End Class
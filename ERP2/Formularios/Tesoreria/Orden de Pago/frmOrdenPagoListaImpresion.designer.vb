﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOrdenPagoListaImpresion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgwOperacion = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNroOP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEmision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFPago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNroCheque = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBanco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAlaOrden = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgwOperacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgwOperacion
        '
        Me.dgwOperacion.AllowUserToAddRows = False
        Me.dgwOperacion.AllowUserToDeleteRows = False
        Me.dgwOperacion.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgwOperacion.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgwOperacion.BackgroundColor = System.Drawing.Color.White
        Me.dgwOperacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwOperacion.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colNroOP, Me.colEmision, Me.colFPago, Me.colImporte, Me.colNroCheque, Me.colBanco, Me.colAlaOrden, Me.colEstado})
        Me.dgwOperacion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgwOperacion.Location = New System.Drawing.Point(0, 0)
        Me.dgwOperacion.Name = "dgwOperacion"
        Me.dgwOperacion.ReadOnly = True
        Me.dgwOperacion.RowHeadersVisible = False
        Me.dgwOperacion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgwOperacion.Size = New System.Drawing.Size(839, 266)
        Me.dgwOperacion.TabIndex = 11
        Me.dgwOperacion.TabStop = False
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colNroOP
        '
        Me.colNroOP.HeaderText = "NroOP"
        Me.colNroOP.Name = "colNroOP"
        Me.colNroOP.ReadOnly = True
        Me.colNroOP.Width = 40
        '
        'colEmision
        '
        Me.colEmision.HeaderText = "Emisión"
        Me.colEmision.Name = "colEmision"
        Me.colEmision.ReadOnly = True
        '
        'colFPago
        '
        Me.colFPago.HeaderText = "FPago"
        Me.colFPago.Name = "colFPago"
        Me.colFPago.ReadOnly = True
        Me.colFPago.Width = 50
        '
        'colImporte
        '
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        Me.colImporte.Width = 70
        '
        'colNroCheque
        '
        Me.colNroCheque.HeaderText = "NroCheque"
        Me.colNroCheque.Name = "colNroCheque"
        Me.colNroCheque.ReadOnly = True
        '
        'colBanco
        '
        Me.colBanco.HeaderText = "Banco"
        Me.colBanco.Name = "colBanco"
        Me.colBanco.ReadOnly = True
        Me.colBanco.Visible = False
        '
        'colAlaOrden
        '
        Me.colAlaOrden.HeaderText = "AlaOrden"
        Me.colAlaOrden.Name = "colAlaOrden"
        Me.colAlaOrden.ReadOnly = True
        Me.colAlaOrden.Width = 250
        '
        'colEstado
        '
        Me.colEstado.HeaderText = "Estado"
        Me.colEstado.Name = "colEstado"
        Me.colEstado.ReadOnly = True
        '
        'frmOrdenPagoListaImpresion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(839, 266)
        Me.Controls.Add(Me.dgwOperacion)
        Me.Name = "frmOrdenPagoListaImpresion"
        Me.Text = "frmOrdenTrabajoListaImpresion"
        CType(Me.dgwOperacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgwOperacion As System.Windows.Forms.DataGridView
    Friend WithEvents colIDTransaccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNroOP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEmision As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFPago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNroCheque As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBanco As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAlaOrden As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEstado As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿Imports System
Imports System.Text
Imports System.Globalization

Public Class frmImpresionCheque

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CReporteCheque As New Reporte.CReporteCheque

    'VARIABLES
    Dim vdtCuentaBancaria As DataTable
    Dim vdtOrdenPago As DataTable
    Dim vdtCiudad As DataTable
    Dim dtOrdenPago As DataTable
    Dim IDFormularioImpresion As Integer
    Dim IDOperacion As Integer
    Dim ChequesRestantes As Int32
    Dim nroChequeAUtilizar As String

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        CSistema.InicializaControles(Me)

        'RadioButton
        rdbNroOrdenPago.Checked = True

        'Botones inhabilitados
        btnDetalle.Enabled = False
        btnImprimir.Enabled = False

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion("frmOrdenPago", "ORDEN DE PAGO", "OP")
        CargarInformacion()

        DesplegarImpresoras()

    End Sub

    Sub CargarInformacion()

        'Tipos de Formularios
        cbxFormulario.cbx.Items.Clear()
        cbxFormulario.cbx.Items.Add("Cheque al Día")
        cbxFormulario.cbx.Items.Add("Diferido")
        cbxFormulario.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Cuentas Bancarias
        vdtCuentaBancaria = CData.GetTable("VCuentaBancaria").Copy
        CSistema.SqlToComboBox(cbxCuenta.cbx, vdtCuentaBancaria, "ID", "Cuenta")

        'Ciudades
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad,CodigoCiudad  From VSucursal Order By 2")

        'Impresion
        CSistema.SqlToComboBox(cbxConfiguracion.cbx, "Select IDFormularioImpresion, Descripcion From VFormularioImpresion Where Estado = 'True' and IDOperacion=" & IDOperacion & " Order By 2")

        'Numeros no impresos, MIN y MAX de OP
        dtOrdenPago = CSistema.ExecuteToDataTable("Select 'Min'=IsNull(MIN(Numero), 0), 'Max'=IsNull(MAX(Numero), 0) From VOrdenPago Where Cheque = 'True' And NroCheque = ''").Copy

        If dtOrdenPago Is Nothing Then
            Exit Sub
        End If

        If dtOrdenPago.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dtOrdenPago.Rows(0)
        txtOPInicial.SetValue(oRow("Min"))
        txtOPFinal.SetValue(oRow("Max"))

    End Sub

    Sub Cargar()
        If ValidarCuenta() = True Then
            MessageBox.Show("La Cuenta Bancaria no coincide con el Cheque de la OP.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        btnDetalle.Enabled = False
        btnImprimir.Enabled = False

        'Validar
        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar el numero de cheque
        If Not ValidarNumeroCheque() Then
            Dim mensaje As String = "El numero no existe en chequera."
            ctrError.SetError(txtPrimerCheque, mensaje)
            ctrError.SetIconAlignment(txtPrimerCheque, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If
        If nroChequeAUtilizar <> Convert.ToInt32(txtPrimerCheque.ObtenerValor) Then
            Dim mensaje As String = "El numero de cheque correlativo debe ser: " & nroChequeAUtilizar
            ctrError.SetError(txtPrimerCheque, mensaje)
            ctrError.SetIconAlignment(txtPrimerCheque, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If
        If ChequesRestantes < Convert.ToInt32(txtOPFinal.ObtenerValor - txtOPInicial.ObtenerValor + 1) Then
            Dim mensaje As String = "Cheques insuficientes para cantidad de OP."
            ctrError.SetError(txtPrimerCheque, mensaje)
            ctrError.SetIconAlignment(txtPrimerCheque, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        'vdtOrdenPago = CSistema.ExecuteToDataTable("Select * From VOrdenPago Where Numero Between " & txtOPInicial.ObtenerValor & " And " & txtOPFinal.ObtenerValor & " And Cheque = 'True' And NroCheque = '' And Anulado='False' And IDSucursal=" & cbxSucursal.GetValue()).Copy
        vdtOrdenPago = CSistema.ExecuteToDataTable("Select * From VOrdenPago Where Numero Between " & txtOPInicial.ObtenerValor & " And " & txtOPFinal.ObtenerValor & " And Cheque = 'True' And IDSucursal=" & cbxSucursal.GetValue() & " order by Numero asc").Copy

        If vdtOrdenPago Is Nothing Then
            MessageBox.Show("No se encontraron registros", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If vdtOrdenPago.Rows.Count = 0 Then
            MessageBox.Show("No se encontraron registros", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        'Habilitar controles
        btnDetalle.Enabled = True
        btnImprimir.Enabled = True

    End Sub

    Sub Procesar()

        'Validar
        'Si es que MAX y MIN OP no sean 0
        If txtOPInicial.ObtenerValor = 0 Or txtOPFinal.ObtenerValor = 0 Then
            CSistema.MostrarError("Los numeros de OP no concuerdan", ctrError, btnImprimir, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        ''MIN no sea menor a MAX
        If Val(txtOPInicial.ObtenerValor) > Val(txtOPFinal.ObtenerValor) Then
            CSistema.MostrarError("La OP Inicial no puede ser mayor a la OP Final", ctrError, btnImprimir, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'El nro de cheque no sea 0
        If txtPrimerCheque.ObtenerValor = 0 Then
            CSistema.MostrarError("El Primer Cheque no puede ser igual a cero", ctrError, btnImprimir, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'Elegir un formulario
        If cbxFormulario.cbx.Text = "" Then
            CSistema.MostrarError("Elegir algún formulario", ctrError, btnImprimir, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'Elegir una impresora
        If cbxImpresora.cbx.Text = "" Then
            CSistema.MostrarError("Debe elegir una impresora", ctrError, btnImprimir, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'Variables
        Dim ImporteLetras As String
        Dim OP As String

        'El Maximo depende del cheque, este valor debe ser configurado por cheque
        '80 es para ITAU, establecer en CuentaBancaria
        Dim MaximoPrimeraLinea As Integer = 70

        For Each oRow As DataRow In vdtOrdenPago.Rows

            IDTransaccion = (oRow("IDTransaccion"))
            'ImporteLetras = Numalet.Convertir(CInt(oRow("Importe").ToString), False)
            ImporteLetras = CSistema.NumeroALetra(CDec(oRow("Importe").ToString))

            OP = (oRow("Numero").ToString)
            OP = "OP: " & OP

            'Impimir
            If chkContinuo.Checked = False Then
                Imprimir(ImporteLetras, OP, MaximoPrimeraLinea)
            Else
                Imprimir2()
            End If

            'Mensaje
            Dim msg As DialogResult = MessageBox.Show("Se imprimió correctamente el cheque?", "Cheque", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

            Select Case msg

                Case Windows.Forms.DialogResult.Yes

                    If ActualizarOrdenPago(oRow("IDTransaccion"), txtPrimerCheque.ObtenerValor, cbxCuenta.cbx.Text, OP) = False Then
                        If MessageBox.Show("Desea continuar?", "Cheque", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
                            Inicializar()
                            Exit Sub
                        End If
                    End If

                Case Windows.Forms.DialogResult.No
                    'Inicializar()
                    Exit Sub
                Case Windows.Forms.DialogResult.Cancel
                    Inicializar()
                    Exit Sub
            End Select

            Dim ProximoNumero As Integer
            ProximoNumero = txtPrimerCheque.ObtenerValor + 1
            txtPrimerCheque.SetValue(ProximoNumero)

        Next

        CargarInformacion()

    End Sub

    Private Function ValidarNumeroCheque() As Boolean

        Dim dtChequera As DataTable               
        dtChequera = CSistema.ExecuteToDataTable("select *, 'Restante' = NroHasta - AUtilizar + 1 from VChequera where CuentaBancaria = '" & cbxCuenta.cbx.Text & "' and '" & Convert.ToInt32(txtPrimerCheque.ObtenerValor) & "' between NroDesde and NroHasta").Copy

        If dtChequera Is Nothing Then
            Return False
        End If

        If dtChequera.Rows.Count = 0 Then
            Return False
        End If

        Dim oRow As DataRow = dtChequera.Rows(0)
        nroChequeAUtilizar = oRow("AUtilizar")
        ChequesRestantes = Convert.ToInt32(oRow("Restante"))

        Return True

    End Function
    Private Function ValidarCuenta() As Boolean

        Dim dtCuentaBancaria As DataTable
        dtCuentaBancaria = CSistema.ExecuteToDataTable("select C.* from Ordenpago  OP join Cheque C on OP.Numero = C.Numero where C.IDCuentaBancaria <> " & cbxCuenta.GetValue & " and OP.Numero between " & txtOPInicial.ObtenerValor & " and " & txtOPFinal.ObtenerValor).Copy

        If dtCuentaBancaria Is Nothing Then
            Return False
        End If

        If dtCuentaBancaria.Rows.Count = 0 Then
            Return False
        End If

        Return True

    End Function
    Function RetornarTexto(ByVal PrimeraLinea As Boolean, ByVal Valor As String, ByVal Maximo As Integer) As String

        Dim TotalPalabras() As String = Valor.Split(" ")
        Dim Texto1 As String = ""
        Dim Texto2 As String = ""
        Dim TextoTemporal As String = ""

        For Palabras = 0 To TotalPalabras.GetLength(0) - 1

            TextoTemporal = TextoTemporal & " " & TotalPalabras(Palabras)

            If TextoTemporal.Length < Maximo Then
                Texto1 = Texto1 & " " & TotalPalabras(Palabras)
            Else
                Texto2 = Texto2 & " " & TotalPalabras(Palabras)
            End If

        Next

        If PrimeraLinea = True Then
            Return Texto1.Trim
        Else
            Return Texto2.Trim
        End If

    End Function

    Sub DesplegarImpresoras()

        cbxImpresora.cbx.Items.Clear()

        For Each strPrinter As String In System.Drawing.Printing.PrinterSettings.InstalledPrinters
            cbxImpresora.cbx.Items.Add(strPrinter)
        Next

    End Sub

    Function ActualizarOrdenPago(ByVal IDTransaccion As Integer, ByVal NroCheque As String, ByVal NroCuentaBancaria As String, Optional OP As String = "") As Boolean

        ActualizarOrdenPago = False

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroCheque", NroCheque, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroCuentaBancaria", NroCuentaBancaria, ParameterDirection.Input)
        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        'Insertar Registro
        Dim MensajeRetorno As String = ""
        If CSistema.ExecuteStoreProcedure(param, "SpActualizarNroCheque", False, False, MensajeRetorno) = False Then
            MessageBox.Show(OP & ": " & MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        Else
            Return True
        End If

    End Function

    Sub Imprimir(ByVal ImporteLetras As String, ByVal OP As String, ByVal MaximoPrimeraLinea As Integer)

        Dim frm As New frmReporte
        Dim Diferido As Boolean = False
        Dim Cadena1 As String = ""
        Dim Cadena2 As String = ""

        If cbxFormulario.cbx.SelectedIndex = 1 Then
            Diferido = True
        End If

        Cadena1 = RetornarTexto(True, ImporteLetras, MaximoPrimeraLinea)
        Cadena2 = RetornarTexto(False, ImporteLetras, MaximoPrimeraLinea)

        CReporteCheque.ImprimirCheque(frm, IDTransaccion, cbxCuenta.GetValue, Diferido, cbxImpresora.cbx.Text, Cadena1, Cadena2, OP)

    End Sub

    Sub Imprimir2()

        Dim CImpresion As New CImpresion
        CImpresion.IDTransaccion = IDTransaccion
        CImpresion.IDFormularioImpresion = cbxConfiguracion.GetValue
        CImpresion.Impresora = cbxImpresora.cbx.Text
        CImpresion.Test = False
        CImpresion.Inicializar()
        CImpresion.Imprimir()

    End Sub

    Sub Detalle()

        Dim frm As New frmOrdenPagoListaImpresion
        frm.dt = vdtOrdenPago
        FGMostrarFormulario(Me, frm, "Lista de Cheques a imprimir", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

    End Sub

    Sub ObtenerSucursal()

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

    End Sub

    Public NotInheritable Class Numalet

        Private Const UNI As Integer = 0, DIECI As Integer = 1, DECENA As Integer = 2, CENTENA As Integer = 3
        Private Shared _matriz As String(,) = New String(CENTENA, 9) { _
            {Nothing, " UNO", " DOS", " TRES", " CUATRO", " CINCO", " SEIS", " SIETE", " OCHO", " NUEVE"}, _
            {" DIEZ", " ONCE", " DOCE", " TRECE", " CATORCE", " QUINCE", " DIECISEIS", " DIECISIETE", " DIECIOCHO", " DIECINUEVE"}, _
            {Nothing, Nothing, Nothing, " TREINTA", " CUARENTA", " CINCUENTA", " SESENTA", " SETENTA", " OCHENTA", " NOVENTA"}, _
            {Nothing, Nothing, Nothing, Nothing, Nothing, " QUINIENTOS", Nothing, " SETECIENTOS", Nothing, " NOVECIENTOS"}}
        Private Const [sub] As Char = CChar(ChrW(26))

        Public Const ApocoparUnoParteEnteraDefault As Boolean = False
        Private _apocoparUnoParteEntera As Boolean = False

        Public Property ApocoparUnoParteEntera() As Boolean
            Get
                Return _apocoparUnoParteEntera
            End Get
            Set(ByVal value As Boolean)
                _apocoparUnoParteEntera = value
            End Set
        End Property


        Public Shared Function Convertir(ByVal Numero As Decimal, ByVal ApocoparUnoParteEntera As Boolean) As String
            Dim Num As Int64
            Dim terna As Int32, centenaTerna As Int32, decenaTerna As Int32, unidadTerna As Int32, iTerna As Int32
            Dim cadTerna As String
            Dim Resultado As New StringBuilder()

            Num = Math.Floor(Math.Abs(Numero))

            If Num >= 1000000000001 OrElse Num < 0 Then
                Throw New ArgumentException("El número '" + Numero.ToString() + "' excedió los límites del conversor: [0;1.000.000.000.001]")
            End If
            If Num = 0 Then
                Resultado.Append(" CERO")
            Else
                iTerna = 0

                Do Until Num = 0

                    iTerna += 1
                    cadTerna = String.Empty
                    terna = Num Mod 1000

                    centenaTerna = Int(terna / 100)
                    decenaTerna = terna - centenaTerna * 100 'Decena junto con la unidad
                    unidadTerna = (decenaTerna - Math.Floor(decenaTerna / 10) * 10)

                    Select Case decenaTerna
                        Case 1 To 9
                            cadTerna = _matriz(UNI, unidadTerna) + cadTerna
                        Case 10 To 19
                            cadTerna = cadTerna + _matriz(DIECI, unidadTerna)
                        Case 20
                            cadTerna = cadTerna + " VEINTE"
                        Case 21 To 29
                            cadTerna = " VEINTI" + _matriz(UNI, unidadTerna).Substring(1)
                        Case 30 To 99
                            If unidadTerna <> 0 Then
                                cadTerna = _matriz(DECENA, Int(decenaTerna / 10)) + " Y" + _matriz(UNI, unidadTerna) + cadTerna
                            Else
                                cadTerna += _matriz(DECENA, Int(decenaTerna / 10))
                            End If
                    End Select

                    Select Case centenaTerna
                        Case 1
                            If decenaTerna > 0 Then
                                cadTerna = " CIENTO" + cadTerna
                            Else
                                cadTerna = " CIEN" + cadTerna
                            End If
                            Exit Select
                        Case 5, 7, 9
                            cadTerna = _matriz(CENTENA, Int(terna / 100)) + cadTerna
                            Exit Select
                        Case Else
                            If Int(terna / 100) > 1 Then
                                cadTerna = _matriz(UNI, Int(terna / 100)) + " CIENTOS" + cadTerna
                            End If
                            Exit Select
                    End Select
                    'Reemplazo el 'uno' por 'un' si no es en las únidades o si se solicító apocopar
                    If (iTerna > 1 OrElse ApocoparUnoParteEntera) AndAlso decenaTerna = 21 Then
                        cadTerna = cadTerna.Replace("VEINTIUNO", "VEINTIUN")
                    ElseIf (iTerna > 1 OrElse ApocoparUnoParteEntera) AndAlso unidadTerna = 1 AndAlso decenaTerna <> 11 Then
                        cadTerna = cadTerna.Substring(0, cadTerna.Length - 1)
                        'Acentúo 'veintidós', 'veintitrés' y 'veintiséis'
                    ElseIf decenaTerna = 22 Then
                        cadTerna = cadTerna.Replace("VEINTIDOS", "VEINTIDOS")
                    ElseIf decenaTerna = 23 Then
                        cadTerna = cadTerna.Replace("VEINTITRES", "VEINTITRES")
                    ElseIf decenaTerna = 26 Then
                        cadTerna = cadTerna.Replace("VEINTISEIS", "VEINTISEIS")
                    End If

                    'Completo miles y millones
                    Select Case iTerna
                        Case 3
                            If Numero < 2000000 Then
                                cadTerna += " MILLON"
                            Else
                                cadTerna += " MILLONES"
                            End If
                        Case 2, 4
                            If terna > 0 Then cadTerna += " MIL"
                    End Select
                    Resultado.Insert(0, cadTerna)
                    Num = Int(Num / 1000)
                Loop
            End If

            Return Resultado.ToString().Substring(1) & " "
        End Function

    End Class

    Private Sub frmImpresionCheque_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmImpresionCheque_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCargar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCargar.Click
        Cargar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Procesar()
    End Sub

    Private Sub btnDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDetalle.Click
        Detalle()
    End Sub

    Private Sub cbxCuenta_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCuenta.PropertyChanged
        For Each oRow As DataRow In vdtCuentaBancaria.Select(" ID = " & cbxCuenta.GetValue)
            txtBanco.txt.Text = oRow("Referencia")
        Next
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub rdbNroOrdenPago_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbNroOrdenPago.CheckedChanged
        If rdbNroOrdenPago.Checked = True Then
            txtFechaDesde.SoloLectura = True
            txtFechaHasta.SoloLectura = True
            txtOPInicial.SoloLectura = False
            txtOPFinal.SoloLectura = False
        End If
    End Sub

    Private Sub rdbFechaCheque_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbFechaCheque.CheckedChanged
        If rdbFechaCheque.Checked = True Then
            txtOPInicial.SoloLectura = True
            txtOPFinal.SoloLectura = True
            txtFechaDesde.SoloLectura = False
            txtFechaHasta.SoloLectura = False
        End If
    End Sub
    Private Sub cbxCiudad_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged
        ObtenerSucursal()
    End Sub


    Private Sub cbxCuenta_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxCuenta.TeclaPrecionada
        For Each oRow As DataRow In vdtCuentaBancaria.Select(" ID = " & cbxCuenta.GetValue)
            txtBanco.txt.Text = oRow("Referencia")
        Next
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmImpresionCheque_Activate()
        Me.Refresh()
    End Sub
End Class

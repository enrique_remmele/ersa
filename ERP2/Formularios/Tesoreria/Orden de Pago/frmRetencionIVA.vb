﻿Imports ERP.Reporte
Public Class frmRetencionIVA

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Dim CData As New CData
    Dim CRetencionIVA As New CReporteOrdenPago

    Dim vdtCiudad As DataTable

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private IDTransaccionOrdenPagoValue As Integer
    Public Property IDTransaccionOrdenPago() As Integer
        Get
            Return IDTransaccionOrdenPagoValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionOrdenPagoValue = value
        End Set
    End Property

    Private OPValue As Boolean
    Public Property OP() As Boolean
        Get
            Return OPValue
        End Get
        Set(ByVal value As Boolean)
            OPValue = value
        End Set
    End Property

    'EVENTOS

    'VARIABLES
    Dim dtEgresos As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim dtPuntoExpedicion As DataTable
    Dim vHabilitar As Boolean
    Dim vConComprobantes As Boolean
    Dim vObservacionDevolucion As String
    Dim vObservacionDescuento As String
    Dim MontoLetras As String

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button


        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "RETENCION IVA", "RI")
        vNuevo = False
        vHabilitar = True
        vConComprobantes = True

        'Otros
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False
        gbxCabecera.BringToFront()
        txtNroComprobante.txt.ReadOnly = False

        'Funciones
        CargarInformacion()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)
        btnEliminar.Visible = False
        btnEliminar.BringToFront()

        btnBusquedaAvanzada.Enabled = False

        txtNroComprobante_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        txtOP.Decimales = False
        If CBool(vgConfiguraciones("TesoreriaBloquerFecha").ToString) = True Then
            txtFecha.Enabled = False
        Else
            txtFecha.Enabled = True
        End If
    End Sub

    Sub SeleccionarTimbrado()

        Dim frm As New frmSeleccionarTimbrado
        frm.ID = txtIDTimbrado.GetValue
        frm.IDOperacion = IDOperacion
        frm.IDSucursal = vgIDSucursal

        FGMostrarFormulario(Me, frm, "Timbrados", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Seleccionado = False Then
            Exit Sub
        End If

        txtIDTimbrado.SetValue(frm.ID)
        ObtenerInformacionRetencionIVA()
        txtNroComprobante_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'Cabecera
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, txtCotizacion)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, cbxProveedor)
        CSistema.CargaControl(vControles, txtOP)
        CSistema.CargaControl(vControles, btnListar)
        CSistema.CargaControl(vControles, chkOP)
        CSistema.CargaControl(vControles, btnCargar)

        'Detalle
        'CSistema.CargaControl(vControles, dgw)

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CARGAR CONTROLES
        'Ciudad
        'cbxCiudad.Conectar()
        CSistema.SqlToComboBox(cbxCiudad.cbx, CData.GetTable("VCiudad"), "ID", "Codigo")

        'Sucursal
        'cbxSucursal.Conectar()
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal"), "ID", "Descripcion")

        'CARGAR CONTROLES
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, CData.GetTable("VTipoComprobante", " IDOperacion = " & IDOperacion), "ID", "Codigo")


        'Puntos de Expediciones
        dtPuntoExpedicion = CData.GetTable("VPuntoExpedicion").Copy
        dtPuntoExpedicion = CData.FiltrarDataTable(dtPuntoExpedicion, " IDOperacion = " & IDOperacion & " And Estado='True' ")
        txtIDTimbrado.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIMBRADO", 0))
        'Si no tenemos el timbrado, solicitar
        If txtIDTimbrado.GetValue = 0 Then
            MessageBox.Show("Para empezar seleccione un timbrado para continuar.", "Timbrado", MessageBoxButtons.OK, MessageBoxIcon.Information)
            SeleccionarTimbrado()
        End If

        ObtenerInformacionRetencionIVA()

        'Ciudades
        cbxCiudad.Conectar("", "Descripcion")

        'Sucursal
        cbxSucursal.Conectar()


        'Monedas
        cbxMoneda.Conectar()
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Ciudades



    End Sub

    Sub GuardarInformacion()

        'TIMBRADO
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIMBRADO", txtIDTimbrado.GetValue)

    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        dtEgresos = CSistema.ExecuteToDataTable("Select Top(0) * From VComprobanteRetencion")

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True
        txtNroComprobante.txt.ReadOnly = False

        'CheckBox
        txtObservacion.SetValue(vObservacionDevolucion)


        'cbxTipoComprobante.cbx.Text = ""

        'Recargar el dtTerminalPuntoExpedicion
        CData.ResetTable("VPuntoExpedicion")
        dtPuntoExpedicion = CData.GetTable("VPuntoExpedicion", " IDOperacion = " & IDOperacion & " And Estado='True' ")
        ObtenerInformacionRetencionIVA()

        LimpiarControles()

        chkOP.Enabled = True
        'chkOP.Checked = False

        'SMC - 21052021 - Obliga a la carga de OP 
        chkOP.Checked = True
        btnCargar.Enabled = False
        HabilitarCargarComprobantes()
        txtOP.Focus()
        'Cabecera
        txtFecha.Hoy()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False


        'Nro de Comprobante, mayor a 0
        If IsNumeric(txtNroComprobante.txt.Text) = False Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Asignamos entero al nro de comprobante
        txtNroComprobante.txt.Text = CInt(txtNroComprobante.txt.Text)

        If CInt(txtNroComprobante.txt.Text) = 0 Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Moneda
        If IsNumeric(cbxMoneda.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el tipo de moneda!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If


        'Existencia en Detalle
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If dtEgresos.Rows.Count = 0 Then
                Dim mensaje As String = "El documento debe tener por lo menos 1 detalle!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

        End If


        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        ValidarDocumento = True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        Dim IndiceOperacion As Integer

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        'Punto de Expedicion
        Dim PuntoExpedicionRow As DataRow = dtPuntoExpedicion.Select("ID=" & txtIDTimbrado.GetValue)(0)

        CSistema.SetSQLParameter(param, "@IDPuntoExpedicion", txtIDTimbrado.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", PuntoExpedicionRow("IDTipoComprobante").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtNroComprobante.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProveedor", cbxProveedor.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", PuntoExpedicionRow("IDSucursal").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTransaccionOrdenPago", IDTransaccionOrdenPago, ParameterDirection.Input)

        If chkOP.Checked = True Then
            OP = True
        Else
            OP = False
        End If

        CSistema.SetSQLParameter(param, "@OP", OP, ParameterDirection.Input)

        'Moneda
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.cbx, ParameterDirection.Input)
        If txtCotizacion.ObtenerValor = 0 Then
            CSistema.SetSQLParameter(param, "@Cotizacion", "1".ToString, ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Cotizacion", txtCotizacion.ObtenerValor, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text.Trim, ParameterDirection.Input)

        'Totales
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(txtTotalRetencion.ObtenerValor, cbxMoneda), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalIVA", CSistema.FormatoMonedaBaseDatos(txtTotalRetencionIVA.ObtenerValor, cbxMoneda), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalRenta", CSistema.FormatoMonedaBaseDatos(txtTotalretencionRenta.ObtenerValor), ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Capturamos el index de la Operacion para un posible proceso posterior
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpRetencionIVA", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then
                If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From RetencionIVA Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                    param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                End If
            End If

            Exit Sub

        End If

        Dim Procesar As Boolean = True

        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
            CargarOperacion(IDTransaccion)
            Exit Sub
        End If

        If chkOP.Checked = False Then

            If IDTransaccion > 0 Then

                'Cargamos el Detalle de la Retencion 
                Procesar = InsertarDetalleRetencion(IDTransaccion)

            End If

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        vNuevo = False
        CargarOperacion(IDTransaccion)

        txtNroComprobante.SoloLectura = False

    End Sub

    Function InsertarDetalleRetencion(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalleRetencion = True

        Dim ID As Integer = 1

        For Each oRow As DataRow In dtEgresos.Rows


            Dim sql As String = "Insert Into DetalleRetencion(IDTransaccion,ID, IDTransaccionEgreso, TipoyComprobante,Fecha,Gravado5,IVA5,Gravado10,IVA10,Total5,Total10,PorcRetencion,RetencionIVA,PorRenta,Renta)Values(" & IDTransaccion & "," & ID & ", '" & oRow("IDTransaccionEgreso").ToString & "', '" & oRow("TipoyComprobante").ToString & "', '" & oRow("Fecha").ToString & "'," & CSistema.FormatoMonedaBaseDatos(oRow("Gravado5").ToString, cbxMoneda) & "," & CSistema.FormatoMonedaBaseDatos(oRow("IVA5").ToString, cbxMoneda) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Gravado10").ToString, cbxMoneda) & "," & CSistema.FormatoMonedaBaseDatos(oRow("IVA10").ToString, cbxMoneda) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Total5").ToString, cbxMoneda) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Total10").ToString, cbxMoneda) & "," & oRow("PorcRetencion").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("RetencionIVA").ToString, cbxMoneda) & "," & oRow("PorcRenta").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("Renta").ToString, cbxMoneda) & ")"

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            ID = ID + 1

        Next

    End Function

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpRetencionIVA", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From RetencionIVA Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
            End If

            Exit Sub

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)



    End Sub

    Sub Eliminar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        'Consultar
        If MessageBox.Show("Desea eliminar el comprobante?", "Elininar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> DialogResult.Yes Then
            Exit Sub
        End If

        tsslEstado.Text = ""
        ctrError.Clear()

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpRetencionIVA", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            Exit Sub

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

        txtNroComprobante.Focus()
        MoverRegistro(New KeyEventArgs(Keys.End))

    End Sub

    Sub MoverRegistro(ByRef e As System.Windows.Forms.KeyEventArgs)

        If vNuevo = True Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtNroComprobante.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtNroComprobante.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(NroComprobante), 1) From VRetencionIVA Where IDPuntoExpedicion=" & txtIDTimbrado.GetValue & " "), Integer)

            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(NroComprobante), 1) From VRetencionIVA Where IDPuntoExpedicion=" & txtIDTimbrado.GetValue & " "), Integer)

            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub


    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'Otros
        Dim frm As New frmConsultaRetencionIVA
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog(Me)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        txtObservacion.SoloLectura = True

        'vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtNroComprobante.txt.Focus()
        txtNroComprobante.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VRetencionIVA Where Comprobante = '" & ObtenerNroComprobante() & "' And IDTipoComprobante=" & cbxTipoComprobante.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtNroComprobante, mensaje)
            ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)


        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VRetencionIVA Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtNroComprobante, mensaje)
            ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        txtTimbrado.SetValue(oRow("Timbrado").ToString)

        cbxCiudad.SelectedValue(oRow("IDCiudad").ToString)
        cbxSucursal.SelectedValue(oRow("IDSucursal").ToString)

        cbxTipoComprobante.SelectedValue(oRow("IDTipoComprobante").ToString)
        txtNroComprobante.txt.Text = oRow("NroComprobante").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        cbxMoneda.SelectedValue(oRow("IDMoneda").ToString)
        txtCotizacion.txt.Text = oRow("Cotizacion").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtOP.txt.Text = oRow("Numero").ToString
        cbxProveedor.cbx.Text = oRow("Proveedor").ToString
        txtRUC.txt.Text = oRow("RUC").ToString
        txtDireccion.txt.Text = oRow("Direccion").ToString

        If oRow("OP").ToString = True Then
            chkOP.Checked = True
        Else
            chkOP.Checked = False
        End If

        chkOP.Enabled = False

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            btnEliminar.Visible = True
        Else
            flpAnuladoPor.Visible = False
            btnEliminar.Visible = False
        End If

        ListarEgresos(IDTransaccion)


    End Sub


    Sub LimpiarControles()
        'Limpiar Controles
        cbxProveedor.cbx.Text = Nothing
        cbxProveedor.txt.Clear()
        txtOP.txt.Clear()
        txtFecha.txt.Clear()
        txtObservacion.Clear()
        cbxMoneda.txt.Clear()
        txtCotizacion.txt.Clear()
        txtObservacion.txt.Clear()
        txtRUC.txt.Clear()
        txtDireccion.txt.Clear()
        dgw.Rows.Clear()
        txtTotalRetencion.txt.Clear()
        txtTotalretencionRenta.txt.Clear()
        txtTotalRetencionIVA.txt.Clear()
        IDTransaccionOrdenPago = 0
        dtEgresos.Rows.Clear()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Desaparecer Label
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        LimpiarControles()

        txtNroComprobante.txt.Focus()

        vNuevo = False

        txtNroComprobante_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Function ObtenerNroComprobante() As String

        Dim PuntoExpedicionRow As DataRow = dtPuntoExpedicion.Select("ID=" & txtIDTimbrado.GetValue)(0)
        ObtenerNroComprobante = PuntoExpedicionRow("ReferenciaSucursal").ToString & "-" & PuntoExpedicionRow("ReferenciaPunto").ToString & "-" & txtNroComprobante.GetValue

    End Function

    Sub ObtenerInformacionRetencionIVA()

        txtTalonario.txt.Text = "0"
        txtVencimientoTimbrado.txt.Text = ""
        txtRestoTimbrado.txt.Text = "0"
        cbxTipoComprobante.cbx.Text = ""
        txtReferenciaSucursal.txt.Text = "000"
        txtReferenciaTerminal.txt.Text = "000"
        txtNroComprobante.txt.Text = "000"
        cbxCiudad.cbx.Text = ""
        cbxSucursal.cbx.Text = ""

        For Each oRow As DataRow In dtPuntoExpedicion.Select("ID=" & txtIDTimbrado.GetValue)

            txtTimbrado.txt.Text = oRow("Timbrado").ToString
            txtTalonario.txt.Text = oRow("TalonarioActual").ToString
            txtVencimientoTimbrado.txt.Text = oRow("Vencimiento").ToString
            txtRestoTimbrado.txt.Text = oRow("Saldo").ToString
            cbxTipoComprobante.SelectedValue(oRow("IDTipoComprobante").ToString)
            txtReferenciaSucursal.txt.Text = oRow("ReferenciaSucursal").ToString
            txtReferenciaTerminal.txt.Text = oRow("ReferenciaPunto").ToString
            txtNroComprobante.txt.Text = oRow("ProximoNumero").ToString
            cbxCiudad.SelectedValue(oRow("IDCiudad").ToString)
            cbxSucursal.SelectedValue(oRow("IDSucursal").ToString)

        Next

    End Sub

    Sub ManejarTecla(ByVal e As KeyEventArgs)

        If vNuevo = True Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtNroComprobante.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtNroComprobante.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(NroComprobante), 1) From VRetencionIVA Where IDPuntoExpedicion=" & txtIDTimbrado.GetValue & " "), Integer)

            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(NroComprobante), 1) From VRetencionIVA Where IDPuntoExpedicion=" & txtIDTimbrado.GetValue & " "), Integer)

            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub ListarEgresos(Optional ByVal vIDTransaccion As Integer = 0)

        Dim TotalRetencion As Decimal = 0
        Dim TotalRenta As Decimal = 0
        Dim Total As Decimal = 0
        Dim Decimales As Boolean = CSistema.MonedaDecimal(cbxMoneda.GetValue)

        dtEgresos.Clear()

        'Proveedor
        If cbxProveedor.txt.Text = "" Then
            Dim mensaje As String = "Ingrese un proveedor!"
            CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
            Exit Sub
        End If

        ctrError.Clear()

        'Cargar
        dtEgresos = CSistema.ExecuteToDataTable("Select * From VComprobanteRetencion Where IDTransaccion = " & vIDTransaccion & " and Proveedor = '" & cbxProveedor.cbx.Text & "'")

        'Limpiar la grilla
        dgw.Rows.Clear()

        For Each oRow As DataRow In dtEgresos.Rows

            Dim oRow1(7) As String
            oRow1(0) = oRow("IDTransaccion").ToString
            oRow1(1) = oRow("TipoyComprobante").ToString
            oRow1(2) = oRow("Fecha").ToString
            oRow1(3) = CSistema.FormatoMoneda(oRow("TotalGravado").ToString, Decimales)
            oRow1(4) = CSistema.FormatoMoneda(oRow("TotalIVA").ToString, Decimales)
            oRow1(5) = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
            oRow1(6) = oRow("PorcRetencion").ToString
            oRow1(7) = CSistema.FormatoMoneda(oRow("RetencionIVA").ToString, Decimales)

            dgw.Rows.Add(oRow1)

            If IsNumeric(oRow("IDTransaccionOrdenPago").ToString) = True Then
                IDTransaccionOrdenPago = oRow("IDTransaccionOrdenPago").ToString
            Else
                IDTransaccionOrdenPago = 0
            End If

            If Decimales = False Then
                TotalRetencion = Math.Round(TotalRetencion + CDec(oRow("RetencionIVA").ToString), 0)
                TotalRenta = Math.Round(TotalRenta + CDec(oRow("Renta").ToString), 0)
                Total = TotalRetencion + TotalRenta
            Else
                TotalRetencion = TotalRetencion + CDec(oRow("RetencionIVA").ToString)
                TotalRenta = TotalRenta + CDec(oRow("Renta").ToString)
                Total = TotalRetencion + TotalRenta
            End If

        Next

        'Formato
        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        Next

        dgw.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        CalcularTotales()

    End Sub

    Sub ListarComprobantesSinOP()

        Dim TotalRetencion As Decimal = 0
        Dim TotalRenta As Decimal = 0
        Dim Total As Decimal = 0
        Dim Decimales As Boolean = CSistema.MonedaDecimal(cbxMoneda.GetValue)

        ctrError.Clear()

        'Limpiar la grilla
        dgw.Rows.Clear()

        For Each oRow As DataRow In dtEgresos.Rows

            Dim oRow1(8) As String
            oRow1(1) = oRow("TipoyComprobante").ToString()
            oRow1(2) = oRow("Fecha").ToString
            oRow1(3) = CSistema.FormatoMoneda(oRow("TotalGravado").ToString, Decimales)
            oRow1(4) = CSistema.FormatoMoneda(oRow("TotalIVA").ToString, Decimales)
            oRow1(5) = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
            oRow1(6) = oRow("PorcRetencion").ToString
            oRow1(7) = CSistema.FormatoMoneda(oRow("RetencionIVA").ToString, Decimales)

            dgw.Rows.Add(oRow1)

            TotalRetencion = TotalRetencion + CDec(oRow("RetencionIVA").ToString)
            TotalRenta = TotalRenta + CDec(oRow("Renta").ToString)
            Total = Total + (TotalRetencion + TotalRenta)

        Next

        'Formato
        Dim FormatoCeldas As String = "N0"

        If Decimales Then
            FormatoCeldas = "N2"
        End If

        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        Next

        dgw.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns(3).DefaultCellStyle.Format = FormatoCeldas
        dgw.Columns(4).DefaultCellStyle.Format = FormatoCeldas
        dgw.Columns(5).DefaultCellStyle.Format = FormatoCeldas
        dgw.Columns(7).DefaultCellStyle.Format = FormatoCeldas

        CalcularTotales()

    End Sub

    Sub CargarEgresos()

        'Cargar
        dtEgresos = CSistema.ExecuteToDataTable("Select * From VOrdenPagoEgreso Where Numero=" & txtOP.ObtenerValor & " and Proveedor='" & cbxProveedor.txt.Text & "'")

        Dim frm As New frmListaComprobantesRet
        frm.Text = "Selección de Comprobante para Retención"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.IDMoneda = cbxMoneda.GetValue
        frm.dt = dtEgresos

        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccion de Comprobantes para Retención", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        dtEgresos = frm.dt

        ListarEgresosRetencion()

    End Sub

    Sub ListarEgresosRetencion(Optional ByVal vIDTransaccion As Integer = 0)

        Dim TotalRetencion As Decimal = 0
        Dim TotalRenta As Decimal = 0
        Dim Total As Decimal = 0
        Dim Decimales As Boolean = CSistema.MonedaDecimal(cbxMoneda.GetValue)

        ctrError.Clear()


        'Limpiar la grilla
        dgw.Rows.Clear()

        For Each oRow As DataRow In dtEgresos.Rows

            Dim oRow1(7) As String
            oRow1(0) = oRow("IDTransaccion").ToString
            oRow1(1) = oRow("TipoyComprobante").ToString
            oRow1(2) = oRow("Fecha").ToString
            oRow1(3) = CSistema.FormatoMoneda(oRow("TotalGravado").ToString, Decimales)
            oRow1(4) = CSistema.FormatoMoneda(oRow("TotalIVA").ToString, Decimales)
            oRow1(5) = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
            oRow1(6) = oRow("PorcRetencion").ToString
            oRow1(7) = CSistema.FormatoMoneda(oRow("RetencionIVA").ToString, Decimales)

            dgw.Rows.Add(oRow1)

            If IsNumeric(oRow("IDTransaccionOrdenPago").ToString) = True Then
                IDTransaccionOrdenPago = oRow("IDTransaccionOrdenPago").ToString
            Else
                IDTransaccionOrdenPago = 0
            End If

            If Decimales = False Then
                TotalRetencion = Math.Round(TotalRetencion + CDec(oRow("RetencionIVA").ToString), 0)
                TotalRenta = Math.Round(TotalRenta + CDec(oRow("Renta").ToString), 0)
                Total = TotalRetencion + TotalRenta
            Else
                TotalRetencion = TotalRetencion + CDec(oRow("RetencionIVA").ToString)
                TotalRenta = TotalRenta + CDec(oRow("Renta").ToString)
                Total = TotalRetencion + TotalRenta
            End If

        Next

        'Formato
        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        Next

        dgw.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        CalcularTotales()

    End Sub

    Sub ListarEgresosConOP()

        Dim TotalRetencion As Decimal = 0
        Dim TotalRenta As Decimal = 0
        Dim Total As Decimal = 0

        dtEgresos.Clear()

        'Comprobante
        If cbxProveedor.txt.Text = "" Then
            Dim mensaje As String = "Ingrese una orden de pago!"
            CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
            Exit Sub
        End If

        ctrError.Clear()

        'Cargar
        dtEgresos = CSistema.ExecuteToDataTable("Select * From VOrdenPagoEgreso Where Numero=" & txtOP.ObtenerValor & " and Proveedor='" & cbxProveedor.txt.Text & "'")

        'Limpiar la grilla
        dgw.Rows.Clear()

        For Each oRow As DataRow In dtEgresos.Rows

            Dim oRow1(8) As String
            oRow1(0) = oRow("IDTransaccion").ToString
            oRow1(1) = oRow("TipoComprobante").ToString & " " & oRow("NroComprobante").ToString
            oRow1(2) = oRow("Fec").ToString
            oRow1(3) = CSistema.FormatoMoneda(oRow("TotalDiscriminado").ToString)
            oRow1(4) = CSistema.FormatoMoneda(oRow("TotalImpuesto").ToString)
            oRow1(5) = CSistema.FormatoMoneda(oRow("Total").ToString)
            oRow1(6) = oRow("PorcRetencion").ToString
            oRow1(7) = CSistema.FormatoMoneda(oRow("RetencionIVA").ToString)

            dgw.Rows.Add(oRow1)

            IDTransaccionOrdenPago = oRow("IDTransaccionOrdenPago").ToString
            TotalRetencion = TotalRetencion + CDec(oRow("RetencionIVA").ToString)
            'TotalRenta = TotalRenta + CDec(oRow("Importe").ToString)
            'Total = Total + CDec(oRow("Total").ToString)
            Total = Total + TotalRetencion
        Next

        'Formato
        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        Next

        dgw.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim TotalImpuesto As Decimal
        Dim TotalRetencionIVA As Decimal
        Dim TotalRetencionRenta As Decimal
        Dim TotalRetencion As Decimal

        TotalImpuesto = CSistema.dtSumColumn(dtEgresos, "TotalIVA")
        TotalRetencionIVA = CSistema.dtSumColumn(dtEgresos, "RetencionIVA")
        TotalRetencionRenta = CSistema.dtSumColumn(dtEgresos, "Renta")
        TotalRetencion = TotalRetencionIVA + TotalRetencionRenta

        txtTotalImpuesto.SetValue(TotalImpuesto)
        txtTotalRetencionIVA.SetValue(TotalRetencionIVA)
        txtTotalretencionRenta.SetValue(TotalRetencionRenta)
        txtTotalRetencion.SetValue(TotalRetencion)


    End Sub

    Sub Imprimir(Optional ByVal ImprimirDirecto As Boolean = False)

        If ImprimirDirecto = False Then
            If CBool(vgConfiguraciones("RetencionImprimirDocumento").ToString) = False Then
                Exit Sub
            End If
        End If

        Dim frm As New frmReporte
        MontoLetras = CSistema.NumeroALetra(txtTotalRetencionIVA.ObtenerValor)

        Dim Path As String = vgImpresionRetencionPath

        Path = vgImpresionRetencionPath


        CRetencionIVA.ImprimirRetencionIVA(frm, IDTransaccion, IDTransaccionOrdenPago, vgUsuarioIdentificador, MontoLetras, Path, vgImpresionFacturaImpresora)

    End Sub

    Sub ObtenerDatosProveedor()

        Dim dt As DataTable = CData.GetTable("VProveedor", " ID = " & cbxProveedor.GetValue)

        If dt Is Nothing Then
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)
        txtRUC.txt.Text = oRow("RUC").ToString
        txtDireccion.txt.Text = oRow("Direccion").ToString
    End Sub

    Sub ObtenerProveedor()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VOrdenPagoEgreso Where Numero=" & txtOP.ObtenerValor)

        If dt Is Nothing Then
            Dim mensaje As String = "La OP está inexistente!"
            ctrError.SetError(txtOP, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            cbxProveedor.cbx.Text = Nothing
            txtRUC.Clear()
            txtDireccion.Clear()
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Dim mensaje As String = "La OP está inexistente!"
            ctrError.SetError(txtOP, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            cbxProveedor.cbx.Text = Nothing
            txtRUC.Clear()
            txtDireccion.Clear()
            Exit Sub
        End If

        ctrError.Clear()
        tsslEstado.Text = ""


        If txtOP.txt.Text = "" Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxProveedor, "Select Distinct IDProveedor,Proveedor From VOrdenPagoEgreso Where Numero=" & txtOP.ObtenerValor)


    End Sub

    Sub HabilitarCargarComprobantes()
        If chkOP.Checked = True And chkOP.Enabled = True Then
            dgw.Rows.Clear()
            dtEgresos.Rows.Clear()
            cbxProveedor.cbx.DataSource = Nothing
            txtOP.SoloLectura = False
            btnCargar.Enabled = False
            btnListar.Enabled = True
            txtOP.Focus()
        End If
        If chkOP.Checked = False And chkOP.Enabled = True Then
            dgw.Rows.Clear()
            dtEgresos.Rows.Clear()
            cbxProveedor.cbx.DataSource = Nothing
            CSistema.SqlToComboBox(cbxProveedor.cbx, "Select Distinct ID, RazonSocial From Proveedor Order By 2")
            btnCargar.Enabled = True
            btnListar.Enabled = False
            txtOP.SoloLectura = True
            txtOP.txt.Clear()
            cbxProveedor.Focus()
        End If
    End Sub

    Sub CargarComprobantes()

        'dtEgresos.Rows.Clear()

        Dim frm As New frmCargarComprobantesSinOP
        frm.dt = dtEgresos
        frm.IDMoneda = cbxMoneda.GetValue
        frm.IDProveedor = cbxProveedor.GetValue

        FGMostrarFormulario(Me, frm, "Selección de Comprobantes sin OP", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
        dtEgresos = frm.dt
        ListarComprobantesSinOP()

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)
        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)
    End Sub

    Private Sub frmNotaCreditoCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmNotaCreditoCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmNotaCreditoCliente_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub lvComprobantes_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Delete Then
            'EliminarProductoDevolucion()
        End If

    End Sub

    Private Sub cbxPuntoExpedicion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ObtenerInformacionRetencionIVA()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub cbxCiudad_PropertyChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

        cbxSucursal.cbx.Text = ""

        If cbxCiudad.Validar() = False Then
            Exit Sub
        End If

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal", " IDCiudad = " & cbxCiudad.GetValue), "ID", "Descripcion")
        cbxSucursal.SelectedValue(vgIDSucursal)

    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Close()
    End Sub

    Private Sub txtOP_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOP.TeclaPrecionada

        If e.KeyValue = Keys.Enter Then

            ObtenerProveedor()

        End If

    End Sub

    Private Sub btnAgregarComprobante_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        CargarEgresos()
        'SMC - 24052021 - Se comenta linea de abajo 
        'ListarEgresosConOP()
    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub cbxProveedor_Leave(sender As Object, e As System.EventArgs) Handles cbxProveedor.Leave
        ObtenerDatosProveedor()
    End Sub

    Private Sub cbxProveedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxProveedor.PropertyChanged
        ObtenerDatosProveedor()
    End Sub

    Private Sub btnCargar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCargar.Click
        CargarComprobantes()
    End Sub

    Private Sub chkOP_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOP.CheckedChanged
        HabilitarCargarComprobantes()
    End Sub

    Private Sub lklTimbrado_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklTimbrado.LinkClicked
        SeleccionarTimbrado()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Eliminar(ERP.CSistema.NUMOperacionesRegistro.DEL)
    End Sub

    Private Sub chkOP_GotFocus(sender As Object, e As System.EventArgs) Handles chkOP.GotFocus
        chkOP.BackColor = Color.FromArgb(vgColorFocus)
        Label1.BackColor = Color.FromArgb(vgColorFocus)
    End Sub

    Private Sub chkOP_Leave(sender As System.Object, e As System.EventArgs) Handles chkOP.Leave
        chkOP.BackColor = Color.FromArgb(vgColorBackColor)
        Label1.BackColor = Color.FromArgb(vgColorBackColor)
    End Sub

    'SMC - 20052021 - Validar que se cargue la OP relacionada al Pago de la Factura
    Private Sub txtOP_LostFocus(sender As Object, e As EventArgs) Handles txtOP.LostFocus
        If txtOP.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe cargar la OP relacionada!"
            ctrError.SetError(txtOP, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            txtOP.Focus()
        End If
    End Sub



End Class
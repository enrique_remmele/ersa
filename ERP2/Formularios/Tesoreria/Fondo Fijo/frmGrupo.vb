﻿Public Class frmGrupo

    'CLASES
    Dim CSistema As New CSistema


    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Focus
        dgvLista.Focus()

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtDescripcion)
        CSistema.CargaControl(vControles, rdbActivo)
        CSistema.CargaControl(vControles, rdbDesactivado)
        CSistema.CargaControl(vControles, ocxCuenta)
        CSistema.CargaControl(vControles, chkSoloVales)

        'CuentaContable
        ocxCuenta.Conectar()

        'Usuarios
        CSistema.SqlToDataGrid(dgvUsuario, "Select ID, Usuario, Nombre From VUsuario order by Nombre")
        dgvUsuario.Columns("ID").Visible = False
        dgvUsuario.Columns("Nombre").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        'Cargamos los registos en el lv
        Listar()

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If dgvLista.SelectedRows.Count = 0 Then
            ctrError.SetError(dgvLista, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgvLista, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = dgvLista.SelectedRows(0).Cells(0).Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VGrupo Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            txtDescripcion.txt.Text = oRow("Descripcion").ToString
            ocxCuenta.SeleccionarRegistro(oRow("Codigo").ToString)
            ocxCuenta.txtCodigo.Texto = oRow("Codigo").ToString
            ocxCuenta.txtDescripcion.Texto = oRow("DescripcionCC").ToString

            If oRow("SoloVales").ToString = "True" Then
                chkSoloVales.Valor = True
            Else
                chkSoloVales.Valor = False
            End If



            If CBool(oRow("Estado")) = True Then
                rdbActivo.Checked = True
            Else
                rdbDesactivado.Checked = True
            End If

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ListarUsuarios()

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        CSistema.SqlToDataGrid(dgvLista, "Select ID, Descripcion, 'Estado'=(Case When Estado = 'True' Then 'OK' Else '-' End) From VGrupo Order By 2")

        If dgvLista.Columns.Count = 0 Then
            Exit Sub
        End If

        'Formato
        dgvLista.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        txtCantidad.SetValue(dgvLista.RowCount)
        ListarUsuarios()

    End Sub

    Sub ListarUsuarios()

        ''Quitar todos los seleccionados
        'For Each oRow As DataGridViewRow In dgvUsuario.Rows
        '    oRow.DefaultCellStyle.BackColor = Color.White
        'Next

        If dgvLista.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim IDGrupo As Integer = dgvLista.SelectedRows(0).Cells("ID").Value
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VGrupoUsuario Where IDGrupo=" & IDGrupo)

        For Each oRow As DataRow In dt.Rows
            For Each UsuarioRow As DataGridViewRow In dgvUsuario.Rows
                If UsuarioRow.Cells("ID").Value = oRow("IDUsuario") Then
                    UsuarioRow.DefaultCellStyle.BackColor = Color.LightGreen
                Else
                    UsuarioRow.DefaultCellStyle.BackColor = Color.White
                End If
            Next
        Next

        dgvUsuario.Refresh()

    End Sub

    Sub ActualizarGrupoUsuario(ByVal Insertar As Boolean)

        'Validar
        If dgvLista.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        If dgvUsuario.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim IDUsuario As Integer = dgvUsuario.SelectedRows(0).Cells("ID").Value
        Dim IDGrupo As Integer = dgvLista.SelectedRows(0).Cells("ID").Value

        Dim SQL As String = "Exec SpActualizarGrupoUsuario "
        CSistema.ConcatenarParametro(SQL, "@IDUsuario", IDUsuario)
        CSistema.ConcatenarParametro(SQL, "@IDGrupo", IDGrupo)
        CSistema.ConcatenarParametro(SQL, "@Activar", Insertar)

        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)


        If dt Is Nothing Then
            MessageBox.Show("Error en el sistema! No se proceso.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            MessageBox.Show("No se proceso.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Dim Resultado As DataRow = dt.Rows(0)

        If Resultado("Procesado") = True Then
            ListarUsuarios()
        End If

    End Sub

    Sub InicializarControles()

        'TextBox
        txtDescripcion.txt.Clear()

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Grupo"), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Grupo"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        txtDescripcion.Focus()
        ocxCuenta.txtCodigo.Clear()
        ocxCuenta.txtDescripcion.Clear()

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Nombre
        If txtDescripcion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar un nombre valido!"
            CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'Escritura de la Descripcion
        If ocxCuenta.txtCodigo.txt.Text = "" Then
            Dim mensaje As String = "Debe ingresar una Cuenta Contable valida!"
            ctrError.SetError(ocxCuenta.txtCodigo, mensaje)
            ctrError.SetIconAlignment(ocxCuenta.txtCodigo, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgvLista.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgvLista, mensaje)
                ctrError.SetIconAlignment(dgvLista, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCuentaContable", ocxCuenta.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@SoloVales", chkSoloVales.chk.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpGrupo", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            Listar(txtID.txt.Text)
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno

            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        txtDescripcion.Focus()
        txtDescripcion.txt.SelectAll()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub frmGrupo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub dgvLista_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLista.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub HabilitarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HabilitarToolStripMenuItem.Click
        ActualizarGrupoUsuario(True)
    End Sub

    Private Sub DeshabilitarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeshabilitarToolStripMenuItem.Click
        ActualizarGrupoUsuario(False)
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmGrupo_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmRendicionFondoFijo

    'CLASES
    Dim CSistema As New CSistema
    Public CData As New CData
    'VARIABLES
    Dim vNuevo As Boolean
    Dim vNuevoPE As Boolean
    Dim vControles() As Control
    Dim dtFondoFijo As DataTable
    Dim dtDetalleFondoFijo As DataTable
    Dim dtGrupo As DataTable

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private NuevoIDTransaccionValue As Integer

    Public Property NuevoIDTransaccion() As Integer
        Get
            Return NuevoIDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            NuevoIDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property
    Private IDTransaccionOPValue As Integer
    Public Property IDTransaccionOP() As Integer
        Get
            Return IDTransaccionOPValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionOPValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Variables
        vNuevo = False
        vNuevoPE = False

        'Funciones
        CargarInformacion()

        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "RENDICION", "REN")
        CSistema.InicializaControles(vControles)

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        txtID.txt.ReadOnly = False

        lklNroOP.Enabled = False

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))


    End Sub

    Sub CargarInformacion()

        'Punto de Expedicion
        ReDim vControles(-1)

        CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxGrupo)
        CSistema.CargaControl(vControles, txtOBS)
        CSistema.CargaControl(vControles, txtNroOP)
        CSistema.CargaControl(vControles, btnCargarComprobantes)
        CSistema.CargaControl(vControles, txtFecha)

        'Fondo Fijo
        dtFondoFijo = CData.GetTable("VFondoFijo")

        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, " Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2 ")

        'Grupo
        CSistema.SqlToComboBox(cbxGrupo.cbx, " Select ID, Descripcion From Grupo Order By 2 ")
        cbxGrupo.cbx.SelectedIndex = 1


    End Sub

    Sub ListarComprobantes(Optional ByVal ID As Integer = 0)

        dgw.Rows.Clear()

        For Each oRow As DataRow In dtDetalleFondoFijo.Rows
            Dim Registro(10) As String
            Registro(0) = oRow("Numero").ToString
            Registro(1) = oRow("Tipo").ToString
            Registro(2) = oRow("Fecha").ToString
            Registro(3) = oRow("NroComprobante").ToString
            Registro(4) = oRow("RazonSocial").ToString
            Registro(5) = CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(6) = oRow("IDTransaccionGasto").ToString
            Registro(7) = oRow("IDTransaccionVale").ToString
            Registro(8) = oRow("IDTransaccionRendicionFondoFijo").ToString
            Registro(9) = oRow("IDSucursal").ToString
            Registro(10) = oRow("IDGrupo").ToString
            dgw.Rows.Add(Registro)
        Next

    End Sub

    Sub CargarComprobantes()

        'Cargar Tabla
        dtDetalleFondoFijo = CSistema.ExecuteToDataTable("Select Numero,Tipo,Fecha,NroComprobante,RazonSocial,Total,IDTransaccionGasto,IDTransaccionVale,IDTransaccionRendicionFondoFijo,IDSucursal,IDGrupo  From VComprobantesRendicion Where IDTransaccionOrdenPago= " & IDTransaccionOP)

        'Listar
        ListarComprobantes()

        Calculartotales()

        btnGuardar.Focus()
    End Sub


    Sub InicializarControles()

        ObtenerInformacionFondoFijo()
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From RendicionFondoFijo),1)"), Integer)
        txtID.txt.ReadOnly = True

        'TextBox
        CSistema.InicializaControles(vControles)
        txtMonto.txt.ReadOnly = True
        'Funciones

        'Error
        ctrError.Clear()
        ctrError.Clear()
        tsslEstado.Text = ""
        'Foco
        cbxCiudad.cbx.Focus()

        txtReponer.txt.Clear()
        txtSaldo.txt.Clear()
        txtCantidadComprobantes.txt.Clear()

    End Sub

    Sub LimpiarControles()
        txtFecha.Clear()
        txtFechaOP.Clear()
    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDSucursalFF", cbxSucursal.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDGrupo", cbxGrupo.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", txtFecha.GetValueString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtOBS.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTransaccionOrdenPago", IDTransaccionOP, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Monto", txtMonto.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Reponer", txtReponer.ObtenerValor, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        IndiceOperacion = param.GetLength(0) - 1


        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpRendicionFondoFijo", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            Exit Sub

        End If

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

    End Sub

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)
         tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpRendicionFondoFijo", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            Exit Sub

        End If

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)
    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If dgw.SelectedRows.Count = 0 Then
            ctrError.SetError(dgw, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim IDSucursal As Integer
        Dim IDGrupo As Integer
        IDSucursal = dgw.SelectedRows(0).Cells("colIDSucursal").Value
        IDGrupo = dgw.SelectedRows(0).Cells("colIDGrupo").Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select Sucursal,Grupo,Tope,TotalRendir,Saldo,IDSucursal,IDGrupo From VFondoFijo Where IDSucursal=" & IDSucursal & " and IDGrupo=" & IDGrupo)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            cbxGrupo.cbx.Text = oRow("Grupo").ToString
            cbxSucursal.cbx.Text = oRow("Sucursal").ToString
            txtTope.txt.Text = oRow("Tope").ToString

            'Configuramos los controles ABM como EDITAR
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        End If

        ctrError.Clear()

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VRendicionFondoFijo Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And IDGrupo=" & cbxGrupo.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtNroOP, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VRendicionFondoFijo RFF Join FondoFijo FF on RFF.IDSucursal=FF.IDSucursal and RFF.IDGrupo=FF.IDGrupo Where Numero =" & txtID.txt.Text)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxGrupo.cbx.Text = oRow("Grupo").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        txtNroOP.txt.Text = oRow("NroOP").ToString
        txtMonto.txt.Text = oRow("Total").ToString
        txtOBSOP.txt.Text = oRow("ObservacionOP").ToString
        txtFechaOP.txt.Text = oRow("FechaOP").ToString
        txtTope.txt.Text = oRow("Tope").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString


        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
            IDTransaccionOP = CInt(oRow("IDTransaccionOrdenPago").ToString)
        End If

        Listar()

    End Sub

    Sub ObtenerOP(Optional ByVal vIDTransaccion As Integer = 0)

        'Validar
        'Obtener el ID Registro
        Dim ID As Decimal

        If IsNumeric(txtNroOP.txt.Text) = False Then
            Exit Sub
        End If

        If CInt(txtNroOP.txt.Text) = 0 Then
            Exit Sub
        End If

        ID = CDec(txtNroOP.txt.Text)

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VOrdenPago Where IDTransaccion =" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count = 0 Then

            CSistema.InicializaControles(Me)

            ctrError.SetError(txtNroOP, "No se encontro ningun registro!")
            ctrError.SetIconAlignment(txtNroOP, ErrorIconAlignment.TopRight)

            txtNroOP.txt.Text = ID
            txtNroOP.txt.SelectAll()

            Exit Sub

        End If

        'Cargamos la fila "0" en un nuevo objeto DATAROW
        Dim oRow As DataRow
        oRow = dt.Rows(0)

        txtNroOP.txt.Text = oRow("Numero").ToString
        txtMonto.txt.Text = oRow("Total").ToString
        txtOBSOP.txt.Text = oRow("Observacion").ToString
        txtFechaOP.txt.Text = oRow("Fecha").ToString
        IDTransaccionOP = oRow("IDTransaccion").ToString
       

        ctrError.Clear()

        btnCargarComprobantes.Focus()

    End Sub

    Sub ObtenerInformacionOP()

        'Validar
        'Obtener el ID Registro
        Dim ID As String

        If IsNumeric(txtNroOP.txt.Text) = False Then
            Exit Sub
        End If

        If CInt(txtNroOP.txt.Text) = 0 Then
            Exit Sub
        End If

        ID = txtNroOP.txt.Text

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VOrdenPago Where Numero =" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count = 0 Then

            CSistema.InicializaControles(Me)

            ctrError.SetError(txtNroOP, "No se encontro ningun registro!")
            ctrError.SetIconAlignment(txtNroOP, ErrorIconAlignment.TopRight)

            txtNroOP.txt.Text = ID
            txtNroOP.txt.SelectAll()

            Exit Sub

        End If

        Dim oRow As DataRow
        oRow = dt.Rows(0)

        txtMonto.txt.Text = oRow("Total").ToString
        txtOBSOP.txt.Text = oRow("Observacion").ToString
        txtFechaOP.txt.Text = oRow("Fecha").ToString
        IDTransaccionOP = oRow("IDTransaccion").ToString


        ctrError.Clear()

    End Sub

    Sub ObtenerInformacionFondoFijo()

        txtTope.SetValue(0)

        'Validar
        'Sucursal
        If cbxSucursal.Validar() = False Then
            Exit Sub
        End If

        If cbxGrupo.Validar = False Then
            Exit Sub
        End If

        If Not dtFondoFijo Is Nothing Then
            For Each oRow As DataRow In dtFondoFijo.Select(" IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And IDGrupo=" & cbxGrupo.cbx.SelectedValue & " ")
                txtTope.txt.Text = oRow("Tope").ToString
                Exit For
            Next
        End If

    End Sub

    Sub Nuevo()

        'Comportamientos
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO)

        'Variables
        vNuevo = True
        lklNroOP.Enabled = True

        'Funciones
        InicializarControles()

        dgw.Rows.Clear()
        'Foco
        txtMonto.txt.Clear()
        cbxSucursal.Focus()
        txtFecha.Clear()
        txtFechaOP.Clear()
        txtTope.txt.Clear()
        txtOBSOP.Clear()

        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

    End Sub

    Sub Editar()
        'Establecemos los botones a Editando
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        vNuevo = False

        'Foco
        txtID.Focus()
        txtID.txt.SelectAll()
    End Sub

    Sub Guardar()
        Procesar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Sub Anular()
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)
    End Sub
    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        vNuevo = False
        txtID.txt.ReadOnly = False
        lklNroOP.Enabled = False

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.Focus()
    End Sub

    Sub CalcularTotales()

        Dim TotalComprobantes As Decimal = 0
        Dim CantidadComprobantes As Decimal = 0
        Dim Saldo As Decimal = 0
        For Each Item As DataGridViewRow In dgw.Rows
            TotalComprobantes = TotalComprobantes + CDbl(Item.Cells("colTotal").Value)
            CantidadComprobantes = CantidadComprobantes + 1
            Saldo = txtTope.txt.Text - CDbl(TotalComprobantes)
        Next

        txtReponer.SetValue(TotalComprobantes)
        txtSaldo.SetValue(Saldo)
        txtCantidadComprobantes.SetValue(CantidadComprobantes)

    End Sub


    Sub Listar()

        'Cargar Tabla
        dtDetalleFondoFijo = CSistema.ExecuteToDataTable("Select Numero,Tipo,Fecha,NroComprobante,RazonSocial,Total,IDTransaccionGasto,IDTransaccionVale,IDTransaccionRendicionFondoFijo,IDSucursal,IDGrupo  From VComprobantesRendicion Where NroRendicion = " & txtID.txt.Text)


        dgw.Rows.Clear()

        For Each oRow As DataRow In dtDetalleFondoFijo.Rows
            Dim Registro(10) As String
            Registro(0) = oRow("Numero").ToString
            Registro(1) = oRow("Tipo").ToString
            Registro(2) = oRow("Fecha").ToString
            Registro(3) = oRow("NroComprobante").ToString
            Registro(4) = oRow("RazonSocial").ToString
            Registro(5) = CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(6) = oRow("IDTransaccionGasto").ToString
            Registro(7) = oRow("IDTransaccionVale").ToString
            Registro(8) = oRow("IDTransaccionRendicionFondoFijo").ToString
            Registro(9) = oRow("IDSucursal").ToString
            Registro(10) = oRow("IDGrupo").ToString
            dgw.Rows.Add(Registro)
        Next


        CalcularTotales()

    End Sub

    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'Otros
        Dim frm As New frmConsultaRendicion
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog(Me)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VRendicionFondoFijo Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VDepositoBancario Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If
    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        If dtDetalleFondoFijo.Rows.Count = 0 Then
            Dim mensaje As String = "La OP no contiene comprobantes de fondo fijo!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtNroOP, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Atención! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If

        End If


        Return True
    End Function

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)

    End Sub

    Sub Eliminar()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Sub ObtenerSucursal()
        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)
    End Sub

    Private Sub frmRendicionFondoFijo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Editar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Eliminar()
    End Sub


    Private Sub txtNroOP_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroOP.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ObtenerInformacionOP()
        End If
    End Sub
    Private Sub cbxCiudad_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged
        ObtenerSucursal()
    End Sub


    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        ObtenerInformacionFondoFijo()
    End Sub

    Private Sub cbxGrupo_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxGrupo.PropertyChanged
        ObtenerInformacionFondoFijo()
    End Sub

    Private Sub frmRendicionFondoFijo_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub btnCargarComprobantes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCargarComprobantes.Click
        CargarComprobantes()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub lklNroOP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lklNroOP.Click
        Dim frm As New frmConsultaOrdenPago
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog()
        txtNroOP.txt.Text = frm.IDTransaccion
        ObtenerOP(frm.IDTransaccion)
        btnCargarComprobantes.Focus()
    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

        If e.ColumnIndex = dgw.Columns.Item("colSel").Index Then

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSel").Value = False Then
                        oRow.Cells("colSel").Value = True
                        oRow.Cells("colSel").ReadOnly = False
                    Else
                        oRow.Cells("colSel").ReadOnly = True
                        oRow.Cells("colSel").Value = False
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

        End If

    End Sub

    Private Sub cbxGrupo_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxGrupo.TeclaPrecionada
        ObtenerInformacionFondoFijo()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmRendicionFondoFijo_Activate()
        Me.Refresh()
    End Sub
End Class
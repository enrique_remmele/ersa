﻿Public Class frmFondoFijo

    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vNuevoPE As Boolean
    Dim vControles() As Control
    Dim dtPuntoExpedicion As DataTable
    Dim dtGrupo As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Variables
        vNuevo = False
        vNuevoPE = False

        'Funciones
        CargarInformacion()

        'Controles
        CSistema.InicializaControles(TabControl1.TabPages(0))

        'Cargamos los registos en el lv
        Listar()

        'Botones
        EstablecerBotones(CSistema.NUMHabilitacionBotonesABM.INICIO)

        'Focus
        cbxSucursal.Focus()

    End Sub


    Sub CargarInformacion()

        'Punto de Expedicion
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxGrupo)
        CSistema.CargaControl(vControles, cbxSucursal)

        'Terminales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Descripcion From Sucursal Order By 2")

        'Operaciones
        CSistema.SqlToComboBox(cbxGrupo.cbx, "Select ID, Descripcion From Grupo Order By 2")

    End Sub



    Sub Listar(Optional ByVal ID As Integer = 0)

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        CSistema.SqlToLv(lvLista, "Select Sucursal,Grupo,Tope,TotalRendir,Saldo,IDSucursal,IDGrupo From VFondoFijo")

        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If lvLista.Columns.Count > 0 Then

            'Esto hacemos para que: 
            '1- Que el ID sea visible en la primera columna
            '2- Y para que cuando el usuario escriba en el lv, el control filtre por su descripcion.
            lvLista.Columns(0).DisplayIndex = 1

            'Ahora seleccionamos automaticamente el registro especificado
            If lvLista.Items.Count > 0 Then
                If ID = 0 Then

                    lvLista.Items(0).Selected = True
                Else
                    For i As Integer = 0 To lvLista.Items.Count - 1
                        If lvLista.Items(i).SubItems(1).Text = ID Then
                            lvLista.Items(i).Selected = True
                            Exit For
                        End If
                    Next

                End If
            End If


        End If


        lvLista.Refresh()

    End Sub

    Sub InicializarControles()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO)

        txtTope.txt.Clear()

        'TextBox
        CSistema.InicializaControles(vControles)

        'Funciones

        'Error
        ctrError.Clear()

        'Foco
        cbxSucursal.cbx.Focus()

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDGrupo", cbxGrupo.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Tope", CSistema.FormatoMonedaBaseDatos(txtTope.txt.Text), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpFondoFijo", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            Listar()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If lvLista.SelectedItems.Count = 0 Then
            ctrError.SetError(lvLista, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim IDSucursal As Integer
        Dim IDGrupo As Integer
        IDSucursal = lvLista.SelectedItems(0).SubItems(5).Text
        IDGrupo = lvLista.SelectedItems(0).SubItems(6).Text

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select Sucursal,Grupo,Tope,TotalRendir,Saldo,IDSucursal,IDGrupo From VFondoFijo Where IDSucursal=" & IDSucursal & " and IDGrupo=" & IDGrupo)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            cbxGrupo.cbx.Text = oRow("Grupo").ToString
            cbxSucursal.cbx.Text = oRow("Sucursal").ToString
            txtTope.txt.Text = oRow("Tope").ToString

            'Configuramos los controles ABM como EDITAR
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR)

        End If

        ctrError.Clear()

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub Nuevo()
        vNuevo = True
        InicializarControles()
    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesABM(Operacion, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

    End Sub

    Sub Editar()
        'Establecemos los botones a Editando
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO)
        vNuevo = False

        'Foco
        txtTope.Focus()
        txtTope.txt.SelectAll()
    End Sub

    Sub Eliminar()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.GUARDAR)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub frmFondoFijo_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmGrupos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Editar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

    Private Sub lvLista_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvLista.Click
        ObtenerInformacion()
    End Sub

    Private Sub lvLista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvLista.SelectedIndexChanged
        ObtenerInformacion()
    End Sub

    Private Sub cbxSucursal_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxSucursal.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmFondoFijo_Activate()
        Me.Refresh()
    End Sub
End Class
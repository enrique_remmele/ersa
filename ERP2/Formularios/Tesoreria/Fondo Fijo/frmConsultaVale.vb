﻿Public Class frmConsultaVale

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim dtOperacion As DataTable

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'Form

        'TextBox

        txtCantidadOperacion.txt.ResetText()

        txtOperacion.txt.ResetText()

        txtTotalOperacion.txt.ResetText()

        'CheckBox

        chkUsuario.Checked = False
        chkSucursal.Checked = False

        'ComboBox

        cbxUsuario.Enabled = False
        cbxSucursal.Enabled = False

        'RadioButton

        'ListView

        dgwOperacion.Rows.Clear()

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        CargarInformacion()


    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'Tipo de Comprobante

        'Usuarios
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxUsuario, "Select ID, Usuario From Usuario Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

        'Usuario
        chkUsuario.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "USUARIO ACTIVO", "False")
        cbxUsuario.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "USUARIO", "")

        'Foco
        txtOperacion.txt.Focus()
        txtOperacion.txt.SelectAll()

    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCRUSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)



        'Usuario
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "USUARIO ACTIVO", chkUsuario.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "USUARIO", cbxUsuario.Text)

    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Operaciones
    Sub ListarOperaciones(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "")

        ctrError.Clear()

        Where = Condicion

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la Sucursal!") = False Then
            Exit Sub
        End If


        'Usuario
        If EstablecerCondicion(cbxUsuario, chkUsuario, "IDUsuario", "Seleccione correctamente el usuario!") = False Then
            Exit Sub
        End If

        'Solo por numero
        If Numero > 0 Then
            Where = " Where Numero=" & Numero & " "
        End If

        dgwOperacion.Rows.Clear()
        Dim Total As Decimal = 0
        Dim Cantidad As Decimal = 0
        Consulta = "Select IDTransaccion,Numero,Sucursal,IDSucursal,Fec,Motivo,Nombre,Total,Estado,IDUsuario From VVale " & Where

        dtOperacion = CSistema.ExecuteToDataTable(Consulta)


        For Each oRow As DataRow In dtOperacion.Rows
            Dim Registro(9) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Numero").ToString
            Registro(2) = oRow("Sucursal").ToString
            Registro(3) = oRow("IDSucursal").ToString
            Registro(4) = oRow("Fec").ToString
            Registro(5) = oRow("Motivo").ToString
            Registro(6) = oRow("Nombre").ToString
            Registro(7) = CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(8) = oRow("Estado").ToString
            Registro(9) = oRow("IDUsuario").ToString
            Total = Total + CDec(oRow("Total").ToString)
            Cantidad = Cantidad + 1

            dgwOperacion.Rows.Add(Registro)
        Next
        txtTotalOperacion.SetValue(Total)
        txtCantidadOperacion.SetValue(Cantidad)

    End Sub

    'Listar Detalle
    Sub ListarDetalle()

        ctrError.Clear()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells(10).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If


    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = CInt(dgwOperacion.SelectedRows(0).Cells(0).Value)

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub btnRegistrosDelDia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosDelDia.Click
        Where = " Where (DateDiff(dd, Fecha, GetDate())=0) "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub frmConsultaCompra_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaCompra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub dgwOperacion_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgwOperacion.DoubleClick
        SeleccionarRegistro()
    End Sub

    Private Sub chkUsuario_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUsuario.CheckedChanged
        HabilitarControles(chkUsuario, cbxUsuario)
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub

    Private Sub btnRegistrosDelMes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosDelMes.Click
        Where = " Where (DateDiff(MM, Fecha, GetDate())=0) "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub btnRegistrosGenerales_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosGenerales.Click
        ListarOperaciones()
    End Sub

    Private Sub dtbRegistrosPorFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtbRegistrosPorFecha.Click
        Where = " Where (Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "') "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarOperaciones(txtOperacion.ObtenerValor)
            txtOperacion.txt.Focus()
            txtOperacion.txt.SelectAll()
        End If
    End Sub

    Private Sub dgwOperacion_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgwOperacion.KeyUp
        If e.KeyCode = Keys.Enter Then
            SeleccionarRegistro()
        End If

    End Sub

    Private Sub frmConsultaMovimiento_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmConsultaVale_Activate()
        Me.Refresh()
    End Sub

End Class


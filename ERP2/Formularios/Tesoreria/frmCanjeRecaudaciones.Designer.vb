﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCanjeRecaudaciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblObsrevacion = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.gbxComprobantes = New System.Windows.Forms.GroupBox()
        Me.lblTotalDepositado = New System.Windows.Forms.Label()
        Me.btnAgregarTarjetas = New System.Windows.Forms.Button()
        Me.btnAgregarDocumentos = New System.Windows.Forms.Button()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBanco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNroComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCotizacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCuentaContable = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnAgregarCheques = New System.Windows.Forms.Button()
        Me.lklEliminarVenta = New System.Windows.Forms.LinkLabel()
        Me.btnAgregarEfectivo = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lblSaldoTotal = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.txtSaldoTotal = New ERP.ocxTXTNumeric()
        Me.OcxFormaPago1 = New ERP.ocxFormaPagoLote()
        Me.txtTotalRecaudaciones = New ERP.ocxTXTNumeric()
        Me.txtMoneda = New ERP.ocxCotizacion()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtfecha = New ERP.ocxTXTDate()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.GroupBox1.SuspendLayout()
        Me.gbxComprobantes.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.flpRegistradoPor.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblMoneda)
        Me.GroupBox1.Controls.Add(Me.txtMoneda)
        Me.GroupBox1.Controls.Add(Me.txtID)
        Me.GroupBox1.Controls.Add(Me.txtfecha)
        Me.GroupBox1.Controls.Add(Me.lblFecha)
        Me.GroupBox1.Controls.Add(Me.txtObservacion)
        Me.GroupBox1.Controls.Add(Me.lblObsrevacion)
        Me.GroupBox1.Controls.Add(Me.cbxSucursal)
        Me.GroupBox1.Controls.Add(Me.lblOperacion)
        Me.GroupBox1.Location = New System.Drawing.Point(2, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(767, 69)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(360, 20)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 40
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(226, 20)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 8
        Me.lblFecha.Text = "Fecha:"
        '
        'lblObsrevacion
        '
        Me.lblObsrevacion.AutoSize = True
        Me.lblObsrevacion.Location = New System.Drawing.Point(8, 47)
        Me.lblObsrevacion.Name = "lblObsrevacion"
        Me.lblObsrevacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObsrevacion.TabIndex = 16
        Me.lblObsrevacion.Text = "Observacion:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(9, 20)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'gbxComprobantes
        '
        Me.gbxComprobantes.Controls.Add(Me.txtTotalRecaudaciones)
        Me.gbxComprobantes.Controls.Add(Me.lblTotalDepositado)
        Me.gbxComprobantes.Controls.Add(Me.btnAgregarTarjetas)
        Me.gbxComprobantes.Controls.Add(Me.btnAgregarDocumentos)
        Me.gbxComprobantes.Controls.Add(Me.dgw)
        Me.gbxComprobantes.Controls.Add(Me.btnAgregarCheques)
        Me.gbxComprobantes.Controls.Add(Me.lklEliminarVenta)
        Me.gbxComprobantes.Controls.Add(Me.btnAgregarEfectivo)
        Me.gbxComprobantes.Location = New System.Drawing.Point(2, 71)
        Me.gbxComprobantes.Name = "gbxComprobantes"
        Me.gbxComprobantes.Size = New System.Drawing.Size(767, 209)
        Me.gbxComprobantes.TabIndex = 2
        Me.gbxComprobantes.TabStop = False
        '
        'lblTotalDepositado
        '
        Me.lblTotalDepositado.AutoSize = True
        Me.lblTotalDepositado.Location = New System.Drawing.Point(499, 176)
        Me.lblTotalDepositado.Name = "lblTotalDepositado"
        Me.lblTotalDepositado.Size = New System.Drawing.Size(101, 13)
        Me.lblTotalDepositado.TabIndex = 13
        Me.lblTotalDepositado.Text = "Total Recaudacion:"
        '
        'btnAgregarTarjetas
        '
        Me.btnAgregarTarjetas.Location = New System.Drawing.Point(258, 11)
        Me.btnAgregarTarjetas.Name = "btnAgregarTarjetas"
        Me.btnAgregarTarjetas.Size = New System.Drawing.Size(77, 21)
        Me.btnAgregarTarjetas.TabIndex = 3
        Me.btnAgregarTarjetas.Text = "Tarjetas"
        Me.btnAgregarTarjetas.UseVisualStyleBackColor = True
        '
        'btnAgregarDocumentos
        '
        Me.btnAgregarDocumentos.Location = New System.Drawing.Point(175, 11)
        Me.btnAgregarDocumentos.Name = "btnAgregarDocumentos"
        Me.btnAgregarDocumentos.Size = New System.Drawing.Size(77, 21)
        Me.btnAgregarDocumentos.TabIndex = 2
        Me.btnAgregarDocumentos.Text = "Documentos"
        Me.btnAgregarDocumentos.UseVisualStyleBackColor = True
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        Me.dgw.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colTipo, Me.colBanco, Me.colNroComprobante, Me.colImporte, Me.colCotizacion, Me.colCliente, Me.colCuentaContable, Me.colID})
        Me.dgw.Location = New System.Drawing.Point(10, 38)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(751, 127)
        Me.dgw.TabIndex = 4
        Me.dgw.TabStop = False
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        Me.colIDTransaccion.Width = 5
        '
        'colTipo
        '
        Me.colTipo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colTipo.DefaultCellStyle = DataGridViewCellStyle2
        Me.colTipo.HeaderText = "Tipo"
        Me.colTipo.Name = "colTipo"
        Me.colTipo.ReadOnly = True
        '
        'colBanco
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colBanco.DefaultCellStyle = DataGridViewCellStyle3
        Me.colBanco.HeaderText = "Banco"
        Me.colBanco.Name = "colBanco"
        Me.colBanco.ReadOnly = True
        '
        'colNroComprobante
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colNroComprobante.DefaultCellStyle = DataGridViewCellStyle4
        Me.colNroComprobante.HeaderText = "NroComprobante"
        Me.colNroComprobante.Name = "colNroComprobante"
        Me.colNroComprobante.ReadOnly = True
        '
        'colImporte
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle5
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        '
        'colCotizacion
        '
        Me.colCotizacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colCotizacion.DefaultCellStyle = DataGridViewCellStyle6
        Me.colCotizacion.HeaderText = "Cotizacion"
        Me.colCotizacion.Name = "colCotizacion"
        Me.colCotizacion.ReadOnly = True
        Me.colCotizacion.Width = 81
        '
        'colCliente
        '
        Me.colCliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.Format = "N0"
        DataGridViewCellStyle7.NullValue = "0"
        Me.colCliente.DefaultCellStyle = DataGridViewCellStyle7
        Me.colCliente.HeaderText = "Cliente"
        Me.colCliente.Name = "colCliente"
        Me.colCliente.ReadOnly = True
        '
        'colCuentaContable
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colCuentaContable.DefaultCellStyle = DataGridViewCellStyle8
        Me.colCuentaContable.HeaderText = "Cuenta Contable"
        Me.colCuentaContable.Name = "colCuentaContable"
        Me.colCuentaContable.ReadOnly = True
        Me.colCuentaContable.Width = 150
        '
        'colID
        '
        Me.colID.HeaderText = "ID"
        Me.colID.Name = "colID"
        Me.colID.ReadOnly = True
        Me.colID.Visible = False
        Me.colID.Width = 5
        '
        'btnAgregarCheques
        '
        Me.btnAgregarCheques.Location = New System.Drawing.Point(92, 11)
        Me.btnAgregarCheques.Name = "btnAgregarCheques"
        Me.btnAgregarCheques.Size = New System.Drawing.Size(77, 21)
        Me.btnAgregarCheques.TabIndex = 1
        Me.btnAgregarCheques.Text = "Cheques"
        Me.btnAgregarCheques.UseVisualStyleBackColor = True
        '
        'lklEliminarVenta
        '
        Me.lklEliminarVenta.AutoSize = True
        Me.lklEliminarVenta.Location = New System.Drawing.Point(11, 170)
        Me.lklEliminarVenta.Name = "lklEliminarVenta"
        Me.lklEliminarVenta.Size = New System.Drawing.Size(108, 13)
        Me.lklEliminarVenta.TabIndex = 5
        Me.lklEliminarVenta.TabStop = True
        Me.lklEliminarVenta.Text = "Eliminar comprobante"
        '
        'btnAgregarEfectivo
        '
        Me.btnAgregarEfectivo.Location = New System.Drawing.Point(9, 11)
        Me.btnAgregarEfectivo.Name = "btnAgregarEfectivo"
        Me.btnAgregarEfectivo.Size = New System.Drawing.Size(77, 21)
        Me.btnAgregarEfectivo.TabIndex = 0
        Me.btnAgregarEfectivo.Text = "Efectivo"
        Me.btnAgregarEfectivo.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(6, 530)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 26
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(365, 530)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 29
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(689, 530)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 33
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(608, 530)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 32
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(527, 530)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 31
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(446, 530)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 30
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lblSaldoTotal
        '
        Me.lblSaldoTotal.AutoSize = True
        Me.lblSaldoTotal.Location = New System.Drawing.Point(574, 503)
        Me.lblSaldoTotal.Name = "lblSaldoTotal"
        Me.lblSaldoTotal.Size = New System.Drawing.Size(37, 13)
        Me.lblSaldoTotal.TabIndex = 35
        Me.lblSaldoTotal.Text = "Saldo:"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(6, 482)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(277, 37)
        Me.flpRegistradoPor.TabIndex = 37
        Me.flpRegistradoPor.Visible = False
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(286, 482)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(277, 37)
        Me.flpAnuladoPor.TabIndex = 38
        Me.flpAnuladoPor.Visible = False
        '
        'lblAnulado
        '
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Red
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(89, 20)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(98, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(150, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'txtSaldoTotal
        '
        Me.txtSaldoTotal.Color = System.Drawing.Color.Empty
        Me.txtSaldoTotal.Decimales = True
        Me.txtSaldoTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldoTotal.Indicaciones = Nothing
        Me.txtSaldoTotal.Location = New System.Drawing.Point(617, 498)
        Me.txtSaldoTotal.Name = "txtSaldoTotal"
        Me.txtSaldoTotal.Size = New System.Drawing.Size(147, 22)
        Me.txtSaldoTotal.SoloLectura = True
        Me.txtSaldoTotal.TabIndex = 36
        Me.txtSaldoTotal.TabStop = False
        Me.txtSaldoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoTotal.Texto = "0"
        '
        'OcxFormaPago1
        '
        Me.OcxFormaPago1.Comprobante = Nothing
        Me.OcxFormaPago1.dtCheques = Nothing
        Me.OcxFormaPago1.dtChequesTercero = Nothing
        Me.OcxFormaPago1.dtDocumento = Nothing
        Me.OcxFormaPago1.dtEfectivo = Nothing
        Me.OcxFormaPago1.dtFormaPago = Nothing
        Me.OcxFormaPago1.dtVentaRetencion = Nothing
        Me.OcxFormaPago1.Fecha = New Date(CType(0, Long))
        Me.OcxFormaPago1.Form = Nothing
        Me.OcxFormaPago1.Location = New System.Drawing.Point(2, 282)
        Me.OcxFormaPago1.Name = "OcxFormaPago1"
        Me.OcxFormaPago1.Restar = False
        Me.OcxFormaPago1.Saldo = New Decimal(New Integer() {0, 0, 0, 0})
        Me.OcxFormaPago1.Size = New System.Drawing.Size(767, 211)
        Me.OcxFormaPago1.SoloLectura = False
        Me.OcxFormaPago1.TabIndex = 34
        Me.OcxFormaPago1.Tipo = Nothing
        Me.OcxFormaPago1.Total = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'txtTotalRecaudaciones
        '
        Me.txtTotalRecaudaciones.Color = System.Drawing.Color.Empty
        Me.txtTotalRecaudaciones.Decimales = True
        Me.txtTotalRecaudaciones.Indicaciones = Nothing
        Me.txtTotalRecaudaciones.Location = New System.Drawing.Point(607, 171)
        Me.txtTotalRecaudaciones.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalRecaudaciones.Name = "txtTotalRecaudaciones"
        Me.txtTotalRecaudaciones.Size = New System.Drawing.Size(153, 22)
        Me.txtTotalRecaudaciones.SoloLectura = True
        Me.txtTotalRecaudaciones.TabIndex = 14
        Me.txtTotalRecaudaciones.TabStop = False
        Me.txtTotalRecaudaciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalRecaudaciones.Texto = "0"
        '
        'txtMoneda
        '
        Me.txtMoneda.dt = Nothing
        Me.txtMoneda.Enabled = False
        Me.txtMoneda.FiltroFecha = Nothing
        Me.txtMoneda.Location = New System.Drawing.Point(415, 12)
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.Registro = Nothing
        Me.txtMoneda.Saltar = False
        Me.txtMoneda.Seleccionado = True
        Me.txtMoneda.Size = New System.Drawing.Size(148, 28)
        Me.txtMoneda.SoloLectura = False
        Me.txtMoneda.TabIndex = 39
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(141, 16)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(65, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtfecha
        '
        Me.txtfecha.AñoFecha = 0
        Me.txtfecha.Color = System.Drawing.Color.Empty
        Me.txtfecha.Fecha = New Date(CType(0, Long))
        Me.txtfecha.Location = New System.Drawing.Point(266, 16)
        Me.txtfecha.Margin = New System.Windows.Forms.Padding(4)
        Me.txtfecha.MesFecha = 0
        Me.txtfecha.Name = "txtfecha"
        Me.txtfecha.PermitirNulo = False
        Me.txtfecha.Size = New System.Drawing.Size(69, 20)
        Me.txtfecha.SoloLectura = False
        Me.txtfecha.TabIndex = 9
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(83, 43)
        Me.txtObservacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(677, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 17
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(83, 16)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(58, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'frmCanjeRecaudaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(771, 568)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.flpAnuladoPor)
        Me.Controls.Add(Me.lblSaldoTotal)
        Me.Controls.Add(Me.txtSaldoTotal)
        Me.Controls.Add(Me.OcxFormaPago1)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.gbxComprobantes)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmCanjeRecaudaciones"
        Me.Text = "frmCanjeRecaudaciones"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gbxComprobantes.ResumeLayout(False)
        Me.gbxComprobantes.PerformLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtID As ocxTXTNumeric
    Friend WithEvents txtfecha As ocxTXTDate
    Friend WithEvents lblFecha As Label
    Friend WithEvents txtObservacion As ocxTXTString
    Friend WithEvents lblObsrevacion As Label
    Friend WithEvents cbxSucursal As ocxCBX
    Friend WithEvents lblOperacion As Label
    Friend WithEvents gbxComprobantes As GroupBox
    Friend WithEvents btnAgregarTarjetas As Button
    Friend WithEvents btnAgregarDocumentos As Button
    Friend WithEvents dgw As DataGridView
    Friend WithEvents colIDTransaccion As DataGridViewTextBoxColumn
    Friend WithEvents colTipo As DataGridViewTextBoxColumn
    Friend WithEvents colBanco As DataGridViewTextBoxColumn
    Friend WithEvents colNroComprobante As DataGridViewTextBoxColumn
    Friend WithEvents colImporte As DataGridViewTextBoxColumn
    Friend WithEvents colCotizacion As DataGridViewTextBoxColumn
    Friend WithEvents colCliente As DataGridViewTextBoxColumn
    Friend WithEvents colCuentaContable As DataGridViewTextBoxColumn
    Friend WithEvents colID As DataGridViewTextBoxColumn
    Friend WithEvents btnAgregarCheques As Button
    Friend WithEvents lklEliminarVenta As LinkLabel
    Friend WithEvents btnAgregarEfectivo As Button
    Friend WithEvents lblMoneda As Label
    Friend WithEvents txtMoneda As ocxCotizacion
    Friend WithEvents btnAnular As Button
    Friend WithEvents btnAsiento As Button
    Friend WithEvents btnSalir As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnNuevo As Button
    Friend WithEvents OcxFormaPago1 As ocxFormaPagoLote
    Friend WithEvents txtTotalRecaudaciones As ocxTXTNumeric
    Friend WithEvents lblTotalDepositado As Label
    Friend WithEvents lblSaldoTotal As Label
    Friend WithEvents txtSaldoTotal As ocxTXTNumeric
    Friend WithEvents flpRegistradoPor As FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As Label
    Friend WithEvents lblUsuarioRegistro As Label
    Friend WithEvents lblFechaRegistro As Label
    Friend WithEvents flpAnuladoPor As FlowLayoutPanel
    Friend WithEvents lblAnulado As Label
    Friend WithEvents lblUsuarioAnulado As Label
    Friend WithEvents lblFechaAnulado As Label
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConciliaciondeMovimientoBancaria
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnSeleccionar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtFecha2 = New ERP.ocxTXTDate()
        Me.txtFecha1 = New ERP.ocxTXTDate()
        Me.txtTitular = New ERP.ocxTXTString()
        Me.txtNombre = New ERP.ocxTXTString()
        Me.txtMoneda = New ERP.ocxTXTString()
        Me.cbxCuenta = New ERP.ocxCBX()
        Me.txtBanco = New ERP.ocxTXTString()
        Me.SuspendLayout()
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(1, 20)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 0
        Me.lblCuenta.Text = "Cuenta:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(1, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Nombre:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(1, 76)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Titular(es):"
        '
        'btnSeleccionar
        '
        Me.btnSeleccionar.Location = New System.Drawing.Point(536, 40)
        Me.btnSeleccionar.Name = "btnSeleccionar"
        Me.btnSeleccionar.Size = New System.Drawing.Size(75, 23)
        Me.btnSeleccionar.TabIndex = 11
        Me.btnSeleccionar.Text = "S&eleccionar"
        Me.btnSeleccionar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(536, 66)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 12
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(1, 105)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Periodo:"
        '
        'txtFecha2
        '
        Me.txtFecha2.AñoFecha = 0
        Me.txtFecha2.Color = System.Drawing.Color.Empty
        Me.txtFecha2.Fecha = New Date(CType(0, Long))
        Me.txtFecha2.Location = New System.Drawing.Point(147, 101)
        Me.txtFecha2.MesFecha = 0
        Me.txtFecha2.Name = "txtFecha2"
        Me.txtFecha2.PermitirNulo = False
        Me.txtFecha2.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha2.SoloLectura = False
        Me.txtFecha2.TabIndex = 10
        '
        'txtFecha1
        '
        Me.txtFecha1.AñoFecha = 0
        Me.txtFecha1.Color = System.Drawing.Color.Empty
        Me.txtFecha1.Fecha = New Date(CType(0, Long))
        Me.txtFecha1.Location = New System.Drawing.Point(67, 101)
        Me.txtFecha1.MesFecha = 0
        Me.txtFecha1.Name = "txtFecha1"
        Me.txtFecha1.PermitirNulo = False
        Me.txtFecha1.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha1.SoloLectura = False
        Me.txtFecha1.TabIndex = 9
        '
        'txtTitular
        '
        Me.txtTitular.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTitular.Color = System.Drawing.Color.Empty
        Me.txtTitular.Indicaciones = Nothing
        Me.txtTitular.Location = New System.Drawing.Point(67, 73)
        Me.txtTitular.Multilinea = False
        Me.txtTitular.Name = "txtTitular"
        Me.txtTitular.Size = New System.Drawing.Size(463, 21)
        Me.txtTitular.SoloLectura = False
        Me.txtTitular.TabIndex = 7
        Me.txtTitular.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTitular.Texto = ""
        '
        'txtNombre
        '
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.Color = System.Drawing.Color.Empty
        Me.txtNombre.Indicaciones = Nothing
        Me.txtNombre.Location = New System.Drawing.Point(67, 44)
        Me.txtNombre.Multilinea = False
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(463, 21)
        Me.txtNombre.SoloLectura = False
        Me.txtNombre.TabIndex = 5
        Me.txtNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNombre.Texto = ""
        '
        'txtMoneda
        '
        Me.txtMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMoneda.Color = System.Drawing.Color.Empty
        Me.txtMoneda.Indicaciones = Nothing
        Me.txtMoneda.Location = New System.Drawing.Point(482, 16)
        Me.txtMoneda.Multilinea = False
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.Size = New System.Drawing.Size(48, 21)
        Me.txtMoneda.SoloLectura = True
        Me.txtMoneda.TabIndex = 3
        Me.txtMoneda.TabStop = False
        Me.txtMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMoneda.Texto = ""
        '
        'cbxCuenta
        '
        Me.cbxCuenta.CampoWhere = Nothing
        Me.cbxCuenta.CargarUnaSolaVez = False
        Me.cbxCuenta.DataDisplayMember = Nothing
        Me.cbxCuenta.DataFilter = Nothing
        Me.cbxCuenta.DataOrderBy = Nothing
        Me.cbxCuenta.DataSource = Nothing
        Me.cbxCuenta.DataValueMember = Nothing
        Me.cbxCuenta.dtSeleccionado = Nothing
        Me.cbxCuenta.FormABM = Nothing
        Me.cbxCuenta.Indicaciones = Nothing
        Me.cbxCuenta.Location = New System.Drawing.Point(67, 16)
        Me.cbxCuenta.Name = "cbxCuenta"
        Me.cbxCuenta.SeleccionMultiple = False
        Me.cbxCuenta.SeleccionObligatoria = False
        Me.cbxCuenta.Size = New System.Drawing.Size(255, 21)
        Me.cbxCuenta.SoloLectura = False
        Me.cbxCuenta.TabIndex = 1
        Me.cbxCuenta.Texto = ""
        '
        'txtBanco
        '
        Me.txtBanco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBanco.Color = System.Drawing.Color.Empty
        Me.txtBanco.Indicaciones = Nothing
        Me.txtBanco.Location = New System.Drawing.Point(328, 16)
        Me.txtBanco.Multilinea = False
        Me.txtBanco.Name = "txtBanco"
        Me.txtBanco.Size = New System.Drawing.Size(151, 21)
        Me.txtBanco.SoloLectura = True
        Me.txtBanco.TabIndex = 2
        Me.txtBanco.TabStop = False
        Me.txtBanco.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBanco.Texto = ""
        '
        'frmConciliaciondeMovimientoBancaria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(623, 152)
        Me.Controls.Add(Me.txtFecha2)
        Me.Controls.Add(Me.txtFecha1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnSeleccionar)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtTitular)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.txtMoneda)
        Me.Controls.Add(Me.lblCuenta)
        Me.Controls.Add(Me.cbxCuenta)
        Me.Controls.Add(Me.txtBanco)
        Me.Name = "frmConciliaciondeMovimientoBancaria"
        Me.Tag = "frmConciliaciondeMovimientoBancaria"
        Me.Text = "frmConciliaciondeMovimientoBancaria"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtMoneda As ERP.ocxTXTString
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents cbxCuenta As ERP.ocxCBX
    Friend WithEvents txtBanco As ERP.ocxTXTString
    Friend WithEvents txtNombre As ERP.ocxTXTString
    Friend WithEvents txtTitular As ERP.ocxTXTString
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnSeleccionar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtFecha1 As ERP.ocxTXTDate
    Friend WithEvents txtFecha2 As ERP.ocxTXTDate
End Class

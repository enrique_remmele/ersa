﻿Public Class frmConciliaciondeMovimientoBancaria

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    'Public Property IDOperacion() As Integer
    '    Get
    '        Return IDOperacionValue
    '    End Get
    '    Set(ByVal value As Integer)
    '        IDOperacionValue = value
    '    End Set
    'End Property

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    'Variables
    Dim vNuevo As Boolean
    Dim vControles() As Control
    Dim dtCuentaBancaria As New DataTable
    Dim dtConciliacion As New DataTable

    'FUNCIONES

    Sub Inicializar()

        'Form 
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Propiedades
        IDTransaccion = 0
        ' IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "CONSILIACION BANCARIA ", "CNB")
        vNuevo = True

        'Fecha
        Dim D As Date
        D = "01-01-" & Date.Now.Year.ToString
        txtFecha1.SetValue(D)

        Dim F As Date
        F = Date.Now.ToString
        txtFecha2.SetValue(F)

        'Funciones
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, cbxCuenta)
        CSistema.CargaControl(vControles, txtBanco)
        CSistema.CargaControl(vControles, txtMoneda)
        CSistema.CargaControl(vControles, txtNombre)
        CSistema.CargaControl(vControles, txtTitular)

        'flpRegistradoPor.Visible = False

        'Forma de Pago
        ' CSistema.CargaControl(vControles, OcxFormaPago1)

        'CARGAR ESTRUCTURA DEL DETALLE VENTA

        'CARGAR CONTROLES

        dtCuentaBancaria = CSistema.ExecuteToDataTable("Select * From VCuentaBancaria order by CuentaBancaria").Copy
        'CSistema.SqlToComboBox(cbxCuenta.cbx, dtCuentaBancaria, "ID", "CuentaBancaria")
        CSistema.SqlToComboBox(cbxCuenta.cbx, dtCuentaBancaria, "ID", "Descripcion")

        'CARGAR LA ULTIMA CONFIGURACION

        'Cuenta
        cbxCuenta.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, " CUENTA", "")

        'Banco
        txtBanco.txt.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "BANCO", "")

        'Moneda
        txtMoneda.txt.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MONEDA", "")

        ''Botones
        'CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO, btnSeleccionar, btnActualizar, btnGuardar, btnGuardar, btnSalir, New Button, New Button, vControles)

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CUENTA", cbxCuenta.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "BANCO", txtBanco.txt.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", txtMoneda.txt.Text)

    End Sub

    Sub ObtenerCuenta()

        If cbxCuenta.cbx.SelectedValue Is Nothing Then
            Exit Sub
        End If

        txtBanco.txt.Clear()
        txtMoneda.txt.Clear()

        For Each oRow As DataRow In dtCuentaBancaria.Select(" ID=" & cbxCuenta.cbx.SelectedValue)

            txtBanco.txt.Text = oRow("Banco").ToString
            txtMoneda.txt.Text = oRow("Mon").ToString
            txtNombre.txt.Text = oRow("Nombre").ToString
            txtTitular.txt.Text = oRow("Titulares").ToString
        Next

    End Sub

    Sub Seleccionar()

        Dim frm As New frmSeleccionConciliacionBancaria
        frm.Text = "Cuenta: " & txtBanco.txt.Text & "-" & cbxCuenta.cbx.Text
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen

        '30-07-2021 SM - Se agrega nuevo campo Fecha Vencimiento
        dtConciliacion = CSistema.ExecuteToDataTable("Select 'Sel'=Case When(Conciliado)='Si' Then 'True' Else 'False' End,Fec,Movimiento,Operacion,Comprobante,Sucursal,Debito,Credito,Saldo,Observacion,'AlaOrden'=[A la Orden],Diferido,FechaVencimiento,Conciliado,IDTransaccion From VMovimientoBancario2 Where IDCuentaBancaria=" & cbxCuenta.GetValue & " And Fecha Between '" & txtFecha1.GetValueString & "' And '" & txtFecha2.GetValueString & "' Order By Fecha").Copy
        frm.dt = dtConciliacion
        frm.Inicializar()
        frm.ShowDialog(Me)

    End Sub

    Private Sub frmConciliaciondeMovimientoBancaria_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConciliaciondeMovimientoBancaria_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmConciliaciondeMovimientoBancaria_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub cbxCuenta_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCuenta.PropertyChanged
        ObtenerCuenta()
    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        Seleccionar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmConciliaciondeMovimientoBancaria_Activate()
        Me.Refresh()
    End Sub
End Class
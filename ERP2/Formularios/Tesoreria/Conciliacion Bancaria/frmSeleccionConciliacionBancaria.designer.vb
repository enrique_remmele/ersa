﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeleccionConciliacionBancaria
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.EliminarConciliacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InvertirSeleccionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colSel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMovimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOperacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSucursal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDebito = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCredito = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colObservacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAlaOrden = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFechaVencimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConciliado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCantidadSeleccionado = New ERP.ocxTXTNumeric()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.TxtSaldoConciliado = New ERP.ocxTXTNumeric()
        Me.txtChequesNoPagados = New ERP.ocxTXTNumeric()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtDepositoAConfirmar = New ERP.ocxTXTNumeric()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.cbxOperaciones = New ERP.ocxCBX()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ContextMenuStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EliminarConciliacionToolStripMenuItem, Me.InvertirSeleccionToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(231, 48)
        '
        'EliminarConciliacionToolStripMenuItem
        '
        Me.EliminarConciliacionToolStripMenuItem.Name = "EliminarConciliacionToolStripMenuItem"
        Me.EliminarConciliacionToolStripMenuItem.Size = New System.Drawing.Size(230, 22)
        Me.EliminarConciliacionToolStripMenuItem.Text = "Eliminar Conciliacion anterior"
        '
        'InvertirSeleccionToolStripMenuItem
        '
        Me.InvertirSeleccionToolStripMenuItem.Name = "InvertirSeleccionToolStripMenuItem"
        Me.InvertirSeleccionToolStripMenuItem.Size = New System.Drawing.Size(230, 22)
        Me.InvertirSeleccionToolStripMenuItem.Text = "Invertir Seleccion"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.dgw, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel5, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1074, 573)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        Me.dgw.AllowUserToResizeColumns = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colSel, Me.colFecha, Me.colMovimiento, Me.colOperacion, Me.colComprobante, Me.colSucursal, Me.colDebito, Me.colCredito, Me.colSaldo, Me.colObservacion, Me.colAlaOrden, Me.colFechaVencimiento, Me.colConciliado, Me.colIDTransaccion})
        Me.dgw.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgw.Location = New System.Drawing.Point(3, 3)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(1068, 474)
        Me.dgw.TabIndex = 0
        '
        'colSel
        '
        Me.colSel.HeaderText = "Sel."
        Me.colSel.Name = "colSel"
        Me.colSel.ReadOnly = True
        Me.colSel.Width = 35
        '
        'colFecha
        '
        Me.colFecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colFecha.DefaultCellStyle = DataGridViewCellStyle2
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.ReadOnly = True
        '
        'colMovimiento
        '
        Me.colMovimiento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colMovimiento.HeaderText = "Movimiento"
        Me.colMovimiento.Name = "colMovimiento"
        Me.colMovimiento.ReadOnly = True
        '
        'colOperacion
        '
        Me.colOperacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colOperacion.HeaderText = "Operación"
        Me.colOperacion.Name = "colOperacion"
        Me.colOperacion.ReadOnly = True
        '
        'colComprobante
        '
        Me.colComprobante.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colComprobante.HeaderText = "Comprobante"
        Me.colComprobante.Name = "colComprobante"
        Me.colComprobante.ReadOnly = True
        '
        'colSucursal
        '
        Me.colSucursal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colSucursal.HeaderText = "Sucursal"
        Me.colSucursal.Name = "colSucursal"
        Me.colSucursal.ReadOnly = True
        '
        'colDebito
        '
        Me.colDebito.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colDebito.HeaderText = "Débito"
        Me.colDebito.Name = "colDebito"
        Me.colDebito.ReadOnly = True
        '
        'colCredito
        '
        Me.colCredito.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colCredito.HeaderText = "Crédito"
        Me.colCredito.Name = "colCredito"
        Me.colCredito.ReadOnly = True
        '
        'colSaldo
        '
        Me.colSaldo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colSaldo.HeaderText = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.ReadOnly = True
        '
        'colObservacion
        '
        Me.colObservacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colObservacion.HeaderText = "Observación"
        Me.colObservacion.Name = "colObservacion"
        Me.colObservacion.ReadOnly = True
        '
        'colAlaOrden
        '
        Me.colAlaOrden.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colAlaOrden.HeaderText = "AlaOrden"
        Me.colAlaOrden.Name = "colAlaOrden"
        Me.colAlaOrden.ReadOnly = True
        '
        'colFechaVencimiento
        '
        Me.colFechaVencimiento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colFechaVencimiento.DefaultCellStyle = DataGridViewCellStyle3
        Me.colFechaVencimiento.HeaderText = "FechaVencimiento"
        Me.colFechaVencimiento.Name = "colFechaVencimiento"
        Me.colFechaVencimiento.ReadOnly = True
        '
        'colConciliado
        '
        Me.colConciliado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colConciliado.HeaderText = "Conciliado"
        Me.colConciliado.Name = "colConciliado"
        Me.colConciliado.ReadOnly = True
        Me.colConciliado.Visible = False
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Label5)
        Me.Panel5.Controls.Add(Me.Label1)
        Me.Panel5.Controls.Add(Me.txtCantidadSeleccionado)
        Me.Panel5.Controls.Add(Me.btnSalir)
        Me.Panel5.Controls.Add(Me.TxtSaldoConciliado)
        Me.Panel5.Controls.Add(Me.txtChequesNoPagados)
        Me.Panel5.Controls.Add(Me.Label3)
        Me.Panel5.Controls.Add(Me.txtDepositoAConfirmar)
        Me.Panel5.Controls.Add(Me.Label2)
        Me.Panel5.Controls.Add(Me.btnGuardar)
        Me.Panel5.Controls.Add(Me.cbxOperaciones)
        Me.Panel5.Controls.Add(Me.Label4)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(3, 483)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1068, 60)
        Me.Panel5.TabIndex = 19
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(442, 38)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 13)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Cant Seleccionado:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(213, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(114, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Depositos A Confirmar:"
        '
        'txtCantidadSeleccionado
        '
        Me.txtCantidadSeleccionado.Color = System.Drawing.Color.Empty
        Me.txtCantidadSeleccionado.Decimales = True
        Me.txtCantidadSeleccionado.Indicaciones = Nothing
        Me.txtCantidadSeleccionado.Location = New System.Drawing.Point(548, 33)
        Me.txtCantidadSeleccionado.Name = "txtCantidadSeleccionado"
        Me.txtCantidadSeleccionado.Size = New System.Drawing.Size(103, 22)
        Me.txtCantidadSeleccionado.SoloLectura = True
        Me.txtCantidadSeleccionado.TabIndex = 26
        Me.txtCantidadSeleccionado.TabStop = False
        Me.txtCantidadSeleccionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadSeleccionado.Texto = "0"
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(671, 33)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 22)
        Me.btnSalir.TabIndex = 20
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'TxtSaldoConciliado
        '
        Me.TxtSaldoConciliado.Color = System.Drawing.Color.Empty
        Me.TxtSaldoConciliado.Decimales = True
        Me.TxtSaldoConciliado.Indicaciones = Nothing
        Me.TxtSaldoConciliado.Location = New System.Drawing.Point(548, 8)
        Me.TxtSaldoConciliado.Name = "TxtSaldoConciliado"
        Me.TxtSaldoConciliado.Size = New System.Drawing.Size(103, 22)
        Me.TxtSaldoConciliado.SoloLectura = True
        Me.TxtSaldoConciliado.TabIndex = 24
        Me.TxtSaldoConciliado.TabStop = False
        Me.TxtSaldoConciliado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TxtSaldoConciliado.Texto = "0"
        '
        'txtChequesNoPagados
        '
        Me.txtChequesNoPagados.Color = System.Drawing.Color.Empty
        Me.txtChequesNoPagados.Decimales = True
        Me.txtChequesNoPagados.Indicaciones = Nothing
        Me.txtChequesNoPagados.Location = New System.Drawing.Point(333, 33)
        Me.txtChequesNoPagados.Name = "txtChequesNoPagados"
        Me.txtChequesNoPagados.Size = New System.Drawing.Size(103, 22)
        Me.txtChequesNoPagados.SoloLectura = True
        Me.txtChequesNoPagados.TabIndex = 22
        Me.txtChequesNoPagados.TabStop = False
        Me.txtChequesNoPagados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtChequesNoPagados.Texto = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(453, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 13)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "Saldo Conciliado:"
        '
        'txtDepositoAConfirmar
        '
        Me.txtDepositoAConfirmar.Color = System.Drawing.Color.Empty
        Me.txtDepositoAConfirmar.Decimales = True
        Me.txtDepositoAConfirmar.Indicaciones = Nothing
        Me.txtDepositoAConfirmar.Location = New System.Drawing.Point(333, 8)
        Me.txtDepositoAConfirmar.Name = "txtDepositoAConfirmar"
        Me.txtDepositoAConfirmar.Size = New System.Drawing.Size(103, 22)
        Me.txtDepositoAConfirmar.SoloLectura = True
        Me.txtDepositoAConfirmar.TabIndex = 18
        Me.txtDepositoAConfirmar.TabStop = False
        Me.txtDepositoAConfirmar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDepositoAConfirmar.Texto = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(213, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(114, 13)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "Cheques No Pagados:"
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(671, 8)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 22)
        Me.btnGuardar.TabIndex = 19
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'cbxOperaciones
        '
        Me.cbxOperaciones.CampoWhere = Nothing
        Me.cbxOperaciones.CargarUnaSolaVez = False
        Me.cbxOperaciones.DataDisplayMember = Nothing
        Me.cbxOperaciones.DataFilter = Nothing
        Me.cbxOperaciones.DataOrderBy = Nothing
        Me.cbxOperaciones.DataSource = Nothing
        Me.cbxOperaciones.DataValueMember = Nothing
        Me.cbxOperaciones.dtSeleccionado = Nothing
        Me.cbxOperaciones.FormABM = Nothing
        Me.cbxOperaciones.Indicaciones = Nothing
        Me.cbxOperaciones.Location = New System.Drawing.Point(73, 8)
        Me.cbxOperaciones.Name = "cbxOperaciones"
        Me.cbxOperaciones.SeleccionMultiple = False
        Me.cbxOperaciones.SeleccionObligatoria = True
        Me.cbxOperaciones.Size = New System.Drawing.Size(124, 23)
        Me.cbxOperaciones.SoloLectura = False
        Me.cbxOperaciones.TabIndex = 15
        Me.cbxOperaciones.Texto = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(4, 13)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 13)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Operaciones:"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 551)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1074, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'frmSeleccionConciliacionBancaria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1074, 573)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmSeleccionConciliacionBancaria"
        Me.Text = "frmSeleccionConciliacionBancaria"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents EliminarConciliacionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InvertirSeleccionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCantidadSeleccionado As ERP.ocxTXTNumeric
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents TxtSaldoConciliado As ERP.ocxTXTNumeric
    Friend WithEvents txtChequesNoPagados As ERP.ocxTXTNumeric
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtDepositoAConfirmar As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents cbxOperaciones As ERP.ocxCBX
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents colSel As DataGridViewCheckBoxColumn
    Friend WithEvents colFecha As DataGridViewTextBoxColumn
    Friend WithEvents colMovimiento As DataGridViewTextBoxColumn
    Friend WithEvents colOperacion As DataGridViewTextBoxColumn
    Friend WithEvents colComprobante As DataGridViewTextBoxColumn
    Friend WithEvents colSucursal As DataGridViewTextBoxColumn
    Friend WithEvents colDebito As DataGridViewTextBoxColumn
    Friend WithEvents colCredito As DataGridViewTextBoxColumn
    Friend WithEvents colSaldo As DataGridViewTextBoxColumn
    Friend WithEvents colObservacion As DataGridViewTextBoxColumn
    Friend WithEvents colAlaOrden As DataGridViewTextBoxColumn
    Friend WithEvents colFechaVencimiento As DataGridViewTextBoxColumn
    Friend WithEvents colConciliado As DataGridViewTextBoxColumn
    Friend WithEvents colIDTransaccion As DataGridViewTextBoxColumn
End Class

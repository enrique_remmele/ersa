﻿Public Class frmSeleccionConciliacionBancaria
    Dim CSistema As New CSistema

    Dim Filtro As String = ""

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Sub Inicializar()

        cbxOperaciones.cbx.Items.Add("Ver Todas")
        cbxOperaciones.cbx.Items.Add("Conciliado")
        cbxOperaciones.cbx.Items.Add("No Conciliado")

        cbxOperaciones.cbx.SelectedIndex = 0

        Listar(Filtro)

        calculartotales()

    End Sub

    Sub Listar(ByVal Filtro As String)

        dgw.Rows.Clear()

        For Each oRow As DataRow In dt.Select(Filtro)
            Dim Registro(13) As String
            Registro(0) = oRow("Sel").ToString
            Registro(1) = oRow("Fec").ToString
            Registro(2) = oRow("Movimiento").ToString
            Registro(3) = oRow("Operacion").ToString
            Registro(4) = oRow("Comprobante").ToString
            Registro(5) = oRow("Sucursal").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Debito").ToString, True)
            Registro(7) = CSistema.FormatoMoneda(oRow("Credito").ToString, True)
            Registro(8) = CSistema.FormatoMoneda(oRow("Saldo").ToString, True)
            Registro(9) = oRow("Observacion").ToString
            Registro(10) = oRow("AlaOrden").ToString
            'Registro(11) = oRow("Diferido").ToString
            Registro(11) = oRow("FechaVencimiento").ToString
            Registro(12) = oRow("Conciliado").ToString
            Registro(13) = oRow("IDTransaccion").ToString

            dgw.Rows.Add(Registro)

        Next

        CalcularTotales()


        PintarCelda()

    End Sub

    Sub PintarCelda()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(0).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next

        If dgw.CurrentRow Is Nothing Then
            Exit Sub
        End If

    End Sub

    Sub Filtrar()

        Select Case cbxOperaciones.cbx.SelectedIndex
            Case 0
                Filtro = ""
                Listar(Filtro)
            Case 1
                Filtro = " Conciliado = 'SI' "
                Listar(Filtro)
            Case 2
                Filtro = "Conciliado = '---'"
                Listar(Filtro)
        End Select

        PintarCelda()

    End Sub

    Sub CalcularTotales()

        Dim TotalDebito As Decimal = 0
        Dim TotalCredito As Decimal = 0
        Dim TotalDebitoConciliado As Decimal = 0
        Dim TotalCreditoConciliado As Decimal = 0
        Dim Saldo As Decimal = 0
        Dim SaldoConciliado As Decimal = 0
        Dim CantidadSeleccinado As Integer = 0

        For Each item As DataGridViewRow In dgw.Rows

            If item.Cells("colSel").Value = False Then

                TotalDebito = TotalDebito + CSistema.FormatoMoneda(CDbl(item.Cells("colDebito").Value), True)
                TotalCredito = TotalCredito + CSistema.FormatoMoneda(CDbl(item.Cells("colCredito").Value), True)
                Saldo = TotalDebito - TotalCredito
            Else
                TotalDebitoConciliado = TotalDebitoConciliado + CSistema.FormatoMoneda(CDbl(item.Cells("colDebito").Value), True)
                TotalCreditoConciliado = TotalCreditoConciliado + CSistema.FormatoMoneda(CDbl(item.Cells("colCredito").Value), True)
                SaldoConciliado = TotalDebitoConciliado - TotalCreditoConciliado
                CantidadSeleccinado = CantidadSeleccinado + 1

            End If

        Next

        txtDepositoAConfirmar.SetValue(TotalDebito)
        txtChequesNoPagados.SetValue(TotalCredito)
        TxtSaldoConciliado.SetValue(SaldoConciliado)
        txtCantidadSeleccionado.SetValue(CantidadSeleccinado)
    End Sub

    Sub Guardar()
        Dim Transaccion As Integer
        Dim conciliado As Boolean

        For Each item As DataGridViewRow In dgw.Rows

            conciliado = item.Cells("colSel").Value
            Dim IDTransaccion As Integer = item.Cells("colIDTransaccion").Value
            Transaccion = IDTransaccion

            Dim param(-1) As SqlClient.SqlParameter
            Dim IndiceOperacion As Integer = 0

            'Entrada
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Conciliado", conciliado, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Operacion", "UPD", ParameterDirection.Input)
            IndiceOperacion = param.GetLength(0) - 1

            'Transaccion
            CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpConciliar", False, True, MensajeRetorno) = False Then
                CSistema.MostrarError("Atencion: " & MensajeRetorno, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)

                Exit Sub
            End If


        Next

        Dim Consulta As String
        'ELI',@VTipoComprobante, @IDSucursal)
        Dim IDCuentaBancaria As Integer = CType(CSistema.ExecuteScalar("Select IsNull((IDCuentaBancaria), 0) from VMovimientoBancario where IDTransaccion = " & Transaccion, VGCadenaConexion), Integer)
        Consulta = "Insert into AuditoriaDocumentos	values(0,0," & IDCuentaBancaria & ",getdate(),0,0,'CONCILIACION',getdate()," & vgIDUsuario & ", 'UPD','CONCILIACION'," & vgIDSucursal & ")"
        If CSistema.ExecuteNonQuery(Consulta, VGCadenaConexion) > 0 Then
            MessageBox.Show("Conciliacion guardada correctamente.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If
    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick
        ctrError.Clear()
        tsslEstado.Text = ""

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        If e.ColumnIndex = dgw.Columns.Item("colSel").Index Then

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSel").Value = False Then
                        oRow.Cells("colSel").Value = True
                        oRow.Cells("colSel").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                    Else
                        oRow.Cells("colSel").ReadOnly = True
                        oRow.Cells("colSel").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.White

                        ''Calcular 
                        dgw.Update()

                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Space Then

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSel").Value = False Then
                        oRow.Cells("colSel").Value = True
                        oRow.Cells("colSel").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                    Else
                        oRow.Cells("colSel").ReadOnly = True
                        oRow.Cells("colSel").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.White

                        ''Calcular 
                        dgw.Update()

                    End If

                    Exit For

                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colSaldo")
            End If

            dgw.Update()

            CalcularTotales()

            ' Your code here
            e.SuppressKeyPress = True

        End If
    End Sub

    Private Sub EliminarConciliacionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarConciliacionToolStripMenuItem.Click
        'Guardar(False)
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub frmSeleccionConciliacionBancaria_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dgw.Focus()
    End Sub

    Private Sub cbxOperaciones_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Filtrar()
    End Sub

    Private Sub cbxOperaciones_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

    End Sub

    Private Sub btnGuardar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
        Me.Close()
    End Sub

    Private Sub cbxOperaciones_PropertyChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxOperaciones.PropertyChanged
        Filtrar()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmSeleccionConciliacionBancaria_Activate()
        Me.Refresh()
    End Sub

    Private Sub btnSalir_Click_1(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
End Class
﻿Public Class frmConsultarSaldoProveedorFecha
    Dim CSistema As New CSistema
    Dim dtInventarioDocumentosPendientesPago As DataTable
    Dim Listando As Boolean = False
    Sub Inicializar()
        txtProveedor.Conectar()
        txtCuentaContable.Conectar()


    End Sub

    Sub Listar()

        Listando = True

        Dim SQL As String = "Exec SpViewExtractoMovimientoProveedorFacturaCCSaldo"
        Dim where As String = " @FechaPago = '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.Text, True, False) & "' "


        If chkCuentaContable.Checked Then
            If txtCuentaContable.Seleccionado Then
                where = where & " ,@CuentaContable = '" & txtCuentaContable.txtCodigo.Texto & "' "
            Else
                MessageBox.Show("Seleccione una cuenta contable", "Atencion", MessageBoxButtons.OK)
                Exit Sub
            End If
        End If

        If chkProveedor.Checked Then
            If txtProveedor.Seleccionado Then
                where = where & " ,@IDProveedor = " & txtProveedor.Registro("ID") & " "
            Else
                MessageBox.Show("Seleccione un proveedor", "Atencion", MessageBoxButtons.OK)
                Exit Sub
            End If
        End If

        dtInventarioDocumentosPendientesPago = CSistema.ExecuteToDataTable(SQL & where,, 600)

        CSistema.dtToGrid(dgvFacturas, dtInventarioDocumentosPendientesPago)
        CSistema.DataGridColumnasVisibles(dgvFacturas, {"Fecha", "Proveedor", "Documento", "[Detalle/Concepto]", "Debito", "Credito", "Saldo", "Cotizacion", "CreditoGs", "SaldoGs"})
        CSistema.DataGridColumnasNumericas(dgvFacturas, {"Debito", "Credito", "Saldo", "CreditoGs", "SaldoGs", "Cotizacion"}, True)
        dgvFacturas.Columns("Documento").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        CalcularTotales()
        Listando = False
    End Sub

    Sub CalcularTotales()

        Dim Total As Decimal = CSistema.dtSumColumn(dtInventarioDocumentosPendientesPago, "CreditoGs")
        Dim Saldo As Decimal = CSistema.dtSumColumn(dtInventarioDocumentosPendientesPago, "SaldoGs")
        txtTotal.Text = CSistema.FormatoMoneda(Total, False)
        txtSaldo.Text = CSistema.FormatoMoneda(Saldo, False)
    End Sub

    Sub ListarExtracto(ByVal IDTransaccion As Integer)

        Dim SQL As String = "Select IDTransaccion, Operacion, Fecha, Documento, [Detalle/Concepto], Debito, Credito, Saldo From VExtractoMovimientoProveedorFactura Where ComprobanteAsociado=" & IDTransaccion & " Order By Fecha "
        Dim vdt As DataTable = CSistema.ExecuteToDataTable(SQL)
        Dim SaldoAnterior As Decimal = 0
        For Each oRow As DataRow In vdt.Rows

            SaldoAnterior = (SaldoAnterior + oRow("Credito")) - oRow("Debito")
            oRow("Saldo") = SaldoAnterior

        Next

        CSistema.dtToGrid(dgvExtracto, vdt)

        'Formato
        Dim IDMoneda As Integer = dgvExtracto.SelectedRows(0).Cells("IDMoneda").Value
        CSistema.DataGridColumnasNumericas(dgvExtracto, {"Debito", "Credito", "Saldo"}, True)
        dgvExtracto.Columns("IDTransaccion").HeaderText = "Nro. Trans."
        dgvExtracto.Columns("Documento").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill


    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub frmConsultarSaldoProveedorFecha_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkProveedor_CheckedChanged(sender As Object, e As EventArgs) Handles chkProveedor.CheckedChanged
        txtProveedor.Enabled = chkProveedor.Checked
    End Sub

    Private Sub chkCuentaContable_CheckedChanged(sender As Object, e As EventArgs) Handles chkCuentaContable.CheckedChanged
        txtCuentaContable.Enabled = chkCuentaContable.Checked
    End Sub

    Private Sub dgvFacturas_SelectionChanged(sender As Object, e As EventArgs) Handles dgvFacturas.SelectionChanged
        If Listando Then
            Exit Sub
        End If

        Try
            ListarExtracto(dgvFacturas.SelectedRows(0).Cells("IDTransaccion").Value)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        CSistema.dtToExcel2(dtInventarioDocumentosPendientesPago, "Inventario de Documentos Pendientes de Pago")
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmConsultarSaldoProveedorFecha_Activate()
        Me.Refresh()
    End Sub
End Class
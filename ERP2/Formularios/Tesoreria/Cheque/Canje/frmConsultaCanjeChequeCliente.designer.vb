﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaCanjeChequeCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalCobranza = New ERP.ocxTXTNumeric()
        Me.lblTotalCobranza = New System.Windows.Forms.Label()
        Me.btnSeleccionar = New System.Windows.Forms.Button()
        Me.txtCantidadFormaPago = New ERP.ocxTXTNumeric()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.lblFormaPago = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.lblCantidadFormaPago = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.chkSucursal = New System.Windows.Forms.CheckBox()
        Me.cbxSucursal = New System.Windows.Forms.ComboBox()
        Me.btn4 = New System.Windows.Forms.Button()
        Me.chkFecha = New System.Windows.Forms.CheckBox()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.chkTipoComprobante = New System.Windows.Forms.CheckBox()
        Me.cbxTipoComprobante = New System.Windows.Forms.ComboBox()
        Me.chkCliente = New System.Windows.Forms.CheckBox()
        Me.cbxCliente = New System.Windows.Forms.ComboBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtTotalComprobantes = New ERP.ocxTXTNumeric()
        Me.lblTotalComprobantes = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblCantidadComprobantes = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblCantidadCobranza = New System.Windows.Forms.Label()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.lblComprobantes = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.txtCantidadComprobantes = New ERP.ocxTXTNumeric()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtNroCheque = New ERP.ocxTXTString()
        Me.lblNroCheque = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.txtOperacion = New ERP.ocxTXTNumeric()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtCantidadCobranza = New ERP.ocxTXTNumeric()
        Me.lvComprobantes = New System.Windows.Forms.ListView()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalFormaPago = New ERP.ocxTXTNumeric()
        Me.lblTotalFormaPago = New System.Windows.Forms.Label()
        Me.lvOperacion = New System.Windows.Forms.ListView()
        Me.lvFormaPago = New System.Windows.Forms.ListView()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel6.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 2
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.Controls.Add(Me.FlowLayoutPanel1, 1, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.btnSeleccionar, 0, 0)
        Me.TableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(4, 109)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 1
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(467, 26)
        Me.TableLayoutPanel6.TabIndex = 9
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalCobranza)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblTotalCobranza)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(236, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(228, 20)
        Me.FlowLayoutPanel1.TabIndex = 2
        '
        'txtTotalCobranza
        '
        Me.txtTotalCobranza.Color = System.Drawing.Color.Empty
        Me.txtTotalCobranza.Decimales = True
        Me.txtTotalCobranza.Indicaciones = Nothing
        Me.txtTotalCobranza.Location = New System.Drawing.Point(122, 3)
        Me.txtTotalCobranza.Name = "txtTotalCobranza"
        Me.txtTotalCobranza.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalCobranza.SoloLectura = False
        Me.txtTotalCobranza.TabIndex = 1
        Me.txtTotalCobranza.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCobranza.Texto = "0"
        '
        'lblTotalCobranza
        '
        Me.lblTotalCobranza.AutoSize = True
        Me.lblTotalCobranza.Location = New System.Drawing.Point(82, 0)
        Me.lblTotalCobranza.Name = "lblTotalCobranza"
        Me.lblTotalCobranza.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalCobranza.TabIndex = 0
        Me.lblTotalCobranza.Text = "Total:"
        '
        'btnSeleccionar
        '
        Me.btnSeleccionar.Location = New System.Drawing.Point(3, 3)
        Me.btnSeleccionar.Name = "btnSeleccionar"
        Me.btnSeleccionar.Size = New System.Drawing.Size(75, 20)
        Me.btnSeleccionar.TabIndex = 3
        Me.btnSeleccionar.Text = "Seleccionar"
        Me.btnSeleccionar.UseVisualStyleBackColor = True
        '
        'txtCantidadFormaPago
        '
        Me.txtCantidadFormaPago.Color = System.Drawing.Color.Empty
        Me.txtCantidadFormaPago.Decimales = True
        Me.txtCantidadFormaPago.Indicaciones = Nothing
        Me.txtCantidadFormaPago.Location = New System.Drawing.Point(3, 1)
        Me.txtCantidadFormaPago.Name = "txtCantidadFormaPago"
        Me.txtCantidadFormaPago.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadFormaPago.SoloLectura = False
        Me.txtCantidadFormaPago.TabIndex = 0
        Me.txtCantidadFormaPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadFormaPago.Texto = "0"
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.lblFormaPago)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel7.Location = New System.Drawing.Point(3, 3)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(272, 20)
        Me.Panel7.TabIndex = 0
        '
        'lblFormaPago
        '
        Me.lblFormaPago.AutoSize = True
        Me.lblFormaPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormaPago.Location = New System.Drawing.Point(3, 8)
        Me.lblFormaPago.Name = "lblFormaPago"
        Me.lblFormaPago.Size = New System.Drawing.Size(79, 13)
        Me.lblFormaPago.TabIndex = 0
        Me.lblFormaPago.Text = "Forma de Pago"
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.lblCantidadFormaPago)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel8.Location = New System.Drawing.Point(342, 3)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(60, 20)
        Me.Panel8.TabIndex = 1
        '
        'lblCantidadFormaPago
        '
        Me.lblCantidadFormaPago.AutoSize = True
        Me.lblCantidadFormaPago.Location = New System.Drawing.Point(3, 8)
        Me.lblCantidadFormaPago.Name = "lblCantidadFormaPago"
        Me.lblCantidadFormaPago.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadFormaPago.TabIndex = 0
        Me.lblCantidadFormaPago.Text = "Cantidad:"
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.txtCantidadFormaPago)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel9.Location = New System.Drawing.Point(408, 3)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(56, 20)
        Me.Panel9.TabIndex = 2
        '
        'TabControl1
        '
        Me.TabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(3, 16)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(225, 393)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.chkSucursal)
        Me.TabPage1.Controls.Add(Me.cbxSucursal)
        Me.TabPage1.Controls.Add(Me.btn4)
        Me.TabPage1.Controls.Add(Me.chkFecha)
        Me.TabPage1.Controls.Add(Me.dtpHasta)
        Me.TabPage1.Controls.Add(Me.dtpDesde)
        Me.TabPage1.Controls.Add(Me.chkTipoComprobante)
        Me.TabPage1.Controls.Add(Me.cbxTipoComprobante)
        Me.TabPage1.Controls.Add(Me.chkCliente)
        Me.TabPage1.Controls.Add(Me.cbxCliente)
        Me.TabPage1.Location = New System.Drawing.Point(4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(217, 367)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'chkSucursal
        '
        Me.chkSucursal.AutoSize = True
        Me.chkSucursal.Location = New System.Drawing.Point(6, 94)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(70, 17)
        Me.chkSucursal.TabIndex = 6
        Me.chkSucursal.Text = "Sucursal:"
        Me.chkSucursal.UseVisualStyleBackColor = True
        '
        'cbxSucursal
        '
        Me.cbxSucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormattingEnabled = True
        Me.cbxSucursal.Location = New System.Drawing.Point(6, 111)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.Size = New System.Drawing.Size(203, 21)
        Me.cbxSucursal.TabIndex = 7
        '
        'btn4
        '
        Me.btn4.Location = New System.Drawing.Point(6, 213)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(203, 24)
        Me.btn4.TabIndex = 11
        Me.btn4.Text = "Emitir informe"
        Me.btn4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn4.UseVisualStyleBackColor = True
        '
        'chkFecha
        '
        Me.chkFecha.AutoSize = True
        Me.chkFecha.Location = New System.Drawing.Point(6, 138)
        Me.chkFecha.Name = "chkFecha"
        Me.chkFecha.Size = New System.Drawing.Size(59, 17)
        Me.chkFecha.TabIndex = 8
        Me.chkFecha.Text = "Fecha:"
        Me.chkFecha.UseVisualStyleBackColor = True
        '
        'dtpHasta
        '
        Me.dtpHasta.Enabled = False
        Me.dtpHasta.Location = New System.Drawing.Point(6, 181)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(203, 20)
        Me.dtpHasta.TabIndex = 10
        '
        'dtpDesde
        '
        Me.dtpDesde.Enabled = False
        Me.dtpDesde.Location = New System.Drawing.Point(6, 155)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(203, 20)
        Me.dtpDesde.TabIndex = 9
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.AutoSize = True
        Me.chkTipoComprobante.Location = New System.Drawing.Point(6, 49)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(131, 17)
        Me.chkTipoComprobante.TabIndex = 2
        Me.chkTipoComprobante.Text = "Tipo de Comprobante:"
        Me.chkTipoComprobante.UseVisualStyleBackColor = True
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormattingEnabled = True
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(6, 66)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(203, 21)
        Me.cbxTipoComprobante.TabIndex = 3
        '
        'chkCliente
        '
        Me.chkCliente.AutoSize = True
        Me.chkCliente.Location = New System.Drawing.Point(6, 6)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(61, 17)
        Me.chkCliente.TabIndex = 0
        Me.chkCliente.Text = "Cliente:"
        Me.chkCliente.UseVisualStyleBackColor = True
        '
        'cbxCliente
        '
        Me.cbxCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxCliente.Enabled = False
        Me.cbxCliente.FormattingEnabled = True
        Me.cbxCliente.Location = New System.Drawing.Point(6, 22)
        Me.cbxCliente.Name = "cbxCliente"
        Me.cbxCliente.Size = New System.Drawing.Size(203, 21)
        Me.cbxCliente.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TabControl1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(484, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(231, 412)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 4
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.01285!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.98715!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61.0!))
        Me.TableLayoutPanel5.Controls.Add(Me.Panel7, 0, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.Panel8, 2, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.Panel9, 3, 0)
        Me.TableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(4, 278)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(467, 26)
        Me.TableLayoutPanel5.TabIndex = 7
        '
        'txtTotalComprobantes
        '
        Me.txtTotalComprobantes.Color = System.Drawing.Color.Empty
        Me.txtTotalComprobantes.Decimales = True
        Me.txtTotalComprobantes.Indicaciones = Nothing
        Me.txtTotalComprobantes.Location = New System.Drawing.Point(361, 3)
        Me.txtTotalComprobantes.Name = "txtTotalComprobantes"
        Me.txtTotalComprobantes.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalComprobantes.SoloLectura = False
        Me.txtTotalComprobantes.TabIndex = 1
        Me.txtTotalComprobantes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalComprobantes.Texto = "0"
        '
        'lblTotalComprobantes
        '
        Me.lblTotalComprobantes.AutoSize = True
        Me.lblTotalComprobantes.Location = New System.Drawing.Point(321, 0)
        Me.lblTotalComprobantes.Name = "lblTotalComprobantes"
        Me.lblTotalComprobantes.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalComprobantes.TabIndex = 0
        Me.lblTotalComprobantes.Text = "Total:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblCantidadComprobantes)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(342, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(60, 20)
        Me.Panel1.TabIndex = 1
        '
        'lblCantidadComprobantes
        '
        Me.lblCantidadComprobantes.AutoSize = True
        Me.lblCantidadComprobantes.Location = New System.Drawing.Point(3, 8)
        Me.lblCantidadComprobantes.Name = "lblCantidadComprobantes"
        Me.lblCantidadComprobantes.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadComprobantes.TabIndex = 0
        Me.lblCantidadComprobantes.Text = "Cantidad:"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblCantidadCobranza)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(342, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(60, 20)
        Me.Panel2.TabIndex = 1
        '
        'lblCantidadCobranza
        '
        Me.lblCantidadCobranza.AutoSize = True
        Me.lblCantidadCobranza.Location = New System.Drawing.Point(3, 8)
        Me.lblCantidadCobranza.Name = "lblCantidadCobranza"
        Me.lblCantidadCobranza.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadCobranza.TabIndex = 0
        Me.lblCantidadCobranza.Text = "Cantidad:"
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 4
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.01285!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.98715!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Panel5, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel1, 2, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel6, 3, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(4, 142)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(467, 26)
        Me.TableLayoutPanel4.TabIndex = 4
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.lblComprobantes)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(3, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(272, 20)
        Me.Panel5.TabIndex = 0
        '
        'lblComprobantes
        '
        Me.lblComprobantes.AutoSize = True
        Me.lblComprobantes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComprobantes.Location = New System.Drawing.Point(3, 8)
        Me.lblComprobantes.Name = "lblComprobantes"
        Me.lblComprobantes.Size = New System.Drawing.Size(158, 13)
        Me.lblComprobantes.TabIndex = 0
        Me.lblComprobantes.Text = "Lista de comprobantes pagados"
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.txtCantidadComprobantes)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(408, 3)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(56, 20)
        Me.Panel6.TabIndex = 2
        '
        'txtCantidadComprobantes
        '
        Me.txtCantidadComprobantes.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobantes.Decimales = True
        Me.txtCantidadComprobantes.Indicaciones = Nothing
        Me.txtCantidadComprobantes.Location = New System.Drawing.Point(3, 1)
        Me.txtCantidadComprobantes.Name = "txtCantidadComprobantes"
        Me.txtCantidadComprobantes.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadComprobantes.SoloLectura = False
        Me.txtCantidadComprobantes.TabIndex = 0
        Me.txtCantidadComprobantes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobantes.Texto = "0"
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 4
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.01285!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.98715!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Panel2, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel3, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel4, 3, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(4, 4)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(467, 26)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.txtNroCheque)
        Me.Panel3.Controls.Add(Me.lblNroCheque)
        Me.Panel3.Controls.Add(Me.lblOperacion)
        Me.Panel3.Controls.Add(Me.txtOperacion)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(272, 20)
        Me.Panel3.TabIndex = 0
        '
        'txtNroCheque
        '
        Me.txtNroCheque.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroCheque.Color = System.Drawing.Color.Empty
        Me.txtNroCheque.Indicaciones = Nothing
        Me.txtNroCheque.Location = New System.Drawing.Point(232, 2)
        Me.txtNroCheque.Name = "txtNroCheque"
        Me.txtNroCheque.Size = New System.Drawing.Size(145, 21)
        Me.txtNroCheque.SoloLectura = False
        Me.txtNroCheque.TabIndex = 3
        Me.txtNroCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroCheque.Texto = ""
        '
        'lblNroCheque
        '
        Me.lblNroCheque.AutoSize = True
        Me.lblNroCheque.Location = New System.Drawing.Point(142, 6)
        Me.lblNroCheque.Name = "lblNroCheque"
        Me.lblNroCheque.Size = New System.Drawing.Size(82, 13)
        Me.lblNroCheque.TabIndex = 2
        Me.lblNroCheque.Text = "Nro. de Recibo:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(3, 6)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'txtOperacion
        '
        Me.txtOperacion.Color = System.Drawing.Color.Empty
        Me.txtOperacion.Decimales = True
        Me.txtOperacion.Indicaciones = Nothing
        Me.txtOperacion.Location = New System.Drawing.Point(68, 1)
        Me.txtOperacion.Name = "txtOperacion"
        Me.txtOperacion.Size = New System.Drawing.Size(68, 22)
        Me.txtOperacion.SoloLectura = False
        Me.txtOperacion.TabIndex = 1
        Me.txtOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOperacion.Texto = "0"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.txtCantidadCobranza)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(408, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(56, 20)
        Me.Panel4.TabIndex = 2
        '
        'txtCantidadCobranza
        '
        Me.txtCantidadCobranza.Color = System.Drawing.Color.Empty
        Me.txtCantidadCobranza.Decimales = True
        Me.txtCantidadCobranza.Indicaciones = Nothing
        Me.txtCantidadCobranza.Location = New System.Drawing.Point(3, 1)
        Me.txtCantidadCobranza.Name = "txtCantidadCobranza"
        Me.txtCantidadCobranza.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadCobranza.SoloLectura = False
        Me.txtCantidadCobranza.TabIndex = 0
        Me.txtCantidadCobranza.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadCobranza.Texto = "0"
        '
        'lvComprobantes
        '
        Me.lvComprobantes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvComprobantes.Location = New System.Drawing.Point(4, 175)
        Me.lvComprobantes.Name = "lvComprobantes"
        Me.lvComprobantes.Size = New System.Drawing.Size(467, 63)
        Me.lvComprobantes.TabIndex = 5
        Me.lvComprobantes.UseCompatibleStateImageBehavior = False
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel3, 0, 8)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel4, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.lvOperacion, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.lvComprobantes, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel2, 0, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel5, 0, 6)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel6, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.lvFormaPago, 0, 7)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 9
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(475, 412)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.txtTotalFormaPago)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblTotalFormaPago)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(4, 381)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(467, 27)
        Me.FlowLayoutPanel3.TabIndex = 0
        '
        'txtTotalFormaPago
        '
        Me.txtTotalFormaPago.Color = System.Drawing.Color.Empty
        Me.txtTotalFormaPago.Decimales = True
        Me.txtTotalFormaPago.Indicaciones = Nothing
        Me.txtTotalFormaPago.Location = New System.Drawing.Point(361, 3)
        Me.txtTotalFormaPago.Name = "txtTotalFormaPago"
        Me.txtTotalFormaPago.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalFormaPago.SoloLectura = False
        Me.txtTotalFormaPago.TabIndex = 1
        Me.txtTotalFormaPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalFormaPago.Texto = "0"
        '
        'lblTotalFormaPago
        '
        Me.lblTotalFormaPago.AutoSize = True
        Me.lblTotalFormaPago.Location = New System.Drawing.Point(321, 0)
        Me.lblTotalFormaPago.Name = "lblTotalFormaPago"
        Me.lblTotalFormaPago.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalFormaPago.TabIndex = 0
        Me.lblTotalFormaPago.Text = "Total:"
        '
        'lvOperacion
        '
        Me.lvOperacion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvOperacion.Location = New System.Drawing.Point(4, 37)
        Me.lvOperacion.Name = "lvOperacion"
        Me.lvOperacion.Size = New System.Drawing.Size(467, 65)
        Me.lvOperacion.TabIndex = 1
        Me.lvOperacion.UseCompatibleStateImageBehavior = False
        '
        'lvFormaPago
        '
        Me.lvFormaPago.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvFormaPago.Location = New System.Drawing.Point(4, 311)
        Me.lvFormaPago.Name = "lvFormaPago"
        Me.lvFormaPago.Size = New System.Drawing.Size(467, 63)
        Me.lvFormaPago.TabIndex = 8
        Me.lvFormaPago.UseCompatibleStateImageBehavior = False
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.txtTotalComprobantes)
        Me.FlowLayoutPanel2.Controls.Add(Me.lblTotalComprobantes)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(4, 245)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(467, 26)
        Me.FlowLayoutPanel2.TabIndex = 6
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 237.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(718, 418)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'frmConsultaPagoChequeCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(718, 418)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmConsultaPagoChequeCliente"
        Me.Text = "frmConsultaPagoChequeCliente"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotalFormaPago As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalFormaPago As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents lblComprobantes As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidadComprobantes As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidadComprobantes As ERP.ocxTXTNumeric
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidadCobranza As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtNroCheque As ERP.ocxTXTString
    Friend WithEvents lblNroCheque As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtOperacion As ERP.ocxTXTNumeric
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidadCobranza As ERP.ocxTXTNumeric
    Friend WithEvents lvOperacion As System.Windows.Forms.ListView
    Friend WithEvents lvComprobantes As System.Windows.Forms.ListView
    Friend WithEvents lvFormaPago As System.Windows.Forms.ListView
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotalComprobantes As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalComprobantes As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents lblFormaPago As System.Windows.Forms.Label
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidadFormaPago As System.Windows.Forms.Label
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidadFormaPago As ERP.ocxTXTNumeric
    Friend WithEvents TableLayoutPanel6 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotalCobranza As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalCobranza As System.Windows.Forms.Label
    Friend WithEvents btnSeleccionar As System.Windows.Forms.Button
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents chkSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents cbxSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents btn4 As System.Windows.Forms.Button
    Friend WithEvents chkFecha As System.Windows.Forms.CheckBox
    Friend WithEvents dtpHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkTipoComprobante As System.Windows.Forms.CheckBox
    Friend WithEvents cbxTipoComprobante As System.Windows.Forms.ComboBox
    Friend WithEvents chkCliente As System.Windows.Forms.CheckBox
    Friend WithEvents cbxCliente As System.Windows.Forms.ComboBox
End Class

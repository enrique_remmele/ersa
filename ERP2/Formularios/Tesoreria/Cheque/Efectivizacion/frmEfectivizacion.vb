﻿Imports ERP.Reporte
Public Class frmEfectivizacion
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CAsiento As New CAsientoDepositoBancario
    Dim CReporte As New CReporteCheque
    Public ID As Integer

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property
    Private IDTransaccionChequeValue As Integer
    Public Property IDTransaccionCheque() As Integer
        Get
            Return IDTransaccionChequeValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionChequeValue = value
        End Set
    End Property

    'EVENTOS

    'VARIABLES
    Dim dtEfectivo As New DataTable
    Dim dtCheque As New DataTable
    Dim dtDocumento As New DataTable
    Dim dtRedondeo As New DataTable
    Dim dtCuentaBancaria As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "EFECTIVIZACION", "EFEC")
        vNuevo = True

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        'CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtfecha)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, txtComprobante)

        'Efectivo, Cheques, Otros Cheques
        CSistema.CargaControl(vControles, btnAgregarEfectivo)
        CSistema.CargaControl(vControles, lklEliminarVenta)
        CSistema.CargaControl(vControles, btnAgregarCheque)

        'Forma de Pago
        ' CSistema.CargaControl(vControles, OcxFormaPago1)

        'CARGAR ESTRUCTURA DEL DETALLE VENTA

        'CARGAR CONTROLES
        'Ciudad
        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)


        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "OPERACION", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")


    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "OPERACION", cbxCiudad.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        ''Limpiar detalle

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        CAsiento.Limpiar()

        vNuevo = True

        'Cabecera
        txtComprobante.txt.Clear()

        'detalle
        dgw.Rows.Clear()
        txtTotalEfectivo.txt.Clear()
        TxtCantidadDepositado.txt.Clear()
        dtCheque.Rows.Clear()
        dtEfectivo.Rows.Clear()
        dtDocumento.Rows.Clear()
        dtRedondeo.Rows.Clear()
        txtCliente.Clear()
        txtTitular.Clear()
        txtBanco.Clear()
        txtNroCheque.Clear()
        txtImporte.txt.Clear()
        txtCliente.Clear()
        txtCuentaBancaria.Clear()
        txtSaldo.txt.Clear()
        txtfecha.Clear()

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From Efectivizacion Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)
        dtEfectivo = CSistema.ExecuteToDataTable("Select NroOperacionCobranza,IDTransaccion,ID,Numero,IDTipoComprobante,TipoComprobante,CodigoComprobante,IDSucursal,Comprobante,Fecha,Fec,IDMoneda,Moneda,ImporteMoneda,Cotizacion,Importe,Depositado,Saldo,'Sel'='False','Cancelar'='False',IDCobrador,Cobrador,Generado From VEfectivo Where Cancelado = 'False' And Saldo > 0 Order By Fecha").Copy
        dtCheque = CSistema.ExecuteToDataTable("Select *, 'Sel'='False', 'Cancelar'='False' From VChequeCliente Where (Cartera = 'True' Or Rechazado = 'True') And Saldo > 0 Order By Fecha").Copy


        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        txtComprobante.txt.Text = txtID.txt.Text

        'Poner el foco en el proveedor
        cbxTipoComprobante.cbx.Focus()

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If

        End If

        'Validar
        'Comprobante
        If IsNumeric(txtComprobante.txt.Text) = False Then
            CSistema.MostrarError("Debe Ingresar Solo Numeros", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.BottomRight)
            Exit Function
        End If

        'Validar Detalle
        If dgw.Rows.Count = 0 Then
            CSistema.MostrarError("Dede Ingrsar los Detalles", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.BottomRight)
        End If

        'Ciudad
        If cbxCiudad.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente la ciudad!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Tipo Comprobante
        If cbxTipoComprobante.cbx.Text.Trim = "" Then
            Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
            Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
            ctrError.SetError(cbxTipoComprobante, mensaje)
            ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Sucursal
        If cbxSucursal.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If cbxSucursal.cbx.Text.Trim = "" Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If CDbl(txtImporte.txt.Text) <> CDbl(txtTotalEfectivo.txt.Text) Then
            Dim mensaje As String = "El importe del cheque debe ser igual al Total Efectivo!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        Return True


    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtfecha.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTransaccionCheque", IDTransaccionCheque, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpEfectivizacion", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            Exit Sub
        End If


        Dim Procesar As Boolean = True

        'Guardar detalle
        If IDTransaccion > 0 Then

            'Guardar Efectivo
            Procesar = InsertarDetalle(IDTransaccion)


        End If

        'Aplicar el Deposito Bancario
        '
        Try

            ReDim param(-1)

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            'Aplicar
            If CSistema.ExecuteStoreProcedure(param, "SpEfectivizacionProcesar", False, False, MensajeRetorno) = False Then

            End If


        Catch ex As Exception

        End Try

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False

    End Sub

    Sub CargarVenta(ByVal IDTransaccion As Integer)

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra las ventas asociadas!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        'Limpiar ventas seleccionadas
        ListarComprobantes()

    End Sub

    Sub ListarEfectivo()

        dgw.Rows.Clear()

        Dim Total As Decimal = 0

        For Each oRow As DataRow In dtEfectivo.Rows
            Dim Registro(7) As String

            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("CodigoComprobante").ToString
            Registro(2) = oRow("Fec").ToString
            Registro(3) = oRow("Comprobante").ToString
            Registro(4) = (CSistema.FormatoMoneda(oRow("Importe").ToString))
            Registro(5) = oRow("Generado").ToString
            Registro(6) = ("---")
            Registro(7) = oRow("ID").ToString

            Total = Total + CDec(oRow("Importe").ToString)

            dgw.Rows.Add(Registro)

        Next

        CalcularTotales()


    End Sub

    Sub ListarComprobantes()

        dgw.Rows.Clear()

        Dim Total As Decimal = 0

        For Each oRow As DataRow In dtEfectivo.Rows
            If oRow("Sel") = True Then
                Dim Registro(7) As String

                Registro(0) = oRow("IDTransaccion").ToString
                Registro(1) = oRow("CodigoComprobante").ToString
                Registro(2) = oRow("Fec").ToString
                Registro(3) = oRow("Comprobante").ToString
                Registro(4) = (CSistema.FormatoMoneda(oRow("Importe").ToString))
                Registro(5) = oRow("Generado").ToString
                Registro(6) = ("---")
                Registro(7) = oRow("ID").ToString

                Total = Total + CDec(oRow("Importe").ToString)

                dgw.Rows.Add(Registro)
            End If

        Next

        CalcularTotales()


    End Sub

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtfecha.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTransaccionCheque", IDTransaccionCheque, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Eliminar
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpEfectivizacion", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub EliminarComprobante()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each item As DataGridViewRow In dgw.SelectedRows

            Dim IDTransaccion As String = item.Cells(0).Value
            Dim ID As String = item.Cells(7).Value

            'buscamos en Efectivo
            For Each oRow As DataRow In dtEfectivo.Select(" IDTransaccion = '" & IDTransaccion & "' And ID = '" & ID & "' ")

                oRow("Sel") = False
                Exit For

            Next


        Next

        ListarComprobantes()

    End Sub

    Sub CalcularTotales()

        TxtCantidadDepositado.SetValue(dgw.Rows.Count)

        Dim TotalEfectivo As Decimal = 0
        Dim Saldo As Decimal = 0
        Dim ImporteCheque As Decimal = txtImporte.ObtenerValor

        For Each oRow As DataGridViewRow In dgw.Rows
            TotalEfectivo = TotalEfectivo + CInt(oRow.Cells("colImporte").Value)
        Next

        'Calcula Total Efectivo
        txtTotalEfectivo.txt.Text = TotalEfectivo
        Saldo = ImporteCheque - TotalEfectivo
        txtSaldo.SetValue(Saldo)

    End Sub


    Sub Buscar()

        Dim frm As New frmConsultaEfectivizacion
        frm.ShowDialog()
        CargarOperacion(frm.IDTransaccion)

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From Efectivizacion Where Numero=" & txtID.ObtenerValor & "And IDSucursal = " & cbxSucursal.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VEfectivizacion Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("NroComprobante").ToString
        txtfecha.SetValueFromString(oRow("Fecha").ToString)
        txtCliente.txtReferencia.txt.Text = oRow("Referencia").ToString
        txtCliente.txtRazonSocial.txt.Text = oRow("Cliente").ToString
        txtCliente.txtRUC.txt.Text = oRow("RUC").ToString
        txtTitular.SetValue(oRow("Titular").ToString)
        txtCuentaBancaria.SetValue(oRow("CuentaBancaria").ToString)
        txtNroCheque.SetValue(oRow("NroCheque").ToString)
        txtBanco.SetValue(oRow("Banco").ToString)
        txtImporte.txt.Text = CSistema.FormatoMoneda(oRow("Importe").ToString)
        IDTransaccionCheque = oRow("IDTransaccionCheque").ToString

        'Cargamos el detalle de Comprobantes
        dtEfectivo = CSistema.ExecuteToDataTable("Select * From VDetalleEfectivizacion Where IDTransaccion=" & IDTransaccion & " Order By Fecha").Copy

        ListarEfectivo()


        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            'lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            lblUsuarioAnulado.Text = oRow("UsuarioAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If


        'Calcular Totales
        CalcularTotales()

    End Sub

    Sub CargarEfectivo()

        Dim frm As New frmDepositoBancarioSeleccionarEfectivo
        frm.Text = "Selección de Efectivo"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtEfectivo
        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccionar Efectivo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
        dtEfectivo = frm.dt

        ListarComprobantes()

    End Sub

    Sub AplicarOrdenCompra()

    End Sub

    Sub ObtenerSucursal()

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From Efectivizacion Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From Efectivizacion Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalle = True

        Dim id As Integer = 1

        'Insertar si hay efectivo
        For Each oRow As DataRow In dtEfectivo.Select(" Sel = 'True' ")

            Dim sql As String = "Insert Into DetalleEfectivizacion(IDTransaccion,IDTransaccionCheque,IDTransaccionEfectivo,IDEfectivo,Importe) Values(" & IDTransaccion & "," & IDTransaccionCheque & ", " & oRow("IDTransaccion").ToString & "," & id & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ")"
            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If
            id = id + 1

        Next


    End Function

    Sub CargarCheque()

        Dim frm As New frmSeleccionarChequeEfectivizacion
        frm.Text = "Selección de Cheque"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtCheque
        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccionar Cheque", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
        dtCheque = frm.dt
        ID = frm.ID
        ObtenerDatosCheque()
        btnAgregarEfectivo.Focus()

    End Sub
    Sub ObtenerDatosCheque()
        For Each oRow As DataRow In dtCheque.Select("IDTransaccion=" & ID)
            txtCliente.txtReferencia.txt.Text = oRow("CodigoCliente").ToString
            txtCliente.txtRazonSocial.txt.Text = oRow("Cliente").ToString
            txtCliente.txtRUC.txt.Text = oRow("RUC").ToString
            txtTitular.txt.Text = oRow("Titular").ToString
            txtCuentaBancaria.txt.Text = oRow("CuentaBancaria").ToString
            txtNroCheque.SetValue(oRow("NroCheque").ToString)
            txtBanco.SetValue(oRow("Banco").ToString)
            txtImporte.txt.Text = CSistema.FormatoMoneda(oRow("Importe").ToString)
            IDTransaccionCheque = oRow("IDTransaccion").ToString
        Next
    End Sub

    Sub Imprimir()
        Dim Titulo As String = "EFECTIVIZACION DE CHEQUE"
        Dim frm As New frmReporte
        CReporte.ImprimirEfectivizacion(frm, IDTransaccion, vgUsuarioIdentificador, Titulo)
    End Sub

    Private Sub frmDepositoBancario_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmDepositoBancario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged
        ObtenerSucursal()
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub btnAgregarEfectivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarEfectivo.Click
        CargarEfectivo()
    End Sub

    Private Sub lklEliminarVenta_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarVenta.LinkClicked
        EliminarComprobante()
    End Sub

    Private Sub frmDepositoBancario_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub


    Private Sub btnAgregarCheque_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarCheque.Click
        CargarCheque()
    End Sub

    Private Sub cbxCiudad_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxCiudad.TeclaPrecionada
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmEfectivizacion_Activate()
        Me.Refresh()
    End Sub
End Class
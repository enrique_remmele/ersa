﻿Public Class frmConsultaEfectivizacion

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim dtBanco As DataTable
    Dim dtCliente As DataTable
    Dim dtOperacion As DataTable

    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Form

        'TextBox
        txtCantidadCheque.txt.ResetText()
        txtCantidadDocumentos.txt.ResetText()
        txtOperacion.txt.ResetText()
        txtTotalCheques.txt.ResetText()

        'CheckBox
        chkBanco.Checked = False
        chkBancoSucursal.Checked = False
        chkCliente.Checked = False
        chkClienteSucursal.Checked = False
        chkFecha.Checked = False
        chkFechaSucursal.Checked = False

        'ComboBox
        cbxBanco.Enabled = False
        cbxBancoSucursal.Enabled = False
        cbxCliente.Enabled = False
        cbxClienteSucursal.Enabled = False

        'datagridView
        dgwEfectivo.Rows.Clear()
        dgwOperacion.Rows.Clear()


        'Funciones
        CargarInformacion()

        'Foco
        txtOperacion.txt.Focus()
        txtOperacion.txt.SelectAll()
    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'Banco
        dtBanco = CSistema.ExecuteToDataTable("Select ID, Descripcion From Banco Order By 2").Copy
        CSistema.SqlToComboBox(cbxBanco, dtBanco.Copy, "ID", "Descripcion")
        CSistema.SqlToComboBox(cbxBancoSucursal, dtBanco.Copy, "ID", "Descripcion")

        'Cliente
        dtCliente = CSistema.ExecuteToDataTable("Select ID, RazonSocial From Cliente Order By 2").Copy
        CSistema.SqlToComboBox(cbxCliente, dtCliente.Copy, "ID", "RazonSocial")
        CSistema.SqlToComboBox(cbxClienteSucursal, dtCliente.Copy, "ID", "RazonSocial")


        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

        'Cliente
        chkCliente.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO", "False")
        chkClienteSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO SUCRUSAL", "False")
        cbxCliente.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE", "")
        cbxClienteSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE SUCURSAL", "")

        'Banco
        chkBanco.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO", "False")
        chkBancoSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO SUCURSAL", "False")
        cbxBanco.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO", "")
        cbxBancoSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO SUCURSAL", "")

        'dtOperacion = CSistema.ExecuteToDataTable("Select Numero, Cliente, Banco, 'Fecha'=Fec,NroCheque, Importe, Saldo, Estado From VChequeCliente")

    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Cliente
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO", chkCliente.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO SUCURSAL", chkClienteSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE", cbxCliente.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE SUCURSAL", cbxClienteSucursal.Text)

        'Banco
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO", chkBanco.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO SUCURSAL", chkBancoSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO", cbxBanco.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO SUCURSAL", cbxBancoSucursal.Text)


    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Operaciones
    Sub ListarOperaciones(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "", Optional ByVal Sucursal As Boolean = False)

        ctrError.Clear()

        Where = Condicion


        'Solo por numero
        If Numero > 0 Then
            Where = " Where Numero = " & Numero & ""
        End If

        If Sucursal = True Then
            If Where = "" Then
                Where = " Where (IDSucursal=" & cbxSucursal.SelectedValue & ") "
            Else
                Where = Where & " And (IDSucursal=" & cbxSucursal.SelectedValue & ") "
            End If
        End If

        'Cliente
        If Sucursal = True Then
            If EstablecerCondicion(cbxClienteSucursal, chkClienteSucursal, "IDCliente", "Seleccione correctamente el cliente!") = False Then
                Exit Sub
            End If
        Else
            If EstablecerCondicion(cbxCliente, chkCliente, "IDCliente", "Seleccione correctamente el cliente!") = False Then
                Exit Sub
            End If
        End If

        'Banco
        If Sucursal = True Then
            If EstablecerCondicion(cbxBancoSucursal, chkBancoSucursal, "IDBanco", "Seleccione correctamente el banco!") = False Then
                Exit Sub
            End If
        Else
            If EstablecerCondicion(cbxBanco, chkBanco, "IDBanco", "Seleccione correctamente el banco!") = False Then
                Exit Sub
            End If
        End If

        'Fecha
        If Sucursal = True Then
            If chkFechaSucursal.Checked = True Then
                If Where = "" Then
                    Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesdeSucursal.GetValueString, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHastaSucursal.GetValueString, True, False) & "' ) "
                Else
                    Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesdeSucursal.GetValueString, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHastaSucursal.GetValueString, True, False) & "' )  "
                End If
            End If

        Else

            If chkFecha.Checked = True Then
                If Where = "" Then
                    Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.GetValueString, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.GetValueString, True, False) & "' ) "
                Else
                    Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.GetValueString, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.GetValueString, True, False) & "' )  "
                End If
            End If

        End If

        dgwOperacion.Rows.Clear()
        dgwEfectivo.Rows.Clear()


        dtOperacion = CSistema.ExecuteToDataTable("Select * From VEfectivizacion" & Where)

        'Limpiar la grilla
        dgwOperacion.Rows.Clear()
        Dim TotalCheques As Decimal = 0
        Dim TotalCantidad As Decimal = 0

        For Each oRow As DataRow In dtOperacion.Rows
            Dim Registro(10) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Numero").ToString
            Registro(2) = oRow("NroCheque").ToString
            Registro(3) = oRow("Fec").ToString
            Registro(4) = oRow("Sucursal").ToString
            Registro(5) = oRow("Cliente").ToString
            Registro(6) = oRow("Titular").ToString
            Registro(7) = oRow("CuentaBancaria").ToString
            Registro(8) = oRow("Banco").ToString
            Registro(9) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(10) = oRow("Estado").ToString

            'Sumar el total del saldo
            TotalCheques = TotalCheques + CDec(oRow("Importe").ToString)
            TotalCantidad = TotalCantidad + 1

            dgwOperacion.Rows.Add(Registro)

        Next

        txtTotalCheques.SetValue(TotalCheques)
        txtCantidadCheque.SetValue(dtOperacion.Rows.Count)


        txtCantidadCheque.txt.Text = dgwOperacion.Rows.Count

    End Sub

    Sub ListarEfectivos()

        ctrError.Clear()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Limpiar ListView
        dgwEfectivo.Rows.Clear()

        Dim IDTransaccion As String = dgwOperacion.SelectedRows(0).Cells("colIDTransaccion").Value
        Dim dtEfectivo As DataTable = CSistema.ExecuteToDataTable("Select * From VDetalleEfectivizacion Where IDTransaccion=" & IDTransaccion)


        dgwEfectivo.Rows.Clear()

        Dim TotalEfectivos As Decimal = 0
        Dim CantidadEfectivos As Decimal = 0

        For Each oRow As DataRow In dtEfectivo.Rows
            Dim Registro(6) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("CodigoComprobante").ToString
            Registro(2) = oRow("Fec").ToString
            Registro(3) = oRow("Comprobante").ToString
            Registro(4) = (CSistema.FormatoMoneda(oRow("Importe").ToString))
            Registro(5) = oRow("Generado").ToString
            Registro(6) = ("---")
            TotalEfectivos = TotalEfectivos + CDec(oRow("Importe").ToString)
            CantidadEfectivos = CantidadEfectivos + 1

            dgwEfectivo.Rows.Add(Registro)
        Next
        txtTotalEfectivos.SetValue(TotalEfectivos)
        txtCantidadDocumentos.SetValue(CantidadEfectivos)


    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = CInt(dgwOperacion.SelectedRows(0).Cells(0).Value)

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub frmConsultaChequeCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaChequeCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmConsultaChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkCliente_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCliente.CheckedChanged
        HabilitarControles(chkCliente, cbxCliente)
    End Sub

    Private Sub chkBanco_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBanco.CheckedChanged
        HabilitarControles(chkBanco, cbxBanco)
    End Sub

    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged
        HabilitarControles(chkFecha, txtFechaDesde)
        HabilitarControles(chkFecha, txtFechaHasta)
    End Sub

    Private Sub chkClienteSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkClienteSucursal.CheckedChanged
        HabilitarControles(chkClienteSucursal, cbxClienteSucursal)
    End Sub

    Private Sub chkBancoSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBancoSucursal.CheckedChanged
        HabilitarControles(chkBancoSucursal, cbxBancoSucursal)
    End Sub

    Private Sub chkFechaSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFechaSucursal.CheckedChanged
        HabilitarControles(chkFechaSucursal, txtFechaDesdeSucursal)
        HabilitarControles(chkFechaSucursal, txtFechaHastaSucursal)
    End Sub

    Private Sub btn13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn13.Click
        ListarOperaciones(0, "", True)
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarOperaciones(txtOperacion.ObtenerValor)
            txtOperacion.txt.Focus()
            txtOperacion.txt.SelectAll()
        End If
    End Sub

    Private Sub dgwOperacion_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgwOperacion.CellClick
        ListarEfectivos()
    End Sub

    Private Sub dgwOperacion_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgwOperacion.DoubleClick
        SeleccionarRegistro()
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        ListarOperaciones()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmConsultaEfectivizacion_Activate()
        Me.Refresh()
    End Sub
End Class
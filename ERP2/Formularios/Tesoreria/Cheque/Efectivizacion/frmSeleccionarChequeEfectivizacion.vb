﻿Public Class frmSeleccionarChequeEfectivizacion

    'CLASES
    Dim CSistema As New CSistema
    Public ID As Integer

    'EVENTOS
    Public Event VentasSeleccionadas(ByVal sender As Object, ByVal e As EventArgs)

    'PROPIEDADES

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property
    'Public Property ID As Integer
    '    Get
    '        Return IDValue
    '    End Get
    '    Set(ByVal value As Integer)
    '        IDValue = ID
    '    End Set
    'End Property



    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Txt
        txtDesde.SetValueFromString("01" & "/" & Date.Now.Month & "/" & Date.Now.Year)
        txtHasta.SetValue(Date.Now)

        'Funciones
        Listar()
        'Cargar()

        'Foco
        dgw.Focus()

    End Sub

    Sub Listar()

        'Limpiar la grilla
        dgw.Rows.Clear()
        Dim TotalDeuda As Decimal = 0

        For Each oRow As DataRow In dt.Select(" NroCheque Like '%" & txtComprobante.GetValue & "%' And (Fecha >= '" & txtDesde.GetValueString & "' And Fecha <= '" & txtHasta.GetValueString & "')")

            Dim oRow1(7) As String

            oRow1(0) = oRow("IDTransaccion").ToString
            oRow1(1) = oRow("Banco").ToString
            oRow1(2) = oRow("NroCheque").ToString
            oRow1(3) = oRow("CodigoTipo").ToString
            oRow1(4) = oRow("Cliente").ToString
            oRow1(5) = oRow("Fec").ToString
            oRow1(6) = oRow("Vencimiento").ToString
            oRow1(7) = CSistema.FormatoMoneda(oRow("Total").ToString)

            'Sumar el total de la deuda
            TotalDeuda = TotalDeuda + CDec(oRow("Saldo").ToString)

            dgw.Rows.Add(oRow1)

        Next

        CalcularTotales()


    End Sub

    Sub Seleccionar()

        For Each oRow As DataGridViewRow In dgw.SelectedRows

            ID = oRow.Cells("colIDTransaccion").Value

            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & ID)
                oRow1("IDTransaccion") = oRow.Cells("colIDTransaccion").Value
                oRow1("Total") = oRow.Cells("colTotal").Value
            Next

        Next

        '
        Me.Close()

    End Sub

    Sub CalcularTotales()

        Dim TotalComprobantes As Decimal = 0
        Dim TotalSeleccionado As Decimal = 0
        Dim CantidadComprobantes As Integer = 0
        Dim CantidadSeleccionado As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows
            TotalComprobantes = TotalComprobantes + oRow.Cells("colTotal").Value
            CantidadComprobantes = CantidadComprobantes + 1
        Next

        txtTotalComprobantes.SetValue(TotalComprobantes)
        txtCantidadComprobantes.SetValue(CantidadComprobantes)

        txtTotalSelecionado.SetValue(TotalSeleccionado)
        txtCantidadSeleccionado.SetValue(CantidadSeleccionado)

    End Sub

    Sub PintarCelda()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(1).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next



        If dgw.CurrentRow Is Nothing Then
            Exit Sub
        End If

        dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Columns(e.ColumnIndex).Name = "colImporte" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value

            Else
                dgw.CurrentCell.Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells("colImporte").Value)
            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgw.DoubleClick
        Seleccionar()
    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Space Then
            Seleccionar()
        End If
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells("colTotal")
        End If

    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = True
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = False
        Next

        PintarCelda()
        CalcularTotales()
    End Sub

    Private Sub TableLayoutPanel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles TableLayoutPanel1.Paint

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub frmDepositoBancarioSeleccionarCheque_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If dgw.Focused = True Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmSeleccionarChequeEfectivizacion_Activate()
        Me.Refresh()
    End Sub
End Class
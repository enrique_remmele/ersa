﻿Public Class frmChequeCliente

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES    
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property
    Public Property RowCliente As DataRow

    'EVENTOS

    'VARIABLES
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim dtCuentaBancaria As DataTable

    'FUNCIONES
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        'txtCliente.Conectar()

        'Propiedades
        IDTransaccion = 0
        vNuevo = True

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "CHEQUE CLIENTE", "CHQC")
        CargarInformacion()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        'txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        ''Foco
        'txtID.txt.Focus()
        'txtID.txt.SelectAll()

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'Combrobante
        'CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, cbxBanco)
        CSistema.CargaControl(vControles, cbxNroCuentaBancaria)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtNroCheque)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, txtCotizacion)
        CSistema.CargaControl(vControles, txtImporteMoneda)
        'CSistema.CargaControl(vControles, txtImporte)
        CSistema.CargaControl(vControles, chkDiferido)
        CSistema.CargaControl(vControles, txtVencimiento)
        CSistema.CargaControl(vControles, chkChequeTercero)
        CSistema.CargaControl(vControles, txtLiberador)
        CSistema.CargaControl(vControles, txtTitular)
        CSistema.CargaControl(vControles, txtFechaCobranza)
        CSistema.CargaControl(vControles, cbxDocumentosPagados)


        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select Distinct ID, Codigo  From VSucursal Order By 2")

        'Banco
        CSistema.SqlToComboBox(cbxBanco.cbx, "Select ID, Descripcion  From Banco Order By 2")

        ''Cuentas Bancarias
        'dtCuentaBancaria = CSistema.ExecuteToDataTable("Select * From ClienteCuentaBancaria")

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Referencia From Moneda")
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'BAnco
        cbxBanco.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "BANCO", "")

        'Moneda
        cbxMoneda.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MONEDA", "")

        'ID
        'txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From ChequeCliente),1)"), Integer)

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxSucursal.cbx.Text)

        'Banco
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "BANCO", cbxBanco.cbx.Text)

        'Moneda
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", cbxMoneda.cbx.Text)

    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True

        'Controles
        txtCliente.Clear()
        cbxNroCuentaBancaria.cbx.Text = ""
        txtFecha.txt.Clear()
        txtNroCheque.txt.Clear()
        txtImporteMoneda.txt.Text = "0"
        txtImporte.txt.Clear()
        txtTitular.txt.Clear()

        chkDiferido.Checked = False
        txtVencimiento.txt.Clear()
        chkChequeTercero.Checked = False
        txtLiberador.txt.Clear()
        txtSaldo.txt.Text = 0
        txtEstado.txt.Clear()
        cbxNroCuentaBancaria.cbx.DataSource = Nothing
        cbxNroCuentaBancaria.cbx.Items.Clear()
        cbxNroCuentaBancaria.cbx.Text = ""
        txtFechaCobranza.Clear()
        cbxDocumentosPagados.cbx.DataSource = Nothing
        cbxDocumentosPagados.SoloLectura = True

        cbxMoneda.SelectedValue(1)

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From ChequeCliente Where IDSucursal=" & cbxSucursal.GetValue & "),1)"), Integer)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Poner el foco en el cliente
        txtCliente.Clear()
        txtCliente.Focus()

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False
        Dim Keyascii As Integer

        'Validar

        'Sucursal
        If cbxSucursal.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Cliente
        If txtCliente.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Banco
        If cbxBanco.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente el banco!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Nro de Cheque
        If txtNroCheque.txt.Text.Trim = "" Then
            Dim mensaje As String = "El Nro del Cheque no es valido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If Not IsNumeric(txtNroCheque.txt.Text) Then
            Dim mensaje As String = "Ingrese solo numeros para el Nro de Cheque!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Importe
        If CDec(txtImporteMoneda.ObtenerValor) <= 0 Then
            Dim mensaje As String = "El importe del documento no es valido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Control para Tesorería agregado en Fecha 27022024 JCA
        If Not IsNumeric(cbxNroCuentaBancaria.cbx.Text) Then
            Dim mensaje As String = "La Cuenta Bancaria debe contener 12 dígitos y no tener Caracteres Especiales"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If Len(cbxNroCuentaBancaria.cbx.Text) <> 12 Then
            Dim mensaje As String = "La Cuenta Bancaria debe contener 12 dígitos"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'If (cbxNroCuentaBancaria.cbx.Text) Then
        '    Dim mensaje As String = "La Cuenta Bancaria no debe tener caracteres"
        '    ctrError.SetError(btnGuardar, mensaje)
        '    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
        '    tsslEstado.Text = mensaje
        '    Exit Function
        'End If

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)


        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim vIDTransaccion As Integer = 0
        Dim IndiceOperacion As Integer = 0

        If txtCotizacion.ObtenerValor = 0 Then
            txtCotizacion.SetValue(1)
        End If

        'Entrada
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalDocumento", cbxSucursal.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDBanco", cbxBanco.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaBancaria", cbxNroCuentaBancaria.cbx.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaCobranza", CSistema.FormatoFechaBaseDatos(txtFechaCobranza.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroCheque", txtNroCheque.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", txtCotizacion.ObtenerValor, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(txtImporte.ObtenerValor, txtImporte.Decimales), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(txtImporteMoneda.ObtenerValor, txtImporteMoneda.Decimales), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ImporteMoneda", CSistema.FormatoMonedaBaseDatos(txtImporteMoneda.ObtenerValor, txtImporteMoneda.Decimales), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Diferido", chkDiferido.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Titular", txtTitular.GetValue, ParameterDirection.Input)

        If chkDiferido.Checked = True Then
            CSistema.SetSQLParameter(param, "@FechaVencimiento", CSistema.FormatoFechaBaseDatos(txtVencimiento.GetValue, True, False), ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@ChequeTercero", chkChequeTercero.Checked.ToString, ParameterDirection.Input)

        If chkChequeTercero.Checked = True Then
            CSistema.SetSQLParameter(param, "@Librador", txtLiberador.txt.Text.Trim, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpChequeCliente", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From ChequeCliente Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpChequeCliente", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False

    End Sub

    Sub Eliminar()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para eliminar!"
            ctrError.SetError(btnEliminar, mensaje)
            ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.DEL.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Eliminar
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpChequeCliente", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnEliminar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub Buscar()

        Dim frm As New frmConsultaChequeCliente
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog()
        CargarOperacion(frm.IDTransaccion)

    End Sub

    Sub ListarCuentaBancaria()

        'Seleccion de Cliente


        Try
            Dim IDCliente As String = txtCliente.Registro("ID").ToString
            Dim IDBanco As String = cbxBanco.cbx.SelectedValue

            cbxNroCuentaBancaria.cbx.Items.Clear()
            cbxNroCuentaBancaria.cbx.Text = ""

            If IsNumeric(IDCliente) = False Then
                Exit Sub
            End If

            If IsNumeric(IDBanco) = False Then
                Exit Sub
            End If

            For Each oRow As DataRow In dtCuentaBancaria.Select("IDCliente=" & IDCliente & " And IDBanco=" & IDBanco)
                cbxNroCuentaBancaria.cbx.Items.Add(oRow("CuentaBancaria").ToString)
            Next

            cbxNroCuentaBancaria.cbx.SelectedIndex = 0

        Catch ex As Exception

        End Try

    End Sub

    Sub ListarDocumentosPagados(ByVal IDTransaccion As Integer)

        cbxDocumentosPagados.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        Dim sql As String = "Select IDTransaccion, Comprobante From VChequeClienteDocumentosPagados Where IDTransaccion=" & IDTransaccion
        CSistema.SqlToComboBox(cbxDocumentosPagados.cbx, sql)

        cbxDocumentosPagados.SoloLectura = False

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From ChequeCliente Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VChequeCliente Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas técnicos."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        cbxSucursal.cbx.Text = oRow("CodigoSucursal").ToString
        txtID.txt.Text = oRow("Numero").ToString
        txtCliente.txtReferencia.txt.Text = oRow("CodigoCliente").ToString
        txtCliente.txtRazonSocial.txt.Text = oRow("Cliente").ToString
        txtCliente.txtRUC.txt.Text = oRow("RUC").ToString
        cbxBanco.cbx.Text = oRow("Banco").ToString
        cbxNroCuentaBancaria.cbx.Text = oRow("CuentaBancaria").ToString
        txtFecha.SetValue(CDate(oRow("Fecha").ToString))
        txtNroCheque.txt.Text = oRow("NroCheque").ToString
        cbxMoneda.cbx.Text = oRow("Moneda").ToString
        txtCotizacion.txt.Text = oRow("Cotizacion").ToString
        txtImporteMoneda.txt.Text = oRow("ImporteMoneda").ToString
        txtImporte.txt.Text = CSistema.FormatoMoneda(oRow("Importe").ToString * oRow("Cotizacion").ToString, 0)
        txtTitular.SetValue(oRow("Titular").ToString)

        txtFechaCobranza.SetValue(oRow("FechaCobranza").ToString)

        chkDiferido.Checked = CBool(oRow("Diferido").ToString)

        If chkDiferido.Checked = True Then
            txtVencimiento.SetValue(CDate(oRow("FechaVencimiento").ToString))
        Else
            txtVencimiento.txt.Clear()
        End If

        chkChequeTercero.Checked = CBool(oRow("ChequeTercero").ToString)

        If chkChequeTercero.Checked = True Then
            txtLiberador.txt.Text = oRow("Librador").ToString
        Else
            txtLiberador.txt.Clear()
        End If

        txtSaldo.SetValue(oRow("Saldo").ToString)
        txtEstado.txt.Text = oRow("Estado").ToString

        'Cargar Documentos Asociados
        ListarDocumentosPagados(IDTransaccion)

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VChequeCliente Where IDSucursal=" & cbxSucursal.GetValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VChequeCliente Where IDSucursal=" & cbxSucursal.GetValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnEliminar, New Button, btnBusquedaAvanzada, New Button, vControles, New Button)

    End Sub

    Private Sub frmChequeCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
        LiberarMemoria()
    End Sub

    Private Sub frmChequeCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado
        If vNuevo = True Then
            txtTitular.txt.Focus()
            ListarCuentaBancaria()
        End If
        RowCliente = txtCliente.Registro
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

    Private Sub cbxBanco_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxBanco.PropertyChanged
        ListarCuentaBancaria()
    End Sub

    Private Sub txtImporte_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtImporteMoneda.TeclaPrecionada
        txtImporte.SetValue(CSistema.Cotizador(txtCotizacion.ObtenerValor, txtImporteMoneda.ObtenerValor, cbxMoneda.cbx.SelectedValue))
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub cbxSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        'txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub cbxSucursal_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxSucursal.TeclaPrecionada
        'txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub cbxMoneda_Leave(sender As System.Object, e As System.EventArgs) Handles cbxMoneda.Leave
        CSistema.SetIDMoneda(cbxMoneda, txtImporteMoneda)
        If vNuevo Then
            If cbxMoneda.cbx.SelectedValue = 1 Then
                txtCotizacion.txt.Text = 1
                txtCotizacion.Enabled = False
            Else
                txtCotizacion.Enabled = True

            End If
        End If
    End Sub

    Private Sub cbxMoneda_PropertyChanged(sender As Object, e As System.EventArgs) Handles cbxMoneda.PropertyChanged
        CSistema.SetIDMoneda(cbxMoneda, txtImporteMoneda)
        If vNuevo Then
            If cbxMoneda.cbx.SelectedValue = 1 Then
                txtCotizacion.txt.Text = 1
                txtCotizacion.Enabled = False
            Else
                txtCotizacion.Enabled = True

            End If
        End If
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmChequeCliente_Activate()
        Me.Refresh()
    End Sub

    Private Sub cbxNroCuentaBancaria_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cbxNroCuentaBancaria.KeyPress

        'Verifica si el carácter ingresado no es una letra, número o un carácter especial deseado
        Dim KeyAscii As Integer
        'Select Case KeyAscii
        '    Case 48 To 57
        '    Case Else
        '        KeyAscii = 0
        'End Select




    End Sub


End Class
﻿Public Class frmConsultaChequeCliente

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim dtBanco As DataTable
    Dim dtCliente As DataTable
    Dim dtOperacion As DataTable

    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Form

        'TextBox
        txtCantidadCheque.txt.ResetText()
        txtCantidadDocumentos.txt.ResetText()
        txtNroCheque.txt.ResetText()
        txtOperacion.txt.ResetText()
        txtTotalCheques.txt.ResetText()
        txtTotalSaldo.txt.ResetText()

        'CheckBox
        chkBanco.Checked = False
        chkBancoSucursal.Checked = False
        chkCliente.Checked = False
        chkClienteSucursal.Checked = False
        chkFecha.Checked = False
        chkFechaSucursal.Checked = False
        chkTipo.Checked = False
        chkTipoSucursal.Checked = False

        'ComboBox
        cbxBanco.Enabled = False
        cbxBancoSucursal.Enabled = False
        cbxCliente.Enabled = False
        cbxClienteSucursal.Enabled = False
        cbxTipo.Enabled = False
        cbxTipoSucursal.Enabled = False

        'datagridView
        dgwDocumento.Rows.Clear()
        dgwOperacion.Rows.Clear()


        'Funciones
        CargarInformacion()

        'Foco
        txtOperacion.txt.Focus()
        txtOperacion.txt.SelectAll()
    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'Banco
        dtBanco = CSistema.ExecuteToDataTable("Select ID, Descripcion From Banco Order By 2").Copy
        CSistema.SqlToComboBox(cbxBanco, dtBanco.Copy, "ID", "Descripcion")
        CSistema.SqlToComboBox(cbxBancoSucursal, dtBanco.Copy, "ID", "Descripcion")

        'Cliente
        dtCliente = CSistema.ExecuteToDataTable("Select ID, RazonSocial From Cliente Order By 2").Copy
        CSistema.SqlToComboBox(cbxCliente, dtCliente.Copy, "ID", "RazonSocial")
        CSistema.SqlToComboBox(cbxClienteSucursal, dtCliente.Copy, "ID", "RazonSocial")

        'Tipo
        cbxTipo.Items.Add("AL DIA")
        cbxTipo.Items.Add("DIFERIDO")
        cbxTipo.DropDownStyle = ComboBoxStyle.DropDownList

        cbxTipoSucursal.Items.Add("AL DIA")
        cbxTipoSucursal.Items.Add("DIFERIDO")
        cbxTipoSucursal.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

        'Cliente
        chkCliente.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO", "False")
        chkClienteSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO SUCRUSAL", "False")
        cbxCliente.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE", "")
        cbxClienteSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE SUCURSAL", "")

        'Banco
        chkBanco.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO", "False")
        chkBancoSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO SUCURSAL", "False")
        cbxBanco.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO", "")
        cbxBancoSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO SUCURSAL", "")

        'dtOperacion = CSistema.ExecuteToDataTable("Select Numero, Cliente, Banco, 'Fecha'=Fec,NroCheque, Importe, Saldo, Estado From VChequeCliente")
        'dtOperacion = CSistema.ExecuteToDataTable("Select CC.IDTransaccion,Numero,Cliente, Banco,'Fecha'=Fec,NroCheque,Importe,Saldo, Estado,CDD.FechaDeposito, CDD.NumeroDeposito, CDD.ComprobanteDeposito, CDD.NumeroDescuento, CDD.FechaDescuento From VChequeCliente CC join VChequeClienteDepositadoDescontado CDD on CDD.idtransaccion = CC.IDTransaccion")
        dtOperacion = CSistema.ExecuteToDataTable("Select IDTransaccion,Numero,Cliente, Banco,Fecha,NroCheque,Importe,Saldo, Estado, FechaDeposito, NumeroDeposito, ComprobanteDeposito, NumeroDescuento, FechaDescuento, NroVencimiento From VChequeClienteConsulta")

    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Cliente
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO", chkCliente.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO SUCURSAL", chkClienteSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE", cbxCliente.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE SUCURSAL", cbxClienteSucursal.Text)

        'Banco
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO", chkBanco.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO SUCURSAL", chkBancoSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO", cbxBanco.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO SUCURSAL", cbxBancoSucursal.Text)

        'Tipo
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO ACTIVO", chkTipo.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO ACTIVO SUCURSAL", chkTipoSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO", cbxTipo.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO SUCURSAL", cbxTipoSucursal.Text)


    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Operaciones
    Sub ListarOperaciones(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "", Optional ByVal Sucursal As Boolean = False)

        ctrError.Clear()

        Where = Condicion


        'Solo por numero
        If Numero > 0 Then
            Where = " Where Numero = " & Numero & ""
        End If

        If IsNumeric(txtNroCheque.txt.Text) > 0 Then
            Where = "Where NroCheque like '%" & txtNroCheque.txt.Text & "'"
        End If

        If Sucursal = True Then
            If Where = "" Then
                Where = " Where (IDSucursal=" & cbxSucursal.SelectedValue & ") "
            Else
                Where = Where & " And (IDSucursal=" & cbxSucursal.SelectedValue & ") "
            End If
        End If

        'Cliente
        If Sucursal = True Then
            If EstablecerCondicion(cbxClienteSucursal, chkClienteSucursal, "IDCliente", "Seleccione correctamente el cliente!") = False Then
                Exit Sub
            End If
        Else
            If EstablecerCondicion(cbxCliente, chkCliente, "IDCliente", "Seleccione correctamente el cliente!") = False Then
                Exit Sub
            End If
        End If

        'Banco
        If Sucursal = True Then
            If EstablecerCondicion(cbxBancoSucursal, chkBancoSucursal, "IDBanco", "Seleccione correctamente el banco!") = False Then
                Exit Sub
            End If
        Else
            If EstablecerCondicion(cbxBanco, chkBanco, "IDBanco", "Seleccione correctamente el banco!") = False Then
                Exit Sub
            End If
        End If

        'Tipo
        If Sucursal = True Then
            If chkTipoSucursal.Checked = True Then
                If Where = "" Then
                    Where = " Where (Tipo='" & cbxTipoSucursal.Text & "') "
                Else
                    Where = Where & " And (Tipo='" & cbxTipoSucursal.Text & "')  "
                End If
            End If
        Else

            If chkTipo.Checked = True Then
                If Where = "" Then
                    Where = " Where (Tipo='" & cbxTipo.Text & "') "
                Else
                    Where = Where & " And (Tipo='" & cbxTipo.Text & "')  "
                End If
            End If

        End If

        'Fecha
        If Sucursal = True Then
            If chkFechaSucursal.Checked = True Then
                If Where = "" Then
                    Where = " Where (Fec  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesdeSucursal.GetValue, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHastaSucursal.GetValue, True, False) & "' ) "
                    'Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesdeSucursal.GetValueString, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHastaSucursal.GetValueString, True, False) & "' ) "
                Else
                    Where = Where & " And (Fec  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesdeSucursal.GetValue, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHastaSucursal.GetValue, True, False) & "' )  "
                    'Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesdeSucursal.GetValueString, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHastaSucursal.GetValueString, True, False) & "' )  "
                End If
            End If

        Else

            If chkFecha.Checked = True Then
                If Where = "" Then
                    Where = " Where (Fec  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.GetValue, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.GetValue, True, False) & "' ) "
                    'Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.GetValueString, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.GetValueString, True, False) & "' ) "
                Else
                    Where = Where & " And (Fec  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.GetValue, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.GetValue, True, False) & "' )  "
                    'Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.GetValueString, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.GetValueString, True, False) & "' )  "
                End If
            End If

        End If

        dgwOperacion.Rows.Clear()
        dgwDocumento.Rows.Clear()


        'dtOperacion = CSistema.ExecuteToDataTable("Select IDTransaccion,Numero,Cliente, Banco,'Fecha'=Fec,NroCheque,Importe,Saldo, Estado From VChequeCliente" & Where)
        'dtOperacion = CSistema.ExecuteToDataTable("Select CC.IDTransaccion,Numero,Cliente, Banco,'Fecha'=Fec,NroCheque,Importe,Saldo, Estado,CDD.FechaDeposito, CDD.NumeroDeposito, CDD.ComprobanteDeposito, CDD.NumeroDescuento, CDD.FechaDescuento From VChequeCliente CC join VChequeClienteDepositadoDescontado CDD on CDD.idtransaccion = CC.IDTransaccion" & Where)
        dtOperacion = CSistema.ExecuteToDataTable("Select IDTransaccion,Numero,Cliente, Banco,Fecha,NroCheque,Importe,Saldo, Estado, FechaDeposito, NumeroDeposito, ComprobanteDeposito, NumeroDescuento, FechaDescuento, NroVencimiento From VChequeClienteConsulta" & Where)
        'dtOperacion = CSistema.ExecuteToDataTable("Select CC.IDTransaccion,CC.Numero,CC.Cliente, CC.Banco,'Fecha'=CC.Fec,CC.NroCheque,CC.Importe,CC.Saldo, CC.Estado,CDD.FechaDeposito, CDD.NumeroDeposito, CDD.ComprobanteDeposito, CDD.NumeroDescuento, CDD.FechaDescuento, 'NroVencimiento' = VCD.Numero From VChequeCliente CC join VChequeClienteDepositadoDescontado CDD on CDD.idtransaccion = CC.IDTransaccion Join VVencimientoChequeDescontado VCD on VCD.IDTransaccionCheque = CC.IDTransaccion" & Where)

        'Limpiar la grilla
        dgwOperacion.Rows.Clear()
        Dim TotalCheques As Decimal = 0
        Dim TotalSaldo As Decimal = 0

        For Each oRow As DataRow In dtOperacion.Rows
            Dim Registro(15) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Numero").ToString
            Registro(2) = oRow("Cliente").ToString
            Registro(3) = oRow("Banco").ToString
            Registro(4) = oRow("Fecha").ToString
            Registro(5) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(6) = oRow("NroCheque").ToString
            Registro(7) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            Registro(8) = oRow("Estado").ToString
            Registro(9) = oRow("FechaDeposito").ToString
            Registro(10) = oRow("NumeroDeposito").ToString
            Registro(11) = oRow("ComprobanteDeposito").ToString
            Registro(12) = oRow("NumeroDescuento").ToString
            Registro(13) = oRow("FechaDescuento").ToString
            Registro(14) = oRow("NroVencimiento").ToString
            'Sumar el total del saldo
            TotalCheques = TotalCheques + CDec(oRow("Importe").ToString)
            TotalSaldo = TotalSaldo + CDec(oRow("Saldo").ToString)

            dgwOperacion.Rows.Add(Registro)

        Next

        txtTotalCheques.SetValue(TotalCheques)
        txtTotalSaldo.SetValue(TotalSaldo)
        txtCantidadCheque.SetValue(dtOperacion.Rows.Count)


        txtCantidadCheque.txt.Text = dgwOperacion.Rows.Count

    End Sub
    'Listar x NroCheque
    Sub ListarOperacionesxNroCheque(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "", Optional ByVal Sucursal As Boolean = False)

        ctrError.Clear()

        Where = Condicion

        'Solo por numero
        If Numero > 0 Then
            'Where = " Where NroCheque = '" & Numero & "'"
            Where = " Where NroCheque like'%" & Numero & "%'"
        End If

        If Sucursal = True Then
            If Where = "" Then
                Where = " Where (IDSucursal=" & cbxSucursal.SelectedValue & ") "
            Else
                Where = Where & " And (IDSucursal=" & cbxSucursal.SelectedValue & ") "
            End If
        End If

        'Cliente
        If Sucursal = True Then
            If EstablecerCondicion(cbxClienteSucursal, chkClienteSucursal, "IDCliente", "Seleccione correctamente el cliente!") = False Then
                Exit Sub
            End If
        Else
            If EstablecerCondicion(cbxCliente, chkCliente, "IDCliente", "Seleccione correctamente el cliente!") = False Then
                Exit Sub
            End If
        End If

        'Banco
        If Sucursal = True Then
            If EstablecerCondicion(cbxBancoSucursal, chkBancoSucursal, "IDBanco", "Seleccione correctamente el banco!") = False Then
                Exit Sub
            End If
        Else
            If EstablecerCondicion(cbxBanco, chkBanco, "IDBanco", "Seleccione correctamente el banco!") = False Then
                Exit Sub
            End If
        End If

        'Tipo
        If Sucursal = True Then
            If chkTipoSucursal.Checked = True Then
                If Where = "" Then
                    Where = " Where (Tipo='" & cbxTipoSucursal.Text & "') "
                Else
                    Where = Where & " And (Tipo='" & cbxTipoSucursal.Text & "')  "
                End If
            End If
        Else

            If chkTipo.Checked = True Then
                If Where = "" Then
                    Where = " Where (Tipo='" & cbxTipo.Text & "') "
                Else
                    Where = Where & " And (Tipo='" & cbxTipo.Text & "')  "
                End If
            End If

        End If

        'Fecha
        If Sucursal = True Then
            If chkFechaSucursal.Checked = True Then
                If Where = "" Then
                    Where = " Where (Fec  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesdeSucursal.GetValue, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHastaSucursal.GetValue, True, False) & "' ) "
                    'Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesdeSucursal.GetValueString, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHastaSucursal.GetValueString, True, False) & "' ) "
                Else
                    Where = Where & " And (Fec  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesdeSucursal.GetValue, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHastaSucursal.GetValue, True, False) & "' )  "
                    'Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesdeSucursal.GetValueString, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHastaSucursal.GetValueString, True, False) & "' )  "
                End If
            End If

        Else

            If chkFecha.Checked = True Then
                If Where = "" Then
                    Where = " Where (Fec  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.GetValue, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.GetValue, True, False) & "' ) "
                    'Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.GetValueString, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.GetValueString, True, False) & "' ) "
                Else
                    Where = Where & " And (Fec  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.GetValue, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.GetValue, True, False) & "' )  "
                    'Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.GetValueString, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.GetValueString, True, False) & "' )  "
                End If
            End If

        End If

        dgwOperacion.Rows.Clear()
        dgwDocumento.Rows.Clear()

        'dtOperacion = CSistema.ExecuteToDataTable("Select IDTransaccion,Numero, Cliente, Banco, 'Fecha'=Fec,NroCheque, Importe, Saldo, Estado From VChequeCliente" & Where)
        'dtOperacion = CSistema.ExecuteToDataTable("Select CC.IDTransaccion,Numero,Cliente, Banco,'Fecha'=Fec,NroCheque,Importe,Saldo, Estado,CDD.FechaDeposito, CDD.NumeroDeposito, CDD.ComprobanteDeposito, CDD.NumeroDescuento, CDD.FechaDescuento From VChequeCliente CC join VChequeClienteDepositadoDescontado CDD on CDD.idtransaccion = CC.IDTransaccion" & Where)
        dtOperacion = CSistema.ExecuteToDataTable("Select IDTransaccion,Numero,Cliente, Banco,Fecha,NroCheque,Importe,Saldo, Estado, FechaDeposito, NumeroDeposito, ComprobanteDeposito, NumeroDescuento, FechaDescuento, NroVencimiento From VChequeClienteConsulta" & Where)
        'dtOperacion = CSistema.ExecuteToDataTable("Select CC.IDTransaccion,CC.Numero,CC.Cliente, CC.Banco,'Fecha'=CC.Fec,CC.NroCheque,CC.Importe,CC.Saldo, CC.Estado,CDD.FechaDeposito, CDD.NumeroDeposito, CDD.ComprobanteDeposito, CDD.NumeroDescuento, CDD.FechaDescuento, 'NroVencimiento' = VCD.Numero From VChequeCliente CC join VChequeClienteDepositadoDescontado CDD on CDD.idtransaccion = CC.IDTransaccion Join VVencimientoChequeDescontado VCD on VCD.IDTransaccionCheque = CC.IDTransaccion" & Where)


        'Limpiar la grilla
        dgwOperacion.Rows.Clear()
        Dim TotalCheques As Decimal = 0
        Dim TotalSaldo As Decimal = 0

        For Each oRow As DataRow In dtOperacion.Rows
            Dim Registro(14) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Numero").ToString
            Registro(2) = oRow("Cliente").ToString
            Registro(3) = oRow("Banco").ToString
            Registro(4) = oRow("Fecha").ToString
            Registro(5) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(6) = oRow("NroCheque").ToString
            Registro(7) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            Registro(8) = oRow("Estado").ToString
            Registro(9) = oRow("FechaDeposito").ToString
            Registro(10) = oRow("NumeroDeposito").ToString
            Registro(11) = oRow("ComprobanteDeposito").ToString
            Registro(12) = oRow("NumeroDescuento").ToString
            Registro(13) = oRow("FechaDescuento").ToString
            Registro(14) = oRow("NroVencimiento").ToString

            'Sumar el total del saldo
            TotalCheques = TotalCheques + CDec(oRow("Importe").ToString)
            TotalSaldo = TotalSaldo + CDec(oRow("Saldo").ToString)

            dgwOperacion.Rows.Add(Registro)

        Next

        txtTotalCheques.SetValue(TotalCheques)
        txtTotalSaldo.SetValue(CSistema.FormatoMoneda(TotalSaldo))

        txtCantidadCheque.txt.Text = dgwOperacion.Rows.Count

    End Sub
    'Listar Documentos
    Sub ListarDocumentos()

        ctrError.Clear()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Limpiar ListView
        dgwDocumento.Rows.Clear()

        Dim Numero As String = dgwOperacion.SelectedRows(0).Cells("colNroOperacion").Value
        Dim Idtransaccion As String = dgwOperacion.SelectedRows(0).Cells("colIDTransaccion").Value
        Dim dtDocumento As DataTable = CSistema.ExecuteToDataTable("Select TipoComprobante, Comprobante, Fec, ImporteCheque From VChequeClienteDocumentosPagados Where Numero=" & Numero & "and Idtransaccion = " & Idtransaccion)


        dgwDocumento.Rows.Clear()
        Dim TotalImporte As Decimal = 0

        For Each oRow As DataRow In dtDocumento.Rows
            Dim Registro(3) As String
            Registro(0) = oRow("TipoComprobante").ToString
            Registro(1) = oRow("Comprobante").ToString
            Registro(2) = CSistema.FormatoMoneda(oRow("Fec").ToString)
            Registro(3) = CSistema.FormatoMoneda(oRow("ImporteCheque").ToString)
            TotalImporte = TotalImporte + CDec(oRow("ImporteCheque").ToString)
            dgwDocumento.Rows.Add(Registro)

        Next
        txtTotalImporte.SetValue(TotalImporte)

    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = CInt(dgwOperacion.SelectedRows(0).Cells(0).Value)

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub frmConsultaChequeCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaChequeCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmConsultaChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn1.Click
        ListarOperaciones(0, " Where (Estado='CARTERA') ", False)
    End Sub

    Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn2.Click
        ListarOperaciones(0, " Where (Cancelado='False') ", False)
    End Sub

    Private Sub btn3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn3.Click
        ListarOperaciones(0, " Where (Estado='DEPOSITADO') ", False)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        ListarOperaciones(0, " Where (Estado='RECHAZADO') ", False)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        ListarOperaciones(0, "", False)
    End Sub

    Private Sub chkCliente_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCliente.CheckedChanged
        HabilitarControles(chkCliente, cbxCliente)
    End Sub

    Private Sub chkBanco_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBanco.CheckedChanged
        HabilitarControles(chkBanco, cbxBanco)
    End Sub

    Private Sub chkTipo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipo.CheckedChanged
        HabilitarControles(chkTipo, cbxTipo)
    End Sub

    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged
        HabilitarControles(chkFecha, txtFechaDesde)
        HabilitarControles(chkFecha, txtFechaHasta)
    End Sub

    Private Sub chkClienteSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkClienteSucursal.CheckedChanged
        HabilitarControles(chkClienteSucursal, cbxClienteSucursal)
    End Sub

    Private Sub chkBancoSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBancoSucursal.CheckedChanged
        HabilitarControles(chkBancoSucursal, cbxBancoSucursal)
    End Sub

    Private Sub chkTipoSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipoSucursal.CheckedChanged
        HabilitarControles(chkTipoSucursal, cbxTipoSucursal)
    End Sub

    Private Sub chkFechaSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFechaSucursal.CheckedChanged
        HabilitarControles(chkFechaSucursal, txtFechaDesdeSucursal)
        HabilitarControles(chkFechaSucursal, txtFechaHastaSucursal)
    End Sub

    Private Sub btn10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn10.Click
        ListarOperaciones(0, " Where (Estado='CARTERA') ", True)
    End Sub

    Private Sub btn11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn11.Click
        ListarOperaciones(0, " Where (Cancelado='False') ", True)
    End Sub

    Private Sub btn12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn12.Click
        ListarOperaciones(0, " Where (Estado='DEPOSITADO') ", True)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ListarOperaciones(0, " Where (Estado='RECHAZADO') ", True)
    End Sub

    Private Sub btn13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn13.Click
        ListarOperaciones(0, "", True)
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarOperaciones(txtOperacion.ObtenerValor)
            txtOperacion.txt.Focus()
            txtOperacion.txt.SelectAll()
        End If
    End Sub

    Private Sub txtNroCheque_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroCheque.TeclaPrecionada
        If IsNumeric(txtNroCheque.txt.Text) = False Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            ListarOperacionesxNroCheque(txtNroCheque.GetValue)
            txtOperacion.txt.Focus()
            txtOperacion.txt.SelectAll()
        End If
    End Sub

    Private Sub dgwOperacion_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgwOperacion.CellClick
        ListarDocumentos()
    End Sub

    Private Sub dgwOperacion_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgwOperacion.DoubleClick
        SeleccionarRegistro()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmConsultaChequeCliente_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmModificarChequeRechazado


    'CLASE
    Dim CSistema As New CSistema

    'PRIPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private FechaValue As Date
    Public Property Fecha() As Date
        Get
            Return FechaValue
        End Get
        Set(ByVal value As Date)
            FechaValue = value
        End Set
    End Property

    Private SaldoValue As Decimal
    Public Property Saldo() As Decimal
        Get
            Return SaldoValue
        End Get
        Set(ByVal value As Decimal)
            SaldoValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()
        CargarOperacion()


    End Sub

    Sub CargarInformacion()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CSistema.SqlToComboBox(cbxMotivo, "Select ID,Descripcion From MotivoRechazoCheque Order By 2")


    End Sub
    Sub CargarOperacion()

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)
        txtFecha.txt.Text = oRow("Fecha").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        cbxMotivo.txt.Text = oRow("MotivoRechazo").ToString
    End Sub

    Sub Modificar()
        'Aplicar
        Try
            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Fecha", txtFecha.GetValueString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDMotivoRechazo", cbxMotivo.GetValue, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Aplicar la Cobranza
            If CSistema.ExecuteStoreProcedure(param, "SpActualizarChequeRechazado", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnModificar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnModificar, ErrorIconAlignment.TopRight)
            End If

            Me.Close()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub frmSeleccionFormaPagoDocumento_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmSeleccionEfectivo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmModificarChequeRechazado_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmRechazarChequeCliente

    'CLASES
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim CSistema As New CSistema
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim Where As String
    Dim Consulta As String
    Dim frm As New frmMotivoRechazo
    Dim VUltimoEjecutado As String

    'FUNCIONES
    Sub Inicializar()

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(frmDepositoBancario.Name, "DEPOSITO BANCARIO", "DEP")
        vNuevo = True

        'Botones

        'Funciones
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        'CARGAR CONTROLES

        'Ciudad
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

    End Sub

    Sub GuardarInformacion()

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

    End Sub

    Sub ListarDeposito(Optional ByVal Condicion As String = "")

        ctrError.Clear()

        'Inicializr el Where
        Where = ""

        Consulta = "Select IDtransaccion, Cuenta, Banco,'T.Comp'= DescripcionTipoComprobante, Fec,Observacion,'Efectivo' = TotalEfectivo,'Chq.Local'= TotalChequeLocal,'Chq.Otros'=TotalChequeOtros,Total From VDepositoBancario  "

        'Comprobante
        If EstablecerCondicion(cbxSucursal.cbx, "IDSucursal", "Seleccione correctamente la sucursal!") = False Then
            Exit Sub
        End If


        'Cuenta Bancaria

        lvDeposito.Items.Clear()
        lvComprobantes.Items.Clear()

        'Buscar por Operacion
        If Condicion <> "" Then
            Where = Condicion
        End If

        'buscar por Comprobantes

        If Condicion <> "" Then
            Where = Condicion
        End If

        CSistema.SqlToLv(lvDeposito, Consulta & " " & Where & " Order By Fecha")

        'Formato

        'Totales
        CSistema.FormatoMoneda(lvDeposito, 6)
        CSistema.FormatoMoneda(lvDeposito, 7)
        CSistema.FormatoMoneda(lvDeposito, 8)
        CSistema.FormatoMoneda(lvDeposito, 9)
        CSistema.TotalesLv(lvDeposito, txtTotalCobranza.txt, 9)
        txtCantidadCobranza.txt.Text = lvDeposito.Items.Count
        lvDeposito.Columns(0).Width = 0
        VUltimoEjecutado = "DEPOSITO"
    End Sub

    Sub ListarComprobantes()

        ctrError.Clear()

        'Validar
        If lvDeposito.SelectedItems.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvDeposito, Mensaje)
            ctrError.SetIconAlignment(lvDeposito, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(lvDeposito.SelectedItems(0).SubItems(0).Text) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvDeposito, Mensaje)
            ctrError.SetIconAlignment(lvDeposito, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        Dim vIDTransaccion As Integer

        vIDTransaccion = lvDeposito.SelectedItems(0).SubItems(0).Text

        'Limpiar ListView
        lvComprobantes.Items.Clear()

        Dim sql As String = "Select IDTransaccion,CodigoTipo,Banco,NroCheque, Importe,Cliente,Estado From VDetalleDepositoBancarioCheque Where IDTransaccionDepositoBancario =" & vIDTransaccion

        CSistema.SqlToLv(lvComprobantes, sql)
        CSistema.FormatoMoneda(lvComprobantes, 4)
        'CSistema.FormatoMoneda(lvComprobantes, 5)

        CSistema.TotalesLv(lvComprobantes, txtTotalComprobantes.txt, 4)
        txtCantidadComprobantes.txt.Text = lvComprobantes.Items.Count
        lvComprobantes.Columns(0).Width = 0

    End Sub

    Sub ListarCheque()

        If IsNumeric(txtNroCheque.txt.Text) = False Then
            Exit Sub
        End If

        Dim sql As String = "Select IDTransaccion,CodigoTipo,Banco,NroCheque, Importe,Cliente,Estado From VDetalleDepositoBancarioCheque where Depositado = 'True' And NroCheque like '%" & txtNroCheque.txt.Text & "%'"
        CSistema.SqlToLv(lvComprobantes, sql)
        txtCantidadComprobantes.txt.Text = lvComprobantes.Items.Count
        CSistema.TotalesLv(lvComprobantes, txtTotalComprobantes.txt, 4)
        CSistema.FormatoMoneda(lvComprobantes, 4)
        lvComprobantes.Columns(0).Width = 0
        VUltimoEjecutado = "CHEQUE"
    End Sub

    Sub Rechazar()

        'Validar

        'Seleccion de Registro en LV
        If lvComprobantes.SelectedItems.Count = 0 Then
            CSistema.MostrarError("Debe Seleccionar un Registro", ctrError, btnRechazar, tsslEstado, ErrorIconAlignment.MiddleRight)
            Exit Sub
        End If

        frm.Inicializar()
        frm.Text = "Motivo de Rechazo"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog(Me)

        If frm.Procesado = True Then

            Guardar(ERP.CSistema.NUMOperacionesRegistro.INS, btnRechazar)

        End If

    End Sub

    Sub CancelarRechazo()

        'Validar
        'Seleccion de Registro en LV
        If lvComprobantes.SelectedItems.Count = 0 Then
            CSistema.MostrarError("Debe Seleccionar un Registro ", ctrError, btnCancelarRechazo, tsslEstado, ErrorIconAlignment.MiddleRight)
            Exit Sub
        End If

        Guardar(ERP.CSistema.NUMOperacionesRegistro.DEL, btnRechazar)



    End Sub

    Sub Buscar()

        Dim frm As New frmConsultaDepositoBancario
        frm.ShowDialog()

        Dim where As String
        where = " Where IDTransaccion = " & frm.IDTransaccion

        ListarDeposito(where)

    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro, ByVal btn As Button)

        tsslEstado.Text = ""
        ctrError.Clear()

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        CSistema.SetSQLParameter(param, "@IDTransaccion", lvComprobantes.SelectedItems(0).SubItems(0).Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMotivoRechazo", frm.IDMotivo, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpChequeClienteRechazado", True, True, MensajeRetorno) = False Then
            Exit Sub
        End If

        If VUltimoEjecutado = "CHEQUE" Then
            ListarCheque()
        Else
            ListarComprobantes()
        End If


    End Sub

    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True



        If cbx.SelectedValue = 0 Then
            ctrError.SetError(cbx, MensajeError)
            ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
            Return False
        End If

        If Where = "" Then
            Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
        Else
            Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
        End If




    End Function

    Private Sub frmDepositoBancarioRechazado_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmDepositoBancarioRechazado_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmDepositoBancarioRechazado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub lvDeposito_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvDeposito.SelectedIndexChanged

        ListarComprobantes()

    End Sub

    Private Sub txtComprobante_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then

            If IsNumeric(txtNroComprobante.txt.Text) = False Then
                Exit Sub
            End If

            Dim Condicion1 As String = " Where Comprobante = " & txtNroComprobante.txt.Text & "And IDSucursal = " & cbxSucursal.cbx.SelectedValue & "And IDTipoComprobante = " & cbxTipoComprobante.cbx.SelectedValue

            ListarDeposito(Condicion1)

        End If
    End Sub

    Private Sub txtNroCheque_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroCheque.TeclaPrecionada

        If e.KeyCode = Keys.Enter Then

            ListarCheque()

        End If

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Buscar()
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            Dim Condicion As String = " Where Numero = " & txtOperacion.ObtenerValor & "And IDSucursal = " & cbxSucursal.cbx.SelectedValue & "And IDTipoComprobante = " & cbxTipoComprobante.cbx.SelectedValue
            ListarDeposito(Condicion)
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelarRechazo.Click
        CancelarRechazo()
    End Sub

    Private Sub btnRecahazar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRechazar.Click
        Rechazar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmRechazarChequeCliente_Activate()
        Me.Refresh()
    End Sub
End Class
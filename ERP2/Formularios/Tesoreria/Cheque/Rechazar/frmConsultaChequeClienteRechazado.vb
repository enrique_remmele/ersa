﻿Public Class frmConsultaChequeClienteRechazado

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim dtBanco As DataTable
    Dim dtCliente As DataTable
    Dim dtOperacion As DataTable

    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Form

        'TextBox
        txtCantidadCheque.txt.ResetText()
        txtNroCheque.txt.ResetText()
        txtOperacion.txt.ResetText()
        txtTotalCheques.txt.ResetText()

        'CheckBox
        chkBanco.Checked = False
        'chkBancoSucursal.Checked = False
        chkCliente.Checked = False
        'chkClienteSucursal.Checked = False
        chkFecha.Checked = False
        'chkFechaSucursal.Checked = False
        chkTipo.Checked = False
        'chkTipoSucursal.Checked = False

        'ComboBox
        cbxBanco.Enabled = False
        'cbxBancoSucursal.Enabled = False
        cbxCliente.Enabled = False
        'cbxClienteSucursal.Enabled = False
        cbxTipo.Enabled = False
        'cbxTipoSucursal.Enabled = False

        'datagridView
        dgwOperacion.Rows.Clear()


        'Funciones
        CargarInformacion()

        'Foco
        txtOperacion.txt.Focus()
        txtOperacion.txt.SelectAll()
    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Sucursales
        'CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'Banco
        dtBanco = CSistema.ExecuteToDataTable("Select ID, Descripcion From Banco Order By 2").Copy
        CSistema.SqlToComboBox(cbxBanco, dtBanco.Copy, "ID", "Descripcion")
        'CSistema.SqlToComboBox(cbxBancoSucursal, dtBanco.Copy, "ID", "Descripcion")

        'Cliente
        dtCliente = CSistema.ExecuteToDataTable("Select ID, RazonSocial From Cliente Order By 2").Copy
        CSistema.SqlToComboBox(cbxCliente, dtCliente.Copy, "ID", "RazonSocial")
        'CSistema.SqlToComboBox(cbxClienteSucursal, dtCliente.Copy, "ID", "RazonSocial")

        'Tipo
        cbxTipo.Items.Add("AL DIA")
        cbxTipo.Items.Add("DIFERIDO")
        cbxTipo.DropDownStyle = ComboBoxStyle.DropDownList

        'cbxTipoSucursal.Items.Add("AL DIA")
        'cbxTipoSucursal.Items.Add("DIFERIDO")
        'cbxTipoSucursal.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        'cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

        'Cliente
        chkCliente.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO", "False")
        'chkClienteSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO SUCRUSAL", "False")
        cbxCliente.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE", "")
        'cbxClienteSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE SUCURSAL", "")

        'Banco
        chkBanco.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO", "False")
        'chkBancoSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO SUCURSAL", "False")
        cbxBanco.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO", "")
        'cbxBancoSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "BANCO SUCURSAL", "")

        'dtOperacion = CSistema.ExecuteToDataTable("Select Numero, Cliente, Banco, 'Fecha'=Fec,NroCheque, Importe, Saldo, Estado From VChequeCliente")
        dtOperacion = CSistema.ExecuteToDataTable("Select CC.IDTransaccion,Numero,Cliente, Banco,'Fecha'=Fec,NroCheque,Importe,Saldo, Estado,CDD.FechaDeposito, CDD.NumeroDeposito, CDD.ComprobanteDeposito, CDD.NumeroDescuento, CDD.FechaDescuento From VChequeCliente CC join VChequeClienteDepositadoDescontado CDD on CDD.idtransaccion = CC.IDTransaccion")

    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        'CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Cliente
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO", chkCliente.Checked.ToString)
        'CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO SUCURSAL", chkClienteSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE", cbxCliente.Text)
        'CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE SUCURSAL", cbxClienteSucursal.Text)

        'Banco
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO", chkBanco.Checked.ToString)
        'CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO ACTIVO SUCURSAL", chkBancoSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO", cbxBanco.Text)
        'CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "BANCO SUCURSAL", cbxBancoSucursal.Text)

        'Tipo
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO ACTIVO", chkTipo.Checked.ToString)
        'CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO ACTIVO SUCURSAL", chkTipoSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO", cbxTipo.Text)


    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Operaciones
    Sub ListarOperaciones(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "")

        ctrError.Clear()

        Where = Condicion


        'Solo por numero
        If Numero > 0 Then
            Where = " Where Numero = " & Numero & ""
        End If

        If IsNumeric(txtNroCheque.txt.Text) > 0 Then
            Where = "Where NroCheque = " & txtNroCheque.txt.Text
        End If


        'Cliente
        If EstablecerCondicion(cbxCliente, chkCliente, "IDCliente", "Seleccione correctamente el cliente!") = False Then
            Exit Sub
        End If

        'Banco
        If EstablecerCondicion(cbxBanco, chkBanco, "IDBanco", "Seleccione correctamente el banco!") = False Then
            Exit Sub
        End If


        'Tipo
        If chkTipo.Checked = True Then
            If Where = "" Then
                Where = " Where (Tipo='" & cbxTipo.Text & "') "
            Else
                Where = Where & " And (Tipo='" & cbxTipo.Text & "')  "
            End If
        End If

        'Fecha
        If chkFecha.Checked = True Then
            If Where = "" Then
                Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.GetValueString, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.GetValueString, True, False) & "' ) "
            Else
                Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.GetValueString, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.GetValueString, True, False) & "' )  "
            End If
        End If


        dgwOperacion.Rows.Clear()


        'dtOperacion = CSistema.ExecuteToDataTable("Select IDTransaccion,Numero,Cliente, Banco,'Fecha'=Fec,NroCheque,Importe,Saldo, Estado From VChequeCliente" & Where)
        dtOperacion = CSistema.ExecuteToDataTable("Select CC.IDTransaccion,Numero,Cliente, Banco,'Fecha'=Fec,NroCheque,Importe from vChequeClienteRechazado CC" & Where)

        'Limpiar la grilla
        dgwOperacion.DataSource = Nothing
        dgwOperacion.Refresh()
        Dim TotalCheques As Decimal = 0
        Dim TotalSaldo As Decimal = 0

        For Each oRow As DataRow In dtOperacion.Rows
            Dim Registro(7) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Numero").ToString
            Registro(2) = oRow("Cliente").ToString
            Registro(3) = oRow("Banco").ToString
            Registro(4) = oRow("NroCheque").ToString
            Registro(5) = oRow("Fecha").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Importe").ToString)

            'Sumar el total del saldo
            TotalCheques = TotalCheques + CDec(oRow("Importe").ToString)
            'TotalSaldo = TotalSaldo + CDec(oRow("Saldo").ToString)

            dgwOperacion.Rows.Add(Registro)

        Next

        txtTotalCheques.SetValue(TotalCheques)
        txtCantidadCheque.SetValue(dtOperacion.Rows.Count)


        txtCantidadCheque.txt.Text = dgwOperacion.Rows.Count

    End Sub
    'Listar x NroCheque
    Sub ListarOperacionesxNroCheque(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "", Optional ByVal Sucursal As Boolean = False)

        ctrError.Clear()

        Where = Condicion

        'Solo por numero
        If Numero > 0 Then
            Where = " Where NroCheque like '%" & Numero & "'"
        End If


        dgwOperacion.Rows.Clear()
        'dgwDocumento.Rows.Clear()

        'dtOperacion = CSistema.ExecuteToDataTable("Select IDTransaccion,Numero, Cliente, Banco, 'Fecha'=Fec,NroCheque, Importe, Saldo, Estado From VChequeCliente" & Where)
        dtOperacion = CSistema.ExecuteToDataTable("Select CC.IDTransaccion,Numero,Cliente, Banco,'Fecha'=Fec,NroCheque,Importe From VChequeClienteRechazado CC" & Where)


        'Limpiar la grilla
        dgwOperacion.Rows.Clear()
        Dim TotalCheques As Decimal = 0
        Dim TotalSaldo As Decimal = 0

        For Each oRow As DataRow In dtOperacion.Rows
            Dim Registro(7) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Numero").ToString
            Registro(2) = oRow("Cliente").ToString
            Registro(3) = oRow("Banco").ToString
            Registro(4) = oRow("NroCheque").ToString
            Registro(5) = oRow("Fecha").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Importe").ToString)

            'Sumar el total del saldo
            TotalCheques = TotalCheques + CDec(oRow("Importe").ToString)
            'TotalSaldo = TotalSaldo + CDec(oRow("Saldo").ToString)

            dgwOperacion.Rows.Add(Registro)

        Next

        txtTotalCheques.SetValue(TotalCheques)
        'txtTotalSaldo.SetValue(CSistema.FormatoMoneda(TotalSaldo))

        txtCantidadCheque.txt.Text = dgwOperacion.Rows.Count

    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = CInt(dgwOperacion.SelectedRows(0).Cells(0).Value)

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub frmConsultaChequeCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaChequeCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmConsultaChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        ListarOperaciones(0, "")
    End Sub

    Private Sub chkCliente_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCliente.CheckedChanged
        HabilitarControles(chkCliente, cbxCliente)
    End Sub

    Private Sub chkBanco_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBanco.CheckedChanged
        HabilitarControles(chkBanco, cbxBanco)
    End Sub

    Private Sub chkTipo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipo.CheckedChanged
        HabilitarControles(chkTipo, cbxTipo)
    End Sub

    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged
        HabilitarControles(chkFecha, txtFechaDesde)
        HabilitarControles(chkFecha, txtFechaHasta)
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarOperaciones(txtOperacion.ObtenerValor)
            txtOperacion.txt.Focus()
            txtOperacion.txt.SelectAll()
        End If
    End Sub

    Private Sub txtNroCheque_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroCheque.TeclaPrecionada
        If IsNumeric(txtNroCheque.txt.Text) = False Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            ListarOperacionesxNroCheque(txtNroCheque.GetValue)
            txtOperacion.txt.Focus()
            txtOperacion.txt.SelectAll()
        End If
    End Sub

    Private Sub dgwOperacion_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgwOperacion.DoubleClick
        SeleccionarRegistro()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmConsultaChequeClienteRechazado_Activate()
        Me.Refresh()
    End Sub
End Class
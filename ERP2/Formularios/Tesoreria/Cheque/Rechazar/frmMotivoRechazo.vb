﻿Public Class frmMotivoRechazo

    'CLASES
    Dim CSistema As New CSistema
    Dim IDRetorno As Integer


    'PROPIEDADES
    Private IDMotivoValue As Integer
    Public Property IDMotivo() As Integer
        Get
            Return IDMotivoValue
        End Get
        Set(ByVal value As Integer)
            IDMotivoValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        Procesado = False

        'Funciones
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Motivo de Rechazo
        CSistema.SqlToComboBox(cbxMotivo, "Select ID,Descripcion From MotivoRechazoCheque ")

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        IDMotivo = CSistema.ExecuteScalar("Select ID From MotivoRechazoCheque Where ID=" & cbxMotivo.GetValue)

        If IDMotivo = 0 Then

            If ValidarDocumento(Operacion) = False Then
                Exit Sub
            End If

            Dim param(-1) As SqlClient.SqlParameter
            CSistema.SetSQLParameter(param, "@Descripcion", cbxMotivo.cbx.Text, ParameterDirection.Input)

            'Información de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpMotivoRechazoCheque", False, True, MensajeRetorno) = False Then
                Exit Sub
            End If

            IDMotivo = CSistema.ExecuteScalar("Select ID From MotivoRechazoCheque Where Descripcion='" & cbxMotivo.cbx.Text & "'")
            Procesado = True
            Me.Close()

        Else

            IDMotivo = cbxMotivo.GetValue

            Procesado = True
            Me.Close()
        End If

    End Sub

    Sub Cancelar()

        Procesado = False
        Me.Close()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        If cbxMotivo.cbx.Text = "" Then
            Dim mensaje As String = "Ingrese Descripción del Motivo!"
            ctrError.SetError(btnAceptar, mensaje)
            ctrError.SetIconAlignment(btnAceptar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        Return True
    End Function

    Private Sub frmMotivoRechazo_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub


    Private Sub frmMotivoRechazo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Procesar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmMotivoRechazo_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmVencimientoChequeDescontado

    'CLASES
    Dim CArchivoInicio As New CArchivoInicio
    Dim CAsiento As New CAsientoVencimientoChequeDescontado
    Dim CData As New CData
    Dim dtChequeRechazado As DataTable

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private IDTransaccionChequeValue As Integer
    Public Property IDTransaccionCheque() As Integer
        Get
            Return IDTransaccionChequeValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionChequeValue = value
        End Set
    End Property

    'VARIABLES
    Dim CSistema As New CSistema
    Dim dtCheque As DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim VUltimoEjecutado As String

    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Controles
        txtCliente.Conectar()

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "VENCIMIENTO DE CHEQUE DESCONTADO", "VCHD")
        vNuevo = True

        'Otros
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()
        CAsiento.NoAgrupar = True

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)
        'Botones

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.Focus()
        txtID.txt.SelectAll()

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)
        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, New Button, btnAsiento, vControles, btnModificar)
    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, txtfecha)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, btnAgregar)
        'CARGAR CONTROLES

        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, CData.GetTable("VMoneda").Copy, "ID", "Referencia")

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)


    End Sub

    Sub GuardarInformacion()

        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "OPERACION", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0


        vNuevo = True

        'Cabecera
        txtObservacion.txt.Clear()
        txtNroCheque.txt.Clear()
        'detalle

        CbxCiudadCheque.txt.Text = ""
        txtNumeroCheque.txt.Text = ""
        txtCliente.Clear()
        cbxBanco.txt.Text = ""
        cbxNroCuentaBancaria.txt.Text = ""

        txtNroCheque.txt.Text = ""
        cbxMoneda.txt.Text = ""
        txtCotizacion.txt.Text = ""
        txtImporteMoneda.txt.Text = ""
        txtImporte.txt.Text = ""
        chkDiferido.Checked = False
        chkChequeTercero.Checked = False
        txtSaldo.txt.Text = ""
        txtEstado.txt.Text = ""
        cbxDocumentosPagados.txt.Text = ""
        txtfecha.Clear()
        txtFechaCheque.Clear()


        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False


        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From VencimientoChequeDescontado  Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True
        cbxSucursal.Focus()

    End Sub

    Sub CargarChequeDescontado()

        Dim frm As New frmSeleccionarChequeDescontado
        frm.Text = " Seleccionar Seleccionar Cheque Descontado "
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog(Me)
        If frm.Seleccionado = True Then
            IDTransaccionCheque = frm.IDTransaccion
            CargarCheque()
            btnAsiento.Focus()
        End If

    End Sub

    Sub CargarCheque()
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VChequeCliente Where IDTransaccion=" & IDTransaccionCheque)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnicos."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        CbxCiudadCheque.txt.Text = oRow("CodigoSucursal").ToString
        txtNumeroCheque.txt.Text = oRow("Numero").ToString
        txtCliente.txtReferencia.txt.Text = oRow("CodigoCliente").ToString
        txtCliente.txtRazonSocial.txt.Text = oRow("Cliente").ToString
        txtCliente.txtRUC.txt.Text = oRow("RUC").ToString
        cbxBanco.txt.Text = oRow("Banco").ToString
        txtFechaCheque.txt.Text = oRow("Fecha").ToString
        cbxNroCuentaBancaria.txt.Text = oRow("CuentaBancaria").ToString
        txtNroCheque.txt.Text = oRow("NroCheque").ToString
        cbxMoneda.Texto = oRow("Moneda").ToString
        txtCotizacion.txt.Text = oRow("Cotizacion").ToString
        txtImporteMoneda.txt.Text = oRow("ImporteMoneda").ToString
        txtImporte.txt.Text = oRow("Importe").ToString

        chkDiferido.Checked = CBool(oRow("Diferido").ToString)

        If chkDiferido.Checked = True Then
            txtVencimiento.SetValue(CDate(oRow("FechaVencimiento").ToString))
        Else
            txtVencimiento.txt.Clear()
        End If

        chkChequeTercero.Checked = CBool(oRow("ChequeTercero").ToString)

        If chkChequeTercero.Checked = True Then
            txtLiberador.txt.Text = oRow("Librador").ToString
        Else
            txtLiberador.txt.Clear()
        End If

        CAsiento.IDSucursalCheque = oRow("IDSucursal").ToString

        txtSaldo.SetValue(oRow("Saldo").ToString)
        txtEstado.txt.Text = oRow("Estado").ToString

        If vNuevo = True Then
            txtfecha.SetValue(CDate(oRow("FechaVencimiento").ToString))
        End If

        'Cargar Documentos Asociados
        ListarDocumentosPagados(IDTransaccionCheque)

    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro, ByVal btn As Button)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer


        'Entrada
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDTransaccionCheque", IDTransaccionCheque, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtfecha.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)
        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpVencimientoChequeDescontado", False, True, MensajeRetorno, IDTransaccion) = False Then
            Exit Sub
        End If

        'Cargamos el asiento
        CAsiento.IDTransaccion = IDTransaccion
        GenerarAsiento()
        CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False


    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Desea anular el proceso? Obs: Podra igualmente procesar luego", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If

        End If

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            'Validar el Asiento
            If CAsiento.ObtenerSaldo <> 0 Then
                CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Function
            End If

            If CAsiento.ObtenerTotal = 0 Then
                CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Function
            End If
        End If

        Return True

    End Function

    Sub Anular(ByRef Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer


        'Entrada
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDTransaccionCheque", IDTransaccionCheque, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtfecha.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)
        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpVencimientoChequeDescontado", False, False, MensajeRetorno, IDTransaccion) = False Then
            Exit Sub
        End If


        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion  from VencimientoChequeDescontado  Where Numero=" & txtID.ObtenerValor & "And IDSucursal = " & cbxSucursal.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtChequeRechazado = CSistema.ExecuteToDataTable("Select * From VVencimientoChequeDescontado  Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dtChequeRechazado Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dtChequeRechazado.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dtChequeRechazado.Rows(0)

        CbxCiudadCheque.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        txtImporte.txt.Text = txtImporteMoneda.ObtenerValor


        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtfecha.SetValue(oRow("Fecha").ToString)

        IDTransaccionCheque = oRow("IDTransaccionCheque").ToString
        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            'lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            lblUsuarioAnulado.Text = oRow("UsuarioAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos el detalle de 
        dtCheque = CSistema.ExecuteToDataTable("Select * From VChequeCliente Where IDTransaccion=" & IDTransaccionCheque).Copy
        CargarCheque()

        'Inicializamos el Asiento
        CAsiento.Limpiar()


    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = "VENCIMIENTO DE CHEQUE DESCONTADO " & txtNroCheque.GetValue

            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VVencimientoChequeDescontado Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else

            'Validar
            If cbxCiudad.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la ciudad de operacion!"
                ctrError.SetError(cbxCiudad, mensaje)
                ctrError.SetIconAlignment(cbxCiudad, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = "VENCIMIENTO DE CHEQUE DESCONTADO " & txtNroCheque.GetValue

            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            CAsiento.ListarDetalle(frm.dgv)
            frm.CalcularTotales()

            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
            CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            CAsiento.NroCaja = frm.CAsiento.NroCaja
            CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                CAsiento.dtAsiento.Clear()
                CAsiento.dtDetalleAsiento.Clear()
                VisualizarAsiento()
            End If

        End If

    End Sub

    Sub GenerarAsiento()

        'EstablecerCabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = cbxCiudad.cbx.SelectedValue
        oRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
        oRow("Fecha") = txtfecha.GetValue
        oRow("IDMoneda") = cbxMoneda.GetValue
        oRow("Cotizacion") = txtCotizacion.ObtenerValor
        oRow("TipoComprobante") = "VENCIMIENTO DE CHEQUE DESCONTADO"
        oRow("NroComprobante") = txtNumeroCheque.ObtenerValor
        'oRow("Comprobante") = "CHEQUE" & " " & txtNumeroCheque.ObtenerValor
        oRow("Comprobante") = "Vencimiento" & " " & txtID.ObtenerValor
        'oRow("Detalle") = txtObservacion.txt.Text
        oRow("Detalle") = "Cheque" & " " & txtNumeroCheque.ObtenerValor
        oRow("Total") = txtImporte.ObtenerValor

        oRow("GastosMultiples") = False

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)

        CAsiento.Total = txtImporte.ObtenerValor * txtCotizacion.ObtenerValor
        CAsiento.Cotizacion = txtCotizacion.ObtenerValor

        CAsiento.Generar()

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VencimientoChequeDescontado Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VencimientoChequeDescontado Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub


    Function InsertarDetalle(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalle = True


        Dim sql As String = "Insert Into DetalleVencimientoChequeDescontado (IDTansaccionVencimientoChequeDescontado, IDTransaccionCheque) Values(" & IDTransaccion & ", " & IDTransaccionCheque & " )"
        If CSistema.ExecuteNonQuery(sql) = 0 Then
            Return False
        End If


    End Function
    Sub Modificar()
        Dim frm As New frmModificarChequeRechazado
        frm.Text = "Modificar Depósito Bancario"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen

        frm.IDTransaccion = IDTransaccion
        frm.dt = dtChequeRechazado

        frm.ShowDialog(Me)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Sub ListarDocumentosPagados(ByVal IDTransaccion As Integer)

        cbxDocumentosPagados.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        Dim sql As String = "Select IDTransaccion, Comprobante From VChequeClienteDocumentosPagados Where IDTransaccion=" & IDTransaccion
        CSistema.SqlToComboBox(cbxDocumentosPagados.cbx, sql)


    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged
        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub btnNuevo_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub btnGuardar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS, btnNuevo)
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        CargarChequeDescontado()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub cbxCiudad_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxCiudad.TeclaPrecionada
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    Private Sub frmVencimientoChequeDescontado_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmVencimientoChequeDescontado_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmVencimientoChequeDescontado_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmVencimientoChequeDescontado_Activate()
        Me.Refresh()
    End Sub
End Class
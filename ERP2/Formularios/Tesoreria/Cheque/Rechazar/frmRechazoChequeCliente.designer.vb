﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRechazoChequeCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbxCuentaBancaria = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.txtfecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.lblObsrevacion = New System.Windows.Forms.Label()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.gbxDatos = New System.Windows.Forms.GroupBox()
        Me.txtmotivo = New ERP.ocxTXTString()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.txtEstado = New ERP.ocxTXTString()
        Me.cbxDocumentosPagados = New ERP.ocxCBX()
        Me.lblDocumentosPagados = New System.Windows.Forms.Label()
        Me.lblDiferido = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.lblSaldo = New System.Windows.Forms.Label()
        Me.txtLiberador = New ERP.ocxTXTString()
        Me.chkChequeTercero = New System.Windows.Forms.CheckBox()
        Me.chkDiferido = New System.Windows.Forms.CheckBox()
        Me.txtVencimiento = New ERP.ocxTXTDate()
        Me.lblVencimiento = New System.Windows.Forms.Label()
        Me.txtImporteMoneda = New ERP.ocxTXTNumeric()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.txtNroCheque = New ERP.ocxTXTString()
        Me.lblNroCheque = New System.Windows.Forms.Label()
        Me.txtFechaCheque = New ERP.ocxTXTDate()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbxNroCuentaBancaria = New ERP.ocxCBX()
        Me.lblNroCuentaBancaria = New System.Windows.Forms.Label()
        Me.CbxCiudadCheque = New ERP.ocxCBX()
        Me.txtNumeroCheque = New ERP.ocxTXTNumeric()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cbxBanco = New ERP.ocxCBX()
        Me.lblBanco = New System.Windows.Forms.Label()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.flpAnuladoPor.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.gbxDatos.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cbxCuentaBancaria)
        Me.GroupBox1.Controls.Add(Me.txtID)
        Me.GroupBox1.Controls.Add(Me.cbxSucursal)
        Me.GroupBox1.Controls.Add(Me.lblSucursal)
        Me.GroupBox1.Controls.Add(Me.txtfecha)
        Me.GroupBox1.Controls.Add(Me.lblFecha)
        Me.GroupBox1.Controls.Add(Me.txtObservacion)
        Me.GroupBox1.Controls.Add(Me.lblObsrevacion)
        Me.GroupBox1.Controls.Add(Me.cbxCiudad)
        Me.GroupBox1.Controls.Add(Me.lblOperacion)
        Me.GroupBox1.Location = New System.Drawing.Point(1, -2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(642, 67)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(327, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Cta.Bancaria:"
        '
        'cbxCuentaBancaria
        '
        Me.cbxCuentaBancaria.CampoWhere = Nothing
        Me.cbxCuentaBancaria.CargarUnaSolaVez = False
        Me.cbxCuentaBancaria.DataDisplayMember = Nothing
        Me.cbxCuentaBancaria.DataFilter = Nothing
        Me.cbxCuentaBancaria.DataOrderBy = Nothing
        Me.cbxCuentaBancaria.DataSource = Nothing
        Me.cbxCuentaBancaria.DataValueMember = Nothing
        Me.cbxCuentaBancaria.dtSeleccionado = Nothing
        Me.cbxCuentaBancaria.FormABM = Nothing
        Me.cbxCuentaBancaria.Indicaciones = Nothing
        Me.cbxCuentaBancaria.Location = New System.Drawing.Point(398, 11)
        Me.cbxCuentaBancaria.Name = "cbxCuentaBancaria"
        Me.cbxCuentaBancaria.SeleccionMultiple = False
        Me.cbxCuentaBancaria.SeleccionObligatoria = True
        Me.cbxCuentaBancaria.Size = New System.Drawing.Size(119, 21)
        Me.cbxCuentaBancaria.SoloLectura = False
        Me.cbxCuentaBancaria.TabIndex = 6
        Me.cbxCuentaBancaria.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(136, 11)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(45, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(218, 11)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(108, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.Texto = ""
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(186, 15)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(32, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Suc.:"
        '
        'txtfecha
        '
        Me.txtfecha.AñoFecha = 0
        Me.txtfecha.Color = System.Drawing.Color.Empty
        Me.txtfecha.Fecha = New Date(2013, 4, 15, 14, 46, 45, 250)
        Me.txtfecha.Location = New System.Drawing.Point(553, 12)
        Me.txtfecha.MesFecha = 0
        Me.txtfecha.Name = "txtfecha"
        Me.txtfecha.PermitirNulo = False
        Me.txtfecha.Size = New System.Drawing.Size(78, 20)
        Me.txtfecha.SoloLectura = False
        Me.txtfecha.TabIndex = 8
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(516, 15)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 7
        Me.lblFecha.Text = "Fecha:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(77, 37)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(554, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 10
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'lblObsrevacion
        '
        Me.lblObsrevacion.AutoSize = True
        Me.lblObsrevacion.Location = New System.Drawing.Point(5, 41)
        Me.lblObsrevacion.Name = "lblObsrevacion"
        Me.lblObsrevacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObsrevacion.TabIndex = 9
        Me.lblObsrevacion.Text = "Observación:"
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(78, 11)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(58, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(5, 15)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operación:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 318)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(686, 22)
        Me.StatusStrip1.TabIndex = 12
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(596, 310)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(70, 23)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "&Salir"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(522, 310)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(71, 23)
        Me.btnCancelar.TabIndex = 10
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(374, 310)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(70, 23)
        Me.btnNuevo.TabIndex = 8
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(300, 310)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(70, 23)
        Me.btnAsiento.TabIndex = 7
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(448, 310)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(71, 23)
        Me.btnGuardar.TabIndex = 9
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(329, 281)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(312, 20)
        Me.flpAnuladoPor.TabIndex = 4
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(6, 281)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(277, 20)
        Me.flpRegistradoPor.TabIndex = 3
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'gbxDatos
        '
        Me.gbxDatos.Controls.Add(Me.txtmotivo)
        Me.gbxDatos.Controls.Add(Me.Label6)
        Me.gbxDatos.Controls.Add(Me.txtImporte)
        Me.gbxDatos.Controls.Add(Me.Label1)
        Me.gbxDatos.Controls.Add(Me.txtCliente)
        Me.gbxDatos.Controls.Add(Me.txtEstado)
        Me.gbxDatos.Controls.Add(Me.cbxDocumentosPagados)
        Me.gbxDatos.Controls.Add(Me.lblDocumentosPagados)
        Me.gbxDatos.Controls.Add(Me.lblDiferido)
        Me.gbxDatos.Controls.Add(Me.lblEstado)
        Me.gbxDatos.Controls.Add(Me.txtSaldo)
        Me.gbxDatos.Controls.Add(Me.lblSaldo)
        Me.gbxDatos.Controls.Add(Me.txtLiberador)
        Me.gbxDatos.Controls.Add(Me.chkChequeTercero)
        Me.gbxDatos.Controls.Add(Me.chkDiferido)
        Me.gbxDatos.Controls.Add(Me.txtVencimiento)
        Me.gbxDatos.Controls.Add(Me.lblVencimiento)
        Me.gbxDatos.Controls.Add(Me.txtImporteMoneda)
        Me.gbxDatos.Controls.Add(Me.lblImporte)
        Me.gbxDatos.Controls.Add(Me.txtCotizacion)
        Me.gbxDatos.Controls.Add(Me.cbxMoneda)
        Me.gbxDatos.Controls.Add(Me.lblMoneda)
        Me.gbxDatos.Controls.Add(Me.txtNroCheque)
        Me.gbxDatos.Controls.Add(Me.lblNroCheque)
        Me.gbxDatos.Controls.Add(Me.txtFechaCheque)
        Me.gbxDatos.Controls.Add(Me.Label4)
        Me.gbxDatos.Controls.Add(Me.cbxNroCuentaBancaria)
        Me.gbxDatos.Controls.Add(Me.lblNroCuentaBancaria)
        Me.gbxDatos.Controls.Add(Me.CbxCiudadCheque)
        Me.gbxDatos.Controls.Add(Me.txtNumeroCheque)
        Me.gbxDatos.Controls.Add(Me.Label5)
        Me.gbxDatos.Controls.Add(Me.cbxBanco)
        Me.gbxDatos.Controls.Add(Me.lblBanco)
        Me.gbxDatos.Controls.Add(Me.lblCliente)
        Me.gbxDatos.Location = New System.Drawing.Point(4, 94)
        Me.gbxDatos.Name = "gbxDatos"
        Me.gbxDatos.Size = New System.Drawing.Size(637, 180)
        Me.gbxDatos.TabIndex = 2
        Me.gbxDatos.TabStop = False
        '
        'txtmotivo
        '
        Me.txtmotivo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtmotivo.Color = System.Drawing.Color.Empty
        Me.txtmotivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtmotivo.Indicaciones = Nothing
        Me.txtmotivo.Location = New System.Drawing.Point(202, 151)
        Me.txtmotivo.Multilinea = False
        Me.txtmotivo.Name = "txtmotivo"
        Me.txtmotivo.Size = New System.Drawing.Size(423, 21)
        Me.txtmotivo.SoloLectura = True
        Me.txtmotivo.TabIndex = 33
        Me.txtmotivo.TabStop = False
        Me.txtmotivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtmotivo.Texto = ""
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(99, 151)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(103, 13)
        Me.Label6.TabIndex = 32
        Me.Label6.Text = "Motivo de Rechazo:"
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = False
        Me.txtImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporte.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporte.Location = New System.Drawing.Point(535, 68)
        Me.txtImporte.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(90, 21)
        Me.txtImporte.SoloLectura = True
        Me.txtImporte.TabIndex = 19
        Me.txtImporte.TabStop = False
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(294, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Cambio:"
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 140
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(253, 14)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(374, 23)
        Me.txtCliente.SoloLectura = True
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 4
        '
        'txtEstado
        '
        Me.txtEstado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstado.Color = System.Drawing.Color.Empty
        Me.txtEstado.Indicaciones = Nothing
        Me.txtEstado.Location = New System.Drawing.Point(208, 120)
        Me.txtEstado.Multilinea = False
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.Size = New System.Drawing.Size(170, 21)
        Me.txtEstado.SoloLectura = True
        Me.txtEstado.TabIndex = 29
        Me.txtEstado.TabStop = False
        Me.txtEstado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEstado.Texto = ""
        '
        'cbxDocumentosPagados
        '
        Me.cbxDocumentosPagados.CampoWhere = Nothing
        Me.cbxDocumentosPagados.CargarUnaSolaVez = False
        Me.cbxDocumentosPagados.DataDisplayMember = Nothing
        Me.cbxDocumentosPagados.DataFilter = Nothing
        Me.cbxDocumentosPagados.DataOrderBy = Nothing
        Me.cbxDocumentosPagados.DataSource = Nothing
        Me.cbxDocumentosPagados.DataValueMember = Nothing
        Me.cbxDocumentosPagados.dtSeleccionado = Nothing
        Me.cbxDocumentosPagados.FormABM = Nothing
        Me.cbxDocumentosPagados.Indicaciones = Nothing
        Me.cbxDocumentosPagados.Location = New System.Drawing.Point(463, 120)
        Me.cbxDocumentosPagados.Name = "cbxDocumentosPagados"
        Me.cbxDocumentosPagados.SeleccionMultiple = False
        Me.cbxDocumentosPagados.SeleccionObligatoria = True
        Me.cbxDocumentosPagados.Size = New System.Drawing.Size(162, 21)
        Me.cbxDocumentosPagados.SoloLectura = True
        Me.cbxDocumentosPagados.TabIndex = 31
        Me.cbxDocumentosPagados.TabStop = False
        Me.cbxDocumentosPagados.Texto = ""
        '
        'lblDocumentosPagados
        '
        Me.lblDocumentosPagados.AutoSize = True
        Me.lblDocumentosPagados.Location = New System.Drawing.Point(386, 124)
        Me.lblDocumentosPagados.Name = "lblDocumentosPagados"
        Me.lblDocumentosPagados.Size = New System.Drawing.Size(72, 13)
        Me.lblDocumentosPagados.TabIndex = 30
        Me.lblDocumentosPagados.Text = "Doc. pagado:"
        '
        'lblDiferido
        '
        Me.lblDiferido.AutoSize = True
        Me.lblDiferido.Location = New System.Drawing.Point(27, 98)
        Me.lblDiferido.Name = "lblDiferido"
        Me.lblDiferido.Size = New System.Drawing.Size(46, 13)
        Me.lblDiferido.TabIndex = 20
        Me.lblDiferido.Text = "Diferido:"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(165, 124)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 28
        Me.lblEstado.Text = "Estado:"
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = False
        Me.txtSaldo.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtSaldo.Location = New System.Drawing.Point(74, 120)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(85, 21)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 27
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'lblSaldo
        '
        Me.lblSaldo.AutoSize = True
        Me.lblSaldo.Location = New System.Drawing.Point(36, 124)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(37, 13)
        Me.lblSaldo.TabIndex = 26
        Me.lblSaldo.Text = "Saldo:"
        '
        'txtLiberador
        '
        Me.txtLiberador.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLiberador.Color = System.Drawing.Color.Empty
        Me.txtLiberador.Indicaciones = Nothing
        Me.txtLiberador.Location = New System.Drawing.Point(343, 94)
        Me.txtLiberador.Multilinea = False
        Me.txtLiberador.Name = "txtLiberador"
        Me.txtLiberador.Size = New System.Drawing.Size(282, 21)
        Me.txtLiberador.SoloLectura = True
        Me.txtLiberador.TabIndex = 25
        Me.txtLiberador.TabStop = False
        Me.txtLiberador.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtLiberador.Texto = ""
        '
        'chkChequeTercero
        '
        Me.chkChequeTercero.AutoSize = True
        Me.chkChequeTercero.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkChequeTercero.Enabled = False
        Me.chkChequeTercero.Location = New System.Drawing.Point(216, 96)
        Me.chkChequeTercero.Name = "chkChequeTercero"
        Me.chkChequeTercero.Size = New System.Drawing.Size(121, 17)
        Me.chkChequeTercero.TabIndex = 24
        Me.chkChequeTercero.Text = "Cheque de Tercero:"
        Me.chkChequeTercero.UseVisualStyleBackColor = True
        '
        'chkDiferido
        '
        Me.chkDiferido.AutoSize = True
        Me.chkDiferido.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkDiferido.Enabled = False
        Me.chkDiferido.Location = New System.Drawing.Point(74, 97)
        Me.chkDiferido.Name = "chkDiferido"
        Me.chkDiferido.Size = New System.Drawing.Size(15, 14)
        Me.chkDiferido.TabIndex = 21
        Me.chkDiferido.UseVisualStyleBackColor = True
        '
        'txtVencimiento
        '
        Me.txtVencimiento.AñoFecha = 0
        Me.txtVencimiento.Color = System.Drawing.Color.Empty
        Me.txtVencimiento.Fecha = New Date(2013, 4, 15, 14, 46, 45, 31)
        Me.txtVencimiento.Location = New System.Drawing.Point(127, 94)
        Me.txtVencimiento.MesFecha = 0
        Me.txtVencimiento.Name = "txtVencimiento"
        Me.txtVencimiento.PermitirNulo = False
        Me.txtVencimiento.Size = New System.Drawing.Size(83, 20)
        Me.txtVencimiento.SoloLectura = True
        Me.txtVencimiento.TabIndex = 23
        Me.txtVencimiento.TabStop = False
        '
        'lblVencimiento
        '
        Me.lblVencimiento.AutoSize = True
        Me.lblVencimiento.Location = New System.Drawing.Point(91, 98)
        Me.lblVencimiento.Name = "lblVencimiento"
        Me.lblVencimiento.Size = New System.Drawing.Size(38, 13)
        Me.lblVencimiento.TabIndex = 22
        Me.lblVencimiento.Text = "Venc.:"
        '
        'txtImporteMoneda
        '
        Me.txtImporteMoneda.Color = System.Drawing.Color.Empty
        Me.txtImporteMoneda.Decimales = False
        Me.txtImporteMoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteMoneda.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporteMoneda.Location = New System.Drawing.Point(429, 68)
        Me.txtImporteMoneda.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtImporteMoneda.Name = "txtImporteMoneda"
        Me.txtImporteMoneda.Size = New System.Drawing.Size(98, 21)
        Me.txtImporteMoneda.SoloLectura = True
        Me.txtImporteMoneda.TabIndex = 18
        Me.txtImporteMoneda.TabStop = False
        Me.txtImporteMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteMoneda.Texto = "0"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(384, 72)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(45, 13)
        Me.lblImporte.TabIndex = 17
        Me.lblImporte.Text = "Importe:"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(339, 68)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(39, 21)
        Me.txtCotizacion.SoloLectura = True
        Me.txtCotizacion.TabIndex = 16
        Me.txtCotizacion.TabStop = False
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "0"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(241, 68)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(53, 21)
        Me.cbxMoneda.SoloLectura = True
        Me.cbxMoneda.TabIndex = 14
        Me.cbxMoneda.TabStop = False
        Me.cbxMoneda.Texto = ""
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(192, 72)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 13
        Me.lblMoneda.Text = "Moneda:"
        '
        'txtNroCheque
        '
        Me.txtNroCheque.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroCheque.Color = System.Drawing.Color.Empty
        Me.txtNroCheque.Indicaciones = Nothing
        Me.txtNroCheque.Location = New System.Drawing.Point(74, 68)
        Me.txtNroCheque.Multilinea = False
        Me.txtNroCheque.Name = "txtNroCheque"
        Me.txtNroCheque.Size = New System.Drawing.Size(109, 21)
        Me.txtNroCheque.SoloLectura = True
        Me.txtNroCheque.TabIndex = 12
        Me.txtNroCheque.TabStop = False
        Me.txtNroCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroCheque.Texto = ""
        '
        'lblNroCheque
        '
        Me.lblNroCheque.AutoSize = True
        Me.lblNroCheque.Location = New System.Drawing.Point(3, 72)
        Me.lblNroCheque.Name = "lblNroCheque"
        Me.lblNroCheque.Size = New System.Drawing.Size(70, 13)
        Me.lblNroCheque.TabIndex = 11
        Me.lblNroCheque.Text = "Nro. Cheque:"
        '
        'txtFechaCheque
        '
        Me.txtFechaCheque.AñoFecha = 0
        Me.txtFechaCheque.Color = System.Drawing.Color.Empty
        Me.txtFechaCheque.Fecha = New Date(2013, 4, 15, 14, 46, 45, 31)
        Me.txtFechaCheque.Location = New System.Drawing.Point(558, 43)
        Me.txtFechaCheque.MesFecha = 0
        Me.txtFechaCheque.Name = "txtFechaCheque"
        Me.txtFechaCheque.PermitirNulo = False
        Me.txtFechaCheque.Size = New System.Drawing.Size(67, 20)
        Me.txtFechaCheque.SoloLectura = True
        Me.txtFechaCheque.TabIndex = 10
        Me.txtFechaCheque.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(518, 47)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Fecha:"
        '
        'cbxNroCuentaBancaria
        '
        Me.cbxNroCuentaBancaria.CampoWhere = Nothing
        Me.cbxNroCuentaBancaria.CargarUnaSolaVez = False
        Me.cbxNroCuentaBancaria.DataDisplayMember = Nothing
        Me.cbxNroCuentaBancaria.DataFilter = Nothing
        Me.cbxNroCuentaBancaria.DataOrderBy = Nothing
        Me.cbxNroCuentaBancaria.DataSource = Nothing
        Me.cbxNroCuentaBancaria.DataValueMember = Nothing
        Me.cbxNroCuentaBancaria.dtSeleccionado = Nothing
        Me.cbxNroCuentaBancaria.FormABM = Nothing
        Me.cbxNroCuentaBancaria.Indicaciones = Nothing
        Me.cbxNroCuentaBancaria.Location = New System.Drawing.Point(380, 43)
        Me.cbxNroCuentaBancaria.Name = "cbxNroCuentaBancaria"
        Me.cbxNroCuentaBancaria.SeleccionMultiple = False
        Me.cbxNroCuentaBancaria.SeleccionObligatoria = True
        Me.cbxNroCuentaBancaria.Size = New System.Drawing.Size(138, 21)
        Me.cbxNroCuentaBancaria.SoloLectura = True
        Me.cbxNroCuentaBancaria.TabIndex = 8
        Me.cbxNroCuentaBancaria.TabStop = False
        Me.cbxNroCuentaBancaria.Texto = ""
        '
        'lblNroCuentaBancaria
        '
        Me.lblNroCuentaBancaria.AutoSize = True
        Me.lblNroCuentaBancaria.Location = New System.Drawing.Point(268, 47)
        Me.lblNroCuentaBancaria.Name = "lblNroCuentaBancaria"
        Me.lblNroCuentaBancaria.Size = New System.Drawing.Size(112, 13)
        Me.lblNroCuentaBancaria.TabIndex = 7
        Me.lblNroCuentaBancaria.Text = "Nro. Cuenta Bancaria:"
        '
        'CbxCiudadCheque
        '
        Me.CbxCiudadCheque.CampoWhere = Nothing
        Me.CbxCiudadCheque.CargarUnaSolaVez = False
        Me.CbxCiudadCheque.DataDisplayMember = Nothing
        Me.CbxCiudadCheque.DataFilter = Nothing
        Me.CbxCiudadCheque.DataOrderBy = Nothing
        Me.CbxCiudadCheque.DataSource = Nothing
        Me.CbxCiudadCheque.DataValueMember = Nothing
        Me.CbxCiudadCheque.dtSeleccionado = Nothing
        Me.CbxCiudadCheque.FormABM = Nothing
        Me.CbxCiudadCheque.Indicaciones = Nothing
        Me.CbxCiudadCheque.Location = New System.Drawing.Point(74, 16)
        Me.CbxCiudadCheque.Name = "CbxCiudadCheque"
        Me.CbxCiudadCheque.SeleccionMultiple = False
        Me.CbxCiudadCheque.SeleccionObligatoria = True
        Me.CbxCiudadCheque.Size = New System.Drawing.Size(63, 21)
        Me.CbxCiudadCheque.SoloLectura = True
        Me.CbxCiudadCheque.TabIndex = 1
        Me.CbxCiudadCheque.TabStop = False
        Me.CbxCiudadCheque.Texto = ""
        '
        'txtNumeroCheque
        '
        Me.txtNumeroCheque.Color = System.Drawing.Color.Empty
        Me.txtNumeroCheque.Decimales = False
        Me.txtNumeroCheque.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtNumeroCheque.Location = New System.Drawing.Point(137, 16)
        Me.txtNumeroCheque.Name = "txtNumeroCheque"
        Me.txtNumeroCheque.Size = New System.Drawing.Size(74, 21)
        Me.txtNumeroCheque.SoloLectura = True
        Me.txtNumeroCheque.TabIndex = 2
        Me.txtNumeroCheque.TabStop = False
        Me.txtNumeroCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNumeroCheque.Texto = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Operación:"
        '
        'cbxBanco
        '
        Me.cbxBanco.CampoWhere = Nothing
        Me.cbxBanco.CargarUnaSolaVez = False
        Me.cbxBanco.DataDisplayMember = Nothing
        Me.cbxBanco.DataFilter = Nothing
        Me.cbxBanco.DataOrderBy = Nothing
        Me.cbxBanco.DataSource = Nothing
        Me.cbxBanco.DataValueMember = Nothing
        Me.cbxBanco.dtSeleccionado = Nothing
        Me.cbxBanco.FormABM = Nothing
        Me.cbxBanco.Indicaciones = Nothing
        Me.cbxBanco.Location = New System.Drawing.Point(74, 43)
        Me.cbxBanco.Name = "cbxBanco"
        Me.cbxBanco.SeleccionMultiple = False
        Me.cbxBanco.SeleccionObligatoria = True
        Me.cbxBanco.Size = New System.Drawing.Size(188, 21)
        Me.cbxBanco.SoloLectura = True
        Me.cbxBanco.TabIndex = 6
        Me.cbxBanco.TabStop = False
        Me.cbxBanco.Texto = ""
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(32, 47)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(41, 13)
        Me.lblBanco.TabIndex = 5
        Me.lblBanco.Text = "Banco:"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(211, 20)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(42, 13)
        Me.lblCliente.TabIndex = 3
        Me.lblCliente.Text = "Cliente:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(-2, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Cheque Cliente"
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(78, 72)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(109, 23)
        Me.btnAgregar.TabIndex = 2
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(137, 310)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(70, 23)
        Me.btnAnular.TabIndex = 6
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(224, 310)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(70, 23)
        Me.btnModificar.TabIndex = 13
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(5, 310)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(126, 23)
        Me.btnBusquedaAvanzada.TabIndex = 14
        Me.btnBusquedaAvanzada.Text = "&Busqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'frmRechazoChequeCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(686, 340)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.gbxDatos)
        Me.Controls.Add(Me.flpAnuladoPor)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmRechazoChequeCliente"
        Me.Text = "frmRechazoChequeCliente"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.gbxDatos.ResumeLayout(False)
        Me.gbxDatos.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents txtfecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents lblObsrevacion As System.Windows.Forms.Label
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents gbxDatos As System.Windows.Forms.GroupBox
    Friend WithEvents txtImporte As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents txtEstado As ERP.ocxTXTString
    Friend WithEvents cbxDocumentosPagados As ERP.ocxCBX
    Friend WithEvents lblDocumentosPagados As System.Windows.Forms.Label
    Friend WithEvents lblDiferido As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents txtSaldo As ERP.ocxTXTNumeric
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents txtLiberador As ERP.ocxTXTString
    Friend WithEvents chkChequeTercero As System.Windows.Forms.CheckBox
    Friend WithEvents chkDiferido As System.Windows.Forms.CheckBox
    Friend WithEvents txtVencimiento As ERP.ocxTXTDate
    Friend WithEvents lblVencimiento As System.Windows.Forms.Label
    Friend WithEvents txtImporteMoneda As ERP.ocxTXTNumeric
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents txtNroCheque As ERP.ocxTXTString
    Friend WithEvents lblNroCheque As System.Windows.Forms.Label
    Friend WithEvents txtFechaCheque As ERP.ocxTXTDate
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbxNroCuentaBancaria As ERP.ocxCBX
    Friend WithEvents lblNroCuentaBancaria As System.Windows.Forms.Label
    Friend WithEvents CbxCiudadCheque As ERP.ocxCBX
    Friend WithEvents txtNumeroCheque As ERP.ocxTXTNumeric
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cbxBanco As ERP.ocxCBX
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbxCuentaBancaria As ERP.ocxCBX
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnBusquedaAvanzada As Button
    Friend WithEvents txtmotivo As ocxTXTString
End Class

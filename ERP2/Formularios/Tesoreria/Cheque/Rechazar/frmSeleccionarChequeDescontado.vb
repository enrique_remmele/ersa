﻿Public Class frmSeleccionarChequeDescontado
    Dim CSistema As New CSistema
    Dim dtComprobante As DataTable

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property
    'ID Cuenta Bancaria
    Private IDCuentaBancariaValue As Integer
    Public Property IDCuentaBancaria() As Integer
        Get
            Return IDCuentaBancariaValue
        End Get
        Set(ByVal value As Integer)
            IDCuentaBancariaValue = value
        End Set
    End Property

    Private SeleccionadoValue As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionadoValue = value
        End Set
    End Property



    Sub Inicializar()

        CSistema.SqlToComboBox(cbxSucursal, "Select ID,Descripcion From Sucursal Order By 2")

        Listar()

    End Sub

    Sub Listar()

        Dim Where As String = ""

        If chkSucursal.Valor = True Then
            Where = " And IDSucursal=" & cbxSucursal.GetValue
        End If

        dgwOperacion.Rows.Clear()

        Dim sql As String = "Select distinct IDTransaccion,Numero,Ciudad,CodigoTipo,NroCheque,Importe,Cliente,Estado From VDetalleDepositoyDescuentoBancarioCheque Where Depositado='True' and Estado = 'DESCONTADO' "

        If txtNroCheque.txt.Text <> "" Then
            sql = sql & " And NroCheque like '%" & txtNroCheque.txt.Text & "%' " & Where
        End If

        If txtOperacion.txt.Text <> "" Then
            sql = sql & " And Numero = " & txtOperacion.GetValue & Where
        End If


        dtComprobante = CSistema.ExecuteToDataTable(sql)

        For Each oRow As DataRow In dtComprobante.Rows
            Dim Registro(7) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Numero").ToString
            Registro(2) = oRow("Ciudad").ToString
            Registro(3) = oRow("CodigoTipo").ToString
            Registro(4) = oRow("NroCheque").ToString
            Registro(5) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(6) = oRow("Cliente").ToString
            Registro(7) = oRow("Estado").ToString
            dgwOperacion.Rows.Add(Registro)
        Next


    End Sub

    Sub Seleccionar()

        If dgwOperacion.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        'For Each item As ListViewItem In LvListar.SelectedItems
        '    IDTransaccion = item.SubItems(7).Text()
        'Next

        IDTransaccion = dgwOperacion.SelectedRows(0).Cells(0).Value
        Seleccionado = True

        Me.Close()

    End Sub

    Private Sub frmSeleccionarNotaCreditoAplicacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub BtnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancelar.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Seleccionar()
    End Sub

    Private Sub txtNroCheque_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroCheque.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            Listar()
        End If
    End Sub

    Private Sub dgwOperacion_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgwOperacion.DoubleClick
        Seleccionar()
    End Sub

    Private Sub dgwOperacion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgwOperacion.KeyDown
        If e.KeyCode = Keys.Enter Then
            Seleccionar()
        End If
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            Listar()
        End If
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmSeleccionarChequeDescontado_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmDescuentosdeChequesEliminarComprobante
    Dim CSistema As New CSistema
    Public Property IDTransaccionDescuentosdeCheque As Integer
    Public Property Procesado As Boolean

    Sub Listar()

        Dim dtDetalle As DataTable = CSistema.ExecuteToDataTable("select 'Sel' = Cast(0 as bit), IDTransaccion, NroCheque, NroOperacionCheque, CodigoTipo, Comprobante, Cliente, Fecha, 'Importe'= Cast(Importe as numeric) from vDetalleDescuentoCheque where IDTransaccionDescuentoCheque = " & IDTransaccionDescuentosdeCheque)
        Dim dtMotivoEliminacion As DataTable = CSistema.ExecuteToDataTable("Select ID, Descripcion from MotivoEliminarComprobanteAplicado")
        CSistema.dtToGrid(dgv, dtDetalle)

        Dim comboboxColumn As New DataGridViewComboBoxColumn
        comboboxColumn.Name = "Motivo"
        comboboxColumn.DataSource = dtMotivoEliminacion
        comboboxColumn.DisplayMember = "Descripcion"
        comboboxColumn.ValueMember = "Descripcion"
        dgv.Columns.Add(comboboxColumn)

        CSistema.DataGridColumnasVisibles(dgv, {"Sel", "NroCheque", "NroOperacionCheque", "CodigoTipo", "Comprobante", "Cliente", "Fecha", "Importe", "Motivo"})
        CSistema.DataGridColumnasNumericas(dgv, {"Importe"}, False)
        dgv.Columns("Cliente").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("Motivo").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Update()


    End Sub

    Sub Aceptar()

        For i = 0 To dgv.Rows.Count - 1

            If CBool(dgv.Rows(i).Cells("Sel").Value) Then

                Dim param(-1) As SqlClient.SqlParameter

                If dgv.Rows(i).Cells("Motivo").Value = "" Then
                    MessageBox.Show("Seleccione un motivo", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If

                'Entrada
                CSistema.SetSQLParameter(param, "@IDTransaccionDescuentodeCheque", IDTransaccionDescuentosdeCheque, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDTransaccionCheque", dgv.Rows(i).Cells("IDTransaccion").Value, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@NroCheque", dgv.Rows(i).Cells("NroCheque").Value, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Comprobante", dgv.Rows(i).Cells("Comprobante").Value, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoNumero(dgv.Rows(i).Cells("Importe").Value), ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Motivo", dgv.Rows(i).Cells("Motivo").Value, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Accion", "ELIMINADO", ParameterDirection.Input)

                'Informacion de Salida
                CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
                CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

                Dim MensajeRetorno As String = ""

                'Insertar Registro
                If CSistema.ExecuteStoreProcedure(param, "SpDescuentodeChequesAuditoria", False, False, MensajeRetorno,, True) = False Then
                    MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        Next

    End Sub

    Private Sub frmOrdenPagoEliminarComprobante_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Listar()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Aceptar()
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Procesado = False
        Me.Close()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmOrdenPagoEliminarComprobante_Activate()
        Me.Refresh()
    End Sub
End Class
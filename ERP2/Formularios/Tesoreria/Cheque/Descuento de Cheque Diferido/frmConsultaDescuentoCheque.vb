﻿Public Class frmConsultaDescuentoCheque

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim dtOperacion As New DataTable
    Dim dtComprobantes As New DataTable

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim IDOperacion As Integer
    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'Form

        'TextBox
        txtCantidadCobranza.txt.ResetText()
        txtCantidadComprobantes.txt.ResetText()
        txtOperacion.txt.ResetText()
        txtTotalDescontado.txt.ResetText()

        'CheckBox
        chkTipoComprobante.Checked = False
        chkCuentaBancaria.Checked = False
        chkFecha.Checked = False


        'ComboBox
        cbxTipoComprobante.Enabled = False
        cbxCuentaBancaria.Enabled = False

        'DataGridView
        dgwDocumento.Rows.Clear()
        dgwOperacion.Rows.Clear()

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion(frmDescuentodeCheques.Name, "DESCUENTO CHEQUE", "DES")
        CargarInformacion()

        'Foco

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Cuenta Bancaria
        CSistema.SqlToComboBox(cbxCuentaBancaria, "Select  ID, CuentaBancaria From CuentaBancaria Order By 2")

        'TipoComprobante
        CSistema.SqlToComboBox(cbxTipoComprobante, "Select ID, Descripcion From VTipoComprobante Where IDOperacion=" & IDOperacion & " Order By 2")


        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Cuenta Bancaria
        chkCuentaBancaria.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA ACTIVO", "False")
        cbxCuentaBancaria.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA", "")

        'Tipo de Comprobante
        chkTipoComprobante.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO", "False")
        cbxTipoComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", "")



        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Cuenta Bancaria
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA ACTIVO", chkCuentaBancaria.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA", cbxCuentaBancaria.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO", chkTipoComprobante.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", cbxTipoComprobante.Text)



    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Deposito
    Sub ListarDeposito(Optional ByVal Condicion As String = "")

        ctrError.Clear()

        'Inicializr el Where
        Where = ""

        'Cuenta Bancaria
        If EstablecerCondicion(cbxCuentaBancaria, chkCuentaBancaria, "IDCuentaBancaria", "Seleccione correctamente el cliente!") = False Then
            Exit Sub
        End If

        'Comprobante
        If EstablecerCondicion(cbxTipoComprobante, chkTipoComprobante, "IDTipoComprobante", "Seleccione correctamente el tipo de comprobante!") = False Then
            Exit Sub
        End If

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la sucursal!") = False Then
            Exit Sub
        End If

        'Fecha
        If chkFecha.Checked = True Then
            If Where = "" Then
                Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' ) "
            Else
                Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' )  "
            End If
        End If

        dgwOperacion.Rows.Clear()
        dgwDocumento.Rows.Clear()

        'Buscar por Operacion
        If Condicion <> "" Then
            Where = Condicion
        End If

        'buscar por Comprobantes

        If Condicion <> "" Then
            Where = Condicion
        End If

        dgwOperacion.Rows.Clear()

        Consulta = "Select IDTransaccion,Numero,Cuenta, Banco,'T.Comp'= DescripcionTipoComprobante, Fec,Observacion,TotalDescontado,TotalAcreditado From VDescuentoCheque " & Where

        dtOperacion = CSistema.ExecuteToDataTable(Consulta)

        Dim TotalAcreditado As Decimal = 0
        Dim TotalDescontado As Decimal = 0
        Dim Cantidad As Decimal = 0

        For Each oRow As DataRow In dtOperacion.Rows
            Dim Registro(9) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Numero").ToString
            Registro(2) = oRow("Cuenta").ToString
            Registro(3) = oRow("Banco").ToString
            Registro(4) = oRow("T.Comp").ToString
            Registro(5) = oRow("Fec").ToString
            Registro(6) = oRow("Observacion").ToString
            Registro(7) = CSistema.FormatoMoneda(oRow("TotalDescontado").ToString)
            Registro(8) = CSistema.FormatoMoneda(oRow("TotalAcreditado").ToString)

            TotalAcreditado = CDec(oRow("TotalAcreditado").ToString) + TotalAcreditado
            TotalDescontado = CDec(oRow("TotalDescontado").ToString) + TotalDescontado
            Cantidad = Cantidad + 1

            dgwOperacion.Rows.Add(Registro)
        Next

        txtTotalAcreditado.SetValue(TotalAcreditado)
        txtTotalDescontado.SetValue(TotalDescontado)
        txtCantidadCobranza.SetValue(Cantidad)


    End Sub

    'Listar Comprobantes
    Sub ListarComprobantes()

        ctrError.Clear()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells("colIDTransaccion").Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        Dim vIDTransaccion As Integer

        vIDTransaccion = dgwOperacion.SelectedRows(0).Cells("colIDTransaccion").Value


        'Limpiar DataGridView
        dgwDocumento.Rows.Clear()

        Dim sql As String = "Select NroOperacionCheque,Comprobante,Fec,Importe  From VDetalleDescuentoCheque Where IDTransaccionDescuentoCheque=" & vIDTransaccion

        dtComprobantes = CSistema.ExecuteToDataTable(sql)

        Dim Total As Decimal = 0
        Dim Cantidad As Decimal = 0
        For Each oRow As DataRow In dtComprobantes.Rows
            Dim Registro(3) As String
            Registro(0) = oRow("NroOperacionCheque").ToString
            Registro(1) = oRow("Comprobante").ToString
            Registro(2) = oRow("Fec").ToString
            Registro(3) = CSistema.FormatoMoneda(oRow("Importe").ToString)

            Total = CDec(oRow("Importe").ToString) + Total
            Cantidad = Cantidad + 1

            dgwDocumento.Rows.Add(Registro)
        Next

        txtTotalComprobantes.SetValue(Total)
        txtCantidadComprobantes.SetValue(Cantidad)

    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells("colIDTransaccion").Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = dgwOperacion.SelectedRows(0).Cells("colIDTransaccion").Value

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub frmConsultaChequeCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkCliente_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCuentaBancaria.CheckedChanged
        HabilitarControles(chkCuentaBancaria, cbxCuentaBancaria)
    End Sub

    Private Sub chkBanco_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipoComprobante.CheckedChanged
        HabilitarControles(chkTipoComprobante, cbxTipoComprobante)
    End Sub

    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged
        HabilitarControles(chkFecha, dtpDesde)
        HabilitarControles(chkFecha, dtpHasta)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        ListarDeposito()
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        SeleccionarRegistro()
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada

        If e.KeyCode = Keys.Enter Then
            Dim Condicion As String = " Where Numero = " & txtOperacion.ObtenerValor
            ListarDeposito(Condicion)
        End If


    End Sub

    Private Sub dgwOperacion_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgwOperacion.CellClick
        ListarComprobantes()
    End Sub

    Private Sub dgwOperacion_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgwOperacion.DoubleClick
        SeleccionarRegistro()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmConsultaDescuentoCheque_Activate()
        Me.Refresh()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmDescuentodeCheques
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.txtCambio = New ERP.ocxTXTNumeric()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.txtfecha = New ERP.ocxTXTDate()
        Me.txtMoneda = New ERP.ocxTXTString()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.lblCambio = New System.Windows.Forms.Label()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.cbxCuenta = New ERP.ocxCBX()
        Me.txtBanco = New ERP.ocxTXTString()
        Me.lblObsrevacion = New System.Windows.Forms.Label()
        Me.lklEliminarVenta = New System.Windows.Forms.LinkLabel()
        Me.btnAgregarCheques = New System.Windows.Forms.Button()
        Me.lblTotalDescontado = New System.Windows.Forms.Label()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dgwComprobante = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNumero = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnAplicarComprobantes = New System.Windows.Forms.Button()
        Me.btnEliminarAplicacion = New System.Windows.Forms.Button()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.Documentos = New System.Windows.Forms.TabPage()
        Me.dgwOperacion = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBanco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNroComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCuentaContable = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.Auditoria = New System.Windows.Forms.TabPage()
        Me.dgvAuditoria = New System.Windows.Forms.DataGridView()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtCantidadDepositado = New ERP.ocxTXTNumeric()
        Me.txtTotalDescontado = New ERP.ocxTXTNumeric()
        Me.txtTotalAcreditado = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtGastoBancario = New ERP.ocxTXTNumeric()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCotizacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colProveedor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgwComprobante, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.Documentos.SuspendLayout()
        CType(Me.dgwOperacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Auditoria.SuspendLayout()
        CType(Me.dgvAuditoria, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(3, 22)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operación:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbxSucursal)
        Me.GroupBox1.Controls.Add(Me.cbxCiudad)
        Me.GroupBox1.Controls.Add(Me.txtComprobante)
        Me.GroupBox1.Controls.Add(Me.txtCambio)
        Me.GroupBox1.Controls.Add(Me.txtID)
        Me.GroupBox1.Controls.Add(Me.cbxTipoComprobante)
        Me.GroupBox1.Controls.Add(Me.lblComprobante)
        Me.GroupBox1.Controls.Add(Me.lblSucursal)
        Me.GroupBox1.Controls.Add(Me.txtfecha)
        Me.GroupBox1.Controls.Add(Me.txtMoneda)
        Me.GroupBox1.Controls.Add(Me.lblFecha)
        Me.GroupBox1.Controls.Add(Me.txtObservacion)
        Me.GroupBox1.Controls.Add(Me.lblCambio)
        Me.GroupBox1.Controls.Add(Me.lblCuenta)
        Me.GroupBox1.Controls.Add(Me.cbxCuenta)
        Me.GroupBox1.Controls.Add(Me.txtBanco)
        Me.GroupBox1.Controls.Add(Me.lblOperacion)
        Me.GroupBox1.Controls.Add(Me.lblObsrevacion)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(761, 101)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(232, 18)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(103, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(77, 18)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(59, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(463, 18)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(98, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 7
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'txtCambio
        '
        Me.txtCambio.Color = System.Drawing.Color.Empty
        Me.txtCambio.Decimales = False
        Me.txtCambio.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCambio.Location = New System.Drawing.Point(481, 45)
        Me.txtCambio.Name = "txtCambio"
        Me.txtCambio.Size = New System.Drawing.Size(75, 21)
        Me.txtCambio.SoloLectura = False
        Me.txtCambio.TabIndex = 15
        Me.txtCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCambio.Texto = "0"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(136, 18)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(65, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(381, 18)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(82, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 6
        Me.cbxTipoComprobante.Texto = ""
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(341, 22)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(40, 13)
        Me.lblComprobante.TabIndex = 5
        Me.lblComprobante.Text = "Comp.:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(201, 22)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(32, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Suc.:"
        '
        'txtfecha
        '
        Me.txtfecha.AñoFecha = 0
        Me.txtfecha.Color = System.Drawing.Color.Empty
        Me.txtfecha.Fecha = New Date(2013, 4, 23, 16, 46, 10, 250)
        Me.txtfecha.Location = New System.Drawing.Point(601, 18)
        Me.txtfecha.MesFecha = 0
        Me.txtfecha.Name = "txtfecha"
        Me.txtfecha.PermitirNulo = False
        Me.txtfecha.Size = New System.Drawing.Size(69, 20)
        Me.txtfecha.SoloLectura = False
        Me.txtfecha.TabIndex = 9
        '
        'txtMoneda
        '
        Me.txtMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMoneda.Color = System.Drawing.Color.Empty
        Me.txtMoneda.Indicaciones = Nothing
        Me.txtMoneda.Location = New System.Drawing.Point(383, 45)
        Me.txtMoneda.Multilinea = False
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.Size = New System.Drawing.Size(53, 21)
        Me.txtMoneda.SoloLectura = True
        Me.txtMoneda.TabIndex = 13
        Me.txtMoneda.TabStop = False
        Me.txtMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMoneda.Texto = ""
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(561, 22)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 8
        Me.lblFecha.Text = "Fecha:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(77, 72)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(613, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 17
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'lblCambio
        '
        Me.lblCambio.AutoSize = True
        Me.lblCambio.Location = New System.Drawing.Point(436, 49)
        Me.lblCambio.Name = "lblCambio"
        Me.lblCambio.Size = New System.Drawing.Size(45, 13)
        Me.lblCambio.TabIndex = 14
        Me.lblCambio.Text = "Cambio:"
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(1, 49)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 10
        Me.lblCuenta.Text = "Cuenta:"
        '
        'cbxCuenta
        '
        Me.cbxCuenta.CampoWhere = Nothing
        Me.cbxCuenta.CargarUnaSolaVez = False
        Me.cbxCuenta.DataDisplayMember = Nothing
        Me.cbxCuenta.DataFilter = Nothing
        Me.cbxCuenta.DataOrderBy = Nothing
        Me.cbxCuenta.DataSource = Nothing
        Me.cbxCuenta.DataValueMember = Nothing
        Me.cbxCuenta.dtSeleccionado = Nothing
        Me.cbxCuenta.FormABM = Nothing
        Me.cbxCuenta.Indicaciones = Nothing
        Me.cbxCuenta.Location = New System.Drawing.Point(77, 45)
        Me.cbxCuenta.Name = "cbxCuenta"
        Me.cbxCuenta.SeleccionMultiple = False
        Me.cbxCuenta.SeleccionObligatoria = False
        Me.cbxCuenta.Size = New System.Drawing.Size(155, 21)
        Me.cbxCuenta.SoloLectura = False
        Me.cbxCuenta.TabIndex = 11
        Me.cbxCuenta.Texto = ""
        '
        'txtBanco
        '
        Me.txtBanco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBanco.Color = System.Drawing.Color.Empty
        Me.txtBanco.Indicaciones = Nothing
        Me.txtBanco.Location = New System.Drawing.Point(232, 45)
        Me.txtBanco.Multilinea = False
        Me.txtBanco.Name = "txtBanco"
        Me.txtBanco.Size = New System.Drawing.Size(151, 21)
        Me.txtBanco.SoloLectura = True
        Me.txtBanco.TabIndex = 12
        Me.txtBanco.TabStop = False
        Me.txtBanco.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBanco.Texto = ""
        '
        'lblObsrevacion
        '
        Me.lblObsrevacion.AutoSize = True
        Me.lblObsrevacion.Location = New System.Drawing.Point(1, 76)
        Me.lblObsrevacion.Name = "lblObsrevacion"
        Me.lblObsrevacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObsrevacion.TabIndex = 16
        Me.lblObsrevacion.Text = "Observación:"
        '
        'lklEliminarVenta
        '
        Me.lklEliminarVenta.AutoSize = True
        Me.lklEliminarVenta.Location = New System.Drawing.Point(3, 156)
        Me.lklEliminarVenta.Name = "lklEliminarVenta"
        Me.lklEliminarVenta.Size = New System.Drawing.Size(108, 13)
        Me.lklEliminarVenta.TabIndex = 2
        Me.lklEliminarVenta.TabStop = True
        Me.lklEliminarVenta.Text = "Eliminar comprobante"
        '
        'btnAgregarCheques
        '
        Me.btnAgregarCheques.Location = New System.Drawing.Point(3, 3)
        Me.btnAgregarCheques.Name = "btnAgregarCheques"
        Me.btnAgregarCheques.Size = New System.Drawing.Size(69, 21)
        Me.btnAgregarCheques.TabIndex = 0
        Me.btnAgregarCheques.Text = "Cheques"
        Me.btnAgregarCheques.UseVisualStyleBackColor = True
        '
        'lblTotalDescontado
        '
        Me.lblTotalDescontado.Location = New System.Drawing.Point(495, 0)
        Me.lblTotalDescontado.Name = "lblTotalDescontado"
        Me.lblTotalDescontado.Size = New System.Drawing.Size(95, 25)
        Me.lblTotalDescontado.TabIndex = 4
        Me.lblTotalDescontado.Text = "Total Descontado:"
        Me.lblTotalDescontado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(7, 29)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 7
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(88, 29)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 8
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(271, 29)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 1
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(169, 29)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(75, 23)
        Me.btnBusquedaAvanzada.TabIndex = 9
        Me.btnBusquedaAvanzada.Text = "&Busqueda"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(677, 29)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 6
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(595, 29)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 5
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(514, 29)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 2
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(433, 29)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 4
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 586)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(767, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(6, 3)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(262, 20)
        Me.flpRegistradoPor.TabIndex = 0
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgwComprobante)
        Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox3.Location = New System.Drawing.Point(3, 334)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(761, 185)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        '
        'dgwComprobante
        '
        Me.dgwComprobante.AllowUserToAddRows = False
        Me.dgwComprobante.AllowUserToDeleteRows = False
        Me.dgwComprobante.AllowUserToOrderColumns = True
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgwComprobante.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle3
        Me.dgwComprobante.BackgroundColor = System.Drawing.Color.White
        Me.dgwComprobante.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwComprobante.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion2, Me.colNumero, Me.colComprobante, Me.colFecha, Me.colImporte2})
        Me.dgwComprobante.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgwComprobante.Location = New System.Drawing.Point(3, 16)
        Me.dgwComprobante.Name = "dgwComprobante"
        Me.dgwComprobante.ReadOnly = True
        Me.dgwComprobante.RowHeadersVisible = False
        Me.dgwComprobante.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgwComprobante.Size = New System.Drawing.Size(755, 166)
        Me.dgwComprobante.TabIndex = 0
        Me.dgwComprobante.TabStop = False
        '
        'colIDTransaccion2
        '
        Me.colIDTransaccion2.HeaderText = "IDTransaccion"
        Me.colIDTransaccion2.Name = "colIDTransaccion2"
        Me.colIDTransaccion2.ReadOnly = True
        Me.colIDTransaccion2.Visible = False
        '
        'colNumero
        '
        Me.colNumero.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colNumero.HeaderText = "Número"
        Me.colNumero.Name = "colNumero"
        Me.colNumero.ReadOnly = True
        '
        'colComprobante
        '
        Me.colComprobante.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colComprobante.HeaderText = "Comprobante"
        Me.colComprobante.Name = "colComprobante"
        Me.colComprobante.ReadOnly = True
        '
        'colFecha
        '
        Me.colFecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.ReadOnly = True
        '
        'colImporte2
        '
        Me.colImporte2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colImporte2.HeaderText = "Importe"
        Me.colImporte2.Name = "colImporte2"
        Me.colImporte2.ReadOnly = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox3, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 4)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 107.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 63.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(767, 586)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.lklEliminarVenta, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel2, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 110)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 4
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 148.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(761, 185)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.btnAgregarCheques)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnAplicarComprobantes)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnEliminarAplicacion)
        Me.FlowLayoutPanel2.Controls.Add(Me.TabControl2)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(755, 142)
        Me.FlowLayoutPanel2.TabIndex = 0
        '
        'btnAplicarComprobantes
        '
        Me.btnAplicarComprobantes.Location = New System.Drawing.Point(78, 3)
        Me.btnAplicarComprobantes.Name = "btnAplicarComprobantes"
        Me.btnAplicarComprobantes.Size = New System.Drawing.Size(130, 21)
        Me.btnAplicarComprobantes.TabIndex = 13
        Me.btnAplicarComprobantes.Text = "Aplicar comprobantes"
        Me.btnAplicarComprobantes.UseVisualStyleBackColor = True
        '
        'btnEliminarAplicacion
        '
        Me.btnEliminarAplicacion.Location = New System.Drawing.Point(214, 3)
        Me.btnEliminarAplicacion.Name = "btnEliminarAplicacion"
        Me.btnEliminarAplicacion.Size = New System.Drawing.Size(130, 21)
        Me.btnEliminarAplicacion.TabIndex = 14
        Me.btnEliminarAplicacion.Text = "Eliminar Aplicacion"
        Me.btnEliminarAplicacion.UseVisualStyleBackColor = True
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.Documentos)
        Me.TabControl2.Controls.Add(Me.Auditoria)
        Me.TabControl2.Location = New System.Drawing.Point(3, 30)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(755, 109)
        Me.TabControl2.TabIndex = 15
        '
        'Documentos
        '
        Me.Documentos.Controls.Add(Me.dgwOperacion)
        Me.Documentos.Controls.Add(Me.dgw)
        Me.Documentos.Location = New System.Drawing.Point(4, 22)
        Me.Documentos.Name = "Documentos"
        Me.Documentos.Padding = New System.Windows.Forms.Padding(3)
        Me.Documentos.Size = New System.Drawing.Size(747, 83)
        Me.Documentos.TabIndex = 0
        Me.Documentos.Text = "Documentos"
        Me.Documentos.UseVisualStyleBackColor = True
        '
        'dgwOperacion
        '
        Me.dgwOperacion.AllowUserToAddRows = False
        Me.dgwOperacion.AllowUserToDeleteRows = False
        Me.dgwOperacion.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgwOperacion.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgwOperacion.BackgroundColor = System.Drawing.Color.White
        Me.dgwOperacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwOperacion.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colTipo, Me.colBanco, Me.colNroComprobante, Me.colImporte, Me.colCliente, Me.colCuentaContable})
        Me.dgwOperacion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgwOperacion.Location = New System.Drawing.Point(3, 3)
        Me.dgwOperacion.Name = "dgwOperacion"
        Me.dgwOperacion.ReadOnly = True
        Me.dgwOperacion.RowHeadersVisible = False
        Me.dgwOperacion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgwOperacion.Size = New System.Drawing.Size(741, 77)
        Me.dgwOperacion.TabIndex = 3
        Me.dgwOperacion.TabStop = False
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colTipo
        '
        Me.colTipo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colTipo.HeaderText = "Tipo"
        Me.colTipo.Name = "colTipo"
        Me.colTipo.ReadOnly = True
        '
        'colBanco
        '
        Me.colBanco.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colBanco.HeaderText = "Banco"
        Me.colBanco.Name = "colBanco"
        Me.colBanco.ReadOnly = True
        '
        'colNroComprobante
        '
        Me.colNroComprobante.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colNroComprobante.HeaderText = "NroComprobante"
        Me.colNroComprobante.Name = "colNroComprobante"
        Me.colNroComprobante.ReadOnly = True
        '
        'colImporte
        '
        Me.colImporte.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        '
        'colCliente
        '
        Me.colCliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colCliente.HeaderText = "Cliente"
        Me.colCliente.Name = "colCliente"
        Me.colCliente.ReadOnly = True
        '
        'colCuentaContable
        '
        Me.colCuentaContable.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colCuentaContable.HeaderText = "CuentaContable"
        Me.colCuentaContable.Name = "colCuentaContable"
        Me.colCuentaContable.ReadOnly = True
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgw.Location = New System.Drawing.Point(3, 3)
        Me.dgw.Name = "dgw"
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(741, 77)
        Me.dgw.StandardTab = True
        Me.dgw.TabIndex = 2
        '
        'Auditoria
        '
        Me.Auditoria.Controls.Add(Me.dgvAuditoria)
        Me.Auditoria.Location = New System.Drawing.Point(4, 22)
        Me.Auditoria.Name = "Auditoria"
        Me.Auditoria.Padding = New System.Windows.Forms.Padding(3)
        Me.Auditoria.Size = New System.Drawing.Size(747, 83)
        Me.Auditoria.TabIndex = 1
        Me.Auditoria.Text = "Auditoria"
        Me.Auditoria.UseVisualStyleBackColor = True
        '
        'dgvAuditoria
        '
        Me.dgvAuditoria.BackgroundColor = System.Drawing.Color.White
        Me.dgvAuditoria.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAuditoria.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAuditoria.Location = New System.Drawing.Point(3, 3)
        Me.dgvAuditoria.Name = "dgvAuditoria"
        Me.dgvAuditoria.Size = New System.Drawing.Size(741, 77)
        Me.dgvAuditoria.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtCantidadDepositado)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalDescontado)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblTotalDescontado)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalAcreditado)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtGastoBancario)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label2)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 301)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(761, 27)
        Me.FlowLayoutPanel1.TabIndex = 2
        '
        'txtCantidadDepositado
        '
        Me.txtCantidadDepositado.Color = System.Drawing.Color.Empty
        Me.txtCantidadDepositado.Decimales = True
        Me.txtCantidadDepositado.Indicaciones = Nothing
        Me.txtCantidadDepositado.Location = New System.Drawing.Point(709, 3)
        Me.txtCantidadDepositado.Name = "txtCantidadDepositado"
        Me.txtCantidadDepositado.Size = New System.Drawing.Size(49, 22)
        Me.txtCantidadDepositado.SoloLectura = True
        Me.txtCantidadDepositado.TabIndex = 6
        Me.txtCantidadDepositado.TabStop = False
        Me.txtCantidadDepositado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadDepositado.Texto = "0"
        '
        'txtTotalDescontado
        '
        Me.txtTotalDescontado.Color = System.Drawing.Color.Empty
        Me.txtTotalDescontado.Decimales = True
        Me.txtTotalDescontado.Indicaciones = Nothing
        Me.txtTotalDescontado.Location = New System.Drawing.Point(596, 3)
        Me.txtTotalDescontado.Name = "txtTotalDescontado"
        Me.txtTotalDescontado.Size = New System.Drawing.Size(107, 22)
        Me.txtTotalDescontado.SoloLectura = True
        Me.txtTotalDescontado.TabIndex = 5
        Me.txtTotalDescontado.TabStop = False
        Me.txtTotalDescontado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDescontado.Texto = "0"
        '
        'txtTotalAcreditado
        '
        Me.txtTotalAcreditado.Color = System.Drawing.Color.Empty
        Me.txtTotalAcreditado.Decimales = True
        Me.txtTotalAcreditado.Indicaciones = Nothing
        Me.txtTotalAcreditado.Location = New System.Drawing.Point(382, 3)
        Me.txtTotalAcreditado.Name = "txtTotalAcreditado"
        Me.txtTotalAcreditado.Size = New System.Drawing.Size(107, 22)
        Me.txtTotalAcreditado.SoloLectura = True
        Me.txtTotalAcreditado.TabIndex = 3
        Me.txtTotalAcreditado.TabStop = False
        Me.txtTotalAcreditado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalAcreditado.Texto = "0"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(281, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 25)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Total Acreditado:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtGastoBancario
        '
        Me.txtGastoBancario.Color = System.Drawing.Color.Empty
        Me.txtGastoBancario.Decimales = True
        Me.txtGastoBancario.Indicaciones = Nothing
        Me.txtGastoBancario.Location = New System.Drawing.Point(168, 3)
        Me.txtGastoBancario.Name = "txtGastoBancario"
        Me.txtGastoBancario.Size = New System.Drawing.Size(107, 22)
        Me.txtGastoBancario.SoloLectura = True
        Me.txtGastoBancario.TabIndex = 1
        Me.txtGastoBancario.TabStop = False
        Me.txtGastoBancario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtGastoBancario.Texto = "0"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(67, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 25)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Gastos:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnModificar)
        Me.Panel1.Controls.Add(Me.flpRegistradoPor)
        Me.Panel1.Controls.Add(Me.btnNuevo)
        Me.Panel1.Controls.Add(Me.btnGuardar)
        Me.Panel1.Controls.Add(Me.btnEliminar)
        Me.Panel1.Controls.Add(Me.btnCancelar)
        Me.Panel1.Controls.Add(Me.btnImprimir)
        Me.Panel1.Controls.Add(Me.btnSalir)
        Me.Panel1.Controls.Add(Me.btnAsiento)
        Me.Panel1.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 525)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(761, 58)
        Me.Panel1.TabIndex = 4
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(352, 29)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 3
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle4.NullValue = Nothing
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn4.HeaderText = "Importe"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 80
        '
        'colSaldo
        '
        DataGridViewCellStyle5.NullValue = Nothing
        Me.colSaldo.DefaultCellStyle = DataGridViewCellStyle5
        Me.colSaldo.HeaderText = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.Width = 75
        '
        'colTotal
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.NullValue = "0"
        Me.colTotal.DefaultCellStyle = DataGridViewCellStyle6
        Me.colTotal.HeaderText = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.ReadOnly = True
        Me.colTotal.Width = 80
        '
        'colCotizacion
        '
        Me.colCotizacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colCotizacion.HeaderText = "Cot"
        Me.colCotizacion.Name = "colCotizacion"
        '
        'colMoneda
        '
        Me.colMoneda.HeaderText = "Mon"
        Me.colMoneda.Name = "colMoneda"
        Me.colMoneda.ReadOnly = True
        Me.colMoneda.Width = 35
        '
        'colProveedor
        '
        Me.colProveedor.HeaderText = "Proveedor"
        Me.colProveedor.Name = "colProveedor"
        Me.colProveedor.ReadOnly = True
        Me.colProveedor.Width = 170
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewTextBoxColumn3.HeaderText = "Fecha"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 80
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Comprobante"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn1
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "C2"
        DataGridViewCellStyle8.NullValue = "0"
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridViewTextBoxColumn1.HeaderText = "IDTransaccion"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'frmDescuentodeCheques
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 608)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmDescuentodeCheques"
        Me.Tag = "frmDescuentodeCheques"
        Me.Text = "frmDescuento de Cheques"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.dgwComprobante, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.TabControl2.ResumeLayout(False)
        Me.Documentos.ResumeLayout(False)
        CType(Me.dgwOperacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Auditoria.ResumeLayout(False)
        CType(Me.dgvAuditoria, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents txtCambio As ERP.ocxTXTNumeric
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents txtfecha As ERP.ocxTXTDate
    Friend WithEvents txtMoneda As ERP.ocxTXTString
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents lblCambio As System.Windows.Forms.Label
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents cbxCuenta As ERP.ocxCBX
    Friend WithEvents txtBanco As ERP.ocxTXTString
    Friend WithEvents lblObsrevacion As System.Windows.Forms.Label
    Friend WithEvents lklEliminarVenta As System.Windows.Forms.LinkLabel
    Friend WithEvents btnAgregarCheques As System.Windows.Forms.Button
    Friend WithEvents txtTotalDescontado As ERP.ocxTXTNumeric
    Friend WithEvents txtCantidadDepositado As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalDescontado As System.Windows.Forms.Label
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents txtTotalAcreditado As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtGastoBancario As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgwComprobante As System.Windows.Forms.DataGridView
    Friend WithEvents colIDTransaccion2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNumero As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents btnAplicarComprobantes As Button
    Friend WithEvents btnEliminarAplicacion As Button
    Friend WithEvents TabControl2 As TabControl
    Friend WithEvents Documentos As TabPage
    Friend WithEvents dgw As DataGridView
    Friend WithEvents Auditoria As TabPage
    Friend WithEvents dgvAuditoria As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents colSaldo As DataGridViewTextBoxColumn
    Friend WithEvents colTotal As DataGridViewTextBoxColumn
    Friend WithEvents colCotizacion As DataGridViewTextBoxColumn
    Friend WithEvents colMoneda As DataGridViewTextBoxColumn
    Friend WithEvents colProveedor As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents dgwOperacion As DataGridView
    Friend WithEvents colIDTransaccion As DataGridViewTextBoxColumn
    Friend WithEvents colTipo As DataGridViewTextBoxColumn
    Friend WithEvents colBanco As DataGridViewTextBoxColumn
    Friend WithEvents colNroComprobante As DataGridViewTextBoxColumn
    Friend WithEvents colImporte As DataGridViewTextBoxColumn
    Friend WithEvents colCliente As DataGridViewTextBoxColumn
    Friend WithEvents colCuentaContable As DataGridViewTextBoxColumn
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaChequeCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtFechaHasta = New ERP.ocxTXTDate()
        Me.txtFechaDesde = New ERP.ocxTXTDate()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.btn4 = New System.Windows.Forms.Button()
        Me.btn3 = New System.Windows.Forms.Button()
        Me.btn2 = New System.Windows.Forms.Button()
        Me.chkFecha = New System.Windows.Forms.CheckBox()
        Me.chkTipo = New System.Windows.Forms.CheckBox()
        Me.cbxTipo = New System.Windows.Forms.ComboBox()
        Me.btn1 = New System.Windows.Forms.Button()
        Me.chkBanco = New System.Windows.Forms.CheckBox()
        Me.cbxBanco = New System.Windows.Forms.ComboBox()
        Me.chkCliente = New System.Windows.Forms.CheckBox()
        Me.cbxCliente = New System.Windows.Forms.ComboBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btn13 = New System.Windows.Forms.Button()
        Me.btn12 = New System.Windows.Forms.Button()
        Me.btn11 = New System.Windows.Forms.Button()
        Me.chkFechaSucursal = New System.Windows.Forms.CheckBox()
        Me.chkTipoSucursal = New System.Windows.Forms.CheckBox()
        Me.cbxTipoSucursal = New System.Windows.Forms.ComboBox()
        Me.btn10 = New System.Windows.Forms.Button()
        Me.chkBancoSucursal = New System.Windows.Forms.CheckBox()
        Me.cbxBancoSucursal = New System.Windows.Forms.ComboBox()
        Me.chkClienteSucursal = New System.Windows.Forms.CheckBox()
        Me.cbxClienteSucursal = New System.Windows.Forms.ComboBox()
        Me.cbxSucursal = New System.Windows.Forms.ComboBox()
        Me.txtFechaHastaSucursal = New ERP.ocxTXTDate()
        Me.txtFechaDesdeSucursal = New ERP.ocxTXTDate()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalImporte = New ERP.ocxTXTNumeric()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dgwDocumento = New System.Windows.Forms.DataGridView()
        Me.colTipoComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFec = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporteCheque = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblCantidadDocumentos = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.txtCantidadDocumentos = New ERP.ocxTXTNumeric()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblCantidadCheque = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtNroCheque = New ERP.ocxTXTString()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblNroCheque = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.txtOperacion = New ERP.ocxTXTNumeric()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtCantidadCheque = New ERP.ocxTXTNumeric()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalSaldo = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTotalCheques = New ERP.ocxTXTNumeric()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dgwOperacion = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNroOperacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBanco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNroCheque = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFechaDeposito = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNroDeposito = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colComprobanteDeposito = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNroDescuento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFechaDescuento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColNroVencimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.dgwDocumento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.dgwOperacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 237.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(876, 515)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TabControl1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(642, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(231, 509)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'TabControl1
        '
        Me.TabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(3, 16)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(225, 490)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.txtFechaHasta)
        Me.TabPage1.Controls.Add(Me.txtFechaDesde)
        Me.TabPage1.Controls.Add(Me.Button2)
        Me.TabPage1.Controls.Add(Me.btn4)
        Me.TabPage1.Controls.Add(Me.btn3)
        Me.TabPage1.Controls.Add(Me.btn2)
        Me.TabPage1.Controls.Add(Me.chkFecha)
        Me.TabPage1.Controls.Add(Me.chkTipo)
        Me.TabPage1.Controls.Add(Me.cbxTipo)
        Me.TabPage1.Controls.Add(Me.btn1)
        Me.TabPage1.Controls.Add(Me.chkBanco)
        Me.TabPage1.Controls.Add(Me.cbxBanco)
        Me.TabPage1.Controls.Add(Me.chkCliente)
        Me.TabPage1.Controls.Add(Me.cbxCliente)
        Me.TabPage1.Location = New System.Drawing.Point(4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(217, 464)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(145, 158)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(35, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Hasta"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(27, 158)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Desde"
        '
        'txtFechaHasta
        '
        Me.txtFechaHasta.AñoFecha = 0
        Me.txtFechaHasta.Color = System.Drawing.Color.Empty
        Me.txtFechaHasta.Enabled = False
        Me.txtFechaHasta.Fecha = New Date(CType(0, Long))
        Me.txtFechaHasta.Location = New System.Drawing.Point(119, 177)
        Me.txtFechaHasta.MesFecha = 0
        Me.txtFechaHasta.Name = "txtFechaHasta"
        Me.txtFechaHasta.PermitirNulo = False
        Me.txtFechaHasta.Size = New System.Drawing.Size(90, 20)
        Me.txtFechaHasta.SoloLectura = False
        Me.txtFechaHasta.TabIndex = 10
        '
        'txtFechaDesde
        '
        Me.txtFechaDesde.AñoFecha = 0
        Me.txtFechaDesde.Color = System.Drawing.Color.Empty
        Me.txtFechaDesde.Enabled = False
        Me.txtFechaDesde.Fecha = New Date(CType(0, Long))
        Me.txtFechaDesde.Location = New System.Drawing.Point(8, 177)
        Me.txtFechaDesde.MesFecha = 0
        Me.txtFechaDesde.Name = "txtFechaDesde"
        Me.txtFechaDesde.PermitirNulo = False
        Me.txtFechaDesde.Size = New System.Drawing.Size(90, 20)
        Me.txtFechaDesde.SoloLectura = False
        Me.txtFechaDesde.TabIndex = 9
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(6, 296)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(203, 24)
        Me.Button2.TabIndex = 14
        Me.Button2.Text = "Cheques Rechazados"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.UseVisualStyleBackColor = True
        '
        'btn4
        '
        Me.btn4.Location = New System.Drawing.Point(6, 326)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(203, 24)
        Me.btn4.TabIndex = 15
        Me.btn4.Text = "Todos"
        Me.btn4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn4.UseVisualStyleBackColor = True
        '
        'btn3
        '
        Me.btn3.Location = New System.Drawing.Point(6, 266)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(203, 24)
        Me.btn3.TabIndex = 13
        Me.btn3.Text = "Cheques Depositados"
        Me.btn3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn3.UseVisualStyleBackColor = True
        '
        'btn2
        '
        Me.btn2.Location = New System.Drawing.Point(6, 236)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(203, 24)
        Me.btn2.TabIndex = 12
        Me.btn2.Text = "Cheques no Cancelados"
        Me.btn2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn2.UseVisualStyleBackColor = True
        '
        'chkFecha
        '
        Me.chkFecha.AutoSize = True
        Me.chkFecha.Location = New System.Drawing.Point(6, 137)
        Me.chkFecha.Name = "chkFecha"
        Me.chkFecha.Size = New System.Drawing.Size(59, 17)
        Me.chkFecha.TabIndex = 6
        Me.chkFecha.Text = "Fecha:"
        Me.chkFecha.UseVisualStyleBackColor = True
        '
        'chkTipo
        '
        Me.chkTipo.AutoSize = True
        Me.chkTipo.Location = New System.Drawing.Point(6, 93)
        Me.chkTipo.Name = "chkTipo"
        Me.chkTipo.Size = New System.Drawing.Size(50, 17)
        Me.chkTipo.TabIndex = 4
        Me.chkTipo.Text = "Tipo:"
        Me.chkTipo.UseVisualStyleBackColor = True
        '
        'cbxTipo
        '
        Me.cbxTipo.Enabled = False
        Me.cbxTipo.FormattingEnabled = True
        Me.cbxTipo.Location = New System.Drawing.Point(6, 110)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.Size = New System.Drawing.Size(203, 21)
        Me.cbxTipo.TabIndex = 5
        '
        'btn1
        '
        Me.btn1.Location = New System.Drawing.Point(6, 206)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(203, 24)
        Me.btn1.TabIndex = 11
        Me.btn1.Text = "Cheques en Cartera"
        Me.btn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn1.UseVisualStyleBackColor = True
        '
        'chkBanco
        '
        Me.chkBanco.AutoSize = True
        Me.chkBanco.Location = New System.Drawing.Point(6, 49)
        Me.chkBanco.Name = "chkBanco"
        Me.chkBanco.Size = New System.Drawing.Size(60, 17)
        Me.chkBanco.TabIndex = 2
        Me.chkBanco.Text = "Banco:"
        Me.chkBanco.UseVisualStyleBackColor = True
        '
        'cbxBanco
        '
        Me.cbxBanco.Enabled = False
        Me.cbxBanco.FormattingEnabled = True
        Me.cbxBanco.Location = New System.Drawing.Point(6, 66)
        Me.cbxBanco.Name = "cbxBanco"
        Me.cbxBanco.Size = New System.Drawing.Size(203, 21)
        Me.cbxBanco.TabIndex = 3
        '
        'chkCliente
        '
        Me.chkCliente.AutoSize = True
        Me.chkCliente.Location = New System.Drawing.Point(6, 6)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(61, 17)
        Me.chkCliente.TabIndex = 0
        Me.chkCliente.Text = "Cliente:"
        Me.chkCliente.UseVisualStyleBackColor = True
        '
        'cbxCliente
        '
        Me.cbxCliente.Enabled = False
        Me.cbxCliente.FormattingEnabled = True
        Me.cbxCliente.Location = New System.Drawing.Point(6, 22)
        Me.cbxCliente.Name = "cbxCliente"
        Me.cbxCliente.Size = New System.Drawing.Size(203, 21)
        Me.cbxCliente.TabIndex = 1
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.Button1)
        Me.TabPage2.Controls.Add(Me.btn13)
        Me.TabPage2.Controls.Add(Me.btn12)
        Me.TabPage2.Controls.Add(Me.btn11)
        Me.TabPage2.Controls.Add(Me.chkFechaSucursal)
        Me.TabPage2.Controls.Add(Me.chkTipoSucursal)
        Me.TabPage2.Controls.Add(Me.cbxTipoSucursal)
        Me.TabPage2.Controls.Add(Me.btn10)
        Me.TabPage2.Controls.Add(Me.chkBancoSucursal)
        Me.TabPage2.Controls.Add(Me.cbxBancoSucursal)
        Me.TabPage2.Controls.Add(Me.chkClienteSucursal)
        Me.TabPage2.Controls.Add(Me.cbxClienteSucursal)
        Me.TabPage2.Controls.Add(Me.cbxSucursal)
        Me.TabPage2.Controls.Add(Me.txtFechaHastaSucursal)
        Me.TabPage2.Controls.Add(Me.txtFechaDesdeSucursal)
        Me.TabPage2.Location = New System.Drawing.Point(4, 4)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(217, 464)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Sucursal"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(147, 186)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 13)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "Hasta"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(23, 186)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(38, 13)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Desde"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(6, 325)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(203, 24)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "Cheques Rechazados"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btn13
        '
        Me.btn13.Location = New System.Drawing.Point(6, 355)
        Me.btn13.Name = "btn13"
        Me.btn13.Size = New System.Drawing.Size(203, 24)
        Me.btn13.TabIndex = 14
        Me.btn13.Text = "Todos"
        Me.btn13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn13.UseVisualStyleBackColor = True
        '
        'btn12
        '
        Me.btn12.Location = New System.Drawing.Point(6, 295)
        Me.btn12.Name = "btn12"
        Me.btn12.Size = New System.Drawing.Size(203, 24)
        Me.btn12.TabIndex = 12
        Me.btn12.Text = "Cheques Depositados"
        Me.btn12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn12.UseVisualStyleBackColor = True
        '
        'btn11
        '
        Me.btn11.Location = New System.Drawing.Point(6, 265)
        Me.btn11.Name = "btn11"
        Me.btn11.Size = New System.Drawing.Size(203, 24)
        Me.btn11.TabIndex = 11
        Me.btn11.Text = "Cheques no Cancelados"
        Me.btn11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn11.UseVisualStyleBackColor = True
        '
        'chkFechaSucursal
        '
        Me.chkFechaSucursal.AutoSize = True
        Me.chkFechaSucursal.Location = New System.Drawing.Point(6, 164)
        Me.chkFechaSucursal.Name = "chkFechaSucursal"
        Me.chkFechaSucursal.Size = New System.Drawing.Size(59, 17)
        Me.chkFechaSucursal.TabIndex = 7
        Me.chkFechaSucursal.Text = "Fecha:"
        Me.chkFechaSucursal.UseVisualStyleBackColor = True
        '
        'chkTipoSucursal
        '
        Me.chkTipoSucursal.AutoSize = True
        Me.chkTipoSucursal.Location = New System.Drawing.Point(6, 120)
        Me.chkTipoSucursal.Name = "chkTipoSucursal"
        Me.chkTipoSucursal.Size = New System.Drawing.Size(50, 17)
        Me.chkTipoSucursal.TabIndex = 5
        Me.chkTipoSucursal.Text = "Tipo:"
        Me.chkTipoSucursal.UseVisualStyleBackColor = True
        '
        'cbxTipoSucursal
        '
        Me.cbxTipoSucursal.Enabled = False
        Me.cbxTipoSucursal.FormattingEnabled = True
        Me.cbxTipoSucursal.Location = New System.Drawing.Point(6, 137)
        Me.cbxTipoSucursal.Name = "cbxTipoSucursal"
        Me.cbxTipoSucursal.Size = New System.Drawing.Size(203, 21)
        Me.cbxTipoSucursal.TabIndex = 6
        '
        'btn10
        '
        Me.btn10.Location = New System.Drawing.Point(6, 235)
        Me.btn10.Name = "btn10"
        Me.btn10.Size = New System.Drawing.Size(203, 24)
        Me.btn10.TabIndex = 10
        Me.btn10.Text = "Cheques en Cartera"
        Me.btn10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn10.UseVisualStyleBackColor = True
        '
        'chkBancoSucursal
        '
        Me.chkBancoSucursal.AutoSize = True
        Me.chkBancoSucursal.Location = New System.Drawing.Point(6, 76)
        Me.chkBancoSucursal.Name = "chkBancoSucursal"
        Me.chkBancoSucursal.Size = New System.Drawing.Size(60, 17)
        Me.chkBancoSucursal.TabIndex = 3
        Me.chkBancoSucursal.Text = "Banco:"
        Me.chkBancoSucursal.UseVisualStyleBackColor = True
        '
        'cbxBancoSucursal
        '
        Me.cbxBancoSucursal.Enabled = False
        Me.cbxBancoSucursal.FormattingEnabled = True
        Me.cbxBancoSucursal.Location = New System.Drawing.Point(6, 93)
        Me.cbxBancoSucursal.Name = "cbxBancoSucursal"
        Me.cbxBancoSucursal.Size = New System.Drawing.Size(203, 21)
        Me.cbxBancoSucursal.TabIndex = 4
        '
        'chkClienteSucursal
        '
        Me.chkClienteSucursal.AutoSize = True
        Me.chkClienteSucursal.Location = New System.Drawing.Point(6, 33)
        Me.chkClienteSucursal.Name = "chkClienteSucursal"
        Me.chkClienteSucursal.Size = New System.Drawing.Size(61, 17)
        Me.chkClienteSucursal.TabIndex = 1
        Me.chkClienteSucursal.Text = "Cliente:"
        Me.chkClienteSucursal.UseVisualStyleBackColor = True
        '
        'cbxClienteSucursal
        '
        Me.cbxClienteSucursal.Enabled = False
        Me.cbxClienteSucursal.FormattingEnabled = True
        Me.cbxClienteSucursal.Location = New System.Drawing.Point(6, 49)
        Me.cbxClienteSucursal.Name = "cbxClienteSucursal"
        Me.cbxClienteSucursal.Size = New System.Drawing.Size(203, 21)
        Me.cbxClienteSucursal.TabIndex = 2
        '
        'cbxSucursal
        '
        Me.cbxSucursal.FormattingEnabled = True
        Me.cbxSucursal.Location = New System.Drawing.Point(6, 6)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.Size = New System.Drawing.Size(203, 21)
        Me.cbxSucursal.TabIndex = 0
        '
        'txtFechaHastaSucursal
        '
        Me.txtFechaHastaSucursal.AñoFecha = 0
        Me.txtFechaHastaSucursal.Color = System.Drawing.Color.Empty
        Me.txtFechaHastaSucursal.Enabled = False
        Me.txtFechaHastaSucursal.Fecha = New Date(CType(0, Long))
        Me.txtFechaHastaSucursal.Location = New System.Drawing.Point(125, 205)
        Me.txtFechaHastaSucursal.MesFecha = 0
        Me.txtFechaHastaSucursal.Name = "txtFechaHastaSucursal"
        Me.txtFechaHastaSucursal.PermitirNulo = False
        Me.txtFechaHastaSucursal.Size = New System.Drawing.Size(84, 20)
        Me.txtFechaHastaSucursal.SoloLectura = False
        Me.txtFechaHastaSucursal.TabIndex = 19
        '
        'txtFechaDesdeSucursal
        '
        Me.txtFechaDesdeSucursal.AñoFecha = 0
        Me.txtFechaDesdeSucursal.Color = System.Drawing.Color.Empty
        Me.txtFechaDesdeSucursal.Enabled = False
        Me.txtFechaDesdeSucursal.Fecha = New Date(CType(0, Long))
        Me.txtFechaDesdeSucursal.Location = New System.Drawing.Point(6, 205)
        Me.txtFechaDesdeSucursal.MesFecha = 0
        Me.txtFechaDesdeSucursal.Name = "txtFechaDesdeSucursal"
        Me.txtFechaDesdeSucursal.PermitirNulo = False
        Me.txtFechaDesdeSucursal.Size = New System.Drawing.Size(84, 20)
        Me.txtFechaDesdeSucursal.SoloLectura = False
        Me.txtFechaDesdeSucursal.TabIndex = 18
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel2, 0, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.dgwDocumento, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel4, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.dgwOperacion, 0, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 6
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 194.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(633, 509)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.txtTotalImporte)
        Me.FlowLayoutPanel2.Controls.Add(Me.Label10)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(4, 481)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(625, 24)
        Me.FlowLayoutPanel2.TabIndex = 6
        '
        'txtTotalImporte
        '
        Me.txtTotalImporte.Color = System.Drawing.Color.Empty
        Me.txtTotalImporte.Decimales = True
        Me.txtTotalImporte.Indicaciones = Nothing
        Me.txtTotalImporte.Location = New System.Drawing.Point(519, 3)
        Me.txtTotalImporte.Name = "txtTotalImporte"
        Me.txtTotalImporte.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalImporte.SoloLectura = True
        Me.txtTotalImporte.TabIndex = 1
        Me.txtTotalImporte.TabStop = False
        Me.txtTotalImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalImporte.Texto = "0"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(479, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(34, 13)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Total:"
        '
        'dgwDocumento
        '
        Me.dgwDocumento.AllowUserToAddRows = False
        Me.dgwDocumento.AllowUserToDeleteRows = False
        Me.dgwDocumento.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgwDocumento.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgwDocumento.BackgroundColor = System.Drawing.Color.White
        Me.dgwDocumento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwDocumento.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colTipoComprobante, Me.colComprobante, Me.colFec, Me.colImporteCheque})
        Me.dgwDocumento.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgwDocumento.Location = New System.Drawing.Point(4, 286)
        Me.dgwDocumento.Name = "dgwDocumento"
        Me.dgwDocumento.ReadOnly = True
        Me.dgwDocumento.RowHeadersVisible = False
        Me.dgwDocumento.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgwDocumento.Size = New System.Drawing.Size(625, 188)
        Me.dgwDocumento.TabIndex = 3
        Me.dgwDocumento.TabStop = False
        '
        'colTipoComprobante
        '
        Me.colTipoComprobante.HeaderText = "TipoComprobante"
        Me.colTipoComprobante.Name = "colTipoComprobante"
        Me.colTipoComprobante.ReadOnly = True
        Me.colTipoComprobante.Width = 80
        '
        'colComprobante
        '
        Me.colComprobante.HeaderText = "Comprobante"
        Me.colComprobante.Name = "colComprobante"
        Me.colComprobante.ReadOnly = True
        '
        'colFec
        '
        Me.colFec.HeaderText = "Fecha"
        Me.colFec.Name = "colFec"
        Me.colFec.ReadOnly = True
        '
        'colImporteCheque
        '
        Me.colImporteCheque.HeaderText = "Importe"
        Me.colImporteCheque.Name = "colImporteCheque"
        Me.colImporteCheque.ReadOnly = True
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 4
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.01285!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.98715!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 67.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Panel5, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel1, 2, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel6, 3, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(4, 247)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(625, 32)
        Me.TableLayoutPanel4.TabIndex = 2
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Label5)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(3, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(397, 26)
        Me.Panel5.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(330, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Lista de Documentos asociados al cheque seleccionado:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblCantidadDocumentos)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(494, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(60, 26)
        Me.Panel1.TabIndex = 1
        '
        'lblCantidadDocumentos
        '
        Me.lblCantidadDocumentos.AutoSize = True
        Me.lblCantidadDocumentos.Location = New System.Drawing.Point(3, 8)
        Me.lblCantidadDocumentos.Name = "lblCantidadDocumentos"
        Me.lblCantidadDocumentos.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadDocumentos.TabIndex = 0
        Me.lblCantidadDocumentos.Text = "Cantidad:"
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.txtCantidadDocumentos)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(560, 3)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(62, 26)
        Me.Panel6.TabIndex = 2
        '
        'txtCantidadDocumentos
        '
        Me.txtCantidadDocumentos.Color = System.Drawing.Color.Empty
        Me.txtCantidadDocumentos.Decimales = True
        Me.txtCantidadDocumentos.Indicaciones = Nothing
        Me.txtCantidadDocumentos.Location = New System.Drawing.Point(3, 1)
        Me.txtCantidadDocumentos.Name = "txtCantidadDocumentos"
        Me.txtCantidadDocumentos.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadDocumentos.SoloLectura = True
        Me.txtCantidadDocumentos.TabIndex = 0
        Me.txtCantidadDocumentos.TabStop = False
        Me.txtCantidadDocumentos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadDocumentos.Texto = "0"
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 3
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 58.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Panel2, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel3, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel4, 2, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(4, 4)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(625, 30)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblCantidadCheque)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(504, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(60, 24)
        Me.Panel2.TabIndex = 1
        '
        'lblCantidadCheque
        '
        Me.lblCantidadCheque.AutoSize = True
        Me.lblCantidadCheque.Location = New System.Drawing.Point(3, 4)
        Me.lblCantidadCheque.Name = "lblCantidadCheque"
        Me.lblCantidadCheque.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadCheque.TabIndex = 0
        Me.lblCantidadCheque.Text = "Cantidad:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.txtNroCheque)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.lblNroCheque)
        Me.Panel3.Controls.Add(Me.lblOperacion)
        Me.Panel3.Controls.Add(Me.txtOperacion)
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(495, 24)
        Me.Panel3.TabIndex = 0
        '
        'txtNroCheque
        '
        Me.txtNroCheque.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroCheque.Color = System.Drawing.Color.Empty
        Me.txtNroCheque.Indicaciones = Nothing
        Me.txtNroCheque.Location = New System.Drawing.Point(358, 2)
        Me.txtNroCheque.Multilinea = False
        Me.txtNroCheque.Name = "txtNroCheque"
        Me.txtNroCheque.Size = New System.Drawing.Size(134, 21)
        Me.txtNroCheque.SoloLectura = False
        Me.txtNroCheque.TabIndex = 4
        Me.txtNroCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroCheque.Texto = ""
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(9, 1)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 20)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Cheque Cliente:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNroCheque
        '
        Me.lblNroCheque.AutoSize = True
        Me.lblNroCheque.Location = New System.Drawing.Point(276, 4)
        Me.lblNroCheque.Name = "lblNroCheque"
        Me.lblNroCheque.Size = New System.Drawing.Size(85, 13)
        Me.lblNroCheque.TabIndex = 3
        Me.lblNroCheque.Text = "Nro. de Cheque:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(144, 4)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 1
        Me.lblOperacion.Text = "Operacion:"
        '
        'txtOperacion
        '
        Me.txtOperacion.Color = System.Drawing.Color.Empty
        Me.txtOperacion.Decimales = True
        Me.txtOperacion.Indicaciones = Nothing
        Me.txtOperacion.Location = New System.Drawing.Point(205, 1)
        Me.txtOperacion.Name = "txtOperacion"
        Me.txtOperacion.Size = New System.Drawing.Size(68, 22)
        Me.txtOperacion.SoloLectura = False
        Me.txtOperacion.TabIndex = 2
        Me.txtOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOperacion.Texto = "0"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.txtCantidadCheque)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(570, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(52, 24)
        Me.Panel4.TabIndex = 2
        '
        'txtCantidadCheque
        '
        Me.txtCantidadCheque.Color = System.Drawing.Color.Empty
        Me.txtCantidadCheque.Decimales = True
        Me.txtCantidadCheque.Indicaciones = Nothing
        Me.txtCantidadCheque.Location = New System.Drawing.Point(3, 1)
        Me.txtCantidadCheque.Name = "txtCantidadCheque"
        Me.txtCantidadCheque.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadCheque.SoloLectura = True
        Me.txtCantidadCheque.TabIndex = 0
        Me.txtCantidadCheque.TabStop = False
        Me.txtCantidadCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadCheque.Texto = "0"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalSaldo)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalCheques)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label2)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(4, 213)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(625, 27)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'txtTotalSaldo
        '
        Me.txtTotalSaldo.Color = System.Drawing.Color.Empty
        Me.txtTotalSaldo.Decimales = True
        Me.txtTotalSaldo.Indicaciones = Nothing
        Me.txtTotalSaldo.Location = New System.Drawing.Point(519, 3)
        Me.txtTotalSaldo.Name = "txtTotalSaldo"
        Me.txtTotalSaldo.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalSaldo.SoloLectura = True
        Me.txtTotalSaldo.TabIndex = 3
        Me.txtTotalSaldo.TabStop = False
        Me.txtTotalSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalSaldo.Texto = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(476, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Saldo:"
        '
        'txtTotalCheques
        '
        Me.txtTotalCheques.Color = System.Drawing.Color.Empty
        Me.txtTotalCheques.Decimales = True
        Me.txtTotalCheques.Indicaciones = Nothing
        Me.txtTotalCheques.Location = New System.Drawing.Point(367, 3)
        Me.txtTotalCheques.Name = "txtTotalCheques"
        Me.txtTotalCheques.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalCheques.SoloLectura = True
        Me.txtTotalCheques.TabIndex = 1
        Me.txtTotalCheques.TabStop = False
        Me.txtTotalCheques.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCheques.Texto = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(327, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Total:"
        '
        'dgwOperacion
        '
        Me.dgwOperacion.AllowUserToAddRows = False
        Me.dgwOperacion.AllowUserToDeleteRows = False
        Me.dgwOperacion.AllowUserToOrderColumns = True
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgwOperacion.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgwOperacion.BackgroundColor = System.Drawing.Color.White
        Me.dgwOperacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwOperacion.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colNroOperacion, Me.colCliente, Me.colBanco, Me.colFecha, Me.colImporte, Me.colNroCheque, Me.colSaldo, Me.colEstado, Me.colFechaDeposito, Me.colNroDeposito, Me.colComprobanteDeposito, Me.colNroDescuento, Me.colFechaDescuento, Me.ColNroVencimiento})
        Me.dgwOperacion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgwOperacion.Location = New System.Drawing.Point(4, 41)
        Me.dgwOperacion.Name = "dgwOperacion"
        Me.dgwOperacion.ReadOnly = True
        Me.dgwOperacion.RowHeadersVisible = False
        Me.dgwOperacion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgwOperacion.Size = New System.Drawing.Size(625, 165)
        Me.dgwOperacion.TabIndex = 5
        Me.dgwOperacion.TabStop = False
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "Transaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colNroOperacion
        '
        Me.colNroOperacion.HeaderText = "NroOperación"
        Me.colNroOperacion.Name = "colNroOperacion"
        Me.colNroOperacion.ReadOnly = True
        Me.colNroOperacion.Width = 40
        '
        'colCliente
        '
        Me.colCliente.HeaderText = "Cliente"
        Me.colCliente.Name = "colCliente"
        Me.colCliente.ReadOnly = True
        '
        'colBanco
        '
        Me.colBanco.HeaderText = "Banco"
        Me.colBanco.Name = "colBanco"
        Me.colBanco.ReadOnly = True
        '
        'colFecha
        '
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.ReadOnly = True
        Me.colFecha.Width = 50
        '
        'colImporte
        '
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        Me.colImporte.Width = 70
        '
        'colNroCheque
        '
        Me.colNroCheque.HeaderText = "NroCheque"
        Me.colNroCheque.Name = "colNroCheque"
        Me.colNroCheque.ReadOnly = True
        '
        'colSaldo
        '
        Me.colSaldo.HeaderText = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.ReadOnly = True
        Me.colSaldo.Width = 70
        '
        'colEstado
        '
        Me.colEstado.HeaderText = "Estado"
        Me.colEstado.Name = "colEstado"
        Me.colEstado.ReadOnly = True
        '
        'colFechaDeposito
        '
        Me.colFechaDeposito.HeaderText = "Fecha Deposito"
        Me.colFechaDeposito.Name = "colFechaDeposito"
        Me.colFechaDeposito.ReadOnly = True
        '
        'colNroDeposito
        '
        Me.colNroDeposito.HeaderText = "Nro.Deposito"
        Me.colNroDeposito.Name = "colNroDeposito"
        Me.colNroDeposito.ReadOnly = True
        '
        'colComprobanteDeposito
        '
        Me.colComprobanteDeposito.HeaderText = "Comprobante Deposito"
        Me.colComprobanteDeposito.Name = "colComprobanteDeposito"
        Me.colComprobanteDeposito.ReadOnly = True
        '
        'colNroDescuento
        '
        Me.colNroDescuento.HeaderText = "Nro. Descuento"
        Me.colNroDescuento.Name = "colNroDescuento"
        Me.colNroDescuento.ReadOnly = True
        '
        'colFechaDescuento
        '
        Me.colFechaDescuento.HeaderText = "Fecha Descuento"
        Me.colFechaDescuento.Name = "colFechaDescuento"
        Me.colFechaDescuento.ReadOnly = True
        '
        'ColNroVencimiento
        '
        Me.ColNroVencimiento.HeaderText = "Nro. Vencimiento"
        Me.ColNroVencimiento.Name = "ColNroVencimiento"
        Me.ColNroVencimiento.ReadOnly = True
        '
        'frmConsultaChequeCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(876, 515)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmConsultaChequeCliente"
        Me.Text = "frmConsultaChequeCliente"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        CType(Me.dgwDocumento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        CType(Me.dgwOperacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents chkBanco As System.Windows.Forms.CheckBox
    Friend WithEvents cbxBanco As System.Windows.Forms.ComboBox
    Friend WithEvents chkCliente As System.Windows.Forms.CheckBox
    Friend WithEvents cbxCliente As System.Windows.Forms.ComboBox
    Friend WithEvents btn1 As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidadCheque As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtOperacion As ERP.ocxTXTNumeric
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidadCheque As ERP.ocxTXTNumeric
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotalSaldo As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTotalCheques As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblNroCheque As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents chkFecha As System.Windows.Forms.CheckBox
    Friend WithEvents chkTipo As System.Windows.Forms.CheckBox
    Friend WithEvents cbxTipo As System.Windows.Forms.ComboBox
    Friend WithEvents btn4 As System.Windows.Forms.Button
    Friend WithEvents btn3 As System.Windows.Forms.Button
    Friend WithEvents btn2 As System.Windows.Forms.Button
    Friend WithEvents btn13 As System.Windows.Forms.Button
    Friend WithEvents btn12 As System.Windows.Forms.Button
    Friend WithEvents btn11 As System.Windows.Forms.Button
    Friend WithEvents chkFechaSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents chkTipoSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents cbxTipoSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents btn10 As System.Windows.Forms.Button
    Friend WithEvents chkBancoSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents cbxBancoSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents chkClienteSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents cbxClienteSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidadDocumentos As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidadDocumentos As ERP.ocxTXTNumeric
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNroCheque As ERP.ocxTXTString
    Friend WithEvents txtFechaHasta As ERP.ocxTXTDate
    Friend WithEvents txtFechaDesde As ERP.ocxTXTDate
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtFechaHastaSucursal As ERP.ocxTXTDate
    Friend WithEvents txtFechaDesdeSucursal As ERP.ocxTXTDate
    Friend WithEvents dgwOperacion As System.Windows.Forms.DataGridView
    Friend WithEvents dgwDocumento As System.Windows.Forms.DataGridView
    Friend WithEvents colTipoComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFec As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporteCheque As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotalImporte As ERP.ocxTXTNumeric
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents colIDTransaccion As DataGridViewTextBoxColumn
    Friend WithEvents colNroOperacion As DataGridViewTextBoxColumn
    Friend WithEvents colCliente As DataGridViewTextBoxColumn
    Friend WithEvents colBanco As DataGridViewTextBoxColumn
    Friend WithEvents colFecha As DataGridViewTextBoxColumn
    Friend WithEvents colImporte As DataGridViewTextBoxColumn
    Friend WithEvents colNroCheque As DataGridViewTextBoxColumn
    Friend WithEvents colSaldo As DataGridViewTextBoxColumn
    Friend WithEvents colEstado As DataGridViewTextBoxColumn
    Friend WithEvents colFechaDeposito As DataGridViewTextBoxColumn
    Friend WithEvents colNroDeposito As DataGridViewTextBoxColumn
    Friend WithEvents colComprobanteDeposito As DataGridViewTextBoxColumn
    Friend WithEvents colNroDescuento As DataGridViewTextBoxColumn
    Friend WithEvents colFechaDescuento As DataGridViewTextBoxColumn
    Friend WithEvents ColNroVencimiento As DataGridViewTextBoxColumn
End Class

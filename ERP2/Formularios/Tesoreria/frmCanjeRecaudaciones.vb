﻿Public Class frmCanjeRecaudaciones

    Dim CSistema As New CSistema
    Dim IDTransaccion As Integer = 0
    Dim IDOperacion As Integer = 0

    'VARIABLES
    Dim dtRecaudacionEfectivo As New DataTable
    Dim dtRecaudacionCheque As New DataTable
    Dim dtRecaudacionDocumento As New DataTable
    Dim dtRecaudacionTarjeta As New DataTable

    Dim dtCanjeRecaudacion As New DataTable

    'Dim dtEfectivo As New DataTable
    'Dim dtCheque As New DataTable
    'Dim dtDocumento As New DataTable
    'Dim dtTarjeta As New DataTable

    Dim vControles() As Control
    Dim vNuevo As Boolean


    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "CANJE DE RECAUDACIONES", "CANJREC")
        vNuevo = True

        'Funciones
        CargarInformacion()

        'Controles
        OcxFormaPago1.Inicializar()
        OcxFormaPago1.Tipo = "CONTADO"
        OcxFormaPago1.Form = Me

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        If CBool(vgConfiguraciones("TesoreriaBloquerFecha").ToString) = True Then
            txtfecha.Enabled = False
        Else
            txtfecha.Enabled = True
        End If


    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        'CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, txtfecha)
        CSistema.CargaControl(vControles, txtObservacion)
        'CSistema.CargaControl(vControles, cbxSucursal)

        'Efectivo, Cheques, Otros Cheques
        CSistema.CargaControl(vControles, btnAgregarEfectivo)
        CSistema.CargaControl(vControles, btnAgregarCheques)
        CSistema.CargaControl(vControles, btnAgregarDocumentos)
        CSistema.CargaControl(vControles, btnAgregarTarjetas)
        CSistema.CargaControl(vControles, lklEliminarVenta)

        'Forma de Pago
        CSistema.CargaControl(vControles, OcxFormaPago1)

        'CARGAR ESTRUCTURA DEL DETALLE VENTA

        'CARGAR CONTROLES
        'Ciudad
        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Order By 2")
        txtMoneda.Inicializar()
        txtMoneda.cbxMoneda.cbx.SelectedValue = 1
        txtMoneda.Seleccionar()

    End Sub

    Sub Nuevo()
        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        'detalle
        dgw.Rows.Clear()
        txtTotalRecaudaciones.txt.Clear()
        dtRecaudacionCheque.Rows.Clear()
        dtRecaudacionEfectivo.Rows.Clear()
        dtRecaudacionDocumento.Rows.Clear()

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(MAX(Numero + 1),1) From VCanjeRecaudacion Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)
        dtRecaudacionEfectivo = CSistema.ExecuteToDataTable("Select IDTransaccion,ID,Numero,IDTipoComprobante,TipoComprobante,CodigoComprobante,IDSucursal,Comprobante,Fecha,Fec,IDMoneda,Moneda,ImporteMoneda,Cotizacion,Importe,Depositado,Saldo,'Sel'='False','Cancelar'='False',IDCobrador,Cobrador,NroOperacionCobranza, IdUsuario, Decimales From VEfectivo Where Cancelado = 'False' And Anulado = 'False' And Saldo > 0 And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " Order By Fecha").Copy
        dtRecaudacionCheque = CSistema.ExecuteToDataTable("Select *, 'Sel'='False', 'Cancelar'='False',IdUsuario From VChequeClienteTransaccion Where (Cartera = 'True' Or Rechazado = 'True') Order By Fecha").Copy
        dtRecaudacionDocumento = CSistema.ExecuteToDataTable("Select *,'Sel'='False','Cancelar'='False',IdUsuario From VFormaPagoDocumento Where Deposito='True' And Saldo > 0 and IdTransaccion not in (select IDTransaccion from OrdenPago)").Copy
        dtRecaudacionTarjeta = CSistema.ExecuteToDataTable("select *,'Sel'='False','Cancelar'='False',IDUsuario from VFormaPagoTarjeta where Deposito = 'True' and Saldo > 0").Copy

        ''''''

        'Forma de Pago
        OcxFormaPago1.dtCheques.Rows.Clear()
        OcxFormaPago1.dtChequesTercero.Rows.Clear()
        OcxFormaPago1.dtEfectivo.Rows.Clear()
        OcxFormaPago1.dtDocumento.Rows.Clear()
        OcxFormaPago1.dtFormaPago.Rows.Clear()
        OcxFormaPago1.ListarFormaPago()

        'Otros
        IDTransaccion = 0

        vNuevo = True

        'Cabecera
        txtfecha.Hoy()
        txtObservacion.txt.Clear()

        'Bloquear fecha de la cobranza
        txtfecha.SoloLectura = CType(CSistema.ExecuteScalar("(Select IsNull(TesoreriaBloquerFecha,'False') From Configuraciones Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ") "), Boolean)
        txtfecha.Enabled = Not CType(CSistema.ExecuteScalar("(Select IsNull(TesoreriaBloquerFecha,'False') From Configuraciones Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ") "), Boolean)

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From CanjeRecaudacion Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)


        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        flpRegistradoPor.Visible = False



    End Sub

    Sub CargarOperacion(Optional ByVal vIDTRansaccion As Integer = 0)

        vNuevo = False

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTRansaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From CanjeRecaudacion Where Numero=" & txtID.ObtenerValor & "And IDSucursal = " & cbxSucursal.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTRansaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtCanjeRecaudacion = CSistema.ExecuteToDataTable("Select * From VCanjeRecaudacion Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dtCanjeRecaudacion Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If


        If dtCanjeRecaudacion.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dtCanjeRecaudacion.Rows(0)

        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        txtfecha.SetValueFromString(oRow("Fecha").ToString)
        'ObtenerCuenta()
        txtObservacion.txt.Text = oRow("Observacion").ToString

        'Cargamos el detalle de Comprobantes
        dtRecaudacionEfectivo = CSistema.ExecuteToDataTable("Select * From VDetalleCanjeRecaudacionEfectivo Where IDTransaccion = " & IDTransaccion).Copy
        dtRecaudacionCheque = CSistema.ExecuteToDataTable("Select * From VDetalleCanjeRecaudacionCheque Where IDTransaccion = " & IDTransaccion).Copy
        dtRecaudacionTarjeta = CSistema.ExecuteToDataTable("Select * From VDetalleCanjeRecaudacionTarjeta Where IDTransaccion = " & IDTransaccion).Copy
        dtRecaudacionDocumento = CSistema.ExecuteToDataTable("Select * From VDetalleCanjeRecaudacionDocumento Where IDTransaccion = " & IDTransaccion & " And Deposito = 'True' ").Copy
        ListarComprobantes()

        'Cargar Forma de Pago
        OcxFormaPago1.ListarFormaPago(IDTransaccion)

        'Calcular Totales
        CalcularTotales()


        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("UsuarioTransaccion").ToString
        If CBool(oRow("Anulado").ToString) = True Then

            'Visible Label
            lblUsuarioAnulado.Visible = True
            lblAnulado.Visible = True
            lblFechaAnulado.Visible = True

            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulado").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioAnulado").ToString
        Else
            lblUsuarioAnulado.Visible = False
            lblFechaAnulado.Visible = False
            flpAnuladoPor.Visible = False
        End If


    End Sub

    Sub CargarEfectivo()

        Dim frm As New frmDepositoBancarioSeleccionarEfectivo
        frm.Text = "Selección de Efectivo"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtRecaudacionEfectivo
        frm.IDMoneda = txtMoneda.Registro("ID")
        frm.Decimales = txtMoneda.Registro("Decimales")
        frm.Moneda = txtMoneda.GetValue

        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccionar Efectivo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
        dtRecaudacionEfectivo = frm.dt

        ListarComprobantes()

    End Sub

    Sub CargarCheque()

        Dim frm As New frmDepositoBancarioSeleccionarCheque
        frm.Text = "Selección de Cheque"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtRecaudacionCheque
        frm.IDMoneda = txtMoneda.Registro("ID")
        frm.Decimales = txtMoneda.Registro("Decimales")
        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccionar Cheque", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
        dtRecaudacionCheque = frm.dt

        ListarComprobantes()

    End Sub

    Sub CargarTarjeta()
        Dim frm As New frmDepositoBancarioSeleccionarTarjeta
        frm.Text = "Selección de Tarjeta"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtRecaudacionTarjeta
        frm.IDMoneda = txtMoneda.Registro("ID")
        frm.Decimales = txtMoneda.Registro("Decimales")
        frm.Moneda = txtMoneda.GetValue
        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccionar tarjeta", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
        dtRecaudacionTarjeta = frm.dt

        ListarComprobantes()
    End Sub

    Sub CargarDocumento()
        Dim frm As New frmDepositoBancarioSeleccionarDocumento
        frm.Text = "Selección de Documento"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.IDMoneda = txtMoneda.Registro("ID")
        frm.Decimales = txtMoneda.Registro("Decimales")
        frm.dt = dtRecaudacionDocumento
        frm.Inicializar()
        frm.ShowDialog(Me)
        dtRecaudacionDocumento = frm.dt

        ListarComprobantes()

    End Sub

    Sub EliminarComprobante()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each item As DataGridViewRow In dgw.SelectedRows

            Dim IDTransaccionSeleccionado As String = item.Cells(0).Value
            Dim ID As String = item.Cells(7).Value

            'buscamos en Efectivo
            For Each oRow As DataRow In dtRecaudacionEfectivo.Select(" IDTransaccion = '" & IDTransaccionSeleccionado & "' And ID = '" & ID & "' ")
                oRow("Sel") = False
                Exit For
            Next

            'buscamos en Cheque
            For Each oRow As DataRow In dtRecaudacionCheque.Select(" IDTransaccion = " & IDTransaccionSeleccionado & "")
                oRow("Sel") = False
                Exit For
            Next

            'buscamos en Documento
            For Each oRow As DataRow In dtRecaudacionDocumento.Select(" IDTransaccion = " & IDTransaccionSeleccionado & " And ID = " & ID)
                oRow("Sel") = False
                Exit For
            Next

        Next

        ListarComprobantes()

    End Sub


    Sub ListarComprobantes()

        dgw.Rows.Clear()

        Dim Total As Decimal = 0
        Dim Decimales As Boolean = False
        Dim IDMoneda As Integer = txtMoneda.Registro("ID")
        Decimales = txtMoneda.Registro("Decimales")
        Dim DiferenciaCambio As Decimal = 0

        If CSistema.ExecuteScalar("select count(*) from Cotizacion where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtfecha.GetValue, True, False) & "' and IDMoneda = " & IDMoneda) = 0 And IDMoneda > 1 Then
            MessageBox.Show("No se ha fijado cotizacion para la moneda seleccionada.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
            Exit Sub
        End If

        For Each oRow As DataRow In dtRecaudacionEfectivo.Rows
            If oRow("Sel") = True Then

                Dim Registro(8) As String

                Registro(0) = oRow("IDTransaccion").ToString
                Registro(1) = oRow("CodigoComprobante").ToString
                Registro(2) = ("---")
                Registro(3) = oRow("Comprobante").ToString
                'Registro(4) = (CSistema.FormatoMoneda(oRow("Importe").ToString, oRow("Decimales").ToString))
                Registro(4) = (CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales))
                Registro(5) = CSistema.FormatoMoneda(oRow("Cotizacion").ToString, False)
                Registro(6) = ("---")
                Registro(7) = ("---")
                Registro(8) = oRow("ID").ToString

                Total = Total + CDec(oRow("Importe").ToString)

                dgw.Rows.Add(Registro)

            End If
        Next

        Total = 0

        For Each oRow As DataRow In dtRecaudacionCheque.Rows
            If oRow("Sel") = True Then

                Dim Registro(8) As String

                Registro(0) = oRow("IDTransaccion").ToString
                Registro(1) = oRow("CodigoTipo").ToString
                Registro(2) = oRow("Banco").ToString
                Registro(3) = oRow("NroCheque").ToString
                Registro(4) = CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales)
                Registro(5) = CSistema.FormatoMoneda(oRow("Cotizacion").ToString, False)
                Registro(6) = oRow("Cliente").ToString
                Registro(7) = oRow("CuentaBancaria").ToString
                Registro(8) = 0


                Total = Total + CDec(oRow("Importe").ToString)

                dgw.Rows.Add(Registro)

            End If
        Next

        For Each oRow As DataRow In dtRecaudacionTarjeta.Rows
            If oRow("Sel") = True Then

                Dim Registro(8) As String

                Registro(0) = oRow("IDTransaccion").ToString
                Registro(1) = oRow("CodigoComprobante").ToString
                Registro(2) = ("---")
                Registro(3) = oRow("Comprobante").ToString
                Registro(4) = CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales)
                Registro(5) = CSistema.FormatoMoneda(oRow("Cotizacion").ToString, False)
                Registro(6) = ("---")
                Registro(7) = ("---")
                Registro(8) = oRow("ID").ToString


                Total = Total + CDec(oRow("Importe").ToString)

                dgw.Rows.Add(Registro)

            End If
        Next

        For Each oRow As DataRow In dtRecaudacionDocumento.Rows
            If oRow("Sel") = True Then

                Dim Registro(8) As String

                Registro(0) = oRow("IDTransaccion").ToString
                Registro(1) = oRow("CodigoComprobante").ToString
                Registro(2) = ("---")
                Registro(3) = oRow("Comprobante").ToString
                Registro(4) = (CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales))
                Registro(5) = CSistema.FormatoMoneda(oRow("Cotizacion").ToString, False)
                Registro(6) = ("---")
                Registro(8) = ("---")
                Registro(7) = oRow("ID").ToString

                Total = Total + CDec(oRow("Importe").ToString)

                dgw.Rows.Add(Registro)

            End If
        Next
        CalcularTotales()

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, New Button, New Button, btnAsiento, vControles, New Button)

    End Sub
    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If

        End If

        'Validar Detalle
        If dgw.Rows.Count = 0 Then
            MessageBox.Show("Debe Ingresarlos detalles", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

        'Sucursal
        If cbxSucursal.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Function
        End If

        If cbxSucursal.cbx.Text.Trim = "" Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Function
        End If

        If txtSaldoTotal.ObtenerValor <> 0 Then
            Dim mensaje As String = "El saldo no es válido!"
            MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Function
        End If

        Return True


    End Function

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesRegistro)


        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", CInt(txtID.txt.Text), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtfecha.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", txtMoneda.Registro("ID"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(txtMoneda.Registro("Cotizacion")), ParameterDirection.Input)

        'Totales
        CSistema.SetSQLParameter(param, "@Total", OcxFormaPago1.Total, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpCanjeRecaudacion", False, False, MensajeRetorno, IDTransaccion) = False Then
            MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK)

            Exit Sub

        End If

        Dim Procesar As Boolean = True


        If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then
            If IDTransaccion > 0 Then

                'Cargamos el Detalle de Ventas
                Procesar = InsertarDetalle(IDTransaccion)

                'Cargamos la Forma de Pago
                Procesar = InsertarFormaPago(IDTransaccion)

            End If

        End If


        'Procesar
        Try

            ReDim param(-1)

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            'Aplicar la Cobranza
            If CSistema.ExecuteStoreProcedure(param, "SpCanjeRecaudacionProcesar", False, False, MensajeRetorno,,,, 120) = False Then

            End If

            'Cargamos el Asiento
            'CAsiento.IDTransaccion = IDTransaccion
            'CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        Catch ex As Exception

        End Try


        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False



    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalle = True

        Dim id As Integer = 0
        Dim Decimales As Boolean = False
        Dim IDMoneda As Integer = txtMoneda.Registro("ID")
        Decimales = txtMoneda.Registro("Decimales")

        'Insertar si hay efectivo
        For Each oRow As DataRow In dtRecaudacionEfectivo.Select(" Sel = 'True' ")


            Dim sql As String = "Insert Into DetalleCanjeRecaudacion(IDTransaccion, ID, IDTransaccionEfectivo, IDEfectivo,Importe,IDDetalleDocumento) " &
                                " Values(" & IDTransaccion & ", " & id & ", " & oRow("IDTransaccion").ToString & "," & oRow("ID").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & ",0" & ")"
            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            id = id + 1

        Next

        'Insertar si hay cheques
        For Each oRow As DataRow In dtRecaudacionCheque.Select(" Sel = 'True' ")

            Dim sql As String = "Insert Into DetalleCanjeRecaudacion(IDTransaccion, ID, IDTransaccionCheque, Importe, IDDetalleDocumento) Values(" & IDTransaccion & ", " & id & " , " & oRow("IDTransaccion").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & ",0" & ")"
            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            id = id + 1

        Next

        'Insertar si hay documentos
        For Each oRow As DataRow In dtRecaudacionDocumento.Select(" Sel = 'True' ")

            Dim sql As String = "Insert Into DetalleCanjeRecaudacion(IDTransaccion, ID, IDTransaccionDocumento, Importe,IDDetalleDocumento) Values(" & IDTransaccion & ", " & id & " , " & oRow("IDTransaccion").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & "," & oRow("ID").ToString & ")"
            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            id = id + 1

        Next

        'Insertar si hay tarjetas
        For Each oRow As DataRow In dtRecaudacionTarjeta.Select(" Sel = 'True' ")

            Dim sql As String = "Insert Into DetalleCanjeRecaudacion(IDTransaccion, ID, IDTransaccionTarjeta, Importe,IDDetalleDocumento) Values(" & IDTransaccion & ", " & id & " , " & oRow("IDTransaccion").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & "," & oRow("ID").ToString & ")"
            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            id = id + 1

        Next

    End Function

    Function InsertarFormaPago(ByVal IDTransaccion As Integer) As Boolean

        InsertarFormaPago = True

        Dim dtEfectivo As DataTable
        Dim dtCheque As DataTable
        Dim dtDocumento As DataTable

        Dim ID As Integer = 1

        dtEfectivo = OcxFormaPago1.dtEfectivo.Copy
        dtCheque = OcxFormaPago1.dtCheques.Copy
        dtDocumento = OcxFormaPago1.dtDocumento.Copy

        'Cargar el Efectivo
        For Each oRow As DataRow In dtEfectivo.Rows

            Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe) values(" & IDTransaccion & ", " & ID & ", 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", 'False', 0, NULL, NULL, 'False', 0, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ")"
            Dim sqlEfectivo As String = "Insert Into Efectivo(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, VentasCredito, VentasContado, Gastos, IDMoneda, ImporteMoneda, Cotizacion, Observacion, Depositado, Cancelado, Saldo,ImporteHabilitado) values(" & IDTransaccion & ", " & ID & ", " & cbxSucursal.cbx.SelectedValue & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("Comprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True) & "', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", 0, 0, " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString) & ", '" & oRow("Observacion").ToString & "', 0, 'False', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ")"

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            If CSistema.ExecuteNonQuery(sqlEfectivo) = 0 Then
                Return False
            End If

            ID = ID + 1

        Next

        'Cargar el Documento
        For Each oRow As DataRow In dtDocumento.Rows

            Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, 0, 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ")"
            'Dim sqlDocumento As String = "Insert Into FormaPagoDocumento(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, IDMoneda, ImporteMoneda, Cotizacion, Total, Saldo ,Observacion) values(" & IDTransaccion & ", " & ID & ", " & cbxSucursal.cbx.SelectedValue & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("Comprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True) & "', " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", '" & oRow("Observacion").ToString & "')"
            Dim sqlDocumento As String = "Insert Into FormaPagoDocumento(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, IDMoneda, ImporteMoneda, Cotizacion, Total, Saldo ,Observacion,IDBanco) values(" & IDTransaccion & ", " & ID & ", " & cbxSucursal.cbx.SelectedValue & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("Comprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True) & "', " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", '" & oRow("Observacion").ToString & "'," & oRow("IDBanco").ToString & ")"

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            If CSistema.ExecuteNonQuery(sqlDocumento) = 0 Then
                Return False
            End If

            ID = ID + 1

        Next

        'Cargar el Cheque
        For Each oRow As DataRow In dtCheque.Rows

            If CBool(oRow("Sel").ToString) = True Then

                Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, CancelarCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & oRow("IDTransaccion").ToString & ", NULL, '" & oRow("Cancelar").ToString & "' ,'False', NULL, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ")"

                If CSistema.ExecuteNonQuery(sql) = 0 Then
                    Return False
                End If

                ID = ID + 1

            End If

        Next

        Dim dtChequeTercero As DataTable
        dtChequeTercero = OcxFormaPago1.dtChequesTercero.Copy

        'Cargar el Cheque
        For Each oRow As DataRow In dtChequeTercero.Rows

            If CBool(oRow("Sel").ToString) = True Then

                Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Importe) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & oRow("IDTransaccion").ToString & ", NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ")"

                If CSistema.ExecuteNonQuery(sql) = 0 Then
                    Return False
                End If

                ID = ID + 1

            End If

        Next

    End Function


    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

    End Sub

    Sub CalcularTotales()

        Dim TotalFormaPago As Decimal
        Dim SaldoTotal As Decimal

        Dim TotalEfectivo As Decimal
        Dim TotalCheques As Decimal
        Dim TotalTarjetas As Decimal
        Dim TotalDocumento As Decimal
        Dim TotalRecaudaciones As Decimal

        'Calcula Total Efectivo
        TotalEfectivo = CSistema.dtSumColumn(dtRecaudacionEfectivo, "Importe", "True", "Sel")

        'Calcula Total Documento
        TotalDocumento = CSistema.dtSumColumn(dtRecaudacionDocumento, "Importe", "True", "Sel")

        'Calcula Total Tarjeta
        TotalTarjetas = CSistema.dtSumColumn(dtRecaudacionTarjeta, "Importe", "True", "Sel")

        'Total Banco Local
        dtRecaudacionCheque.DefaultView.RowFilter = "Sel = 'True'"
        TotalCheques = CSistema.TotalesDataTable(dtRecaudacionCheque.DefaultView, "Importe", True)

        'Calcula Total Depositado
        TotalRecaudaciones = TotalEfectivo + TotalCheques + TotalTarjetas + TotalDocumento

        txtTotalRecaudaciones.SetValue(TotalRecaudaciones)
        '''''
        TotalFormaPago = CSistema.FormatoMoneda(OcxFormaPago1.Total, True)

        'Calcular
        SaldoTotal = TotalRecaudaciones - TotalFormaPago

        txtSaldoTotal.SetValue(SaldoTotal)
        OcxFormaPago1.Saldo = SaldoTotal

    End Sub

    Sub VisualizarAsiento()
        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "Canje de Recaudaciones " & txtID.ObtenerValor

            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From CanjeRecaudacion Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)
        Else
            MessageBox.Show("Debe Guardar la Operacion para visualizar el asiento", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If


    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VCanjeRecaudacion Where IDSucursal=" & cbxSucursal.GetValue & " "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VCanjeRecaudacion Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub frmCanjeRecaudaciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnAnular.Click
        Procesar(CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub btnAsiento_Click(sender As Object, e As EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Procesar(CSistema.NUMOperacionesRegistro.INS)

    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAgregarEfectivo_Click(sender As Object, e As EventArgs) Handles btnAgregarEfectivo.Click
        CargarEfectivo()
    End Sub

    Private Sub btnAgregarCheques_Click(sender As Object, e As EventArgs) Handles btnAgregarCheques.Click
        CargarCheque()
    End Sub

    Private Sub btnAgregarDocumentos_Click(sender As Object, e As EventArgs) Handles btnAgregarDocumentos.Click
        CargarDocumento()
    End Sub

    Private Sub btnAgregarTarjetas_Click(sender As Object, e As EventArgs) Handles btnAgregarTarjetas.Click
        CargarTarjeta()
    End Sub

    Private Sub lklEliminarVenta_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lklEliminarVenta.LinkClicked
        EliminarComprobante()
    End Sub

    Private Sub OcxFormaPago1_ImporteModificado(sender As Object, e As EventArgs) Handles OcxFormaPago1.ImporteModificado
        CalcularTotales()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmCanjeRecaudaciones_Activate()
        Me.Refresh()
    End Sub
End Class
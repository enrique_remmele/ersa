﻿Public Class frmValidarCobranza
    Dim CSistema As New CSistema
    Dim CData As New CData

    Dim dtRecibo As DataTable
    Dim dtFactura As DataTable
    Dim dtDetalleCobranzaVenta As New DataTable
    Dim dtFormaPago As DataTable

    Dim dtCheque As DataTable

    Dim IDRecibo As Integer
    Dim IDTransaccion As Integer
    Dim IDTransaccionCheque As Integer
    Dim IDSucursal As Integer

    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    Sub Inicializar()
        Listar()
    End Sub

    Sub Listar()
        Dim Consulta As String
        If chkCobrador.chk.Checked = True Then
            Consulta = "Select 'Sel'= cast(0 as bit) , * From VC_cast_v_Recibos Where Rendido = 0 and IDCobrador =  " & cbxCobrador.cbx.SelectedValue
        Else
            Consulta = "Select 'Sel'= cast(0 as bit) , * From VC_cast_v_Recibos Where Rendido = 0 "
        End If

        'Dim Consulta As String= "Select 'Sel'= cast(0 as bit) , * From VC_cast_v_Recibos Where Rendido = 0 "
        dtRecibo = CSistema.ExecuteToDataTable(Consulta, "", 1000)
        CSistema.dtToGrid(dgvCabecera, dtRecibo)

        CSistema.DataGridColumnasVisibles(dgvCabecera, {"Sel", "Sucursal", "NroRecibo", "Cliente", "Fecha", "Cobrador", "Total", "Anulado"})
        CSistema.DataGridColumnasNumericas(dgvCabecera, {"Total"})
    End Sub

    Sub ListarDetalle()

        If dgvCabecera.SelectedRows.Count = 0 Then
            Exit Sub
        End If
        ctrError.Clear()

        dgvFactura.DataSource = Nothing

        'Validar
        If dgvCabecera.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgvCabecera, Mensaje)
            ctrError.SetIconAlignment(dgvCabecera, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDRecibo = CInt(dgvCabecera.CurrentRow.Cells("ID").Value)
        Dim Consulta As String = "Select * From VC_cast_t_factura_recibos where ID_Recibo =" & IDRecibo
        dtFactura = CSistema.ExecuteToDataTable(Consulta)
        dgvFactura.DataSource = dtFactura

        CSistema.DataGridColumnasVisibles(dgvFactura, {"Comprobante", "FechaEmision", "FechaVencimiento", "Total", "Cobrado"})

        'Formato
        dgvFactura.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        dgvFactura.RowHeadersVisible = False
        dgvFactura.BackgroundColor = Color.White

        CSistema.DataGridColumnasNumericas(dgvFactura, {"Total", "Cobrado"})

        'dgvFactura.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        'dgvFactura.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        'dgvFactura.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        'dgvFactura.Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        'dgvFactura.Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        'dgvFactura.Columns(5).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

        'Obtener el IDTransaccion
        IDRecibo = CInt(dgvCabecera.CurrentRow.Cells("ID").Value)
        Dim Consulta1 As String = "Select * From VC_cast_v_valores_recibo where ID_Recibo =" & IDRecibo
        dtFormaPago = CSistema.ExecuteToDataTable(Consulta1)
        dgvFormaPago.DataSource = dtFormaPago

        CSistema.DataGridColumnasVisibles(dgvFormaPago, {"TipoComprobante", "Cobrado", "NroComprobante", "Fecha", "FechaVencimiento", "Cliente", "Tercerizado", "Banco", "CuentaBancaria"})

        'Formato
        dgvFormaPago.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        dgvFormaPago.RowHeadersVisible = False
        dgvFormaPago.BackgroundColor = Color.White

        CSistema.DataGridColumnasNumericas(dgvFormaPago, {"Cobrado"})

    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)
        tsslEstado.Text = ""
        ctrError.Clear()

        'If ValidarDocumento(Operacion) = False Then
        '    Exit Sub
        'End If
        Dim Procesado As Boolean

        Dim dv As DataView
        dv = New DataView(dgvCabecera.DataSource, "Sel = 'True' ", "", DataViewRowState.CurrentRows)

        Dim gridfiltrado As New DataGridView

        gridfiltrado.DataSource = dv
        CSistema.dtToGrid(gridfiltrado, dv.ToTable())
        Dim dtfiltrado As DataTable = gridfiltrado.DataSource
        'dtRecibo = gridfiltrado.DataSource
        'For Each item As DataGridViewRow In gridfiltrado.Rows
        For i = 0 To dtfiltrado.Rows.Count - 1

            Procesado = dtfiltrado.Rows(i)("Sel")
            If Procesado = False Then
                GoTo Seguir
            End If

            IDSucursal = dtfiltrado.Rows(i)("IDSucursal").ToString


            Dim Consulta As String = "Select * From VC_cast_t_factura_recibos where ID_Recibo =" & dtfiltrado.Rows(i)("ID").ToString
            dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable(Consulta)

            Dim Consulta1 As String = "Select * From VC_cast_v_valores_recibo where ID_Recibo =" & dtfiltrado.Rows(i)("ID").ToString
            dtFormaPago.Clear()
            dtFormaPago = CSistema.ExecuteToDataTable(Consulta1)
            'If dtfiltrado.Rows(i)("YaConciliado") = True Then
            '    GoTo Seguir
            'End If

            Dim param(-1) As SqlClient.SqlParameter
            Dim IndiceOperacion As Integer

            'SetSQLParameter, ayuda a generar y configurar los parametros.
            'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

            If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
                CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            End If

            CSistema.SetSQLParameter(param, "@IDSucursalOperacion", dtfiltrado.Rows(i)("IDSucursal").ToString, ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@Numero", CInt(txtID.ObtenerValor), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@FechaEmision", CSistema.FormatoFechaBaseDatos(dtfiltrado.Rows(i)("FechaEmision").ToString, True, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTipoComprobante", "33", ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroComprobante", dtfiltrado.Rows(i)("NroRecibo").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Comprobante", dtfiltrado.Rows(i)("NroRecibo").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDCliente", dtfiltrado.Rows(i)("IDCliente").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDCobrador", dtfiltrado.Rows(i)("IDCobrador").ToString, ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text.Trim, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroPlanilla", "0", ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDMoneda", "1", ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cotizacion", "1", ParameterDirection.Input)

            'Totales
            CSistema.SetSQLParameter(param, "@Total", dtfiltrado.Rows(i)("Total").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalImpuesto", 0, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalDiscriminado", 0, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalDescuento", 0, ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@DiferenciaCambio", CSistema.FormatoNumeroBaseDatos(txtDiferenciaCambio.txt.Text, False), ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@DiferenciaCambio", CSistema.FormatoNumeroBaseDatos(DiferenciaCambio, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@AnticipoCliente", "False", ParameterDirection.Input)
            'If chkAnticipoCliente.Valor = True Then
            '    CSistema.SetSQLParameter(param, "@IDTipoAnticipo", vIDTipoAnticipo, ParameterDirection.Input)
            'End If
            'CSistema.SetSQLParameter(param, "@ReciboInterno", vReciboInterno, ParameterDirection.Input)
            'Operacion
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
            IndiceOperacion = param.GetLength(0) - 1

            'Transaccion
            'CSistema.SetSQLParameter(param, "@IDMotivoAnulacion", IDMotivoAnulacion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDOperacion", 19, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)
            'CCaja.IDSucursal = cbxSucursal.cbx.SelectedValue
            'CCaja.ObtenerUltimoNumero()

            'CSistema.SetSQLParameter(param, "@IDTransaccionCaja", CCaja.IDTransaccion, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
            CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpCobranzaCredito", False, False, MensajeRetorno, IDTransaccion, False, CadenaConexion, 6000) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnProcesar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnProcesar, ErrorIconAlignment.TopRight)
                Exit Sub
                ''Eliminar la operacion solo si no es para ANULAR
                'If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
                '    If IDTransaccion <> 0 Then
                '        GoTo Eliminar
                '    Else
                '        Exit Sub
                '    End If
                'End If

            End If

            Dim Procesar As Boolean = True

            If IDTransaccion > 0 Then

                'Cargamos el Detalle de VentasCobranza
                Procesar = InsertarDetalleVentas(IDTransaccion)

                'Si hubo un error, eliminar todo
                If Procesar = False Then
                    GoTo Eliminar
                End If
                'FA 25/09/2023 COMENTADO PARA PRUEBA
                Procesar = InsertarFormaPago(IDTransaccion)

                'Si hubo un error, eliminar todo
                If Procesar = False Then
                    GoTo Eliminar
                End If

            End If

            'Aplicar el recibo
            Try

                ReDim param(-1)

                CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

                'Informacion de Salida
                CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
                CSistema.SetSQLParameter(param, "@Procesado", "True", ParameterDirection.Output)

                'Aplicar la Cobranza
                If CSistema.ExecuteStoreProcedure(param, "SpCobranzaCreditoProcesar", False, False, MensajeRetorno, "", False, CadenaConexion, 6000) = False Then
                    MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    GoTo Eliminar
                End If


                'Verificar si se proceso
                If param(param.Length - 1).Value = False Then
                    MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    GoTo Eliminar
                End If

                Dim sql As String = "Update cast_t_recibos Set Rendido = 1 Where id = " & dtfiltrado.Rows(i)("id").ToString

                If CSistema.ExecuteNonQuery(sql, CadenaConexion) = 0 Then
                    '    Return False
                End If

            Catch ex As Exception
                If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
                    GoTo Eliminar
                End If
            End Try

            'Control para casos de recibos anulados
            If dtfiltrado.Rows(i)("Anulado") = 1 Then
                Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
            End If

        Next



        Inicializar()

        Exit Sub

Eliminar:

        Eliminar(IDTransaccion)
Seguir:



    End Sub

    'Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

    '    ValidarDocumento = False

    '    'Ciudad
    '    If cbxCiudad.cbx.SelectedValue = Nothing Then
    '        Dim mensaje As String = "¡Seleccione correctamente la ciudad!"
    '        ctrError.SetError(btnGuardar, mensaje)
    '        ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
    '        tsslEstado.Text = mensaje
    '        Exit Function
    '    End If


    '    'Tipo Comprobante
    '    If cbxTipoComprobante.cbx.Text.Trim = "" Then
    '            Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
    '            ctrError.SetError(btnGuardar, mensaje)
    '            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
    '            tsslEstado.Text = mensaje
    '            vReciboInterno = False
    '            Exit Function
    '        End If

    '        If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
    '            Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
    '            ctrError.SetError(cbxTipoComprobante, mensaje)
    '            ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
    '            tsslEstado.Text = mensaje
    '            Exit Function
    '        End If

    '        'Comprobante
    '        'If (txtComprobante.txt.Text.Trim.Length = 0) And (cbxTipoComprobante.Texto = "RECI" Or cbxTipoComprobante.Texto = "RECIBO" Or Or cbxTipoComprobante.Texto = "RECARE") Then
    '        '    If MessageBox.Show("Desea Generar Recibo Interno?", "Recibo Interno", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
    '        '        vReciboInterno = True
    '        '    Else
    '        '        Dim mensaje As String = "Ingrese un número de comprobante!"
    '        '        ctrError.SetError(btnGuardar, mensaje)
    '        '        ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
    '        '        tsslEstado.Text = mensaje
    '        '        vReciboInterno = False
    '        '        Exit Function
    '        '    End If
    '        'End If
    '        If MessageBox.Show("Desea Generar Recibo Interno?", "Recibo Interno", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
    '            vReciboInterno = True
    '        Else
    '            vReciboInterno = False
    '        End If

    '    'Sucursal
    '    If cbxSucursal.cbx.SelectedValue = Nothing Then
    '            Dim mensaje As String = "Seleccione correctamente la sucursal!"
    '            ctrError.SetError(btnGuardar, mensaje)
    '            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
    '            tsslEstado.Text = mensaje
    '            Exit Function
    '        End If

    '        If cbxSucursal.cbx.Text.Trim = "" Then
    '            Dim mensaje As String = "Seleccione correctamente la sucursal!"
    '            ctrError.SetError(btnGuardar, mensaje)
    '            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
    '            tsslEstado.Text = mensaje
    '            Exit Function
    '        End If

    '        'Venta
    '        If OcxFormaPago1.dgw.Rows.Count = 0 And chkAnticipoCliente.Valor = False Then
    '            Dim mensaje As String = "El documento debe tener comprobantes de pago!"
    '            ctrError.SetError(btnGuardar, mensaje)
    '            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
    '            tsslEstado.Text = mensaje
    '            Exit Function
    '        End If

    '        If txtTotalVenta.ObtenerValor > 0 And chkAnticipoCliente.Valor = True Then
    '            Dim mensaje As String = "El importe de Venta para un Anticipo debe ser Cero!"
    '            ctrError.SetError(btnGuardar, mensaje)
    '            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
    '            tsslEstado.Text = mensaje
    '            Exit Function
    '        End If

    '        If txtTotalVenta.ObtenerValor <= 0 And chkAnticipoCliente.Valor = False Then
    '            Dim mensaje As String = "El importe de los comprobantes de venta no es válido!"
    '            ctrError.SetError(btnGuardar, mensaje)
    '            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
    '            tsslEstado.Text = mensaje
    '            Exit Function
    '        End If

    '        If txtSaldoTotal.ObtenerValor <> 0 And chkAnticipoCliente.Valor = False Then
    '            Dim mensaje As String = "¡Los importes no son validos!"
    '            ctrError.SetError(btnGuardar, mensaje)
    '            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
    '            tsslEstado.Text = mensaje
    '            Exit Function
    '        End If

    '        'Validar Cheques diferidos con facturas contado --Cheques de clientes
    '        If CType(CSistema.ExecuteScalar("(Select IsNull(ContadoChequeDiferido,'False') From Configuraciones Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ") ", CadenaConexion), Boolean) = False Then
    '            Dim dtVentas As New DataTable
    '            Dim dtChequesDiferidos As New DataTable
    '            dtVentas = CData.FiltrarDataTable(dtDetalleCobranzaVenta, " Credito = 'false' and Sel = 'True' ").Copy
    '            dtChequesDiferidos = CData.FiltrarDataTable(OcxFormaPago1.dtCheques, " Tipo = 'DIFERIDO' and Sel = 'True' ").Copy

    '            If dtVentas.Rows.Count > 0 And dtChequesDiferidos.Rows.Count > 0 Then
    '                Dim mensaje As String = "¡No se permite Cheques diferidos con Facturas Contado!"
    '                ctrError.SetError(btnGuardar, mensaje)
    '                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
    '                tsslEstado.Text = mensaje
    '                Exit Function
    '            End If
    '        End If

    '        'Validar Cheques diferidos con facturas contado -- Cheques de terceros
    '        If CType(CSistema.ExecuteScalar("(Select IsNull(ContadoChequeDiferido,'False') From Configuraciones Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ") ", VGCadenaConexion), Boolean) = False Then
    '            Dim dtVentas As New DataTable
    '            Dim dtChequesDiferidos As New DataTable
    '            dtVentas = CData.FiltrarDataTable(dtDetalleCobranzaVenta, " Credito = 'false' and Sel = 'True'  ").Copy
    '            dtChequesDiferidos = CData.FiltrarDataTable(OcxFormaPago1.dtChequesTercero, " Tipo = 'DIFERIDO' and Sel = 'True' ").Copy

    '            If dtVentas.Rows.Count > 0 And dtChequesDiferidos.Rows.Count > 0 Then
    '                Dim mensaje As String = "¡No se permite Cheques diferidos con Facturas Contado!"
    '                ctrError.SetError(btnProcesar, mensaje)
    '                ctrError.SetIconAlignment(btnProcesar, ErrorIconAlignment.MiddleRight)
    '                tsslEstado.Text = mensaje
    '                Exit Function
    '            End If
    '        End If

    '    Return True


    'End Function

    Sub Eliminar(IDTransaccion As Integer)

        tsslEstado.Text = ""
        ctrError.Clear()

        Dim param(-1) As SqlClient.SqlParameter
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "DEL", ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpCobranzaCreditoProcesar", True, True, MensajeRetorno, "", False, CadenaConexion) = False Then
            Exit Sub
        End If

        MessageBox.Show("El sistema elimino el registro por motivos de errores en la carga! Vuelva a intentarlo!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)

    End Sub

    Function InsertarDetalleVentas(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalleVentas = True

        For Each oRow As DataRow In dtDetalleCobranzaVenta.Rows

            'If oRow("Sel") = True Then

            Dim sql As String = "Insert Into VentaCobranza(IDTransaccionVenta, IDTransaccionCobranza, Importe, ImporteGs, Cancelar) Values(" & oRow("IDTransaccion").ToString & ", " & IDTransaccion & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, False) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, False) & ", 'False')"

            If CSistema.ExecuteNonQuery(sql, CadenaConexion) = 0 Then
                Return False
            End If

            'End If

        Next

    End Function

    Function InsertarFormaPago(ByVal IDTransaccion As Integer) As Boolean

        InsertarFormaPago = True

        Dim dtEfectivo As DataTable
        Dim dtDocumento As DataTable
        'Dim dtTarjeta As DataTable
        'Dim dtVentaRetencion As DataTable

        Dim ID As Integer = 1

        'dtEfectivo = OcxFormaPago1.dtEfectivo.Copy
        'dtCheque = OcxFormaPago1.dtCheques.Copy
        'dtDocumento = OcxFormaPago1.dtDocumento.Copy
        'dtTarjeta = OcxFormaPago1.dtTarjeta.Copy
        'dtVentaRetencion = OcxFormaPago1.dtVentaRetencion.Copy

        Dim dvEfectivo As DataView
        dvEfectivo = New DataView(dtFormaPago, "IDTipoComprobante = '20'", "", DataViewRowState.CurrentRows)

        Dim gridEfectivo As New DataGridView

        gridEfectivo.DataSource = dvEfectivo
        CSistema.dtToGrid(gridEfectivo, dvEfectivo.ToTable())
        dtEfectivo = gridEfectivo.DataSource

        Dim dvDocumento As DataView
        dvDocumento = New DataView(dtFormaPago, "IDTipoComprobante in (18,28,34,37,44,46,52,55,58,59,62,66,92,124,125,144,145,148,154)", "", DataViewRowState.CurrentRows)

        Dim gridDocumento As New DataGridView

        gridDocumento.DataSource = dvDocumento
        CSistema.dtToGrid(gridDocumento, dvDocumento.ToTable())
        dtDocumento = gridDocumento.DataSource

        'Dim dvTarjeta As DataView
        'dvTarjeta = New DataView(dtFormaPago, "IDTipoComprobante = '20'", "", DataViewRowState.CurrentRows)

        'Dim gridTarjeta As New DataGridView

        'gridTarjeta.DataSource = dvTarjeta
        'CSistema.dtToGrid(gridTarjeta, dvTarjeta.ToTable())
        'dtTarjeta = gridTarjeta.DataSource

        Dim dvCheque As DataView
        dvCheque = New DataView(dtFormaPago, "IDTipoComprobante in ('146','147','151','152')", "", DataViewRowState.CurrentRows)

        Dim gridCheque As New DataGridView

        gridCheque.DataSource = dvCheque
        CSistema.dtToGrid(gridCheque, dvCheque.ToTable())
        dtCheque = gridCheque.DataSource

        Dim IDMoneda As Integer = 0
        Dim Decimales As Boolean = False
        'dtEfectivo = dtFormaPago.Copy
        'Cargar el Efectivo
        For Each oRow As DataRow In dtEfectivo.Rows
            Try
                IDMoneda = CSistema.RetornarValorInteger(oRow("Moneda").ToString)
                Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & IDMoneda, "VMoneda")("Decimales"))

            Catch ex As Exception

            End Try

            Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe,Cotizacion) values(" & IDTransaccion & ", " & ID & ", 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString) & ", " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, Decimales) & ", 'False', 0, NULL, NULL, 'False', 0, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString) & "," & "1" & ")"
            Dim sqlEfectivo As String = "Insert Into Efectivo(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, VentasCredito, VentasContado, Gastos, IDMoneda, ImporteMoneda, Cotizacion, Observacion, Depositado, Cancelado, Saldo,ImporteHabilitado) values(" & IDTransaccion & ", " & ID & ", " & IDSucursal & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("NroComprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, False) & "', " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString) & ", 0, 0, " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, Decimales) & ", " & "1" & ", '" & "Prueba" & "', 0, 'False', " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, Decimales) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, Decimales) & ")"


            If CSistema.ExecuteNonQuery(sql, CadenaConexion) = 0 Then
                Return False
            End If

            If CSistema.ExecuteNonQuery(sqlEfectivo, CadenaConexion) = 0 Then
                Return False
            End If

            ID = ID + 1

        Next

        'Cargar el Documento
        For Each oRow As DataRow In dtDocumento.Rows
            IDMoneda = CSistema.RetornarValorInteger(oRow("IDMoneda").ToString)
            Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & IDMoneda, "VMoneda")("Decimales"))

            Dim sql As String
            Dim sqlDocumento As String
            If oRow("IDTipoComprobante").ToString = "55" Then
                sql = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe,cotizacion) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, 0, 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString * (-1), Decimales) & ", " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString * (-1), Decimales) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString * (-1)) & "," & "1" & ")"
                sqlDocumento = "Insert Into FormaPagoDocumento(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, IDMoneda, ImporteMoneda, Cotizacion, Total, Saldo ,Observacion, IDBanco) values(" & IDTransaccion & ", " & ID & ", " & IDSucursal & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("NroComprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, False) & "', " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString * (-1), Decimales) & ", " & "1" & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString * (-1), Decimales) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString * (-1), Decimales) & ",''," & oRow("IDBanco") & " )"
            Else
                sql = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe,cotizacion) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, 0, 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, Decimales) & ", " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, Decimales) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString) & "," & "1" & ")"
                sqlDocumento = "Insert Into FormaPagoDocumento(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, IDMoneda, ImporteMoneda, Cotizacion, Total, Saldo ,Observacion, IDBanco) values(" & IDTransaccion & ", " & ID & ", " & IDSucursal & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("NroComprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, False) & "', " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, Decimales) & ", " & "1" & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, Decimales) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, Decimales) & ",''," & oRow("IDBanco") & " )"
            End If

            'Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe,cotizacion) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, 0, 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, Decimales) & ", " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, Decimales) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString) & "," & "1" & ")"
            'Dim sqlDocumento As String = "Insert Into FormaPagoDocumento(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, IDMoneda, ImporteMoneda, Cotizacion, Total, Saldo ,Observacion, IDBanco) values(" & IDTransaccion & ", " & ID & ", " & IDSucursal & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("NroComprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, False) & "', " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, Decimales) & ", " & "1" & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, Decimales) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, Decimales) & ",''," & oRow("IDBanco") & " )"

            If CSistema.ExecuteNonQuery(sql, CadenaConexion) = 0 Then
                Return False
            End If

            If CSistema.ExecuteNonQuery(sqlDocumento, CadenaConexion) = 0 Then
                Return False
            End If

            'Actualizar el saldo de la NC
            'Solo si el tipo de comprobante nc
            If oRow("IDTipoComprobante").ToString = "154" Then

                'For Each oRow1 As DataRow In dtVentaRetencion.Select("Sel='True' And ID=" & oRow("ID").ToString & " And ComprobanteRetencion='" & oRow("Comprobante").ToString & "' ")
                'dbs ver importeMoneda
                Dim sql1 As String = "Update NotaCredito set Saldo = Saldo - '" & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, Decimales) & "' Where IDTransaccion = " & oRow("IDTransaccionNotaCredito").ToString

                If CSistema.ExecuteNonQuery(sql1, CadenaConexion) = 0 Then
                    Return False
                End If

                'Next

            End If

            ID = ID + 1

        Next

        'Cargar el Cheque
        'For Each oRow As DataRow In dtCheque.Rows
        If dtCheque IsNot Nothing Then
            GuardarCheque()
        End If


        ''If CBool(oRow("Sel").ToString) = True Then
        'IDMoneda = CSistema.RetornarValorInteger(oRow("IDMoneda").ToString)
        'Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & IDMoneda, "VMoneda")("Decimales"))

        ''Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, CancelarCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe,cotizacion) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & ", " & oRow("IDTransaccion").ToString & ", NULL, '" & oRow("Cancelar").ToString & "' ,'False', NULL, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteGS").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("cotizacionHoy").ToString) & ")"
        ''Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, CancelarCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe,cotizacion) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & ", " & IDTransaccionCheque & ", NULL, '" & oRow("Cancelar").ToString & "' ,'False', NULL, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteGS").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("cotizacion").ToString) & ")"
        'Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, CancelarCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe, cotizacion) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, False) & ", " & IDTransaccionCheque & ", NULL, 'False' ,'False', NULL, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("cotizacion").ToString) & ")"
        'If CSistema.ExecuteNonQuery(Sql, CadenaConexion) = 0 Then
        '    Return False
        'End If

        'ID = ID + 1

        ''End If

        'Next

        ''Cargar la tarjeta
        'For Each oRow As DataRow In dtTarjeta.Rows
        '    IDMoneda = CSistema.RetornarValorInteger(oRow("IDMoneda").ToString)
        '    Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & IDMoneda, "VMoneda")("Decimales"))

        '    Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe,Cotizacion) values(" & IDTransaccion & ", " & ID & ", 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales) & ", " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales) & ", 'False', 0, NULL, NULL, 'False', 0, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("cotizacion").ToString) & ")"
        '    'Dim sqlTarjeta As String = "Insert Into FormaTarjeta(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, IDMoneda, ImporteMoneda, Cotizacion, Total, Saldo ,Observacion, TerminacionTarjeta, FechaVencimientoTarjeta, IDTipoTarjeta, TipoComprobante, Moneda, Importe) values(" & IDTransaccion & ", " & ID & ", " & cbxSucursal.cbx.SelectedValue & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("Comprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True) & "', " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", '" & oRow("Observacion").ToString & ", '" & oRow("TerminacionTarjeta").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("FechaVencimientoTarjeta").ToString, True, True) & ", '" & oRow("IDTipoTarjeta").ToString & ", '" & oRow("TipoComprobante").ToString & ", '" & oRow("Moneda").ToString & ", '" & oRow("Importe").ToString & "')"
        '    Dim sqlTarjeta As String = String.Empty
        '    Try
        '        sqlTarjeta =
        '       String.Format("Insert Into FormaPagoTarjeta(IDTransaccion, ID, " &
        '                                           "IDSucursal, IDTipoComprobante, Comprobante, " &
        '                                           "Fecha, IDMoneda, ImporteMoneda, Cotizacion, Total, " &
        '                                           "Saldo, Observacion, TerminacionTarjeta, " &
        '                                           "FechaVencimientoTarjeta, IDTipoTarjeta, TipoComprobante, " &
        '                                             "Moneda, Importe, Boleta, cancelado) values({0},{1},{2},{3},'{4}','{5}',{6},{7},{8},{9},{10},'{11}','{12}','{13}',{14},'{15}','{16}',{17},'{18}','{19}')",
        '                                            IDTransaccion,
        '                                            ID, cbxSucursal.cbx.SelectedValue,
        '                                            oRow("IDTipoComprobante").ToString,
        '                                            oRow("Comprobante").ToString,
        '                                            CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True),
        '                                            oRow("IDMoneda").ToString,
        '                                            CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales),
        '                                            CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString),
        '                                            CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString),
        '                                            CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString),
        '                                            oRow("Observacion").ToString,
        '                                            oRow("TerminacionTarjeta").ToString,
        '                                            CSistema.FormatoFechaBaseDatos(oRow("FechaVencimientoTarjeta").ToString, True, True),
        '                                            oRow("IDTipoTarjeta").ToString,
        '                                            oRow("TipoComprobante").ToString, oRow("Moneda").ToString,
        '                                            CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales), oRow("Boleta").ToString, 0)
        '    Catch ex As Exception
        '        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    End Try


        '    If CSistema.ExecuteNonQuery(sql) = 0 Then
        '        Return False
        '    End If

        '    If CSistema.ExecuteNonQuery(sqlTarjeta) = 0 Then
        '        Return False
        '    End If

        '    ID = ID + 1

        'Next

    End Function

    Sub GuardarCheque()

        tsslEstado.Text = ""
        ctrError.Clear()


        Dim vIDTransaccion As Integer = 0
        Dim IndiceOperacion As Integer = 0

        'For i = 0 To dtCheque.Rows.Count - 1
        For Each oRow As DataRow In dtCheque.Rows
            Dim param(-1) As SqlClient.SqlParameter

            'CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDSucursalDocumento", oRow("IDSucursal").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDCliente", oRow("IDCliente").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDBanco", oRow("IDBanco").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@CuentaBancaria", oRow("CuentaBancaria").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@FechaCobranza", CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroCheque", oRow("NroComprobante").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDMoneda", oRow("IDMoneda").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMoneda(oRow("Cotizacion").ToString, False), ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(txtImporte.ObtenerValor, txtImporte.Decimales), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ImporteMoneda", CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, False), ParameterDirection.Input)
            If CSistema.FormatoFechaBaseDatos(oRow("FechaVencimiento").ToString, True, False) > CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, False) Then
                CSistema.SetSQLParameter(param, "@Diferido", True, ParameterDirection.Input)
            Else
                CSistema.SetSQLParameter(param, "@Diferido", False, ParameterDirection.Input)
            End If
            CSistema.SetSQLParameter(param, "@Titular", oRow("Cliente").ToString, ParameterDirection.Input)

            If CSistema.FormatoFechaBaseDatos(oRow("FechaVencimiento").ToString, True, False) > CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, False) Then
                CSistema.SetSQLParameter(param, "@FechaVencimiento", CSistema.FormatoFechaBaseDatos(oRow("FechaVencimiento").ToString, True, False), ParameterDirection.Input)
            End If

            If oRow("Cliente").ToString <> oRow("Tercerizado").ToString Then
                CSistema.SetSQLParameter(param, "@ChequeTercero", True, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Librador", oRow("Tercerizado").ToString, ParameterDirection.Input)
            Else
                CSistema.SetSQLParameter(param, "@ChequeTercero", False, ParameterDirection.Input)
            End If

            CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)
            IndiceOperacion = param.GetLength(0) - 1

            'Transaccion
            CSistema.SetSQLParameter(param, "@IDOperacion", 22, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
            CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpChequeCliente", False, False, MensajeRetorno, IDTransaccionCheque) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                'ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                'ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

                'Eliminar el Registro si es que se registro
                If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From ChequeCliente Where IDTransaccion=" & IDTransaccionCheque & ") Is Null Then 'False' Else 'True' End)")) = True Then
                    param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                    CSistema.ExecuteStoreProcedure(param, "SpChequeCliente", False, False, MensajeRetorno, IDTransaccionCheque)
                End If

                Exit Sub

            End If

            Dim IDMoneda As Integer = 0
            Dim Decimales As Boolean = False
            Dim ID As Integer = 1
            ID = CSistema.ExecuteScalar("Select IsNull(Max(ID)+1,1) From FormaPago Where IDTransaccion = " & IDTransaccion)
            IDMoneda = CSistema.RetornarValorInteger(oRow("IDMoneda").ToString)
            Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & IDMoneda, "VMoneda")("Decimales"))

            'Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, CancelarCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe,cotizacion) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & ", " & oRow("IDTransaccion").ToString & ", NULL, '" & oRow("Cancelar").ToString & "' ,'False', NULL, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteGS").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("cotizacionHoy").ToString) & ")"
            'Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, CancelarCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe,cotizacion) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & ", " & IDTransaccionCheque & ", NULL, '" & oRow("Cancelar").ToString & "' ,'False', NULL, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteGS").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("cotizacion").ToString) & ")"
            Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, CancelarCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe, cotizacion) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, False) & ", " & IDTransaccionCheque & ", NULL, 'False' ,'False', NULL, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("cotizacion").ToString) & ")"
            If CSistema.ExecuteNonQuery(sql, CadenaConexion) = 0 Then
                'Return False
                Exit Sub
            End If

            ID = ID + 1

        Next

    End Sub

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        Dim dtCobranza As DataTable
        dtCobranza = CSistema.ExecuteToDataTable("Select * From CobranzaCredito Where IDTransaccion = " & IDTransaccion)
        tsslEstado.Text = ""
        ctrError.Clear()

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        For i = 0 To dtCobranza.Rows.Count - 1

            If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
                CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            End If

            CSistema.SetSQLParameter(param, "@IDSucursalOperacion", dtCobranza.Rows(i)("IDSucursal").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Numero", dtCobranza.Rows(i)("NUmero").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@FechaEmision", CSistema.FormatoFechaBaseDatos(dtCobranza.Rows(i)("FechaEmision").ToString, True, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTipoComprobante", dtCobranza.Rows(i)("IDTipoComprobante").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroComprobante", dtCobranza.Rows(i)("NroComprobante").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Comprobante", dtCobranza.Rows(i)("Comprobante").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDCliente", dtCobranza.Rows(i)("IDCliente").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDCobrador", dtCobranza.Rows(i)("IDCobrador").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Observacion", dtCobranza.Rows(i)("Observacion").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroPlanilla", dtCobranza.Rows(i)("NroPlanilla").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDMotivoAnulacion", 5, ParameterDirection.Input)
            'Totales
            CSistema.SetSQLParameter(param, "@Total", dtCobranza.Rows(i)("Total").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalImpuesto", 0, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalDiscriminado", 0, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalDescuento", 0, ParameterDirection.Input)

            'Operacion
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
            IndiceOperacion = param.GetLength(0) - 1

            'Transaccion
            CSistema.SetSQLParameter(param, "@IDOperacion", 19, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
            CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpCobranzaCredito", False, False, MensajeRetorno, IDTransaccion, False, CadenaConexion, 6000) = False Then
                MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

        Next

    End Sub

    Private Sub frmValidarCobranza_Load(sender As Object, e As EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnProcesar_Click(sender As Object, e As EventArgs) Handles btnProcesar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

    End Sub

    Private Sub dgvCabecera_Click(sender As Object, e As EventArgs) Handles dgvCabecera.Click
        dgvCabecera.SelectedRows(0).Cells("Sel").Value = CStr(Not CBool(dgvCabecera.SelectedRows(0).Cells("Sel").Value))
        ListarDetalle()
    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub chkCobrador_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkCobrador.PropertyChanged
        If chkCobrador.chk.Checked = True Then
            CSistema.SqlToComboBox(cbxCobrador.cbx, "Select ID, Nombres From Cobrador where Estado = 1 Order By 2", CadenaConexion)
            cbxCobrador.Enabled = True
        Else
            cbxCobrador.Enabled = False
        End If
    End Sub

End Class
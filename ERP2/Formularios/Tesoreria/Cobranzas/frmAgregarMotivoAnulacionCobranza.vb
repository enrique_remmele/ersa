﻿Public Class frmAgregarMotivoAnulacionCobranza
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDMotivoValue As Integer
    Public Property IDMotivo() As Integer
        Get
            Return IDMotivoValue
        End Get
        Set(ByVal value As Integer)
            IDMotivoValue = value
        End Set
    End Property

    Public Property Procesado As Boolean

    'CLASES
    Dim CSistema As New CSistema

    Sub Inicializar()

        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion from MotivoAnulacionCobranza where estado = 1")
        Procesado = False
        IDMotivo = 0

    End Sub

    Private Sub frmAgregarMotivoAlulacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        IDMotivo = cbxMotivo.GetValue
        Close()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmAgregarMotivoAnulacionCobranza_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmAplicacionVentasAnticipo
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CAsiento As New CAsientoCobranza
    Dim CData As New CData
    Dim CCaja As New CCaja
    Dim vdecimalOperacion As Boolean

    'PROPIEDADES
    Private CadenaConexionValue As String

    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    Private PropietarioBDValue As String
    Public Property PropietarioBD() As String
        Get
            Return PropietarioBDValue
        End Get
        Set(ByVal value As String)
            PropietarioBDValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDTransaccionVisualizarValue As Integer
    Public Property IDTransaccionVisualizar() As Integer
        Get
            Return IDTransaccionVisualizarValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionVisualizarValue = value
        End Set
    End Property

    Private IDTransaccionCobranzaValue As Integer
    Public Property IDTransaccionCobranza() As Integer
        Get
            Return IDTransaccionCobranzaValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionCobranzaValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property

    Private MonedaValue As String
    Public Property Moneda() As String
        Get
            Return MonedaValue
        End Get
        Set(ByVal value As String)
            MonedaValue = value
        End Set
    End Property

    Private NroPlanillaValue As Integer
    Public Property NroPlanilla() As Integer
        Get
            Return NroPlanillaValue
        End Get
        Set(ByVal value As Integer)
            NroPlanillaValue = value
        End Set
    End Property

    Private SaldoValue As Decimal
    Public Property Saldo() As Decimal
        Get
            Return SaldoValue
        End Get
        Set(ByVal value As Decimal)
            SaldoValue = value
        End Set
    End Property

    Private orowConfiguracionesValue As DataRow
    Public Property orowConfiguraciones() As DataRow
        Get
            Return orowConfiguracionesValue
        End Get
        Set(ByVal value As DataRow)
            orowConfiguracionesValue = value
        End Set
    End Property

    Public Property IDMoneda As Integer
    Public Property Cotizacion As Decimal

    'EVENTOS

    'VARIABLES
    Dim dtDetalleCobranzaVenta As New DataTable
    Dim dtVentasPendientes As New DataTable
    Dim dtVentasRetencion As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Otros

        'Propiedades
        'IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "APLICAR VENTAS A ANTICIPO", "AVAN", CadenaConexion)
        vNuevo = False

        'Cotizacion
        ' txtMoneda.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
        ' txtMoneda.Inicializar()
        'txtMoneda.txtCotizacion.SoloLectura = True
        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.IDSucursal = cbxSucursal.GetValue
        CAsiento.IDMoneda = 1
        CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)
        txtFecha.Hoy()
        txtCotizacionCobranza.Enabled = False

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        If IDTransaccionVisualizar > 0 Then
            CargarOperacion(IDTransaccionVisualizar)
        End If
    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtCliente)
        'CSistema.CargaControl(vControles, cbxCobrador)
        CSistema.CargaControl(vControles, txtObservacion)
        'CSistema.CargaControl(vControles, txtPlanilla)
        'CSistema.CargaControl(vControles, cbxSucursal)
        'CSistema.CargaControl(vControles, dgw)
        'CSistema.CargaControl(vControles, txtMoneda)

        'Ventas
        CSistema.CargaControl(vControles, btnAgregarVenta)
        CSistema.CargaControl(vControles, lklEliminarVenta)

        'CARGAR ESTRUCTURA DEL DETALLE VENTA
        dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Select Top(0) * From VVentaDetalleCobranzaCredito ", CadenaConexion).Clone

        'CARGAR ESTRUCTURA DE VENTAS SI ES QUE SE ESTA
        dtVentasRetencion = CData.GetStructure("VFormaPagoDocumentoRetencion", "Select Top(0) 'Sel'='False', IDTransaccion, 'ID'=0, IDTransaccionVenta, 'ComprobanteVenta'='', 'FechaVenta'=GetDate(), 'TotalImpuesto'=0.00, 'Porcentaje'=0.00, 'Importe'=0.00, 'ComprobanteRetencion'='', 'Aplicado'='False' From FormaPagoDocumentoRetencion ")


        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2", CadenaConexion)

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion, CadenaConexion)

        'Cobrador
        CSistema.SqlToComboBox(cbxCobrador.cbx, "Select ID, Nombres From Cobrador where IdSucursal =" & cbxSucursal.cbx.SelectedValue & " Order By 2", CadenaConexion)
        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'COBRADOR
        cbxCobrador.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "COBRADOR", "")

        'Ultimo Registro
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From VAnticipoAplicacion Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

        'Cobrador
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "COBRADOR", cbxCobrador.cbx.Text)

    End Sub

    Sub Nuevo()
        CSistema.SqlToComboBox(cbxCobrador.cbx, "Select ID, Nombres From Cobrador where IdSucursal =" & cbxSucursal.cbx.SelectedValue & " and Estado = 1 Order By 2", CadenaConexion)
        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Limpiar detalle
        dtDetalleCobranzaVenta.Rows.Clear()
        dtVentasPendientes.Rows.Clear()
        dtVentasRetencion.Rows.Clear()
        'txtMoneda.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
        'txtMoneda.Inicializar()
        'txtMoneda.txtCotizacion.SoloLectura = True
        txtMoneda_CambioMoneda()

        txtTipoMoneda.txt.Text = CSistema.ExecuteScalar("Select Referencia from moneda where id = " & IDMoneda)
        txtCotizacionCobranza.Texto = Cotizacion
        txtTipoMoneda.SoloLectura = True
        txtCotizacionCobranza.SoloLectura = True
        ListarVentas()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        CAsiento.Limpiar()

        vNuevo = True

        'Cabecera
        'txtFecha.txt.Text = ""
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()
        txtCliente.Clear()

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("(Select IsNull(MAX(Numero+1),1) From VAnticipoAplicacion Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ") ", CadenaConexion), Integer)
        txtComprobante.txt.Text = CSistema.ObtenerProximoNroComprobante(cbxTipoComprobante.cbx.SelectedValue, "VAnticipoAplicacion", "NroComprobante", cbxSucursal.cbx.SelectedValue, CadenaConexion)

        'Cliente
        'txtCliente.Conectar("Select Distinct C.ID, C.Referencia, C.RazonSocial, C.RUC, C.NombreFantasia, C.Direccion, C.Telefono, C.ListaPrecio, C.Descuento, C.Deuda, C.SaldoCredito, C.Vendedor, C.Promotor, C.Cobrador, C.Ciudad, C.Barrio, C.ZonaVenta, C.TieneSucursales, C.ClienteVario From VCliente C Where C.IDSucursal=" & cbxSucursal.GetValue & " Order By RazonSocial", CadenaConexion)

        txtPlanilla.txt.Text = NroPlanilla
        'txtMoneda.SetValue(Moneda, "1")
        vdecimalOperacion = CSistema.RetornarValorBoolean(CData.GetRow(" referencia = '" & Moneda & "'", "vMoneda")("Decimales").ToString)
        txtTotalVentaOper.Decimales = vdecimalOperacion
        txtFecha.Hoy()
        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Bloquear fecha de la cobranza
        txtFecha.SoloLectura = CType(CSistema.ExecuteScalar("(Select IsNull(CobranzaCreditoBloquearFecha,'False') From Configuraciones Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ") ", CadenaConexion), Boolean)

        LimpiarControles()
        txtCliente.SetValue(IDCliente)
        'Poner el foco en el proveedor
        cbxTipoComprobante.cbx.Focus()
        btnAgregarVenta.Enabled = True
    End Sub

    Sub Cancelar()
        'Funciones
        CargarInformacion()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        'LimpiarControles()

        'Ultimo Registro
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()
        txtID.txt.SelectAll()
        btnAgregarVenta.Enabled = False
    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Ciudad
        If cbxCiudad.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "¡Seleccione correctamente la ciudad!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If
        If txtFecha.txt.Text = "  /  /" Then
            Dim mensaje As String = "¡Seleccione correctamente la fecha!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If txtCliente.txtID.ObtenerValor <> IDCliente Then
            Dim mensaje As String = "¡El Cliente no pertenece a la cobranza asociada!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Cuando no es por anulacion
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            'Tipo Comprobante
            If cbxTipoComprobante.cbx.Text.Trim = "" Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(cbxTipoComprobante, mensaje)
                ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            'Comprobante
            If txtComprobante.txt.Text.Trim.Length = 0 Then
                Dim mensaje As String = "Ingrese un número de comprobante!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            'Sucursal
            If cbxSucursal.cbx.SelectedValue = Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If cbxSucursal.cbx.Text.Trim = "" Then
                Dim mensaje As String = "Seleccione correctamente la sucursal!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If txtTotalVenta.ObtenerValor <= 0 Then
                Dim mensaje As String = "El importe de los comprobantes de venta no es válido!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If txtTotalVentaOper.ObtenerValor > (Saldo * (-1)) Then
                Dim mensaje As String = "El Total de Ventas sobrepasa el saldo del Anticipo!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

        End If

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("¡Atención! Esto anulará permanentemente el registro. ¿Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If

        End If

        Return True


    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)
        tsslEstado.Text = ""
        ctrError.Clear()

        'Generar la cabecera del asiento
        'GenerarAsiento()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Numero", CInt(txtID.ObtenerValor), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTransaccionCobranza", IDTransaccionCobranza, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", IDMoneda, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoNumeroBaseDatos(txtCotizacionCobranza.Texto, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DiferenciaCambio", CSistema.FormatoNumeroBaseDatos(txtDiferenciaCambio.txt.Text, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoNumeroBaseDatos(txtTotalVentaOper.txt.Text, vdecimalOperacion), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text.Trim, ParameterDirection.Input)
        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)
        CCaja.IDSucursal = cbxSucursal.cbx.SelectedValue
        CCaja.ObtenerUltimoNumero()

        CSistema.SetSQLParameter(param, "@IDTransaccionCaja", CCaja.IDTransaccion, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAnticipoAplicacion", False, False, MensajeRetorno, IDTransaccion, False, CadenaConexion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar la operacion solo si no es para ANULAR
            If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
                If IDTransaccion <> 0 Then
                    GoTo Eliminar
                Else
                    Exit Sub
                End If
            End If

        End If

        Dim Procesar As Boolean = True

        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
            CargarOperacion(IDTransaccion)
            txtID.SoloLectura = False
            Exit Sub
        End If

        If IDTransaccion > 0 Then

            '01/04/2022 - SC - para que en el saldo tome las aplicaciones de anticipo
            'Cargamos el Detalle de VentasCobranza
            Procesar = InsertarDetalleVentas(IDTransaccionCobranza)

            'Cargamos el Detalle de Ventas
            Procesar = InsertarAnticipoVentas(IDTransaccion)

            'Si hubo un error, eliminar todo
            If Procesar = False Then
                GoTo Eliminar
            End If

            'Si hubo un error, eliminar todo
            If Procesar = False Then
                GoTo Eliminar
            End If

        End If

        'Aplicar el recibo
        Try

            ReDim param(-1)

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "True", ParameterDirection.Output)

            'Aplicar la Cobranza
            If CSistema.ExecuteStoreProcedure(param, "SpAnticipoAplicacionProcesar", False, False, MensajeRetorno, "", False, CadenaConexion) = False Then
                MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                GoTo Eliminar
            End If

            'Verificar si se proceso
            If param(param.Length - 1).Value = False Then
                MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                GoTo Eliminar
            End If

        Catch ex As Exception
            If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
                GoTo Eliminar
            End If
        End Try

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False
        txtID.txt.Focus()

        Exit Sub

Eliminar:

        Eliminar(IDTransaccion)

    End Sub

    Sub Eliminar(IDTransaccion As Integer)

        tsslEstado.Text = ""
        ctrError.Clear()

        Dim param(-1) As SqlClient.SqlParameter
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "DEL", ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAnticipoAplicacionProcesar", True, True, MensajeRetorno, "", False, CadenaConexion) = False Then
            Exit Sub
        End If

        MessageBox.Show("El sistema elimino el registro por motivos de errores en la carga! Vuelva a intentarlo!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)

    End Sub

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            txtID.Focus()
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAnticipoAplicacion", False, False, MensajeRetorno, IDTransaccion, False, CadenaConexion) = False Then
            MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If


        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False


    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        Dim Comprobante As String = cbxTipoComprobante.cbx.Text & ": " & txtComprobante.txt.Text

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante
            'Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From vAnticipoAplicacion Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & "), 0 )", CadenaConexion)
            frm.IDTransaccion = IDTransaccionVisualizar

            'Mostramos
            frm.ShowDialog(Me)


        Else

            'Validar
            If cbxCiudad.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la ciudad de operacion!"
                ctrError.SetError(cbxCiudad, mensaje)
                ctrError.SetIconAlignment(cbxCiudad, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante

            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            frm.CalcularTotales()

            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
            CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            CAsiento.NroCaja = frm.CAsiento.NroCaja
            CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                CAsiento.dtAsiento.Clear()
                CAsiento.dtDetalleAsiento.Clear()
                VisualizarAsiento()
            End If

        End If

    End Sub

    Sub GenerarAsiento()

        'EstablecerCabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = cbxCiudad.GetValue
        oRow("IDSucursal") = cbxSucursal.GetValue
        oRow("Fecha") = txtFecha.GetValue
        oRow("IDMoneda") = 1
        oRow("Cotizacion") = 1
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = txtComprobante.txt.Text
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text
        oRow("Detalle") = txtObservacion.txt.Text
        oRow("Total") = txtTotalVenta.ObtenerValor

        CAsiento.IDMoneda = IDMoneda
        CAsiento.IDSucursal = cbxSucursal.GetValue

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)

        'Solo establecer la primera vez, esto es para el detalle
        If CAsiento.Generado = True Then
            Exit Sub
        End If
        CAsiento.InicializarAsiento()
        CAsiento.dtVenta = CData.FiltrarDataTable(dtDetalleCobranzaVenta, " Sel = 'True' ").Copy

        CAsiento.TotalDiferenciaCambio = CSistema.FormatoMoneda(txtDiferenciaCambio.txt.Text, False)

        CAsiento.Total = txtTotalVenta.ObtenerValor

        CAsiento.Generar()

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VAnticipoAplicacion Where IDSucursal=" & cbxSucursal.GetValue, CadenaConexion), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VAnticipoAplicacion Where IDSucursal=" & cbxSucursal.GetValue, CadenaConexion), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Function InsertarDetalleVentas(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalleVentas = True

        For Each oRow As DataRow In dtDetalleCobranzaVenta.Rows

            If oRow("Sel") = True Then

                Dim sql As String = "Insert Into VentaCobranza(IDTransaccionVenta, IDTransaccionCobranza, Importe, ImporteGs, Cancelar) Values(" & oRow("IDTransaccion").ToString & ", " & IDTransaccion & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, vdecimalOperacion) & ", " & CSistema.FormatoMonedaBaseDatos((oRow("Importe").ToString * txtCotizacionCobranza.Texto), False) & ", 'False')"

                If CSistema.ExecuteNonQuery(sql, CadenaConexion) = 0 Then
                    Return False
                End If

            End If

        Next

    End Function

    Function InsertarAnticipoVentas(ByVal IDTransaccion As Integer) As Boolean

        InsertarAnticipoVentas = True

        For Each oRow As DataRow In dtDetalleCobranzaVenta.Rows

            If oRow("Sel") = True Then

                Dim sql As String = "Insert Into AnticipoVenta(IDTransaccionVenta, IDTransaccionAnticipo, Importe, ImporteGs, Cancelar) Values(" & oRow("IDTransaccion").ToString & ", " & IDTransaccion & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, vdecimalOperacion) & ", " & CSistema.FormatoMonedaBaseDatos((oRow("Importe").ToString * txtCotizacionCobranza.Texto), False) & ", 'False')"

                If CSistema.ExecuteNonQuery(sql, CadenaConexion) = 0 Then
                    Return False
                End If

            End If

        Next

    End Function

    Function InsertarDetalleVenta(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalleVenta = False

    End Function

    Sub ObtenerVentasClientes()

        If txtCliente.Seleccionado = True Then
            'dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Select * From VVentasPendientes Where IDCliente=" & txtCliente.Registro("ID").ToString & " And Saldo > 0", CadenaConexion).Copy
            dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Execute SpViewVentasPendientes @IDCliente = " & txtCliente.txtID.ObtenerValor & ", @IDMoneda = '" & IDMoneda & "' , @FechaCotizacion = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' ", CadenaConexion)
        End If

        ListarVentas()

    End Sub

    Sub CargarVenta()

        If dtDetalleCobranzaVenta.Rows.Count = 0 And txtCliente.txtID.txt.Text = "" Then
            dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Execute SpViewVentasPendientes @IDCliente = 0, @IDMoneda = '" & IDMoneda & "' , @FechaCotizacion = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' ", CadenaConexion)
        End If
        If txtCliente.Seleccionado = True Then
            dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Execute SpViewVentasPendientes @IDCliente = " & txtCliente.txtID.ObtenerValor & ", @IDMoneda = '" & IDMoneda & "' , @FechaCotizacion = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' ", CadenaConexion)
        End If

        Dim frm As New frmCobranzaCreditoSeleccionarVentas
        frm.decimales = vdecimalOperacion
        frm.Text = "Seleccion de Comprobantes pendiente de Cobro"
        frm.Cliente = txtCliente.txtRazonSocial.Text
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtDetalleCobranzaVenta
        frm.ShowDialog(Me)
        dtDetalleCobranzaVenta = frm.dt

        ListarVentas()
    End Sub

    Sub CargarVenta(ByVal IDTransaccion As Integer)

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra las ventas asociadas!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        'Limpiar ventas seleccionadas
        dtDetalleCobranzaVenta.Rows.Clear()
        dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Select *, 'Sel'='True' From VVentaDetalleCobranzaCredito Where IDTransaccionAnticipo=" & IDTransaccion, CadenaConexion).Copy
        ListarVentas()

    End Sub

    Sub ListarVentas()

        dgw.Rows.Clear()

        Dim Total As Decimal = 0
        Dim TotalOper As Decimal = 0.0
        Dim DiferenciaCambio As Decimal = 0.0
        For Each oRow As DataRow In dtDetalleCobranzaVenta.Rows

            If oRow("Sel") = True Then

                Dim Registro(10) As String
                Registro(0) = oRow("Comprobante").ToString
                Registro(1) = oRow("Cliente").ToString
                Registro(2) = oRow("Fec").ToString
                Registro(3) = oRow("Moneda").ToString
                Registro(4) = CSistema.FormatoNumero(oRow("Cotizacion").ToString)
                Registro(5) = CSistema.FormatoMoneda(oRow("Total").ToString, vdecimalOperacion)
                Registro(6) = CSistema.FormatoMoneda(oRow("Cobrado").ToString, vdecimalOperacion)
                Registro(7) = CSistema.FormatoMoneda(oRow("Descontado").ToString, vdecimalOperacion)
                Registro(8) = CSistema.FormatoMoneda(oRow("Saldo").ToString, vdecimalOperacion)
                Registro(9) = CSistema.FormatoMoneda(oRow("Importe").ToString, vdecimalOperacion)
                Registro(10) = CSistema.FormatoMoneda(CSistema.FormatoMoneda(oRow("Importe").ToString, vdecimalOperacion) * oRow("Cotizacion").ToString, False)


                Total = Total + CSistema.FormatoMoneda((CSistema.FormatoMoneda(oRow("Importe").ToString, vdecimalOperacion) * oRow("Cotizacion").ToString), False)
                TotalOper = TotalOper + CSistema.FormatoMoneda(oRow("Importe").ToString, vdecimalOperacion)
                If vdecimalOperacion = True Then
                    If vNuevo = False Then
                        'DiferenciaCambio = DiferenciaCambio + CSistema.FormatoMonedaBaseDatos((CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, vdecimalOperacion) * CSistema.FormatoNumero(oRow("Cotizacion").ToString)) - (CSistema.FormatoMonedaBaseDatos(CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, vdecimalOperacion) * CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString), vdecimalOperacion)), False)
                    Else
                        'DiferenciaCambio = DiferenciaCambio + CSistema.FormatoMoneda((CSistema.FormatoNumero(oRow("Importe").ToString, vdecimalOperacion) * CSistema.FormatoNumero(oRow("Cotizacion").ToString, False)) - (CSistema.FormatoMoneda(CSistema.FormatoNumero(oRow("Importe").ToString, vdecimalOperacion) * CSistema.FormatoNumero(txtCotizacionCobranza.texto, False), False)), False)
                        'DiferenciaCambio = DiferenciaCambio + CSistema.FormatoMonedaBaseDatos((CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, vdecimalOperacion) * CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString, False)) - (CSistema.FormatoMonedaBaseDatos(CSistema.FormatoNumero(oRow("Importe").ToString, vdecimalOperacion) * CSistema.FormatoMonedaBaseDatos(txtCotizacionCobranza.texto, False), False)), False)
                        Dim valor1 As Double
                        Dim valor2 As Double
                        valor1 = CSistema.FormatoMoneda3Decimales(oRow("Importe").ToString, True) * CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString, False)
                        valor2 = CSistema.FormatoMoneda3Decimales(oRow("Importe").ToString, True) * CSistema.FormatoMonedaBaseDatos(txtCotizacionCobranza.Texto, False)
                        'valor2 = CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, vdecimalOperacion) * CSistema.FormatoMonedaBaseDatos(txtCotizacionCobranza.Texto, False)
                        valor2 = CSistema.FormatoMoneda3Decimales(oRow("Importe").ToString, True) * CSistema.FormatoMonedaBaseDatos(txtCotizacionCobranza.Texto, False)
                        DiferenciaCambio = DiferenciaCambio + (valor1 - valor2)
                    End If
                Else
                    DiferenciaCambio = 0
                End If
                dgw.Rows.Add(Registro)

            End If
        Next

        txtTotalVenta.SetValue(Total)
        txtTotalVentaOper.SetValue(TotalOper)
        If vNuevo = True Then
            txtDiferenciaCambio.SetValue(DiferenciaCambio * (-1))
        End If

        CalcularTotales()
        'If vNuevo = True Then
        '    If dgw.Rows.Count = 0 Then
        '        txtMoneda.SoloLectura = False
        '    Else
        '        txtMoneda.SoloLectura = True
        '    End If
        'End If


    End Sub

    Sub EliminarVenta()

        For Each item As DataGridViewRow In dgw.SelectedRows

            Dim Comprobante As String = item.Cells(0).Value

            For Each oRow As DataRow In dtDetalleCobranzaVenta.Select("Comprobante='" & Comprobante & "'")
                oRow("Sel") = False
            Next
            Exit For

        Next

        ListarVentas()

    End Sub

    Sub CalcularTotales()

        Dim TotalVentas As Decimal
        Dim TotalFormaPago As Decimal
        Dim SaldoTotal As Decimal
        Dim TotalVentasOper As Decimal
        Dim TotalFormaPagoOper As Decimal
        Dim SaldoTotalOper As Decimal

        TotalVentas = txtTotalVenta.ObtenerValor
        TotalVentasOper = CSistema.FormatoMoneda(txtTotalVentaOper.txt.Text, vdecimalOperacion)

        'Calcular
        SaldoTotal = TotalVentas - TotalFormaPago
        SaldoTotalOper = TotalVentasOper - TotalFormaPagoOper

        'txtSaldoTotal.SetValue(SaldoTotal)
        txtSaldoTotal.SetValue(SaldoTotalOper)
        'OcxFormaPago1.Saldo = SaldoTotal

    End Sub

    Sub Imprimir()

    End Sub

    Sub Buscar()

        Dim frm As New frmConsultaAnticipo
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog()
        CargarOperacion(frm.IDTransaccion)
    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select top 1 IsNull((Select top 1 IDTransaccion From VAnticipoAplicacion Where Numero=" & txtID.ObtenerValor & " and IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )", CadenaConexion)
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select top 1 * From VAnticipoAplicacion Where IDTransaccion=" & IDTransaccion, CadenaConexion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas técnicos."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)
        txtDiferenciaCambio.SetValue(oRow("DiferenciaCambio").ToString)
        txtCliente.txtReferencia.txt.Text = oRow("Referencia").ToString
        txtCliente.txtRazonSocial.txt.Text = oRow("Cliente").ToString
        txtCliente.txtRUC.txt.Text = oRow("RUC").ToString
        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("NroComprobante").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        txtCliente.SetValue(oRow("IDCliente").ToString)
        txtObservacion.txt.Text = oRow("Observacion").ToString
        cbxCobrador.cbx.Text = oRow("Cobrador").ToString
        txtPlanilla.SetValue(oRow("NroPlanilla").ToString)
        'txtMoneda.SetValue(oRow("Moneda").ToString, oRow("Cotizacion").ToString)
        txtTipoMoneda.txt.Text = oRow("Moneda").ToString
        txtCotizacionCobranza.txt.Text = oRow("Cotizacion").ToString
        vdecimalOperacion = CSistema.RetornarValorBoolean(CData.GetRow(" referencia = '" & oRow("Moneda").ToString & "'", "vMoneda")("Decimales").ToString)
        txtTotalVentaOper.Decimales = vdecimalOperacion


        'Visible Label
        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then

            'Visible Label
            lblRegistradoPor.Visible = True
            lblUsuarioAnulado.Visible = True
            lblUsuarioRegistro.Visible = True
            lblFechaRegistro.Visible = True
            lblAnulado.Visible = True
            lblAnulado.Visible = True
            lblFechaAnulado.Visible = True

            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            'lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            lblUsuarioAnulado.Text = oRow("UsuarioAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos el detalle de Venta
        CargarVenta(IDTransaccion)

        'Calcular Totales
        CalcularTotales()

        'Inicializamos el Asiento
        CAsiento.Limpiar()
        btnAnular.Enabled = Not lblAnulado.Visible
        btnAgregarVenta.Enabled = False
    End Sub

    Sub LimpiarControles()
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False
        'txtPlanilla.txt.Text = ""
    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles, btnModificar)

    End Sub

    Sub ModificarCobranza()
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub frmCobranzaCredito_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Public Sub EstablecerVariablesGlobales(Optional _vgIDUsuario As Integer = 0, Optional _vgIDSucursal As Integer = 0, Optional _vgIDDeposito As Integer = 0, Optional _vgIDTerminal As Integer = 0, Optional _vgIDPerfil As Integer = 0)

        If _vgIDUsuario <> 0 Then
            vgIDUsuario = _vgIDUsuario
        End If

        If _vgIDSucursal <> 0 Then
            vgIDSucursal = _vgIDSucursal
        End If

        If _vgIDDeposito <> 0 Then
            vgIDDeposito = _vgIDDeposito
        End If

        If _vgIDTerminal <> 0 Then
            vgIDTerminal = _vgIDTerminal
        End If

        If _vgIDPerfil <> 0 Then
            vgIDPerfil = _vgIDPerfil
        End If

    End Sub

    Private Sub frmCobranzaCredito_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        'Controles para obviar
        Dim Controles() As String = {"OcxFormaPago1"}
        If Controles.Contains(Me.ActiveControl.Name) Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmCobranzaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub btnAgregarVenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarVenta.Click
        'If CSistema.ExecuteScalar("select count(*) from Cotizacion where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False) & "' and IDMoneda = " & IDMoneda) = 0 And IDMoneda > 1 Then
        '    MessageBox.Show("No se ha fijado cotizacion para la moneda seleccionada.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    Cancelar()
        '    Me.Close()
        '    Exit Sub
        'End If
        CargarVenta()
    End Sub

    Private Sub lklEliminarVenta_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarVenta.LinkClicked
        EliminarVenta()
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue, CadenaConexion)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado

        'Cobrador
        cbxCobrador.cbx.Text = txtCliente.Registro("Cobrador").ToString

        'Ventas pendientes
        ObtenerVentasClientes()
        'Foco
        If vNuevo = True Then
            cbxCobrador.Focus()
        End If

    End Sub

    Private Sub txtCliente_ItemMalSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemMalSeleccionado

        If txtCliente.SoloLectura = True Then
            Exit Sub
        End If

    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        ModificarCobranza()
    End Sub


    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        CAsiento.IDSucursal = cbxSucursal.GetValue
        CAsiento.IDMoneda = 1
        CAsiento.InicializarAsiento()
        'cbxCobrador
        cbxCobrador.cbx.DataSource = Nothing

        If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxSucursal.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxCobrador.cbx, "Select ID, Nombres  From Cobrador Where IDSucursal=" & cbxSucursal.cbx.SelectedValue, CadenaConexion)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub txtMoneda_CambioMoneda()
        'OcxImpuesto1.SetIDMoneda(OcxCotizacion1.Registro("ID"))
        'MostrarDecimales(OcxCotizacion1.Registro("ID"))
        'txtMoneda.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
        'txtMoneda.Recargar()
        vdecimalOperacion = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & IDMoneda, "vMoneda")("Decimales").ToString)
        txtTotalVentaOper.Decimales = vdecimalOperacion
    End Sub


    Private Sub txtFecha_Validated(sender As System.Object, e As System.EventArgs) Handles txtFecha.Validated
        Dim Fecha As String = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
        'If Fecha.Length = 8 Then
        '    txtMoneda.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
        '    txtMoneda.Recargar()
        'End If
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmAplicacionVentasAnticipo_Activate()
        Me.Refresh()
    End Sub

End Class
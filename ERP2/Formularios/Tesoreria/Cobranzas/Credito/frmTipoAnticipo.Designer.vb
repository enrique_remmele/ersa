﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTipoAnticipo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxTipoAnticipo = New ERP.ocxCBX()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 88)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(364, 22)
        Me.StatusStrip1.TabIndex = 61
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(277, 52)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 60
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(66, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(137, 13)
        Me.Label1.TabIndex = 59
        Me.Label1.Text = "Selecionar Tipo de Anticipo"
        '
        'cbxTipoAnticipo
        '
        Me.cbxTipoAnticipo.CampoWhere = Nothing
        Me.cbxTipoAnticipo.CargarUnaSolaVez = False
        Me.cbxTipoAnticipo.DataDisplayMember = ""
        Me.cbxTipoAnticipo.DataFilter = Nothing
        Me.cbxTipoAnticipo.DataOrderBy = Nothing
        Me.cbxTipoAnticipo.DataSource = ""
        Me.cbxTipoAnticipo.DataValueMember = ""
        Me.cbxTipoAnticipo.FormABM = Nothing
        Me.cbxTipoAnticipo.Indicaciones = Nothing
        Me.cbxTipoAnticipo.Location = New System.Drawing.Point(51, 24)
        Me.cbxTipoAnticipo.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoAnticipo.Name = "cbxTipoAnticipo"
        Me.cbxTipoAnticipo.SeleccionObligatoria = False
        Me.cbxTipoAnticipo.Size = New System.Drawing.Size(301, 21)
        Me.cbxTipoAnticipo.SoloLectura = False
        Me.cbxTipoAnticipo.TabIndex = 58
        Me.cbxTipoAnticipo.Texto = ""
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(4, 27)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(31, 13)
        Me.lblObservacion.TabIndex = 57
        Me.lblObservacion.Text = "Tipo:"
        '
        'frmTipoAnticipo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(364, 110)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbxTipoAnticipo)
        Me.Controls.Add(Me.lblObservacion)
        Me.Name = "frmTipoAnticipo"
        Me.Text = "frmTipoAnticipo"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxTipoAnticipo As ERP.ocxCBX
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
End Class

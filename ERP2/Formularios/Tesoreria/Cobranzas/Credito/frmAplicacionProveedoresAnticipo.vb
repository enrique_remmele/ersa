﻿Imports ERP

Public Class frmAplicacionProveedoresAnticipo

    'CLASES
    Public CSistema As New CSistema
    Public CArchivoInicio As New CArchivoInicio
    Public CAsiento As New CAsientoAplicacionAnticipoProveedores
    Public CData As New CData
    Private vCotizacionRetencion As Integer

    Enum ENUMFormaPago
        ComprobanteGS_PagoGS = 0
        ComprobanteGS_PagoUS = 1
        ComprobanteUS_PagoUS = 2
        ComprobanteUS_PagoGS = 3
    End Enum

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private DecimalesValue As Boolean
    Public Property Decimales() As Boolean
        Get
            Return DecimalesValue
        End Get
        Set(ByVal value As Boolean)
            DecimalesValue = value
            'txtTotalFacturaProveedor.Decimales = value
        End Set
    End Property

    Private CotizacionDelDiaValue As Decimal
    Public Property CotizacionDelDia() As Decimal
        Get
            Return CotizacionDelDiaValue
        End Get
        Set(ByVal value As Decimal)
            CotizacionDelDiaValue = value
            txtCotizacion.SetValue(value)
        End Set
    End Property

    Public Property CargarAlIniciar As Boolean
    Public Property CerrarAlGuardar As Boolean
    ' Public Property IDMonedaPago As Integer


    'VARIABLES
    Dim Where As String
    Dim dt As New DataTable
    Dim dtAnticipoProveedores As New DataTable
    Dim dtEgresos As New DataTable
    Dim dtOrdenPago As New DataTable
    Public vControles() As Control

    Dim TotalEgreso As Decimal
    Dim TotalAnticipo As Decimal
    Dim SaldoAnticipo As Decimal
    Dim SaldoActual As Decimal
    Dim SaldoTotal As Decimal
    Public vAsiento As Boolean = False
    Public vNuevo As Boolean = False

    Dim vImporte As Decimal
    Dim vTotalop As Decimal
    Dim Monto As Double
    Dim MontoLetras As String
    Private ReadOnly dtegreso As Object
    Dim IdTransaccionOrdenPago As Integer
    Dim IdTransaccionFactura As Integer
    Public Property IDMonedaPago As Integer

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion("frmAplicacionProveedoresAnticipo", "APLICACION DE ANTICIPO A PROVEEDORES", "APA")

        vNuevo = False
        vAsiento = False
        CerrarAlGuardar = False
        CargarAlIniciar = False
        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()

        txtProveedor.Conectar()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)
        Dim k As Keys = vgKeyProcesar
        btnGuardar.Text = "&Guardar (" & k.ToString & ")"

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        If CBool(vgConfiguraciones("TesoreriaBloquerFecha").ToString) = True Then
            txtFecha.Enabled = False
        Else
            txtFecha.Enabled = True
        End If

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, txtProveedor)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtCotizacion)

        'Egresos
        CSistema.CargaControl(vControles, btnAgregarFacturasProveedor)
        CSistema.CargaControl(vControles, btnAgregarOrdenPago)

        If vNuevo = True Then
            CSistema.CargaControl(vControles, btnAgregarFacturasProveedor)
            CSistema.CargaControl(vControles, btnAgregarOrdenPago)
        End If

        CSistema.CargaControl(vControles, chkAplicarRetencion)
        CSistema.CargaControl(vControles, txtTotalRetencion)

        'Facturas de Proveedores
        CSistema.CargaControl(vControles, dgw)
        'Ordenes de Pago
        CSistema.CargaControl(vControles, dgwOrdenPago)

        'CARGAR CONTROLES
        'Cotizacion
        'CSistema.SqlToComboBox(txtCotizacion, "Select cotizacion from VCotizacion where idmoneda=" & cbxMoneda.cbx)

        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Ultimo Registro
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From AplicacionAnticipoProveedores Where IDSucursal=" & cbxSucursal.GetValue & "),1) "), Integer)
        txtComprobante.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From AplicacionAnticipoProveedores Where IDSucursal=" & cbxSucursal.GetValue & "),1) "), Integer)

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        dgw.Rows.Clear()
        dgwOrdenPago.Rows.Clear()

        txtCantidadComprobante.txt.Clear()
        txtCantidadOrdenPago.txt.Clear()
        txtTotalComprobante.txt.Clear()
        txtTotalOrdenPago.txt.Clear()
        txtTotalRetencion.txt.Clear()

        dtEgresos.Rows.Clear()
        dtOrdenPago.Rows.Clear()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From AplicacionAnticipoProveedores Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Se busca las Aplicaciones de Anticipo a Proveedores por cabecera
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
        dt = CSistema.ExecuteToDataTable("Select * From VAplicacionAnticipoProveedores Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas técnicos."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim vIdTransaccionOrdenPago As Integer = 0
        Dim vIdTransaccionProveedores As Integer = 0

        Dim oRow As DataRow = dt.Rows(0)

        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("NroComprobante").ToString
        cbxMoneda.SelectedValue(oRow("IDMoneda").ToString)
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        txtObservacion.txt.Text = oRow("Observacion").ToString

        'Proveedor
        If oRow("IDProveedor") > 0 Then
            txtProveedor.SetValue(oRow("IDProveedor"))
        Else
            txtProveedor.SetValueString(0, "", "")
        End If

        vImporte = oRow("TotalEgreso")

        If CBool(oRow("Anulado").ToString) = True Then
            'Visible Label
            lblRegistradoPor.Visible = True
            lblUsuarioAnulado.Visible = True
            lblUsuarioRegistro.Visible = True
            lblFechaRegistro.Visible = True
            lblAnulado.Visible = True
            lblFechaAnulado.Visible = True

            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Visible Label
        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("Fecha").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        'Cargamos el detalle siempre y cuando la Aplicacion no este anulada - Anticipo
        If CBool(oRow("Anulado").ToString) = False Then
            vIdTransaccionOrdenPago = CSistema.ExecuteScalar("Select distinct IdTransaccionOrdenPago From VAplicacionAnticipoProveedoresDetalle Where IDTransaccion=" & IDTransaccion)

            'Se Carga el detalle de Egresos
            CargarEgreso(vIdTransaccionOrdenPago)
            'Se Carga el detalle de la Orden de Pago
            CargarOrdenPago(vIdTransaccionOrdenPago)
        End If

        'Calcular Totales
        CalcularTotales()

        'Inicializamos el Asiento
        CAsiento.Limpiar()

        Dim dtAsiento As DataTable = CSistema.ExecuteToDataTable("Select 'Credito'=Sum(Credito) From VDetalleAsiento Where IDTransaccion=" & IDTransaccion)

        If dtAsiento.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow2 As DataRow = dtAsiento.Rows(0)
        Monto = CSistema.FormatoNumero(oRow2("Credito").ToString)

        'SC:Probar para dejar cargada la operación de Asiento
        CAsiento.CargarOperacion(IDTransaccion)

    End Sub

    Sub LimpiarControles()

        dgw.Rows.Clear()
        dgwOrdenPago.Rows.Clear()

        txtSaldoTotal.txt.Text = ""
        txtTotalComprobante.txt.Text = ""
        txtTotalOrdenPago.txt.Text = ""

        txtTotalRetencion.SetValue(0)

        txtProveedor.Clear()
        txtFecha.txt.Clear()
        txtObservacion.Clear()

        'Desaparecer Label
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

    End Sub

    Sub Nuevo()
        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)
        'chkAplicarRetencion.Valor = True
        chkAplicarRetencion.Valor = False

        'Limpiar detalle
        dtEgresos.Rows.Clear()
        dtOrdenPago.Rows.Clear()

        'Funciones
        LimpiarControles()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""

        IDTransaccion = 0
        CAsiento.Limpiar()
        vNuevo = True
        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Cotizaciones
        '31 corresponde al IDOperación de la ORDEN DE PAGO, ya que el 261 de la Aplicacion Anticipo a Proveedores no se encuentra dentro de la 
        'configuración preestablecida
        CotizacionDelDia = CSistema.CotizacionDelDia(IDMonedaPago, 31)

        'Cabecera
        txtFecha.Hoy()
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()
        txtProveedor.Clear()
        txtSaldoTotal.txt.Clear()
        txtTotalOrdenPago.txt.Clear()
        txtCantidadComprobante.txt.Clear()
        txtCantidadOrdenPago.txt.Clear()

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From AplicacionAnticipoProveedores Where IDSucursal=" & cbxSucursal.GetValue & "),1) "), Integer)
        txtComprobante.txt.Text = CSistema.ObtenerProximoNroComprobante(cbxTipoComprobante.cbx.SelectedValue, "AplicacionAnticipoProveedores", "NroComprobante", cbxSucursal.GetValue)

        'Habilitar Botones de Carga
        btnAgregarFacturasProveedor.Enabled = True
        btnAgregarOrdenPago.Enabled = True

        txtComprobante.txt.Text = txtID.ObtenerValor
        txtProveedor.Focus()

        'Limpiar detalle 'Clases
        CAsiento.InicializarAsiento()

    End Sub

    Sub Cancelar()

        'Deshabilitar Botones
        btnAgregarFacturasProveedor.Enabled = False
        btnAgregarOrdenPago.Enabled = False

        'CargarInformacion()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        vNuevo = False
        txtID.txt.Focus()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            'Ciudad
            If cbxCiudad.Validar("Seleccione correctamente la ciudad!", ctrError, btnGuardar, tsslEstado) = False Then
                Exit Function
            End If

            'Tipo Comprobante
            If cbxTipoComprobante.Validar("Seleccione correctamente el tipo de comprobante!", ctrError, btnGuardar, tsslEstado) = False Then
                Exit Function
            End If

            'Comprobante
            If txtComprobante.txt.Text.Trim.Length = 0 Then
                txtComprobante.SetValue(txtID.ObtenerValor.ToString)
            End If

            'Sucursal
            If cbxSucursal.Validar("Seleccione correctamente la sucursal!", ctrError, btnGuardar, tsslEstado) = False Then
                Exit Function
            End If

            'Observacion
            If txtObservacion.txt.Text = "" Then
                Dim mensaje As String = "Se sugiere generar una breve Observacion para el Movimiento!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If

            'Detalle
            If dgw.Rows.Count = 0 Then
                Dim mensaje As String = "No se ha seleccionado ningun comprobante del Proveedor para el pago!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If

            If dgwOrdenPago.Rows.Count = 0 Then
                Dim mensaje As String = "No se ha seleccionado ningun Anticipo!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If

            'Totales
            If txtSaldoTotal.ObtenerValor <> 0 And Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
                Dim mensaje As String = "El importe no corresponde al total de comprobantes!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If

        End If

        If txtTotalComprobante.ObtenerValor < 0 Then
            Dim mensaje As String = "El importe de Factura Proveedor no es válido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Asiento
        If CAsiento.ObtenerSaldo <> 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        If CAsiento.ObtenerTotal = 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'No calcular el saldo
        Select Case FormaPago()
            Case ENUMFormaPago.ComprobanteUS_PagoGS
        End Select

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If
        Else
            If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
                'Validar el Asiento
                If CAsiento.ObtenerSaldo <> 0 Then
                    CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                    Exit Function
                End If

                If CAsiento.ObtenerTotal = 0 Then
                    CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                    Exit Function
                End If
            End If

            'Total de Pago
            If txtTotalOrdenPago.ObtenerValor = 0 Then
                Dim mensaje As String = "Debe seleccionar un monto para orden de Pago!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If

            'Total de Factura
            If txtTotalComprobante.ObtenerValor = 0 Then
                Dim mensaje As String = "Debe seleccionar un monto para Facturas de Proveedor!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If
        End If

        Return True

    End Function

    Private Function FormaPago() As ENUMFormaPago

        FormaPago = ENUMFormaPago.ComprobanteGS_PagoGS

        Dim vIDMonedaFormaPago As Integer = IDMonedaPago
        Dim vIDMonedaComprobantes As Integer = cbxMoneda.GetValue

        'ComprobanteGS_PagoGS
        If vIDMonedaComprobantes = 1 And vIDMonedaFormaPago = 1 Then
            Return ENUMFormaPago.ComprobanteGS_PagoGS
        End If

        'ComprobanteGS_PagoUS
        If vIDMonedaComprobantes = 1 And vIDMonedaFormaPago <> 1 Then
            Return ENUMFormaPago.ComprobanteGS_PagoUS
        End If

        'ComprobanteUS_PagoUS
        If vIDMonedaComprobantes <> 1 And vIDMonedaFormaPago <> 1 Then
            Return ENUMFormaPago.ComprobanteUS_PagoUS
        End If

        'ComprobanteUS_PagoGS
        If vIDMonedaComprobantes <> 1 And vIDMonedaFormaPago = 1 Then
            Return ENUMFormaPago.ComprobanteUS_PagoGS
        End If

    End Function

    Sub EliminaAnticipo()
        For Each oRow As DataRow In dtEgresos.Select("Seleccionado = 'True' ")
            IdTransaccionFactura = oRow("IDTransaccion").ToString
            IdTransaccionOrdenPago = dgwOrdenPago.SelectedRows(0).Cells(0).Value

            Dim param(-1) As SqlClient.SqlParameter
            Dim IndiceOperacion As Integer = 0

            'Entrada
            CSistema.SetSQLParameter(param, "@IDTransaccionOP", IdTransaccionOrdenPago, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccionEgreso", IdTransaccionFactura, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cuota", 1, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "SOLICITAR", ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpOrdenPagoEliminarAplicacion", False, False, MensajeRetorno,, True) = False Then
                MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Next
    End Sub

    Sub AplicaAnticipo()

        'Extraer Orden de Pago de la Grid de dgwOrdenPago
        IdTransaccionOrdenPago = dgwOrdenPago.SelectedRows(0).Cells(0).Value

        If IdTransaccionOrdenPago > 0 Then

            'Cargamos el Detalle de Egresos
            If InsertarDetalleEgresos(IdTransaccionOrdenPago) = False Then
                Dim mensaje As String = "Error al intentar Insertar en OrdenPagoEgreso!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Sub
            End If
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer = 0

        'Entrada
        CSistema.SetSQLParameter(param, "@IDTransaccion", IdTransaccionOrdenPago, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", ERP.CSistema.NUMOperacionesABM.UPD.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpOrdenPagoAplicarProcesar", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

    End Sub

    Function InsertarDetalleEgresos(IdTransaccionOrdenPago As Integer) As Boolean

        InsertarDetalleEgresos = True
        For Each oRow As DataRow In dtEgresos.Select("Seleccionado = 'True' ")

            Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)
            Dim sql As String = "Insert Into OrdenPagoEgreso(IDTransaccionOrdenPago, IDTransaccionEgreso,Cuota, Importe, Saldo) Values( " & IdTransaccionOrdenPago & "," & oRow("IDTransaccion").ToString & "," & oRow("Cuota").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString, Decimales) & ")"

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If
        Next

    End Function

    Function ValidarSaldo() As Boolean

        ValidarSaldo = False

        'Saldo
        If txtSaldoTotal.ObtenerValor < 0 Then
            Dim mensaje As String = "La cantidad selecionada no puede ser mayor al saldo de la orden de pago!"
            CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
            Exit Function
        End If
        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then
            If vAsiento = False Then
                Dim mensaje As String = "ATENCION: Se debe Generar el Asiento en forma Manual!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Sub
            End If
        End If

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer
        Dim Decimales As Boolean = False

        'Rutina que aplica los antipos a las facturas del proveedor
        AplicaAnticipo()

        'Generar la cabecera del asiento
        'Comentado por necesidad de realizarlo en forma manual
        'GenerarAsiento()

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If
        CSistema.SetSQLParameter(param, "@Numero", CInt(txtID.ObtenerValor), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)
        'Moneda
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.GetValue, ParameterDirection.Input)

        'Proveedor
        If Not txtProveedor.Registro Is Nothing Then
            CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.Registro("ID").ToString, ParameterDirection.Input)
        End If

        'Totales
        CSistema.SetSQLParameter(param, "@TotalEgreso", CSistema.FormatoMonedaBaseDatos(txtTotalComprobante.ObtenerValor, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalOrdenPago", CSistema.FormatoMonedaBaseDatos(txtTotalOrdenPago.ObtenerValor, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAplicacionAnticipoAProveedores", False, False, MensajeRetorno, IDTransaccion) = False Then

            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From AplicacionAnticipoProveedores Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpAplicacionAnticipoAProveedores", False, False, MensajeRetorno, IDTransaccion)
            End If
            Exit Sub

        End If

        'Dim Procesar As Boolean = True
        IdTransaccionOrdenPago = dgwOrdenPago.SelectedRows(0).Cells(0).Value

        'Inserta Detalle de la Aplicacion
        For Each oRow As DataRow In dtEgresos.Select("Seleccionado='True'")
            Try
                ReDim param(-1)
                CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDTransaccionOrdenPago", IdTransaccionOrdenPago, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDTransaccionProveedores", oRow("IDTransaccion").ToString, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)
                'Totales
                CSistema.SetSQLParameter(param, "@TotalEgreso", CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString), ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@TotalOrdenPago", CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString), ParameterDirection.Input)
                'Operacion
                CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
                IndiceOperacion = param.GetLength(0) - 1

                'Transaccion
                CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

                'Informacion de Salida
                CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
                CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
                CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

                'Insertar Registro
                If CSistema.ExecuteStoreProcedure(param, "SpAplicacionAnticipoAProveedoresDetalle", False, False, MensajeRetorno, IDTransaccion) = False Then

                    tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                    ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

                    'Eliminar el Registro si es que se registro
                    If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From AplicacionAnticipoProveedoresDetalle Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                        param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                        CSistema.ExecuteStoreProcedure(param, "SpAplicacionAnticipoAProveedoresDetalle", False, False, MensajeRetorno, IDTransaccion)
                    End If
                    Exit Sub

                End If
            Catch ex As Exception
            End Try
        Next

        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
            CargarOperacion(IDTransaccion)
            txtID.SoloLectura = False
            Exit Sub
        End If

        'Procesar la OP
        Try
            ReDim param(-1)
            CSistema.SetSQLParameter(param, "@IDTransaccion", IdTransaccionOrdenPago, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)
            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "True", ParameterDirection.Output)

            'Aplicar la Cobranza de los Anticipos a las Facturas
            If CSistema.ExecuteStoreProcedure(param, "SpOrdenPagoProcesar", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            End If

            'Guardar Asiento de la transacccion
            CAsiento.IDTransaccion = IDTransaccion
            CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        Catch ex As Exception
        End Try

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

        vNuevo = True
        If CerrarAlGuardar = True Then
            Me.Close()
        End If

        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False
        txtID.txt.Focus()

    End Sub

    Sub CargarEgreso(ByVal IDTransaccion As Integer)
        'Genera la carga cuando el egreso esta relacionado a una Aplicación de Anticipo
        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra los Egresos asociados!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        dtEgresos.Rows.Clear()
        dtEgresos = CSistema.ExecuteToDataTable("Select * From VOrdenPagoEgreso Where IDTransaccionOrdenPago=" & IDTransaccion).Copy
        ListarEgresos()

    End Sub

    Sub ListarEgresos()

        dgw.Rows.Clear()

        Dim Total As Decimal = 0
        Dim Saldo As Decimal = 0
        Dim TotalImporte As Decimal = 0
        Dim TotalRetencion As Decimal = 0
        Dim Retentor As Boolean = False

        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)

        If dtEgresos.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each oRow As DataRow In dtEgresos.Rows
            If CSistema.RetornarValorBoolean(oRow("Seleccionado").ToString) = True Then

                Dim oRow1(11) As String

                oRow1(0) = oRow("IDTransaccion").ToString
                oRow1(1) = oRow("NroComprobante").ToString
                oRow1(2) = oRow("Fecha").ToString
                oRow1(3) = oRow("Proveedor").ToString
                oRow1(4) = oRow("Moneda").ToString
                oRow1(5) = oRow("Cotizacion").ToString
                oRow1(6) = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
                oRow1(7) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)
                oRow1(8) = CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales)
                oRow1(9) = CSistema.FormatoMoneda(oRow("TotalImpuesto").ToString, Decimales)
                oRow1(10) = oRow("Cuota").ToString
                oRow1(11) = (oRow("RetencionIVA").ToString)

                Total = Total + CDec(oRow("Importe").ToString)
                Saldo = Saldo + CDec(oRow("Saldo").ToString)
                TotalRetencion = TotalRetencion + CDec(oRow("RetencionIVA").ToString)
                If TotalRetencion > 0 Then
                    chkAplicarRetencion.Valor = True
                End If

                dgw.Rows.Add(oRow1)

                Retentor = CBool(oRow("Retentor").ToString)

            End If
        Next

        If chkAplicarRetencion.Valor = True Then
            TotalImporte = Total - TotalRetencion
        Else
            TotalImporte = Total
        End If

        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)

        txtTotalComprobante.SetValue(Total)
        txtSaldoTotal.SetValue(Total)
        txtTotalRetencion.SetValue(CSistema.FormatoMoneda(TotalRetencion, Decimales))
        txtSaldoTotal.SetValue(CSistema.FormatoMoneda(TotalImporte, Decimales))
        txtCantidadComprobante.SetValue(dgw.Rows.Count)
        CalcularTotales()

        If vNuevo = True Then
            If dgw.Rows.Count > 0 Then
                txtProveedor.SoloLectura = True
                cbxMoneda.SoloLectura = True
            Else
                txtProveedor.SoloLectura = False
                cbxMoneda.SoloLectura = False
            End If

        End If

    End Sub

    Sub CargarOrdenPago(ByVal IDTransaccion As Integer)

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra las Ordenes asociadas!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        dtOrdenPago.Rows.Clear()
        dtOrdenPago = CSistema.ExecuteToDataTable("Select * From VOrdenPagoSaldo Where IDTransaccion=" & IDTransaccion).Copy
        ListarOrdenPago()

    End Sub

    Private Sub dgwOrdenpago_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgwOrdenPago.DoubleClick
        SeleccionarRegistro()

    End Sub

    Sub SeleccionarRegistro()
        'Validar cantidad de registros seleccionados
        If dgwOrdenPago.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOrdenPago, Mensaje)
            ctrError.SetIconAlignment(dgwOrdenPago, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOrdenPago.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOrdenPago, Mensaje)
            ctrError.SetIconAlignment(dgwOrdenPago, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IdTransaccionOrdenPago = dgwOrdenPago.SelectedRows(0).Cells("colIDTransaccionOP").Value

        If IdTransaccionOrdenPago > 0 Then
            'txtSaldoTotal
            'Llama a rutina de totales
        End If

    End Sub

    Sub EliminarEgreso()

        If vNuevo = False Then
            Exit Sub
        End If

        For Each item As DataGridViewRow In dgw.SelectedRows

            For Each oRow As DataRow In dtEgresos.Select(" IDTransaccion = " & item.Cells(0).Value & " ")
                oRow("Seleccionado") = False
                Exit For
            Next

        Next

        ListarEgresos()

    End Sub

    Sub EliminarOrdenPago()

        If vNuevo = False Then
            Exit Sub
        End If

        For Each item As DataGridViewRow In dgwOrdenPago.SelectedRows

            For Each oRow As DataRow In dtOrdenPago.Select(" IDTransaccion = " & item.Cells(0).Value & " ")
                oRow("Seleccionado") = False
                Exit For
            Next

        Next

        ListarOrdenPago()

    End Sub
    Sub BuscarOP()

        Dim where As String = ""
        Dim valorwhere As String = ""

        'Filtra por Proveedor
        If txtProveedor.Registro("ID") > 0 Then
            where = " IDProveedor = " & txtProveedor.Registro("ID")
        End If

        'Filtra por Sucursal
        If cbxSucursal.GetValue > 0 Then
            where = where & " AND IDSucursal = " & cbxSucursal.GetValue
        End If

        'Filtra por Moneda
        'If cbxMoneda.GetValue > 0 Then
        '    where = where & " AND IDMoneda = " & cbxMoneda.GetValue
        'End If

        'Filtra por Tipo de Comprobante Orden de Pago = 4
        valorwhere = 4
        where = where & " AND IDTipoComprobante = " & valorwhere

        'Filtra por Anulado NO = 0
        valorwhere = 0
        where = where & " AND Anulado = " & valorwhere

        'Filtra por Cancelado NO = 0
        valorwhere = 0
        where = where & " AND EstadoOP = " & valorwhere

        'Filtra por AnticipoProveedor NO = 0
        valorwhere = 1
        where = where & " AND AnticipoProveedor = " & valorwhere

        'Filtra por AnticipoProveedor NO = 0
        valorwhere = 0
        where = where & " AND Saldo > " & valorwhere

        'Limpiar estructura de datos
        dtOrdenPago.Rows.Clear()
        dtOrdenPago = CSistema.ExecuteToDataTable("Select IDTransaccion, Seleccionado, Numero, NroComprobante, Fecha, Proveedor, Total, Saldo, Importe, Observacion, Cancelar,Cotizacion, Moneda,IDMoneda From VOrdenPagoSaldoSelect  Where " & where).Copy

        Dim frm As New frmOrdenPagoSeleccionar
        frm.Text = "Selección de Anticipo a Aproveedores"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtOrdenPago

        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccion de Anticipo a Proveedores", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)
        dtOrdenPago = frm.dt
        ListarOrdenPago()

    End Sub

    Sub ListarOrdenPago()

        ctrError.Clear()
        dgwOrdenPago.Rows.Clear()

        Dim TotalOrdenPago As Decimal = 0
        Dim SaldoOrdenPago As Decimal = 0

        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)

        If dtOrdenPago.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each oRow1 As DataRow In dtOrdenPago.Rows
            If CSistema.RetornarValorBoolean(oRow1("Seleccionado").ToString) = True Then

                Dim Registro(10) As String
                Registro(0) = oRow1("IDTransaccion").ToString
                Registro(1) = oRow1("NroComprobante").ToString
                Registro(2) = oRow1("Fecha").ToString
                Registro(3) = oRow1("Proveedor").ToString
                Registro(4) = oRow1("Moneda").ToString
                Registro(5) = CSistema.FormatoMoneda(oRow1("Total").ToString, Decimales)
                Registro(6) = CSistema.FormatoMoneda(oRow1("Saldo").ToString, Decimales)
                Registro(7) = CSistema.FormatoMoneda(oRow1("Importe").ToString, Decimales)
                Registro(8) = oRow1("Observacion").ToString
                Registro(9) = CSistema.FormatoMoneda(oRow1("Cotizacion").ToString, Decimales)
                Registro(10) = oRow1("IDMoneda").ToString
                'Sumar el total del saldo
                TotalOrdenPago = TotalOrdenPago + CDec(oRow1("Importe").ToString)

                dgwOrdenPago.Rows.Add(Registro)
            End If
        Next

        txtTotalOrdenPago.SetValue(TotalOrdenPago)
        txtCantidadOrdenPago.txt.Text = dgwOrdenPago.Rows.Count

        If vNuevo = True Then
            If dgwOrdenPago.Rows.Count > 0 Then
                txtProveedor.SoloLectura = True
                cbxMoneda.SoloLectura = True
            Else
                txtProveedor.SoloLectura = False
                cbxMoneda.SoloLectura = False
            End If
        End If

        'Llama a ruitna de Totales
        CalcularTotales()

    End Sub

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        'IdTransaccionFactura = dgw.SelectedRows(0).Cells(0).Value
        'IdTransaccionOrdenPago = dgwOrdenPago.SelectedRows(0).Cells(0).Value

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim frm As New frmMotivoAnulacionOP
        frm.Text = " Motivo Anulacion Aplicacion de Anticipo "
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.ShowDialog(Me)

        EliminaAnticipo()

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", CInt(txtID.ObtenerValor), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.GetValue, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)
        'Moneda
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.GetValue, ParameterDirection.Input)

        'Proveedor
        If Not txtProveedor.Registro Is Nothing Then
            CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.Registro("ID").ToString, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMotivoAnulacion", frm.ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAplicacionAnticipoAProveedores", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)
            Exit Sub
        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False

    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""
        'Generado = True

        Dim Comprobante As String = cbxTipoComprobante.cbx.Text & ": " & txtComprobante.txt.Text

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante
            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VAplicacionAnticipoProveedores Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)

        Else

            'Validar
            If cbxCiudad.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la ciudad de operacion!"
                ctrError.SetError(cbxCiudad, mensaje)
                ctrError.SetIconAlignment(cbxCiudad, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante


            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            CAsiento.ListarDetalle(frm.dgv)
            frm.CalcularTotales()

            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
            CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            CAsiento.NroCaja = frm.CAsiento.NroCaja
            CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                CAsiento.dtAsiento.Clear()
                CAsiento.dtDetalleAsiento.Clear()
                VisualizarAsiento()
            End If

        End If

    End Sub

    Sub GenerarAsiento()

        'Establecemos la moneda y cotización de la operacion establecida por la Orden de Pago
        Dim idMonedaOperacion As Integer
        Dim cotizacionOperacion As Decimal

        'idMonedaOperacion = dgwOrdenPago.Rows(0).Cells.Item(10).Value
        'cotizacionOperacion = dgwOrdenPago.Rows(0).Cells.Item(9).Value
        idMonedaOperacion = cbxMoneda.GetValue
        'cotizacionOperacion = CDec(txtCotizacion.txt.Text)
        cotizacionOperacion = dgwOrdenPago.Rows(0).Cells.Item(9).Value
        'EstablecerCabecera
        Dim Total As Decimal

        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = cbxCiudad.GetValue
        oRow("IDSucursal") = cbxSucursal.GetValue
        oRow("Fecha") = txtFecha.GetValue
        oRow("IDMoneda") = idMonedaOperacion
        oRow("Cotizacion") = cotizacionOperacion
        'oRow("IDTipoComprobante") = cbxTipoComprobante.GetValue
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = txtComprobante.txt.Text
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text.Trim
        oRow("Detalle") = txtObservacion.txt.Text
        'oRow("Total") = TotalEgreso

        'Si la moneda es diferente al local, pasar convertido 
        Total = ObtenerImporteComprobantes(False, True)

        oRow("Total") = Total

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)
        CAsiento.Total = Total

        CAsiento.IDMoneda = idMonedaOperacion

        If CAsiento.Generado = True Then
            Exit Sub
        End If

        'Se envia estructura de datos del Egreso para generar el detalle del asiento
        GenerarDetalleAsiento(dtEgresos)

        'Marca booleana que señala el asiento manual establecido
        vAsiento = True

    End Sub

    Sub GenerarDetalleAsiento(dtEgresos As DataTable)

        'If Bloquear = True Then
        '    Exit Sub
        'End If

        'If Generado = True Then
        '    Exit Sub
        'Else
        '    Generado = True
        'End If

        Dim IDMoneda As Integer
        Dim IDSucursal As Integer
        Dim Total As Decimal

        'Establecer la moneda
        For Each oRow As DataRow In CAsiento.dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
            IDSucursal = oRow("IDSucursal").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0
        'Dim dtProveedores As DataTable
        Dim codigo As String = ""
        Dim codigonew As Boolean = False
        Dim Debito As Integer = 0

        Dim NewRow As DataRow = CAsiento.dtDetalleAsiento.NewRow

        For Each oRow As DataRow In dtEgresos.Select("Seleccionado = 'True' ")
            Try
                'Si la moneda es diferente al local, pasar convertido 
                Total = ObtenerImporteComprobantes(False, True)

                codigo = oRow("DACodigo").ToString
                'InsertarDetalle la primera vez que recorre dtEgresos
                If codigonew = False And CAsiento.dtDetalleAsiento.Rows.Count < 2 Then
                    NewRow("IDTransaccion") = 0
                    NewRow("ID") = CAsiento.dtDetalleAsiento.Rows.Count
                    NewRow("IDCuentaContable") = oRow("DAIDCuentaContable").ToString
                    NewRow("Codigo") = oRow("DACodigo").ToString
                    NewRow("Credito") = oRow("DADebito").ToString
                    NewRow("Debito") = Total 'oRow("Importe").ToString
                    NewRow("Importe") = Total 'oRow("Importe").ToString
                    NewRow("Descripcion") = oRow("DADescripcion").ToString
                    NewRow("Observacion") = oRow("DAObservacion").ToString
                    NewRow("Alias") = oRow("DAAlias").ToString
                    codigonew = True
                    CAsiento.dtDetalleAsiento.Rows.Add(NewRow)
                Else
                    'Si ya existe la misma cuentacontable acumula
                    If NewRow("Codigo") = codigo Then
                        Debito = CDec(NewRow("Debito"))
                        'Debito = Debito + CDec(oRow("Importe").ToString)
                        Debito = Debito + Total
                        NewRow("Debito") = Debito
                        codigonew = True
                    Else
                        'Sino es la misma cuentacontable, resguarda el registro nuevo
                        NewRow("IDTransaccion") = 0
                        NewRow("ID") = CAsiento.dtDetalleAsiento.Rows.Count
                        NewRow("IDCuentaContable") = oRow("DAIDCuentaContable").ToString
                        NewRow("Codigo") = oRow("DACodigo").ToString
                        NewRow("Credito") = oRow("Importe").ToString
                        NewRow("Debito") = oRow("DADebito").ToString
                        NewRow("Importe") = Total 'oRow("Importe").ToString
                        NewRow("Descripcion") = oRow("DADescripcion").ToString
                        NewRow("Observacion") = oRow("DAObservacion").ToString
                        NewRow("Alias") = oRow("DAAlias").ToString
                        codigonew = True
                        CAsiento.dtDetalleAsiento.Rows.Add(NewRow)

                    End If
                End If
            Catch ex As Exception

            End Try
        Next
        'Generado = True
    End Sub

    Private Function ObtenerImporteComprobantes(Optional RestarRetencion As Boolean = True, Optional EnImporteLocal As Boolean = False) As Decimal

        ObtenerImporteComprobantes = 0

        If dtEgresos Is Nothing Then
            Exit Function
        End If

        If dtEgresos.Rows.Count = 0 Then
            Exit Function
        End If

        Dim TotalComprobante As Decimal = 0
        Dim TotalRetencion As Decimal = 0
        Dim Importe As Decimal = 0
        Dim Retencion As Decimal = 0

        For Each oRow As DataRow In dtEgresos.Select("Seleccionado='True'")

            Select Case FormaPago()

                Case ENUMFormaPago.ComprobanteGS_PagoGS
                    TotalComprobante = TotalComprobante + oRow("Importe")
                    TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    TotalComprobante = TotalComprobante + oRow("Importe")
                    TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                Case ENUMFormaPago.ComprobanteUS_PagoUS
                    If EnImporteLocal Then
                        TotalComprobante = TotalComprobante + (oRow("Importe") * oRow("Cotizacion"))
                        TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * oRow("Cotizacion"))
                    Else
                        TotalComprobante = TotalComprobante + oRow("Importe")
                        TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                    End If
                Case ENUMFormaPago.ComprobanteUS_PagoGS
                    If EnImporteLocal Then
                        TotalComprobante = TotalComprobante + (oRow("Importe") * oRow("Cotizacion"))
                        TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * oRow("Cotizacion"))
                    Else
                        TotalComprobante = TotalComprobante + oRow("Importe")
                        TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                    End If

                Case Else
                    TotalComprobante = TotalComprobante + oRow("Importe")
                    TotalRetencion = TotalRetencion + oRow("RetencionIVA")
            End Select

        Next

        'Sacamos la retencion
        If RestarRetencion = True Then
            If chkAplicarRetencion.Valor Then
                TotalComprobante = TotalComprobante - CSistema.FormatoNumero(TotalRetencion, False)
            End If
        End If

        Return TotalComprobante

    End Function

    Sub AnalizarSiAplicaRetencion()
        vCotizacionRetencion = CType(CSistema.ExecuteScalar("Select IsNull(Max(cotizacion), 1) From Cotizacion Where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' and IDMoneda=" & cbxMoneda.GetValue), Integer)
        'Validamos la retencion
        Dim MontoMinimoRetencion As Decimal = 0

        If IsNumeric(vgConfiguraciones("CompraImporteMinimoRetencion").ToString) = False Then
            MontoMinimoRetencion = 0
        Else
            MontoMinimoRetencion = vgConfiguraciones("CompraImporteMinimoRetencion")
        End If

        '701560
        Dim TotalComprobanteConRetencion As Decimal = CSistema.dtSumColumn(dtEgresos, "Total", "Retener='True' And Seleccionado='True'")

        'Si la moneda es distinta a la local, tenemos que convertir a local
        If cbxMoneda.GetValue <> 1 Then
            TotalComprobanteConRetencion = TotalComprobanteConRetencion * txtCotizacion.ObtenerValor
        End If

        'Ver cotizacion del documento
        If (TotalComprobanteConRetencion * vCotizacionRetencion) < MontoMinimoRetencion Then
            chkAplicarRetencion.Valor = False
        End If

        'Verificamos que el importe supero el monto minimo
        'Si es una moneda diferente a la principal
        Dim TotalComprobante As Decimal = txtTotalComprobante.ObtenerValor
        If cbxMoneda.GetValue <> 1 Then
            'Volvemos a la moneda local
        End If

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From AplicacionAnticipoProveedores Where IDSucursal=" & cbxSucursal.GetValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From AplicacionAnticipoProveedores Where IDSucursal=" & cbxSucursal.GetValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            BuscarOP()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

        'Guardar
        If e.KeyCode = vgKeyProcesar Then
            Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
        End If

    End Sub

    Sub CargarEgresos()
        Dim vCotizacionMoneda As Integer = CType(CSistema.ExecuteScalar("Select IsNull(Max(cotizacion), 1) From Cotizacion Where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' and IDMoneda=" & cbxMoneda.GetValue), Integer)
        If vCotizacionMoneda = 1 And cbxMoneda.GetValue <> 1 Then
            MessageBox.Show("Cargar cotizacion del dia para la moneda.", "Cotizacion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        CSistema.CargaControl(vControles, txtCotizacion)
        dtEgresos = CSistema.ExecuteToDataTable("Exec SpViewEgresosParaOrdenPagoAplicacion " & cbxSucursal.cbx.SelectedValue & ", @SoloAPagar='True' ")

        Dim frm As New frmOrdenPagoSeleccionarEgresos
        frm.Text = "Selección de Comprobantes Pendientes de Pago"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.IDMoneda = cbxMoneda.GetValue
        frm.dt = dtEgresos
        If txtProveedor.Seleccionado = True Then
            frm.IDProveedor = txtProveedor.Registro("ID")
        Else
            frm.IDProveedor = 0
        End If

        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccion de Egresos para pago", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        dtEgresos = frm.dt
        'dbs
        'Validar que deba generar o no retencion
        Try
            chkAplicarRetencion.Valor = CType(CSistema.ExecuteScalar("Select SujetoRetencion From Proveedor Where ID=" & txtProveedor.Registro("ID")), Boolean)
        Catch ex As Exception
            chkAplicarRetencion.Valor = 0
        End Try

        ListarEgresos()
        CambiarTotalEgresoPorPagoOtraMoneda()
        ObtenerEgresoRetencion()
        AnalizarSiAplicaRetencion()

    End Sub

    Sub CambiarTotalEgresoPorPagoOtraMoneda()

        'Validar
        If dtEgresos Is Nothing Then
            Exit Sub
        End If

        If dtEgresos.Rows.Count = 0 Then
            Exit Sub
        End If

        If dtOrdenPago Is Nothing Then
            Exit Sub
        End If

        If dtOrdenPago.Rows.Count = 0 Then
            Exit Sub
        End If

        'Si no se selecciono ningun comprobante, salir
        If dtEgresos.Select("Seleccionado='True'").Count = 0 Then
            Exit Sub
        End If

        'Si las monedas de pago y de los comprobantes son iguales, salir
        Dim IDMonedaComprobantes As Integer
        Dim Total As Decimal = 0
        Dim TotalRetencion As Decimal = 0

        For Each oRow As DataRow In dtEgresos.Select("Seleccionado='True'")
            IDMonedaComprobantes = oRow("IDMoneda")

            Dim Cotizacion As Decimal = oRow("Cotizacion")
            Dim Importe As Decimal = oRow("Importe")
            Dim Retencion As Decimal = oRow("RetencionIVA")

            If IDMonedaPago = IDMonedaComprobantes Then
                Total = Total + Importe
                TotalRetencion = TotalRetencion + Retencion
            Else
                'De GS a US
                If IDMonedaComprobantes = 1 Then

                    'Usamos la cotizacion del dia
                    Cotizacion = CotizacionDelDia
                    Total = Total + (Importe / Cotizacion)
                    TotalRetencion = TotalRetencion + (Retencion / Cotizacion)

                End If

                'De US a GS
                If IDMonedaComprobantes <> 1 Then
                    Total = Total + (Importe * Cotizacion)
                    TotalRetencion = TotalRetencion + (Retencion * Cotizacion)
                End If

            End If

        Next

        txtTotalComprobante.SetValue(Total)
        txtTotalRetencion.SetValue(TotalRetencion)

    End Sub

    Sub CargarAplicar()

        dtEgresos = CSistema.ExecuteToDataTable("Exec SpViewEgresosParaOrdenPagoAplicacion " & cbxSucursal.cbx.SelectedValue & ", 'True', " & IDTransaccion & " ")
        Dim frm As New frmOrdenPagoAplicarComprobante
        frm.Text = "Selección de Comprobantes Pendientes de Pago"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.IDTransaccion = IDTransaccion
        frm.dt = dtEgresos
        frm.IDMoneda = cbxMoneda.GetValue
        frm.Totalop = vTotalop
        frm.Totalop = txtSaldoTotal.ObtenerValor
        frm.Importe = vImporte
        If txtProveedor.txtRazonSocial.txt.Text <> "" Then
            frm.IDProveedor = txtProveedor.Registro("ID")
        Else
            frm.IDProveedor = 0
        End If

        frm.Inicializar()
        frm.ShowDialog(Me)
        CargarOperacion()

    End Sub

    Sub CargarEgresos(ByRef dt As DataTable)

        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpViewEgresosParaOrdenPagoAplicacion " & cbxSucursal.cbx.SelectedValue & " ").Copy

        For Each oRow As DataRow In dttemp.Rows

            Dim NewRow As DataRow = dt.NewRow
            NewRow("IDTransaccion") = oRow("IDTransaccion")
            NewRow("NroComprobante") = oRow("NroComprobante")
            NewRow("T. Comp.") = oRow("T. Comp.")
            NewRow("Proveedor") = oRow("Proveedor")
            NewRow("IDProveedor") = oRow("IDProveedor")
            NewRow("Fecha") = oRow("Fecha")
            NewRow("Fec") = oRow("Fec")
            NewRow("FechaVencimiento") = oRow("FechaVencimiento")
            NewRow("Fec. Venc.") = oRow("Fec. Venc.")
            NewRow("Total") = oRow("Total")
            NewRow("Saldo") = oRow("Saldo")
            NewRow("Importe") = oRow("Importe")
            NewRow("IDMoneda") = oRow("IDMOneda")
            NewRow("Moneda") = oRow("Moneda")
            NewRow("Cotizacion") = oRow("Cotizacion")
            NewRow("Observacion") = oRow("Observacion")
            NewRow("Seleccionado") = oRow("Seleccionado")
            NewRow("Cancelar") = oRow("Cancelar")
            dt.Rows.Add(NewRow)
        Next
    End Sub


    Function ExecuteToDataTableXLS(ByVal consulta As String, ByVal Archivo As String, Optional ByVal vConexion As String = "") As DataTable

        ExecuteToDataTableXLS = Nothing
        Try
            If vConexion <> "" Then
                VGCadenaConexion = vConexion
            End If
            '            Dim MiConexion As New System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Archivo & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'")
            Dim MiConexion As New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Archivo & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'")
            Dim MiAdaptador As New System.Data.OleDb.OleDbDataAdapter(consulta, MiConexion)
            Dim MiDataSet As New DataSet()
            Dim MiEnlazador As New BindingSource

            Dim commandbuilder As New OleDb.OleDbCommandBuilder(MiAdaptador)
            MiConexion.Open()
            MiAdaptador.Fill(MiDataSet)
            Return MiDataSet.Tables(0)

        Catch ex As Exception
            'XtraMessageBox.Show("Verifique el formato del archivo!", "No es posible importar.", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return Nothing
        End Try

    End Function

    Sub CalcularTotales()
        'Variables
        Dim TotalEgreso As Decimal = 0
        Dim SaldoTotal As Decimal = 0

        Dim DecimalesCobraza As Boolean = False
        Dim DecimalesFormaPago As Boolean = False

        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & IDMonedaPago, "VMoneda")("Decimales").ToString)
        DecimalesFormaPago = Decimales
        DecimalesCobraza = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)

        'Ver si se paga con una moneda diferente a la de la factura para colocar o no decimal en el saldo
        If DecimalesFormaPago <> DecimalesCobraza Then
            Decimales = True
        End If

        TotalEgreso = CSistema.gridSumColumn(dgw, "colImporte")
        TotalEgreso = CSistema.FormatoNumero(TotalEgreso, Decimales)

        Select Case FormaPago()
            Case ENUMFormaPago.ComprobanteUS_PagoGS
                TotalEgreso = ObtenerImporteComprobantes(False, True)

            Case ENUMFormaPago.ComprobanteUS_PagoUS
                TotalEgreso = ObtenerImporteComprobantes(True, False)

            Case Else
                TotalEgreso = ObtenerImporteComprobantes(True, True)
                TotalEgreso = CSistema.ExtraerDecimal(TotalEgreso)

        End Select

        If TotalEgreso < 0 Then
            TotalEgreso = TotalEgreso * -1
        End If

        'Total del Anticipo
        TotalAnticipo = CSistema.gridSumColumn(dgwOrdenPago, "colImporteOP")
        TotalAnticipo = CSistema.FormatoNumero(TotalAnticipo, Decimales)

        SaldoTotal = (TotalAnticipo) - (TotalEgreso)

        txtSaldoTotal.SetValue(CSistema.FormatoMoneda(SaldoTotal, Decimales))
        txtCantidadComprobante.SetValue(dgw.Rows.Count)

        txtSaldoTotal.SetValue(CSistema.FormatoMoneda(SaldoTotal, Decimales))
    End Sub

    Sub ObtenerEgresoRetencion()
        CalcularTotales()
    End Sub


    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)
        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnAgregarOrdenPago, btnAsiento, vControles, btnModificar)
    End Sub

    Public Sub EstablecerVariablesGlobales(Optional _vgIDUsuario As Integer = 0, Optional _vgIDSucursal As Integer = 0, Optional _vgIDDeposito As Integer = 0, Optional _vgIDTerminal As Integer = 0, Optional _vgIDPerfil As Integer = 0)

        If _vgIDUsuario <> 0 Then
            vgIDUsuario = _vgIDUsuario
        End If

        If _vgIDSucursal <> 0 Then
            vgIDSucursal = _vgIDSucursal
        End If

        If _vgIDDeposito <> 0 Then
            vgIDDeposito = _vgIDDeposito
        End If

        If _vgIDTerminal <> 0 Then
            vgIDTerminal = _vgIDTerminal
        End If

        If _vgIDPerfil <> 0 Then
            vgIDPerfil = _vgIDPerfil
        End If

    End Sub

    Private Sub frmAplicacionAnticipoProveedores_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If CargarAlIniciar = False Then
            Exit Sub
        End If
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBuscarOP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarOrdenPago.Click

        If txtProveedor.txtRazonSocial.txt.Text <> "" Then
            BuscarOP()

        Else
            MessageBox.Show("No se ha establecido un Proveedor para la busqueda.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtProveedor.Focus()

        End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub btnAgregarFacturasProveedor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarFacturasProveedor.Click

        If txtProveedor.txtRazonSocial.txt.Text <> "" Then
            CargarEgresos()

        Else
            MessageBox.Show("No se ha establecido un Proveedor para la busqueda.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtProveedor.Focus()

        End If
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Private Sub txtProveedor_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'Foco
        If vNuevo = True Then
            txtFecha.txt.Focus()
        End If

    End Sub

    Private Sub txtProveedor_ItemMalSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If txtProveedor.SoloLectura = True Then
            Exit Sub
        End If

    End Sub

    Sub ObtenerInformacionProveedor()

        Dim oRow As DataRow = txtProveedor.Registro
        Dim oRowSucursal As DataRow = txtProveedor.Sucursal

        'Datos Comunes
        txtProveedor.txtReferencia.txt.Text = oRow("Referencia").ToString
        txtProveedor.txtRazonSocial.txt.Text = oRow("RazonSocial").ToString

    End Sub

    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        CAsiento.IDSucursal = cbxSucursal.GetValue
        CAsiento.IDMoneda = 1
        CAsiento.InicializarAsiento()

        If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxSucursal.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub chkAplicarRetencion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkAplicarRetencion.PropertyChanged
        ObtenerEgresoRetencion()
    End Sub

    Private Sub dgwOrdenPago_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgwOrdenPago.CellClick
        ctrError.Clear()

        'Validar
        If dgwOrdenPago.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOrdenPago, Mensaje)
            ctrError.SetIconAlignment(dgwOrdenPago, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOrdenPago.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOrdenPago, Mensaje)
            ctrError.SetIconAlignment(dgwOrdenPago, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        Dim vIDTransaccion As Integer

        vIDTransaccion = dgwOrdenPago.SelectedRows(0).Cells(0).Value

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub frmAplicacionProveedoresAnticipo_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmAplicacionProveedoresAnticipo_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Enter Then

            If txtObservacion.txt.Focused Then
                Exit Sub
            End If

            CSistema.SelectNextControl(Me, e.KeyCode)

        End If
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmAplicacionProveedoresAnticipo_Activate()
        Me.Refresh()
    End Sub

    Private Sub lklEliminarEgreso_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lklEliminarEgreso.LinkClicked
        EliminarEgreso()
    End Sub

    Private Sub lklEliminarOrdenPago_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lklEliminarOrdenPago.LinkClicked
        EliminarOrdenPago()
    End Sub

    Private Sub cbxMoneda_Leave(sender As Object, e As EventArgs) Handles cbxMoneda.Leave

        'Dim Cotizacion As Integer = CType(CSistema.ExecuteScalar("Select IsNull(Max(cotizacion), 1) From Cotizacion Where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' and IDMoneda=" & cbxMoneda.GetValue), Integer)

        'txtCotizacion.txt.Text = Cotizacion

    End Sub
End Class
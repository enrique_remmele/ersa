﻿Public Class frmTipoAnticipo
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDTipoAnticipoValue As Integer
    Public Property IDTipoAnticipo() As Integer
        Get
            Return IDTipoAnticipoValue
        End Get
        Set(ByVal value As Integer)
            IDTipoAnticipoValue = value
        End Set
    End Property

    Private DescripcionValue As String
    Public Property Descripcion() As String
        Get
            Return DescripcionValue
        End Get
        Set(ByVal value As String)
            DescripcionValue = value
        End Set
    End Property

    Public Property Procesado As Boolean

    'CLASES
    Dim CSistema As New CSistema

    Sub Inicilizar()

        CSistema.SqlToComboBox(cbxTipoAnticipo.cbx, "Select ID, Descripcion from TipoAnticipo where estado = 1")
        Procesado = False

    End Sub

    Sub Anular()

        If cbxTipoAnticipo.cbx.SelectedValue Is Nothing Then
            tsslEstado.Text = "Atencion: Tipo invalido"
            ctrError.SetError(cbxTipoAnticipo, "Atencion: Tipo invalido")
            ctrError.SetIconAlignment(cbxTipoAnticipo, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        IDTipoAnticipo = cbxTipoAnticipo.GetValue
        Descripcion = cbxTipoAnticipo.Texto

        Procesado = True
        Me.Close()

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Anular()
    End Sub

    Private Sub frmTipoAnticipo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicilizar()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmTipoAnticipo_Activate()
        Me.Refresh()
    End Sub
End Class
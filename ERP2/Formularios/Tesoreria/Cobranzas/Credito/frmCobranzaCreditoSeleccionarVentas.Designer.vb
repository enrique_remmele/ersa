﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCobranzaCreditoSeleccionarVentas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCondicion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colVencimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCobrado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDescontado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldoGs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporteGs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCancelar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colCotizacionHoy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SeleccionarTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuitarTodaSeleccionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblTotalCobrado = New System.Windows.Forms.Label()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblDeudaTotal = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.txtCliente = New ERP.ocxTXTString()
        Me.txtCantidadCobrado = New ERP.ocxTXTNumeric()
        Me.txtDeudaTotal = New ERP.ocxTXTNumeric()
        Me.txtCantidadComprobantes = New ERP.ocxTXTNumeric()
        Me.txtTotalCobrado = New ERP.ocxTXTNumeric()
        Me.txtTotalCobradoGs = New ERP.ocxTXTNumeric()
        Me.Label2 = New System.Windows.Forms.Label()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colSel, Me.colComprobante, Me.colTipo, Me.colCondicion, Me.colVencimiento, Me.colTotal, Me.colCobrado, Me.colDescontado, Me.colSaldo, Me.colSaldoGs, Me.colImporte, Me.colImporteGs, Me.colCancelar, Me.colCotizacionHoy})
        Me.dgw.ContextMenuStrip = Me.ContextMenuStrip1
        Me.dgw.Location = New System.Drawing.Point(12, 31)
        Me.dgw.Name = "dgw"
        Me.dgw.RowHeadersVisible = False
        Me.dgw.Size = New System.Drawing.Size(850, 270)
        Me.dgw.TabIndex = 2
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colSel
        '
        Me.colSel.HeaderText = "Sel"
        Me.colSel.Name = "colSel"
        Me.colSel.ReadOnly = True
        Me.colSel.Width = 30
        '
        'colComprobante
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colComprobante.DefaultCellStyle = DataGridViewCellStyle2
        Me.colComprobante.HeaderText = "Comprobante"
        Me.colComprobante.Name = "colComprobante"
        Me.colComprobante.ReadOnly = True
        Me.colComprobante.ToolTipText = "Comprobante de Venta"
        Me.colComprobante.Width = 95
        '
        'colTipo
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colTipo.DefaultCellStyle = DataGridViewCellStyle3
        Me.colTipo.HeaderText = "Tipo"
        Me.colTipo.Name = "colTipo"
        Me.colTipo.ReadOnly = True
        Me.colTipo.ToolTipText = "Tipo de Comprobante"
        Me.colTipo.Width = 50
        '
        'colCondicion
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colCondicion.DefaultCellStyle = DataGridViewCellStyle4
        Me.colCondicion.HeaderText = "Cond."
        Me.colCondicion.Name = "colCondicion"
        Me.colCondicion.ReadOnly = True
        Me.colCondicion.Width = 50
        '
        'colVencimiento
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colVencimiento.DefaultCellStyle = DataGridViewCellStyle5
        Me.colVencimiento.HeaderText = "Venc."
        Me.colVencimiento.Name = "colVencimiento"
        Me.colVencimiento.ReadOnly = True
        Me.colVencimiento.Width = 65
        '
        'colTotal
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N0"
        DataGridViewCellStyle6.NullValue = "0"
        Me.colTotal.DefaultCellStyle = DataGridViewCellStyle6
        Me.colTotal.HeaderText = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.ReadOnly = True
        Me.colTotal.Width = 78
        '
        'colCobrado
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N0"
        DataGridViewCellStyle7.NullValue = "0"
        Me.colCobrado.DefaultCellStyle = DataGridViewCellStyle7
        Me.colCobrado.HeaderText = "Cobrado"
        Me.colCobrado.Name = "colCobrado"
        Me.colCobrado.ReadOnly = True
        Me.colCobrado.Width = 78
        '
        'colDescontado
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N0"
        DataGridViewCellStyle8.NullValue = "0"
        Me.colDescontado.DefaultCellStyle = DataGridViewCellStyle8
        Me.colDescontado.HeaderText = "Descontado"
        Me.colDescontado.Name = "colDescontado"
        Me.colDescontado.ReadOnly = True
        Me.colDescontado.Width = 78
        '
        'colSaldo
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N0"
        DataGridViewCellStyle9.NullValue = "0"
        Me.colSaldo.DefaultCellStyle = DataGridViewCellStyle9
        Me.colSaldo.HeaderText = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.ReadOnly = True
        Me.colSaldo.Width = 78
        '
        'colSaldoGs
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N0"
        Me.colSaldoGs.DefaultCellStyle = DataGridViewCellStyle10
        Me.colSaldoGs.HeaderText = "Saldo Gs"
        Me.colSaldoGs.Name = "colSaldoGs"
        '
        'colImporte
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.Format = "N0"
        DataGridViewCellStyle11.NullValue = "0"
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle11
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        Me.colImporte.ToolTipText = "Importe Cobrado"
        Me.colImporte.Width = 80
        '
        'colImporteGs
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colImporteGs.DefaultCellStyle = DataGridViewCellStyle12
        Me.colImporteGs.HeaderText = "ImporteGs"
        Me.colImporteGs.Name = "colImporteGs"
        '
        'colCancelar
        '
        Me.colCancelar.HeaderText = "Cancel"
        Me.colCancelar.Name = "colCancelar"
        Me.colCancelar.ReadOnly = True
        Me.colCancelar.ToolTipText = "Cancelar manualmente el comprobante"
        Me.colCancelar.Width = 35
        '
        'colCotizacionHoy
        '
        Me.colCotizacionHoy.HeaderText = "Cotizacion hoy"
        Me.colCotizacionHoy.Name = "colCotizacionHoy"
        Me.colCotizacionHoy.Visible = False
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SeleccionarTodoToolStripMenuItem, Me.QuitarTodaSeleccionToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(187, 48)
        '
        'SeleccionarTodoToolStripMenuItem
        '
        Me.SeleccionarTodoToolStripMenuItem.Name = "SeleccionarTodoToolStripMenuItem"
        Me.SeleccionarTodoToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.SeleccionarTodoToolStripMenuItem.Text = "Seleccionar todo"
        '
        'QuitarTodaSeleccionToolStripMenuItem
        '
        Me.QuitarTodaSeleccionToolStripMenuItem.Name = "QuitarTodaSeleccionToolStripMenuItem"
        Me.QuitarTodaSeleccionToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.QuitarTodaSeleccionToolStripMenuItem.Text = "Quitar toda seleccion"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(9, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Cliente:"
        '
        'lblTotalCobrado
        '
        Me.lblTotalCobrado.AutoSize = True
        Me.lblTotalCobrado.Location = New System.Drawing.Point(458, 312)
        Me.lblTotalCobrado.Name = "lblTotalCobrado"
        Me.lblTotalCobrado.Size = New System.Drawing.Size(60, 13)
        Me.lblTotalCobrado.TabIndex = 7
        Me.lblTotalCobrado.Text = "Total Oper:"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(9, 307)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(87, 13)
        Me.LinkLabel1.TabIndex = 3
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Seleccionar todo"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(696, 335)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(85, 23)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "&Aceptar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(787, 335)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 11
        Me.Button2.Text = "&Cancelar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 366)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(874, 22)
        Me.StatusStrip1.TabIndex = 12
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'lblDeudaTotal
        '
        Me.lblDeudaTotal.AutoSize = True
        Me.lblDeudaTotal.Location = New System.Drawing.Point(215, 312)
        Me.lblDeudaTotal.Name = "lblDeudaTotal"
        Me.lblDeudaTotal.Size = New System.Drawing.Size(65, 13)
        Me.lblDeudaTotal.TabIndex = 4
        Me.lblDeudaTotal.Text = "Deuda total:"
        '
        'lblComprobante
        '
        Me.lblComprobante.Location = New System.Drawing.Point(401, 2)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(106, 23)
        Me.lblComprobante.TabIndex = 15
        Me.lblComprobante.Text = "Nro. Comprobante:"
        Me.lblComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(513, 5)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(71, 20)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 14
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'txtCliente
        '
        Me.txtCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCliente.Color = System.Drawing.Color.Empty
        Me.txtCliente.Indicaciones = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(65, 4)
        Me.txtCliente.Multilinea = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(330, 21)
        Me.txtCliente.SoloLectura = True
        Me.txtCliente.TabIndex = 13
        Me.txtCliente.TabStop = False
        Me.txtCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCliente.Texto = ""
        '
        'txtCantidadCobrado
        '
        Me.txtCantidadCobrado.Color = System.Drawing.Color.Empty
        Me.txtCantidadCobrado.Decimales = True
        Me.txtCantidadCobrado.Indicaciones = Nothing
        Me.txtCantidadCobrado.Location = New System.Drawing.Point(638, 307)
        Me.txtCantidadCobrado.Name = "txtCantidadCobrado"
        Me.txtCantidadCobrado.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadCobrado.SoloLectura = True
        Me.txtCantidadCobrado.TabIndex = 9
        Me.txtCantidadCobrado.TabStop = False
        Me.txtCantidadCobrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadCobrado.Texto = "0"
        '
        'txtDeudaTotal
        '
        Me.txtDeudaTotal.Color = System.Drawing.Color.Empty
        Me.txtDeudaTotal.Decimales = True
        Me.txtDeudaTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDeudaTotal.Indicaciones = Nothing
        Me.txtDeudaTotal.Location = New System.Drawing.Point(284, 307)
        Me.txtDeudaTotal.Name = "txtDeudaTotal"
        Me.txtDeudaTotal.Size = New System.Drawing.Size(108, 22)
        Me.txtDeudaTotal.SoloLectura = True
        Me.txtDeudaTotal.TabIndex = 5
        Me.txtDeudaTotal.TabStop = False
        Me.txtDeudaTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDeudaTotal.Texto = "0"
        '
        'txtCantidadComprobantes
        '
        Me.txtCantidadComprobantes.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobantes.Decimales = True
        Me.txtCantidadComprobantes.Indicaciones = Nothing
        Me.txtCantidadComprobantes.Location = New System.Drawing.Point(398, 307)
        Me.txtCantidadComprobantes.Name = "txtCantidadComprobantes"
        Me.txtCantidadComprobantes.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadComprobantes.SoloLectura = True
        Me.txtCantidadComprobantes.TabIndex = 6
        Me.txtCantidadComprobantes.TabStop = False
        Me.txtCantidadComprobantes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobantes.Texto = "0"
        '
        'txtTotalCobrado
        '
        Me.txtTotalCobrado.Color = System.Drawing.Color.Empty
        Me.txtTotalCobrado.Decimales = True
        Me.txtTotalCobrado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalCobrado.Indicaciones = Nothing
        Me.txtTotalCobrado.Location = New System.Drawing.Point(517, 307)
        Me.txtTotalCobrado.Name = "txtTotalCobrado"
        Me.txtTotalCobrado.Size = New System.Drawing.Size(115, 22)
        Me.txtTotalCobrado.SoloLectura = True
        Me.txtTotalCobrado.TabIndex = 8
        Me.txtTotalCobrado.TabStop = False
        Me.txtTotalCobrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCobrado.Texto = "0"
        '
        'txtTotalCobradoGs
        '
        Me.txtTotalCobradoGs.Color = System.Drawing.Color.Empty
        Me.txtTotalCobradoGs.Decimales = False
        Me.txtTotalCobradoGs.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalCobradoGs.Indicaciones = Nothing
        Me.txtTotalCobradoGs.Location = New System.Drawing.Point(747, 307)
        Me.txtTotalCobradoGs.Name = "txtTotalCobradoGs"
        Me.txtTotalCobradoGs.Size = New System.Drawing.Size(115, 22)
        Me.txtTotalCobradoGs.SoloLectura = True
        Me.txtTotalCobradoGs.TabIndex = 17
        Me.txtTotalCobradoGs.TabStop = False
        Me.txtTotalCobradoGs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCobradoGs.Texto = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(698, 312)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Total Gs:"
        '
        'frmCobranzaCreditoSeleccionarVentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(874, 388)
        Me.Controls.Add(Me.txtTotalCobradoGs)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dgw)
        Me.Controls.Add(Me.lblComprobante)
        Me.Controls.Add(Me.txtComprobante)
        Me.Controls.Add(Me.txtCliente)
        Me.Controls.Add(Me.txtCantidadCobrado)
        Me.Controls.Add(Me.txtDeudaTotal)
        Me.Controls.Add(Me.lblDeudaTotal)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.txtCantidadComprobantes)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.LinkLabel1)
        Me.Controls.Add(Me.txtTotalCobrado)
        Me.Controls.Add(Me.lblTotalCobrado)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmCobranzaCreditoSeleccionarVentas"
        Me.Text = "frmCobranzaCreditoSeleccionarVentas"
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTotalCobrado As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalCobrado As System.Windows.Forms.Label
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents txtCantidadComprobantes As ERP.ocxTXTNumeric
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents txtCantidadCobrado As ERP.ocxTXTNumeric
    Friend WithEvents txtDeudaTotal As ERP.ocxTXTNumeric
    Friend WithEvents lblDeudaTotal As System.Windows.Forms.Label
    Friend WithEvents txtCliente As ERP.ocxTXTString
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SeleccionarTodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuitarTodaSeleccionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents colIDTransaccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSel As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCondicion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colVencimiento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCobrado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDescontado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSaldo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSaldoGs As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporteGs As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCancelar As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colCotizacionHoy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtTotalCobradoGs As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class

﻿Public Class frmSeleccionarDebitoCreditoBancario
    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Public Property IDMoneda As Integer
    Public Property Decimales As Boolean

    'VARIABLES
    Dim vCargado As Boolean
    Dim vdt As DataTable
    Dim SeleccionRegistro As Boolean
    'FUNCIONES

    Sub Inicializar()

        vCargado = False
        SeleccionRegistro = False
        'Propiedades
        'Decimales = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & IDMoneda, "VMoneda")("Decimales"))

        'Funciones
        Listar(False)
        Cargar()

        'Foco
        dgw.Focus()

    End Sub

    Sub Listar(ByVal PorFactura As Boolean)

        'dbs
        Dim Where As String = ""

        If PorFactura = True Then
            Where = "NroComprobante =" & txtComprobante.GetValue
        Else
            Where = ""
        End If
        'fin dbs
        
        'Limpiar la grilla
        dgw.Rows.Clear()
        Dim Total As Decimal = 0
        Dim TotalImpuesto As Decimal = 0
        vCargado = False



        For Each oRow As DataRow In dt.Select(Where)
            Dim Registro(12) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Seleccionado").ToString
            Registro(2) = oRow("NroComprobante").ToString
            Registro(3) = oRow("Tipo").ToString
            Registro(4) = oRow("Fecha").ToString
            Registro(5) = oRow("Cuenta").ToString
            Registro(6) = oRow("Banco").ToString
            Registro(7) = oRow("Moneda").ToString
            Registro(8) = CSistema.FormatoNumeroBaseDatos((oRow("Total").ToString), True)
            Registro(9) = CSistema.FormatoNumeroBaseDatos((oRow("TotalImpuesto").ToString), True)
            Registro(10) = oRow("Observacion").ToString
            Registro(11) = oRow("Cancelar").ToString
            Registro(12) = oRow("Cotizacion").ToString

            'Sumar el total de la deuda
            Total = Total + CDec(oRow("Total").ToString)

            dgw.Rows.Add(Registro)

        Next

    End Sub

    Sub Seleccionar()

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells(0).Value

            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Seleccionado") = oRow.Cells(1).Value
                oRow1("Total") = CDec(oRow1("Total").ToString)
                oRow1("TotalImpuesto") = CDec(oRow1("TotalImpuesto").ToString)

                oRow1("Cancelar") = oRow.Cells(11).Value

            Next

        Next
        Me.Close()

    End Sub

    Sub Cargar()
        For Each oRow As DataRow In dt.Rows
            For Each oRow1 As DataGridViewRow In dgw.Rows
                If oRow("IDTransaccion") = oRow1.Cells(0).Value Then
                    oRow1.Cells(1).Value = CBool(oRow("Seleccionado").ToString)
                    oRow1.Cells(8).Value = CDec(oRow("Total").ToString)
                    oRow1.Cells(9).Value = CDec(oRow("TotalImpuesto").ToString)
                    oRow1.Cells(11).Value = CBool(oRow("Cancelar").ToString)
                End If
            Next
        Next

        ' PintarCelda()
        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim Total As Decimal = 0
        Dim contador As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows

            If oRow.Cells(1).Value = True Then
                Total = Total + CDec(oRow.Cells(8).Value)
                contador = contador + 1
                'Marca registro Marcado
                SeleccionRegistro = True
            End If

        Next

        txtCantidadSeleccionado.SetValue(contador)
        txtSeleccionado.SetValue(Total)

    End Sub

    Private Sub frmOrdenPagoSeleccionar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        vCargado = True
    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick

        ctrError.Clear()
        tsslEstado.Text = ""

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        If e.ColumnIndex = dgw.Columns(1).Index Then
            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(10).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(6)

                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(6).ReadOnly = True
                        oRow.Cells(10).ReadOnly = True
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next
            CalcularTotales()
            dgw.Update()
        End If

    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Columns(e.ColumnIndex).Name = "colTotal" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El Total debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("Total").Value

            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Space Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(6).ReadOnly = False
                        oRow.Cells(10).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(6).ReadOnly = True
                        oRow.Cells(10).ReadOnly = True
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If

        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(8).ReadOnly = False
                        oRow.Cells(10).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        ' dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(6)
                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(8).ReadOnly = True
                        oRow.Cells(10).ReadOnly = True
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colTotal")
            End If

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If

    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        If vCargado = False Then
            Exit Sub
        End If

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)

        If dgw.Rows(e.RowIndex).Cells(1).Value = False Then
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
        End If

        dgw.Rows(e.RowIndex).DefaultCellStyle.Font = f1

    End Sub

    Private Sub dgw_RowLeave(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowLeave

        If vCargado = False Then
            Exit Sub
        End If

        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        dgw.Rows(e.RowIndex).DefaultCellStyle.Font = f2

        If dgw.Rows(e.RowIndex).Cells(1).Value = False Then
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
        Else
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.PaleTurquoise
        End If

    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp

        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

        End If

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If txtCantidadSeleccionado.txt.Text = 0 Then
            Dim mensaje As String = " Debe seleccionar un DBC para generar EXTORNO "
            ctrError.SetError(btnAceptar, mensaje)
            ctrError.SetIconAlignment(btnAceptar, ErrorIconAlignment.BottomRight)
            tsslEstado.Text = mensaje
            'Foco
            dgw.Focus()
        Else

            Seleccionar()
        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        If txtCantidadSeleccionado.txt.Text <> 0 Then
            Dim mensaje As String = " Se Cancela Operacion SIN generar EXTORNO "
            ctrError.SetError(btnCancelar, mensaje)
            ctrError.SetIconAlignment(btnCancelar, ErrorIconAlignment.BottomRight)
            tsslEstado.Text = mensaje
            'Foco
            'dgw.Focus()
            Me.Close()
        Else
            Dim mensaje As String = " Debe seleccionar un DCB para generar EXTORNO "
            ctrError.SetError(btnCancelar, mensaje)
            ctrError.SetIconAlignment(btnCancelar, ErrorIconAlignment.BottomRight)
            tsslEstado.Text = mensaje
            'Foco
            dgw.Focus()
        End If
    End Sub

    Private Sub lnkSeleccionar_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkSeleccionar.LinkClicked
        For Each fila As DataGridViewRow In dgw.Rows
            fila.Cells("colSel").Value = True
        Next
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmSeleccionarDebitoCreditoBancario_Activate()
        Me.Refresh()
    End Sub

    Private Sub txtComprobante_TeclaPrecionada(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            Listar(True)
        End If
    End Sub
End Class
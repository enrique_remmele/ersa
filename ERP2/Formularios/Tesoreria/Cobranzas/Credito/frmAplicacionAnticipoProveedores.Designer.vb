﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAplicacionAnticipoProveedores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.lblOP = New System.Windows.Forms.Label()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tpCheque = New System.Windows.Forms.TabPage()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.lblCuentaBancaria = New System.Windows.Forms.Label()
        Me.lblOrden = New System.Windows.Forms.Label()
        Me.lblFechaCheque = New System.Windows.Forms.Label()
        Me.lblFechaPagoCheque = New System.Windows.Forms.Label()
        Me.lblNroCheque = New System.Windows.Forms.Label()
        Me.lblCotizacionCheque = New System.Windows.Forms.Label()
        Me.lblImporteMoneda = New System.Windows.Forms.Label()
        Me.lblDiferido = New System.Windows.Forms.Label()
        Me.chkDiferido = New System.Windows.Forms.CheckBox()
        Me.lblVencimiento = New System.Windows.Forms.Label()
        Me.tpDocumento = New System.Windows.Forms.TabPage()
        Me.btnAgregarComprobantes = New System.Windows.Forms.Button()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBanco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colProveedor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCotizacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotalImpuesto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRetencionIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIDTransaccionEgreso = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIDTransaccionOrdenPago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.lklEliminarEgreso = New System.Windows.Forms.LinkLabel()
        Me.lblTotalComprobante = New System.Windows.Forms.Label()
        Me.lblDiferenciaCambio = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.lblSaldoTotal = New System.Windows.Forms.Label()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmiEliminarComprobante = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.VisualizarAsientoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VisualizarElComprobanteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LblTotalOP = New System.Windows.Forms.Label()
        Me.LblSaldoOP = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtEquivalente = New ERP.ocxTXTNumeric()
        Me.TxtTotalOP = New ERP.ocxTXTNumeric()
        Me.TxtSaldoOP = New ERP.ocxTXTNumeric()
        Me.txtDiferenciaCambio = New ERP.ocxTXTNumeric()
        Me.txtSaldoTotal = New ERP.ocxTXTNumeric()
        Me.txtCantidadComprobante = New ERP.ocxTXTNumeric()
        Me.txtTotalComprobante = New ERP.ocxTXTNumeric()
        Me.txtImporteMoneda = New ERP.ocxTXTNumeric()
        Me.txtVencimiento = New ERP.ocxTXTDate()
        Me.txtOrden = New ERP.ocxTXTString()
        Me.cbxCuentaBancaria = New ERP.ocxCBX()
        Me.txtFechaPagoCheque = New ERP.ocxTXTDate()
        Me.txtFechaCheque = New ERP.ocxTXTDate()
        Me.txtMoneda = New ERP.ocxTXTString()
        Me.txtBanco = New ERP.ocxTXTString()
        Me.txtNroCheque = New ERP.ocxTXTString()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.OcxOrdenPagoDocumento1 = New ERP.ocxOrdenPagoDocumento()
        Me.txtNroOrdenPago = New ERP.ocxTXTString()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.gbxCabecera.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tpCheque.SuspendLayout()
        Me.tpDocumento.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.flpRegistradoPor.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.txtNroOrdenPago)
        Me.gbxCabecera.Controls.Add(Me.lblOP)
        Me.gbxCabecera.Controls.Add(Me.cbxMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.txtProveedor)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblProveedor)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Location = New System.Drawing.Point(12, 12)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(669, 94)
        Me.gbxCabecera.TabIndex = 1
        Me.gbxCabecera.TabStop = False
        '
        'lblOP
        '
        Me.lblOP.AutoSize = True
        Me.lblOP.Location = New System.Drawing.Point(504, 16)
        Me.lblOP.Name = "lblOP"
        Me.lblOP.Size = New System.Drawing.Size(82, 13)
        Me.lblOP.TabIndex = 8
        Me.lblOP.Text = "Orden Pago N°:"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(15, 42)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 10
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(24, 68)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 14
        Me.lblFecha.Text = "Fecha:"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(141, 42)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(59, 13)
        Me.lblProveedor.TabIndex = 12
        Me.lblProveedor.Text = "Proveedor:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(168, 68)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(32, 13)
        Me.lblObservacion.TabIndex = 16
        Me.lblObservacion.Text = "Obs.:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(194, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(339, 16)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(37, 13)
        Me.lblComprobante.TabIndex = 5
        Me.lblComprobante.Text = "Comp:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(5, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operación:"
        '
        'TabControl1
        '
        Me.TabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons
        Me.TabControl1.Controls.Add(Me.tpCheque)
        Me.TabControl1.Controls.Add(Me.tpDocumento)
        Me.TabControl1.Location = New System.Drawing.Point(11, 112)
        Me.TabControl1.Multiline = True
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(670, 155)
        Me.TabControl1.TabIndex = 2
        '
        'tpCheque
        '
        Me.tpCheque.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.tpCheque.Controls.Add(Me.txtImporteMoneda)
        Me.tpCheque.Controls.Add(Me.lblImporte)
        Me.tpCheque.Controls.Add(Me.txtVencimiento)
        Me.tpCheque.Controls.Add(Me.txtOrden)
        Me.tpCheque.Controls.Add(Me.lblCuentaBancaria)
        Me.tpCheque.Controls.Add(Me.lblOrden)
        Me.tpCheque.Controls.Add(Me.cbxCuentaBancaria)
        Me.tpCheque.Controls.Add(Me.txtFechaPagoCheque)
        Me.tpCheque.Controls.Add(Me.lblFechaCheque)
        Me.tpCheque.Controls.Add(Me.lblFechaPagoCheque)
        Me.tpCheque.Controls.Add(Me.txtFechaCheque)
        Me.tpCheque.Controls.Add(Me.txtMoneda)
        Me.tpCheque.Controls.Add(Me.lblNroCheque)
        Me.tpCheque.Controls.Add(Me.txtBanco)
        Me.tpCheque.Controls.Add(Me.txtNroCheque)
        Me.tpCheque.Controls.Add(Me.txtImporte)
        Me.tpCheque.Controls.Add(Me.txtCotizacion)
        Me.tpCheque.Controls.Add(Me.lblCotizacionCheque)
        Me.tpCheque.Controls.Add(Me.lblImporteMoneda)
        Me.tpCheque.Controls.Add(Me.lblDiferido)
        Me.tpCheque.Controls.Add(Me.chkDiferido)
        Me.tpCheque.Controls.Add(Me.lblVencimiento)
        Me.tpCheque.Location = New System.Drawing.Point(4, 25)
        Me.tpCheque.Name = "tpCheque"
        Me.tpCheque.Padding = New System.Windows.Forms.Padding(3)
        Me.tpCheque.Size = New System.Drawing.Size(662, 126)
        Me.tpCheque.TabIndex = 0
        Me.tpCheque.Text = "Cheque"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(406, 73)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(74, 13)
        Me.lblImporte.TabIndex = 18
        Me.lblImporte.Text = "Importe Local:"
        '
        'lblCuentaBancaria
        '
        Me.lblCuentaBancaria.AutoSize = True
        Me.lblCuentaBancaria.Location = New System.Drawing.Point(34, 21)
        Me.lblCuentaBancaria.Name = "lblCuentaBancaria"
        Me.lblCuentaBancaria.Size = New System.Drawing.Size(44, 13)
        Me.lblCuentaBancaria.TabIndex = 0
        Me.lblCuentaBancaria.Text = "Cuenta:"
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Location = New System.Drawing.Point(34, 100)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(75, 13)
        Me.lblOrden.TabIndex = 20
        Me.lblOrden.Text = "A la Orden de:"
        '
        'lblFechaCheque
        '
        Me.lblFechaCheque.AutoSize = True
        Me.lblFechaCheque.Location = New System.Drawing.Point(34, 47)
        Me.lblFechaCheque.Name = "lblFechaCheque"
        Me.lblFechaCheque.Size = New System.Drawing.Size(40, 13)
        Me.lblFechaCheque.TabIndex = 6
        Me.lblFechaCheque.Text = "Fecha:"
        '
        'lblFechaPagoCheque
        '
        Me.lblFechaPagoCheque.AutoSize = True
        Me.lblFechaPagoCheque.Location = New System.Drawing.Point(181, 47)
        Me.lblFechaPagoCheque.Name = "lblFechaPagoCheque"
        Me.lblFechaPagoCheque.Size = New System.Drawing.Size(83, 13)
        Me.lblFechaPagoCheque.TabIndex = 8
        Me.lblFechaPagoCheque.Text = "Fecha de Pago:"
        '
        'lblNroCheque
        '
        Me.lblNroCheque.AutoSize = True
        Me.lblNroCheque.Location = New System.Drawing.Point(431, 21)
        Me.lblNroCheque.Name = "lblNroCheque"
        Me.lblNroCheque.Size = New System.Drawing.Size(70, 13)
        Me.lblNroCheque.TabIndex = 4
        Me.lblNroCheque.Text = "Nro. Cheque:"
        '
        'lblCotizacionCheque
        '
        Me.lblCotizacionCheque.AutoSize = True
        Me.lblCotizacionCheque.Location = New System.Drawing.Point(34, 73)
        Me.lblCotizacionCheque.Name = "lblCotizacionCheque"
        Me.lblCotizacionCheque.Size = New System.Drawing.Size(59, 13)
        Me.lblCotizacionCheque.TabIndex = 14
        Me.lblCotizacionCheque.Text = "Cotización:"
        '
        'lblImporteMoneda
        '
        Me.lblImporteMoneda.AutoSize = True
        Me.lblImporteMoneda.Location = New System.Drawing.Point(181, 73)
        Me.lblImporteMoneda.Name = "lblImporteMoneda"
        Me.lblImporteMoneda.Size = New System.Drawing.Size(102, 13)
        Me.lblImporteMoneda.TabIndex = 16
        Me.lblImporteMoneda.Text = "Importe de Moneda:"
        '
        'lblDiferido
        '
        Me.lblDiferido.AutoSize = True
        Me.lblDiferido.Location = New System.Drawing.Point(341, 46)
        Me.lblDiferido.Name = "lblDiferido"
        Me.lblDiferido.Size = New System.Drawing.Size(86, 13)
        Me.lblDiferido.TabIndex = 10
        Me.lblDiferido.Text = "Cheque Diferido:"
        '
        'chkDiferido
        '
        Me.chkDiferido.AutoSize = True
        Me.chkDiferido.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkDiferido.Enabled = False
        Me.chkDiferido.Location = New System.Drawing.Point(429, 46)
        Me.chkDiferido.Name = "chkDiferido"
        Me.chkDiferido.Size = New System.Drawing.Size(15, 14)
        Me.chkDiferido.TabIndex = 11
        Me.chkDiferido.UseVisualStyleBackColor = True
        '
        'lblVencimiento
        '
        Me.lblVencimiento.AutoSize = True
        Me.lblVencimiento.Location = New System.Drawing.Point(445, 47)
        Me.lblVencimiento.Name = "lblVencimiento"
        Me.lblVencimiento.Size = New System.Drawing.Size(116, 13)
        Me.lblVencimiento.TabIndex = 12
        Me.lblVencimiento.Text = "Fecha de Vencimiento:"
        '
        'tpDocumento
        '
        Me.tpDocumento.Controls.Add(Me.OcxOrdenPagoDocumento1)
        Me.tpDocumento.Location = New System.Drawing.Point(4, 25)
        Me.tpDocumento.Margin = New System.Windows.Forms.Padding(2)
        Me.tpDocumento.Name = "tpDocumento"
        Me.tpDocumento.Padding = New System.Windows.Forms.Padding(2)
        Me.tpDocumento.Size = New System.Drawing.Size(662, 126)
        Me.tpDocumento.TabIndex = 2
        Me.tpDocumento.Text = "Documentos"
        Me.tpDocumento.UseVisualStyleBackColor = True
        '
        'btnAgregarComprobantes
        '
        Me.btnAgregarComprobantes.Location = New System.Drawing.Point(12, 302)
        Me.btnAgregarComprobantes.Name = "btnAgregarComprobantes"
        Me.btnAgregarComprobantes.Size = New System.Drawing.Size(130, 21)
        Me.btnAgregarComprobantes.TabIndex = 4
        Me.btnAgregarComprobantes.Text = "Agregar Comprobantes"
        Me.btnAgregarComprobantes.UseVisualStyleBackColor = True
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colBanco, Me.colFecha, Me.colProveedor, Me.colMoneda, Me.colCotizacion, Me.colTotal, Me.colSaldo, Me.colImporte, Me.colTotalImpuesto, Me.colRetencionIVA, Me.colIDTransaccionEgreso, Me.colIDTransaccionOrdenPago})
        Me.dgw.Location = New System.Drawing.Point(15, 329)
        Me.dgw.Name = "dgw"
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(662, 153)
        Me.dgw.StandardTab = True
        Me.dgw.TabIndex = 5
        '
        'colIDTransaccion
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = "0"
        Me.colIDTransaccion.DefaultCellStyle = DataGridViewCellStyle2
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colBanco
        '
        Me.colBanco.HeaderText = "Comprobante"
        Me.colBanco.Name = "colBanco"
        '
        'colFecha
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colFecha.DefaultCellStyle = DataGridViewCellStyle3
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.ReadOnly = True
        Me.colFecha.Width = 80
        '
        'colProveedor
        '
        Me.colProveedor.HeaderText = "Proveedor"
        Me.colProveedor.Name = "colProveedor"
        Me.colProveedor.ReadOnly = True
        Me.colProveedor.Width = 170
        '
        'colMoneda
        '
        Me.colMoneda.HeaderText = "Mon"
        Me.colMoneda.Name = "colMoneda"
        Me.colMoneda.ReadOnly = True
        Me.colMoneda.Width = 35
        '
        'colCotizacion
        '
        Me.colCotizacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colCotizacion.HeaderText = "Cot"
        Me.colCotizacion.Name = "colCotizacion"
        '
        'colTotal
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.NullValue = "0"
        Me.colTotal.DefaultCellStyle = DataGridViewCellStyle4
        Me.colTotal.HeaderText = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.ReadOnly = True
        Me.colTotal.Width = 80
        '
        'colSaldo
        '
        DataGridViewCellStyle5.NullValue = Nothing
        Me.colSaldo.DefaultCellStyle = DataGridViewCellStyle5
        Me.colSaldo.HeaderText = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.Width = 75
        '
        'colImporte
        '
        DataGridViewCellStyle6.NullValue = Nothing
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle6
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.Width = 80
        '
        'colTotalImpuesto
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colTotalImpuesto.DefaultCellStyle = DataGridViewCellStyle7
        Me.colTotalImpuesto.HeaderText = "TotalImpuesto"
        Me.colTotalImpuesto.Name = "colTotalImpuesto"
        Me.colTotalImpuesto.ReadOnly = True
        '
        'colRetencionIVA
        '
        Me.colRetencionIVA.HeaderText = "RetencionIVA"
        Me.colRetencionIVA.Name = "colRetencionIVA"
        '
        'colIDTransaccionEgreso
        '
        Me.colIDTransaccionEgreso.HeaderText = "IDTransaccionEgreso"
        Me.colIDTransaccionEgreso.Name = "colIDTransaccionEgreso"
        '
        'colIDTransaccionOrdenPago
        '
        Me.colIDTransaccionOrdenPago.HeaderText = "IDTransaccionOrdenPago"
        Me.colIDTransaccionOrdenPago.Name = "colIDTransaccionOrdenPago"
        '
        'LinkLabel2
        '
        Me.LinkLabel2.AutoSize = True
        Me.LinkLabel2.Location = New System.Drawing.Point(133, 494)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(88, 13)
        Me.LinkLabel2.TabIndex = 8
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "Ver comprobante"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(67, 494)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(60, 13)
        Me.LinkLabel1.TabIndex = 7
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Ver asiento"
        '
        'lklEliminarEgreso
        '
        Me.lklEliminarEgreso.AutoSize = True
        Me.lklEliminarEgreso.Location = New System.Drawing.Point(18, 494)
        Me.lklEliminarEgreso.Name = "lklEliminarEgreso"
        Me.lklEliminarEgreso.Size = New System.Drawing.Size(43, 13)
        Me.lklEliminarEgreso.TabIndex = 6
        Me.lklEliminarEgreso.TabStop = True
        Me.lklEliminarEgreso.Text = "Eliminar"
        '
        'lblTotalComprobante
        '
        Me.lblTotalComprobante.AutoSize = True
        Me.lblTotalComprobante.Location = New System.Drawing.Point(486, 494)
        Me.lblTotalComprobante.Name = "lblTotalComprobante"
        Me.lblTotalComprobante.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalComprobante.TabIndex = 11
        Me.lblTotalComprobante.Text = "Total:"
        '
        'lblDiferenciaCambio
        '
        Me.lblDiferenciaCambio.AutoSize = True
        Me.lblDiferenciaCambio.Location = New System.Drawing.Point(298, 525)
        Me.lblDiferenciaCambio.Name = "lblDiferenciaCambio"
        Me.lblDiferenciaCambio.Size = New System.Drawing.Size(96, 13)
        Me.lblDiferenciaCambio.TabIndex = 25
        Me.lblDiferenciaCambio.Text = "Diferencia Cambio:"
        Me.lblDiferenciaCambio.Visible = False
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(12, 522)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(239, 20)
        Me.flpRegistradoPor.TabIndex = 21
        Me.flpRegistradoPor.Visible = False
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 3)
        Me.lblRegistradoPor.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 3)
        Me.lblUsuarioRegistro.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 3)
        Me.lblFechaRegistro.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(257, 522)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(257, 20)
        Me.flpAnuladoPor.TabIndex = 22
        Me.flpAnuladoPor.Visible = False
        '
        'lblAnulado
        '
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Red
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(89, 20)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(98, 3)
        Me.lblUsuarioAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(150, 3)
        Me.lblFechaAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'lblSaldoTotal
        '
        Me.lblSaldoTotal.AutoSize = True
        Me.lblSaldoTotal.Location = New System.Drawing.Point(513, 526)
        Me.lblSaldoTotal.Name = "lblSaldoTotal"
        Me.lblSaldoTotal.Size = New System.Drawing.Size(37, 13)
        Me.lblSaldoTotal.TabIndex = 23
        Me.lblSaldoTotal.Text = "Saldo:"
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(204, 559)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 32
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(205, 559)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 30
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        Me.btnModificar.Visible = False
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(284, 559)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 31
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(595, 559)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 33
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(516, 559)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 29
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(439, 559)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 28
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(361, 559)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 27
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(284, 559)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 34
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        Me.btnImprimir.Visible = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 588)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(682, 22)
        Me.StatusStrip1.TabIndex = 36
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        Me.ctrError.Tag = "frmAplicacionAnticipoProveedores"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmiEliminarComprobante, Me.ToolStripSeparator1, Me.VisualizarAsientoToolStripMenuItem, Me.VisualizarElComprobanteToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(211, 76)
        '
        'tsmiEliminarComprobante
        '
        Me.tsmiEliminarComprobante.Name = "tsmiEliminarComprobante"
        Me.tsmiEliminarComprobante.Size = New System.Drawing.Size(210, 22)
        Me.tsmiEliminarComprobante.Text = "Elimininar de la lista"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(207, 6)
        '
        'VisualizarAsientoToolStripMenuItem
        '
        Me.VisualizarAsientoToolStripMenuItem.Name = "VisualizarAsientoToolStripMenuItem"
        Me.VisualizarAsientoToolStripMenuItem.Size = New System.Drawing.Size(210, 22)
        Me.VisualizarAsientoToolStripMenuItem.Text = "Visualizar asiento"
        '
        'VisualizarElComprobanteToolStripMenuItem
        '
        Me.VisualizarElComprobanteToolStripMenuItem.Name = "VisualizarElComprobanteToolStripMenuItem"
        Me.VisualizarElComprobanteToolStripMenuItem.Size = New System.Drawing.Size(210, 22)
        Me.VisualizarElComprobanteToolStripMenuItem.Text = "Visualizar el comprobante"
        '
        'LblTotalOP
        '
        Me.LblTotalOP.AutoSize = True
        Me.LblTotalOP.Location = New System.Drawing.Point(376, 271)
        Me.LblTotalOP.Name = "LblTotalOP"
        Me.LblTotalOP.Size = New System.Drawing.Size(34, 13)
        Me.LblTotalOP.TabIndex = 37
        Me.LblTotalOP.Text = "Total:"
        '
        'LblSaldoOP
        '
        Me.LblSaldoOP.AutoSize = True
        Me.LblSaldoOP.Location = New System.Drawing.Point(529, 271)
        Me.LblSaldoOP.Name = "LblSaldoOP"
        Me.LblSaldoOP.Size = New System.Drawing.Size(37, 13)
        Me.LblSaldoOP.TabIndex = 39
        Me.LblSaldoOP.Text = "Saldo:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(459, 305)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 13)
        Me.Label1.TabIndex = 42
        Me.Label1.Text = "Equivalente:"
        '
        'txtEquivalente
        '
        Me.txtEquivalente.Color = System.Drawing.Color.Empty
        Me.txtEquivalente.Decimales = True
        Me.txtEquivalente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEquivalente.Indicaciones = Nothing
        Me.txtEquivalente.Location = New System.Drawing.Point(532, 301)
        Me.txtEquivalente.Margin = New System.Windows.Forms.Padding(4)
        Me.txtEquivalente.Name = "txtEquivalente"
        Me.txtEquivalente.Size = New System.Drawing.Size(145, 22)
        Me.txtEquivalente.SoloLectura = True
        Me.txtEquivalente.TabIndex = 43
        Me.txtEquivalente.TabStop = False
        Me.txtEquivalente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtEquivalente.Texto = "0"
        '
        'TxtTotalOP
        '
        Me.TxtTotalOP.Color = System.Drawing.Color.Empty
        Me.TxtTotalOP.Decimales = True
        Me.TxtTotalOP.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtTotalOP.Indicaciones = Nothing
        Me.TxtTotalOP.Location = New System.Drawing.Point(408, 267)
        Me.TxtTotalOP.Margin = New System.Windows.Forms.Padding(4)
        Me.TxtTotalOP.Name = "TxtTotalOP"
        Me.TxtTotalOP.Size = New System.Drawing.Size(113, 22)
        Me.TxtTotalOP.SoloLectura = True
        Me.TxtTotalOP.TabIndex = 41
        Me.TxtTotalOP.TabStop = False
        Me.TxtTotalOP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TxtTotalOP.Texto = "0"
        '
        'TxtSaldoOP
        '
        Me.TxtSaldoOP.Color = System.Drawing.Color.Empty
        Me.TxtSaldoOP.Decimales = True
        Me.TxtSaldoOP.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtSaldoOP.Indicaciones = Nothing
        Me.TxtSaldoOP.Location = New System.Drawing.Point(564, 267)
        Me.TxtSaldoOP.Margin = New System.Windows.Forms.Padding(4)
        Me.TxtSaldoOP.Name = "TxtSaldoOP"
        Me.TxtSaldoOP.Size = New System.Drawing.Size(113, 22)
        Me.TxtSaldoOP.SoloLectura = True
        Me.TxtSaldoOP.TabIndex = 40
        Me.TxtSaldoOP.TabStop = False
        Me.TxtSaldoOP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TxtSaldoOP.Texto = "0"
        '
        'txtDiferenciaCambio
        '
        Me.txtDiferenciaCambio.Color = System.Drawing.Color.Empty
        Me.txtDiferenciaCambio.Decimales = True
        Me.txtDiferenciaCambio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiferenciaCambio.Indicaciones = Nothing
        Me.txtDiferenciaCambio.Location = New System.Drawing.Point(397, 521)
        Me.txtDiferenciaCambio.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDiferenciaCambio.Name = "txtDiferenciaCambio"
        Me.txtDiferenciaCambio.Size = New System.Drawing.Size(113, 22)
        Me.txtDiferenciaCambio.SoloLectura = True
        Me.txtDiferenciaCambio.TabIndex = 26
        Me.txtDiferenciaCambio.TabStop = False
        Me.txtDiferenciaCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDiferenciaCambio.Texto = "0"
        Me.txtDiferenciaCambio.Visible = False
        '
        'txtSaldoTotal
        '
        Me.txtSaldoTotal.Color = System.Drawing.Color.Empty
        Me.txtSaldoTotal.Decimales = True
        Me.txtSaldoTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldoTotal.Indicaciones = Nothing
        Me.txtSaldoTotal.Location = New System.Drawing.Point(556, 521)
        Me.txtSaldoTotal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSaldoTotal.Name = "txtSaldoTotal"
        Me.txtSaldoTotal.Size = New System.Drawing.Size(113, 22)
        Me.txtSaldoTotal.SoloLectura = True
        Me.txtSaldoTotal.TabIndex = 24
        Me.txtSaldoTotal.TabStop = False
        Me.txtSaldoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoTotal.Texto = "0"
        '
        'txtCantidadComprobante
        '
        Me.txtCantidadComprobante.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobante.Decimales = True
        Me.txtCantidadComprobante.Indicaciones = Nothing
        Me.txtCantidadComprobante.Location = New System.Drawing.Point(625, 489)
        Me.txtCantidadComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCantidadComprobante.Name = "txtCantidadComprobante"
        Me.txtCantidadComprobante.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadComprobante.SoloLectura = True
        Me.txtCantidadComprobante.TabIndex = 13
        Me.txtCantidadComprobante.TabStop = False
        Me.txtCantidadComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobante.Texto = "0"
        '
        'txtTotalComprobante
        '
        Me.txtTotalComprobante.Color = System.Drawing.Color.Empty
        Me.txtTotalComprobante.Decimales = True
        Me.txtTotalComprobante.Indicaciones = Nothing
        Me.txtTotalComprobante.Location = New System.Drawing.Point(521, 489)
        Me.txtTotalComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalComprobante.Name = "txtTotalComprobante"
        Me.txtTotalComprobante.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalComprobante.SoloLectura = True
        Me.txtTotalComprobante.TabIndex = 12
        Me.txtTotalComprobante.TabStop = False
        Me.txtTotalComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalComprobante.Texto = "0"
        '
        'txtImporteMoneda
        '
        Me.txtImporteMoneda.Color = System.Drawing.Color.Empty
        Me.txtImporteMoneda.Decimales = True
        Me.txtImporteMoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteMoneda.Indicaciones = Nothing
        Me.txtImporteMoneda.Location = New System.Drawing.Point(284, 69)
        Me.txtImporteMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.txtImporteMoneda.Name = "txtImporteMoneda"
        Me.txtImporteMoneda.Size = New System.Drawing.Size(113, 22)
        Me.txtImporteMoneda.SoloLectura = True
        Me.txtImporteMoneda.TabIndex = 42
        Me.txtImporteMoneda.TabStop = False
        Me.txtImporteMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteMoneda.Texto = "0"
        '
        'txtVencimiento
        '
        Me.txtVencimiento.AñoFecha = 0
        Me.txtVencimiento.Color = System.Drawing.Color.Empty
        Me.txtVencimiento.Enabled = False
        Me.txtVencimiento.Fecha = New Date(2013, 5, 30, 8, 29, 32, 578)
        Me.txtVencimiento.Location = New System.Drawing.Point(567, 43)
        Me.txtVencimiento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtVencimiento.MesFecha = 0
        Me.txtVencimiento.Name = "txtVencimiento"
        Me.txtVencimiento.PermitirNulo = False
        Me.txtVencimiento.Size = New System.Drawing.Size(87, 20)
        Me.txtVencimiento.SoloLectura = False
        Me.txtVencimiento.TabIndex = 13
        '
        'txtOrden
        '
        Me.txtOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOrden.Color = System.Drawing.Color.Empty
        Me.txtOrden.Indicaciones = Nothing
        Me.txtOrden.Location = New System.Drawing.Point(108, 96)
        Me.txtOrden.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOrden.Multilinea = False
        Me.txtOrden.Name = "txtOrden"
        Me.txtOrden.Size = New System.Drawing.Size(546, 21)
        Me.txtOrden.SoloLectura = False
        Me.txtOrden.TabIndex = 21
        Me.txtOrden.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtOrden.Texto = ""
        '
        'cbxCuentaBancaria
        '
        Me.cbxCuentaBancaria.CampoWhere = Nothing
        Me.cbxCuentaBancaria.CargarUnaSolaVez = False
        Me.cbxCuentaBancaria.DataDisplayMember = Nothing
        Me.cbxCuentaBancaria.DataFilter = Nothing
        Me.cbxCuentaBancaria.DataOrderBy = Nothing
        Me.cbxCuentaBancaria.DataSource = Nothing
        Me.cbxCuentaBancaria.DataValueMember = Nothing
        Me.cbxCuentaBancaria.dtSeleccionado = Nothing
        Me.cbxCuentaBancaria.FormABM = Nothing
        Me.cbxCuentaBancaria.Indicaciones = Nothing
        Me.cbxCuentaBancaria.Location = New System.Drawing.Point(108, 17)
        Me.cbxCuentaBancaria.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCuentaBancaria.Name = "cbxCuentaBancaria"
        Me.cbxCuentaBancaria.SeleccionMultiple = False
        Me.cbxCuentaBancaria.SeleccionObligatoria = True
        Me.cbxCuentaBancaria.Size = New System.Drawing.Size(138, 21)
        Me.cbxCuentaBancaria.SoloLectura = False
        Me.cbxCuentaBancaria.TabIndex = 1
        Me.cbxCuentaBancaria.Texto = ""
        '
        'txtFechaPagoCheque
        '
        Me.txtFechaPagoCheque.AñoFecha = 0
        Me.txtFechaPagoCheque.Color = System.Drawing.Color.Empty
        Me.txtFechaPagoCheque.Enabled = False
        Me.txtFechaPagoCheque.Fecha = New Date(2013, 5, 30, 8, 29, 32, 578)
        Me.txtFechaPagoCheque.Location = New System.Drawing.Point(268, 43)
        Me.txtFechaPagoCheque.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFechaPagoCheque.MesFecha = 0
        Me.txtFechaPagoCheque.Name = "txtFechaPagoCheque"
        Me.txtFechaPagoCheque.PermitirNulo = False
        Me.txtFechaPagoCheque.Size = New System.Drawing.Size(67, 20)
        Me.txtFechaPagoCheque.SoloLectura = False
        Me.txtFechaPagoCheque.TabIndex = 9
        '
        'txtFechaCheque
        '
        Me.txtFechaCheque.AñoFecha = 0
        Me.txtFechaCheque.Color = System.Drawing.Color.Empty
        Me.txtFechaCheque.Enabled = False
        Me.txtFechaCheque.Fecha = New Date(2013, 5, 30, 8, 29, 32, 594)
        Me.txtFechaCheque.Location = New System.Drawing.Point(108, 43)
        Me.txtFechaCheque.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFechaCheque.MesFecha = 0
        Me.txtFechaCheque.Name = "txtFechaCheque"
        Me.txtFechaCheque.PermitirNulo = False
        Me.txtFechaCheque.Size = New System.Drawing.Size(67, 20)
        Me.txtFechaCheque.SoloLectura = False
        Me.txtFechaCheque.TabIndex = 7
        '
        'txtMoneda
        '
        Me.txtMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMoneda.Color = System.Drawing.Color.Empty
        Me.txtMoneda.Indicaciones = Nothing
        Me.txtMoneda.Location = New System.Drawing.Point(353, 17)
        Me.txtMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.txtMoneda.Multilinea = False
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.Size = New System.Drawing.Size(62, 21)
        Me.txtMoneda.SoloLectura = True
        Me.txtMoneda.TabIndex = 3
        Me.txtMoneda.TabStop = False
        Me.txtMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMoneda.Texto = ""
        '
        'txtBanco
        '
        Me.txtBanco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBanco.Color = System.Drawing.Color.Empty
        Me.txtBanco.Indicaciones = Nothing
        Me.txtBanco.Location = New System.Drawing.Point(246, 17)
        Me.txtBanco.Margin = New System.Windows.Forms.Padding(4)
        Me.txtBanco.Multilinea = False
        Me.txtBanco.Name = "txtBanco"
        Me.txtBanco.Size = New System.Drawing.Size(107, 21)
        Me.txtBanco.SoloLectura = True
        Me.txtBanco.TabIndex = 2
        Me.txtBanco.TabStop = False
        Me.txtBanco.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBanco.Texto = ""
        '
        'txtNroCheque
        '
        Me.txtNroCheque.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroCheque.Color = System.Drawing.Color.Empty
        Me.txtNroCheque.Enabled = False
        Me.txtNroCheque.Indicaciones = Nothing
        Me.txtNroCheque.Location = New System.Drawing.Point(502, 17)
        Me.txtNroCheque.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNroCheque.Multilinea = False
        Me.txtNroCheque.Name = "txtNroCheque"
        Me.txtNroCheque.Size = New System.Drawing.Size(152, 21)
        Me.txtNroCheque.SoloLectura = False
        Me.txtNroCheque.TabIndex = 5
        Me.txtNroCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroCheque.Texto = ""
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = False
        Me.txtImporte.Enabled = False
        Me.txtImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporte.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporte.Location = New System.Drawing.Point(487, 68)
        Me.txtImporte.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(167, 21)
        Me.txtImporte.SoloLectura = False
        Me.txtImporte.TabIndex = 19
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(108, 68)
        Me.txtCotizacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(67, 21)
        Me.txtCotizacion.SoloLectura = False
        Me.txtCotizacion.TabIndex = 15
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "1"
        '
        'OcxOrdenPagoDocumento1
        '
        Me.OcxOrdenPagoDocumento1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.OcxOrdenPagoDocumento1.Comprobante = Nothing
        Me.OcxOrdenPagoDocumento1.Decimales = 0
        Me.OcxOrdenPagoDocumento1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxOrdenPagoDocumento1.dtDocumento = Nothing
        Me.OcxOrdenPagoDocumento1.Fecha = New Date(CType(0, Long))
        Me.OcxOrdenPagoDocumento1.Form = Nothing
        Me.OcxOrdenPagoDocumento1.IDMoneda = 0
        Me.OcxOrdenPagoDocumento1.Location = New System.Drawing.Point(2, 2)
        Me.OcxOrdenPagoDocumento1.Name = "OcxOrdenPagoDocumento1"
        Me.OcxOrdenPagoDocumento1.Saldo = New Decimal(New Integer() {0, 0, 0, 0})
        Me.OcxOrdenPagoDocumento1.Size = New System.Drawing.Size(658, 122)
        Me.OcxOrdenPagoDocumento1.SolocomprobanteCliente = False
        Me.OcxOrdenPagoDocumento1.SoloComprobanteProveedor = False
        Me.OcxOrdenPagoDocumento1.SoloLectura = False
        Me.OcxOrdenPagoDocumento1.TabIndex = 0
        Me.OcxOrdenPagoDocumento1.Tipo = Nothing
        Me.OcxOrdenPagoDocumento1.Total = New Decimal(New Integer() {0, 0, 0, 0})
        Me.OcxOrdenPagoDocumento1.TotalPago = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'txtNroOrdenPago
        '
        Me.txtNroOrdenPago.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroOrdenPago.Color = System.Drawing.Color.Empty
        Me.txtNroOrdenPago.Indicaciones = Nothing
        Me.txtNroOrdenPago.Location = New System.Drawing.Point(583, 12)
        Me.txtNroOrdenPago.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNroOrdenPago.Multilinea = False
        Me.txtNroOrdenPago.Name = "txtNroOrdenPago"
        Me.txtNroOrdenPago.Size = New System.Drawing.Size(76, 21)
        Me.txtNroOrdenPago.SoloLectura = False
        Me.txtNroOrdenPago.TabIndex = 9
        Me.txtNroOrdenPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroOrdenPago.Texto = ""
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = True
        Me.cbxMoneda.DataDisplayMember = "Referencia"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(76, 38)
        Me.cbxMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(63, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 11
        Me.cbxMoneda.Texto = ""
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 100
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(210, 35)
        Me.txtProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(449, 27)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 13
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(210, 64)
        Me.txtObservacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(450, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 17
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(242, 12)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(94, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 5, 30, 8, 29, 32, 812)
        Me.txtFecha.Location = New System.Drawing.Point(76, 64)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(69, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 15
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(373, 12)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(63, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 6
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(438, 12)
        Me.txtComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(59, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 7
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(76, 12)
        Me.cbxCiudad.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(63, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Introduzca el código y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(139, 12)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(56, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'frmAplicacionAnticipoProveedores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(682, 610)
        Me.Controls.Add(Me.txtEquivalente)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TxtTotalOP)
        Me.Controls.Add(Me.TxtSaldoOP)
        Me.Controls.Add(Me.LblSaldoOP)
        Me.Controls.Add(Me.LblTotalOP)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.txtDiferenciaCambio)
        Me.Controls.Add(Me.lblDiferenciaCambio)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.flpAnuladoPor)
        Me.Controls.Add(Me.txtSaldoTotal)
        Me.Controls.Add(Me.lblSaldoTotal)
        Me.Controls.Add(Me.txtCantidadComprobante)
        Me.Controls.Add(Me.txtTotalComprobante)
        Me.Controls.Add(Me.lblTotalComprobante)
        Me.Controls.Add(Me.LinkLabel2)
        Me.Controls.Add(Me.LinkLabel1)
        Me.Controls.Add(Me.lklEliminarEgreso)
        Me.Controls.Add(Me.dgw)
        Me.Controls.Add(Me.btnAgregarComprobantes)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Name = "frmAplicacionAnticipoProveedores"
        Me.Tag = "frmAplicacionAnticipoProveedores"
        Me.Text = "frmAplicacionAnticipoProveedores"
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.tpCheque.ResumeLayout(False)
        Me.tpCheque.PerformLayout()
        Me.tpDocumento.ResumeLayout(False)
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents gbxCabecera As GroupBox
    Friend WithEvents cbxMoneda As ocxCBX
    Friend WithEvents lblMoneda As Label
    Friend WithEvents txtProveedor As ocxTXTProveedor
    Friend WithEvents txtObservacion As ocxTXTString
    Friend WithEvents cbxSucursal As ocxCBX
    Friend WithEvents txtFecha As ocxTXTDate
    Friend WithEvents lblFecha As Label
    Friend WithEvents cbxTipoComprobante As ocxCBX
    Friend WithEvents txtComprobante As ocxTXTString
    Friend WithEvents lblProveedor As Label
    Friend WithEvents cbxCiudad As ocxCBX
    Friend WithEvents txtID As ocxTXTNumeric
    Friend WithEvents lblObservacion As Label
    Friend WithEvents lblSucursal As Label
    Friend WithEvents lblComprobante As Label
    Friend WithEvents lblOperacion As Label
    Friend WithEvents lblOP As Label
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents tpCheque As TabPage
    Friend WithEvents lblImporte As Label
    Friend WithEvents txtVencimiento As ocxTXTDate
    Friend WithEvents txtOrden As ocxTXTString
    Friend WithEvents lblCuentaBancaria As Label
    Friend WithEvents lblOrden As Label
    Friend WithEvents cbxCuentaBancaria As ocxCBX
    Friend WithEvents txtFechaPagoCheque As ocxTXTDate
    Friend WithEvents lblFechaCheque As Label
    Friend WithEvents lblFechaPagoCheque As Label
    Friend WithEvents txtFechaCheque As ocxTXTDate
    Friend WithEvents txtMoneda As ocxTXTString
    Friend WithEvents lblNroCheque As Label
    Friend WithEvents txtBanco As ocxTXTString
    Friend WithEvents txtNroCheque As ocxTXTString
    Friend WithEvents txtImporte As ocxTXTNumeric
    Friend WithEvents txtCotizacion As ocxTXTNumeric
    Friend WithEvents lblCotizacionCheque As Label
    Friend WithEvents lblImporteMoneda As Label
    Friend WithEvents lblDiferido As Label
    Friend WithEvents chkDiferido As CheckBox
    Friend WithEvents lblVencimiento As Label
    Friend WithEvents tpDocumento As TabPage
    Friend WithEvents OcxOrdenPagoDocumento1 As ocxOrdenPagoDocumento
    Friend WithEvents btnAgregarComprobantes As Button
    Friend WithEvents dgw As DataGridView
    Friend WithEvents LinkLabel2 As LinkLabel
    Friend WithEvents LinkLabel1 As LinkLabel
    Friend WithEvents lklEliminarEgreso As LinkLabel
    Friend WithEvents txtCantidadComprobante As ocxTXTNumeric
    Friend WithEvents txtTotalComprobante As ocxTXTNumeric
    Friend WithEvents lblTotalComprobante As Label
    Friend WithEvents txtDiferenciaCambio As ocxTXTNumeric
    Friend WithEvents lblDiferenciaCambio As Label
    Friend WithEvents flpRegistradoPor As FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As Label
    Friend WithEvents lblUsuarioRegistro As Label
    Friend WithEvents lblFechaRegistro As Label
    Friend WithEvents flpAnuladoPor As FlowLayoutPanel
    Friend WithEvents lblAnulado As Label
    Friend WithEvents lblUsuarioAnulado As Label
    Friend WithEvents lblFechaAnulado As Label
    Friend WithEvents txtSaldoTotal As ocxTXTNumeric
    Friend WithEvents lblSaldoTotal As Label
    Friend WithEvents btnAnular As Button
    Friend WithEvents btnModificar As Button
    Friend WithEvents btnAsiento As Button
    Friend WithEvents btnSalir As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnNuevo As Button
    Friend WithEvents btnImprimir As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents tsmiEliminarComprobante As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents VisualizarAsientoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents VisualizarElComprobanteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TxtTotalOP As ocxTXTNumeric
    Friend WithEvents TxtSaldoOP As ocxTXTNumeric
    Friend WithEvents LblSaldoOP As Label
    Friend WithEvents LblTotalOP As Label
    Friend WithEvents txtNroOrdenPago As ocxTXTString
    Friend WithEvents txtImporteMoneda As ocxTXTNumeric
    Friend WithEvents colIDTransaccion As DataGridViewTextBoxColumn
    Friend WithEvents colBanco As DataGridViewTextBoxColumn
    Friend WithEvents colFecha As DataGridViewTextBoxColumn
    Friend WithEvents colProveedor As DataGridViewTextBoxColumn
    Friend WithEvents colMoneda As DataGridViewTextBoxColumn
    Friend WithEvents colCotizacion As DataGridViewTextBoxColumn
    Friend WithEvents colTotal As DataGridViewTextBoxColumn
    Friend WithEvents colSaldo As DataGridViewTextBoxColumn
    Friend WithEvents colImporte As DataGridViewTextBoxColumn
    Friend WithEvents colTotalImpuesto As DataGridViewTextBoxColumn
    Friend WithEvents colRetencionIVA As DataGridViewTextBoxColumn
    Friend WithEvents colIDTransaccionEgreso As DataGridViewTextBoxColumn
    Friend WithEvents colIDTransaccionOrdenPago As DataGridViewTextBoxColumn
    Friend WithEvents txtEquivalente As ocxTXTNumeric
    Friend WithEvents Label1 As Label
End Class

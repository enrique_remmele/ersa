﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCobranzaValidar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ExportarAPlanillaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.StatusStrip2 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.dgvCabecera = New System.Windows.Forms.DataGridView()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.rdbFechaRegistro = New System.Windows.Forms.RadioButton()
        Me.rdbFechaDocumento = New System.Windows.Forms.RadioButton()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.FlowLayoutPanel7 = New System.Windows.Forms.FlowLayoutPanel()
        Me.rdbFechaRegistro2 = New System.Windows.Forms.RadioButton()
        Me.rdbFechaDocumento2 = New System.Windows.Forms.RadioButton()
        Me.txtDesde2 = New System.Windows.Forms.DateTimePicker()
        Me.txtHasta2 = New System.Windows.Forms.DateTimePicker()
        Me.chkFecha2 = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtNumeroOperacion = New ERP.ocxTXTString()
        Me.btnExportar = New System.Windows.Forms.Button()
        Me.lblNumero = New System.Windows.Forms.Label()
        Me.cbxSucursalOperacion = New ERP.ocxCBX()
        Me.chkSucursal = New System.Windows.Forms.CheckBox()
        Me.txtComprobanteOperacion = New ERP.ocxTXTString()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.cbxOperacion = New ERP.ocxCBX()
        Me.chkOperacion = New System.Windows.Forms.CheckBox()
        Me.chkSinConciliar = New System.Windows.Forms.CheckBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.lblSaldo = New System.Windows.Forms.Label()
        Me.txtTotalCredito = New ERP.ocxTXTNumeric()
        Me.lblTotalCredito = New System.Windows.Forms.Label()
        Me.txtTotalDebito = New ERP.ocxTXTNumeric()
        Me.lblTotalDebito = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtCantidadDetalle = New ERP.ocxTXTNumeric()
        Me.lblCantidadDetalle = New System.Windows.Forms.Label()
        Me.dgvDetalleAsiento = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.dgvFactura = New System.Windows.Forms.DataGridView()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.StatusStrip2.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.FlowLayoutPanel7.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.dgvDetalleAsiento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.dgvFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 583)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1254, 22)
        Me.StatusStrip1.TabIndex = 6
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExportarAPlanillaToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(169, 26)
        '
        'ExportarAPlanillaToolStripMenuItem
        '
        Me.ExportarAPlanillaToolStripMenuItem.Name = "ExportarAPlanillaToolStripMenuItem"
        Me.ExportarAPlanillaToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ExportarAPlanillaToolStripMenuItem.Text = "Exportar a Planilla"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 244.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.StatusStrip2, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TabControl1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel3, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 146.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 168.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1254, 583)
        Me.TableLayoutPanel1.TabIndex = 7
        '
        'StatusStrip2
        '
        Me.StatusStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStrip2.Location = New System.Drawing.Point(0, 563)
        Me.StatusStrip2.Name = "StatusStrip2"
        Me.StatusStrip2.Size = New System.Drawing.Size(1010, 20)
        Me.StatusStrip2.TabIndex = 5
        Me.StatusStrip2.Text = "StatusStrip2"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(0, 15)
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.dgvCabecera)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1004, 243)
        Me.Panel2.TabIndex = 1
        '
        'dgvCabecera
        '
        Me.dgvCabecera.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCabecera.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvCabecera.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvCabecera.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvCabecera.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvCabecera.Location = New System.Drawing.Point(0, 0)
        Me.dgvCabecera.Name = "dgvCabecera"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCabecera.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvCabecera.Size = New System.Drawing.Size(1004, 243)
        Me.dgvCabecera.TabIndex = 5
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.chkSinConciliar)
        Me.TabPage1.Controls.Add(Me.chkOperacion)
        Me.TabPage1.Controls.Add(Me.cbxOperacion)
        Me.TabPage1.Controls.Add(Me.lblComprobante)
        Me.TabPage1.Controls.Add(Me.txtComprobanteOperacion)
        Me.TabPage1.Controls.Add(Me.chkSucursal)
        Me.TabPage1.Controls.Add(Me.cbxSucursalOperacion)
        Me.TabPage1.Controls.Add(Me.lblNumero)
        Me.TabPage1.Controls.Add(Me.btnExportar)
        Me.TabPage1.Controls.Add(Me.txtNumeroOperacion)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.chkFecha2)
        Me.TabPage1.Controls.Add(Me.txtHasta2)
        Me.TabPage1.Controls.Add(Me.txtDesde2)
        Me.TabPage1.Controls.Add(Me.FlowLayoutPanel7)
        Me.TabPage1.Controls.Add(Me.dtpHasta)
        Me.TabPage1.Controls.Add(Me.dtpDesde)
        Me.TabPage1.Controls.Add(Me.FlowLayoutPanel5)
        Me.TabPage1.Controls.Add(Me.lblFecha)
        Me.TabPage1.Controls.Add(Me.btnListar)
        Me.TabPage1.Location = New System.Drawing.Point(4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(230, 551)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(7, 326)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(112, 30)
        Me.btnListar.TabIndex = 4
        Me.btnListar.Text = "Listar"
        Me.btnListar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(6, 8)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(55, 13)
        Me.lblFecha.TabIndex = 0
        Me.lblFecha.Text = "Fecha de:"
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FlowLayoutPanel5.Controls.Add(Me.rdbFechaDocumento)
        Me.FlowLayoutPanel5.Controls.Add(Me.rdbFechaRegistro)
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(61, 5)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(117, 20)
        Me.FlowLayoutPanel5.TabIndex = 1
        '
        'rdbFechaRegistro
        '
        Me.rdbFechaRegistro.AutoSize = True
        Me.rdbFechaRegistro.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbFechaRegistro.Location = New System.Drawing.Point(62, 1)
        Me.rdbFechaRegistro.Margin = New System.Windows.Forms.Padding(1, 1, 3, 3)
        Me.rdbFechaRegistro.Name = "rdbFechaRegistro"
        Me.rdbFechaRegistro.Size = New System.Drawing.Size(43, 16)
        Me.rdbFechaRegistro.TabIndex = 1
        Me.rdbFechaRegistro.Text = "Reg."
        Me.rdbFechaRegistro.UseVisualStyleBackColor = True
        '
        'rdbFechaDocumento
        '
        Me.rdbFechaDocumento.AutoSize = True
        Me.rdbFechaDocumento.Checked = True
        Me.rdbFechaDocumento.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbFechaDocumento.Location = New System.Drawing.Point(2, 1)
        Me.rdbFechaDocumento.Margin = New System.Windows.Forms.Padding(2, 1, 3, 3)
        Me.rdbFechaDocumento.Name = "rdbFechaDocumento"
        Me.rdbFechaDocumento.Size = New System.Drawing.Size(56, 16)
        Me.rdbFechaDocumento.TabIndex = 0
        Me.rdbFechaDocumento.TabStop = True
        Me.rdbFechaDocumento.Text = "Docum."
        Me.rdbFechaDocumento.UseVisualStyleBackColor = True
        '
        'dtpDesde
        '
        Me.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDesde.Location = New System.Drawing.Point(120, 30)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(105, 20)
        Me.dtpDesde.TabIndex = 2
        '
        'dtpHasta
        '
        Me.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHasta.Location = New System.Drawing.Point(120, 53)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(105, 20)
        Me.dtpHasta.TabIndex = 3
        '
        'FlowLayoutPanel7
        '
        Me.FlowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FlowLayoutPanel7.Controls.Add(Me.rdbFechaDocumento2)
        Me.FlowLayoutPanel7.Controls.Add(Me.rdbFechaRegistro2)
        Me.FlowLayoutPanel7.Location = New System.Drawing.Point(60, 82)
        Me.FlowLayoutPanel7.Name = "FlowLayoutPanel7"
        Me.FlowLayoutPanel7.Size = New System.Drawing.Size(117, 20)
        Me.FlowLayoutPanel7.TabIndex = 25
        '
        'rdbFechaRegistro2
        '
        Me.rdbFechaRegistro2.AutoSize = True
        Me.rdbFechaRegistro2.Enabled = False
        Me.rdbFechaRegistro2.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbFechaRegistro2.Location = New System.Drawing.Point(62, 1)
        Me.rdbFechaRegistro2.Margin = New System.Windows.Forms.Padding(1, 1, 3, 3)
        Me.rdbFechaRegistro2.Name = "rdbFechaRegistro2"
        Me.rdbFechaRegistro2.Size = New System.Drawing.Size(43, 16)
        Me.rdbFechaRegistro2.TabIndex = 1
        Me.rdbFechaRegistro2.Text = "Reg."
        Me.rdbFechaRegistro2.UseVisualStyleBackColor = True
        '
        'rdbFechaDocumento2
        '
        Me.rdbFechaDocumento2.AutoSize = True
        Me.rdbFechaDocumento2.Checked = True
        Me.rdbFechaDocumento2.Enabled = False
        Me.rdbFechaDocumento2.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbFechaDocumento2.Location = New System.Drawing.Point(2, 1)
        Me.rdbFechaDocumento2.Margin = New System.Windows.Forms.Padding(2, 1, 3, 3)
        Me.rdbFechaDocumento2.Name = "rdbFechaDocumento2"
        Me.rdbFechaDocumento2.Size = New System.Drawing.Size(56, 16)
        Me.rdbFechaDocumento2.TabIndex = 0
        Me.rdbFechaDocumento2.TabStop = True
        Me.rdbFechaDocumento2.Text = "Docum."
        Me.rdbFechaDocumento2.UseVisualStyleBackColor = True
        '
        'txtDesde2
        '
        Me.txtDesde2.Enabled = False
        Me.txtDesde2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtDesde2.Location = New System.Drawing.Point(121, 107)
        Me.txtDesde2.Name = "txtDesde2"
        Me.txtDesde2.Size = New System.Drawing.Size(103, 20)
        Me.txtDesde2.TabIndex = 26
        '
        'txtHasta2
        '
        Me.txtHasta2.Enabled = False
        Me.txtHasta2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtHasta2.Location = New System.Drawing.Point(121, 128)
        Me.txtHasta2.Name = "txtHasta2"
        Me.txtHasta2.Size = New System.Drawing.Size(103, 20)
        Me.txtHasta2.TabIndex = 27
        '
        'chkFecha2
        '
        Me.chkFecha2.AutoSize = True
        Me.chkFecha2.Location = New System.Drawing.Point(4, 84)
        Me.chkFecha2.Name = "chkFecha2"
        Me.chkFecha2.Size = New System.Drawing.Size(56, 17)
        Me.chkFecha2.TabIndex = 28
        Me.chkFecha2.Text = "2 Fec:"
        Me.chkFecha2.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(61, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Desde:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(61, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Hasta:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(58, 107)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 13)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "Desde:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(58, 128)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 32
        Me.Label3.Text = "Hasta:"
        '
        'txtNumeroOperacion
        '
        Me.txtNumeroOperacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumeroOperacion.Color = System.Drawing.Color.Empty
        Me.txtNumeroOperacion.Enabled = False
        Me.txtNumeroOperacion.Indicaciones = Nothing
        Me.txtNumeroOperacion.Location = New System.Drawing.Point(181, 208)
        Me.txtNumeroOperacion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 3)
        Me.txtNumeroOperacion.Multilinea = False
        Me.txtNumeroOperacion.Name = "txtNumeroOperacion"
        Me.txtNumeroOperacion.Size = New System.Drawing.Size(43, 20)
        Me.txtNumeroOperacion.SoloLectura = False
        Me.txtNumeroOperacion.TabIndex = 5
        Me.txtNumeroOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNumeroOperacion.Texto = ""
        '
        'btnExportar
        '
        Me.btnExportar.Location = New System.Drawing.Point(125, 326)
        Me.btnExportar.Name = "btnExportar"
        Me.btnExportar.Size = New System.Drawing.Size(100, 30)
        Me.btnExportar.TabIndex = 33
        Me.btnExportar.Text = "Exportar"
        Me.btnExportar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExportar.UseVisualStyleBackColor = True
        '
        'lblNumero
        '
        Me.lblNumero.AutoSize = True
        Me.lblNumero.Location = New System.Drawing.Point(122, 210)
        Me.lblNumero.Margin = New System.Windows.Forms.Padding(0, 6, 0, 0)
        Me.lblNumero.Name = "lblNumero"
        Me.lblNumero.Size = New System.Drawing.Size(56, 13)
        Me.lblNumero.TabIndex = 4
        Me.lblNumero.Text = "Nro. Ope.:"
        Me.lblNumero.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbxSucursalOperacion
        '
        Me.cbxSucursalOperacion.CampoWhere = "IDSucursal"
        Me.cbxSucursalOperacion.CargarUnaSolaVez = False
        Me.cbxSucursalOperacion.DataDisplayMember = "Codigo"
        Me.cbxSucursalOperacion.DataFilter = Nothing
        Me.cbxSucursalOperacion.DataOrderBy = Nothing
        Me.cbxSucursalOperacion.DataSource = "Sucursal"
        Me.cbxSucursalOperacion.DataValueMember = "ID"
        Me.cbxSucursalOperacion.dtSeleccionado = Nothing
        Me.cbxSucursalOperacion.Enabled = False
        Me.cbxSucursalOperacion.FormABM = Nothing
        Me.cbxSucursalOperacion.Indicaciones = Nothing
        Me.cbxSucursalOperacion.Location = New System.Drawing.Point(60, 208)
        Me.cbxSucursalOperacion.Name = "cbxSucursalOperacion"
        Me.cbxSucursalOperacion.SeleccionMultiple = False
        Me.cbxSucursalOperacion.SeleccionObligatoria = False
        Me.cbxSucursalOperacion.Size = New System.Drawing.Size(54, 23)
        Me.cbxSucursalOperacion.SoloLectura = False
        Me.cbxSucursalOperacion.TabIndex = 35
        Me.cbxSucursalOperacion.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.AutoSize = True
        Me.chkSucursal.Location = New System.Drawing.Point(3, 211)
        Me.chkSucursal.Margin = New System.Windows.Forms.Padding(3, 6, 3, 3)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(51, 17)
        Me.chkSucursal.TabIndex = 34
        Me.chkSucursal.Text = "Suc.:"
        Me.chkSucursal.UseVisualStyleBackColor = True
        '
        'txtComprobanteOperacion
        '
        Me.txtComprobanteOperacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobanteOperacion.Color = System.Drawing.Color.Empty
        Me.txtComprobanteOperacion.Enabled = False
        Me.txtComprobanteOperacion.Indicaciones = Nothing
        Me.txtComprobanteOperacion.Location = New System.Drawing.Point(84, 235)
        Me.txtComprobanteOperacion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 3)
        Me.txtComprobanteOperacion.Multilinea = False
        Me.txtComprobanteOperacion.Name = "txtComprobanteOperacion"
        Me.txtComprobanteOperacion.Size = New System.Drawing.Size(140, 20)
        Me.txtComprobanteOperacion.SoloLectura = False
        Me.txtComprobanteOperacion.TabIndex = 37
        Me.txtComprobanteOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobanteOperacion.Texto = ""
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(8, 240)
        Me.lblComprobante.Margin = New System.Windows.Forms.Padding(0, 6, 0, 0)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 36
        Me.lblComprobante.Text = "Comprobante:"
        Me.lblComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbxOperacion
        '
        Me.cbxOperacion.CampoWhere = "Tipo"
        Me.cbxOperacion.CargarUnaSolaVez = False
        Me.cbxOperacion.DataDisplayMember = "Tipo"
        Me.cbxOperacion.DataFilter = Nothing
        Me.cbxOperacion.DataOrderBy = Nothing
        Me.cbxOperacion.DataSource = ""
        Me.cbxOperacion.DataValueMember = "Tipo"
        Me.cbxOperacion.dtSeleccionado = Nothing
        Me.cbxOperacion.FormABM = Nothing
        Me.cbxOperacion.Indicaciones = Nothing
        Me.cbxOperacion.Location = New System.Drawing.Point(9, 179)
        Me.cbxOperacion.Name = "cbxOperacion"
        Me.cbxOperacion.SeleccionMultiple = False
        Me.cbxOperacion.SeleccionObligatoria = False
        Me.cbxOperacion.Size = New System.Drawing.Size(218, 23)
        Me.cbxOperacion.SoloLectura = False
        Me.cbxOperacion.TabIndex = 39
        Me.cbxOperacion.Texto = ""
        '
        'chkOperacion
        '
        Me.chkOperacion.AutoSize = True
        Me.chkOperacion.Checked = True
        Me.chkOperacion.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkOperacion.Enabled = False
        Me.chkOperacion.Location = New System.Drawing.Point(3, 156)
        Me.chkOperacion.Margin = New System.Windows.Forms.Padding(3, 6, 3, 3)
        Me.chkOperacion.Name = "chkOperacion"
        Me.chkOperacion.Size = New System.Drawing.Size(78, 17)
        Me.chkOperacion.TabIndex = 38
        Me.chkOperacion.Text = "Operacion:"
        Me.chkOperacion.UseVisualStyleBackColor = True
        '
        'chkSinConciliar
        '
        Me.chkSinConciliar.AutoSize = True
        Me.chkSinConciliar.Location = New System.Drawing.Point(4, 294)
        Me.chkSinConciliar.Margin = New System.Windows.Forms.Padding(3, 6, 3, 3)
        Me.chkSinConciliar.Name = "chkSinConciliar"
        Me.chkSinConciliar.Size = New System.Drawing.Size(87, 17)
        Me.chkSinConciliar.TabIndex = 40
        Me.chkSinConciliar.Text = "Sin Conciliar:"
        Me.chkSinConciliar.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(1013, 3)
        Me.TabControl1.Name = "TabControl1"
        Me.TableLayoutPanel1.SetRowSpan(Me.TabControl1, 4)
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(238, 577)
        Me.TabControl1.TabIndex = 4
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 286.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.FlowLayoutPanel2, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.FlowLayoutPanel6, 1, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(0, 131)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(1004, 31)
        Me.TableLayoutPanel4.TabIndex = 3
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.Controls.Add(Me.lblTotalDebito)
        Me.FlowLayoutPanel6.Controls.Add(Me.txtTotalDebito)
        Me.FlowLayoutPanel6.Controls.Add(Me.lblTotalCredito)
        Me.FlowLayoutPanel6.Controls.Add(Me.txtTotalCredito)
        Me.FlowLayoutPanel6.Controls.Add(Me.lblSaldo)
        Me.FlowLayoutPanel6.Controls.Add(Me.txtSaldo)
        Me.FlowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(289, 3)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(712, 25)
        Me.FlowLayoutPanel6.TabIndex = 0
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(387, 3)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(95, 22)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 8
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'lblSaldo
        '
        Me.lblSaldo.Location = New System.Drawing.Point(324, 0)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(57, 25)
        Me.lblSaldo.TabIndex = 7
        Me.lblSaldo.Text = "Saldo:"
        Me.lblSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTotalCredito
        '
        Me.txtTotalCredito.Color = System.Drawing.Color.Empty
        Me.txtTotalCredito.Decimales = True
        Me.txtTotalCredito.Indicaciones = Nothing
        Me.txtTotalCredito.Location = New System.Drawing.Point(223, 3)
        Me.txtTotalCredito.Name = "txtTotalCredito"
        Me.txtTotalCredito.Size = New System.Drawing.Size(95, 22)
        Me.txtTotalCredito.SoloLectura = True
        Me.txtTotalCredito.TabIndex = 6
        Me.txtTotalCredito.TabStop = False
        Me.txtTotalCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCredito.Texto = "0"
        '
        'lblTotalCredito
        '
        Me.lblTotalCredito.Location = New System.Drawing.Point(160, 0)
        Me.lblTotalCredito.Name = "lblTotalCredito"
        Me.lblTotalCredito.Size = New System.Drawing.Size(57, 25)
        Me.lblTotalCredito.TabIndex = 5
        Me.lblTotalCredito.Text = "Credito:"
        Me.lblTotalCredito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTotalDebito
        '
        Me.txtTotalDebito.Color = System.Drawing.Color.Empty
        Me.txtTotalDebito.Decimales = True
        Me.txtTotalDebito.Indicaciones = Nothing
        Me.txtTotalDebito.Location = New System.Drawing.Point(59, 3)
        Me.txtTotalDebito.Name = "txtTotalDebito"
        Me.txtTotalDebito.Size = New System.Drawing.Size(95, 22)
        Me.txtTotalDebito.SoloLectura = True
        Me.txtTotalDebito.TabIndex = 4
        Me.txtTotalDebito.TabStop = False
        Me.txtTotalDebito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDebito.Texto = "0"
        '
        'lblTotalDebito
        '
        Me.lblTotalDebito.Location = New System.Drawing.Point(3, 0)
        Me.lblTotalDebito.Name = "lblTotalDebito"
        Me.lblTotalDebito.Size = New System.Drawing.Size(50, 25)
        Me.lblTotalDebito.TabIndex = 0
        Me.lblTotalDebito.Text = "Debito:"
        Me.lblTotalDebito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.lblCantidadDetalle)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtCantidadDetalle)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(280, 25)
        Me.FlowLayoutPanel2.TabIndex = 1
        '
        'txtCantidadDetalle
        '
        Me.txtCantidadDetalle.Color = System.Drawing.Color.Empty
        Me.txtCantidadDetalle.Decimales = True
        Me.txtCantidadDetalle.Indicaciones = Nothing
        Me.txtCantidadDetalle.Location = New System.Drawing.Point(61, 3)
        Me.txtCantidadDetalle.Name = "txtCantidadDetalle"
        Me.txtCantidadDetalle.Size = New System.Drawing.Size(48, 22)
        Me.txtCantidadDetalle.SoloLectura = True
        Me.txtCantidadDetalle.TabIndex = 4
        Me.txtCantidadDetalle.TabStop = False
        Me.txtCantidadDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadDetalle.Texto = "0"
        '
        'lblCantidadDetalle
        '
        Me.lblCantidadDetalle.Location = New System.Drawing.Point(3, 0)
        Me.lblCantidadDetalle.Name = "lblCantidadDetalle"
        Me.lblCantidadDetalle.Size = New System.Drawing.Size(52, 25)
        Me.lblCantidadDetalle.TabIndex = 0
        Me.lblCantidadDetalle.Text = "Cantidad:"
        Me.lblCantidadDetalle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgvDetalleAsiento
        '
        Me.dgvDetalleAsiento.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalleAsiento.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvDetalleAsiento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetalleAsiento.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvDetalleAsiento.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDetalleAsiento.Location = New System.Drawing.Point(0, 0)
        Me.dgvDetalleAsiento.Name = "dgvDetalleAsiento"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalleAsiento.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvDetalleAsiento.Size = New System.Drawing.Size(1004, 131)
        Me.dgvDetalleAsiento.TabIndex = 4
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvDetalleAsiento)
        Me.Panel1.Controls.Add(Me.TableLayoutPanel4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 398)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1004, 162)
        Me.Panel1.TabIndex = 6
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.dgvFactura)
        Me.Panel3.Location = New System.Drawing.Point(3, 252)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1004, 140)
        Me.Panel3.TabIndex = 7
        '
        'dgvFactura
        '
        Me.dgvFactura.BackgroundColor = System.Drawing.Color.White
        Me.dgvFactura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFactura.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvFactura.Location = New System.Drawing.Point(0, 0)
        Me.dgvFactura.Name = "dgvFactura"
        Me.dgvFactura.Size = New System.Drawing.Size(1004, 140)
        Me.dgvFactura.TabIndex = 0
        '
        'frmCobranzaValidar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1254, 605)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmCobranzaValidar"
        Me.Text = "frmCobranzaValidar"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.StatusStrip2.ResumeLayout(False)
        Me.StatusStrip2.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel5.PerformLayout()
        Me.FlowLayoutPanel7.ResumeLayout(False)
        Me.FlowLayoutPanel7.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        CType(Me.dgvDetalleAsiento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        CType(Me.dgvFactura, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents ExportarAPlanillaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents StatusStrip2 As StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents dgvCabecera As DataGridView
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents chkSinConciliar As CheckBox
    Friend WithEvents chkOperacion As CheckBox
    Friend WithEvents cbxOperacion As ocxCBX
    Friend WithEvents lblComprobante As Label
    Friend WithEvents txtComprobanteOperacion As ocxTXTString
    Friend WithEvents chkSucursal As CheckBox
    Friend WithEvents cbxSucursalOperacion As ocxCBX
    Friend WithEvents lblNumero As Label
    Friend WithEvents btnExportar As Button
    Friend WithEvents txtNumeroOperacion As ocxTXTString
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents chkFecha2 As CheckBox
    Friend WithEvents txtHasta2 As DateTimePicker
    Friend WithEvents txtDesde2 As DateTimePicker
    Friend WithEvents FlowLayoutPanel7 As FlowLayoutPanel
    Friend WithEvents rdbFechaDocumento2 As RadioButton
    Friend WithEvents rdbFechaRegistro2 As RadioButton
    Friend WithEvents dtpHasta As DateTimePicker
    Friend WithEvents dtpDesde As DateTimePicker
    Friend WithEvents FlowLayoutPanel5 As FlowLayoutPanel
    Friend WithEvents rdbFechaDocumento As RadioButton
    Friend WithEvents rdbFechaRegistro As RadioButton
    Friend WithEvents lblFecha As Label
    Friend WithEvents btnListar As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents dgvDetalleAsiento As DataGridView
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents lblCantidadDetalle As Label
    Friend WithEvents txtCantidadDetalle As ocxTXTNumeric
    Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
    Friend WithEvents lblTotalDebito As Label
    Friend WithEvents txtTotalDebito As ocxTXTNumeric
    Friend WithEvents lblTotalCredito As Label
    Friend WithEvents txtTotalCredito As ocxTXTNumeric
    Friend WithEvents lblSaldo As Label
    Friend WithEvents txtSaldo As ocxTXTNumeric
    Friend WithEvents Panel3 As Panel
    Friend WithEvents dgvFactura As DataGridView
End Class

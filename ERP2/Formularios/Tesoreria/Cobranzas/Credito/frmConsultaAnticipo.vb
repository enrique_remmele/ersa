﻿Public Class frmConsultaAnticipo

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim dtOperacion As DataTable
    Dim dtComprobante As DataTable
    Dim dtFormaPago As DataTable

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim IDOperacion As Integer
    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'Form

        'TextBox
        txtCantidadCobranza.txt.ResetText()
        txtOperacion.txt.ResetText()
        txtTotalCobranza.txt.ResetText()

        'CheckBox
        chkTipoComprobante.Checked = False
        chkCliente.Checked = False
        chkFecha.Checked = False
        chkCobrador.Checked = False

        'ComboBox
        cbxTipoComprobante.Enabled = False
        cbxCliente.Enabled = False
        cbxCobrador.Enabled = False

        'ListView
        dgwOperacion.Rows.Clear()

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion(frmAplicacionProveedoresAnticipo.Name, "COBRANZA CREDITO", "CCRE")
        CargarInformacion()

        'Foco

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Cliente
        CSistema.SqlToComboBox(cbxCliente, "Select Distinct IDCliente, Cliente From VCobranzaCredito Order By 2")

        'TipoComprobante
        CSistema.SqlToComboBox(cbxTipoComprobante, "Select ID, Descripcion From VTipoComprobante Where IDOperacion=" & IDOperacion & " Order By 2")

        'Cobrador
        CSistema.SqlToComboBox(cbxCobrador, "Select ID, Nombres From Cobrador Order By 2")

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Cliente
        chkCliente.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO", "False")
        cbxCliente.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CLIENTE", "")

        'Tipo de Comprobante
        chkTipoComprobante.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO", "False")
        cbxTipoComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", "")

        'Cobrador
        chkCobrador.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "COBRADOR ACTIVO", "False")
        cbxCobrador.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "COBRADOR", "")

        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

    End Sub

    'Guardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Cliente
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE ACTIVO", chkCliente.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CLIENTE", cbxCliente.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO", chkTipoComprobante.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", cbxTipoComprobante.Text)

        'Cobrador
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "COBRADOR ACTIVO", chkCobrador.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "COBRADOR", cbxCobrador.Text)

    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If

    End Function

    'Listar Cobranza
    Sub ListarCobranzas(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "")

        ctrError.Clear()

        Where = Condicion

        'Cliente
        If EstablecerCondicion(cbxCliente, chkCliente, "IDCliente", "Seleccione correctamente el cliente!") = False Then
            Exit Sub
        End If

        'Comprobante
        If EstablecerCondicion(cbxTipoComprobante, chkTipoComprobante, "IDTipoComprobante", "Seleccione correctamente el tipo de comprobante!") = False Then
            Exit Sub
        End If

        'Cobrador
        If EstablecerCondicion(cbxCobrador, chkCobrador, "IDCobrador", "Seleccione correctamente el cobrador!") = False Then
            Exit Sub
        End If

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la sucursal!") = False Then
            Exit Sub
        End If

        'Fecha
        If chkFecha.Checked = True Then
            If Where = "" Then
                Where = " Where (FechaEmision  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' ) "
            Else
                Where = Where & " And (FechaEmision  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' )  "
            End If
        End If

        dgwOperacion.Rows.Clear()

        'Solo por numero
        If Numero > 0 Then
            Where = " Where Numero = " & Numero & ""
        End If

        Dim sql As String = "Select IDTransaccion,Numero,'Comprobante'=NroComprobante,'Estado'=1,Cliente,'Fec'=Fecha,Cobrador,Total,Observacion From VAnticipoAplicacion " & Where

        dtOperacion = CSistema.ExecuteToDataTable(sql)

        Dim TotalCobranza As Decimal = 0
        Dim CantidadCobranza As Decimal = 0

        For Each oRow As DataRow In dtOperacion.Rows
            Dim Registro(8) As String

            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Numero").ToString
            Registro(2) = oRow("Comprobante").ToString
            Registro(3) = oRow("Estado").ToString
            Registro(4) = oRow("Cliente").ToString
            Registro(5) = oRow("Fec").ToString
            Registro(6) = oRow("Cobrador").ToString
            Registro(7) = oRow("Total").ToString
            Registro(8) = oRow("Observacion").ToString

            dgwOperacion.Rows.Add(Registro)
            TotalCobranza = TotalCobranza + CDec(oRow("Total").ToString)
            CantidadCobranza = CantidadCobranza + 1

        Next

        txtTotalCobranza.SetValue(TotalCobranza)
        txtCantidadCobranza.SetValue(CantidadCobranza)

    End Sub

    'Listar Cobranza
    Sub ListarCobranzasxAnticipo(Optional ByVal Numero As String = "", Optional ByVal Condicion As String = "")

        ctrError.Clear()


        Where = Condicion

        ''Cliente
        'If EstablecerCondicion(cbxCliente, chkCliente, "IDCliente", "Seleccione correctamente el cliente!") = False Then
        '    Exit Sub
        'End If

        ''Comprobante
        'If EstablecerCondicion(cbxTipoComprobante, chkTipoComprobante, "IDTipoComprobante", "Seleccione correctamente el tipo de comprobante!") = False Then
        '    Exit Sub
        'End If

        ''Cobrador
        'If EstablecerCondicion(cbxCobrador, chkCobrador, "IDCobrador", "Seleccione correctamente el cobrador!") = False Then
        '    Exit Sub
        'End If

        ''Sucursal
        'If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la sucursal!") = False Then
        '    Exit Sub
        'End If

        ''Fecha
        'If chkFecha.Checked = True Then
        '    If Where = "" Then
        '        Where = " Where (FechaEmision  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' ) "
        '    Else
        '        Where = Where & " And (FechaEmision  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' )  "
        '    End If
        'End If

        dgwOperacion.Rows.Clear()

        'Solo por numero
        If Numero <> "" Then
            Where = " Where NroComprobante like '%" & Numero & "%'"
        End If

        Dim sql As String = "Select IDTransaccion,Numero,'Comprobante'=NroComprobante,'Estado'=1,Cliente,'Fec'=Fecha,Cobrador,Total,Observacion From VAnticipoAplicacion " & Where

        dtOperacion = CSistema.ExecuteToDataTable(sql)

        Dim TotalCobranza As Decimal = 0
        Dim CantidadCobranza As Decimal = 0

        For Each oRow As DataRow In dtOperacion.Rows
            Dim Registro(8) As String

            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Numero").ToString
            Registro(2) = oRow("Comprobante").ToString
            Registro(3) = oRow("Estado").ToString
            Registro(4) = oRow("Cliente").ToString
            Registro(5) = oRow("Fec").ToString
            Registro(6) = oRow("Cobrador").ToString
            Registro(7) = oRow("Total").ToString
            Registro(8) = oRow("Observacion").ToString

            dgwOperacion.Rows.Add(Registro)
            TotalCobranza = TotalCobranza + CDec(oRow("Total").ToString)
            CantidadCobranza = CantidadCobranza + 1

        Next

        txtTotalCobranza.SetValue(TotalCobranza)
        txtCantidadCobranza.SetValue(CantidadCobranza)

    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = dgwOperacion.SelectedRows(0).Cells("colIDTransaccion").Value

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub frmConsultaChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkCliente_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCliente.CheckedChanged
        HabilitarControles(chkCliente, cbxCliente)
    End Sub

    Private Sub chkBanco_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipoComprobante.CheckedChanged
        HabilitarControles(chkTipoComprobante, cbxTipoComprobante)
    End Sub

    Private Sub chkTipo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCobrador.CheckedChanged
        HabilitarControles(chkCobrador, cbxCobrador)
    End Sub

    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged
        HabilitarControles(chkFecha, dtpDesde)
        HabilitarControles(chkFecha, dtpHasta)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        ListarCobranzas()
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        SeleccionarRegistro()
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarCobranzas(txtOperacion.ObtenerValor)
            txtNroAnticipo.txt.Clear()
            txtOperacion.txt.Focus()
            txtOperacion.txt.SelectAll()
        End If
    End Sub

    Private Sub dtpDesde_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpDesde.ValueChanged
        dtpHasta.Value = dtpDesde.Text
    End Sub


    Private Sub dgwOperacion_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgwOperacion.CellClick
        ctrError.Clear()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        Dim vIDTransaccion As Integer

        vIDTransaccion = dgwOperacion.SelectedRows(0).Cells("colIDTransaccion").Value

    End Sub

    Private Sub dgwOperacion_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgwOperacion.DoubleClick
        SeleccionarRegistro()
    End Sub

    Private Sub txtNroAnticipo_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtNroAnticipo.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarCobranzasxAnticipo(txtNroAnticipo.txt.Text, "")
            txtNroAnticipo.txt.Focus()
            txtNroAnticipo.txt.SelectAll()
        End If
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmConsultaAnticipo_Activate()
        Me.Refresh()
    End Sub
End Class
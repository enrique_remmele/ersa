﻿Public Class frmModificarCobranzaCredito


    'CLASE
    Dim CSistema As New CSistema

    'PRIPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private FechaValue As Date
    Public Property Fecha() As Date
        Get
            Return FechaValue
        End Get
        Set(ByVal value As Date)
            FechaValue = value
        End Set
    End Property

    Private SaldoValue As Decimal
    Public Property Saldo() As Decimal
        Get
            Return SaldoValue
        End Get
        Set(ByVal value As Decimal)
            SaldoValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True
        txtMoneda.Inicializar()
        CargarInformacion()
        CargarOperacion()

        If vgIDPerfil = 1 Then
            txtMoneda.Enabled = True
        Else
            txtMoneda.Enabled = False
        End If


    End Sub

    Sub CargarInformacion()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Cobrador
        CSistema.SqlToComboBox(cbxCobrador.cbx, "Select Distinct ID,Nombres From Cobrador")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante where IDOperacion = " & IDOperacion)

    End Sub
    Sub CargarOperacion()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VCobranzaCredito Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnicos."
            ctrError.SetError(txtComprobante, mensaje)
            ctrError.SetIconAlignment(txtComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        cbxTipoComprobante.txt.Text = oRow("TipoComprobante").ToString
        cbxCobrador.cbx.Text = oRow("Cobrador").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtPlanilla.txt.Text = oRow("NroPlanilla").ToString
        txtMoneda.SetValue(oRow("Moneda").ToString, oRow("Cotizacion").ToString)

        If CBool(oRow("ReciboInterno")) = True Then
            txtComprobante.Enabled = False
            cbxTipoComprobante.Enabled = False
        End If


    End Sub

    Sub Modificar()
        'Aplicar
        Try
            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDCobrador", cbxCobrador.GetValue, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Comprobante", txtComprobante.txt.Text, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroPlanilla", txtPlanilla.txt.Text, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@IDMoneda", txtMoneda.Registro("ID"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoNumeroBaseDatos(txtMoneda.Registro("Cotizacion"), True), ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Aplicar la Cobranza
            If CSistema.ExecuteStoreProcedure(param, "SpActualizarCobranzaCredito", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnModificar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnModificar, ErrorIconAlignment.TopRight)
                MsgBox(MensajeRetorno, MsgBoxStyle.Exclamation)
            End If

            Me.Close()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub frmSeleccionFormaPagoDocumento_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmSeleccionEfectivo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    Private Sub txtMoneda_CambioMoneda() Handles txtMoneda.CambioMoneda
        Dim Fecha As Date = CSistema.ExecuteScalar("Select FechaEmision from cobranzacredito where idtransaccion = " & IDTransaccion)

        txtMoneda.FiltroFecha = CSistema.FormatoFechaBaseDatos(Fecha, True, False)
        txtMoneda.Recargar()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmModificarCobranzaCredito_Activate()
        Me.Refresh()
    End Sub
End Class
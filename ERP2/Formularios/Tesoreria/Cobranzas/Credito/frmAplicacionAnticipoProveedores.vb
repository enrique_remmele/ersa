﻿Public Class frmAplicacionAnticipoProveedores
    'CLASES
    Public CSistema As New CSistema
    Public CArchivoInicio As New CArchivoInicio
    Public CAsiento As New CAsientoAplicacionAnticipoProveedores
    Public CData As New CData


    Enum ENUMFormaPago
        ComprobanteGS_PagoGS = 0
        ComprobanteGS_PagoUS = 1
        ComprobanteUS_PagoUS = 2
        ComprobanteUS_PagoGS = 3
    End Enum

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private ChequeValue As Boolean
    Public Property Cheque As Boolean
        Get
            Return ChequeValue
        End Get
        Set(ByVal value As Boolean)
            ChequeValue = value
        End Set
    End Property

    Private DecimalesValue As Boolean
    Public Property Decimales() As Boolean
        Get
            Return DecimalesValue
        End Get
        Set(ByVal value As Boolean)
            DecimalesValue = value
            txtImporteMoneda.Decimales = value
        End Set
    End Property

    Private CotizacionDelDiaValue As Decimal
    Public Property CotizacionDelDia() As Decimal
        Get
            Return CotizacionDelDiaValue
        End Get
        Set(ByVal value As Decimal)
            CotizacionDelDiaValue = value
            txtCotizacion.SetValue(value)
        End Set
    End Property

    Private SaldoOpMonedaFactValue As Decimal
    Public Property SaldoOpMonedaFact() As Decimal
        Get
            Return SaldoOpMonedaFactValue
        End Get
        Set(ByVal value As Decimal)
            SaldoOpMonedaFactValue = value
        End Set
    End Property

    Public Property CargarAlIniciar As Boolean = False
    Public Property CerrarAlGuardar As Boolean = False
    Public Property IDMonedaPago As Integer

    'VARIABLES
    Public Property dtEgresos As New DataTable
    Public Property dtEgresosCargarOperacion As New DataTable
    Dim dtEfectivo As New DataTable
    Dim dt As New DataTable
    Dim dtOrdenPago As New DataTable
    Dim dtDocumento As New DataTable
    Dim dtAplicacionDetalle As New DataTable
    Public vControles() As Control
    Public vNuevo As Boolean = False
    Dim vImporte As Decimal
    Dim vTotalop As Decimal
    Dim Monto As Double = 0
    Dim MontoLetras As String = ""
    Dim vSaldoAsiento As Decimal
    Dim vAsiento As Boolean = False
    Dim Cotizacion As Decimal
    Dim IDTransaccionOrdenPago As Integer
    Dim IDTransaccionEgreso As Integer
    Dim IDMonedaOP As Integer
    Dim FechaOp As String
    Dim CotizacionParaSaldo As Decimal

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion("frmAplicacionAnticipoProveedores", "APLICACION DE ANTICIPO A PROVEEDORES", "APA")
        vNuevo = False
        vAsiento = False

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()

        'Controles
        txtProveedor.Conectar()
        txtNroOrdenPago.Enabled = False

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)
        Dim k As Keys = vgKeyProcesar
        btnGuardar.Text = "&Guardar (" & k.ToString & ")"

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, txtProveedor)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtObservacion)

        'Egresos
        CSistema.CargaControl(vControles, btnAgregarComprobantes)

        CSistema.CargaControl(vControles, lklEliminarEgreso)

        'Cheque
        CSistema.CargaControl(vControles, cbxCuentaBancaria)
        CSistema.CargaControl(vControles, txtNroCheque)
        CSistema.CargaControl(vControles, txtFechaCheque)
        CSistema.CargaControl(vControles, txtFechaPagoCheque)
        CSistema.CargaControl(vControles, txtCotizacion)
        CSistema.CargaControl(vControles, txtImporteMoneda)
        CSistema.CargaControl(vControles, txtImporte)
        CSistema.CargaControl(vControles, chkDiferido)
        CSistema.CargaControl(vControles, txtOrden)

        'Documentos
        CSistema.CargaControl(vControles, Me.OcxOrdenPagoDocumento1)

        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select IDCiudad, CodigoCiudad  From VSucursal WHERE ID=1 Order By 2")

        'Ciudad
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Descripcion From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Cuenta Bancaria
        CSistema.SqlToComboBox(cbxCuentaBancaria.cbx, CData.GetTable("VCuentaBancaria").Copy, "ID", "CuentaBancaria")

        'Ultimo Registro
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From AplicacionAnticipoProveedores Where IDSucursal=" & cbxSucursal.GetValue & "),1) "), Integer)
        txtComprobante.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From AplicacionAnticipoProveedores Where IDSucursal=" & cbxSucursal.GetValue & "),1) "), Integer)

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)
        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnAgregarComprobantes, btnAsiento, vControles, btnModificar)
    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        'Cuenta Bancaria
        CSistema.SqlToComboBox(cbxCuentaBancaria.cbx, CData.GetTable("VCuentaBancaria").Copy, "ID", "CuentaBancaria")

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        dtEgresosCargarOperacion.Rows.Clear()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From AplicacionAnticipoProveedores Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
        dt = CSistema.ExecuteToDataTable("Select * From VAplicacionAnticipoProveedores Where IDTransaccion=" & IDTransaccion)

        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Numero").ToString
        cbxMoneda.SelectedValue(oRow("IDMoneda").ToString)
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        txtObservacion.txt.Text = oRow("Observacion").ToString
        'chkAplicarRetencion.Valor = oRow("AplicarRetencion").ToString

        vImporte = oRow("Total")

        'Proveedor
        If oRow("IDProveedor") > 0 Then
            txtProveedor.SetValue(oRow("IDProveedor"))
        Else
            txtProveedor.SetValueString(0, "", "")
        End If

        'Cheque
        InicializarControlesCheque()

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        CalcularTotales()

        If CBool(oRow("Anulado").ToString) = True Then

            'Visible Label
            lblRegistradoPor.Visible = True
            lblUsuarioAnulado.Visible = True
            lblUsuarioRegistro.Visible = True
            lblFechaRegistro.Visible = True
            lblAnulado.Visible = True
            lblAnulado.Visible = True
            lblFechaAnulado.Visible = True

            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If


        dtAplicacionDetalle = CSistema.ExecuteToDataTable("Select * From VAplicacionAnticipoProveedoresDetalle where IDTransaccion = " & IDTransaccion)
        Dim oRow1 As DataRow = dtAplicacionDetalle.Rows(0)
        IDTransaccionOrdenPago = oRow1("IDTransaccionOrdenPago")
        dtOrdenPago = CSistema.ExecuteToDataTable("Select * from vOrdenPagoSaldoSelect where IDTransaccion = " & IDTransaccionOrdenPago)
        Dim oRow2 As DataRow = dtOrdenPago.Rows(0)
        If CBool(oRow2("Cheque")) = True Then

            cbxCuentaBancaria.SelectedValue(oRow2("IDCuentaBancaria"))
            txtBanco.txt.Text = oRow2("Banco")
            txtMoneda.txt.Text = oRow2("Mon")
            txtNroCheque.txt.Text = oRow2("NroCheque").ToString
            txtFechaCheque.SetValue(oRow2("FechaCheque"))
            txtFechaPagoCheque.SetValue(oRow2("FechaPago"))
            txtCotizacion.SetValue(oRow2("Cotizacion"))
            txtImporteMoneda.SetValue(oRow2("ImporteMoneda"))
            txtImporte.SetValue(oRow2("Importe"))
            'CalcularImporteCheque()
            chkDiferido.Checked = oRow2("Diferido")
            txtVencimiento.SetValue(oRow2("FechaVencimiento"))
            txtOrden.txt.Text = oRow2("ALaOrden").ToString

        End If
        txtNroOrdenPago.txt.Text = oRow2("Numero")
        txtNroOrdenPago.SoloLectura = True

        'TxtTotalOP.SetValue(CSistema.FormatoMoneda((oRow2("Total")), Decimales))
        'TxtSaldoOP.SetValue(CSistema.FormatoMoneda((oRow2("Saldo")), Decimales))
        If oRow2("ImporteMoneda") > 0 Then
            TxtTotalOP.SetValue(CSistema.FormatoMoneda((oRow2("ImporteMoneda")), Decimales))
            TxtSaldoOP.SetValue(CSistema.FormatoMoneda((oRow2("Saldo")), Decimales))
        Else
            TxtTotalOP.SetValue(CSistema.FormatoMoneda((oRow2("ImporteMonedaDocumento")), Decimales))
            TxtSaldoOP.SetValue(CSistema.FormatoMoneda((oRow2("Saldo")), Decimales))
        End If

        'Documentos
        OcxOrdenPagoDocumento1.Inicializar()
        OcxOrdenPagoDocumento1.ListarFormaPago(IDTransaccionOrdenPago)
        dtDocumento = CSistema.ExecuteToDataTable("Select * From VFormaPagoDocumento Where IDTransaccion = " & IDTransaccionOrdenPago & " Order By ID ASC ").Copy

        If dtDocumento.Rows.Count() > 0 Then
            Dim oRowDoc As DataRow = dtDocumento.Rows(0)
            If oRowDoc("Cotizacion") <> 1 Then
                Decimales = True
            End If
        End If

        'ListarEgresos de Cargar Operacion()
        CargarEgreso(IDTransaccion)

        'Inicializamos el Asiento
        CAsiento.Limpiar()


    End Sub

    Sub ListarEgresos()

        dgw.Rows.Clear()

        Dim Total As Decimal = 0
        Dim Saldo As Decimal = 0
        Dim TotalImporte As Decimal = 0
        Dim TotalRetencion As Decimal = 0
        Dim Retentor As Boolean = False

        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)

        If dtEgresos.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each oRow As DataRow In dtEgresos.Rows
            If CSistema.RetornarValorBoolean(oRow("Seleccionado").ToString) = True Then

                Dim oRow1(11) As String

                oRow1(0) = oRow("IDTransaccion").ToString
                oRow1(1) = oRow("NroComprobante").ToString
                oRow1(2) = oRow("Fecha").ToString
                oRow1(3) = oRow("Proveedor").ToString
                oRow1(4) = oRow("Moneda").ToString
                oRow1(5) = oRow("Cotizacion").ToString
                'If oRow("ImporteMoneda") > 0 Then
                'oRow1(6) = CSistema.FormatoMoneda((oRow("ImporteMoneda")), Decimales)
                'Else
                'oRow1(6) = CSistema.FormatoMoneda((oRow("ImporteMonedaDocumento")), Decimales)
                'End If
                oRow1(6) = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
                oRow1(7) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)
                oRow1(8) = CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales)
                oRow1(9) = CSistema.FormatoMoneda(oRow("TotalImpuesto").ToString, Decimales)
                oRow1(10) = (oRow("RetencionIVA").ToString)
                oRow1(11) = oRow("IDTransaccion").ToString
                'oRow1(11) = oRow("IDTransaccionOrdenPago").ToString
                'SC:21062022


                Total = Total + CDec(oRow("Importe").ToString)
                Saldo = Saldo + CDec(oRow("Saldo").ToString)
                'SC:21062022
                TotalRetencion = TotalRetencion + CDec(oRow("RetencionIVA").ToString)
                'If TotalRetencion > 0 Then
                '    chkAplicarRetencion.Valor = True
                'End If

                dgw.Rows.Add(oRow1)

                    'Retentor = CBool(oRow("Retentor").ToString)

                End If
        Next

        'If chkAplicarRetencion.Valor = True Then
        'SC:21062022
        If TotalRetencion <> 0 Then
            TotalImporte = Total - TotalRetencion
        Else
            TotalImporte = Total
        End If

        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)

        txtTotalComprobante.SetValue(Total)
        'txtSaldoTotal.SetValue(Total)
        'txtSaldoTotal.SetValue(CSistema.FormatoMoneda(TotalImporte, Decimales))
        txtCantidadComprobante.SetValue(dgw.Rows.Count)
        CalcularTotales()

        If vNuevo = True Then
            If dgw.Rows.Count > 0 Then
                txtProveedor.SoloLectura = True
                cbxMoneda.SoloLectura = True
            Else
                txtProveedor.SoloLectura = False
                cbxMoneda.SoloLectura = False
            End If

        End If

    End Sub

    Sub ListarEgresosCargaOperacion()

        dgw.Rows.Clear()

        Dim Total As Decimal = 0
        Dim Saldo As Decimal = 0
        Dim TotalImporte As Decimal = 0
        Dim TotalRetencion As Decimal = 0
        'Dim Retentor As Boolean = False

        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)

        If dtEgresosCargarOperacion.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each oRow As DataRow In dtEgresosCargarOperacion.Rows
            If CSistema.RetornarValorBoolean(oRow("Seleccionado").ToString) = True Then

                Dim oRow1(12) As String

                oRow1(0) = oRow("IDTransaccion").ToString
                oRow1(1) = oRow("NroComprobante").ToString
                oRow1(2) = oRow("Fecha").ToString
                oRow1(3) = oRow("Proveedor").ToString
                oRow1(4) = oRow("Moneda").ToString
                oRow1(5) = oRow("Cotizacion").ToString
                If oRow("ImporteMoneda") > 0 Then
                    oRow1(6) = CSistema.FormatoMoneda((oRow("ImporteMoneda")), Decimales)
                Else
                    oRow1(6) = CSistema.FormatoMoneda((oRow("ImporteMonedaDocumento")), Decimales)
                End If
                oRow1(6) = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
                oRow1(7) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)
                oRow1(8) = CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales)
                oRow1(9) = CSistema.FormatoMoneda(oRow("TotalImpuesto").ToString, Decimales)
                oRow1(10) = (oRow("RetencionIVA").ToString)
                oRow1(11) = oRow("IDTransaccionEgreso").ToString
                oRow1(12) = oRow("IDTransaccionOrdenPago").ToString


                Total = Total + CDec(oRow("Importe").ToString)
                Saldo = Saldo + CDec(oRow("Saldo").ToString)
                TotalRetencion = TotalRetencion + CDec(oRow("RetencionIVA").ToString)
                'If TotalRetencion > 0 Then
                '    chkAplicarRetencion.Valor = True
                'End If

                dgw.Rows.Add(oRow1)

                'Retentor = CBool(oRow("Retentor").ToString)

            End If
        Next

        'If chkAplicarRetencion.Valor = True Then
        If TotalRetencion <> 0 Then
            TotalImporte = Total - TotalRetencion
        Else
            TotalImporte = Total
        End If

        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)

        txtTotalComprobante.SetValue(Total)
        'txtSaldoTotal.SetValue(Total)
        'txtSaldoTotal.SetValue(CSistema.FormatoMoneda(TotalImporte, Decimales))
        txtCantidadComprobante.SetValue(dgw.Rows.Count)
        CalcularTotales()

        If vNuevo = True Then
            If dgw.Rows.Count > 0 Then
                txtProveedor.SoloLectura = True
                cbxMoneda.SoloLectura = True
            Else
                txtProveedor.SoloLectura = False
                cbxMoneda.SoloLectura = False
            End If

        End If

    End Sub

    Sub CalcularTotales()

        'Variables
        Dim TotalEgreso As Decimal = 0
        Dim TotalCheque As Decimal = 0
        Dim TotalDocumento As Decimal = 0
        Dim TotalDocumentoNegativo As Decimal = 0
        Dim TotalEfectivo As Decimal = 0
        Dim SaldoTotal As Decimal = 0
        Dim DiferenciaCambio As Decimal = 0

        TotalEgreso = CSistema.gridSumColumn(dgw, "colImporte")

        'Obtener valores Cheque
        If txtImporte.ObtenerValor > 0 Or txtImporteMoneda.ObtenerValor > 0 Then
            Select Case FormaPago()
                Case ENUMFormaPago.ComprobanteUS_PagoGS
                    TotalEgreso = ObtenerImporteComprobantes(False, True)
                    TotalCheque = ObtenerImporteCheque(True, True)
                    'SC:21062022. Agregado
                    TotalCheque = TotalCheque + ObtenerImporteRetencion(True, False)
                Case ENUMFormaPago.ComprobanteUS_PagoUS
                    TotalEgreso = ObtenerImporteComprobantes(True, False)
                    TotalCheque = ObtenerImporteCheque(True, False)
                Case Else
                    TotalEgreso = ObtenerImporteComprobantes(True, True)
                    TotalCheque = ObtenerImporteCheque(True, True)
            End Select
        End If
        'Obtener valores Documentos
        If Not OcxOrdenPagoDocumento1.dtDocumento Is Nothing Then
            If OcxOrdenPagoDocumento1.dtDocumento.Rows.Count > 0 Then
                Select Case FormaPagoDocumento()
                    Case ENUMFormaPago.ComprobanteUS_PagoGS
                        'Egresos
                        TotalEgreso = ObtenerImporteComprobantes(False, True)
                        'Documento
                        TotalDocumento = ObtenerImporteDocumento(False, True, True)
                        'Documento Negativo
                        TotalDocumentoNegativo = ObtenerImporteDocumento(True, True, True)
                    Case ENUMFormaPago.ComprobanteUS_PagoUS
                        'Egresos
                        TotalEgreso = ObtenerImporteComprobantes(True, False)
                        'Documentos
                        TotalDocumento = ObtenerImporteDocumento(False, True, False)
                        'Documentos Negativo
                        TotalDocumentoNegativo = ObtenerImporteDocumento(True, True, False)
                    Case Else
                        TotalEgreso = ObtenerImporteComprobantes(True, True)
                        TotalDocumento = ObtenerImporteDocumento(False, True, True)
                        TotalDocumentoNegativo = ObtenerImporteDocumento(True, True, True)
                End Select

            End If

        End If

        DiferenciaCambio = ObtenerDiferenciaCambio()

        If TotalEgreso < 0 Then
            TotalEgreso = TotalEgreso * -1
        End If

        SaldoTotal = (TotalCheque + TotalEfectivo + TotalDocumento) - (TotalEgreso + TotalDocumentoNegativo)

        'Si hay diferencia de cambio
        Dim CalcularDiferenciaCambioEnSaldo As Boolean = False

        If txtImporte.ObtenerValor > 0 Or txtImporteMoneda.ObtenerValor > 0 Then
            Select Case FormaPago()
                Case ENUMFormaPago.ComprobanteUS_PagoGS
                    CalcularDiferenciaCambioEnSaldo = True
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    CalcularDiferenciaCambioEnSaldo = True
                Case Else
                    CalcularDiferenciaCambioEnSaldo = False
            End Select
        Else
            Select Case FormaPagoDocumento()
                Case ENUMFormaPago.ComprobanteUS_PagoGS
                    CalcularDiferenciaCambioEnSaldo = True
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    CalcularDiferenciaCambioEnSaldo = True
                Case Else
                    CalcularDiferenciaCambioEnSaldo = False
            End Select
        End If

        If CalcularDiferenciaCambioEnSaldo = True And DiferenciaCambio <> 0 Then

            If DiferenciaCambio > 0 Then
                If SaldoTotal > 0 Then
                    SaldoTotal = SaldoTotal - DiferenciaCambio
                Else
                    SaldoTotal = SaldoTotal - (DiferenciaCambio * -1)
                End If
            End If

            If DiferenciaCambio < 0 Then
                If SaldoTotal > 0 Then
                    SaldoTotal = SaldoTotal + DiferenciaCambio
                Else
                    SaldoTotal = (SaldoTotal * -1) + DiferenciaCambio
                End If
            End If

        End If

        If OcxOrdenPagoDocumento1.dgw.Rows.Count > 0 Then
            IDMonedaPago = OcxOrdenPagoDocumento1.dgw.Rows(0).Cells.Item(11).Value
        End If

        Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & IDMonedaPago, "VMoneda")("Decimales").ToString)

        'If chkAplicarRetencion.Valor = True And SaldoTotal <> 0 Then
        '    SaldoTotal = SaldoTotal + CSistema.FormatoMoneda(txtTotalRetencion.txt.Text, Decimales)
        'End If

        'txtSaldoTotal.SetValue(CSistema.FormatoMoneda(SaldoTotal, Decimales))
        txtCantidadComprobante.SetValue(dgw.Rows.Count)

        'Actualizar saldo para documentos
        OcxOrdenPagoDocumento1.Saldo = SaldoTotal * -1
        OcxOrdenPagoDocumento1.IDMoneda = cbxMoneda.GetValue
        OcxOrdenPagoDocumento1.Decimales = IIf(CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString), 1, 0)

        If TotalCheque > 0 Then
            TabControl1.TabPages(0).Text = "Cheque*"
        Else
            TabControl1.TabPages(0).Text = "Cheque"
        End If

        If TotalDocumento > 0 Or TotalDocumentoNegativo > 0 Or OcxOrdenPagoDocumento1.dgw.Rows.Count > 0 Then
            TabControl1.TabPages(1).Text = "Documento*"
        Else
            TabControl1.TabPages(1).Text = "Documento"
        End If

    End Sub

    Sub CargarEgreso(ByVal IDTransaccion As Integer)
        'Genera la carga cuando el egreso esta relacionado a una Aplicación de Anticipo
        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra los Egresos asociados!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        dtEgresosCargarOperacion.Rows.Clear()
        dtEgresosCargarOperacion = CSistema.ExecuteToDataTable("Select * From VAplicacionAnticipoProveedoresDetalle Where IDTransaccion=" & IDTransaccion).Copy
        ListarEgresosCargaOperacion()

    End Sub

    Sub CargarEgresos()
        Dim vCotizacionMoneda As Integer = CType(CSistema.ExecuteScalar("Select IsNull(Max(cotizacion), 1) From Cotizacion Where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' and IDMoneda=" & cbxMoneda.GetValue), Integer)
        If vCotizacionMoneda = 1 And cbxMoneda.GetValue <> 1 Then
            MessageBox.Show("Cargar cotizacion del dia para la moneda.", "Cotizacion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        dtEgresos = CSistema.ExecuteToDataTable("Exec SpViewEgresosParaOrdenPagoAplicacion " & cbxSucursal.cbx.SelectedValue & ", @SoloAPagar='True' ")

        Dim frm As New frmOrdenPagoSeleccionarEgresos
        frm.Text = "Selección de Comprobantes Pendientes de Pago"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.IDMoneda = cbxMoneda.GetValue
        frm.dt = dtEgresos
        If txtProveedor.Seleccionado = True Then
            frm.IDProveedor = txtProveedor.Registro("ID")
        Else
            frm.IDProveedor = 0
        End If

        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccion de Egresos para pago", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        dtEgresos = frm.dt
        'dbs
        'Try
        '    chkAplicarRetencion.Valor = CType(CSistema.ExecuteScalar("Select SujetoRetencion From Proveedor Where ID=" & txtProveedor.Registro("ID")), Boolean)
        'Catch ex As Exception
        '    chkAplicarRetencion.Valor = 0
        'End Try

        ListarEgresos()
        'CambiarTotalEgresoPorPagoOtraMoneda()
        'ObtenerEgresoRetencion()
        'AnalizarSiAplicaRetencion()

    End Sub

    Sub CargarEgresos(ByRef dt As DataTable)

        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpViewEgresosParaOrdenPagoAplicacion " & cbxSucursal.cbx.SelectedValue & " ").Copy

        For Each oRow As DataRow In dttemp.Rows

            Dim NewRow As DataRow = dt.NewRow
            NewRow("IDTransaccion") = oRow("IDTransaccion")
            NewRow("NroComprobante") = oRow("NroComprobante")
            NewRow("T. Comp.") = oRow("T. Comp.")
            NewRow("Proveedor") = oRow("Proveedor")
            NewRow("IDProveedor") = oRow("IDProveedor")
            NewRow("Fecha") = oRow("Fecha")
            NewRow("Fec") = oRow("Fec")
            NewRow("FechaVencimiento") = oRow("FechaVencimiento")
            NewRow("Fec. Venc.") = oRow("Fec. Venc.")
            NewRow("Total") = oRow("Total")
            NewRow("Saldo") = oRow("Saldo")
            NewRow("Importe") = oRow("Importe")
            NewRow("IDMoneda") = oRow("IDMOneda")
            NewRow("Moneda") = oRow("Moneda")
            NewRow("Cotizacion") = oRow("Cotizacion")
            NewRow("Observacion") = oRow("Observacion")
            NewRow("Seleccionado") = oRow("Seleccionado")
            NewRow("Cancelar") = oRow("Cancelar")
            dt.Rows.Add(NewRow)
        Next
    End Sub

    Sub InicializarControlesCheque()

        txtNroCheque.txt.Clear()
        txtCotizacion.SetValue(1)
        txtImporte.SetValue(0)
        txtImporteMoneda.SetValue(0)
        chkDiferido.Checked = 0
        txtVencimiento.txt.Clear()
        txtOrden.txt.Clear()
        txtTotalComprobante.txt.Clear()

    End Sub

    Private Function FormaPago() As ENUMFormaPago

        FormaPago = ENUMFormaPago.ComprobanteGS_PagoGS

        Dim vIDMonedaFormaPago As Integer = IDMonedaPago
        Dim vIDMonedaComprobantes As Integer = cbxMoneda.GetValue

        'ComprobanteGS_PagoGS
        If vIDMonedaComprobantes = 1 And vIDMonedaFormaPago = 1 Then
            Return ENUMFormaPago.ComprobanteGS_PagoGS
        End If

        'ComprobanteGS_PagoUS
        If vIDMonedaComprobantes = 1 And vIDMonedaFormaPago <> 1 Then
            Return ENUMFormaPago.ComprobanteGS_PagoUS
        End If

        'ComprobanteUS_PagoUS
        If vIDMonedaComprobantes <> 1 And vIDMonedaFormaPago <> 1 Then
            Return ENUMFormaPago.ComprobanteUS_PagoUS
        End If

        'ComprobanteUS_PagoGS
        If vIDMonedaComprobantes <> 1 And vIDMonedaFormaPago = 1 Then
            Return ENUMFormaPago.ComprobanteUS_PagoGS
        End If

    End Function
    'SC:21062022. Agregado
    Private Function ObtenerImporteRetencion(Optional EnImporteLocal As Boolean = False, Optional EnLaCotizacionDelDia As Boolean = True) As Decimal

        ObtenerImporteRetencion = 0

        If dtEgresos Is Nothing Then
            Exit Function
        End If

        If dtEgresos.Rows.Count = 0 Then
            Exit Function
        End If

        Dim TotalRetencion As Decimal = 0

        'La cotizacion se usa desde el del dia
        For Each oRow As DataRow In dtEgresos.Select("Seleccionado='True'")

            Select Case FormaPago()

                Case ENUMFormaPago.ComprobanteGS_PagoGS
                    TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                Case ENUMFormaPago.ComprobanteUS_PagoUS
                    If EnImporteLocal Then
                        If EnLaCotizacionDelDia = True Then
                            TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * txtCotizacion.ObtenerValor)
                            'TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * oRow("Cotizacion"))
                        Else
                            TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * oRow("Cotizacion"))
                        End If
                    Else
                        TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                    End If
                Case ENUMFormaPago.ComprobanteUS_PagoGS
                    If EnImporteLocal Then
                        If EnLaCotizacionDelDia = True Then
                            TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * txtCotizacion.ObtenerValor)
                            'TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * oRow("Cotizacion"))
                        Else
                            TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * oRow("Cotizacion"))
                        End If
                    Else
                        TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                    End If

                Case Else
                    TotalRetencion = TotalRetencion + oRow("RetencionIVA")
            End Select

        Next

        'Sacamos la retencion
        'If chkAplicarRetencion.Valor = False Then
        '    Return 0
        'End If

        Return TotalRetencion

    End Function

    Private Function ObtenerImporteComprobantes(Optional RestarRetencion As Boolean = True, Optional EnImporteLocal As Boolean = False) As Decimal

        ObtenerImporteComprobantes = 0

        If dtEgresos Is Nothing Then
            Exit Function
        End If

        If dtEgresos.Rows.Count = 0 Then
            Exit Function
        End If

        Dim TotalComprobante As Decimal = 0
        Dim TotalRetencion As Decimal = 0
        Dim Importe As Decimal = 0
        Dim Retencion As Decimal = 0

        For Each oRow As DataRow In dtEgresos.Select("Seleccionado='True'")

            Select Case FormaPago()

                Case ENUMFormaPago.ComprobanteGS_PagoGS
                    TotalComprobante = TotalComprobante + oRow("Importe")
                    'TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    TotalComprobante = TotalComprobante + oRow("Importe")
                    'TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                Case ENUMFormaPago.ComprobanteUS_PagoUS
                    If EnImporteLocal Then
                        TotalComprobante = TotalComprobante + (oRow("Importe") * oRow("Cotizacion"))
                        'TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * oRow("Cotizacion"))
                    Else
                        TotalComprobante = TotalComprobante + oRow("Importe")
                        'TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                    End If
                Case ENUMFormaPago.ComprobanteUS_PagoGS
                    If EnImporteLocal Then
                        TotalComprobante = TotalComprobante + (oRow("Importe") * oRow("Cotizacion"))
                        'TotalRetencion = TotalRetencion + (oRow("RetencionIVA") * oRow("Cotizacion"))
                    Else
                        TotalComprobante = TotalComprobante + oRow("Importe")
                        'TotalRetencion = TotalRetencion + oRow("RetencionIVA")
                    End If

                Case Else
                    TotalComprobante = TotalComprobante + oRow("Importe")
                    'TotalRetencion = TotalRetencion + oRow("RetencionIVA")
            End Select

        Next

        'Sacamos la retencion
        'If RestarRetencion = True Then
        '    If chkAplicarRetencion.Valor Then
        '        TotalComprobante = TotalComprobante - TotalRetencion
        '    End If
        'End If

        Return TotalComprobante

    End Function

    Sub CambiarTotalEgresoPorPagoOtraMoneda()

        'Exit Sub

        'Validar
        If dtEgresos Is Nothing Then
            Exit Sub
        End If

        If dtEgresos.Rows.Count = 0 Then
            Exit Sub
        End If

        'Si no se selecciono ningun comprobante, salir
        If dtEgresos.Select("Seleccionado='True'").Count = 0 Then
            Exit Sub
        End If

        'Si las monedas de pago y de los comprobantes son iguales, salir
        Dim IDMonedaComprobantes As Integer
        Dim Total As Decimal = 0
        Dim TotalRetencion As Decimal = 0

        For Each oRow As DataRow In dtEgresos.Select("Seleccionado='True'")
            IDMonedaComprobantes = oRow("IDMoneda")

            Dim Cotizacion As Decimal = oRow("Cotizacion")
            Dim Importe As Decimal = oRow("Importe")
            'SC:21062022-Agregado
            Dim Retencion As Decimal = oRow("RetencionIVA")

            If IDMonedaPago = IDMonedaComprobantes Then
                Total = Total + Importe
                'SC:21062022-Agregado
                TotalRetencion = TotalRetencion + Retencion
            Else
                'De GS a US
                If IDMonedaComprobantes = 1 Then

                    'Usamos la cotizacion del dia
                    Cotizacion = CotizacionDelDia
                    Total = Total + (Importe / Cotizacion)
                    'SC:21062022-Agregado
                    TotalRetencion = TotalRetencion + (Retencion / Cotizacion)

                End If

                'De US a GS
                If IDMonedaComprobantes <> 1 Then
                    Total = Total + (Importe * Cotizacion)
                    'SC:21062022-Agregado
                    TotalRetencion = TotalRetencion + (Retencion * Cotizacion)
                End If

            End If

        Next

        txtTotalComprobante.SetValue(Total)
        'txtTotalRetencion.SetValue(TotalRetencion)
        'CalcularImporteCheque()

        ObtenerDiferenciaCambio()

    End Sub

    Private Function ObtenerDiferenciaCambio() As Decimal


        ObtenerDiferenciaCambio = 0

        lblDiferenciaCambio.Visible = False
        txtDiferenciaCambio.Visible = False

        'Validar
        If vNuevo = False Then

            If txtDiferenciaCambio.ObtenerValor <> 0 Then
                lblDiferenciaCambio.Visible = True
                txtDiferenciaCambio.Visible = True
            End If

            Return txtDiferenciaCambio.ObtenerValor

        End If

        If dtEgresos Is Nothing Then
            Exit Function
        End If

        If dtEgresos.Rows.Count = 0 Then
            Exit Function
        End If

        'Si no se selecciono ningun comprobante, salir
        If dtEgresos.Select("Seleccionado='True'").Count = 0 Then
            Exit Function
        End If

        'Variables
        Dim TotalComprobantes As Decimal = 0
        Dim TotalCheque As Decimal = 0
        Dim TotalRetencion As Decimal = 0
        Dim TotalDocumento As Decimal = 0
        Dim TotalDocumentoNegativo As Decimal = 0

        'Si la moneda de pago es GS y los del comprobante GS, no hay diferencia de cambio, se usa el mismo
        'Obtener valores Cheque
        If txtImporte.ObtenerValor > 0 Or txtImporteMoneda.ObtenerValor > 0 Then
            Select Case FormaPago()
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    TotalComprobantes = ObtenerImporteComprobantes(True, False)
                    TotalCheque = ObtenerImporteCheque(True, True)
                Case ENUMFormaPago.ComprobanteGS_PagoGS
                    txtDiferenciaCambio.SetValue(0)
                    Exit Function
                Case ENUMFormaPago.ComprobanteUS_PagoGS

                    'Transformar en US, hayar la diferencia y volver a convertir en GS
                    TotalComprobantes = ObtenerImporteComprobantes(False, False)
                    'TotalRetencion = ObtenerImporteRetencion(False)
                    TotalRetencion = 0
                    TotalComprobantes = TotalComprobantes - TotalRetencion

                    TotalCheque = TotalComprobantes
                    TotalCheque = TotalCheque * txtCotizacion.ObtenerValor

                    TotalComprobantes = ObtenerImporteComprobantes(False, True)
                    'TotalRetencion = ObtenerImporteRetencion(True, False)
                    TotalRetencion = 0
                    TotalComprobantes = TotalComprobantes - TotalRetencion

                Case ENUMFormaPago.ComprobanteUS_PagoUS
                    TotalComprobantes = ObtenerImporteComprobantes(False, True)
                    TotalComprobantes = TotalComprobantes '- ObtenerImporteRetencion(True)
                    TotalCheque = ObtenerImporteCheque(True, True)
            End Select
        End If

        'Obtener valores Documentos
        If OcxOrdenPagoDocumento1.dtDocumento.Rows.Count > 0 Then
            Select Case FormaPagoDocumento()
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    TotalComprobantes = ObtenerImporteComprobantes(True, False)
                    TotalDocumento = ObtenerImporteDocumento(False, True, True)
                    TotalDocumentoNegativo = ObtenerImporteDocumento(True, True, True)
                Case ENUMFormaPago.ComprobanteGS_PagoGS
                    txtDiferenciaCambio.SetValue(0)
                    Exit Function
                Case ENUMFormaPago.ComprobanteUS_PagoGS

                    'Transformar en US, hayar la diferencia y volver a convertir en GS
                    TotalComprobantes = ObtenerImporteComprobantes(False, False)
                    TotalRetencion = 0
                    TotalComprobantes = TotalComprobantes - TotalRetencion

                    'Documento
                    If OcxOrdenPagoDocumento1.ObtenerImporte(True, False) > 0 Then
                        'TotalDocumento = TotalComprobantes
                        TotalDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(True, False)
                        TotalDocumento = TotalDocumento * OcxOrdenPagoDocumento1.dtDocumento.Rows(0)("Cotizacion")
                    Else
                        TotalDocumento = 0
                    End If

                    'Documento Negativo
                    If OcxOrdenPagoDocumento1.ObtenerImporte(True, True) > 0 Then
                        'TotalDocumentoNegativo = TotalComprobantes
                        TotalDocumentoNegativo = OcxOrdenPagoDocumento1.ObtenerImporte(True, True)
                        TotalDocumentoNegativo = TotalDocumento * txtCotizacion.ObtenerValor
                    Else
                        TotalDocumentoNegativo = 0
                    End If

                    TotalComprobantes = ObtenerImporteComprobantes(False, True)
                    TotalRetencion = 0
                    TotalComprobantes = TotalComprobantes - TotalRetencion

                Case ENUMFormaPago.ComprobanteUS_PagoUS
                    TotalComprobantes = ObtenerImporteComprobantes(False, True)
                    TotalComprobantes = TotalComprobantes '- ObtenerImporteRetencion(True)
                    TotalDocumento = ObtenerImporteDocumento(False, True, True)
                    TotalDocumentoNegativo = ObtenerImporteDocumento(True, True, True)
            End Select
        End If

        ObtenerDiferenciaCambio = TotalCheque + TotalDocumento - TotalComprobantes
        txtDiferenciaCambio.SetValue(ObtenerDiferenciaCambio)

        lblDiferenciaCambio.Visible = True
        txtDiferenciaCambio.Visible = True

    End Function

    Private Function ObtenerImporteDocumento(DocumentoDeResta As Boolean, Optional SinRetencion As Boolean = True, Optional EnImporteLocal As Boolean = False) As Decimal

        'SOLO SE PUEDE PAGAR CON LA MISMA MONEDA!!!!
        'EN EL DETALLE OBLIGAR QUE LA MONEDA SEA LA MISMA PARA TODAS LAS FORMAS DE PAGO

        ObtenerImporteDocumento = 0

        For Each oRow As DataRow In OcxOrdenPagoDocumento1.dtDocumento.Rows

            Select Case FormaPagoDocumento()

                Case ENUMFormaPago.ComprobanteGS_PagoGS
                    ObtenerImporteDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(True, DocumentoDeResta)
                Case ENUMFormaPago.ComprobanteGS_PagoUS
                    ObtenerImporteDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(False, DocumentoDeResta) * oRow("Cotizacion")
                Case ENUMFormaPago.ComprobanteUS_PagoUS
                    If EnImporteLocal Then
                        ObtenerImporteDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(True, DocumentoDeResta)
                    Else
                        ObtenerImporteDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(False, DocumentoDeResta)
                    End If
                Case ENUMFormaPago.ComprobanteUS_PagoGS
                    If EnImporteLocal Then
                        ObtenerImporteDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(True, DocumentoDeResta)
                    Else
                        ObtenerImporteDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(False, DocumentoDeResta)
                    End If
                Case Else
                    ObtenerImporteDocumento = OcxOrdenPagoDocumento1.ObtenerImporte(True, DocumentoDeResta)
            End Select

            Exit For

        Next

    End Function

    Private Function FormaPagoDocumento() As ENUMFormaPago

        'SOLO SE PUEDE PAGAR CON LA MISMA MONEDA!!!!
        'EN EL DETALLE OBLIGAR QUE LA MONEDA SEA LA MISMA PARA TODAS LAS FORMAS DE PAGO

        FormaPagoDocumento = ENUMFormaPago.ComprobanteGS_PagoGS

        If OcxOrdenPagoDocumento1.dtDocumento Is Nothing Then
            Exit Function
        End If

        If OcxOrdenPagoDocumento1.dtDocumento.Rows.Count = 0 Then
            Exit Function
        End If

        Dim vIDMonedaFormaPago As Integer = OcxOrdenPagoDocumento1.dtDocumento.Rows(0)("IDMoneda")
        Dim vIDMonedaComprobantes As Integer = cbxMoneda.GetValue

        'ComprobanteGS_PagoGS
        If vIDMonedaComprobantes = 1 And vIDMonedaFormaPago = 1 Then
            Return ENUMFormaPago.ComprobanteGS_PagoGS
        End If

        'ComprobanteGS_PagoUS
        If vIDMonedaComprobantes = 1 And vIDMonedaFormaPago <> 1 Then
            Return ENUMFormaPago.ComprobanteGS_PagoUS
        End If

        'ComprobanteUS_PagoUS
        If vIDMonedaComprobantes <> 1 And vIDMonedaFormaPago <> 1 Then
            Return ENUMFormaPago.ComprobanteUS_PagoUS
        End If

        'ComprobanteUS_PagoGS
        If vIDMonedaComprobantes <> 1 And vIDMonedaFormaPago = 1 Then
            Return ENUMFormaPago.ComprobanteUS_PagoGS
        End If

    End Function

    Private Function ObtenerImporteCheque(Optional SinRetencion As Boolean = True, Optional EnImporteLocal As Boolean = False) As Decimal

        ObtenerImporteCheque = 0

        If txtImporteMoneda.ObtenerValor < 0 Then
            txtImporteMoneda.SetValue(txtImporteMoneda.ObtenerValor * -1)
        End If

        Select Case FormaPago()

            Case ENUMFormaPago.ComprobanteGS_PagoGS
                ObtenerImporteCheque = txtImporte.ObtenerValor
            Case ENUMFormaPago.ComprobanteGS_PagoUS
                ObtenerImporteCheque = txtImporteMoneda.ObtenerValor * txtCotizacion.ObtenerValor
            Case ENUMFormaPago.ComprobanteUS_PagoUS
                If EnImporteLocal Then
                    ObtenerImporteCheque = txtImporte.ObtenerValor
                Else
                    ObtenerImporteCheque = txtImporteMoneda.ObtenerValor
                End If
            Case ENUMFormaPago.ComprobanteUS_PagoGS
                If EnImporteLocal Then
                    ObtenerImporteCheque = txtImporte.ObtenerValor
                Else
                    ObtenerImporteCheque = txtImporteMoneda.ObtenerValor
                End If
            Case Else
                ObtenerImporteCheque = txtImporte.ObtenerValor
        End Select

    End Function

    Sub Nuevo()
        txtNroOrdenPago.SoloLectura = False
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)
        'Clases
        CAsiento.InicializarAsiento()
        'Limpiar detalle
        dtEgresos.Rows.Clear()
        ListarEgresos()
        txtNroOrdenPago.Enabled = True

        cbxCuentaBancaria.cbx.Text = ""
        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        'chkAplicarRetencion.Valor = True

        'Funciones
        LimpiarControles()

        IDTransaccion = 0
        CAsiento.Limpiar()
        InicializarControlesCheque()
        vNuevo = True
        txtID.txt.ReadOnly = True

        'Variable Booleana para marcar el asiento
        vAsiento = False

        'Cotizaciones
        '31 corresponde al IDOperación de la ORDEN DE PAGO, ya que el 261 de la Aplicacion Anticipo a Proveedores no se encuentra dentro de la 
        'configuración preestablecida
        ' CotizacionDelDia = CSistema.CotizacionDelDia(IDMonedaPago, 31)
        CotizacionDelDia = CSistema.CotizacionDelDia(IDMonedaPago, IDOperacion)

        'Controles
        OcxOrdenPagoDocumento1.Inicializar()
        OcxOrdenPagoDocumento1.btnDocumentos.Enabled = False
        OcxOrdenPagoDocumento1.btnEliminar.Enabled = False
        txtProveedor.Enabled = False
        txtFecha.Enabled = False

        'Cabecera
        txtFecha.txt.Text = ""
        txtFecha.Hoy()
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From AplicacionAnticipoProveedores Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)
        txtComprobante.txt.Text = CSistema.ObtenerProximoNroComprobante(cbxTipoComprobante.cbx.SelectedValue, "AplicacionAnticipoProveedores", "NroComprobante", cbxSucursal.cbx.SelectedValue)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Bloquear aplicar 
        btnAgregarComprobantes.Enabled = False

        txtComprobante.txt.Text = txtID.ObtenerValor

        ''Tipo de Pago
        'EstablecerTipoPago(Me.TipoPago)

        'Limpiar Datos Proveedor
        txtProveedor.Clear()
        TxtTotalOP.txt.Text = 0
        TxtSaldoOP.txt.Text = 0
        txtCantidadComprobante.txt.Text = 0

        'Limpiar detalle 'Clases
        CAsiento.InicializarAsiento()

        'Cargar solo las cuentas que se encuentran activas en la chequera
        CSistema.SqlToComboBox(cbxCuentaBancaria.cbx, "select distinct CB.ID, CuentaBancaria from chequera CH join CuentaBancaria CB on CH.IDCuentaBancaria = CB.ID")
        txtComprobante.SoloLectura = True
        txtNroOrdenPago.Clear()
        txtNroOrdenPago.Focus()
    End Sub

    Sub LimpiarControles()

        'Limpiar Controles
        dgw.Rows.Clear()
        'txtSaldoTotal.txt.Text = ""
        txtTotalComprobante.txt.Text = ""
        txtProveedor.Clear()
        txtFecha.txt.Clear()
        txtObservacion.Clear()
        txtImporteMoneda.txt.Clear()
        txtImporte.txt.Clear()
        'txtTotalRetencion.SetValue(0)
        txtOrden.Clear()
        txtNroCheque.txt.Clear()
        txtFechaCheque.txt.Clear()
        txtFechaPagoCheque.txt.Clear()

        'Desaparecer Label
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From AplicacionAnticipoProveedores Where IDSucursal=" & cbxSucursal.GetValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From AplicacionAnticipoProveedores Where IDSucursal=" & cbxSucursal.GetValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            'BuscarOP()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

        'Guardar
        If e.KeyCode = vgKeyProcesar Then
            'Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
        End If

    End Sub

    'Sub BuscarOP()

    '    Dim where As String = ""
    '    Dim valorwhere As String = ""
    '    'Dim Decimales As Boolean = True

    '    'Filtra por Tipo de Comprobante Orden de Pago = 4
    '    valorwhere = 4
    '    where = where & " IDTipoComprobante = " & valorwhere

    '    'Filtra por Anulado NO = 0
    '    valorwhere = 0
    '    where = where & " AND Anulado = " & valorwhere

    '    'Filtra por Estado NO = 0
    '    valorwhere = 0
    '    where = where & " AND EstadoOP = " & valorwhere

    '    'Filtra por AnticipoProveedor SI = 1
    '    valorwhere = 1
    '    where = where & " AND AnticipoProveedor = " & valorwhere

    '    'Filtra por Saldo mayor a 0
    '    valorwhere = 0
    '    where = where & " AND Saldo > " & valorwhere

    '    'Filtra por Numero OP
    '    valorwhere = 0
    '    where = where & " AND numero = " & txtNroOrdenPago.txt.Text


    '    'Limpiar estructura de datos
    '    dtOrdenPago.Rows.Clear()
    '    dtOrdenPago = CSistema.ExecuteToDataTable("Select * From vOrdenpagoSaldoSelect Where " & where).Copy
    '    If dtOrdenPago.Rows.Count < 1 Then
    '        MsgBox("El Nro. de OP no cumple con las condiciones para ser seleccionda.")
    '        txtNroOrdenPago.txt.Clear()
    '        txtNroOrdenPago.txt.Focus()
    '        Exit Sub
    '    End If

    '    Dim oRow As DataRow = dtOrdenPago.Rows(0)

    '    If CInt(oRow("Saldo").ToString) <= 0 Then
    '        MsgBox("La OP no cuenta con saldo")
    '        txtNroOrdenPago.txt.Clear()
    '        Exit Sub
    '    End If

    '    IDTransaccion = oRow("IDTransaccion").ToString
    '    txtObservacion.txt.Text = oRow("Observacion").ToString
    '    vImporte = oRow("Total")

    '    IDTransaccionOrdenPago = oRow("IDTransaccion").ToString

    '    'Proveedor
    '    If oRow("IDProveedor") > 0 Then
    '        txtProveedor.SetValue(oRow("IDProveedor"))
    '    Else
    '        txtProveedor.SetValueString(0, "", "")
    '    End If

    '    'Cheque
    '    InicializarControlesCheque()


    '    If CBool(oRow("Cheque")) = True Then

    '        If oRow("Cotizacion") <> 1 Then
    '            Decimales = True
    '        Else
    '            Decimales = False
    '        End If

    '        cbxCuentaBancaria.SelectedValue(oRow("IDCuentaBancaria"))
    '        txtBanco.txt.Text = oRow("Banco")
    '        txtMoneda.txt.Text = oRow("Mon")
    '        txtNroCheque.txt.Text = oRow("NroCheque").ToString
    '        txtFechaCheque.SetValue(oRow("FechaCheque"))
    '        txtFechaPagoCheque.SetValue(oRow("FechaPago"))
    '        txtCotizacion.SetValue(oRow("Cotizacion"))
    '        txtImporteMoneda.SetValue(CSistema.FormatoMoneda(oRow("ImporteMoneda"), True))
    '        txtImporte.SetValue(oRow("Importe"))
    '        'CalcularImporteCheque()
    '        chkDiferido.Checked = oRow("Diferido")
    '        txtVencimiento.SetValue(oRow("FechaVencimiento"))
    '        txtOrden.txt.Text = oRow("ALaOrden").ToString
    '        If cbxMoneda.GetValue = 1 Then
    '            If oRow("Cotizacion") <> 1 Then

    '                TxtTotalOP.SetValue(CSistema.FormatoMoneda((oRow("TotalImporteMoneda")), True))
    '                TxtSaldoOP.SetValue(CSistema.FormatoMoneda(oRow("Saldo"), True))
    '            Else
    '                TxtTotalOP.SetValue(CSistema.FormatoMoneda((oRow("TotalImporteMoneda")), False))
    '                TxtSaldoOP.SetValue(CSistema.FormatoMoneda(oRow("Saldo"), False))

    '            End If
    '        Else
    '            If oRow("Cotizacion") <> 1 Then
    '                TxtTotalOP.SetValue(CSistema.FormatoMoneda((oRow("TotalImporteMoneda")), False))
    '                TxtSaldoOP.SetValue(CSistema.FormatoMoneda(oRow("Saldo"), False))
    '            Else
    '                TxtTotalOP.SetValue(CSistema.FormatoMoneda((oRow("TotalImporteMoneda") * oRow("Cotizacion")), True))
    '                TxtSaldoOP.SetValue(CSistema.FormatoMoneda(oRow("Saldo"), True))
    '            End If
    '        End If
    '    End If
    '    'Documentos
    '    OcxOrdenPagoDocumento1.Inicializar()
    '    OcxOrdenPagoDocumento1.ListarFormaPago(IDTransaccion)
    '    dtDocumento = CSistema.ExecuteToDataTable("Select * From VFormaPagoDocumento Where IDTransaccion = " & IDTransaccion & " Order By ID ASC ").Copy

    '    If dtDocumento.Rows.Count() > 0 Then
    '        Dim oRowDoc As DataRow = dtDocumento.Rows(0)
    '        If cbxMoneda.GetValue = 1 Then
    '            If oRowDoc("Cotizacion") <> 1 Then
    '                TxtTotalOP.SetValue(CSistema.FormatoMoneda((oRowDoc("ImporteMoneda")), True))
    '                TxtSaldoOP.SetValue(CSistema.FormatoMoneda(oRowDoc("Saldo"), True))
    '            Else
    '                TxtTotalOP.SetValue(CSistema.FormatoMoneda((oRowDoc("ImporteMoneda")), False))
    '                TxtSaldoOP.SetValue(CSistema.FormatoMoneda(oRowDoc("Saldo"), False))
    '            End If
    '        Else
    '            If oRowDoc("Cotizacion") <> 1 Then
    '                TxtTotalOP.SetValue(CSistema.FormatoMoneda((oRowDoc("ImporteMoneda")), True))
    '                TxtSaldoOP.SetValue(CSistema.FormatoMoneda(oRowDoc("Saldo"), True))
    '            Else
    '                TxtTotalOP.SetValue(CSistema.FormatoMoneda((oRowDoc("ImporteMoneda")), False))
    '                TxtSaldoOP.SetValue(CSistema.FormatoMoneda(oRowDoc("Saldo"), False))
    '            End If
    '        End If
    '    End If
    '    'Diferencia de Cambio
    '    Dim vDiferenciaCambio As Decimal = CSistema.RetornarValorInteger(oRow("DiferenciaCambio").ToString)
    '    If vDiferenciaCambio <> 0 And CBool(oRow("Anulado").ToString) = False Then
    '        txtDiferenciaCambio.Visible = True
    '        lblDiferenciaCambio.Visible = True
    '        txtDiferenciaCambio.SetValue(vDiferenciaCambio)
    '    Else
    '        txtDiferenciaCambio.Visible = False
    '        lblDiferenciaCambio.Visible = False
    '        txtDiferenciaCambio.SetValue(0)
    '    End If

    '    'Calcular Totales
    '    CalcularTotales()

    '    'Bloquear botones

    '    btnAgregarComprobantes.Enabled = True
    '    txtImporte.SoloLectura = True
    '    txtImporteMoneda.SoloLectura = True
    '    cbxCuentaBancaria.SoloLectura = True
    '    txtFechaCheque.SoloLectura = True
    '    txtCotizacion.SoloLectura = True
    '    txtOrden.SoloLectura = True
    '    txtNroCheque.SoloLectura = True

    '    If CBool(oRow("Anulado").ToString) = True Then
    '        MsgBox("Esta OP esta anulada")
    '    End If

    '    'TxtTotalOP.SetValue(CSistema.FormatoMoneda((oRow("Total")), Decimales))
    '    'TxtSaldoOP.SetValue(CSistema.FormatoMoneda((oRow("Saldo")), True))
    '    IDMonedaOP = oRow("IDMoneda")

    '    'Dim SaldoOpMonedaFac As Integer
    '    'Dim CotizacionParaSaldo As Integer
    '    'CotizacionParaSaldo = CSistema.CotizacionDelDia(oRow("IDMoneda"), IDOperacion)
    '    'SaldoOpMonedaFact = TxtSaldoOP.ObtenerValor * CotizacionParaSaldo
    'End Sub
    Sub BuscarOP()

        Dim where As String = ""
        Dim valorwhere As String = ""
        'Dim Decimales As Boolean = True

        'Filtra por Tipo de Comprobante Orden de Pago = 4
        valorwhere = 4
        where = where & " IDTipoComprobante = " & valorwhere

        'Filtra por Anulado NO = 0
        valorwhere = 0
        where = where & " AND Anulado = " & valorwhere

        'Filtra por Estado NO = 0
        valorwhere = 0
        where = where & " AND EstadoOP = " & valorwhere

        'Filtra por AnticipoProveedor SI = 1
        valorwhere = 1
        where = where & " AND AnticipoProveedor = " & valorwhere

        'Filtra por Saldo mayor a 0
        valorwhere = 0
        where = where & " AND Saldo > " & valorwhere

        'Filtra por Numero OP
        valorwhere = 0
        where = where & " AND numero = " & txtNroOrdenPago.txt.Text

        'Limpiar estructura de datos
        dtOrdenPago.Rows.Clear()
        dtOrdenPago = CSistema.ExecuteToDataTable("Select * From vOrdenpagoSaldoSelect Where " & where).Copy

        'Limpiar estructura de datos
        dtOrdenPago = CSistema.ExecuteToDataTable("Select * From vOrdenpagoSaldoSelect Where " & where).Copy
        If dtOrdenPago.Rows.Count < 1 Then
            MsgBox("El Nro. de OP no cumple con las condiciones para ser seleccionda.")
            txtNroOrdenPago.txt.Clear()
            txtNroOrdenPago.txt.Focus()
            Exit Sub
        End If

        Dim oRow As DataRow = dtOrdenPago.Rows(0)

        If CInt(oRow("Saldo").ToString) <= 0 Then
            MsgBox("La OP no cuenta con saldo")
            txtNroOrdenPago.txt.Clear()
            Exit Sub
        End If

        IDTransaccion = oRow("IDTransaccion").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        FechaOp = CSistema.FormatoFechaBaseDatos(CSistema.ExecuteScalar("Select cast(Fecha as date) From Vordenpago where IDTransaccion = " & oRow("IDTransaccion").ToString), True, False)
        vImporte = oRow("Total")

        IDTransaccionOrdenPago = oRow("IDTransaccion").ToString

        'Proveedor
        If oRow("IDProveedor") > 0 Then
            txtProveedor.SetValue(oRow("IDProveedor"))
        Else
            txtProveedor.SetValueString(0, "", "")
        End If

        'Cheque
        InicializarControlesCheque()

        If CBool(oRow("Cheque")) = True Then

            cbxCuentaBancaria.SelectedValue(oRow("IDCuentaBancaria"))
            txtBanco.txt.Text = oRow("Banco")
            txtMoneda.txt.Text = oRow("Mon")
            txtNroCheque.txt.Text = oRow("NroCheque").ToString
            txtFechaCheque.SetValue(oRow("FechaCheque"))
            txtFechaPagoCheque.SetValue(oRow("FechaPago"))
            txtCotizacion.SetValue(oRow("Cotizacion"))
            txtImporteMoneda.SetValue(oRow("ImporteMoneda"))
            txtImporte.SetValue(oRow("Importe"))
            'CalcularImporteCheque()
            chkDiferido.Checked = oRow("Diferido")
            txtVencimiento.SetValue(oRow("FechaVencimiento"))
            txtOrden.txt.Text = oRow("ALaOrden").ToString

        End If

        'Documentos
        OcxOrdenPagoDocumento1.Inicializar()
        OcxOrdenPagoDocumento1.ListarFormaPago(IDTransaccion)

        'Diferencia de Cambio
        Dim vDiferenciaCambio As Decimal = CSistema.RetornarValorInteger(oRow("DiferenciaCambio").ToString)
        If vDiferenciaCambio <> 0 And CBool(oRow("Anulado").ToString) = False Then
            txtDiferenciaCambio.Visible = True
            lblDiferenciaCambio.Visible = True
            txtDiferenciaCambio.SetValue(vDiferenciaCambio)
        Else
            txtDiferenciaCambio.Visible = False
            lblDiferenciaCambio.Visible = False
            txtDiferenciaCambio.SetValue(0)
        End If

        'Calcular Totales
        CalcularTotales()

        'Bloquear botones

        btnAgregarComprobantes.Enabled = True
        txtImporte.SoloLectura = True
        txtImporteMoneda.SoloLectura = True
        cbxCuentaBancaria.SoloLectura = True
        txtFechaCheque.SoloLectura = True
        txtCotizacion.SoloLectura = True
        txtOrden.SoloLectura = True
        txtNroCheque.SoloLectura = True

        If CBool(oRow("Anulado").ToString) = True Then
            MsgBox("Esta OP esta anulada")
        End If

        If oRow("ImporteMoneda") > 0 Then
            TxtTotalOP.SetValue(CSistema.FormatoMoneda((oRow("ImporteMoneda")), Decimales))
        Else
            TxtTotalOP.SetValue(CSistema.FormatoMoneda((oRow("ImporteMonedaDocumento")), Decimales))
        End If
        TxtSaldoOP.SetValue(CSistema.FormatoMoneda((oRow("Saldo")), True))
        IDMonedaOP = oRow("IDMoneda")

        CotizacionParaSaldo = CSistema.CotizacionDelDiaFecha(cbxMoneda.GetValue, IDOperacion, "'" & FechaOp & "'")
        SaldoOpMonedaFact = TxtSaldoOP.txt.Text
        If IDMonedaOP = 1 Then
            'SaldoOpMonedaFact = CSistema.FormatoMoneda2Decimales(SaldoOpMonedaFact / CotizacionParaSaldo, True)
            SaldoOpMonedaFact = CSistema.FormatoMoneda3Decimales(SaldoOpMonedaFact / CotizacionParaSaldo, True)
        End If
        If IDMonedaOP = 2 Then
            SaldoOpMonedaFact = SaldoOpMonedaFact * CotizacionParaSaldo
        End If
        txtEquivalente.SetValue(CSistema.FormatoMoneda3Decimales(SaldoOpMonedaFact, True))
    End Sub

    Sub EliminarEgreso()

        If vNuevo = False Then
            Exit Sub
        End If

        For Each item As DataGridViewRow In dgw.SelectedRows

            For Each oRow As DataRow In dtEgresos.Select(" IDTransaccion = " & item.Cells(0).Value & " ")
                'CAsientoContableOrdenPago.EliminarVenta(oRow("Importe").ToString, oRow("IDTipoComprobante").ToString, oRow("IDMoneda").ToString, oRow("Credito").ToString)
                oRow("Seleccionado") = False
                Exit For
            Next

        Next

        ListarEgresos()

    End Sub

    Sub BuscarOP(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            BuscarOP()
        End If

    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then
            'If vAsiento = False Then
            '    Dim mensaje As String = "ATENCION: Se debe Generar el Asiento en forma Manual!"
            '    CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
            '    Exit Sub
            'End If
        End If

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer
        Dim Decimales As Boolean = False

        'Rutina que aplica los antipos a las facturas del proveedor
        'AplicaAnticipo()

        'Generar la cabecera del asiento
        'Comentado por necesidad de realizarlo en forma manual
        'GenerarAsiento()

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If
        'CSistema.SetSQLParameter(param, "@Numero", CInt(txtID.ObtenerValor), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)

        'Proveedor
        If Not txtProveedor.Registro Is Nothing Then
            CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.Registro("ID").ToString, ParameterDirection.Input)
        End If

        'Moneda
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        'Totales
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(txtTotalComprobante.ObtenerValor, True), ParameterDirection.Input)

        IDMonedaPago = cbxMoneda.GetValue
        If IDMonedaPago <> 1 Then
            CotizacionDelDia = CSistema.CotizacionDelDia(IDMonedaPago, IDOperacion)
            Cotizacion = CotizacionDelDia
            Decimales = True
        End If
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(Cotizacion, False), ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAplicacionAnticipoAProveedores", False, False, MensajeRetorno, IDTransaccion) = False Then

            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From AplicacionAnticipoProveedores Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpAplicacionAnticipoAProveedores", False, False, MensajeRetorno, IDTransaccion)
            End If
            Exit Sub

        End If

        'Dim Procesar As Boolean = True
        'IdTransaccionOrdenPago = dgwOrdenPago.SelectedRows(0).Cells(0).Value

        'Inserta Detalle de la Aplicacion
        For Each oRow As DataRow In dtEgresos.Select("Seleccionado='True'")
            Try
                ReDim param(-1)
                CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDTransaccionOrdenPago", IDTransaccionOrdenPago, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDTransaccionEgreso", oRow("IDTransaccion").ToString, ParameterDirection.Input)
                'CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)
                'Totales
                'CSistema.SetSQLParameter(param, "@TotalEgreso", CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString), ParameterDirection.Input)
                'CSistema.SetSQLParameter(param, "@TotalOrdenPago", CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString), ParameterDirection.Input)
                'Operacion
                CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
                IndiceOperacion = param.GetLength(0) - 1

                'Transaccion
                'CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
                'CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
                'CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
                'CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
                'CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

                'Informacion de Salida
                CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
                CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
                CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

                'Insertar Registro
                If CSistema.ExecuteStoreProcedure(param, "SpAplicacionAnticipoAProveedoresDetalle", False, False, MensajeRetorno, IDTransaccion) = False Then

                    tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                    ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

                    'Eliminar el Registro si es que se registro
                    If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From AplicacionAnticipoProveedoresDetalle Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                        param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                        CSistema.ExecuteStoreProcedure(param, "SpAplicacionAnticipoAProveedoresDetalle", False, False, MensajeRetorno, IDTransaccion)
                    End If
                    Exit Sub

                End If
            Catch ex As Exception
            End Try
        Next


        'Rutina que aplica los antipos a las facturas del proveedor
        AplicaAnticipo()

        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
            CargarOperacion(IDTransaccion)
            txtID.SoloLectura = False
            Exit Sub
        End If

        'Procesar la OP
        Try
            ReDim param(-1)
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccionOrdenPago, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)
            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "True", ParameterDirection.Output)

            'Aplicar la Cobranza de los Anticipos a las Facturas
            If CSistema.ExecuteStoreProcedure(param, "SpOrdenPagoProcesarAnticipo", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            End If

            CSistema.ExecuteNonQuery("Exec SpAsientoAplicacionAnticipoProveedores " & IDTransaccion)
            'Guardar Asiento de la transacccion
            CAsiento.IDTransaccion = IDTransaccion
            CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        Catch ex As Exception
        End Try

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

        vNuevo = True
        If CerrarAlGuardar = True Then
            Me.Close()
        End If

        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False
        txtID.txt.Focus()

    End Sub

    Sub AplicaAnticipo()

        If IDTransaccion > 0 Then

            'Cargamos el Detalle de Egresos
            If InsertarDetalleEgresos(IDTransaccionOrdenPago) = False Then
                Dim mensaje As String = "Error al intentar Insertar en OrdenPagoEgreso!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Sub
            End If
        End If

        'Dim param(-1) As SqlClient.SqlParameter
        'Dim IndiceOperacion As Integer = 0

        ''Entrada
        'CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccionOrdenPago, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@Operacion", ERP.CSistema.NUMOperacionesABM.UPD.ToString, ParameterDirection.Input)
        'IndiceOperacion = param.GetLength(0) - 1

        ''Informacion de Salida
        'CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        'CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        'Dim MensajeRetorno As String = ""

        ''Insertar Registro
        'If CSistema.ExecuteStoreProcedure(param, "SpOrdenPagoAplicarProcesar", False, False, MensajeRetorno) = False Then
        '    tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
        '    ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
        '    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        '    Exit Sub
        'End If

    End Sub

    Function InsertarDetalleEgresos(IDTransaccion As Integer) As Boolean

        InsertarDetalleEgresos = True
        For Each oRow As DataRow In dtEgresos.Select("Seleccionado = 'True' ")

            Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)
            Dim sql As String = "Insert Into OrdenPagoEgreso(IDTransaccionOrdenPago, IDTransaccionEgreso,Cuota, Importe, Saldo,Retenido,Retencion,IVA) Values( " & IDTransaccion & "," & oRow("IDTransaccion").ToString & "," & oRow("Cuota").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString, Decimales) & ",'" & oRow("Retener").ToString & "'," & CSistema.FormatoMonedaBaseDatos(oRow("RetencionIVA").ToString, Decimales) & "," & CSistema.FormatoMonedaBaseDatos(oRow("IVA").ToString, Decimales) & ")"

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If
        Next

    End Function

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        'IdTransaccionFactura = dgw.SelectedRows(0).Cells(0).Value
        'IdTransaccionOrdenPago = dgwOrdenPago.SelectedRows(0).Cells(0).Value

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim frm As New frmMotivoAnulacionOP
        frm.Text = " Motivo Anulacion Aplicacion de Anticipo "
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.ShowDialog(Me)

        'EliminaAnticipo()
        For Each oRow As DataRow In dtAplicacionDetalle.Select("IDTransaccion= " & IDTransaccion)
            Dim param(-1) As SqlClient.SqlParameter
            Dim IndiceOperacion As Integer

            'SetSQLParameter, ayuda a generar y configurar los parametros.
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccionOrdenPago", oRow("IDTransaccionOrdenPago").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccionEgreso", oRow("IDTransaccionEgreso").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Numero", CInt(txtID.ObtenerValor), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text.Trim, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.GetValue, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)
            'Moneda
            CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.GetValue, ParameterDirection.Input)

            'Proveedor
            If Not txtProveedor.Registro Is Nothing Then
                CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.Registro("ID").ToString, ParameterDirection.Input)
            End If

            CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDMotivoAnulacion", frm.ID, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
            IndiceOperacion = param.GetLength(0) - 1

            'Transaccion
            CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
            CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpAplicacionAnticipoAProveedores", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)
                Exit Sub
            Else
                tsslEstado.Text = MensajeRetorno
            End If
        Next
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False

    End Sub

    Sub EliminaAnticipo()
        For Each oRow As DataRow In dtEgresos.Select("Seleccionado = 'True' ")
            IDTransaccionEgreso = oRow("IDTransaccionEgreso").ToString
            IDTransaccionOrdenPago = IDTransaccionOrdenPago

            Dim param(-1) As SqlClient.SqlParameter
            Dim IndiceOperacion As Integer = 0

            'Entrada
            CSistema.SetSQLParameter(param, "@IDTransaccionOP", IDTransaccionOrdenPago, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccionEgreso", IDTransaccionEgreso, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cuota", 1, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "SOLICITAR", ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpOrdenPagoEliminarAplicacion", False, False, MensajeRetorno, , True) = False Then
                MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Next
    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            'Ciudad
            If cbxCiudad.Validar("Seleccione correctamente la ciudad!", ctrError, btnGuardar, tsslEstado) = False Then
                Exit Function
            End If

            'Tipo Comprobante
            If cbxTipoComprobante.Validar("Seleccione correctamente el tipo de comprobante!", ctrError, btnGuardar, tsslEstado) = False Then
                Exit Function
            End If

            'Comprobante
            If txtComprobante.txt.Text.Trim.Length = 0 Then
                txtComprobante.SetValue(txtID.ObtenerValor.ToString)
            End If

            'Sucursal
            If cbxSucursal.Validar("Seleccione correctamente la sucursal!", ctrError, btnGuardar, tsslEstado) = False Then
                Exit Function
            End If

            'Observacion
            If txtObservacion.txt.Text = "" Then
                Dim mensaje As String = "Se sugiere generar una breve Observacion para el Movimiento!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If

            'Detalle
            If dgw.Rows.Count = 0 Then
                Dim mensaje As String = "No se ha seleccionado ningun comprobante del Proveedor para el pago!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If

            If dtOrdenPago.Rows.Count = 0 Then
                Dim mensaje As String = "No se ha seleccionado ningun Anticipo!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If

            'Totales
            'If txtSaldoTotal.ObtenerValor <> 0 And Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            Dim TotalComprobante As Decimal = txtTotalComprobante.ObtenerValor
            'SaldoOpMonedaFact = CSistema.FormatoMonedaBaseDatos(TxtSaldoOP.ObtenerValor, True)
            SaldoOpMonedaFact = CSistema.FormatoMoneda3Decimales(TxtSaldoOP.ObtenerValor, True)

            CotizacionParaSaldo = CSistema.CotizacionDelDiaFecha(cbxMoneda.GetValue, IDOperacion, "'" & FechaOp & "'")

            If cbxMoneda.GetValue <> IDMonedaOP Then 'And IDMonedaOP = 1 Then
                'SaldoOpMonedaFact = CSistema.FormatoMoneda2Decimales(SaldoOpMonedaFact / CotizacionParaSaldo, True)
                SaldoOpMonedaFact = CSistema.FormatoMoneda3Decimales(SaldoOpMonedaFact / CotizacionParaSaldo, True)
            End If
            If cbxMoneda.GetValue <> IDMonedaOP Then 'And IDMonedaOP = 2 Then
                SaldoOpMonedaFact = SaldoOpMonedaFact * CotizacionParaSaldo
            End If

            'If TotalComprobante > SaldoOpMonedaFact Then
            '        'Dim mensaje As String = "El importe no corresponde al total de comprobantes!"
            '        'CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
            '        MsgBox("El importe del comprobante es mayor al saldo de la OP", MsgBoxStyle.Critical)
            '        Exit Function
            '    End If

        End If

            If txtTotalComprobante.ObtenerValor < 0 Then
            Dim mensaje As String = "El importe de Factura Proveedor no es válido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Asiento
        If CAsiento.ObtenerSaldo <> 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'If CAsiento.ObtenerTotal = 0 Then
        '    CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Function
        'End If

        'No calcular el saldo
        Select Case FormaPago()
            Case ENUMFormaPago.ComprobanteUS_PagoGS
        End Select

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If
        Else
            If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
                'Validar el Asiento
                If CAsiento.ObtenerSaldo <> 0 Then
                    CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                    Exit Function
                End If

                'If CAsiento.ObtenerTotal = 0 Then
                '    CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                '    Exit Function
                'End If
            End If

            'Total de Pago
            'If txtImporte.ObtenerValor = 0 Then
            '    Dim mensaje As String = "Debe seleccionar un monto para orden de Pago!"
            '    CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
            '    Exit Function
            'End If

            'Total de Factura
            If txtTotalComprobante.ObtenerValor = 0 Then
                Dim mensaje As String = "Debe seleccionar un monto para Facturas de Proveedor!"
                CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Function
            End If
        End If

        Return True

    End Function

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        Dim Comprobante As String = cbxTipoComprobante.cbx.Text & ": " & txtComprobante.txt.Text

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante
            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((select IDTransaccion from vAplicacionAnticipoProveedores WHERE Numero = " & txtID.txt.Text & "And IDSucursal=" & cbxSucursal.GetValue & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else
            MessageBox.Show("Debe Guardar la Operacion para visualizar el asiento", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        End If

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        txtNroOrdenPago.Clear()

        vNuevo = False
        txtID.txt.Focus()

    End Sub

    Private Sub frmAplicacionAnticipoProveedores_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If CargarAlIniciar = False Then
            Exit Sub
        End If

        Inicializar()
    End Sub



    'Sub CalcularImporteCheque()

    '    If cbxCuentaBancaria.cbx.SelectedValue Is Nothing Then
    '        Exit Sub
    '    End If

    '    Dim IDMonedaComprobante As Integer = cbxMoneda.GetValue
    '    Dim ImporteMoneda As Decimal
    '    Dim Cotizacion As Decimal
    '    Dim ImporteLocal As Decimal

    '    If txtImporteMoneda.SoloLectura = False Then

    '        ImporteMoneda = txtImporteMoneda.ObtenerValor
    '        Cotizacion = txtCotizacion.ObtenerValor
    '        ImporteLocal = CSistema.Cotizador(Cotizacion, ImporteMoneda, IDMonedaPago)

    '        txtImporte.SetValue(ImporteLocal)

    '    End If

    '    If txtImporte.SoloLectura = False Then

    '        ImporteLocal = txtImporte.ObtenerValor
    '        Cotizacion = txtCotizacion.ObtenerValor
    '        ImporteMoneda = CSistema.Cotizador(Cotizacion, ImporteLocal, IDMonedaPago)

    '        Select Case FormaPago()
    '            Case ENUMFormaPago.ComprobanteGS_PagoUS
    '                ImporteMoneda = CSistema.Cotizador(Cotizacion, ImporteLocal, IDMonedaComprobante, IDMonedaPago)
    '            Case ENUMFormaPago.ComprobanteUS_PagoGS
    '                ImporteMoneda = ImporteLocal
    '            Case ENUMFormaPago.ComprobanteUS_PagoUS
    '                ImporteMoneda = CSistema.Cotizador(Cotizacion, ImporteLocal, IDMonedaPago, IDMonedaComprobante)
    '        End Select

    '        txtImporteMoneda.SetValue(ImporteMoneda)

    '    End If

    '    CalcularTotales()

    'End Sub

    'Sub CalcularTotales()

    '    'Variables
    '    Dim TotalEgreso As Decimal = 0
    '    Dim TotalCheque As Decimal = 0
    '    Dim TotalDocumento As Decimal = 0
    '    Dim TotalDocumentoNegativo As Decimal = 0
    '    Dim TotalEfectivo As Decimal = 0
    '    Dim SaldoTotal As Decimal = 0
    '    Dim DiferenciaCambio As Decimal = 0

    '    Dim DecimalesCobraza As Boolean = False
    '    Dim DecimalesFormaPago As Boolean = False

    '    Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & IDMonedaPago, "VMoneda")("Decimales").ToString)
    '    DecimalesFormaPago = Decimales
    '    DecimalesCobraza = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString)

    '    'Ver si se paga con una moneda diferente a la de la factura para colocar o no decimal en el saldo
    '    If DecimalesFormaPago <> DecimalesCobraza Then
    '        Decimales = True
    '    End If

    '    TotalEgreso = CSistema.gridSumColumn(dgw, "colImporte")
    '    TotalEgreso = CSistema.FormatoNumero(TotalEgreso, Decimales)

    '    'Obtener valores Cheque
    '    If txtImporte.ObtenerValor > 0 Or txtImporteMoneda.ObtenerValor > 0 Then
    '        Select Case FormaPago()
    '            Case ENUMFormaPago.ComprobanteUS_PagoGS
    '                TotalEgreso = ObtenerImporteComprobantes(False, True)
    '                TotalCheque = ObtenerImporteCheque(True, True)
    '                'TotalCheque = TotalCheque + ObtenerImporteRetencion(True, False)
    '            Case ENUMFormaPago.ComprobanteUS_PagoUS
    '                TotalEgreso = ObtenerImporteComprobantes(True, False)
    '                TotalCheque = ObtenerImporteCheque(True, False)
    '            Case Else
    '                TotalEgreso = ObtenerImporteComprobantes(True, True)
    '                TotalCheque = ObtenerImporteCheque(True, True)

    '                If CDec(TotalEgreso.ToString) = CDec(TotalCheque) Then
    '                    If TotalEgreso > TotalCheque Then
    '                        TotalEgreso = TotalCheque
    '                    Else
    '                        TotalCheque = TotalEgreso
    '                    End If
    '                End If

    '                TotalEgreso = CSistema.ExtraerDecimal(TotalEgreso)

    '                TotalCheque = CSistema.ExtraerDecimal(TotalCheque)


    '        End Select
    '    End If
    '    'Obtener valores Documentos
    '    If Not OcxOrdenPagoDocumento1.dtDocumento Is Nothing Then
    '        If OcxOrdenPagoDocumento1.dtDocumento.Rows.Count > 0 Then
    '            Select Case FormaPagoDocumento()
    '                Case ENUMFormaPago.ComprobanteUS_PagoGS
    '                    'Egresos
    '                    TotalEgreso = ObtenerImporteComprobantes(False, True)
    '                    'Documento
    '                    TotalDocumento = ObtenerImporteDocumento(False, True, True)
    '                    'Documento Negativo
    '                    TotalDocumentoNegativo = ObtenerImporteDocumento(True, True, True)
    '                Case ENUMFormaPago.ComprobanteUS_PagoUS
    '                    'Egresos
    '                    TotalEgreso = ObtenerImporteComprobantes(True, False)
    '                    'Documentos
    '                    TotalDocumento = ObtenerImporteDocumento(False, True, False)
    '                    'Documentos Negativo
    '                    TotalDocumentoNegativo = ObtenerImporteDocumento(True, True, False)
    '                Case Else
    '                    TotalEgreso = ObtenerImporteComprobantes(True, True)
    '                    TotalDocumento = ObtenerImporteDocumento(False, True, True)
    '                    TotalDocumentoNegativo = ObtenerImporteDocumento(True, True, True)
    '            End Select

    '        End If

    '    End If

    '    DiferenciaCambio = ObtenerDiferenciaCambio()

    '    If TotalEgreso < 0 Then
    '        TotalEgreso = TotalEgreso * -1
    '    End If

    '    SaldoTotal = (TotalCheque + TotalEfectivo + TotalDocumento) - (TotalEgreso + TotalDocumentoNegativo)

    '    'Si hay diferencia de cambio
    '    Dim CalcularDiferenciaCambioEnSaldo As Boolean = False

    '    If txtImporte.ObtenerValor > 0 Or txtImporteMoneda.ObtenerValor > 0 Then
    '        Select Case FormaPago()
    '            Case ENUMFormaPago.ComprobanteUS_PagoGS
    '                CalcularDiferenciaCambioEnSaldo = True
    '            Case ENUMFormaPago.ComprobanteGS_PagoUS
    '                CalcularDiferenciaCambioEnSaldo = True
    '            Case Else
    '                CalcularDiferenciaCambioEnSaldo = False
    '        End Select
    '    Else
    '        Select Case FormaPagoDocumento()
    '            Case ENUMFormaPago.ComprobanteUS_PagoGS
    '                CalcularDiferenciaCambioEnSaldo = True
    '            Case ENUMFormaPago.ComprobanteGS_PagoUS
    '                CalcularDiferenciaCambioEnSaldo = True
    '            Case Else
    '                CalcularDiferenciaCambioEnSaldo = False
    '        End Select
    '    End If

    '    If CalcularDiferenciaCambioEnSaldo = True And DiferenciaCambio <> 0 Then

    '        If DiferenciaCambio > 0 Then
    '            If SaldoTotal > 0 Then
    '                SaldoTotal = SaldoTotal - DiferenciaCambio
    '            Else
    '                SaldoTotal = SaldoTotal - (DiferenciaCambio * -1)
    '            End If
    '        End If

    '        If DiferenciaCambio < 0 Then
    '            If SaldoTotal > 0 Then
    '                SaldoTotal = SaldoTotal + DiferenciaCambio
    '            Else
    '                SaldoTotal = (SaldoTotal * -1) + DiferenciaCambio
    '            End If
    '        End If

    '    End If

    '    If OcxOrdenPagoDocumento1.dgw.Rows.Count > 0 Then
    '        IDMonedaPago = OcxOrdenPagoDocumento1.dgw.Rows(0).Cells.Item(11).Value
    '    End If

    '    'If chkAplicarRetencion.Valor = True And SaldoTotal <> 0 Then
    '    '    'Se coloca esta consulta porque no redondea de la misma forma los TotalEgreso y TotalDocumento
    '    '    If SaldoTotal > 1 Or SaldoTotal < -1 Then
    '    '        SaldoTotal = SaldoTotal + CSistema.FormatoMoneda(txtTotalRetencion.txt.Text, Decimales)
    '    '    End If
    '    'End If

    '    txtSaldoTotal.SetValue(CSistema.FormatoMoneda(SaldoTotal, Decimales))
    '    txtCantidadComprobante.SetValue(dgw.Rows.Count)

    '    'Actualizar saldo para documentos
    '    OcxOrdenPagoDocumento1.Saldo = SaldoTotal * -1
    '    OcxOrdenPagoDocumento1.IDMoneda = cbxMoneda.GetValue
    '    OcxOrdenPagoDocumento1.Decimales = IIf(CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & cbxMoneda.GetValue, "VMoneda")("Decimales").ToString), 1, 0)

    '    If TotalCheque > 0 Then
    '        TabControl1.TabPages(0).Text = "Cheque*"
    '        'Control fuera del chorizo
    '        'If chkAplicarRetencion.chk.Checked Then
    '        If txtImporteMoneda.ObtenerValor = (txtTotalComprobante.ObtenerValor) Then
    '            txtSaldoTotal.SetValue(0)
    '        End If
    '        'End If
    '    Else
    '        TabControl1.TabPages(0).Text = "Cheque"
    '    End If

    '    If TotalDocumento > 0 Or TotalDocumentoNegativo > 0 Or OcxOrdenPagoDocumento1.dgw.Rows.Count > 0 Then
    '        TabControl1.TabPages(1).Text = "Documento*"
    '        'Control fuera del chorizo
    '        'If chkAplicarRetencion.chk.Checked Then
    '        If OcxOrdenPagoDocumento1.Total = (txtTotalComprobante.ObtenerValor) Then
    '            txtSaldoTotal.SetValue(0)
    '        End If
    '        'End If

    '    Else
    '        TabControl1.TabPages(1).Text = "Documento"
    '    End If

    'End Sub


    'Sub ObtenerDatosCuentaBancaria()

    '    txtBanco.txt.Clear()
    '    txtMoneda.txt.Clear()

    '    If IsNumeric(cbxCuentaBancaria.cbx.SelectedValue) = False Then
    '        Exit Sub
    '    End If

    '    For Each oRow As DataRow In CData.GetTable("VCuentaBancaria").Select(" ID = " & cbxCuentaBancaria.cbx.SelectedValue)
    '        txtBanco.txt.Text = oRow("Banco")
    '        txtMoneda.txt.Text = oRow("Mon")

    '        If OcxOrdenPagoDocumento1.dgw.Rows.Count > 0 Then
    '            IDMonedaPago = OcxOrdenPagoDocumento1.dgw.Rows(0).Cells.Item(11).Value
    '        Else
    '            IDMonedaPago = oRow("IDMoneda")
    '        End If

    '        txtImporteMoneda.Decimales = oRow("Decimales")
    '    Next

    '    CambiarTotalEgresoPorPagoOtraMoneda()
    '    ComportamientoImporteCheque()

    '    'Si es nuevo, reiniciar valores de importes del cheque
    '    If vNuevo = True Then
    '        txtImporte.SetValue(0)
    '        txtImporteMoneda.SetValue(0)
    '        txtDiferenciaCambio.SetValue(0)
    '        CalcularTotales()
    '    End If

    'End Sub
    'Sub ComportamientoImporteCheque()
    '    Dim CotizacionDia As Decimal = 1
    '    txtImporte.SoloLectura = True
    '    txtImporteMoneda.SoloLectura = True

    '    'Solo si es nuevo
    '    If vNuevo = False Then
    '        Exit Sub
    '    End If
    '    'CotizacionDia = CSistema.ExecuteScalar("Select dbo.FCotizacionAlDia(" & cbxMoneda.GetValue & ", 1)")

    '    'SC:14-10-2021 Cambio necesario para extraer cotizacion de la fecha de operacion
    '    CotizacionDia = CSistema.ExecuteScalar("Select dbo.FCotizacionAlDiaFecha(" & cbxMoneda.GetValue & ",1,'" & CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, False) & "')")
    '    If txtCotizacion.txt.Text = "0" Then
    '        txtCotizacion.txt.Text = CotizacionDia

    '    End If


    '    Select Case FormaPago()
    '        Case ENUMFormaPago.ComprobanteGS_PagoGS
    '            txtImporte.SoloLectura = True
    '            txtImporteMoneda.SoloLectura = False
    '        Case ENUMFormaPago.ComprobanteUS_PagoUS
    '            txtImporte.SoloLectura = True
    '            txtImporteMoneda.SoloLectura = False
    '        Case ENUMFormaPago.ComprobanteUS_PagoGS
    '            txtImporte.SoloLectura = False
    '            txtImporteMoneda.SoloLectura = True
    '        Case ENUMFormaPago.ComprobanteGS_PagoUS
    '            txtImporte.SoloLectura = False
    '            txtImporteMoneda.SoloLectura = True
    '    End Select

    'End Sub

    'Function ValidarMoneda() As Boolean

    '    Dim idMonedaDocumento As Integer
    '    If OcxOrdenPagoDocumento1.dgw.Rows.Count > 0 Then
    '        idMonedaDocumento = OcxOrdenPagoDocumento1.dgw.Rows(0).Cells.Item(11).Value
    '    Else
    '        Return True
    '    End If

    '    If txtImporte.ObtenerValor <> 0 And (idMonedaDocumento <> IDMonedaPago) Then
    '        Return False
    '    End If
    '    Return True

    'End Function

    'Function ValidarCotizacion() As Boolean

    '    Dim cotizacionDocumento As Decimal
    '    If OcxOrdenPagoDocumento1.dgw.Rows.Count > 0 Then
    '        cotizacionDocumento = OcxOrdenPagoDocumento1.dgw.Rows(0).Cells.Item(13).Value
    '    Else
    '        Return True
    '    End If

    '    If txtImporte.ObtenerValor <> 0 And (txtCotizacion.ObtenerValor <> cotizacionDocumento) Then
    '        Return False
    '    End If
    '    Return True

    'End Function

    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub txtID_TeclaPrecionada(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub txtNroOrdenPago_TeclaPrecionada(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtNroOrdenPago.TeclaPrecionada
        BuscarOP(e)
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAgregarComprobantes_Click(sender As System.Object, e As System.EventArgs) Handles btnAgregarComprobantes.Click

        If txtProveedor.txtRazonSocial.txt.Text <> "" Then
            CargarEgresos()

        Else
            MessageBox.Show("No se ha establecido un Proveedor para la busqueda.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtProveedor.Focus()

        End If
    End Sub

    Private Sub lklEliminarEgreso_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarEgreso.LinkClicked
        EliminarEgreso()
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnAnular_Click(sender As Object, e As EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub btnAsiento_Click(sender As Object, e As EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        VisualizarAsientoComprobante()
    End Sub
    Sub VisualizarAsientoComprobante()

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim frm As New frmVisualizarAsiento
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.Text = "Compra : " & dgw.SelectedRows(0).Cells(1).Value
        'SC:20-06-2022 - Envia el IDTransaccionEgreso
        frm.IDTransaccion = dgw.SelectedRows(0).Cells(11).Value

        'Mostramos
        frm.ShowDialog(Me)

    End Sub

    Private Sub LinkLabel2_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        VisualizarDocumento()
    End Sub

    Sub VisualizarDocumento()

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        'Dim IDTransaccion As Integer = dgw.Rows(dgw.CurrentCell.RowIndex).Cells("colIDTransaccionEgreso").Value
        Dim IDTransaccion As Integer = dgw.SelectedRows(0).Cells(11).Value

        Dim vdt As DataTable = CSistema.ExecuteToDataTable("Select * From VTransaccion Where IDTransaccion=" & IDTransaccion)

        If vdt Is Nothing Then
            Exit Sub
        End If

        If vdt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = vdt.Rows(0)

        Dim Tipo As String = oRow("FormName").ToString

        Select Case Tipo
            Case "frmCompra"
                Dim frm As New frmCompra
                frm.IDTransaccion = IDTransaccion

                'Ocultar botones
                frm.btnEliminar.Visible = False
                frm.btnAsiento.Visible = False
                frm.btnBusquedaAvanzada.Visible = False
                frm.btnCancelar.Visible = False
                frm.btnGuardar.Visible = False
                frm.btnNuevo.Visible = False

                FGMostrarFormulario(Me, frm, "Compra de Mercaderias", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)
            Case "frmGastos"
                Dim frm As New frmGastos
                frm.IDTransaccion = IDTransaccion
                Dim CajaChica As Boolean = CSistema.RetornarValorBoolean(CSistema.ExecuteToDataTable("Select Top(1) CajaChica From VGasto Where IDTransaccion=" & IDTransaccion)(0)("CajaChica").ToString)

                'Ocultar botones
                frm.btnAnular.Visible = False
                frm.btnAsiento.Visible = False
                frm.btnBusquedaAvanzada.Visible = False
                frm.btnCancelar.Visible = False
                frm.btnGuardar.Visible = False
                frm.btnNuevo.Visible = False

                'Verificar si es caja chica
                frm.CajaChica = CajaChica
                FGMostrarFormulario(Me, frm, "Gastos", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)
            Case "frmVale"
                Dim frm As New frmVale
                frm.IDTransaccion = IDTransaccion

                'Ocultar botones
                frm.btnAnular.Visible = False
                frm.btnAsiento.Visible = False
                frm.btnBusquedaAvanzada.Visible = False
                frm.btnCancelar.Visible = False
                frm.btnGuardar.Visible = False
                frm.btnNuevo.Visible = False
                frm.btnEditar.Visible = False

                FGMostrarFormulario(Me, frm, "Vales", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)
        End Select

    End Sub
End Class
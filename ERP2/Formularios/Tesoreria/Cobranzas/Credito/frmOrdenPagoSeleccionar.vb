﻿Public Class frmOrdenPagoSeleccionar

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Public Property IDMoneda As Integer
    Public Property Decimales As Boolean

    'VARIABLES
    Dim vCargado As Boolean
    Dim vdt As DataTable


    'FUNCIONES

    Sub Inicializar()

        vCargado = False

        'Propiedades
        'Decimales = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & IDMoneda, "VMoneda")("Decimales"))

        'Funciones
        Listar()
        Cargar()

        'Foco
        dgw.Focus()

    End Sub

    Sub Listar()

        'Limpiar la grilla
        dgw.Rows.Clear()
        Dim Total As Decimal = 0
        Dim Saldo As Decimal = 0

        vCargado = False

        For Each oRow As DataRow In dt.Rows
            Dim Registro(12) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Seleccionado").ToString
            Registro(2) = oRow("Numero").ToString
            Registro(3) = oRow("NroComprobante").ToString
            Registro(4) = oRow("Fecha").ToString
            Registro(5) = oRow("Proveedor").ToString
            Registro(6) = CDec(oRow("Total").ToString)                'CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(7) = CDec(oRow("Saldo").ToString) 'CSistema.FormatoMoneda(oRow("Saldo").ToString)
            Registro(8) = CDec(oRow("Importe").ToString) 'CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(9) = oRow("Observacion").ToString
            Registro(10) = oRow("Cancelar").ToString
            Registro(9) = oRow("Moneda").ToString
            Registro(10) = oRow("Cotizacion").ToString

            'Sumar el total de la deuda
            Total = Total + CDec(oRow("Total").ToString)
            Saldo = Saldo + CDec(oRow("Saldo").ToString)

            dgw.Rows.Add(Registro)

        Next

    End Sub

    Sub Seleccionar()

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells(0).Value

            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Seleccionado") = oRow.Cells(1).Value
                oRow1("Total") = oRow.Cells(6).Value
                oRow1("Saldo") = CDec(oRow.Cells(7).Value)
                oRow1("Cancelar") = oRow.Cells(10).Value

            Next

        Next
        Me.Close()

    End Sub

    Sub Cargar()
        For Each oRow As DataRow In dt.Rows
            For Each oRow1 As DataGridViewRow In dgw.Rows
                If oRow("IDTransaccion") = oRow1.Cells(0).Value Then
                    oRow1.Cells(1).Value = CBool(oRow("Seleccionado").ToString)
                    oRow1.Cells(6).Value = CDec(oRow("Total").ToString) 'CSistema.FormatoMoneda(oRow("Total").ToString)
                    oRow1.Cells(7).Value = CDec(oRow("Saldo").ToString) 'CSistema.FormatoMoneda(oRow("Saldo").ToString)
                    oRow1.Cells(10).Value = CBool(oRow("Cancelar").ToString)
                End If
            Next
        Next

        ' PintarCelda()
        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim Total As Decimal = 0
        Dim Saldo As Decimal = 0

        Dim contador As Integer = 0
        Dim contadortotal As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows
            'If CSistema.RetornarValorBoolean(oRow("Seleccionado").ToString) = True Then
            If oRow.Cells(1).Value = True Then

                Saldo = Saldo + oRow.Cells(7).Value
                contador = contador + 1
            End If
            Total = Total + oRow.Cells(6).Value
            contadortotal = contadortotal + 1
        Next

        txtSaldo.SetValue(Saldo)
        txtTotal.SetValue(Total)
        txtCantidadTotal.SetValue(contadortotal)
        txtCantidadSaldo.SetValue(contador)

    End Sub

    Sub PintarCelda()

        'If dgw.Rows.Count = 0 Then
        '    Exit Sub
        'End If

        'For Each Row As DataGridViewRow In dgw.Rows
        '    If Row.Cells(1).Value = True Then
        '        Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
        '    Else
        '        Row.DefaultCellStyle.BackColor = Color.White
        '    End If
        'Next



        'If dgw.CurrentRow Is Nothing Then
        '    Exit Sub
        'End If

        'dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow


    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeleccionarTodoToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = True
        Next

        CalcularTotales()

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuitarTodaSeleccionToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = False
        Next

        CalcularTotales()
    End Sub


    Private Sub frmOrdenPagoSeleccionar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        vCargado = True
    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick

        ctrError.Clear()
        tsslEstado.Text = ""

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        If e.ColumnIndex = dgw.Columns(1).Index Then
            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(10).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(6)

                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(6).ReadOnly = True
                        oRow.Cells(10).ReadOnly = True
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next
            CalcularTotales()
            dgw.Update()
        End If

    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Columns(e.ColumnIndex).Name = "colTotal" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El Total debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("TotalImpuesto").Value

            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Space Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(6).ReadOnly = False
                        oRow.Cells(10).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(6).ReadOnly = True
                        oRow.Cells(10).ReadOnly = True
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If

        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(6).ReadOnly = False
                        oRow.Cells(10).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        ' dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(6)
                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(6).ReadOnly = True
                        oRow.Cells(10).ReadOnly = True
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colTotal")
            End If


            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If

    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        If vCargado = False Then
            Exit Sub
        End If

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)

        If dgw.Rows(e.RowIndex).Cells(1).Value = False Then
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
        End If

        dgw.Rows(e.RowIndex).DefaultCellStyle.Font = f1

    End Sub

    Private Sub dgw_RowLeave(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowLeave

        If vCargado = False Then
            Exit Sub
        End If

        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        dgw.Rows(e.RowIndex).DefaultCellStyle.Font = f2

        If dgw.Rows(e.RowIndex).Cells(1).Value = False Then
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
        Else
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.PaleTurquoise
        End If

    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp

        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Seleccionar()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub


    Private Sub lnkSeleccionar_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkSeleccionar.LinkClicked
        For Each fila As DataGridViewRow In dgw.Rows
            fila.Cells("colSel").Value = True
        Next
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmOrdenPagoSeleccionar_Activate()
        Me.Refresh()
    End Sub
End Class
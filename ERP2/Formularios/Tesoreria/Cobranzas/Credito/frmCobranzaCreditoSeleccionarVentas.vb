﻿Public Class frmCobranzaCreditoSeleccionarVentas

    'CLASES
    Dim CSistema As New CSistema

    'EVENTOS
    Public Event VentasSeleccionadas(ByVal sender As Object, ByVal e As EventArgs)

    'PROPIEDADES
    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer

        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set

    End Property

    Private ClienteValue As String
    Public Property Cliente() As String

        Get
            Return ClienteValue
        End Get
        Set(ByVal value As String)
            ClienteValue = value
        End Set
    End Property

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private Decimalesvalue As Boolean
    Public Property decimales() As Boolean
        Get
            Return Decimalesvalue
        End Get
        Set(ByVal value As Boolean)
            Decimalesvalue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Cliente
        txtCliente.txt.Text = Cliente

        'Funciones
        ListarVentas(False)
        CargarVentas()

        'Foco
        dgw.Focus()


    End Sub

    Sub ListarVentas(ByVal PorFactura As Boolean)
        'dbs
        Dim Where As String = ""

        If PorFactura = True Then
            Where = "Comprobante Like '%" & txtComprobante.GetValue & "%'"
        Else
            Where = "Comprobante Like '%%'"
        End If
        'fin dbs





        'Limpiar la grilla
        dgw.Rows.Clear()
        Dim TotalDeuda As Decimal = 0

        For Each oRow As DataRow In dt.Select(Where)

            'For Each oRow As DataRow In dt.Rows
            Dim Registro(14) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Sel").ToString
            Registro(2) = oRow("Comprobante").ToString
            Registro(3) = oRow("Tipo").ToString
            Registro(4) = oRow("Condicion").ToString
            Registro(5) = oRow("Vencimiento").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Total").ToString, decimales)
            Registro(7) = CSistema.FormatoMoneda(oRow("Cobrado").ToString, decimales)
            Registro(8) = CSistema.FormatoMoneda(oRow("Descontado").ToString, decimales)
            Registro(9) = CSistema.FormatoMoneda(oRow("Saldo").ToString, decimales)
            Registro(10) = CSistema.FormatoMoneda(oRow("Saldo").ToString, True) * oRow("Cotizacion").ToString
            Registro(11) = CSistema.FormatoMoneda(oRow("Importe").ToString, decimales)
            Registro(12) = CSistema.FormatoMoneda(oRow("Importe").ToString, True) * oRow("Cotizacion").ToString
            Registro(13) = oRow("Cancelar").ToString
            Registro(14) = oRow("Cotizacion").ToString


            'Sumar el total de la deuda
            TotalDeuda = TotalDeuda + CSistema.FormatoMoneda((oRow("Saldo").ToString), decimales)

            dgw.Rows.Add(Registro)

        Next

        txtDeudaTotal.SetValue(TotalDeuda)
        txtCantidadComprobantes.SetValue(dt.Rows.Count)


    End Sub

    Sub SeleccionarVentas()

        'Validar nuevamente que los importes sean mayor a 0
        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Cells("colImporte").Value <= 0 Then
                MessageBox.Show("El importe no puede ser 0", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

        Next

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells(0).Value

            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Sel") = oRow.Cells(1).Value
                oRow1("Importe") = oRow.Cells(11).Value
                oRow1("Cancelar") = oRow.Cells(12).Value
            Next

        Next

        Me.Close()

    End Sub

    Sub CargarVentas()

        For Each oRow As DataRow In dt.Rows
            For Each oRow1 As DataGridViewRow In dgw.Rows
                If oRow("IDTransaccion") = oRow1.Cells(0).Value Then
                    oRow1.Cells(1).Value = CBool(oRow("Sel").ToString)
                    oRow1.Cells(11).Value = CSistema.FormatoMoneda(oRow("Importe").ToString, decimales)
                    oRow1.Cells(13).Value = CBool(oRow("Cancelar").ToString)
                End If
            Next
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim Total As Decimal = 0
        Dim TotalGs As Decimal = 0
        Dim TotalComprobantes As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows

            If oRow.Cells(1).Value = True Then
                Total = Total + oRow.Cells(11).Value
                TotalGs = TotalGs + oRow.Cells(12).Value
                TotalComprobantes = TotalComprobantes + 1
            End If

        Next

        txtTotalCobrado.SetValue(Total)
        txtTotalCobradoGs.SetValue(TotalGs)
        txtCantidadCobrado.SetValue(TotalComprobantes)

    End Sub

    Sub PintarCelda()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(1).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next

        dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub

    Private Sub frmCobranzaCreditoSeleccionarVentas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Columns(e.ColumnIndex).Name = "colImporte" Then

            'Que el valor sea numerico
            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells(9).Value
                dgw.CurrentRow.Cells("colImporteGs").Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells("colImporte").Value * dgw.CurrentRow.Cells("colCotizacionHoy").Value, False)

                Exit Sub

            End If

            'Que el valor sea mayor a 0
            If CDec(dgw.CurrentCell.Value) <= 0 Then

                Dim mensaje As String = "El importe debe ser mayor a 0!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value

                Exit Sub

            End If

            'Validar que no supere el saldo
            Dim Saldo As Decimal = dgw.CurrentRow.Cells("colSaldo").Value.ToString.Replace(".", "")
            Dim Importe As Decimal = dgw.CurrentRow.Cells("colImporte").Value.ToString.Replace(".", "")

            If CDec(dgw.CurrentRow.Cells("colSaldo").Value) < CDec(dgw.CurrentRow.Cells("colImporte").Value) Then
                Dim mensaje As String = "El importe no puede ser mayor al saldo!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value
            End If

            dgw.CurrentCell.Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells("colImporte").Value, decimales)
            dgw.CurrentRow.Cells("colImporteGs").Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells("colImporte").Value * dgw.CurrentRow.Cells("colCotizacionHoy").Value, False)

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(11).ReadOnly = False
                        oRow.Cells(13).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(10)

                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(11).ReadOnly = True
                        oRow.Cells(13).ReadOnly = True
                        oRow.Cells(11).Value = oRow.Cells(9).Value
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colImporte")
            End If

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If
    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Index = e.RowIndex Then
                If oRow.Cells(1).Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                End If

                oRow.DefaultCellStyle.Font = f1

                oRow.Cells(11).Selected = True

            Else

                oRow.DefaultCellStyle.Font = f2

                If oRow.Cells(1).Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.White
                Else
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                End If
            End If
        Next
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Tab Then
            Button1.Focus()
        End If

    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick

        ctrError.Clear()
        tsslEstado.Text = ""

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        If e.ColumnIndex = dgw.Columns.Item("colSel").Index Then

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(11).ReadOnly = False
                        oRow.Cells(13).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(11)

                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(11).ReadOnly = True
                        oRow.Cells(13).ReadOnly = True
                        oRow.Cells(11).Value = oRow.Cells(9).Value
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colImporte")
            End If

            CalcularTotales()

            dgw.Update()

        End If
    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeleccionarTodoToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = True
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuitarTodaSeleccionToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = False
        Next

        PintarCelda()
        CalcularTotales()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        SeleccionarVentas()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub dgw_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellLeave

    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        For Each fila As DataGridViewRow In dgw.Rows
            fila.Cells("colSel").Value = True
        Next
    End Sub

    Private Sub txtComprobante_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarVentas(True)
        End If
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmCobranzaCreditoSeleccionarVentas_Activate()
        Me.Refresh()
    End Sub

    Private Sub txtComprobante_Load(sender As Object, e As EventArgs) Handles txtComprobante.Load

    End Sub
End Class
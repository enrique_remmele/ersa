﻿Public Class frmCobranzaCredito

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CAsiento As New CAsientoCobranza
    Dim CData As New CData
    Dim CCaja As New CCaja
    Dim vdecimalOperacion As Boolean
    Dim vIdcliente As Integer
    Dim vIDTipoAnticipo As Integer
    Dim vHayChequeDiferido As Boolean = False
    Dim vHayContado As Boolean = False
    Dim vReciboInterno As Boolean = False
    Dim IDMotivoAnulacion As Integer
    Dim DiferenciaCambio As Integer

    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    Private PropietarioBDValue As String
    Public Property PropietarioBD() As String
        Get
            Return PropietarioBDValue
        End Get
        Set(ByVal value As String)
            PropietarioBDValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private EsMovilValue As Boolean
    Public Property EsMovil() As Boolean
        Get
            Return EsMovilValue
        End Get
        Set(ByVal value As Boolean)
            EsMovilValue = value
        End Set
    End Property

    Private orowConfiguracionesValue As DataRow
    Public Property orowConfiguraciones() As DataRow
        Get
            Return orowConfiguracionesValue
        End Get
        Set(ByVal value As DataRow)
            orowConfiguracionesValue = value
        End Set
    End Property

    'Private CotizacionDelDiaValue As Decimal
    'Public Property CotizacionDelDia() As Decimal
    '    Get
    '        Return CotizacionDelDiaValue
    '    End Get
    '    Set(ByVal value As Decimal)
    '        CotizacionDelDiaValue = value
    '        'txtCotizacion.SetValue(value)
    '    End Set
    'End Property
    'EVENTOS

    'VARIABLES
    Dim dtDetalleCobranzaVenta As New DataTable
    Dim dtVentasPendientes As New DataTable
    Dim dtVentasRetencion As New DataTable
    Dim dtNotaCredito As New DataTable
    Dim dtFacturasDesdeRecibo As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    ''SC 10-08-2021 Nueva estructura de datos
    'Dim dtDC As New DataTable
    'Public Property IDMonedaPago As Integer

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Otros

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "COBRANZA CREDITO", "CCRE", CadenaConexion)
        vNuevo = False

        'Cotizacion
        txtMoneda.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
        txtMoneda.Inicializar()
        txtMoneda.txtCotizacion.SoloLectura = True

        'Controles
        OcxFormaPago1.Inicializar()
        OcxFormaPago1.Tipo = "CREDITO"
        OcxFormaPago1.Cotizacion = txtMoneda.txtCotizacion.Texto
        OcxFormaPago1.Form = Me

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.IDSucursal = cbxSucursal.GetValue
        CAsiento.IDMoneda = 1
        CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        OcxFormaPago1.Fecha = txtFecha.GetValue
        txtMoneda.txtCotizacion.Enabled = False
    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, cbxCobrador)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtPlanilla)
        'CSistema.CargaControl(vControles, cbxSucursal)
        'CSistema.CargaControl(vControles, dgw)
        CSistema.CargaControl(vControles, txtMoneda)
        CSistema.CargaControl(vControles, chkAnticipoCliente)

        'Ventas
        CSistema.CargaControl(vControles, btnAgregarVenta)
        CSistema.CargaControl(vControles, btnImportar)
        CSistema.CargaControl(vControles, lklEliminarVenta)

        'Forma de Pago
        CSistema.CargaControl(vControles, OcxFormaPago1)

        'CARGAR ESTRUCTURA DEL DETALLE VENTA
        dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Select Top(0) * From VVentaDetalleCobranzaCredito ", CadenaConexion).Clone

        'CARGAR ESTRUCTURA DE VENTAS SI ES QUE SE ESTA
        dtVentasRetencion = CData.GetStructure("VFormaPagoDocumentoRetencion", "Select Top(0) 'Sel'='False', IDTransaccion, 'ID'=0, IDTransaccionVenta, 'ComprobanteVenta'='', 'FechaVenta'=GetDate(), 'TotalImpuesto'=0.00, 'Porcentaje'=0.00, 'Importe'=0.00, 'ComprobanteRetencion'='', 'Aplicado'='False' From FormaPagoDocumentoRetencion ")
        OcxFormaPago1.dtVentaRetencion = dtVentasRetencion.Clone

        'CARGAR ESTRUCTURA DE VENTAS SI ES QUE SE ESTA
        dtNotaCredito = CData.GetStructure("VNotaCredito", "Select Top(0) 'Sel'='False', IDTransaccion, 'ID'=0, 'NotaCredito'='', 'FechaVenta'=GetDate(),Saldo as 'Importe' From VNotaCredito")
        OcxFormaPago1.dtVentaNotaCredito = dtNotaCredito.Clone

        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2", CadenaConexion)

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion, CadenaConexion)

        'Cobrador
        CSistema.SqlToComboBox(cbxCobrador.cbx, "Select ID, Nombres From Cobrador where IdSucursal =" & cbxSucursal.cbx.SelectedValue & " Order By 2", CadenaConexion)

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'COBRADOR
        cbxCobrador.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "COBRADOR", "")

        'Ultimo Registro
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From VCobranzaCredito Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

        'Cobrador
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "COBRADOR", cbxCobrador.cbx.Text)

    End Sub

    Sub Nuevo()
        CSistema.SqlToComboBox(cbxCobrador.cbx, "Select ID, Nombres From Cobrador where IdSucursal =" & cbxSucursal.cbx.SelectedValue & " and Estado = 1 Order By 2", CadenaConexion)
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion & " and Estado = 1", CadenaConexion)

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Limpiar detalle
        dtDetalleCobranzaVenta.Rows.Clear()
        dtVentasPendientes.Rows.Clear()
        dtVentasRetencion.Rows.Clear()
        txtMoneda.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
        txtMoneda.Inicializar()
        txtMoneda.txtCotizacion.SoloLectura = True
        txtMoneda_CambioMoneda()


        ListarVentas()

        'Forma de Pago
        OcxFormaPago1.dtCheques.Rows.Clear()
        OcxFormaPago1.dtEfectivo.Rows.Clear()
        OcxFormaPago1.dtFormaPago.Rows.Clear()
        OcxFormaPago1.dtChequesTercero.Rows.Clear()
        OcxFormaPago1.dtTarjeta.Rows.Clear()
        OcxFormaPago1.dtVentaRetencion.Rows.Clear()
        OcxFormaPago1.ListarFormaPago()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        CAsiento.Limpiar()

        vNuevo = True

        'Cabecera
        'txtFecha.txt.Text = ""
        txtFecha.Hoy()
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()
        txtCliente.Clear()
        chkAnticipoCliente.Valor = False

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("(Select IsNull(MAX(Numero+1),1) From VCobranzaCredito Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ") ", CadenaConexion), Integer)
        txtComprobante.txt.Text = CSistema.ObtenerProximoNroComprobante(cbxTipoComprobante.cbx.SelectedValue, "VCobranzaCredito", "NroComprobante", cbxSucursal.cbx.SelectedValue, CadenaConexion)

        'Cliente
        'txtCliente.Conectar("Select Distinct C.ID, C.Referencia, C.RazonSocial, C.RUC, C.NombreFantasia, C.Direccion, C.Telefono, C.ListaPrecio, C.Descuento, C.Deuda, C.SaldoCredito, C.Vendedor, C.Promotor, C.Cobrador, C.Ciudad, C.Barrio, C.ZonaVenta, C.TieneSucursales, C.ClienteVario From VCliente C Where C.IDSucursal=" & cbxSucursal.GetValue & " Order By RazonSocial", CadenaConexion)
        txtCliente.Conectar("Select Distinct C.ID, C.Referencia, C.RazonSocial, C.RUC, C.NombreFantasia, C.Direccion, C.Telefono, C.ListaPrecio, C.Descuento, C.Deuda, C.SaldoCredito, C.Vendedor, C.Promotor, C.Cobrador, C.Ciudad, C.Barrio, C.ZonaVenta, C.TieneSucursales, C.ClienteVario From VCliente C ", CadenaConexion)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Bloquear fecha de la cobranza
        txtFecha.SoloLectura = CType(CSistema.ExecuteScalar("(Select IsNull(CobranzaCreditoBloquearFecha,'False') From Configuraciones Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ") ", CadenaConexion), Boolean)
        txtFecha.Enabled = Not CType(CSistema.ExecuteScalar("(Select IsNull(CobranzaCreditoBloquearFecha,'False') From Configuraciones Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ") ", CadenaConexion), Boolean)
        LimpiarControles()

        'Poner el foco en el proveedor
        cbxTipoComprobante.cbx.Focus()
        btnAplicarVenta.Enabled = False

        vReciboInterno = False

        If cbxTipoComprobante.Texto = "CONTADO" Then
            chkAnticipoCliente.chk.Checked = False
            chkAnticipoCliente.Enabled = False
        Else
            chkAnticipoCliente.Enabled = True
        End If

    End Sub

    Sub Cancelar()
        'Funciones
        btnAplicarVenta.Enabled = False
        CargarInformacion()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        'LimpiarControles()

        'Ultimo Registro
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()
        txtID.txt.SelectAll()

    End Sub


    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Ciudad
        If cbxCiudad.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "¡Seleccione correctamente la ciudad!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If
        If txtFecha.txt.Text = "  /  /" Then
            Dim mensaje As String = "¡Seleccione correctamente la fecha!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Cuando no es por anulacion
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            'Tipo Comprobante
            If cbxTipoComprobante.cbx.Text.Trim = "" Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                vReciboInterno = False
                Exit Function
            End If

            If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(cbxTipoComprobante, mensaje)
                ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            'Comprobante
            'If (txtComprobante.txt.Text.Trim.Length = 0) And (cbxTipoComprobante.Texto = "RECI" Or cbxTipoComprobante.Texto = "RECIBO" Or Or cbxTipoComprobante.Texto = "RECARE") Then
            '    If MessageBox.Show("Desea Generar Recibo Interno?", "Recibo Interno", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
            '        vReciboInterno = True
            '    Else
            '        Dim mensaje As String = "Ingrese un número de comprobante!"
            '        ctrError.SetError(btnGuardar, mensaje)
            '        ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            '        tsslEstado.Text = mensaje
            '        vReciboInterno = False
            '        Exit Function
            '    End If
            'End If
            If cbxTipoComprobante.cbx.SelectedValue <> "14" Then
                If MessageBox.Show("Desea Generar Recibo Interno?", "Recibo Interno", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                    vReciboInterno = True
                Else
                    vReciboInterno = False
                End If
            End If
            'If (txtComprobante.txt.Text.Trim.Length) = 0 And (cbxTipoComprobante.Texto = "CONTADO") Then
            '    Dim mensaje As String = "Ingrese un número de comprobante!"
            '    ctrError.SetError(btnGuardar, mensaje)
            '    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            '    tsslEstado.Text = mensaje
            '    vReciboInterno = False
            '    Exit Function
            'End If
            'Sucursal
            If cbxSucursal.cbx.SelectedValue = Nothing Then
                    Dim mensaje As String = "Seleccione correctamente la sucursal!"
                    ctrError.SetError(btnGuardar, mensaje)
                    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Function
                End If

                If cbxSucursal.cbx.Text.Trim = "" Then
                    Dim mensaje As String = "Seleccione correctamente la sucursal!"
                    ctrError.SetError(btnGuardar, mensaje)
                    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Function
                End If

                'Venta
                If OcxFormaPago1.dgw.Rows.Count = 0 And chkAnticipoCliente.Valor = False Then
                    Dim mensaje As String = "El documento debe tener comprobantes de pago!"
                    ctrError.SetError(btnGuardar, mensaje)
                    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Function
                End If

                If txtTotalVenta.ObtenerValor > 0 And chkAnticipoCliente.Valor = True Then
                    Dim mensaje As String = "El importe de Venta para un Anticipo debe ser Cero!"
                    ctrError.SetError(btnGuardar, mensaje)
                    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Function
                End If

                If txtTotalVenta.ObtenerValor <= 0 And chkAnticipoCliente.Valor = False Then
                    Dim mensaje As String = "El importe de los comprobantes de venta no es válido!"
                    ctrError.SetError(btnGuardar, mensaje)
                    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Function
                End If

                If txtSaldoTotal.ObtenerValor <> 0 And chkAnticipoCliente.Valor = False Then
                    Dim mensaje As String = "¡Los importes no son validos!"
                    ctrError.SetError(btnGuardar, mensaje)
                    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Function
                End If

                'Validar Cheques diferidos con facturas contado --Cheques de clientes
                If CType(CSistema.ExecuteScalar("(Select IsNull(ContadoChequeDiferido,'False') From Configuraciones Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ") ", CadenaConexion), Boolean) = False Then
                    Dim dtVentas As New DataTable
                    Dim dtChequesDiferidos As New DataTable
                    dtVentas = CData.FiltrarDataTable(dtDetalleCobranzaVenta, " Credito = 'false' and Sel = 'True' ").Copy
                    dtChequesDiferidos = CData.FiltrarDataTable(OcxFormaPago1.dtCheques, " Tipo = 'DIFERIDO' and Sel = 'True' ").Copy

                    If dtVentas.Rows.Count > 0 And dtChequesDiferidos.Rows.Count > 0 Then
                        Dim mensaje As String = "¡No se permite Cheques diferidos con Facturas Contado!"
                        ctrError.SetError(btnGuardar, mensaje)
                        ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                        tsslEstado.Text = mensaje
                        Exit Function
                    End If
                End If

                'Validar Cheques diferidos con facturas contado -- Cheques de terceros
                If CType(CSistema.ExecuteScalar("(Select IsNull(ContadoChequeDiferido,'False') From Configuraciones Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ") ", VGCadenaConexion), Boolean) = False Then
                    Dim dtVentas As New DataTable
                    Dim dtChequesDiferidos As New DataTable
                    dtVentas = CData.FiltrarDataTable(dtDetalleCobranzaVenta, " Credito = 'false' and Sel = 'True'  ").Copy
                    dtChequesDiferidos = CData.FiltrarDataTable(OcxFormaPago1.dtChequesTercero, " Tipo = 'DIFERIDO' and Sel = 'True' ").Copy

                    If dtVentas.Rows.Count > 0 And dtChequesDiferidos.Rows.Count > 0 Then
                        Dim mensaje As String = "¡No se permite Cheques diferidos con Facturas Contado!"
                        ctrError.SetError(btnGuardar, mensaje)
                        ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                        tsslEstado.Text = mensaje
                        Exit Function
                    End If
                End If
            End If

            ''Si es para anular
            'If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            '    If MessageBox.Show("¡Atención! Esto anulará permanentemente el registro. ¿Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
            '        Return True
            '    Else
            '        Return False
            '    End If

            'End If


            'Si va a anular
            If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            Dim frm As New frmAgregarMotivoAnulacionCobranza
            frm.Text = " Motivo Anulacion NC"
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.ShowDialog(Me)
            IDMotivoAnulacion = frm.IDMotivo
            If IDMotivoAnulacion = 0 Then
                Exit Function
            End If
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        Return True


    End Function

    Public Function CalcularDiferenciaCambio(ByVal valor As String) As Integer
        Dim Totalventa As Integer = 0
        Dim TotalFormapago As Integer = 0
        'Dim DiferenciaCambio As Integer = 0

        For Each oRow As DataRow In dtDetalleCobranzaVenta.Rows
            If oRow("Sel") = True Then
                Totalventa = Totalventa + (oRow("cotizacion") * oRow("Importe"))

                'DiferenciaCambio = txtCantidad.ObtenerValor
            End If
        Next

        For Each oRow1 As DataRow In OcxFormaPago1.dtFormaPago.Rows
            If oRow1("Moneda") <> "GS" Then
                TotalFormapago = TotalFormapago + (oRow1("Cambio") * oRow1("Importemoneda"))
            Else
                TotalFormapago = TotalFormapago + oRow1("ImporteMoneda")
            End If
            'Totalventa = OcxFormaPago1.Total
            'TotalFormapago = txtTotalVenta.txt.Text
            valor = Totalventa - TotalFormapago

            'If valor < 0 Then
            '    valor = valor * (-1)
            'End If

        Next

        DiferenciaCambio = valor

        Return valor


    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)
        tsslEstado.Text = ""
        ctrError.Clear()

        'Generar la cabecera del asiento
        'GenerarAsiento()f

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        If chkAnticipoCliente.chk.Checked = False Then
            CalcularDiferenciaCambio(DiferenciaCambio)
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", CInt(txtID.ObtenerValor), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaEmision", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Comprobante", txtComprobante.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCobrador", cbxCobrador.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroPlanilla", CInt(txtPlanilla.ObtenerValor), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", txtMoneda.Registro("ID"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoNumeroBaseDatos(txtMoneda.Registro("Cotizacion"), True), ParameterDirection.Input)

        'Totales
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(OcxFormaPago1.TotalOper, vdecimalOperacion), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalImpuesto", 0, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", 0, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDescuento", 0, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@DiferenciaCambio", CSistema.FormatoNumeroBaseDatos(txtDiferenciaCambio.txt.Text, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DiferenciaCambio", CSistema.FormatoNumeroBaseDatos(DiferenciaCambio, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@AnticipoCliente", chkAnticipoCliente.Valor, ParameterDirection.Input)
        If chkAnticipoCliente.Valor = True Then
            CSistema.SetSQLParameter(param, "@IDTipoAnticipo", vIDTipoAnticipo, ParameterDirection.Input)
        End If
        CSistema.SetSQLParameter(param, "@ReciboInterno", vReciboInterno, ParameterDirection.Input)
        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDMotivoAnulacion", IDMotivoAnulacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)
        CCaja.IDSucursal = cbxSucursal.cbx.SelectedValue
        CCaja.ObtenerUltimoNumero()

        CSistema.SetSQLParameter(param, "@IDTransaccionCaja", CCaja.IDTransaccion, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpCobranzaCredito", False, False, MensajeRetorno, IDTransaccion, False, CadenaConexion, 6000) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar la operacion solo si no es para ANULAR
            If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
                If IDTransaccion <> 0 Then
                    GoTo Eliminar
                Else
                    Exit Sub
                End If
            End If

        End If

        Dim Procesar As Boolean = True

        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
            CargarOperacion(IDTransaccion)
            txtID.SoloLectura = False
            Exit Sub
        End If

        If IDTransaccion > 0 Then

            'Cargamos el Detalle de VentasCobranza
            Procesar = InsertarDetalleVentas(IDTransaccion)

            'Si hubo un error, eliminar todo
            If Procesar = False Then
                GoTo Eliminar
            End If

            Procesar = InsertarFormaPago(IDTransaccion)

            'Si hubo un error, eliminar todo
            If Procesar = False Then
                GoTo Eliminar
            End If

        End If

        'Aplicar el recibo
        Try

            ReDim param(-1)

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "True", ParameterDirection.Output)

            'Aplicar la Cobranza
            If CSistema.ExecuteStoreProcedure(param, "SpCobranzaCreditoProcesar", False, False, MensajeRetorno, "", False, CadenaConexion, 6000) = False Then
                MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                GoTo Eliminar
            End If


            'Verificar si se proceso
            If param(param.Length - 1).Value = False Then
                MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                GoTo Eliminar
            End If

        Catch ex As Exception
            If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
                GoTo Eliminar
            End If
        End Try

        'SC 10-08-2021 Nueva Rutina que selecciona DebitoCreditoBancario para Extorno
        'SeleccionaDebitoCreditoBancario(IDTransaccion)


        If vReciboInterno Then
            Imprimir(vReciboInterno)
        End If


        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        vReciboInterno = False
        txtID.SoloLectura = False
        txtID.txt.Focus()

        Exit Sub

Eliminar:

        Eliminar(IDTransaccion)

    End Sub

    Sub Eliminar(IDTransaccion As Integer)

        tsslEstado.Text = ""
        ctrError.Clear()

        Dim param(-1) As SqlClient.SqlParameter
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "DEL", ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpCobranzaCreditoProcesar", True, True, MensajeRetorno, "", False, CadenaConexion) = False Then
            Exit Sub
        End If

        MessageBox.Show("El sistema elimino el registro por motivos de errores en la carga! Vuelva a intentarlo!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)

    End Sub

    Sub PreCargar()
        'Validar si es RECIBO y Nro de Recibo interno
        If vNuevo Then
            Dim comp As String = txtComprobante.txt.Text.Trim
            If comp <> "" Then
                If comp.Length > 3 Then
                    If comp.Substring(0, 4) <> "020-" Then
                        Exit Sub
                    End If
                End If
                If (cbxTipoComprobante.Texto = "RECI" Or cbxTipoComprobante.Texto = "RECIBO") Then
                    'Verificar si existe como Recibo Interno
                    Dim dtRecibo As DataTable
                    '04/1/2022 SC: Agregado Anulado = 0 para asociar a cobranza
                    If CType(CSistema.ExecuteScalar("Select count(*) From vReciboVenta where Anulado = 0 and Documento='" & txtComprobante.txt.Text.Trim & "'", VGCadenaConexion), Integer) > 0 Then

                        dtRecibo = CSistema.ExecuteToDataTable("Select top 1 * From vReciboVenta Where Anulado = 0 and Documento='" & txtComprobante.txt.Text.Trim & "'", CadenaConexion)
                        If dtRecibo Is Nothing Then
                            txtComprobante.txt.Text = ""
                            Exit Sub
                        End If
                        For Each oRow As DataRow In dtRecibo.Rows
                            txtCliente.SetValue(CSistema.RetornarValorInteger(oRow("IDCliente").ToString))
                            'txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
                            'Moneda
                            txtMoneda.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
                            txtMoneda.Inicializar()
                            txtMoneda.txtCotizacion.SoloLectura = True
                            txtMoneda.cbxMoneda.Texto = oRow("Moneda").ToString
                            txtMoneda_CambioMoneda()
                            'Cuando no es Boleta contra Boleta cargar 1 Factura
                            '   If CBool(oRow("Boleta").ToString) = False Then
                            CargarVentaDesdeRecibo(CSistema.RetornarValorInteger(oRow("IDTransaccionVenta").ToString))
                            '  End If
                            ''Cuando es Boleta contra Boleta cargar 5 Facturas
                            ' If CBool(oRow("Boleta").ToString) = True Then
                            '    CargarVentaBoletaContraBoleta()
                            ' End If
                        Next
                    Else
                        Exit Sub
                    End If
                End If
            End If
        End If
    End Sub

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)



        tsslEstado.Text = ""
        ctrError.Clear()



        If ValidarDocumento(Operacion) = False Then
            txtID.Focus()
            Exit Sub
        End If



        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer



        ''SetSQLParameter, ayuda a generar y configurar los parametros.
        'CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        'IndiceOperacion = param.GetLength(0) - 1



        ''Transaccion
        'CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)



        ''Informacion de Salida
        'CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        'CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        'CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)



        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If



        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", CInt(txtID.ObtenerValor), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaEmision", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Comprobante", txtComprobante.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCobrador", cbxCobrador.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroPlanilla", CInt(txtPlanilla.ObtenerValor), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMotivoAnulacion", IDMotivoAnulacion, ParameterDirection.Input)
        'Totales
        CSistema.SetSQLParameter(param, "@Total", OcxFormaPago1.Total, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalImpuesto", 0, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", 0, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDescuento", 0, ParameterDirection.Input)



        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1



        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)



        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)



        Dim MensajeRetorno As String = ""



        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpCobranzaCredito", False, False, MensajeRetorno, IDTransaccion, False, CadenaConexion, 6000) = False Then
            MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If



        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)



        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False




    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        Dim Comprobante As String = cbxTipoComprobante.cbx.Text & ": " & txtComprobante.txt.Text.Trim

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante
            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From vCobranzaCredito Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & "), 0 )", CadenaConexion)
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else

            'Validar
            If cbxCiudad.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la ciudad de operacion!"
                ctrError.SetError(cbxCiudad, mensaje)
                ctrError.SetIconAlignment(cbxCiudad, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante

            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            frm.CalcularTotales()

            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
            CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            CAsiento.NroCaja = frm.CAsiento.NroCaja
            CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                CAsiento.dtAsiento.Clear()
                CAsiento.dtDetalleAsiento.Clear()
                VisualizarAsiento()
            End If

        End If

    End Sub

    Sub GenerarAsiento()

        'EstablecerCabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = cbxCiudad.GetValue
        oRow("IDSucursal") = cbxSucursal.GetValue
        oRow("Fecha") = txtFecha.GetValue
        oRow("IDMoneda") = 1
        oRow("Cotizacion") = 1
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = txtComprobante.txt.Text.Trim
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text.Trim
        oRow("Detalle") = txtObservacion.txt.Text
        oRow("Total") = txtTotalVenta.ObtenerValor

        CAsiento.IDMoneda = txtMoneda.Registro("ID")
        CAsiento.IDSucursal = cbxSucursal.GetValue

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)

        'Solo establecer la primera vez, esto es para el detalle
        If CAsiento.Generado = True Then
            Exit Sub
        End If
        CAsiento.InicializarAsiento()
        CAsiento.dtVenta = CData.FiltrarDataTable(dtDetalleCobranzaVenta, " Sel = 'True' ").Copy
        CAsiento.dtCheque = CData.FiltrarDataTable(OcxFormaPago1.dtCheques, " Sel = 'True' ").Copy
        CAsiento.dtEfectivo = OcxFormaPago1.dtEfectivo.Copy
        CAsiento.dtDocumento = OcxFormaPago1.dtDocumento.Copy
        CAsiento.dtTarjeta = OcxFormaPago1.dtTarjeta.Copy

        'CAsiento.TotalDiferenciaCambio = CSistema.FormatoMoneda(txtDiferenciaCambio.txt.Text, False)
        CAsiento.TotalDiferenciaCambio = CSistema.FormatoMoneda(txtDiferenciaCambio.txt.Text, False)

        CAsiento.Total = txtTotalVenta.ObtenerValor

        CAsiento.Generar()

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VCobranzaCredito Where IDSucursal=" & cbxSucursal.GetValue, CadenaConexion), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VCobranzaCredito Where IDSucursal=" & cbxSucursal.GetValue, CadenaConexion), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Function InsertarDetalleVentas(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalleVentas = True

        For Each oRow As DataRow In dtDetalleCobranzaVenta.Rows

            If oRow("Sel") = True Then

                Dim sql As String = "Insert Into VentaCobranza(IDTransaccionVenta, IDTransaccionCobranza, Importe, ImporteGs, Cancelar) Values(" & oRow("IDTransaccion").ToString & ", " & IDTransaccion & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, vdecimalOperacion) & ", " & CSistema.FormatoMonedaBaseDatos((oRow("Importe").ToString * txtMoneda.Registro("Cotizacion")), False) & ", 'False')"

                If CSistema.ExecuteNonQuery(sql, CadenaConexion) = 0 Then
                    Return False
                End If

            End If

        Next

    End Function

    Function InsertarFormaPago(ByVal IDTransaccion As Integer) As Boolean

        InsertarFormaPago = True

        Dim dtEfectivo As DataTable
        Dim dtCheque As DataTable
        Dim dtDocumento As DataTable
        Dim dtTarjeta As DataTable
        Dim dtVentaRetencion As DataTable
        Dim dtNotaCredito As DataTable

        Dim ID As Integer = 1

        dtEfectivo = OcxFormaPago1.dtEfectivo.Copy
        dtCheque = OcxFormaPago1.dtCheques.Copy
        dtDocumento = OcxFormaPago1.dtDocumento.Copy
        dtTarjeta = OcxFormaPago1.dtTarjeta.Copy
        dtVentaRetencion = OcxFormaPago1.dtVentaRetencion.Copy
        dtNotaCredito = OcxFormaPago1.dtVentaNotaCredito.Copy

        Dim IDMoneda As Integer = 0
        Dim Decimales As Boolean = False
        'Cargar el Efectivo
        For Each oRow As DataRow In dtEfectivo.Rows
            Try
                IDMoneda = CSistema.RetornarValorInteger(oRow("IDMoneda").ToString)
                Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & IDMoneda, "VMoneda")("Decimales"))

            Catch ex As Exception

            End Try

            Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe,Cotizacion) values(" & IDTransaccion & ", " & ID & ", 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales) & ", 'False', 0, NULL, NULL, 'False', 0, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("cotizacion").ToString) & ")"
            Dim sqlEfectivo As String = "Insert Into Efectivo(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, VentasCredito, VentasContado, Gastos, IDMoneda, ImporteMoneda, Cotizacion, Observacion, Depositado, Cancelado, Saldo,ImporteHabilitado) values(" & IDTransaccion & ", " & ID & ", " & cbxSucursal.cbx.SelectedValue & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("Comprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True) & "', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", 0, 0, " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString) & ", '" & oRow("Observacion").ToString & "', 0, 'False', " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales) & ")"


            If CSistema.ExecuteNonQuery(sql, CadenaConexion) = 0 Then
                Return False
            End If

            If CSistema.ExecuteNonQuery(sqlEfectivo, CadenaConexion) = 0 Then
                Return False
            End If

            ID = ID + 1

        Next

        'Cargar el Documento
        For Each oRow As DataRow In dtDocumento.Rows
            IDMoneda = CSistema.RetornarValorInteger(oRow("IDMoneda").ToString)
            Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & IDMoneda, "VMoneda")("Decimales"))

            Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe,cotizacion) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, 0, 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales) & ", " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString) & ")"
            'Dim sqlDocumento As String = "Insert Into FormaPagoDocumento(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, IDMoneda, ImporteMoneda, Cotizacion, Total, Saldo ,Observacion) values(" & IDTransaccion & ", " & ID & ", " & cbxSucursal.cbx.SelectedValue & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("Comprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True) & "', " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", '" & oRow("Observacion").ToString & "')"
            Dim sqlDocumento As String = "Insert Into FormaPagoDocumento(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, IDMoneda, ImporteMoneda, Cotizacion, Total, Saldo ,Observacion, IDBanco) values(" & IDTransaccion & ", " & ID & ", " & cbxSucursal.cbx.SelectedValue & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("Comprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True) & "', " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales) & "," & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales) & ", '" & oRow("Observacion").ToString & "', " & oRow("IDBanco").ToString & ")"

            If CSistema.ExecuteNonQuery(sql, CadenaConexion) = 0 Then
                Return False
            End If

            If CSistema.ExecuteNonQuery(sqlDocumento, CadenaConexion) = 0 Then
                Return False
            End If

            'Cargar el Documento retencion
            'Solo si el tipo de comprobante es de Retencion
            If oRow("IDTipoComprobante").ToString = vgConfiguraciones("CobranzaCreditoTipoComprobanteRetencion") Then

                For Each oRow1 As DataRow In dtVentaRetencion.Select("Sel='True' And ID=" & oRow("ID").ToString & " And ComprobanteRetencion='" & oRow("Comprobante").ToString & "' ")
                    'dbs ver importeMoneda
                    Dim sql1 As String = "Insert Into FormaPagoDocumentoRetencion(IDTransaccion, ID, IDTransaccionVenta, Porcentaje, Importe) values(" & IDTransaccion & ", " & ID & ", " & oRow1("IDTransaccionVenta") & ", '" & oRow1("Porcentaje").ToString & "', " & CSistema.FormatoMonedaBaseDatos(oRow1("Importe").ToString, Decimales) & ")"

                    If CSistema.ExecuteNonQuery(sql1, CadenaConexion) = 0 Then
                        Return False
                    End If

                Next

            End If

            'Solo para casos de forma de pago NC
            If oRow("IDTipoComprobante").ToString = "154" Then

                For Each oRow1 As DataRow In dtNotaCredito.Select("Sel='True'")
                    'dbs ver importeMoneda
                    Dim sql1 As String = "Update NotaCredito Set Saldo = Saldo - " & oRow1("Importe").ToString & "Where IDTransaccion = " & oRow1("IDTransaccion").ToString

                    If CSistema.ExecuteNonQuery(sql1, CadenaConexion) = 0 Then
                        Return False
                    End If

                Next

            End If

            ID = ID + 1

        Next

        'Cargar el Cheque
        For Each oRow As DataRow In dtCheque.Rows

            If CBool(oRow("Sel").ToString) = True Then
                IDMoneda = CSistema.RetornarValorInteger(oRow("IDMoneda").ToString)
                Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & IDMoneda, "VMoneda")("Decimales"))

                'Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, CancelarCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe,cotizacion) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & ", " & oRow("IDTransaccion").ToString & ", NULL, '" & oRow("Cancelar").ToString & "' ,'False', NULL, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteGS").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("cotizacionHoy").ToString) & ")"
                Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, CancelarCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe,cotizacion) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & ", " & oRow("IDTransaccion").ToString & ", NULL, '" & oRow("Cancelar").ToString & "' ,'False', NULL, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteGS").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("cotizacion").ToString) & ")"
                If CSistema.ExecuteNonQuery(sql, CadenaConexion) = 0 Then
                    Return False
                End If

                ID = ID + 1

            End If

        Next

        Dim dtChequeTercero As DataTable
        dtChequeTercero = OcxFormaPago1.dtChequesTercero.Copy

        'Cargar el Cheque
        For Each oRow As DataRow In dtChequeTercero.Rows

            If CBool(oRow("Sel").ToString) = True Then

                Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Importe,cotizacion) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & oRow("IDTransaccion").ToString & ", NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("cotizacionHoy").ToString) & ")"

                If CSistema.ExecuteNonQuery(sql, CadenaConexion) = 0 Then
                    Return False
                End If

                ID = ID + 1

            End If

        Next

        'Cargar la tarjeta
        For Each oRow As DataRow In dtTarjeta.Rows
            IDMoneda = CSistema.RetornarValorInteger(oRow("IDMoneda").ToString)
            Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & IDMoneda, "VMoneda")("Decimales"))

            Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe,Cotizacion) values(" & IDTransaccion & ", " & ID & ", 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales) & ", " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales) & ", 'False', 0, NULL, NULL, 'False', 0, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("cotizacion").ToString) & ")"
            'Dim sqlTarjeta As String = "Insert Into FormaTarjeta(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, IDMoneda, ImporteMoneda, Cotizacion, Total, Saldo ,Observacion, TerminacionTarjeta, FechaVencimientoTarjeta, IDTipoTarjeta, TipoComprobante, Moneda, Importe) values(" & IDTransaccion & ", " & ID & ", " & cbxSucursal.cbx.SelectedValue & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("Comprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True) & "', " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", '" & oRow("Observacion").ToString & ", '" & oRow("TerminacionTarjeta").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("FechaVencimientoTarjeta").ToString, True, True) & ", '" & oRow("IDTipoTarjeta").ToString & ", '" & oRow("TipoComprobante").ToString & ", '" & oRow("Moneda").ToString & ", '" & oRow("Importe").ToString & "')"
            Dim sqlTarjeta As String = String.Empty
            Try
                sqlTarjeta = _
               String.Format("Insert Into FormaPagoTarjeta(IDTransaccion, ID, " & _
                                                   "IDSucursal, IDTipoComprobante, Comprobante, " & _
                                                   "Fecha, IDMoneda, ImporteMoneda, Cotizacion, Total, " & _
                                                   "Saldo, Observacion, TerminacionTarjeta, " & _
                                                   "FechaVencimientoTarjeta, IDTipoTarjeta, TipoComprobante, " & _
                                                     "Moneda, Importe, Boleta, cancelado) values({0},{1},{2},{3},'{4}','{5}',{6},{7},{8},{9},{10},'{11}','{12}','{13}',{14},'{15}','{16}',{17},'{18}','{19}')", _
                                                    IDTransaccion, _
                                                    ID, cbxSucursal.cbx.SelectedValue, _
                                                    oRow("IDTipoComprobante").ToString, _
                                                    oRow("Comprobante").ToString, _
                                                    CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True), _
                                                    oRow("IDMoneda").ToString, _
                                                    CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales), _
                                                    CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString), _
                                                    CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString), _
                                                    CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString), _
                                                    oRow("Observacion").ToString, _
                                                    oRow("TerminacionTarjeta").ToString, _
                                                    CSistema.FormatoFechaBaseDatos(oRow("FechaVencimientoTarjeta").ToString, True, True), _
                                                    oRow("IDTipoTarjeta").ToString, _
                                                    oRow("TipoComprobante").ToString, oRow("Moneda").ToString, _
                                                    CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString, Decimales), oRow("Boleta").ToString, 0)
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try


            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            If CSistema.ExecuteNonQuery(sqlTarjeta) = 0 Then
                Return False
            End If

            ID = ID + 1

        Next

    End Function

    Function InsertarDetalleVenta(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalleVenta = False

    End Function

    Sub ObtenerVentasClientes()

        If txtCliente.Seleccionado = True Then
            'dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Select * From VVentasPendientes Where IDCliente=" & txtCliente.Registro("ID").ToString & " And Saldo > 0", CadenaConexion).Copy
            dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Execute SpViewVentasPendientes @IDCliente = " & txtCliente.txtID.ObtenerValor & ", @IDMoneda = '" & txtMoneda.Registro("ID") & "' , @FechaCotizacion = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' ", CadenaConexion)
        End If

        ListarVentas()

    End Sub

    Sub CargarVenta()

        'If dtDetalleCobranzaVenta.Rows.Count = 0 And txtCliente.txtID.txt.Text = "" Then
        '    dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable(String.Format("Execute SpViewVentasPendientes @IDCliente = 0, 
        '                                                                                                        @IDMoneda = '{0}' , 
        '                                                                                                        @FechaCotizacion = '{1}' ", txtMoneda.Registro("ID"), CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)), CadenaConexion)
        'End If
        Dim Contado As Boolean = False
        If cbxTipoComprobante.Texto = "CONTADO" Then
            Contado = True
        End If

        If txtCliente.Seleccionado = True Then
            dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Execute SpViewVentasPendientes @IDCliente = " & txtCliente.txtID.ObtenerValor & ", 
                                                                                                 @IDMoneda = '" & txtMoneda.Registro("ID") & "' , 
                                                                                                 @Contado = '" & Contado & "',
                                                                                                 @FechaCotizacion = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' ", CadenaConexion)
        Else
            MessageBox.Show("Seleccione un Cliente antes de continuar", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Dim frm As New frmCobranzaCreditoSeleccionarVentas
        frm.decimales = vdecimalOperacion
        frm.Text = "Seleccion de Comprobantes pendiente de Cobro"
        frm.Cliente = txtCliente.txtRazonSocial.Text
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtDetalleCobranzaVenta
        frm.ShowDialog(Me)
        dtDetalleCobranzaVenta = frm.dt

        ListarVentas()

        OcxFormaPago1.btnEfectivo.Focus()
    End Sub

    Function ValidarArchivo(ByVal dtImportado As DataTable) As Boolean

        ValidarArchivo = False

        If dtImportado Is Nothing Then
            MessageBox.Show("El formato del archivo no es correcto! No fue encontrado la hoja: 'Importar'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        'Obtener muestra
        Dim vdt As DataTable = CSistema.ExecuteToDataTable("Select 'Comprobante'='','Fecha Emisión'='','Comprobante Venta'='','Retenido IVA'=''")
        Dim NoEncontrado As Boolean = True

        'Controlar las columnas
        For c1 As Integer = 0 To vdt.Columns.Count - 1
            NoEncontrado = True
            For c2 As Integer = 0 To dtImportado.Columns.Count - 1
                If dtImportado.Columns(c2).ColumnName = vdt.Columns(c1).ColumnName Then
                    NoEncontrado = False
                    GoTo seguir
                End If
            Next

seguir:
            If NoEncontrado = True Then
                MessageBox.Show("El formato del archivo no es correcto! No fue encontrado la columna: " & vdt.Columns(c1).ColumnName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Return False
            End If
        Next

        Return True

    End Function

    Function ExecuteToDataTableXLS(ByVal consulta As String, ByVal Archivo As String, Optional ByVal vConexion As String = "") As DataTable

        ExecuteToDataTableXLS = Nothing
        Try
            If vConexion <> "" Then
                VGCadenaConexion = vConexion
            End If
            '            Dim MiConexion As New System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Archivo & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'")
            Dim MiConexion As New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Archivo & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'")
            Dim MiAdaptador As New System.Data.OleDb.OleDbDataAdapter(consulta, MiConexion)
            Dim MiDataSet As New DataSet()
            Dim MiEnlazador As New BindingSource

            Dim commandbuilder As New OleDb.OleDbCommandBuilder(MiAdaptador)
            MiConexion.Open()
            MiAdaptador.Fill(MiDataSet)
            Return MiDataSet.Tables(0)

        Catch ex As Exception
            'XtraMessageBox.Show("Verifique el formato del archivo!", "No es posible importar.", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return Nothing
        End Try


    End Function

    'Function ExecuteToDataTableXLS(ByVal consulta As String, ByVal Archivo As String, Optional ByVal vConexion As String = "") As DataTable

    '    ExecuteToDataTableXLS = Nothing
    '    Try
    '        If vConexion <> "" Then
    '            VGCadenaConexion = vConexion
    '        End If

    '        Dim MiConexion As New System.Data.OleDb.OleDbConnection("Provider= Microsoft.Jet.OLEDB.4.0;Data Source=" & Archivo & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'")
    '        Dim MiAdaptador As New System.Data.OleDb.OleDbDataAdapter(consulta, MiConexion)
    '        Dim MiDataSet As New DataSet()
    '        Dim MiEnlazador As New BindingSource

    '        Dim commandbuilder As New OleDb.OleDbCommandBuilder(MiAdaptador)
    '        MiConexion.Open()
    '        MiAdaptador.Fill(MiDataSet)
    '        Return MiDataSet.Tables(0)

    '    Catch ex As Exception
    '        'XtraMessageBox.Show("Verifique el formato del archivo!", "No es posible importar.", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        Return Nothing
    '    End Try


    'End Function

    'Function ExecuteToDataTableXLS2(ByVal consulta As String, ByVal Archivo As String, Optional ByVal vConexion As String = "") As DataTable

    '    ExecuteToDataTableXLS2 = Nothing
    '    Try
    '        If vConexion <> "" Then
    '            VGCadenaConexion = vConexion
    '        End If

    '        Dim MiConexion As New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Archivo & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'")
    '        Dim MiAdaptador As New System.Data.OleDb.OleDbDataAdapter(consulta, MiConexion)
    '        Dim MiDataSet As New DataSet()
    '        Dim MiEnlazador As New BindingSource

    '        Dim commandbuilder As New OleDb.OleDbCommandBuilder(MiAdaptador)
    '        MiConexion.Open()
    '        MiAdaptador.Fill(MiDataSet)
    '        Return MiDataSet.Tables(0)

    '    Catch ex As Exception
    '        'XtraMessageBox.Show("Verifique el formato del archivo!", "No es posible importar.", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        Return Nothing
    '    End Try


    'End Function

    'Function ExecuteToDataTableXLSX(ByVal consulta As String, ByVal Archivo As String, Optional ByVal vConexion As String = "") As DataTable

    '    ExecuteToDataTableXLSX = Nothing
    '    Try
    '        If vConexion <> "" Then
    '            VGCadenaConexion = vConexion
    '        End If
    '        Dim MiConexion As New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Archivo & ";Extended Properties='Excel 12.0';")
    '        'Dim MiConexion As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Archivo & "';Extended Properties='Excel 12.0';"
    '        Dim MiAdaptador As New System.Data.OleDb.OleDbDataAdapter(consulta, MiConexion)
    '        Dim MiDataSet As New DataSet()
    '        Dim MiEnlazador As New BindingSource

    '        Dim commandbuilder As New OleDb.OleDbCommandBuilder(MiAdaptador)
    '        MiConexion.Open()
    '        MiAdaptador.Fill(MiDataSet)
    '        Return MiDataSet.Tables(0)


    '    Catch ex As Exception
    '        'XtraMessageBox.Show("Verifique el formato del archivo!", "No es posible importar.", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        Return Nothing
    '    End Try


    'End Function

    Sub Importar()

        dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Execute SpViewVentasPendientes @IDCliente = " & txtCliente.txtID.ObtenerValor & ", 
                                                                                                 @IDMoneda = '" & txtMoneda.Registro("ID") & "' , 
                                                                                                 @FechaCotizacion = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' ", CadenaConexion)

        Dim dt As DataTable
        Dim txt As New OpenFileDialog
        'txt.Filter = "*.xls | *.xlsx"
        txt.Filter = "*.xls |"
        If txt.ShowDialog <> DialogResult.OK Then
            Me.Close()
        End If


        dt = ExecuteToDataTableXLS("Select * from [Datos$]", txt.FileName)
        'If dt Is Nothing Then
        '    dt = ExecuteToDataTableXLSX("Select CI, Sueldo, DiasTrabajados,DiasVacaciones,HorasExtras100, CantidadHorasExtras100, HorasExtras50, CantidadHorasExtras50,HorasExtras30, CantidadHorasExtras30,Colacion,Comision,BonificacionFamiliar,GratificacionUnicaYEspecial,Descuentos, OtrasRemuneraciones, Vacaciones, Anticipos,AporteIPSEmpleado, AporteIPSPatronal, Aguinaldo from [Importar$]", txt.FileName)
        'End If

        'If dt Is Nothing Then
        '    dt = ExecuteToDataTableXLS2("Select CI, Sueldo, DiasTrabajados,DiasVacaciones,HorasExtras100, CantidadHorasExtras100, HorasExtras50, CantidadHorasExtras50,HorasExtras30, CantidadHorasExtras30,Colacion,Comision,BonificacionFamiliar,GratificacionUnicaYEspecial,Descuentos, OtrasRemuneraciones, Vacaciones, Anticipos,AporteIPSEmpleado, AporteIPSPatronal, Aguinaldo from [Importar$]", txt.FileName)
        'End If
        If ValidarArchivo(dt) = False Then
            Me.Close()
            Exit Sub
        End If
        Dim RUCSinDV As String = ""
        Dim lengDV As String = txtCliente.Registro("RUC").ToString.IndexOf("-")
        RUCSinDV = txtCliente.Registro("RUC").ToString.Substring(0, lengDV)


        For Each Row In dt.Select("[RUC Informante] = '" & RUCSinDV & "'")
            Dim Comprobante As String() = Row("Comprobante Venta").ToString.Split("-")
            Dim NroComprobante As String = Comprobante(2)
            Dim Nro As Integer = CInt(NroComprobante)
            'For Each oRow As DataRow In dtDetalleCobranzaVenta.Select("Comprobante = '" & Row("Comprobante Venta") & "'")
            For Each oRow As DataRow In dtDetalleCobranzaVenta.Select("NroComprobante = " & Nro)
                oRow("Sel") = True
                oRow("Importe") = Row("Retenido IVA")

            Next

        Next
        ListarVentas()

        OcxFormaPago1.dtVentaRetencion.Rows.Clear()

        For Each oRow As DataRow In dtDetalleCobranzaVenta.Select("Sel='True'")
            Dim NewRow As DataRow = OcxFormaPago1.dtVentaRetencion.NewRow
            NewRow("Aplicado") = False
            NewRow("Sel") = False
            NewRow("IDTransaccion") = 0
            NewRow("ID") = 0
            NewRow("ComprobanteRetencion") = ""
            NewRow("IDTransaccionVenta") = oRow("IDTransaccion")
            NewRow("ComprobanteVenta") = oRow("Comprobante")
            NewRow("FechaVenta") = oRow("FechaEmision")
            NewRow("Porcentaje") = vgConfiguraciones("CompraPorcentajeRetencion")
            NewRow("TotalImpuesto") = CSistema.FormatoMoneda(oRow("TotalImpuesto"), vdecimalOperacion)

            If oRow("Total") > vgConfiguraciones("CompraImporteMinimoRetencion") Then
                NewRow("Sel") = True
                NewRow("Importe") = CSistema.FormatoMoneda(NewRow("TotalImpuesto") * (NewRow("Porcentaje") / 100), vdecimalOperacion)
            Else
                NewRow("Importe") = 0
            End If

            OcxFormaPago1.dtVentaRetencion.Rows.Add(NewRow)

        Next
        Dim Contador As Integer = 0
        For Each RetRow In dt.Select("[RUC Informante] = '" & RUCSinDV & "'")
            Dim Comprobante As String() = RetRow("Comprobante Venta").ToString.Split("-")
            Dim NroComprobante As String = Comprobante(2)
            Dim Nro As Integer = CInt(NroComprobante)
            'For Each oRow As DataRow In dtDetalleCobranzaVenta.Select("Comprobante = '" & Row("Comprobante Venta") & "'")
            For Each oRow As DataRow In dtDetalleCobranzaVenta.Select("NroComprobante = " & Nro)
                OcxFormaPago1.ImportarRetencion(RetRow, Contador)
            Next
            Contador = Contador + 1

        Next


    End Sub





    Sub CargarVentaDesdeRecibo(ByVal IDTransaccion As Integer)

        If txtCliente.Seleccionado = True Then
            dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Execute SpViewVentasPendientesRecibo @IDCliente = " & txtCliente.txtID.ObtenerValor & ", @IDMoneda = '" & txtMoneda.Registro("ID") & "' , @FechaCotizacion = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "', @IDTransaccion = " & IDTransaccion, CadenaConexion)
        End If
        'dtFacturasDesdeRecibo = 

        ListarVentas()

        OcxFormaPago1.btnEfectivo.Focus()
    End Sub
    Sub CargarVentaBoletaContraBoleta()

        If txtCliente.Seleccionado = True Then
            dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Execute SpViewVentasPendientesBoletaContraBoleta @IDCliente = " & txtCliente.txtID.ObtenerValor & ", @IDMoneda = '" & txtMoneda.Registro("ID") & "' , @FechaCotizacion = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "'", CadenaConexion)
        End If
        'dtFacturasDesdeRecibo = 

        ListarVentas()

        OcxFormaPago1.btnEfectivo.Focus()
    End Sub


    Sub AplicarVentas()
        Dim frm As New frmAplicacionVentasAnticipo
        frm.Saldo = txtSaldoTotal.ObtenerValor
        frm.IDTransaccionCobranza = IDTransaccion
        frm.Moneda = txtMoneda.cbxMoneda.Texto
        frm.IDMoneda = txtMoneda.cbxMoneda.GetValue
        frm.Cotizacion = txtMoneda.txtCotizacion.ObtenerValor
        frm.IDCliente = vIdcliente
        frm.NroPlanilla = txtPlanilla.txt.Text
        frm.Text = "Aplicacion de ventas a Anticipo de cliente"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog(Me)
    End Sub

    Sub CargarVenta(ByVal IDTransaccion As Integer)

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra las ventas asociadas!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        'Limpiar ventas seleccionadas
        dtDetalleCobranzaVenta.Rows.Clear()
        dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Select *, 'Sel'='True' From VVentaDetalleCobranzaCredito Where IDTransaccionCobranza=" & IDTransaccion, CadenaConexion).Copy
        ListarVentas()

    End Sub

    Sub ListarVentas()

        dgw.Rows.Clear()

        Dim Total As Decimal = 0
        Dim TotalOper As Decimal = 0.0
        Dim DiferenciaCambio As Decimal = 0.0
        For Each oRow As DataRow In dtDetalleCobranzaVenta.Rows

            If oRow("Sel") = True Then

                Dim Registro(11) As String
                Registro(0) = oRow("Comprobante").ToString
                Registro(1) = oRow("Cliente").ToString
                Registro(2) = oRow("Fec").ToString
                Registro(3) = oRow("Moneda").ToString
                Registro(4) = CSistema.FormatoNumero(oRow("Cotizacion").ToString)
                Registro(5) = CSistema.FormatoMoneda(oRow("Total").ToString, vdecimalOperacion)
                Registro(6) = CSistema.FormatoMoneda(oRow("Cobrado").ToString, vdecimalOperacion)
                Registro(7) = CSistema.FormatoMoneda(oRow("Descontado").ToString, vdecimalOperacion)
                Registro(8) = CSistema.FormatoMoneda(oRow("Saldo").ToString, vdecimalOperacion)
                Registro(9) = CSistema.FormatoMoneda(oRow("Importe").ToString, vdecimalOperacion)
                'Se multiplica el importe de la factura por la cotizacion de la factura
                Registro(10) = CSistema.FormatoMoneda(CSistema.FormatoMoneda(oRow("Importe").ToString, vdecimalOperacion) * oRow("Cotizacion").ToString, False)
                Registro(11) = oRow("IDTransaccionAnticipo").ToString
                'El total debe ser de los importes de la factura por la cotizaciones de la factura
                Total = Total + CSistema.FormatoMoneda((CSistema.FormatoMoneda(oRow("Importe").ToString, vdecimalOperacion) * oRow("Cotizacion").ToString), False)
                TotalOper = TotalOper + CSistema.FormatoMoneda(oRow("Importe").ToString, vdecimalOperacion)
                If vdecimalOperacion = True Then
                    If vNuevo = False Then
                        'DiferenciaCambio = DiferenciaCambio + CSistema.FormatoMonedaBaseDatos((CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, vdecimalOperacion) * CSistema.FormatoNumero(oRow("Cotizacion").ToString)) - (CSistema.FormatoMonedaBaseDatos(CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, vdecimalOperacion) * CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString), vdecimalOperacion)), False)
                    Else
                        'DiferenciaCambio = DiferenciaCambio + CSistema.FormatoMoneda((CSistema.FormatoNumero(oRow("Importe").ToString, vdecimalOperacion) * CSistema.FormatoNumero(oRow("Cotizacion").ToString, False)) - (CSistema.FormatoMoneda(CSistema.FormatoNumero(oRow("Importe").ToString, vdecimalOperacion) * CSistema.FormatoNumero(txtMoneda.Registro("Cotizacion"), False), False)), False)
                        'DiferenciaCambio = DiferenciaCambio + CSistema.FormatoMonedaBaseDatos((CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, vdecimalOperacion) * CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString, False)) - (CSistema.FormatoMonedaBaseDatos(CSistema.FormatoNumero(oRow("Importe").ToString, vdecimalOperacion) * CSistema.FormatoMonedaBaseDatos(txtMoneda.Registro("Cotizacion"), False), False)), False)
                        Dim valor1 As Double
                        Dim valor2 As Double
                        'La diferencia de precio debe ser del importe por la cotizacion de la factura contra el importe por la cotizacion de la cobranza
                        If txtMoneda.cbxMoneda.GetValue <> 1 Then
                            valor1 = CSistema.FormatoMoneda(oRow("Importe").ToString, vdecimalOperacion) * CSistema.FormatoMoneda(oRow("Cotizacion").ToString, False)
                            valor2 = CSistema.FormatoMoneda(oRow("Importe").ToString, vdecimalOperacion) * CSistema.FormatoMoneda(txtMoneda.Registro("Cotizacion"), False)
                            DiferenciaCambio = CSistema.FormatoMoneda(DiferenciaCambio + (valor1 - valor2), False)

                        End If
                    End If
                Else
                    DiferenciaCambio = 0
                End If
                    dgw.Rows.Add(Registro)

                End If
        Next

        txtTotalVenta.SetValue(Total)
        txtTotalVentaOper.SetValue(TotalOper)
        If vNuevo = True Then
            txtDiferenciaCambio.SetValue(DiferenciaCambio * (-1))
        End If
        'Solo si es nuevo, pasamos las ventas a la forma de pago para las retenciones
        If vNuevo = True Then
            OcxFormaPago1.dtVentaRetencion.Rows.Clear()

            For Each oRow As DataRow In dtDetalleCobranzaVenta.Select("Sel='True'")
                Dim NewRow As DataRow = OcxFormaPago1.dtVentaRetencion.NewRow
                NewRow("Aplicado") = False
                NewRow("Sel") = False
                NewRow("IDTransaccion") = 0
                NewRow("ID") = 0
                NewRow("ComprobanteRetencion") = ""
                NewRow("IDTransaccionVenta") = oRow("IDTransaccion")
                NewRow("ComprobanteVenta") = oRow("Comprobante")
                NewRow("FechaVenta") = oRow("FechaEmision")
                NewRow("Porcentaje") = vgConfiguraciones("CompraPorcentajeRetencion")
                NewRow("TotalImpuesto") = CSistema.FormatoMoneda(oRow("TotalImpuesto"), vdecimalOperacion)

                If oRow("Total") > vgConfiguraciones("CompraImporteMinimoRetencion") Then
                    NewRow("Sel") = True
                    NewRow("Importe") = CSistema.FormatoMoneda(NewRow("TotalImpuesto") * (NewRow("Porcentaje") / 100), vdecimalOperacion)
                Else
                    NewRow("Importe") = 0
                End If

                OcxFormaPago1.dtVentaRetencion.Rows.Add(NewRow)
            Next
        End If

        CalcularTotales()
        If vNuevo = True Then
            If dgw.Rows.Count = 0 Then
                txtMoneda.SoloLectura = False
            Else
                txtMoneda.SoloLectura = True
            End If
        End If


    End Sub

   Sub EliminarVenta()

        For Each item As DataGridViewRow In dgw.SelectedRows

            Dim Comprobante As String = item.Cells(0).Value

            For Each oRow As DataRow In dtDetalleCobranzaVenta.Select("Comprobante='" & Comprobante & "'")
                oRow("Sel") = False
            Next
            Exit For

        Next

        ListarVentas()

    End Sub

    Sub CalcularTotales()

        Dim TotalVentas As Decimal
        Dim TotalFormaPago As Decimal
        Dim SaldoTotal As Decimal
        Dim TotalVentasOper As Decimal
        Dim TotalFormaPagoOper As Decimal
        Dim SaldoTotalOper As Decimal

        TotalVentas = txtTotalVenta.ObtenerValor
        TotalFormaPago = CSistema.FormatoMoneda(OcxFormaPago1.Total, True)

        TotalVentasOper = CSistema.FormatoMoneda(txtTotalVentaOper.txt.Text, vdecimalOperacion)
        TotalFormaPagoOper = CSistema.FormatoMoneda(OcxFormaPago1.TotalOper, vdecimalOperacion)

        'Calcular
        SaldoTotal = TotalVentas - TotalFormaPago
        SaldoTotalOper = TotalVentasOper - TotalFormaPagoOper

        'txtSaldoTotal.SetValue(SaldoTotal)
        txtSaldoTotal.SetValue(SaldoTotalOper)
        'OcxFormaPago1.Saldo = SaldoTotal
        OcxFormaPago1.Saldo = SaldoTotalOper

        If TotalVentas > 0 Then
            cbxTipoComprobante.Enabled = False
        Else
            cbxTipoComprobante.Enabled = True
        End If

    End Sub

    Sub Imprimir(ByVal Original As Boolean)
        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para Imprimir!"
            ctrError.SetError(btnImprimir, mensaje)
            ctrError.SetIconAlignment(btnImprimir, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim frm As New frmImprimirReciboDirecto
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog()

        Dim CMovimiento As New Reporte.CReporteReciboVenta
        CMovimiento.Cobranza(IDTransaccion, frm.Impresora, Original)
    End Sub

    Sub Buscar()

        Dim frm As New frmConsultaCobranzaCredito
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog()
        CargarOperacion(frm.IDTransaccion)
    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select top 1 IsNull((Select top 1 IDTransaccion From VCobranzaCredito Where Numero=" & txtID.ObtenerValor & " and IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )", CadenaConexion)
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select top 1 * From VCobranzaCredito Where IDTransaccion=" & IDTransaccion, CadenaConexion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas técnicos."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)
        txtDiferenciaCambio.SetValue(oRow("DiferenciaCambio").ToString)
        vIdcliente = oRow("IDCliente").ToString
        txtCliente.txtReferencia.txt.Text = oRow("Referencia").ToString
        txtCliente.txtRazonSocial.txt.Text = oRow("Cliente").ToString
        txtCliente.txtRUC.txt.Text = oRow("RUC").ToString
        'cbxCiudad.Texto = oRow("Ciudad").ToString
        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        'cbxSucursal.Texto = oRow("Sucursal").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtFecha.SetValueFromString(CDate(oRow("FechaEmision").ToString))
        txtCliente.SetValue(oRow("IDCliente").ToString)
        txtObservacion.txt.Text = oRow("Observacion").ToString
        cbxCobrador.cbx.Text = oRow("Cobrador").ToString
        txtPlanilla.SetValue(oRow("NroPlanilla").ToString)
        txtMoneda.SetValue(oRow("Moneda").ToString, oRow("Cotizacion").ToString)
        vdecimalOperacion = CSistema.RetornarValorBoolean(CData.GetRow(" referencia = '" & oRow("Moneda").ToString & "'", "vMoneda")("Decimales").ToString)
        txtTotalVentaOper.Decimales = vdecimalOperacion
        chkAnticipoCliente.Valor = CBool(oRow("AnticipoCliente").ToString)
        lblTipoAnticipo.Text = oRow("TipoAnticipo").ToString

        btnAplicarVenta.Enabled = chkAnticipoCliente.Valor
        lklVerAplicacionAnticipo.Enabled = chkAnticipoCliente.Valor

        'Visible Label
        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then

            'Visible Label
            lblRegistradoPor.Visible = True
            lblUsuarioAnulado.Visible = True
            lblUsuarioRegistro.Visible = True
            lblFechaRegistro.Visible = True
            lblAnulado.Visible = True
            lblAnulado.Visible = True
            lblFechaAnulado.Visible = True

            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            'lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            lblUsuarioAnulado.Text = oRow("UsuarioAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos el detalle de Venta
        CargarVenta(IDTransaccion)
        If dtDetalleCobranzaVenta.Rows.Count = 0 Then
            lklVerAplicacionAnticipo.Enabled = False
        Else
            lklVerAplicacionAnticipo.Enabled = chkAnticipoCliente.Valor
        End If

        'Cargar Forma de Pago
        OcxFormaPago1.Cotizacion = oRow("Cotizacion").ToString
        OcxFormaPago1.DecimalesOper = vdecimalOperacion
        OcxFormaPago1.IDMoneda = oRow("IDMoneda").ToString
        OcxFormaPago1.ListarFormaPago(IDTransaccion)

        'Calcular Totales
        CalcularTotales()

        'Inicializamos el Asiento
        CAsiento.Limpiar()
        btnAnular.Enabled = Not lblAnulado.Visible
        If txtSaldoTotal.ObtenerValor = 0 Then
            btnAplicarVenta.Enabled = False
        End If
    End Sub

    Sub LimpiarControles()
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False
        txtPlanilla.txt.Text = ""
    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles, btnModificar)

    End Sub

    Sub ModificarCobranza()

        Dim frm As New frmModificarCobranzaCredito
        frm.Text = "Modificar Cobranza Crédito"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.IDOperacion = IDOperacion
        frm.IDTransaccion = IDTransaccion
        frm.ShowDialog(Me)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub VisualizarAnticipo()

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        Dim IDTransaccion As Integer = dgw.Rows(dgw.CurrentCell.RowIndex).Cells("colIDTransaccionAnticipo").Value
        Dim frm As New frmAplicacionVentasAnticipo
        'Ocultar botones
        'frm.btnAnular.Visible = False
        'frm.btnAsiento.Visible = False
        frm.btnBusquedaAvanzada.Visible = False
        frm.btnCancelar.Visible = False
        frm.btnGuardar.Visible = False
        frm.btnNuevo.Visible = False
        frm.IDTransaccionVisualizar = IDTransaccion
        frm.IDCliente = vIdcliente
        FGMostrarFormulario(Me, frm, "Aplicacion de Ventas a Anticipo", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)


    End Sub

    Private Sub frmCobranzaCredito_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
        LiberarMemoria()
    End Sub

    Public Sub EstablecerVariablesGlobales(Optional _vgIDUsuario As Integer = 0, Optional _vgIDSucursal As Integer = 0, Optional _vgIDDeposito As Integer = 0, Optional _vgIDTerminal As Integer = 0, Optional _vgIDPerfil As Integer = 0)

        If _vgIDUsuario <> 0 Then
            vgIDUsuario = _vgIDUsuario
        End If

        If _vgIDSucursal <> 0 Then
            vgIDSucursal = _vgIDSucursal
        End If

        If _vgIDDeposito <> 0 Then
            vgIDDeposito = _vgIDDeposito
        End If

        If _vgIDTerminal <> 0 Then
            vgIDTerminal = _vgIDTerminal
        End If

        If _vgIDPerfil <> 0 Then
            vgIDPerfil = _vgIDPerfil
        End If

    End Sub

    Private Sub frmCobranzaCredito_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        'Controles para obviar
        Dim Controles() As String = {"OcxFormaPago1"}
        If Controles.Contains(Me.ActiveControl.Name) Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmCobranzaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir(False)
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub btnAgregarVenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarVenta.Click
        If CSistema.ExecuteScalar("select count(*) from Cotizacion where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False) & "' and IDMoneda = " & txtMoneda.Registro("ID")) = 0 And txtMoneda.Registro("ID") > 1 Then
            MessageBox.Show("No se ha fijado cotizacion para la moneda seleccionada.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Cancelar()
            Me.Close()
            Exit Sub
        End If
        CargarVenta()
    End Sub

    Private Sub lklEliminarVenta_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarVenta.LinkClicked
        EliminarVenta()
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue, CadenaConexion)

        'txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado

        'Cobrador
        cbxCobrador.cbx.Text = txtCliente.Registro("Cobrador").ToString

        'Ventas pendientes
        ObtenerVentasClientes()

        'Cheques y Forma de Pago
        OcxFormaPago1.RowCliente = txtCliente.Registro
        OcxFormaPago1.Reset()

        'Foco
        If vNuevo = True Then
            txtPlanilla.txt.Focus()
        End If

    End Sub

    Private Sub txtCliente_ItemMalSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemMalSeleccionado

        If txtCliente.SoloLectura = True Then
            Exit Sub
        End If

    End Sub

    Private Sub OcxFormaPago1_ImporteModificado(ByVal sender As Object, ByVal e As System.EventArgs) Handles OcxFormaPago1.ImporteModificado
        CalcularTotales()

        If Me.ActiveControl Is Nothing Then
            Exit Sub
        End If

        'Si los saldos concuerdan, ir a guardar
        'Solo si el foco esta en el control
        If Me.ActiveControl.Name = OcxFormaPago1.Name Then
            If txtSaldoTotal.ObtenerValor = 0 Then
                btnGuardar.Focus()
            End If
        End If

    End Sub

    Private Sub txtComprobante_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtComprobante.TeclaPrecionada
        If vNuevo = True Then
            Dim comp As String = txtComprobante.txt.Text.Trim
            If comp.Length > 3 Then
                If comp.Substring(0, 4) = "030-" Then
                    txtComprobante.txt.Text = ""
                End If
            End If
            If e.KeyCode = Keys.Enter Then
                PreCargar()
            End If
        End If
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub OcxFormaPago1_ListarCheques(ByVal sender As Object, ByVal e As System.EventArgs)

        'Cheques y Forma de Pago
        OcxFormaPago1.RowCliente = txtCliente.Registro
        OcxFormaPago1.Reset()

    End Sub

    Private Sub OcxFormaPago1_RegistroEliminado(ByVal sender As Object, ByVal e As System.EventArgs, ByVal oRow As System.Data.DataRow, ByVal Tipo As ocxFormaPago.ENUMFormaPago)
        CalcularTotales()
    End Sub

    Private Sub OcxFormaPago1_RegistroInsertado(ByVal sender As Object, ByVal e As System.EventArgs, ByVal oRow As System.Data.DataRow, ByVal Tipo As ocxFormaPago.ENUMFormaPago)
        
    End Sub

    Private Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        ModificarCobranza()
    End Sub

    Private Sub txtPlanilla_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPlanilla.TeclaPrecionada
        OcxFormaPago1.Comprobante = txtPlanilla.ObtenerValor
    End Sub

    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        If vNuevo Then
            CAsiento.IDSucursal = cbxSucursal.GetValue
            CAsiento.IDMoneda = 1
            CAsiento.InicializarAsiento()
            'cbxCobrador
            cbxCobrador.cbx.DataSource = Nothing

            If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
                Exit Sub
            End If

            If cbxSucursal.cbx.Text.Trim = "" Then
                Exit Sub
            End If

            ''Sucursales
            'CSistema.SqlToComboBox(cbxCobrador.cbx, "Select ID, Nombres  From Cobrador Where IDSucursal=" & cbxSucursal.cbx.SelectedValue, CadenaConexion)

            'txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        End If

    End Sub

    Private Sub txtMoneda_CambioMoneda() Handles txtMoneda.CambioMoneda
        If vNuevo = True Then
            'OcxImpuesto1.SetIDMoneda(OcxCotizacion1.Registro("ID"))
            'MostrarDecimales(OcxCotizacion1.Registro("ID"))
            txtMoneda.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
            txtMoneda.Recargar()
            vdecimalOperacion = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & txtMoneda.Registro("ID"), "vMoneda")("Decimales").ToString)
            'txtProducto.IDMoneda = OcxCotizacion1.Registro("ID")
            'txtProducto.Cotizacion = OcxCotizacion1.Registro("Cotizacion")
            'txtImporte.Decimales = vDecimalOperacion
            'txtPrecioUnitario.Decimales = vDecimalOperacion
            OcxFormaPago1.DecimalesOper = vdecimalOperacion
            OcxFormaPago1.Cotizacion = txtMoneda.Registro("Cotizacion")
            txtTotalVentaOper.Decimales = vdecimalOperacion
            OcxFormaPago1.IDMoneda = txtMoneda.Registro("ID")
        End If
    End Sub

    Private Sub txtFecha_Validated(sender As System.Object, e As System.EventArgs) Handles txtFecha.Validated
        Dim Fecha As String = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
        If Fecha.Length = 8 Then
            OcxFormaPago1.Fecha = txtFecha.GetValue
            txtMoneda.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
            txtMoneda.Recargar()
        End If
    End Sub

    Private Sub btnAplicarVenta_Click(sender As System.Object, e As System.EventArgs) Handles btnAplicarVenta.Click
        If chkAnticipoCliente.Valor = True Then
            AplicarVentas()
        End If
    End Sub

    Private Sub lklVerAplicacionAnticipo_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklVerAplicacionAnticipo.LinkClicked
        VisualizarAnticipo()
    End Sub

    Private Sub chkAnticipoCliente_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkAnticipoCliente.PropertyChanged


        If vNuevo = True And value = True Then
            Dim frm As New frmTipoAnticipo
            frm.IDTransaccion = IDTransaccion
            frm.Text = " Tipo de Anticipo "
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.ShowDialog(Me)
            lblTipoAnticipo.Text = frm.Descripcion
            vIDTipoAnticipo = frm.IDTipoAnticipo
        End If
        If vNuevo = True And value = False Then
            lblTipoAnticipo.Text = ""
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnImportar.Click
        If CSistema.ExecuteScalar("select count(*) from Cotizacion where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False) & "' and IDMoneda = " & txtMoneda.Registro("ID")) = 0 And txtMoneda.Registro("ID") > 1 Then
            MessageBox.Show("No se ha fijado cotizacion para la moneda seleccionada.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Cancelar()
            Me.Close()
            Exit Sub
        End If
        If txtCliente.Seleccionado = False Then
            MessageBox.Show("Seleccione un Cliente antes de continuar", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Importar()
    End Sub

    Private Sub cbxTipoComprobante_PropertyChanged(sender As Object, e As EventArgs) Handles cbxTipoComprobante.PropertyChanged
        If cbxTipoComprobante.Texto = "CONTADO" And vNuevo Then
            chkAnticipoCliente.chk.Checked = False
            chkAnticipoCliente.Enabled = False
        Else
            chkAnticipoCliente.Enabled = True
        End If

        If cbxTipoComprobante.Texto = "RECARE" And vNuevo Then
            btnAgregarVenta.Enabled = False
        Else
            btnAgregarVenta.Enabled = True
        End If

    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmCobranzaCredito_Activate()
        Me.Refresh()
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAplicacionProveedoresAnticipo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.gbxComprobantes = New System.Windows.Forms.GroupBox()
        Me.lklEliminarEgreso = New System.Windows.Forms.LinkLabel()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNroComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colProveedor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCotizacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotalImpuesto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCuota = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRetencionIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblTotalComprobante = New System.Windows.Forms.Label()
        Me.btnAgregarFacturasProveedor = New System.Windows.Forms.Button()
        Me.dgwOrdenPago = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccionOP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNroComprobanteOP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFechaOP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colProveedorOP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMonedaOP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotalOP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldoOP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporteOP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colObservacionOP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCotizacionOP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIDMonedaOP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnAgregarOrdenPago = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.OrdenPago = New System.Windows.Forms.GroupBox()
        Me.lklEliminarOrdenPago = New System.Windows.Forms.LinkLabel()
        Me.lblTotalAnticipo = New System.Windows.Forms.Label()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.lblCotizacionCheque = New System.Windows.Forms.Label()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.lblSaldoTotal = New System.Windows.Forms.Label()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtCantidadOrdenPago = New ERP.ocxTXTNumeric()
        Me.txtTotalOrdenPago = New ERP.ocxTXTNumeric()
        Me.txtSaldoTotal = New ERP.ocxTXTNumeric()
        Me.chkAplicarRetencion = New ERP.ocxCHK()
        Me.txtTotalRetencion = New ERP.ocxTXTNumeric()
        Me.txtCantidadComprobante = New ERP.ocxTXTNumeric()
        Me.txtTotalComprobante = New ERP.ocxTXTNumeric()
        Me.gbxComprobantes.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgwOrdenPago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.OrdenPago.SuspendLayout()
        Me.gbxCabecera.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'gbxComprobantes
        '
        Me.gbxComprobantes.Controls.Add(Me.lklEliminarEgreso)
        Me.gbxComprobantes.Controls.Add(Me.dgw)
        Me.gbxComprobantes.Controls.Add(Me.chkAplicarRetencion)
        Me.gbxComprobantes.Controls.Add(Me.txtTotalRetencion)
        Me.gbxComprobantes.Controls.Add(Me.txtCantidadComprobante)
        Me.gbxComprobantes.Controls.Add(Me.txtTotalComprobante)
        Me.gbxComprobantes.Controls.Add(Me.lblTotalComprobante)
        Me.gbxComprobantes.Controls.Add(Me.btnAgregarFacturasProveedor)
        Me.gbxComprobantes.Location = New System.Drawing.Point(12, 147)
        Me.gbxComprobantes.Name = "gbxComprobantes"
        Me.gbxComprobantes.Size = New System.Drawing.Size(758, 193)
        Me.gbxComprobantes.TabIndex = 1
        Me.gbxComprobantes.TabStop = False
        Me.gbxComprobantes.Text = "FACTURAS DEL PROVEEDOR"
        '
        'lklEliminarEgreso
        '
        Me.lklEliminarEgreso.AutoSize = True
        Me.lklEliminarEgreso.Location = New System.Drawing.Point(703, 17)
        Me.lklEliminarEgreso.Name = "lklEliminarEgreso"
        Me.lklEliminarEgreso.Size = New System.Drawing.Size(43, 13)
        Me.lklEliminarEgreso.TabIndex = 20
        Me.lklEliminarEgreso.TabStop = True
        Me.lklEliminarEgreso.Text = "Eliminar"
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colNroComprobante, Me.colFecha, Me.colProveedor, Me.colMoneda, Me.colCotizacion, Me.colTotal, Me.colSaldo, Me.colImporte, Me.colTotalImpuesto, Me.colCuota, Me.colRetencionIVA})
        Me.dgw.Location = New System.Drawing.Point(9, 44)
        Me.dgw.Name = "dgw"
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(742, 108)
        Me.dgw.StandardTab = True
        Me.dgw.TabIndex = 19
        '
        'colIDTransaccion
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = "0"
        Me.colIDTransaccion.DefaultCellStyle = DataGridViewCellStyle2
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colNroComprobante
        '
        Me.colNroComprobante.HeaderText = "NroComprobante"
        Me.colNroComprobante.Name = "colNroComprobante"
        '
        'colFecha
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colFecha.DefaultCellStyle = DataGridViewCellStyle3
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.ReadOnly = True
        Me.colFecha.Width = 80
        '
        'colProveedor
        '
        Me.colProveedor.HeaderText = "Proveedor"
        Me.colProveedor.Name = "colProveedor"
        Me.colProveedor.ReadOnly = True
        Me.colProveedor.Width = 170
        '
        'colMoneda
        '
        Me.colMoneda.HeaderText = "Moneda"
        Me.colMoneda.Name = "colMoneda"
        Me.colMoneda.ReadOnly = True
        Me.colMoneda.Width = 35
        '
        'colCotizacion
        '
        Me.colCotizacion.HeaderText = "Cotizacion"
        Me.colCotizacion.Name = "colCotizacion"
        Me.colCotizacion.Width = 119
        '
        'colTotal
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = "0"
        Me.colTotal.DefaultCellStyle = DataGridViewCellStyle4
        Me.colTotal.HeaderText = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.ReadOnly = True
        Me.colTotal.Width = 80
        '
        'colSaldo
        '
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.colSaldo.DefaultCellStyle = DataGridViewCellStyle5
        Me.colSaldo.HeaderText = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.Width = 75
        '
        'colImporte
        '
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle6
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.Width = 80
        '
        'colTotalImpuesto
        '
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.colTotalImpuesto.DefaultCellStyle = DataGridViewCellStyle7
        Me.colTotalImpuesto.HeaderText = "TotalImpuesto"
        Me.colTotalImpuesto.Name = "colTotalImpuesto"
        '
        'colCuota
        '
        Me.colCuota.HeaderText = "Cuota"
        Me.colCuota.Name = "colCuota"
        '
        'colRetencionIVA
        '
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.colRetencionIVA.DefaultCellStyle = DataGridViewCellStyle8
        Me.colRetencionIVA.HeaderText = "RetencionIVA"
        Me.colRetencionIVA.Name = "colRetencionIVA"
        '
        'lblTotalComprobante
        '
        Me.lblTotalComprobante.AutoSize = True
        Me.lblTotalComprobante.Location = New System.Drawing.Point(522, 163)
        Me.lblTotalComprobante.Name = "lblTotalComprobante"
        Me.lblTotalComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblTotalComprobante.TabIndex = 16
        Me.lblTotalComprobante.Text = "Total Factura:"
        '
        'btnAgregarFacturasProveedor
        '
        Me.btnAgregarFacturasProveedor.Location = New System.Drawing.Point(547, 10)
        Me.btnAgregarFacturasProveedor.Name = "btnAgregarFacturasProveedor"
        Me.btnAgregarFacturasProveedor.Size = New System.Drawing.Size(134, 28)
        Me.btnAgregarFacturasProveedor.TabIndex = 0
        Me.btnAgregarFacturasProveedor.Text = "Agregar Facturas"
        Me.btnAgregarFacturasProveedor.UseVisualStyleBackColor = True
        '
        'dgwOrdenPago
        '
        Me.dgwOrdenPago.AllowUserToAddRows = False
        Me.dgwOrdenPago.AllowUserToDeleteRows = False
        Me.dgwOrdenPago.AllowUserToOrderColumns = True
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgwOrdenPago.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle9
        Me.dgwOrdenPago.BackgroundColor = System.Drawing.Color.White
        Me.dgwOrdenPago.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwOrdenPago.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccionOP, Me.colNroComprobanteOP, Me.colFechaOP, Me.colProveedorOP, Me.colMonedaOP, Me.colTotalOP, Me.colSaldoOP, Me.colImporteOP, Me.colObservacionOP, Me.colCotizacionOP, Me.colIDMonedaOP})
        Me.dgwOrdenPago.Location = New System.Drawing.Point(9, 47)
        Me.dgwOrdenPago.Name = "dgwOrdenPago"
        Me.dgwOrdenPago.ReadOnly = True
        Me.dgwOrdenPago.RowHeadersVisible = False
        Me.dgwOrdenPago.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgwOrdenPago.Size = New System.Drawing.Size(738, 101)
        Me.dgwOrdenPago.StandardTab = True
        Me.dgwOrdenPago.TabIndex = 3
        Me.dgwOrdenPago.TabStop = False
        '
        'colIDTransaccionOP
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "C2"
        DataGridViewCellStyle10.NullValue = "0"
        Me.colIDTransaccionOP.DefaultCellStyle = DataGridViewCellStyle10
        Me.colIDTransaccionOP.HeaderText = "IDTransaccion"
        Me.colIDTransaccionOP.Name = "colIDTransaccionOP"
        Me.colIDTransaccionOP.ReadOnly = True
        Me.colIDTransaccionOP.Visible = False
        '
        'colNroComprobanteOP
        '
        Me.colNroComprobanteOP.HeaderText = "NroComprobante"
        Me.colNroComprobanteOP.Name = "colNroComprobanteOP"
        Me.colNroComprobanteOP.ReadOnly = True
        '
        'colFechaOP
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colFechaOP.DefaultCellStyle = DataGridViewCellStyle11
        Me.colFechaOP.HeaderText = "Fecha"
        Me.colFechaOP.Name = "colFechaOP"
        Me.colFechaOP.ReadOnly = True
        Me.colFechaOP.Width = 80
        '
        'colProveedorOP
        '
        Me.colProveedorOP.HeaderText = "Proveedor"
        Me.colProveedorOP.Name = "colProveedorOP"
        Me.colProveedorOP.ReadOnly = True
        Me.colProveedorOP.Width = 170
        '
        'colMonedaOP
        '
        Me.colMonedaOP.HeaderText = "Moneda"
        Me.colMonedaOP.Name = "colMonedaOP"
        Me.colMonedaOP.ReadOnly = True
        Me.colMonedaOP.Width = 60
        '
        'colTotalOP
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.Format = "N4"
        DataGridViewCellStyle12.NullValue = "0"
        Me.colTotalOP.DefaultCellStyle = DataGridViewCellStyle12
        Me.colTotalOP.HeaderText = "Total"
        Me.colTotalOP.Name = "colTotalOP"
        Me.colTotalOP.ReadOnly = True
        Me.colTotalOP.Width = 80
        '
        'colSaldoOP
        '
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "N4"
        DataGridViewCellStyle13.NullValue = "0"
        Me.colSaldoOP.DefaultCellStyle = DataGridViewCellStyle13
        Me.colSaldoOP.HeaderText = "Saldo"
        Me.colSaldoOP.Name = "colSaldoOP"
        Me.colSaldoOP.ReadOnly = True
        Me.colSaldoOP.Width = 80
        '
        'colImporteOP
        '
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "N4"
        DataGridViewCellStyle14.NullValue = "0"
        Me.colImporteOP.DefaultCellStyle = DataGridViewCellStyle14
        Me.colImporteOP.HeaderText = "Importe"
        Me.colImporteOP.Name = "colImporteOP"
        Me.colImporteOP.ReadOnly = True
        Me.colImporteOP.Width = 80
        '
        'colObservacionOP
        '
        Me.colObservacionOP.FillWeight = 300.0!
        Me.colObservacionOP.HeaderText = "Observación"
        Me.colObservacionOP.Name = "colObservacionOP"
        Me.colObservacionOP.ReadOnly = True
        Me.colObservacionOP.Width = 200
        '
        'colCotizacionOP
        '
        Me.colCotizacionOP.HeaderText = "Cotizacion"
        Me.colCotizacionOP.Name = "colCotizacionOP"
        Me.colCotizacionOP.ReadOnly = True
        '
        'colIDMonedaOP
        '
        Me.colIDMonedaOP.HeaderText = "IDMoneda"
        Me.colIDMonedaOP.Name = "colIDMonedaOP"
        Me.colIDMonedaOP.ReadOnly = True
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(343, 585)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 4
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnAgregarOrdenPago
        '
        Me.btnAgregarOrdenPago.Location = New System.Drawing.Point(547, 10)
        Me.btnAgregarOrdenPago.Name = "btnAgregarOrdenPago"
        Me.btnAgregarOrdenPago.Size = New System.Drawing.Size(134, 28)
        Me.btnAgregarOrdenPago.TabIndex = 0
        Me.btnAgregarOrdenPago.Text = "Agregar Orden de Pago"
        Me.btnAgregarOrdenPago.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(653, 585)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 6
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(574, 585)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(497, 585)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 1
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(419, 585)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 0
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 620)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(782, 22)
        Me.StatusStrip1.TabIndex = 16
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'OrdenPago
        '
        Me.OrdenPago.Controls.Add(Me.lklEliminarOrdenPago)
        Me.OrdenPago.Controls.Add(Me.txtCantidadOrdenPago)
        Me.OrdenPago.Controls.Add(Me.lblTotalAnticipo)
        Me.OrdenPago.Controls.Add(Me.txtTotalOrdenPago)
        Me.OrdenPago.Controls.Add(Me.dgwOrdenPago)
        Me.OrdenPago.Controls.Add(Me.btnAgregarOrdenPago)
        Me.OrdenPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OrdenPago.Location = New System.Drawing.Point(15, 350)
        Me.OrdenPago.Name = "OrdenPago"
        Me.OrdenPago.Size = New System.Drawing.Size(755, 193)
        Me.OrdenPago.TabIndex = 2
        Me.OrdenPago.TabStop = False
        Me.OrdenPago.Text = "ORDEN DE PAGO"
        '
        'lklEliminarOrdenPago
        '
        Me.lklEliminarOrdenPago.AutoSize = True
        Me.lklEliminarOrdenPago.Location = New System.Drawing.Point(700, 18)
        Me.lklEliminarOrdenPago.Name = "lklEliminarOrdenPago"
        Me.lklEliminarOrdenPago.Size = New System.Drawing.Size(43, 13)
        Me.lklEliminarOrdenPago.TabIndex = 21
        Me.lklEliminarOrdenPago.TabStop = True
        Me.lklEliminarOrdenPago.Text = "Eliminar"
        '
        'lblTotalAnticipo
        '
        Me.lblTotalAnticipo.AutoSize = True
        Me.lblTotalAnticipo.Location = New System.Drawing.Point(522, 156)
        Me.lblTotalAnticipo.Name = "lblTotalAnticipo"
        Me.lblTotalAnticipo.Size = New System.Drawing.Size(75, 13)
        Me.lblTotalAnticipo.TabIndex = 12
        Me.lblTotalAnticipo.Text = "Total Anticipo:"
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(263, 585)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 3
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        Me.btnModificar.Visible = False
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.txtCotizacion)
        Me.gbxCabecera.Controls.Add(Me.lblCotizacionCheque)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.cbxMoneda)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtProveedor)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.lblProveedor)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Location = New System.Drawing.Point(12, 19)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(758, 118)
        Me.gbxCabecera.TabIndex = 17
        Me.gbxCabecera.TabStop = False
        '
        'lblCotizacionCheque
        '
        Me.lblCotizacionCheque.AutoSize = True
        Me.lblCotizacionCheque.Location = New System.Drawing.Point(2, 93)
        Me.lblCotizacionCheque.Name = "lblCotizacionCheque"
        Me.lblCotizacionCheque.Size = New System.Drawing.Size(59, 13)
        Me.lblCotizacionCheque.TabIndex = 17
        Me.lblCotizacionCheque.Text = "Cotización:"
        Me.lblCotizacionCheque.Visible = False
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(15, 42)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 9
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(348, 16)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(37, 13)
        Me.lblComprobante.TabIndex = 12
        Me.lblComprobante.Text = "Comp:"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(24, 68)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 13
        Me.lblFecha.Text = "Fecha:"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(141, 42)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(59, 13)
        Me.lblProveedor.TabIndex = 11
        Me.lblProveedor.Text = "Proveedor:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(168, 68)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(32, 13)
        Me.lblObservacion.TabIndex = 15
        Me.lblObservacion.Text = "Obs.:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(194, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(5, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operación:"
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(263, 585)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 5
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(263, 552)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(266, 20)
        Me.flpAnuladoPor.TabIndex = 19
        Me.flpAnuladoPor.Visible = False
        '
        'lblAnulado
        '
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Red
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(89, 20)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(98, 3)
        Me.lblUsuarioAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(150, 3)
        Me.lblFechaAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(342, 585)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 20
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        Me.btnImprimir.Visible = False
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(15, 552)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(239, 20)
        Me.flpRegistradoPor.TabIndex = 21
        Me.flpRegistradoPor.Visible = False
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 3)
        Me.lblRegistradoPor.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 3)
        Me.lblUsuarioRegistro.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 3)
        Me.lblFechaRegistro.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'lblSaldoTotal
        '
        Me.lblSaldoTotal.AutoSize = True
        Me.lblSaldoTotal.Location = New System.Drawing.Point(585, 557)
        Me.lblSaldoTotal.Name = "lblSaldoTotal"
        Me.lblSaldoTotal.Size = New System.Drawing.Size(64, 13)
        Me.lblSaldoTotal.TabIndex = 5
        Me.lblSaldoTotal.Text = "Saldo Total:"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(76, 89)
        Me.txtCotizacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(67, 21)
        Me.txtCotizacion.SoloLectura = False
        Me.txtCotizacion.TabIndex = 9
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "1"
        Me.txtCotizacion.Visible = False
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(382, 12)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(63, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 3
        Me.cbxTipoComprobante.Texto = ""
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = True
        Me.cbxMoneda.DataDisplayMember = "Referencia"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(76, 38)
        Me.cbxMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(63, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 5
        Me.cbxMoneda.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(447, 12)
        Me.txtComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(59, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 4
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 100
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(210, 35)
        Me.txtProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(541, 27)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 6
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(210, 64)
        Me.txtObservacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(542, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 8
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(242, 12)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(94, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 2
        Me.cbxSucursal.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 5, 30, 8, 29, 32, 812)
        Me.txtFecha.Location = New System.Drawing.Point(76, 64)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(69, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 7
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(76, 12)
        Me.cbxCiudad.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(63, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 0
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Introduzca el código y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(139, 12)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(56, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 1
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtCantidadOrdenPago
        '
        Me.txtCantidadOrdenPago.Color = System.Drawing.Color.Empty
        Me.txtCantidadOrdenPago.Decimales = True
        Me.txtCantidadOrdenPago.Indicaciones = Nothing
        Me.txtCantidadOrdenPago.Location = New System.Drawing.Point(706, 152)
        Me.txtCantidadOrdenPago.Name = "txtCantidadOrdenPago"
        Me.txtCantidadOrdenPago.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadOrdenPago.SoloLectura = True
        Me.txtCantidadOrdenPago.TabIndex = 13
        Me.txtCantidadOrdenPago.TabStop = False
        Me.txtCantidadOrdenPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadOrdenPago.Texto = "0"
        '
        'txtTotalOrdenPago
        '
        Me.txtTotalOrdenPago.Color = System.Drawing.Color.Empty
        Me.txtTotalOrdenPago.Decimales = True
        Me.txtTotalOrdenPago.Indicaciones = Nothing
        Me.txtTotalOrdenPago.Location = New System.Drawing.Point(602, 152)
        Me.txtTotalOrdenPago.Name = "txtTotalOrdenPago"
        Me.txtTotalOrdenPago.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalOrdenPago.SoloLectura = True
        Me.txtTotalOrdenPago.TabIndex = 11
        Me.txtTotalOrdenPago.TabStop = False
        Me.txtTotalOrdenPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalOrdenPago.Texto = "0"
        '
        'txtSaldoTotal
        '
        Me.txtSaldoTotal.Color = System.Drawing.Color.Empty
        Me.txtSaldoTotal.Decimales = True
        Me.txtSaldoTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldoTotal.Indicaciones = Nothing
        Me.txtSaldoTotal.Location = New System.Drawing.Point(654, 552)
        Me.txtSaldoTotal.Name = "txtSaldoTotal"
        Me.txtSaldoTotal.Size = New System.Drawing.Size(108, 22)
        Me.txtSaldoTotal.SoloLectura = True
        Me.txtSaldoTotal.TabIndex = 6
        Me.txtSaldoTotal.TabStop = False
        Me.txtSaldoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoTotal.Texto = "0"
        '
        'chkAplicarRetencion
        '
        Me.chkAplicarRetencion.BackColor = System.Drawing.Color.Transparent
        Me.chkAplicarRetencion.Color = System.Drawing.Color.Empty
        Me.chkAplicarRetencion.Location = New System.Drawing.Point(315, 160)
        Me.chkAplicarRetencion.Margin = New System.Windows.Forms.Padding(4)
        Me.chkAplicarRetencion.Name = "chkAplicarRetencion"
        Me.chkAplicarRetencion.Size = New System.Drawing.Size(107, 21)
        Me.chkAplicarRetencion.SoloLectura = False
        Me.chkAplicarRetencion.TabIndex = 0
        Me.chkAplicarRetencion.Texto = "Aplicar retención:"
        Me.chkAplicarRetencion.Valor = False
        '
        'txtTotalRetencion
        '
        Me.txtTotalRetencion.Color = System.Drawing.Color.Empty
        Me.txtTotalRetencion.Decimales = True
        Me.txtTotalRetencion.Indicaciones = Nothing
        Me.txtTotalRetencion.Location = New System.Drawing.Point(424, 159)
        Me.txtTotalRetencion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalRetencion.Name = "txtTotalRetencion"
        Me.txtTotalRetencion.Size = New System.Drawing.Size(98, 22)
        Me.txtTotalRetencion.SoloLectura = True
        Me.txtTotalRetencion.TabIndex = 1
        Me.txtTotalRetencion.TabStop = False
        Me.txtTotalRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalRetencion.Texto = "0"
        '
        'txtCantidadComprobante
        '
        Me.txtCantidadComprobante.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobante.Decimales = True
        Me.txtCantidadComprobante.Indicaciones = Nothing
        Me.txtCantidadComprobante.Location = New System.Drawing.Point(706, 159)
        Me.txtCantidadComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCantidadComprobante.Name = "txtCantidadComprobante"
        Me.txtCantidadComprobante.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadComprobante.SoloLectura = True
        Me.txtCantidadComprobante.TabIndex = 18
        Me.txtCantidadComprobante.TabStop = False
        Me.txtCantidadComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobante.Texto = "0"
        '
        'txtTotalComprobante
        '
        Me.txtTotalComprobante.Color = System.Drawing.Color.Empty
        Me.txtTotalComprobante.Decimales = True
        Me.txtTotalComprobante.Indicaciones = Nothing
        Me.txtTotalComprobante.Location = New System.Drawing.Point(602, 159)
        Me.txtTotalComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalComprobante.Name = "txtTotalComprobante"
        Me.txtTotalComprobante.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalComprobante.SoloLectura = True
        Me.txtTotalComprobante.TabIndex = 17
        Me.txtTotalComprobante.TabStop = False
        Me.txtTotalComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalComprobante.Texto = "0"
        '
        'frmAplicacionProveedoresAnticipo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(782, 642)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.flpAnuladoPor)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.OrdenPago)
        Me.Controls.Add(Me.txtSaldoTotal)
        Me.Controls.Add(Me.lblSaldoTotal)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.gbxComprobantes)
        Me.Controls.Add(Me.btnImprimir)
        Me.KeyPreview = True
        Me.Name = "frmAplicacionProveedoresAnticipo"
        Me.Tag = "frmAplicacionProveedoresAnticipo"
        Me.Text = "frmAplicacionProveedoresAnticipo"
        Me.gbxComprobantes.ResumeLayout(False)
        Me.gbxComprobantes.PerformLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgwOrdenPago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.OrdenPago.ResumeLayout(False)
        Me.OrdenPago.PerformLayout()
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxComprobantes As System.Windows.Forms.GroupBox
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents btnAgregarOrdenPago As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnAgregarFacturasProveedor As System.Windows.Forms.Button
    Friend WithEvents OrdenPago As System.Windows.Forms.GroupBox
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents gbxCabecera As GroupBox
    Friend WithEvents cbxMoneda As ocxCBX
    Friend WithEvents lblMoneda As Label
    Friend WithEvents txtProveedor As ocxTXTProveedor
    Friend WithEvents txtObservacion As ocxTXTString
    Friend WithEvents cbxSucursal As ocxCBX
    Friend WithEvents txtFecha As ocxTXTDate
    Friend WithEvents lblFecha As Label
    Friend WithEvents lblProveedor As Label
    Friend WithEvents cbxCiudad As ocxCBX
    Friend WithEvents txtID As ocxTXTNumeric
    Friend WithEvents lblObservacion As Label
    Friend WithEvents lblSucursal As Label
    Friend WithEvents lblOperacion As Label
    Friend WithEvents dgwOrdenPago As DataGridView

    Friend WithEvents btnAnular As Button
    Friend WithEvents flpAnuladoPor As FlowLayoutPanel
    Friend WithEvents lblAnulado As Label
    Friend WithEvents lblUsuarioAnulado As Label
    Friend WithEvents lblFechaAnulado As Label
    Friend WithEvents btnImprimir As Button
    Friend WithEvents cbxTipoComprobante As ocxCBX
    Friend WithEvents txtComprobante As ocxTXTString
    Friend WithEvents lblComprobante As Label
    Friend WithEvents chkAplicarRetencion As ocxCHK
    Friend WithEvents txtTotalRetencion As ocxTXTNumeric
    Friend WithEvents txtCantidadComprobante As ocxTXTNumeric
    Friend WithEvents txtTotalComprobante As ocxTXTNumeric
    Friend WithEvents lblTotalComprobante As Label
    Friend WithEvents txtTotalOrdenPago As ocxTXTNumeric
    Friend WithEvents lblTotalAnticipo As Label
    Friend WithEvents txtCantidadOrdenPago As ocxTXTNumeric
    Friend WithEvents flpRegistradoPor As FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As Label
    Friend WithEvents lblUsuarioRegistro As Label
    Friend WithEvents lblFechaRegistro As Label
    Friend WithEvents txtCotizacion As ocxTXTNumeric
    Friend WithEvents lblCotizacionCheque As Label
    Friend WithEvents txtSaldoTotal As ocxTXTNumeric
    Friend WithEvents lblSaldoTotal As Label
    Friend WithEvents dgw As DataGridView
    Friend WithEvents colIDTransaccion As DataGridViewTextBoxColumn
    Friend WithEvents colNroComprobante As DataGridViewTextBoxColumn
    Friend WithEvents colFecha As DataGridViewTextBoxColumn
    Friend WithEvents colProveedor As DataGridViewTextBoxColumn
    Friend WithEvents colMoneda As DataGridViewTextBoxColumn
    Friend WithEvents colCotizacion As DataGridViewTextBoxColumn
    Friend WithEvents colTotal As DataGridViewTextBoxColumn
    Friend WithEvents colSaldo As DataGridViewTextBoxColumn
    Friend WithEvents colImporte As DataGridViewTextBoxColumn
    Friend WithEvents colTotalImpuesto As DataGridViewTextBoxColumn
    Friend WithEvents colCuota As DataGridViewTextBoxColumn
    Friend WithEvents colRetencionIVA As DataGridViewTextBoxColumn
    Friend WithEvents lklEliminarEgreso As LinkLabel
    Friend WithEvents lklEliminarOrdenPago As LinkLabel
    Friend WithEvents colIDTransaccionOP As DataGridViewTextBoxColumn
    Friend WithEvents colNroComprobanteOP As DataGridViewTextBoxColumn
    Friend WithEvents colFechaOP As DataGridViewTextBoxColumn
    Friend WithEvents colProveedorOP As DataGridViewTextBoxColumn
    Friend WithEvents colMonedaOP As DataGridViewTextBoxColumn
    Friend WithEvents colTotalOP As DataGridViewTextBoxColumn
    Friend WithEvents colSaldoOP As DataGridViewTextBoxColumn
    Friend WithEvents colImporteOP As DataGridViewTextBoxColumn
    Friend WithEvents colObservacionOP As DataGridViewTextBoxColumn
    Friend WithEvents colCotizacionOP As DataGridViewTextBoxColumn
    Friend WithEvents colIDMonedaOP As DataGridViewTextBoxColumn
End Class

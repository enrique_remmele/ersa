﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRetencionClienteSeleccionarOtrasVentas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtReferenciaTerminal = New ERP.ocxTXTString()
        Me.txtReferenciaSucursal = New ERP.ocxTXTString()
        Me.txtComprobanteFactura = New ERP.ocxTXTString()
        Me.lblImporteRetenido = New System.Windows.Forms.Label()
        Me.txtImporteRetenido = New ERP.ocxTXTNumeric()
        Me.txtComprobanteRetencion = New ERP.ocxTXTString()
        Me.lblComprobanteRetencion = New System.Windows.Forms.Label()
        Me.OcxTXTString1 = New ERP.ocxTXTString()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.OcxTXTString2 = New ERP.ocxTXTString()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnAgregarFactura = New System.Windows.Forms.Button()
        Me.lklVisualizarCobranza = New System.Windows.Forms.LinkLabel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblImporteTotalImpuestoVenta = New System.Windows.Forms.Label()
        Me.txtImporteTotalImpuestoVenta = New ERP.ocxTXTNumeric()
        Me.lblmporteTotalDiscriminadoVenta = New System.Windows.Forms.Label()
        Me.txtImporteTotalDiscriminadoVenta = New ERP.ocxTXTNumeric()
        Me.lblImporteTotalVenta = New System.Windows.Forms.Label()
        Me.txtImporteTotalVenta = New ERP.ocxTXTNumeric()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(163, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(10, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "-"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(127, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(10, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "-"
        '
        'txtReferenciaTerminal
        '
        Me.txtReferenciaTerminal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferenciaTerminal.Color = System.Drawing.Color.Empty
        Me.txtReferenciaTerminal.Indicaciones = Nothing
        Me.txtReferenciaTerminal.Location = New System.Drawing.Point(137, 5)
        Me.txtReferenciaTerminal.Multilinea = False
        Me.txtReferenciaTerminal.Name = "txtReferenciaTerminal"
        Me.txtReferenciaTerminal.Size = New System.Drawing.Size(26, 21)
        Me.txtReferenciaTerminal.SoloLectura = False
        Me.txtReferenciaTerminal.TabIndex = 3
        Me.txtReferenciaTerminal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferenciaTerminal.Texto = "001"
        '
        'txtReferenciaSucursal
        '
        Me.txtReferenciaSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferenciaSucursal.Color = System.Drawing.Color.Empty
        Me.txtReferenciaSucursal.Indicaciones = Nothing
        Me.txtReferenciaSucursal.Location = New System.Drawing.Point(101, 5)
        Me.txtReferenciaSucursal.Multilinea = False
        Me.txtReferenciaSucursal.Name = "txtReferenciaSucursal"
        Me.txtReferenciaSucursal.Size = New System.Drawing.Size(26, 21)
        Me.txtReferenciaSucursal.SoloLectura = False
        Me.txtReferenciaSucursal.TabIndex = 1
        Me.txtReferenciaSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferenciaSucursal.Texto = "001"
        '
        'txtComprobanteFactura
        '
        Me.txtComprobanteFactura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobanteFactura.Color = System.Drawing.Color.Empty
        Me.txtComprobanteFactura.Indicaciones = Nothing
        Me.txtComprobanteFactura.Location = New System.Drawing.Point(173, 5)
        Me.txtComprobanteFactura.Multilinea = False
        Me.txtComprobanteFactura.Name = "txtComprobanteFactura"
        Me.txtComprobanteFactura.Size = New System.Drawing.Size(110, 21)
        Me.txtComprobanteFactura.SoloLectura = False
        Me.txtComprobanteFactura.TabIndex = 5
        Me.txtComprobanteFactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobanteFactura.Texto = ""
        '
        'lblImporteRetenido
        '
        Me.lblImporteRetenido.AutoSize = True
        Me.lblImporteRetenido.Location = New System.Drawing.Point(289, 9)
        Me.lblImporteRetenido.Name = "lblImporteRetenido"
        Me.lblImporteRetenido.Size = New System.Drawing.Size(91, 13)
        Me.lblImporteRetenido.TabIndex = 6
        Me.lblImporteRetenido.Text = "Importe Retenido:"
        '
        'txtImporteRetenido
        '
        Me.txtImporteRetenido.Color = System.Drawing.Color.Empty
        Me.txtImporteRetenido.Decimales = True
        Me.txtImporteRetenido.Indicaciones = Nothing
        Me.txtImporteRetenido.Location = New System.Drawing.Point(386, 5)
        Me.txtImporteRetenido.Name = "txtImporteRetenido"
        Me.txtImporteRetenido.Size = New System.Drawing.Size(86, 21)
        Me.txtImporteRetenido.SoloLectura = False
        Me.txtImporteRetenido.TabIndex = 7
        Me.txtImporteRetenido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteRetenido.Texto = "0"
        '
        'txtComprobanteRetencion
        '
        Me.txtComprobanteRetencion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobanteRetencion.Color = System.Drawing.Color.Empty
        Me.txtComprobanteRetencion.Indicaciones = Nothing
        Me.txtComprobanteRetencion.Location = New System.Drawing.Point(101, 59)
        Me.txtComprobanteRetencion.Multilinea = False
        Me.txtComprobanteRetencion.Name = "txtComprobanteRetencion"
        Me.txtComprobanteRetencion.Size = New System.Drawing.Size(62, 21)
        Me.txtComprobanteRetencion.SoloLectura = True
        Me.txtComprobanteRetencion.TabIndex = 15
        Me.txtComprobanteRetencion.TabStop = False
        Me.txtComprobanteRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobanteRetencion.Texto = ""
        '
        'lblComprobanteRetencion
        '
        Me.lblComprobanteRetencion.AutoSize = True
        Me.lblComprobanteRetencion.Location = New System.Drawing.Point(42, 63)
        Me.lblComprobanteRetencion.Name = "lblComprobanteRetencion"
        Me.lblComprobanteRetencion.Size = New System.Drawing.Size(55, 13)
        Me.lblComprobanteRetencion.TabIndex = 14
        Me.lblComprobanteRetencion.Text = "Cobranza:"
        '
        'OcxTXTString1
        '
        Me.OcxTXTString1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTString1.Color = System.Drawing.Color.Empty
        Me.OcxTXTString1.Indicaciones = Nothing
        Me.OcxTXTString1.Location = New System.Drawing.Point(386, 59)
        Me.OcxTXTString1.Multilinea = False
        Me.OcxTXTString1.Name = "OcxTXTString1"
        Me.OcxTXTString1.Size = New System.Drawing.Size(86, 21)
        Me.OcxTXTString1.SoloLectura = True
        Me.OcxTXTString1.TabIndex = 18
        Me.OcxTXTString1.TabStop = False
        Me.OcxTXTString1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.OcxTXTString1.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(307, 63)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Comprobante:"
        '
        'OcxTXTString2
        '
        Me.OcxTXTString2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTString2.Color = System.Drawing.Color.Empty
        Me.OcxTXTString2.Indicaciones = Nothing
        Me.OcxTXTString2.Location = New System.Drawing.Point(173, 59)
        Me.OcxTXTString2.Multilinea = False
        Me.OcxTXTString2.Name = "OcxTXTString2"
        Me.OcxTXTString2.Size = New System.Drawing.Size(110, 21)
        Me.OcxTXTString2.SoloLectura = True
        Me.OcxTXTString2.TabIndex = 16
        Me.OcxTXTString2.TabStop = False
        Me.OcxTXTString2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.OcxTXTString2.Texto = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(51, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Factura:"
        '
        'btnAgregarFactura
        '
        Me.btnAgregarFactura.Location = New System.Drawing.Point(342, 86)
        Me.btnAgregarFactura.Name = "btnAgregarFactura"
        Me.btnAgregarFactura.Size = New System.Drawing.Size(58, 23)
        Me.btnAgregarFactura.TabIndex = 20
        Me.btnAgregarFactura.Text = "Agregar"
        Me.btnAgregarFactura.UseVisualStyleBackColor = True
        '
        'lklVisualizarCobranza
        '
        Me.lklVisualizarCobranza.AutoSize = True
        Me.lklVisualizarCobranza.Location = New System.Drawing.Point(98, 91)
        Me.lklVisualizarCobranza.Name = "lklVisualizarCobranza"
        Me.lklVisualizarCobranza.Size = New System.Drawing.Size(109, 13)
        Me.lklVisualizarCobranza.TabIndex = 19
        Me.lklVisualizarCobranza.TabStop = True
        Me.lklVisualizarCobranza.Text = "Vizualizar la cobranza"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(406, 86)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(58, 23)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "Cancelar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lblImporteTotalImpuestoVenta
        '
        Me.lblImporteTotalImpuestoVenta.AutoSize = True
        Me.lblImporteTotalImpuestoVenta.Location = New System.Drawing.Point(341, 36)
        Me.lblImporteTotalImpuestoVenta.Name = "lblImporteTotalImpuestoVenta"
        Me.lblImporteTotalImpuestoVenta.Size = New System.Drawing.Size(53, 13)
        Me.lblImporteTotalImpuestoVenta.TabIndex = 12
        Me.lblImporteTotalImpuestoVenta.Text = "Impuesto:"
        '
        'txtImporteTotalImpuestoVenta
        '
        Me.txtImporteTotalImpuestoVenta.Color = System.Drawing.Color.Empty
        Me.txtImporteTotalImpuestoVenta.Decimales = True
        Me.txtImporteTotalImpuestoVenta.Indicaciones = Nothing
        Me.txtImporteTotalImpuestoVenta.Location = New System.Drawing.Point(394, 32)
        Me.txtImporteTotalImpuestoVenta.Name = "txtImporteTotalImpuestoVenta"
        Me.txtImporteTotalImpuestoVenta.Size = New System.Drawing.Size(78, 21)
        Me.txtImporteTotalImpuestoVenta.SoloLectura = False
        Me.txtImporteTotalImpuestoVenta.TabIndex = 13
        Me.txtImporteTotalImpuestoVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteTotalImpuestoVenta.Texto = "0"
        '
        'lblmporteTotalDiscriminadoVenta
        '
        Me.lblmporteTotalDiscriminadoVenta.AutoSize = True
        Me.lblmporteTotalDiscriminadoVenta.Location = New System.Drawing.Point(186, 36)
        Me.lblmporteTotalDiscriminadoVenta.Name = "lblmporteTotalDiscriminadoVenta"
        Me.lblmporteTotalDiscriminadoVenta.Size = New System.Drawing.Size(70, 13)
        Me.lblmporteTotalDiscriminadoVenta.TabIndex = 10
        Me.lblmporteTotalDiscriminadoVenta.Text = "Discriminado:"
        '
        'txtImporteTotalDiscriminadoVenta
        '
        Me.txtImporteTotalDiscriminadoVenta.Color = System.Drawing.Color.Empty
        Me.txtImporteTotalDiscriminadoVenta.Decimales = True
        Me.txtImporteTotalDiscriminadoVenta.Indicaciones = Nothing
        Me.txtImporteTotalDiscriminadoVenta.Location = New System.Drawing.Point(256, 32)
        Me.txtImporteTotalDiscriminadoVenta.Name = "txtImporteTotalDiscriminadoVenta"
        Me.txtImporteTotalDiscriminadoVenta.Size = New System.Drawing.Size(85, 21)
        Me.txtImporteTotalDiscriminadoVenta.SoloLectura = False
        Me.txtImporteTotalDiscriminadoVenta.TabIndex = 11
        Me.txtImporteTotalDiscriminadoVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteTotalDiscriminadoVenta.Texto = "0"
        '
        'lblImporteTotalVenta
        '
        Me.lblImporteTotalVenta.AutoSize = True
        Me.lblImporteTotalVenta.Location = New System.Drawing.Point(10, 36)
        Me.lblImporteTotalVenta.Name = "lblImporteTotalVenta"
        Me.lblImporteTotalVenta.Size = New System.Drawing.Size(91, 13)
        Me.lblImporteTotalVenta.TabIndex = 8
        Me.lblImporteTotalVenta.Text = "Importe de Venta:"
        '
        'txtImporteTotalVenta
        '
        Me.txtImporteTotalVenta.Color = System.Drawing.Color.Empty
        Me.txtImporteTotalVenta.Decimales = True
        Me.txtImporteTotalVenta.Indicaciones = Nothing
        Me.txtImporteTotalVenta.Location = New System.Drawing.Point(101, 32)
        Me.txtImporteTotalVenta.Name = "txtImporteTotalVenta"
        Me.txtImporteTotalVenta.Size = New System.Drawing.Size(85, 21)
        Me.txtImporteTotalVenta.SoloLectura = False
        Me.txtImporteTotalVenta.TabIndex = 9
        Me.txtImporteTotalVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteTotalVenta.Texto = "0"
        '
        'frmRetencionClienteSeleccionarOtrasVentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(481, 119)
        Me.Controls.Add(Me.lblImporteTotalImpuestoVenta)
        Me.Controls.Add(Me.txtImporteTotalImpuestoVenta)
        Me.Controls.Add(Me.lblmporteTotalDiscriminadoVenta)
        Me.Controls.Add(Me.txtImporteTotalDiscriminadoVenta)
        Me.Controls.Add(Me.lblImporteTotalVenta)
        Me.Controls.Add(Me.txtImporteTotalVenta)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lklVisualizarCobranza)
        Me.Controls.Add(Me.btnAgregarFactura)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.OcxTXTString2)
        Me.Controls.Add(Me.OcxTXTString1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtComprobanteRetencion)
        Me.Controls.Add(Me.lblComprobanteRetencion)
        Me.Controls.Add(Me.lblImporteRetenido)
        Me.Controls.Add(Me.txtImporteRetenido)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtReferenciaTerminal)
        Me.Controls.Add(Me.txtReferenciaSucursal)
        Me.Controls.Add(Me.txtComprobanteFactura)
        Me.Name = "frmRetencionClienteSeleccionarOtrasVentas"
        Me.Text = "frmRetencionClienteSeleccionarOtrasVentas"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtReferenciaTerminal As ERP.ocxTXTString
    Friend WithEvents txtReferenciaSucursal As ERP.ocxTXTString
    Friend WithEvents txtComprobanteFactura As ERP.ocxTXTString
    Friend WithEvents lblImporteRetenido As System.Windows.Forms.Label
    Friend WithEvents txtImporteRetenido As ERP.ocxTXTNumeric
    Friend WithEvents txtComprobanteRetencion As ERP.ocxTXTString
    Friend WithEvents lblComprobanteRetencion As System.Windows.Forms.Label
    Friend WithEvents OcxTXTString1 As ERP.ocxTXTString
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents OcxTXTString2 As ERP.ocxTXTString
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnAgregarFactura As System.Windows.Forms.Button
    Friend WithEvents lklVisualizarCobranza As System.Windows.Forms.LinkLabel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lblImporteTotalImpuestoVenta As System.Windows.Forms.Label
    Friend WithEvents txtImporteTotalImpuestoVenta As ERP.ocxTXTNumeric
    Friend WithEvents lblmporteTotalDiscriminadoVenta As System.Windows.Forms.Label
    Friend WithEvents txtImporteTotalDiscriminadoVenta As ERP.ocxTXTNumeric
    Friend WithEvents lblImporteTotalVenta As System.Windows.Forms.Label
    Friend WithEvents txtImporteTotalVenta As ERP.ocxTXTNumeric
End Class

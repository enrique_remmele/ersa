﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaAsociarRetencionCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotal = New ERP.ocxTXTNumeric()
        Me.lblTotalCobranza = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtNroComprobante = New ERP.ocxTXTString()
        Me.lblNroRetencion = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.txtOperacion = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblCantidadCobranza = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.tlpSeleccionar = New System.Windows.Forms.TableLayoutPanel()
        Me.btnSeleccionar = New System.Windows.Forms.Button()
        Me.dgwOperacion = New System.Windows.Forms.DataGridView()
        Me.tlpInforme = New System.Windows.Forms.TableLayoutPanel()
        Me.cbxSucursal = New System.Windows.Forms.ComboBox()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btn4 = New System.Windows.Forms.Button()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.chkSucursal = New System.Windows.Forms.CheckBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.tlpGeneral = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.tlpSeleccionar.SuspendLayout()
        CType(Me.dgwOperacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tlpInforme.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.tlpGeneral.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotal)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblTotalCobranza)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(330, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(322, 37)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'txtTotal
        '
        Me.txtTotal.Color = System.Drawing.Color.Empty
        Me.txtTotal.Decimales = True
        Me.txtTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.Indicaciones = Nothing
        Me.txtTotal.Location = New System.Drawing.Point(172, 4)
        Me.txtTotal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(146, 22)
        Me.txtTotal.SoloLectura = True
        Me.txtTotal.TabIndex = 1
        Me.txtTotal.TabStop = False
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Texto = "0"
        '
        'lblTotalCobranza
        '
        Me.lblTotalCobranza.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalCobranza.Location = New System.Drawing.Point(119, 0)
        Me.lblTotalCobranza.Name = "lblTotalCobranza"
        Me.lblTotalCobranza.Size = New System.Drawing.Size(46, 26)
        Me.lblTotalCobranza.TabIndex = 0
        Me.lblTotalCobranza.Text = "Total:"
        Me.lblTotalCobranza.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.txtNroComprobante)
        Me.Panel3.Controls.Add(Me.lblNroRetencion)
        Me.Panel3.Controls.Add(Me.lblOperacion)
        Me.Panel3.Controls.Add(Me.txtOperacion)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(488, 23)
        Me.Panel3.TabIndex = 0
        '
        'txtNroComprobante
        '
        Me.txtNroComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroComprobante.Color = System.Drawing.Color.Empty
        Me.txtNroComprobante.Indicaciones = Nothing
        Me.txtNroComprobante.Location = New System.Drawing.Point(209, 2)
        Me.txtNroComprobante.Multilinea = False
        Me.txtNroComprobante.Name = "txtNroComprobante"
        Me.txtNroComprobante.Size = New System.Drawing.Size(122, 21)
        Me.txtNroComprobante.SoloLectura = False
        Me.txtNroComprobante.TabIndex = 3
        Me.txtNroComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroComprobante.Texto = ""
        '
        'lblNroRetencion
        '
        Me.lblNroRetencion.AutoSize = True
        Me.lblNroRetencion.Location = New System.Drawing.Point(112, 6)
        Me.lblNroRetencion.Name = "lblNroRetencion"
        Me.lblNroRetencion.Size = New System.Drawing.Size(97, 13)
        Me.lblNroRetencion.TabIndex = 2
        Me.lblNroRetencion.Text = "Nro. de Retencion:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(3, 6)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operación:"
        '
        'txtOperacion
        '
        Me.txtOperacion.Color = System.Drawing.Color.Empty
        Me.txtOperacion.Decimales = True
        Me.txtOperacion.Indicaciones = Nothing
        Me.txtOperacion.Location = New System.Drawing.Point(62, 2)
        Me.txtOperacion.Name = "txtOperacion"
        Me.txtOperacion.Size = New System.Drawing.Size(50, 22)
        Me.txtOperacion.SoloLectura = False
        Me.txtOperacion.TabIndex = 1
        Me.txtOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOperacion.Texto = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Cantidad:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(57, 23)
        Me.Panel1.TabIndex = 0
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 3
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 63.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 98.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Panel2, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel3, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel4, 2, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(4, 4)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(655, 29)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Controls.Add(Me.lblCantidadCobranza)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(497, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(57, 23)
        Me.Panel2.TabIndex = 0
        '
        'lblCantidadCobranza
        '
        Me.lblCantidadCobranza.AutoSize = True
        Me.lblCantidadCobranza.Location = New System.Drawing.Point(3, 5)
        Me.lblCantidadCobranza.Name = "lblCantidadCobranza"
        Me.lblCantidadCobranza.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadCobranza.TabIndex = 0
        Me.lblCantidadCobranza.Text = "Cantidad:"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.txtCantidad)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(560, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(92, 23)
        Me.Panel4.TabIndex = 1
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = Nothing
        Me.txtCantidad.Location = New System.Drawing.Point(3, 2)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(84, 22)
        Me.txtCantidad.SoloLectura = True
        Me.txtCantidad.TabIndex = 0
        Me.txtCantidad.TabStop = False
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'tlpSeleccionar
        '
        Me.tlpSeleccionar.ColumnCount = 2
        Me.tlpSeleccionar.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpSeleccionar.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpSeleccionar.Controls.Add(Me.FlowLayoutPanel1, 1, 0)
        Me.tlpSeleccionar.Controls.Add(Me.btnSeleccionar, 0, 0)
        Me.tlpSeleccionar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpSeleccionar.Location = New System.Drawing.Point(4, 392)
        Me.tlpSeleccionar.Name = "tlpSeleccionar"
        Me.tlpSeleccionar.RowCount = 1
        Me.tlpSeleccionar.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpSeleccionar.Size = New System.Drawing.Size(655, 43)
        Me.tlpSeleccionar.TabIndex = 2
        '
        'btnSeleccionar
        '
        Me.btnSeleccionar.Location = New System.Drawing.Point(3, 3)
        Me.btnSeleccionar.Name = "btnSeleccionar"
        Me.btnSeleccionar.Size = New System.Drawing.Size(190, 35)
        Me.btnSeleccionar.TabIndex = 0
        Me.btnSeleccionar.Text = "Seleccionar"
        Me.btnSeleccionar.UseVisualStyleBackColor = True
        '
        'dgwOperacion
        '
        Me.dgwOperacion.AllowUserToAddRows = False
        Me.dgwOperacion.AllowUserToDeleteRows = False
        Me.dgwOperacion.AllowUserToOrderColumns = True
        Me.dgwOperacion.BackgroundColor = System.Drawing.Color.White
        Me.dgwOperacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwOperacion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgwOperacion.Location = New System.Drawing.Point(4, 40)
        Me.dgwOperacion.Name = "dgwOperacion"
        Me.dgwOperacion.ReadOnly = True
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgwOperacion.RowHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgwOperacion.Size = New System.Drawing.Size(655, 345)
        Me.dgwOperacion.StandardTab = True
        Me.dgwOperacion.TabIndex = 1
        '
        'tlpInforme
        '
        Me.tlpInforme.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.tlpInforme.ColumnCount = 1
        Me.tlpInforme.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpInforme.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.tlpInforme.Controls.Add(Me.tlpSeleccionar, 0, 2)
        Me.tlpInforme.Controls.Add(Me.dgwOperacion, 0, 1)
        Me.tlpInforme.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpInforme.Location = New System.Drawing.Point(3, 3)
        Me.tlpInforme.Name = "tlpInforme"
        Me.tlpInforme.RowCount = 3
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49.0!))
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpInforme.Size = New System.Drawing.Size(663, 439)
        Me.tlpInforme.TabIndex = 0
        '
        'cbxSucursal
        '
        Me.cbxSucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbxSucursal.FormattingEnabled = True
        Me.cbxSucursal.Location = New System.Drawing.Point(6, 23)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.Size = New System.Drawing.Size(203, 21)
        Me.cbxSucursal.TabIndex = 5
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'btn4
        '
        Me.btn4.Location = New System.Drawing.Point(6, 108)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(203, 24)
        Me.btn4.TabIndex = 9
        Me.btn4.Text = "Emitir informe"
        Me.btn4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn4.UseVisualStyleBackColor = True
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.cbxSucursal)
        Me.TabPage1.Controls.Add(Me.btn4)
        Me.TabPage1.Controls.Add(Me.dtpHasta)
        Me.TabPage1.Controls.Add(Me.dtpDesde)
        Me.TabPage1.Controls.Add(Me.chkSucursal)
        Me.TabPage1.Location = New System.Drawing.Point(4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(217, 394)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'dtpHasta
        '
        Me.dtpHasta.Location = New System.Drawing.Point(6, 76)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(203, 20)
        Me.dtpHasta.TabIndex = 8
        '
        'dtpDesde
        '
        Me.dtpDesde.Location = New System.Drawing.Point(6, 50)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(203, 20)
        Me.dtpDesde.TabIndex = 7
        '
        'chkSucursal
        '
        Me.chkSucursal.AutoSize = True
        Me.chkSucursal.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.chkSucursal.Location = New System.Drawing.Point(6, 6)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(68, 17)
        Me.chkSucursal.TabIndex = 4
        Me.chkSucursal.Text = "Sucursal:"
        Me.chkSucursal.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(3, 16)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(225, 420)
        Me.TabControl1.TabIndex = 0
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.TabControl1)
        Me.gbxFiltro.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxFiltro.Location = New System.Drawing.Point(672, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(231, 439)
        Me.gbxFiltro.TabIndex = 1
        Me.gbxFiltro.TabStop = False
        '
        'tlpGeneral
        '
        Me.tlpGeneral.ColumnCount = 2
        Me.tlpGeneral.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpGeneral.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 237.0!))
        Me.tlpGeneral.Controls.Add(Me.gbxFiltro, 1, 0)
        Me.tlpGeneral.Controls.Add(Me.tlpInforme, 0, 0)
        Me.tlpGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpGeneral.Location = New System.Drawing.Point(0, 0)
        Me.tlpGeneral.Name = "tlpGeneral"
        Me.tlpGeneral.RowCount = 1
        Me.tlpGeneral.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpGeneral.Size = New System.Drawing.Size(906, 445)
        Me.tlpGeneral.TabIndex = 1
        '
        'frmConsultaAsociarRetencionCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(906, 445)
        Me.Controls.Add(Me.tlpGeneral)
        Me.Name = "frmConsultaAsociarRetencionCliente"
        Me.Text = "frmConsultaAsociarRetencionCliente"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.tlpSeleccionar.ResumeLayout(False)
        CType(Me.dgwOperacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tlpInforme.ResumeLayout(False)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.gbxFiltro.ResumeLayout(False)
        Me.tlpGeneral.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotal As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalCobranza As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtNroComprobante As ERP.ocxTXTString
    Friend WithEvents lblNroRetencion As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtOperacion As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidadCobranza As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidad As ERP.ocxTXTNumeric
    Friend WithEvents tlpSeleccionar As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnSeleccionar As System.Windows.Forms.Button
    Friend WithEvents dgwOperacion As System.Windows.Forms.DataGridView
    Friend WithEvents tlpInforme As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents cbxSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents tlpGeneral As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents btn4 As System.Windows.Forms.Button
    Friend WithEvents dtpHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkSucursal As System.Windows.Forms.CheckBox
End Class

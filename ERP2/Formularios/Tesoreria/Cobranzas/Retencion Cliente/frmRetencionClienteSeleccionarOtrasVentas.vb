﻿Public Class frmRetencionClienteSeleccionarOtrasVentas

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Property PorcentajeRetencion As Decimal
    Property ReferenciaSucursal As String
    Property ReferenciaTerminal As String
    Property IDFormaPago As Integer
    Property IDTransaccion As Integer
    Property IDTransaccionCobranza As Integer
    Property RowVenta As DataRow
    Property Procesado As Boolean


    'VARIABLES
    Dim vImporteRetencion As Decimal

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        txtReferenciaSucursal.SetValue(ReferenciaSucursal)
        txtReferenciaTerminal.SetValue(ReferenciaTerminal)

    End Sub

    Sub BuscarVenta()

        RowVenta = Nothing

        Dim vComprobante As String = txtReferenciaSucursal.GetValue & "-" & txtReferenciaTerminal.GetValue & "-" & txtComprobanteFactura.GetValue

        'Validar
        'Comprobante
        If PorcentajeRetencion = 0 Then
            MessageBox.Show("Debe especificar el porcentaje de retencion para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        'Comprobante
        If txtComprobanteFactura.GetValue.Length = 0 Then
            Exit Sub
        End If

        Dim SQL As String = "Select Top(1) 'IDTransaccion'=" & IDTransaccion & ", 'ID'=" & IDFormaPago & ", 'IDTransaccionVenta'=IDTransaccion, 'ComprobanteVenta'=Comprobante, FechaEmision, Total, TotalDiscriminado, TotalImpuesto, 'Importe'=0 From VVenta Where Comprobante='" & vComprobante & "' "

        Dim vdt As DataTable = CSistema.ExecuteToDataTable(SQL)

        If vdt Is Nothing Then
            MessageBox.Show("No se encontro la venta para este cliente con esta retencion!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtComprobanteFactura.txt.Focus()
            Exit Sub
        End If

        If vdt.Rows.Count = 0 Then
            MessageBox.Show("No se encontro la venta para este cliente con esta retencion!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        RowVenta = vdt.Rows(0)

        'Ponemos el monto sugerido, redondeado
        txtImporteRetenido.SetValue(Math.Round(RowVenta("TotalImpuesto") * (PorcentajeRetencion / 100), 0))
        vImporteRetencion = txtImporteRetenido.ObtenerValor

        txtImporteTotalVenta.SetValue(RowVenta("Total"))
        txtImporteTotalDiscriminadoVenta.SetValue(RowVenta("TotalDiscriminado"))
        txtImporteTotalImpuestoVenta.SetValue(RowVenta("TotalImpuesto"))

        'Hayamos
        ObtenerCobranza()

    End Sub

    Sub ObtenerCobranza()

        'Verificar si es una cobranza normal
        Try

            Dim vComprobante As String = txtReferenciaSucursal.GetValue & "-" & txtReferenciaTerminal.GetValue & "-" & txtComprobanteFactura.GetValue

            Dim dt As DataTable = CSistema.ExecuteToDataTable(" Select * From VVentaDetalleCobranza Where Comprobante='" & vComprobante & "' ")

            If dt Is Nothing Then
                Exit Sub
            End If

            If dt.Rows.Count = 0 Then
                Exit Sub
            End If

            IDTransaccionCobranza = dt.Rows(0)("IDTransaccionCobranza")
            OcxTXTString2.SetValue(dt.Rows(0)("T. Comp."))
            OcxTXTString1.SetValue(dt.Rows(0)("Comp. Cob."))

        Catch ex As Exception

        End Try


    End Sub

    Sub VisualizarCobranza()

        'Verificar si es una cobranza normal
        Try

            If IsNumeric(IDTransaccionCobranza) = False Then
                Exit Sub
            End If

            Dim Existe As Integer = CSistema.ExecuteScalar("Select Count(*) From CobranzaCredito Where IDTransaccion=" & IDTransaccionCobranza)

            If Existe > 0 Then
                Dim frm As New frmAplicacionProveedoresAnticipo
                FGMostrarFormulario(Me, frm, "Cobranza")
                frm.CargarOperacion(IDTransaccionCobranza)
            Else
                Dim frm As New frmCobranzaContado
                FGMostrarFormulario(Me, frm, "Cobranza")
                frm.CargarOperacion(IDTransaccionCobranza)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub Agregar()

        If RowVenta Is Nothing Then
            MessageBox.Show("Seleccione una venta para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        RowVenta("Importe") = txtImporteRetenido.ObtenerValor
        RowVenta("Total") = txtImporteTotalVenta.ObtenerValor
        RowVenta("TotalDiscriminado") = txtImporteTotalDiscriminadoVenta.ObtenerValor
        RowVenta("TotalImpuesto") = txtImporteTotalImpuestoVenta.ObtenerValor

        Procesado = True
        Me.Close()

    End Sub

    Private Sub frmRetencionClienteSeleccionarOtrasVentas_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Enter Then

            'Controles para obviar
            Dim Controles() As String = {"txtImporteRetenido"}
            If Controles.Contains(Me.ActiveControl.Name) Then
                Exit Sub
            End If

            CSistema.SelectNextControl(Me, e.KeyCode)

        End If
    End Sub

    Private Sub frmRetencionClienteSeleccionarOtrasVentas_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub txtComprobanteFactura_Leave(sender As Object, e As System.EventArgs) Handles txtComprobanteFactura.Leave
        BuscarVenta()
    End Sub

    Private Sub lklVisualizarCobranza_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklVisualizarCobranza.LinkClicked
        VisualizarCobranza()
    End Sub

    Private Sub btnAgregarFactura_Click(sender As System.Object, e As System.EventArgs) Handles btnAgregarFactura.Click
        Agregar()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Procesado = False
        Me.Close()
    End Sub

    Private Sub txtImporteRetenido_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtImporteRetenido.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then

            'Si el importe de la retencion fue modificada, saltar a valores de la factura
            If vImporteRetencion <> txtImporteRetenido.ObtenerValor Then
                txtImporteTotalVenta.txt.Focus()
                Exit Sub
            End If

            Agregar()
        End If
    End Sub

    Private Sub txtImporteTotalImpuestoVenta_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtImporteTotalImpuestoVenta.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            Agregar()
        End If
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmRetencionClienteSeleccionarOtrasVentas_Activate()
        Me.Refresh()
    End Sub
End Class
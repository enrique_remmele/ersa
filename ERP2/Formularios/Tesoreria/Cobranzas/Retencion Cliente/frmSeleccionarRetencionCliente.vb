﻿Public Class frmSeleccionarRetencionCliente

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Property IDCliente As Integer
    Property Cliente As String
    Property Seleccionado As Boolean
    Property dt As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Form 
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Fecha
        txtDesde.SetValueFromString("01/01/" & Date.Now.Year)
        txtHasta.SetValueFromString("01/" & Date.Now.Month & "/" & Date.Now.Year)

        'Cliente
        txtCliente.SetValue(Cliente)

        'Funciones
        Listar()

    End Sub

    Sub Listar()

        Dim SQL As String = "Select Distinct IDTransaccion, ID, 'Tipo'=CodigoComprobante, 'Comprobante'=ComprobanteRetencion, Fecha, Observacion, Importe, Sel='False' From VRetencionClientePendiente "
        Dim Where As String = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' And IDCliente=" & IDCliente & "  And ComprobanteRetencion Like '%" & txtComprobante.GetValue & "%'"
        Dim OrderBy As String = " Order By Fecha"

        CSistema.SqlToDataGrid(dgv, SQL & Where & OrderBy)

        'Formato
        dgv.Columns("IDTransaccion").Visible = False
        dgv.Columns("ID").Visible = False
        dgv.Columns("Sel").Visible = False
        dgv.Columns("Observacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("Tipo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv.Columns("Fecha").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv.Columns("Importe").DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dgv.Columns("Importe").DefaultCellStyle.Format = "N0"


    End Sub

    Sub Seleccionar()

        If dgv.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        dt = CType(dgv.DataSource, DataTable).Clone
        Dim vdttemp As DataTable = CType(dgv.DataSource, DataTable).Copy

        For Each SelRow As DataGridViewRow In dgv.SelectedRows
            Dim vIDTransaccion As Integer = SelRow.Cells("IDTransaccion").Value
            Dim vID As Integer = SelRow.Cells("ID").Value

            For Each oRow As DataRow In vdttemp.Select("IDTransaccion=" & vIDTransaccion & " And ID=" & vID & " ")
                Dim NewRow As DataRow = dt.NewRow
                For c As Integer = 0 To vdttemp.Columns.Count - 1
                    NewRow(c) = oRow(c)
                Next
                NewRow("Sel") = True
                dt.Rows.Add(NewRow)
            Next

        Next

        Seleccionado = True
        Me.Close()

    End Sub

    Sub Cancelar()

    End Sub

    Private Sub frmSeleccionarRetencionCliente_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        Dim vControles() As String = {"dgv"}
        If vControles.Contains(Me.ActiveControl.Name.ToString) Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub frmSeleccionarRetencionCliente_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(sender As System.Object, e As System.EventArgs) Handles btnListar.Click
        Listar()

        If dgv.Rows.Count = 0 Then
            txtDesde.txt.Focus()
            Exit Sub
        End If

        dgv.Focus()

    End Sub

    Private Sub dgv_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyDown
        If e.KeyCode = Keys.Enter Then
            Seleccionar()
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub dgv_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyUp

        If e.KeyCode = Keys.Enter Then

        End If

    End Sub

    Private Sub btnSeleccionar_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmSeleccionarRetencionCliente_Activate()
        Me.Refresh()
    End Sub
End Class
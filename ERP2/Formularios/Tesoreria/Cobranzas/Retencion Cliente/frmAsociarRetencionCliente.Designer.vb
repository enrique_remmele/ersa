﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAsociarRetencionCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.lklVisualizarCobranza = New System.Windows.Forms.LinkLabel()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.lblSaldo = New System.Windows.Forms.Label()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lblPorcentajeRetencion = New System.Windows.Forms.Label()
        Me.lblTotalRetencion = New System.Windows.Forms.Label()
        Me.gbxFacturas = New System.Windows.Forms.GroupBox()
        Me.lblImporteTotalImpuestoVenta = New System.Windows.Forms.Label()
        Me.txtImporteTotalImpuestoVenta = New ERP.ocxTXTNumeric()
        Me.lblmporteTotalDiscriminadoVenta = New System.Windows.Forms.Label()
        Me.txtImporteTotalDiscriminadoVenta = New ERP.ocxTXTNumeric()
        Me.lblImporteTotalVenta = New System.Windows.Forms.Label()
        Me.txtImporteTotalVenta = New ERP.ocxTXTNumeric()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtReferenciaTerminal = New ERP.ocxTXTString()
        Me.txtReferenciaSucursal = New ERP.ocxTXTString()
        Me.lblImporteRetenido = New System.Windows.Forms.Label()
        Me.txtImporteRetenido = New ERP.ocxTXTNumeric()
        Me.lklEliminarVenta = New System.Windows.Forms.LinkLabel()
        Me.txtTotalRetenido = New ERP.ocxTXTNumeric()
        Me.lblTotalRetenido = New System.Windows.Forms.Label()
        Me.txtTotalIVA = New ERP.ocxTXTNumeric()
        Me.lblTotalIVA = New System.Windows.Forms.Label()
        Me.txtTotalVenta = New ERP.ocxTXTNumeric()
        Me.lblTotalVenta = New System.Windows.Forms.Label()
        Me.dgvFactura = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIDTransaccionVenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotalDiscriminado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotalImpuesto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lklSeleccionarFactura = New System.Windows.Forms.LinkLabel()
        Me.btnAgregarFactura = New System.Windows.Forms.Button()
        Me.txtComprobanteFactura = New ERP.ocxTXTString()
        Me.gbxRetencion = New System.Windows.Forms.GroupBox()
        Me.txtTotalRetencion = New ERP.ocxTXTNumeric()
        Me.txtObservacionRetencion = New ERP.ocxTXTString()
        Me.lblObservacionRetencion = New System.Windows.Forms.Label()
        Me.txtPorcentajeRetencion = New ERP.ocxTXTNumeric()
        Me.txtFechaRetencion = New ERP.ocxTXTDate()
        Me.lblFechaRetencion = New System.Windows.Forms.Label()
        Me.btnSeleccionarRetencion = New System.Windows.Forms.Button()
        Me.txtComprobanteRetencion = New ERP.ocxTXTString()
        Me.lblComprobanteRetencion = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.gbxCabecera.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.gbxFacturas.SuspendLayout()
        CType(Me.dgvFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxRetencion.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.txtCliente)
        Me.gbxCabecera.Controls.Add(Me.lklVisualizarCobranza)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.flpAnuladoPor)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblCliente)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Location = New System.Drawing.Point(12, 12)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(562, 113)
        Me.gbxCabecera.TabIndex = 0
        Me.gbxCabecera.TabStop = False
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 100
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(77, 47)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(479, 24)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 7
        '
        'lklVisualizarCobranza
        '
        Me.lklVisualizarCobranza.AutoSize = True
        Me.lklVisualizarCobranza.Location = New System.Drawing.Point(447, 81)
        Me.lklVisualizarCobranza.Name = "lklVisualizarCobranza"
        Me.lklVisualizarCobranza.Size = New System.Drawing.Size(109, 13)
        Me.lklVisualizarCobranza.TabIndex = 10
        Me.lklVisualizarCobranza.TabStop = True
        Me.lklVisualizarCobranza.Text = "Vizualizar la cobranza"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(134, 21)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(43, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(297, 21)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(259, 20)
        Me.flpAnuladoPor.TabIndex = 5
        Me.flpAnuladoPor.Visible = False
        '
        'lblAnulado
        '
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Red
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(89, 20)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(98, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(150, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(77, 21)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(57, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(32, 53)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(42, 13)
        Me.lblCliente.TabIndex = 6
        Me.lblCliente.Text = "Cliente:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(77, 77)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(364, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 9
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 7, 18, 9, 5, 29, 681)
        Me.txtFecha.Location = New System.Drawing.Point(217, 21)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 4
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(177, 25)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 3
        Me.lblFecha.Text = "Fecha:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(4, 81)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 8
        Me.lblObservacion.Text = "Observación:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(15, 25)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operación:"
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(98, 463)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 7
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'lblSaldo
        '
        Me.lblSaldo.AutoSize = True
        Me.lblSaldo.Location = New System.Drawing.Point(415, 427)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(37, 13)
        Me.lblSaldo.TabIndex = 4
        Me.lblSaldo.Text = "Saldo:"
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(20, 463)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(75, 23)
        Me.btnBusquedaAvanzada.TabIndex = 6
        Me.btnBusquedaAvanzada.Text = "&Busqueda"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(493, 463)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 11
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(395, 463)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 10
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(318, 463)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 9
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(240, 463)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 8
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lblPorcentajeRetencion
        '
        Me.lblPorcentajeRetencion.AutoSize = True
        Me.lblPorcentajeRetencion.Location = New System.Drawing.Point(337, 24)
        Me.lblPorcentajeRetencion.Name = "lblPorcentajeRetencion"
        Me.lblPorcentajeRetencion.Size = New System.Drawing.Size(41, 13)
        Me.lblPorcentajeRetencion.TabIndex = 3
        Me.lblPorcentajeRetencion.Text = "Ret. %:"
        '
        'lblTotalRetencion
        '
        Me.lblTotalRetencion.AutoSize = True
        Me.lblTotalRetencion.Location = New System.Drawing.Point(423, 51)
        Me.lblTotalRetencion.Name = "lblTotalRetencion"
        Me.lblTotalRetencion.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalRetencion.TabIndex = 9
        Me.lblTotalRetencion.Text = "Total:"
        '
        'gbxFacturas
        '
        Me.gbxFacturas.Controls.Add(Me.lblImporteTotalImpuestoVenta)
        Me.gbxFacturas.Controls.Add(Me.txtImporteTotalImpuestoVenta)
        Me.gbxFacturas.Controls.Add(Me.lblmporteTotalDiscriminadoVenta)
        Me.gbxFacturas.Controls.Add(Me.txtImporteTotalDiscriminadoVenta)
        Me.gbxFacturas.Controls.Add(Me.lblImporteTotalVenta)
        Me.gbxFacturas.Controls.Add(Me.txtImporteTotalVenta)
        Me.gbxFacturas.Controls.Add(Me.Label2)
        Me.gbxFacturas.Controls.Add(Me.Label1)
        Me.gbxFacturas.Controls.Add(Me.txtReferenciaTerminal)
        Me.gbxFacturas.Controls.Add(Me.txtReferenciaSucursal)
        Me.gbxFacturas.Controls.Add(Me.lblImporteRetenido)
        Me.gbxFacturas.Controls.Add(Me.txtImporteRetenido)
        Me.gbxFacturas.Controls.Add(Me.lklEliminarVenta)
        Me.gbxFacturas.Controls.Add(Me.txtTotalRetenido)
        Me.gbxFacturas.Controls.Add(Me.lblTotalRetenido)
        Me.gbxFacturas.Controls.Add(Me.txtTotalIVA)
        Me.gbxFacturas.Controls.Add(Me.lblTotalIVA)
        Me.gbxFacturas.Controls.Add(Me.txtTotalVenta)
        Me.gbxFacturas.Controls.Add(Me.lblTotalVenta)
        Me.gbxFacturas.Controls.Add(Me.dgvFactura)
        Me.gbxFacturas.Controls.Add(Me.lklSeleccionarFactura)
        Me.gbxFacturas.Controls.Add(Me.btnAgregarFactura)
        Me.gbxFacturas.Controls.Add(Me.txtComprobanteFactura)
        Me.gbxFacturas.Location = New System.Drawing.Point(12, 216)
        Me.gbxFacturas.Name = "gbxFacturas"
        Me.gbxFacturas.Size = New System.Drawing.Size(562, 200)
        Me.gbxFacturas.TabIndex = 2
        Me.gbxFacturas.TabStop = False
        Me.gbxFacturas.Text = "Facturas"
        '
        'lblImporteTotalImpuestoVenta
        '
        Me.lblImporteTotalImpuestoVenta.AutoSize = True
        Me.lblImporteTotalImpuestoVenta.Location = New System.Drawing.Point(373, 49)
        Me.lblImporteTotalImpuestoVenta.Name = "lblImporteTotalImpuestoVenta"
        Me.lblImporteTotalImpuestoVenta.Size = New System.Drawing.Size(53, 13)
        Me.lblImporteTotalImpuestoVenta.TabIndex = 14
        Me.lblImporteTotalImpuestoVenta.Text = "Impuesto:"
        '
        'txtImporteTotalImpuestoVenta
        '
        Me.txtImporteTotalImpuestoVenta.Color = System.Drawing.Color.Empty
        Me.txtImporteTotalImpuestoVenta.Decimales = True
        Me.txtImporteTotalImpuestoVenta.Indicaciones = Nothing
        Me.txtImporteTotalImpuestoVenta.Location = New System.Drawing.Point(426, 45)
        Me.txtImporteTotalImpuestoVenta.Name = "txtImporteTotalImpuestoVenta"
        Me.txtImporteTotalImpuestoVenta.Size = New System.Drawing.Size(78, 21)
        Me.txtImporteTotalImpuestoVenta.SoloLectura = False
        Me.txtImporteTotalImpuestoVenta.TabIndex = 15
        Me.txtImporteTotalImpuestoVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteTotalImpuestoVenta.Texto = "0"
        '
        'lblmporteTotalDiscriminadoVenta
        '
        Me.lblmporteTotalDiscriminadoVenta.AutoSize = True
        Me.lblmporteTotalDiscriminadoVenta.Location = New System.Drawing.Point(199, 49)
        Me.lblmporteTotalDiscriminadoVenta.Name = "lblmporteTotalDiscriminadoVenta"
        Me.lblmporteTotalDiscriminadoVenta.Size = New System.Drawing.Size(70, 13)
        Me.lblmporteTotalDiscriminadoVenta.TabIndex = 12
        Me.lblmporteTotalDiscriminadoVenta.Text = "Discriminado:"
        '
        'txtImporteTotalDiscriminadoVenta
        '
        Me.txtImporteTotalDiscriminadoVenta.Color = System.Drawing.Color.Empty
        Me.txtImporteTotalDiscriminadoVenta.Decimales = True
        Me.txtImporteTotalDiscriminadoVenta.Indicaciones = Nothing
        Me.txtImporteTotalDiscriminadoVenta.Location = New System.Drawing.Point(269, 45)
        Me.txtImporteTotalDiscriminadoVenta.Name = "txtImporteTotalDiscriminadoVenta"
        Me.txtImporteTotalDiscriminadoVenta.Size = New System.Drawing.Size(104, 21)
        Me.txtImporteTotalDiscriminadoVenta.SoloLectura = False
        Me.txtImporteTotalDiscriminadoVenta.TabIndex = 13
        Me.txtImporteTotalDiscriminadoVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteTotalDiscriminadoVenta.Texto = "0"
        '
        'lblImporteTotalVenta
        '
        Me.lblImporteTotalVenta.AutoSize = True
        Me.lblImporteTotalVenta.Location = New System.Drawing.Point(4, 49)
        Me.lblImporteTotalVenta.Name = "lblImporteTotalVenta"
        Me.lblImporteTotalVenta.Size = New System.Drawing.Size(91, 13)
        Me.lblImporteTotalVenta.TabIndex = 10
        Me.lblImporteTotalVenta.Text = "Importe de Venta:"
        '
        'txtImporteTotalVenta
        '
        Me.txtImporteTotalVenta.Color = System.Drawing.Color.Empty
        Me.txtImporteTotalVenta.Decimales = True
        Me.txtImporteTotalVenta.Indicaciones = Nothing
        Me.txtImporteTotalVenta.Location = New System.Drawing.Point(95, 45)
        Me.txtImporteTotalVenta.Name = "txtImporteTotalVenta"
        Me.txtImporteTotalVenta.Size = New System.Drawing.Size(104, 21)
        Me.txtImporteTotalVenta.SoloLectura = False
        Me.txtImporteTotalVenta.TabIndex = 11
        Me.txtImporteTotalVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteTotalVenta.Texto = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(205, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(10, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "-"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(169, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(10, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "-"
        '
        'txtReferenciaTerminal
        '
        Me.txtReferenciaTerminal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferenciaTerminal.Color = System.Drawing.Color.Empty
        Me.txtReferenciaTerminal.Indicaciones = Nothing
        Me.txtReferenciaTerminal.Location = New System.Drawing.Point(179, 18)
        Me.txtReferenciaTerminal.Multilinea = False
        Me.txtReferenciaTerminal.Name = "txtReferenciaTerminal"
        Me.txtReferenciaTerminal.Size = New System.Drawing.Size(26, 21)
        Me.txtReferenciaTerminal.SoloLectura = False
        Me.txtReferenciaTerminal.TabIndex = 4
        Me.txtReferenciaTerminal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferenciaTerminal.Texto = "001"
        '
        'txtReferenciaSucursal
        '
        Me.txtReferenciaSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferenciaSucursal.Color = System.Drawing.Color.Empty
        Me.txtReferenciaSucursal.Indicaciones = Nothing
        Me.txtReferenciaSucursal.Location = New System.Drawing.Point(143, 18)
        Me.txtReferenciaSucursal.Multilinea = False
        Me.txtReferenciaSucursal.Name = "txtReferenciaSucursal"
        Me.txtReferenciaSucursal.Size = New System.Drawing.Size(26, 21)
        Me.txtReferenciaSucursal.SoloLectura = False
        Me.txtReferenciaSucursal.TabIndex = 1
        Me.txtReferenciaSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferenciaSucursal.Texto = "001"
        '
        'lblImporteRetenido
        '
        Me.lblImporteRetenido.AutoSize = True
        Me.lblImporteRetenido.Location = New System.Drawing.Point(313, 22)
        Me.lblImporteRetenido.Name = "lblImporteRetenido"
        Me.lblImporteRetenido.Size = New System.Drawing.Size(91, 13)
        Me.lblImporteRetenido.TabIndex = 7
        Me.lblImporteRetenido.Text = "Importe Retenido:"
        '
        'txtImporteRetenido
        '
        Me.txtImporteRetenido.Color = System.Drawing.Color.Empty
        Me.txtImporteRetenido.Decimales = True
        Me.txtImporteRetenido.Indicaciones = Nothing
        Me.txtImporteRetenido.Location = New System.Drawing.Point(410, 18)
        Me.txtImporteRetenido.Name = "txtImporteRetenido"
        Me.txtImporteRetenido.Size = New System.Drawing.Size(78, 21)
        Me.txtImporteRetenido.SoloLectura = False
        Me.txtImporteRetenido.TabIndex = 8
        Me.txtImporteRetenido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteRetenido.Texto = "0"
        '
        'lklEliminarVenta
        '
        Me.lklEliminarVenta.AutoSize = True
        Me.lklEliminarVenta.Location = New System.Drawing.Point(12, 168)
        Me.lklEliminarVenta.Name = "lklEliminarVenta"
        Me.lklEliminarVenta.Size = New System.Drawing.Size(43, 13)
        Me.lklEliminarVenta.TabIndex = 17
        Me.lklEliminarVenta.TabStop = True
        Me.lklEliminarVenta.Text = "Eliminar"
        '
        'txtTotalRetenido
        '
        Me.txtTotalRetenido.Color = System.Drawing.Color.Empty
        Me.txtTotalRetenido.Decimales = True
        Me.txtTotalRetenido.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalRetenido.Indicaciones = Nothing
        Me.txtTotalRetenido.Location = New System.Drawing.Point(447, 168)
        Me.txtTotalRetenido.Name = "txtTotalRetenido"
        Me.txtTotalRetenido.Size = New System.Drawing.Size(108, 22)
        Me.txtTotalRetenido.SoloLectura = True
        Me.txtTotalRetenido.TabIndex = 0
        Me.txtTotalRetenido.TabStop = False
        Me.txtTotalRetenido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalRetenido.Texto = "0"
        '
        'lblTotalRetenido
        '
        Me.lblTotalRetenido.AutoSize = True
        Me.lblTotalRetenido.Location = New System.Drawing.Point(388, 173)
        Me.lblTotalRetenido.Name = "lblTotalRetenido"
        Me.lblTotalRetenido.Size = New System.Drawing.Size(53, 13)
        Me.lblTotalRetenido.TabIndex = 22
        Me.lblTotalRetenido.Text = "Retenido:"
        '
        'txtTotalIVA
        '
        Me.txtTotalIVA.Color = System.Drawing.Color.Empty
        Me.txtTotalIVA.Decimales = True
        Me.txtTotalIVA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalIVA.Indicaciones = Nothing
        Me.txtTotalIVA.Location = New System.Drawing.Point(303, 168)
        Me.txtTotalIVA.Name = "txtTotalIVA"
        Me.txtTotalIVA.Size = New System.Drawing.Size(80, 22)
        Me.txtTotalIVA.SoloLectura = True
        Me.txtTotalIVA.TabIndex = 21
        Me.txtTotalIVA.TabStop = False
        Me.txtTotalIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalIVA.Texto = "0"
        '
        'lblTotalIVA
        '
        Me.lblTotalIVA.AutoSize = True
        Me.lblTotalIVA.Location = New System.Drawing.Point(276, 173)
        Me.lblTotalIVA.Name = "lblTotalIVA"
        Me.lblTotalIVA.Size = New System.Drawing.Size(27, 13)
        Me.lblTotalIVA.TabIndex = 20
        Me.lblTotalIVA.Text = "IVA:"
        '
        'txtTotalVenta
        '
        Me.txtTotalVenta.Color = System.Drawing.Color.Empty
        Me.txtTotalVenta.Decimales = True
        Me.txtTotalVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalVenta.Indicaciones = Nothing
        Me.txtTotalVenta.Location = New System.Drawing.Point(168, 168)
        Me.txtTotalVenta.Name = "txtTotalVenta"
        Me.txtTotalVenta.Size = New System.Drawing.Size(97, 22)
        Me.txtTotalVenta.SoloLectura = True
        Me.txtTotalVenta.TabIndex = 19
        Me.txtTotalVenta.TabStop = False
        Me.txtTotalVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalVenta.Texto = "0"
        '
        'lblTotalVenta
        '
        Me.lblTotalVenta.AutoSize = True
        Me.lblTotalVenta.Location = New System.Drawing.Point(134, 173)
        Me.lblTotalVenta.Name = "lblTotalVenta"
        Me.lblTotalVenta.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalVenta.TabIndex = 18
        Me.lblTotalVenta.Text = "Total:"
        '
        'dgvFactura
        '
        Me.dgvFactura.AllowUserToAddRows = False
        Me.dgvFactura.AllowUserToDeleteRows = False
        Me.dgvFactura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFactura.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colID, Me.colIDTransaccionVenta, Me.colComprobante, Me.colFecha, Me.colTotal, Me.colTotalDiscriminado, Me.colTotalImpuesto, Me.colImporte})
        Me.dgvFactura.Location = New System.Drawing.Point(6, 72)
        Me.dgvFactura.MultiSelect = False
        Me.dgvFactura.Name = "dgvFactura"
        Me.dgvFactura.ReadOnly = True
        Me.dgvFactura.RowHeadersVisible = False
        Me.dgvFactura.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvFactura.Size = New System.Drawing.Size(549, 86)
        Me.dgvFactura.StandardTab = True
        Me.dgvFactura.TabIndex = 16
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colID
        '
        Me.colID.HeaderText = "ID"
        Me.colID.Name = "colID"
        Me.colID.ReadOnly = True
        Me.colID.Visible = False
        '
        'colIDTransaccionVenta
        '
        Me.colIDTransaccionVenta.HeaderText = "IDTransaccionVenta"
        Me.colIDTransaccionVenta.Name = "colIDTransaccionVenta"
        Me.colIDTransaccionVenta.ReadOnly = True
        Me.colIDTransaccionVenta.Visible = False
        '
        'colComprobante
        '
        Me.colComprobante.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colComprobante.HeaderText = "Comprobante"
        Me.colComprobante.Name = "colComprobante"
        Me.colComprobante.ReadOnly = True
        '
        'colFecha
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colFecha.DefaultCellStyle = DataGridViewCellStyle1
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.ReadOnly = True
        Me.colFecha.Width = 75
        '
        'colTotal
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        Me.colTotal.DefaultCellStyle = DataGridViewCellStyle2
        Me.colTotal.HeaderText = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.ReadOnly = True
        '
        'colTotalDiscriminado
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        Me.colTotalDiscriminado.DefaultCellStyle = DataGridViewCellStyle3
        Me.colTotalDiscriminado.HeaderText = "Discriminado"
        Me.colTotalDiscriminado.Name = "colTotalDiscriminado"
        Me.colTotalDiscriminado.ReadOnly = True
        '
        'colTotalImpuesto
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        Me.colTotalImpuesto.DefaultCellStyle = DataGridViewCellStyle4
        Me.colTotalImpuesto.HeaderText = "IVA"
        Me.colTotalImpuesto.Name = "colTotalImpuesto"
        Me.colTotalImpuesto.ReadOnly = True
        Me.colTotalImpuesto.Width = 75
        '
        'colImporte
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle5
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        Me.colImporte.Width = 75
        '
        'lklSeleccionarFactura
        '
        Me.lklSeleccionarFactura.AutoSize = True
        Me.lklSeleccionarFactura.Location = New System.Drawing.Point(12, 22)
        Me.lklSeleccionarFactura.Name = "lklSeleccionarFactura"
        Me.lklSeleccionarFactura.Size = New System.Drawing.Size(123, 13)
        Me.lklSeleccionarFactura.TabIndex = 0
        Me.lklSeleccionarFactura.TabStop = True
        Me.lklSeleccionarFactura.Text = "Otros comprobantes (F2)"
        '
        'btnAgregarFactura
        '
        Me.btnAgregarFactura.Location = New System.Drawing.Point(497, 16)
        Me.btnAgregarFactura.Name = "btnAgregarFactura"
        Me.btnAgregarFactura.Size = New System.Drawing.Size(58, 23)
        Me.btnAgregarFactura.TabIndex = 9
        Me.btnAgregarFactura.Text = "Agregar"
        Me.btnAgregarFactura.UseVisualStyleBackColor = True
        '
        'txtComprobanteFactura
        '
        Me.txtComprobanteFactura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobanteFactura.Color = System.Drawing.Color.Empty
        Me.txtComprobanteFactura.Indicaciones = Nothing
        Me.txtComprobanteFactura.Location = New System.Drawing.Point(215, 18)
        Me.txtComprobanteFactura.Multilinea = False
        Me.txtComprobanteFactura.Name = "txtComprobanteFactura"
        Me.txtComprobanteFactura.Size = New System.Drawing.Size(89, 21)
        Me.txtComprobanteFactura.SoloLectura = False
        Me.txtComprobanteFactura.TabIndex = 6
        Me.txtComprobanteFactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobanteFactura.Texto = ""
        '
        'gbxRetencion
        '
        Me.gbxRetencion.Controls.Add(Me.txtTotalRetencion)
        Me.gbxRetencion.Controls.Add(Me.txtObservacionRetencion)
        Me.gbxRetencion.Controls.Add(Me.lblTotalRetencion)
        Me.gbxRetencion.Controls.Add(Me.lblObservacionRetencion)
        Me.gbxRetencion.Controls.Add(Me.lblPorcentajeRetencion)
        Me.gbxRetencion.Controls.Add(Me.txtPorcentajeRetencion)
        Me.gbxRetencion.Controls.Add(Me.txtFechaRetencion)
        Me.gbxRetencion.Controls.Add(Me.lblFechaRetencion)
        Me.gbxRetencion.Controls.Add(Me.btnSeleccionarRetencion)
        Me.gbxRetencion.Controls.Add(Me.txtComprobanteRetencion)
        Me.gbxRetencion.Controls.Add(Me.lblComprobanteRetencion)
        Me.gbxRetencion.Location = New System.Drawing.Point(12, 131)
        Me.gbxRetencion.Name = "gbxRetencion"
        Me.gbxRetencion.Size = New System.Drawing.Size(562, 79)
        Me.gbxRetencion.TabIndex = 1
        Me.gbxRetencion.TabStop = False
        Me.gbxRetencion.Text = "Retencion"
        '
        'txtTotalRetencion
        '
        Me.txtTotalRetencion.Color = System.Drawing.Color.Empty
        Me.txtTotalRetencion.Decimales = True
        Me.txtTotalRetencion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalRetencion.Indicaciones = Nothing
        Me.txtTotalRetencion.Location = New System.Drawing.Point(462, 46)
        Me.txtTotalRetencion.Name = "txtTotalRetencion"
        Me.txtTotalRetencion.Size = New System.Drawing.Size(94, 22)
        Me.txtTotalRetencion.SoloLectura = True
        Me.txtTotalRetencion.TabIndex = 10
        Me.txtTotalRetencion.TabStop = False
        Me.txtTotalRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalRetencion.Texto = "0"
        '
        'txtObservacionRetencion
        '
        Me.txtObservacionRetencion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacionRetencion.Color = System.Drawing.Color.Empty
        Me.txtObservacionRetencion.Indicaciones = Nothing
        Me.txtObservacionRetencion.Location = New System.Drawing.Point(84, 47)
        Me.txtObservacionRetencion.Multilinea = False
        Me.txtObservacionRetencion.Name = "txtObservacionRetencion"
        Me.txtObservacionRetencion.Size = New System.Drawing.Size(327, 21)
        Me.txtObservacionRetencion.SoloLectura = True
        Me.txtObservacionRetencion.TabIndex = 8
        Me.txtObservacionRetencion.TabStop = False
        Me.txtObservacionRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacionRetencion.Texto = ""
        '
        'lblObservacionRetencion
        '
        Me.lblObservacionRetencion.AutoSize = True
        Me.lblObservacionRetencion.Location = New System.Drawing.Point(12, 51)
        Me.lblObservacionRetencion.Name = "lblObservacionRetencion"
        Me.lblObservacionRetencion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacionRetencion.TabIndex = 7
        Me.lblObservacionRetencion.Text = "Observación:"
        '
        'txtPorcentajeRetencion
        '
        Me.txtPorcentajeRetencion.Color = System.Drawing.Color.Empty
        Me.txtPorcentajeRetencion.Decimales = True
        Me.txtPorcentajeRetencion.Indicaciones = Nothing
        Me.txtPorcentajeRetencion.Location = New System.Drawing.Point(384, 20)
        Me.txtPorcentajeRetencion.Name = "txtPorcentajeRetencion"
        Me.txtPorcentajeRetencion.Size = New System.Drawing.Size(27, 21)
        Me.txtPorcentajeRetencion.SoloLectura = False
        Me.txtPorcentajeRetencion.TabIndex = 4
        Me.txtPorcentajeRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajeRetencion.Texto = "0"
        '
        'txtFechaRetencion
        '
        Me.txtFechaRetencion.Color = System.Drawing.Color.Empty
        Me.txtFechaRetencion.Fecha = New Date(2013, 7, 18, 9, 5, 29, 681)
        Me.txtFechaRetencion.Location = New System.Drawing.Point(462, 20)
        Me.txtFechaRetencion.Name = "txtFechaRetencion"
        Me.txtFechaRetencion.PermitirNulo = False
        Me.txtFechaRetencion.Size = New System.Drawing.Size(93, 20)
        Me.txtFechaRetencion.SoloLectura = True
        Me.txtFechaRetencion.TabIndex = 6
        Me.txtFechaRetencion.TabStop = False
        '
        'lblFechaRetencion
        '
        Me.lblFechaRetencion.AutoSize = True
        Me.lblFechaRetencion.Location = New System.Drawing.Point(423, 24)
        Me.lblFechaRetencion.Name = "lblFechaRetencion"
        Me.lblFechaRetencion.Size = New System.Drawing.Size(40, 13)
        Me.lblFechaRetencion.TabIndex = 5
        Me.lblFechaRetencion.Text = "Fecha:"
        '
        'btnSeleccionarRetencion
        '
        Me.btnSeleccionarRetencion.Location = New System.Drawing.Point(13, 19)
        Me.btnSeleccionarRetencion.Name = "btnSeleccionarRetencion"
        Me.btnSeleccionarRetencion.Size = New System.Drawing.Size(128, 23)
        Me.btnSeleccionarRetencion.TabIndex = 0
        Me.btnSeleccionarRetencion.Text = "Seleccionar Retencion:"
        Me.btnSeleccionarRetencion.UseVisualStyleBackColor = True
        '
        'txtComprobanteRetencion
        '
        Me.txtComprobanteRetencion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobanteRetencion.Color = System.Drawing.Color.Empty
        Me.txtComprobanteRetencion.Indicaciones = Nothing
        Me.txtComprobanteRetencion.Location = New System.Drawing.Point(216, 20)
        Me.txtComprobanteRetencion.Multilinea = False
        Me.txtComprobanteRetencion.Name = "txtComprobanteRetencion"
        Me.txtComprobanteRetencion.Size = New System.Drawing.Size(115, 21)
        Me.txtComprobanteRetencion.SoloLectura = True
        Me.txtComprobanteRetencion.TabIndex = 2
        Me.txtComprobanteRetencion.TabStop = False
        Me.txtComprobanteRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobanteRetencion.Texto = ""
        '
        'lblComprobanteRetencion
        '
        Me.lblComprobanteRetencion.AutoSize = True
        Me.lblComprobanteRetencion.Location = New System.Drawing.Point(147, 24)
        Me.lblComprobanteRetencion.Name = "lblComprobanteRetencion"
        Me.lblComprobanteRetencion.Size = New System.Drawing.Size(63, 13)
        Me.lblComprobanteRetencion.TabIndex = 1
        Me.lblComprobanteRetencion.Text = "Nro. Comp.:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(12, 422)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(404, 20)
        Me.flpRegistradoPor.TabIndex = 3
        Me.flpRegistradoPor.Visible = False
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(460, 422)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(108, 22)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 5
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'frmAsociarRetencionCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(586, 498)
        Me.Controls.Add(Me.gbxRetencion)
        Me.Controls.Add(Me.gbxFacturas)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.txtSaldo)
        Me.Controls.Add(Me.lblSaldo)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Name = "frmAsociarRetencionCliente"
        Me.Text = "frmAsociarRetencion"
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.gbxFacturas.ResumeLayout(False)
        Me.gbxFacturas.PerformLayout()
        CType(Me.dgvFactura, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxRetencion.ResumeLayout(False)
        Me.gbxRetencion.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents txtSaldo As ERP.ocxTXTNumeric
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lblPorcentajeRetencion As System.Windows.Forms.Label
    Friend WithEvents txtPorcentajeRetencion As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalRetencion As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalRetencion As System.Windows.Forms.Label
    Friend WithEvents gbxFacturas As System.Windows.Forms.GroupBox
    Friend WithEvents gbxRetencion As System.Windows.Forms.GroupBox
    Friend WithEvents lklSeleccionarFactura As System.Windows.Forms.LinkLabel
    Friend WithEvents btnAgregarFactura As System.Windows.Forms.Button
    Friend WithEvents txtComprobanteFactura As ERP.ocxTXTString
    Friend WithEvents dgvFactura As System.Windows.Forms.DataGridView
    Friend WithEvents txtTotalIVA As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalIVA As System.Windows.Forms.Label
    Friend WithEvents txtTotalVenta As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalVenta As System.Windows.Forms.Label
    Friend WithEvents txtFechaRetencion As ERP.ocxTXTDate
    Friend WithEvents lblFechaRetencion As System.Windows.Forms.Label
    Friend WithEvents btnSeleccionarRetencion As System.Windows.Forms.Button
    Friend WithEvents txtComprobanteRetencion As ERP.ocxTXTString
    Friend WithEvents lblComprobanteRetencion As System.Windows.Forms.Label
    Friend WithEvents txtObservacionRetencion As ERP.ocxTXTString
    Friend WithEvents lblObservacionRetencion As System.Windows.Forms.Label
    Friend WithEvents txtTotalRetenido As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalRetenido As System.Windows.Forms.Label
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lklEliminarVenta As System.Windows.Forms.LinkLabel
    Friend WithEvents lblImporteRetenido As System.Windows.Forms.Label
    Friend WithEvents txtImporteRetenido As ERP.ocxTXTNumeric
    Friend WithEvents lklVisualizarCobranza As System.Windows.Forms.LinkLabel
    Friend WithEvents colIDTransaccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIDTransaccionVenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotalDiscriminado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotalImpuesto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtReferenciaTerminal As ERP.ocxTXTString
    Friend WithEvents txtReferenciaSucursal As ERP.ocxTXTString
    Friend WithEvents lblImporteTotalImpuestoVenta As System.Windows.Forms.Label
    Friend WithEvents txtImporteTotalImpuestoVenta As ERP.ocxTXTNumeric
    Friend WithEvents lblmporteTotalDiscriminadoVenta As System.Windows.Forms.Label
    Friend WithEvents txtImporteTotalDiscriminadoVenta As ERP.ocxTXTNumeric
    Friend WithEvents lblImporteTotalVenta As System.Windows.Forms.Label
    Friend WithEvents txtImporteTotalVenta As ERP.ocxTXTNumeric
End Class

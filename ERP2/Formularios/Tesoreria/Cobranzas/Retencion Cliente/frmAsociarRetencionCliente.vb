﻿Public Class frmAsociarRetencionCliente

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CData As New CData

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dt As New DataTable
    Dim dtVentas As New DataTable
    Dim RowVenta As DataRow
    Dim vRetencionSeleccionado As DataRow
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim vImporteRetencion As Decimal

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Otros

        'Propiedades
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "ASOCIAR RETENCION A VENTA", "RVE")
        vNuevo = False

        'Controles
        txtCliente.Conectar()

        'Funciones
        CargarInformacion()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, btnSeleccionarRetencion)

        'Facturas
        CSistema.CargaControl(vControles, lklSeleccionarFactura)
        CSistema.CargaControl(vControles, txtReferenciaSucursal)
        CSistema.CargaControl(vControles, txtReferenciaTerminal)
        CSistema.CargaControl(vControles, txtComprobanteFactura)
        CSistema.CargaControl(vControles, txtImporteRetenido)
        CSistema.CargaControl(vControles, btnAgregarFactura)
        CSistema.CargaControl(vControles, lklEliminarVenta)
        CSistema.CargaControl(vControles, txtImporteTotalDiscriminadoVenta)
        CSistema.CargaControl(vControles, txtImporteTotalImpuestoVenta)
        CSistema.CargaControl(vControles, txtImporteTotalVenta)

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal ")

        'CARGAR ESTRUCTURA DEL DETALLE VENTA
        dtVentas = CSistema.ExecuteToDataTable("Select Top(0) * From VRetencionClientePendiente ").Clone

        'CARGAR CONTROLES

        'CARGAR LA ULTIMA CONFIGURACION

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Referencia de Sucursal
        txtReferenciaSucursal.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "REFERENCIA SUCURSAL", vgReferenciaSucursal))

        'Referencia Terminal
        txtReferenciaTerminal.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "REFERENCIA TERMINAL", "001"))

        'Ultimo Registro
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From AsociarVentaRetencion Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)

    End Sub

    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

        'Referencia de Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "REFERENCIA SUCURSAL", txtReferenciaSucursal.GetValue)

        'Referencia Terminal
        CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "REFERENCIA TERMINAL", txtReferenciaTerminal.GetValue)

    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Limpiar detalle
        dtVentas.Rows.Clear()
        ListarVentas()

        vNuevo = True

        'Cabecera
        txtFecha.txt.Text = ""
        txtObservacion.txt.Clear()
        txtCliente.Clear()

        'Retencion
        btnSeleccionarRetencion.Enabled = True
        txtPorcentajeRetencion.SoloLectura = False
        txtPorcentajeRetencion.SetValue(0)
        txtComprobanteRetencion.Clear()
        txtFechaRetencion.Clear()
        txtObservacionRetencion.Clear()
        txtTotalRetencion.SetValue(0)
        txtTotalRetenido.SetValue(0)
        CalcularTotales()

        vRetencionSeleccionado = Nothing


        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero+1) From AsociarVentaRetencion Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)

        'Bloquear Nro de Operacion
        txtID.SoloLectura = True
        cbxSucursal.SoloLectura = True

        LimpiarControles()

        'Foco
        txtFecha.txt.Focus()

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        'LimpiarControles()
        dtVentas.Rows.Clear()
        ListarVentas()

        'Ultimo Registro
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.SoloLectura = False
        cbxSucursal.SoloLectura = False

        txtID.txt.Focus()
        txtID.txt.SelectAll()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Cuando no es por anulacion
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            'Sucursal
            If cbxSucursal.cbx.SelectedValue = Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal!"
                MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Function
            End If

            If cbxSucursal.cbx.Text.Trim = "" Then
                Dim mensaje As String = "Seleccione correctamente la sucursal!"
                MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Function
            End If

            If txtTotalVenta.ObtenerValor <= 0 Then
                Dim mensaje As String = "El importe de los comprobantes de venta no es válido!"
                MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Function
            End If

            If txtSaldo.ObtenerValor <> 0 Then
                Dim mensaje As String = "¡Los importes no son validos!"
                MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Function
            End If

        End If

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("¡Atención! Esto anulará permanentemente el registro. ¿Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If

        End If

        Return True


    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", CInt(txtID.ObtenerValor), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", txtFecha.GetValue.Date, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text.Trim, ParameterDirection.Input)

        'Retencion
        CSistema.SetSQLParameter(param, "@IDTransaccionRetencion", vRetencionSeleccionado("IDTransaccion"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDFormaPago", vRetencionSeleccionado("ID"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PorcentajeRetencion", txtPorcentajeRetencion.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(txtTotalRetencion.ObtenerValor), ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAsociarVentaRetencion", False, False, MensajeRetorno, IDTransaccion, False) = False Then
            MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub

        End If

        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

            CargarOperacion(IDTransaccion)

            txtID.SoloLectura = False
            cbxSucursal.SoloLectura = False
            Exit Sub

        End If

        If IDTransaccion > 0 Then

            'Cargamos la Forma de Pago
            InsertarFormaPago()

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False
        cbxSucursal.SoloLectura = False

        txtID.txt.Focus()

    End Sub

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        If ValidarDocumento(Operacion) = False Then
            txtID.Focus()
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", CInt(txtID.ObtenerValor), ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAsociarVentaRetencion", False, False, MensajeRetorno, IDTransaccion, False) = False Then
            MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False
        cbxSucursal.SoloLectura = False

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From AsociarVentaRetencion Where IDSucursal=" & cbxSucursal.GetValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From AsociarVentaRetencion Where IDSucursal=" & cbxSucursal.GetValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub SeleccionarOtrasFacturas()

        If txtCliente.Seleccionado = False Then
            Exit Sub
        End If

        If vRetencionSeleccionado Is Nothing Then
            Exit Sub
        End If

        Dim frm As New frmRetencionClienteSeleccionarOtrasVentas
        frm.IDFormaPago = vRetencionSeleccionado("ID")
        frm.IDTransaccion = vRetencionSeleccionado("IDTransaccion")
        frm.PorcentajeRetencion = txtPorcentajeRetencion.ObtenerValor
        frm.ReferenciaSucursal = txtReferenciaSucursal.GetValue
        frm.ReferenciaTerminal = txtReferenciaTerminal.GetValue

        FGMostrarFormulario(Me, frm, "Seleccione la factura para agregar", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        RowVenta = frm.RowVenta

        CargarVenta(0, True)

    End Sub

    Function InsertarFormaPago() As Boolean

        InsertarFormaPago = True

        Dim dtDocumento As DataTable = Nothing
        Dim dtVentaRetencion As DataTable = Nothing

        'Cargar el Documento
        For Each oRow As DataRow In dtVentas.Rows

            Dim sql As String = "Insert Into FormaPagoDocumentoRetencion(IDTransaccion, ID, IDTransaccionVenta, Porcentaje, Importe, Total, TotalDiscriminado, TotalImpuesto) values(" & vRetencionSeleccionado("IDTransaccion") & ", " & vRetencionSeleccionado("ID") & ", " & oRow("IDTransaccionVenta") & ", '" & txtPorcentajeRetencion.ObtenerValor & "', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString) & ")"

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

        Next

    End Function

    Sub BuscarVenta()

        RowVenta = Nothing

        Dim vComprobante As String = txtReferenciaSucursal.GetValue & "-" & txtReferenciaTerminal.GetValue & "-" & txtComprobanteFactura.GetValue

        'Validar
        'Comprobante
        If txtPorcentajeRetencion.ObtenerValor = 0 Then
            MessageBox.Show("Debe especificar el porcentaje de retencion para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtPorcentajeRetencion.txt.Focus()
            Exit Sub
        End If

        'Comprobante
        If txtComprobanteFactura.GetValue.Length = 0 Then
            'MessageBox.Show("Debe especificar un nro de comprobante para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            'txtComprobanteFactura.txt.Focus()
            Exit Sub
        End If

        Dim SQL As String = "Select IDTransaccion, ID, IDTransaccionVenta, ComprobanteVenta, FechaEmision, Total, TotalDiscriminado, TotalImpuesto, Importe From VRetencionClientePendiente Where IDTransaccion=" & vRetencionSeleccionado("IDTransaccion") & " And ID=" & vRetencionSeleccionado("ID") & " And ComprobanteVenta='" & vComprobante & "' "

        Dim vdt As DataTable = CSistema.ExecuteToDataTable(SQL)

        If vdt Is Nothing Then
            MessageBox.Show("No se encontro la venta para este cliente con esta retencion!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtComprobanteFactura.txt.Focus()
            Exit Sub
        End If

        If vdt.Rows.Count = 0 Then
            MessageBox.Show("No se encontro la venta para este cliente con esta retencion!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        RowVenta = vdt.Rows(0)

        'Ponemos el monto sugerido, redondeado
        txtImporteRetenido.SetValue(Math.Round(RowVenta("TotalImpuesto") * (txtPorcentajeRetencion.ObtenerValor / 100), 0))
        vImporteRetencion = txtImporteRetenido.ObtenerValor
        txtImporteTotalVenta.SetValue(RowVenta("Total"))
        txtImporteTotalDiscriminadoVenta.SetValue(RowVenta("TotalDiscriminado"))
        txtImporteTotalImpuestoVenta.SetValue(RowVenta("TotalImpuesto"))

    End Sub

    Sub CargarVenta(Optional ByVal IDTransaccion As Integer = 0, Optional Otros As Boolean = False)

        'Validar
        'Comprobante
        If Otros = False Then
            If txtComprobanteFactura.GetValue.Length = 0 Then
                MessageBox.Show("Debe especificar un nro de comprobante para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtComprobanteFactura.txt.Focus()
                Exit Sub
            End If
        End If

        'Seleccionado
        If RowVenta Is Nothing Then
            MessageBox.Show("Seleccione correctamente un comprobante de venta!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtComprobanteFactura.txt.Focus()
            Exit Sub
        End If

        'Importe
        If Otros = False Then
            If txtImporteRetenido.ObtenerValor = 0 Then
                MessageBox.Show("El importe no puede ser 0!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtImporteRetenido.txt.Focus()
                Exit Sub
            End If
        End If


        'Agregar al datatable
        Dim NewRow As DataRow = dtVentas.NewRow
        NewRow("IDTransaccion") = RowVenta("IDTransaccion")
        NewRow("ID") = RowVenta("ID")
        NewRow("IDTransaccionVenta") = RowVenta("IDTransaccionVenta")
        NewRow("ComprobanteVenta") = RowVenta("ComprobanteVenta")
        NewRow("FechaEmision") = RowVenta("FechaEmision")

        If Otros = False Then
            NewRow("Importe") = txtImporteRetenido.ObtenerValor
            NewRow("Total") = txtImporteTotalVenta.ObtenerValor
            NewRow("TotalDiscriminado") = txtImporteTotalDiscriminadoVenta.ObtenerValor
            NewRow("TotalImpuesto") = txtImporteTotalImpuestoVenta.ObtenerValor
        Else
            NewRow("Importe") = RowVenta("Importe")
            NewRow("Total") = RowVenta("Total")
            NewRow("TotalDiscriminado") = RowVenta("TotalDiscriminado")
            NewRow("TotalImpuesto") = RowVenta("TotalImpuesto")
        End If

        dtVentas.Rows.Add(NewRow)

        ListarVentas()

        'Limpiar controles
        RowVenta = Nothing
        txtComprobanteFactura.txt.Clear()
        txtImporteRetenido.SetValue(0)

        txtImporteTotalVenta.SetValue(0)
        txtImporteTotalDiscriminadoVenta.SetValue(0)
        txtImporteTotalImpuestoVenta.SetValue(0)

        txtComprobanteFactura.txt.Focus()

    End Sub

    Sub ListarVentas()

        dgvFactura.Rows.Clear()

        For Each oRow As DataRow In dtVentas.Rows

            Dim Registro(dgvFactura.Columns.Count - 1) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("ID").ToString
            Registro(2) = oRow("IDTransaccionVenta").ToString
            Registro(3) = oRow("ComprobanteVenta").ToString
            Registro(4) = oRow("FechaEmision").ToString
            Registro(5) = CSistema.FormatoNumero(oRow("Total").ToString)
            Registro(6) = CSistema.FormatoMoneda(oRow("TotalDiscriminado").ToString)
            Registro(7) = CSistema.FormatoMoneda(oRow("TotalImpuesto").ToString)
            Registro(8) = CSistema.FormatoMoneda(oRow("Importe").ToString)

            dgvFactura.Rows.Add(Registro)

        Next

        'Bloquear controles de Retencion si es que se agregaron facturas
        If vNuevo = True Then
            If dtVentas.Rows.Count > 0 Then
                btnSeleccionarRetencion.Enabled = False
                txtPorcentajeRetencion.SoloLectura = True
            Else
                btnSeleccionarRetencion.Enabled = True
                txtPorcentajeRetencion.SoloLectura = False
            End If
        End If
        
        CalcularTotales()

    End Sub

    Sub EliminarVenta()

        For Each item As DataGridViewRow In dgvFactura.SelectedRows

            Dim Comprobante As String = item.Cells("colComprobante").Value

            For Each oRow As DataRow In dtVentas.Select("ComprobanteVenta='" & Comprobante & "'")
                dtVentas.Rows.Remove(oRow)
            Next
            Exit For

        Next

        ListarVentas()

    End Sub

    Sub CalcularTotales()

        txtTotalVenta.SetValue(0)
        txtTotalIVA.SetValue(0)
        txtTotalRetenido.SetValue(0)
        txtSaldo.SetValue(txtTotalRetencion.ObtenerValor)

        If dtVentas Is Nothing Then
            Exit Sub
        End If

        If dtVentas.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim Total As Decimal = dtVentas.Compute("Sum(Total)", "0=0")
        Dim IVA As Decimal = dtVentas.Compute("Sum(TotalImpuesto)", "0=0")
        Dim Retenido As Decimal = dtVentas.Compute("Sum(Importe)", "0=0")
        Dim Saldo As Decimal = 0

        'Calcular
        Saldo = txtTotalRetencion.ObtenerValor - Retenido

        txtTotalVenta.SetValue(Total)
        txtTotalIVA.SetValue(IVA)
        txtTotalRetenido.SetValue(Retenido)
        txtSaldo.SetValue(Saldo)

    End Sub

    Sub Buscar()

        Dim frm As New frmConsultaAsociarRetencionCliente
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog()
        CargarOperacion(frm.IDTransaccion)

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        txtID.txt.Focus()
        txtID.txt.SelectAll()
        txtPorcentajeRetencion.SoloLectura = True

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select top 1 IsNull((Select top 1 IDTransaccion From AsociarVentaRetencion Where Numero=" & txtID.ObtenerValor & " and IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            If txtID.ObtenerValor > 1 Then
                Dim mensaje As String = "El sistema no encuentra el registro!"
                MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dt = CSistema.ExecuteToDataTable("Select top 1 * From VAsociarVentaRetencion Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas técnicos."
            MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        dtVentas = CSistema.ExecuteToDataTable("Select * From VFormaPagoDocumentoRetencion Where IDTransaccion=" & oRow("IDTransaccionRetencion").ToString & " And ID=" & oRow("IDFormaPago").ToString)

        txtCliente.txtReferencia.txt.Text = oRow("Referencia").ToString
        txtCliente.txtRazonSocial.txt.Text = oRow("Cliente").ToString
        txtCliente.txtRUC.txt.Text = oRow("RUC").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        txtCliente.SetValue(oRow("IDCliente").ToString)
        txtObservacion.txt.Text = oRow("Observacion").ToString

        'Retencion
        txtComprobanteRetencion.SetValue(oRow("Comprobante").ToString)
        txtPorcentajeRetencion.SetValue(oRow("PorcentajeRetencion").ToString)
        txtFechaRetencion.SetValue(oRow("FechaRetencion").ToString)
        txtTotalRetencion.SetValue(oRow("Total").ToString)
        txtObservacion.SetValue(oRow("ObservacionRetencion").ToString)

        'Visible Label
        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then

            'Visible Label
            lblRegistradoPor.Visible = True
            lblUsuarioAnulado.Visible = True
            lblUsuarioRegistro.Visible = True
            lblFechaRegistro.Visible = True
            lblAnulado.Visible = True
            lblAnulado.Visible = True
            lblFechaAnulado.Visible = True

            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos el detalle de Venta
        ListarVentas()

        'Calcular Totales
        CalcularTotales()

    End Sub

    Sub SeleccionarRetencion()

        vRetencionSeleccionado = Nothing

        'Validar
        'Cliente
        If txtCliente.Seleccionado = False Then
            MessageBox.Show("Seleccione un cliente para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim frm As New frmSeleccionarRetencionCliente
        frm.IDCliente = txtCliente.Registro("ID")
        frm.Cliente = txtCliente.Registro("RazonSocial")

        FGMostrarFormulario(Me, frm, "Retenciones de Clientes pendientes de asociacion", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Seleccionado = False Then
            Exit Sub
        End If

        txtComprobanteRetencion.SetValue(frm.dt.Rows(0)("Comprobante"))
        txtFechaRetencion.SetValue(frm.dt.Rows(0)("Fecha"))
        txtObservacionRetencion.SetValue(frm.dt.Rows(0)("Observacion"))
        txtTotalRetencion.SetValue(frm.dt.Rows(0)("Importe"))

        vRetencionSeleccionado = frm.dt.Rows(0)

        'Cambiar de foco
        txtPorcentajeRetencion.txt.Focus()


    End Sub

    Sub LimpiarControles()
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False
    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, New Button, btnBusquedaAvanzada, New Button, vControles)

    End Sub

    Sub VisualizarCobranza()

        'Validar
        Dim IDTransaccionDocumento As Integer

        If Not vRetencionSeleccionado Is Nothing Then
            IDTransaccionDocumento = vRetencionSeleccionado("IDTransaccion")
        Else
            IDTransaccionDocumento = dt.Rows(0)("IDTransaccionRetencion")
        End If

        If IsNumeric(IDTransaccionDocumento) = False Then
            Exit Sub
        End If

        'Verificar si es una cobranza normal
        Try

            Dim Existe As Integer = CSistema.ExecuteScalar("Select Count(*) From CobranzaCredito Where IDTransaccion=" & IDTransaccionDocumento)

            If Existe > 0 Then
                Dim frm As New frmAplicacionProveedoresAnticipo
                FGMostrarFormulario(frmPrincipal2, frm, "Cobranza")
                frm.CargarOperacion(IDTransaccionDocumento)
            Else
                Dim frm As New frmCobranzaContado
                FGMostrarFormulario(frmPrincipal2, frm, "Cobranza")
                frm.CargarOperacion(IDTransaccionDocumento)
            End If
            

        Catch ex As Exception

        End Try
        

    End Sub

    Private Sub frmAsociarVentaRetencion_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmAsociarRetencionCliente_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then

            CSistema.SelectNextControl(Me, Keys.Enter)

            e.Handled = True
        End If
    End Sub

    Private Sub frmAsociarVentaRetencion_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Enter Then

            'Controles para obviar
            Dim Controles() As String = {"OcxFormaPago1", "txtPorcentajeRetencion", "txtObservacion", "txtComprobanteFactura", "txtImporteRetenido"}
            If Controles.Contains(Me.ActiveControl.Name) Then
                Exit Sub
            End If

            CSistema.SelectNextControl(Me, e.KeyCode)

        End If

        If e.KeyCode = Keys.F2 Then
            If vNuevo = True Then
                SeleccionarOtrasFacturas()
            End If
        End If

    End Sub

    Private Sub frmAsociarVentaRetencion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub lklEliminarVenta_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarVenta.LinkClicked
        EliminarVenta()
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado

        'Foco
        If vNuevo = True Then
            txtObservacion.txt.Focus()
        End If

    End Sub

    Private Sub txtCliente_ItemMalSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemMalSeleccionado

        If txtCliente.SoloLectura = True Then
            Exit Sub
        End If

    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub btnSeleccionarRetencion_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionarRetencion.Click
        SeleccionarRetencion()
    End Sub

    Private Sub txtPorcentajeRetencion_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtPorcentajeRetencion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            If txtPorcentajeRetencion.ObtenerValor = 0 Then
                txtPorcentajeRetencion.SetValue(30)
                txtPorcentajeRetencion.txt.SelectAll()
                Exit Sub
            End If
            txtComprobanteFactura.txt.Focus()

        End If
    End Sub

    Private Sub btnAgregarFactura_Click(sender As System.Object, e As System.EventArgs) Handles btnAgregarFactura.Click
        CargarVenta()
    End Sub

    Private Sub txtObservacion_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtObservacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            btnSeleccionarRetencion.Focus()
        End If
    End Sub

    Private Sub txtComprobanteFactura_Leave(sender As Object, e As System.EventArgs) Handles txtComprobanteFactura.Leave
        BuscarVenta()
    End Sub

    Private Sub txtComprobanteFactura_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtComprobanteFactura.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then

            If txtComprobanteFactura.GetValue.Length = 0 Then
                Exit Sub
            End If

            txtImporteRetenido.txt.Focus()

        End If

    End Sub

    Private Sub txtComprobanteFactura_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtComprobanteFactura.KeyUp

    End Sub

    Private Sub lklVisualizarCobranza_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklVisualizarCobranza.LinkClicked
        VisualizarCobranza()
    End Sub

    Private Sub cbxSucursal_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        Try
            txtReferenciaSucursal.SetValue(vgData.Tables("VSucursal").Select("ID=" & cbxSucursal.GetValue)(0)("Referencia").ToString)
        Catch ex As Exception

        End Try

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Private Sub lklSeleccionarFactura_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklSeleccionarFactura.LinkClicked
        SeleccionarOtrasFacturas()
    End Sub

    Private Sub txtImporteTotalImpuestoVenta_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtImporteTotalImpuestoVenta.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarVenta()
        End If
    End Sub

    Private Sub txtImporteRetenido_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtImporteRetenido.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then

            'Si el importe de la retencion fue modificada, saltar a valores de la factura
            If vImporteRetencion <> txtImporteRetenido.ObtenerValor Then
                txtImporteTotalVenta.txt.Focus()
                Exit Sub
            End If
            CargarVenta()
        End If
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmAsociarRetencionCliente_Activate()
        Me.Refresh()
    End Sub
End Class
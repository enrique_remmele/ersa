﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmValidarCobranza
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.pnlCabecera = New System.Windows.Forms.Panel()
        Me.dgvCabecera = New System.Windows.Forms.DataGridView()
        Me.pnlFactura = New System.Windows.Forms.Panel()
        Me.dgvFactura = New System.Windows.Forms.DataGridView()
        Me.pnlFormaPago = New System.Windows.Forms.Panel()
        Me.dgvFormaPago = New System.Windows.Forms.DataGridView()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ExportarAPlanillaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnProcesar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.chkCobrador = New ERP.ocxCHK()
        Me.cbxCobrador = New ERP.ocxCBX()
        Me.pnlCabecera.SuspendLayout()
        CType(Me.dgvCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFactura.SuspendLayout()
        CType(Me.dgvFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFormaPago.SuspendLayout()
        CType(Me.dgvFormaPago, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlCabecera
        '
        Me.pnlCabecera.Controls.Add(Me.dgvCabecera)
        Me.pnlCabecera.Location = New System.Drawing.Point(3, 26)
        Me.pnlCabecera.Name = "pnlCabecera"
        Me.pnlCabecera.Size = New System.Drawing.Size(793, 176)
        Me.pnlCabecera.TabIndex = 0
        '
        'dgvCabecera
        '
        Me.dgvCabecera.AllowUserToAddRows = False
        Me.dgvCabecera.AllowUserToDeleteRows = False
        Me.dgvCabecera.AllowUserToOrderColumns = True
        Me.dgvCabecera.BackgroundColor = System.Drawing.Color.White
        Me.dgvCabecera.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCabecera.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvCabecera.Location = New System.Drawing.Point(0, 0)
        Me.dgvCabecera.Name = "dgvCabecera"
        Me.dgvCabecera.ReadOnly = True
        Me.dgvCabecera.RowHeadersVisible = False
        Me.dgvCabecera.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCabecera.Size = New System.Drawing.Size(793, 176)
        Me.dgvCabecera.StandardTab = True
        Me.dgvCabecera.TabIndex = 0
        '
        'pnlFactura
        '
        Me.pnlFactura.Controls.Add(Me.dgvFactura)
        Me.pnlFactura.Location = New System.Drawing.Point(3, 259)
        Me.pnlFactura.Name = "pnlFactura"
        Me.pnlFactura.Size = New System.Drawing.Size(793, 141)
        Me.pnlFactura.TabIndex = 1
        '
        'dgvFactura
        '
        Me.dgvFactura.AllowUserToAddRows = False
        Me.dgvFactura.AllowUserToDeleteRows = False
        Me.dgvFactura.AllowUserToOrderColumns = True
        Me.dgvFactura.BackgroundColor = System.Drawing.Color.White
        Me.dgvFactura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFactura.Location = New System.Drawing.Point(0, 0)
        Me.dgvFactura.Name = "dgvFactura"
        Me.dgvFactura.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvFactura.Size = New System.Drawing.Size(793, 138)
        Me.dgvFactura.StandardTab = True
        Me.dgvFactura.TabIndex = 1
        '
        'pnlFormaPago
        '
        Me.pnlFormaPago.Controls.Add(Me.dgvFormaPago)
        Me.pnlFormaPago.Location = New System.Drawing.Point(3, 429)
        Me.pnlFormaPago.Name = "pnlFormaPago"
        Me.pnlFormaPago.Size = New System.Drawing.Size(793, 151)
        Me.pnlFormaPago.TabIndex = 2
        '
        'dgvFormaPago
        '
        Me.dgvFormaPago.AllowUserToAddRows = False
        Me.dgvFormaPago.AllowUserToDeleteRows = False
        Me.dgvFormaPago.AllowUserToOrderColumns = True
        Me.dgvFormaPago.BackgroundColor = System.Drawing.Color.White
        Me.dgvFormaPago.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFormaPago.Location = New System.Drawing.Point(0, 0)
        Me.dgvFormaPago.Name = "dgvFormaPago"
        Me.dgvFormaPago.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvFormaPago.Size = New System.Drawing.Size(793, 151)
        Me.dgvFormaPago.StandardTab = True
        Me.dgvFormaPago.TabIndex = 2
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 583)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1069, 22)
        Me.StatusStrip1.TabIndex = 6
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExportarAPlanillaToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(169, 26)
        '
        'ExportarAPlanillaToolStripMenuItem
        '
        Me.ExportarAPlanillaToolStripMenuItem.Name = "ExportarAPlanillaToolStripMenuItem"
        Me.ExportarAPlanillaToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ExportarAPlanillaToolStripMenuItem.Text = "Exportar a Planilla"
        '
        'btnProcesar
        '
        Me.btnProcesar.Location = New System.Drawing.Point(12, 208)
        Me.btnProcesar.Name = "btnProcesar"
        Me.btnProcesar.Size = New System.Drawing.Size(75, 23)
        Me.btnProcesar.TabIndex = 7
        Me.btnProcesar.Text = "Procesar"
        Me.btnProcesar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(101, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Cabecera Cobranza"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 243)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Factura Asociada"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 413)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(79, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Forma de Pago"
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(802, 81)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(75, 23)
        Me.btnListar.TabIndex = 11
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'chkCobrador
        '
        Me.chkCobrador.BackColor = System.Drawing.Color.Transparent
        Me.chkCobrador.Color = System.Drawing.Color.Empty
        Me.chkCobrador.Location = New System.Drawing.Point(802, 26)
        Me.chkCobrador.Name = "chkCobrador"
        Me.chkCobrador.Size = New System.Drawing.Size(76, 20)
        Me.chkCobrador.SoloLectura = False
        Me.chkCobrador.TabIndex = 13
        Me.chkCobrador.Texto = "Cobrador"
        Me.chkCobrador.Valor = False
        '
        'cbxCobrador
        '
        Me.cbxCobrador.CampoWhere = ""
        Me.cbxCobrador.CargarUnaSolaVez = False
        Me.cbxCobrador.DataDisplayMember = ""
        Me.cbxCobrador.DataFilter = Nothing
        Me.cbxCobrador.DataOrderBy = Nothing
        Me.cbxCobrador.DataSource = ""
        Me.cbxCobrador.DataValueMember = ""
        Me.cbxCobrador.dtSeleccionado = Nothing
        Me.cbxCobrador.Enabled = False
        Me.cbxCobrador.FormABM = Nothing
        Me.cbxCobrador.Indicaciones = Nothing
        Me.cbxCobrador.Location = New System.Drawing.Point(802, 52)
        Me.cbxCobrador.Name = "cbxCobrador"
        Me.cbxCobrador.SeleccionMultiple = False
        Me.cbxCobrador.SeleccionObligatoria = False
        Me.cbxCobrador.Size = New System.Drawing.Size(255, 23)
        Me.cbxCobrador.SoloLectura = False
        Me.cbxCobrador.TabIndex = 12
        Me.cbxCobrador.Texto = ""
        '
        'frmValidarCobranza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1069, 605)
        Me.Controls.Add(Me.chkCobrador)
        Me.Controls.Add(Me.cbxCobrador)
        Me.Controls.Add(Me.btnListar)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnProcesar)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.pnlFormaPago)
        Me.Controls.Add(Me.pnlFactura)
        Me.Controls.Add(Me.pnlCabecera)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmValidarCobranza"
        Me.Text = "frmValidarCobranza"
        Me.pnlCabecera.ResumeLayout(False)
        CType(Me.dgvCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFactura.ResumeLayout(False)
        CType(Me.dgvFactura, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFormaPago.ResumeLayout(False)
        CType(Me.dgvFormaPago, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pnlCabecera As Panel
    Friend WithEvents pnlFactura As Panel
    Friend WithEvents pnlFormaPago As Panel
    Friend WithEvents dgvFactura As DataGridView
    Friend WithEvents dgvFormaPago As DataGridView
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents ExportarAPlanillaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnProcesar As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents dgvCabecera As DataGridView
    Friend WithEvents btnListar As Button
    Friend WithEvents cbxCobrador As ocxCBX
    Friend WithEvents chkCobrador As ocxCHK
End Class

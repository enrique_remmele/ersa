﻿Public Class frmCobranzaContado

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CData As New CData
    Dim CAsiento As New CAsientoCobranza

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private IDTransaccionLoteValue As Integer
    Public Property IDTransaccionLote() As Integer
        Get
            Return IDTransaccionLoteValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionLoteValue = value
        End Set
    End Property

    'EVENTOS

    'VARIABLES
    Dim dtDetalleCobranzaVenta As New DataTable
    Dim dtLote As New DataTable
    Dim dtVentasRetencion As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles

        'Otros

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "COBRANZA POR LOTE", "CLOT")
        vNuevo = True

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.IDSucursal = cbxSucursal.GetValue
        CAsiento.IDMoneda = 1
        CAsiento.InicializarAsiento()

        'Controles
        OcxFormaPago1.Inicializar()
        OcxFormaPago1.Tipo = "CONTADO"
        OcxFormaPago1.Form = Me

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.Enter))
    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        'CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtNroLote)
        CSistema.CargaControl(vControles, txtObservacion)

        'Ventas
        CSistema.CargaControl(vControles, btnAgregarVenta)
        CSistema.CargaControl(vControles, lklEliminarVenta)

        'Forma de Pago
        CSistema.CargaControl(vControles, OcxFormaPago1)

        'CARGAR ESTRUCTURA DEL DETALLE VENTA
        dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Select Top(0) * From VVentaDetalleCobranzaContado Where Anulado='False' And Cancelado='False' ").Clone

        'CARGAR ESTRUCTURA DE VENTAS SI ES QUE SE ESTA
        dtVentasRetencion = CData.GetStructure("VFormaPagoDocumentoRetencion", "Select Top(0) 'Sel'='False', IDTransaccion, 'ID'=0, IDTransaccionVenta, 'ComprobanteVenta'='', 'FechaVenta'=GetDate(), 'TotalImpuesto'=0, 'Porcentaje'=0, 'Importe'=0, 'ComprobanteRetencion'='', 'Aplicado'='False' From FormaPagoDocumentoRetencion ")
        OcxFormaPago1.dtVentaRetencion = dtVentasRetencion.Clone

        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Ultimo Registro
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From CobranzaContado Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Limpiar detalle
        dtDetalleCobranzaVenta.Rows.Clear()
        dtVentasRetencion.Rows.Clear()
        ListarVentas()

        'Forma de Pago
        OcxFormaPago1.dtCheques.Rows.Clear()
        OcxFormaPago1.dtChequesTercero.Rows.Clear()
        OcxFormaPago1.dtEfectivo.Rows.Clear()
        OcxFormaPago1.dtDocumento.Rows.Clear()
        OcxFormaPago1.dtFormaPago.Rows.Clear()
        OcxFormaPago1.ListarFormaPago()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        CAsiento.Limpiar()
        txtFechaLote.Clear()
        txtDistribuidor.Clear()

        vNuevo = True
        LimpiarControles()

        'Cabecera
        txtFecha.Hoy()
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()
        txtNroLote.txt.Clear()
        txtFechaLote.txt.Clear()
        txtDistribuidor.txt.Clear()

        'Bloquear fecha de la cobranza
        txtFecha.SoloLectura = CType(CSistema.ExecuteScalar("(Select IsNull(CobranzaCreditoBloquearFecha,'False') From Configuraciones Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ") "), Boolean)
        txtFecha.Enabled = Not CType(CSistema.ExecuteScalar("(Select IsNull(CobranzaCreditoBloquearFecha,'False') From Configuraciones Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ") "), Boolean)

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From CobranzaContado Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)
        txtComprobante.txt.Text = CSistema.ObtenerProximoNroComprobante(cbxTipoComprobante.cbx.SelectedValue, "CobranzaContado", "Comprobante", cbxSucursal.cbx.SelectedValue)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Poner el foco en el proveedor
        cbxTipoComprobante.cbx.Focus()

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        LimpiarControles()


        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If

        End If

        'Validar
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            'Ciudad
            If cbxCiudad.cbx.SelectedValue = Nothing Then
                Dim mensaje As String = "Seleccione correctamente la ciudad!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            'Tipo Comprobante
            If cbxTipoComprobante.cbx.Text.Trim = "" Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(cbxTipoComprobante, mensaje)
                ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            'Comprobante
            If txtComprobante.txt.Text.Trim.Length = 0 Then
                Dim mensaje As String = "Ingrese un numero de comprobante!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            'Sucursal
            If cbxSucursal.cbx.SelectedValue = Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If cbxSucursal.cbx.Text.Trim = "" Then
                Dim mensaje As String = "Seleccione correctamente la sucursal!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If


            'Venta
            If dgw.Columns.Count = 0 Then
                Dim mensaje As String = "El documento tiene que tener comprobantes de venta!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If txtTotalVenta.ObtenerValor <= 0 Then
                Dim mensaje As String = "El importe de los comprobantes de venta no es válido!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If txtSaldoTotal.ObtenerValor <> 0 Then
                Dim mensaje As String = "El saldo no es válido!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            'Validar Cheques diferidos con facturas contado
            If CType(CSistema.ExecuteScalar("(Select IsNull(ContadoChequeDiferido,'False') From Configuraciones Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ") ", VGCadenaConexion), Boolean) = False Then
                Dim dtVentas As New DataTable
                Dim dtChequesDiferidos As New DataTable
                dtVentas = CData.FiltrarDataTable(dtDetalleCobranzaVenta, " Credito = 'false' and Sel = 'True' ").Copy
                dtChequesDiferidos = CData.FiltrarDataTable(OcxFormaPago1.dtChequesTercero, " Tipo = 'DIFERIDO' and Sel = 'True'").Copy

                If dtVentas.Rows.Count > 0 And dtChequesDiferidos.Rows.Count > 0 Then
                    Dim mensaje As String = "¡No se permite Cheques diferidos con Facturas Contado!"
                    ctrError.SetError(btnGuardar, mensaje)
                    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Function
                End If
            End If

        End If

        Return True


    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        'Generar la cabecera del asiento
        'GenerarAsiento()

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", CInt(txtID.txt.Text), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTransaccionLote", IDTransaccionLote, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Comprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        'Totales
        CSistema.SetSQLParameter(param, "@Total", OcxFormaPago1.Total, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalImpuesto", 0, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", 0, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDescuento", 0, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpCobranzaContado", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From CobranzaContado Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
            End If

            Exit Sub

        End If

        Dim Procesar As Boolean = True


        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
            CargarOperacion(IDTransaccion)

            txtID.SoloLectura = False
        End If

        If IDTransaccion > 0 Then

            'Cargamos el Detalle de Ventas
            Procesar = InsertarDetalleVentas(IDTransaccion)

            'Cargamos la Forma de Pago
            Procesar = InsertarFormaPago(IDTransaccion)

        End If

        'Aplicar el recibo
        Try

            ReDim param(-1)

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)

            If Procesar = True Then
                CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)
            Else
                CSistema.SetSQLParameter(param, "@Operacion", "DEL", ParameterDirection.Input)
            End If

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            'Aplicar la Cobranza
            If CSistema.ExecuteStoreProcedure(param, "SpCobranzaContadoProcesar", False, False, MensajeRetorno,,,, 120) = False Then

            End If

            'Cargamos el Asiento
            'CAsiento.IDTransaccion = IDTransaccion
            'CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        Catch ex As Exception

        End Try

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False

    End Sub

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpCobranzaContado", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que es un registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From CobranzaContado Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then

            End If

            Exit Sub

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False


    End Sub

    Sub LimpiarControles()
        'Desaparecer Label
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False
    End Sub

    Function InsertarDetalleVentas(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalleVentas = True

        For Each oRow As DataRow In dtDetalleCobranzaVenta.Rows

            If oRow("Sel") = True Then

                Dim sql As String = "Insert Into VentaCobranza(IDTransaccionVenta, IDTransaccionCobranza, Importe, Cancelar) Values(" & oRow("IDTransaccion").ToString & ", " & IDTransaccion & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", 'False')"

                If CSistema.ExecuteNonQuery(sql) = 0 Then
                    Return False
                End If

            End If
        Next

    End Function

    Function InsertarFormaPago(ByVal IDTransaccion As Integer) As Boolean

        InsertarFormaPago = True

        Dim dtEfectivo As DataTable
        Dim dtCheque As DataTable
        Dim dtDocumento As DataTable
        Dim dtVentaRetencion As DataTable

        Dim ID As Integer = 1

        dtEfectivo = OcxFormaPago1.dtEfectivo.Copy
        dtCheque = OcxFormaPago1.dtCheques.Copy
        dtDocumento = OcxFormaPago1.dtDocumento.Copy
        dtVentaRetencion = OcxFormaPago1.dtVentaRetencion.Copy

        'Cargar el Efectivo
        For Each oRow As DataRow In dtEfectivo.Rows

            Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe) values(" & IDTransaccion & ", " & ID & ", 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", 'False', 0, NULL, NULL, 'False', 0, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ")"
            Dim sqlEfectivo As String = "Insert Into Efectivo(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, VentasCredito, VentasContado, Gastos, IDMoneda, ImporteMoneda, Cotizacion, Observacion, Depositado, Cancelado, Saldo,ImporteHabilitado) values(" & IDTransaccion & ", " & ID & ", " & cbxSucursal.cbx.SelectedValue & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("Comprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True) & "', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", 0, 0, " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString) & ", '" & oRow("Observacion").ToString & "', 0, 'False', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ")"

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            If CSistema.ExecuteNonQuery(sqlEfectivo) = 0 Then
                Return False
            End If

            ID = ID + 1

        Next

        'Cargar el Documento
        For Each oRow As DataRow In dtDocumento.Rows

            Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, 0, 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ")"
            'Dim sqlDocumento As String = "Insert Into FormaPagoDocumento(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, IDMoneda, ImporteMoneda, Cotizacion, Total, Saldo ,Observacion) values(" & IDTransaccion & ", " & ID & ", " & cbxSucursal.cbx.SelectedValue & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("Comprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True) & "', " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", '" & oRow("Observacion").ToString & "')"
            Dim sqlDocumento As String = "Insert Into FormaPagoDocumento(IDTransaccion, ID, IDSucursal, IDTipoComprobante, Comprobante, Fecha, IDMoneda, ImporteMoneda, Cotizacion, Total, Saldo ,Observacion,IDBanco) values(" & IDTransaccion & ", " & ID & ", " & cbxSucursal.cbx.SelectedValue & ", " & oRow("IDTipoComprobante").ToString & ", '" & oRow("Comprobante").ToString & "', '" & CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, True) & "', " & oRow("IDMoneda").ToString & ", " & CSistema.FormatoMonedaBaseDatos(oRow("ImporteMoneda").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", '" & oRow("Observacion").ToString & "'," & oRow("IDBanco").ToString & ")"

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            If CSistema.ExecuteNonQuery(sqlDocumento) = 0 Then
                Return False
            End If

            'Cargar el Documento retencion
            'Solo si el tipo de comprobante es de Retencion
            If oRow("IDTipoComprobante").ToString = vgConfiguraciones("CobranzaCreditoTipoComprobanteRetencion") Then

                For Each oRow1 As DataRow In dtVentaRetencion.Select("Sel='True' And ID=" & oRow("ID").ToString & " And ComprobanteRetencion='" & oRow("Comprobante").ToString & "' ")

                    Dim sql1 As String = "Insert Into FormaPagoDocumentoRetencion(IDTransaccion, ID, IDTransaccionVenta, Porcentaje, Importe) values(" & IDTransaccion & ", " & ID & ", " & oRow1("IDTransaccionVenta") & ", '" & oRow1("Porcentaje").ToString & "', " & CSistema.FormatoMonedaBaseDatos(oRow1("Importe").ToString) & ")"

                    If CSistema.ExecuteNonQuery(sql1) = 0 Then
                        Return False
                    End If

                Next

            End If

            ID = ID + 1

        Next

        'Cargar el Cheque
        For Each oRow As DataRow In dtCheque.Rows

            If CBool(oRow("Sel").ToString) = True Then

                Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, CancelarCheque, Documento, ImporteDocumento, IDMonedaDocumento, ImporteMonedaDocumento, Importe) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & oRow("IDTransaccion").ToString & ", NULL, '" & oRow("Cancelar").ToString & "' ,'False', NULL, NULL, NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ")"

                If CSistema.ExecuteNonQuery(sql) = 0 Then
                    Return False
                End If

                ID = ID + 1

            End If

        Next

        Dim dtChequeTercero As DataTable
        dtChequeTercero = OcxFormaPago1.dtChequesTercero.Copy

        'Cargar el Cheque
        For Each oRow As DataRow In dtChequeTercero.Rows

            If CBool(oRow("Sel").ToString) = True Then

                Dim sql As String = "Insert Into FormaPago(IDTransaccion, ID, Efectivo, ImporteEfectivo, IDMonedaEfectivo, ImporteMonedaEfectivo, Cheque, ImporteCheque, IDTransaccionCheque, IDMonedaCheque, Importe) values(" & IDTransaccion & ", " & ID & ", 'False', 0, NULL, NULL, 'True', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ", " & oRow("IDTransaccion").ToString & ", NULL, " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ")"

                If CSistema.ExecuteNonQuery(sql) = 0 Then
                    Return False
                End If

                ID = ID + 1

            End If

        Next

    End Function

    Sub ObtenerComprobantesLote()

        If txtNroLote.txt.Text = "" Then
            Exit Sub
        End If

        dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Select * From VVentasPendientesParaLote Where ComprobanteLote='" & txtNroLote.GetValue & "' and IDSucursal =" & cbxSucursal.GetValue & "").Copy

        OcxFormaPago1.Comprobante = txtNroLote.txt.Text
        ListarVentas()

    End Sub

    Sub CargarVenta(ByVal IDTransaccion As Integer)

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra las ventas asociadas!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        'Limpiar ventas seleccionadas
        dtDetalleCobranzaVenta.Rows.Clear()
        dtDetalleCobranzaVenta = CSistema.ExecuteToDataTable("Select *, 'Sel'='True' From VVentaDetalleCobranzaContado Where IDTransaccionCobranza=" & IDTransaccion).Copy
        ListarVentas()

    End Sub

    Sub ListarVentas()

        dgw.Rows.Clear()

        Dim Total As Decimal = 0
        Dim Cantidad As Integer = 0

        For Each oRow As DataRow In dtDetalleCobranzaVenta.Rows

            If oRow("Sel") = True Then

                Dim Registro(9) As String
                Registro(0) = oRow("Comprobante").ToString
                Registro(1) = oRow("Cliente").ToString
                Registro(2) = oRow("Fec").ToString
                Registro(3) = oRow("Moneda").ToString
                Registro(4) = CSistema.FormatoNumero(oRow("Cotizacion").ToString)
                Registro(5) = CSistema.FormatoMoneda(oRow("Total").ToString)
                Registro(6) = CSistema.FormatoMoneda(oRow("Cobrado").ToString)
                Registro(7) = CSistema.FormatoMoneda(oRow("Descontado").ToString)
                Registro(8) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
                Registro(9) = CSistema.FormatoMoneda(oRow("Importe").ToString)


                Total = Total + CDec(oRow("Importe").ToString)
                Cantidad = Cantidad + 1

                dgw.Rows.Add(Registro)

            End If
        Next

        txtTotalVenta.SetValue(Total)
        txtTotalComprobantes.SetValue(Cantidad)

        'Solo si es nuevo, pasamos las ventas a la forma de pago para las retenciones
        If vNuevo = True Then
            OcxFormaPago1.dtVentaRetencion.Rows.Clear()

            For Each oRow As DataRow In dtDetalleCobranzaVenta.Select("Sel='True'")
                Dim NewRow As DataRow = OcxFormaPago1.dtVentaRetencion.NewRow
                NewRow("Aplicado") = False
                NewRow("Sel") = False
                NewRow("IDTransaccion") = 0
                NewRow("ID") = 0
                NewRow("ComprobanteRetencion") = ""
                NewRow("IDTransaccionVenta") = oRow("IDTransaccion")
                NewRow("ComprobanteVenta") = oRow("Comprobante")
                NewRow("FechaVenta") = oRow("Fecha")
                NewRow("Porcentaje") = vgConfiguraciones("CompraPorcentajeRetencion")
                NewRow("TotalImpuesto") = oRow("TotalImpuesto")

                If oRow("Total") > vgConfiguraciones("CompraImporteMinimoRetencion") Then
                    NewRow("Sel") = True
                    NewRow("Importe") = NewRow("TotalImpuesto") * (NewRow("Porcentaje") / 100)
                Else
                    NewRow("Importe") = 0
                End If

                OcxFormaPago1.dtVentaRetencion.Rows.Add(NewRow)
            Next
        End If

        CalcularTotales()


    End Sub

    Sub EliminarVenta()

        For Each item As DataGridViewRow In dgw.SelectedRows

            Dim Comprobante As String = item.Cells(0).Value

            For Each oRow As DataRow In dtDetalleCobranzaVenta.Select("Comprobante='" & Comprobante & "'")
                oRow("Sel") = False
                Exit For
            Next

        Next

        ListarVentas()

    End Sub

    Sub CalcularTotales()

        Dim TotalVentas As Decimal
        Dim TotalFormaPago As Decimal
        Dim SaldoTotal As Decimal

        TotalVentas = txtTotalVenta.ObtenerValor
        TotalFormaPago = CSistema.FormatoMoneda(OcxFormaPago1.Total, True)

        'Calcular
        SaldoTotal = TotalVentas - TotalFormaPago

        txtSaldoTotal.SetValue(SaldoTotal)
        OcxFormaPago1.Saldo = SaldoTotal

    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        Dim Comprobante As String = cbxTipoComprobante.cbx.Text & ": " & txtComprobante.txt.Text

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante
            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From vCobranzaContado Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else

            'Validar
            If cbxCiudad.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la ciudad de operacion!"
                ctrError.SetError(cbxCiudad, mensaje)
                ctrError.SetIconAlignment(cbxCiudad, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante

            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            frm.CalcularTotales()

            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
            CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            CAsiento.NroCaja = frm.CAsiento.NroCaja
            CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                CAsiento.dtAsiento.Clear()
                CAsiento.dtDetalleAsiento.Clear()
                VisualizarAsiento()
            End If

        End If

    End Sub

    Sub GenerarAsiento()

        'EstablecerCabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = cbxCiudad.GetValue
        oRow("IDSucursal") = cbxSucursal.GetValue
        oRow("Fecha") = txtFecha.GetValue
        oRow("IDMoneda") = 1
        oRow("Cotizacion") = 1
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = txtComprobante.txt.Text
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text
        oRow("Detalle") = txtObservacion.txt.Text
        oRow("Total") = txtTotalVenta.ObtenerValor

        CAsiento.IDMoneda = 1
        CAsiento.IDSucursal = cbxSucursal.GetValue

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)

        CAsiento.dtVenta = CData.FiltrarDataTable(dtDetalleCobranzaVenta, " Sel = 'True' ").Copy
        CAsiento.dtCheque = CData.FiltrarDataTable(OcxFormaPago1.dtChequesTercero, " Sel = 'True' ").Copy
        CAsiento.dtEfectivo = OcxFormaPago1.dtEfectivo.Copy
        CAsiento.dtDocumento = OcxFormaPago1.dtDocumento.Copy

        CAsiento.Total = txtTotalVenta.ObtenerValor

        CAsiento.Generar()

    End Sub

    Sub Imprimir()

    End Sub

    Sub Buscar()

        Dim frm As New frmConsultaCobranzaContado
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog()
        CargarOperacion(frm.IDTransaccion)
    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From CobranzaContado Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VCobranzaContado Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        txtNroLote.txt.Text = oRow("ComprobanteLote").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtFechaLote.txt.Text = oRow("FechaLote").ToString
        txtDistribuidor.txt.Text = oRow("Distribuidor").ToString


        'Visible Label
        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then

            'Visible Label
            lblRegistradoPor.Visible = True
            lblUsuarioAnulado.Visible = True
            lblUsuarioRegistro.Visible = True
            lblFechaRegistro.Visible = True
            lblAnulado.Visible = True
            lblAnulado.Visible = True
            lblFechaAnulado.Visible = True

            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            'lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            lblUsuarioAnulado.Text = oRow("UsuarioAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        dgw.AllowUserToDeleteRows = False

        'Cargamos el detalle de Venta
        CargarVenta(IDTransaccion)

        'Cargar Forma de Pago
        OcxFormaPago1.ListarFormaPago(IDTransaccion)

        'Calcular Totales
        CalcularTotales()

        'Inicializamos el Asiento
        CAsiento.Limpiar()
        btnAnular.Enabled = Not lblAnulado.Visible
    End Sub

    Sub CargarVenta()

        Dim frm As New frmCobranzaContadoSeleccionarVentas
        frm.dt = dtDetalleCobranzaVenta
        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccion de Comprobantes del Lote", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
        dtDetalleCobranzaVenta = frm.dt

        ListarVentas()

        OcxFormaPago1.btnEfectivo.Focus()
    End Sub

    Sub CargarNuevoCheque()

    End Sub

    Sub AplicarOrdenCompra()

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From CobranzaContado"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From CobranzaContado"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles, btnModificar)

    End Sub

    Sub ObtenerDatosLote()

        If txtNroLote.txt.Text = "" Then
            Exit Sub
        End If

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VVentasPendientesParaLote Where ComprobanteLote = '" & txtNroLote.GetValue & "' and IDSucursal =" & cbxSucursal.GetValue & "")

        If dt Is Nothing Then
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)
        IDTransaccionLote = oRow("IDTransaccionLote").ToString
        txtFechaLote.SetValueFromString(CDate(oRow("FechaLote").ToString))
        txtDistribuidor.txt.Text = oRow("Distribuidor").ToString

    End Sub

    Sub ModificarCobranza()

        Dim frm As New frmModificarCobranzaContado
        frm.Text = "Modificar Cobranza Contado"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen

        frm.IDTransaccion = IDTransaccion
        frm.ShowDialog(Me)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Private Sub frmCobranzaCredito_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmCobranzaContado_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmCobranzaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub btnAgregarVenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarVenta.Click
        CargarVenta()
    End Sub

    Private Sub lklEliminarVenta_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarVenta.LinkClicked
        EliminarVenta()
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub txtComprobante_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtComprobante.TeclaPrecionada

    End Sub

    'Private Sub txtFecha_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtFecha.TeclaPrecionada
    '    OcxFormaPago1.Fecha = txtFecha.GetValue
    'End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub OcxFormaPago1_ImporteModificado(ByVal sender As Object, ByVal e As System.EventArgs) Handles OcxFormaPago1.ImporteModificado
        CalcularTotales()
    End Sub

    Private Sub OcxFormaPago1_ListarCheques(ByVal sender As Object, ByVal e As System.EventArgs) Handles OcxFormaPago1.ListarCheques

        'Cheques y Forma de Pago
        OcxFormaPago1.Reset()

    End Sub

    Private Sub OcxFormaPago1_RegistroEliminado(ByVal sender As Object, ByVal e As System.EventArgs, ByVal oRow As System.Data.DataRow, ByVal Tipo As ocxFormaPago.ENUMFormaPago) Handles OcxFormaPago1.RegistroEliminado
        CalcularTotales()
    End Sub

    Private Sub cbxLote_Leave(ByVal sender As Object, ByVal e As System.EventArgs)
        ObtenerComprobantesLote()
    End Sub

    Private Sub txtNroLote_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroLote.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ObtenerDatosLote()
            ObtenerComprobantesLote()
        End If
    End Sub

    Private Sub OcxFormaPago1_RegistroEliminado(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal oRow As System.Data.DataRow, ByVal Tipo As ERP.ocxFormaPagoLote.ENUMFormaPago) Handles OcxFormaPago1.RegistroEliminado

    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        ModificarCobranza()
    End Sub

    Private Sub txtFecha_Validated(sender As Object, e As EventArgs) Handles txtFecha.Validated
        OcxFormaPago1.Fecha = txtFecha.GetValue
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmCobranzaContado_Activate()
        Me.Refresh()
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCobranzaContado
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.OcxFormaPago1 = New ERP.ocxFormaPagoLote()
        Me.lblSaldoTotal = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.txtNroLote = New ERP.ocxTXTString()
        Me.txtFechaLote = New ERP.ocxTXTDate()
        Me.txtDistribuidor = New ERP.ocxTXTString()
        Me.lblFechaLote = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblDistribuidor = New System.Windows.Forms.Label()
        Me.lblLote = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.lblTotalVenta = New System.Windows.Forms.Label()
        Me.lklEliminarVenta = New System.Windows.Forms.LinkLabel()
        Me.gbxComprobantes = New System.Windows.Forms.GroupBox()
        Me.txtTotalComprobantes = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colVencimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCotizacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCobrado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDescontado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtTotalVenta = New ERP.ocxTXTNumeric()
        Me.btnAgregarVenta = New System.Windows.Forms.Button()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.txtSaldoTotal = New ERP.ocxTXTNumeric()
        Me.GroupBox1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxCabecera.SuspendLayout()
        Me.gbxComprobantes.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.flpRegistradoPor.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(85, 597)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 8
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.OcxFormaPago1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 368)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(749, 196)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'OcxFormaPago1
        '
        Me.OcxFormaPago1.Comprobante = Nothing
        Me.OcxFormaPago1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxFormaPago1.dtCheques = Nothing
        Me.OcxFormaPago1.dtChequesTercero = Nothing
        Me.OcxFormaPago1.dtDocumento = Nothing
        Me.OcxFormaPago1.dtEfectivo = Nothing
        Me.OcxFormaPago1.dtFormaPago = Nothing
        Me.OcxFormaPago1.dtVentaRetencion = Nothing
        Me.OcxFormaPago1.Fecha = New Date(CType(0, Long))
        Me.OcxFormaPago1.Form = Nothing
        Me.OcxFormaPago1.Location = New System.Drawing.Point(3, 16)
        Me.OcxFormaPago1.Name = "OcxFormaPago1"
        Me.OcxFormaPago1.Restar = False
        Me.OcxFormaPago1.Saldo = New Decimal(New Integer() {0, 0, 0, 0})
        Me.OcxFormaPago1.Size = New System.Drawing.Size(743, 177)
        Me.OcxFormaPago1.SoloLectura = False
        Me.OcxFormaPago1.TabIndex = 0
        Me.OcxFormaPago1.Tipo = Nothing
        Me.OcxFormaPago1.Total = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'lblSaldoTotal
        '
        Me.lblSaldoTotal.AutoSize = True
        Me.lblSaldoTotal.Location = New System.Drawing.Point(560, 574)
        Me.lblSaldoTotal.Name = "lblSaldoTotal"
        Me.lblSaldoTotal.Size = New System.Drawing.Size(37, 13)
        Me.lblSaldoTotal.TabIndex = 5
        Me.lblSaldoTotal.Text = "Saldo:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 635)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(764, 22)
        Me.StatusStrip1.TabIndex = 16
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(162, 597)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 9
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        Me.btnImprimir.Visible = False
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(332, 597)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(72, 23)
        Me.btnAsiento.TabIndex = 11
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(4, 597)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(75, 23)
        Me.btnBusquedaAvanzada.TabIndex = 7
        Me.btnBusquedaAvanzada.Text = "&Busqueda"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(665, 597)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 15
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(565, 597)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 14
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(410, 597)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 12
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(487, 597)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 13
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.txtNroLote)
        Me.gbxCabecera.Controls.Add(Me.txtFechaLote)
        Me.gbxCabecera.Controls.Add(Me.txtDistribuidor)
        Me.gbxCabecera.Controls.Add(Me.lblFechaLote)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblDistribuidor)
        Me.gbxCabecera.Controls.Add(Me.lblLote)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Location = New System.Drawing.Point(12, 12)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(749, 97)
        Me.gbxCabecera.TabIndex = 0
        Me.gbxCabecera.TabStop = False
        '
        'txtNroLote
        '
        Me.txtNroLote.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroLote.Color = System.Drawing.Color.Empty
        Me.txtNroLote.Indicaciones = Nothing
        Me.txtNroLote.Location = New System.Drawing.Point(75, 38)
        Me.txtNroLote.Multilinea = False
        Me.txtNroLote.Name = "txtNroLote"
        Me.txtNroLote.Size = New System.Drawing.Size(123, 21)
        Me.txtNroLote.SoloLectura = False
        Me.txtNroLote.TabIndex = 11
        Me.txtNroLote.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroLote.Texto = ""
        '
        'txtFechaLote
        '
        Me.txtFechaLote.AñoFecha = 0
        Me.txtFechaLote.Color = System.Drawing.Color.Empty
        Me.txtFechaLote.Fecha = New Date(2013, 7, 3, 15, 8, 50, 982)
        Me.txtFechaLote.Location = New System.Drawing.Point(277, 39)
        Me.txtFechaLote.MesFecha = 0
        Me.txtFechaLote.Name = "txtFechaLote"
        Me.txtFechaLote.PermitirNulo = False
        Me.txtFechaLote.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaLote.SoloLectura = True
        Me.txtFechaLote.TabIndex = 13
        Me.txtFechaLote.TabStop = False
        '
        'txtDistribuidor
        '
        Me.txtDistribuidor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDistribuidor.Color = System.Drawing.Color.Empty
        Me.txtDistribuidor.Indicaciones = Nothing
        Me.txtDistribuidor.Location = New System.Drawing.Point(415, 38)
        Me.txtDistribuidor.Multilinea = False
        Me.txtDistribuidor.Name = "txtDistribuidor"
        Me.txtDistribuidor.Size = New System.Drawing.Size(325, 21)
        Me.txtDistribuidor.SoloLectura = True
        Me.txtDistribuidor.TabIndex = 15
        Me.txtDistribuidor.TabStop = False
        Me.txtDistribuidor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDistribuidor.Texto = ""
        '
        'lblFechaLote
        '
        Me.lblFechaLote.AutoSize = True
        Me.lblFechaLote.Location = New System.Drawing.Point(198, 43)
        Me.lblFechaLote.Name = "lblFechaLote"
        Me.lblFechaLote.Size = New System.Drawing.Size(81, 13)
        Me.lblFechaLote.TabIndex = 12
        Me.lblFechaLote.Text = "Fecha del Lote:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(230, 12)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(123, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.Texto = ""
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(198, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(32, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Suc.:"
        '
        'lblDistribuidor
        '
        Me.lblDistribuidor.AutoSize = True
        Me.lblDistribuidor.Location = New System.Drawing.Point(353, 42)
        Me.lblDistribuidor.Name = "lblDistribuidor"
        Me.lblDistribuidor.Size = New System.Drawing.Size(62, 13)
        Me.lblDistribuidor.TabIndex = 14
        Me.lblDistribuidor.Text = "Distribuidor:"
        '
        'lblLote
        '
        Me.lblLote.AutoSize = True
        Me.lblLote.Location = New System.Drawing.Point(5, 42)
        Me.lblLote.Name = "lblLote"
        Me.lblLote.Size = New System.Drawing.Size(31, 13)
        Me.lblLote.TabIndex = 10
        Me.lblLote.Text = "Lote:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(76, 65)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(664, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 17
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 7, 3, 15, 8, 50, 982)
        Me.txtFecha.Location = New System.Drawing.Point(666, 12)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 9
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(626, 16)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 8
        Me.lblFecha.Text = "Fecha:"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(415, 12)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(82, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 6
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(497, 12)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(107, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 7
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(76, 12)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(63, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(139, 12)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(59, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(5, 69)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 16
        Me.lblObservacion.Text = "Observacion:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(375, 16)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(40, 13)
        Me.lblComprobante.TabIndex = 5
        Me.lblComprobante.Text = "Comp.:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(5, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'lblTotalVenta
        '
        Me.lblTotalVenta.AutoSize = True
        Me.lblTotalVenta.Location = New System.Drawing.Point(550, 223)
        Me.lblTotalVenta.Name = "lblTotalVenta"
        Me.lblTotalVenta.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalVenta.TabIndex = 5
        Me.lblTotalVenta.Text = "Total:"
        '
        'lklEliminarVenta
        '
        Me.lklEliminarVenta.AutoSize = True
        Me.lklEliminarVenta.Location = New System.Drawing.Point(3, 224)
        Me.lklEliminarVenta.Name = "lklEliminarVenta"
        Me.lklEliminarVenta.Size = New System.Drawing.Size(108, 13)
        Me.lklEliminarVenta.TabIndex = 2
        Me.lklEliminarVenta.TabStop = True
        Me.lklEliminarVenta.Text = "Eliminar comprobante"
        '
        'gbxComprobantes
        '
        Me.gbxComprobantes.Controls.Add(Me.txtTotalComprobantes)
        Me.gbxComprobantes.Controls.Add(Me.Label1)
        Me.gbxComprobantes.Controls.Add(Me.dgw)
        Me.gbxComprobantes.Controls.Add(Me.txtTotalVenta)
        Me.gbxComprobantes.Controls.Add(Me.lblTotalVenta)
        Me.gbxComprobantes.Controls.Add(Me.lklEliminarVenta)
        Me.gbxComprobantes.Controls.Add(Me.btnAgregarVenta)
        Me.gbxComprobantes.Location = New System.Drawing.Point(12, 115)
        Me.gbxComprobantes.Name = "gbxComprobantes"
        Me.gbxComprobantes.Size = New System.Drawing.Size(749, 247)
        Me.gbxComprobantes.TabIndex = 1
        Me.gbxComprobantes.TabStop = False
        '
        'txtTotalComprobantes
        '
        Me.txtTotalComprobantes.Color = System.Drawing.Color.Empty
        Me.txtTotalComprobantes.Decimales = True
        Me.txtTotalComprobantes.Indicaciones = Nothing
        Me.txtTotalComprobantes.Location = New System.Drawing.Point(441, 219)
        Me.txtTotalComprobantes.Name = "txtTotalComprobantes"
        Me.txtTotalComprobantes.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalComprobantes.SoloLectura = True
        Me.txtTotalComprobantes.TabIndex = 4
        Me.txtTotalComprobantes.TabStop = False
        Me.txtTotalComprobantes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalComprobantes.Texto = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(381, 223)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Total Com.:"
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        Me.dgw.AllowUserToResizeRows = False
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle10
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colComprobante, Me.colCliente, Me.colVencimiento, Me.colMoneda, Me.colCotizacion, Me.colTotal, Me.colCobrado, Me.colDescontado, Me.colSaldo, Me.colImporte})
        Me.dgw.Location = New System.Drawing.Point(6, 35)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(737, 178)
        Me.dgw.TabIndex = 1
        Me.dgw.TabStop = False
        '
        'colComprobante
        '
        Me.colComprobante.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colComprobante.HeaderText = "Comprobante"
        Me.colComprobante.Name = "colComprobante"
        Me.colComprobante.ReadOnly = True
        '
        'colCliente
        '
        Me.colCliente.HeaderText = "Cliente"
        Me.colCliente.Name = "colCliente"
        Me.colCliente.ReadOnly = True
        Me.colCliente.Width = 110
        '
        'colVencimiento
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colVencimiento.DefaultCellStyle = DataGridViewCellStyle11
        Me.colVencimiento.HeaderText = "Venc."
        Me.colVencimiento.Name = "colVencimiento"
        Me.colVencimiento.ReadOnly = True
        Me.colVencimiento.Width = 60
        '
        'colMoneda
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colMoneda.DefaultCellStyle = DataGridViewCellStyle12
        Me.colMoneda.HeaderText = "Mon"
        Me.colMoneda.Name = "colMoneda"
        Me.colMoneda.ReadOnly = True
        Me.colMoneda.Width = 40
        '
        'colCotizacion
        '
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colCotizacion.DefaultCellStyle = DataGridViewCellStyle13
        Me.colCotizacion.HeaderText = "Cot"
        Me.colCotizacion.Name = "colCotizacion"
        Me.colCotizacion.ReadOnly = True
        Me.colCotizacion.Width = 40
        '
        'colTotal
        '
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "N0"
        DataGridViewCellStyle14.NullValue = "0"
        Me.colTotal.DefaultCellStyle = DataGridViewCellStyle14
        Me.colTotal.HeaderText = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.ReadOnly = True
        Me.colTotal.Width = 75
        '
        'colCobrado
        '
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colCobrado.DefaultCellStyle = DataGridViewCellStyle15
        Me.colCobrado.HeaderText = "Cobrado"
        Me.colCobrado.Name = "colCobrado"
        Me.colCobrado.ReadOnly = True
        Me.colCobrado.Width = 70
        '
        'colDescontado
        '
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDescontado.DefaultCellStyle = DataGridViewCellStyle16
        Me.colDescontado.HeaderText = "Descontado"
        Me.colDescontado.Name = "colDescontado"
        Me.colDescontado.ReadOnly = True
        Me.colDescontado.Width = 70
        '
        'colSaldo
        '
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colSaldo.DefaultCellStyle = DataGridViewCellStyle17
        Me.colSaldo.HeaderText = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.ReadOnly = True
        Me.colSaldo.Width = 70
        '
        'colImporte
        '
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle18.Format = "N0"
        DataGridViewCellStyle18.NullValue = "0"
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle18
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        Me.colImporte.Width = 75
        '
        'txtTotalVenta
        '
        Me.txtTotalVenta.Color = System.Drawing.Color.Empty
        Me.txtTotalVenta.Decimales = True
        Me.txtTotalVenta.Indicaciones = Nothing
        Me.txtTotalVenta.Location = New System.Drawing.Point(590, 219)
        Me.txtTotalVenta.Name = "txtTotalVenta"
        Me.txtTotalVenta.Size = New System.Drawing.Size(147, 22)
        Me.txtTotalVenta.SoloLectura = True
        Me.txtTotalVenta.TabIndex = 6
        Me.txtTotalVenta.TabStop = False
        Me.txtTotalVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalVenta.Texto = "0"
        '
        'btnAgregarVenta
        '
        Me.btnAgregarVenta.Location = New System.Drawing.Point(6, 11)
        Me.btnAgregarVenta.Name = "btnAgregarVenta"
        Me.btnAgregarVenta.Size = New System.Drawing.Size(62, 21)
        Me.btnAgregarVenta.TabIndex = 0
        Me.btnAgregarVenta.Text = "Agregar"
        Me.btnAgregarVenta.UseVisualStyleBackColor = True
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(6, 570)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(256, 20)
        Me.flpRegistradoPor.TabIndex = 3
        Me.flpRegistradoPor.Visible = False
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(290, 570)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(247, 20)
        Me.flpAnuladoPor.TabIndex = 4
        Me.flpAnuladoPor.Visible = False
        '
        'lblAnulado
        '
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Red
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(89, 20)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(98, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(150, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(259, 597)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(70, 23)
        Me.btnModificar.TabIndex = 10
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'txtSaldoTotal
        '
        Me.txtSaldoTotal.Color = System.Drawing.Color.Empty
        Me.txtSaldoTotal.Decimales = True
        Me.txtSaldoTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldoTotal.Indicaciones = Nothing
        Me.txtSaldoTotal.Location = New System.Drawing.Point(603, 569)
        Me.txtSaldoTotal.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtSaldoTotal.Name = "txtSaldoTotal"
        Me.txtSaldoTotal.Size = New System.Drawing.Size(147, 22)
        Me.txtSaldoTotal.SoloLectura = True
        Me.txtSaldoTotal.TabIndex = 6
        Me.txtSaldoTotal.TabStop = False
        Me.txtSaldoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoTotal.Texto = "0"
        '
        'frmCobranzaContado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(764, 657)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.flpAnuladoPor)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblSaldoTotal)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Controls.Add(Me.txtSaldoTotal)
        Me.Controls.Add(Me.gbxComprobantes)
        Me.Name = "frmCobranzaContado"
        Me.Text = "frmCobranzaContado"
        Me.GroupBox1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.gbxComprobantes.ResumeLayout(False)
        Me.gbxComprobantes.PerformLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblSaldoTotal As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblLote As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtSaldoTotal As ERP.ocxTXTNumeric
    Friend WithEvents gbxComprobantes As System.Windows.Forms.GroupBox
    Friend WithEvents txtTotalVenta As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalVenta As System.Windows.Forms.Label
    Friend WithEvents lklEliminarVenta As System.Windows.Forms.LinkLabel
    Friend WithEvents lblDistribuidor As System.Windows.Forms.Label
    Friend WithEvents OcxFormaPago1 As ERP.ocxFormaPagoLote
    Friend WithEvents lblFechaLote As System.Windows.Forms.Label
    Friend WithEvents txtDistribuidor As ERP.ocxTXTString
    Friend WithEvents btnAgregarVenta As System.Windows.Forms.Button
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents txtFechaLote As ERP.ocxTXTDate
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents txtTotalComprobantes As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtNroLote As ERP.ocxTXTString
    Friend WithEvents colComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colVencimiento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colMoneda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCotizacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCobrado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDescontado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSaldo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

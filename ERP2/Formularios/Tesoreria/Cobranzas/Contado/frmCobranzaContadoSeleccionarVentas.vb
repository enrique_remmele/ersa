﻿Public Class frmCobranzaContadoSeleccionarVentas

    'CLASES
    Dim CSistema As New CSistema

    'EVENTOS
    Public Event VentasSeleccionadas(ByVal sender As Object, ByVal e As EventArgs)

    'PROPIEDADES

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Funciones
        ListarVentas()
        CargarVentas()

        'Foco
        dgw.Focus()

    End Sub

    Sub ListarVentas()

        'Limpiar la grilla
        dgw.Rows.Clear()
        Dim TotalDeuda As Decimal = 0

        For Each oRow As DataRow In dt.Rows
            Dim Registro(10) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Sel").ToString
            Registro(2) = oRow("Comprobante").ToString
            Registro(3) = oRow("Condicion").ToString
            Registro(4) = oRow("Cliente").ToString
            Registro(5) = CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(6) = CSistema.FormatoMoneda(oRow("Cobrado").ToString)
            Registro(7) = CSistema.FormatoMoneda(oRow("Descontado").ToString)
            Registro(8) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            Registro(9) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(10) = oRow("Cancelar").ToString

            'Sumar el total de la deuda
            TotalDeuda = TotalDeuda + CDec(oRow("Saldo").ToString)

            dgw.Rows.Add(Registro)

        Next


    End Sub

    Sub SeleccionarVentas()

        'Validar nuevamente que los importes sean mayor a 0
        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Cells("colImporte").Value <= 0 Then
                MessageBox.Show("El importe no puede ser 0", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

        Next

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells(0).Value

            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Sel") = oRow.Cells(1).Value
                oRow1("Importe") = oRow.Cells(9).Value
                oRow1("Cancelar") = oRow.Cells(10).Value
            Next

        Next

        Me.Close()

    End Sub

    Sub CargarVentas()

        For Each oRow As DataRow In dt.Rows
            For Each oRow1 As DataGridViewRow In dgw.Rows
                If oRow("IDTransaccion") = oRow1.Cells(0).Value Then
                    oRow1.Cells(1).Value = CBool(oRow("Sel").ToString)
                    oRow1.Cells(8).Value = CSistema.FormatoMoneda(oRow("Saldo").ToString)
                    oRow1.Cells(9).Value = CSistema.FormatoMoneda(oRow("Importe").ToString)
                    oRow1.Cells(10).Value = CBool(oRow("Cancelar").ToString)
                End If
            Next
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim Total As Decimal = 0
        Dim TotalComprobantes As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows

            If oRow.Cells(1).Value = True Then
                Total = Total + oRow.Cells("colImporte").Value
                TotalComprobantes = TotalComprobantes + 1
            End If

        Next

        txtTotalCobrado.SetValue(Total)
        txtCantidadCobrado.SetValue(TotalComprobantes)

    End Sub

    Sub PintarCelda()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(1).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next



        If dgw.CurrentRow Is Nothing Then
            Exit Sub
        End If

        dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub
    Private Sub dgwTextBox_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim columna As Integer = dgw.CurrentCell.ColumnIndex

        If columna = 10 Then

            If (Char.IsNumber(e.KeyChar) Or e.KeyChar = Microsoft.VisualBasic.ChrW(46) Or e.KeyChar = Microsoft.VisualBasic.ChrW(127) Or e.KeyChar = Microsoft.VisualBasic.ChrW(8)) Then
                e.Handled = False

                If Asc(e.KeyChar) = Keys.Enter Then
                    If CSistema.FormatoNumero(dgw.Columns.Item("colImporte").ToString) > CSistema.FormatoNumero(dgw.Columns.Item("colSaldo").ToString) Then
                        Dim mensaje As String = "El Importe debe ser menor al saldo!"
                        ctrError.SetError(dgw, mensaje)
                        ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                        tsslEstado.Text = mensaje
                    End If
                End If


            Else
                e.Handled = True
            End If

        End If

    End Sub

    Private Sub dgw_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgw.EditingControlShowing

        If (dgw.CurrentCell.ColumnIndex = 10) Then

            Dim oTexbox As TextBox = CType(e.Control, TextBox)
            AddHandler oTexbox.KeyPress, AddressOf dgwTextBox_Keypress

        End If

    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick

        ctrError.Clear()
        tsslEstado.Text = ""

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        If e.ColumnIndex = dgw.Columns.Item("colSel").Index Then
            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(9).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(9)
                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(9).ReadOnly = True
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

        End If

    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Columns(e.ColumnIndex).Name = "colImporte" Then


            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value

                Exit Sub

            End If

            'Que el valor sea mayor a 0
            If CDec(dgw.CurrentCell.Value) <= 0 Then

                Dim mensaje As String = "El importe debe ser mayor a 0!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value

                Exit Sub

            End If

            'Validar que no supere el saldo
            Dim Saldo As Decimal = dgw.CurrentRow.Cells("colSaldo").Value.ToString.Replace(".", "")
            Dim Importe As Decimal = dgw.CurrentRow.Cells("colImporte").Value.ToString.Replace(".", "")

            If CDec(dgw.CurrentRow.Cells("colSaldo").Value) < CDec(dgw.CurrentRow.Cells("colImporte").Value) Then
                Dim mensaje As String = "El importe no puede ser mayor al saldo!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value
            End If

            dgw.CurrentCell.Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells("colImporte").Value, True)

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(9).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(9)
                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(8).ReadOnly = True
                        oRow.Cells(9).ReadOnly = True
                        oRow.Cells(9).Value = oRow.Cells(8).Value
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colSaldo")
            End If


            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If
    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Index = e.RowIndex Then
                If oRow.Cells(1).Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                End If

                oRow.DefaultCellStyle.Font = f1

                oRow.Cells(9).Selected = True

            Else

                oRow.DefaultCellStyle.Font = f2

                If oRow.Cells(1).Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.White
                Else
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                End If
            End If
        Next
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Tab Then
            Button1.Focus()
        End If

    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeleccionarTodoToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = True
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuitarTodaSeleccionToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = False
        Next

        PintarCelda()
        CalcularTotales()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        SeleccionarVentas()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        For Each fila As DataGridViewRow In dgw.Rows
            fila.Cells("colSel").Value = True
        Next
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmCobranzaContadoSeleccionarVentas_Activate()
        Me.Refresh()
    End Sub

End Class
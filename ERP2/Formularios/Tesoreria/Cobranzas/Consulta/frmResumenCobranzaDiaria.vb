﻿Public Class frmResumenCobranzaDiaria

    'CLASES
    Dim CSistemas As New CSistema
    Dim CReporte As New Reporte.CReporteCobranza

    'VARIABLES
    Dim vdt As DataTable
    Dim vdtCobranzas As DataTable
    Dim vdtCheques As DataTable
    Dim vdtChequesContado As DataTable
    Dim vdtDocumentos As DataTable
    Dim vdtOrdenPago As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'TextBox
        txtFecha.SetValue(Date.Now)

        'Funciones
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Sucursal
        CSistemas.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

    End Sub

    Sub Listar(Optional ByVal Nuevo As Boolean = True, Optional ByVal NroPlanilla As String = "")

        If Nuevo Then
            If NroPlanilla = "" Then
                vdt = CSistemas.ExecuteToDataTable("Select Lote,SaldoEfectivo,Efectivo,Cantidad,Efectivizacion,OrdenPago,ChequesAlDia,CantidadChequesAlDia,ChequesDiferidos,CantidadChequesDiferidos,OtrosDocumentos,CantidadOtrosDocumentos From VResumenCobranzaContado Where Fecha='" & txtFecha.GetValueString & "' And IDSucursal=" & cbxSucursal.GetValue & " Order By Lote")
            Else
                vdt = CSistemas.ExecuteToDataTable("Select Lote,SaldoEfectivo,Efectivo,Cantidad,Efectivizacion,OrdenPago,ChequesAlDia,CantidadChequesAlDia,ChequesDiferidos,CantidadChequesDiferidos,OtrosDocumentos,CantidadOtrosDocumentos From VResumenCobranzaContado Where Fecha='" & txtFecha.GetValueString & "' And IDSucursal=" & cbxSucursal.GetValue & " And Lote='" & NroPlanilla.Replace(".", Nothing) & "' Order By Lote")
            End If

        End If

        'Listar en la Grilla
        CSistemas.dtToGrid(dgvLista, vdt)

        dgvLista.Columns("SaldoEfectivo").HeaderText = "Efe. Saldo"
        dgvLista.Columns("Cantidad").HeaderText = "Cant."
        dgvLista.Columns("OrdenPago").HeaderText = "OP"
        dgvLista.Columns("ChequesAlDia").HeaderText = "CHQ"
        dgvLista.Columns("CantidadChequesAlDia").HeaderText = "Cant."
        dgvLista.Columns("ChequesDiferidos").HeaderText = "DIF"
        dgvLista.Columns("CantidadChequesDiferidos").HeaderText = "Cant."
        dgvLista.Columns("OtrosDocumentos").HeaderText = "Doc."
        dgvLista.Columns("CantidadOtrosDocumentos").HeaderText = "Cant."



        'Totalizador
        If vdt Is Nothing Then
            Exit Sub
        End If

        If Nuevo = False Then

            'Quitamos el total
            For Each oRow As DataRow In vdt.Select("Lote='" & NroPlanilla & "'")
                vdt.Rows.Remove(oRow)
                Exit For
            Next

        End If

        Dim NewRow As DataRow = vdt.NewRow
        NewRow("Lote") = ""
        For c As Integer = 1 To vdt.Columns.Count - 1
            NewRow(c) = CSistemas.dtSumColumn(vdt, vdt.Columns(c).ColumnName)
        Next

        vdt.Rows.Add(NewRow)

        'Formato
        dgvLista.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        For c As Integer = 1 To dgvLista.ColumnCount - 1
            dgvLista.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            dgvLista.Columns(c).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLista.Columns(c).DefaultCellStyle.Format = "N0"
        Next

        dgvLista.Rows(dgvLista.Rows.Count - 1).DefaultCellStyle.Font = New Font(Me.Font.FontFamily.Name, 8, FontStyle.Bold)

    End Sub

    Sub EliminarElemento()

        'Eliminar de la tabla
        Dim Lote As String = dgvLista.SelectedRows(0).Cells("Lote").Value
        For Each oRow As DataRow In vdt.Select("Lote='" & Lote & "'")
            vdt.Rows.Remove(oRow)
            Exit For
        Next

        Listar(False)

    End Sub

    Sub EmitirReporte()

    End Sub

    Sub ListarDetalle()

        If dgvLista.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim NroPlanilla As String = dgvLista.SelectedRows(0).Cells("Lote").Value.ToString.Replace(".", Nothing)
        vdtCobranzas = CSistemas.ExecuteToDataTable("Select IDtransaccionCobranza, 'Comp. Efe.'=E.Comprobante, 'Venta'=C.Comprobante, 'Planilla'=C.NroPlanilla, C.Importe, C.Numero, C.FechaCobranza, C.Cobrador, C.Observacion From VVentaDetalleCobranza C Join Efectivo E On C.IDTransaccionCobranza=E.IDTransaccion  Where C.FechaCobranza='" & txtFecha.GetValueString & "' And E.Comprobante='" & NroPlanilla & "' ")

        CSistemas.dtToGrid(dgvCobranzas, vdtCobranzas)

        'Formato
        dgvCobranzas.Columns("IDtransaccionCobranza").Visible = False
        dgvCobranzas.Columns("Observacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvCobranzas.Columns("Importe").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvCobranzas.Columns("Importe").DefaultCellStyle.Format = "N0"

        txtTotal.SetValue(CSistemas.dtSumColumn(vdtCobranzas, "Importe"))

        If dgvCobranzas.Rows.Count > 0 Then
            TabControl1.TabPages(0).Text = "VENTAS EN EFECTIVOS (" & dgvCobranzas.Rows.Count & ")"
        Else
            TabControl1.TabPages(0).Text = "VENTAS EN EFECTIVOS"
        End If

    End Sub

    Sub ListarCheques()

        If dgvLista.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim NroPlanilla As String = dgvLista.SelectedRows(0).Cells("Lote").Value.ToString.Replace(".", Nothing)
        vdtCheques = CSistemas.ExecuteToDataTable("Select * From VResumenCobranzaDiariaCheque Where Fecha='" & txtFecha.GetValueString & "' And NroPlanilla='" & NroPlanilla & "'")
        CSistemas.dtToGrid(dgvCheques, vdtCheques)

        'Formato
        dgvCheques.Columns("IDTransaccion").Visible = False
        dgvCheques.Columns("Cliente").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvCheques.Columns("Importe").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvCheques.Columns("Utilizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvCheques.Columns("Importe").DefaultCellStyle.Format = "N0"
        dgvCheques.Columns("Utilizado").DefaultCellStyle.Format = "N0"

        txtTotalCheques.SetValue(CSistemas.dtSumColumn(vdtCheques, "Utilizado"))

        If dgvCheques.Rows.Count > 0 Then
            TabControl1.TabPages(1).Text = "CHEQUES (" & dgvCheques.Rows.Count & ")"
        Else
            TabControl1.TabPages(1).Text = "CHEQUES"
        End If

    End Sub

    Sub ListarOrdenPago()

        If dgvLista.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim NroPlanilla As String = dgvLista.SelectedRows(0).Cells("Lote").Value.ToString.Replace(".", Nothing)
        vdtOrdenPago = CSistemas.ExecuteToDataTable("Select OP.IDTransaccion, EFE.Comprobante, 'OP'=OP.Comprobante, 'Operacion'=Op.Numero, OP.Fecha, OP.usuario, OP.Observacion, OP.Referencia, OP.Proveedor, OP.Estado, OPE.Importe From VOrdenPagoEfectivo OPE Join VOrdenPago OP On OPE.IDTransaccionOrdenPago=OP.IDTransaccion Join Efectivo EFE On OPE.IDTransaccion=EFE.IDTransaccion Join VCobranzaCredito C On EFE.IDTransaccion=C.IDTransaccion Where OP.Anulado='False' And C.FechaEmision='" & txtFecha.GetValueString & "' And C.NroPlanilla='" & NroPlanilla & "'")
        'vdtOrdenPago = CSistemas.ExecuteToDataTable("Select OP.IDTransaccion, EFE.Comprobante, 'OP'=OP.Comprobante, 'Operacion'=Op.Numero, OP.Fecha, OP.usuario, OP.Observacion, OP.Referencia, OP.Proveedor, OP.Estado, OPE.Importe From VOrdenPagoEfectivo OPE Join VOrdenPago OP On OPE.IDTransaccionOrdenPago=OP.IDTransaccion Join Efectivo EFE On OPE.IDTransaccion=EFE.IDTransaccion Join VCobranzaContado C On EFE.IDTransaccion=C.IDTransaccion Where OP.Anulado='False' And C.Fecha='" & txtFecha.GetValueString & "'")
        CSistemas.dtToGrid(dgvOrdenPago, vdtOrdenPago)

        'Formato
        dgvOrdenPago.Columns("IDTransaccion").Visible = False
        dgvOrdenPago.Columns("Observacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvOrdenPago.Columns("Importe").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvOrdenPago.Columns("Importe").DefaultCellStyle.Format = "N0"

        txtTotalOrdenPago.SetValue(CSistemas.dtSumColumn(vdtOrdenPago, "Importe"))

        If dgvOrdenPago.Rows.Count > 0 Then
            TabControl1.TabPages(3).Text = "ORDEN DE PAGO (" & dgvCheques.Rows.Count & ")"
        Else
            TabControl1.TabPages(3).Text = "ORDEN DE PAGO"
        End If

    End Sub

    Sub ListarDocumentos()

        If dgvLista.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim NroPlanilla As String = dgvLista.SelectedRows(0).Cells("Lote").Value.ToString.Replace(".", Nothing)
        vdtDocumentos = CSistemas.ExecuteToDataTable("Select * From VResumenCobranzaDiariaDocumentos where Fecha='" & txtFecha.GetValueString & "' And NroPlanilla='" & NroPlanilla & "'")
        
        CSistemas.dtToGrid(dgvDocumentos, vdtDocumentos)

        'Formato
        dgvDocumentos.Columns("IDTransaccion").Visible = False
        dgvDocumentos.Columns("Importe").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDocumentos.Columns("Importe").DefaultCellStyle.Format = "N0"
        dgvDocumentos.Columns("Observacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        txtTotalDocumentos.SetValue(CSistemas.dtSumColumn(vdtDocumentos, "Importe"))

        If dgvDocumentos.Rows.Count > 0 Then
            TabControl1.TabPages(2).Text = "DOCUMENTOS (" & dgvDocumentos.Rows.Count & ")"
        Else
            TabControl1.TabPages(2).Text = "DOCUMENTOS"
        End If

    End Sub

    Sub VisualizarDocumento()

        Dim IDTransaccionDocumento As Integer

        If TabControl1.TabPages(0).Visible = True Then

            If dgvCobranzas.SelectedRows.Count = 0 Then
                Exit Sub
            End If

            IDTransaccionDocumento = dgvCobranzas.SelectedRows(0).Cells("IDtransaccionCobranza").Value

        End If

        If TabControl1.TabPages(1).Visible = True Then

            If dgvCheques.SelectedRows.Count = 0 Then
                Exit Sub
            End If

            IDTransaccionDocumento = dgvCheques.SelectedRows(0).Cells("IDtransaccion").Value

        End If

        If TabControl1.TabPages(2).Visible = True Then

            If dgvDocumentos.SelectedRows.Count = 0 Then
                Exit Sub
            End If

            IDTransaccionDocumento = dgvDocumentos.SelectedRows(0).Cells("IDtransaccion").Value

        End If

        Dim frm As New frmAplicacionProveedoresAnticipo
        FGMostrarFormulario(frmPrincipal2, frm, "Cobranza")
        frm.CargarOperacion(IDTransaccionDocumento)


    End Sub

    Private Sub frmResumenCobranzaDiaria_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown, Me.KeyUp
        CSistemas.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmResumenCobranzaDiaria_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnEmitirReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        EmitirReporte()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub ExportarAPlanillaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportarAPlanillaToolStripMenuItem.Click
        CSistemas.dtToExcel(vdt)
    End Sub

    Private Sub dgvLista_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvLista.KeyDown
        If e.KeyCode = Keys.Delete Then
            EliminarElemento()
        End If
    End Sub

    Private Sub dgvLista_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLista.SelectionChanged
        ListarDetalle()
        ListarCheques()
        ListarDocumentos()
        ListarOrdenPago()
    End Sub

    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem1.Click
        CSistemas.dtToExcel(vdtCobranzas)
    End Sub

    Private Sub VerDocumentoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VerDocumentoToolStripMenuItem.Click
        VisualizarDocumento()
    End Sub

    Private Sub txtPlanilla_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPlanilla.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            Listar(True, txtPlanilla.ObtenerValor)
        End If
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked

        Dim Titulo As String = "RESUMEN DE COBRANZA CONTADO"
        Dim TituloInforme As String = "FECHA: " & txtFecha.GetValueString & " - SUCURSAL: " & cbxSucursal.Texto
        Dim Where As String = ""
        Dim dt As New DataTable
        dt = vdt.Copy

        Dim frm As New frmReporte
        CReporte.ResumenCobranzaContado(frm, Titulo, TituloInforme, dt)

    End Sub

    Private Sub FlowLayoutPanel3_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles FlowLayoutPanel3.Paint

    End Sub

    Private Sub dgvCobranzas_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCobranzas.CellContentClick

    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmResumenCobranzaDiaria_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmDebitoCreditoBancarioPrestamo

    'CLASES
    Dim CSistema As New CSistema

    'ENUMERACION
    Enum ENUMTipoOperacion
        Debito = 0
        Credito = 1
    End Enum

    'PORPIEDADES
    Public Property IDSucursal As Integer
    Public Property IDCuentaBancaria As Integer
    Public Property TipoOperacion As ENUMTipoOperacion
    Public Property Observacion As String
    Public Property Total As Decimal
    Public Property Fecha As Date
    Public Property Cotizacion As Decimal

    'FUNCIONES
    Overloads Sub Inicializar()

        'Form 
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles

        'Otros
        OcxImpuesto1.Inicializar()
        CDetalleImpuesto.Inicializar()

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(frmDebitoCreditoBancario.Name, "DEBITO CREDITO BANCARIO", "DCB")
        DebitoCreditoBancarioPrestamo = True

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()
        CAsiento.NoAgrupar = True

        'Establecer valores
        Nuevo()
        cbxSucursal.SelectedValue(IDSucursal)
        cbxCuenta.SelectedValue(IDCuentaBancaria)
        txtfecha.SetValue(Fecha)
        txtCambio.SetValue(Cotizacion)

        Select Case TipoOperacion
            Case ENUMTipoOperacion.Debito
                cbxDebitoCredito.cbx.SelectedIndex = 0
            Case ENUMTipoOperacion.Credito
                cbxDebitoCredito.cbx.SelectedIndex = 1
        End Select

        txtObservacion.SetValue(Observacion)

        CDetalleImpuesto.EstablecerImporte(Total, ERP.CDetalleImpuesto.ENUMTipoImpuesto.EXENTO, False)
        OcxImpuesto1.CargarValores(CDetalleImpuesto.dt)

        'Ocultar/Bloquear controles
        txtID.Enabled = False
        cbxDebitoCredito.Enabled = False
        cbxSucursal.Enabled = False
        cbxCuenta.Enabled = False
        btnAnular.Visible = False
        btnBusquedaAvanzada.Visible = False
        btnCancelar.Visible = False
        btnNuevo.Visible = False
        txtBanco.SoloLectura = True
        txtMoneda.SoloLectura = True

    End Sub

    Private Sub frmDebitoCreditoBancarioPrestamo_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Total = OcxImpuesto1.txtTotal.ObtenerValor
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmDebitoCreditoBancarioPrestamo_Activate()
        Me.Refresh()
    End Sub

End Class

﻿Imports ERP.Reporte
Public Class frmDebitoCreditoBancarioSobranteFaltante
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Public CAsiento As New CAsientoDebitoCreditoBancario
    Public CDetalleImpuesto As New CDetalleImpuesto
    Public CData As New CData
    Dim CReporte As New CReporteBanco
    Dim vFecha As String

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Public Property DebitoCreditoBancarioPrestamo As Boolean = False
    Public Property Procesado As Boolean = False

    'VARIABLES
    Dim dtCuentaBancaria As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim dtDetalle As New DataTable
    Public vAsiento As Boolean = False

    'FUNCIONES
    Sub Inicializar()

        'Form 
        Me.AcceptButton = New Button
        Me.KeyPreview = True
        vAsiento = False
        'Controles

        'Otros
        OcxImpuesto1.Inicializar()
        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "DEBITO CREDITO BANCARIO", "DCB")
        vNuevo = True

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()
        CAsiento.NoAgrupar = True

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        If CBool(vgConfiguraciones("TesoreriaBloquerFecha").ToString) = True Then
            txtfecha.Enabled = False
        Else
            txtfecha.Enabled = True
        End If
    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtfecha)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, cbxCuenta)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtCambio)
        CSistema.CargaControl(vControles, txtBanco)
        CSistema.CargaControl(vControles, txtMoneda)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, cbxDebitoCredito)
        'SC 04082021 - Nuevo campo para distinguir si la operacion corresponde a una cobranza
        CSistema.CargaControl(vControles, chkEsCobranza)

        'FA 15/11/2022 - Adecuacion al plan de cuenta nuevo 
        CSistema.CargaControl(vControles, cbxUnidadNegocio)
        'CSistema.CargaControl(vControles, cbxCentroCosto)
        CSistema.CargaControl(vControles, cbxDepartamento)

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CARGAR CONTROLES
        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal ")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Unidad de Negocio
        CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, "Select ID, Descripcion From UnidadNegocio Where Estado = 1")

        'Cuenta Bancaria
        dtCuentaBancaria = CSistema.ExecuteToDataTable("Select * From VCuentaBancaria").Copy
        CSistema.SqlToComboBox(cbxCuenta.cbx, dtCuentaBancaria, "ID", "CuentaBancaria")

        'Debito Credito y Credito
        cbxDebitoCredito.cbx.Items.Add("DEBITO")
        cbxDebitoCredito.cbx.Items.Add("CREDITO")
        cbxDebitoCredito.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxDebitoCredito.cbx.SelectedIndex = 0

        'Tipo Debito Credito
        cbxTipoDebitoCredito.cbx.Items.Add("SOBRANTE")
        cbxTipoDebitoCredito.cbx.Items.Add("FALTANTE")
        cbxTipoDebitoCredito.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipoDebitoCredito.cbx.SelectedIndex = 0

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

    End Sub

    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        'CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, New Button, New Button, btnBusquedaAvanzada, btnAsiento, vControles, Nothing, Nothing, False)
        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles, Nothing, Nothing, False)

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Num), 1) From VDebitoCreditoBancario Where IDSucursal= " & cbxSucursal.cbx.SelectedValue & " "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Num), 1) From VDebitoCreditoBancario Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub Nuevo()

        vAsiento = False
        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)


        flpRegistradoPor.Visible = False

        'Limpiar detalle
        dtDetalle.Rows.Clear()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)

        'Otros
        IDTransaccion = 0
        'CAsiento.Limpiar()
        OcxImpuesto1.Reestablecer()
        CDetalleImpuesto.dt.Rows.Clear()

        'SC 04082021 - Nuevo campo para distinguir si la operacion corresponde a una cobranza
        chkEsCobranza.Checked = False

        vNuevo = True

        'Cuenta Bancaria
        dtCuentaBancaria = CSistema.ExecuteToDataTable("Select * From VCuentaBancaria where Estado = 1").Copy
        CSistema.SqlToComboBox(cbxCuenta.cbx, dtCuentaBancaria, "ID", "CuentaBancaria")

        'Cabecera
        txtfecha.Hoy()
        cbxTipoComprobante.cbx.Text = ""
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()
        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""

        'FA 15/11/2022 - Adecuacion de plan de cuenta nuevo 
        cbxUnidadNegocio.cbx.Text = ""
        'cbxCentroCosto.cbx.Text = ""
        cbxDepartamento.cbx.Text = ""
        chkGastosVarios.Enabled = True
        chkGastosVarios.Checked = False

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From VDebitoCreditoBancario Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1)"), Integer)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        txtComprobante.txt.Focus()

        Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " IDSucursal=" & cbxSucursal.GetValue)
        CSistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Departamento")
        cbxDepartamento.cbx.Text = ""

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Comprobante
        If IsNumeric(txtComprobante.txt.Text) = False Then
            CSistema.MostrarError("Debe Ingresar Solo Numeros", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.BottomRight)
            Exit Function
        End If
        'Moneda extranjera
        If OcxImpuesto1.IDMoneda <> 1 And (txtCambio.txt.Text = "1" Or txtCambio.txt.Text = "0" Or txtCambio.txt.Text = "") Then
            CSistema.MostrarError("Debe Ingresar cotizacion de la moneda", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.BottomRight)
            Exit Function
        End If
        'Moneda nacional
        If OcxImpuesto1.IDMoneda = 1 And (txtCambio.txt.Text <> "1") Then
            CSistema.MostrarError("La cotizacion debe ser 1", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.BottomRight)
            Exit Function
        End If

        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            'Validar el Asiento
            If CAsiento.ObtenerSaldo <> 0 Then
                CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Function
            End If

            If CAsiento.ObtenerTotal = 0 Then
                CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Function
            End If
        End If
        If chkGastosVarios.Checked = False Then
            If cbxUnidadNegocio.cbx.Text = "" Then
                CSistema.MostrarError("Debe de seleccionar una Unidad de Negocio", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Function
            End If
        End If

        'If cbxCentroCosto.cbx.Text = "" Then
        '    CSistema.MostrarError("Debe de seleccionar un Centro de Costo", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Function
        'End If

        If cbxDepartamento.cbx.Text = "" Then
            CSistema.MostrarError("Debe de seleccionar un Departamento", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        If vAsiento = False Then
            Dim mensaje As String = "ATENCION: Se debe generar asiento de forma manual!"
            CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
            Exit Sub
        End If

        tsslEstado.Text = ""
        ctrError.Clear()

        GenerarAsiento()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IDTransaccion As Integer
        Dim IndiceOperacion As Integer

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtfecha.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCuentaBancaria", cbxCuenta.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(txtCambio.txt.Text, OcxImpuesto1.Decimales), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        If cbxDebitoCredito.cbx.Text = "CREDITO" Then
            CSistema.SetSQLParameter(param, "@Debito", "FALSE", ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Credito", "TRUE", ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Debito", "TRUE", ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Credito", "FALSE", ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        IndiceOperacion = param.GetLength(0) - 1


        'Totales
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(CSistema.gridSumColumn(OcxImpuesto1.dg, "colTotal"), OcxImpuesto1.Decimales), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(CSistema.gridSumColumn(OcxImpuesto1.dg, "colTotalImpuesto"), OcxImpuesto1.Decimales), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(CSistema.gridSumColumn(OcxImpuesto1.dg, "colTotalDiscriminado"), OcxImpuesto1.Decimales), ParameterDirection.Input)

        'SC 04082021 - Nuevo campo para distinguir si la operacion corresponde a una cobranza
        CSistema.SetSQLParameter(param, "@EsCobranza", chkEsCobranza.Checked.ToString, ParameterDirection.Input)

        'FA 15/11/2022 - Nuevos campos para adecuacion plan de cuenta
        If chkGastosVarios.Checked = False Then
            CSistema.SetSQLParameter(param, "@IDUnidadNegocio", cbxUnidadNegocio.cbx.SelectedValue, ParameterDirection.Input)
        End If
        'CSistema.SetSQLParameter(param, "@IDCentroCosto", cbxCentroCosto.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepartamento", cbxDepartamento.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@vGastosVarios", chkGastosVarios.Checked, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpDebitoCreditoBancario", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            Exit Sub

        End If

        'Si es nuevo
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then

            'Cargamos el DetalleImpuesto
            OcxImpuesto1.Generar(IDTransaccion)
            CDetalleImpuesto.dt = OcxImpuesto1.dtImpuesto
            CDetalleImpuesto.IDMoneda = OcxImpuesto1.IDMoneda
            CDetalleImpuesto.Guardar(IDTransaccion)

            'Cargamos el asiento
            CAsiento.IDTransaccion = IDTransaccion
            CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

        'Si es desde un Prestamo, cerrar y devolver el IDTransaccion
        If DebitoCreditoBancarioPrestamo Then
            Procesado = True
            Me.Close()
        End If

        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False
        Cancelar()

    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        Dim Comprobante As String = cbxDebitoCredito.cbx.Text & " - " & cbxTipoComprobante.cbx.Text & ": " & txtComprobante.txt.Text

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante
            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From vDebitoCreditoBancario Where IDSucursal = " & cbxSucursal.GetValue & " and Numero=" & txtID.ObtenerValor & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante
            'frm.cbxUnidadNegocio.get = cbxUnidadNegocio.cbx.SelectedValue
            'frm.cbxCentroCosto.SelectedValue = cbxCentroCosto.cbx.SelectedValue
            'frm.cbxDepartamento.SelectedValue = cbxDepartamento.cbx.SelectedValue

            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            CAsiento.ListarDetalle(frm.dgv)
            frm.CalcularTotales()

            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
            CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            CAsiento.NroCaja = frm.CAsiento.NroCaja
            CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                CAsiento.dtAsiento.Clear()
                CAsiento.dtDetalleAsiento.Clear()
                VisualizarAsiento()
            End If

        End If

    End Sub

    Sub GenerarAsiento()

        'EstablecerCabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = CData.GetRow("ID=" & cbxSucursal.GetValue, "VSucursal")("IDCiudad")
        oRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
        oRow("Fecha") = txtfecha.GetValue

        Dim CuentaRow As DataRow = CData.GetRow("ID=" & cbxCuenta.GetValue, dtCuentaBancaria)

        oRow("IDMoneda") = CuentaRow("IDMoneda")
        oRow("Cotizacion") = txtCambio.ObtenerValor
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = txtComprobante.txt.Text
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text
        oRow("Detalle") = txtObservacion.txt.Text

        'SC: 20-09-2021 Guaraniza cabecera asiento
        If txtCambio.ObtenerValor <> 1 Then
            oRow("Total") = txtCambio.ObtenerValor * CSistema.FormatoMoneda3Decimales(OcxImpuesto1.Total, True)
            oRow("Debito") = txtCambio.ObtenerValor * CSistema.FormatoMoneda3Decimales(OcxImpuesto1.Total, True)
            oRow("Credito") = txtCambio.ObtenerValor * CSistema.FormatoMoneda3Decimales(OcxImpuesto1.Total, True)

        Else
            'SC - Agregue estos 2 que faltaban, igualados al total - 01-09-2021
            oRow("Total") = OcxImpuesto1.txtTotal.ObtenerValor
            oRow("Debito") = OcxImpuesto1.txtTotal.ObtenerValor
            oRow("Credito") = OcxImpuesto1.txtTotal.ObtenerValor
        End If

        'FA 22/11/2022 - Plan de cuenta
        oRow("IDUnidadNegocio") = cbxUnidadNegocio.GetValue
        'oRow("IDCentroCosto") = cbxCentroCosto.GetValue
        oRow("IDDepartamento") = cbxDepartamento.GetValue
        oRow("GastosMultiples") = chkGastosVarios.Checked

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)

        CAsiento.IDCuentaBancaria = cbxCuenta.GetValue
        CAsiento.Total = OcxImpuesto1.txtTotal.ObtenerValor

        CAsiento.IDSucursal = cbxSucursal.GetValue

        If cbxDebitoCredito.cbx.SelectedIndex = 0 Then
            CAsiento.Credito = False
        Else
            CAsiento.Credito = True
        End If

        If cbxDebitoCredito.cbx.SelectedIndex = 1 Then
            CAsiento.Credito = True
        Else
            CAsiento.Credito = False
        End If

        CAsiento.Generar()

        vAsiento = True

    End Sub

    Sub CalcularTotales()

        OcxImpuesto1.CargarValores(CDetalleImpuesto.dt)

    End Sub

    Sub Eliminar()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'SC: 20-08-2021 - Restringe la eliminación de un Extorno generado desde una cobranza
        Dim NumeroDeposito As Integer = 0

        ''Se extrae el número del deposito relacionado al DebitoCredito por Extorno, siempre que el mismo se encuentre relacionado
        NumeroDeposito = CSistema.ExecuteScalar("Select IsNull((Select NumeroDeposito From VDebitoCreditoBancarioDeposito Where EstadoMovimiento = 1 and IDTransaccionExtorno = " & IDTransaccion & "), 0 )")

        If NumeroDeposito <> 0 Then
            Dim mensaje As String = " El DebitoCreditoBancario por EXTORNO no se puede ELIMINAR. Esta asociado al Deposito: " & NumeroDeposito
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.BottomRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If
        'SC: Validar Transaccion Acreditacion

        'Se extrae el número del deposito relacionado al DebitoCredito por Acreditacion, siempre que el mismo se encuentre relacionado
        NumeroDeposito = CSistema.ExecuteScalar("Select IsNull((Select NumeroDeposito From VDebitoCreditoBancarioDeposito Where EstadoMovimiento = 1 and IDTransaccionAcreditacion = " & IDTransaccion & "), 0 )")

        If NumeroDeposito <> 0 Then
            Dim mensaje As String = " El DebitoCreditoBancario por ACREDITACION no se puede ELIMINAR. Esta asociado al Deposito: " & NumeroDeposito
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.BottomRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.DEL.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Eliminar
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpDebitoCreditoBancario", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From DebitoCreditoBancario Where Numero=" & txtID.ObtenerValor & " And IDSucursal = " & cbxSucursal.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VDebitoCreditoBancario Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        txtID.txt.Text = oRow("Numero").ToString

        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtfecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        txtCambio.txt.Text = oRow("Cotizacion").ToString
        cbxCuenta.cbx.Text = oRow("Cuenta").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxDebitoCredito.txt.Text = oRow("Tipo").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString

        'SC 04082021 - Nuevo campo para distinguir si la operacion corresponde a una cobranza
        chkEsCobranza.Checked = CBool(oRow("EsCobranza").ToString)

        'FA 15/11/2022 - Ajuste segun plan de cuenta
        cbxUnidadNegocio.cbx.Text = oRow("UnidadNegocio").ToString
        'cbxCentroCosto.cbx.Text = oRow("CentroCosto").ToString
        cbxDepartamento.cbx.Text = oRow("Departamento").ToString
        chkGastosVarios.Checked = oRow("GastosVarios").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        'Cargamos el detalle
        OcxImpuesto1.IDMoneda = oRow("IDMoneda")
        OcxImpuesto1.CargarValores(IDTransaccion)

        'Inicializamos el Asiento
        CAsiento.Limpiar()
        chkGastosVarios.Enabled = False

    End Sub

    Sub ObtenerCuenta()

        If cbxCuenta.cbx.SelectedValue Is Nothing Then
            Exit Sub
        End If

        txtBanco.txt.Clear()
        txtMoneda.txt.Clear()
        If vNuevo = True And txtfecha.txt.Text = "  /  /" Then
            txtfecha.Hoy()
        End If

        For Each oRow As DataRow In dtCuentaBancaria.Select(" ID=" & cbxCuenta.cbx.SelectedValue)

            txtBanco.txt.Text = oRow("Banco").ToString
            txtMoneda.txt.Text = oRow("Mon").ToString

            'Configuramos la moneda en total de impuesto
            OcxImpuesto1.IDMoneda = oRow("IDMoneda")
            If vNuevo = True Then
                If oRow("IDMoneda") = 1 Then
                    txtCambio.txt.Text = "1"
                Else
                    txtCambio.txt.Text = CSistema.ExecuteScalar("Select isnull(cotizacion,1) from vCotizacion where ID = (Select max(ID) from Cotizacion where cast(Fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "' and IDMoneda = " & oRow("IDMoneda") & ")")
                End If
            End If

        Next


    End Sub

    Sub Buscar()

        Dim frm As New frmConsultaDebitoCreditoBancario
        frm.WindowState = FormWindowState.Maximized
        FGMostrarFormulario(Me, frm, "Consulta", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)
        CargarOperacion(frm.IDTransaccion)

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim TipoInforme As String = CType(CSistema.ExecuteScalar("Select Concat(Ciudad,' ',numero,' ',Tipo, ' - FECHA: ',Fec,' - BANCO-CTA: ',Banco, ' ',Cuenta, ' - ',Moneda) From VDebitoCreditoBancario Where IDTransaccion =" & IDTransaccion), String)
        Dim Titulo As String = " INFORME DEBITO Y CREDITO BANCARIO "
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        Where = " Where IDTransaccion = " & IDTransaccion
        CReporte.ListadoDebitoCreditoBancario(frm, Titulo, Where, OrderBy, Top, TipoInforme, True, True)


    End Sub

    Private Sub frmDebitoCreditoBancario_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
        'FA 20/06/2023
        LiberarMemoria()
    End Sub

    Private Sub frmDebitoCreditoBancario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub cbxCuenta_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxCuenta.LostFocus
        ObtenerCuenta()
    End Sub

    Private Sub cbxCuenta_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCuenta.PropertyChanged
        ObtenerCuenta()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Public Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Eliminar()
    End Sub

    Private Sub frmDebitoCreditoBancario_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub cbxSucursal_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        If Me.ActiveControl Is Nothing Then
            Exit Sub
        End If

        If Me.ActiveControl.Name.ToString = cbxSucursal.Name.ToString Then
            txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        End If
    End Sub

    Private Sub btnImprimir_Click(sender As System.Object, e As System.EventArgs) Handles btnImprimir.Click
        Listar()
    End Sub

    Private Sub txtMoneda_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtMoneda.TeclaPrecionada
        vFecha = CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False)
        If (vFecha.Length = 8) And (vNuevo = True) Then
            If OcxImpuesto1.IDMoneda = 1 Then
                txtCambio.txt.Text = "1"
            Else
                txtCambio.txt.Text = CSistema.ExecuteScalar("Select isnull(cotizacion,1) from vCotizacion where ID = (Select max(ID) from Cotizacion where cast(Fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "' and IDMoneda = " & OcxImpuesto1.IDMoneda & ")")
            End If
        End If
    End Sub

    Private Sub txtfecha_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtfecha.TeclaPrecionada
        vFecha = CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False)
        If vFecha.Length = 8 And vNuevo = True Then
            If OcxImpuesto1.IDMoneda = 1 Then
                txtCambio.txt.Text = "1"
            Else
                txtCambio.txt.Text = CSistema.ExecuteScalar("Select isnull(cotizacion,1) from vCotizacion where ID = (Select max(ID) from Cotizacion where cast(Fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "' and IDMoneda = " & OcxImpuesto1.IDMoneda & ")")
            End If
        End If
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmDebitoCreditoBancario_Activate()
        Me.Refresh()
    End Sub

    Private Sub chkGastosVarios_CheckedChanged(sender As Object, e As EventArgs) Handles chkGastosVarios.CheckedChanged
        If vNuevo = True Then
            If chkGastosVarios.Checked = True Then
                cbxUnidadNegocio.SoloLectura = True
                lblDepartamento.Visible = False
                lblSolicitante.Visible = True
                'cbxDepartamentoEmpresa.SoloLectura = True
            Else
                cbxUnidadNegocio.SoloLectura = False
                lblDepartamento.Visible = True
                lblSolicitante.Visible = False
                'cbxDepartamentoEmpresa.SoloLectura = False
            End If
        End If
    End Sub

End Class
﻿Public Class frmConsultaDebitoCreditoBancario

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CData As New CData

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Public Property Total As Decimal = 0

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim IDOperacion As Integer
    Dim Consulta As String
    Dim Where As String
    Dim dt As DataTable

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'TextBox
        txtCantidad.txt.ResetText()
        txtNroComprobante.txt.ResetText()
        txtOperacion.txt.ResetText()
        txtTotal.txt.ResetText()

        'CheckBox
        chkTipoComprobante.Checked = False
        chkCuentaBancaria.Checked = False
        chkFecha.Checked = False

        'ComboBox
        cbxTipoComprobante.Enabled = False
        cbxCuentaBancaria.Enabled = False

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion(frmDebitoCreditoBancario.Name, "DEBITO CREDITO BANCARIO", "DCB")
        CargarInformacion()

        'Foco

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Cuenta Bancaria
        CSistema.SqlToComboBox(cbxCuentaBancaria, "Select  ID, CuentaBancaria From CuentaBancaria Order By 2")

        'TipoComprobante
        CSistema.SqlToComboBox(cbxTipoComprobante, "Select ID, Descripcion From VTipoComprobante Where IDOperacion=" & IDOperacion & " Order By 2")

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Cuenta Bancaria
        chkCuentaBancaria.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA ACTIVO", "False")
        cbxCuentaBancaria.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA", "")

        'Tipo de Comprobante
        chkTipoComprobante.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO", "False")
        cbxTipoComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", "")

        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Cuenta Bancaria
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA ACTIVO", chkCuentaBancaria.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA", cbxCuentaBancaria.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO", chkTipoComprobante.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", cbxTipoComprobante.Text)

    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar
    Sub Listar(Optional ByVal Condicion As String = "")

        ctrError.Clear()

        'Inicializr el Where
        Where = ""

        'Cuenta Bancaria
        If EstablecerCondicion(cbxCuentaBancaria, chkCuentaBancaria, "IDCuentaBancaria", "Seleccione correctamente el cliente!") = False Then
            Exit Sub
        End If

        'Comprobante
        If EstablecerCondicion(cbxTipoComprobante, chkTipoComprobante, "IDTipoComprobante", "Seleccione correctamente el tipo de comprobante!") = False Then
            Exit Sub
        End If

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la sucursal!") = False Then
            Exit Sub
        End If

        'Fecha
        If chkFecha.Checked = True Then
            If Where = "" Then
                Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' ) "
            Else
                Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' )  "
            End If
        End If

        'Buscar por Operacion
        If Condicion <> "" Then
            Where = Condicion
        End If

        'buscar por Comprobantes

        If Condicion <> "" Then
            Where = Condicion
        End If

        Consulta = "Select IDTransaccion, Numero, Suc, 'T.Comp'= DescripcionTipoComprobante, Comprobante, Fec, Tipo, Cuenta, Banco, Observacion, Moneda, Total, IDMoneda From VDebitoCreditoBancario " & Where

        CSistema.SqlToDataGrid(dgwOperacion, Consulta)

        'Formato
        dgwOperacion.Columns("IDTransaccion").Visible = False
        dgwOperacion.Columns("IDMoneda").Visible = False

        dgwOperacion.Columns("Fec").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgwOperacion.Columns("Moneda").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        dgwOperacion.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgwOperacion.Columns("Observacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        Dim Decimales As Integer = 0

        Try
            If CData.GetRow("IDMoneda=" & dgwOperacion.SelectedRows(0).Cells("IDMoneda").Value, "VImpuesto")("Decimales") = True Then
                Decimales = "2"
            End If

        Catch ex As Exception

        End Try

        dgwOperacion.Columns("Total").DefaultCellStyle.Format = "N" & Decimales.ToString



        txtTotal.SetValue(CSistema.gridSumColumn(dgwOperacion, "Total"))
        txtCantidad.SetValue(dgwOperacion.RowCount)


    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    Sub SeleccionarRegistro()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = dgwOperacion.SelectedRows(0).Cells(0).Value

        'Obtener el Total
        Total = dgwOperacion.SelectedRows(0).Cells("Total").Value

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub frmConsultaDebitoCreditoBancario_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaDebitoCreditoBancario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkCliente_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCuentaBancaria.CheckedChanged
        HabilitarControles(chkCuentaBancaria, cbxCuentaBancaria)
    End Sub

    Private Sub chkBanco_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipoComprobante.CheckedChanged
        HabilitarControles(chkTipoComprobante, cbxTipoComprobante)
    End Sub

    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged
        HabilitarControles(chkFecha, dtpDesde)
        HabilitarControles(chkFecha, dtpHasta)
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        Listar()
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            Dim Condicion As String = " Where Numero = " & txtOperacion.ObtenerValor
            Listar(Condicion)
            txtOperacion.txt.SelectAll()
        End If
    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then

            Dim Condicion1 As String = " Where Comprobante Like '%" & txtNroComprobante.txt.Text & "%' "
            Listar(Condicion1)
        End If

    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        SeleccionarRegistro()
    End Sub

    Private Sub dgwOperacion_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        SeleccionarRegistro()
    End Sub

    Private Sub dgwOperacion_CellDoubleClick_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgwOperacion.CellDoubleClick
        SeleccionarRegistro()
    End Sub

    Private Sub dgwOperacion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgwOperacion.KeyDown

        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub dgwOperacion_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgwOperacion.KeyUp
        If e.KeyCode = Keys.Enter Then
            SeleccionarRegistro()
        End If
    End Sub


    '09-06-2021 - SC - Actualiza datos
    Sub frmConsultaDebitoCreditoBancario_Activate()
        Me.Refresh()
    End Sub
End Class
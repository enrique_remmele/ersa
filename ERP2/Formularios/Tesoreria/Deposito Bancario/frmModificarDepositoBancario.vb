﻿Public Class frmModificarDepositoBancario


    'CLASE
    Dim CSistema As New CSistema

    'PRIPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private FechaValue As Date
    Public Property Fecha() As Date
        Get
            Return FechaValue
        End Get
        Set(ByVal value As Date)
            FechaValue = value
        End Set
    End Property

    Private SaldoValue As Decimal
    Public Property Saldo() As Decimal
        Get
            Return SaldoValue
        End Get
        Set(ByVal value As Decimal)
            SaldoValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()
        CargarOperacion()


    End Sub

    Sub CargarInformacion()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True


    End Sub
    Sub CargarOperacion()

        'Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VCobranzaContado Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtComprobante, mensaje)
            ctrError.SetIconAlignment(txtComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtFecha.txt.Text = oRow("Fecha").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString

    End Sub

    Sub Modificar()
        'Aplicar
        Try
            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Comprobante", txtComprobante.txt.Text, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Fecha", txtFecha.GetValueString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Aplicar la Cobranza
            If CSistema.ExecuteStoreProcedure(param, "SpActualizarDepositoBancario", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnModificar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnModificar, ErrorIconAlignment.TopRight)
            End If

            Me.Close()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub frmSeleccionFormaPagoDocumento_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmSeleccionEfectivo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmModificarDepositoBancario_Activate()
        Me.Refresh()
    End Sub

End Class
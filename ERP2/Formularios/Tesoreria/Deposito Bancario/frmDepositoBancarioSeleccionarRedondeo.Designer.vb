﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDepositoBancarioSeleccionarRedondeo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblRedondearA = New System.Windows.Forms.Label()
        Me.lblTotalComprobante = New System.Windows.Forms.Label()
        Me.lblDiferencia = New System.Windows.Forms.Label()
        Me.gbxDatos = New System.Windows.Forms.GroupBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.txtRedondearA = New ERP.ocxTXTNumeric()
        Me.txtDiferencia = New ERP.ocxTXTNumeric()
        Me.txtTotalComprobante = New ERP.ocxTXTNumeric()
        Me.gbxDatos.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblRedondearA
        '
        Me.lblRedondearA.AutoSize = True
        Me.lblRedondearA.Location = New System.Drawing.Point(50, 34)
        Me.lblRedondearA.Name = "lblRedondearA"
        Me.lblRedondearA.Size = New System.Drawing.Size(72, 13)
        Me.lblRedondearA.TabIndex = 0
        Me.lblRedondearA.Text = "Redondear a:"
        '
        'lblTotalComprobante
        '
        Me.lblTotalComprobante.AutoSize = True
        Me.lblTotalComprobante.Location = New System.Drawing.Point(17, 60)
        Me.lblTotalComprobante.Name = "lblTotalComprobante"
        Me.lblTotalComprobante.Size = New System.Drawing.Size(105, 13)
        Me.lblTotalComprobante.TabIndex = 2
        Me.lblTotalComprobante.Text = "Total Comprobantes:"
        '
        'lblDiferencia
        '
        Me.lblDiferencia.AutoSize = True
        Me.lblDiferencia.Location = New System.Drawing.Point(64, 86)
        Me.lblDiferencia.Name = "lblDiferencia"
        Me.lblDiferencia.Size = New System.Drawing.Size(58, 13)
        Me.lblDiferencia.TabIndex = 4
        Me.lblDiferencia.Text = "Diferencia:"
        '
        'gbxDatos
        '
        Me.gbxDatos.Controls.Add(Me.txtRedondearA)
        Me.gbxDatos.Controls.Add(Me.txtDiferencia)
        Me.gbxDatos.Controls.Add(Me.lblRedondearA)
        Me.gbxDatos.Controls.Add(Me.lblDiferencia)
        Me.gbxDatos.Controls.Add(Me.lblTotalComprobante)
        Me.gbxDatos.Controls.Add(Me.txtTotalComprobante)
        Me.gbxDatos.Location = New System.Drawing.Point(12, 12)
        Me.gbxDatos.Name = "gbxDatos"
        Me.gbxDatos.Size = New System.Drawing.Size(271, 128)
        Me.gbxDatos.TabIndex = 0
        Me.gbxDatos.TabStop = False
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(195, 155)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(114, 155)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 1
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'txtRedondearA
        '
        Me.txtRedondearA.Color = System.Drawing.Color.Empty
        Me.txtRedondearA.Decimales = True
        Me.txtRedondearA.Indicaciones = Nothing
        Me.txtRedondearA.Location = New System.Drawing.Point(128, 30)
        Me.txtRedondearA.Name = "txtRedondearA"
        Me.txtRedondearA.Size = New System.Drawing.Size(107, 20)
        Me.txtRedondearA.SoloLectura = False
        Me.txtRedondearA.TabIndex = 1
        Me.txtRedondearA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRedondearA.Texto = "0"
        '
        'txtDiferencia
        '
        Me.txtDiferencia.Color = System.Drawing.Color.Empty
        Me.txtDiferencia.Decimales = True
        Me.txtDiferencia.Indicaciones = Nothing
        Me.txtDiferencia.Location = New System.Drawing.Point(128, 82)
        Me.txtDiferencia.Name = "txtDiferencia"
        Me.txtDiferencia.Size = New System.Drawing.Size(107, 20)
        Me.txtDiferencia.SoloLectura = False
        Me.txtDiferencia.TabIndex = 5
        Me.txtDiferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDiferencia.Texto = "0"
        '
        'txtTotalComprobante
        '
        Me.txtTotalComprobante.Color = System.Drawing.Color.Empty
        Me.txtTotalComprobante.Decimales = True
        Me.txtTotalComprobante.Indicaciones = Nothing
        Me.txtTotalComprobante.Location = New System.Drawing.Point(128, 56)
        Me.txtTotalComprobante.Name = "txtTotalComprobante"
        Me.txtTotalComprobante.Size = New System.Drawing.Size(107, 20)
        Me.txtTotalComprobante.SoloLectura = True
        Me.txtTotalComprobante.TabIndex = 3
        Me.txtTotalComprobante.TabStop = False
        Me.txtTotalComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalComprobante.Texto = "0"
        '
        'frmDepositoBancarioSeleccionarRedondeo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(293, 190)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.gbxDatos)
        Me.Name = "frmDepositoBancarioSeleccionarRedondeo"
        Me.Text = "frmSeleccionarRedondeo"
        Me.gbxDatos.ResumeLayout(False)
        Me.gbxDatos.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblRedondearA As System.Windows.Forms.Label
    Friend WithEvents txtRedondearA As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalComprobante As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalComprobante As System.Windows.Forms.Label
    Friend WithEvents txtDiferencia As ERP.ocxTXTNumeric
    Friend WithEvents lblDiferencia As System.Windows.Forms.Label
    Friend WithEvents gbxDatos As System.Windows.Forms.GroupBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
End Class

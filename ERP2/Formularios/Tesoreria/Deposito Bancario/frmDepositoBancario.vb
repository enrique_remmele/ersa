﻿Public Class frmDepositoBancario
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CAsiento As New CAsientoDepositoBancario
    Dim CData As New CData

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property
    Private dtDepositoBancarioValue As DataTable
    Public Property dtDepositoBancario As DataTable
        Get
            Return dtDepositoBancarioValue
        End Get
        Set(ByVal value As DataTable)
            dtDepositoBancarioValue = value
        End Set
    End Property
    Private TotalRestarValue As Decimal
    Public Property TotalRestar() As Decimal
        Get
            Return TotalRestarValue
        End Get
        Set(ByVal value As Decimal)
            TotalRestarValue = value
        End Set
    End Property

    'SC: 23082021 - Codigo nuevo para obtener cotizacion del dia
    Private CotizacionDelDiaValue As Decimal
    Public Property CotizacionDelDia() As Decimal
        Get
            Return CotizacionDelDiaValue
        End Get
        Set(ByVal value As Decimal)
            CotizacionDelDiaValue = value
            'txtCotizacion.SetValue(value)
        End Set
    End Property

    'EVENTOS

    'VARIABLES
    Dim dtEfectivo As New DataTable
    Dim dtCheque As New DataTable
    Dim dtDocumento As New DataTable
    Dim dtTarjeta As New DataTable
    Dim dtRedondeo As New DataTable
    Dim dtCuentaBancaria As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    'SC - 23-08-2021 - Variable para controlar Documento
    Dim vDocumento As Boolean
    'SC 23-08-2021 Nueva estructura de datos
    Dim dtDC As New DataTable
    Public Property IDMonedaPago As Integer

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "DEPOSITO BANCARIO", "DEPB")
        vNuevo = True

        'SC: variable para controlar forma de Pago Documento
        vDocumento = False

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        If CBool(vgConfiguraciones("TesoreriaBloquerFecha").ToString) = True Then
            txtfecha.Enabled = False
        Else
            txtfecha.Enabled = True
        End If
    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        'CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtfecha)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, cbxCuenta)
        CSistema.CargaControl(vControles, txtObservacion)
        'CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, txtCambio)
        CSistema.CargaControl(vControles, txtComprobante)

        'Efectivo, Cheques, Otros Cheques
        CSistema.CargaControl(vControles, btnAgregarEfectivo)
        CSistema.CargaControl(vControles, btnAgregarCheques)
        CSistema.CargaControl(vControles, btnAgregarDocumentos)
        CSistema.CargaControl(vControles, btnAgregarTarjetas)
        CSistema.CargaControl(vControles, btnRedondear)
        CSistema.CargaControl(vControles, lklEliminarVenta)

        'Forma de Pago
        ' CSistema.CargaControl(vControles, OcxFormaPago1)

        'CARGAR ESTRUCTURA DEL DETALLE VENTA

        'CARGAR CONTROLES
        'Ciudad
        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Cuenta Bancaria
        dtCuentaBancaria = CSistema.ExecuteToDataTable("Select * From VCuentaBancaria").Copy
        CSistema.SqlToComboBox(cbxCuenta.cbx, dtCuentaBancaria, "ID", "Descripcion")

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "OPERACION", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        txtCambio.txt.Text = 1

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "OPERACION", cbxCiudad.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        ''Limpiar detalle
        'ListarComprobantes()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        CAsiento.Limpiar()

        vNuevo = True

        'SC: variable para controlar forma de Pago Documento
        vDocumento = False

        'Cabecera
        cbxCuenta.Enabled = True
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()
        If txtCambio.ObtenerValor = 0 Then
            txtCambio.SetValue(1)
        End If

        'Cuenta Bancaria
        dtCuentaBancaria = CSistema.ExecuteToDataTable("Select * From VCuentaBancaria where Estado = 1").Copy
        CSistema.SqlToComboBox(cbxCuenta.cbx, dtCuentaBancaria, "ID", "Descripcion")

        'detalle
        dgw.Rows.Clear()
        txtTotalEfectivo.txt.Clear()
        txtTotalDepositado.txt.Clear()
        txtTotalChequeLocal.txt.Clear()
        txtTotalChequeOtro.txt.Clear()
        txtTotalDocumento.txt.Clear()
        TxtCantidadDepositado.txt.Clear()
        dtCheque.Rows.Clear()
        dtEfectivo.Rows.Clear()
        dtDocumento.Rows.Clear()
        dtRedondeo.Rows.Clear()

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(MAX(Numero + 1),1) From VDepositoBancario Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)
        dtEfectivo = CSistema.ExecuteToDataTable("Select IDTransaccion,ID,Numero,IDTipoComprobante,TipoComprobante,CodigoComprobante,IDSucursal,Comprobante,Fecha,Fec,IDMoneda,Moneda,ImporteMoneda,Cotizacion,Importe,Depositado,Saldo,'Sel'='False','Cancelar'='False',IDCobrador,Cobrador,NroOperacionCobranza, IdUsuario, Decimales From VEfectivo Where Cancelado = 'False' And Anulado = 'False' And Saldo > 0 And IDSucursal=" & cbxSucursal.GetValue & " Order By Fecha").Copy
        dtCheque = CSistema.ExecuteToDataTable("Select *, 'Sel'='False', 'Cancelar'='False',IdUsuario From VChequeClienteTransaccion Where (Cartera = 'True' Or Rechazado = 'True') Order By Fecha").Copy
        dtDocumento = CSistema.ExecuteToDataTable("Select *,'Sel'='False','Cancelar'='False',IdUsuario From VFormaPagoDocumento Where Deposito='True' And Saldo > 0 and IdTransaccion not in (select IDTransaccion from OrdenPago)").Copy
        dtRedondeo = CSistema.ExecuteToDataTable("Select Top(0) *,'Sel'='False','Cancelar'='False' From VDetalleDepositoBancario ").Clone
        dtTarjeta = CSistema.ExecuteToDataTable("select *,'Sel'='False','Cancelar'='False',IDUsuario from VFormaPagoTarjeta where Deposito = 'True' and Saldo > 0").Copy

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        flpRegistradoPor.Visible = False
        ObtenerCuenta()
        'Poner el foco en el proveedor
        cbxTipoComprobante.cbx.Focus()
        txtfecha.Hoy()

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles, btnModificar)

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If

        End If

        'Validar
        'Comprobante
        If IsNumeric(txtComprobante.txt.Text) = False Then
            CSistema.MostrarError("Debe Ingresar Solo Numeros", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.BottomRight)
            Exit Function
        End If

        'Validar Detalle
        If dgw.Rows.Count = 0 Then
            CSistema.MostrarError("Debe Ingresar los Detalles", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.BottomRight)
            Exit Function
        End If

        'Ciudad
        If cbxCiudad.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente la ciudad!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Tipo Comprobante
        If cbxTipoComprobante.cbx.Text.Trim = "" Then
            Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
            Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
            ctrError.SetError(cbxTipoComprobante, mensaje)
            ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Sucursal
        If cbxSucursal.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If cbxSucursal.cbx.Text.Trim = "" Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Lote
        ' If cbxLote.Validar("Seleccione correctamente la sucursal!", ctrError, btnGuardar, tsslEstado) = False Then
        't Function
        ' End If

        'Venta
        'If dgw.Rows.Count = 0 Then
        '    'Dim mensaje As String = "El documento tiene que tener comprobantes de venta!"
        '    'ctrError.SetError(btnGuardar, mensaje)
        '    'ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
        '    'tsslEstado.Text = mensaje
        '    'Exit Function
        'End If

        'If txtTotalVenta.ObtenerValor <= 0 Then
        '    Dim mensaje As String = "El importe de los comprobantes de venta no es valido!"
        '    ctrError.SetError(btnGuardar, mensaje)
        '    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
        '    tsslEstado.Text = mensaje
        '    Exit Function
        'End If

        'Forma de Pago

        ''Saldo
        'If txtSaldoTotal.ObtenerValor <> 0 Then
        '    Dim mensaje As String = "El importe de los comprobantes de venta y la forma de pago no concuerdan!"
        '    ctrError.SetError(btnGuardar, mensaje)
        '    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
        '    tsslEstado.Text = mensaje
        '    Exit Function
        'End If

        'Asiento
        'If Operacion <> ERP.CSistema.NUMOperacionesRegistro.DEL Then

        '    'Validar el Asiento
        '    If CAsiento.ObtenerSaldo <> 0 Then
        '        CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '        Exit Function
        '    End If

        '    If CAsiento.ObtenerTotal = 0 Then
        '        CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '        Exit Function
        '    End If
        'End If

        Return True


    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtfecha.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCuentaBancaria", cbxCuenta.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(txtCambio.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        'Totales
        Dim IDMoneda As Integer = CSistema.RetornarValorInteger(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("IDMoneda").ToString)
        Dim Decimales As Boolean = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & IDMoneda, "VMoneda")("Decimales").ToString)

        CSistema.SetSQLParameter(param, "@TotalEfectivo", CSistema.FormatoMonedaBaseDatos(txtTotalEfectivo.ObtenerValor, Decimales), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalChequeLocal", CSistema.FormatoMonedaBaseDatos(txtTotalChequeLocal.ObtenerValor, Decimales), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalChequeOtros", CSistema.FormatoMonedaBaseDatos(txtTotalChequeOtro.ObtenerValor, Decimales), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDocumento", CSistema.FormatoMonedaBaseDatos(txtTotalDocumento.ObtenerValor, Decimales), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalTarjeta", CSistema.FormatoMonedaBaseDatos(txtTotalTarjeta.ObtenerValor, Decimales), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(txtTotalDepositado.ObtenerValor, Decimales), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DiferenciaCambio", CSistema.FormatoMonedaBaseDatos(txtDiferenciaCambio.ObtenerValor, False), ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpDepositoBancario", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            Exit Sub

        End If

        Dim Procesar As Boolean = True


        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
            CargarOperacion(IDTransaccion)

            txtID.SoloLectura = False
        End If

        'Guardar detalle
        If IDTransaccion > 0 Then

            'Guardar Efectivo
            Procesar = InsertarDetalle(IDTransaccion)


        End If

        'Aplicar el Deposito Bancario
        '
        Try

            ReDim param(-1)

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            'Aplicar
            If CSistema.ExecuteStoreProcedure(param, "SpDepositoBancarioProcesar", False, False, MensajeRetorno) = False Then

            End If

            CSistema.ExecuteNonQuery("Exec SPAsientoDepositoBancario " & IDTransaccion)
            ''Cargamos el asiento
            'CAsiento.IDTransaccion = IDTransaccion
            'GenerarAsiento()
            'CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        Catch ex As Exception

        End Try

        'SC 10-08-2021 Nueva Rutina que selecciona DebitoCreditoBancario para Extorno
        SeleccionaDebitoCreditoBancario(IDTransaccion)


        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False

    End Sub

    Sub SeleccionaDebitoCreditoBancario(ByVal IDTransaccionCobranza As Integer)
        'Desde aqui SC 23-08-2021 para Gestionar Depositos que corresponden a DebitoCredito
        If vDocumento = False Then
            Exit Sub
            'Continua
        Else
            Dim where As String = ""
            Dim valorwhere As String = ""

            'Filtra por Sucursal
            'If cbxSucursal.GetValue > 0 Then
            '    where = where & " IDSucursal = " & cbxSucursal.GetValue
            'End If

            'Filtra por EsCobranza SI = 1
            valorwhere = 1
            where = where & "EsCobranza = " & valorwhere

            'Filtra por Procesado NO = 0
            valorwhere = 0
            where = where & " AND EsProcesado = " & valorwhere

            dtDC = CSistema.ExecuteToDataTable("Select IDSucursal, IDTransaccion, Seleccionado, NroComprobante, Tipo, Fecha, IDCuentaBancaria,Cuenta, Banco, Moneda, Total, TotalImpuesto, TotalDiscriminado, Observacion, Cancelar,Cotizacion,idmoneda, IDUnidadNegocio,IDDepartamento,GastosVarios From vDebitoCreditoBancarioSelect  Where " & where).Copy

            'Prepara datos para llamar al formulario donde se seleccionará DebitosCreditosBancarios para Cobranza
            Dim frm As New frmSeleccionarDebitoCreditoBancario
            frm.Text = "Selección de Débito Crédito Bancario para Cobranza"
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.dt = dtDC

            frm.Inicializar()
            FGMostrarFormulario(Me, frm, "Selección de Débito Crédito Bancario para Cobranza", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

            'Obtiene datos seleccionados y los procesa para EXTORNO
            dtDC = frm.dt
            ListarDebitoCreditoBancarioCobranza(IDTransaccionCobranza)

        End If
        'Hasta aqui SC 23-08-2021 para Gestionar cobranzas que corresponden a DebitoCredito

    End Sub

    'SC 23-08-2021 Rutina nueva para Gestion de Extorno por DebitoCreditoBancario
    Sub ListarDebitoCreditoBancarioCobranza(ByVal IDTransaccionDeposito As Integer)
        Dim Decimales As Boolean = False
        Dim IndiceOperacion As Integer
        Dim Cotizacion As Integer = 1

        ctrError.Clear()

        If dtDC.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each oRow1 As DataRow In dtDC.Rows
            If CSistema.RetornarValorBoolean(oRow1("Seleccionado").ToString) = True Then

                'Tipo de Comprobante 24 = DEBITO - DEB BAN
                Dim IDTipoComprobanteDCB As Integer = 24
                'Obtener registro nuevo
                Dim IDDCB As String
                IDDCB = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From VDebitoCreditoBancario Where IDSucursal=" & oRow1("IDSucursal").ToString & "),1)"), Integer)

                IDMonedaPago = CSistema.FormatoNumeroBaseDatos((oRow1("IDmoneda").ToString), True)
                If IDMonedaPago <> 1 Then
                    CotizacionDelDia = CSistema.CotizacionDelDia(IDMonedaPago, IDOperacion)
                    Cotizacion = CotizacionDelDia
                    Decimales = True
                End If

                Dim param(-1) As SqlClient.SqlParameter
                CSistema.SetSQLParameter(param, "@IDSucursalOperacion", oRow1("IDSucursal").ToString, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDTipoComprobante", IDTipoComprobanteDCB, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Numero", IDDCB, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@NroComprobante", oRow1("NroComprobante").ToString, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtfecha.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDCuentaBancaria", oRow1("IDCuentaBancaria").ToString, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Cotizacion", Cotizacion, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Observacion", "EXTORNO - " & oRow1("Observacion").ToString, ParameterDirection.Input)

                CSistema.SetSQLParameter(param, "@Debito", "FALSE", ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Credito", "TRUE", ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

                IndiceOperacion = param.GetLength(0) - 1

                'Totales
                CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(oRow1("Total").ToString, Decimales), ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(oRow1("TotalImpuesto").ToString, Decimales), ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(oRow1("TotalDiscriminado").ToString, Decimales), ParameterDirection.Input)

                CSistema.SetSQLParameter(param, "@EsProcesado", "TRUE", ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@EsCobranza", "TRUE", ParameterDirection.Input)

                'FA 15/11/2022 - Nuevos campos para adecuacion plan de cuenta
                If oRow1("GastosVarios") = False Then
                    CSistema.SetSQLParameter(param, "@IDUnidadNegocio", oRow1("IDUnidadNegocio"), ParameterDirection.Input)
                End If
                'CSistema.SetSQLParameter(param, "@IDCentroCosto", cbxCentroCosto.cbx.SelectedValue, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDDepartamento", oRow1("IDDepartamento"), ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@vGastosVarios", oRow1("GastosVarios"), ParameterDirection.Input)

                'Transaccion
                CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

                'Informacion de Salida
                CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
                CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
                CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

                Dim MensajeRetorno As String = ""

                Dim IDTransaccionExtorno As Integer
                'Insertar Registro
                If CSistema.ExecuteStoreProcedure(param, "SpDebitoCreditoBancario", False, False, MensajeRetorno, IDTransaccionExtorno) = False Then
                    MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                End If


                'Prepara datos para guardar detalleImpuesto similar al de la ACREDITACION
                Dim dtimpuestos As DataTable
                dtimpuestos = CSistema.ExecuteToDataTable("Select IDTransaccion, IDImpuesto, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, RetencionIVA, RetencionRenta from DetalleImpuesto where IDTransaccion= " & oRow1("IDTransaccion").ToString).Copy

                For Each oRow As DataRow In dtimpuestos.Rows
                    Dim paramx(-1) As SqlClient.SqlParameter
                    CSistema.SetSQLParameter(paramx, "@IDTransaccion", IDTransaccionExtorno, ParameterDirection.Input)
                    CSistema.SetSQLParameter(paramx, "@IDImpuesto", oRow("IDImpuesto").ToString, ParameterDirection.Input)
                    CSistema.SetSQLParameter(paramx, "@Total", CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString, Decimales), ParameterDirection.Input)
                    CSistema.SetSQLParameter(paramx, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString, Decimales), ParameterDirection.Input)
                    CSistema.SetSQLParameter(paramx, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString, Decimales), ParameterDirection.Input)
                    CSistema.SetSQLParameter(paramx, "@TotalDescuento", CSistema.FormatoMonedaBaseDatos(oRow("TotalDescuento").ToString, Decimales), ParameterDirection.Input)

                    CSistema.SetSQLParameter(paramx, "@Operacion", "INS", ParameterDirection.Input)

                    'Informacion de Salida
                    CSistema.SetSQLParameter(paramx, "@Mensaje", "", ParameterDirection.Output, 200)
                    CSistema.SetSQLParameter(paramx, "@Procesado", "", ParameterDirection.Output)

                    'Insertar el detalle
                    Dim MensajeRetorno1 As String = ""

                    If CSistema.ExecuteStoreProcedure(paramx, "SpDetalleImpuesto", False, False, MensajeRetorno1) = False Then
                        MessageBox.Show(MensajeRetorno1, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                        'Return False
                    End If
                Next

                'Genera Asiento Cabecera del Extorno. Envia IDtransaccion de la Acreditacion y del Extorno, total correspondiente al Extorno en relacion a la moneda asociada
                'GenerarAsientoExtorno(oRow1("IDTransaccion").ToString, IDTransaccionExtorno, CSistema.FormatoMonedaBaseDatos(oRow1("Total").ToString, Decimales))
                GenerarAsientoExtorno(oRow1("IDTransaccion").ToString, IDTransaccionExtorno)

                'Genera Detalle Asiento Cabecera del Extorno. Envia IDtransaccion de la Acreditacion y del Extorno
                GenerarDetalleAsientoExtorno(oRow1("IDTransaccion").ToString, IDTransaccionExtorno)

                'Genera Registro del DebitoCredito por Acreditacion, por Extorno y la cobranza 
                InsertaCobranzaDebitoCreditoBancario(oRow1("IDTransaccion").ToString, IDTransaccionExtorno, IDTransaccionDeposito)

            End If

        Next

    End Sub

    Sub InsertaCobranzaDebitoCreditoBancario(ByVal IDTransaccion As Integer, ByVal IDTransaccionExtorno As Integer, ByVal IDTransaccionDeposito As Integer)

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDTransaccionCDB_Acreditacion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTransaccionCDB_Extorno", IDTransaccionExtorno, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTransaccionDeposito", IDTransaccionDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtfecha.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)
        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpDebitoCreditoBancarioDeposito", False, False, MensajeRetorno) = False Then
            MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            'Return False
        End If

    End Sub

    Sub GenerarAsientoExtorno(ByVal IDTransaccionAcreditacion As Integer, ByVal IDTransaccionExtorno As Integer)
        'Prepara para guardar Asiento 
        Dim Decimales As Boolean = False
        Dim dtAsiento As DataTable
        Dim Cotizacion As Integer = 1
        Dim Total As Decimal = 0

        dtAsiento = CSistema.ExecuteToDataTable("select IDSucursal, IDMoneda,Cotizacion,IDTipoComprobante,NroComprobante, Detalle,total,Debito,Credito,Saldo,Anulado,IDCentroCosto,Conciliado,FechaConciliado,IDUsuarioConciliado,Bloquear, Descripcion,IDUnidadNegocio from Asiento where IDTransaccion = " & IDTransaccionAcreditacion)

        Total = CSistema.ExecuteScalar("select total from vDebitoCreditoBancario where IDTransaccion = " & IDTransaccionAcreditacion)


        Dim param(-1) As SqlClient.SqlParameter
        If IDMonedaPago <> 1 Then
            Cotizacion = CotizacionDelDia
            Decimales = True
        End If

        Total = Total * Cotizacion
        For Each oRow As DataRow In dtAsiento.Rows

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccionExtorno, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDSucursal", oRow("IDSucursal").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtfecha.GetValue, True, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDMoneda", oRow("IDMoneda").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cotizacion", Cotizacion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTipoComprobante", oRow("IDTipoComprobante").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroComprobante", oRow("NroComprobante").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Detalle", "EXTORNO - " & oRow("Detalle").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Debito", CSistema.FormatoMonedaBaseDatos(Total, Decimales), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Credito", CSistema.FormatoMonedaBaseDatos(Total, Decimales), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Saldo", CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString, Decimales), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(Total, Decimales), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Bloquear", oRow("Bloquear").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDCentroCosto", oRow("IDCentroCosto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)

            Dim NroCaja As Integer
            'Caja
            If NroCaja = 0 Then
                Dim CCaja As New CCaja
                CCaja.IDSucursal = oRow("IDSucursal").ToString
                CCaja.ObtenerUltimoNumero()
                CSistema.SetSQLParameter(param, "@IDTransaccionCaja", CCaja.IDTransaccion, ParameterDirection.Input)
            Else
                Dim CCaja As New CCaja
                CCaja.IDSucursal = oRow("IDSucursal").ToString
                CCaja.Obtener(NroCaja)
                CSistema.SetSQLParameter(param, "@IDTransaccionCaja", CCaja.IDTransaccion, ParameterDirection.Input)
            End If

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpAsiento", False, False, MensajeRetorno) = False Then
                MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                'Return False
            End If
        Next

    End Sub

    Sub GenerarDetalleAsientoExtorno(ByVal IDTransaccionAcreditacion As Integer, ByVal IDTransaccionExtorno As Integer)
        'Prepara para guardar DetalleAsiento
        Dim Decimales As Boolean = False
        Dim Cotizacion As Integer = 1

        If IDMonedaPago <> 1 Then
            Cotizacion = CotizacionDelDia
            Decimales = True
        End If


        Dim dtAsiento As DataTable
        dtAsiento = CSistema.ExecuteToDataTable("select detalle,total,nrocomprobante from Asiento where IDTransaccion = " & IDTransaccionExtorno)

        Dim TotalAsiento As Decimal = 0.00
        Dim ObservacionAsiento As String
        Dim NroComprobanteAsiento As String

        For Each oRow As DataRow In dtAsiento.Rows

            TotalAsiento = CDec(oRow("Total").ToString) 'Ya se encuentra Guaranizado
            ObservacionAsiento = "EXTORNO - " & (oRow("Detalle").ToString)
            NroComprobanteAsiento = (oRow("NroComprobante").ToString)

            Dim dtDetalleAsiento As DataTable
            dtDetalleAsiento = CSistema.ExecuteToDataTable("select id, idcuentacontable,CuentaContable, Credito, Debito, Importe,Observacion,IDSucursal, TipoComprobante, NroComprobante, IDCentroCosto, IDUnidadNegocio,IDDepartamentoEmpresa from DetalleAsiento where IDTransaccion = " & IDTransaccionAcreditacion)

            'Variable para dirigir Detalle Debito (UNO=1) y Credito (UNO=0) en el Detalle de Asiento
            Dim UNO As Integer = 0

            For Each oRow1 As DataRow In dtDetalleAsiento.Rows
                Dim param(-1) As SqlClient.SqlParameter

                CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccionExtorno, ParameterDirection.Input)

                If UNO = 0 Then
                    CSistema.SetSQLParameter(param, "@IDCuentaContable", oRow1("IDCuentaContable").ToString, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@ID", oRow1("ID").ToString, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@Debito", 0, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@Credito", (CSistema.FormatoMonedaBaseDatos(TotalAsiento, Decimales)), ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(TotalAsiento, Decimales), ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@Observacion", "", ParameterDirection.Input)

                    CSistema.SetSQLParameter(param, "@IDSucursal", oRow1("IDSucursal").ToString, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@TipoComprobante", "---", ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@NroComprobante", NroComprobanteAsiento, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@IDUnidadNegocio", CSistema.RetornarValorInteger(oRow1("IDUnidadNegocio").ToString), ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@IDCentroCosto", CSistema.RetornarValorInteger(oRow1("IDCentroCosto").ToString), ParameterDirection.Input)

                    UNO = 1
                Else

                    CSistema.SetSQLParameter(param, "@IDCuentaContable", oRow1("IDCuentaContable").ToString, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@ID", oRow1("ID").ToString, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@Debito", CSistema.FormatoMonedaBaseDatos(TotalAsiento, Decimales), ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@Credito", 0, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(TotalAsiento, Decimales), ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@Observacion", ObservacionAsiento, ParameterDirection.Input)

                    CSistema.SetSQLParameter(param, "@IDSucursal", oRow1("IDSucursal").ToString, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@TipoComprobante", "DEB", ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@NroComprobante", NroComprobanteAsiento, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@IDUnidadNegocio", CSistema.RetornarValorInteger(oRow1("IDUnidadNegocio").ToString), ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@IDCentroCosto", CSistema.RetornarValorInteger(oRow1("IDCentroCosto").ToString), ParameterDirection.Input)

                End If

                'Informacion de Salida
                CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
                CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

                Dim MensajeRetorno As String = ""

                'Insertar Registro
                If CSistema.ExecuteStoreProcedure(param, "SpDetalleAsiento", False, False, MensajeRetorno) = False Then
                    MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    'Return False

                End If

            Next 'FIN del for de DetalleAsiento

        Next 'FIN del for de Asiento

    End Sub
    Sub ObtenerCuenta()

        If cbxCuenta.cbx.SelectedValue Is Nothing Then
            Exit Sub
        End If

        txtBanco.txt.Clear()
        txtMoneda.txt.Clear()

        'Totales y decimales
        Dim Decimales As Boolean = False

        For Each oRow As DataRow In dtCuentaBancaria.Select(" ID=" & cbxCuenta.cbx.SelectedValue)

            txtBanco.txt.Text = oRow("Banco").ToString
            txtMoneda.txt.Text = oRow("Mon").ToString
            Decimales = CSistema.RetornarValorBoolean(oRow("Decimales").ToString)
            Dim IDMoneda As Integer = CSistema.RetornarValorInteger(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("IDMoneda").ToString)
            'txtCambio.txt.Text = CSistema.CotizacionDelDia(IDMoneda, 1)
            txtCambio.txt.Text = CSistema.ExecuteScalar("Select isnull(max(Cotizacion),1) from Cotizacion where IDMoneda = " & IDMoneda & " and cast(fecha as date) ='" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
            If IDMoneda = 1 Then
                txtCambio.SoloLectura = True
            Else
                txtCambio.SoloLectura = False
            End If
            'Totales
            txtTotalChequeLocal.Decimales = Decimales
            txtTotalChequeOtro.Decimales = Decimales
            txtTotalDepositado.Decimales = Decimales
            txtTotalDocumento.Decimales = Decimales
            txtTotalEfectivo.Decimales = Decimales

        Next

    End Sub
    Sub CambiarMoneda()
        'Totales y decimales
        Dim Decimales As Boolean = False

        For Each oRow As DataRow In dtCuentaBancaria.Select(" ID=" & cbxCuenta.cbx.SelectedValue)

            txtBanco.txt.Text = oRow("Banco").ToString
            txtMoneda.txt.Text = oRow("Mon").ToString
            Decimales = CSistema.RetornarValorBoolean(oRow("Decimales").ToString)
            Dim IDMoneda As Integer = CSistema.RetornarValorInteger(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("IDMoneda").ToString)
            'txtCambio.txt.Text = CSistema.CotizacionDelDia(IDMoneda, 1)
            If txtfecha.txt.TextLength = 8 Then
                txtCambio.txt.Text = CSistema.ExecuteScalar("Select isnull(max(Cotizacion),1) from Cotizacion where IDMoneda = " & IDMoneda & " and cast(fecha as date) ='" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
            End If

            'Totales
            txtTotalChequeLocal.Decimales = Decimales
            txtTotalChequeOtro.Decimales = Decimales
            txtTotalDepositado.Decimales = Decimales
            txtTotalDocumento.Decimales = Decimales
            txtTotalEfectivo.Decimales = Decimales

        Next
    End Sub

    Sub CargarVenta(ByVal IDTransaccion As Integer)

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra las ventas asociadas!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        'Limpiar ventas seleccionadas
        ListarComprobantes()

    End Sub

    Sub ListarComprobantes()

        dgw.Rows.Clear()

        Dim Total As Decimal = 0
        Dim Decimales As Boolean = False
        Dim IDMoneda As Integer = CSistema.RetornarValorInteger(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("IDMoneda").ToString)
        Decimales = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & IDMoneda, "VMoneda")("Decimales").ToString)
        Dim DiferenciaCambio As Decimal = 0

        If CSistema.ExecuteScalar("select count(*) from Cotizacion where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtfecha.GetValue, True, False) & "' and IDMoneda = " & IDMoneda) = 0 And IDMoneda > 1 Then
            MessageBox.Show("No se ha fijado cotizacion para la moneda seleccionada.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
            Exit Sub
        End If

        For Each oRow As DataRow In dtEfectivo.Rows
            If oRow("Sel") = True Then

                Dim Registro(8) As String

                Registro(0) = oRow("IDTransaccion").ToString
                Registro(1) = oRow("CodigoComprobante").ToString
                Registro(2) = ("---")
                Registro(3) = oRow("Comprobante").ToString
                'Registro(4) = (CSistema.FormatoMoneda(oRow("Importe").ToString, oRow("Decimales").ToString))
                Registro(4) = (CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales))
                Registro(5) = CSistema.FormatoMoneda(oRow("Cotizacion").ToString, False)
                Registro(6) = ("---")
                Registro(7) = ("---")
                Registro(8) = oRow("ID").ToString

                Total = Total + CDec(oRow("Importe").ToString)
                DiferenciaCambio = DiferenciaCambio + ((Registro(4) * oRow("cotizacion").ToString) - (Registro(4) * (txtCambio.txt.Text)))

                dgw.Rows.Add(Registro)

            End If
        Next

        Total = 0

        For Each oRow As DataRow In dtCheque.Rows
            If oRow("Sel") = True Then

                Dim Registro(8) As String

                Registro(0) = oRow("IDTransaccion").ToString
                Registro(1) = oRow("CodigoTipo").ToString
                Registro(2) = oRow("Banco").ToString
                Registro(3) = oRow("NroCheque").ToString
                Registro(4) = CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales)
                Registro(5) = CSistema.FormatoMoneda(oRow("Cotizacion").ToString, False)
                Registro(6) = oRow("Cliente").ToString
                Registro(7) = oRow("CuentaBancaria").ToString
                Registro(8) = 0


                Total = Total + CDec(oRow("Importe").ToString)
                DiferenciaCambio = DiferenciaCambio + ((Registro(4) * oRow("cotizacion").ToString) - (Registro(4) * (txtCambio.txt.Text)))

                dgw.Rows.Add(Registro)

            End If
        Next

        For Each oRow As DataRow In dtTarjeta.Rows
            If oRow("Sel") = True Then

                Dim Registro(8) As String

                Registro(0) = oRow("IDTransaccion").ToString
                Registro(1) = oRow("CodigoComprobante").ToString
                Registro(2) = ("---")
                Registro(3) = oRow("Comprobante").ToString
                Registro(4) = CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales)
                Registro(5) = CSistema.FormatoMoneda(oRow("Cotizacion").ToString, False)
                Registro(6) = ("---")
                Registro(7) = ("---")
                Registro(8) = oRow("ID").ToString


                Total = Total + CDec(oRow("Importe").ToString)
                DiferenciaCambio = DiferenciaCambio + ((Registro(4) * oRow("cotizacion").ToString) - (Registro(4) * (txtCambio.txt.Text)))

                dgw.Rows.Add(Registro)

            End If
        Next

        For Each oRow As DataRow In dtDocumento.Rows
            If oRow("Sel") = True Then

                'SC: variable para controlar forma de Pago Documento
                vDocumento = True

                Dim Registro(8) As String

                Registro(0) = oRow("IDTransaccion").ToString
                Registro(1) = oRow("CodigoComprobante").ToString
                Registro(2) = ("---")
                Registro(3) = oRow("Comprobante").ToString
                Registro(4) = (CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales))
                Registro(5) = CSistema.FormatoMoneda(oRow("Cotizacion").ToString, False)
                Registro(6) = ("---")
                Registro(8) = ("---")
                Registro(7) = oRow("ID").ToString

                Total = Total + CDec(oRow("Importe").ToString)
                DiferenciaCambio = DiferenciaCambio + ((Registro(4) * oRow("cotizacion").ToString) - (Registro(4) * (txtCambio.txt.Text)))

                dgw.Rows.Add(Registro)

            End If
        Next

        For Each oRow As DataRow In dtRedondeo.Rows

            Dim Registro(8) As String

            Registro(0) = oRow("IDTransaccionDepositoBancario").ToString
            Registro(1) = "REDONDEO"
            Registro(2) = ("---")
            Registro(3) = ("---")
            Registro(4) = (CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales))
            Registro(5) = ("---")
            Registro(6) = ("---")
            Registro(7) = ("---")
            Registro(8) = oRow("ID").ToString

            Total = Total + CDec(oRow("Importe").ToString)

            dgw.Rows.Add(Registro)

        Next

        CalcularTotales()
        txtDiferenciaCambio.SetValue(CSistema.FormatoMoneda(DiferenciaCambio, 0))

        'Bloquear si se cargo algo
        If vNuevo = True Then
            If dgw.RowCount > 0 Then
                cbxCuenta.Enabled = False
            Else
                cbxCuenta.Enabled = True
            End If
        End If

        '--RecalcularAsiento()

    End Sub

    Sub Eliminar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        Dim IDTransaccion_Anular As Integer
        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnEliminar, mensaje)
            ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            'SC: 20-08-2021 Guardamos el IDTransaccion de la Cobranza a Anular para anular el Extorno.
            IDTransaccion_Anular = IDTransaccion
        End If
        'Datos

        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.DEL.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Eliminar
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpDepositoBancario", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnEliminar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        'SC: Llama a la rutina que elimina el Extorno asociado a la cobranza.
        AnulaExtorno(IDTransaccion_Anular)

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    'SC: Rutina que elimina el extorno
    Sub AnulaExtorno(ByVal IDtransaccionDeposito As Integer)

        Dim IDTransaccionExtorno As Integer = 0

        IDTransaccionExtorno = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccionCDB_Extorno From DebitoCreditoBancarioDeposito Where Estado = 1 and IDTransaccionDeposito = " & IDtransaccionDeposito & "), 0 )")

        If IDTransaccionExtorno <> 0 Then

            Dim paramx(-1) As SqlClient.SqlParameter

            'Datos
            CSistema.SetSQLParameter(paramx, "@IDTransaccion", IDTransaccionExtorno, ParameterDirection.Input)
            CSistema.SetSQLParameter(paramx, "@Operacion", CSistema.NUMOperacionesRegistro.DEL.ToString, ParameterDirection.Input)

            'Transaccion
            CSistema.SetSQLParameter(paramx, "@IDOperacion", IDOperacion, ParameterDirection.Input)
            CSistema.SetSQLParameter(paramx, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
            CSistema.SetSQLParameter(paramx, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
            CSistema.SetSQLParameter(paramx, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
            CSistema.SetSQLParameter(paramx, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(paramx, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(paramx, "@Procesado", "False", ParameterDirection.Output)
            CSistema.SetSQLParameter(paramx, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

            'Eliminar
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(paramx, "SpDebitoCreditoBancario", False, False, MensajeRetorno) = False Then
                'tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)

                Exit Sub

            End If

        Else

            Exit Sub
        End If


    End Sub


    Sub EliminarComprobante()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each item As DataGridViewRow In dgw.SelectedRows

            Dim IDTransaccion As String = item.Cells(0).Value
            Dim ID As String = item.Cells(7).Value

            'buscamos en Efectivo
            For Each oRow As DataRow In dtEfectivo.Select(" IDTransaccion = '" & IDTransaccion & "' And ID = '" & ID & "' ")

                oRow("Sel") = False
                Exit For

            Next

            'buscamos en Cheque
            For Each oRow As DataRow In dtCheque.Select(" IDTransaccion = " & IDTransaccion & "")


                oRow("Sel") = False
                Exit For


            Next

            'buscamos en Documento
            For Each oRow As DataRow In dtDocumento.Select(" IDTransaccion = " & IDTransaccion & " And ID = " & ID)

                oRow("Sel") = False
                Exit For


            Next

            'buscamos en Documento
            For Each oRow As DataRow In dtRedondeo.Select(" ID = " & ID)

                dtRedondeo.Rows.Remove(oRow)
                Exit For


            Next

        Next

        ListarComprobantes()

    End Sub

    Sub CalcularTotales()

        TxtCantidadDepositado.SetValue(dgw.Rows.Count)

        Dim IDMoneda As Integer
        Dim Decimales As Boolean = False
        Dim TotalEfectivo As Decimal
        Dim TotalCheques As Decimal
        Dim TotalTarjetas As Decimal
        Dim TotalChequeOtros As Decimal
        Dim TotalDocumento As Decimal
        Dim TotalFaltante As Decimal
        Dim TotalSobrante As Decimal
        Dim Total As Decimal

        TotalRestar = 0

        'Verificar si los importes son en decimales
        IDMoneda = CSistema.RetornarValorInteger(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("IDMoneda").ToString)
        Decimales = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & IDMoneda, "VMoneda")("Decimales").ToString)

        'Calcula Total Efectivo
        TotalEfectivo = CSistema.dtSumColumn(dtEfectivo, "Importe", "True", "Sel")
        txtTotalEfectivo.txt.Text = TotalEfectivo

        'Calcula Total Documento
        TotalDocumento = CSistema.dtSumColumn(dtDocumento, "Importe", "True", "Sel")
        txtTotalDocumento.txt.Text = TotalDocumento

        'Calcula Total Tarjeta
        TotalTarjetas = CSistema.dtSumColumn(dtTarjeta, "Importe", "True", "Sel")
        txtTotalTarjeta.txt.Text = TotalTarjetas

        'Total Banco Local
        dtCheque.DefaultView.RowFilter = "BancoLocal = 'True' And Sel = 'True'"
        TotalCheques = CSistema.TotalesDataTable(dtCheque.DefaultView, "Importe", True, Decimales)
        txtTotalChequeLocal.txt.Text = TotalCheques

        'Otros Bancos
        dtCheque.DefaultView.RowFilter = "BancoLocal = 'False' And Sel = 'True'"
        TotalChequeOtros = CSistema.TotalesDataTable(dtCheque.DefaultView, "Importe", True, Decimales)
        txtTotalChequeOtro.txt.Text = TotalChequeOtros

        'Redondeo Faltante
        TotalFaltante = CSistema.dtSumColumn(dtRedondeo, "Importe", "True", "Faltante")

        'Redondeo Sobrante
        TotalSobrante = CSistema.dtSumColumn(dtRedondeo, "Importe", "True", "Sobrante")

        If dtDocumento.Rows.Count <> 0 Then
            For Each oRow As DataRow In dtDocumento.Select("Sel='True' And Restar='True'")
                TotalRestar = TotalRestar + CDec(oRow("Importe").ToString)
            Next
        End If

        'Calcula Total Depositado
        Total = TotalEfectivo + TotalCheques + TotalTarjetas + TotalDocumento + TotalChequeOtros - TotalRestar

        'Sumar
        Total = Total + TotalFaltante

        'Restar
        Total = Total - TotalSobrante

        'Total = Total

        Total = Total - TotalRestar

        'Redondeo
        txtRedondeo.SetValue(0)
        If TotalFaltante > 0 Then
            txtRedondeo.SetValue(TotalFaltante)
        End If

        If TotalSobrante > 0 Then
            txtRedondeo.SetValue(TotalSobrante * -1)
        End If

        txtTotalDepositado.txt.Text = Total

    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "DEPOSITO BANCARIO " & txtID.ObtenerValor

            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From DepositoBancario Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else
            MessageBox.Show("Debe Guardar la Operacion para visualizar el asiento", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            'Dim frm As New frmAsiento
            'frm.WindowState = FormWindowState.Normal
            'frm.StartPosition = FormStartPosition.CenterScreen
            'frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            'frm.Text = "DEPOSITO BANCARIO " & txtID.ObtenerValor

            'GenerarAsiento()

            'frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            'CAsiento.ListarDetalle(frm.dgv)
            'frm.CalcularTotales()

            'frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento
            'frm.CAsiento.NoAgrupar = False

            ''Mostramos
            'frm.ShowDialog(Me)

            ''Actualizamos el asiento si es que este tuvo alguna modificacion
            'CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            'CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            'CAsiento.Bloquear = frm.CAsiento.Bloquear
            'CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            'CAsiento.NroCaja = frm.CAsiento.NroCaja
            'CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            'If frm.VolverAGenerar = True Then
            '    CAsiento.Generado = False
            '    CAsiento.dtAsiento.Clear()
            '    CAsiento.dtDetalleAsiento.Clear()
            '    VisualizarAsiento()
            'End If

        End If


    End Sub

    Sub GenerarAsiento()

        'Establecer Cabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDDeposito") = 1
        oRow("IDSucursal") = cbxSucursal.GetValue
        oRow("IDCiudad") = cbxCiudad.GetValue
        oRow("Fecha") = txtfecha.GetValue
        oRow("IDMoneda") = CSistema.RetornarValorInteger(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("IDMoneda").ToString)
        oRow("Cotizacion") = txtCambio.ObtenerValor
        oRow("IDTipoComprobante") = cbxTipoComprobante.GetValue
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = txtComprobante.GetValue
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.GetValue
        oRow("Detalle") = txtObservacion.txt.Text
        oRow("Total") = txtTotalDepositado.ObtenerValor

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)

        CAsiento.dtEfectivo = dtEfectivo
        CAsiento.dtCheque = dtCheque
        CAsiento.IDCuentaBancaria = cbxCuenta.GetValue
        CAsiento.Total = txtTotalDepositado.ObtenerValor

        CAsiento.IDSucursal = cbxSucursal.GetValue
        CAsiento.Cotizacion = txtCambio.ObtenerValor
        CAsiento.NoAgrupar = True

        CAsiento.TotalRestar = TotalRestar
        'Totales
        Dim IDMoneda As Integer = CSistema.RetornarValorInteger(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("IDMoneda").ToString)
        Dim Decimales As Boolean = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & IDMoneda, "VMoneda")("Decimales").ToString)
        CAsiento.TotalDiferenciaCambio = CSistema.FormatoMoneda(txtDiferenciaCambio.txt.Text, 0)
        CAsiento.TotalDocumentos = CSistema.FormatoMoneda(txtTotalDocumento.txt.Text, Decimales)
        CAsiento.Generar()

    End Sub

    Sub Imprimir()

    End Sub

    Sub Buscar()

        Dim frm As New frmConsultaDepositoBancario
        frm.Text = "Consulta de Depósitos Bancarios"
        frm.ShowDialog()
        CargarOperacion(frm.IDTransaccion)

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From vDepositoBancario Where Numero=" & txtID.ObtenerValor & "And IDSucursal = " & cbxSucursal.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDepositoBancario = CSistema.ExecuteToDataTable("Select * From VDepositoBancario Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dtDepositoBancario Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dtDepositoBancario.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dtDepositoBancario.Rows(0)

        cbxCiudad.cbx.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtfecha.SetValueFromString(oRow("Fecha").ToString)
        cbxCuenta.SelectedValue(oRow("IDCuentaBancaria").ToString)
        'ObtenerCuenta()
        txtCambio.txt.Text = oRow("Cotizacion")
        txtObservacion.txt.Text = oRow("Observacion").ToString

        'Cargamos el detalle de Comprobantes
        dtEfectivo = CSistema.ExecuteToDataTable("Select * From VDetalleDepositoBancarioEfectivo Where IDTransaccionDepositoBancario = " & IDTransaccion).Copy
        dtCheque = CSistema.ExecuteToDataTable("Select * From VDetalleDepositoBancarioCheque Where IDTransaccionDepositoBancario = " & IDTransaccion).Copy
        dtTarjeta = CSistema.ExecuteToDataTable("Select * From VDetalleDepositoBancarioTarjeta Where IDTransaccionDepositoBancario = " & IDTransaccion).Copy
        dtDocumento = CSistema.ExecuteToDataTable("Select * From VDetalleDepositoBancarioDocumento Where IDTransaccionDepositoBancario = " & IDTransaccion & " And Deposito = 'True' ").Copy
        dtRedondeo = CSistema.ExecuteToDataTable("Select * From VDetalleDepositoBancario Where IDTransaccionDepositoBancario = " & IDTransaccion & " And Redondeo = 'True' ").Copy
        ListarComprobantes()

        'Calcular Totales
        CalcularTotales()


        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        'Inicializamos el Asiento
        CAsiento.Limpiar()

    End Sub

    Sub CargarEfectivo()

        Dim frm As New frmDepositoBancarioSeleccionarEfectivo
        frm.Text = "Selección de Efectivo"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtEfectivo
        frm.IDMoneda = CSistema.RetornarValorInteger(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("IDMoneda").ToString)
        frm.Decimales = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("Decimales").ToString)
        frm.Moneda = txtMoneda.GetValue

        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccionar Efectivo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
        dtEfectivo = frm.dt

        ListarComprobantes()

    End Sub

    Sub CargarCheque()

        Dim frm As New frmDepositoBancarioSeleccionarCheque
        frm.Text = "Selección de Cheque"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtCheque
        frm.IDMoneda = CSistema.RetornarValorInteger(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("IDMoneda").ToString)
        frm.Decimales = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("Decimales").ToString)
        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccionar Cheque", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
        dtCheque = frm.dt

        ListarComprobantes()

    End Sub

    Sub CargarTarjeta()
        Dim frm As New frmDepositoBancarioSeleccionarTarjeta
        frm.Text = "Selección de Tarjeta"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtTarjeta
        frm.IDMoneda = CSistema.RetornarValorInteger(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("IDMoneda").ToString)
        frm.Decimales = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("Decimales").ToString)
        frm.Moneda = txtMoneda.GetValue
        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccionar tarjeta", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
        dtTarjeta = frm.dt

        ListarComprobantes()
    End Sub

    Sub CargarDocumento()
        Dim frm As New frmDepositoBancarioSeleccionarDocumento
        frm.Text = "Selección de Documento"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.IDMoneda = CSistema.RetornarValorInteger(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("IDMoneda").ToString)
        frm.Decimales = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("Decimales").ToString)
        frm.dt = dtDocumento
        frm.Inicializar()
        frm.ShowDialog(Me)
        dtDocumento = frm.dt

        ListarComprobantes()

    End Sub

    Sub CargarRedondeo()

        Dim frm As New frmDepositoBancarioSeleccionarRedondeo
        frm.Text = "Redondear"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtRedondeo
        frm.Total = txtTotalDepositado.ObtenerValor
        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Redondear", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
        dtRedondeo = frm.dt

        ListarComprobantes()

    End Sub

    Sub AplicarOrdenCompra()

    End Sub

    Sub ObtenerSucursal()

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VDepositoBancario Where IDSucursal=" & cbxSucursal.GetValue & " "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VDepositoBancario Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalle = True

        Dim id As Integer = 0
        Dim Decimales As Boolean = False
        Dim IDMoneda As Integer = CSistema.RetornarValorInteger(CData.GetRow("ID=" & cbxCuenta.GetValue, "VCuentaBancaria")("IDMoneda").ToString)
        Decimales = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & IDMoneda, "VMoneda")("Decimales").ToString)

        'Insertar si hay efectivo
        For Each oRow As DataRow In dtEfectivo.Select(" Sel = 'True' ")


            Dim sql As String = "Insert Into DetalleDepositoBancario(IDTransaccionDepositoBancario, ID, IDTransaccionEfectivo, IDEfectivo,Importe,IDDetalleDocumento) " &
                                " Values(" & IDTransaccion & ", " & id & ", " & oRow("IDTransaccion").ToString & "," & oRow("ID").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & ",0" & ")"
            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            id = id + 1

        Next

        'Insertar si hay cheques
        For Each oRow As DataRow In dtCheque.Select(" Sel = 'True' ")

            Dim sql As String = "Insert Into DetalleDepositoBancario(IDTransaccionDepositoBancario, ID, IDTransaccionCheque, Importe, IDDetalleDocumento) Values(" & IDTransaccion & ", " & id & " , " & oRow("IDTransaccion").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & ",0" & ")"
            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            id = id + 1

        Next

        'Insertar si hay documentos
        For Each oRow As DataRow In dtDocumento.Select(" Sel = 'True' ")

            Dim sql As String = "Insert Into DetalleDepositoBancario(IDTransaccionDepositoBancario, ID, IDTransaccionDocumento, Importe,IDDetalleDocumento) Values(" & IDTransaccion & ", " & id & " , " & oRow("IDTransaccion").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & "," & oRow("ID").ToString & ")"
            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            id = id + 1

        Next

        'Insertar si hay tarjetas
        For Each oRow As DataRow In dtTarjeta.Select(" Sel = 'True' ")

            Dim sql As String = "Insert Into DetalleDepositoBancario(IDTransaccionDepositoBancario, ID, IDTransaccionTarjeta, Importe,IDDetalleDocumento) Values(" & IDTransaccion & ", " & id & " , " & oRow("IDTransaccion").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & "," & oRow("ID").ToString & ")"
            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            id = id + 1

        Next

        'Insertar si hay Redondeos
        For Each oRow As DataRow In dtRedondeo.Rows

            Dim sql As String = "Insert Into DetalleDepositoBancario(IDTransaccionDepositoBancario, ID, Redondeo, Faltante, Sobrante, Importe,IDDetalleDocumento) Values(" & IDTransaccion & ", " & id & " , 'True', '" & oRow("Faltante").ToString & "', '" & oRow("Sobrante").ToString & "', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, Decimales) & ",0" & ")"
            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If

            id = id + 1

        Next

    End Function

    Sub ModificarDepositoBancario()
        Dim frm As New frmModificarDepositoBancario
        frm.Text = "Modificar Depósito Bancario"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen

        frm.IDTransaccion = IDTransaccion
        frm.dt = dtDepositoBancario

        frm.ShowDialog(Me)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub frmDepositoBancario_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
        'FA 20/06/2023
        LiberarMemoria()
    End Sub

    Private Sub frmDepositoBancario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged
        ObtenerSucursal()
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub txtFecha_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtfecha.TeclaPrecionada
        'OcxFormaPago1.Fecha = txtFecha.GetValue
        CambiarMoneda()
    End Sub

    Private Sub cbxCuenta_Leave(sender As Object, e As System.EventArgs) Handles cbxCuenta.Leave
        ObtenerCuenta()
    End Sub

    'Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
    '    Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    'End Sub

    'Private Sub OcxFormaPago1_ImporteModificado(ByVal sender As Object, ByVal e As System.EventArgs) Handles OcxFormaPago1.ImporteModificado
    '    CalcularTotales()
    'End Sub

    'Private Sub OcxFormaPago1_ListarCheques(ByVal sender As Object, ByVal e As System.EventArgs) Handles OcxFormaPago1.ListarCheques

    '    'Cheques y Forma de Pago
    '    OcxFormaPago1.Reset()

    'End Sub

    'Private Sub OcxFormaPago1_RegistroEliminado(ByVal sender As Object, ByVal e As System.EventArgs, ByVal oRow As System.Data.DataRow, ByVal Tipo As ocxFormaPago.ENUMFormaPago) Handles OcxFormaPago1.RegistroEliminado

    '    Select Case Tipo
    '        Case ocxFormaPago.ENUMFormaPago.Efectivo
    '            CAsientoContableCobranzaCredito.EliminarEfectivo(oRow("Importe").ToString, oRow("IDTipoComprobante").ToString, oRow("IDMoneda").ToString)
    '        Case ocxFormaPago.ENUMFormaPago.Cheque
    '            CAsientoContableCobranzaCredito.EliminarCheque(oRow("Importe").ToString, oRow("Diferido").ToString, oRow("IDMoneda").ToString)
    '        Case ocxFormaPago.ENUMFormaPago.ChequeTercero
    '            CAsientoContableCobranzaCredito.EliminarCheque(oRow("Importe").ToString, oRow("Diferido").ToString, oRow("IDMoneda").ToString)
    '        Case ocxFormaPago.ENUMFormaPago.Documento
    '            CAsientoContableCobranzaCredito.EliminarDocumento(oRow("Importe").ToString, oRow("IDTipoComprobante").ToString, oRow("IDMoneda").ToString)
    '    End Select

    '    CalcularTotales()

    'End Sub

    'Private Sub OcxFormaPago1_RegistroInsertado(ByVal sender As Object, ByVal e As System.EventArgs, ByVal oRow As System.Data.DataRow, ByVal Tipo As ocxFormaPago.ENUMFormaPago) Handles OcxFormaPago1.RegistroInsertado
    '    Select Case Tipo
    '        Case ocxFormaPago.ENUMFormaPago.Efectivo
    '            CAsientoContableCobranzaCredito.InsertarEfectivo(oRow("Importe").ToString, oRow("IDTipoComprobante").ToString, oRow("IDMoneda").ToString)
    '        Case ocxFormaPago.ENUMFormaPago.Cheque
    '            CAsientoContableCobranzaCredito.InsertarCheque(oRow("Importe").ToString, oRow("Diferido").ToString, oRow("IDMoneda").ToString)
    '        Case ocxFormaPago.ENUMFormaPago.ChequeTercero
    '            CAsientoContableCobranzaCredito.InsertarCheque(oRow("Importe").ToString, oRow("Diferido").ToString, oRow("IDMoneda").ToString)
    '        Case ocxFormaPago.ENUMFormaPago.Documento
    '            CAsientoContableCobranzaCredito.InsertarDocumento(oRow("Importe").ToString, oRow("IDTipoComprobante").ToString, oRow("IDMoneda").ToString)
    '    End Select
    'End Sub

    Private Sub cbxCuenta_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCuenta.PropertyChanged
        ObtenerCuenta()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Eliminar(ERP.CSistema.NUMOperacionesRegistro.DEL)
    End Sub

    Private Sub btnAgregarEfectivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarEfectivo.Click
        CargarEfectivo()
    End Sub

    Private Sub lklEliminarVenta_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarVenta.LinkClicked
        EliminarComprobante()
    End Sub

    Private Sub btnAgregarCheques_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarCheques.Click
        CargarCheque()
    End Sub

    Private Sub lvLista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub frmDepositoBancario_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub


    Private Sub btnDocumentos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarDocumentos.Click
        CargarDocumento()
    End Sub

    Private Sub btnRedondear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRedondear.Click
        CargarRedondeo()
    End Sub

    Private Sub cbxCiudad_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxCiudad.TeclaPrecionada
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        ModificarDepositoBancario()
    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown
        If e.KeyCode = Keys.Delete Then
            EliminarComprobante()
        End If
    End Sub

    Private Sub cbxCuenta_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxCuenta.TeclaPrecionada
        ObtenerCuenta()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles btnAgregarTarjetas.Click
        CargarTarjeta()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmDepositoBancario_Activate()
        Me.Refresh()
    End Sub
End Class
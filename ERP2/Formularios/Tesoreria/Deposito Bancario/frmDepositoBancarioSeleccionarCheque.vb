﻿Public Class frmDepositoBancarioSeleccionarCheque

    'CLASES
    Dim CSistema As New CSistema

    'EVENTOS
    Public Event VentasSeleccionadas(ByVal sender As Object, ByVal e As EventArgs)

    'PROPIEDADES

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private DecimalesValue As Boolean
    Public Property Decimales() As Boolean
        Get
            Return DecimalesValue
        End Get
        Set(ByVal value As Boolean)
            DecimalesValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property
    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Txt
        txtDesde.SetValueFromString("01" & "/" & Date.Now.Month & "/" & Date.Now.Year)
        txtHasta.SetValue(Date.Now)

        CSistema.SqlToComboBox(cbxBanco, "Select ID, Referencia From VBanco Order By 2")
        cbxTipo.cbx.Items.Add("CHQ")
        cbxTipo.cbx.Items.Add("DIF")

        'Funciones
        Listar(False, True)
        'Cargar()

        'Foco
        dgw.Focus()

    End Sub

    Sub Listar(ByVal PorCheque As Boolean, Optional ByVal Todos As Boolean = False)

        'Limpiar la grilla
        dgw.Rows.Clear()
        Dim TotalDeuda As Decimal = 0

        Dim Where As String = ""

        If PorCheque = True Then
            Where = "NroCheque Like '%" & txtComprobante.GetValue & "%'"
            Where = Where & " and IDMoneda = " & IDMoneda
        Else
            If Todos = False Then
                'Where = " (Fecha >= '" & txtDesde.GetValueString & "' And Fecha <= '" & txtHasta.GetValueString & "') "
                Where = " (Fecha >= '" & txtDesde.txt.Text & "' And Fecha <= '" & txtHasta.txt.Text & "') "

                'Tipo
                If chkTipo.Valor = True Then
                    Where = Where & " And CodigoTipo='" & cbxTipo.cbx.Text & "' "
                End If

                'Banco
                If chkBanco.Valor = True Then
                    Where = Where & " And IDBanco=" & cbxBanco.GetValue & " "
                End If

            Else
                Where = "NroCheque Like '%%'"
            End If
            Where = Where & " and IDMoneda = " & IDMoneda
        End If
        

        For Each oRow As DataRow In dt.Select(Where)

            Dim oRow1(9) As String

            oRow1(0) = oRow("IDTransaccion").ToString
            oRow1(1) = CBool(oRow("Sel").ToString)
            oRow1(2) = oRow("Banco").ToString
            oRow1(3) = oRow("NroCheque").ToString
            oRow1(4) = oRow("CodigoTipo").ToString
            oRow1(5) = oRow("Cliente").ToString
            oRow1(6) = oRow("Fecha").ToString
            oRow1(7) = oRow("FechaVencimiento").ToString
            oRow1(8) = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
            oRow1(9) = CBool(oRow("Cancelar").ToString)

            'Sumar el total de la deuda
            TotalDeuda = TotalDeuda + CDec(oRow("Saldo").ToString)

            dgw.Rows.Add(oRow1)

        Next

        CalcularTotales()


    End Sub

    Sub Seleccionar()

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells("colIDTransaccion").Value

            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Sel") = oRow.Cells("colSeleccionar").Value
                oRow1("Total") = oRow.Cells("colTotal").Value
            Next

        Next

        '
        Me.Close()

    End Sub

    Sub Cargar()

        For Each oRow As DataRow In dt.Rows

            Dim oRow1(8) As String

            oRow1(0) = "0"
            oRow1(1) = CBool(oRow("Sel").ToString)
            oRow1(2) = oRow("NroCheque").ToString
            oRow1(3) = oRow("CodigoTipo").ToString
            oRow1(4) = oRow("Fec").ToString
            oRow1(5) = CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales)
            oRow1(6) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)
            oRow1(7) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)
            oRow1(8) = CBool(oRow("Cancelar").ToString)

            dgw.Rows.Add(oRow1)

        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim TotalComprobantes As Decimal = 0
        Dim TotalSeleccionado As Decimal = 0
        Dim CantidadComprobantes As Integer = 0
        Dim CantidadSeleccionado As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows
            If IsNumeric(oRow.Cells("colTotal").Value) = True Then
                If oRow.Cells("colSeleccionar").Value = True Then
                    TotalSeleccionado = TotalSeleccionado + oRow.Cells("colTotal").Value
                    CantidadSeleccionado = CantidadSeleccionado + 1
                End If

                TotalComprobantes = TotalComprobantes + oRow.Cells("colTotal").Value
                CantidadComprobantes = CantidadComprobantes + 1
            End If
        Next

        txtTotalComprobantes.SetValue(TotalComprobantes)
        txtCantidadComprobantes.SetValue(CantidadComprobantes)

        txtTotalSelecionado.SetValue(TotalSeleccionado)
        txtCantidadSeleccionado.SetValue(CantidadSeleccionado)

    End Sub

    Sub PintarCelda()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(1).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next



        If dgw.CurrentRow Is Nothing Then
            Exit Sub
        End If

        dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick
        ctrError.Clear()
        tsslEstado.Text = ""

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

        If e.ColumnIndex = dgw.Columns.Item("colSeleccionar").Index Then

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSeleccionar").Value = False Then
                        oRow.Cells("colSeleccionar").Value = True
                        oRow.Cells("colSeleccionar").ReadOnly = False
                        oRow.Cells("colTotal").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("colTotal")
                    Else
                        oRow.Cells("colSeleccionar").ReadOnly = True
                        oRow.Cells("colTotal").ReadOnly = True
                        oRow.Cells("colSeleccionar").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

        End If
    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Columns(e.ColumnIndex).Name = "colImporte" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value

            Else
                dgw.CurrentCell.Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells("colImporte").Value)
            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Space Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSeleccionar").Value = False Then
                        oRow.Cells("colSeleccionar").ReadOnly = False
                        oRow.Cells("colSeleccionar").Value = True
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("colTotal")
                    Else
                        oRow.Cells("colSeleccionar").ReadOnly = False
                        oRow.Cells("colSeleccionar").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            If e.KeyCode = Keys.Enter Then
                If RowIndex < dgw.Rows.Count - 1 Then
                    dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colTotal")
                End If
            End If
            

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If
    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Index = e.RowIndex Then
                If oRow.Cells("colSeleccionar").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                End If

                oRow.DefaultCellStyle.Font = f1

                oRow.Cells("colTotal").Selected = True

            Else

                oRow.DefaultCellStyle.Font = f2

                If oRow.Cells("colSeleccionar").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.White
                Else
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                End If
            End If
        Next
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells("colTotal")
        End If

    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = True
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = False
        Next

        PintarCelda()
        CalcularTotales()
    End Sub

    Private Sub TableLayoutPanel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles TableLayoutPanel1.Paint

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar(False, False)
    End Sub

    Private Sub frmDepositoBancarioSeleccionarCheque_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If dgw.Focused = True Then
            Exit Sub
        End If

        If txtComprobante.Focused = True Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub txtHasta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtHasta.Load

    End Sub

    Private Sub txtComprobante_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            Listar(True)
        End If
    End Sub

    Private Sub chkTipo_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipo.PropertyChanged
        cbxTipo.Enabled = chkTipo.Valor
    End Sub

    Private Sub chkBanco_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkBanco.PropertyChanged
        cbxBanco.Enabled = chkBanco.Valor
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmDepositoBancarioSeleccionarCheque_Activate()
        Me.Refresh()
    End Sub

End Class
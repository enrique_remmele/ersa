﻿Public Class frmDepositoBancarioSeleccionarTarjeta

    'CLASES
    Dim CSistema As New CSistema
    Dim Importe As Decimal

    'EVENTOS
    Public Event VentasSeleccionadas(ByVal sender As Object, ByVal e As EventArgs)

    'PROPIEDADES

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Public Property IDMoneda As Integer
    Public Property Moneda As String
    Public Property Decimales As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Txt
        'txtDesde.SetValueFromString("01" & "/" & Date.Now.Month & "/" & Date.Now.Year) <- es lento asi
        txtDesde.SetValue(Date.Now)
        txtHasta.SetValue(Date.Now)
        txtMoneda.SetValue(Moneda)

        'Funciones
        Listar()
        'Cargar()

        'Si la moneda tiene decimales
        If Decimales = True Then
            CSistema.DataGridColumnasNumericas(dgw, {"colTotal", "colSaldo", "colImporte"}, Decimales)
        End If

        'Foco
        System.Threading.Thread.Sleep(1000)

    End Sub

    Sub Listar()

        'Limpiar la grilla
        dgw.Rows.Clear()
        Dim TotalDeuda As Decimal = 0
        Dim Filtro As String = ""

        'Solo de la moneda de la cuenta
        Filtro = " IDMoneda = " & IDMoneda & " And (Fecha >= '" & txtDesde.GetValue & "' And Fecha <= '" & txtHasta.GetValue & "') "

        'Comprobante
        If txtComprobante.GetValue.Trim.Length > 0 Then
            Filtro = Filtro & " And Comprobante Like '%" & txtComprobante.GetValue & "%' "
        End If

        For Each oRow As DataRow In dt.Select(Filtro)
            Dim oRow1(10) As String

            oRow1(0) = oRow("IDTransaccion").ToString
            oRow1(1) = CBool(oRow("Sel").ToString)
            oRow1(2) = oRow("Comprobante").ToString
            oRow1(3) = oRow("CodigoComprobante").ToString
            oRow1(4) = oRow("Fec").ToString
            oRow1(5) = CSistema.FormatoMoneda(oRow("ImporteMoneda").ToString, Decimales)
            oRow1(6) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)
            oRow1(7) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)
            oRow1(8) = CBool(oRow("Cancelar").ToString)
            oRow1(9) = oRow("ID").ToString

            'Sumar el total de la deuda
            'TotalDeuda = TotalDeuda + CDec(oRow("Saldo").ToString)

            dgw.Rows.Add(oRow1)

        Next

        CalcularTotales()

        dgw.Focus()

    End Sub

    Sub Seleccionar()

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells("colIDTransaccion").Value
            Dim ID As Integer = oRow.Cells("colID").Value

            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & IDTransaccion & " and ID = " & ID)
                oRow1("ID") = oRow.Cells("colID").Value
                oRow1("Sel") = oRow.Cells("colSeleccionar").Value
                oRow1("Importe") = oRow.Cells("colImporte").Value
                oRow1("Cancelar") = oRow.Cells("colCancelar").Value
            Next

        Next

        Me.Close()

    End Sub

    Sub Cargar()

        For Each oRow As DataRow In dt.Rows

            Dim oRow1(10) As String

            oRow1(0) = oRow("IDTransaccion").ToString
            oRow1(1) = CBool(oRow("Sel").ToString)
            oRow1(2) = oRow("Numero").ToString
            oRow1(3) = oRow("CodigoComprobante").ToString
            oRow1(4) = oRow("Fec").ToString
            oRow1(5) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            oRow1(6) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            oRow1(7) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            oRow1(8) = CBool(oRow("Cancelar").ToString)
            oRow1(9) = oRow("ID").ToString
            dgw.Rows.Add(oRow1)

        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim TotalComprobantes As Decimal = 0
        Dim TotalSeleccionado As Decimal = 0
        Dim CantidadComprobantes As Integer = 0
        Dim CantidadSeleccionado As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows

            If oRow.Cells("colSeleccionar").Value = True Then
                TotalSeleccionado = TotalSeleccionado + oRow.Cells("colImporte").Value
                CantidadSeleccionado = CantidadSeleccionado + 1
            End If

            TotalComprobantes = TotalComprobantes + oRow.Cells("colSaldo").Value
            CantidadComprobantes = CantidadComprobantes + 1
        Next

        txtTotalComprobantes.SetValue(TotalComprobantes)
        txtCantidadComprobantes.SetValue(CantidadComprobantes)

        txtTotalSelecionado.SetValue(TotalSeleccionado)
        txtCantidadSeleccionado.SetValue(CantidadSeleccionado)

    End Sub

    Sub PintarCelda()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(1).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next



        If dgw.CurrentRow Is Nothing Then
            Exit Sub
        End If

        dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick
        ctrError.Clear()
        tsslEstado.Text = ""

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

        If e.ColumnIndex = dgw.Columns.Item("colSeleccionar").Index Then

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSeleccionar").Value = False Then
                        oRow.Cells("colSeleccionar").Value = True
                        oRow.Cells("colSeleccionar").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = False
                        oRow.Cells("colCancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        If txtImporte.ObtenerValor > 0 Then

                            Importe = txtImporte.ObtenerValor
                            Dim TotalSeleccionado As Decimal = CSistema.gridSumColumn(dgw, "colSaldo", "colSeleccionar", "True")
                            Dim TotalActual As Decimal = oRow.Cells("colSaldo").Value
                            Dim Resultante As Decimal = 0
                            If Importe > TotalSeleccionado Then
                                oRow.Cells("colImporte").Value = TotalActual
                            Else
                                oRow.Cells("colImporte").Value = Importe - (TotalSeleccionado - TotalActual)
                            End If

                            oRow.Cells("colImporte").Value = CSistema.FormatoMoneda(oRow.Cells("colImporte").Value)
                            oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        End If



                    Else
                        oRow.Cells("colSeleccionar").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colSeleccionar").Value = False
                        oRow.Cells("colImporte").Value = CSistema.FormatoMoneda(oRow.Cells("colSaldo").Value)
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

        End If

    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Columns(e.ColumnIndex).Name = "colImporte" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value

            Else

                'No se puede superar el saldo
                If CDec(dgw.CurrentRow.Cells("colImporte").Value.ToString.Replace(".", "")) > CDec(dgw.CurrentRow.Cells("colSaldo").Value.ToString.Replace(".", "")) Then
                    MessageBox.Show("El importe no puede ser mayor a el saldo!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value
                    Exit Sub
                End If

                dgw.CurrentCell.Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells("colImporte").Value)

            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Space Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSeleccionar").Value = False Then
                        oRow.Cells("colSeleccionar").Value = True
                        oRow.Cells("colSeleccionar").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = False
                        oRow.Cells("colCancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        If txtImporte.ObtenerValor > 0 Then

                            Importe = txtImporte.ObtenerValor
                            Dim TotalSeleccionado As Decimal = CSistema.gridSumColumn(dgw, "colSaldo", "colSeleccionar", "True")
                            Dim TotalActual As Decimal = oRow.Cells("colSaldo").Value
                            Dim Resultante As Decimal = 0
                            If Importe > TotalSeleccionado Then
                                oRow.Cells("colImporte").Value = TotalActual
                            Else
                                oRow.Cells("colImporte").Value = Importe - (TotalSeleccionado - TotalActual)
                            End If

                            oRow.Cells("colImporte").Value = CSistema.FormatoMoneda(oRow.Cells("colImporte").Value)
                            oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        End If


                    Else
                        oRow.Cells("colSeleccionar").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colSeleccionar").Value = False
                        oRow.Cells("colImporte").Value = CSistema.FormatoMoneda(oRow.Cells("colSaldo").Value)
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

                    End If

                    Exit For

                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colImporte")
            End If

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If
    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Index = e.RowIndex Then
                If oRow.Cells("colSeleccionar").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                End If

                oRow.DefaultCellStyle.Font = f1

                oRow.Cells("colImporte").Selected = True

            Else

                oRow.DefaultCellStyle.Font = f2

                If oRow.Cells("colSeleccionar").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.White
                Else
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                End If
            End If
        Next
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells("colImporte")
        End If

    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = True
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = False
        Next

        PintarCelda()
        CalcularTotales()
    End Sub

    Private Sub TableLayoutPanel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles TableLayoutPanel1.Paint

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub FlowLayoutPanel2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles FlowLayoutPanel2.Paint

    End Sub

    Private Sub frmDepositoBancarioSeleccionarEfectivo_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If dgw.Focused = True Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub lklSeleccionarTodo_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklSeleccionarTodo.LinkClicked

        If lklSeleccionarTodo.Text = "Seleccionar todo" Then
            For Each oRow As DataGridViewRow In dgw.Rows
                oRow.Cells("ColSeleccionar").Value = True
            Next
            lklSeleccionarTodo.Text = "Quitar todo"
        Else
            For Each oRow As DataGridViewRow In dgw.Rows
                oRow.Cells("ColSeleccionar").Value = False
            Next
            lklSeleccionarTodo.Text = "Seleccionar todo"
        End If

        CalcularTotales()

    End Sub

    Private Sub StatusStrip1_ItemClicked(sender As System.Object, e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles StatusStrip1.ItemClicked

    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmDepositoBancarioSeleccionarTarjeta_Activate()
        Me.Refresh()
    End Sub
End Class
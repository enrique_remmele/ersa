﻿Public Class frmConsultaDepositoBancario

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim dtOperacion As DataTable
    Dim dtComprobante As DataTable

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim IDOperacion As Integer
    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'Form

        'TextBox
        txtCantidadCobranza.txt.ResetText()
        txtCantidadComprobantes.txt.ResetText()
        txtNroComprobante.txt.ResetText()
        txtOperacion.txt.ResetText()
        txtTotalCobranza.txt.ResetText()

        'CheckBox
        chkTipoComprobante.Checked = False
        chkCuentaBancaria.Checked = False
        chkFecha.Checked = False


        'ComboBox
        cbxTipoComprobante.Enabled = False
        cbxCuentaBancaria.Enabled = False

        'ListView
        dgwComprobante.Rows.Clear()
        dgwOperacion.Rows.Clear()

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion(frmDepositoBancario.Name, "DEPOSITO BANCARIO", "DEP")
        CargarInformacion()

        'Foco
        txtOperacion.Focus()

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Cuenta Bancaria
        CSistema.SqlToComboBox(cbxCuentaBancaria, "Select  ID, CuentaBancaria From CuentaBancaria Order By 2")

        'TipoComprobante
        CSistema.SqlToComboBox(cbxTipoComprobante, "Select ID, Descripcion From VTipoComprobante Where IDOperacion=" & IDOperacion & " Order By 2")


        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Cuenta Bancaria
        chkCuentaBancaria.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA ACTIVO", "False")
        cbxCuentaBancaria.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA", "")

        'Tipo de Comprobante
        chkTipoComprobante.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO", "False")
        cbxTipoComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", "")



        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Cuenta Bancaria
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA ACTIVO", chkCuentaBancaria.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CUENTA BANCARIA", cbxCuentaBancaria.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE ACTIVO", chkTipoComprobante.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", cbxTipoComprobante.Text)



    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Deposito
    Sub ListarDeposito(Optional ByVal Condicion As String = "")

        ctrError.Clear()

        'Inicializr el Where
        Where = ""

        'Cuenta Bancaria
        If EstablecerCondicion(cbxCuentaBancaria, chkCuentaBancaria, "IDCuentaBancaria", "Seleccione correctamente el cliente!") = False Then
            Exit Sub
        End If

        'Comprobante
        If EstablecerCondicion(cbxTipoComprobante, chkTipoComprobante, "IDTipoComprobante", "Seleccione correctamente el tipo de comprobante!") = False Then
            Exit Sub
        End If

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la sucursal!") = False Then
            Exit Sub
        End If

        'Fecha
        If chkFecha.Checked = True Then
            If Where = "" Then
                Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' ) "
            Else
                Where = Where & " And (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' )  "
            End If
        End If

        dgwComprobante.Rows.Clear()
        dgwOperacion.Rows.Clear()

        'Buscar por Operacion
        If Condicion <> "" Then
            Where = Condicion
        End If

        'buscar por Comprobantes
        
        If Condicion <> "" Then
            Where = Condicion
        End If

        Dim sql As String = "Select IDTransaccion,Numero,Cuenta,Banco,Comprobante,Sucursal,Fec,Observacion,'Efectivo' = TotalEfectivo,'Chq.Local'= TotalChequeLocal,'Chq.Otros'=TotalChequeOtros,Total From VDepositoBancario " & Where

        dtOperacion = CSistema.ExecuteToDataTable(sql)


        Dim TotalDepositoBancario As Decimal = 0
        Dim CantidadDepositoBancario As Decimal = 0

        For Each oRow As DataRow In dtOperacion.Rows
            Dim Registro(12) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Numero").ToString
            Registro(2) = oRow("Comprobante").ToString
            Registro(3) = oRow("Cuenta").ToString
            Registro(4) = oRow("Banco").ToString
            Registro(5) = oRow("Sucursal").ToString
            Registro(6) = oRow("Fec").ToString
            Registro(7) = oRow("Observacion").ToString
            Registro(8) = CSistema.FormatoMoneda(oRow("Efectivo").ToString)
            Registro(9) = CSistema.FormatoMoneda(oRow("Chq.Local").ToString)
            Registro(10) = CSistema.FormatoMoneda(oRow("Chq.Otros").ToString)
            Registro(11) = CSistema.FormatoMoneda(oRow("Total").ToString)

            'Sumar el total del saldo
            TotalDepositoBancario = TotalDepositoBancario + CDec(oRow("Total").ToString)

            CantidadDepositoBancario = CantidadDepositoBancario + 1

            dgwOperacion.Rows.Add(Registro)

        Next

        'Totales
        txtTotalCobranza.SetValue(TotalDepositoBancario)
        txtCantidadCobranza.SetValue(CantidadDepositoBancario)

    End Sub

    'Listar Comprobantes
    Sub ListarComprobantes(ByVal vIDTransaccion As Integer)

        'Limpiar ListView
        dgwComprobante.Rows.Clear()

        Dim sql As String = "Select Valor,Banco,Numero,Importe,Cliente,Tipo From VDetalleDepositoBancario Where IDTransaccionDepositoBancario =" & vIDTransaccion

        dtComprobante = CSistema.ExecuteToDataTable(sql)

        Dim TotalComprobante As Decimal = 0
        Dim CantidadComprobante As Decimal = 0

        For Each oRow As DataRow In dtComprobante.Rows
            Dim Registro(11) As String
            Registro(0) = oRow("Valor").ToString
            Registro(1) = oRow("Banco").ToString
            Registro(2) = oRow("Numero").ToString
            Registro(3) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(4) = oRow("Cliente").ToString
            Registro(5) = oRow("Tipo").ToString

            'Sumar el total del saldo
            TotalComprobante = TotalComprobante + CDec(oRow("Importe").ToString)

            CantidadComprobante = CantidadComprobante + 1

            dgwComprobante.Rows.Add(Registro)

        Next

        'Totales
        txtTotalComprobantes.SetValue(TotalComprobante)
        txtCantidadComprobantes.SetValue(CantidadComprobante)

        

    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = dgwOperacion.SelectedRows(0).Cells(0).Value

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub frmConsultaChequeCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkCliente_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCuentaBancaria.CheckedChanged
        HabilitarControles(chkCuentaBancaria, cbxCuentaBancaria)
    End Sub

    Private Sub chkBanco_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipoComprobante.CheckedChanged
        HabilitarControles(chkTipoComprobante, cbxTipoComprobante)
    End Sub

    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged
        HabilitarControles(chkFecha, dtpDesde)
        HabilitarControles(chkFecha, dtpHasta)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        ListarDeposito()
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        SeleccionarRegistro()
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada

        If e.KeyCode = Keys.Enter Then
            Dim Condicion As String = " Where Numero = " & txtOperacion.ObtenerValor
            ListarDeposito(Condicion)
        End If

        
    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada

        If e.KeyCode = Keys.Enter Then

            If IsNumeric(txtNroComprobante.txt.Text) = False Then
                Exit Sub
            End If

            Dim Condicion1 As String = " Where Comprobante = '" & txtNroComprobante.txt.Text & "'"
            ListarDeposito(Condicion1)

        End If

    End Sub

    Private Sub dgwOperacion_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgwOperacion.CellClick
        ctrError.Clear()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        Dim vIDTransaccion As Integer

        vIDTransaccion = dgwOperacion.SelectedRows(0).Cells(0).Value

        ListarComprobantes(vIDTransaccion)
    End Sub

    Private Sub dgwOperacion_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgwOperacion.CellDoubleClick
        SeleccionarRegistro()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmConsultaDepositoBancario_Activate()
        Me.Refresh()
    End Sub

End Class
﻿Public Class frmDepositoBancarioSeleccionarRedondeo

    'CLASES
    Dim CSistema As New CSistema

    'EVENTOS
    Public Event VentasSeleccionadas(ByVal sender As Object, ByVal e As EventArgs)

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private TotalValue As Decimal
    Public Property Total() As Decimal
        Get
            Return TotalValue
        End Get
        Set(ByVal value As Decimal)
            TotalValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Txt
        txtTotalComprobante.SetValue(Total)

        'Foco
        txtRedondearA.Focus()

    End Sub

    Sub Guardar()

        Dim Total As Decimal = txtDiferencia.ObtenerValor
        Dim Faltante As Boolean = False
        Dim Sobrante As Boolean = True

        If Total < 0 Then
            Faltante = True
            Sobrante = False
            Total = Total * -1
        End If

        Dim NewRow As DataRow = dt.NewRow
        NewRow("IDTransaccionDepositoBancario") = 0
        NewRow("ID") = dt.Rows.Count
        NewRow("Redondeo") = True
        NewRow("Faltante") = Faltante
        NewRow("Sobrante") = Sobrante
        NewRow("Importe") = Total

        dt.Rows.Add(NewRow)

        Me.Close()

    End Sub

    Sub CalcularSaldo(ByVal Diferencia As Boolean)

        If Diferencia Then
            txtDiferencia.SetValue(Total - CDec(txtRedondearA.ObtenerValor))
        End If

        If Diferencia = False Then
            txtRedondearA.SetValue(Total - CDec(txtDiferencia.ObtenerValor))
        End If


    End Sub

    Private Sub frmDepositoBancarioSeleccionarRedondeo_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub txtRedondearA_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtRedondearA.TeclaPrecionada
        CalcularSaldo(True)
    End Sub

    Private Sub txtDiferencia_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDiferencia.TeclaPrecionada
        CalcularSaldo(False)
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmDepositoBancarioSeleccionarRedondeo_Activate()
        Me.Refresh()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaDepositoBancario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalCobranza = New ERP.ocxTXTNumeric()
        Me.lblTotalCobranza = New System.Windows.Forms.Label()
        Me.btnSeleccionar = New System.Windows.Forms.Button()
        Me.cbxSucursal = New System.Windows.Forms.ComboBox()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblCantidadCobranza = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtNroComprobante = New ERP.ocxTXTString()
        Me.lblNroComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.txtOperacion = New ERP.ocxTXTNumeric()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtCantidadCobranza = New ERP.ocxTXTNumeric()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.chkTipoComprobante = New System.Windows.Forms.CheckBox()
        Me.cbxTipoComprobante = New System.Windows.Forms.ComboBox()
        Me.chkCuentaBancaria = New System.Windows.Forms.CheckBox()
        Me.lblTotalComprobantes = New System.Windows.Forms.Label()
        Me.cbxCuentaBancaria = New System.Windows.Forms.ComboBox()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.chkSucursal = New System.Windows.Forms.CheckBox()
        Me.btn4 = New System.Windows.Forms.Button()
        Me.chkFecha = New System.Windows.Forms.CheckBox()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.lblComprobantes = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblCantidadComprobantes = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.txtCantidadComprobantes = New ERP.ocxTXTNumeric()
        Me.flpTotal = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalComprobantes = New ERP.ocxTXTNumeric()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.tlpInforme = New System.Windows.Forms.TableLayoutPanel()
        Me.dgwComprobante = New System.Windows.Forms.DataGridView()
        Me.colValor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBancoComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNumero = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgwOperacion = New System.Windows.Forms.DataGridView()
        Me.tlpSeleccionar = New System.Windows.Forms.TableLayoutPanel()
        Me.tlpGeneral = New System.Windows.Forms.TableLayoutPanel()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNroOperacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCuenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBanco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSucursal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colObservacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEfectivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colChequeLocal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colChequeOtros = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.flpTotal.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tlpInforme.SuspendLayout()
        CType(Me.dgwComprobante, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgwOperacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tlpSeleccionar.SuspendLayout()
        Me.tlpGeneral.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalCobranza)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblTotalCobranza)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(301, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(293, 25)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'txtTotalCobranza
        '
        Me.txtTotalCobranza.Color = System.Drawing.Color.Empty
        Me.txtTotalCobranza.Decimales = True
        Me.txtTotalCobranza.Indicaciones = Nothing
        Me.txtTotalCobranza.Location = New System.Drawing.Point(187, 3)
        Me.txtTotalCobranza.Name = "txtTotalCobranza"
        Me.txtTotalCobranza.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalCobranza.SoloLectura = False
        Me.txtTotalCobranza.TabIndex = 1
        Me.txtTotalCobranza.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCobranza.Texto = "0"
        '
        'lblTotalCobranza
        '
        Me.lblTotalCobranza.AutoSize = True
        Me.lblTotalCobranza.Location = New System.Drawing.Point(147, 0)
        Me.lblTotalCobranza.Name = "lblTotalCobranza"
        Me.lblTotalCobranza.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalCobranza.TabIndex = 0
        Me.lblTotalCobranza.Text = "Total:"
        '
        'btnSeleccionar
        '
        Me.btnSeleccionar.Location = New System.Drawing.Point(3, 3)
        Me.btnSeleccionar.Name = "btnSeleccionar"
        Me.btnSeleccionar.Size = New System.Drawing.Size(75, 20)
        Me.btnSeleccionar.TabIndex = 0
        Me.btnSeleccionar.Text = "Seleccionar"
        Me.btnSeleccionar.UseVisualStyleBackColor = True
        '
        'cbxSucursal
        '
        Me.cbxSucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormattingEnabled = True
        Me.cbxSucursal.Location = New System.Drawing.Point(6, 110)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.Size = New System.Drawing.Size(203, 21)
        Me.cbxSucursal.TabIndex = 5
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 4
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 84.56376!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.43624!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Panel2, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel3, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel4, 3, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(4, 4)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(597, 32)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblCantidadCobranza)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(449, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(60, 26)
        Me.Panel2.TabIndex = 1
        '
        'lblCantidadCobranza
        '
        Me.lblCantidadCobranza.AutoSize = True
        Me.lblCantidadCobranza.Location = New System.Drawing.Point(3, 6)
        Me.lblCantidadCobranza.Name = "lblCantidadCobranza"
        Me.lblCantidadCobranza.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadCobranza.TabIndex = 0
        Me.lblCantidadCobranza.Text = "Cantidad:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.txtNroComprobante)
        Me.Panel3.Controls.Add(Me.lblNroComprobante)
        Me.Panel3.Controls.Add(Me.lblOperacion)
        Me.Panel3.Controls.Add(Me.txtOperacion)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(372, 26)
        Me.Panel3.TabIndex = 0
        '
        'txtNroComprobante
        '
        Me.txtNroComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroComprobante.Color = System.Drawing.Color.Empty
        Me.txtNroComprobante.Indicaciones = Nothing
        Me.txtNroComprobante.Location = New System.Drawing.Point(256, 3)
        Me.txtNroComprobante.Multilinea = False
        Me.txtNroComprobante.Name = "txtNroComprobante"
        Me.txtNroComprobante.Size = New System.Drawing.Size(111, 21)
        Me.txtNroComprobante.SoloLectura = False
        Me.txtNroComprobante.TabIndex = 3
        Me.txtNroComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroComprobante.Texto = ""
        '
        'lblNroComprobante
        '
        Me.lblNroComprobante.AutoSize = True
        Me.lblNroComprobante.Location = New System.Drawing.Point(146, 6)
        Me.lblNroComprobante.Name = "lblNroComprobante"
        Me.lblNroComprobante.Size = New System.Drawing.Size(111, 13)
        Me.lblNroComprobante.TabIndex = 2
        Me.lblNroComprobante.Text = "Nro. de Comprobante:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperacion.Location = New System.Drawing.Point(3, 6)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(69, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operación:"
        '
        'txtOperacion
        '
        Me.txtOperacion.Color = System.Drawing.Color.Empty
        Me.txtOperacion.Decimales = True
        Me.txtOperacion.Indicaciones = Nothing
        Me.txtOperacion.Location = New System.Drawing.Point(73, 2)
        Me.txtOperacion.Name = "txtOperacion"
        Me.txtOperacion.Size = New System.Drawing.Size(68, 22)
        Me.txtOperacion.SoloLectura = False
        Me.txtOperacion.TabIndex = 1
        Me.txtOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOperacion.Texto = "0"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.txtCantidadCobranza)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(515, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(79, 26)
        Me.Panel4.TabIndex = 2
        '
        'txtCantidadCobranza
        '
        Me.txtCantidadCobranza.Color = System.Drawing.Color.Empty
        Me.txtCantidadCobranza.Decimales = True
        Me.txtCantidadCobranza.Indicaciones = Nothing
        Me.txtCantidadCobranza.Location = New System.Drawing.Point(3, 2)
        Me.txtCantidadCobranza.Name = "txtCantidadCobranza"
        Me.txtCantidadCobranza.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadCobranza.SoloLectura = False
        Me.txtCantidadCobranza.TabIndex = 0
        Me.txtCantidadCobranza.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadCobranza.Texto = "0"
        '
        'dtpHasta
        '
        Me.dtpHasta.Enabled = False
        Me.dtpHasta.Location = New System.Drawing.Point(6, 224)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(203, 20)
        Me.dtpHasta.TabIndex = 8
        '
        'dtpDesde
        '
        Me.dtpDesde.Enabled = False
        Me.dtpDesde.Location = New System.Drawing.Point(6, 198)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(203, 20)
        Me.dtpDesde.TabIndex = 7
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.AutoSize = True
        Me.chkTipoComprobante.Location = New System.Drawing.Point(6, 49)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(131, 17)
        Me.chkTipoComprobante.TabIndex = 2
        Me.chkTipoComprobante.Text = "Tipo de Comprobante:"
        Me.chkTipoComprobante.UseVisualStyleBackColor = True
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormattingEnabled = True
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(6, 66)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(203, 21)
        Me.cbxTipoComprobante.TabIndex = 3
        '
        'chkCuentaBancaria
        '
        Me.chkCuentaBancaria.AutoSize = True
        Me.chkCuentaBancaria.Location = New System.Drawing.Point(6, 6)
        Me.chkCuentaBancaria.Name = "chkCuentaBancaria"
        Me.chkCuentaBancaria.Size = New System.Drawing.Size(108, 17)
        Me.chkCuentaBancaria.TabIndex = 0
        Me.chkCuentaBancaria.Text = "Cuenta Bancaria:"
        Me.chkCuentaBancaria.UseVisualStyleBackColor = True
        '
        'lblTotalComprobantes
        '
        Me.lblTotalComprobantes.AutoSize = True
        Me.lblTotalComprobantes.Location = New System.Drawing.Point(451, 0)
        Me.lblTotalComprobantes.Name = "lblTotalComprobantes"
        Me.lblTotalComprobantes.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalComprobantes.TabIndex = 0
        Me.lblTotalComprobantes.Text = "Total:"
        '
        'cbxCuentaBancaria
        '
        Me.cbxCuentaBancaria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxCuentaBancaria.Enabled = False
        Me.cbxCuentaBancaria.FormattingEnabled = True
        Me.cbxCuentaBancaria.Location = New System.Drawing.Point(6, 22)
        Me.cbxCuentaBancaria.Name = "cbxCuentaBancaria"
        Me.cbxCuentaBancaria.Size = New System.Drawing.Size(203, 21)
        Me.cbxCuentaBancaria.TabIndex = 1
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.TabControl1)
        Me.gbxFiltro.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxFiltro.Location = New System.Drawing.Point(614, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(231, 486)
        Me.gbxFiltro.TabIndex = 1
        Me.gbxFiltro.TabStop = False
        '
        'TabControl1
        '
        Me.TabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(3, 16)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(225, 467)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.chkSucursal)
        Me.TabPage1.Controls.Add(Me.cbxSucursal)
        Me.TabPage1.Controls.Add(Me.btn4)
        Me.TabPage1.Controls.Add(Me.chkFecha)
        Me.TabPage1.Controls.Add(Me.dtpHasta)
        Me.TabPage1.Controls.Add(Me.dtpDesde)
        Me.TabPage1.Controls.Add(Me.chkTipoComprobante)
        Me.TabPage1.Controls.Add(Me.cbxTipoComprobante)
        Me.TabPage1.Controls.Add(Me.chkCuentaBancaria)
        Me.TabPage1.Controls.Add(Me.cbxCuentaBancaria)
        Me.TabPage1.Location = New System.Drawing.Point(4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(217, 441)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'chkSucursal
        '
        Me.chkSucursal.AutoSize = True
        Me.chkSucursal.Location = New System.Drawing.Point(6, 93)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(70, 17)
        Me.chkSucursal.TabIndex = 4
        Me.chkSucursal.Text = "Sucursal:"
        Me.chkSucursal.UseVisualStyleBackColor = True
        '
        'btn4
        '
        Me.btn4.Location = New System.Drawing.Point(6, 256)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(203, 24)
        Me.btn4.TabIndex = 9
        Me.btn4.Text = "Emitir informe"
        Me.btn4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn4.UseVisualStyleBackColor = True
        '
        'chkFecha
        '
        Me.chkFecha.AutoSize = True
        Me.chkFecha.Location = New System.Drawing.Point(6, 181)
        Me.chkFecha.Name = "chkFecha"
        Me.chkFecha.Size = New System.Drawing.Size(59, 17)
        Me.chkFecha.TabIndex = 6
        Me.chkFecha.Text = "Fecha:"
        Me.chkFecha.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 4
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.01285!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.98715!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Panel5, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel1, 2, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel6, 3, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(4, 250)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(597, 30)
        Me.TableLayoutPanel4.TabIndex = 3
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.lblComprobantes)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(3, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(360, 24)
        Me.Panel5.TabIndex = 0
        '
        'lblComprobantes
        '
        Me.lblComprobantes.AutoSize = True
        Me.lblComprobantes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComprobantes.Location = New System.Drawing.Point(3, 8)
        Me.lblComprobantes.Name = "lblComprobantes"
        Me.lblComprobantes.Size = New System.Drawing.Size(211, 13)
        Me.lblComprobantes.TabIndex = 0
        Me.lblComprobantes.Text = "Lista de comprobantes depositados:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblCantidadComprobantes)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(449, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(60, 24)
        Me.Panel1.TabIndex = 1
        '
        'lblCantidadComprobantes
        '
        Me.lblCantidadComprobantes.AutoSize = True
        Me.lblCantidadComprobantes.Location = New System.Drawing.Point(3, 8)
        Me.lblCantidadComprobantes.Name = "lblCantidadComprobantes"
        Me.lblCantidadComprobantes.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadComprobantes.TabIndex = 0
        Me.lblCantidadComprobantes.Text = "Cantidad:"
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.txtCantidadComprobantes)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(515, 3)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(79, 24)
        Me.Panel6.TabIndex = 1
        '
        'txtCantidadComprobantes
        '
        Me.txtCantidadComprobantes.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobantes.Decimales = True
        Me.txtCantidadComprobantes.Indicaciones = Nothing
        Me.txtCantidadComprobantes.Location = New System.Drawing.Point(3, 1)
        Me.txtCantidadComprobantes.Name = "txtCantidadComprobantes"
        Me.txtCantidadComprobantes.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadComprobantes.SoloLectura = False
        Me.txtCantidadComprobantes.TabIndex = 0
        Me.txtCantidadComprobantes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobantes.Texto = "0"
        '
        'flpTotal
        '
        Me.flpTotal.Controls.Add(Me.txtTotalComprobantes)
        Me.flpTotal.Controls.Add(Me.lblTotalComprobantes)
        Me.flpTotal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.flpTotal.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.flpTotal.Location = New System.Drawing.Point(4, 456)
        Me.flpTotal.Name = "flpTotal"
        Me.flpTotal.Size = New System.Drawing.Size(597, 26)
        Me.flpTotal.TabIndex = 5
        '
        'txtTotalComprobantes
        '
        Me.txtTotalComprobantes.Color = System.Drawing.Color.Empty
        Me.txtTotalComprobantes.Decimales = True
        Me.txtTotalComprobantes.Indicaciones = Nothing
        Me.txtTotalComprobantes.Location = New System.Drawing.Point(491, 3)
        Me.txtTotalComprobantes.Name = "txtTotalComprobantes"
        Me.txtTotalComprobantes.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalComprobantes.SoloLectura = False
        Me.txtTotalComprobantes.TabIndex = 1
        Me.txtTotalComprobantes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalComprobantes.Texto = "0"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'tlpInforme
        '
        Me.tlpInforme.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.tlpInforme.ColumnCount = 1
        Me.tlpInforme.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpInforme.Controls.Add(Me.dgwComprobante, 0, 4)
        Me.tlpInforme.Controls.Add(Me.TableLayoutPanel4, 0, 3)
        Me.tlpInforme.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.tlpInforme.Controls.Add(Me.dgwOperacion, 0, 1)
        Me.tlpInforme.Controls.Add(Me.flpTotal, 0, 5)
        Me.tlpInforme.Controls.Add(Me.tlpSeleccionar, 0, 2)
        Me.tlpInforme.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpInforme.Location = New System.Drawing.Point(3, 3)
        Me.tlpInforme.Name = "tlpInforme"
        Me.tlpInforme.RowCount = 6
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpInforme.Size = New System.Drawing.Size(605, 486)
        Me.tlpInforme.TabIndex = 0
        '
        'dgwComprobante
        '
        Me.dgwComprobante.AllowUserToAddRows = False
        Me.dgwComprobante.AllowUserToDeleteRows = False
        Me.dgwComprobante.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgwComprobante.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgwComprobante.BackgroundColor = System.Drawing.Color.White
        Me.dgwComprobante.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwComprobante.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colValor, Me.colBancoComprobante, Me.colNumero, Me.colImporte, Me.colCliente, Me.colTipo})
        Me.dgwComprobante.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgwComprobante.Location = New System.Drawing.Point(4, 287)
        Me.dgwComprobante.Name = "dgwComprobante"
        Me.dgwComprobante.ReadOnly = True
        Me.dgwComprobante.RowHeadersVisible = False
        Me.dgwComprobante.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgwComprobante.Size = New System.Drawing.Size(597, 162)
        Me.dgwComprobante.TabIndex = 4
        Me.dgwComprobante.TabStop = False
        '
        'colValor
        '
        Me.colValor.HeaderText = "Valor"
        Me.colValor.Name = "colValor"
        Me.colValor.ReadOnly = True
        '
        'colBancoComprobante
        '
        Me.colBancoComprobante.HeaderText = "Banco"
        Me.colBancoComprobante.Name = "colBancoComprobante"
        Me.colBancoComprobante.ReadOnly = True
        '
        'colNumero
        '
        Me.colNumero.HeaderText = "Número"
        Me.colNumero.Name = "colNumero"
        Me.colNumero.ReadOnly = True
        '
        'colImporte
        '
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        '
        'colCliente
        '
        Me.colCliente.HeaderText = "Cliente"
        Me.colCliente.Name = "colCliente"
        Me.colCliente.ReadOnly = True
        '
        'colTipo
        '
        Me.colTipo.HeaderText = "Tipo"
        Me.colTipo.Name = "colTipo"
        Me.colTipo.ReadOnly = True
        '
        'dgwOperacion
        '
        Me.dgwOperacion.AllowUserToAddRows = False
        Me.dgwOperacion.AllowUserToDeleteRows = False
        Me.dgwOperacion.AllowUserToOrderColumns = True
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgwOperacion.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgwOperacion.BackgroundColor = System.Drawing.Color.White
        Me.dgwOperacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwOperacion.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colNroOperacion, Me.colComprobante, Me.colCuenta, Me.colBanco, Me.colSucursal, Me.colFecha, Me.colObservacion, Me.colEfectivo, Me.colChequeLocal, Me.colChequeOtros, Me.colTotal})
        Me.dgwOperacion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgwOperacion.Location = New System.Drawing.Point(4, 43)
        Me.dgwOperacion.Name = "dgwOperacion"
        Me.dgwOperacion.ReadOnly = True
        Me.dgwOperacion.RowHeadersVisible = False
        Me.dgwOperacion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgwOperacion.Size = New System.Drawing.Size(597, 162)
        Me.dgwOperacion.TabIndex = 1
        Me.dgwOperacion.TabStop = False
        '
        'tlpSeleccionar
        '
        Me.tlpSeleccionar.ColumnCount = 2
        Me.tlpSeleccionar.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpSeleccionar.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpSeleccionar.Controls.Add(Me.FlowLayoutPanel1, 1, 0)
        Me.tlpSeleccionar.Controls.Add(Me.btnSeleccionar, 0, 0)
        Me.tlpSeleccionar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpSeleccionar.Location = New System.Drawing.Point(4, 212)
        Me.tlpSeleccionar.Name = "tlpSeleccionar"
        Me.tlpSeleccionar.RowCount = 1
        Me.tlpSeleccionar.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpSeleccionar.Size = New System.Drawing.Size(597, 31)
        Me.tlpSeleccionar.TabIndex = 2
        '
        'tlpGeneral
        '
        Me.tlpGeneral.ColumnCount = 2
        Me.tlpGeneral.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpGeneral.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 237.0!))
        Me.tlpGeneral.Controls.Add(Me.gbxFiltro, 1, 0)
        Me.tlpGeneral.Controls.Add(Me.tlpInforme, 0, 0)
        Me.tlpGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpGeneral.Location = New System.Drawing.Point(0, 0)
        Me.tlpGeneral.Name = "tlpGeneral"
        Me.tlpGeneral.RowCount = 1
        Me.tlpGeneral.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpGeneral.Size = New System.Drawing.Size(848, 492)
        Me.tlpGeneral.TabIndex = 0
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colNroOperacion
        '
        Me.colNroOperacion.HeaderText = "NroOper"
        Me.colNroOperacion.Name = "colNroOperacion"
        Me.colNroOperacion.ReadOnly = True
        Me.colNroOperacion.Width = 50
        '
        'colComprobante
        '
        Me.colComprobante.HeaderText = "Compr"
        Me.colComprobante.Name = "colComprobante"
        Me.colComprobante.ReadOnly = True
        Me.colComprobante.Width = 50
        '
        'colCuenta
        '
        Me.colCuenta.HeaderText = "Cuenta"
        Me.colCuenta.Name = "colCuenta"
        Me.colCuenta.ReadOnly = True
        Me.colCuenta.Width = 90
        '
        'colBanco
        '
        Me.colBanco.HeaderText = "Banco"
        Me.colBanco.Name = "colBanco"
        Me.colBanco.ReadOnly = True
        '
        'colSucursal
        '
        Me.colSucursal.HeaderText = "Sucursal"
        Me.colSucursal.Name = "colSucursal"
        Me.colSucursal.ReadOnly = True
        Me.colSucursal.Width = 70
        '
        'colFecha
        '
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.ReadOnly = True
        Me.colFecha.Width = 70
        '
        'colObservacion
        '
        Me.colObservacion.HeaderText = "Observación"
        Me.colObservacion.Name = "colObservacion"
        Me.colObservacion.ReadOnly = True
        '
        'colEfectivo
        '
        Me.colEfectivo.HeaderText = "Efectivo"
        Me.colEfectivo.Name = "colEfectivo"
        Me.colEfectivo.ReadOnly = True
        '
        'colChequeLocal
        '
        Me.colChequeLocal.HeaderText = "Chq.Local"
        Me.colChequeLocal.Name = "colChequeLocal"
        Me.colChequeLocal.ReadOnly = True
        Me.colChequeLocal.Visible = False
        '
        'colChequeOtros
        '
        Me.colChequeOtros.HeaderText = "Chq.Otros"
        Me.colChequeOtros.Name = "colChequeOtros"
        Me.colChequeOtros.ReadOnly = True
        '
        'colTotal
        '
        Me.colTotal.HeaderText = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.ReadOnly = True
        '
        'frmConsultaDepositoBancario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(848, 492)
        Me.Controls.Add(Me.tlpGeneral)
        Me.Name = "frmConsultaDepositoBancario"
        Me.Text = "frmConsultaDepositoBancario"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.gbxFiltro.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.flpTotal.ResumeLayout(False)
        Me.flpTotal.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tlpInforme.ResumeLayout(False)
        CType(Me.dgwComprobante, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgwOperacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tlpSeleccionar.ResumeLayout(False)
        Me.tlpGeneral.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotalCobranza As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalCobranza As System.Windows.Forms.Label
    Friend WithEvents btnSeleccionar As System.Windows.Forms.Button
    Friend WithEvents cbxSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidadCobranza As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtNroComprobante As ERP.ocxTXTString
    Friend WithEvents lblNroComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtOperacion As ERP.ocxTXTNumeric
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidadCobranza As ERP.ocxTXTNumeric
    Friend WithEvents dtpHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkTipoComprobante As System.Windows.Forms.CheckBox
    Friend WithEvents cbxTipoComprobante As System.Windows.Forms.ComboBox
    Friend WithEvents chkCuentaBancaria As System.Windows.Forms.CheckBox
    Friend WithEvents lblTotalComprobantes As System.Windows.Forms.Label
    Friend WithEvents cbxCuentaBancaria As System.Windows.Forms.ComboBox
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents chkSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents btn4 As System.Windows.Forms.Button
    Friend WithEvents chkFecha As System.Windows.Forms.CheckBox
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents lblComprobantes As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidadComprobantes As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidadComprobantes As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalComprobantes As ERP.ocxTXTNumeric
    Friend WithEvents flpTotal As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents tlpGeneral As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tlpInforme As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tlpSeleccionar As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents dgwComprobante As System.Windows.Forms.DataGridView
    Friend WithEvents dgwOperacion As System.Windows.Forms.DataGridView
    Friend WithEvents colValor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBancoComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNumero As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIDTransaccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNroOperacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCuenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBanco As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSucursal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colObservacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEfectivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colChequeLocal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colChequeOtros As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotal As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

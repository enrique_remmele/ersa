﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDepositoBancario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblObsrevacion = New System.Windows.Forms.Label()
        Me.lblCambio = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.txtCambio = New ERP.ocxTXTNumeric()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.txtfecha = New ERP.ocxTXTDate()
        Me.txtMoneda = New ERP.ocxTXTString()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxCuenta = New ERP.ocxCBX()
        Me.txtBanco = New ERP.ocxTXTString()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.gbxComprobantes = New System.Windows.Forms.GroupBox()
        Me.btnAgregarTarjetas = New System.Windows.Forms.Button()
        Me.btnRedondear = New System.Windows.Forms.Button()
        Me.btnAgregarDocumentos = New System.Windows.Forms.Button()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBanco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNroComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCotizacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCuentaContable = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnAgregarCheques = New System.Windows.Forms.Button()
        Me.lklEliminarVenta = New System.Windows.Forms.LinkLabel()
        Me.btnAgregarEfectivo = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lblTotalDepositado = New System.Windows.Forms.Label()
        Me.lblTotalEfectivo = New System.Windows.Forms.Label()
        Me.lblTotalChqLocal = New System.Windows.Forms.Label()
        Me.lblTotalChqOtros = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblRedondeo = New System.Windows.Forms.Label()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDiferenciaCambio = New ERP.ocxTXTNumeric()
        Me.txtRedondeo = New ERP.ocxTXTNumeric()
        Me.txtTotalDocumento = New ERP.ocxTXTNumeric()
        Me.txtTotalChequeOtro = New ERP.ocxTXTNumeric()
        Me.txtTotalDepositado = New ERP.ocxTXTNumeric()
        Me.TxtCantidadDepositado = New ERP.ocxTXTNumeric()
        Me.txtTotalChequeLocal = New ERP.ocxTXTNumeric()
        Me.txtTotalEfectivo = New ERP.ocxTXTNumeric()
        Me.txtTotalTarjeta = New ERP.ocxTXTNumeric()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.gbxComprobantes.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.flpRegistradoPor.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(12, 25)
        Me.lblOperacion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(78, 17)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(12, 60)
        Me.lblCuenta.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(57, 17)
        Me.lblCuenta.TabIndex = 10
        Me.lblCuenta.Text = "Cuenta:"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(869, 25)
        Me.lblFecha.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(51, 17)
        Me.lblFecha.TabIndex = 8
        Me.lblFecha.Text = "Fecha:"
        '
        'lblObsrevacion
        '
        Me.lblObsrevacion.AutoSize = True
        Me.lblObsrevacion.Location = New System.Drawing.Point(12, 94)
        Me.lblObsrevacion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblObsrevacion.Name = "lblObsrevacion"
        Me.lblObsrevacion.Size = New System.Drawing.Size(92, 17)
        Me.lblObsrevacion.TabIndex = 16
        Me.lblObsrevacion.Text = "Observacion:"
        '
        'lblCambio
        '
        Me.lblCambio.AutoSize = True
        Me.lblCambio.Location = New System.Drawing.Point(551, 60)
        Me.lblCambio.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCambio.Name = "lblCambio"
        Me.lblCambio.Size = New System.Drawing.Size(59, 17)
        Me.lblCambio.TabIndex = 14
        Me.lblCambio.Text = "Cambio:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtComprobante)
        Me.GroupBox1.Controls.Add(Me.txtCambio)
        Me.GroupBox1.Controls.Add(Me.txtID)
        Me.GroupBox1.Controls.Add(Me.cbxTipoComprobante)
        Me.GroupBox1.Controls.Add(Me.lblComprobante)
        Me.GroupBox1.Controls.Add(Me.cbxSucursal)
        Me.GroupBox1.Controls.Add(Me.lblSucursal)
        Me.GroupBox1.Controls.Add(Me.txtfecha)
        Me.GroupBox1.Controls.Add(Me.txtMoneda)
        Me.GroupBox1.Controls.Add(Me.lblFecha)
        Me.GroupBox1.Controls.Add(Me.txtObservacion)
        Me.GroupBox1.Controls.Add(Me.lblCambio)
        Me.GroupBox1.Controls.Add(Me.lblCuenta)
        Me.GroupBox1.Controls.Add(Me.cbxCuenta)
        Me.GroupBox1.Controls.Add(Me.txtBanco)
        Me.GroupBox1.Controls.Add(Me.lblObsrevacion)
        Me.GroupBox1.Controls.Add(Me.cbxCiudad)
        Me.GroupBox1.Controls.Add(Me.lblOperacion)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 2)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(1023, 127)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(657, 20)
        Me.txtComprobante.Margin = New System.Windows.Forms.Padding(5)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(204, 26)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 7
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'txtCambio
        '
        Me.txtCambio.Color = System.Drawing.Color.Empty
        Me.txtCambio.Decimales = False
        Me.txtCambio.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCambio.Location = New System.Drawing.Point(613, 55)
        Me.txtCambio.Margin = New System.Windows.Forms.Padding(5)
        Me.txtCambio.Name = "txtCambio"
        Me.txtCambio.Size = New System.Drawing.Size(100, 26)
        Me.txtCambio.SoloLectura = False
        Me.txtCambio.TabIndex = 15
        Me.txtCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCambio.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(188, 20)
        Me.txtID.Margin = New System.Windows.Forms.Padding(5)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(87, 26)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(540, 20)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(5)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(109, 26)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 6
        Me.cbxTipoComprobante.Texto = ""
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(487, 25)
        Me.lblComprobante.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(52, 17)
        Me.lblComprobante.TabIndex = 5
        Me.lblComprobante.Text = "Comp.:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(336, 20)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(5)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(144, 26)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.Texto = ""
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(293, 25)
        Me.lblSucursal.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(40, 17)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Suc.:"
        '
        'txtfecha
        '
        Me.txtfecha.Color = System.Drawing.Color.Empty
        Me.txtfecha.Fecha = New Date(CType(0, Long))
        Me.txtfecha.Location = New System.Drawing.Point(923, 20)
        Me.txtfecha.Margin = New System.Windows.Forms.Padding(5)
        Me.txtfecha.Name = "txtfecha"
        Me.txtfecha.PermitirNulo = False
        Me.txtfecha.Size = New System.Drawing.Size(92, 25)
        Me.txtfecha.SoloLectura = False
        Me.txtfecha.TabIndex = 9
        '
        'txtMoneda
        '
        Me.txtMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMoneda.Color = System.Drawing.Color.Empty
        Me.txtMoneda.Indicaciones = Nothing
        Me.txtMoneda.Location = New System.Drawing.Point(480, 55)
        Me.txtMoneda.Margin = New System.Windows.Forms.Padding(5)
        Me.txtMoneda.Multilinea = False
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.Size = New System.Drawing.Size(71, 26)
        Me.txtMoneda.SoloLectura = True
        Me.txtMoneda.TabIndex = 13
        Me.txtMoneda.TabStop = False
        Me.txtMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMoneda.Texto = ""
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(112, 89)
        Me.txtObservacion.Margin = New System.Windows.Forms.Padding(5)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(903, 26)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 17
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxCuenta
        '
        Me.cbxCuenta.CampoWhere = Nothing
        Me.cbxCuenta.CargarUnaSolaVez = False
        Me.cbxCuenta.DataDisplayMember = Nothing
        Me.cbxCuenta.DataFilter = Nothing
        Me.cbxCuenta.DataOrderBy = Nothing
        Me.cbxCuenta.DataSource = Nothing
        Me.cbxCuenta.DataValueMember = Nothing
        Me.cbxCuenta.FormABM = Nothing
        Me.cbxCuenta.Indicaciones = Nothing
        Me.cbxCuenta.Location = New System.Drawing.Point(112, 55)
        Me.cbxCuenta.Margin = New System.Windows.Forms.Padding(5)
        Me.cbxCuenta.Name = "cbxCuenta"
        Me.cbxCuenta.SeleccionObligatoria = False
        Me.cbxCuenta.Size = New System.Drawing.Size(167, 26)
        Me.cbxCuenta.SoloLectura = False
        Me.cbxCuenta.TabIndex = 11
        Me.cbxCuenta.Texto = ""
        '
        'txtBanco
        '
        Me.txtBanco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBanco.Color = System.Drawing.Color.Empty
        Me.txtBanco.Indicaciones = Nothing
        Me.txtBanco.Location = New System.Drawing.Point(279, 55)
        Me.txtBanco.Margin = New System.Windows.Forms.Padding(5)
        Me.txtBanco.Multilinea = False
        Me.txtBanco.Name = "txtBanco"
        Me.txtBanco.Size = New System.Drawing.Size(201, 26)
        Me.txtBanco.SoloLectura = True
        Me.txtBanco.TabIndex = 12
        Me.txtBanco.TabStop = False
        Me.txtBanco.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBanco.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(111, 20)
        Me.cbxCiudad.Margin = New System.Windows.Forms.Padding(5)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(77, 26)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'gbxComprobantes
        '
        Me.gbxComprobantes.Controls.Add(Me.btnAgregarTarjetas)
        Me.gbxComprobantes.Controls.Add(Me.btnRedondear)
        Me.gbxComprobantes.Controls.Add(Me.btnAgregarDocumentos)
        Me.gbxComprobantes.Controls.Add(Me.dgw)
        Me.gbxComprobantes.Controls.Add(Me.btnAgregarCheques)
        Me.gbxComprobantes.Controls.Add(Me.lklEliminarVenta)
        Me.gbxComprobantes.Controls.Add(Me.btnAgregarEfectivo)
        Me.gbxComprobantes.Location = New System.Drawing.Point(3, 137)
        Me.gbxComprobantes.Margin = New System.Windows.Forms.Padding(4)
        Me.gbxComprobantes.Name = "gbxComprobantes"
        Me.gbxComprobantes.Padding = New System.Windows.Forms.Padding(4)
        Me.gbxComprobantes.Size = New System.Drawing.Size(1023, 338)
        Me.gbxComprobantes.TabIndex = 1
        Me.gbxComprobantes.TabStop = False
        '
        'btnAgregarTarjetas
        '
        Me.btnAgregarTarjetas.Location = New System.Drawing.Point(344, 13)
        Me.btnAgregarTarjetas.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAgregarTarjetas.Name = "btnAgregarTarjetas"
        Me.btnAgregarTarjetas.Size = New System.Drawing.Size(103, 26)
        Me.btnAgregarTarjetas.TabIndex = 3
        Me.btnAgregarTarjetas.Text = "Tarjetas"
        Me.btnAgregarTarjetas.UseVisualStyleBackColor = True
        '
        'btnRedondear
        '
        Me.btnRedondear.Location = New System.Drawing.Point(907, 14)
        Me.btnRedondear.Margin = New System.Windows.Forms.Padding(4)
        Me.btnRedondear.Name = "btnRedondear"
        Me.btnRedondear.Size = New System.Drawing.Size(103, 26)
        Me.btnRedondear.TabIndex = 4
        Me.btnRedondear.Text = "Redondear"
        Me.btnRedondear.UseVisualStyleBackColor = True
        '
        'btnAgregarDocumentos
        '
        Me.btnAgregarDocumentos.Location = New System.Drawing.Point(233, 14)
        Me.btnAgregarDocumentos.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAgregarDocumentos.Name = "btnAgregarDocumentos"
        Me.btnAgregarDocumentos.Size = New System.Drawing.Size(103, 26)
        Me.btnAgregarDocumentos.TabIndex = 2
        Me.btnAgregarDocumentos.Text = "Documentos"
        Me.btnAgregarDocumentos.UseVisualStyleBackColor = True
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        Me.dgw.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colTipo, Me.colBanco, Me.colNroComprobante, Me.colImporte, Me.colCotizacion, Me.colCliente, Me.colCuentaContable, Me.colID})
        Me.dgw.Location = New System.Drawing.Point(13, 47)
        Me.dgw.Margin = New System.Windows.Forms.Padding(4)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(1001, 268)
        Me.dgw.TabIndex = 4
        Me.dgw.TabStop = False
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        Me.colIDTransaccion.Width = 5
        '
        'colTipo
        '
        Me.colTipo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colTipo.DefaultCellStyle = DataGridViewCellStyle2
        Me.colTipo.HeaderText = "Tipo"
        Me.colTipo.Name = "colTipo"
        Me.colTipo.ReadOnly = True
        '
        'colBanco
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colBanco.DefaultCellStyle = DataGridViewCellStyle3
        Me.colBanco.HeaderText = "Banco"
        Me.colBanco.Name = "colBanco"
        Me.colBanco.ReadOnly = True
        '
        'colNroComprobante
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colNroComprobante.DefaultCellStyle = DataGridViewCellStyle4
        Me.colNroComprobante.HeaderText = "NroComprobante"
        Me.colNroComprobante.Name = "colNroComprobante"
        Me.colNroComprobante.ReadOnly = True
        '
        'colImporte
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle5
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        '
        'colCotizacion
        '
        Me.colCotizacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colCotizacion.DefaultCellStyle = DataGridViewCellStyle6
        Me.colCotizacion.HeaderText = "Cotizacion"
        Me.colCotizacion.Name = "colCotizacion"
        Me.colCotizacion.ReadOnly = True
        Me.colCotizacion.Width = 98
        '
        'colCliente
        '
        Me.colCliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.Format = "N0"
        DataGridViewCellStyle7.NullValue = "0"
        Me.colCliente.DefaultCellStyle = DataGridViewCellStyle7
        Me.colCliente.HeaderText = "Cliente"
        Me.colCliente.Name = "colCliente"
        Me.colCliente.ReadOnly = True
        '
        'colCuentaContable
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colCuentaContable.DefaultCellStyle = DataGridViewCellStyle8
        Me.colCuentaContable.HeaderText = "Cuenta Contable"
        Me.colCuentaContable.Name = "colCuentaContable"
        Me.colCuentaContable.ReadOnly = True
        Me.colCuentaContable.Width = 150
        '
        'colID
        '
        Me.colID.HeaderText = "ID"
        Me.colID.Name = "colID"
        Me.colID.ReadOnly = True
        Me.colID.Visible = False
        Me.colID.Width = 5
        '
        'btnAgregarCheques
        '
        Me.btnAgregarCheques.Location = New System.Drawing.Point(123, 14)
        Me.btnAgregarCheques.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAgregarCheques.Name = "btnAgregarCheques"
        Me.btnAgregarCheques.Size = New System.Drawing.Size(103, 26)
        Me.btnAgregarCheques.TabIndex = 1
        Me.btnAgregarCheques.Text = "Cheques"
        Me.btnAgregarCheques.UseVisualStyleBackColor = True
        '
        'lklEliminarVenta
        '
        Me.lklEliminarVenta.AutoSize = True
        Me.lklEliminarVenta.Location = New System.Drawing.Point(12, 319)
        Me.lklEliminarVenta.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lklEliminarVenta.Name = "lklEliminarVenta"
        Me.lklEliminarVenta.Size = New System.Drawing.Size(145, 17)
        Me.lklEliminarVenta.TabIndex = 5
        Me.lklEliminarVenta.TabStop = True
        Me.lklEliminarVenta.Text = "Eliminar comprobante"
        '
        'btnAgregarEfectivo
        '
        Me.btnAgregarEfectivo.Location = New System.Drawing.Point(12, 14)
        Me.btnAgregarEfectivo.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAgregarEfectivo.Name = "btnAgregarEfectivo"
        Me.btnAgregarEfectivo.Size = New System.Drawing.Size(103, 26)
        Me.btnAgregarEfectivo.TabIndex = 0
        Me.btnAgregarEfectivo.Text = "Efectivo"
        Me.btnAgregarEfectivo.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(15, 644)
        Me.btnEliminar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(100, 28)
        Me.btnEliminar.TabIndex = 16
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(123, 644)
        Me.btnImprimir.Margin = New System.Windows.Forms.Padding(4)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(100, 28)
        Me.btnImprimir.TabIndex = 17
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(493, 644)
        Me.btnAsiento.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(100, 28)
        Me.btnAsiento.TabIndex = 19
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(231, 644)
        Me.btnBusquedaAvanzada.Margin = New System.Windows.Forms.Padding(4)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(100, 28)
        Me.btnBusquedaAvanzada.TabIndex = 18
        Me.btnBusquedaAvanzada.Text = "&Busqueda"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(925, 644)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(100, 28)
        Me.btnSalir.TabIndex = 23
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(817, 644)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(100, 28)
        Me.btnCancelar.TabIndex = 22
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(709, 644)
        Me.btnGuardar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(100, 28)
        Me.btnGuardar.TabIndex = 21
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(601, 644)
        Me.btnNuevo.Margin = New System.Windows.Forms.Padding(4)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(100, 28)
        Me.btnNuevo.TabIndex = 20
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lblTotalDepositado
        '
        Me.lblTotalDepositado.AutoSize = True
        Me.lblTotalDepositado.Location = New System.Drawing.Point(659, 492)
        Me.lblTotalDepositado.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblTotalDepositado.Name = "lblTotalDepositado"
        Me.lblTotalDepositado.Size = New System.Drawing.Size(120, 17)
        Me.lblTotalDepositado.TabIndex = 11
        Me.lblTotalDepositado.Text = "Total Depositado:"
        '
        'lblTotalEfectivo
        '
        Me.lblTotalEfectivo.AutoSize = True
        Me.lblTotalEfectivo.Location = New System.Drawing.Point(368, 492)
        Me.lblTotalEfectivo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblTotalEfectivo.Name = "lblTotalEfectivo"
        Me.lblTotalEfectivo.Size = New System.Drawing.Size(98, 17)
        Me.lblTotalEfectivo.TabIndex = 3
        Me.lblTotalEfectivo.Text = "Total Efectivo:"
        '
        'lblTotalChqLocal
        '
        Me.lblTotalChqLocal.AutoSize = True
        Me.lblTotalChqLocal.Location = New System.Drawing.Point(368, 523)
        Me.lblTotalChqLocal.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblTotalChqLocal.Name = "lblTotalChqLocal"
        Me.lblTotalChqLocal.Size = New System.Drawing.Size(115, 17)
        Me.lblTotalChqLocal.TabIndex = 5
        Me.lblTotalChqLocal.Text = "Total Chq. Local:"
        '
        'lblTotalChqOtros
        '
        Me.lblTotalChqOtros.AutoSize = True
        Me.lblTotalChqOtros.Location = New System.Drawing.Point(368, 554)
        Me.lblTotalChqOtros.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblTotalChqOtros.Name = "lblTotalChqOtros"
        Me.lblTotalChqOtros.Size = New System.Drawing.Size(116, 17)
        Me.lblTotalChqOtros.TabIndex = 7
        Me.lblTotalChqOtros.Text = "Total Chq. Otros:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 677)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(1028, 22)
        Me.StatusStrip1.TabIndex = 24
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(11, 489)
        Me.flpRegistradoPor.Margin = New System.Windows.Forms.Padding(4)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(349, 25)
        Me.flpRegistradoPor.TabIndex = 2
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(4, 0)
        Me.lblRegistradoPor.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(106, 17)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(118, 0)
        Me.lblUsuarioRegistro.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(61, 17)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(187, 0)
        Me.lblFechaRegistro.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(47, 17)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(368, 586)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 17)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Total Documento:"
        '
        'lblRedondeo
        '
        Me.lblRedondeo.AutoSize = True
        Me.lblRedondeo.Location = New System.Drawing.Point(876, 523)
        Me.lblRedondeo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblRedondeo.Name = "lblRedondeo"
        Me.lblRedondeo.Size = New System.Drawing.Size(78, 17)
        Me.lblRedondeo.TabIndex = 14
        Me.lblRedondeo.Text = "Redondeo:"
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(385, 644)
        Me.btnModificar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(100, 28)
        Me.btnModificar.TabIndex = 25
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(659, 554)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(147, 17)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Diferencia de Cambio:"
        '
        'txtDiferenciaCambio
        '
        Me.txtDiferenciaCambio.Color = System.Drawing.Color.Empty
        Me.txtDiferenciaCambio.Decimales = True
        Me.txtDiferenciaCambio.Indicaciones = Nothing
        Me.txtDiferenciaCambio.Location = New System.Drawing.Point(808, 548)
        Me.txtDiferenciaCambio.Margin = New System.Windows.Forms.Padding(5)
        Me.txtDiferenciaCambio.Name = "txtDiferenciaCambio"
        Me.txtDiferenciaCambio.Size = New System.Drawing.Size(143, 27)
        Me.txtDiferenciaCambio.SoloLectura = True
        Me.txtDiferenciaCambio.TabIndex = 27
        Me.txtDiferenciaCambio.TabStop = False
        Me.txtDiferenciaCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDiferenciaCambio.Texto = "0"
        '
        'txtRedondeo
        '
        Me.txtRedondeo.Color = System.Drawing.Color.Empty
        Me.txtRedondeo.Decimales = True
        Me.txtRedondeo.Indicaciones = Nothing
        Me.txtRedondeo.Location = New System.Drawing.Point(959, 517)
        Me.txtRedondeo.Margin = New System.Windows.Forms.Padding(5)
        Me.txtRedondeo.Name = "txtRedondeo"
        Me.txtRedondeo.Size = New System.Drawing.Size(64, 27)
        Me.txtRedondeo.SoloLectura = True
        Me.txtRedondeo.TabIndex = 15
        Me.txtRedondeo.TabStop = False
        Me.txtRedondeo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRedondeo.Texto = "0"
        '
        'txtTotalDocumento
        '
        Me.txtTotalDocumento.Color = System.Drawing.Color.Empty
        Me.txtTotalDocumento.Decimales = True
        Me.txtTotalDocumento.Indicaciones = Nothing
        Me.txtTotalDocumento.Location = New System.Drawing.Point(507, 580)
        Me.txtTotalDocumento.Margin = New System.Windows.Forms.Padding(5)
        Me.txtTotalDocumento.Name = "txtTotalDocumento"
        Me.txtTotalDocumento.Size = New System.Drawing.Size(137, 27)
        Me.txtTotalDocumento.SoloLectura = True
        Me.txtTotalDocumento.TabIndex = 10
        Me.txtTotalDocumento.TabStop = False
        Me.txtTotalDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDocumento.Texto = "0"
        '
        'txtTotalChequeOtro
        '
        Me.txtTotalChequeOtro.Color = System.Drawing.Color.Empty
        Me.txtTotalChequeOtro.Decimales = True
        Me.txtTotalChequeOtro.Indicaciones = Nothing
        Me.txtTotalChequeOtro.Location = New System.Drawing.Point(507, 548)
        Me.txtTotalChequeOtro.Margin = New System.Windows.Forms.Padding(5)
        Me.txtTotalChequeOtro.Name = "txtTotalChequeOtro"
        Me.txtTotalChequeOtro.Size = New System.Drawing.Size(137, 27)
        Me.txtTotalChequeOtro.SoloLectura = True
        Me.txtTotalChequeOtro.TabIndex = 8
        Me.txtTotalChequeOtro.TabStop = False
        Me.txtTotalChequeOtro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalChequeOtro.Texto = "0"
        '
        'txtTotalDepositado
        '
        Me.txtTotalDepositado.Color = System.Drawing.Color.Empty
        Me.txtTotalDepositado.Decimales = True
        Me.txtTotalDepositado.Indicaciones = Nothing
        Me.txtTotalDepositado.Location = New System.Drawing.Point(808, 487)
        Me.txtTotalDepositado.Margin = New System.Windows.Forms.Padding(5)
        Me.txtTotalDepositado.Name = "txtTotalDepositado"
        Me.txtTotalDepositado.Size = New System.Drawing.Size(143, 27)
        Me.txtTotalDepositado.SoloLectura = True
        Me.txtTotalDepositado.TabIndex = 12
        Me.txtTotalDepositado.TabStop = False
        Me.txtTotalDepositado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDepositado.Texto = "0"
        '
        'TxtCantidadDepositado
        '
        Me.TxtCantidadDepositado.Color = System.Drawing.Color.Empty
        Me.TxtCantidadDepositado.Decimales = True
        Me.TxtCantidadDepositado.Indicaciones = Nothing
        Me.TxtCantidadDepositado.Location = New System.Drawing.Point(959, 487)
        Me.TxtCantidadDepositado.Margin = New System.Windows.Forms.Padding(5)
        Me.TxtCantidadDepositado.Name = "TxtCantidadDepositado"
        Me.TxtCantidadDepositado.Size = New System.Drawing.Size(65, 27)
        Me.TxtCantidadDepositado.SoloLectura = True
        Me.TxtCantidadDepositado.TabIndex = 13
        Me.TxtCantidadDepositado.TabStop = False
        Me.TxtCantidadDepositado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TxtCantidadDepositado.Texto = "0"
        '
        'txtTotalChequeLocal
        '
        Me.txtTotalChequeLocal.Color = System.Drawing.Color.Empty
        Me.txtTotalChequeLocal.Decimales = True
        Me.txtTotalChequeLocal.Indicaciones = Nothing
        Me.txtTotalChequeLocal.Location = New System.Drawing.Point(507, 517)
        Me.txtTotalChequeLocal.Margin = New System.Windows.Forms.Padding(5)
        Me.txtTotalChequeLocal.Name = "txtTotalChequeLocal"
        Me.txtTotalChequeLocal.Size = New System.Drawing.Size(137, 27)
        Me.txtTotalChequeLocal.SoloLectura = True
        Me.txtTotalChequeLocal.TabIndex = 6
        Me.txtTotalChequeLocal.TabStop = False
        Me.txtTotalChequeLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalChequeLocal.Texto = "0"
        '
        'txtTotalEfectivo
        '
        Me.txtTotalEfectivo.Color = System.Drawing.Color.Empty
        Me.txtTotalEfectivo.Decimales = True
        Me.txtTotalEfectivo.Indicaciones = Nothing
        Me.txtTotalEfectivo.Location = New System.Drawing.Point(507, 486)
        Me.txtTotalEfectivo.Margin = New System.Windows.Forms.Padding(5)
        Me.txtTotalEfectivo.Name = "txtTotalEfectivo"
        Me.txtTotalEfectivo.Size = New System.Drawing.Size(137, 27)
        Me.txtTotalEfectivo.SoloLectura = True
        Me.txtTotalEfectivo.TabIndex = 4
        Me.txtTotalEfectivo.TabStop = False
        Me.txtTotalEfectivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalEfectivo.Texto = "0"
        '
        'txtTotalTarjeta
        '
        Me.txtTotalTarjeta.Color = System.Drawing.Color.Empty
        Me.txtTotalTarjeta.Decimales = True
        Me.txtTotalTarjeta.Indicaciones = Nothing
        Me.txtTotalTarjeta.Location = New System.Drawing.Point(507, 607)
        Me.txtTotalTarjeta.Margin = New System.Windows.Forms.Padding(5)
        Me.txtTotalTarjeta.Name = "txtTotalTarjeta"
        Me.txtTotalTarjeta.Size = New System.Drawing.Size(137, 27)
        Me.txtTotalTarjeta.SoloLectura = True
        Me.txtTotalTarjeta.TabIndex = 29
        Me.txtTotalTarjeta.TabStop = False
        Me.txtTotalTarjeta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalTarjeta.Texto = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(368, 613)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(93, 17)
        Me.Label3.TabIndex = 28
        Me.Label3.Text = "Total Tarjeta:"
        '
        'frmDepositoBancario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(1028, 699)
        Me.Controls.Add(Me.txtTotalTarjeta)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtDiferenciaCambio)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.txtRedondeo)
        Me.Controls.Add(Me.lblRedondeo)
        Me.Controls.Add(Me.txtTotalDocumento)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtTotalChequeOtro)
        Me.Controls.Add(Me.txtTotalDepositado)
        Me.Controls.Add(Me.TxtCantidadDepositado)
        Me.Controls.Add(Me.txtTotalChequeLocal)
        Me.Controls.Add(Me.txtTotalEfectivo)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.lblTotalChqOtros)
        Me.Controls.Add(Me.lblTotalChqLocal)
        Me.Controls.Add(Me.lblTotalEfectivo)
        Me.Controls.Add(Me.lblTotalDepositado)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.gbxComprobantes)
        Me.Controls.Add(Me.GroupBox1)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmDepositoBancario"
        Me.Text = "frmDeposito Bancario"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gbxComprobantes.ResumeLayout(False)
        Me.gbxComprobantes.PerformLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents cbxCuenta As ERP.ocxCBX
    Friend WithEvents txtBanco As ERP.ocxTXTString
    Friend WithEvents txtMoneda As ERP.ocxTXTString
    Friend WithEvents txtfecha As ERP.ocxTXTDate
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents lblObsrevacion As System.Windows.Forms.Label
    Friend WithEvents lblCambio As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents gbxComprobantes As System.Windows.Forms.GroupBox
    Friend WithEvents lklEliminarVenta As System.Windows.Forms.LinkLabel
    Friend WithEvents btnAgregarEfectivo As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lblTotalDepositado As System.Windows.Forms.Label
    Friend WithEvents lblTotalEfectivo As System.Windows.Forms.Label
    Friend WithEvents lblTotalChqLocal As System.Windows.Forms.Label
    Friend WithEvents lblTotalChqOtros As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnAgregarCheques As System.Windows.Forms.Button
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents txtTotalDepositado As ERP.ocxTXTNumeric
    Friend WithEvents TxtCantidadDepositado As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalChequeLocal As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalEfectivo As ERP.ocxTXTNumeric
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents txtCambio As ERP.ocxTXTNumeric
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents txtTotalChequeOtro As ERP.ocxTXTNumeric
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents btnAgregarDocumentos As System.Windows.Forms.Button
    Friend WithEvents txtTotalDocumento As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnRedondear As System.Windows.Forms.Button
    Friend WithEvents txtRedondeo As ERP.ocxTXTNumeric
    Friend WithEvents lblRedondeo As System.Windows.Forms.Label
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents txtDiferenciaCambio As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents colIDTransaccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBanco As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNroComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCotizacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCuentaContable As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnAgregarTarjetas As System.Windows.Forms.Button
    Friend WithEvents txtTotalTarjeta As ERP.ocxTXTNumeric
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class

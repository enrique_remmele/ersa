﻿Public Class frmModificarCantidadAbastecimiento
    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private PesoValue As Decimal
    Public Property Peso() As Decimal
        Get
            Return PesoValue
        End Get
        Set(ByVal value As Decimal)
            PesoValue = value
        End Set
    End Property

    Private PesoUnitarioValue As Decimal
    Public Property PesoUnitario() As Decimal
        Get
            Return PesoUnitarioValue
        End Get
        Set(ByVal value As Decimal)
            PesoUnitarioValue = value
        End Set
    End Property

    Private IDTransaccionValue As Decimal
    Public Property IDTransaccion() As Decimal
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Decimal)
            IDTransaccionValue = value
        End Set
    End Property

    Private CantidadSolicidadaValue As Decimal
    Public Property CantidadSolicidada() As Decimal
        Get
            Return CantidadSolicidadaValue
        End Get
        Set(ByVal value As Decimal)
            CantidadSolicidadaValue = value
        End Set
    End Property

    Private CantidadEntregarValue As Decimal
    Public Property CantidadEntregar() As Decimal
        Get
            Return CantidadEntregarValue
        End Get
        Set(ByVal value As Decimal)
            CantidadEntregarValue = value
        End Set
    End Property

    Private ProductoValue As String
    Public Property Producto() As String
        Get
            Return ProductoValue
        End Get
        Set(ByVal value As String)
            ProductoValue = value
        End Set
    End Property

    Sub Inicializar()
        txtProducto.txt.Text = Producto
        txtPeso.txt.Text = Peso
        txtCantidadSolicitada.txt.Text = CantidadSolicidada
        txtCantidadEntregar.txt.Text = CantidadSolicidada
    End Sub

    Sub Aceptar()
        If txtCantidadEntregar.ObtenerValor > txtCantidadSolicitada.ObtenerValor Then
            MessageBox.Show("Cantidad a Entregar no puede ser mayor a la solicitada")
            txtCantidadEntregar.txt.Text = 0
            Exit Sub
        End If
    End Sub


    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Aceptar()
    End Sub

    Private Sub txtCantidadEntregar_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtCantidadEntregar.TeclaPrecionada
        If txtCantidadEntregar.ObtenerValor > 0 And txtPeso.ObtenerValor > 0 Then
            txtPesoTotal.txt.Text = txtCantidadEntregar.ObtenerValor * PesoUnitario
        End If
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmModificarCantidadAbastecimiento_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmAsignacionAbastecimiento
    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property
    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set

    End Property

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Foco
        Me.ActiveControl = txtNroComprobante
        txtDesde.Hoy()
        txtHasta.UltimoDiaSemana()
        txtDesdeDetalle.Hoy()

    End Sub

    Sub CargarComprobante(Optional Where As String = "")

        dgvComprobante.Rows.Clear()

        Dim SQL As String = "Select IDTransaccion, Fec, TipoComprobante, NroComprobante, IDDeposito, Deposito, Observacion from VOrdenDePedido  where PedidoCliente = 'False' and Anulado = 'False' and IDSucursal = " & IDSucursal & ""
        If Where <> "" Then
            SQL = SQL & " And " & Where
        End If
        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        For Each oRow As DataRow In dt.Rows
            Dim Registro(dgvComprobante.Columns.Count - 1) As String
            Registro(0) = oRow("IDTransaccion")
            Registro(1) = False
            Registro(2) = oRow("Fec")
            Registro(3) = oRow("TipoComprobante")
            Registro(4) = oRow("NroComprobante")
            Registro(5) = oRow("IDDeposito")
            Registro(6) = oRow("Deposito")
            Registro(7) = oRow("Observacion")

            dgvComprobante.Rows.Add(Registro)

        Next

        'Formato
        dgvComprobante.Columns("Sel").ReadOnly = False
        dgvComprobante.Columns("IDTransaccion").Visible = False
        dgvComprobante.Columns("IDDeposito").Visible = False
        dgvComprobante.Columns("Observacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        'Totales
        txtCantidadComprobante.SetValue(dgvComprobante.RowCount)

        If dgvComprobante.Rows.Count > 0 Then
            Me.ActiveControl = dgvComprobante
        End If

    End Sub

    Sub CargarProductos()


        Dim SQL As String = "select IDTransaccion, ID, IDProducto, Producto, 'Cantidad'=CantidadAEntregar,'Fecha Entrega'= FecEntrega, 'Comprobante'=Concat(Cod,'-',NroComprobante), 'Peso'=(CantidadAEntregar*Peso), Saldo, 'PesoUnitario'=Peso from VDetalleOrdenDePedido  "
        Dim Where As String = ""
        Dim GroupBy As String = " "

        For Each oRow As DataGridViewRow In dgvComprobante.Rows

            If oRow.Cells("Sel").Value = True Then
                If Where = "" Then
                    Where = " Where (IDTransaccion=" & oRow.Cells("IDTransaccion").Value & " and cast(FechaEntrega as date) = '" & CSistema.FormatoFechaBaseDatos(txtDesdeDetalle.txt.Text, True, False) & "') "
                Else
                    Where = Where & " Or (IDTransaccion=" & oRow.Cells("IDTransaccion").Value & " and cast(FechaEntrega as date) = '" & CSistema.FormatoFechaBaseDatos(txtDesdeDetalle.txt.Text, True, False) & "') "
                End If
            End If
        Next

        If Where = "" Then
            dt.Rows.Clear()
        Else
            Where = Where & " and Saldo > 0"
            dt = CSistema.ExecuteToDataTable(SQL & Where & GroupBy)
        End If

        CSistema.dtToGrid(dgvDetalle, dt)

        'Formato
        dgvDetalle.Columns("IDTransaccion").Visible = False
        dgvDetalle.Columns("ID").Visible = False
        dgvDetalle.Columns("IDProducto").Visible = False
        dgvDetalle.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvDetalle.Columns("Cantidad").DefaultCellStyle.Format = "N2"
        dgvDetalle.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Peso").DefaultCellStyle.Format = "N2"
        dgvDetalle.Columns("Peso").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Cantidad").ReadOnly = False
        dgvDetalle.Columns("Saldo").DefaultCellStyle.Format = "N2"
        dgvDetalle.Columns("Saldo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDetalle.Columns("Saldo").ReadOnly = True
        dgvDetalle.Columns("Producto").ReadOnly = True
        dgvDetalle.Columns("Cantidad").ReadOnly = False
        dgvDetalle.Columns("Peso").ReadOnly = True
        dgvDetalle.Columns("PesoUnitario").Visible = False

        If dgvDetalle.Rows.Count > 0 Then
            Me.ActiveControl = dgvComprobante
        End If

        'Totales
        txtCantidadDetalle.SetValue(dgvDetalle.RowCount)
        txtPesoTotal.txt.Text = CSistema.FormatoMoneda(CSistema.dtSumColumn(dt, "Peso"), True)

    End Sub

    Sub Aceptar()

        Procesado = True
        Me.Close()

    End Sub

    Sub Cancelar()

        Procesado = False
        Me.Close()

    End Sub

    Sub ModificarCantidad()
        'Validar
        If dgvDetalle.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a modificar!"
            ctrError.SetError(dgvDetalle, mensaje)
            ctrError.SetIconAlignment(dgvDetalle, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim frm As New frmModificarCantidadAbastecimiento
        frm.Text = "Modificar Cantidad"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
        frm.Producto = dgvDetalle.Rows(dgvDetalle.CurrentCell.RowIndex).Cells("Producto").Value

        frm.Peso = CSistema.FormatoMoneda(dgvDetalle.Rows(dgvDetalle.CurrentCell.RowIndex).Cells("Peso").Value, True)
        frm.CantidadSolicidada = CSistema.FormatoMoneda(dgvDetalle.Rows(dgvDetalle.CurrentCell.RowIndex).Cells("Cantidad").Value, True)
        frm.CantidadEntregar = CSistema.FormatoMoneda(dgvDetalle.Rows(dgvDetalle.CurrentCell.RowIndex).Cells("cantidad").Value, True)
        frm.ID = dgvDetalle.Rows(dgvDetalle.CurrentCell.RowIndex).Cells("ID").Value
        frm.IDTransaccion = dgvDetalle.Rows(dgvDetalle.CurrentCell.RowIndex).Cells("IDTransaccion").Value
        frm.PesoUnitario = CSistema.FormatoMoneda(dgvDetalle.Rows(dgvDetalle.CurrentCell.RowIndex).Cells("Peso").Value, True) / CSistema.FormatoMoneda(dgvDetalle.Rows(dgvDetalle.CurrentCell.RowIndex).Cells("Cantidad").Value, True)
        frm.Inicializar()
        frm.Show()

    End Sub

    Sub EliminarProducto()
        'If IDTransaccion > 0 Then
        '    Exit Sub
        'End If

        'Validar
        If dgvDetalle.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgvDetalle, mensaje)
            ctrError.SetIconAlignment(dgvDetalle, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'dt.Rows(dgvDetalle.SelectedRows(0).Index).Delete()
        dt.Rows.RemoveAt(dgvDetalle.SelectedRows(0).Index)

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dt.Rows
            'oRow("ID") = Index
            'Index = Index + 1
        Next
        txtCantidadDetalle.SetValue(dgvDetalle.RowCount)
        txtPesoTotal.txt.Text = CSistema.FormatoMoneda(CSistema.dtSumColumn(dt, "Peso"), True)
        'ListarGastoAdicional()

    End Sub

    Private Sub frmRemisionCargarFactura_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If Me.ActiveControl.Name = txtNroComprobante.Name Then
            Exit Sub
        End If

        If Me.ActiveControl.Name = dgvComprobante.Name Then
            Exit Sub
        End If

        If Me.ActiveControl.Name = dgvDetalle.Name Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmRemisionCargarFactura_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCargarComprobante_Click(sender As System.Object, e As System.EventArgs) Handles btnCargarComprobante.Click
        CargarComprobante(" cast(Fecha as date) Between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' ")
    End Sub

    Private Sub lklComprobantesMes_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklComprobantesMes.LinkClicked
        CargarComprobante(" Month(Fecha)=" & Date.Now.Month & " And Year(Fecha)=" & Date.Now.Year & " ")
    End Sub

    Private Sub lklSeleccionarTodo_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklSeleccionarTodo.LinkClicked

        If lklSeleccionarTodo.Text = "Seleccionar todo" Then
            For Each oRow As DataGridViewRow In dgvComprobante.Rows
                oRow.Cells("Sel").Value = True
            Next
            lklSeleccionarTodo.Text = "Quitar todo"
            CargarProductos()
            Exit Sub
        End If

        If lklSeleccionarTodo.Text = "Quitar todo" Then
            For Each oRow As DataGridViewRow In dgvComprobante.Rows
                oRow.Cells("Sel").Value = False
            Next
            lklSeleccionarTodo.Text = "Seleccionar todo"
            CargarProductos()
            Exit Sub
        End If

    End Sub

    Private Sub btnCargarMercaderias_Click(sender As System.Object, e As System.EventArgs) Handles btnModificarCantidad.Click
        ModificarCantidad()
    End Sub

    Private Sub lklQuitarSeleccionados_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklQuitarSeleccionados.LinkClicked
        EliminarProducto()
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Aceptar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then

            If txtNroComprobante.GetValue = "" Then
                Exit Sub
            End If

            CargarComprobante(" NroComprobante Like '%" & txtNroComprobante.GetValue & "%'")
        End If
    End Sub

    Private Sub dgvComprobante_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvComprobante.CellClick

    End Sub

    Private Sub dgvComprobante_Click(sender As Object, e As System.EventArgs) Handles dgvComprobante.Click
        dgvComprobante.SelectedRows(0).Cells("Sel").Value = CStr(Not CBool(dgvComprobante.SelectedRows(0).Cells("Sel").Value))
        CargarProductos()
    End Sub

    Private Sub dgvComprobante_DoubleClick(sender As Object, e As System.EventArgs) Handles dgvComprobante.DoubleClick

    End Sub

    Private Sub dgvComprobante_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvComprobante.KeyDown

        If e.KeyCode = Keys.Enter Then

            'MArcar
            If dgvComprobante.SelectedRows.Count > 0 Then

                dgvComprobante.SelectedRows(0).Cells("Sel").Value = CStr(Not CBool(dgvComprobante.SelectedRows(0).Cells("Sel").Value))

                If dgvComprobante.SelectedRows(0).Index <> dgvComprobante.Rows.Count - 1 Then
                    dgvComprobante.CurrentCell = dgvComprobante.SelectedRows(0).Cells("Sel")
                End If

                CargarProductos()
            End If
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub dgvDetalle_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvDetalle.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If
    End Sub


    Private Sub dgvDetalle_CellEndEdit(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellEndEdit
        'Que el valor sea mayor a 0
        If CDec(dgvDetalle.CurrentCell.Value) <= 0 Then

            Dim mensaje As String = "Cantidad debe ser mayor a 0!"
            ctrError.SetError(dgvDetalle, mensaje)
            ctrError.SetIconAlignment(dgvDetalle, ErrorIconAlignment.TopRight)
            tsslEstado.Text = mensaje

            dgvDetalle.CurrentCell.Value = dgvDetalle.CurrentRow.Cells("Saldo").Value
            Exit Sub
        End If

        If CDec(dgvDetalle.CurrentRow.Cells("Saldo").Value) < CDec(dgvDetalle.CurrentRow.Cells("Cantidad").Value) Then
            Dim mensaje As String = "La cantidad no puede ser mayor al saldo!"
            ctrError.SetError(dgvDetalle, mensaje)
            ctrError.SetIconAlignment(dgvDetalle, ErrorIconAlignment.TopRight)
            tsslEstado.Text = mensaje
            MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            dgvDetalle.CurrentCell.Value = dgvDetalle.CurrentRow.Cells("Saldo").Value
            Exit Sub
        End If

        dgvDetalle.CurrentCell.Value = dgvDetalle.CurrentRow.Cells("Cantidad").Value
        dgvDetalle.CurrentRow.Cells("Peso").Value = dgvDetalle.CurrentCell.Value * dgvDetalle.CurrentRow.Cells("PesoUnitario").Value
        txtPesoTotal.txt.Text = CSistema.FormatoMoneda(CSistema.dtSumColumn(dt, "Peso"), True)
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmAsignacionAbastecimiento_Activate()
        Me.Refresh()
    End Sub

End Class
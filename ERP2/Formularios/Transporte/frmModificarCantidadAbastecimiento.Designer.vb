﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificarCantidadAbastecimiento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtCantidadSolicitada = New ERP.ocxTXTNumeric()
        Me.txtCantidadEntregar = New ERP.ocxTXTNumeric()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtProducto = New ERP.ocxTXTString()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPeso = New ERP.ocxTXTNumeric()
        Me.txtPesoTotal = New ERP.ocxTXTNumeric()
        Me.SuspendLayout()
        '
        'txtCantidadSolicitada
        '
        Me.txtCantidadSolicitada.Color = System.Drawing.Color.Empty
        Me.txtCantidadSolicitada.Decimales = True
        Me.txtCantidadSolicitada.Indicaciones = Nothing
        Me.txtCantidadSolicitada.Location = New System.Drawing.Point(123, 68)
        Me.txtCantidadSolicitada.Name = "txtCantidadSolicitada"
        Me.txtCantidadSolicitada.Size = New System.Drawing.Size(98, 21)
        Me.txtCantidadSolicitada.SoloLectura = True
        Me.txtCantidadSolicitada.TabIndex = 0
        Me.txtCantidadSolicitada.TabStop = False
        Me.txtCantidadSolicitada.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadSolicitada.Texto = "0"
        '
        'txtCantidadEntregar
        '
        Me.txtCantidadEntregar.Color = System.Drawing.Color.Empty
        Me.txtCantidadEntregar.Decimales = True
        Me.txtCantidadEntregar.Indicaciones = Nothing
        Me.txtCantidadEntregar.Location = New System.Drawing.Point(123, 94)
        Me.txtCantidadEntregar.Name = "txtCantidadEntregar"
        Me.txtCantidadEntregar.Size = New System.Drawing.Size(98, 21)
        Me.txtCantidadEntregar.SoloLectura = False
        Me.txtCantidadEntregar.TabIndex = 1
        Me.txtCantidadEntregar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadEntregar.Texto = "0"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(123, 121)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 2
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(205, 121)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 68)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Cantidad Solicitada"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 101)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(98, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Cantida a Entregar:"
        '
        'txtProducto
        '
        Me.txtProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProducto.Color = System.Drawing.Color.Empty
        Me.txtProducto.Indicaciones = Nothing
        Me.txtProducto.Location = New System.Drawing.Point(16, 12)
        Me.txtProducto.Multilinea = False
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(310, 27)
        Me.txtProducto.SoloLectura = True
        Me.txtProducto.TabIndex = 6
        Me.txtProducto.TabStop = False
        Me.txtProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtProducto.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(80, 47)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Peso"
        '
        'txtPeso
        '
        Me.txtPeso.Color = System.Drawing.Color.Empty
        Me.txtPeso.Decimales = True
        Me.txtPeso.Indicaciones = Nothing
        Me.txtPeso.Location = New System.Drawing.Point(123, 41)
        Me.txtPeso.Name = "txtPeso"
        Me.txtPeso.Size = New System.Drawing.Size(51, 21)
        Me.txtPeso.SoloLectura = True
        Me.txtPeso.TabIndex = 8
        Me.txtPeso.TabStop = False
        Me.txtPeso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPeso.Texto = "0"
        '
        'txtPesoTotal
        '
        Me.txtPesoTotal.Color = System.Drawing.Color.Empty
        Me.txtPesoTotal.Decimales = True
        Me.txtPesoTotal.Indicaciones = Nothing
        Me.txtPesoTotal.Location = New System.Drawing.Point(228, 94)
        Me.txtPesoTotal.Name = "txtPesoTotal"
        Me.txtPesoTotal.Size = New System.Drawing.Size(98, 21)
        Me.txtPesoTotal.SoloLectura = True
        Me.txtPesoTotal.TabIndex = 9
        Me.txtPesoTotal.TabStop = False
        Me.txtPesoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPesoTotal.Texto = "0"
        '
        'frmModificarCantidadAbastecimiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(339, 157)
        Me.Controls.Add(Me.txtPesoTotal)
        Me.Controls.Add(Me.txtPeso)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtProducto)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.txtCantidadEntregar)
        Me.Controls.Add(Me.txtCantidadSolicitada)
        Me.Name = "frmModificarCantidadAbastecimiento"
        Me.Text = "frmModificarCantidadAbastecimiento"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtCantidadSolicitada As ERP.ocxTXTNumeric
    Friend WithEvents txtCantidadEntregar As ERP.ocxTXTNumeric
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtProducto As ERP.ocxTXTString
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPeso As ERP.ocxTXTNumeric
    Friend WithEvents txtPesoTotal As ERP.ocxTXTNumeric
End Class

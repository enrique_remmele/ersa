﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAsignacionCamion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtPesoTotal = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCantidadComprobante = New ERP.ocxTXTNumeric()
        Me.lblCantidadComprobante = New System.Windows.Forms.Label()
        Me.txtCantidadMaximaProductos = New ERP.ocxTXTNumeric()
        Me.lblCantidadDetalle = New System.Windows.Forms.Label()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.dgvLista = New System.Windows.Forms.DataGridView()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAgregarPor = New System.Windows.Forms.Label()
        Me.btnAbastecimiento = New System.Windows.Forms.Button()
        Me.txtAgregar2 = New System.Windows.Forms.Label()
        Me.btnAbastecimiento2 = New System.Windows.Forms.Button()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.cbxTransporte = New ERP.ocxCBX()
        Me.cbxChofer = New ERP.ocxCBX()
        Me.lblChofer = New System.Windows.Forms.Label()
        Me.cbxVehiculo = New ERP.ocxCBX()
        Me.lblVehiculo = New System.Windows.Forms.Label()
        Me.cbxDistribuidor = New ERP.ocxCBX()
        Me.lblDistribuidor = New System.Windows.Forms.Label()
        Me.cbxSucursalOperacion = New ERP.ocxCBX()
        Me.lblSucursalOperacion = New System.Windows.Forms.Label()
        Me.txtDetalle = New ERP.ocxTXTString()
        Me.lblDetalle = New System.Windows.Forms.Label()
        Me.txtFechaInicio = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblTransporte = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.gbxCabecera.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 3)
        Me.lblUsuarioRegistro.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 15)
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 566)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(757, 20)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 3)
        Me.lblAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.txtPesoTotal)
        Me.FlowLayoutPanel3.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtCantidadComprobante)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblCantidadComprobante)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtCantidadMaximaProductos)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblCantidadDetalle)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 473)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(751, 24)
        Me.FlowLayoutPanel3.TabIndex = 7
        '
        'txtPesoTotal
        '
        Me.txtPesoTotal.Color = System.Drawing.Color.Empty
        Me.txtPesoTotal.Decimales = True
        Me.txtPesoTotal.Indicaciones = Nothing
        Me.txtPesoTotal.Location = New System.Drawing.Point(625, 3)
        Me.txtPesoTotal.Name = "txtPesoTotal"
        Me.txtPesoTotal.Size = New System.Drawing.Size(123, 21)
        Me.txtPesoTotal.SoloLectura = True
        Me.txtPesoTotal.TabIndex = 9
        Me.txtPesoTotal.TabStop = False
        Me.txtPesoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPesoTotal.Texto = "0"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(524, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 26)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Peso:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCantidadComprobante
        '
        Me.txtCantidadComprobante.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobante.Decimales = True
        Me.txtCantidadComprobante.Indicaciones = Nothing
        Me.txtCantidadComprobante.Location = New System.Drawing.Point(451, 3)
        Me.txtCantidadComprobante.Name = "txtCantidadComprobante"
        Me.txtCantidadComprobante.Size = New System.Drawing.Size(67, 21)
        Me.txtCantidadComprobante.SoloLectura = True
        Me.txtCantidadComprobante.TabIndex = 5
        Me.txtCantidadComprobante.TabStop = False
        Me.txtCantidadComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobante.Texto = "0"
        '
        'lblCantidadComprobante
        '
        Me.lblCantidadComprobante.Location = New System.Drawing.Point(267, 0)
        Me.lblCantidadComprobante.Name = "lblCantidadComprobante"
        Me.lblCantidadComprobante.Size = New System.Drawing.Size(178, 26)
        Me.lblCantidadComprobante.TabIndex = 4
        Me.lblCantidadComprobante.Text = "Cantidad:"
        Me.lblCantidadComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCantidadMaximaProductos
        '
        Me.txtCantidadMaximaProductos.Color = System.Drawing.Color.Empty
        Me.txtCantidadMaximaProductos.Decimales = True
        Me.txtCantidadMaximaProductos.Indicaciones = Nothing
        Me.txtCantidadMaximaProductos.Location = New System.Drawing.Point(205, 3)
        Me.txtCantidadMaximaProductos.Name = "txtCantidadMaximaProductos"
        Me.txtCantidadMaximaProductos.Size = New System.Drawing.Size(56, 21)
        Me.txtCantidadMaximaProductos.SoloLectura = True
        Me.txtCantidadMaximaProductos.TabIndex = 7
        Me.txtCantidadMaximaProductos.TabStop = False
        Me.txtCantidadMaximaProductos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadMaximaProductos.Texto = "0"
        '
        'lblCantidadDetalle
        '
        Me.lblCantidadDetalle.Location = New System.Drawing.Point(16, 0)
        Me.lblCantidadDetalle.Name = "lblCantidadDetalle"
        Me.lblCantidadDetalle.Size = New System.Drawing.Size(183, 26)
        Me.lblCantidadDetalle.TabIndex = 6
        Me.lblCantidadDetalle.Text = "Cantidad maxima de productos:"
        Me.lblCantidadDetalle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.ForeColor = System.Drawing.SystemColors.ControlText
        Me.flpAnuladoPor.Location = New System.Drawing.Point(364, 3)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(355, 20)
        Me.flpAnuladoPor.TabIndex = 0
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 3)
        Me.lblUsuarioAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 3)
        Me.lblFechaAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 3)
        Me.lblFechaRegistro.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLista.Location = New System.Drawing.Point(3, 176)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.Size = New System.Drawing.Size(751, 291)
        Me.dgvLista.TabIndex = 6
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.lblAgregarPor)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnAbastecimiento)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtAgregar2)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnAbastecimiento2)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 139)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(751, 31)
        Me.FlowLayoutPanel2.TabIndex = 2
        '
        'lblAgregarPor
        '
        Me.lblAgregarPor.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAgregarPor.Location = New System.Drawing.Point(3, 0)
        Me.lblAgregarPor.Name = "lblAgregarPor"
        Me.lblAgregarPor.Size = New System.Drawing.Size(65, 26)
        Me.lblAgregarPor.TabIndex = 5
        Me.lblAgregarPor.Text = "Pedido de:"
        Me.lblAgregarPor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnAbastecimiento
        '
        Me.btnAbastecimiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAbastecimiento.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnAbastecimiento.Location = New System.Drawing.Point(74, 3)
        Me.btnAbastecimiento.Name = "btnAbastecimiento"
        Me.btnAbastecimiento.Size = New System.Drawing.Size(99, 23)
        Me.btnAbastecimiento.TabIndex = 7
        Me.btnAbastecimiento.Text = "Abastecimiento"
        Me.btnAbastecimiento.UseVisualStyleBackColor = True
        '
        'txtAgregar2
        '
        Me.txtAgregar2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAgregar2.Location = New System.Drawing.Point(179, 0)
        Me.txtAgregar2.Name = "txtAgregar2"
        Me.txtAgregar2.Size = New System.Drawing.Size(187, 26)
        Me.txtAgregar2.TabIndex = 10
        Me.txtAgregar2.Text = " Agregar Pedido de:"
        Me.txtAgregar2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnAbastecimiento2
        '
        Me.btnAbastecimiento2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAbastecimiento2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnAbastecimiento2.Location = New System.Drawing.Point(372, 3)
        Me.btnAbastecimiento2.Name = "btnAbastecimiento2"
        Me.btnAbastecimiento2.Size = New System.Drawing.Size(99, 23)
        Me.btnAbastecimiento2.TabIndex = 11
        Me.btnAbastecimiento2.Text = "Abastecimiento"
        Me.btnAbastecimiento2.UseVisualStyleBackColor = True
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.ForeColor = System.Drawing.SystemColors.ControlText
        Me.flpRegistradoPor.Location = New System.Drawing.Point(3, 3)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(355, 20)
        Me.flpRegistradoPor.TabIndex = 1
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 3)
        Me.lblRegistradoPor.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.StatusStrip1, 0, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxCabecera, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvLista, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 4)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 8
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 128.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(757, 586)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnAnular)
        Me.Panel2.Controls.Add(Me.btnEliminar)
        Me.Panel2.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Panel2.Controls.Add(Me.btnImprimir)
        Me.Panel2.Controls.Add(Me.btnSalir)
        Me.Panel2.Controls.Add(Me.btnCancelar)
        Me.Panel2.Controls.Add(Me.btnGuardar)
        Me.Panel2.Controls.Add(Me.btnNuevo)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 533)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(751, 30)
        Me.Panel2.TabIndex = 4
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(224, 4)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 10
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(224, 3)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 9
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        Me.btnEliminar.Visible = False
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBusquedaAvanzada.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(0, 3)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(126, 23)
        Me.btnBusquedaAvanzada.TabIndex = 0
        Me.btnBusquedaAvanzada.Text = "&Busqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnImprimir.Location = New System.Drawing.Point(132, 3)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 1
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnSalir.Location = New System.Drawing.Point(672, 3)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 5
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnCancelar.Location = New System.Drawing.Point(555, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 4
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnGuardar.Location = New System.Drawing.Point(474, 3)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 3
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnNuevo.Location = New System.Drawing.Point(393, 3)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 2
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'gbxCabecera
        '
        Me.gbxCabecera.BackColor = System.Drawing.Color.Transparent
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Controls.Add(Me.cbxTransporte)
        Me.gbxCabecera.Controls.Add(Me.cbxChofer)
        Me.gbxCabecera.Controls.Add(Me.lblChofer)
        Me.gbxCabecera.Controls.Add(Me.cbxVehiculo)
        Me.gbxCabecera.Controls.Add(Me.lblVehiculo)
        Me.gbxCabecera.Controls.Add(Me.cbxDistribuidor)
        Me.gbxCabecera.Controls.Add(Me.lblDistribuidor)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursalOperacion)
        Me.gbxCabecera.Controls.Add(Me.lblSucursalOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtDetalle)
        Me.gbxCabecera.Controls.Add(Me.lblDetalle)
        Me.gbxCabecera.Controls.Add(Me.txtFechaInicio)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.lblTransporte)
        Me.gbxCabecera.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxCabecera.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbxCabecera.Location = New System.Drawing.Point(3, 11)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(751, 122)
        Me.gbxCabecera.TabIndex = 1
        Me.gbxCabecera.TabStop = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(68, 7)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(63, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 28
        Me.cbxSucursal.Texto = ""
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(326, 9)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(68, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 31
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(402, 9)
        Me.txtComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(97, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 32
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(132, 7)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(97, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 29
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(264, 13)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(49, 13)
        Me.lblComprobante.TabIndex = 30
        Me.lblComprobante.Text = "Compro.:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(3, 11)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 27
        Me.lblOperacion.Text = "Operación:"
        '
        'cbxTransporte
        '
        Me.cbxTransporte.CampoWhere = Nothing
        Me.cbxTransporte.CargarUnaSolaVez = False
        Me.cbxTransporte.DataDisplayMember = ""
        Me.cbxTransporte.DataFilter = Nothing
        Me.cbxTransporte.DataOrderBy = ""
        Me.cbxTransporte.DataSource = ""
        Me.cbxTransporte.DataValueMember = ""
        Me.cbxTransporte.dtSeleccionado = Nothing
        Me.cbxTransporte.FormABM = Nothing
        Me.cbxTransporte.Indicaciones = Nothing
        Me.cbxTransporte.Location = New System.Drawing.Point(326, 37)
        Me.cbxTransporte.Name = "cbxTransporte"
        Me.cbxTransporte.SeleccionMultiple = False
        Me.cbxTransporte.SeleccionObligatoria = True
        Me.cbxTransporte.Size = New System.Drawing.Size(235, 21)
        Me.cbxTransporte.SoloLectura = False
        Me.cbxTransporte.TabIndex = 14
        Me.cbxTransporte.Texto = ""
        '
        'cbxChofer
        '
        Me.cbxChofer.CampoWhere = Nothing
        Me.cbxChofer.CargarUnaSolaVez = False
        Me.cbxChofer.DataDisplayMember = "Nombres"
        Me.cbxChofer.DataFilter = Nothing
        Me.cbxChofer.DataOrderBy = "Nombres"
        Me.cbxChofer.DataSource = "VChofer"
        Me.cbxChofer.DataValueMember = "ID"
        Me.cbxChofer.dtSeleccionado = Nothing
        Me.cbxChofer.FormABM = Nothing
        Me.cbxChofer.Indicaciones = Nothing
        Me.cbxChofer.Location = New System.Drawing.Point(548, 64)
        Me.cbxChofer.Name = "cbxChofer"
        Me.cbxChofer.SeleccionMultiple = False
        Me.cbxChofer.SeleccionObligatoria = True
        Me.cbxChofer.Size = New System.Drawing.Size(195, 21)
        Me.cbxChofer.SoloLectura = False
        Me.cbxChofer.TabIndex = 20
        Me.cbxChofer.Texto = ""
        '
        'lblChofer
        '
        Me.lblChofer.AutoSize = True
        Me.lblChofer.Location = New System.Drawing.Point(505, 68)
        Me.lblChofer.Name = "lblChofer"
        Me.lblChofer.Size = New System.Drawing.Size(41, 13)
        Me.lblChofer.TabIndex = 19
        Me.lblChofer.Text = "Chofer:"
        '
        'cbxVehiculo
        '
        Me.cbxVehiculo.CampoWhere = Nothing
        Me.cbxVehiculo.CargarUnaSolaVez = False
        Me.cbxVehiculo.DataDisplayMember = "Descripcion"
        Me.cbxVehiculo.DataFilter = Nothing
        Me.cbxVehiculo.DataOrderBy = Nothing
        Me.cbxVehiculo.DataSource = "VCamion"
        Me.cbxVehiculo.DataValueMember = "ID"
        Me.cbxVehiculo.dtSeleccionado = Nothing
        Me.cbxVehiculo.FormABM = Nothing
        Me.cbxVehiculo.Indicaciones = Nothing
        Me.cbxVehiculo.Location = New System.Drawing.Point(362, 64)
        Me.cbxVehiculo.Name = "cbxVehiculo"
        Me.cbxVehiculo.SeleccionMultiple = False
        Me.cbxVehiculo.SeleccionObligatoria = True
        Me.cbxVehiculo.Size = New System.Drawing.Size(139, 21)
        Me.cbxVehiculo.SoloLectura = False
        Me.cbxVehiculo.TabIndex = 18
        Me.cbxVehiculo.Texto = ""
        '
        'lblVehiculo
        '
        Me.lblVehiculo.AutoSize = True
        Me.lblVehiculo.Location = New System.Drawing.Point(311, 68)
        Me.lblVehiculo.Name = "lblVehiculo"
        Me.lblVehiculo.Size = New System.Drawing.Size(51, 13)
        Me.lblVehiculo.TabIndex = 17
        Me.lblVehiculo.Text = "Vehiculo:"
        '
        'cbxDistribuidor
        '
        Me.cbxDistribuidor.CampoWhere = Nothing
        Me.cbxDistribuidor.CargarUnaSolaVez = False
        Me.cbxDistribuidor.DataDisplayMember = "Nombres"
        Me.cbxDistribuidor.DataFilter = Nothing
        Me.cbxDistribuidor.DataOrderBy = "Nombres"
        Me.cbxDistribuidor.DataSource = "VDistribuidor"
        Me.cbxDistribuidor.DataValueMember = "ID"
        Me.cbxDistribuidor.dtSeleccionado = Nothing
        Me.cbxDistribuidor.FormABM = Nothing
        Me.cbxDistribuidor.Indicaciones = Nothing
        Me.cbxDistribuidor.Location = New System.Drawing.Point(69, 64)
        Me.cbxDistribuidor.Name = "cbxDistribuidor"
        Me.cbxDistribuidor.SeleccionMultiple = False
        Me.cbxDistribuidor.SeleccionObligatoria = True
        Me.cbxDistribuidor.Size = New System.Drawing.Size(236, 21)
        Me.cbxDistribuidor.SoloLectura = False
        Me.cbxDistribuidor.TabIndex = 16
        Me.cbxDistribuidor.Texto = ""
        '
        'lblDistribuidor
        '
        Me.lblDistribuidor.AutoSize = True
        Me.lblDistribuidor.Location = New System.Drawing.Point(4, 68)
        Me.lblDistribuidor.Name = "lblDistribuidor"
        Me.lblDistribuidor.Size = New System.Drawing.Size(62, 13)
        Me.lblDistribuidor.TabIndex = 15
        Me.lblDistribuidor.Text = "Distribuidor:"
        '
        'cbxSucursalOperacion
        '
        Me.cbxSucursalOperacion.CampoWhere = Nothing
        Me.cbxSucursalOperacion.CargarUnaSolaVez = False
        Me.cbxSucursalOperacion.DataDisplayMember = "Descripcion"
        Me.cbxSucursalOperacion.DataFilter = Nothing
        Me.cbxSucursalOperacion.DataOrderBy = "Descripcion"
        Me.cbxSucursalOperacion.DataSource = "VSucursal"
        Me.cbxSucursalOperacion.DataValueMember = "ID"
        Me.cbxSucursalOperacion.dtSeleccionado = Nothing
        Me.cbxSucursalOperacion.FormABM = Nothing
        Me.cbxSucursalOperacion.Indicaciones = Nothing
        Me.cbxSucursalOperacion.Location = New System.Drawing.Point(77, 37)
        Me.cbxSucursalOperacion.Name = "cbxSucursalOperacion"
        Me.cbxSucursalOperacion.SeleccionMultiple = False
        Me.cbxSucursalOperacion.SeleccionObligatoria = True
        Me.cbxSucursalOperacion.Size = New System.Drawing.Size(160, 21)
        Me.cbxSucursalOperacion.SoloLectura = False
        Me.cbxSucursalOperacion.TabIndex = 10
        Me.cbxSucursalOperacion.Texto = ""
        '
        'lblSucursalOperacion
        '
        Me.lblSucursalOperacion.AutoSize = True
        Me.lblSucursalOperacion.Location = New System.Drawing.Point(2, 41)
        Me.lblSucursalOperacion.Name = "lblSucursalOperacion"
        Me.lblSucursalOperacion.Size = New System.Drawing.Size(71, 13)
        Me.lblSucursalOperacion.TabIndex = 9
        Me.lblSucursalOperacion.Text = "Suc. Destino:"
        '
        'txtDetalle
        '
        Me.txtDetalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDetalle.Color = System.Drawing.Color.Empty
        Me.txtDetalle.Indicaciones = Nothing
        Me.txtDetalle.Location = New System.Drawing.Point(69, 90)
        Me.txtDetalle.Multilinea = False
        Me.txtDetalle.Name = "txtDetalle"
        Me.txtDetalle.Size = New System.Drawing.Size(671, 21)
        Me.txtDetalle.SoloLectura = False
        Me.txtDetalle.TabIndex = 26
        Me.txtDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDetalle.Texto = ""
        '
        'lblDetalle
        '
        Me.lblDetalle.AutoSize = True
        Me.lblDetalle.Location = New System.Drawing.Point(23, 94)
        Me.lblDetalle.Name = "lblDetalle"
        Me.lblDetalle.Size = New System.Drawing.Size(43, 13)
        Me.lblDetalle.TabIndex = 25
        Me.lblDetalle.Text = "Detalle:"
        '
        'txtFechaInicio
        '
        Me.txtFechaInicio.AñoFecha = 0
        Me.txtFechaInicio.Color = System.Drawing.Color.Empty
        Me.txtFechaInicio.Fecha = New Date(CType(0, Long))
        Me.txtFechaInicio.Location = New System.Drawing.Point(625, 11)
        Me.txtFechaInicio.MesFecha = 0
        Me.txtFechaInicio.Name = "txtFechaInicio"
        Me.txtFechaInicio.PermitirNulo = False
        Me.txtFechaInicio.Size = New System.Drawing.Size(77, 20)
        Me.txtFechaInicio.SoloLectura = False
        Me.txtFechaInicio.TabIndex = 3
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(580, 15)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Text = "Fecha:"
        '
        'lblTransporte
        '
        Me.lblTransporte.AutoSize = True
        Me.lblTransporte.Location = New System.Drawing.Point(260, 41)
        Me.lblTransporte.Name = "lblTransporte"
        Me.lblTransporte.Size = New System.Drawing.Size(61, 13)
        Me.lblTransporte.TabIndex = 13
        Me.lblTransporte.Text = "Transporte:"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.flpRegistradoPor)
        Me.FlowLayoutPanel1.Controls.Add(Me.flpAnuladoPor)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 503)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(751, 24)
        Me.FlowLayoutPanel1.TabIndex = 3
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmAsignacionCamion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(757, 586)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmAsignacionCamion"
        Me.Text = "frmAsignacionCamion"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtCantidadComprobante As ERP.ocxTXTNumeric
    Friend WithEvents lblCantidadComprobante As System.Windows.Forms.Label
    Friend WithEvents txtCantidadMaximaProductos As ERP.ocxTXTNumeric
    Friend WithEvents lblCantidadDetalle As System.Windows.Forms.Label
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents dgvLista As System.Windows.Forms.DataGridView
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblAgregarPor As System.Windows.Forms.Label
    Friend WithEvents btnAbastecimiento As System.Windows.Forms.Button
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents cbxTransporte As ERP.ocxCBX
    Friend WithEvents cbxChofer As ERP.ocxCBX
    Friend WithEvents lblChofer As System.Windows.Forms.Label
    Friend WithEvents cbxVehiculo As ERP.ocxCBX
    Friend WithEvents lblVehiculo As System.Windows.Forms.Label
    Friend WithEvents cbxDistribuidor As ERP.ocxCBX
    Friend WithEvents lblDistribuidor As System.Windows.Forms.Label
    Friend WithEvents cbxSucursalOperacion As ERP.ocxCBX
    Friend WithEvents lblSucursalOperacion As System.Windows.Forms.Label
    Friend WithEvents txtDetalle As ERP.ocxTXTString
    Friend WithEvents lblDetalle As System.Windows.Forms.Label
    Friend WithEvents txtFechaInicio As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lblTransporte As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtAgregar2 As System.Windows.Forms.Label
    Friend WithEvents btnAbastecimiento2 As System.Windows.Forms.Button
    Friend WithEvents txtPesoTotal As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class

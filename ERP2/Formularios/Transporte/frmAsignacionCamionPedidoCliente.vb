﻿Public Class frmAsignacionCamionPedidoCliente
    'CLASES
    Public CSistema As New CSistema
    Public CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    'Dim CReporte As New CReporteRemision

    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private NuevoIDTransaccionValue As Integer
    Public Property NuevoIDTransaccion() As Integer
        Get
            Return NuevoIDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            NuevoIDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private dtDetalleValue As DataTable
    Public Property dtDetalle() As DataTable
        Get
            Return dtDetalleValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleValue = value
        End Set
    End Property

    'VARIABLES
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Public dtPuntoExpedicion As DataTable
    Dim dtTransporte As DataTable

    Sub Inicializar()

        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Otros
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        'Propiedades
        IDTransaccion = 0
        vNuevo = False

        'Funciones
        Dim PrimeraVez As Boolean = False
        IDOperacion = CSistema.ObtenerIDOperacion("frmAsignacionCamion", "Carga de Camiones", "CCAM", CadenaConexion, PrimeraVez)
        CargarInformacion()

        'Clases

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        'CONFIGURACIONES
        txtFechaInicio.SetValue(VGFechaHoraSistema)
        If IsNumeric(vgConfiguraciones("RemisionCantidadLineas")) = False Then
            vgConfiguraciones("RemisionCantidadLineas") = 20
        End If

        txtCantidadMaximaProductos.SetValue(vgConfiguraciones("RemisionCantidadLineas").ToString)

        'Foco
        Me.ActiveControl = txtID
        txtID.txt.SelectAll()
        txtID.Enabled = True
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub


    Sub CargarInformacion()

        ReDim vControles(-1)

        'Cabecera

        CSistema.CargaControl(vControles, txtFechaInicio)
        CSistema.CargaControl(vControles, cbxSucursalDestino)
        CSistema.CargaControl(vControles, cbxTransporte)
        CSistema.CargaControl(vControles, cbxDistribuidor)
        CSistema.CargaControl(vControles, cbxVehiculo)
        CSistema.CargaControl(vControles, cbxChofer)
        CSistema.CargaControl(vControles, txtDetalle)

        'Detalle
        CSistema.CargaControl(vControles, btnPedido)

        dtDetalle = CData.GetStructure("VDetalleAsignacionCamion", "Select Top(0) * From VDetalleAsignacionCamion")

        'CARGAR CONTROLES
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, CData.GetTable("VTipoComprobante", " IDOperacion = " & IDOperacion), "ID", "Codigo")

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal", " "), "ID", "Codigo")
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", "")


        'Transporte
        'cbxTransporte.Conectar()
        dtTransporte = CSistema.ExecuteToDataTable("Select * From VTransporte Order By Descripcion").Copy
        CSistema.SqlToComboBox(cbxTransporte.cbx, dtTransporte, "ID", "Descripcion")


        'distrbuidor
        cbxDistribuidor.Conectar()

        'Vehiculo
        cbxVehiculo.Conectar()


        'Chofer
        cbxChofer.Conectar()
        'Fecha de Venta
        txtFechaInicio.SetValue(Now)
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From AsignacionCamion),1)"), Integer)
    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)
        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, New Button, vControles)
    End Sub

    Sub GuardarInformacion()

        'TIMBRADO
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.Texto)
    End Sub

    Sub Nuevo()
        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Vehiculo
        cbxVehiculo.Conectar(" Abastecimiento = 1 ")


        'Chofer
        cbxChofer.Conectar(" Abastecimiento = 1 ")

        'Limpiar cabecera
        'txtCliente.SetValue(Nothing)

        'Limpiar detalle
        dtDetalle.Rows.Clear()
        ListarDetalle()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0

        vNuevo = True

        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        'ObtenerDeposito()

        'Configuraciones
        'txtDireccionPartida.SetValue(vgConfiguraciones("RemisionDireccionSalida").ToString)

        'Poner el foco en el proveedor
        txtID.txt.SelectAll()
        txtID.txt.Focus()
        btnPedido2.Visible = False
        txtAgregar2.Visible = False
        lblAgregarPor.Visible = True
        btnPedido.Visible = True
        cbxSucursal.Texto = "MOL"
        cbxSucursalDestino.Texto = "ASUNCION"
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From AsignacionCamion Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ),1)"), Integer)
        txtComprobante.txt.Text = txtID.txt.Text
        txtFechaInicio.Hoy()
    End Sub

    Sub Cancelar()

        vNuevo = False

        Dim e As New KeyEventArgs(Keys.End)
        txtNroComprobante_TeclaPrecionada(New Object, e)

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        txtID.txt.Focus()
        Me.ActiveControl = txtID
        txtID.Enabled = "True"
        dtDetalle.Clear()
    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Nro de Comprobante, mayor a 0
        If IsNumeric(txtComprobante.txt.Text) = False Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Asignamos entero al nro de comprobante
        txtComprobante.txt.Text = CInt(txtComprobante.txt.Text)

        If CInt(txtComprobante.txt.Text) = 0 Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Sucursal
        If IsNumeric(cbxSucursalDestino.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If
        'Seleccion de Chofer
        If IsNumeric(cbxChofer.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente un Chofer!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Camion
        If IsNumeric(cbxVehiculo.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente un Camion!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If
        'Existencia en Detalle
        If dtDetalle.Rows.Count = 0 Then
            Dim mensaje As String = "El documento debe tener por lo menos 1 detalle!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Total de Productos
        If txtCantidadMaximaProductos.ObtenerValor < txtCantidadComprobante.ObtenerValor Then
            Dim mensaje As String = "La cantidad de productos supera el maximo permitido! Elimine de la lista los productos necesarios."
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        ValidarDocumento = True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IDTransaccion As Integer
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFechaInicio.txt.Text, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtDetalle.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDChofer", cbxChofer.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCamion", cbxVehiculo.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalAbastecer", cbxSucursalDestino.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PedidoCliente", "1", ParameterDirection.Input)
        'Capturamos el index de la Operacion para un posible proceso posterior
        IndiceOperacion = param.GetLength(0) - 1
        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAsignacionCamion", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From AsignacionCamion Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpAsignacionCamion", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        If IDTransaccion > 0 Then

            'Insertamos el Detalle
            Dim Detalle As String = InsertarDetalle(IDTransaccion)

            'Ejecutar
            CSistema.ExecuteNonQuery(Detalle)
            Dim SqlActualizar As String = ""
            SqlActualizar = "Execute SpDetalleAsignacionCamion " & IDTransaccion & "," & "'INS','','False'"
            CSistema.ExecuteNonQuery(SqlActualizar)
            'Si es nuevo
            If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then

                'Imprimimos
                'Imprimir()

            End If

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False

    End Sub

    Function InsertarDetalle(IDTransaccion As Integer) As String

        InsertarDetalle = ""

        For Each oRow As DataRow In dtDetalle.Rows

            Dim param As New DataTable

            CSistema.SetSQLParameter(param, "IDTransaccion", IDTransaccion)
            CSistema.SetSQLParameter(param, "ID", oRow("ID").ToString)
            CSistema.SetSQLParameter(param, "IDTransaccionAbastecimiento", oRow("IDTransaccionAbastecimiento").ToString)
            CSistema.SetSQLParameter(param, "IDTransaccionPedido", oRow("IDTransaccionPedido").ToString)
            CSistema.SetSQLParameter(param, "IDOperacion", oRow("IDOperacion").ToString)
            CSistema.SetSQLParameter(param, "Tipo", oRow("Tipo").ToString)


            CSistema.SetSQLParameter(param, "IDProducto", oRow("IDProducto").ToString)
            CSistema.SetSQLParameter(param, "Cantidad", CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString))
            CSistema.SetSQLParameter(param, "Anulado", False)

            InsertarDetalle = InsertarDetalle & CSistema.InsertSQL(param, "DetalleAsignacionCamion") & vbCrLf

        Next

    End Function

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Consultar
        If MessageBox.Show("Desea anular el comprobante?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> DialogResult.Yes Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAsignacionCamion", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            Exit Sub

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

    End Sub

    Sub Eliminar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        'Consultar
        If MessageBox.Show("Esto eliminara permanentemente el registro. Desea continuar?", "ATENCION", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2) <> DialogResult.Yes Then
            Exit Sub
        End If

        tsslEstado.Text = ""
        ctrError.Clear()

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAsignacionCamion", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            Exit Sub

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

        txtID.Focus()

        Cancelar()

    End Sub

    Sub EliminarProducto()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If dgvLista.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgvLista, mensaje)
            ctrError.SetIconAlignment(dgvLista, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        For Each item As DataGridViewRow In dgvLista.SelectedRows
            dtDetalle.Rows.RemoveAt(item.Index)
            Exit For
        Next

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtDetalle.Rows
            If Not oRow.RowState = DataRowState.Deleted Then
                oRow("ID") = Index
                Index = Index + 1
            End If
        Next

        ListarDetalle()

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        CSistema.dtToGrid(dgvLista, dtDetalle)

        'Formato
        dgvLista.Columns("IDTransaccion").Visible = False
        dgvLista.Columns("ID").Visible = False
        dgvLista.Columns("IDTransaccionAbastecimiento").Visible = False
        dgvLista.Columns("IDTransaccionPedido").Visible = False
        dgvLista.Columns("IDOperacion").Visible = False
        dgvLista.Columns("IDProducto").Visible = False
        dgvLista.Columns("Anulado").Visible = False
        dgvLista.Columns("Numero").Visible = False
        dgvLista.Columns("Fecha").Visible = True
        dgvLista.Columns("Fec").Visible = False
        dgvLista.Columns("CodigoBarra").Visible = False
        dgvLista.Columns("Ref").Visible = False
        dgvLista.Columns("Cod.").Visible = False
        dgvLista.Columns("IDSucursal").Visible = False
        dgvLista.Columns("IDTipoComprobante").Visible = False
        dgvLista.Columns("Tipo").Visible = True
        dgvLista.Columns("NroComprobante").Visible = False
        dgvLista.Columns("IDUsuarioPedido").Visible = False
        dgvLista.Columns("Producto").Visible = True
        dgvLista.Columns("Descripcion").Visible = True
        dgvLista.Columns("Cantidad").Visible = True
        dgvLista.Columns("Peso").Visible = True


        dgvLista.Columns("IDTransaccion").DisplayIndex = 0
        dgvLista.Columns("ID").DisplayIndex = 1
        dgvLista.Columns("IDTransaccionAbastecimiento").DisplayIndex = 2
        dgvLista.Columns("IDTransaccionPedido").DisplayIndex = 3
        dgvLista.Columns("IDOperacion").DisplayIndex = 4
        dgvLista.Columns("IDProducto").DisplayIndex = 5
        dgvLista.Columns("Anulado").DisplayIndex = 6
        dgvLista.Columns("Numero").DisplayIndex = 7
        dgvLista.Columns("Cantidad").DisplayIndex = 8
        dgvLista.Columns("Producto").DisplayIndex = 9
        dgvLista.Columns("Fec").DisplayIndex = 19
        dgvLista.Columns("Tipo").DisplayIndex = 11
        dgvLista.Columns("Descripcion").DisplayIndex = 12
        dgvLista.Columns("CodigoBarra").DisplayIndex = 13
        dgvLista.Columns("Ref").DisplayIndex = 14
        dgvLista.Columns("Cod.").DisplayIndex = 15
        dgvLista.Columns("IDSucursal").DisplayIndex = 16
        dgvLista.Columns("IDTipoComprobante").DisplayIndex = 17
        dgvLista.Columns("NroComprobante").DisplayIndex = 18
        dgvLista.Columns("Fecha").DisplayIndex = 10
        dgvLista.Columns("Peso").DisplayIndex = 20



        dgvLista.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvLista.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Cantidad").DefaultCellStyle.Format = "N2"
        dgvLista.Columns("Peso").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Peso").DefaultCellStyle.Format = "N2"

        txtPeso.txt.Text = CSistema.dtSumColumn(dtDetalle, "Peso")
        txtCantidadComprobante.SetValue(dgvLista.RowCount)

    End Sub

    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'Otros
        Dim frm As New frmConsultaRemision
        frm.SoloConsulta = False
        frm.IDOperacion = IDOperacion
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.Size = New Size(frmPrincipal2.ClientSize.Width - 75, frmPrincipal2.ClientSize.Height - 75)
        frm.StartPosition = FormStartPosition.CenterScreen
        FGMostrarFormulario(Me, frm, "Seleccion de Comprobante", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VAsignacionCamion Where PedidoCliente = 'True' and IDSucursal=" & cbxSucursal.GetValue & " And Numero=" & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select Top(1) * From VAsignacionCamion Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select * From VDetalleAsignacionCamion Where IDTransaccion=" & IDTransaccion & " Order By IDProducto, IDTransaccion, IDOperacion").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.txt.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        cbxSucursalDestino.txt.Text = oRow("SucursalAbastecer").ToString
        txtFechaInicio.SetValueFromString(CDate(oRow("Fecha").ToString))

        cbxTransporte.txt.Text = ""

        'cbxDistribuidor.txt.Text = oRow("Distribuidor").ToString
        cbxChofer.txt.Text = oRow("Chofer").ToString
        cbxVehiculo.txt.Text = oRow("Camion").ToString


        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            btnImprimir.Enabled = False

            btnEliminar.Visible = True
            btnAnular.Visible = False

        Else
            flpAnuladoPor.Visible = False
            btnEliminar.Visible = False
            btnAnular.Visible = True
        End If

        'Cargamos el detalle
        ListarDetalle()
        btnPedido2.Visible = True
        txtAgregar2.Visible = True
        lblAgregarPor.Visible = False
        btnPedido.Visible = False
    End Sub

    Sub ListarSucursalesClientes()

    End Sub

    'Sub Imprimir(Optional ByVal ImprimirDirecto As Boolean = False)

    '    If ImprimirDirecto = False Then
    '        Try
    '            If CBool(vgConfiguraciones("RemisionImprimir").ToString) = False Then
    '                Exit Sub
    '            End If
    '        Catch ex As Exception
    '            Exit Sub
    '        End Try

    '    End If

    '    Dim Where As String = ""

    '    Dim frm As New frmReporte
    '    frm.MdiParent = My.Application.ApplicationContext.MainForm

    '    'Filtrar IDTransaccion
    '    Where = " Where IDTransaccion =" & IDTransaccion

    '    'Dim Path As String = vgImpresionRemisionPath

    '    'CReporte.Imprimir(frm, IDTransaccion)
    '    Imprimir2()
    'End Sub

    Sub Imprimir2()

        Dim CImpresion As New CImpresion
        CImpresion.IDTransaccion = IDTransaccion
        CImpresion.IDFormularioImpresion = CSistema.RetornarValorString(CData.GetRow("Descripcion='REMISION'", "FormularioImpresion")("IDFormularioImpresion").ToString)
        CImpresion.Impresora = vgImpresionFacturaImpresora
        CImpresion.Test = False
        CImpresion.Inicializar()
        CImpresion.Imprimir()

    End Sub
    Sub Imprimir()

        Dim CAsignacionCamion As New Reporte.CReporteAsignacionCamion
        CAsignacionCamion.AbastecimientoCamion(IDTransaccion)

    End Sub

    Sub ManejarTecla(ByRef e As System.Windows.Forms.KeyEventArgs)

        If vNuevo Then

            If e.KeyCode = vgKeyNuevoRegistro Then
                Nuevo()
            End If
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text
            ID = CInt(ID)
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), " & ID & ") From VAsignacionCamion Where Numero > " & ID & " and PedidoCliente = 'True' and IDSucursal=" & cbxSucursal.GetValue & " "), Integer)
            txtID.SetValue(ID)
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID)
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), " & ID & ") From VAsignacionCamion Where Numero < " & ID & " and PedidoCliente = 'True' and IDSucursal=" & cbxSucursal.GetValue & " "), Integer)
            txtID.SetValue(ID)
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VAsignacionCamion Where PedidoCliente = 'True' and IDSucursal=" & cbxSucursal.GetValue & " "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()


        End If

        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VAsignacionCamion Where Numero=" & txtID.txt.Text & " "), Integer)

            CargarOperacion()

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub


    Sub SeleccionarTransporte()

        If vNuevo = False Then
            Exit Sub
        End If

        If IsNumeric(cbxTransporte.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        cbxDistribuidor.cbx.Text = ""
        cbxVehiculo.cbx.Text = ""
        cbxChofer.cbx.Text = ""

        For Each oRow As DataRow In CData.GetTable("VTransporte").Select(" ID = " & cbxTransporte.cbx.SelectedValue)
            cbxDistribuidor.cbx.Text = oRow("Distribuidor").ToString
            cbxVehiculo.cbx.Text = oRow("Camion").ToString
            cbxChofer.cbx.Text = oRow("Chofer").ToString
        Next

    End Sub
    Sub CargarPedidoDeClientes()

        Dim frm As New frmAsignacionCamionSeleccionarPedido
        frm.IDSucursal = cbxSucursal.GetValue
        frm.IDSucursalOperacion = cbxSucursalDestino.GetValue
        FGMostrarFormulario(Me, frm, "Seleccionar productos de Pedidos de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        If frm.dt Is Nothing AndAlso frm.dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim Actualizar As Boolean = False

        'If dtDetalle.Rows.Count > 0 Then
        '    If MessageBox.Show("Desea anexar los productos a los existentes?", "Productos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
        '        Actualizar = True
        '    End If
        'End If

        'If Actualizar = False Then
        '    dtDetalle.Rows.Clear()
        'End If


        For Each oRow As DataRow In frm.dt.Rows
            'para no cargar dos veces la misma operacion
            If dtDetalle.Select(" IDTransaccionPedido=" & oRow("IDTransaccion") & " and IDOperacion = " & oRow("ID")).Count = 0 Then
                'Si ya existe, actualizar la cantidad
                'If dtDetalle.Select("IDProducto=" & oRow("IDProducto")).Count > 0 Then

                '    'Actualizar
                '    Dim dRow As DataRow = dtDetalle.Select("IDProducto=" & oRow("IDProducto"))(0)

                '    'Obtener Valores
                '    dRow("Cantidad") = dRow("Cantidad") + oRow("Cantidad")

                'Else

                'Cargamos el registro en el detalle
                Dim dRow As DataRow = dtDetalle.NewRow()

                'Obtener Valores
                'Productos
                dRow("IDTransaccion") = NuevoIDTransaccion
                dRow("IDProducto") = oRow("IDProducto")
                dRow("ID") = dtDetalle.Rows.Count
                dRow("Tipo") = "Pedido" & " " & oRow("Comprobante")
                dRow("Producto") = oRow("Producto")
                dRow("IDTransaccionAbastecimiento") = 0
                dRow("IDTransaccionPedido") = oRow("IDTransaccion")
                dRow("IDOperacion") = oRow("ID")
                dRow("Fecha") = oRow("Fecha Entrega")
                dRow("Fec") = oRow("Fecha Entrega")
                dRow("Cantidad") = oRow("Cantidad")
                dRow("Peso") = oRow("Peso")

                'Agregamos al detalle
                dtDetalle.Rows.Add(dRow)

                'End If
            End If
            'Fin de existe IDTransaccion + IDOperacion
        Next

        ListarDetalle()

    End Sub

    Private Sub frm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub frm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frm_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Down Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Or e.KeyCode = Keys.Up Then

            'Solo ejecutar fuera de ciertos controles
            If txtID.txt.Focused = True Then
                Exit Sub
            End If

            If dgvLista.Focused = True Then
                Exit Sub
            End If

        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
        'LimpiarControles()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvLista.KeyUp

        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If

    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        ManejarTecla(e)
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub cbxTransporte_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTransporte.PropertyChanged
        SeleccionarTransporte()
    End Sub
    Private Sub btnAnular_Click(sender As System.Object, e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As System.EventArgs) Handles btnEliminar.Click
        Eliminar(ERP.CSistema.NUMOperacionesRegistro.DEL)
    End Sub


    Private Sub btnMovimiento_Click(sender As System.Object, e As System.EventArgs)
        CargarPedidoDeClientes()
        Exit Sub
    End Sub

    Private Sub btnLotes_Click(sender As System.Object, e As System.EventArgs) Handles btnPedido.Click
        CargarPedidoDeClientes()
        'MessageBox.Show("Modulo en desarrollo!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Exit Sub
    End Sub

    Private Sub txtID_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmAsignacionCamionPedidoCliente_Activate()
        Me.Refresh()
    End Sub
End Class
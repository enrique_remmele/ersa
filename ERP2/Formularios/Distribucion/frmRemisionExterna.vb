﻿Imports ERP.Reporte

Public Class frmRemisionExterna
    'CLASES
    Public CSistema As New CSistema
    Public CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim CReporte As New CReporteRemision

    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDTransaccionMovimientoValue As Integer
    Public Property IDTransaccionMovimiento() As Integer
        Get
            Return IDTransaccionMovimientoValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionMovimientoValue = value
        End Set
    End Property

    Private NuevoIDTransaccionValue As Integer
    Public Property NuevoIDTransaccion() As Integer
        Get
            Return NuevoIDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            NuevoIDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private dtDetalleValue As DataTable
    Public Property dtDetalle() As DataTable
        Get
            Return dtDetalleValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleValue = value
        End Set
    End Property

    'VARIABLES
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim IDTipoComprobante As Integer = 0

    'Public dtPuntoExpedicion As DataTable

    Sub Inicializar()

        Me.KeyPreview = True
        Me.AcceptButton = New Button

        txtCliente.Conectar()
        txtCliente.frm = Me

        cbxMotivo.Conectar()
        cbxDepositoOperacion.Conectar()

        'Otros
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        'Propiedades
        IDTransaccion = 0
        IDTransaccionMovimiento = 0
        vNuevo = False


        'Propiedades
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "FACTURA DE PROVEEDOR PARA DISTRIBUCION", "FACTDISTRI")
        IDTipoComprobante = (CSistema.ExecuteScalar("Select ID From TipoComprobante where Estado = 1 and IDOperacion =" & IDOperacion))


        CargarInformacion()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        'CONFIGURACIONES
        txtFecha.SetValue(VGFechaHoraSistema)
        'If IsNumeric(vgConfiguraciones("RemisionCantidadLineas")) = False Then
        '    vgConfiguraciones("RemisionCantidadLineas") = 20
        'End If

        'txtCantidadMaximaProductos.SetValue(vgConfiguraciones("RemisionCantidadLineas").ToString)
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        'Foco
        Me.ActiveControl = txtNroComprobante
        txtNroComprobante.txt.SelectAll()
        txtNroComprobante.Enabled = False
    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'Cabecera
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, cbxSucursalOperacion)
        CSistema.CargaControl(vControles, cbxDepositoOperacion)
        CSistema.CargaControl(vControles, cbxMotivo)

        'Detalle
        CSistema.CargaControl(vControles, btnMovimiento)

        dtDetalle = CData.GetStructure("VDetalleRemisionExterna", "Select Top(0) IDTransaccion, IDProducto, 'ID'=Convert(int, ID), Referencia, Producto, CodigoBarra, Cantidad, Deposito From VDetalleRemisionExterna")

        'CARGAR CONTROLES
        'Sucursales
        CSistema.SqlToComboBox(cbxSucursalOperacion.cbx, "Select ID, Codigo From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, CData.GetTable("VTipoComprobante", " IDOperacion = " & IDOperacion), "ID", "Codigo")

        'Deposito
        'cbxDepositoOperacion.Conectar()
        CSistema.SqlToComboBox(cbxDepositoOperacion.cbx, " Select * From VDeposito Where Activo='True' and IDSucursal=" & cbxSucursalOperacion.cbx.SelectedValue & "And deposito = 'PROD.TERMINADOS DE TERCEROS' Order By 2")

        CSistema.SqlToComboBox(cbxMotivo.cbx, CData.GetTable("vMotivoRemision", " "), "ID", "Descripcion")

        'cbxMotivo.Texto = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "MOTIVO", "Remision Externa")

        'Fecha 
        txtFecha.SetValue(Now)

        'Ultimo Registro
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(ID) From VRemisionExterna ),1) "), Integer)

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)
        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, New Button, vControles)
    End Sub

    Sub GuardarInformacion()

        'Motivo
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MOTIVO", cbxMotivo.Texto)

    End Sub

    Sub Nuevo()

        txtNroComprobante.Enabled = True

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Limpiar cabecera
        txtCliente.SetValue(Nothing)
        'cbxMotivo.txt.Text = ""
        'cbxSucursalOperacion.txt.Text = ""
        txtDireccionDestino.txt.Text = ""
        txtObservacion.txt.Text = "DISTRIBUCION"
        txtCantidadComprobante.txt.Text = ""
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From VMotivoRemision where Descripcion like '%DISTRIBUCION%' Order By 2")

        'Limpiar detalle
        dtDetalle.Rows.Clear()
        ListarDetalle()

        'Recargar el dtTerminalPuntoExpedicion

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0

        vNuevo = True

        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        'ObtenerDeposito()

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(ID + 1) From RemisionExterna Where IDSucursal=" & cbxSucursalOperacion.cbx.SelectedValue & "),1) "), Integer)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        txtObservacion.Enabled = False
        cbxDepositoOperacion.Enabled = False
        cbxSucursalOperacion.Enabled = False
        cbxMotivo.Enabled = False

        'Poner el foco en el proveedor
        txtNroComprobante.txt.SelectAll()
        txtNroComprobante.txt.Text = 0
        txtNroComprobante.txt.Focus()

    End Sub

    Sub Cancelar()

        vNuevo = False

        Dim e As New KeyEventArgs(Keys.End)
        txtNroComprobante_TeclaPrecionada(New Object, e)

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        txtNroComprobante.txt.Focus()
        Me.ActiveControl = txtNroComprobante
        txtNroComprobante.Enabled = "True"
    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False


        'Asignamos entero al nro de comprobante
        ''txtNroComprobante.txt.Text = CInt(txtNroComprobante.txt.Text)

        If txtNroComprobante.txt.Text Is Nothing Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Cliente
        If txtCliente.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If txtCliente.Registro Is Nothing Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If IsNumeric(txtCliente.Registro("ID").ToString) = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Sucursal
        If IsNumeric(cbxSucursalOperacion.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Deposito
        If IsNumeric(cbxDepositoOperacion.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Motivo
        If IsNumeric(cbxMotivo.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente un motivo!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Existencia en Detalle
        If dtDetalle.Rows.Count = 0 Then
            Dim mensaje As String = "El documento debe tener por lo menos 1 detalle!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Total de Productos
        'If txtCantidadMaximaProductos.ObtenerValor < txtCantidadComprobante.ObtenerValor Then
        '    Dim mensaje As String = "La cantidad de productos supera el maximo permitido! Elimine de la lista los productos necesarios."
        '    ctrError.SetError(btnGuardar, mensaje)
        '    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
        '    tsslEstado.Text = mensaje
        '    Exit Function
        'End If

        ValidarDocumento = True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IDTransaccion As Integer
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtNroComprobante.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Direccion", txtDireccionDestino.txt.Text, ParameterDirection.Input, 100)

        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, False), ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursalOperacion.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoOperacion", cbxDepositoOperacion.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTransaccionMovimiento", IDTransaccionMovimiento, ParameterDirection.Input)
        Try
            If Not txtCliente.Sucursal Is Nothing Then
                If txtCliente.Sucursal("ID").ToString <> "" Then
                    CSistema.SetSQLParameter(param, "IDSucursalCliente", txtCliente.Sucursal("ID").ToString, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "EsRemisionSucursal", "True", ParameterDirection.Input)
                End If
            End If
        Catch ex As Exception

        End Try
        CSistema.SetSQLParameter(param, "@IDMotivo", cbxMotivo.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.GetValue, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Capturamos el index de la Operacion para un posible proceso posterior
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpRemisionExterna", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From RemisionExterna Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpRemisionExterna", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub
        Else

            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

        End If

        If IDTransaccion > 0 Then

            'Insertamos el Detalle
            Dim Detalle As String = InsertarDetalle(IDTransaccion)

            'Ejecutar
            CSistema.ExecuteNonQuery(Detalle)

            'Si es nuevo
            If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then

                'Imprimimos
                Imprimir()

            End If

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtNroComprobante.SoloLectura = False

    End Sub

    Function InsertarDetalle(IDTransaccion As Integer) As String

        InsertarDetalle = ""

        For Each oRow As DataRow In dtDetalle.Rows

            Dim param As New DataTable

            CSistema.SetSQLParameter(param, "IDTransaccion", IDTransaccion)
            CSistema.SetSQLParameter(param, "IDTransaccionMovimiento", IDTransaccionMovimiento)
            CSistema.SetSQLParameter(param, "IDProducto", oRow("IDProducto").ToString)
            CSistema.SetSQLParameter(param, "ID", oRow("ID").ToString)
            CSistema.SetSQLParameter(param, "IDDeposito", cbxDepositoOperacion.GetValue)
            CSistema.SetSQLParameter(param, "Observacion", "")
            CSistema.SetSQLParameter(param, "Cantidad", CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString, True))

            InsertarDetalle = InsertarDetalle & CSistema.InsertSQL(param, "DetalleRemisionExterna") & vbCrLf

        Next

    End Function



    Function ValidarProducto() As Boolean

        ValidarProducto = False

        'Limite de lineas en el detalle
        'If dgvLista.Rows.Count >= txtCantidadMaximaProductos.ObtenerValor Then
        '    MessageBox.Show("Se ha llegado al limite de lineas del detalle del comprobante cierre esta operacion.!", "Limite de lineas.!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        '    Exit Function
        'End If

        ValidarProducto = True

    End Function

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Consultar
        If MessageBox.Show("Desea anular el comprobante?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> DialogResult.Yes Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpRemisionExterna", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            Exit Sub

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

    End Sub

    Sub Eliminar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        'Consultar
        If MessageBox.Show("Esto eliminara permanentemente el registro. Desea continuar?", "ATENCION", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2) <> DialogResult.Yes Then
            Exit Sub
        End If

        tsslEstado.Text = ""
        ctrError.Clear()

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpRemisionExterna", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            Exit Sub

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

        txtNroComprobante.Focus()

        Cancelar()

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        CSistema.dtToGrid(dgvLista, dtDetalle)

        'Formato
        dgvLista.Columns("IDTransaccion").Visible = False
        dgvLista.Columns("ID").Visible = False
        dgvLista.Columns("IDProducto").Visible = False
        dgvLista.Columns("Deposito").Visible = False

        dgvLista.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvLista.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Cantidad").DefaultCellStyle.Format = "N2"

        dgvLista.Columns("Cantidad").ReadOnly = False
        txtCantidadComprobante.SetValue(dgvLista.RowCount)

    End Sub

    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'Otros
        Dim frm As New frmConsultaRemisionExterna
        frm.SoloConsulta = False
        frm.IDOperacion = IDOperacion
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.Size = New Size(frmPrincipal2.ClientSize.Width - 75, frmPrincipal2.ClientSize.Height - 75)
        frm.StartPosition = FormStartPosition.CenterScreen
        FGMostrarFormulario(Me, frm, "Seleccion de Comprobante", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()



        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select Top(1) IDTransaccion From VRemisionExterna ), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtNroComprobante, mensaje)
            ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select Top(1) * From VRemisionExterna Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select IDTransaccion, IDProducto, 'ID'=Convert(int, ID), Referencia, Producto, CodigoBarra, Cantidad, Deposito From VDetalleRemisionExterna Where IDTransaccion=" & IDTransaccion & " Order By ID").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtNroComprobante, mensaje)
            ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)


        cbxSucursalOperacion.txt.Text = oRow("SucursalOperacion").ToString
        cbxTipoComprobante.txt.Text = oRow("TipoComprobante").ToString
        txtNroComprobante.txt.Text = oRow("NroComprobante").ToString
        txtCliente.SetValue(oRow("IDCliente").ToString)
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        txtObservacion.SetValue(oRow("Observacion").ToString)
        txtDireccionDestino.SetValue(oRow("DireccionRemision").ToString)

        cbxSucursalOperacion.SelectedValue(oRow("IDSucursalOperacion").ToString)
        cbxDepositoOperacion.SelectedValue(oRow("IDDeposito").ToString)

        cbxMotivo.txt.Text = oRow("Motivo").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            btnImprimir.Enabled = False

            btnEliminar.Visible = True
            btnAnular.Visible = False

        Else
            btnAnular.Visible = True
            flpAnuladoPor.Visible = False
            btnAnular.Text = "Anular"
        End If

        txtNroComprobante.Enabled = False
        cbxMotivo.Enabled = False
        cbxDepositoOperacion.Enabled = False
        txtDireccionDestino.Enabled = False
        txtObservacion.Enabled = False
        'Cargamos el detalle
        ListarDetalle()

    End Sub


    Sub ObtenerDeposito()

        If cbxSucursalOperacion.cbx.SelectedValue Is Nothing Then
            Exit Sub
        End If

        Dim dttemp As DataTable = CData.GetTable("VDeposito", "IDSucursal=" & cbxSucursalOperacion.cbx.SelectedValue).Copy

        CSistema.SqlToComboBox(cbxDepositoOperacion.cbx, dttemp)

        Dim DepositoRow As DataRow = CData.GetRow("IDSucursal=" & cbxSucursalOperacion.cbx.SelectedValue, "VDeposito")

        cbxDepositoOperacion.cbx.Text = DepositoRow("Deposito").ToString
        cbxDepositoOperacion.cbx.Text = vgDeposito


    End Sub

    Sub ObtenerInformacionClientes()

        Dim oRow As DataRow = txtCliente.Registro
        Dim oRowSucursal As DataRow = txtCliente.Sucursal

        If txtCliente.SucursalSeleccionada = False Then
            txtDireccionDestino.txt.Text = oRow("Direccion").ToString
        Else
            txtDireccionDestino.txt.Text = oRowSucursal("Direccion").ToString
        End If

    End Sub

    Sub Imprimir(Optional ByVal ImprimirDirecto As Boolean = False)

        If ImprimirDirecto = False Then
            Try
                If CBool(vgConfiguraciones("RemisionImprimir").ToString) = False Then
                    Exit Sub
                End If
            Catch ex As Exception
                Exit Sub
            End Try

        End If

        Dim Where As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar IDTransaccion
        Where = " Where IDTransaccion =" & IDTransaccion

        'Dim Path As String = vgImpresionRemisionPath

        'CReporte.Imprimir(frm, IDTransaccion)
        Imprimir2()
    End Sub

    Sub Imprimir2()

        Dim CImpresion As New CImpresion
        CImpresion.IDTransaccion = IDTransaccion
        CImpresion.IDFormularioImpresion = CSistema.RetornarValorString(CData.GetRow("Descripcion='FACTURA DE PROVEEDOR PARA DISTRIBUCION'", "FormularioImpresion")("IDFormularioImpresion").ToString)
        CImpresion.Impresora = vgImpresionFacturaImpresora
        CImpresion.Test = False
        CImpresion.Inicializar()
        CImpresion.Imprimir()

    End Sub

    Sub ManejarTecla(ByRef e As System.Windows.Forms.KeyEventArgs)

        If vNuevo Then
            If e.KeyCode = vgKeyNuevoRegistro Then
                Nuevo()
            End If
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            Dim ID As String
            ID = txtID.txt.Text
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select Top(1) IDTransaccion From VRemisionExterna where id = " & CInt(txtID.txt.Text) & "), 0 )")
            CargarOperacion(IDTransaccion)

        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select Top(1) IDTransaccion From VRemisionExterna where id = " & CInt(txtID.txt.Text) & "), 0 )")

            CargarOperacion(IDTransaccion)

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select Top(1) IDTransaccion From VRemisionExterna where id = " & CInt(txtID.txt.Text) & "), 0 )")

            CargarOperacion(IDTransaccion)

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From VRemisionExterna"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select Top(1) IDTransaccion From VRemisionExterna where id = " & CInt(txtID.txt.Text) & "), 0 )")

            CargarOperacion(IDTransaccion)

        End If

        If e.KeyCode = vgKeyConsultar Then

            'Si no esta en el cliente
            If txtCliente.IsFocus = True Then
                Exit Sub
            End If

            'Si esta en el producto
            If txtNroComprobante.Focus = True Then
                Dim IDCliente As String
                If txtCliente.Registro Is Nothing Then
                    Exit Sub
                End If

                IDCliente = txtCliente.Registro("ID").ToString

                Exit Sub
            End If

            Buscar()
        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(ID), 1) From VRemisionExterna"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select Top(1) IDTransaccion From VRemisionExterna where id = " & CInt(txtID.txt.Text) & "), 0 )")

            CargarOperacion(IDTransaccion)

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

        'Guardar
        'If e.KeyCode = vgKeyProcesar Then
        '    Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
        'End If
    End Sub

    Sub CargarMovimiento()

        'Validar
        If IsNumeric(cbxDepositoOperacion.GetValue) = False Then
            Exit Sub
        End If

        Dim frm As New frmRemisionCargaMovimientoDistribucion
        frm.IDDeposito = cbxDepositoOperacion.GetValue
        FGMostrarFormulario(Me, frm, "Seleccionar productos por factura", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        If frm.dt Is Nothing AndAlso frm.dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim Actualizar As Boolean = False

        If dtDetalle.Rows.Count > 0 Then
            If MessageBox.Show("Desea anexar los productos a los existentes?", "Productos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                Actualizar = True
            End If
        End If

        If Actualizar = False Then
            dtDetalle.Rows.Clear()
        End If


        For Each oRow As DataRow In frm.dt.Rows

            'Si ya existe, actualizar la cantidad
            If dtDetalle.Select("IDProducto=" & oRow("IDProducto")).Count > 0 Then
                IDTransaccionMovimiento = oRow("IdTransaccion")

                'Actualizar
                Dim dRow As DataRow = dtDetalle.Select("IDProducto=" & oRow("IDProducto"))(0)

                'Obtener Valores
                dRow("Cantidad") = dRow("Cantidad") + oRow("Cantidad")

            Else

                'Cargamos el registro en el detalle
                Dim dRow As DataRow = dtDetalle.NewRow()
                IDTransaccionMovimiento = oRow("IdTransaccion")

                'Obtener Valores
                'Productos
                dRow("IDTransaccion") = NuevoIDTransaccion
                ' dRow("IDTransaccionMovimiento") = IDTransaccionMovimiento
                dRow("IDProducto") = oRow("IDProducto")
                dRow("ID") = dtDetalle.Rows.Count
                dRow("Referencia") = oRow("Referencia")
                dRow("Producto") = oRow("Producto")
                dRow("CodigoBarra") = oRow("CodigoBarra")
                dRow("Cantidad") = oRow("Cantidad")

                'Deposito
                dRow("Deposito") = cbxDepositoOperacion.cbx.Text

                'Agregamos al detalle
                dtDetalle.Rows.Add(dRow)

            End If
        Next

        ListarDetalle()

    End Sub

    Private Sub frm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub frm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frm_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Down Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Or e.KeyCode = Keys.Up Then

            'Solo ejecutar fuera de ciertos controles
            'If txtCantidad.txt.Focused = True Then
            '    Exit Sub
            'End If

            If dgvLista.Focused = True Then
                Exit Sub
            End If

        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvLista.KeyUp

        If e.KeyCode = Keys.Delete Then
            'EliminarProducto()
        End If

    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub txtCliente_ItemMalSeleccionado(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCliente.ItemMalSeleccionado

        'Limpiar los otros controles
        'txtDireccionDestino.txt.Clear()

        'Datos Comunes
        'txtProducto.IDCliente = Nothing

    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado
        ObtenerInformacionClientes()
        If vNuevo = True Then
            txtFecha.txt.Focus()
        End If
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir(True)
    End Sub


    Private Sub cbxSucursalOperacion_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxSucursalOperacion.PropertyChanged

        cbxDepositoOperacion.DataSource = Nothing
        cbxDepositoOperacion.cbx.Text = ""

        Try
            CSistema.SqlToComboBox(cbxDepositoOperacion.cbx, CData.GetTable("VDeposito", "IDSucursal=" & cbxSucursalOperacion.GetValue))

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnAnular_Click(sender As System.Object, e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As System.EventArgs) Handles btnEliminar.Click
        Eliminar(ERP.CSistema.NUMOperacionesRegistro.DEL)
    End Sub

    Private Sub btnMovimiento_Click(sender As System.Object, e As System.EventArgs) Handles btnMovimiento.Click
        CargarMovimiento()
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    '10-06-2021 - SC - Actualiza datos
    Sub frmRemisionExterna_Activate()
        Me.Refresh()
    End Sub


End Class
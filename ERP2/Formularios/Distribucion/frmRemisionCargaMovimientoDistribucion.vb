﻿Public Class frmRemisionCargaMovimientoDistribucion
    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Private IDDepositoValue As Integer
    Public Property IDDeposito() As Integer
        Get
            Return IDDepositoValue
        End Get
        Set(ByVal value As Integer)
            IDDepositoValue = value
        End Set

    End Property

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Foco
        Me.ActiveControl = txtNroComprobante

    End Sub

    Sub CargarComprobante(Optional Where As String = "")

        dgvComprobante.Rows.Clear()

        Dim SQL As String = "Select IDTransaccion, 'Suc'=CodigoSucursalOperacion, Num, Fecha, [Cod.], Comprobante, Operacion, Motivo, Observacion From VMovimientoRemisionDistribucion Where IDDepositoSalida=" & IDDeposito & " And IDSucursal = " & cbxSucursalOperacion.GetValue & "  And Anulado='False' "

        If Where <> "" Then
            SQL = SQL & " And " & Where
        End If

        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        If dt Is Nothing Then
            Exit Sub
        End If

        For Each oRow As DataRow In dt.Rows
            Dim Registro(dgvComprobante.Columns.Count - 1) As String
            Registro(0) = oRow("IDTransaccion")
            Registro(1) = False
            Registro(2) = oRow("Suc")
            Registro(3) = oRow("Num")
            Registro(4) = oRow("Fecha")
            Registro(5) = oRow("Cod.")
            Registro(6) = oRow("Comprobante")
            Registro(7) = oRow("Operacion")
            Registro(8) = oRow("Motivo")
            Registro(9) = oRow("Observacion")

            dgvComprobante.Rows.Add(Registro)

        Next

        'Formato
        dgvComprobante.Columns("Sel").ReadOnly = False
        dgvComprobante.Columns("IDTransaccion").Visible = False
        dgvComprobante.Columns("Operacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        'Totales
        txtCantidadComprobante.SetValue(dgvComprobante.RowCount)

        If dgvComprobante.Rows.Count > 0 Then
            Me.ActiveControl = dgvComprobante
        End If

    End Sub

    Sub CargarProductos()

        Dim SQL As String = "Select IDTransaccion, IDProducto, 'Referencia'=Ref, Producto, CodigoBarra, 'Cantidad'=SUM(Cantidad) From VDetalleMovimiento "
        Dim Where As String = ""
        Dim GroupBy As String = " Group By IDTransaccion, IDProducto, Ref, Producto, CodigoBarra "

        For Each oRow As DataGridViewRow In dgvComprobante.Rows

            If oRow.Cells("Sel").Value = True Then
                If Where = "" Then
                    Where = " Where (IDTransaccion=" & oRow.Cells("IDTransaccion").Value & ") "
                Else
                    Where = Where & " Or (IDTransaccion=" & oRow.Cells("IDTransaccion").Value & ") "
                End If
            End If
        Next

        If Where = "" Then
            dt.Rows.Clear()
        Else
            dt = CSistema.ExecuteToDataTable(SQL & Where & GroupBy)
        End If

        CSistema.dtToGrid(dgvDetalle, dt)

        'Formato
        dgvDetalle.Columns("IDProducto").Visible = False
        dgvDetalle.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvDetalle.Columns("Cantidad").DefaultCellStyle.Format = "N2"
        dgvDetalle.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        If dgvDetalle.Rows.Count > 0 Then
            Me.ActiveControl = dgvComprobante
        End If

        'Totales
        txtCantidadDetalle.SetValue(dgvDetalle.RowCount)

    End Sub

    Sub Aceptar()

        Procesado = True
        Me.Close()

    End Sub

    Sub Cancelar()

        Procesado = False
        Me.Close()

    End Sub

    Private Sub frmRemisionCargarMovimientoUnisal_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCargarComprobante_Click(sender As System.Object, e As System.EventArgs) Handles btnCargarComprobante.Click
        CargarComprobante(" Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' ")
    End Sub

    Private Sub lklComprobantesMes_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklComprobantesMes.LinkClicked
        CargarComprobante(" Month(Fecha)=" & Date.Now.Month & " And Year(Fecha)=" & Date.Now.Year & " ")
    End Sub

    Private Sub lklSeleccionarTodo_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklSeleccionarTodo.LinkClicked

        If lklSeleccionarTodo.Text = "Seleccionar todo" Then
            For Each oRow As DataGridViewRow In dgvComprobante.Rows
                oRow.Cells("Sel").Value = True
            Next
            lklSeleccionarTodo.Text = "Quitar todo"
            CargarProductos()
            Exit Sub
        End If

        If lklSeleccionarTodo.Text = "Quitar todo" Then
            For Each oRow As DataGridViewRow In dgvComprobante.Rows
                oRow.Cells("Sel").Value = False
            Next
            lklSeleccionarTodo.Text = "Seleccionar todo"
            CargarProductos()
            Exit Sub
        End If

    End Sub

    Private Sub btnCargarMercaderias_Click(sender As System.Object, e As System.EventArgs) Handles btnCargarMercaderias.Click
        CargarProductos()
    End Sub

    Private Sub lklQuitarSeleccionados_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklQuitarSeleccionados.LinkClicked

    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Aceptar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then

            If txtNroComprobante.GetValue = "" Then
                Exit Sub
            End If

            CargarComprobante(" IDSucursal=" & cbxSucursalOperacion.GetValue & " And Numero = '" & txtNroComprobante.GetValue & "' ")

        End If
    End Sub

    Private Sub dgvComprobante_Click(sender As Object, e As System.EventArgs) Handles dgvComprobante.Click
        dgvComprobante.SelectedRows(0).Cells("Sel").Value = CStr(Not CBool(dgvComprobante.SelectedRows(0).Cells("Sel").Value))
        CargarProductos()
    End Sub

    Private Sub dgvComprobante_DoubleClick(sender As Object, e As System.EventArgs) Handles dgvComprobante.DoubleClick

    End Sub

    Private Sub dgvComprobante_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvComprobante.KeyDown

        If e.KeyCode = Keys.Enter Then

            'MArcar
            If dgvComprobante.SelectedRows.Count > 0 Then

                dgvComprobante.SelectedRows(0).Cells("Sel").Value = CStr(Not CBool(dgvComprobante.SelectedRows(0).Cells("Sel").Value))

                If dgvComprobante.SelectedRows(0).Index <> dgvComprobante.Rows.Count - 1 Then
                    dgvComprobante.CurrentCell = dgvComprobante.SelectedRows(0).Cells("Sel")
                End If

                CargarProductos()
            End If
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub dgvDetalle_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvDetalle.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If
    End Sub

    Private Sub dgvComprobante_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvComprobante.KeyUp

    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmRemisionCargarMovimientoUnisal_Activate()
        Me.Refresh()
    End Sub

End Class
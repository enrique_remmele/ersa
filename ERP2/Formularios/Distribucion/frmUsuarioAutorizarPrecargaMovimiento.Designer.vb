﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsuarioAutorizarPrecargaMovimiento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxDato = New System.Windows.Forms.GroupBox()
        Me.txtPerfil = New ERP.ocxTXTString()
        Me.lblPerfil = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.dgvUsuario = New System.Windows.Forms.DataGridView()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblUsuario1 = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.gbxDato.SuspendLayout()
        CType(Me.dgvUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxDato
        '
        Me.gbxDato.Controls.Add(Me.txtPerfil)
        Me.gbxDato.Controls.Add(Me.lblPerfil)
        Me.gbxDato.Controls.Add(Me.btnCancelar)
        Me.gbxDato.Controls.Add(Me.btnEliminar)
        Me.gbxDato.Controls.Add(Me.btnGuardar)
        Me.gbxDato.Controls.Add(Me.btnEditar)
        Me.gbxDato.Controls.Add(Me.btnNuevo)
        Me.gbxDato.Controls.Add(Me.cbxUsuario)
        Me.gbxDato.Controls.Add(Me.dgvUsuario)
        Me.gbxDato.Controls.Add(Me.lblID)
        Me.gbxDato.Controls.Add(Me.lblUsuario1)
        Me.gbxDato.Controls.Add(Me.lblEstado)
        Me.gbxDato.Controls.Add(Me.txtID)
        Me.gbxDato.Controls.Add(Me.rdbActivo)
        Me.gbxDato.Controls.Add(Me.rdbDesactivado)
        Me.gbxDato.Location = New System.Drawing.Point(5, 6)
        Me.gbxDato.Name = "gbxDato"
        Me.gbxDato.Size = New System.Drawing.Size(454, 404)
        Me.gbxDato.TabIndex = 1
        Me.gbxDato.TabStop = False
        '
        'txtPerfil
        '
        Me.txtPerfil.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPerfil.Color = System.Drawing.Color.Empty
        Me.txtPerfil.Indicaciones = Nothing
        Me.txtPerfil.Location = New System.Drawing.Point(87, 67)
        Me.txtPerfil.Multilinea = False
        Me.txtPerfil.Name = "txtPerfil"
        Me.txtPerfil.Size = New System.Drawing.Size(301, 21)
        Me.txtPerfil.SoloLectura = False
        Me.txtPerfil.TabIndex = 61
        Me.txtPerfil.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPerfil.Texto = ""
        '
        'lblPerfil
        '
        Me.lblPerfil.AutoSize = True
        Me.lblPerfil.Location = New System.Drawing.Point(49, 71)
        Me.lblPerfil.Name = "lblPerfil"
        Me.lblPerfil.Size = New System.Drawing.Size(33, 13)
        Me.lblPerfil.TabIndex = 60
        Me.lblPerfil.Text = "Perfil:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(318, 131)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(64, 23)
        Me.btnCancelar.TabIndex = 58
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(382, 131)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(64, 23)
        Me.btnEliminar.TabIndex = 59
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(254, 131)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(64, 23)
        Me.btnGuardar.TabIndex = 55
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(190, 131)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(64, 23)
        Me.btnEditar.TabIndex = 57
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(125, 131)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(64, 23)
        Me.btnNuevo.TabIndex = 56
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = Nothing
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = Nothing
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = Nothing
        Me.cbxUsuario.DataSource = Nothing
        Me.cbxUsuario.DataValueMember = Nothing
        Me.cbxUsuario.dtSeleccionado = Nothing
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(87, 38)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionMultiple = False
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(295, 23)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 50
        Me.cbxUsuario.Texto = ""
        '
        'dgvUsuario
        '
        Me.dgvUsuario.AllowUserToAddRows = False
        Me.dgvUsuario.AllowUserToDeleteRows = False
        Me.dgvUsuario.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvUsuario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvUsuario.Location = New System.Drawing.Point(9, 164)
        Me.dgvUsuario.Name = "dgvUsuario"
        Me.dgvUsuario.ReadOnly = True
        Me.dgvUsuario.Size = New System.Drawing.Size(437, 223)
        Me.dgvUsuario.TabIndex = 49
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(61, 18)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 41
        Me.lblID.Text = "ID:"
        '
        'lblUsuario1
        '
        Me.lblUsuario1.AutoSize = True
        Me.lblUsuario1.Location = New System.Drawing.Point(36, 44)
        Me.lblUsuario1.Name = "lblUsuario1"
        Me.lblUsuario1.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuario1.TabIndex = 43
        Me.lblUsuario1.Text = "Usuario:"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(39, 103)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 45
        Me.lblEstado.Text = "Estado:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(87, 13)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 42
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(87, 101)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 46
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(148, 101)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 47
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmUsuarioAutorizarPrecargaMovimiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(465, 426)
        Me.Controls.Add(Me.gbxDato)
        Me.Name = "frmUsuarioAutorizarPrecargaMovimiento"
        Me.Tag = "frmUsuarioAutorizarPrecargaMovimiento"
        Me.Text = "frmUsuarioAutorizarPrecargaMovimiento"
        Me.gbxDato.ResumeLayout(False)
        Me.gbxDato.PerformLayout()
        CType(Me.dgvUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gbxDato As GroupBox
    Friend WithEvents lblPerfil As Label
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnEliminar As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnEditar As Button
    Friend WithEvents btnNuevo As Button
    Friend WithEvents cbxUsuario As ocxCBX
    Friend WithEvents dgvUsuario As DataGridView
    Friend WithEvents lblID As Label
    Friend WithEvents lblUsuario1 As Label
    Friend WithEvents lblEstado As Label
    Friend WithEvents txtID As ocxTXTNumeric
    Friend WithEvents rdbActivo As RadioButton
    Friend WithEvents rdbDesactivado As RadioButton
    Friend WithEvents txtPerfil As ocxTXTString
    Friend WithEvents ctrError As ErrorProvider
End Class

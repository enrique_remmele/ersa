﻿Public Class frmSubMotivoMovimiento
    'CLASES
    Dim CSistema As New CSistema


    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control
    Dim Editando As Boolean = False
    Dim IDOperacion As Integer
    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        'Funciones
        CargarInformacion()

        'Botones
        EstablecerBotones(CSistema.NUMHabilitacionBotonesABM.INICIO)

        'Focus
        dgv.Focus()

    End Sub
    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesABM)

        CSistema.ControlBotonesABM(Operacion, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtDescripcion)
        CSistema.CargaControl(vControles, cbxOperacion)
        CSistema.CargaControl(vControles, cbxTipoOperacion)
        CSistema.CargaControl(vControles, cbxMotivo)
        CSistema.CargaControl(vControles, rdbActivo)
        CSistema.CargaControl(vControles, rdbDesactivado)


        'Cargamos la estructura del dgv
        CSistema.SqlToDataGrid(dgv, "Select Top(0) ID, SubMotivoMovimiento,MotivoMovimiento,Operacion,Tipo,EstadoSubMotivo From vSubMotivoMovimiento")

        'Operaciones
        CSistema.SqlToComboBox(cbxOperacion, "Select ID, Descripcion From Operacion where descripcion like '%MOVIMIENTO%DISTRIBUCION%' order by Descripcion")

        'Tipo de Operaciones
        CSistema.SqlToComboBox(cbxTipoOperacion.cbx, "Select ID, Descripcion From TipoOperacion Where IDOperacion= " & cbxOperacion.cbx.SelectedValue & "order by Descripcion")

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento where IDTipoMovimiento = " & cbxTipoOperacion.cbx.SelectedValue & "order by Descripcion")

        'Cargamos los registos en el lv
        Listar()

        'Obtenemos Informacion del ultimo registro cargado
        ObtenerInformacion()

    End Sub

    Sub ObtenerInformacion()
        'Obtener el ID Registro
        Dim ID As Integer

        If dgv.SelectedRows.Count = 0 Then
            Exit Sub
        Else
            ID = dgv.SelectedRows(0).Cells("ID").Value
        End If

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("select ID, SubMotivoMovimiento,MotivoMovimiento,Operacion,Tipo,EstadoSubMotivo from vSubMotivoMovimiento Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            txtDescripcion.txt.Text = oRow("SubMotivoMovimiento").ToString
            cbxMotivo.cbx.Text = oRow("MotivoMovimiento").ToString
            cbxOperacion.cbx.Text = oRow("Operacion").ToString
            cbxTipoOperacion.cbx.Text = oRow("Tipo").ToString

            If CSistema.ExecuteScalar("Select EstadoSubMotivo From vSubMotivoMovimiento Where ID =" & ID) = True Then
                rdbActivo.Checked = True
            Else
                rdbDesactivado.Checked = True
            End If
            'Configuramos los controles ABM como EDITAR
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        Editando = False
        dgv.Refresh()

        CSistema.SqlToDataGrid(dgv, "select ID, SubMotivoMovimiento,MotivoMovimiento,Operacion,Tipo,EstadoSubMotivo from vSubMotivoMovimiento Order by 1")

        dgv.Refresh()

    End Sub

    Sub InicializarControles()
        'TextBox
        txtID.txt.Clear()
        cbxOperacion.cbx.Text = " "
        cbxTipoOperacion.cbx.Text = " "
        cbxMotivo.cbx.Text = " "
        txtDescripcion.txt.Text = " "


        'RadioButton()
        rdbActivo.Checked = True

        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From SubMotivoMovimiento"), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From SubMotivoMovimiento"), Integer))

        End If

        'Error
        ctrError.Clear()

        'Foco
        cbxOperacion.Focus()


    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        ctrError.Clear()

        'Validar
        'Operacion
        If cbxOperacion.GetValue = 0 Then
            Dim mensaje As String = "Debe seleccionar una Operacion valida!"
            ctrError.SetError(cbxOperacion, mensaje)
            ctrError.SetIconAlignment(cbxOperacion, ErrorIconAlignment.MiddleRight)
            Exit Sub
        End If

        'TipoOperacion
        If cbxTipoOperacion.GetValue = 0 Then
            Dim mensaje As String = "Debe seleccionar un Tipo de Operacion valido!"
            ctrError.SetError(cbxTipoOperacion, mensaje)
            ctrError.SetIconAlignment(cbxTipoOperacion, ErrorIconAlignment.MiddleRight)
            Exit Sub
        End If

        'Motivo
        If cbxMotivo.GetValue = 0 Then
            Dim mensaje As String = "Debe seleccionar un Motivo valido!"
            ctrError.SetError(cbxMotivo, mensaje)
            ctrError.SetIconAlignment(cbxMotivo, ErrorIconAlignment.MiddleRight)
            Exit Sub
        End If

        'Motivo
        If txtDescripcion.txt.Text = "" Then
            Dim mensaje As String = "Debe cargar su SubMotivo valido!"
            ctrError.SetError(txtDescripcion, mensaje)
            ctrError.SetIconAlignment(txtDescripcion, ErrorIconAlignment.MiddleRight)
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgv.SelectedCells.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgv, mensaje)
                ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopRight)
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoMovimiento", cbxTipoOperacion.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMotivoMovimiento", cbxMotivo.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpSubMotivoMovimiento", False, False, MensajeRetorno, "", True) = True Then
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO)
            ctrError.Clear()
            Listar(txtID.txt.Text)
        Else
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub Nuevo()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO)
        vNuevo = True
        InicializarControles()
        'Operaciones
        CSistema.SqlToComboBox(cbxOperacion.cbx, "Select ID, Descripcion From Operacion where descripcion like '%MOVIMIENTO%DISTRIBUCION%' order by Descripcion")

        'Tipo de Operaciones
        'CSistema.SqlToComboBox(cbxTipoOperacion.cbx, "Select ID, Descripcion From TipoOperacion Where IDOperacion= " & cbxOperacion.cbx.SelectedValue & "order by Descripcion")

        'Motivos
        'CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento where IDTipoMovimiento = " & cbxTipoOperacion.cbx.SelectedValue & "order by Descripcion")

    End Sub

    Sub Editar()
        'Establecemos los botones a Editando
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO)
        vNuevo = False
        'Editar = True
        'Foco
        txtID.Focus()

    End Sub

    Private Sub frmSubMotivoMovimiento_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()

    End Sub
    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnEditar_Click(sender As System.Object, e As System.EventArgs) Handles btnEditar.Click
        Editar()
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnEliminar_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminar.Click
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub dgv_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellContentClick
        ObtenerInformacion()
    End Sub

    Private Sub dgv_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles dgv.SelectionChanged
        ObtenerInformacion()
    End Sub

    Sub frmSubMotivoMovimiento_Activate()
        Me.Refresh()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub cbxOperacion_PropertyChanged(sender As Object, e As EventArgs) Handles cbxOperacion.PropertyChanged
        'Tipo de Operaciones
        CSistema.SqlToComboBox(cbxTipoOperacion.cbx, "Select ID, Descripcion From TipoOperacion Where IDOperacion= " & cbxOperacion.cbx.SelectedValue & "order by Descripcion")

    End Sub

    Private Sub cbxTipoOperacion_PropertyChanged(sender As Object, e As EventArgs) Handles cbxTipoOperacion.PropertyChanged

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento where IDTipoMovimiento = " & cbxTipoOperacion.cbx.SelectedValue & "order by Descripcion")

    End Sub

    Private Sub cbxOperacion_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxOperacion.TeclaPrecionada
        'Tipo de Operaciones
        CSistema.SqlToComboBox(cbxTipoOperacion.cbx, "Select ID, Descripcion From TipoOperacion Where IDOperacion= " & cbxOperacion.cbx.SelectedValue & "order by Descripcion")

    End Sub

    Private Sub cbxTipoOperacion_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxTipoOperacion.TeclaPrecionada
        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento where IDTipoMovimiento = " & cbxTipoOperacion.cbx.SelectedValue & "order by Descripcion")

    End Sub

End Class
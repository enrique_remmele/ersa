﻿Public Class frmUsuarioAutorizarPrecargaMovimiento
    'CLASES
    Dim CSistema As New CSistema


    'VARIABLES
    Dim vNuevo As Boolean
    Dim vEditar As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False
        vEditar = False
        'Funciones
        CargarInformacion()

        'Botones
        EstablecerBotones(CSistema.NUMHabilitacionBotonesABM.INICIO)

        'Focus
        txtID.Focus()

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesABM)

        CSistema.ControlBotonesABM(Operacion, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles, New Button)

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtID)
        CSistema.CargaControl(vControles, cbxUsuario)
        CSistema.CargaControl(vControles, txtPerfil)
        CSistema.CargaControl(vControles, rdbActivo)
        CSistema.CargaControl(vControles, rdbDesactivado)

        'Cargamos la estructura del dgv
        CSistema.SqlToDataGrid(dgvUsuario, "select Top(0) ID, IDUsuario,Nombre,Usuario,Identificador,Perfil,AutorizaMovimiento from VUsuarioAutorizarPrecargaMovimiento")

        'Cargamos el combo box de usuario
        CSistema.SqlToComboBox(cbxUsuario, "Select ID, Nombre From VUsuario order by Nombre")

        'Cargamos los registos en el lv
        Listar()

        'Obtenemos Informacion del ultimo registro cargado
        ObtenerInformacion()

    End Sub

    Sub ObtenerInformacion()
        'Obtener el ID Registro
        Dim ID As Integer

        If dgvUsuario.SelectedRows.Count = 0 Then
            Exit Sub
        Else
            ID = dgvUsuario.SelectedRows(0).Cells("ID").Value
        End If

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select ID, IDUsuario, Nombre, Usuario, Identificador, Perfil, AutorizaMovimiento From VUsuarioAutorizarPrecargaMovimiento Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            cbxUsuario.cbx.Text = oRow("Nombre").ToString
            txtPerfil.txt.Text = oRow("Perfil").ToString

            If CSistema.ExecuteScalar("Select AutorizaMovimiento From VUsuarioAutorizarPrecargaMovimiento Where ID =" & ID) = True Then
                rdbActivo.Checked = True
            Else
                rdbDesactivado.Checked = True
            End If
            'Configuramos los controles ABM como EDITAR
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR)

        End If

        ctrError.Clear()

    End Sub

    Sub InicializarControles()
        'TextBox
        txtID.txt.Clear()
        cbxUsuario.cbx.Text = " "
        txtPerfil.txt.Text = " "


        'RadioButton()
        rdbActivo.Checked = True

        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From UsuarioAutorizarPrecargaMovimiento"), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(IDUsuario), 1) From VUsuarioAutorizarPrecargaMovimiento"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        cbxUsuario.Focus()


    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        CSistema.SqlToDataGrid(dgvUsuario, "Select ID, IDUsuario,Nombre, Usuario, Perfil, AutorizaMovimiento From VUsuarioAutorizarPrecargaMovimiento Order by 1")

        dgvUsuario.Refresh()

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        ctrError.Clear()

        'Validar
        'Usuario
        If cbxUsuario.GetValue = 0 Then
            Dim mensaje As String = "Debe seleccionar un usuario valido!"
            ctrError.SetError(cbxUsuario, mensaje)
            ctrError.SetIconAlignment(cbxUsuario, ErrorIconAlignment.MiddleRight)
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgvUsuario.SelectedCells.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgvUsuario, mensaje)
                ctrError.SetIconAlignment(dgvUsuario, ErrorIconAlignment.TopRight)
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuarioAutorizar", cbxUsuario.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpUsuarioAutorizarPrecargaMovimiento", False, False, MensajeRetorno, "", True) = True Then
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO)
            ctrError.Clear()
            Listar(txtID.txt.Text)
        Else
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub Nuevo()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO)
        vNuevo = True
        InicializarControles()
    End Sub

    Sub Editar()
        'Establecemos los botones a Editando
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO)
        vNuevo = False
        vEditar = True
        'Foco
        txtID.Focus()

    End Sub

    Private Sub frmUsuarioAutorizarPedidoNotaCredito_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()

    End Sub
    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnEditar_Click(sender As System.Object, e As System.EventArgs) Handles btnEditar.Click
        Editar()
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnEliminar_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminar.Click
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub dgvUsuario_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvUsuario.CellContentClick
        ObtenerInformacion()
    End Sub

    Private Sub dgvUsuario_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles dgvUsuario.SelectionChanged
        ObtenerInformacion()
    End Sub

    Sub frmUsuarioAutorizarPrecargaMovimiento_Activate()
        Me.Refresh()
    End Sub

    Private Sub cbxUsuario_PropertyChanged(sender As Object, e As EventArgs) Handles cbxUsuario.PropertyChanged
        If IsNumeric(cbxUsuario.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        txtPerfil.txt.Text = (CSistema.ExecuteScalar("Select perfil From vUsuario where id=" & cbxUsuario.cbx.SelectedValue))
    End Sub
End Class
﻿Public Class frmMovimientoDistribucion

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Dim CAsiento As New CAsientoMovimiento
    Dim CData As New CData
    Dim VAnulado As Boolean = False
    Dim Reprocesar As Boolean = False

    'Dim CReporte As New CReporte

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dtOperaciones As New DataTable
    Dim dtDepositos As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim dtComprobantes As New DataTable
    Dim Precargado As Boolean = False
    Dim CancelarTransferencia As Boolean = False
    Dim IDTransaccionTransferenciaEnvio As Integer
    Dim Enviar As Boolean = False
    Dim Recibir As Boolean = False
    Dim IDTransaccionEnvio As Integer = 0
    Dim IDTipoComprobante As Integer = 0

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        txtProducto.Conectar()

        'Otros
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Propiedades
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "MOVIMIENTO STOCK DISTRIBUCION", "MOVDISTRI")
        IDTipoComprobante = (CSistema.ExecuteScalar("Select ID From TipoComprobante where Estado = 1 and IDOperacion =" & IDOperacion))

        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From Movimiento where MovimientoDistribucion = 'True' and IdTipoComprobante= " & IDTipoComprobante & "),1)"), Integer)

        IDTransaccion = 0
        vNuevo = False
        lklSeleccionarProductos.Enabled = False
        txtCosto.Enabled = False

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxTipoOperacion)
        CSistema.CargaControl(vControles, cbxMotivo)
        CSistema.CargaControl(vControles, cbxSubMotivo)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, cbxDepositoSalida)
        CSistema.CargaControl(vControles, cbxDepositoEntrada)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, txtObservacionProducto)
        CSistema.CargaControl(vControles, cbxUnidad)
        CSistema.CargaControl(vControles, txtCantidad)

        'CARGAR ESTRUCTURA DEL DETALLE
        dtDetalle = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleMovimiento where IdTipoComprobante =" & IDTipoComprobante).Clone

        'INICIALIZAR EL DETALLE IMPUESTO
        'CDetalleImpuesto.Inicializar()

        'CARGAR DATOS DE OPERACION
        dtOperaciones = CSistema.ExecuteToDataTable("Select ID, Descripcion, Activo, Entrada, Salida From TipoOperacion Where IDOperacion=" & IDOperacion)
        'GenerarTipoOperacion()

        'CARGAR UNIDAD
        cbxUnidad.Items.Add("UNIDAD")
        cbxUnidad.Items.Add("CAJA")
        cbxUnidad.Items.Add("KILOGRAMOS")
        cbxUnidad.Items.Add("BOLSAS")
        cbxUnidad.Items.Add("LITROS")
        cbxUnidad.Items.Add("CM")
        cbxUnidad.Items.Add("METRO")
        cbxUnidad.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR CONTROLES
        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo From VSucursal where id=1 Order By 2")

        'Tipo de Operaciones
        CSistema.SqlToComboBox(cbxTipoOperacion.cbx, "Select ID, Descripcion From TipoOperacion Where IDOperacion=" & IDOperacion)

        'Tipo de Comprobante
        dtComprobantes = CSistema.ExecuteToDataTable("Select ID, Codigo, Descripcion From TipoComprobante Where IDOperacion=" & IDOperacion)
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, dtComprobantes, "ID", "Codigo")

        'Depositos
        dtDepositos = CSistema.ExecuteToDataTable(" Select * From VDeposito Where Activo='True' Order By 2").Copy

        'Deposito de Origen
        cbxDepositoEntrada.cbx.ValueMember = dtDepositos.Columns("ID").ToString
        cbxDepositoEntrada.cbx.DisplayMember = dtDepositos.Columns("Suc-Dep").ToString
        cbxDepositoEntrada.cbx.DataSource = dtDepositos.Copy

        'Deposito de Destino
        cbxDepositoSalida.cbx.ValueMember = dtDepositos.Columns("ID").ToString
        cbxDepositoSalida.cbx.DisplayMember = dtDepositos.Columns("Suc-Dep").ToString
        cbxDepositoSalida.cbx.DataSource = dtDepositos.Copy
        'cbxDepositoSalida.cbx.DataSource = CData.FiltrarDataTable(dtDepositos.Copy, " IDSucursal = " & vgIDSucursal)

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento ")

        'SubMotivos
        CSistema.SqlToComboBox(cbxSubMotivo.cbx, "Select ID, Descripcion From SubMotivoMovimiento ")

        'CARGAR LA ULTIMA CONFIGURACION
        'Tipo Operacion
        cbxSucursal.SelectedValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", "0"))

        ''Tipo Operacion
        'cbxTipoOperacion.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO OPERACION", "")

        ''Tipo Motivo
        'cbxMotivo.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MOTIVO", "")

        ''Tipo de Comprobante
        'cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        ''Deposito de Origen
        'cbxDepositoSalida.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DEPOSITO ORIGEN", "")

        ''Deposito de Destino
        'cbxDepositoEntrada.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DEPOSITO DESTINO", "")

        'Unidad
        cbxUnidad.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "UNIDAD", "UNIDAD")

        'CONFIGURACIONES
        If CBool(vgConfiguraciones("MovimientoBloquearFecha").ToString) = True Then
            If CInt(vgConfiguraciones("MovimientoCantidadDiasHabilitados")) = 0 Then
                txtFecha.Enabled = False
            Else
                txtFecha.Enabled = True
            End If

            txtFecha.SetValue(VGFechaHoraSistema)
        Else
            txtFecha.Enabled = True
        End If

        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From Movimiento where MovimientoDistribucion = 'True' and IdTipoComprobante=" & IDTipoComprobante & "),1)"), Integer)

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        cbxMotivo.cbx.Text = ""
        cbxSubMotivo.cbx.Text = ""

        vNuevo = False
        lklSeleccionarProductos.Enabled = False
        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = (CSistema.ExecuteScalar("Select Codigo From TipoComprobante where Estado = 1 and IDOperacion =" & IDOperacion))

        'dtComprobantes = CSistema.ExecuteToDataTable("Select ID, Codigo, Descripcion From TipoComprobante Where IDOperacion=" & IDOperacion)
        'CSistema.SqlToComboBox(cbxTipoComprobante.cbx, dtComprobantes, "ID", "Codigo")

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VMovimiento Where MovimientoDistribucion = 'True' and IDSucursal=" & cbxSucursal.GetValue & "And IdTipoComprobante = " & IDTipoComprobante & " And Numero=" & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select Numero, Fecha, Operacion, Motivo, SubMotivo, [Cod.],  NroComprobante, DepositoEntrada, DepositoSalida, Observacion, Autorizacion, Total, Anulado, UsuarioIdentificacionAnulacion, FechaAnulacion, Usuario, UsuarioIdentificador, FechaTransaccion, IDTransaccionEnvio From VMovimiento Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select IDProducto, ID, Ref, 'Descripcion'=Producto, IDDepositoEntrada, IDDepositoSalida, Cantidad, Observacion, PrecioUnitario, IDImpuesto, Impuesto, Total, TotalImpuesto, TotalDiscriminado, Caja, CantidadCaja, CodigoBarra From VDetalleMovimiento Where IDTransaccion=" & IDTransaccion & " Order By ID").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        IDTransaccionEnvio = oRow("IDTransaccionEnvio")
        If IDTransaccionEnvio > 0 Then
            Enviar = True
            Recibir = False
        Else
            Enviar = False
            Recibir = True
        End If
        CancelarTransferencia = False
        txtID.txt.Text = oRow("Numero").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        cbxTipoOperacion.cbx.Text = oRow("Operacion").ToString
        cbxMotivo.cbx.Text = oRow("Motivo").ToString
        cbxSubMotivo.cbx.Text = oRow("SubMotivo").ToString
        cbxTipoComprobante.Texto = oRow("Cod.").ToString
        txtComprobante.txt.Text = oRow("NroComprobante").ToString
        cbxDepositoEntrada.cbx.Text = oRow("DepositoEntrada").ToString
        cbxDepositoSalida.cbx.Text = oRow("DepositoSalida").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtTotalCantidad.txt.Text = oRow("Total").ToString

        If oRow("DepositoEntrada").ToString = "---" Then
            cbxDepositoEntrada.txt.Text = ""
        End If
        If oRow("DepositoSalida").ToString = "---" Then
            cbxDepositoSalida.txt.Text = ""
        End If

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("UsuarioIdentificador").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            VAnulado = True
            flpAnuladoPor.Visible = True
            btnAnular.Text = "Reprocesar"
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            If Enviar = False Then
                btnAnular.Visible = True
            Else
                btnAnular.Visible = False
            End If
        Else
            btnAnular.Visible = True
            flpAnuladoPor.Visible = False
            btnAnular.Text = "Anular"
            VAnulado = False
        End If

        'Cargamos el detalle
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)

        'Inicializamos el Asiento
        CAsiento.Limpiar()


    End Sub

    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.GetValue)

        'Tipo Operacion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO OPERACION", cbxTipoOperacion.cbx.Text)

        'Motivo
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MOTIVO", cbxMotivo.cbx.Text)

        'SubMotivo
        'CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUBMOTIVO", cbxSubMotivo.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Deposito de Origen
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DEPOSITO ORIGEN", cbxDepositoSalida.cbx.Text)

        'Deposito de Destino
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DEPOSITO DESTINO", cbxDepositoEntrada.cbx.Text)

        'Unidad
        CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "UNIDAD", cbxUnidad.Text)

    End Sub

    Sub LimpiarCampos()
        cbxTipoOperacion.cbx.Text = ""
        cbxMotivo.cbx.Text = ""
        cbxSubMotivo.cbx.Text = ""
        cbxTipoComprobante.cbx.Text = ""
        txtComprobante.txt.Clear()
        cbxDepositoSalida.cbx.Text = ""
        cbxDepositoSalida.Texto = ""
        cbxDepositoEntrada.cbx.Text = ""
        cbxDepositoEntrada.Texto = ""

        txtObservacion.txt.Clear()
        txtTotalCantidad.txt.ResetText()
    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Limpiar campos del Formulario
        LimpiarCampos()

        'Limpiar detalle
        dtDetalle.Rows.Clear()
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True
        Reprocesar = False
        lklSeleccionarProductos.Enabled = False

        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'SC: 23-09-2021 buscar numeracion para MOVSAL
        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) FROM VMovimiento Where IDSucursal = " & cbxSucursal.GetValue & " And IdTipoComprobante = " & IDTipoComprobante & "),1)"), Integer)
        txtComprobante.txt.Text = txtID.txt.Text
        txtComprobante.Enabled = False

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True
        Precargado = False
        txtObservacion.txt.Text = "Distribucion"
        txtObservacion.Enabled = True

        'Depositos
        dtDepositos = CSistema.ExecuteToDataTable(" Select * From VDeposito Where  id = 49 and Activo='True' and IDSUcursal= " & cbxSucursal.GetValue & " Order By 2").Copy

        'Deposito de Origen
        cbxDepositoEntrada.cbx.ValueMember = dtDepositos.Columns("ID").ToString
        cbxDepositoEntrada.cbx.DisplayMember = dtDepositos.Columns("Suc-Dep").ToString
        cbxDepositoEntrada.cbx.DataSource = dtDepositos.Copy
        cbxDepositoEntrada.Enabled = True

        'Deposito de Destino
        cbxDepositoSalida.cbx.ValueMember = dtDepositos.Columns("ID").ToString
        cbxDepositoSalida.cbx.DisplayMember = dtDepositos.Columns("Suc-Dep").ToString
        cbxDepositoSalida.cbx.DataSource = dtDepositos.Copy
        cbxDepositoSalida.Enabled = True

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = (CSistema.ExecuteScalar("Select Codigo From TipoComprobante where Estado = 1 and IDOperacion =" & IDOperacion))
        cbxTipoComprobante.Enabled = False

        txtCosto.Enabled = False

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento Where IDTipoMovimiento=" & cbxTipoOperacion.cbx.SelectedValue)

        'SubMotivos
        CSistema.SqlToComboBox(cbxSubMotivo.cbx, "Select ID, Descripcion From SubMotivoMovimiento Where IDTipoMovimiento=" & cbxTipoOperacion.cbx.SelectedValue & "And IdMotivoMovimiento=" & cbxMotivo.cbx.SelectedValue)

        'Poner el foco en la fecha
        txtFecha.txt.Focus()
        txtFecha.Hoy()
        txtProducto.txt.Text = ""
        txtCantidad.txt.Text = ""
        txtCosto.txt.Text = ""
    End Sub

    Sub Clonar()
        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)


        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True

        If cbxTipoOperacion.Texto = "TRANSFERENCIA" And CancelarTransferencia = False Then
            Enviar = True
            Recibir = False
        End If


        CAsiento.Limpiar()
        CAsiento.Inicializar()

        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Obtener registro clonado
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) FROM VMovimiento Where IDSucursal = " & cbxSucursal.GetValue & "),1)"), Integer)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True
        Precargado = False
        Reprocesar = True

        'Poner el foco en la fecha
        txtFecha.txt.Focus()

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) FROM VMovimiento Where MovimientoDistribucion = 'True' and IDSucursal = " & cbxSucursal.GetValue & "),1)"), Integer)

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        CargarOperacion()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Detalle
        If dgw.Rows.Count = 0 Then
            Dim mensaje As String = "El registro no tiene ningun detalle!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IDTransaccion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoOperacion", cbxTipoOperacion.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMotivo", cbxMotivo.cbx.SelectedValue, ParameterDirection.Input)
        If IsNumeric(cbxSubMotivo.cbx.SelectedValue) = False Then
            CSistema.SqlToComboBox(cbxSubMotivo.cbx, "Select ID, Descripcion From SubMotivoMovimiento where Estado = 1 and Descripcion like '%---%'")
        End If
        CSistema.SetSQLParameter(param, "@IDSubMotivo", cbxSubMotivo.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoSalida", cbxDepositoSalida.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoEntrada", cbxDepositoEntrada.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Autorizacion", "", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(txtTotalCantidad.ObtenerValor), ParameterDirection.Input)

        'Totales
        CSistema.SetSQLParameter(param, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalImpuesto")), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalDiscriminado")), ParameterDirection.Input)


        'Transferencias
        If cbxTipoOperacion.Texto = "TRANSFERENCIA" And CancelarTransferencia = False Then
            CSistema.SetSQLParameter(param, "@Enviar", Enviar, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Recibir", Recibir, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Recibido", Recibir, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccionEnvio", IDTransaccionEnvio, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Tipo de movimientos combustible
        CSistema.SetSQLParameter(param, "@MovimientoDistribucion", True, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input) 'JGR 20140816
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpMovimiento", False, False, MensajeRetorno, IDTransaccion) = False Then

            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            'Eliminar el Registro

            Exit Sub
        Else
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
        End If

        If IDTransaccion > 0 Then

            'Insertamos el Detalle
            If Reprocesar Then
                If InsertarDetalleReprocesado(IDTransaccion, ERP.CSistema.NUMOperacionesRegistro.INS) = False Then

                    'Eliminar Registro

                    Exit Sub

                End If
            Else
                If InsertarDetalle(IDTransaccion, ERP.CSistema.NUMOperacionesRegistro.INS) = False Then

                    'Eliminar Registro

                    Exit Sub

                End If
            End If


            'Cargamos el DetalleImpuesto
            CDetalleImpuesto.Guardar(IDTransaccion)

        End If
        ' CAsiento.ObtenerSaldo()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False

    End Sub

    Function InsertarDetalleReprocesado(ByVal IDTransaccion As Integer, ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        InsertarDetalleReprocesado = True

        For Each oRow As DataRow In dtDetalle.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDProducto", oRow("IDProducto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", oRow("ID").ToString, ParameterDirection.Input)

            If CBool(dtOperaciones.Select(" ID=" & cbxTipoOperacion.cbx.SelectedValue)(0)("Salida").ToString) = True Then
                CSistema.SetSQLParameter(param, "@IDDepositoSalida", oRow("IDDepositoSalida").ToString, ParameterDirection.Input)
            End If

            If CBool(dtOperaciones.Select(" ID=" & cbxTipoOperacion.cbx.SelectedValue)(0)("Entrada").ToString) = True Then
                CSistema.SetSQLParameter(param, "@IDDepositoEntrada", oRow("IDDepositoEntrada").ToString, ParameterDirection.Input)
            End If

            CSistema.SetSQLParameter(param, "@Observacion", oRow("Observacion").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cantidad", CSistema.FormatoMonedaBaseDatos(oRow("Cantidad").ToString.Replace(",0000", ""), True), ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@IDImpuesto", oRow("IDImpuesto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@PrecioUnitario", oRow("PrecioUnitario").ToString.Replace(",0000", ""), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString.Replace(",0000", ""), False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalImpuesto", oRow("TotalImpuesto").ToString.Replace(",0000", ""), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalDiscriminado", oRow("TotalDiscriminado").ToString.Replace(",0000", ""), ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Caja", oRow("Caja").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@CantidadCaja", oRow("CantidadCaja").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""


            If CSistema.ExecuteStoreProcedure(param, "SpDetalleMovimiento", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

                'Eliminar el Registro

                Return False

            End If

        Next

    End Function

    Function InsertarDetalle(ByVal IDTransaccion As Integer, ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        InsertarDetalle = True

        For Each oRow As DataRow In dtDetalle.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDProducto", oRow("IDProducto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", oRow("ID").ToString, ParameterDirection.Input)

            If CBool(dtOperaciones.Select(" ID=" & cbxTipoOperacion.cbx.SelectedValue)(0)("Salida").ToString) = True Then
                CSistema.SetSQLParameter(param, "@IDDepositoSalida", oRow("IDDepositoSalida").ToString, ParameterDirection.Input)
            End If

            If CBool(dtOperaciones.Select(" ID=" & cbxTipoOperacion.cbx.SelectedValue)(0)("Entrada").ToString) = True Then
                CSistema.SetSQLParameter(param, "@IDDepositoEntrada", oRow("IDDepositoEntrada").ToString, ParameterDirection.Input)
            End If

            CSistema.SetSQLParameter(param, "@Observacion", oRow("Observacion").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cantidad", CSistema.FormatoMonedaBaseDatos(oRow("Cantidad").ToString, True), ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@IDImpuesto", oRow("IDImpuesto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@PrecioUnitario", oRow("PrecioUnitario").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalImpuesto", oRow("TotalImpuesto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalDiscriminado", oRow("TotalDiscriminado").ToString, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Caja", oRow("Caja").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@CantidadCaja", oRow("CantidadCaja").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpDetalleMovimiento", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

                'Eliminar el Registro

                Return False

            End If

        Next

    End Function

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

    End Sub

    Sub CargarProducto()

        'Validar
        'Seleccion de Producto
        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'validar que el producto tenga Distribucion ='True'
        Dim Distribucion As Boolean = CData.GetRow(" ID = " & txtProducto.Registro("ID").ToString(), "VProducto")("Distribucion")
        If txtProducto.Seleccionado = True And Distribucion = False Then
            Dim mensaje As String = "El producto no esta habilitado para Distribucion!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Operacion
        If cbxTipoOperacion.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente el tipo de operacion!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If cbxTipoOperacion.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el tipo de operacion!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Deposito Salida
        If cbxDepositoSalida.cbx.Enabled = True Then
            If IsNumeric(cbxDepositoSalida.cbx.SelectedValue) = False Then
                Dim mensaje As String = "Seleccione correctamente el deposito de salida!"
                ctrError.SetError(txtCantidad, mensaje)
                ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            Else
                If cbxDepositoSalida.cbx.Text = "" Then
                    Dim mensaje As String = "Seleccione correctamente el deposito de salida!"
                    ctrError.SetError(txtCantidad, mensaje)
                    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If

                If cbxDepositoSalida.cbx.SelectedValue = Nothing Then
                    Dim mensaje As String = "Seleccione correctamente el deposito de salida!"
                    ctrError.SetError(txtCantidad, mensaje)
                    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If

            End If
        End If

        'Seleccion de Deposito Entrada
        If cbxDepositoEntrada.cbx.Enabled = True Then
            If IsNumeric(cbxDepositoEntrada.cbx.SelectedValue) = False Then
                Dim mensaje As String = "Seleccione correctamente el deposito de entrada!"
                ctrError.SetError(txtCantidad, mensaje)
                ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            Else
                If cbxDepositoEntrada.cbx.Text = "" Then
                    Dim mensaje As String = "Seleccione correctamente el deposito de entrada!"
                    ctrError.SetError(txtCantidad, mensaje)
                    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If

                If cbxDepositoEntrada.cbx.SelectedValue = Nothing Then
                    Dim mensaje As String = "Seleccione correctamente el deposito de entrada!"
                    ctrError.SetError(txtCantidad, mensaje)
                    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If

            End If
        End If

        'Verificar que no se transfiera al mismo tipo de deposito y con Transferir = False
        If cbxDepositoEntrada.Enabled = True And cbxDepositoSalida.Enabled = True And cbxTipoOperacion.cbx.Text = "TRANSFERENCIA" Then
            Dim IDTipoDepositoEntrada As Integer = CSistema.RetornarValorInteger(CData.GetRow(" ID = " & cbxDepositoEntrada.GetValue(), "VDeposito")("IDTipoDeposito").ToString)
            Dim IDTipoDepositoSalida As Integer = CSistema.RetornarValorInteger(CData.GetRow(" ID = " & cbxDepositoSalida.GetValue(), "VDeposito")("IDTipoDeposito").ToString)

            'Validar tipos de depositos
            If IDTipoDepositoEntrada = 0 Or IDTipoDepositoSalida = 0 Then
                MessageBox.Show("Atencion! Uno de los depositos elegidos no tiene definido el tipo de deposito! Se debe primeramente definir para continuar.", "Transferencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)
                Exit Sub
            End If

            Dim Transferencia As Boolean = CData.GetRow(" ID = " & IDTipoDepositoEntrada, "VTipoDeposito")("Transferir")

            'Si los depositos son de tipos iguales y el tipo de deposito no permite la transferencia
            If IDTipoDepositoEntrada = IDTipoDepositoSalida And Transferencia = False Then
                MessageBox.Show("Atencion! No se puede transferir entre estos depositos", "Transferencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)
                Exit Sub
            End If

        End If

        'Cantidad
        If IsNumeric(txtCantidad.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtCantidad.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Verificar que exista el producto en el deposito de salida
        Dim ExistenciaActual As Decimal = 0
        Dim CantidadProducto As Decimal = 0
        Dim CantidadCaja As Integer = 0
        Dim EsCaja As Boolean = False

        'Si es por unidad, cantidad igual a seleccionado
        'Si es por caja, multiplicar
        If cbxUnidad.Text = "UNIDAD" Then
            CantidadProducto = txtCantidad.ObtenerValor
            EsCaja = False
        Else
            CantidadCaja = txtCantidad.ObtenerValor
            CantidadProducto = CInt(txtProducto.Registro("UnidadPorCaja").ToString) * CantidadCaja
            EsCaja = True
        End If

        'If cbxDepositoSalida.SoloLectura = False Then

        'Obtenemos la cantidad que existe en el deposito el producto seleccionado
        ExistenciaActual = CSistema.ExecuteScalar("Select dbo.FExistenciaProducto(" & txtProducto.Registro("ID").ToString & ", " & cbxDepositoSalida.cbx.SelectedValue & ")")

            'Obtenemos la cantidad de productos incluyendo los que estan en el detalle
            For Each oRow As DataRow In dtDetalle.Rows
                If oRow("IDProducto").ToString = txtProducto.Registro("ID").ToString Then
                    CantidadProducto = CantidadProducto + CDec(oRow("Cantidad").ToString)
                End If
            Next
        If cbxDepositoSalida.SoloLectura = False Then
            If CantidadProducto > ExistenciaActual Then
                Dim mensaje As String = "Stock insuficiente! El sistema tiene " & ExistenciaActual & " producto(s) registrado(s)"
                ctrError.SetError(txtCantidad, mensaje)
                ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If
        'End If

        Dim CantidadAnterior As Integer = 0

        'Si existe, sumar la cantidad
        If dtDetalle.Select(" IDProducto=" & txtProducto.Registro("ID")).Count > 0 Then

            Dim Rows() As DataRow = dtDetalle.Select(" IDProducto=" & txtProducto.Registro("ID"))
            Rows(0)("Cantidad") = CantidadProducto
            Rows(0)("Total") = CantidadProducto * CDec(txtCosto.ObtenerValor)
            Rows(0)("TotalImpuesto") = 0
            Rows(0)("TotalDiscriminado") = 0
            CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(Rows(0)("Total").ToString), False, True, Rows(0)("TotalDiscriminado"), Rows(0)("TotalImpuesto"))

            Rows(0)("Caja") = EsCaja.ToString
            Rows(0)("CantidadCaja") = CantidadCaja

        Else

            ' Insertar
            'Cargamos el registro en el detalle
            Dim dRow As DataRow = dtDetalle.NewRow()

            'Obtener Valores
            dRow("IDProducto") = txtProducto.Registro("ID").ToString
            dRow("Ref") = txtProducto.Registro("Ref").ToString
            dRow("ID") = dtDetalle.Rows.Count
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString

            If Not cbxDepositoEntrada.cbx.SelectedValue Is Nothing Then
                dRow("IDDepositoEntrada") = cbxDepositoEntrada.cbx.SelectedValue
            End If

            If Not cbxDepositoSalida.cbx.SelectedValue Is Nothing Then
                dRow("IDDepositoSalida") = cbxDepositoSalida.cbx.SelectedValue
            End If

            dRow("Cantidad") = CantidadProducto
            dRow("Observacion") = txtObservacionProducto.txt.Text

            'Impuestos
            dRow("IDImpuesto") = txtProducto.Registro("IDImpuesto").ToString
            dRow("Impuesto") = txtProducto.Registro("Impuesto").ToString

            'Totales
            dRow("PrecioUnitario") = txtCosto.ObtenerValor
            dRow("Total") = CantidadProducto * CDec(txtCosto.ObtenerValor)
            dRow("TotalImpuesto") = 0
            dRow("TotalDiscriminado") = 0
            CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("Total").ToString), False, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))


            dRow("Caja") = EsCaja.ToString
            dRow("CantidadCaja") = CantidadCaja
            dRow("CodigoBarra") = txtProducto.Registro("CodigoBarra").ToString

            'Agregamos al detalle
            dtDetalle.Rows.Add(dRow)

        End If

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)

        'Inicializamos los valores
        txtObservacionProducto.txt.Clear()
        txtCantidad.txt.Text = "0"
        txtCosto.txt.Text = "0"
        txtProducto.txt.Clear()

        'Retorno de Foco
        txtProducto.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Public Sub CargarMovimiento(ByVal dtProductosCargar As DataTable, ByVal dtCabeceraCargar As DataTable, ByVal Origen As String)

        If (dtProductosCargar Is Nothing) Or (dtProductosCargar.Rows.Count = 0) Then
            Exit Sub
        End If

        If (dtCabeceraCargar Is Nothing) Or (dtCabeceraCargar.Rows.Count = 0) Then
            Exit Sub
        End If

        cbxSucursal.cbx.SelectedValue = dtCabeceraCargar.Rows(0)("IDSucursal")
        Nuevo()
        cbxTipoOperacion.cbx.Text = "SALIDA"

        Select Case Origen
            Case "Panaderia"
                Dim dtOrigen As DataTable
                dtOrigen = CSistema.ExecuteToDataTable("Select * from ConfiguracionMuestraPanaderia")
                cbxTipoComprobante.cbx.SelectedValue = dtOrigen.Rows(0)("IDTipoComprobante")
                cbxMotivo.cbx.SelectedValue = dtOrigen.Rows(0)("IDMotivo")
        End Select
        txtComprobante.Texto = dtCabeceraCargar.Rows(0)("Numero")


        cbxDepositoSalida.cbx.SelectedValue = CData.GetRow(" TIPODEPOSITO = 'DEVOLUCION' and IDSucursal = " & cbxSucursal.GetValue, "VDeposito")("ID")

        For Each Row As DataRow In dtProductosCargar.Rows

            'Verificar que exista el producto en el deposito de salida
            Dim ExistenciaActual As Decimal = 0
            Dim CantidadProducto As Decimal = Row("Cantidad")
            Dim CostoUnitario As Decimal = CSistema.ExecuteScalar("Select dbo.FCostoProductoFechaKardex(" & Row("IDProducto") & ",'" & CSistema.FormatoFechaBaseDatos(dtCabeceraCargar.Rows(0)("Fecha"), True, False) & "')")

            If cbxDepositoSalida.SoloLectura = False Then

                'Obtenemos la cantidad que existe en el deposito el producto seleccionado
                ExistenciaActual = CSistema.ExecuteScalar("Select dbo.FExistenciaProducto(" & Row("IDProducto") & ", " & cbxDepositoSalida.cbx.SelectedValue & ")")

                'Obtenemos la cantidad de productos incluyendo los que estan en el detalle
                For Each oRow As DataRow In dtDetalle.Rows
                    If oRow("IDProducto").ToString = Row("IDProducto") Then
                        CantidadProducto = CantidadProducto + CDec(oRow("Cantidad").ToString)
                    End If
                Next

                If CantidadProducto > ExistenciaActual Then
                    MessageBox.Show("Stock insuficiente! El sistema tiene " & ExistenciaActual & " del producto " & Row("Producto"))
                    Exit Sub
                End If

            End If

            'Si existe, sumar la cantidad
            If dtDetalle.Select(" IDProducto=" & Row("IDProducto")).Count > 0 Then
                Dim Rows() As DataRow = dtDetalle.Select(" IDProducto=" & Row("IDProducto"))
                Rows(0)("Cantidad") = CantidadProducto
                Rows(0)("Total") = CantidadProducto * CostoUnitario
                Rows(0)("TotalImpuesto") = 0
                Rows(0)("TotalDiscriminado") = 0
                CSistema.CalcularIVA(Row("IDImpuesto"), CDec(Rows(0)("Total").ToString), False, True, Rows(0)("TotalDiscriminado"), Rows(0)("TotalImpuesto"))

                Rows(0)("Caja") = False
                Rows(0)("CantidadCaja") = 1

            Else

                ' Insertar
                'Cargamos el registro en el detalle
                Dim dRow As DataRow = dtDetalle.NewRow()

                'Obtener Valores
                dRow("IDProducto") = Row("IDProducto")
                dRow("Ref") = Row("Referencia")
                dRow("ID") = dtDetalle.Rows.Count
                dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString

                If Not cbxDepositoEntrada.cbx.SelectedValue Is Nothing Then
                    dRow("IDDepositoEntrada") = cbxDepositoEntrada.cbx.SelectedValue
                End If

                If Not cbxDepositoSalida.cbx.SelectedValue Is Nothing Then
                    dRow("IDDepositoSalida") = cbxDepositoSalida.cbx.SelectedValue
                End If

                dRow("Cantidad") = CantidadProducto
                dRow("Observacion") = Row("MotivoDevolucion")

                'Impuestos
                dRow("IDImpuesto") = Row("IDImpuesto")
                dRow("Impuesto") = Row("Impuesto")

                'Totales
                dRow("PrecioUnitario") = CostoUnitario
                dRow("Total") = CantidadProducto * CostoUnitario
                dRow("TotalImpuesto") = 0
                dRow("TotalDiscriminado") = 0
                CSistema.CalcularIVA(Row("IDImpuesto"), CDec(dRow("Total").ToString), False, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))


                dRow("Caja") = False
                dRow("CantidadCaja") = 1
                dRow("CodigoBarra") = Row("CodigoBarra")

                'Agregamos al detalle
                dtDetalle.Rows.Add(dRow)

            End If
        Next
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)

        'Inicializamos los valores
        txtObservacionProducto.Enabled = False
        txtCantidad.Enabled = False
        txtCosto.Enabled = False
        txtProducto.Enabled = False

        Precargado = True

    End Sub

    Sub EliminarProducto()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If dgw.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgw, mensaje)
            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If Precargado Then
            MessageBox.Show("No es posible modificar movimiento precargado.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        Dim IDProducto As Integer = dgw.SelectedRows(0).Cells("IDProducto").Value
        Dim dtAux As DataTable = dtDetalle.Clone()

        For Each Row As DataRow In dtDetalle.Rows
            If Row("IDProducto") = IDProducto Then
                GoTo Seguir
            End If
            Dim NewRow As DataRow = dtAux.NewRow()
            For I = 0 To dtDetalle.Columns.Count - 1
                NewRow(I) = Row(I)
            Next
            dtAux.Rows.Add(NewRow)
Seguir:

        Next

        'dtDetalle.Rows(dgw.SelectedRows(0).Index).Delete()
        dtDetalle.Clear()
        dtDetalle = dtAux.Copy()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtDetalle.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)

    End Sub

    Sub ListarDetalle()

        'Variables
        'Dim Total As Decimal = 0
        Dim TotalCantidad As Decimal = 0

        'Cargamos registro por registro
        CSistema.dtToGrid(dgw, dtDetalle)

        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).Visible = False
        Next

        dgw.Columns("Ref").DisplayIndex = 1
        dgw.Columns("Descripcion").DisplayIndex = 2
        dgw.Columns("Cantidad").DisplayIndex = 3
        'dgw.Columns("PrecioUnitario").DisplayIndex = 4
        'dgw.Columns("Total").DisplayIndex = 5

        dgw.Columns("Ref").Visible = True
        dgw.Columns("Descripcion").Visible = True
        dgw.Columns("Cantidad").Visible = True
        'dgw.Columns("PrecioUnitario").Visible = True
        'dgw.Columns("Total").Visible = True

        dgw.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns("Cantidad").DefaultCellStyle.Format = "N2"
        'dgw.Columns("PrecioUnitario").DefaultCellStyle.Format = "N0"
        'dgw.Columns("Total").DefaultCellStyle.Format = "N0"

        dgw.Columns("Ref").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgw.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        'dgw.Columns("PrecioUnitario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        'dgw.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgw.Columns("Cantidad").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        'dgw.Columns("PrecioUnitario").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        'dgw.Columns("Total").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

        'dgw.Columns("PrecioUnitario").HeaderText = "Costo"

        'Total = CSistema.dtSumColumn(dtDetalle, "Total")
        TotalCantidad = CSistema.dtSumColumn(dtDetalle, "Cantidad")

        'Bloqueamos la cabecera si corresponde
        If dtDetalle.Rows.Count > 0 And vNuevo = True Then
            cbxTipoOperacion.cbx.Enabled = False
            cbxMotivo.cbx.Enabled = False
            cbxDepositoEntrada.cbx.Enabled = False
            cbxDepositoSalida.cbx.Enabled = False
        Else
            cbxTipoOperacion.cbx.Enabled = True
            cbxMotivo.cbx.Enabled = True
            cbxTipoOperacion.cbx.SelectedValue = cbxTipoOperacion.GetValue
            cbxDepositoEntrada.cbx.Enabled = True
            cbxDepositoSalida.cbx.Enabled = True
        End If

        If dtDetalle.Rows.Count > 0 Then
            lklSeleccionarProductos.Enabled = False
        Else
            If vNuevo And Recibir Then
                lklSeleccionarProductos.Enabled = True
            End If
        End If

        'Calculamos el Total
        txtTotalCantidad.txt.Text = TotalCantidad


    End Sub

    Sub Anular()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.ANULAR.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Anular
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpMovimiento", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False

    End Sub

    Sub Imprimir()

        Dim CMovimiento As New Reporte.CReporteStock
        CMovimiento.MovimientoStock(IDTransaccion)

    End Sub

    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'Otros
        lblAnulado.Visible = False

        Dim frm As New frmConsultaMovimientoDistribucion
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.TipoMovimiento = "MovimientoStockDistribucion"
        FGMostrarFormulario(Me, frm, "Consulta de Registros", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False, True)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID)
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Num), " & ID & ") From VMovimiento Where Num > " & ID & " and MovimientoDistribucion = 'True' and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID)
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Num), " & ID & ") From VMovimiento Where Num < " & ID & " and MovimientoDistribucion = 'True' and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Num), " & ID & ") From VMovimiento Where MovimientoDistribucion = 'True' and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Num), " & ID & ") From VMovimiento Where MovimientoDistribucion = 'True' and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If
    End Sub

    Sub GenerarTipoOperacion()

        'Validar si ya existe
        If dtOperaciones.Rows.Count > 0 Then
            Exit Sub
        End If

        Dim SQL As String = ""
        SQL = SQL & "Insert Into TipoOperacion(ID, IDOperacion, Descripcion, Activo, Entrada, Salida) Values(1, " & IDOperacion & ", 'ENTRADA', 1, 1, 0)" & vbCrLf
        SQL = SQL & "Insert Into TipoOperacion(ID, IDOperacion, Descripcion, Activo, Entrada, Salida) Values(2, " & IDOperacion & ", 'SALIDA', 1, 0, 1)" & vbCrLf
        SQL = SQL & "Insert Into TipoOperacion(ID, IDOperacion, Descripcion, Activo, Entrada, Salida) Values(3, " & IDOperacion & ", 'TRANSFERENCIA', 1, 1, 1)" & vbCrLf

        CSistema.ExecuteNonQuery(SQL)

        'Volvemos a cargar
        dtOperaciones = CSistema.ExecuteToDataTable("Select ID, Descripcion, Activo, Entrada, Salida From TipoOperacion Where IDOperacion=" & IDOperacion)

    End Sub

    Private Sub OcxTXTProducto1_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado

        'validar que el producto tenga Distribucion ='True'
        Dim Distribucion As Boolean = CData.GetRow(" ID = " & txtProducto.Registro("ID").ToString(), "VProducto")("Distribucion")
        If txtProducto.Seleccionado = True And Distribucion = False Then
            Dim mensaje As String = "El producto no esta habilitado para Distribucion!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If txtProducto.Seleccionado = True Then

            'Obtenemos el costo
            Dim Costo As Decimal = 0
            If cbxTipoComprobante.cbx.SelectedValue = CInt(vgConfiguraciones("IDTipoComprobanteProduccion")) Then
                'Costo = CSistema.ExecuteScalar("select dbo.FCostoProduccion(" & txtProducto.Registro("ID").ToString & ", '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' )")
                Costo = CSistema.ExecuteScalar("Select ISNULL((Select Top(1) Costo from vDetalleProductoCosto where Fecha <=" & txtFecha.txt.Text & " And IDProducto =" & txtProducto.Registro("ID").ToString & " order by Fecha DESC" & "), 0)")
                If Costo = 0 Then
                    Costo = CSistema.ExecuteScalar("Select isnull(CostoPromedio, 0) from vProducto where id =" & txtProducto.Registro("ID").ToString & "AND Distribucion = 'TRUE'")
                    If Costo = 0 Then
                        Costo = txtProducto.Registro("Costo").ToString()
                    End If
                End If
            Else
                Costo = txtProducto.Registro("Costo").ToString()
            End If

            txtCosto.txt.Text = Costo
            txtCantidad.Focus()

            If CBool(vgConfiguraciones("MovimientoBloquearCampoCosto").ToString) = True Then

                txtCosto.SoloLectura = True

                If Costo = 0 Then
                    txtCosto.SoloLectura = False
                End If

            Else
                txtCosto.SoloLectura = False
            End If

        End If

    End Sub

    Private Sub frmMovimientoStock_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmMovimientodistribucion_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = vgKeyConsultar Then

            'Si esta en el producto
            If txtProducto.Focus = True Then

                'Transferencia
                If cbxDepositoEntrada.SoloLectura = False And cbxDepositoSalida.SoloLectura = False Then
                    txtProducto.Consulta = "Select Top(100) ID, Descripcion, 'Codigo de Barra'=CodigoBarra, Ref, 'Tipo de Producto'=TipoProducto, 'Dep. Entrada'=(Select dbo.FExistenciaProducto(ID, " & cbxDepositoEntrada.cbx.SelectedValue & ")), 'Dep. Salida'=(Select dbo.FExistenciaProducto(ID, " & cbxDepositoSalida.cbx.SelectedValue & ")) From VProducto Where ID>0 and Distribucion='True' "
                    txtProducto.IDDeposito = cbxDepositoSalida.cbx.SelectedValue
                    txtProducto.ColumnasNumericas = {"5", "6"}
                    Exit Sub
                End If

                'Entrada
                If cbxDepositoEntrada.SoloLectura = False And cbxDepositoSalida.SoloLectura = True Then
                    txtProducto.Consulta = "Select Top(100) ID, Descripcion, 'Codigo de Barra'=CodigoBarra, Ref, 'Tipo de Producto'=TipoProducto, 'Dep. Entrada'=(Select dbo.FExistenciaProducto(ID, " & cbxDepositoEntrada.cbx.SelectedValue & ")) From VProducto Where ID>0 and Distribucion='True' "
                    txtProducto.IDDeposito = cbxDepositoEntrada.GetValue
                    txtProducto.ColumnasNumericas = {"5"}
                    Exit Sub
                End If

                'Salida
                'If cbxDepositoEntrada.SoloLectura = False And cbxDepositoSalida.SoloLectura = False Then
                '    txtProducto.Consulta = "Select Top(100) ID, Descripcion, 'Codigo de Barra'=CodigoBarra, Ref, 'Tipo de Producto'=TipoProducto, 'Dep. Salida'=(Select dbo.FExistenciaProducto(ID, " & cbxDepositoSalida.cbx.SelectedValue & ")) From VProducto Where ID>0 and Distribucion='True'"
                '    txtProducto.IDDeposito = cbxDepositoSalida.cbx.SelectedValue
                '    txtProducto.ColumnasNumericas = {"5"}
                '    Exit Sub
                'End If

                If cbxDepositoEntrada.SoloLectura = True And cbxDepositoSalida.SoloLectura = False Then
                    txtProducto.Consulta = "Select Top(100) ID, Descripcion, 'Codigo de Barra'=CodigoBarra, Ref, 'Tipo de Producto'=TipoProducto, 'Dep. Salida'=(Select dbo.FExistenciaProducto(ID, " & cbxDepositoSalida.cbx.SelectedValue & ")) From VProducto Where ID>0 and Distribucion='True'"
                    txtProducto.IDDeposito = cbxDepositoSalida.cbx.SelectedValue
                    txtProducto.ColumnasNumericas = {"5"}
                    Exit Sub
                End If
            End If

            'Buscar()

            Exit Sub

        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub frmMovimientoDistribucion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        If VAnulado Then
            Clonar()
        Else
            Anular()
        End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtCantidad_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub cbxMotivo_Editar(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxMotivo.Editar
        Dim frm As New frmMotivo
        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        cbxMotivo.cbx.DataSource = Nothing

        If IsNumeric(cbxTipoOperacion.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento Where IDTipoMovimiento=" & cbxTipoOperacion.cbx.SelectedValue)

        'Habilitar e Inhabilitar Depositos
        Dim oRow As DataRow = dtOperaciones.Select(" ID=" & cbxTipoOperacion.cbx.SelectedValue)(0)
        cbxDepositoSalida.cbx.Enabled = CBool(oRow("Salida").ToString)
        cbxDepositoEntrada.cbx.Enabled = CBool(oRow("Entrada").ToString)

    End Sub

    'Private Sub txtCosto_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCosto.TeclaPrecionada
    '    If e.KeyCode = Keys.Enter Then
    '        CargarProducto()
    '    End If
    'End Sub

    'Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
    'VisualizarAsiento()
    'End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If
    End Sub

    Private Sub txtFecha_Leave(sender As Object, e As EventArgs) Handles txtFecha.Leave
        If vNuevo Then
            Dim DiferenciaDias As Long
            Dim date1 As Date = Now
            If CBool(vgConfiguraciones("MovimientoBloquearFecha").ToString) = False Then
                Exit Sub
            End If
            Try

                DiferenciaDias = DateDiff(DateInterval.Day, Date.Parse(Now.ToShortDateString), Date.Parse(txtFecha.txt.Text))
                If DiferenciaDias > 0 Then
                    If CInt(vgConfiguraciones("MovimientoCantidadDiasHabilitados")) < DiferenciaDias Then
                        txtFecha.SetValue(VGFechaHoraSistema)
                    End If
                Else
                    If CInt(vgConfiguraciones("MovimientoCantidadDiasHabilitados")) < -DiferenciaDias Then
                        txtFecha.SetValue(VGFechaHoraSistema)
                    End If
                End If
            Catch ex As Exception
                txtFecha.SetValue(VGFechaHoraSistema)
            End Try

        End If

    End Sub

    Private Sub cbxTipoComprobante_PropertyChanged(sender As Object, e As EventArgs) Handles cbxTipoComprobante.PropertyChanged
        If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
            lblNombreDocumento.Text = "---"
        Else
            lblNombreDocumento.Text = dtComprobantes.Select("ID = " & cbxTipoComprobante.cbx.SelectedValue)(0)("Descripcion").ToString
        End If

    End Sub

    Private Sub lklSeleccionarProductos_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lklSeleccionarProductos.LinkClicked
        Dim frmTransferencias As New frmTransferenciasPendientes
        frmTransferencias.Recibir = True
        frmTransferencias.IDSucursalDestino = cbxSucursal.cbx.SelectedValue
        frmTransferencias.ShowDialog()

        If frmTransferencias.Procesado Then
            dtDetalle = frmTransferencias.dtDetalle
            cbxDepositoSalida.cbx.Text = frmTransferencias.DepositoEnviado
            IDTransaccionEnvio = frmTransferencias.IDTransaccion
            ListarDetalle()
        End If

    End Sub

    Private Sub btnClonar_Click(sender As Object, e As EventArgs)
        Clonar()
    End Sub

    Sub frmMovimientoDistribucion_Activate()
        Me.Refresh()
    End Sub

    Private Sub cbxTipoOperacion_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxTipoOperacion.TeclaPrecionada
        If vNuevo = False Then
            Exit Sub
        End If

        If IsNumeric(cbxTipoOperacion.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento Where IDTipoMovimiento=" & cbxTipoOperacion.cbx.SelectedValue)

        'Habilitar e Inhabilitar Depositos
        Dim oRow As DataRow = dtOperaciones.Select(" ID=" & cbxTipoOperacion.cbx.SelectedValue)(0)
        cbxDepositoSalida.SoloLectura = Not CBool(oRow("Salida").ToString)
        cbxDepositoSalida.txt.Text = ""
        cbxDepositoEntrada.SoloLectura = Not CBool(oRow("Entrada").ToString)
        cbxDepositoEntrada.txt.Text = ""

    End Sub

    Private Sub cbxMotivo_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxMotivo.TeclaPrecionada
        If vNuevo = False Then
            Exit Sub
        End If


        If IsNumeric(cbxTipoOperacion.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        'SubMotivos
        CSistema.SqlToComboBox(cbxSubMotivo.cbx, "Select ID, Descripcion From SubMotivoMovimiento Where IDTipoMovimiento=" & cbxTipoOperacion.cbx.SelectedValue & "And IdMotivoMovimiento = " & cbxMotivo.cbx.SelectedValue)

        If cbxSubMotivo.cbx.Items.Count = 0 Then
            cbxSubMotivo.Enabled = False
        Else
            cbxSubMotivo.Enabled = True
        End If

    End Sub

    Private Sub cbxTipoOperacion_PropertyChanged(sender As Object, e As EventArgs) Handles cbxTipoOperacion.PropertyChanged
        If vNuevo = False Then
            Exit Sub
        End If

        If IsNumeric(cbxTipoOperacion.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento Where IDTipoMovimiento=" & cbxTipoOperacion.cbx.SelectedValue)

        'Habilitar e Inhabilitar Depositos
        Dim oRow As DataRow = dtOperaciones.Select(" ID=" & cbxTipoOperacion.cbx.SelectedValue)(0)
        cbxDepositoSalida.SoloLectura = Not CBool(oRow("Salida").ToString)
        cbxDepositoSalida.txt.Text = ""
        cbxDepositoEntrada.SoloLectura = Not CBool(oRow("Entrada").ToString)
        cbxDepositoEntrada.txt.Text = ""
    End Sub

    Private Sub cbxMotivo_PropertyChanged(sender As Object, e As EventArgs) Handles cbxMotivo.PropertyChanged
        If vNuevo = False Then
            Exit Sub
        End If

        If IsNumeric(cbxTipoOperacion.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        'SubMotivos
        CSistema.SqlToComboBox(cbxSubMotivo.cbx, "Select ID, Descripcion From SubMotivoMovimiento Where IDTipoMovimiento=" & cbxTipoOperacion.cbx.SelectedValue & "And IdMotivoMovimiento = " & cbxMotivo.cbx.SelectedValue)

        If cbxSubMotivo.cbx.Items.Count = 0 Then
            cbxSubMotivo.Enabled = False
        Else
            cbxSubMotivo.Enabled = True
        End If

    End Sub

End Class
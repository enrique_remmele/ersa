﻿Public Class frmConsultaRemisionExterna

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private SoloConsultaValue As Boolean = True
    Public Property SoloConsulta() As Boolean
        Get
            Return SoloConsultaValue
        End Get
        Set(ByVal value As Boolean)
            SoloConsultaValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        IDOperacion = CSistema.ObtenerIDOperacion("frmRemisionExterna", "REMISION EXTERNA", "REMEXT")

        'TextBox
        txtCantidadDetalle.txt.ResetText()
        txtCantidadOperacion.txt.ResetText()

        'CheckBox
        chkUsuario.Checked = False
        chkSucursal.Checked = False

        'ComboBox
        cbxUsuario.Enabled = False
        cbxSucursal.Enabled = False

        'RadioButton

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        CargarInformacion()

        If SoloConsulta = True Then
            btnSeleccionar.Visible = False
        End If

        'Foco

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxComprobante, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Usuarios
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxUsuario, "Select ID, Usuario From Usuario Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

        'Comprobante
        cbxComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "COMPROBANTE", "")

        'Usuario
        chkUsuario.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "USUARIO ACTIVO", "False")
        cbxUsuario.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "USUARIO", "")

    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCRUSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "COMPROBANTE", cbxComprobante.Text)

        'Usuario
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "USUARIO ACTIVO", chkUsuario.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "USUARIO", cbxUsuario.Text)

    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Operaciones
    Sub ListarOperaciones(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "")

        ctrError.Clear()

        Consulta = "Select 'Suc'=[Suc Operacion], 'Tipo'=TipoComprobante, Comprobante, 'Fecha'=Fecha, 'Ref' = Referencia, Cliente, Observacion, Usuario, IDTransaccion From VRemisionExterna V "

        Where = Condicion

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la Sucursal!") = False Then
            Exit Sub
        End If

        'Usuario
        If EstablecerCondicion(cbxUsuario, chkUsuario, "IDUsuario", "Seleccione correctamente el usuario!") = False Then
            Exit Sub
        End If

        CSistema.SqlToDataGrid(lvOperacion, Consulta & " " & Where & " Order By Fecha")

        If lvOperacion.ColumnCount > 0 Then
            lvOperacion.Columns("IDTransaccion").Visible = False
        End If

        'Formato
        lvOperacion.Columns("Cliente").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        'Totales
        txtCantidadOperacion.txt.Text = lvOperacion.Rows.Count


    End Sub

    'Listar Detalle
    Sub ListarDetalle()

        ctrError.Clear()

        'Validar
        If lvOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = lvOperacion.SelectedRows(0).Cells("IDTransaccion").Value
        Dim Consulta As String = "Select 'Codigo'=Referencia, Producto, CodigoBarra, Cantidad From VDetalleRemisionExterna Where IDTransaccion=" & IDTransaccion & " Order By ID"
        CSistema.SqlToDataGrid(lvDetalle, Consulta)

        'Formato
        lvDetalle.Columns("Cantidad").DefaultCellStyle.Format = "N0"
        lvDetalle.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        lvDetalle.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        txtCantidadDetalle.txt.Text = lvDetalle.Rows.Count


    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        If SoloConsulta = True Then
            Exit Sub
        End If

        'Validar
        If lvOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = lvOperacion.SelectedRows(0).Cells("IDTransaccion").Value

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub btnRegistrosDelDia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosDelDia.Click
        Where = " Where (DateDiff(dd, FechaInicio, GetDate())=0) "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub frmConsultaCompra_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaCompra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub lvOperacion_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvOperacion.DoubleClick
        SeleccionarRegistro()
    End Sub

    Private Sub chkUsuario_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUsuario.CheckedChanged
        HabilitarControles(chkUsuario, cbxUsuario)
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub

    Private Sub btnRegistrosDelMes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosDelMes.Click
        Where = " Where (DateDiff(MM, FechaInicio, GetDate())=0) "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub btnRegistrosGenerales_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosGenerales.Click
        ListarOperaciones()
    End Sub

    Private Sub dtbRegistrosPorFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtbRegistrosPorFecha.Click
        Where = " Where (FechaInicio Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "') "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub lvOperacion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvOperacion.KeyDown
        If e.KeyCode = Keys.Enter Then

            ' Your code here
            e.SuppressKeyPress = True

        End If
    End Sub

    Private Sub lvOperacion_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvOperacion.KeyUp

        If e.KeyCode = Keys.Enter Then
            SeleccionarRegistro()
        End If

    End Sub

    Private Sub frmConsultaMovimiento_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub dtpDesde_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpDesde.ValueChanged
        dtpHasta.Value = dtpDesde.Text
    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            Where = " Where IDTipoComprobante = " & cbxComprobante.SelectedValue & " And (Comprobante Like '%" & txtNroComprobante.GetValue & "%') "
            ListarOperaciones(0, Where)
        End If
    End Sub

    Private Sub lvOperacion_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvOperacion.SelectionChanged
        ListarDetalle()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Where = " Where IDTipoComprobante = " & cbxComprobante.SelectedValue & " And (Comprobante Like '%" & txtNroComprobante.GetValue & "%') "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        SeleccionarRegistro()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmConsultaRemisionExterna_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmConsultaMovimientoDistribucion
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Public Property TipoMovimiento As String

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'Form
        'Propiedades
        'IDOperacion = CSistema.ObtenerIDOperacion("frmMovimientoStock", "MOVIMIENTOS", "MOV")
        IDOperacion = CSistema.ObtenerIDOperacion("frmMovimientoDistribucion", "MOVIMIENTO STOCK DISTRIBUCION", "MOVSAL")
        'TextBox
        txtCantidadDetalle.txt.ResetText()
        txtCantidadOperacion.txt.ResetText()
        txtComprobante.ResetText()
        txtOperacion.txt.ResetText()
        txtTotalDetalle.txt.ResetText()
        txtTotalOperacion.txt.ResetText()

        'CheckBox
        chkComprobante.Checked = False
        chkFiltroTipoComprobante.Checked = False
        chkMotivo.Checked = False
        chkTipo.Checked = False
        chkUsuario.Checked = False
        chkSucursal.Checked = False

        'ComboBox
        cbxComprobante.Enabled = False
        cbxMotivo.Enabled = False
        cbxTipo.Enabled = False
        cbxUsuario.Enabled = False
        cbxSucursal.Enabled = False

        'RadioButton
        rdbIncluirAnulados.Checked = True

        'ListView
        lvDetalle.Items.Clear()
        lvOperacion.Items.Clear()

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        CargarInformacion()

        'Foco
        txtOperacion.txt.Focus()
        txtOperacion.txt.SelectAll()

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'Tipo de Operaciones
        CSistema.SqlToComboBox(cbxTipo, "Select ID, Descripcion From TipoOperacion Where IDOperacion=" & IDOperacion)

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxComprobante, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Usuarios
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxUsuario, "Select ID, Usuario From Usuario Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

        'Tipo de Operaciones
        chkTipo.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO OPERACION ACTIVO", "False")
        cbxTipo.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO OPERACION", "")

        'Motivo
        chkMotivo.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "MOTIVO ACTIVO", "False")
        cbxMotivo.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "MOTIVO", "")

        'Comprobante
        chkComprobante.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "COMPROBANTE ACTIVO", "False")
        cbxComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "COMPROBANTE", "")
        txtComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "NROCOMPROBANTE", "")

        'Usuario
        chkUsuario.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "USUARIO ACTIVO", "False")
        cbxUsuario.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "USUARIO", "")


    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCRUSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Tipo de Operaciones
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO OPERACION ACTIVO", chkTipo.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO OPERACION", cbxTipo.Text)

        'Motivo
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "MOTIVO ACTIVO", chkMotivo.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "MOTIVO", cbxMotivo.Text)

        'Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "COMPROBANTE ACTIVO", chkComprobante.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "COMPROBANTE", cbxComprobante.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "NROCOMPROBANTE", txtComprobante.Text)

        'Usuario
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "USUARIO ACTIVO", chkUsuario.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "USUARIO", cbxUsuario.Text)

    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Operaciones
    Sub ListarOperaciones(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "")

        ctrError.Clear()

        Consulta = "Select 'Suc.'=CodigoSucursalOperacion, Num, Fec, Operacion, Motivo, [Cod.], Comprobante, 'Salida'=DepositoSalida, 'Entrada'=DepositoEntrada, Observacion, Autorizacion, Total, Estado, IDTransaccion From VMovimiento"

        Where = Condicion



        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la Sucursal!") = False Then
            Exit Sub
        End If

        'Tipo
        If EstablecerCondicion(cbxTipo, chkTipo, "IDTipoOperacion", "Seleccione correctamente el tipo!") = False Then
            Exit Sub
        End If

        'Motivo
        If EstablecerCondicion(cbxMotivo, chkMotivo, "IDMotivo", "Seleccione correctamente el motivo!") = False Then
            Exit Sub
        End If

        'Comprobante
        'If EstablecerCondicion(cbxComprobante, chkComprobante, "IDTipoComprobante", "Seleccione correctamente el tipo de comprobante!") = False Then
        If EstablecerCondicion(cbxComprobante, chkFiltroTipoComprobante, "IDTipoComprobante", "Seleccione correctamente el tipo de comprobante!") = False Then
            Exit Sub
        Else
            If Where = "" Then
                Where = " Where (Comprobante Like '%" & txtComprobante.Text.Trim & "%') "
            Else
                Where = Where & " And (Comprobante Like '%" & txtComprobante.Text.Trim & "%') "
            End If
        End If

        'Usuario
        If EstablecerCondicion(cbxUsuario, chkUsuario, "IDUsuario", "Seleccione correctamente el usuario!") = False Then
            Exit Sub
        End If

        'Solo Anulados
        If rdbSoloAnulados.Checked = True Then
            If Where = "" Then
                Where = " Where (Anulado='True') "
            Else
                Where = Where & " And (Anulado='True') "
            End If
        End If

        'Solo por numero
        If Numero > 0 Then
            Where = " Where MovimientoDistribucion = 'True' And Numero=" & Numero & " "
        End If

        If Where = "" Then
            Where = "Where " & TipoMovimiento & "= 1 "
            'Else
            'Where = Where & " And " & TipoMovimiento & "= 'True' "

        End If

        lvOperacion.Items.Clear()
        lvDetalle.Items.Clear()

        CSistema.SqlToLv(lvOperacion, Consulta & " " & Where & " Order By Numero")

        'Formato
        lvOperacion.Columns(12).Width = 0

        'Totales
        CSistema.FormatoMoneda(lvOperacion, 11)
        CSistema.TotalesLv(lvOperacion, txtTotalOperacion.txt, 11)
        txtCantidadOperacion.txt.Text = lvOperacion.Items.Count

        'Pintar Anulados
        CSistema.LvPintarAnulados(lvOperacion, 12, "Anulado")

    End Sub

    'Listar Detalle
    Sub ListarDetalle()

        ctrError.Clear()

        'Validar
        If lvOperacion.SelectedItems.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(lvOperacion.SelectedItems(0).SubItems(13).Text) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Limpiar ListView
        lvDetalle.Items.Clear()

        'Obtener el IDTransaccion
        IDTransaccion = CInt(lvOperacion.SelectedItems(0).SubItems(13).Text)
        Dim Consulta As String = "Select Ref, Producto, Cantidad, 'Pre. Uni.'=PrecioUnitario, Total, Anulado From VDetalleMovimiento Where IDTransaccion=" & IDTransaccion & " Order By ID"
        CSistema.SqlToLv(lvDetalle, Consulta)

        lvDetalle.Columns(5).Width = 0

        'Pintar Anulados
        CSistema.FormatoNumero(lvDetalle, 2)
        CSistema.FormatoMoneda(lvDetalle, 3)
        CSistema.FormatoMoneda(lvDetalle, 4)
        CSistema.LvPintarAnulados(lvDetalle, 5, "True")

    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        If lvOperacion.SelectedItems.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(lvOperacion.SelectedItems(0).SubItems(13).Text) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = CInt(lvOperacion.SelectedItems(0).SubItems(13).Text)

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub btnRegistrosDelDia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosDelDia.Click
        Where = " Where (DateDiff(dd, Fecha, GetDate())=0) "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub frmConsultaMovimiento_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaMovimiento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub lvOperacion_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvOperacion.DoubleClick
        SeleccionarRegistro()
    End Sub

    Private Sub lvOperacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvOperacion.SelectedIndexChanged
        ListarDetalle()
    End Sub

    Private Sub cbxTipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipo.SelectedIndexChanged

        cbxMotivo.DataSource = Nothing

        If IsNumeric(cbxTipo.SelectedValue) = False Then
            Exit Sub
        End If

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo, "Select ID, Descripcion From MotivoMovimiento Where IDTipoMovimiento=" & cbxTipo.SelectedValue)

    End Sub

    Private Sub chkTipo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipo.CheckedChanged
        HabilitarControles(chkTipo, cbxTipo)
    End Sub

    Private Sub chkMotivo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMotivo.CheckedChanged
        HabilitarControles(chkMotivo, cbxMotivo)
    End Sub

    Private Sub chkComprobante_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkComprobante.CheckedChanged
        If chkComprobante.Checked = True Then
            'cbxComprobante.Enabled = True
            txtComprobante.Enabled = True
        Else
            'cbxComprobante.Enabled = False
            txtComprobante.Enabled = False
        End If

    End Sub

    Private Sub chkUsuario_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUsuario.CheckedChanged
        HabilitarControles(chkUsuario, cbxUsuario)
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub

    Private Sub btnRegistrosDelMes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosDelMes.Click
        Where = " Where (DateDiff(MM, Fecha, GetDate())=0) "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub btnRegistrosGenerales_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosGenerales.Click
        ListarOperaciones()
    End Sub

    Private Sub dtbRegistrosPorFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtbRegistrosPorFecha.Click
        Where = " Where (Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "') "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarOperaciones(txtOperacion.ObtenerValor)
            txtOperacion.txt.Focus()
            txtOperacion.txt.SelectAll()
        End If
    End Sub

    Private Sub lvOperacion_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvOperacion.KeyUp
        If e.KeyCode = Keys.Enter Then
            SeleccionarRegistro()
        End If

    End Sub

    Private Sub frmConsultaMovimiento_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub chkFiltroTipoComprobante_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkFiltroTipoComprobante.CheckedChanged
        If chkFiltroTipoComprobante.Checked = True Then
            cbxComprobante.Enabled = True
        ElseIf chkFiltroTipoComprobante.Checked = False Then
            cbxComprobante.Enabled = False
        End If
    End Sub

    Private Sub btnSeleccionar_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionar.Click
        SeleccionarRegistro()
    End Sub

    Sub frmConsultaMovimientoDistribucion_Activate()
        Me.Refresh()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmRemisionExterna
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtCantidadComprobante = New ERP.ocxTXTNumeric()
        Me.lblCantidadComprobante = New System.Windows.Forms.Label()
        Me.dgvLista = New System.Windows.Forms.DataGridView()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnMovimiento = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.txtDireccionDestino = New ERP.ocxTXTString()
        Me.lblDireccionDestino = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.lblDetalle = New System.Windows.Forms.Label()
        Me.cbxMotivo = New ERP.ocxCBX()
        Me.lblMotivo = New System.Windows.Forms.Label()
        Me.cbxSucursalOperacion = New ERP.ocxCBX()
        Me.lblSucursalOperacion = New System.Windows.Forms.Label()
        Me.cbxDepositoOperacion = New ERP.ocxCBX()
        Me.lblDepositoOperacion = New System.Windows.Forms.Label()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.txtNroComprobante = New ERP.ocxTXTString()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel3.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.gbxCabecera.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.txtCantidadComprobante)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblCantidadComprobante)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 439)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(679, 24)
        Me.FlowLayoutPanel3.TabIndex = 7
        '
        'txtCantidadComprobante
        '
        Me.txtCantidadComprobante.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobante.Decimales = True
        Me.txtCantidadComprobante.Indicaciones = Nothing
        Me.txtCantidadComprobante.Location = New System.Drawing.Point(620, 3)
        Me.txtCantidadComprobante.Name = "txtCantidadComprobante"
        Me.txtCantidadComprobante.Size = New System.Drawing.Size(56, 21)
        Me.txtCantidadComprobante.SoloLectura = True
        Me.txtCantidadComprobante.TabIndex = 5
        Me.txtCantidadComprobante.TabStop = False
        Me.txtCantidadComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobante.Texto = "0"
        '
        'lblCantidadComprobante
        '
        Me.lblCantidadComprobante.Location = New System.Drawing.Point(479, 0)
        Me.lblCantidadComprobante.Name = "lblCantidadComprobante"
        Me.lblCantidadComprobante.Size = New System.Drawing.Size(135, 26)
        Me.lblCantidadComprobante.TabIndex = 4
        Me.lblCantidadComprobante.Text = "Cantidad:"
        Me.lblCantidadComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLista.Location = New System.Drawing.Point(3, 236)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.Size = New System.Drawing.Size(679, 197)
        Me.dgvLista.TabIndex = 6
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.btnMovimiento)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 197)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(679, 33)
        Me.FlowLayoutPanel2.TabIndex = 2
        '
        'btnMovimiento
        '
        Me.btnMovimiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMovimiento.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnMovimiento.Location = New System.Drawing.Point(3, 3)
        Me.btnMovimiento.Name = "btnMovimiento"
        Me.btnMovimiento.Size = New System.Drawing.Size(123, 23)
        Me.btnMovimiento.TabIndex = 0
        Me.btnMovimiento.Text = "Movimientos"
        Me.btnMovimiento.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.flpRegistradoPor)
        Me.FlowLayoutPanel1.Controls.Add(Me.flpAnuladoPor)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 469)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(679, 28)
        Me.FlowLayoutPanel1.TabIndex = 3
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.ForeColor = System.Drawing.SystemColors.ControlText
        Me.flpRegistradoPor.Location = New System.Drawing.Point(3, 3)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(313, 20)
        Me.flpRegistradoPor.TabIndex = 1
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 3)
        Me.lblRegistradoPor.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 3)
        Me.lblUsuarioRegistro.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 3)
        Me.lblFechaRegistro.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(322, 3)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(295, 20)
        Me.flpAnuladoPor.TabIndex = 20
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 13
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 15
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 17
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'gbxCabecera
        '
        Me.gbxCabecera.BackColor = System.Drawing.Color.Transparent
        Me.gbxCabecera.Controls.Add(Me.txtCliente)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtDireccionDestino)
        Me.gbxCabecera.Controls.Add(Me.lblDireccionDestino)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblDetalle)
        Me.gbxCabecera.Controls.Add(Me.cbxMotivo)
        Me.gbxCabecera.Controls.Add(Me.lblMotivo)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursalOperacion)
        Me.gbxCabecera.Controls.Add(Me.lblSucursalOperacion)
        Me.gbxCabecera.Controls.Add(Me.cbxDepositoOperacion)
        Me.gbxCabecera.Controls.Add(Me.lblDepositoOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.lblCliente)
        Me.gbxCabecera.Controls.Add(Me.txtNroComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxCabecera.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbxCabecera.Location = New System.Drawing.Point(3, 3)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(679, 188)
        Me.gbxCabecera.TabIndex = 1
        Me.gbxCabecera.TabStop = False
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 117
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.ControlCorto = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(97, 40)
        Me.txtCliente.MostrarSucursal = True
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(488, 24)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 1
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Introduzca el código y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(97, 12)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(83, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 31
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(38, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 30
        Me.lblOperacion.Text = "Operación:"
        '
        'txtDireccionDestino
        '
        Me.txtDireccionDestino.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccionDestino.Color = System.Drawing.Color.Beige
        Me.txtDireccionDestino.Indicaciones = Nothing
        Me.txtDireccionDestino.Location = New System.Drawing.Point(97, 70)
        Me.txtDireccionDestino.Multilinea = False
        Me.txtDireccionDestino.Name = "txtDireccionDestino"
        Me.txtDireccionDestino.Size = New System.Drawing.Size(488, 21)
        Me.txtDireccionDestino.SoloLectura = False
        Me.txtDireccionDestino.TabIndex = 29
        Me.txtDireccionDestino.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccionDestino.Texto = ""
        '
        'lblDireccionDestino
        '
        Me.lblDireccionDestino.AutoSize = True
        Me.lblDireccionDestino.Location = New System.Drawing.Point(38, 70)
        Me.lblDireccionDestino.Name = "lblDireccionDestino"
        Me.lblDireccionDestino.Size = New System.Drawing.Size(55, 13)
        Me.lblDireccionDestino.TabIndex = 28
        Me.lblDireccionDestino.Text = "Dirección:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(97, 160)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(488, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 5
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'lblDetalle
        '
        Me.lblDetalle.AutoSize = True
        Me.lblDetalle.Location = New System.Drawing.Point(23, 162)
        Me.lblDetalle.Name = "lblDetalle"
        Me.lblDetalle.Size = New System.Drawing.Size(70, 13)
        Me.lblDetalle.TabIndex = 27
        Me.lblDetalle.Text = "Observacion:"
        '
        'cbxMotivo
        '
        Me.cbxMotivo.CampoWhere = Nothing
        Me.cbxMotivo.CargarUnaSolaVez = False
        Me.cbxMotivo.DataDisplayMember = "Descripcion"
        Me.cbxMotivo.DataFilter = Nothing
        Me.cbxMotivo.DataOrderBy = Nothing
        Me.cbxMotivo.DataSource = "VMotivoRemision"
        Me.cbxMotivo.DataValueMember = "ID"
        Me.cbxMotivo.dtSeleccionado = Nothing
        Me.cbxMotivo.FormABM = Nothing
        Me.cbxMotivo.Indicaciones = Nothing
        Me.cbxMotivo.Location = New System.Drawing.Point(98, 130)
        Me.cbxMotivo.Name = "cbxMotivo"
        Me.cbxMotivo.SeleccionMultiple = False
        Me.cbxMotivo.SeleccionObligatoria = True
        Me.cbxMotivo.Size = New System.Drawing.Size(487, 21)
        Me.cbxMotivo.SoloLectura = False
        Me.cbxMotivo.TabIndex = 4
        Me.cbxMotivo.Texto = ""
        '
        'lblMotivo
        '
        Me.lblMotivo.AutoSize = True
        Me.lblMotivo.Location = New System.Drawing.Point(51, 134)
        Me.lblMotivo.Name = "lblMotivo"
        Me.lblMotivo.Size = New System.Drawing.Size(42, 13)
        Me.lblMotivo.TabIndex = 21
        Me.lblMotivo.Text = "Motivo:"
        '
        'cbxSucursalOperacion
        '
        Me.cbxSucursalOperacion.CampoWhere = Nothing
        Me.cbxSucursalOperacion.CargarUnaSolaVez = False
        Me.cbxSucursalOperacion.DataDisplayMember = "Codigo"
        Me.cbxSucursalOperacion.DataFilter = Nothing
        Me.cbxSucursalOperacion.DataOrderBy = "Codigo"
        Me.cbxSucursalOperacion.DataSource = "VSucursal"
        Me.cbxSucursalOperacion.DataValueMember = "ID"
        Me.cbxSucursalOperacion.dtSeleccionado = Nothing
        Me.cbxSucursalOperacion.FormABM = Nothing
        Me.cbxSucursalOperacion.Indicaciones = Nothing
        Me.cbxSucursalOperacion.Location = New System.Drawing.Point(98, 98)
        Me.cbxSucursalOperacion.Name = "cbxSucursalOperacion"
        Me.cbxSucursalOperacion.SeleccionMultiple = False
        Me.cbxSucursalOperacion.SeleccionObligatoria = True
        Me.cbxSucursalOperacion.Size = New System.Drawing.Size(92, 21)
        Me.cbxSucursalOperacion.SoloLectura = False
        Me.cbxSucursalOperacion.TabIndex = 2
        Me.cbxSucursalOperacion.Texto = ""
        '
        'lblSucursalOperacion
        '
        Me.lblSucursalOperacion.AutoSize = True
        Me.lblSucursalOperacion.Location = New System.Drawing.Point(42, 102)
        Me.lblSucursalOperacion.Name = "lblSucursalOperacion"
        Me.lblSucursalOperacion.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursalOperacion.TabIndex = 9
        Me.lblSucursalOperacion.Text = "Sucursal:"
        '
        'cbxDepositoOperacion
        '
        Me.cbxDepositoOperacion.CampoWhere = Nothing
        Me.cbxDepositoOperacion.CargarUnaSolaVez = False
        Me.cbxDepositoOperacion.DataDisplayMember = "Deposito"
        Me.cbxDepositoOperacion.DataFilter = Nothing
        Me.cbxDepositoOperacion.DataOrderBy = Nothing
        Me.cbxDepositoOperacion.DataSource = "VDeposito"
        Me.cbxDepositoOperacion.DataValueMember = "ID"
        Me.cbxDepositoOperacion.dtSeleccionado = Nothing
        Me.cbxDepositoOperacion.FormABM = Nothing
        Me.cbxDepositoOperacion.Indicaciones = Nothing
        Me.cbxDepositoOperacion.Location = New System.Drawing.Point(261, 99)
        Me.cbxDepositoOperacion.Name = "cbxDepositoOperacion"
        Me.cbxDepositoOperacion.SeleccionMultiple = False
        Me.cbxDepositoOperacion.SeleccionObligatoria = False
        Me.cbxDepositoOperacion.Size = New System.Drawing.Size(324, 21)
        Me.cbxDepositoOperacion.SoloLectura = False
        Me.cbxDepositoOperacion.TabIndex = 3
        Me.cbxDepositoOperacion.Texto = ""
        '
        'lblDepositoOperacion
        '
        Me.lblDepositoOperacion.AutoSize = True
        Me.lblDepositoOperacion.Location = New System.Drawing.Point(198, 102)
        Me.lblDepositoOperacion.Name = "lblDepositoOperacion"
        Me.lblDepositoOperacion.Size = New System.Drawing.Size(52, 13)
        Me.lblDepositoOperacion.TabIndex = 11
        Me.lblDepositoOperacion.Text = "Deposito:"
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(CType(0, Long))
        Me.txtFecha.Location = New System.Drawing.Point(230, 13)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(77, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 3
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(186, 17)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Text = "Fecha:"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(51, 46)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(42, 13)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Text = "Cliente:"
        '
        'txtNroComprobante
        '
        Me.txtNroComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroComprobante.Color = System.Drawing.Color.Empty
        Me.txtNroComprobante.Indicaciones = Nothing
        Me.txtNroComprobante.Location = New System.Drawing.Point(452, 13)
        Me.txtNroComprobante.Multilinea = False
        Me.txtNroComprobante.Name = "txtNroComprobante"
        Me.txtNroComprobante.Size = New System.Drawing.Size(132, 21)
        Me.txtNroComprobante.SoloLectura = False
        Me.txtNroComprobante.TabIndex = 0
        Me.txtNroComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNroComprobante.Texto = ""
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(317, 18)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 7
        Me.lblComprobante.Text = "Comprobante:"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Codigo"
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = ""
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(394, 13)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(55, 21)
        Me.cbxTipoComprobante.SoloLectura = True
        Me.cbxTipoComprobante.TabIndex = 8
        Me.cbxTipoComprobante.TabStop = False
        Me.cbxTipoComprobante.Texto = ""
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnAnular)
        Me.Panel2.Controls.Add(Me.btnEliminar)
        Me.Panel2.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Panel2.Controls.Add(Me.btnImprimir)
        Me.Panel2.Controls.Add(Me.btnSalir)
        Me.Panel2.Controls.Add(Me.btnCancelar)
        Me.Panel2.Controls.Add(Me.btnGuardar)
        Me.Panel2.Controls.Add(Me.btnNuevo)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 503)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(679, 29)
        Me.Panel2.TabIndex = 4
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(132, 3)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 10
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(213, 3)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 9
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        Me.btnEliminar.Visible = False
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBusquedaAvanzada.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(0, 3)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(126, 23)
        Me.btnBusquedaAvanzada.TabIndex = 0
        Me.btnBusquedaAvanzada.Text = "&Busqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnImprimir.Location = New System.Drawing.Point(132, 3)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 1
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        Me.btnImprimir.Visible = False
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnSalir.Location = New System.Drawing.Point(592, 3)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 5
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnCancelar.Location = New System.Drawing.Point(475, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 4
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnGuardar.Location = New System.Drawing.Point(394, 3)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 0
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnNuevo.Location = New System.Drawing.Point(313, 3)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 2
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxCabecera, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvLista, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 3)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 7
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 194.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(685, 559)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 537)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(685, 22)
        Me.StatusStrip1.TabIndex = 2
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'frmRemisionExterna
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(685, 559)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmRemisionExterna"
        Me.Text = "frmRemisionExterna"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents btnAnular As Button
    Friend WithEvents btnEliminar As Button
    Friend WithEvents btnImprimir As Button
    Friend WithEvents btnSalir As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnNuevo As Button
    Friend WithEvents gbxCabecera As GroupBox
    Friend WithEvents txtCliente As ocxTXTCliente
    Friend WithEvents cbxMotivo As ocxCBX
    Friend WithEvents lblMotivo As Label
    Friend WithEvents cbxSucursalOperacion As ocxCBX
    Friend WithEvents lblSucursalOperacion As Label
    Friend WithEvents cbxDepositoOperacion As ocxCBX
    Friend WithEvents lblDepositoOperacion As Label
    Friend WithEvents txtFecha As ocxTXTDate
    Friend WithEvents lblFecha As Label
    Friend WithEvents lblCliente As Label
    Friend WithEvents txtNroComprobante As ocxTXTString
    Friend WithEvents lblComprobante As Label
    Friend WithEvents cbxTipoComprobante As ocxCBX
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents flpRegistradoPor As FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As Label
    Friend WithEvents lblUsuarioRegistro As Label
    Friend WithEvents lblFechaRegistro As Label
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents btnMovimiento As Button
    Friend WithEvents dgvLista As DataGridView
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents txtCantidadComprobante As ocxTXTNumeric
    Friend WithEvents lblCantidadComprobante As Label
    Friend WithEvents txtObservacion As ocxTXTString
    Friend WithEvents lblDetalle As Label
    Friend WithEvents txtDireccionDestino As ocxTXTString
    Friend WithEvents lblDireccionDestino As Label
    Friend WithEvents txtID As ocxTXTNumeric
    Friend WithEvents lblOperacion As Label
    Friend WithEvents flpAnuladoPor As FlowLayoutPanel
    Friend WithEvents lblAnulado As Label
    Friend WithEvents lblUsuarioAnulado As Label
    Friend WithEvents lblFechaAnulado As Label
    Friend WithEvents btnBusquedaAvanzada As Button
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
End Class

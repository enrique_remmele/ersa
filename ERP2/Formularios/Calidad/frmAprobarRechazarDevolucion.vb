﻿Public Class frmAprobarRechazarDevolucion

    Dim CSistema As New CSistema
    Dim CData As New CData

    Public Property IDTransaccionPedido As Integer
    Public Property Aprobar As Boolean
    Public Property dtPedidos As DataTable
    Public Property IDSucursal As Integer

    Dim IDTransaccion As Integer
    Dim IDOperacion As Integer


    Sub Inicializar()

        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "APROBARRECHAZARDEVOLUCION", "ARD")
        IDTransaccion = 0

        CargarInformacion()
    End Sub

    Sub CargarInformacion()

        cbxDestino.cbx.Items.Add("DESTRUCCION")
        cbxDestino.cbx.Items.Add("REPROCESO")
        cbxDestino.cbx.Items.Add("VENTAS")

        cbxDestino.cbx.SelectedIndex = 0

        cbxSucursal.cbx.SelectedValue = IDSucursal

    End Sub

    Sub CargarOperacion()

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) FROM vAprobarRechazarDevolucion Where IDSucursal = " & cbxSucursal.GetValue & "),1)"), Integer)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Poner el foco en la fecha
        txtFecha.txt.Focus()
        txtFecha.Hoy()
        txtFecha.txt.ReadOnly = True

        Listar()
    End Sub

    Sub Listar()

        CSistema.dtToGrid(dgw, dtPedidos)

        For i = 1 To dgw.RowCount - 1
            dgw.Rows(i).Visible = False
        Next

        CSistema.DataGridColumnasVisibles(dgw, {"Suc", "Numero", "Fecha", "Cliente", "Total"})
        dgw.Columns("Cliente").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub Guardar()

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID)
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Num), " & ID & ") From vAprobarRechazarDevolucion Where Numero > " & ID & " and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID)
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), " & ID & ") From vAprobarRechazarDevolucion Where Numero < " & ID & " and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), " & ID & ") From vAprobarRechazarDevolucion Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), " & ID & ") From vAprobarRechazarDevolucion Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub frmAprobarRechazarDevolucion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub txtID_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    'FA 28/05/2021
    Sub frmAprobarRechazarDevolucion_Activate()
        Me.Refresh()
    End Sub

End Class
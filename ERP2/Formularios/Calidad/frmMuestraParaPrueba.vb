﻿Public Class frmMuestraParaPrueba
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Dim CData As New CData
    'Dim CReporte As New CReporte

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dtOperaciones As New DataTable
    Dim dtDepositos As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim dtComprobantes As New DataTable
    Dim Precargado As Boolean = False

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        txtProducto.Conectar()

        'Otros
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Propiedades
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "MUESTRAPARAPRUEBA", "MPP")
        IDTransaccion = 0
        vNuevo = True

        'Funciones
        CargarInformacion()


        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, txtCantidad)

        'CARGAR ESTRUCTURA DEL DETALLE
        dtDetalle = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleMovimiento").Clone

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CARGAR CONTROLES
        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo From VSucursal Order By 2")

        'Tipo de Comprobante
        dtComprobantes = CSistema.ExecuteToDataTable("Select ID, Codigo, Descripcion From TipoComprobante Where IDOperacion=" & IDOperacion)
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Depositos
        dtDepositos = CSistema.ExecuteToDataTable(" Select * From VDeposito Where Activo='True' Order By 2").Copy

        'CARGAR LA ULTIMA CONFIGURACION
        'Tipo Operacion
        cbxSucursal.SelectedValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", "0"))

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")


        txtFecha.Enabled = False
        txtFecha.SetValue(VGFechaHoraSistema)

        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From MuestraParaPrueba),1)"), Integer)

    End Sub

    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.GetValue)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where Estado = 1 and IDOperacion=" & IDOperacion)
        'Limpiar detalle
        dtDetalle.Rows.Clear()
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True

        cbxTipoComprobante.cbx.Text = ""
        txtComprobante.txt.Clear()

        txtObservacion.txt.Clear()
        txtTotal.txt.ResetText()

        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) FROM VMuestraParaPrueba Where IDSucursal = " & cbxSucursal.GetValue & "),1)"), Integer)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True
        Precargado = False

        'Poner el foco en la fecha
        txtFecha.txt.Focus()
        txtFecha.Hoy()

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) FROM VMuestraParaPrueba Where IDSucursal = " & cbxSucursal.GetValue & "),1)"), Integer)

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        CargarOperacion()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Detalle
        If dgw.Rows.Count = 0 Then
            Dim mensaje As String = "El registro no tiene ningun detalle!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If


        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IDTransaccion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Autorizacion", "", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(txtTotal.ObtenerValor), ParameterDirection.Input)

        'Totales
        CSistema.SetSQLParameter(param, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalImpuesto")), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalDiscriminado")), ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input) 'JGR 20140816
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpMuestraParaPrueba", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro

            Exit Sub

        End If

        If IDTransaccion > 0 Then

            'Insertamos el Detalle
            If InsertarDetalle(IDTransaccion, ERP.CSistema.NUMOperacionesRegistro.INS) = False Then

                'Eliminar Registro

                Exit Sub

            End If

            'Cargamos el DetalleImpuesto
            CDetalleImpuesto.Guardar(IDTransaccion)

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False

    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer, ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        InsertarDetalle = True

        For Each oRow As DataRow In dtDetalle.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDProducto", oRow("IDProducto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", oRow("ID").ToString, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Observacion", oRow("Observacion").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cantidad", CSistema.FormatoMonedaBaseDatos(oRow("Cantidad").ToString, True), ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@IDImpuesto", oRow("IDImpuesto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@PrecioUnitario", oRow("PrecioUnitario").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalImpuesto", oRow("TotalImpuesto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalDiscriminado", oRow("TotalDiscriminado").ToString, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Caja", oRow("Caja").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@CantidadCaja", oRow("CantidadCaja").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpDetalleMuestra", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

                'Eliminar el Registro

                Return False

            End If

        Next

    End Function

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, New Button, New Button, btnAsiento, vControles)

    End Sub

    Sub CargarProducto()

        'Validar
        'Seleccion de Producto
        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cantidad
        If IsNumeric(txtCantidad.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtCantidad.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Verificar que exista el producto en el deposito de salida
        Dim ExistenciaActual As Decimal = 0
        Dim CantidadProducto As Decimal = 0
        Dim CantidadCaja As Integer = 0
        Dim EsCaja As Boolean = False

        'Si existe, sumar la cantidad
        If dtDetalle.Select(" IDProducto=" & txtProducto.Registro("ID")).Count > 0 Then
            Dim Rows() As DataRow = dtDetalle.Select(" IDProducto=" & txtProducto.Registro("ID"))
            Rows(0)("Cantidad") = CantidadProducto
            Rows(0)("Total") = CantidadProducto * CDec(txtCosto.ObtenerValor)
            Rows(0)("TotalImpuesto") = 0
            Rows(0)("TotalDiscriminado") = 0
            CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(Rows(0)("Total").ToString), False, True, Rows(0)("TotalDiscriminado"), Rows(0)("TotalImpuesto"))

            Rows(0)("Caja") = EsCaja.ToString
            Rows(0)("CantidadCaja") = CantidadCaja

        Else

            ' Insertar
            'Cargamos el registro en el detalle
            Dim dRow As DataRow = dtDetalle.NewRow()

            'Obtener Valores
            dRow("IDProducto") = txtProducto.Registro("ID").ToString
            dRow("Ref") = txtProducto.Registro("Ref").ToString
            dRow("ID") = dtDetalle.Rows.Count
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString

            dRow("Cantidad") = CantidadProducto
            dRow("Observacion") = ""

            'Impuestos
            dRow("IDImpuesto") = txtProducto.Registro("IDImpuesto").ToString
            dRow("Impuesto") = txtProducto.Registro("Impuesto").ToString

            'Totales
            dRow("PrecioUnitario") = txtCosto.ObtenerValor
            dRow("Total") = CantidadProducto * CDec(txtCosto.ObtenerValor)
            dRow("TotalImpuesto") = 0
            dRow("TotalDiscriminado") = 0
            CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("Total").ToString), False, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))


            dRow("Caja") = EsCaja.ToString
            dRow("CantidadCaja") = CantidadCaja
            dRow("CodigoBarra") = txtProducto.Registro("CodigoBarra").ToString

            'Agregamos al detalle
            dtDetalle.Rows.Add(dRow)

        End If

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)

        'Inicializamos los valores
        txtCantidad.txt.Text = "0"
        txtCosto.txt.Text = "0"
        txtProducto.txt.Clear()

        'Retorno de Foco
        txtProducto.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Public Sub CargarMuestra(ByVal dtProductosCargar As DataTable, ByVal dtCabeceraCargar As DataTable, ByVal Origen As String)

        If (dtProductosCargar Is Nothing) Or (dtProductosCargar.Rows.Count = 0) Then
            Exit Sub
        End If

        If (dtCabeceraCargar Is Nothing) Or (dtCabeceraCargar.Rows.Count = 0) Then
            Exit Sub
        End If

        cbxSucursal.cbx.SelectedValue = dtCabeceraCargar.Rows(0)("IDSucursal")
        Nuevo()

        Select Case Origen
            Case "Panaderia"
                Dim dtOrigen As DataTable
                dtOrigen = CSistema.ExecuteToDataTable("Select * from ConfiguracionMuestraPanaderia")
                cbxTipoComprobante.cbx.SelectedValue = dtOrigen.Rows(0)("IDTipoComprobante")
        End Select
        txtComprobante.Texto = dtCabeceraCargar.Rows(0)("Numero")

        For Each Row As DataRow In dtProductosCargar.Rows

            'Verificar que exista el producto en el deposito de salida
            Dim ExistenciaActual As Decimal = 0
            Dim CantidadProducto As Decimal = Row("Cantidad")
            Dim CostoUnitario As Decimal = CSistema.ExecuteScalar("Select dbo.FCostoProductoFechaKardex(" & Row("IDProducto") & ",'" & CSistema.FormatoFechaBaseDatos(dtCabeceraCargar.Rows(0)("Fecha"), True, False) & "')")
            Dim RowProductoRegistro As DataRow = CData.GetRow(" ID=" & Row("IDProducto"), "VProducto")

            'Si existe, sumar la cantidad
            If dtDetalle.Select(" IDProducto=" & Row("IDProducto")).Count > 0 Then
                Dim Rows() As DataRow = dtDetalle.Select(" IDProducto=" & Row("IDProducto"))
                Rows(0)("Cantidad") = CantidadProducto
                Rows(0)("Total") = CantidadProducto * CostoUnitario
                Rows(0)("TotalImpuesto") = 0
                Rows(0)("TotalDiscriminado") = 0
                CSistema.CalcularIVA(RowProductoRegistro("IDImpuesto"), CDec(Rows(0)("Total").ToString), False, True, Rows(0)("TotalDiscriminado"), Rows(0)("TotalImpuesto"))

                Rows(0)("Caja") = False
                Rows(0)("CantidadCaja") = 1

            Else
                'Cargamos el registro en el detalle
                Dim dRow As DataRow = dtDetalle.NewRow()

                'Obtener Valores
                dRow("IDProducto") = Row("IDProducto")
                dRow("Ref") = RowProductoRegistro("Referencia")
                dRow("ID") = dtDetalle.Rows.Count
                dRow("Descripcion") = RowProductoRegistro("Descripcion")

                dRow("Cantidad") = CantidadProducto
                dRow("Observacion") = Row("MotivoDevolucion")

                'Impuestos
                dRow("IDImpuesto") = RowProductoRegistro("IDImpuesto")
                dRow("Impuesto") = RowProductoRegistro("Impuesto")

                'Totales
                dRow("PrecioUnitario") = CostoUnitario
                dRow("Total") = CantidadProducto * CostoUnitario
                dRow("TotalImpuesto") = 0
                dRow("TotalDiscriminado") = 0
                CSistema.CalcularIVA(RowProductoRegistro("IDImpuesto"), CDec(dRow("Total").ToString), False, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))

                dRow("Caja") = False
                dRow("CantidadCaja") = 1
                dRow("CodigoBarra") = RowProductoRegistro("CodigoBarra")

                'Agregamos al detalle
                dtDetalle.Rows.Add(dRow)

            End If
        Next

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)

        'Inicializamos los valores
        txtCantidad.Enabled = False
        txtCosto.Enabled = False
        txtProducto.Enabled = False

        Precargado = True

    End Sub

    Sub EliminarProducto()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If dgw.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgw, mensaje)
            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If Precargado Then
            MessageBox.Show("No es posible modificar muestra precargada.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        dtDetalle.Rows(dgw.SelectedRows(0).Index).Delete()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtDetalle.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        ' dgw.Rows.Clear()

        'Variables
        Dim Total As Decimal = 0

        'Cargamos registro por registro
        CSistema.dtToGrid(dgw, dtDetalle)

        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).Visible = False
        Next

        dgw.Columns("Ref").DisplayIndex = 1
        dgw.Columns("Descripcion").DisplayIndex = 2
        dgw.Columns("Cantidad").DisplayIndex = 3
        dgw.Columns("PrecioUnitario").DisplayIndex = 4
        dgw.Columns("Total").DisplayIndex = 5

        dgw.Columns("Ref").Visible = True
        dgw.Columns("Descripcion").Visible = True
        dgw.Columns("Cantidad").Visible = True
        dgw.Columns("PrecioUnitario").Visible = True
        dgw.Columns("Total").Visible = True

        dgw.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns("Cantidad").DefaultCellStyle.Format = "N2"
        dgw.Columns("PrecioUnitario").DefaultCellStyle.Format = "N0"
        dgw.Columns("Total").DefaultCellStyle.Format = "N0"

        dgw.Columns("Ref").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgw.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PrecioUnitario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgw.Columns("Cantidad").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PrecioUnitario").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Total").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

        dgw.Columns("PrecioUnitario").HeaderText = "Costo"

        Total = CSistema.dtSumColumn(dtDetalle, "Total")


        'Calculamos el Total
        txtTotal.txt.Text = Total


    End Sub

    Sub Anular()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.ANULAR.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Anular
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpMuestraParaPrueba", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

    End Sub

    Sub Imprimir()

        Dim CMovimiento As New Reporte.CReporteStock
        CMovimiento.MovimientoStock(IDTransaccion)

    End Sub

    Sub Buscar()

        'EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        ''Otros
        'lblAnulado.Visible = False

        'Dim frm As New frmConsultaMovimiento
        'frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        'frm.WindowState = FormWindowState.Normal
        'frm.StartPosition = FormStartPosition.CenterScreen
        'frm.TipoMovimiento = "MovimientoStock"
        'FGMostrarFormulario(Me, frm, "Consulta de Registros", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False, True)

        'If frm.IDTransaccion > 0 Then
        '    CargarOperacion(frm.IDTransaccion)
        'End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VMuestraParaPrueba Where IDSucursal=" & cbxSucursal.GetValue & " And Numero=" & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select Numero, Fecha, Operacion, Motivo, [Cod.],  NroComprobante, DepositoEntrada, DepositoSalida, Observacion, Autorizacion, Total, Anulado, UsuarioIdentificacionAnulacion, FechaAnulacion, Usuario, UsuarioIdentificador, FechaTransaccion From VMuestraParaPrueba Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select IDProducto, ID, Ref, 'Descripcion'=Producto, IDDepositoEntrada, IDDepositoSalida, Cantidad, Observacion, PrecioUnitario, IDImpuesto, Impuesto, Total, TotalImpuesto, TotalDiscriminado, Caja, CantidadCaja, CodigoBarra From VDetalleMuestra Where IDTransaccion=" & IDTransaccion & " Order By ID").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)


        txtID.txt.Text = oRow("Numero").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        cbxTipoComprobante.Texto = oRow("Cod.").ToString
        txtComprobante.txt.Text = oRow("NroComprobante").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtTotal.txt.Text = oRow("Total").ToString


        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("UsuarioIdentificador").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos el detalle
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)


    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "MUESTRA " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text

            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select Top(1) IDTransaccion From VMuestraParaPrueba Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & " Order By IDTransaccion Desc), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)

        Else

            MessageBox.Show("Debe Guardar la Operacion para visualizar el asiento", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        End If


    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID)
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Num), " & ID & ") From VMuestraParaPrueba Where Num > " & ID & " and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID)
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Num), " & ID & ") From VMuestraParaPrueba Where Num < " & ID & " and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Num), " & ID & ") From VMuestraParaPrueba Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Num), " & ID & ") From VMuestraParaPrueba Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If
    End Sub

    Private Sub OcxTXTProducto1_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado

        If txtProducto.Seleccionado = True Then

            'Obtenemos el costo
            Dim Costo As Decimal = 0
            If cbxTipoComprobante.cbx.SelectedValue = CInt(vgConfiguraciones("IDTipoComprobanteProduccion")) Then
                Costo = CSistema.ExecuteScalar("select dbo.FCostoProduccion(" & txtProducto.Registro("ID").ToString & ", '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' )")
            Else
                Costo = txtProducto.Registro("Costo").ToString()
            End If

            If Costo = 0 Then
                Costo = txtProducto.Registro("Costo").ToString()
            End If

            txtCosto.txt.Text = Costo

            If CBool(vgConfiguraciones("MovimientoBloquearCampoCosto").ToString) = True Then

                txtCosto.SoloLectura = True

                If Costo = 0 Then
                    txtCosto.SoloLectura = False
                End If

            Else
                txtCosto.SoloLectura = False
            End If

        End If

    End Sub

    Private Sub frmMovimientoStock_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmMovimientoStock_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = vgKeyConsultar Then

            Buscar()

            Exit Sub

        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub frmMovimientoStock_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Buscar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Imprimir()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtCantidad_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            If txtCosto.SoloLectura = True Then
                CargarProducto()
            End If
        End If
    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub cbxMotivo_Editar(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Dim frm As New frmMotivo
        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

    End Sub

    Private Sub txtCosto_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCosto.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub cbxTipoOperacion_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs)
        If vNuevo = False Then
            Exit Sub
        End If

    End Sub

    Private Sub txtFecha_Leave(sender As Object, e As EventArgs) Handles txtFecha.Leave
        If vNuevo Then
            Dim DiferenciaDias As Long
            Dim date1 As Date = Now
            If CBool(vgConfiguraciones("MovimientoBloquearFecha").ToString) = False Then
                Exit Sub
            End If
            Try

                DiferenciaDias = DateDiff(DateInterval.Day, Date.Parse(Now.ToShortDateString), Date.Parse(txtFecha.txt.Text))
                If DiferenciaDias > 0 Then
                    If CInt(vgConfiguraciones("MovimientoCantidadDiasHabilitados")) < DiferenciaDias Then
                        txtFecha.SetValue(VGFechaHoraSistema)
                    End If
                Else
                    If CInt(vgConfiguraciones("MovimientoCantidadDiasHabilitados")) < -DiferenciaDias Then
                        txtFecha.SetValue(VGFechaHoraSistema)
                    End If
                End If
            Catch ex As Exception
                txtFecha.SetValue(VGFechaHoraSistema)
            End Try



        End If

    End Sub

    'FA 28/05/2021
    Sub frmMuestraParaPrueba_Activate()
        Me.Refresh()
    End Sub

End Class
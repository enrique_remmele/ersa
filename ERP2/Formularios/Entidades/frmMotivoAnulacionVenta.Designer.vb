﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMotivoAnulacionVenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.lblID = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.chkMantenerPedidoVigente = New ERP.ocxCHK()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.chkPedido = New ERP.ocxCHK()
        Me.chkVenta = New ERP.ocxCHK()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(146, 94)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 30
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(90, 94)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 29
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(43, 94)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 28
        Me.lblEstado.Text = "Estado:"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(20, 39)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 26
        Me.lblDescripcion.Text = "Descripción:"
        '
        'txtID
        '
        Me.txtID.BackColor = System.Drawing.Color.White
        Me.txtID.Location = New System.Drawing.Point(90, 14)
        Me.txtID.Name = "txtID"
        Me.txtID.ReadOnly = True
        Me.txtID.Size = New System.Drawing.Size(61, 20)
        Me.txtID.TabIndex = 32
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(65, 14)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 31
        Me.lblID.Text = "ID:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(360, 118)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 36
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(441, 118)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 37
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(279, 118)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 35
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(198, 118)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 34
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(117, 118)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 33
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lvLista
        '
        Me.lvLista.HideSelection = False
        Me.lvLista.Location = New System.Drawing.Point(22, 147)
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(494, 195)
        Me.lvLista.TabIndex = 38
        Me.lvLista.UseCompatibleStateImageBehavior = False
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 396)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(531, 22)
        Me.StatusStrip1.TabIndex = 39
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(441, 364)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 40
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'chkMantenerPedidoVigente
        '
        Me.chkMantenerPedidoVigente.BackColor = System.Drawing.Color.Transparent
        Me.chkMantenerPedidoVigente.Color = System.Drawing.Color.Empty
        Me.chkMantenerPedidoVigente.Location = New System.Drawing.Point(182, 14)
        Me.chkMantenerPedidoVigente.Name = "chkMantenerPedidoVigente"
        Me.chkMantenerPedidoVigente.Size = New System.Drawing.Size(263, 20)
        Me.chkMantenerPedidoVigente.SoloLectura = False
        Me.chkMantenerPedidoVigente.TabIndex = 41
        Me.chkMantenerPedidoVigente.Texto = "Mantener Pedido Vigente"
        Me.chkMantenerPedidoVigente.Valor = False
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(90, 39)
        Me.txtDescripcion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(399, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 27
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'chkPedido
        '
        Me.chkPedido.BackColor = System.Drawing.Color.Transparent
        Me.chkPedido.Color = System.Drawing.Color.Empty
        Me.chkPedido.Location = New System.Drawing.Point(90, 67)
        Me.chkPedido.Name = "chkPedido"
        Me.chkPedido.Size = New System.Drawing.Size(61, 20)
        Me.chkPedido.SoloLectura = False
        Me.chkPedido.TabIndex = 42
        Me.chkPedido.Texto = "Pedido"
        Me.chkPedido.Valor = False
        '
        'chkVenta
        '
        Me.chkVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkVenta.Color = System.Drawing.Color.Empty
        Me.chkVenta.Location = New System.Drawing.Point(157, 67)
        Me.chkVenta.Name = "chkVenta"
        Me.chkVenta.Size = New System.Drawing.Size(61, 20)
        Me.chkVenta.SoloLectura = False
        Me.chkVenta.TabIndex = 43
        Me.chkVenta.Texto = "Venta"
        Me.chkVenta.Valor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 74)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 13)
        Me.Label1.TabIndex = 44
        Me.Label1.Text = "Anulacion de:"
        '
        'frmMotivoAnulacionVenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(531, 418)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.chkVenta)
        Me.Controls.Add(Me.chkPedido)
        Me.Controls.Add(Me.chkMantenerPedidoVigente)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.lvLista)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.rdbDesactivado)
        Me.Controls.Add(Me.rdbActivo)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frmMotivoAnulacionVenta"
        Me.Text = "Motivo de anulacion de venta"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents rdbDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbActivo As System.Windows.Forms.RadioButton
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lvLista As System.Windows.Forms.ListView
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents chkMantenerPedidoVigente As ocxCHK
    Friend WithEvents chkVenta As ocxCHK
    Friend WithEvents chkPedido As ocxCHK
    Friend WithEvents Label1 As Label
End Class

﻿Public Class frmTipoProductoCuentaContableMotivoNC

    Dim CSistema As New CSistema

    Public Property IDTipoProducto As Integer

    Dim dt As DataTable

    Sub Inicializar()

        txtCuentaContableAcuerdo.Conectar()
        txtCuentaContableDescuento.Conectar()
        txtCuentaContableDiferenciaPrecio.Conectar()
        CargarInformacion()
    End Sub

    Sub CargarInformacion()
        dt = CSistema.ExecuteToDataTable("Select top(1) * from VTipoProductoCuentaContableMotivoNC where IDTipoProducto = " & IDTipoProducto)
        If dt Is Nothing Then
            Exit Sub
        End If
        If dt.Rows.Count = 0 Then
            Exit Sub
        End If
        Dim oRow As DataRow = dt.Rows(0)
        lblTipoProducto.Text = oRow("TipoProducto").ToString
        'txtCuentaContableAcuerdo.SeleccionarRegistro(oRow("IDCuentaContableAcuerdo"))
        'txtCuentaContableDescuento.SeleccionarRegistro(oRow("IDCuentaContableDescuento"))
        'txtCuentaContableDiferenciaPrecio.SeleccionarRegistro(oRow("IDCuentaContableDiferenciaPrecio"))

        CSistema.CargarRowEnControl(oRow, "CuentaContableAcuerdo", txtCuentaContableAcuerdo)
        CSistema.CargarRowEnControl(oRow, "CuentaContableDescuento", txtCuentaContableDescuento)
        CSistema.CargarRowEnControl(oRow, "CuentaContableDiferenciaPrecio", txtCuentaContableDiferenciaPrecio)



        txtUsuarioModificacion.Text = oRow("UltimoUsuarioModificacion").ToString
        txtFechaModificacion.Text = oRow("UltimaFechaModificacion").ToString

    End Sub

    Sub Guardar()

        tsslEstado.Text = ""
        ctrError.Clear()

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDTipoProducto", IDTipoProducto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaContableAcuerdo", txtCuentaContableAcuerdo.txtCodigo.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaContableDescuento", txtCuentaContableDescuento.txtCodigo.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaContableDiferenciaPrecio", txtCuentaContableDiferenciaPrecio.txtCodigo.GetValue, ParameterDirection.Input)

        'Transaccion
        'CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpTipoProductoCuentaContableMotivoNC", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            'CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            'Listar(txtID.txt.Text)
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If
        CargarInformacion()
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub frmTipoProductoCuentaContableMotivoNC_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
End Class
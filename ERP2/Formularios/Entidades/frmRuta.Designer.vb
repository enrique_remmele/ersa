﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRuta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cbxArea = New ERP.ocxCBX()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lklZonaVentaMapa = New System.Windows.Forms.LinkLabel()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.cbxZonaVenta = New ERP.ocxCBX()
        Me.Label2 = New System.Windows.Forms.Label()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cbxArea
        '
        Me.cbxArea.CampoWhere = Nothing
        Me.cbxArea.CargarUnaSolaVez = False
        Me.cbxArea.DataDisplayMember = "Descripcion"
        Me.cbxArea.DataFilter = Nothing
        Me.cbxArea.DataOrderBy = "ID"
        Me.cbxArea.DataSource = "VArea"
        Me.cbxArea.DataValueMember = "ID"
        Me.cbxArea.FormABM = Nothing
        Me.cbxArea.Indicaciones = Nothing
        Me.cbxArea.Location = New System.Drawing.Point(90, 83)
        Me.cbxArea.Name = "cbxArea"
        Me.cbxArea.SeleccionObligatoria = False
        Me.cbxArea.Size = New System.Drawing.Size(237, 21)
        Me.cbxArea.SoloLectura = False
        Me.cbxArea.TabIndex = 103
        Me.cbxArea.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 91)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 102
        Me.Label1.Text = "Area:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = "ID"
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(90, 56)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(237, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 101
        Me.cbxSucursal.Texto = ""
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(12, 64)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 100
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lklZonaVentaMapa
        '
        Me.lklZonaVentaMapa.AutoSize = True
        Me.lklZonaVentaMapa.Location = New System.Drawing.Point(87, 382)
        Me.lklZonaVentaMapa.Name = "lklZonaVentaMapa"
        Me.lklZonaVentaMapa.Size = New System.Drawing.Size(68, 13)
        Me.lklZonaVentaMapa.TabIndex = 99
        Me.lklZonaVentaMapa.TabStop = True
        Me.lklZonaVentaMapa.Text = "Ver en Mapa"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(90, 29)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(237, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 98
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(414, 385)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 95
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(151, 145)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 88
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(90, 145)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 87
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 412)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(545, 22)
        Me.StatusStrip1.TabIndex = 96
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(90, 2)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 97
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(12, 147)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 86
        Me.lblEstado.Text = "Estado:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(333, 179)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 92
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(414, 179)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 93
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(171, 179)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 90
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(90, 179)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 89
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(252, 179)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 91
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'lvLista
        '
        Me.lvLista.Location = New System.Drawing.Point(90, 208)
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(399, 171)
        Me.lvLista.TabIndex = 94
        Me.lvLista.UseCompatibleStateImageBehavior = False
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(12, 33)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 85
        Me.lblDescripcion.Text = "Descripción:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(12, 7)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 84
        Me.lblID.Text = "ID:"
        '
        'cbxZonaVenta
        '
        Me.cbxZonaVenta.CampoWhere = Nothing
        Me.cbxZonaVenta.CargarUnaSolaVez = False
        Me.cbxZonaVenta.DataDisplayMember = "Descripcion"
        Me.cbxZonaVenta.DataFilter = Nothing
        Me.cbxZonaVenta.DataOrderBy = "ID"
        Me.cbxZonaVenta.DataSource = "VZonaVenta"
        Me.cbxZonaVenta.DataValueMember = "ID"
        Me.cbxZonaVenta.FormABM = Nothing
        Me.cbxZonaVenta.Indicaciones = Nothing
        Me.cbxZonaVenta.Location = New System.Drawing.Point(90, 110)
        Me.cbxZonaVenta.Name = "cbxZonaVenta"
        Me.cbxZonaVenta.SeleccionObligatoria = False
        Me.cbxZonaVenta.Size = New System.Drawing.Size(237, 21)
        Me.cbxZonaVenta.SoloLectura = False
        Me.cbxZonaVenta.TabIndex = 105
        Me.cbxZonaVenta.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 118)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 13)
        Me.Label2.TabIndex = 104
        Me.Label2.Text = "Zona Vta:"
        '
        'frmRuta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(545, 434)
        Me.Controls.Add(Me.cbxZonaVenta)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbxArea)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbxSucursal)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.lklZonaVentaMapa)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.rdbDesactivado)
        Me.Controls.Add(Me.rdbActivo)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.lvLista)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.lblID)
        Me.Name = "frmRuta"
        Me.Tag = "frmRuta"
        Me.Text = "frmRuta"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxArea As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lklZonaVentaMapa As System.Windows.Forms.LinkLabel
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents rdbDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbActivo As System.Windows.Forms.RadioButton
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents lvLista As System.Windows.Forms.ListView
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents cbxZonaVenta As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class

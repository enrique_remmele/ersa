﻿Public Class frmTipoProductoCuentaContableSubMotivoNotaCredito
    Dim CSistema As New CSistema
    Public Property IDTipoProducto As Integer

    Dim dt As New DataTable

    Sub Inicializar()
        CargarInformacion()
    End Sub

    Sub CargarInformacion()
        dt = CSistema.ExecuteToDataTable("Select * from VCFMotivoNotaCredito where idtipoproducto is null or idtipoproducto = " & IDTipoProducto)
        Listar()
    End Sub

    Sub Listar()
        CSistema.dtToGrid(dgv, dt)
    End Sub

    Private Sub frmTipoProductoCuentaContableSubMotivoNotaCredito_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
End Class
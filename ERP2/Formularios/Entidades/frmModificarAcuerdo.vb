﻿Public Class frmModificarAcuerdo
    Dim Csistema As New CSistema
    Dim CData As New CData
    'Private dt As Object
    Public Property IDTransaccion As Integer
    Public Property Acuerdo As String
    Public Property MotivoAcuerdo As String
    Public Property VigenciaDesde As String
    Public Property VigenciaHasta As String
    Public Property Porcentaje As String
    Public Property Importe As String
    Public Property Observacion As String
    Public Property Producto As String
    Public Property Presentacion As Integer
    Public Property Familia As Integer
    Public Property Todos As String
    Public Property Estado As Boolean

    Sub Inicializar()
        Cargar()
    End Sub

    Sub Cargar()
        Csistema.SqlToComboBox(cbxMotivoAcuerdo.cbx, "Select ID, Descripcion From MotivoAcuerdoClientes")
        Csistema.SqlToComboBox(cbxPresentacion.cbx, "Select ID, Descripcion From Presentacion where id in (1, 2)")
        Csistema.SqlToComboBox(cbxFamilia.cbx, "Select ID, Descripcion From SubLinea where id in (7, 5, 10, 11, 12, 19, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32)")

        txtNroAcuerdo.txt.Text = Acuerdo
        cbxMotivoAcuerdo.cbx.Text = MotivoAcuerdo
        txtVigenciaDesde.txt.Text = VigenciaDesde
        txtVigenciaHasta.txt.Text = VigenciaHasta
        txtPorcentaje.txt.Text = Porcentaje
        txtImporte.txt.Text = Importe
        txtObservacion.txt.Text = Observacion
        cbxPresentacion.cbx.Text = Presentacion
        cbxFamilia.cbx.Text = Familia


    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()

    End Sub

    Private Sub frmModificarAcuerdo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        'Guardar()
    End Sub

End Class
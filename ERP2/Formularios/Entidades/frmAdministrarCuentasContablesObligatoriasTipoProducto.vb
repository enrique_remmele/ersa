﻿Public Class frmAdministrarCuentasContablesObligatoriasTipoProducto

    Dim CSistema As New CSistema
    Public Property IDTipoProducto As Integer
    Dim dtTipoProductoCuentaContableObligatoria As DataTable

    Sub Inicializar()
        CargarInformacion()
    End Sub

    Sub CargarInformacion()

        dtTipoProductoCuentaContableObligatoria = CSistema.ExecuteToDataTable("Select * from vTipoProductoCuentaContableObligatoria where IDTipoProducto = " & IDTipoProducto)

        'CSistema.CargarRowEnControl(dtTipoProductoCuentaContableObligatoria.Rows(0), "CuentaContableVenta", chkCuentaContableVenta)
        'CSistema.CargarRowEnControl(dtTipoProductoCuentaContableObligatoria.Rows(0), "CuentaContableCompra", chkCuentaContableCompra)
        'CSistema.CargarRowEnControl(dtTipoProductoCuentaContableObligatoria.Rows(0), "CuentaContableDeudor", chkCuentaContableDeudor)
        'CSistema.CargarRowEnControl(dtTipoProductoCuentaContableObligatoria.Rows(0), "CuentaContableCosto", chkCuentaContableCosto)
        If dtTipoProductoCuentaContableObligatoria.Rows.Count > 0 Then
            chkCuentaContableVenta.chk.Checked = CBool(dtTipoProductoCuentaContableObligatoria.Rows(0)("CuentaContableVenta"))
            chkCuentaContableCompra.chk.Checked = CBool(dtTipoProductoCuentaContableObligatoria.Rows(0)("CuentaContableCompra"))
            chkCuentaContableDeudor.chk.Checked = CBool(dtTipoProductoCuentaContableObligatoria.Rows(0)("CuentaContableDeudor"))
            chkCuentaContableCosto.chk.Checked = CBool(dtTipoProductoCuentaContableObligatoria.Rows(0)("CuentaContableCosto"))
            lblUsuario.Text = "Ultimo Usuario en Modificar: " & dtTipoProductoCuentaContableObligatoria.Rows(0)("UsuarioUltimaModificacion") & " - Fecha: " & CDate(dtTipoProductoCuentaContableObligatoria.Rows(0)("FechaUltimaModificacion"))
            lblTipoProducto.Text = dtTipoProductoCuentaContableObligatoria.Rows(0)("TipoProducto")


        Else
            MessageBox.Show("Es necesario cargar las restricciones por primera vez", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

    End Sub

    Sub Guardar()
        Dim Sql As String = ""
        If dtTipoProductoCuentaContableObligatoria.Rows.Count > 0 Then
            Sql = " Update TipoProductoCuentaContableObligatoria " &
                            "Set CuentaContableVenta = '" & chkCuentaContableVenta.Valor & "' " &
                            ",CuentaContableCompra = '" & chkCuentaContableCompra.Valor & "' " &
                            ",CuentaContableDeudor = '" & chkCuentaContableDeudor.Valor & "' " &
                            ",CuentaContableCosto = '" & chkCuentaContableCosto.Valor & "' " &
                            ",IDUsuarioUltimaModificacion = " & vgIDUsuario & " " &
                            ",FechaUltimaModificacion = GetDate() " &
                            " Where IDTipoProducto = " & IDTipoProducto

        Else
            Sql = " insert into TipoProductoCuentaContableObligatoria(IDTipoProducto,CuentaContableVenta,CuentaContableCompra,CuentaContableDeudor,CuentaContableCosto,IDUsuarioUltimaModificacion,FechaUltimaModificacion) " &
                                                            " Values (" & IDTipoProducto & ",'" & chkCuentaContableVenta.Valor & "', '" & chkCuentaContableCompra.Valor & "', '" & chkCuentaContableDeudor.Valor & "', '" & chkCuentaContableCosto.Valor & "'," & vgIDUsuario & ", GetDate())"


        End If


        CSistema.ExecuteNonQuery(Sql)
        Me.Close()

    End Sub

    Private Sub frmAdministrarCuentasContablesObligatoriasTipoProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class
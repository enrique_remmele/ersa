﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTerminalTransaccion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblImpresora = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblDeposito = New System.Windows.Forms.Label()
        Me.lblCodigo = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.btnBuscarImpresora = New System.Windows.Forms.Button()
        Me.txtImpresora = New ERP.ocxTXTString()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tpTerminales = New System.Windows.Forms.TabPage()
        Me.tpPuntosExpedicion = New System.Windows.Forms.TabPage()
        Me.lvListaPE = New System.Windows.Forms.ListView()
        Me.btnPENuevo = New System.Windows.Forms.Button()
        Me.btnPEGuardar = New System.Windows.Forms.Button()
        Me.btnPECancelar = New System.Windows.Forms.Button()
        Me.btnPEEliminar = New System.Windows.Forms.Button()
        Me.lblPEPuntoExpedicion = New System.Windows.Forms.Label()
        Me.cbxPEPuntoExpedicion = New ERP.ocxCBX()
        Me.cbxPETerminal = New ERP.ocxCBX()
        Me.lblPETerminal = New System.Windows.Forms.Label()
        Me.lblPEOperacion = New System.Windows.Forms.Label()
        Me.cbxPEOperacion = New ERP.ocxCBX()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tpTerminales.SuspendLayout()
        Me.tpPuntosExpedicion.SuspendLayout()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'lblImpresora
        '
        Me.lblImpresora.AutoSize = True
        Me.lblImpresora.Location = New System.Drawing.Point(6, 121)
        Me.lblImpresora.Name = "lblImpresora"
        Me.lblImpresora.Size = New System.Drawing.Size(53, 13)
        Me.lblImpresora.TabIndex = 8
        Me.lblImpresora.Text = "Impresora"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 402)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(515, 22)
        Me.StatusStrip1.TabIndex = 2
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(406, 372)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(321, 170)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 26
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(402, 170)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 27
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(240, 170)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 25
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(159, 170)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 24
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(78, 170)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 23
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lvLista
        '
        Me.lvLista.Location = New System.Drawing.Point(78, 199)
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(399, 101)
        Me.lvLista.TabIndex = 28
        Me.lvLista.UseCompatibleStateImageBehavior = False
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(6, 94)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 6
        Me.lblDescripcion.Text = "Descripción:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(6, 40)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 2
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(6, 13)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'lblDeposito
        '
        Me.lblDeposito.AutoSize = True
        Me.lblDeposito.Location = New System.Drawing.Point(6, 67)
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(52, 13)
        Me.lblDeposito.TabIndex = 4
        Me.lblDeposito.Text = "Deposito:"
        '
        'lblCodigo
        '
        Me.lblCodigo.AutoSize = True
        Me.lblCodigo.Location = New System.Drawing.Point(6, 148)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(43, 13)
        Me.lblCodigo.TabIndex = 11
        Me.lblCodigo.Text = "Codigo:"
        '
        'txtCodigo
        '
        Me.txtCodigo.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCodigo.Location = New System.Drawing.Point(78, 144)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.ReadOnly = True
        Me.txtCodigo.Size = New System.Drawing.Size(318, 20)
        Me.txtCodigo.TabIndex = 12
        '
        'btnBuscarImpresora
        '
        Me.btnBuscarImpresora.Location = New System.Drawing.Point(369, 117)
        Me.btnBuscarImpresora.Name = "btnBuscarImpresora"
        Me.btnBuscarImpresora.Size = New System.Drawing.Size(27, 21)
        Me.btnBuscarImpresora.TabIndex = 10
        Me.btnBuscarImpresora.Text = "..."
        Me.btnBuscarImpresora.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBuscarImpresora.UseVisualStyleBackColor = True
        '
        'txtImpresora
        '
        Me.txtImpresora.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImpresora.Color = System.Drawing.Color.Empty
        Me.txtImpresora.Indicaciones = Nothing
        Me.txtImpresora.Location = New System.Drawing.Point(78, 117)
        Me.txtImpresora.Multilinea = False
        Me.txtImpresora.Name = "txtImpresora"
        Me.txtImpresora.Size = New System.Drawing.Size(285, 21)
        Me.txtImpresora.SoloLectura = False
        Me.txtImpresora.TabIndex = 9
        Me.txtImpresora.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtImpresora.Texto = ""
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = Nothing
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = Nothing
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = Nothing
        Me.cbxDeposito.DataSource = Nothing
        Me.cbxDeposito.DataValueMember = Nothing
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(78, 63)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(215, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 5
        Me.cbxDeposito.Texto = ""
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(78, 90)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(215, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 7
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(78, 36)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(215, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 3
        Me.cbxSucursal.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(78, 8)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 1
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tpTerminales)
        Me.TabControl1.Controls.Add(Me.tpPuntosExpedicion)
        Me.TabControl1.Location = New System.Drawing.Point(0, 3)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(510, 363)
        Me.TabControl1.TabIndex = 0
        '
        'tpTerminales
        '
        Me.tpTerminales.Controls.Add(Me.lblID)
        Me.tpTerminales.Controls.Add(Me.btnBuscarImpresora)
        Me.tpTerminales.Controls.Add(Me.txtID)
        Me.tpTerminales.Controls.Add(Me.txtImpresora)
        Me.tpTerminales.Controls.Add(Me.cbxSucursal)
        Me.tpTerminales.Controls.Add(Me.txtCodigo)
        Me.tpTerminales.Controls.Add(Me.lblSucursal)
        Me.tpTerminales.Controls.Add(Me.lblCodigo)
        Me.tpTerminales.Controls.Add(Me.lblDescripcion)
        Me.tpTerminales.Controls.Add(Me.lblDeposito)
        Me.tpTerminales.Controls.Add(Me.txtDescripcion)
        Me.tpTerminales.Controls.Add(Me.cbxDeposito)
        Me.tpTerminales.Controls.Add(Me.lvLista)
        Me.tpTerminales.Controls.Add(Me.lblImpresora)
        Me.tpTerminales.Controls.Add(Me.btnNuevo)
        Me.tpTerminales.Controls.Add(Me.btnEditar)
        Me.tpTerminales.Controls.Add(Me.btnGuardar)
        Me.tpTerminales.Controls.Add(Me.btnCancelar)
        Me.tpTerminales.Controls.Add(Me.btnEliminar)
        Me.tpTerminales.Location = New System.Drawing.Point(4, 22)
        Me.tpTerminales.Name = "tpTerminales"
        Me.tpTerminales.Padding = New System.Windows.Forms.Padding(3)
        Me.tpTerminales.Size = New System.Drawing.Size(502, 337)
        Me.tpTerminales.TabIndex = 0
        Me.tpTerminales.Text = "Terminales"
        Me.tpTerminales.UseVisualStyleBackColor = True
        '
        'tpPuntosExpedicion
        '
        Me.tpPuntosExpedicion.Controls.Add(Me.lvListaPE)
        Me.tpPuntosExpedicion.Controls.Add(Me.btnPENuevo)
        Me.tpPuntosExpedicion.Controls.Add(Me.btnPEGuardar)
        Me.tpPuntosExpedicion.Controls.Add(Me.btnPECancelar)
        Me.tpPuntosExpedicion.Controls.Add(Me.btnPEEliminar)
        Me.tpPuntosExpedicion.Controls.Add(Me.lblPEPuntoExpedicion)
        Me.tpPuntosExpedicion.Controls.Add(Me.cbxPEPuntoExpedicion)
        Me.tpPuntosExpedicion.Controls.Add(Me.cbxPETerminal)
        Me.tpPuntosExpedicion.Controls.Add(Me.lblPETerminal)
        Me.tpPuntosExpedicion.Controls.Add(Me.lblPEOperacion)
        Me.tpPuntosExpedicion.Controls.Add(Me.cbxPEOperacion)
        Me.tpPuntosExpedicion.Location = New System.Drawing.Point(4, 22)
        Me.tpPuntosExpedicion.Name = "tpPuntosExpedicion"
        Me.tpPuntosExpedicion.Padding = New System.Windows.Forms.Padding(3)
        Me.tpPuntosExpedicion.Size = New System.Drawing.Size(502, 337)
        Me.tpPuntosExpedicion.TabIndex = 1
        Me.tpPuntosExpedicion.Text = "Puntos de Expedicion"
        Me.tpPuntosExpedicion.UseVisualStyleBackColor = True
        '
        'lvListaPE
        '
        Me.lvListaPE.Location = New System.Drawing.Point(78, 128)
        Me.lvListaPE.Name = "lvListaPE"
        Me.lvListaPE.Size = New System.Drawing.Size(399, 150)
        Me.lvListaPE.TabIndex = 11
        Me.lvListaPE.UseCompatibleStateImageBehavior = False
        '
        'btnPENuevo
        '
        Me.btnPENuevo.Location = New System.Drawing.Point(78, 99)
        Me.btnPENuevo.Name = "btnPENuevo"
        Me.btnPENuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnPENuevo.TabIndex = 6
        Me.btnPENuevo.Text = "&Nuevo"
        Me.btnPENuevo.UseVisualStyleBackColor = True
        '
        'btnPEGuardar
        '
        Me.btnPEGuardar.Location = New System.Drawing.Point(159, 99)
        Me.btnPEGuardar.Name = "btnPEGuardar"
        Me.btnPEGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnPEGuardar.TabIndex = 8
        Me.btnPEGuardar.Text = "&Guardar"
        Me.btnPEGuardar.UseVisualStyleBackColor = True
        '
        'btnPECancelar
        '
        Me.btnPECancelar.Location = New System.Drawing.Point(240, 99)
        Me.btnPECancelar.Name = "btnPECancelar"
        Me.btnPECancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnPECancelar.TabIndex = 9
        Me.btnPECancelar.Text = "&Cancelar"
        Me.btnPECancelar.UseVisualStyleBackColor = True
        '
        'btnPEEliminar
        '
        Me.btnPEEliminar.Location = New System.Drawing.Point(321, 99)
        Me.btnPEEliminar.Name = "btnPEEliminar"
        Me.btnPEEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnPEEliminar.TabIndex = 10
        Me.btnPEEliminar.Text = "E&liminar"
        Me.btnPEEliminar.UseVisualStyleBackColor = True
        '
        'lblPEPuntoExpedicion
        '
        Me.lblPEPuntoExpedicion.AutoSize = True
        Me.lblPEPuntoExpedicion.Location = New System.Drawing.Point(6, 66)
        Me.lblPEPuntoExpedicion.Name = "lblPEPuntoExpedicion"
        Me.lblPEPuntoExpedicion.Size = New System.Drawing.Size(65, 13)
        Me.lblPEPuntoExpedicion.TabIndex = 4
        Me.lblPEPuntoExpedicion.Text = "P. de Expe.:"
        '
        'cbxPEPuntoExpedicion
        '
        Me.cbxPEPuntoExpedicion.CampoWhere = Nothing
        Me.cbxPEPuntoExpedicion.CargarUnaSolaVez = False
        Me.cbxPEPuntoExpedicion.DataDisplayMember = Nothing
        Me.cbxPEPuntoExpedicion.DataFilter = Nothing
        Me.cbxPEPuntoExpedicion.DataOrderBy = Nothing
        Me.cbxPEPuntoExpedicion.DataSource = Nothing
        Me.cbxPEPuntoExpedicion.DataValueMember = Nothing
        Me.cbxPEPuntoExpedicion.dtSeleccionado = Nothing
        Me.cbxPEPuntoExpedicion.FormABM = Nothing
        Me.cbxPEPuntoExpedicion.Indicaciones = Nothing
        Me.cbxPEPuntoExpedicion.Location = New System.Drawing.Point(78, 62)
        Me.cbxPEPuntoExpedicion.Name = "cbxPEPuntoExpedicion"
        Me.cbxPEPuntoExpedicion.SeleccionMultiple = False
        Me.cbxPEPuntoExpedicion.SeleccionObligatoria = False
        Me.cbxPEPuntoExpedicion.Size = New System.Drawing.Size(215, 21)
        Me.cbxPEPuntoExpedicion.SoloLectura = False
        Me.cbxPEPuntoExpedicion.TabIndex = 5
        Me.cbxPEPuntoExpedicion.Texto = ""
        '
        'cbxPETerminal
        '
        Me.cbxPETerminal.CampoWhere = Nothing
        Me.cbxPETerminal.CargarUnaSolaVez = False
        Me.cbxPETerminal.DataDisplayMember = Nothing
        Me.cbxPETerminal.DataFilter = Nothing
        Me.cbxPETerminal.DataOrderBy = Nothing
        Me.cbxPETerminal.DataSource = Nothing
        Me.cbxPETerminal.DataValueMember = Nothing
        Me.cbxPETerminal.dtSeleccionado = Nothing
        Me.cbxPETerminal.FormABM = Nothing
        Me.cbxPETerminal.Indicaciones = Nothing
        Me.cbxPETerminal.Location = New System.Drawing.Point(78, 8)
        Me.cbxPETerminal.Name = "cbxPETerminal"
        Me.cbxPETerminal.SeleccionMultiple = False
        Me.cbxPETerminal.SeleccionObligatoria = False
        Me.cbxPETerminal.Size = New System.Drawing.Size(215, 21)
        Me.cbxPETerminal.SoloLectura = False
        Me.cbxPETerminal.TabIndex = 1
        Me.cbxPETerminal.Texto = ""
        '
        'lblPETerminal
        '
        Me.lblPETerminal.AutoSize = True
        Me.lblPETerminal.Location = New System.Drawing.Point(6, 12)
        Me.lblPETerminal.Name = "lblPETerminal"
        Me.lblPETerminal.Size = New System.Drawing.Size(50, 13)
        Me.lblPETerminal.TabIndex = 0
        Me.lblPETerminal.Text = "Terminal:"
        '
        'lblPEOperacion
        '
        Me.lblPEOperacion.AutoSize = True
        Me.lblPEOperacion.Location = New System.Drawing.Point(6, 39)
        Me.lblPEOperacion.Name = "lblPEOperacion"
        Me.lblPEOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblPEOperacion.TabIndex = 2
        Me.lblPEOperacion.Text = "Operacion:"
        '
        'cbxPEOperacion
        '
        Me.cbxPEOperacion.CampoWhere = Nothing
        Me.cbxPEOperacion.CargarUnaSolaVez = False
        Me.cbxPEOperacion.DataDisplayMember = Nothing
        Me.cbxPEOperacion.DataFilter = Nothing
        Me.cbxPEOperacion.DataOrderBy = Nothing
        Me.cbxPEOperacion.DataSource = Nothing
        Me.cbxPEOperacion.DataValueMember = Nothing
        Me.cbxPEOperacion.dtSeleccionado = Nothing
        Me.cbxPEOperacion.FormABM = Nothing
        Me.cbxPEOperacion.Indicaciones = Nothing
        Me.cbxPEOperacion.Location = New System.Drawing.Point(78, 35)
        Me.cbxPEOperacion.Name = "cbxPEOperacion"
        Me.cbxPEOperacion.SeleccionMultiple = False
        Me.cbxPEOperacion.SeleccionObligatoria = False
        Me.cbxPEOperacion.Size = New System.Drawing.Size(215, 21)
        Me.cbxPEOperacion.SoloLectura = False
        Me.cbxPEOperacion.TabIndex = 3
        Me.cbxPEOperacion.Texto = ""
        '
        'frmTerminalTransaccion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(515, 424)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnSalir)
        Me.Name = "frmTerminalTransaccion"
        Me.Tag = "TERMINAL DE TRANSACCION"
        Me.Text = "frmTerminalTransaccion"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.tpTerminales.ResumeLayout(False)
        Me.tpTerminales.PerformLayout()
        Me.tpPuntosExpedicion.ResumeLayout(False)
        Me.tpPuntosExpedicion.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents lblImpresora As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lvLista As System.Windows.Forms.ListView
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents lblCodigo As System.Windows.Forms.Label
    Friend WithEvents lblDeposito As System.Windows.Forms.Label
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents btnBuscarImpresora As System.Windows.Forms.Button
    Friend WithEvents txtImpresora As ERP.ocxTXTString
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tpTerminales As System.Windows.Forms.TabPage
    Friend WithEvents tpPuntosExpedicion As System.Windows.Forms.TabPage
    Friend WithEvents cbxPETerminal As ERP.ocxCBX
    Friend WithEvents lblPETerminal As System.Windows.Forms.Label
    Friend WithEvents lblPEOperacion As System.Windows.Forms.Label
    Friend WithEvents cbxPEOperacion As ERP.ocxCBX
    Friend WithEvents lvListaPE As System.Windows.Forms.ListView
    Friend WithEvents btnPENuevo As System.Windows.Forms.Button
    Friend WithEvents btnPEGuardar As System.Windows.Forms.Button
    Friend WithEvents btnPECancelar As System.Windows.Forms.Button
    Friend WithEvents btnPEEliminar As System.Windows.Forms.Button
    Friend WithEvents lblPEPuntoExpedicion As System.Windows.Forms.Label
    Friend WithEvents cbxPEPuntoExpedicion As ERP.ocxCBX
End Class

﻿Public Class frmAcuerdodeClientes

    'Clases
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim vControles() As Control
    Dim DT As DataTable

    'Variables
    Dim vNuevo As Boolean
    Public Property Insertar As Boolean = True
    Dim IdSucursalCliente As Integer = 0
    Dim dtDetalle As New DataTable

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Sub Inicializar()
        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        ''txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito  From VCliente Where Estado='ACTIVO' "
        txtCliente.frm = Me
        txtCliente.MostrarSucursal = True

        txtProveedor.Conectar("Select * From VProveedor Where Estado='True' Order By RazonSocial ")

        txtID.Focus()

        'Funciones
        CargarInformacion()

        'CargarOperacion()

        ''Botones
        'CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnModificar, btnGuardar, btnCancelar, New Button, vControles)
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnGuardar, btnCancelar, New Button, vControles)
        'EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)
        Cancelar()


    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        CSistema.CargaControl(vControles, rbNC)
        CSistema.CargaControl(vControles, rbFactura)
        CSistema.CargaControl(vControles, txtID)
        CSistema.CargaControl(vControles, txtNroAcuerdo)
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, txtDireccion)
        CSistema.CargaControl(vControles, txtProveedor)
        CSistema.CargaControl(vControles, chkTSucursales)
        CSistema.CargaControl(vControles, ChkPorcentaje)
        CSistema.CargaControl(vControles, ChkImporte)
        CSistema.CargaControl(vControles, txtPorcentaje)
        CSistema.CargaControl(vControles, txtImporte)
        CSistema.CargaControl(vControles, cbxMotivoAcuerdo)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, ChkProducto)
        CSistema.CargaControl(vControles, lvlista)
        CSistema.CargaControl(vControles, lklAgregar)
        CSistema.CargaControl(vControles, lklEliminar)
        'CSistema.CargaControl(vControles, cbxProducto)
        'CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, ChkPresentacion)
        CSistema.CargaControl(vControles, cbxPresentacion)
        CSistema.CargaControl(vControles, ChkFamilia)
        CSistema.CargaControl(vControles, cbxFamilia)
        CSistema.CargaControl(vControles, ChkTodos)
        CSistema.CargaControl(vControles, rbActivado)
        CSistema.CargaControl(vControles, rbDesactivado)

        
        'Tipo de Acuerdo
        CSistema.SqlToComboBox(cbxMotivoAcuerdo.cbx, "Select ID, Descripcion From MotivoAcuerdoClientes")
        'CSistema.SqlToComboBox(cbxProducto.cbx, "Select ID, Descripcion From Producto where IDTipoProducto in (1, 2, 6) and Estado = 1 Order By Descripcion Asc")
        CSistema.SqlToComboBox(cbxPresentacion.cbx, "Select ID, Descripcion From Presentacion where id in (1, 2)")
        CSistema.SqlToComboBox(cbxFamilia.cbx, "Select ID, Descripcion From SubLinea where id in (7, 5, 10, 11, 12, 19, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32)")

        dtDetalle = CData.GetStructure("Vdetalleacuerdodeclientes", "Select Top(0) * From Vdetalleacuerdodeclientes")

        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(ID) From AcuerdodeClientes),1)"), Integer)

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)
        txtObservacion.SoloLectura = True

        cbxMotivoAcuerdo.SoloLectura = True

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select ID From AcuerdodeClientes Where ID = " & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            txtID.txt.Focus()
            txtID.txt.SelectAll()
            Exit Sub
        End If

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VAcuerdodeClientes Where ID= " & IDTransaccion)
        'dtDetalle = CSistema.ExecuteToDataTable("Select IDProducto, Referencia, Descripcion From VdetalleAcuerdodeClientes Where IDAcuerdo=" & IDTransaccion & " Order By IDProducto").Copy
        dtDetalle = CSistema.ExecuteToDataTable("Select IDProducto, Referencia, Descripcion, Porcentaje From VdetalleAcuerdodeClientes Where IDAcuerdo=" & IDTransaccion & " Order By IDProducto").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        txtID.SetValue(oRow("ID").ToString)
        txtNroAcuerdo.SetValue(oRow("NroAcuerdo").ToString)

        rbNC.Checked = oRow("NC").ToString
        rbFactura.Checked = oRow("Factura").ToString

        If rbNC.Checked = True Then
            ''txtCliente.SeleccionarRegistro(oRow("IDCliente"))
            txtCliente.txtReferencia.txt.Text = (oRow("ReferenciaC").ToString)
            txtCliente.txtRazonSocial.txt.Text = oRow("Cliente").ToString
            txtCliente.txtRUC.txt.Text = oRow("RUCCli").ToString
            txtCliente.txtID.SetValue(oRow("IDCliente"))
            txtDireccion.txt.Text = oRow("Direccion").ToString
            lblProveedor.Visible = False
            txtProveedor.Visible = False
            IdSucursalCliente = oRow("IDSucursalCliente")
        Else
            'txtProveedor.SeleccionarRegistro(oRow("IDProveedor"))
            txtProveedor.txtReferencia.txt.Text = (oRow("ReferenciaP").ToString)
            txtProveedor.txtRazonSocial.txt.Text = oRow("Proveedor").ToString
            txtProveedor.txtRUC.txt.Text = oRow("RUCPro").ToString
            txtProveedor.txtID.SetValue(oRow("IDProveedor"))

            lblCliente.Visible = False
            txtCliente.Visible = False
            lblDireccion.Visible = False
            txtDireccion.Visible = False
            lblProveedor.Visible = True
            txtProveedor.Visible = True
            chkTSucursales.Visible = False
        End If

        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        If oRow("TSucursales") <> 0 Then
            chkTSucursales.Checked = True
        Else
            chkTSucursales.Checked = False
        End If
        chkTSucursales.Enabled = False

        txtPorcentaje.txt.Text = oRow("Porcentaje").ToString
        txtImporte.txt.Text = oRow("Importe").ToString

        If txtPorcentaje.txt.Text <> 0 Then
            ChkPorcentaje.Checked = True
        Else
            ChkPorcentaje.Checked = False
        End If
        If txtImporte.txt.Text <> 0 Then
            ChkImporte.Checked = True
        Else
            ChkImporte.Checked = False
        End If

        txtVigenciaDesde.SetValueFromString(CDate(oRow("VigenciaDesde").ToString))
        txtVigenciaHasta.SetValueFromString(CDate(oRow("VigenciaHasta").ToString))

        cbxMotivoAcuerdo.cbx.Text = oRow("Motivo")
        txtObservacion.txt.Text = oRow("Observacion").ToString

        If lvlista.Items.Count > 0 Then
            ChkProducto.Checked = True
        Else
            ChkProducto.Checked = False
        End If

        'If ChkProducto.Checked = True Then
        '    cbxProducto.cbx.Text = oRow("ID")
        'Else
        '    cbxProducto.cbx.Text = "xx"
        'End If

        cbxPresentacion.SoloLectura = False
        cbxPresentacion.Enabled = True
        cbxPresentacion.cbx.Text = ""

        'cbxPresentacion.Enabled = False

        If oRow("IDPresentacion").ToString = 0 Then
            ChkPresentacion.Checked = False
            'cbxPresentacion.Text = ""
            cbxPresentacion.Visible = False
            txtpre.Visible = True
        Else
            ChkPresentacion.Checked = True
            cbxPresentacion.Text = oRow("Presentacion").ToString
        End If
        cbxPresentacion.SoloLectura = True

        cbxFamilia.SoloLectura = False
        cbxFamilia.Enabled = True
        cbxFamilia.cbx.Text = ""

        'cbxFamilia.Enabled = False

        If oRow("IDsublinea").ToString = 0 Then
            ChkFamilia.Checked = False
            'cbxFamilia.Text = ""
            cbxFamilia.Visible = False
            txtf.Visible = True
        Else
            ChkFamilia.Checked = True
            cbxFamilia.Text = oRow("Familia").ToString
        End If
        cbxFamilia.SoloLectura = True

        'If ChkPresentacion.Checked = True Then
        '    cbxPresentacion.cbx.Text = oRow("ID")
        'Else
        '    cbxPresentacion.cbx.Text = ""
        'End If

        'If ChkPresentacion.Checked = True Then
        '    cbxFamilia.cbx.Text = oRow("ID")
        'Else
        '    cbxFamilia.cbx.Text = ""
        'End If

        'Cargamos el detalle
        Listardetalle()

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaCarga").ToString)
        lblUsuarioRegistro.Text = oRow("Usuario").ToString

        'Foco
        txtID.txt.Focus()
        txtID.txt.SelectAll()

    End Sub

    Sub Cancelar()

        txtID.Enabled = "True"

        txtID.SoloLectura = False
        txtID.txt.Focus()

        Dim ID As Integer
        ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From AcuerdodeClientes"), Integer)

        txtID.txt.Text = ID
        txtID.txt.SelectAll()
        CargarOperacion()
        vNuevo = False

    End Sub


    Sub Nuevo()

        IDTransaccion = 0

        txtFecha.Hoy()
        txtNroAcuerdo.Clear()
        txtCliente.Clear()
        txtDireccion.Clear()
        txtProveedor.Clear()
        txtPorcentaje.Clear()
        txtImporte.Clear()
        txtObservacion.Clear()
        dtDetalle.Rows.Clear()
        Listardetalle()

        txtNroAcuerdo.Visible = True
        txtPorcentaje.Enabled = False
        txtPorcentaje.Visible = True
        txtImporte.Enabled = False
        txtImporte.Visible = True
        txtObservacion.Visible = True

        txtID.Focus()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True

        'cbxMotivoAcuerdo.cbx.Text = ""
        rbActivado.Checked = True
        rbDesactivado.Checked = False
        rbNC.Checked = True
        rbFactura.Checked = False

        chkTSucursales.Checked = False
        chkTSucursales.Enabled = True

        ChkPorcentaje.Checked = False
        ChkPorcentaje.Enabled = True
        ChkImporte.Checked = False
        ChkImporte.Enabled = True

        ChkProducto.Checked = False
        ChkProducto.Enabled = True
        'cbxproducto.cbx.Enabled = False
        ChkPresentacion.Checked = False
        ChkPresentacion.Enabled = True
        'cbxPresentacion.cbx.Enabled = False
        ChkFamilia.Checked = False
        ChkFamilia.Enabled = True
        'cbxFamilia.cbx.Enabled = False
        ChkTodos.Checked = False
        ChkTodos.Enabled = True

        'Limpiar Detalle
        lvlista.Items.Clear()

        flpRegistradoPor.Visible = False
        lblFechaRegistro.Visible = False
        lblUsuarioRegistro.Visible = False

        IDTransaccion = 0

        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(ID + 1) FROM AcuerdodeClientes),1)"), Integer)
        txtID.SoloLectura = True

        txtID.txt.SelectAll()

        ComportamientoBoton()
        ComportamientoProducto()
        ComportamientoPresentacion()
        ComportamientoFamilia()
        SeleccionarTodos()
        HabilitarPorcentaje()
        HabilitarImporte()

    End Sub

    Private Sub txtID_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        MoverRegistro(e)
    End Sub

    Sub MoverRegistro(ByRef e As System.Windows.Forms.KeyEventArgs)

        If vNuevo = True Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(NroAcuerdo), 1) From AcuerdodeCientes"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(NroAcuerdo), 1) From AcuerdodeCientes"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnGuardar, btnCancelar, New Button, vControles)
            vNuevo = True
            Nuevo()
        End If

    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado
        EstablecerSucursalCliente()
    End Sub

    Sub EstablecerSucursalCliente()
        Dim oRow As DataRow = txtCliente.Registro
        Dim oRowSucursal As DataRow = txtCliente.Sucursal

        If txtCliente.SucursalSeleccionada = False Then
            txtDireccion.txt.Text = oRow("Direccion").ToString
            'txtTelefono.txt.Text = oRow("Telefono").ToString
            'SC-04/02/2022 para obtener adicional con filtro IDSucursal
            IdSucursalCliente = 0
        Else
            txtDireccion.txt.Text = oRowSucursal("Direccion").ToString
            'txtTelefono.txt.Text = oRowSucursal("Telefono").ToString
            'SC-04/02/2022 para obtener adicional con filtro IDSucursal
            IdSucursalCliente = oRowSucursal("ID")

        End If
    End Sub

    Private Sub rbNC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbNC.CheckedChanged
        ComportamientoBoton()
    End Sub

    Sub ComportamientoBoton()
        'Cargar combo box de sub motivos
        CSistema.SqlToComboBox(cbxMotivoAcuerdo, "Select ID, Descripcion From MotivoAcuerdoClientes")

        If rbNC.Checked = True Then
            lblCliente.Visible = True
            lblProveedor.Visible = False
            lblDireccion.Visible = True
            txtCliente.Visible = True
            txtProveedor.Visible = False
            txtDireccion.Visible = True
            txtProveedor.Clear()
            chkTSucursales.Visible = True

            CSistema.SqlToComboBox(cbxMotivoAcuerdo, "Select ID, Descripcion From MotivoAcuerdoClientes where NC = 1")
            chkTSucursales.Enabled = True
            'ChkPorcentaje.Enabled = True
            'txtPorcentaje.txt.Enabled = True
            'ChkImporte.Enabled = True
            'txtImporte.txt.Enabled = True
            ChkProducto.Enabled = True
            'cbxproducto.cbx.Enabled = True
            ChkPresentacion.Enabled = True
            'cbxPresentacion.cbx.Enabled = True
            ChkFamilia.Enabled = True
            'cbxFamilia.cbx.Enabled = True
            ChkTodos.Enabled = True
        ElseIf rbFactura.Checked = True Then
            lblCliente.Visible = False
            lblProveedor.Visible = True
            lblDireccion.Visible = False
            txtCliente.Visible = False
            txtProveedor.Visible = True
            txtDireccion.Visible = False
            txtCliente.Clear()
            txtDireccion.Clear()
            chkTSucursales.Visible = False

            CSistema.SqlToComboBox(cbxMotivoAcuerdo, "Select ID, Descripcion From MotivoAcuerdoClientes where Factura = 1")
            chkTSucursales.Enabled = True
            'ChkPorcentaje.Enabled = False
            'txtPorcentaje.txt.Enabled = False
            'ChkImporte.Enabled = False
            'txtImporte.txt.Enabled = False
            ChkProducto.Enabled = False
            lklAgregar.Enabled = False
            ChkPresentacion.Enabled = False
            cbxPresentacion.cbx.Enabled = False
            ChkFamilia.Enabled = False
            cbxFamilia.cbx.Enabled = False
            ChkTodos.Enabled = False

            If vNuevo = True Then
            End If
        End If
    End Sub

    Private Sub chkTSucursales_CheckedChanged(sender As Object, e As EventArgs) Handles chkTSucursales.CheckedChanged
        Todaslassucursales()
    End Sub

    Private Sub ChkPorcentaje_CheckedChanged(sender As Object, e As EventArgs) Handles ChkPorcentaje.CheckedChanged
        HabilitarPorcentaje()
    End Sub

    Private Sub ChkImporte_CheckedChanged(sender As Object, e As EventArgs) Handles ChkImporte.CheckedChanged
        HabilitarImporte()
    End Sub

    Sub Todaslassucursales()
        If vNuevo = True Then
            If chkTSucursales.Checked = True Then
                lblDireccion.Visible = False
                txtDireccion.Visible = False
            Else
                lblDireccion.Visible = True
                txtDireccion.Visible = True
            End If
        End If
    End Sub

    Sub HabilitarPorcentaje()
        'If vNuevo = True Then
        If ChkPorcentaje.Checked = True Then
                txtPorcentaje.Enabled = True
            Else
                txtPorcentaje.Enabled = False
            End If
        'End If
    End Sub

    Sub HabilitarImporte()
        'If vNuevo = True Then
        If ChkImporte.Checked = True Then
                txtImporte.Enabled = True
            Else
                txtImporte.Enabled = False
            End If
        'End If
    End Sub

    Private Sub ChkProducto_CheckedChanged(sender As Object, e As EventArgs) Handles ChkProducto.CheckedChanged
        ComportamientoProducto()
    End Sub

    Private Sub ChkPresentacion_CheckedChanged(sender As Object, e As EventArgs) Handles ChkPresentacion.CheckedChanged
        ComportamientoPresentacion()
    End Sub

    Private Sub ChkFamilia_CheckedChanged(sender As Object, e As EventArgs) Handles ChkFamilia.CheckedChanged
        ComportamientoFamilia()
    End Sub

    Private Sub ChkTodos_CheckedChanged(sender As Object, e As EventArgs) Handles ChkTodos.CheckedChanged
        SeleccionarTodos()
    End Sub

    Sub ComportamientoProducto()
        'If vNuevo = True Then
        If ChkProducto.Checked = True Then
                lklAgregar.Enabled = True
                lklEliminar.Enabled = True
                ChkPresentacion.Checked = False
                cbxPresentacion.cbx.Enabled = False
                ChkFamilia.Checked = False
                cbxFamilia.cbx.Enabled = False
                ChkTodos.Checked = False
            Else
                lklAgregar.Enabled = False
                lklEliminar.Enabled = False
            End If
        'End If
    End Sub

    Sub ComportamientoPresentacion()
        'If vNuevo = True Then
        If ChkPresentacion.Checked = True Then
                cbxPresentacion.cbx.Enabled = True
                ChkProducto.Checked = False
                lklAgregar.Enabled = False
                ChkFamilia.Checked = False
                cbxFamilia.cbx.Enabled = False
                ChkTodos.Checked = False
                cbxPresentacion.Visible = True
                txtpre.Visible = False
            Else
                cbxPresentacion.cbx.Enabled = False
                cbxPresentacion.Visible = False
                txtpre.Visible = True
            End If
        'End If
    End Sub

    Sub ComportamientoFamilia()
        'If vNuevo = True Then
        If ChkFamilia.Checked = True Then
                cbxFamilia.cbx.Enabled = True
                ChkProducto.Checked = False
                lklAgregar.Enabled = False
                ChkPresentacion.Checked = False
                cbxPresentacion.cbx.Enabled = False
                ChkTodos.Checked = False
                cbxFamilia.Visible = True
                txtf.Visible = False
            Else
                cbxFamilia.cbx.Enabled = False
                cbxFamilia.Visible = False
                txtf.Visible = True
            End If
        'End If
    End Sub

    Sub SeleccionarTodos()
        'If vNuevo = True Then
        If ChkTodos.Checked = True Then
                ChkFamilia.Checked = False
                cbxFamilia.cbx.Enabled = False
                ChkProducto.Checked = False
                lklAgregar.Enabled = False
                ChkPresentacion.Checked = False
                cbxPresentacion.cbx.Enabled = False
            Else
                cbxFamilia.cbx.Enabled = False
            End If
        'End If
    End Sub

    Sub Procesar(ByVal Operacion As String)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", txtID.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroAcuerdo", txtNroAcuerdo.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NC", rbNC.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Factura", rbFactura.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)
        If rbNC.Checked = True Then
            'CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.Registro("ID").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.txtID.ObtenerValor, ParameterDirection.Input)
            If chkTSucursales.Checked = False Then
                CSistema.SetSQLParameter(param, "@IDSucursalCliente", IdSucursalCliente, ParameterDirection.Input)
            Else
                CSistema.SetSQLParameter(param, "@IDSucursalCliente", "", ParameterDirection.Input)
            End If
            CSistema.SetSQLParameter(param, "@IDProveedor", "", ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@IDCliente", "", ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDSucursalCliente", "", ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.Registro("ID").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.txtID.ObtenerValor, ParameterDirection.Input)
        End If
        CSistema.SetSQLParameter(param, "@TSucursales", chkTSucursales.Checked, ParameterDirection.Input)
        If ChkPorcentaje.Checked = True Then
            CSistema.SetSQLParameter(param, "@Porcentaje", txtPorcentaje.txt.Text, ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Porcentaje", "0", ParameterDirection.Input)
        End If
        If ChkImporte.Checked = True Then
            CSistema.SetSQLParameter(param, "@Importe", txtImporte.txt.Text, ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Importe", "0", ParameterDirection.Input)
        End If
        CSistema.SetSQLParameter(param, "@VigenciaDesde", CSistema.FormatoFechaBaseDatos(txtVigenciaDesde.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@VigenciaHasta", CSistema.FormatoFechaBaseDatos(txtVigenciaHasta.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMotivoAcuerdo", cbxMotivoAcuerdo.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        If ChkPresentacion.Checked = True Then
            CSistema.SetSQLParameter(param, "@IDPresentacion", cbxPresentacion.cbx.SelectedValue, ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@IDPresentacion", "", ParameterDirection.Input)
        End If
        If ChkFamilia.Checked = True Then
            CSistema.SetSQLParameter(param, "@IDSublinea", cbxFamilia.cbx.SelectedValue, ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@IDSublinea", "", ParameterDirection.Input)
        End If
        CSistema.SetSQLParameter(param, "@TProductos", ChkTodos.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rbActivado.Checked, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion, ParameterDirection.Input)
        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpAcuerdodeClientes", False, False, MensajeRetorno, "", False) = True Then
            MessageBox.Show(MensajeRetorno, "Informe", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, New Button, vControles)
        Else
            MessageBox.Show(MensajeRetorno, "Informe", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        Insertar = True

        IDTransaccion = txtID.txt.Text

        'If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
        Insertar = InsertarDetalle(IDTransaccion)
        'ElseIf Operacion <> ERP.CSistema.NUMOperacionesRegistro.UPD Then
        '    Eliminar = EliminarDetalle(IDTransaccion)
        '    Insertar = InsertarDetalle(IDTransaccion)
        'End If
        Inicializar()

    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmCategoriaCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()

    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnGuardar, btnCancelar, New Button, vControles)
        vNuevo = True
        Nuevo()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click

        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, New Button, vControles)

        vNuevo = False

        'Para inhabilitar botones más adelante 12/12/2023
        'rbNC.Enabled = False
        'rbFactura.Enabled = False
        'txtFecha.Enabled = False
        'txtNroAcuerdo.SoloLectura = True
        'txtCliente.txtID.SoloLectura = True
        'txtCliente.txtRazonSocial.SoloLectura = True
        'txtCliente.txtRUC.SoloLectura = True
        'txtProveedor.txtID.SoloLectura = True
        'txtProveedor.txtRazonSocial.SoloLectura = True
        'txtProveedor.txtRUC.SoloLectura = True

        txtCliente.Focus()
        txtProveedor.Focus()

        'txtObservacion.Focus()

        ComportamientoProducto()
        ComportamientoPresentacion()
        ComportamientoFamilia()
        ComportamientoNuevo()
        HabilitarImporte()
        HabilitarPorcentaje()

        'Foco
        'txtNombres.Focus()
        'txtNombres.txt.SelectAll()
    End Sub

    '''Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
    '''    'Modificar()

    '''    Dim frm As New frmModificarAcuerdo
    '''    frm.Acuerdo = txtNroAcuerdo.txt.Text
    '''    frm.MotivoAcuerdo = cbxMotivoAcuerdo.cbx.Text
    '''    frm.VigenciaDesde = txtVigenciaDesde.txt.Text
    '''    frm.VigenciaHasta = txtVigenciaHasta.txt.Text
    '''    frm.Porcentaje = txtPorcentaje.txt.Text
    '''    frm.Importe = txtImporte.txt.Text
    '''    frm.Observacion = txtObservacion.txt.Text
    '''    frm.Presentacion = cbxPresentacion.cbx.SelectedValue
    '''    frm.Familia = cbxFamilia.cbx.SelectedValue
    '''    frm.Observacion = txtObservacion.txt.Text
    '''    If ChkFE.Checked = True Then
    '''        frm.ChkFE.Checked = True
    '''    Else
    '''        frm.ChkFE.Checked = False
    '''    End If
    '''    frm.IDTransaccion = IDTransaccion
    '''    'frm.IDOperacion = IDOperacion
    '''    'frm.Timbrado = txtTimbrado.txt.Text
    '''    'frm.VtoTimbrado = txtVtoTimbrado.txt.Text
    '''    ''frm.ChkFE.Checked = True
    '''    'If ChkFE.Checked = True Then
    '''    '    frm.ChkFE.Checked = True
    '''    'Else
    '''    '    frm.ChkFE.Checked = False
    '''    'End If
    '''    'frm.IDSucursal = cbxSucursal.cbx.SelectedValue

    '''    'If chkGastoMultiple.Checked = True Then
    '''    '    frm.GastosVarios = True
    '''    'Else
    '''    '    frm.GastosVarios = False
    '''    'End If

    '''    'frm.UnidadNegocio = UnidadNegocio
    '''    'frm.Departamento = Departamento

    '''    FGMostrarFormulario(Me, frm, "Modificar Acuerdo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False, False)
    '''    CargarOperacion()

    '''End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Select Case vNuevo
            Case True
                Procesar("INS")
            Case False
                Procesar("UPD")
        End Select
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, New Button, vControles)
        vNuevo = False
        Inicializar()
        CargarInformacion()
    End Sub

    Private Sub lklAgregar_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lklAgregar.LinkClicked
        CargarProducto()
    End Sub

    Private Sub lklEliminar_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lklEliminar.LinkClicked
        QuitarProducto()
    End Sub

    Sub CargarProducto()

        Dim Porcentaje As Integer

        'Validar Cliente
        'If txtCliente.Seleccionado = False Then
        If txtCliente.Seleccionado = False And rbNC.Checked = True Then
            MsgBox("Selecione un cliente", vbExclamation)
            Exit Sub
        End If

        If txtProveedor.Seleccionado = False And rbFactura.Checked = True Then
            MsgBox("Selecione un Proveedor", vbExclamation)
            Exit Sub
        End If

        Dim frm As New frmCargarProductoAcuerdo

        FGMostrarFormulario(Me, frm, "Cargar detalle", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)
        Porcentaje = frm.Porcentaje


        If frm.Procesado = False Then
            Exit Sub
        End If

        'Cargamos el registro en el detalle
        Dim dRow As DataRow = dtDetalle.NewRow()


        'Obtener Valores
        'Productos
        dRow("IDProducto") = frm.Registro("ID").ToString
        dRow("Referencia") = frm.Registro("Referencia").ToString
        dRow("Descripcion") = frm.Registro("Descripcion").ToString
        dRow("Porcentaje") = Porcentaje


        'Agregamos al detalle
        dtDetalle.Rows.Add(dRow)

        Listardetalle()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Sub QuitarProducto()

        'If vNuevo = False Then
        '    Exit Sub
        'End If

        'Validar
        If lvlista.SelectedItems.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(lvlista, mensaje)
            ctrError.SetIconAlignment(lvlista, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Quitamos de la lista
        dtDetalle.Rows.RemoveAt(lvlista.SelectedItems(0).Index)
        Listardetalle()

    End Sub

    Sub Listardetalle()

        'Limpiamos todo el detalle
        lvlista.Items.Clear()

        'Variables
        Dim Total As Decimal = 0

        'Cargamos registro por registro
        For Each oRow As DataRow In dtDetalle.Rows

            Dim item As ListViewItem = lvlista.Items.Add(oRow("IDProducto").ToString)

            item.SubItems.Add(oRow("Referencia").ToString)
            item.SubItems.Add(oRow("Descripcion").ToString)
            item.SubItems.Add(oRow("Porcentaje").ToString)

        Next

    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer) As Boolean
        InsertarDetalle = True

        Dim sqlDelete As String = ""
        sqlDelete = "delete from detalleacuerdodeclientes where IDAcuerdo = " & IDTransaccion

        'Ejecutar DetalleAcuerdo
        CSistema.ExecuteNonQuery(sqlDelete)



        Dim i As Integer = 0

        'Recorrer el detalle
        For Each oRow As DataRow In dtDetalle.Rows

            Dim sql As String = ""
            'sql = "Insert Into detalleacuerdodeclientes (IDAcuerdo, IDProducto) Values(" & IDTransaccion & "," & oRow("IDProducto").ToString & ")"
            sql = "Insert Into detalleacuerdodeclientes (IDAcuerdo, IDProducto, Porcentaje) Values(" & IDTransaccion & "," & oRow("IDProducto").ToString & " , " & oRow("Porcentaje").ToString & ")"

            'Ejecutar DetalleAcuerdo
            CSistema.ExecuteNonQuery(sql)

        Next

    End Function

    'Function EliminarDetalle(ByVal IDTransaccion As Integer) As Boolean
    '    EliminarDetalle = True

    '    Dim i As Integer = 0

    '    'Recorrer el detalle
    '    For Each oRow As DataRow In dtDetalle.Rows

    '        Dim sql As String = ""
    '        sql = "delete from detalleacuerdodeclientes where " & IDTransaccion & ""

    '        'Ejecutar DetalleAcuerdo
    '        CSistema.ExecuteNonQuery(sql)

    '    Next

    'End Function

    'Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

    '    CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnEditar, btnGuardar, btnCancelar, New Button, New Button, New Button, vControles, New Button)

    'End Sub

    Private Sub cbxMotivoAcuerdo_PropertyChanged(sender As Object, e As EventArgs) Handles cbxMotivoAcuerdo.PropertyChanged
        ComportamientoNuevo()
    End Sub

    Sub ComportamientoNuevo()
        If cbxMotivoAcuerdo.cbx.SelectedValue = 7 And rbFactura.Checked = True Then
            ChkProducto.Enabled = True
        ElseIf rbNC.Checked = True Then
            ChkProducto.Enabled = True
        Else
            ChkProducto.Enabled = False
            If vNuevo = True Then
            End If
        End If
    End Sub
End Class
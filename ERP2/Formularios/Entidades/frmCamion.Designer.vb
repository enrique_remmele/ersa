﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCamion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblCapacidad = New System.Windows.Forms.Label()
        Me.lblChasis = New System.Windows.Forms.Label()
        Me.lblPatente = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lblToneladas = New System.Windows.Forms.Label()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblNroVehiculo = New System.Windows.Forms.Label()
        Me.lblMarca = New System.Windows.Forms.Label()
        Me.txtMarca = New ERP.ocxTXTString()
        Me.txtNrovehiculo = New ERP.ocxTXTString()
        Me.chkVerEnAbastecimiento = New ERP.ocxCHK()
        Me.txtCapacidad = New ERP.ocxTXTNumeric()
        Me.txtChasis = New ERP.ocxTXTString()
        Me.txtPatente = New ERP.ocxTXTString()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTNumeric()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(422, 431)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 20
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(179, 190)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 13
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(98, 190)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 12
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(12, 192)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 11
        Me.lblEstado.Text = "Estado:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(341, 213)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 17
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(422, 213)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 18
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(260, 213)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 16
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(179, 213)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 15
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(98, 213)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 14
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(12, 35)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(75, 13)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Text = "Tipo Vehiculo:"
        '
        'lblCapacidad
        '
        Me.lblCapacidad.AutoSize = True
        Me.lblCapacidad.Location = New System.Drawing.Point(12, 168)
        Me.lblCapacidad.Name = "lblCapacidad"
        Me.lblCapacidad.Size = New System.Drawing.Size(61, 13)
        Me.lblCapacidad.TabIndex = 8
        Me.lblCapacidad.Text = "Capacidad:"
        '
        'lblChasis
        '
        Me.lblChasis.AutoSize = True
        Me.lblChasis.Location = New System.Drawing.Point(12, 142)
        Me.lblChasis.Name = "lblChasis"
        Me.lblChasis.Size = New System.Drawing.Size(38, 13)
        Me.lblChasis.TabIndex = 6
        Me.lblChasis.Text = "Chasis"
        '
        'lblPatente
        '
        Me.lblPatente.AutoSize = True
        Me.lblPatente.Location = New System.Drawing.Point(12, 116)
        Me.lblPatente.Name = "lblPatente"
        Me.lblPatente.Size = New System.Drawing.Size(47, 13)
        Me.lblPatente.TabIndex = 4
        Me.lblPatente.Text = "Patente:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(12, 9)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'lblToneladas
        '
        Me.lblToneladas.AutoSize = True
        Me.lblToneladas.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToneladas.Location = New System.Drawing.Point(167, 168)
        Me.lblToneladas.Name = "lblToneladas"
        Me.lblToneladas.Size = New System.Drawing.Size(45, 12)
        Me.lblToneladas.TabIndex = 10
        Me.lblToneladas.Text = "toneladas"
        '
        'dgv
        '
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(15, 242)
        Me.dgv.Name = "dgv"
        Me.dgv.Size = New System.Drawing.Size(626, 183)
        Me.dgv.TabIndex = 102
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 463)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(653, 22)
        Me.StatusStrip1.TabIndex = 105
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'lblNroVehiculo
        '
        Me.lblNroVehiculo.AutoSize = True
        Me.lblNroVehiculo.Location = New System.Drawing.Point(12, 62)
        Me.lblNroVehiculo.Name = "lblNroVehiculo"
        Me.lblNroVehiculo.Size = New System.Drawing.Size(73, 13)
        Me.lblNroVehiculo.TabIndex = 106
        Me.lblNroVehiculo.Text = "Nro Vehículo:"
        '
        'lblMarca
        '
        Me.lblMarca.AutoSize = True
        Me.lblMarca.Location = New System.Drawing.Point(12, 87)
        Me.lblMarca.Name = "lblMarca"
        Me.lblMarca.Size = New System.Drawing.Size(43, 13)
        Me.lblMarca.TabIndex = 110
        Me.lblMarca.Text = "Marca :"
        '
        'txtMarca
        '
        Me.txtMarca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMarca.Color = System.Drawing.Color.Empty
        Me.txtMarca.Indicaciones = Nothing
        Me.txtMarca.Location = New System.Drawing.Point(98, 85)
        Me.txtMarca.Multilinea = False
        Me.txtMarca.Name = "txtMarca"
        Me.txtMarca.Size = New System.Drawing.Size(276, 21)
        Me.txtMarca.SoloLectura = False
        Me.txtMarca.TabIndex = 109
        Me.txtMarca.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMarca.Texto = ""
        '
        'txtNrovehiculo
        '
        Me.txtNrovehiculo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNrovehiculo.Color = System.Drawing.Color.Empty
        Me.txtNrovehiculo.Indicaciones = Nothing
        Me.txtNrovehiculo.Location = New System.Drawing.Point(98, 58)
        Me.txtNrovehiculo.Multilinea = False
        Me.txtNrovehiculo.Name = "txtNrovehiculo"
        Me.txtNrovehiculo.Size = New System.Drawing.Size(276, 21)
        Me.txtNrovehiculo.SoloLectura = False
        Me.txtNrovehiculo.TabIndex = 107
        Me.txtNrovehiculo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNrovehiculo.Texto = ""
        '
        'chkVerEnAbastecimiento
        '
        Me.chkVerEnAbastecimiento.BackColor = System.Drawing.Color.Transparent
        Me.chkVerEnAbastecimiento.Color = System.Drawing.Color.Empty
        Me.chkVerEnAbastecimiento.Location = New System.Drawing.Point(422, 163)
        Me.chkVerEnAbastecimiento.Name = "chkVerEnAbastecimiento"
        Me.chkVerEnAbastecimiento.Size = New System.Drawing.Size(136, 21)
        Me.chkVerEnAbastecimiento.SoloLectura = False
        Me.chkVerEnAbastecimiento.TabIndex = 101
        Me.chkVerEnAbastecimiento.Texto = "Ver En Abastecimiento"
        Me.chkVerEnAbastecimiento.Valor = False
        '
        'txtCapacidad
        '
        Me.txtCapacidad.Color = System.Drawing.Color.Empty
        Me.txtCapacidad.Decimales = True
        Me.txtCapacidad.Indicaciones = Nothing
        Me.txtCapacidad.Location = New System.Drawing.Point(98, 162)
        Me.txtCapacidad.Name = "txtCapacidad"
        Me.txtCapacidad.Size = New System.Drawing.Size(63, 22)
        Me.txtCapacidad.SoloLectura = False
        Me.txtCapacidad.TabIndex = 9
        Me.txtCapacidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCapacidad.Texto = "0"
        '
        'txtChasis
        '
        Me.txtChasis.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtChasis.Color = System.Drawing.Color.Empty
        Me.txtChasis.Indicaciones = Nothing
        Me.txtChasis.Location = New System.Drawing.Point(98, 138)
        Me.txtChasis.Multilinea = False
        Me.txtChasis.Name = "txtChasis"
        Me.txtChasis.Size = New System.Drawing.Size(276, 21)
        Me.txtChasis.SoloLectura = False
        Me.txtChasis.TabIndex = 7
        Me.txtChasis.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtChasis.Texto = ""
        '
        'txtPatente
        '
        Me.txtPatente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPatente.Color = System.Drawing.Color.Empty
        Me.txtPatente.Indicaciones = Nothing
        Me.txtPatente.Location = New System.Drawing.Point(98, 112)
        Me.txtPatente.Multilinea = False
        Me.txtPatente.Name = "txtPatente"
        Me.txtPatente.Size = New System.Drawing.Size(120, 21)
        Me.txtPatente.SoloLectura = False
        Me.txtPatente.TabIndex = 5
        Me.txtPatente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPatente.Texto = ""
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(98, 31)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(276, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(98, 4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 1
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'frmCamion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(653, 485)
        Me.Controls.Add(Me.lblMarca)
        Me.Controls.Add(Me.txtMarca)
        Me.Controls.Add(Me.lblNroVehiculo)
        Me.Controls.Add(Me.txtNrovehiculo)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.chkVerEnAbastecimiento)
        Me.Controls.Add(Me.lblToneladas)
        Me.Controls.Add(Me.txtCapacidad)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.rdbDesactivado)
        Me.Controls.Add(Me.rdbActivo)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.lblCapacidad)
        Me.Controls.Add(Me.txtChasis)
        Me.Controls.Add(Me.lblChasis)
        Me.Controls.Add(Me.txtPatente)
        Me.Controls.Add(Me.lblPatente)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.lblID)
        Me.Name = "frmCamion"
        Me.Tag = "CAMION"
        Me.Text = "frmCamion"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents rdbDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbActivo As System.Windows.Forms.RadioButton
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblCapacidad As System.Windows.Forms.Label
    Friend WithEvents txtChasis As ERP.ocxTXTString
    Friend WithEvents lblChasis As System.Windows.Forms.Label
    Friend WithEvents txtPatente As ERP.ocxTXTString
    Friend WithEvents lblPatente As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtCapacidad As ERP.ocxTXTNumeric
    Friend WithEvents lblToneladas As System.Windows.Forms.Label
    Friend WithEvents chkVerEnAbastecimiento As ERP.ocxCHK
    Friend WithEvents dgv As DataGridView
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents txtMarca As ocxTXTString
    Friend WithEvents lblNroVehiculo As Label
    Friend WithEvents txtNrovehiculo As ocxTXTString
    Friend WithEvents lblMarca As Label
End Class

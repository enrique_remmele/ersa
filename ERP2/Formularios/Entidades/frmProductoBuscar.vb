﻿Public Class frmProductoBuscar

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'PROPIEDADES
    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property
    Public Property IDDeposito() As Integer


    Public Property Consulta() As String

    Public Property ConExistencia() As Boolean = True
 

    Private ColumnasNumericasValue As String()
    Public Property ColumnasNumericas() As String()
        Get
            Return ColumnasNumericasValue
        End Get
        Set(ByVal value As String())
            ColumnasNumericasValue = value
        End Set
    End Property

    Private ControlarExistenciaValue As Boolean
    Public Property ControlarExistencia() As Boolean
        Get
            Return ControlarExistenciaValue
        End Get
        Set(ByVal value As Boolean)
            ControlarExistenciaValue = value
        End Set
    End Property

    'VARIABLES

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'FUNCIONES
    'Listar
    Sub Listar()

        If Consulta = "" Or chkExistencia.Valor = False Then
            Consulta = "Select ID, Ref, Descripcion, 'Codigo de Barra'=CodigoBarra, 'Tipo de Producto'=TipoProducto From VProducto Where ID>0 "
        End If

        Dim Where As String = ""
        Dim OrderBy As String = ""

        If rdbDescripcion.Checked = True Then
            Where = " And Descripcion Like '%" & txtDescripcion.Text.Trim & "%' "
            OrderBy = " Order By Descripcion"
        End If

        If rdbCodigoBarra.Checked = True Then
            Where = " And CodigoBarra Like '%" & txtDescripcion.Text.Trim & "%' "
            OrderBy = " Order By CodigoBarra"
        End If

        If rdbReferencia.Checked = True Then
            Where = " And Ref Like '%" & txtDescripcion.Text.Trim & "%' "
            OrderBy = " Order By Ref"
        End If

        If rdbID.Checked = True Then
            Where = " And ID Like '%" & txtDescripcion.Text.Trim & "%' "
            OrderBy = " Order By ID"
        End If

        If chkActivo.chk.Checked = True Then
            'Filtrar por Fecha
            Where = Where & " And Estado='True' "
        End If

        If chkExistencia.chk.Checked = True Then
            Where = Where & " And (Select dbo.FExistenciaProducto(ID, " & IDDeposito & ")) > 0 "
        End If

        If chkVendible.chk.Checked = True Then
            'Filtrar por Fecha
            Where = Where & " And Vendible='True' "
        End If

        CSistema.dtToGrid(dgw, CSistema.ExecuteToDataTable(Consulta & Where & OrderBy, "", 15))

        Try
            'Formato
            dgw.Columns(0).Visible = False
            dgw.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

            If Not ColumnasNumericas Is Nothing Then
                For i As Integer = 0 To ColumnasNumericas.GetLength(0) - 1
                    dgw.Columns(ColumnasNumericas(i)).DefaultCellStyle.Format = "N0"
                    dgw.Columns(ColumnasNumericas(i)).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                Next
            End If
        Catch ex As Exception

        End Try

        tsslCantidad.Text = dgw.RowCount

    End Sub

    'Guardar Informacion
    Sub GuardarInformacion()

        Dim Seleccion As Integer = 1

        If rdbDescripcion.Checked = True Then
            Seleccion = 1
        End If

        If rdbCodigoBarra.Checked = True Then
            Seleccion = 2
        End If

        If rdbReferencia.Checked = True Then
            Seleccion = 3
        End If

        If rdbID.Checked = True Then
            Seleccion = 4
        End If

        If rdbDescripcion.Checked = True Then
            Seleccion = 5
        End If

        'Seleccion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SELECCION", Seleccion)

    End Sub

    'Cargar Informacion
    Sub CargarInformacion()

        Me.AcceptButton = New Button

        'Seleccion
        Dim Seleccion As Integer = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SELECCION", "1")

        Select Case Seleccion

            Case 1
                rdbDescripcion.Checked = True
            Case 2
                rdbCodigoBarra.Checked = True
            Case 3
                rdbReferencia.Checked = True
            Case 4
                rdbID.Checked = True
            Case Else
                rdbDescripcion.Checked = True
        End Select

        chkActivo.chk.Checked = True
        chkExistencia.chk.Checked = ConExistencia
            'Foco
            txtDescripcion.Focus()
            txtDescripcion.SelectAll()

    End Sub

    'Seleccion
    Sub Seleccionar()

        If dgw.RowCount > 0 Then

            If ControlarExistencia = True Then
                ID = dgw.SelectedRows(0).Cells(0).Value

                Dim Existencia As Boolean = dgw.SelectedRows(0).Cells("ControlarExistencia").Value
                Dim Cantidad As Decimal = dgw.SelectedRows(0).Cells("Existencia").Value
                Dim Cantidad1 As Decimal = 1

                If Existencia = False Then
                    Cantidad = Cantidad1
                End If

                If Cantidad = 0 Then
                    MessageBox.Show("El producto no tiene existencia! No se puede procesar.", "Stock Insuficiente", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    txtDescripcion.SelectAll()
                    txtDescripcion.Focus()
                    Exit Sub
                End If

                Me.Visible = False
                RaiseEvent PropertyChanged(New Object, New EventArgs)
            Else
                ID = dgw.SelectedRows(0).Cells(0).Value
                Me.Visible = False
                RaiseEvent PropertyChanged(New Object, New EventArgs)
            End If


        End If

    End Sub

    Sub ExistenciaDeposito()

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim ID As Integer = dgw.SelectedRows(0).Cells("ID").Value

        Dim SQL As String = "Select Sucursal, Deposito, Existencia From VExistenciaDeposito Where IDProducto=" & ID & " Order By Sucursal"
        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        Dim frm As New Form
        frm.KeyPreview = True
        frm.Size = New Size(500, 200)

        Dim dgv As New DataGridView
        dgv.Name = "dgv"
        CSistema.dtToGrid(dgv, dt)
        frm.Controls.Add(dgv)
        dgv.Dock = DockStyle.Fill

        AddHandler frm.KeyUp, AddressOf FGCerrarFormulario
        AddHandler frm.Load, AddressOf InicializarExistenciaDeposito


        FGMostrarFormulario(Me, frm, "Existencia en Sucursales", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Sub InicializarExistenciaDeposito(ByVal sender As Object, ByVal e As System.EventArgs)

        
        For Each c As Control In sender.controls
            Dim dgv As DataGridView = CType(c, DataGridView)

            If dgv.Columns.Count > 0 Then

                'Formato
                dgv.Columns("Sucursal").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                dgv.Columns("Deposito").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

                dgv.Columns("Existencia").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                dgv.Columns("Existencia").DefaultCellStyle.Format = "N0"
                dgv.Columns("Existencia").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            End If

        Next

    End Sub

    Private Sub txtDescripcion_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDescripcion.KeyUp
        If e.KeyCode = Keys.Enter Then
            Listar()
        End If

    End Sub

    Private Sub frmProductoBuscar_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmProductoBuscar_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F2 Then

            If rdbDescripcion.Checked = True Then
                rdbCodigoBarra.Checked = True
                GoTo Siguiente
            End If

            If rdbCodigoBarra.Checked = True Then
                rdbReferencia.Checked = True
                GoTo Siguiente
            End If

            If rdbReferencia.Checked = True Then
                rdbID.Checked = True
                GoTo Siguiente
            End If

            If rdbID.Checked = True Then
                rdbDescripcion.Checked = True
                GoTo Siguiente
            End If

Siguiente:
            Listar()
            txtDescripcion.SelectAll()
            txtDescripcion.Focus()

        End If

        If e.KeyCode = Keys.Down Then
            If dgw.RowCount > 0 Then
                If dgw.SelectedRows.Count = 0 Then
                    dgw.SelectedRows(0).Selected = True
                End If

                dgw.Focus()
            Else
                txtDescripcion.SelectAll()
            End If
        End If

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Private Sub frmProductoBuscar_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CargarInformacion()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Listar()
    End Sub

    Private Sub lvlista_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Seleccionar()
    End Sub

    Private Sub lvlista_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            'Seleccionar()
        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub dgw_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgw.DoubleClick
        Seleccionar()
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Enter Then
            Seleccionar()
        End If

        If e.KeyCode = Keys.F1 Then
            ExistenciaDeposito()
        End If

    End Sub

    Private Sub ToolStripDropDownButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripDropDownButton1.Click
        ExistenciaDeposito()
    End Sub

End Class
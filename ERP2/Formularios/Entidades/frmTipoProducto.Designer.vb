﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTipoProducto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.lklVerArbol = New System.Windows.Forms.LinkLabel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.chkProducido = New ERP.ocxCHK()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCuentaContableProveedor = New ERP.ocxTXTCuentaContable()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblCuentaContableCosto = New System.Windows.Forms.Label()
        Me.lblCuentaContableCliente = New System.Windows.Forms.Label()
        Me.lblCuentaContableVenta = New System.Windows.Forms.Label()
        Me.txtCuentaContableCosto = New ERP.ocxTXTCuentaContable()
        Me.txtCuentaContableCliente = New ERP.ocxTXTCuentaContable()
        Me.txtCuentaContableVenta = New ERP.ocxTXTCuentaContable()
        Me.txtReferencia = New ERP.ocxTXTString()
        Me.lblReferencia = New System.Windows.Forms.Label()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 605)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(535, 22)
        Me.StatusStrip1.TabIndex = 23
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(354, 3)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 22
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'lvLista
        '
        Me.lvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvLista.Location = New System.Drawing.Point(106, 328)
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(426, 247)
        Me.lvLista.TabIndex = 20
        Me.lvLista.UseCompatibleStateImageBehavior = False
        '
        'lklVerArbol
        '
        Me.lklVerArbol.AutoSize = True
        Me.lklVerArbol.Location = New System.Drawing.Point(0, 0)
        Me.lklVerArbol.Name = "lklVerArbol"
        Me.lklVerArbol.Size = New System.Drawing.Size(64, 13)
        Me.lklVerArbol.TabIndex = 21
        Me.lklVerArbol.TabStop = True
        Me.lklVerArbol.Text = "Ver en arbol"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 432.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lvLista, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 1, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 325.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(535, 605)
        Me.TableLayoutPanel1.TabIndex = 27
        '
        'Panel1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.Panel1, 2)
        Me.Panel1.Controls.Add(Me.cbxUnidadNegocio)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.LinkLabel2)
        Me.Panel1.Controls.Add(Me.chkProducido)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txtCuentaContableProveedor)
        Me.Panel1.Controls.Add(Me.LinkLabel1)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.lblCuentaContableCosto)
        Me.Panel1.Controls.Add(Me.lblCuentaContableCliente)
        Me.Panel1.Controls.Add(Me.lblCuentaContableVenta)
        Me.Panel1.Controls.Add(Me.txtCuentaContableCosto)
        Me.Panel1.Controls.Add(Me.txtCuentaContableCliente)
        Me.Panel1.Controls.Add(Me.txtCuentaContableVenta)
        Me.Panel1.Controls.Add(Me.txtReferencia)
        Me.Panel1.Controls.Add(Me.lblReferencia)
        Me.Panel1.Controls.Add(Me.txtDescripcion)
        Me.Panel1.Controls.Add(Me.txtID)
        Me.Panel1.Controls.Add(Me.rdbDesactivado)
        Me.Panel1.Controls.Add(Me.rdbActivo)
        Me.Panel1.Controls.Add(Me.lblEstado)
        Me.Panel1.Controls.Add(Me.btnCancelar)
        Me.Panel1.Controls.Add(Me.btnEliminar)
        Me.Panel1.Controls.Add(Me.btnGuardar)
        Me.Panel1.Controls.Add(Me.btnEditar)
        Me.Panel1.Controls.Add(Me.btnNuevo)
        Me.Panel1.Controls.Add(Me.lblDescripcion)
        Me.Panel1.Controls.Add(Me.lblID)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(535, 325)
        Me.Panel1.TabIndex = 21
        '
        'LinkLabel2
        '
        Me.LinkLabel2.AutoSize = True
        Me.LinkLabel2.Location = New System.Drawing.Point(279, 242)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(222, 13)
        Me.LinkLabel2.TabIndex = 52
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "Administrar Cuentas Obligatorias por Producto"
        '
        'chkProducido
        '
        Me.chkProducido.BackColor = System.Drawing.Color.Transparent
        Me.chkProducido.Color = System.Drawing.Color.Empty
        Me.chkProducido.Location = New System.Drawing.Point(238, 64)
        Me.chkProducido.Name = "chkProducido"
        Me.chkProducido.Size = New System.Drawing.Size(166, 21)
        Me.chkProducido.SoloLectura = False
        Me.chkProducido.TabIndex = 51
        Me.chkProducido.Texto = "Tipo de Producto Producido"
        Me.chkProducido.Valor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 187)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 13)
        Me.Label2.TabIndex = 49
        Me.Label2.Text = "C.C. Proveedor:"
        '
        'txtCuentaContableProveedor
        '
        Me.txtCuentaContableProveedor.AlturaMaxima = 255
        Me.txtCuentaContableProveedor.Consulta = Nothing
        Me.txtCuentaContableProveedor.IDCentroCosto = 0
        Me.txtCuentaContableProveedor.IDUnidadNegocio = 0
        Me.txtCuentaContableProveedor.ListarTodas = False
        Me.txtCuentaContableProveedor.Location = New System.Drawing.Point(118, 181)
        Me.txtCuentaContableProveedor.Name = "txtCuentaContableProveedor"
        Me.txtCuentaContableProveedor.Registro = Nothing
        Me.txtCuentaContableProveedor.Resolucion173 = False
        Me.txtCuentaContableProveedor.Seleccionado = False
        Me.txtCuentaContableProveedor.Size = New System.Drawing.Size(399, 24)
        Me.txtCuentaContableProveedor.SoloLectura = False
        Me.txtCuentaContableProveedor.TabIndex = 50
        Me.txtCuentaContableProveedor.Texto = Nothing
        Me.txtCuentaContableProveedor.whereFiltro = Nothing
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(106, 242)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(58, 13)
        Me.LinkLabel1.TabIndex = 48
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Administrar"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 242)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 13)
        Me.Label1.TabIndex = 47
        Me.Label1.Text = "C.C. Motivo NC:"
        '
        'lblCuentaContableCosto
        '
        Me.lblCuentaContableCosto.AutoSize = True
        Me.lblCuentaContableCosto.Location = New System.Drawing.Point(10, 157)
        Me.lblCuentaContableCosto.Name = "lblCuentaContableCosto"
        Me.lblCuentaContableCosto.Size = New System.Drawing.Size(90, 13)
        Me.lblCuentaContableCosto.TabIndex = 37
        Me.lblCuentaContableCosto.Text = "C.C. Costo Merc.:"
        '
        'lblCuentaContableCliente
        '
        Me.lblCuentaContableCliente.AutoSize = True
        Me.lblCuentaContableCliente.Location = New System.Drawing.Point(10, 127)
        Me.lblCuentaContableCliente.Name = "lblCuentaContableCliente"
        Me.lblCuentaContableCliente.Size = New System.Drawing.Size(79, 13)
        Me.lblCuentaContableCliente.TabIndex = 35
        Me.lblCuentaContableCliente.Text = "C.C. Deudores:"
        '
        'lblCuentaContableVenta
        '
        Me.lblCuentaContableVenta.AutoSize = True
        Me.lblCuentaContableVenta.Location = New System.Drawing.Point(10, 97)
        Me.lblCuentaContableVenta.Name = "lblCuentaContableVenta"
        Me.lblCuentaContableVenta.Size = New System.Drawing.Size(61, 13)
        Me.lblCuentaContableVenta.TabIndex = 33
        Me.lblCuentaContableVenta.Text = "C.C. Venta:"
        '
        'txtCuentaContableCosto
        '
        Me.txtCuentaContableCosto.AlturaMaxima = 255
        Me.txtCuentaContableCosto.Consulta = Nothing
        Me.txtCuentaContableCosto.IDCentroCosto = 0
        Me.txtCuentaContableCosto.IDUnidadNegocio = 0
        Me.txtCuentaContableCosto.ListarTodas = False
        Me.txtCuentaContableCosto.Location = New System.Drawing.Point(118, 151)
        Me.txtCuentaContableCosto.Name = "txtCuentaContableCosto"
        Me.txtCuentaContableCosto.Registro = Nothing
        Me.txtCuentaContableCosto.Resolucion173 = False
        Me.txtCuentaContableCosto.Seleccionado = False
        Me.txtCuentaContableCosto.Size = New System.Drawing.Size(399, 24)
        Me.txtCuentaContableCosto.SoloLectura = False
        Me.txtCuentaContableCosto.TabIndex = 38
        Me.txtCuentaContableCosto.Texto = Nothing
        Me.txtCuentaContableCosto.whereFiltro = Nothing
        '
        'txtCuentaContableCliente
        '
        Me.txtCuentaContableCliente.AlturaMaxima = 255
        Me.txtCuentaContableCliente.Consulta = Nothing
        Me.txtCuentaContableCliente.IDCentroCosto = 0
        Me.txtCuentaContableCliente.IDUnidadNegocio = 0
        Me.txtCuentaContableCliente.ListarTodas = False
        Me.txtCuentaContableCliente.Location = New System.Drawing.Point(118, 121)
        Me.txtCuentaContableCliente.Name = "txtCuentaContableCliente"
        Me.txtCuentaContableCliente.Registro = Nothing
        Me.txtCuentaContableCliente.Resolucion173 = False
        Me.txtCuentaContableCliente.Seleccionado = False
        Me.txtCuentaContableCliente.Size = New System.Drawing.Size(399, 24)
        Me.txtCuentaContableCliente.SoloLectura = False
        Me.txtCuentaContableCliente.TabIndex = 36
        Me.txtCuentaContableCliente.Texto = Nothing
        Me.txtCuentaContableCliente.whereFiltro = Nothing
        '
        'txtCuentaContableVenta
        '
        Me.txtCuentaContableVenta.AlturaMaxima = 255
        Me.txtCuentaContableVenta.Consulta = Nothing
        Me.txtCuentaContableVenta.IDCentroCosto = 0
        Me.txtCuentaContableVenta.IDUnidadNegocio = 0
        Me.txtCuentaContableVenta.ListarTodas = False
        Me.txtCuentaContableVenta.Location = New System.Drawing.Point(118, 91)
        Me.txtCuentaContableVenta.Name = "txtCuentaContableVenta"
        Me.txtCuentaContableVenta.Registro = Nothing
        Me.txtCuentaContableVenta.Resolucion173 = False
        Me.txtCuentaContableVenta.Seleccionado = False
        Me.txtCuentaContableVenta.Size = New System.Drawing.Size(399, 24)
        Me.txtCuentaContableVenta.SoloLectura = False
        Me.txtCuentaContableVenta.TabIndex = 34
        Me.txtCuentaContableVenta.Texto = Nothing
        Me.txtCuentaContableVenta.whereFiltro = Nothing
        '
        'txtReferencia
        '
        Me.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferencia.Color = System.Drawing.Color.Empty
        Me.txtReferencia.Indicaciones = Nothing
        Me.txtReferencia.Location = New System.Drawing.Point(118, 64)
        Me.txtReferencia.Multilinea = False
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Size = New System.Drawing.Size(107, 21)
        Me.txtReferencia.SoloLectura = False
        Me.txtReferencia.TabIndex = 32
        Me.txtReferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtReferencia.Texto = ""
        '
        'lblReferencia
        '
        Me.lblReferencia.AutoSize = True
        Me.lblReferencia.Location = New System.Drawing.Point(11, 68)
        Me.lblReferencia.Name = "lblReferencia"
        Me.lblReferencia.Size = New System.Drawing.Size(62, 13)
        Me.lblReferencia.TabIndex = 31
        Me.lblReferencia.Text = "Referencia:"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(118, 37)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(237, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 30
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(118, 10)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 28
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(164, 269)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(63, 17)
        Me.rdbDesactivado.TabIndex = 41
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Inactivo"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(103, 269)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 40
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(11, 271)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 39
        Me.lblEstado.Text = "Estado:"
        '
        'btnCancelar
        '
        Me.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancelar.Location = New System.Drawing.Point(346, 292)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 45
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(427, 292)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 46
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(265, 292)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 44
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(184, 292)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 43
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(103, 292)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 42
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(11, 41)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 29
        Me.lblDescripcion.Text = "Descripción:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(11, 15)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 27
        Me.lblID.Text = "ID:"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lklVerArbol)
        Me.Panel2.Controls.Add(Me.btnSalir)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(103, 578)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(432, 27)
        Me.Panel2.TabIndex = 22
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 214)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(102, 13)
        Me.Label3.TabIndex = 53
        Me.Label3.Text = "Unidad de Negocio:"
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = Nothing
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = Nothing
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = Nothing
        Me.cbxUnidadNegocio.DataSource = Nothing
        Me.cbxUnidadNegocio.DataValueMember = Nothing
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(118, 211)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(237, 23)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 54
        Me.cbxUnidadNegocio.Texto = ""
        '
        'frmTipoProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(535, 627)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmTipoProducto"
        Me.Tag = "TIPO DE PRODUCTOS"
        Me.Text = "frmTipoProducto"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents lvLista As System.Windows.Forms.ListView
    Friend WithEvents lklVerArbol As System.Windows.Forms.LinkLabel
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents txtCuentaContableProveedor As ocxTXTCuentaContable
    Friend WithEvents LinkLabel1 As LinkLabel
    Friend WithEvents Label1 As Label
    Friend WithEvents lblCuentaContableCosto As Label
    Friend WithEvents lblCuentaContableCliente As Label
    Friend WithEvents lblCuentaContableVenta As Label
    Friend WithEvents txtCuentaContableCosto As ocxTXTCuentaContable
    Friend WithEvents txtCuentaContableCliente As ocxTXTCuentaContable
    Friend WithEvents txtCuentaContableVenta As ocxTXTCuentaContable
    Friend WithEvents txtReferencia As ocxTXTString
    Friend WithEvents lblReferencia As Label
    Friend WithEvents txtDescripcion As ocxTXTString
    Friend WithEvents txtID As ocxTXTNumeric
    Friend WithEvents rdbDesactivado As RadioButton
    Friend WithEvents rdbActivo As RadioButton
    Friend WithEvents lblEstado As Label
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnEliminar As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnEditar As Button
    Friend WithEvents btnNuevo As Button
    Friend WithEvents lblDescripcion As Label
    Friend WithEvents lblID As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents chkProducido As ocxCHK
    Friend WithEvents LinkLabel2 As LinkLabel
    Friend WithEvents cbxUnidadNegocio As ocxCBX
    Friend WithEvents Label3 As Label
End Class

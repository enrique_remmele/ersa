﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFondoFijoCuentaContable
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblFF = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.ocxCuenta = New ERP.ocxTXTCuentaContable()
        Me.cbxGrupo = New ERP.ocxCBX()
        Me.dgv = New System.Windows.Forms.DataGridView()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'lblFF
        '
        Me.lblFF.AutoSize = True
        Me.lblFF.Location = New System.Drawing.Point(11, 15)
        Me.lblFF.Name = "lblFF"
        Me.lblFF.Size = New System.Drawing.Size(59, 13)
        Me.lblFF.TabIndex = 21
        Me.lblFF.Text = "Fondo Fijo:"
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(414, 338)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 32
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(132, 115)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 25
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(71, 115)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 24
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 373)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(530, 22)
        Me.StatusStrip1.TabIndex = 33
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(12, 117)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 23
        Me.lblEstado.Text = "Estado:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(324, 138)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 29
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(414, 138)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 30
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(237, 138)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 28
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(152, 138)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 27
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(68, 138)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 26
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(11, 42)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 35
        Me.lblCuenta.Text = "Cuenta:"
        '
        'ocxCuenta
        '
        Me.ocxCuenta.AlturaMaxima = 84
        Me.ocxCuenta.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
        Me.ocxCuenta.Consulta = Nothing
        Me.ocxCuenta.IDCentroCosto = 0
        Me.ocxCuenta.IDUnidadNegocio = 0
        Me.ocxCuenta.ListarTodas = False
        Me.ocxCuenta.Location = New System.Drawing.Point(77, 42)
        Me.ocxCuenta.Name = "ocxCuenta"
        Me.ocxCuenta.Registro = Nothing
        Me.ocxCuenta.Resolucion173 = False
        Me.ocxCuenta.Seleccionado = False
        Me.ocxCuenta.Size = New System.Drawing.Size(430, 67)
        Me.ocxCuenta.SoloLectura = False
        Me.ocxCuenta.TabIndex = 34
        Me.ocxCuenta.Texto = Nothing
        Me.ocxCuenta.whereFiltro = Nothing
        '
        'cbxGrupo
        '
        Me.cbxGrupo.CampoWhere = Nothing
        Me.cbxGrupo.CargarUnaSolaVez = False
        Me.cbxGrupo.DataDisplayMember = "Descripcion"
        Me.cbxGrupo.DataFilter = Nothing
        Me.cbxGrupo.DataOrderBy = "ID"
        Me.cbxGrupo.DataSource = "VGrupo"
        Me.cbxGrupo.DataValueMember = "ID"
        Me.cbxGrupo.dtSeleccionado = Nothing
        Me.cbxGrupo.FormABM = Nothing
        Me.cbxGrupo.Indicaciones = Nothing
        Me.cbxGrupo.Location = New System.Drawing.Point(77, 15)
        Me.cbxGrupo.Name = "cbxGrupo"
        Me.cbxGrupo.SeleccionMultiple = False
        Me.cbxGrupo.SeleccionObligatoria = False
        Me.cbxGrupo.Size = New System.Drawing.Size(237, 21)
        Me.cbxGrupo.SoloLectura = False
        Me.cbxGrupo.TabIndex = 22
        Me.cbxGrupo.Texto = ""
        '
        'dgv
        '
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(68, 167)
        Me.dgv.Name = "dgv"
        Me.dgv.Size = New System.Drawing.Size(421, 165)
        Me.dgv.TabIndex = 101
        '
        'frmFondoFijoCuentaContable
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(530, 395)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.ocxCuenta)
        Me.Controls.Add(Me.lblCuenta)
        Me.Controls.Add(Me.cbxGrupo)
        Me.Controls.Add(Me.lblFF)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.rdbDesactivado)
        Me.Controls.Add(Me.rdbActivo)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Name = "frmFondoFijoCuentaContable"
        Me.Tag = "frmFondoFijoCuentaContable"
        Me.Text = "frmFondoFijoCuentaContable"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxGrupo As ERP.ocxCBX
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents lblFF As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents rdbDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbActivo As System.Windows.Forms.RadioButton
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents ocxCuenta As ERP.ocxTXTCuentaContable
    Friend WithEvents dgv As DataGridView
End Class

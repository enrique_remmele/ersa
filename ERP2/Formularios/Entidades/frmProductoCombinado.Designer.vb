﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProductoCombinado
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.lblReferencia = New System.Windows.Forms.Label()
        Me.txtReferencia = New ERP.ocxTXTString()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.OcxTXTProducto1 = New ERP.ocxTXTProducto()
        Me.OcxTXTString1 = New ERP.ocxTXTString()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.OcxTXTNumeric1 = New ERP.ocxTXTNumeric()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.OcxTXTString3 = New ERP.ocxTXTString()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.OcxTXTString2 = New ERP.ocxTXTString()
        Me.OcxTXTNumeric2 = New ERP.ocxTXTNumeric()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.OcxTXTString4 = New ERP.ocxTXTString()
        Me.OcxTXTProducto2 = New ERP.ocxTXTProducto()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(115, 16)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 1
        Me.lblDescripcion.Text = "Descripción:"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BackColor = System.Drawing.Color.White
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = "(F1) Para busqueda avanzada"
        Me.txtDescripcion.Location = New System.Drawing.Point(112, 32)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(419, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'lblReferencia
        '
        Me.lblReferencia.AutoSize = True
        Me.lblReferencia.Location = New System.Drawing.Point(15, 16)
        Me.lblReferencia.Name = "lblReferencia"
        Me.lblReferencia.Size = New System.Drawing.Size(62, 13)
        Me.lblReferencia.TabIndex = 0
        Me.lblReferencia.Text = "Referencia:"
        '
        'txtReferencia
        '
        Me.txtReferencia.BackColor = System.Drawing.Color.White
        Me.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferencia.Color = System.Drawing.Color.Empty
        Me.txtReferencia.Indicaciones = Nothing
        Me.txtReferencia.Location = New System.Drawing.Point(18, 32)
        Me.txtReferencia.Multilinea = False
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Size = New System.Drawing.Size(94, 21)
        Me.txtReferencia.SoloLectura = False
        Me.txtReferencia.TabIndex = 2
        Me.txtReferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferencia.Texto = ""
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.OcxTXTString2)
        Me.GroupBox1.Controls.Add(Me.OcxTXTNumeric2)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.OcxTXTString4)
        Me.GroupBox1.Controls.Add(Me.OcxTXTProducto2)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.OcxTXTString3)
        Me.GroupBox1.Controls.Add(Me.OcxTXTNumeric1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.OcxTXTString1)
        Me.GroupBox1.Controls.Add(Me.OcxTXTProducto1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 74)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(526, 209)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'OcxTXTProducto1
        '
        Me.OcxTXTProducto1.AlturaMaxima = 0
        Me.OcxTXTProducto1.ColumnasNumericas = Nothing
        Me.OcxTXTProducto1.Consulta = Nothing
        Me.OcxTXTProducto1.ControlarExistencia = False
        Me.OcxTXTProducto1.dtDescuento = Nothing
        Me.OcxTXTProducto1.IDCliente = 0
        Me.OcxTXTProducto1.IDDeposito = 0
        Me.OcxTXTProducto1.IDSucursal = 0
        Me.OcxTXTProducto1.Location = New System.Drawing.Point(97, 33)
        Me.OcxTXTProducto1.Name = "OcxTXTProducto1"
        Me.OcxTXTProducto1.Precios = Nothing
        Me.OcxTXTProducto1.Registro = Nothing
        Me.OcxTXTProducto1.Seleccionado = False
        Me.OcxTXTProducto1.Size = New System.Drawing.Size(307, 21)
        Me.OcxTXTProducto1.SoloLectura = False
        Me.OcxTXTProducto1.TabIndex = 2
        Me.OcxTXTProducto1.TieneDescuento = False
        Me.OcxTXTProducto1.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.OcxTXTProducto1.Venta = False
        '
        'OcxTXTString1
        '
        Me.OcxTXTString1.BackColor = System.Drawing.Color.White
        Me.OcxTXTString1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTString1.Color = System.Drawing.Color.Empty
        Me.OcxTXTString1.Indicaciones = Nothing
        Me.OcxTXTString1.Location = New System.Drawing.Point(6, 32)
        Me.OcxTXTString1.Multilinea = False
        Me.OcxTXTString1.Name = "OcxTXTString1"
        Me.OcxTXTString1.Size = New System.Drawing.Size(94, 21)
        Me.OcxTXTString1.SoloLectura = False
        Me.OcxTXTString1.TabIndex = 1
        Me.OcxTXTString1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.OcxTXTString1.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(114, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Producto principal:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(399, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Cantidad:"
        '
        'OcxTXTNumeric1
        '
        Me.OcxTXTNumeric1.Color = System.Drawing.Color.Empty
        Me.OcxTXTNumeric1.Decimales = True
        Me.OcxTXTNumeric1.Indicaciones = Nothing
        Me.OcxTXTNumeric1.Location = New System.Drawing.Point(402, 71)
        Me.OcxTXTNumeric1.Name = "OcxTXTNumeric1"
        Me.OcxTXTNumeric1.Size = New System.Drawing.Size(119, 20)
        Me.OcxTXTNumeric1.SoloLectura = False
        Me.OcxTXTNumeric1.TabIndex = 6
        Me.OcxTXTNumeric1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.OcxTXTNumeric1.Texto = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(399, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 13)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Codigo de Barra:"
        '
        'OcxTXTString3
        '
        Me.OcxTXTString3.BackColor = System.Drawing.Color.White
        Me.OcxTXTString3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTString3.Color = System.Drawing.Color.Empty
        Me.OcxTXTString3.Indicaciones = Nothing
        Me.OcxTXTString3.Location = New System.Drawing.Point(402, 32)
        Me.OcxTXTString3.Multilinea = False
        Me.OcxTXTString3.Name = "OcxTXTString3"
        Me.OcxTXTString3.Size = New System.Drawing.Size(119, 21)
        Me.OcxTXTString3.SoloLectura = False
        Me.OcxTXTString3.TabIndex = 4
        Me.OcxTXTString3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.OcxTXTString3.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(399, 126)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(86, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Codigo de Barra:"
        '
        'OcxTXTString2
        '
        Me.OcxTXTString2.BackColor = System.Drawing.Color.White
        Me.OcxTXTString2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTString2.Color = System.Drawing.Color.Empty
        Me.OcxTXTString2.Indicaciones = Nothing
        Me.OcxTXTString2.Location = New System.Drawing.Point(402, 142)
        Me.OcxTXTString2.Multilinea = False
        Me.OcxTXTString2.Name = "OcxTXTString2"
        Me.OcxTXTString2.Size = New System.Drawing.Size(119, 21)
        Me.OcxTXTString2.SoloLectura = False
        Me.OcxTXTString2.TabIndex = 11
        Me.OcxTXTString2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.OcxTXTString2.Texto = ""
        '
        'OcxTXTNumeric2
        '
        Me.OcxTXTNumeric2.Color = System.Drawing.Color.Empty
        Me.OcxTXTNumeric2.Decimales = True
        Me.OcxTXTNumeric2.Indicaciones = Nothing
        Me.OcxTXTNumeric2.Location = New System.Drawing.Point(402, 181)
        Me.OcxTXTNumeric2.Name = "OcxTXTNumeric2"
        Me.OcxTXTNumeric2.Size = New System.Drawing.Size(119, 20)
        Me.OcxTXTNumeric2.SoloLectura = False
        Me.OcxTXTNumeric2.TabIndex = 13
        Me.OcxTXTNumeric2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.OcxTXTNumeric2.Texto = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(399, 165)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Cantidad:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 126)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(128, 13)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Producto secundario:"
        '
        'OcxTXTString4
        '
        Me.OcxTXTString4.BackColor = System.Drawing.Color.White
        Me.OcxTXTString4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTString4.Color = System.Drawing.Color.Empty
        Me.OcxTXTString4.Indicaciones = Nothing
        Me.OcxTXTString4.Location = New System.Drawing.Point(6, 142)
        Me.OcxTXTString4.Multilinea = False
        Me.OcxTXTString4.Name = "OcxTXTString4"
        Me.OcxTXTString4.Size = New System.Drawing.Size(94, 21)
        Me.OcxTXTString4.SoloLectura = False
        Me.OcxTXTString4.TabIndex = 8
        Me.OcxTXTString4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.OcxTXTString4.Texto = ""
        '
        'OcxTXTProducto2
        '
        Me.OcxTXTProducto2.AlturaMaxima = 0
        Me.OcxTXTProducto2.ColumnasNumericas = Nothing
        Me.OcxTXTProducto2.Consulta = Nothing
        Me.OcxTXTProducto2.ControlarExistencia = False
        Me.OcxTXTProducto2.dtDescuento = Nothing
        Me.OcxTXTProducto2.IDCliente = 0
        Me.OcxTXTProducto2.IDDeposito = 0
        Me.OcxTXTProducto2.IDSucursal = 0
        Me.OcxTXTProducto2.Location = New System.Drawing.Point(97, 143)
        Me.OcxTXTProducto2.Name = "OcxTXTProducto2"
        Me.OcxTXTProducto2.Precios = Nothing
        Me.OcxTXTProducto2.Registro = Nothing
        Me.OcxTXTProducto2.Seleccionado = False
        Me.OcxTXTProducto2.Size = New System.Drawing.Size(307, 21)
        Me.OcxTXTProducto2.SoloLectura = False
        Me.OcxTXTProducto2.TabIndex = 9
        Me.OcxTXTProducto2.TieneDescuento = False
        Me.OcxTXTProducto2.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.OcxTXTProducto2.Venta = False
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(462, 289)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 7
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(12, 289)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 5
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(93, 289)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 6
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 319)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(549, 22)
        Me.StatusStrip1.TabIndex = 8
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmProductoCombinado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(549, 341)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblReferencia)
        Me.Controls.Add(Me.txtReferencia)
        Me.Name = "frmProductoCombinado"
        Me.Text = "frmProductoCombinado"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents lblReferencia As System.Windows.Forms.Label
    Friend WithEvents txtReferencia As ERP.ocxTXTString
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents OcxTXTString2 As ERP.ocxTXTString
    Friend WithEvents OcxTXTNumeric2 As ERP.ocxTXTNumeric
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents OcxTXTString4 As ERP.ocxTXTString
    Friend WithEvents OcxTXTProducto2 As ERP.ocxTXTProducto
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents OcxTXTString3 As ERP.ocxTXTString
    Friend WithEvents OcxTXTNumeric1 As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents OcxTXTString1 As ERP.ocxTXTString
    Friend WithEvents OcxTXTProducto1 As ERP.ocxTXTProducto
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
End Class

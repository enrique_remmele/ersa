﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCuentaBancaria
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lblBanco = New System.Windows.Forms.Label()
        Me.LblNombre = New System.Windows.Forms.Label()
        Me.LblFirma = New System.Windows.Forms.Label()
        Me.LblTitular = New System.Windows.Forms.Label()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblCodigo = New System.Windows.Forms.Label()
        Me.lblApertura = New System.Windows.Forms.Label()
        Me.lblObsevacion = New System.Windows.Forms.Label()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.btnExaminarA = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnExaminarD = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDiferido = New ERP.ocxTXTString()
        Me.txtAlDia = New ERP.ocxTXTString()
        Me.txtApertura = New ERP.ocxTXTDate()
        Me.txtCuentaContable = New ERP.ocxTXTCuentaContable()
        Me.txtCuenta = New ERP.ocxTXTString()
        Me.txtFirma2 = New ERP.ocxTXTString()
        Me.txtFirma3 = New ERP.ocxTXTString()
        Me.txtFirma1 = New ERP.ocxTXTString()
        Me.txtTitular = New ERP.ocxTXTString()
        Me.txtNombre = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.cbxBanco = New ERP.ocxCBX()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.PrintDocument = New System.Drawing.Printing.PrintDocument()
        Me.cbxImpresora = New ERP.ocxCBX()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.Label4 = New System.Windows.Forms.Label()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(312, 348)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 36
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(394, 348)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 37
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(231, 348)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 35
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(146, 348)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 34
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(65, 348)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 33
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(7, 38)
        Me.lblBanco.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(41, 13)
        Me.lblBanco.TabIndex = 2
        Me.lblBanco.Text = "Banco:"
        '
        'LblNombre
        '
        Me.LblNombre.AutoSize = True
        Me.LblNombre.Location = New System.Drawing.Point(7, 91)
        Me.LblNombre.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LblNombre.Name = "LblNombre"
        Me.LblNombre.Size = New System.Drawing.Size(47, 13)
        Me.LblNombre.TabIndex = 12
        Me.LblNombre.Text = "Nombre:"
        '
        'LblFirma
        '
        Me.LblFirma.AutoSize = True
        Me.LblFirma.Location = New System.Drawing.Point(7, 138)
        Me.LblFirma.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LblFirma.Name = "LblFirma"
        Me.LblFirma.Size = New System.Drawing.Size(40, 13)
        Me.LblFirma.TabIndex = 16
        Me.LblFirma.Text = "Firmas:"
        '
        'LblTitular
        '
        Me.LblTitular.AutoSize = True
        Me.LblTitular.Location = New System.Drawing.Point(7, 115)
        Me.LblTitular.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LblTitular.Name = "LblTitular"
        Me.LblTitular.Size = New System.Drawing.Size(56, 13)
        Me.LblTitular.TabIndex = 14
        Me.LblTitular.Text = "Titular(es):"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(7, 66)
        Me.lblTipo.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(34, 13)
        Me.lblTipo.TabIndex = 6
        Me.lblTipo.Text = "Tipo :"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(209, 66)
        Me.lblMoneda.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 8
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(7, 14)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 393)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1120, 22)
        Me.StatusStrip1.TabIndex = 40
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(1021, 348)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 39
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(140, 312)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 32
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(79, 312)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 31
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(11, 316)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 30
        Me.lblEstado.Text = "Estado:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(309, 38)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Cuenta:"
        '
        'lblCodigo
        '
        Me.lblCodigo.AutoSize = True
        Me.lblCodigo.Location = New System.Drawing.Point(7, 162)
        Me.lblCodigo.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(61, 13)
        Me.lblCodigo.TabIndex = 20
        Me.lblCodigo.Text = "Codigo CG:"
        '
        'lblApertura
        '
        Me.lblApertura.AutoSize = True
        Me.lblApertura.Location = New System.Drawing.Point(309, 66)
        Me.lblApertura.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblApertura.Name = "lblApertura"
        Me.lblApertura.Size = New System.Drawing.Size(50, 13)
        Me.lblApertura.TabIndex = 10
        Me.lblApertura.Text = "Apertura:"
        '
        'lblObsevacion
        '
        Me.lblObsevacion.AutoSize = True
        Me.lblObsevacion.Location = New System.Drawing.Point(7, 190)
        Me.lblObsevacion.Name = "lblObsevacion"
        Me.lblObsevacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObsevacion.TabIndex = 22
        Me.lblObsevacion.Text = "Observacion:"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'btnExaminarA
        '
        Me.btnExaminarA.Location = New System.Drawing.Point(387, 221)
        Me.btnExaminarA.Name = "btnExaminarA"
        Me.btnExaminarA.Size = New System.Drawing.Size(75, 23)
        Me.btnExaminarA.TabIndex = 26
        Me.btnExaminarA.Text = "E&xaminar"
        Me.btnExaminarA.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(28, 226)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "Al Día:"
        '
        'btnExaminarD
        '
        Me.btnExaminarD.Location = New System.Drawing.Point(387, 250)
        Me.btnExaminarD.Name = "btnExaminarD"
        Me.btnExaminarD.Size = New System.Drawing.Size(75, 23)
        Me.btnExaminarD.TabIndex = 29
        Me.btnExaminarD.Text = "E&xaminar"
        Me.btnExaminarD.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(28, 255)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "Diferido:"
        '
        'txtDiferido
        '
        Me.txtDiferido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDiferido.Color = System.Drawing.Color.Empty
        Me.txtDiferido.Indicaciones = Nothing
        Me.txtDiferido.Location = New System.Drawing.Point(74, 250)
        Me.txtDiferido.Multilinea = False
        Me.txtDiferido.Name = "txtDiferido"
        Me.txtDiferido.Size = New System.Drawing.Size(307, 21)
        Me.txtDiferido.SoloLectura = True
        Me.txtDiferido.TabIndex = 28
        Me.txtDiferido.TabStop = False
        Me.txtDiferido.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDiferido.Texto = ""
        '
        'txtAlDia
        '
        Me.txtAlDia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAlDia.Color = System.Drawing.Color.Empty
        Me.txtAlDia.Indicaciones = Nothing
        Me.txtAlDia.Location = New System.Drawing.Point(74, 223)
        Me.txtAlDia.Multilinea = False
        Me.txtAlDia.Name = "txtAlDia"
        Me.txtAlDia.Size = New System.Drawing.Size(307, 21)
        Me.txtAlDia.SoloLectura = True
        Me.txtAlDia.TabIndex = 25
        Me.txtAlDia.TabStop = False
        Me.txtAlDia.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtAlDia.Texto = ""
        '
        'txtApertura
        '
        Me.txtApertura.AñoFecha = 0
        Me.txtApertura.Color = System.Drawing.Color.Empty
        Me.txtApertura.Fecha = New Date(2013, 1, 11, 15, 45, 54, 765)
        Me.txtApertura.Location = New System.Drawing.Point(356, 61)
        Me.txtApertura.MesFecha = 0
        Me.txtApertura.Name = "txtApertura"
        Me.txtApertura.PermitirNulo = False
        Me.txtApertura.Size = New System.Drawing.Size(74, 20)
        Me.txtApertura.SoloLectura = False
        Me.txtApertura.TabIndex = 11
        '
        'txtCuentaContable
        '
        Me.txtCuentaContable.AlturaMaxima = 98
        Me.txtCuentaContable.Consulta = Nothing
        Me.txtCuentaContable.IDCentroCosto = 0
        Me.txtCuentaContable.IDUnidadNegocio = 0
        Me.txtCuentaContable.ListarTodas = False
        Me.txtCuentaContable.Location = New System.Drawing.Point(77, 158)
        Me.txtCuentaContable.Name = "txtCuentaContable"
        Me.txtCuentaContable.Registro = Nothing
        Me.txtCuentaContable.Resolucion173 = False
        Me.txtCuentaContable.Seleccionado = False
        Me.txtCuentaContable.Size = New System.Drawing.Size(391, 20)
        Me.txtCuentaContable.SoloLectura = False
        Me.txtCuentaContable.TabIndex = 21
        Me.txtCuentaContable.Texto = Nothing
        Me.txtCuentaContable.whereFiltro = Nothing
        '
        'txtCuenta
        '
        Me.txtCuenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCuenta.Color = System.Drawing.Color.Empty
        Me.txtCuenta.Indicaciones = Nothing
        Me.txtCuenta.Location = New System.Drawing.Point(356, 34)
        Me.txtCuenta.Multilinea = False
        Me.txtCuenta.Name = "txtCuenta"
        Me.txtCuenta.Size = New System.Drawing.Size(113, 21)
        Me.txtCuenta.SoloLectura = False
        Me.txtCuenta.TabIndex = 5
        Me.txtCuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCuenta.Texto = ""
        '
        'txtFirma2
        '
        Me.txtFirma2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFirma2.Color = System.Drawing.Color.Empty
        Me.txtFirma2.Indicaciones = Nothing
        Me.txtFirma2.Location = New System.Drawing.Point(210, 133)
        Me.txtFirma2.Multilinea = False
        Me.txtFirma2.Name = "txtFirma2"
        Me.txtFirma2.Size = New System.Drawing.Size(127, 21)
        Me.txtFirma2.SoloLectura = False
        Me.txtFirma2.TabIndex = 18
        Me.txtFirma2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFirma2.Texto = ""
        '
        'txtFirma3
        '
        Me.txtFirma3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFirma3.Color = System.Drawing.Color.Empty
        Me.txtFirma3.Indicaciones = Nothing
        Me.txtFirma3.Location = New System.Drawing.Point(341, 132)
        Me.txtFirma3.Multilinea = False
        Me.txtFirma3.Name = "txtFirma3"
        Me.txtFirma3.Size = New System.Drawing.Size(127, 21)
        Me.txtFirma3.SoloLectura = False
        Me.txtFirma3.TabIndex = 19
        Me.txtFirma3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFirma3.Texto = ""
        '
        'txtFirma1
        '
        Me.txtFirma1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFirma1.Color = System.Drawing.Color.Empty
        Me.txtFirma1.Indicaciones = Nothing
        Me.txtFirma1.Location = New System.Drawing.Point(77, 133)
        Me.txtFirma1.Multilinea = False
        Me.txtFirma1.Name = "txtFirma1"
        Me.txtFirma1.Size = New System.Drawing.Size(127, 21)
        Me.txtFirma1.SoloLectura = False
        Me.txtFirma1.TabIndex = 17
        Me.txtFirma1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFirma1.Texto = ""
        '
        'txtTitular
        '
        Me.txtTitular.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTitular.Color = System.Drawing.Color.Empty
        Me.txtTitular.Indicaciones = Nothing
        Me.txtTitular.Location = New System.Drawing.Point(77, 110)
        Me.txtTitular.Multilinea = False
        Me.txtTitular.Name = "txtTitular"
        Me.txtTitular.Size = New System.Drawing.Size(392, 21)
        Me.txtTitular.SoloLectura = False
        Me.txtTitular.TabIndex = 15
        Me.txtTitular.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTitular.Texto = ""
        '
        'txtNombre
        '
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.Color = System.Drawing.Color.Empty
        Me.txtNombre.Indicaciones = Nothing
        Me.txtNombre.Location = New System.Drawing.Point(77, 87)
        Me.txtNombre.Multilinea = False
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(392, 21)
        Me.txtNombre.SoloLectura = False
        Me.txtNombre.TabIndex = 13
        Me.txtNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNombre.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(77, 11)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 18)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 1
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(260, 62)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(46, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 9
        Me.cbxMoneda.Texto = ""
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = Nothing
        Me.cbxTipo.CargarUnaSolaVez = False
        Me.cbxTipo.DataDisplayMember = Nothing
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = Nothing
        Me.cbxTipo.DataSource = Nothing
        Me.cbxTipo.DataValueMember = Nothing
        Me.cbxTipo.dtSeleccionado = Nothing
        Me.cbxTipo.FormABM = Nothing
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(77, 62)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionMultiple = False
        Me.cbxTipo.SeleccionObligatoria = False
        Me.cbxTipo.Size = New System.Drawing.Size(131, 21)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 7
        Me.cbxTipo.Texto = ""
        '
        'cbxBanco
        '
        Me.cbxBanco.CampoWhere = Nothing
        Me.cbxBanco.CargarUnaSolaVez = False
        Me.cbxBanco.DataDisplayMember = Nothing
        Me.cbxBanco.DataFilter = Nothing
        Me.cbxBanco.DataOrderBy = Nothing
        Me.cbxBanco.DataSource = Nothing
        Me.cbxBanco.DataValueMember = Nothing
        Me.cbxBanco.dtSeleccionado = Nothing
        Me.cbxBanco.FormABM = Nothing
        Me.cbxBanco.Indicaciones = Nothing
        Me.cbxBanco.Location = New System.Drawing.Point(77, 34)
        Me.cbxBanco.Name = "cbxBanco"
        Me.cbxBanco.SeleccionMultiple = False
        Me.cbxBanco.SeleccionObligatoria = False
        Me.cbxBanco.Size = New System.Drawing.Size(229, 21)
        Me.cbxBanco.SoloLectura = False
        Me.cbxBanco.TabIndex = 3
        Me.cbxBanco.Texto = ""
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(77, 184)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(390, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 23
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxImpresora
        '
        Me.cbxImpresora.CampoWhere = Nothing
        Me.cbxImpresora.CargarUnaSolaVez = False
        Me.cbxImpresora.DataDisplayMember = Nothing
        Me.cbxImpresora.DataFilter = Nothing
        Me.cbxImpresora.DataOrderBy = Nothing
        Me.cbxImpresora.DataSource = Nothing
        Me.cbxImpresora.DataValueMember = Nothing
        Me.cbxImpresora.dtSeleccionado = Nothing
        Me.cbxImpresora.FormABM = Nothing
        Me.cbxImpresora.Indicaciones = Nothing
        Me.cbxImpresora.Location = New System.Drawing.Point(231, 308)
        Me.cbxImpresora.Name = "cbxImpresora"
        Me.cbxImpresora.SeleccionMultiple = False
        Me.cbxImpresora.SeleccionObligatoria = False
        Me.cbxImpresora.Size = New System.Drawing.Size(189, 21)
        Me.cbxImpresora.SoloLectura = False
        Me.cbxImpresora.TabIndex = 41
        Me.cbxImpresora.Texto = ""
        '
        'dgv
        '
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(488, 14)
        Me.dgv.Name = "dgv"
        Me.dgv.Size = New System.Drawing.Size(608, 315)
        Me.dgv.TabIndex = 78
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = Nothing
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = Nothing
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = Nothing
        Me.cbxUnidadNegocio.DataSource = Nothing
        Me.cbxUnidadNegocio.DataValueMember = Nothing
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(114, 277)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(267, 21)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 80
        Me.cbxUnidadNegocio.Texto = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 281)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(102, 13)
        Me.Label4.TabIndex = 79
        Me.Label4.Text = "Unidad de Negocio:"
        '
        'frmCuentaBancaria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1120, 415)
        Me.Controls.Add(Me.cbxUnidadNegocio)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.cbxImpresora)
        Me.Controls.Add(Me.btnExaminarD)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtDiferido)
        Me.Controls.Add(Me.btnExaminarA)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtAlDia)
        Me.Controls.Add(Me.lblObsevacion)
        Me.Controls.Add(Me.txtApertura)
        Me.Controls.Add(Me.txtCuentaContable)
        Me.Controls.Add(Me.txtCuenta)
        Me.Controls.Add(Me.txtFirma2)
        Me.Controls.Add(Me.txtFirma3)
        Me.Controls.Add(Me.txtFirma1)
        Me.Controls.Add(Me.txtTitular)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.LblFirma)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.rdbDesactivado)
        Me.Controls.Add(Me.rdbActivo)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.cbxMoneda)
        Me.Controls.Add(Me.lblMoneda)
        Me.Controls.Add(Me.cbxTipo)
        Me.Controls.Add(Me.lblTipo)
        Me.Controls.Add(Me.cbxBanco)
        Me.Controls.Add(Me.LblTitular)
        Me.Controls.Add(Me.LblNombre)
        Me.Controls.Add(Me.lblBanco)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.txtObservacion)
        Me.Controls.Add(Me.lblCodigo)
        Me.Controls.Add(Me.lblApertura)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frmCuentaBancaria"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmCuentaBancaria"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents LblNombre As System.Windows.Forms.Label
    Friend WithEvents LblFirma As System.Windows.Forms.Label
    Friend WithEvents LblTitular As System.Windows.Forms.Label
    Friend WithEvents txtCuentaContable As ERP.ocxTXTCuentaContable
    Friend WithEvents cbxBanco As ERP.ocxCBX
    Friend WithEvents cbxTipo As ERP.ocxCBX
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents rdbDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbActivo As System.Windows.Forms.RadioButton
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblCodigo As System.Windows.Forms.Label
    Friend WithEvents txtCuenta As ERP.ocxTXTString
    Friend WithEvents txtFirma2 As ERP.ocxTXTString
    Friend WithEvents txtFirma3 As ERP.ocxTXTString
    Friend WithEvents txtFirma1 As ERP.ocxTXTString
    Friend WithEvents txtTitular As ERP.ocxTXTString
    Friend WithEvents txtNombre As ERP.ocxTXTString
    Friend WithEvents txtApertura As ERP.ocxTXTDate
    Friend WithEvents lblApertura As System.Windows.Forms.Label
    Friend WithEvents lblObsevacion As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnExaminarA As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtAlDia As ERP.ocxTXTString
    Friend WithEvents btnExaminarD As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDiferido As ERP.ocxTXTString
    Friend WithEvents PrintDocument As System.Drawing.Printing.PrintDocument
    Friend WithEvents cbxImpresora As ERP.ocxCBX
    Friend WithEvents dgv As DataGridView
    Friend WithEvents cbxUnidadNegocio As ocxCBX
    Friend WithEvents Label4 As Label
End Class

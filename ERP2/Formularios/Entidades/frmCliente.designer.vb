﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblRazonSocial = New System.Windows.Forms.Label()
        Me.tcGeneral = New System.Windows.Forms.TabControl()
        Me.tabDatosComerciales = New System.Windows.Forms.TabPage()
        Me.cbxTipoOperacionTipoCliente = New ERP.ocxCBX()
        Me.lblTipoOperacion = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtNroCasa = New ERP.ocxTXTString()
        Me.txtAniversario = New ERP.ocxTXTDate()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblPaginaWeb = New System.Windows.Forms.Label()
        Me.txtFax = New ERP.ocxTXTString()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.lblFax = New System.Windows.Forms.Label()
        Me.txtPaginaWeb = New System.Windows.Forms.TextBox()
        Me.txtEmail1 = New System.Windows.Forms.TextBox()
        Me.cbxSituacion = New ERP.ocxCBX()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cbxClienteProducto = New ERP.ocxCBX()
        Me.LblClienteProducto = New System.Windows.Forms.Label()
        Me.cbxCategoriaCliente = New ERP.ocxCBX()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbxAbogado = New ERP.ocxCBX()
        Me.chkJudicial = New System.Windows.Forms.CheckBox()
        Me.lblCelular = New System.Windows.Forms.Label()
        Me.txtCelulares = New ERP.ocxTXTString()
        Me.cbxRuta = New ERP.ocxCBX()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxArea = New ERP.ocxCBX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chkClienteVario = New System.Windows.Forms.CheckBox()
        Me.PnlCondicionVenta = New System.Windows.Forms.Panel()
        Me.rdbContado = New System.Windows.Forms.RadioButton()
        Me.rdbCredito = New System.Windows.Forms.RadioButton()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.ocxCuenta = New ERP.ocxTXTCuentaContable()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.cbxTipoCliente = New ERP.ocxCBX()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.cbxListaPrecio = New ERP.ocxCBX()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.cbxDistribuidor = New ERP.ocxCBX()
        Me.cbxCobrador = New ERP.ocxCBX()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.cbxPromotor = New ERP.ocxCBX()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.cbxZonaVenta = New ERP.ocxCBX()
        Me.cbxBarrio = New ERP.ocxCBX()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.cbxDepartamento = New ERP.ocxCBX()
        Me.cbxEstado = New ERP.ocxCBX()
        Me.cbxPais = New ERP.ocxCBX()
        Me.txtTelefonos = New ERP.ocxTXTString()
        Me.txtDireccion = New ERP.ocxTXTString()
        Me.txtNombreFantasia = New ERP.ocxTXTString()
        Me.chkIVAExento = New System.Windows.Forms.CheckBox()
        Me.txtUsuarioModificacion = New System.Windows.Forms.TextBox()
        Me.txtFechaModificacion = New System.Windows.Forms.TextBox()
        Me.lblModificacion = New System.Windows.Forms.Label()
        Me.txtFechaUltimaCompra = New System.Windows.Forms.TextBox()
        Me.lblUltimaCompra = New System.Windows.Forms.Label()
        Me.txtUsuarioAlta = New System.Windows.Forms.TextBox()
        Me.txtFechaAlta = New System.Windows.Forms.TextBox()
        Me.lblAlta = New System.Windows.Forms.Label()
        Me.lblDistribuidor = New System.Windows.Forms.Label()
        Me.lblCobrador = New System.Windows.Forms.Label()
        Me.lblVendedor = New System.Windows.Forms.Label()
        Me.lblPromotor = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblCondicionVenta = New System.Windows.Forms.Label()
        Me.lblTipoCliente = New System.Windows.Forms.Label()
        Me.lblListaPrecio = New System.Windows.Forms.Label()
        Me.lblZonaVenta = New System.Windows.Forms.Label()
        Me.lblBarrio = New System.Windows.Forms.Label()
        Me.lblCiudad = New System.Windows.Forms.Label()
        Me.lblDepartamento = New System.Windows.Forms.Label()
        Me.lblPais = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.lblTelefonos = New System.Windows.Forms.Label()
        Me.lblDireccion = New System.Windows.Forms.Label()
        Me.lblNombreFantasia = New System.Windows.Forms.Label()
        Me.tabCredito = New System.Windows.Forms.TabPage()
        Me.chkBoleta = New ERP.ocxCHK()
        Me.lklVerCatastro = New System.Windows.Forms.LinkLabel()
        Me.lvlSaldo = New System.Windows.Forms.Label()
        Me.lblDeuda = New System.Windows.Forms.Label()
        Me.lklRecalcularCredito = New System.Windows.Forms.LinkLabel()
        Me.lblPlazoCheque = New System.Windows.Forms.Label()
        Me.lblPlazoCobro = New System.Windows.Forms.Label()
        Me.lblPlazoCredito = New System.Windows.Forms.Label()
        Me.lblDescuentoSimbolo = New System.Windows.Forms.Label()
        Me.lblDesuento = New System.Windows.Forms.Label()
        Me.lblLimiteCredito = New System.Windows.Forms.Label()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.txtDeuda = New ERP.ocxTXTNumeric()
        Me.txtPlazoCheque = New ERP.ocxTXTNumeric()
        Me.txtPlazoCobro = New ERP.ocxTXTNumeric()
        Me.txtPlazoCredito = New ERP.ocxTXTNumeric()
        Me.txtDescuento = New ERP.ocxTXTNumeric()
        Me.txtLimiteCredito = New ERP.ocxTXTNumeric()
        Me.tabDatosAdicionales = New System.Windows.Forms.TabPage()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.OcxClienteContacto1 = New ERP.ocxClienteContacto()
        Me.tabSucursales = New System.Windows.Forms.TabPage()
        Me.OcxSucursalesClientes1 = New ERP.ocxSucursalesClientes()
        Me.tabLocalizacion = New System.Windows.Forms.TabPage()
        Me.OcxClientesMapa1 = New ERP.ocxClientesMapa()
        Me.tabFirma = New System.Windows.Forms.TabPage()
        Me.OcxRegistroFirmas1 = New ERP.ocxRegistroFirmas()
        Me.tabPrecios = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.OcxClientePrecio1 = New ERP.ocxClientePrecio()
        Me.tabKMEstimado = New System.Windows.Forms.TabPage()
        Me.OcxKMEstimadoMatriz1 = New ERP.ocxKMEstimadoMatriz()
        Me.tabKMEstimadoSucursal = New System.Windows.Forms.TabPage()
        Me.OcxKMEstimadoSucursal1 = New ERP.ocxKMEstimadoSucursal()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.lblRUC = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.OcxCBX1 = New ERP.ocxCBX()
        Me.btnConsultarRUC = New System.Windows.Forms.Button()
        Me.chkCI = New ERP.ocxCHK()
        Me.lblReferencia = New System.Windows.Forms.Label()
        Me.txtReferencia = New ERP.ocxTXTString()
        Me.txtRUC = New ERP.ocxTXTString()
        Me.txtRazonSocial = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.tcGeneral.SuspendLayout()
        Me.tabDatosComerciales.SuspendLayout()
        Me.PnlCondicionVenta.SuspendLayout()
        Me.tabCredito.SuspendLayout()
        Me.tabDatosAdicionales.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.tabSucursales.SuspendLayout()
        Me.tabLocalizacion.SuspendLayout()
        Me.tabFirma.SuspendLayout()
        Me.tabPrecios.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.tabKMEstimado.SuspendLayout()
        Me.tabKMEstimadoSucursal.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblID.Location = New System.Drawing.Point(698, 18)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(43, 13)
        Me.lblID.TabIndex = 7
        Me.lblID.Text = "Código:"
        '
        'lblRazonSocial
        '
        Me.lblRazonSocial.AutoSize = True
        Me.lblRazonSocial.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRazonSocial.Location = New System.Drawing.Point(87, 17)
        Me.lblRazonSocial.Name = "lblRazonSocial"
        Me.lblRazonSocial.Size = New System.Drawing.Size(73, 13)
        Me.lblRazonSocial.TabIndex = 2
        Me.lblRazonSocial.Text = "Razón Social:"
        '
        'tcGeneral
        '
        Me.tcGeneral.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tcGeneral.Controls.Add(Me.tabDatosComerciales)
        Me.tcGeneral.Controls.Add(Me.tabCredito)
        Me.tcGeneral.Controls.Add(Me.tabDatosAdicionales)
        Me.tcGeneral.Controls.Add(Me.tabSucursales)
        Me.tcGeneral.Controls.Add(Me.tabLocalizacion)
        Me.tcGeneral.Controls.Add(Me.tabFirma)
        Me.tcGeneral.Controls.Add(Me.tabPrecios)
        Me.tcGeneral.Controls.Add(Me.tabKMEstimado)
        Me.tcGeneral.Controls.Add(Me.tabKMEstimadoSucursal)
        Me.tcGeneral.Location = New System.Drawing.Point(0, 81)
        Me.tcGeneral.Margin = New System.Windows.Forms.Padding(0)
        Me.tcGeneral.Name = "tcGeneral"
        Me.tcGeneral.Padding = New System.Drawing.Point(0, 0)
        Me.tcGeneral.SelectedIndex = 0
        Me.tcGeneral.Size = New System.Drawing.Size(867, 412)
        Me.tcGeneral.TabIndex = 0
        '
        'tabDatosComerciales
        '
        Me.tabDatosComerciales.BackColor = System.Drawing.SystemColors.Control
        Me.tabDatosComerciales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tabDatosComerciales.Controls.Add(Me.cbxTipoOperacionTipoCliente)
        Me.tabDatosComerciales.Controls.Add(Me.lblTipoOperacion)
        Me.tabDatosComerciales.Controls.Add(Me.Label7)
        Me.tabDatosComerciales.Controls.Add(Me.txtNroCasa)
        Me.tabDatosComerciales.Controls.Add(Me.txtAniversario)
        Me.tabDatosComerciales.Controls.Add(Me.Label3)
        Me.tabDatosComerciales.Controls.Add(Me.lblPaginaWeb)
        Me.tabDatosComerciales.Controls.Add(Me.txtFax)
        Me.tabDatosComerciales.Controls.Add(Me.lblEmail)
        Me.tabDatosComerciales.Controls.Add(Me.lblFax)
        Me.tabDatosComerciales.Controls.Add(Me.txtPaginaWeb)
        Me.tabDatosComerciales.Controls.Add(Me.txtEmail1)
        Me.tabDatosComerciales.Controls.Add(Me.cbxSituacion)
        Me.tabDatosComerciales.Controls.Add(Me.Label5)
        Me.tabDatosComerciales.Controls.Add(Me.cbxClienteProducto)
        Me.tabDatosComerciales.Controls.Add(Me.LblClienteProducto)
        Me.tabDatosComerciales.Controls.Add(Me.cbxCategoriaCliente)
        Me.tabDatosComerciales.Controls.Add(Me.Label4)
        Me.tabDatosComerciales.Controls.Add(Me.cbxAbogado)
        Me.tabDatosComerciales.Controls.Add(Me.chkJudicial)
        Me.tabDatosComerciales.Controls.Add(Me.lblCelular)
        Me.tabDatosComerciales.Controls.Add(Me.txtCelulares)
        Me.tabDatosComerciales.Controls.Add(Me.cbxRuta)
        Me.tabDatosComerciales.Controls.Add(Me.Label1)
        Me.tabDatosComerciales.Controls.Add(Me.cbxArea)
        Me.tabDatosComerciales.Controls.Add(Me.Label2)
        Me.tabDatosComerciales.Controls.Add(Me.chkClienteVario)
        Me.tabDatosComerciales.Controls.Add(Me.PnlCondicionVenta)
        Me.tabDatosComerciales.Controls.Add(Me.Button2)
        Me.tabDatosComerciales.Controls.Add(Me.btnSalir)
        Me.tabDatosComerciales.Controls.Add(Me.btnNuevo)
        Me.tabDatosComerciales.Controls.Add(Me.btnEditar)
        Me.tabDatosComerciales.Controls.Add(Me.ocxCuenta)
        Me.tabDatosComerciales.Controls.Add(Me.lblCuenta)
        Me.tabDatosComerciales.Controls.Add(Me.btnGuardar)
        Me.tabDatosComerciales.Controls.Add(Me.cbxTipoCliente)
        Me.tabDatosComerciales.Controls.Add(Me.btnCancelar)
        Me.tabDatosComerciales.Controls.Add(Me.cbxListaPrecio)
        Me.tabDatosComerciales.Controls.Add(Me.Button1)
        Me.tabDatosComerciales.Controls.Add(Me.btnEliminar)
        Me.tabDatosComerciales.Controls.Add(Me.cbxMoneda)
        Me.tabDatosComerciales.Controls.Add(Me.cbxDistribuidor)
        Me.tabDatosComerciales.Controls.Add(Me.cbxCobrador)
        Me.tabDatosComerciales.Controls.Add(Me.cbxVendedor)
        Me.tabDatosComerciales.Controls.Add(Me.cbxPromotor)
        Me.tabDatosComerciales.Controls.Add(Me.cbxSucursal)
        Me.tabDatosComerciales.Controls.Add(Me.cbxZonaVenta)
        Me.tabDatosComerciales.Controls.Add(Me.cbxBarrio)
        Me.tabDatosComerciales.Controls.Add(Me.cbxCiudad)
        Me.tabDatosComerciales.Controls.Add(Me.cbxDepartamento)
        Me.tabDatosComerciales.Controls.Add(Me.cbxEstado)
        Me.tabDatosComerciales.Controls.Add(Me.cbxPais)
        Me.tabDatosComerciales.Controls.Add(Me.txtTelefonos)
        Me.tabDatosComerciales.Controls.Add(Me.txtDireccion)
        Me.tabDatosComerciales.Controls.Add(Me.txtNombreFantasia)
        Me.tabDatosComerciales.Controls.Add(Me.chkIVAExento)
        Me.tabDatosComerciales.Controls.Add(Me.txtUsuarioModificacion)
        Me.tabDatosComerciales.Controls.Add(Me.txtFechaModificacion)
        Me.tabDatosComerciales.Controls.Add(Me.lblModificacion)
        Me.tabDatosComerciales.Controls.Add(Me.txtFechaUltimaCompra)
        Me.tabDatosComerciales.Controls.Add(Me.lblUltimaCompra)
        Me.tabDatosComerciales.Controls.Add(Me.txtUsuarioAlta)
        Me.tabDatosComerciales.Controls.Add(Me.txtFechaAlta)
        Me.tabDatosComerciales.Controls.Add(Me.lblAlta)
        Me.tabDatosComerciales.Controls.Add(Me.lblDistribuidor)
        Me.tabDatosComerciales.Controls.Add(Me.lblCobrador)
        Me.tabDatosComerciales.Controls.Add(Me.lblVendedor)
        Me.tabDatosComerciales.Controls.Add(Me.lblPromotor)
        Me.tabDatosComerciales.Controls.Add(Me.lblSucursal)
        Me.tabDatosComerciales.Controls.Add(Me.lblMoneda)
        Me.tabDatosComerciales.Controls.Add(Me.lblCondicionVenta)
        Me.tabDatosComerciales.Controls.Add(Me.lblTipoCliente)
        Me.tabDatosComerciales.Controls.Add(Me.lblListaPrecio)
        Me.tabDatosComerciales.Controls.Add(Me.lblZonaVenta)
        Me.tabDatosComerciales.Controls.Add(Me.lblBarrio)
        Me.tabDatosComerciales.Controls.Add(Me.lblCiudad)
        Me.tabDatosComerciales.Controls.Add(Me.lblDepartamento)
        Me.tabDatosComerciales.Controls.Add(Me.lblPais)
        Me.tabDatosComerciales.Controls.Add(Me.lblEstado)
        Me.tabDatosComerciales.Controls.Add(Me.lblTelefonos)
        Me.tabDatosComerciales.Controls.Add(Me.lblDireccion)
        Me.tabDatosComerciales.Controls.Add(Me.lblNombreFantasia)
        Me.tabDatosComerciales.Location = New System.Drawing.Point(4, 22)
        Me.tabDatosComerciales.Name = "tabDatosComerciales"
        Me.tabDatosComerciales.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDatosComerciales.Size = New System.Drawing.Size(859, 386)
        Me.tabDatosComerciales.TabIndex = 0
        Me.tabDatosComerciales.Text = "Datos Comerciales"
        '
        'cbxTipoOperacionTipoCliente
        '
        Me.cbxTipoOperacionTipoCliente.CampoWhere = Nothing
        Me.cbxTipoOperacionTipoCliente.CargarUnaSolaVez = False
        Me.cbxTipoOperacionTipoCliente.DataDisplayMember = "Descripcion"
        Me.cbxTipoOperacionTipoCliente.DataFilter = Nothing
        Me.cbxTipoOperacionTipoCliente.DataOrderBy = Nothing
        Me.cbxTipoOperacionTipoCliente.DataSource = "TipoOperacionTipoCliente"
        Me.cbxTipoOperacionTipoCliente.DataValueMember = "ID"
        Me.cbxTipoOperacionTipoCliente.dtSeleccionado = Nothing
        Me.cbxTipoOperacionTipoCliente.FormABM = "frmTipoCliente"
        Me.cbxTipoOperacionTipoCliente.Indicaciones = Nothing
        Me.cbxTipoOperacionTipoCliente.Location = New System.Drawing.Point(359, 204)
        Me.cbxTipoOperacionTipoCliente.Name = "cbxTipoOperacionTipoCliente"
        Me.cbxTipoOperacionTipoCliente.SeleccionMultiple = False
        Me.cbxTipoOperacionTipoCliente.SeleccionObligatoria = False
        Me.cbxTipoOperacionTipoCliente.Size = New System.Drawing.Size(135, 21)
        Me.cbxTipoOperacionTipoCliente.SoloLectura = False
        Me.cbxTipoOperacionTipoCliente.TabIndex = 81
        Me.cbxTipoOperacionTipoCliente.Texto = ""
        '
        'lblTipoOperacion
        '
        Me.lblTipoOperacion.AutoSize = True
        Me.lblTipoOperacion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblTipoOperacion.Location = New System.Drawing.Point(256, 204)
        Me.lblTipoOperacion.Name = "lblTipoOperacion"
        Me.lblTipoOperacion.Size = New System.Drawing.Size(98, 13)
        Me.lblTipoOperacion.TabIndex = 82
        Me.lblTipoOperacion.Text = "Tipo de Operacion:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(285, 183)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(69, 13)
        Me.Label7.TabIndex = 80
        Me.Label7.Text = "Nro de Casa:"
        '
        'txtNroCasa
        '
        Me.txtNroCasa.BackColor = System.Drawing.Color.White
        Me.txtNroCasa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroCasa.Color = System.Drawing.Color.Empty
        Me.txtNroCasa.Indicaciones = Nothing
        Me.txtNroCasa.Location = New System.Drawing.Point(360, 180)
        Me.txtNroCasa.Multilinea = False
        Me.txtNroCasa.Name = "txtNroCasa"
        Me.txtNroCasa.Size = New System.Drawing.Size(60, 21)
        Me.txtNroCasa.SoloLectura = False
        Me.txtNroCasa.TabIndex = 79
        Me.txtNroCasa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroCasa.Texto = ""
        '
        'txtAniversario
        '
        Me.txtAniversario.AñoFecha = 0
        Me.txtAniversario.Color = System.Drawing.Color.Empty
        Me.txtAniversario.Fecha = New Date(2013, 4, 8, 9, 22, 55, 62)
        Me.txtAniversario.Location = New System.Drawing.Point(623, 99)
        Me.txtAniversario.MesFecha = 0
        Me.txtAniversario.Name = "txtAniversario"
        Me.txtAniversario.PermitirNulo = False
        Me.txtAniversario.Size = New System.Drawing.Size(74, 20)
        Me.txtAniversario.SoloLectura = False
        Me.txtAniversario.TabIndex = 78
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(555, 102)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 13)
        Me.Label3.TabIndex = 77
        Me.Label3.Text = "Aniversario:"
        '
        'lblPaginaWeb
        '
        Me.lblPaginaWeb.AutoSize = True
        Me.lblPaginaWeb.Location = New System.Drawing.Point(283, 230)
        Me.lblPaginaWeb.Name = "lblPaginaWeb"
        Me.lblPaginaWeb.Size = New System.Drawing.Size(69, 13)
        Me.lblPaginaWeb.TabIndex = 71
        Me.lblPaginaWeb.Text = "Pagina Web:"
        '
        'txtFax
        '
        Me.txtFax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFax.Color = System.Drawing.Color.Empty
        Me.txtFax.Indicaciones = Nothing
        Me.txtFax.Location = New System.Drawing.Point(623, 77)
        Me.txtFax.Multilinea = False
        Me.txtFax.Name = "txtFax"
        Me.txtFax.Size = New System.Drawing.Size(104, 21)
        Me.txtFax.SoloLectura = False
        Me.txtFax.TabIndex = 76
        Me.txtFax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtFax.Texto = ""
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Location = New System.Drawing.Point(317, 253)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(35, 13)
        Me.lblEmail.TabIndex = 73
        Me.lblEmail.Text = "Email:"
        '
        'lblFax
        '
        Me.lblFax.AutoSize = True
        Me.lblFax.Location = New System.Drawing.Point(590, 81)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(27, 13)
        Me.lblFax.TabIndex = 75
        Me.lblFax.Text = "Fax:"
        '
        'txtPaginaWeb
        '
        Me.txtPaginaWeb.BackColor = System.Drawing.Color.White
        Me.txtPaginaWeb.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtPaginaWeb.Location = New System.Drawing.Point(360, 226)
        Me.txtPaginaWeb.Name = "txtPaginaWeb"
        Me.txtPaginaWeb.ReadOnly = True
        Me.txtPaginaWeb.Size = New System.Drawing.Size(156, 20)
        Me.txtPaginaWeb.TabIndex = 72
        '
        'txtEmail1
        '
        Me.txtEmail1.BackColor = System.Drawing.Color.White
        Me.txtEmail1.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtEmail1.Location = New System.Drawing.Point(360, 248)
        Me.txtEmail1.Name = "txtEmail1"
        Me.txtEmail1.Size = New System.Drawing.Size(156, 20)
        Me.txtEmail1.TabIndex = 74
        '
        'cbxSituacion
        '
        Me.cbxSituacion.CampoWhere = Nothing
        Me.cbxSituacion.CargarUnaSolaVez = False
        Me.cbxSituacion.DataDisplayMember = "Descripcion"
        Me.cbxSituacion.DataFilter = Nothing
        Me.cbxSituacion.DataOrderBy = Nothing
        Me.cbxSituacion.DataSource = "vSituacion"
        Me.cbxSituacion.DataValueMember = "ID"
        Me.cbxSituacion.dtSeleccionado = Nothing
        Me.cbxSituacion.FormABM = Nothing
        Me.cbxSituacion.Indicaciones = Nothing
        Me.cbxSituacion.Location = New System.Drawing.Point(93, 275)
        Me.cbxSituacion.Name = "cbxSituacion"
        Me.cbxSituacion.SeleccionMultiple = False
        Me.cbxSituacion.SeleccionObligatoria = False
        Me.cbxSituacion.Size = New System.Drawing.Size(135, 21)
        Me.cbxSituacion.SoloLectura = False
        Me.cbxSituacion.TabIndex = 70
        Me.cbxSituacion.Texto = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(31, 279)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(54, 13)
        Me.Label5.TabIndex = 69
        Me.Label5.Text = "Situacion:"
        '
        'cbxClienteProducto
        '
        Me.cbxClienteProducto.CampoWhere = Nothing
        Me.cbxClienteProducto.CargarUnaSolaVez = False
        Me.cbxClienteProducto.DataDisplayMember = "Descripcion"
        Me.cbxClienteProducto.DataFilter = Nothing
        Me.cbxClienteProducto.DataOrderBy = Nothing
        Me.cbxClienteProducto.DataSource = "VClienteProducto"
        Me.cbxClienteProducto.DataValueMember = "ID"
        Me.cbxClienteProducto.dtSeleccionado = Nothing
        Me.cbxClienteProducto.FormABM = Nothing
        Me.cbxClienteProducto.Indicaciones = Nothing
        Me.cbxClienteProducto.Location = New System.Drawing.Point(92, 251)
        Me.cbxClienteProducto.Name = "cbxClienteProducto"
        Me.cbxClienteProducto.SeleccionMultiple = False
        Me.cbxClienteProducto.SeleccionObligatoria = False
        Me.cbxClienteProducto.Size = New System.Drawing.Size(135, 21)
        Me.cbxClienteProducto.SoloLectura = False
        Me.cbxClienteProducto.TabIndex = 68
        Me.cbxClienteProducto.Texto = ""
        '
        'LblClienteProducto
        '
        Me.LblClienteProducto.AutoSize = True
        Me.LblClienteProducto.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.LblClienteProducto.Location = New System.Drawing.Point(1, 254)
        Me.LblClienteProducto.Name = "LblClienteProducto"
        Me.LblClienteProducto.Size = New System.Drawing.Size(88, 13)
        Me.LblClienteProducto.TabIndex = 67
        Me.LblClienteProducto.Text = "Cliente Producto:"
        '
        'cbxCategoriaCliente
        '
        Me.cbxCategoriaCliente.CampoWhere = Nothing
        Me.cbxCategoriaCliente.CargarUnaSolaVez = False
        Me.cbxCategoriaCliente.DataDisplayMember = "Descripcion"
        Me.cbxCategoriaCliente.DataFilter = Nothing
        Me.cbxCategoriaCliente.DataOrderBy = Nothing
        Me.cbxCategoriaCliente.DataSource = "VTipoCliente"
        Me.cbxCategoriaCliente.DataValueMember = "ID"
        Me.cbxCategoriaCliente.dtSeleccionado = Nothing
        Me.cbxCategoriaCliente.FormABM = "frmTipoCliente"
        Me.cbxCategoriaCliente.Indicaciones = Nothing
        Me.cbxCategoriaCliente.Location = New System.Drawing.Point(93, 227)
        Me.cbxCategoriaCliente.Name = "cbxCategoriaCliente"
        Me.cbxCategoriaCliente.SeleccionMultiple = False
        Me.cbxCategoriaCliente.SeleccionObligatoria = False
        Me.cbxCategoriaCliente.Size = New System.Drawing.Size(135, 21)
        Me.cbxCategoriaCliente.SoloLectura = False
        Me.cbxCategoriaCliente.TabIndex = 66
        Me.cbxCategoriaCliente.Texto = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(32, 230)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 65
        Me.Label4.Text = "Categoria:"
        '
        'cbxAbogado
        '
        Me.cbxAbogado.CampoWhere = Nothing
        Me.cbxAbogado.CargarUnaSolaVez = False
        Me.cbxAbogado.DataDisplayMember = Nothing
        Me.cbxAbogado.DataFilter = Nothing
        Me.cbxAbogado.DataOrderBy = Nothing
        Me.cbxAbogado.DataSource = Nothing
        Me.cbxAbogado.DataValueMember = Nothing
        Me.cbxAbogado.dtSeleccionado = Nothing
        Me.cbxAbogado.Enabled = False
        Me.cbxAbogado.FormABM = Nothing
        Me.cbxAbogado.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxAbogado.Location = New System.Drawing.Point(210, 320)
        Me.cbxAbogado.Name = "cbxAbogado"
        Me.cbxAbogado.SeleccionMultiple = False
        Me.cbxAbogado.SeleccionObligatoria = False
        Me.cbxAbogado.Size = New System.Drawing.Size(282, 21)
        Me.cbxAbogado.SoloLectura = False
        Me.cbxAbogado.TabIndex = 64
        Me.cbxAbogado.Texto = ""
        '
        'chkJudicial
        '
        Me.chkJudicial.AutoSize = True
        Me.chkJudicial.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkJudicial.Enabled = False
        Me.chkJudicial.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.chkJudicial.Location = New System.Drawing.Point(131, 324)
        Me.chkJudicial.Name = "chkJudicial"
        Me.chkJudicial.Size = New System.Drawing.Size(61, 17)
        Me.chkJudicial.TabIndex = 63
        Me.chkJudicial.Text = "Judicial"
        Me.chkJudicial.UseVisualStyleBackColor = True
        '
        'lblCelular
        '
        Me.lblCelular.AutoSize = True
        Me.lblCelular.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblCelular.Location = New System.Drawing.Point(564, 60)
        Me.lblCelular.Name = "lblCelular"
        Me.lblCelular.Size = New System.Drawing.Size(53, 13)
        Me.lblCelular.TabIndex = 62
        Me.lblCelular.Text = "Celulares:"
        '
        'txtCelulares
        '
        Me.txtCelulares.BackColor = System.Drawing.Color.White
        Me.txtCelulares.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCelulares.Color = System.Drawing.Color.Empty
        Me.txtCelulares.Indicaciones = Nothing
        Me.txtCelulares.Location = New System.Drawing.Point(623, 55)
        Me.txtCelulares.Multilinea = False
        Me.txtCelulares.Name = "txtCelulares"
        Me.txtCelulares.Size = New System.Drawing.Size(208, 21)
        Me.txtCelulares.SoloLectura = False
        Me.txtCelulares.TabIndex = 61
        Me.txtCelulares.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCelulares.Texto = ""
        '
        'cbxRuta
        '
        Me.cbxRuta.CampoWhere = Nothing
        Me.cbxRuta.CargarUnaSolaVez = False
        Me.cbxRuta.DataDisplayMember = "Descripcion"
        Me.cbxRuta.DataFilter = Nothing
        Me.cbxRuta.DataOrderBy = "Descripcion"
        Me.cbxRuta.DataSource = "VRuta"
        Me.cbxRuta.DataValueMember = "ID"
        Me.cbxRuta.dtSeleccionado = Nothing
        Me.cbxRuta.FormABM = "frmRuta"
        Me.cbxRuta.Indicaciones = Nothing
        Me.cbxRuta.Location = New System.Drawing.Point(360, 158)
        Me.cbxRuta.Name = "cbxRuta"
        Me.cbxRuta.SeleccionMultiple = False
        Me.cbxRuta.SeleccionObligatoria = False
        Me.cbxRuta.Size = New System.Drawing.Size(156, 21)
        Me.cbxRuta.SoloLectura = False
        Me.cbxRuta.TabIndex = 59
        Me.cbxRuta.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(320, 162)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(33, 13)
        Me.Label1.TabIndex = 60
        Me.Label1.Text = "Ruta:"
        '
        'cbxArea
        '
        Me.cbxArea.CampoWhere = Nothing
        Me.cbxArea.CargarUnaSolaVez = False
        Me.cbxArea.DataDisplayMember = "Descripcion"
        Me.cbxArea.DataFilter = Nothing
        Me.cbxArea.DataOrderBy = "Descripcion"
        Me.cbxArea.DataSource = "VArea"
        Me.cbxArea.DataValueMember = "ID"
        Me.cbxArea.dtSeleccionado = Nothing
        Me.cbxArea.FormABM = "frmZonaVenta"
        Me.cbxArea.Indicaciones = Nothing
        Me.cbxArea.Location = New System.Drawing.Point(93, 158)
        Me.cbxArea.Name = "cbxArea"
        Me.cbxArea.SeleccionMultiple = False
        Me.cbxArea.SeleccionObligatoria = False
        Me.cbxArea.Size = New System.Drawing.Size(135, 21)
        Me.cbxArea.SoloLectura = False
        Me.cbxArea.TabIndex = 56
        Me.cbxArea.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(55, 162)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 13)
        Me.Label2.TabIndex = 58
        Me.Label2.Text = "Area:"
        '
        'chkClienteVario
        '
        Me.chkClienteVario.AutoSize = True
        Me.chkClienteVario.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkClienteVario.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.chkClienteVario.Location = New System.Drawing.Point(305, 295)
        Me.chkClienteVario.Name = "chkClienteVario"
        Me.chkClienteVario.Size = New System.Drawing.Size(106, 17)
        Me.chkClienteVario.TabIndex = 55
        Me.chkClienteVario.Text = "Clientes varios    "
        Me.chkClienteVario.UseVisualStyleBackColor = True
        '
        'PnlCondicionVenta
        '
        Me.PnlCondicionVenta.Controls.Add(Me.rdbContado)
        Me.PnlCondicionVenta.Controls.Add(Me.rdbCredito)
        Me.PnlCondicionVenta.Location = New System.Drawing.Point(623, 133)
        Me.PnlCondicionVenta.Name = "PnlCondicionVenta"
        Me.PnlCondicionVenta.Size = New System.Drawing.Size(208, 24)
        Me.PnlCondicionVenta.TabIndex = 54
        '
        'rdbContado
        '
        Me.rdbContado.AutoSize = True
        Me.rdbContado.BackColor = System.Drawing.Color.Transparent
        Me.rdbContado.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdbContado.Location = New System.Drawing.Point(123, 3)
        Me.rdbContado.Name = "rdbContado"
        Me.rdbContado.Size = New System.Drawing.Size(65, 17)
        Me.rdbContado.TabIndex = 1
        Me.rdbContado.TabStop = True
        Me.rdbContado.Text = "Contado"
        Me.rdbContado.UseVisualStyleBackColor = False
        '
        'rdbCredito
        '
        Me.rdbCredito.AutoSize = True
        Me.rdbCredito.BackColor = System.Drawing.Color.Transparent
        Me.rdbCredito.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdbCredito.Location = New System.Drawing.Point(27, 3)
        Me.rdbCredito.Name = "rdbCredito"
        Me.rdbCredito.Size = New System.Drawing.Size(58, 17)
        Me.rdbCredito.TabIndex = 0
        Me.rdbCredito.TabStop = True
        Me.rdbCredito.Text = "Credito"
        Me.rdbCredito.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.Control
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Button2.Location = New System.Drawing.Point(699, 348)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 25)
        Me.Button2.TabIndex = 32
        Me.Button2.Text = "&Salir"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.SystemColors.Control
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnSalir.Location = New System.Drawing.Point(699, 347)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 25)
        Me.btnSalir.TabIndex = 32
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'btnNuevo
        '
        Me.btnNuevo.BackColor = System.Drawing.SystemColors.Control
        Me.btnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnNuevo.Location = New System.Drawing.Point(93, 348)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 25)
        Me.btnNuevo.TabIndex = 27
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = False
        '
        'btnEditar
        '
        Me.btnEditar.BackColor = System.Drawing.SystemColors.Control
        Me.btnEditar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnEditar.Location = New System.Drawing.Point(174, 348)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 25)
        Me.btnEditar.TabIndex = 28
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = False
        '
        'ocxCuenta
        '
        Me.ocxCuenta.AlturaMaxima = 70
        Me.ocxCuenta.Consulta = Nothing
        Me.ocxCuenta.IDCentroCosto = 0
        Me.ocxCuenta.IDUnidadNegocio = 0
        Me.ocxCuenta.ListarTodas = False
        Me.ocxCuenta.Location = New System.Drawing.Point(360, 269)
        Me.ocxCuenta.Name = "ocxCuenta"
        Me.ocxCuenta.Registro = Nothing
        Me.ocxCuenta.Resolucion173 = False
        Me.ocxCuenta.Seleccionado = False
        Me.ocxCuenta.Size = New System.Drawing.Size(340, 24)
        Me.ocxCuenta.SoloLectura = False
        Me.ocxCuenta.TabIndex = 26
        Me.ocxCuenta.Texto = Nothing
        Me.ocxCuenta.whereFiltro = Nothing
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCuenta.Location = New System.Drawing.Point(304, 276)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 46
        Me.lblCuenta.Text = "Cuenta:"
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.SystemColors.Control
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnGuardar.Location = New System.Drawing.Point(255, 348)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 25)
        Me.btnGuardar.TabIndex = 29
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'cbxTipoCliente
        '
        Me.cbxTipoCliente.CampoWhere = Nothing
        Me.cbxTipoCliente.CargarUnaSolaVez = False
        Me.cbxTipoCliente.DataDisplayMember = "Descripcion"
        Me.cbxTipoCliente.DataFilter = Nothing
        Me.cbxTipoCliente.DataOrderBy = Nothing
        Me.cbxTipoCliente.DataSource = "VTipoCliente"
        Me.cbxTipoCliente.DataValueMember = "ID"
        Me.cbxTipoCliente.dtSeleccionado = Nothing
        Me.cbxTipoCliente.FormABM = "frmTipoCliente"
        Me.cbxTipoCliente.Indicaciones = Nothing
        Me.cbxTipoCliente.Location = New System.Drawing.Point(93, 204)
        Me.cbxTipoCliente.Name = "cbxTipoCliente"
        Me.cbxTipoCliente.SeleccionMultiple = False
        Me.cbxTipoCliente.SeleccionObligatoria = False
        Me.cbxTipoCliente.Size = New System.Drawing.Size(135, 21)
        Me.cbxTipoCliente.SoloLectura = False
        Me.cbxTipoCliente.TabIndex = 25
        Me.cbxTipoCliente.Texto = ""
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.SystemColors.Control
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnCancelar.Location = New System.Drawing.Point(336, 348)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 25)
        Me.btnCancelar.TabIndex = 30
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'cbxListaPrecio
        '
        Me.cbxListaPrecio.CampoWhere = Nothing
        Me.cbxListaPrecio.CargarUnaSolaVez = False
        Me.cbxListaPrecio.DataDisplayMember = "Descripcion"
        Me.cbxListaPrecio.DataFilter = Nothing
        Me.cbxListaPrecio.DataOrderBy = Nothing
        Me.cbxListaPrecio.DataSource = Nothing
        Me.cbxListaPrecio.DataValueMember = "ID"
        Me.cbxListaPrecio.dtSeleccionado = Nothing
        Me.cbxListaPrecio.FormABM = "frmListaPrecio"
        Me.cbxListaPrecio.Indicaciones = Nothing
        Me.cbxListaPrecio.Location = New System.Drawing.Point(93, 181)
        Me.cbxListaPrecio.Name = "cbxListaPrecio"
        Me.cbxListaPrecio.SeleccionMultiple = False
        Me.cbxListaPrecio.SeleccionObligatoria = False
        Me.cbxListaPrecio.Size = New System.Drawing.Size(135, 21)
        Me.cbxListaPrecio.SoloLectura = False
        Me.cbxListaPrecio.TabIndex = 24
        Me.cbxListaPrecio.Texto = ""
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Control
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Button1.Location = New System.Drawing.Point(417, 349)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 25)
        Me.Button1.TabIndex = 31
        Me.Button1.Text = "E&liminar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnEliminar
        '
        Me.btnEliminar.BackColor = System.Drawing.SystemColors.Control
        Me.btnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnEliminar.Location = New System.Drawing.Point(417, 348)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 25)
        Me.btnEliminar.TabIndex = 31
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(623, 161)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(208, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 14
        Me.cbxMoneda.Texto = ""
        '
        'cbxDistribuidor
        '
        Me.cbxDistribuidor.CampoWhere = Nothing
        Me.cbxDistribuidor.CargarUnaSolaVez = False
        Me.cbxDistribuidor.DataDisplayMember = Nothing
        Me.cbxDistribuidor.DataFilter = Nothing
        Me.cbxDistribuidor.DataOrderBy = Nothing
        Me.cbxDistribuidor.DataSource = Nothing
        Me.cbxDistribuidor.DataValueMember = Nothing
        Me.cbxDistribuidor.dtSeleccionado = Nothing
        Me.cbxDistribuidor.FormABM = "frmDistribuidor"
        Me.cbxDistribuidor.Indicaciones = Nothing
        Me.cbxDistribuidor.Location = New System.Drawing.Point(360, 137)
        Me.cbxDistribuidor.Name = "cbxDistribuidor"
        Me.cbxDistribuidor.SeleccionMultiple = False
        Me.cbxDistribuidor.SeleccionObligatoria = False
        Me.cbxDistribuidor.Size = New System.Drawing.Size(156, 21)
        Me.cbxDistribuidor.SoloLectura = False
        Me.cbxDistribuidor.TabIndex = 13
        Me.cbxDistribuidor.Texto = ""
        '
        'cbxCobrador
        '
        Me.cbxCobrador.CampoWhere = Nothing
        Me.cbxCobrador.CargarUnaSolaVez = False
        Me.cbxCobrador.DataDisplayMember = "Nombres"
        Me.cbxCobrador.DataFilter = Nothing
        Me.cbxCobrador.DataOrderBy = Nothing
        Me.cbxCobrador.DataSource = "VCobrador"
        Me.cbxCobrador.DataValueMember = "ID"
        Me.cbxCobrador.dtSeleccionado = Nothing
        Me.cbxCobrador.FormABM = "frmCobrador"
        Me.cbxCobrador.Indicaciones = Nothing
        Me.cbxCobrador.Location = New System.Drawing.Point(360, 116)
        Me.cbxCobrador.Name = "cbxCobrador"
        Me.cbxCobrador.SeleccionMultiple = False
        Me.cbxCobrador.SeleccionObligatoria = False
        Me.cbxCobrador.Size = New System.Drawing.Size(156, 21)
        Me.cbxCobrador.SoloLectura = False
        Me.cbxCobrador.TabIndex = 12
        Me.cbxCobrador.Texto = ""
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = Nothing
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = Nothing
        Me.cbxVendedor.DataSource = Nothing
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.FormABM = "frmVendedor"
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(360, 95)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(156, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 11
        Me.cbxVendedor.Texto = ""
        '
        'cbxPromotor
        '
        Me.cbxPromotor.CampoWhere = Nothing
        Me.cbxPromotor.CargarUnaSolaVez = False
        Me.cbxPromotor.DataDisplayMember = "Nombres"
        Me.cbxPromotor.DataFilter = Nothing
        Me.cbxPromotor.DataOrderBy = Nothing
        Me.cbxPromotor.DataSource = "VPromotor"
        Me.cbxPromotor.DataValueMember = "ID"
        Me.cbxPromotor.dtSeleccionado = Nothing
        Me.cbxPromotor.FormABM = "frmPromotor"
        Me.cbxPromotor.Indicaciones = Nothing
        Me.cbxPromotor.Location = New System.Drawing.Point(360, 74)
        Me.cbxPromotor.Name = "cbxPromotor"
        Me.cbxPromotor.SeleccionMultiple = False
        Me.cbxPromotor.SeleccionObligatoria = False
        Me.cbxPromotor.Size = New System.Drawing.Size(156, 21)
        Me.cbxPromotor.SoloLectura = False
        Me.cbxPromotor.TabIndex = 10
        Me.cbxPromotor.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = "frmSucursal"
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(360, 53)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(156, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 9
        Me.cbxSucursal.Texto = ""
        '
        'cbxZonaVenta
        '
        Me.cbxZonaVenta.CampoWhere = Nothing
        Me.cbxZonaVenta.CargarUnaSolaVez = False
        Me.cbxZonaVenta.DataDisplayMember = "Descripcion"
        Me.cbxZonaVenta.DataFilter = Nothing
        Me.cbxZonaVenta.DataOrderBy = "Descripcion"
        Me.cbxZonaVenta.DataSource = "VZonaVenta"
        Me.cbxZonaVenta.DataValueMember = "ID"
        Me.cbxZonaVenta.dtSeleccionado = Nothing
        Me.cbxZonaVenta.FormABM = "frmZonaVenta"
        Me.cbxZonaVenta.Indicaciones = Nothing
        Me.cbxZonaVenta.Location = New System.Drawing.Point(93, 137)
        Me.cbxZonaVenta.Name = "cbxZonaVenta"
        Me.cbxZonaVenta.SeleccionMultiple = False
        Me.cbxZonaVenta.SeleccionObligatoria = False
        Me.cbxZonaVenta.Size = New System.Drawing.Size(135, 21)
        Me.cbxZonaVenta.SoloLectura = False
        Me.cbxZonaVenta.TabIndex = 8
        Me.cbxZonaVenta.Texto = ""
        '
        'cbxBarrio
        '
        Me.cbxBarrio.CampoWhere = Nothing
        Me.cbxBarrio.CargarUnaSolaVez = False
        Me.cbxBarrio.DataDisplayMember = Nothing
        Me.cbxBarrio.DataFilter = Nothing
        Me.cbxBarrio.DataOrderBy = Nothing
        Me.cbxBarrio.DataSource = Nothing
        Me.cbxBarrio.DataValueMember = Nothing
        Me.cbxBarrio.dtSeleccionado = Nothing
        Me.cbxBarrio.FormABM = Nothing
        Me.cbxBarrio.Indicaciones = Nothing
        Me.cbxBarrio.Location = New System.Drawing.Point(93, 116)
        Me.cbxBarrio.Name = "cbxBarrio"
        Me.cbxBarrio.SeleccionMultiple = False
        Me.cbxBarrio.SeleccionObligatoria = False
        Me.cbxBarrio.Size = New System.Drawing.Size(135, 21)
        Me.cbxBarrio.SoloLectura = False
        Me.cbxBarrio.TabIndex = 7
        Me.cbxBarrio.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(93, 95)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(135, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 6
        Me.cbxCiudad.Texto = ""
        '
        'cbxDepartamento
        '
        Me.cbxDepartamento.CampoWhere = Nothing
        Me.cbxDepartamento.CargarUnaSolaVez = False
        Me.cbxDepartamento.DataDisplayMember = Nothing
        Me.cbxDepartamento.DataFilter = Nothing
        Me.cbxDepartamento.DataOrderBy = Nothing
        Me.cbxDepartamento.DataSource = Nothing
        Me.cbxDepartamento.DataValueMember = Nothing
        Me.cbxDepartamento.dtSeleccionado = Nothing
        Me.cbxDepartamento.FormABM = Nothing
        Me.cbxDepartamento.Indicaciones = Nothing
        Me.cbxDepartamento.Location = New System.Drawing.Point(93, 74)
        Me.cbxDepartamento.Name = "cbxDepartamento"
        Me.cbxDepartamento.SeleccionMultiple = False
        Me.cbxDepartamento.SeleccionObligatoria = False
        Me.cbxDepartamento.Size = New System.Drawing.Size(135, 21)
        Me.cbxDepartamento.SoloLectura = False
        Me.cbxDepartamento.TabIndex = 5
        Me.cbxDepartamento.Texto = ""
        '
        'cbxEstado
        '
        Me.cbxEstado.CampoWhere = Nothing
        Me.cbxEstado.CargarUnaSolaVez = False
        Me.cbxEstado.DataDisplayMember = Nothing
        Me.cbxEstado.DataFilter = Nothing
        Me.cbxEstado.DataOrderBy = Nothing
        Me.cbxEstado.DataSource = Nothing
        Me.cbxEstado.DataValueMember = Nothing
        Me.cbxEstado.dtSeleccionado = Nothing
        Me.cbxEstado.FormABM = Nothing
        Me.cbxEstado.Indicaciones = Nothing
        Me.cbxEstado.Location = New System.Drawing.Point(623, 29)
        Me.cbxEstado.Name = "cbxEstado"
        Me.cbxEstado.SeleccionMultiple = False
        Me.cbxEstado.SeleccionObligatoria = True
        Me.cbxEstado.Size = New System.Drawing.Size(208, 21)
        Me.cbxEstado.SoloLectura = False
        Me.cbxEstado.TabIndex = 3
        Me.cbxEstado.Texto = ""
        '
        'cbxPais
        '
        Me.cbxPais.CampoWhere = Nothing
        Me.cbxPais.CargarUnaSolaVez = False
        Me.cbxPais.DataDisplayMember = Nothing
        Me.cbxPais.DataFilter = Nothing
        Me.cbxPais.DataOrderBy = Nothing
        Me.cbxPais.DataSource = Nothing
        Me.cbxPais.DataValueMember = Nothing
        Me.cbxPais.dtSeleccionado = Nothing
        Me.cbxPais.FormABM = Nothing
        Me.cbxPais.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxPais.Location = New System.Drawing.Point(93, 53)
        Me.cbxPais.Name = "cbxPais"
        Me.cbxPais.SeleccionMultiple = False
        Me.cbxPais.SeleccionObligatoria = False
        Me.cbxPais.Size = New System.Drawing.Size(135, 21)
        Me.cbxPais.SoloLectura = False
        Me.cbxPais.TabIndex = 4
        Me.cbxPais.Texto = ""
        '
        'txtTelefonos
        '
        Me.txtTelefonos.BackColor = System.Drawing.Color.White
        Me.txtTelefonos.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefonos.Color = System.Drawing.Color.Empty
        Me.txtTelefonos.Indicaciones = Nothing
        Me.txtTelefonos.Location = New System.Drawing.Point(623, 7)
        Me.txtTelefonos.Multilinea = False
        Me.txtTelefonos.Name = "txtTelefonos"
        Me.txtTelefonos.Size = New System.Drawing.Size(208, 21)
        Me.txtTelefonos.SoloLectura = False
        Me.txtTelefonos.TabIndex = 1
        Me.txtTelefonos.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTelefonos.Texto = ""
        '
        'txtDireccion
        '
        Me.txtDireccion.BackColor = System.Drawing.Color.White
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Color = System.Drawing.Color.Empty
        Me.txtDireccion.Indicaciones = Nothing
        Me.txtDireccion.Location = New System.Drawing.Point(135, 26)
        Me.txtDireccion.Multilinea = False
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(397, 21)
        Me.txtDireccion.SoloLectura = False
        Me.txtDireccion.TabIndex = 2
        Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccion.Texto = ""
        '
        'txtNombreFantasia
        '
        Me.txtNombreFantasia.BackColor = System.Drawing.Color.White
        Me.txtNombreFantasia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreFantasia.Color = System.Drawing.Color.Empty
        Me.txtNombreFantasia.Indicaciones = Nothing
        Me.txtNombreFantasia.Location = New System.Drawing.Point(135, 5)
        Me.txtNombreFantasia.Multilinea = False
        Me.txtNombreFantasia.Name = "txtNombreFantasia"
        Me.txtNombreFantasia.Size = New System.Drawing.Size(397, 21)
        Me.txtNombreFantasia.SoloLectura = False
        Me.txtNombreFantasia.TabIndex = 0
        Me.txtNombreFantasia.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNombreFantasia.Texto = ""
        '
        'chkIVAExento
        '
        Me.chkIVAExento.AutoSize = True
        Me.chkIVAExento.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkIVAExento.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.chkIVAExento.Location = New System.Drawing.Point(5, 324)
        Me.chkIVAExento.Name = "chkIVAExento"
        Me.chkIVAExento.Size = New System.Drawing.Size(97, 17)
        Me.chkIVAExento.TabIndex = 45
        Me.chkIVAExento.Text = "IVA Exento:     "
        Me.chkIVAExento.UseVisualStyleBackColor = True
        '
        'txtUsuarioModificacion
        '
        Me.txtUsuarioModificacion.BackColor = System.Drawing.Color.White
        Me.txtUsuarioModificacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUsuarioModificacion.Location = New System.Drawing.Point(714, 204)
        Me.txtUsuarioModificacion.Name = "txtUsuarioModificacion"
        Me.txtUsuarioModificacion.ReadOnly = True
        Me.txtUsuarioModificacion.Size = New System.Drawing.Size(117, 20)
        Me.txtUsuarioModificacion.TabIndex = 22
        Me.txtUsuarioModificacion.TabStop = False
        Me.txtUsuarioModificacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtFechaModificacion
        '
        Me.txtFechaModificacion.BackColor = System.Drawing.Color.White
        Me.txtFechaModificacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFechaModificacion.Location = New System.Drawing.Point(623, 204)
        Me.txtFechaModificacion.Name = "txtFechaModificacion"
        Me.txtFechaModificacion.ReadOnly = True
        Me.txtFechaModificacion.Size = New System.Drawing.Size(85, 20)
        Me.txtFechaModificacion.TabIndex = 21
        Me.txtFechaModificacion.TabStop = False
        Me.txtFechaModificacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblModificacion
        '
        Me.lblModificacion.AutoSize = True
        Me.lblModificacion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblModificacion.Location = New System.Drawing.Point(547, 208)
        Me.lblModificacion.Name = "lblModificacion"
        Me.lblModificacion.Size = New System.Drawing.Size(70, 13)
        Me.lblModificacion.TabIndex = 36
        Me.lblModificacion.Text = "Modificacion:"
        '
        'txtFechaUltimaCompra
        '
        Me.txtFechaUltimaCompra.BackColor = System.Drawing.Color.White
        Me.txtFechaUltimaCompra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFechaUltimaCompra.Location = New System.Drawing.Point(623, 225)
        Me.txtFechaUltimaCompra.Name = "txtFechaUltimaCompra"
        Me.txtFechaUltimaCompra.ReadOnly = True
        Me.txtFechaUltimaCompra.Size = New System.Drawing.Size(85, 20)
        Me.txtFechaUltimaCompra.TabIndex = 23
        Me.txtFechaUltimaCompra.TabStop = False
        Me.txtFechaUltimaCompra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblUltimaCompra
        '
        Me.lblUltimaCompra.AutoSize = True
        Me.lblUltimaCompra.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblUltimaCompra.Location = New System.Drawing.Point(555, 229)
        Me.lblUltimaCompra.Name = "lblUltimaCompra"
        Me.lblUltimaCompra.Size = New System.Drawing.Size(62, 13)
        Me.lblUltimaCompra.TabIndex = 39
        Me.lblUltimaCompra.Text = "Ul. Compra:"
        '
        'txtUsuarioAlta
        '
        Me.txtUsuarioAlta.BackColor = System.Drawing.Color.White
        Me.txtUsuarioAlta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUsuarioAlta.Location = New System.Drawing.Point(714, 183)
        Me.txtUsuarioAlta.Name = "txtUsuarioAlta"
        Me.txtUsuarioAlta.ReadOnly = True
        Me.txtUsuarioAlta.Size = New System.Drawing.Size(117, 20)
        Me.txtUsuarioAlta.TabIndex = 20
        Me.txtUsuarioAlta.TabStop = False
        Me.txtUsuarioAlta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtFechaAlta
        '
        Me.txtFechaAlta.BackColor = System.Drawing.Color.White
        Me.txtFechaAlta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFechaAlta.Location = New System.Drawing.Point(623, 183)
        Me.txtFechaAlta.Name = "txtFechaAlta"
        Me.txtFechaAlta.ReadOnly = True
        Me.txtFechaAlta.Size = New System.Drawing.Size(85, 20)
        Me.txtFechaAlta.TabIndex = 15
        Me.txtFechaAlta.TabStop = False
        Me.txtFechaAlta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ToolTip1.SetToolTip(Me.txtFechaAlta, "Fecha de Alta del Cliente")
        '
        'lblAlta
        '
        Me.lblAlta.AutoSize = True
        Me.lblAlta.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblAlta.Location = New System.Drawing.Point(589, 187)
        Me.lblAlta.Name = "lblAlta"
        Me.lblAlta.Size = New System.Drawing.Size(28, 13)
        Me.lblAlta.TabIndex = 33
        Me.lblAlta.Text = "Alta:"
        '
        'lblDistribuidor
        '
        Me.lblDistribuidor.AutoSize = True
        Me.lblDistribuidor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblDistribuidor.Location = New System.Drawing.Point(290, 141)
        Me.lblDistribuidor.Name = "lblDistribuidor"
        Me.lblDistribuidor.Size = New System.Drawing.Size(62, 13)
        Me.lblDistribuidor.TabIndex = 26
        Me.lblDistribuidor.Text = "Distribuidor:"
        '
        'lblCobrador
        '
        Me.lblCobrador.AutoSize = True
        Me.lblCobrador.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblCobrador.Location = New System.Drawing.Point(299, 120)
        Me.lblCobrador.Name = "lblCobrador"
        Me.lblCobrador.Size = New System.Drawing.Size(53, 13)
        Me.lblCobrador.TabIndex = 24
        Me.lblCobrador.Text = "Cobrador:"
        '
        'lblVendedor
        '
        Me.lblVendedor.AutoSize = True
        Me.lblVendedor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblVendedor.Location = New System.Drawing.Point(296, 99)
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Size = New System.Drawing.Size(56, 13)
        Me.lblVendedor.TabIndex = 22
        Me.lblVendedor.Text = "Vendedor:"
        '
        'lblPromotor
        '
        Me.lblPromotor.AutoSize = True
        Me.lblPromotor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblPromotor.Location = New System.Drawing.Point(300, 78)
        Me.lblPromotor.Name = "lblPromotor"
        Me.lblPromotor.Size = New System.Drawing.Size(52, 13)
        Me.lblPromotor.TabIndex = 20
        Me.lblPromotor.Text = "Promotor:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblSucursal.Location = New System.Drawing.Point(301, 57)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 18
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblMoneda.Location = New System.Drawing.Point(568, 165)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 31
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblCondicionVenta
        '
        Me.lblCondicionVenta.AutoSize = True
        Me.lblCondicionVenta.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblCondicionVenta.Location = New System.Drawing.Point(548, 138)
        Me.lblCondicionVenta.Name = "lblCondicionVenta"
        Me.lblCondicionVenta.Size = New System.Drawing.Size(69, 13)
        Me.lblCondicionVenta.TabIndex = 28
        Me.lblCondicionVenta.Text = "Cond. Venta:"
        '
        'lblTipoCliente
        '
        Me.lblTipoCliente.AutoSize = True
        Me.lblTipoCliente.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblTipoCliente.Location = New System.Drawing.Point(21, 204)
        Me.lblTipoCliente.Name = "lblTipoCliente"
        Me.lblTipoCliente.Size = New System.Drawing.Size(66, 13)
        Me.lblTipoCliente.TabIndex = 43
        Me.lblTipoCliente.Text = "Tipo Cliente:"
        '
        'lblListaPrecio
        '
        Me.lblListaPrecio.AutoSize = True
        Me.lblListaPrecio.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblListaPrecio.Location = New System.Drawing.Point(2, 185)
        Me.lblListaPrecio.Name = "lblListaPrecio"
        Me.lblListaPrecio.Size = New System.Drawing.Size(85, 13)
        Me.lblListaPrecio.TabIndex = 41
        Me.lblListaPrecio.Text = "Lista de Precios:"
        '
        'lblZonaVenta
        '
        Me.lblZonaVenta.AutoSize = True
        Me.lblZonaVenta.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblZonaVenta.Location = New System.Drawing.Point(6, 141)
        Me.lblZonaVenta.Name = "lblZonaVenta"
        Me.lblZonaVenta.Size = New System.Drawing.Size(81, 13)
        Me.lblZonaVenta.TabIndex = 16
        Me.lblZonaVenta.Text = "Zona de Venta:"
        '
        'lblBarrio
        '
        Me.lblBarrio.AutoSize = True
        Me.lblBarrio.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblBarrio.Location = New System.Drawing.Point(50, 120)
        Me.lblBarrio.Name = "lblBarrio"
        Me.lblBarrio.Size = New System.Drawing.Size(37, 13)
        Me.lblBarrio.TabIndex = 14
        Me.lblBarrio.Text = "Barrio:"
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblCiudad.Location = New System.Drawing.Point(44, 99)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(43, 13)
        Me.lblCiudad.TabIndex = 12
        Me.lblCiudad.Text = "Ciudad:"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblDepartamento.Location = New System.Drawing.Point(10, 78)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(77, 13)
        Me.lblDepartamento.TabIndex = 10
        Me.lblDepartamento.Text = "Departamento:"
        '
        'lblPais
        '
        Me.lblPais.AutoSize = True
        Me.lblPais.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblPais.Location = New System.Drawing.Point(55, 57)
        Me.lblPais.Name = "lblPais"
        Me.lblPais.Size = New System.Drawing.Size(32, 13)
        Me.lblPais.TabIndex = 8
        Me.lblPais.Text = "País:"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblEstado.Location = New System.Drawing.Point(574, 33)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 6
        Me.lblEstado.Text = "Estado:"
        '
        'lblTelefonos
        '
        Me.lblTelefonos.AutoSize = True
        Me.lblTelefonos.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblTelefonos.Location = New System.Drawing.Point(560, 11)
        Me.lblTelefonos.Name = "lblTelefonos"
        Me.lblTelefonos.Size = New System.Drawing.Size(57, 13)
        Me.lblTelefonos.TabIndex = 2
        Me.lblTelefonos.Text = "Telefonos:"
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblDireccion.Location = New System.Drawing.Point(6, 32)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(85, 13)
        Me.lblDireccion.TabIndex = 4
        Me.lblDireccion.Text = "Direccion Envio:"
        '
        'lblNombreFantasia
        '
        Me.lblNombreFantasia.AutoSize = True
        Me.lblNombreFantasia.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblNombreFantasia.Location = New System.Drawing.Point(6, 11)
        Me.lblNombreFantasia.Name = "lblNombreFantasia"
        Me.lblNombreFantasia.Size = New System.Drawing.Size(90, 13)
        Me.lblNombreFantasia.TabIndex = 0
        Me.lblNombreFantasia.Text = "Nombre Fantasia:"
        '
        'tabCredito
        '
        Me.tabCredito.Controls.Add(Me.chkBoleta)
        Me.tabCredito.Controls.Add(Me.lklVerCatastro)
        Me.tabCredito.Controls.Add(Me.lvlSaldo)
        Me.tabCredito.Controls.Add(Me.lblDeuda)
        Me.tabCredito.Controls.Add(Me.lklRecalcularCredito)
        Me.tabCredito.Controls.Add(Me.lblPlazoCheque)
        Me.tabCredito.Controls.Add(Me.lblPlazoCobro)
        Me.tabCredito.Controls.Add(Me.lblPlazoCredito)
        Me.tabCredito.Controls.Add(Me.lblDescuentoSimbolo)
        Me.tabCredito.Controls.Add(Me.lblDesuento)
        Me.tabCredito.Controls.Add(Me.lblLimiteCredito)
        Me.tabCredito.Controls.Add(Me.txtSaldo)
        Me.tabCredito.Controls.Add(Me.txtDeuda)
        Me.tabCredito.Controls.Add(Me.txtPlazoCheque)
        Me.tabCredito.Controls.Add(Me.txtPlazoCobro)
        Me.tabCredito.Controls.Add(Me.txtPlazoCredito)
        Me.tabCredito.Controls.Add(Me.txtDescuento)
        Me.tabCredito.Controls.Add(Me.txtLimiteCredito)
        Me.tabCredito.Location = New System.Drawing.Point(4, 22)
        Me.tabCredito.Name = "tabCredito"
        Me.tabCredito.Size = New System.Drawing.Size(859, 386)
        Me.tabCredito.TabIndex = 4
        Me.tabCredito.Text = "Credito"
        Me.tabCredito.UseVisualStyleBackColor = True
        '
        'chkBoleta
        '
        Me.chkBoleta.BackColor = System.Drawing.Color.Transparent
        Me.chkBoleta.Color = System.Drawing.Color.Empty
        Me.chkBoleta.Location = New System.Drawing.Point(23, 141)
        Me.chkBoleta.Name = "chkBoleta"
        Me.chkBoleta.Size = New System.Drawing.Size(263, 21)
        Me.chkBoleta.SoloLectura = False
        Me.chkBoleta.TabIndex = 17
        Me.chkBoleta.Texto = "Boleta contra Boleta"
        Me.chkBoleta.Valor = False
        '
        'lklVerCatastro
        '
        Me.lklVerCatastro.AutoSize = True
        Me.lklVerCatastro.Location = New System.Drawing.Point(282, 71)
        Me.lklVerCatastro.Name = "lklVerCatastro"
        Me.lklVerCatastro.Size = New System.Drawing.Size(65, 13)
        Me.lklVerCatastro.TabIndex = 16
        Me.lklVerCatastro.TabStop = True
        Me.lklVerCatastro.Text = "Ver Catastro"
        '
        'lvlSaldo
        '
        Me.lvlSaldo.AutoSize = True
        Me.lvlSaldo.Location = New System.Drawing.Point(20, 71)
        Me.lvlSaldo.Name = "lvlSaldo"
        Me.lvlSaldo.Size = New System.Drawing.Size(37, 13)
        Me.lvlSaldo.TabIndex = 14
        Me.lvlSaldo.Text = "Saldo:"
        '
        'lblDeuda
        '
        Me.lblDeuda.AutoSize = True
        Me.lblDeuda.Location = New System.Drawing.Point(20, 45)
        Me.lblDeuda.Name = "lblDeuda"
        Me.lblDeuda.Size = New System.Drawing.Size(42, 13)
        Me.lblDeuda.TabIndex = 2
        Me.lblDeuda.Text = "Deuda:"
        '
        'lklRecalcularCredito
        '
        Me.lklRecalcularCredito.AutoSize = True
        Me.lklRecalcularCredito.Location = New System.Drawing.Point(282, 45)
        Me.lklRecalcularCredito.Name = "lklRecalcularCredito"
        Me.lklRecalcularCredito.Size = New System.Drawing.Size(93, 13)
        Me.lklRecalcularCredito.TabIndex = 4
        Me.lklRecalcularCredito.TabStop = True
        Me.lklRecalcularCredito.Text = "Recalcular credito"
        '
        'lblPlazoCheque
        '
        Me.lblPlazoCheque.AutoSize = True
        Me.lblPlazoCheque.Location = New System.Drawing.Point(20, 118)
        Me.lblPlazoCheque.Name = "lblPlazoCheque"
        Me.lblPlazoCheque.Size = New System.Drawing.Size(115, 13)
        Me.lblPlazoCheque.TabIndex = 7
        Me.lblPlazoCheque.Text = "Plazo Cheque Diferido:"
        '
        'lblPlazoCobro
        '
        Me.lblPlazoCobro.AutoSize = True
        Me.lblPlazoCobro.Location = New System.Drawing.Point(177, 118)
        Me.lblPlazoCobro.Name = "lblPlazoCobro"
        Me.lblPlazoCobro.Size = New System.Drawing.Size(67, 13)
        Me.lblPlazoCobro.TabIndex = 12
        Me.lblPlazoCobro.Text = "Plazo Cobro:"
        '
        'lblPlazoCredito
        '
        Me.lblPlazoCredito.AutoSize = True
        Me.lblPlazoCredito.Location = New System.Drawing.Point(20, 98)
        Me.lblPlazoCredito.Name = "lblPlazoCredito"
        Me.lblPlazoCredito.Size = New System.Drawing.Size(72, 13)
        Me.lblPlazoCredito.TabIndex = 5
        Me.lblPlazoCredito.Text = "Plazo Credito:"
        '
        'lblDescuentoSimbolo
        '
        Me.lblDescuentoSimbolo.AutoSize = True
        Me.lblDescuentoSimbolo.Location = New System.Drawing.Point(282, 98)
        Me.lblDescuentoSimbolo.Name = "lblDescuentoSimbolo"
        Me.lblDescuentoSimbolo.Size = New System.Drawing.Size(15, 13)
        Me.lblDescuentoSimbolo.TabIndex = 11
        Me.lblDescuentoSimbolo.Text = "%"
        '
        'lblDesuento
        '
        Me.lblDesuento.AutoSize = True
        Me.lblDesuento.Location = New System.Drawing.Point(182, 98)
        Me.lblDesuento.Name = "lblDesuento"
        Me.lblDesuento.Size = New System.Drawing.Size(62, 13)
        Me.lblDesuento.TabIndex = 9
        Me.lblDesuento.Text = "Descuento:"
        '
        'lblLimiteCredito
        '
        Me.lblLimiteCredito.AutoSize = True
        Me.lblLimiteCredito.Location = New System.Drawing.Point(20, 18)
        Me.lblLimiteCredito.Name = "lblLimiteCredito"
        Me.lblLimiteCredito.Size = New System.Drawing.Size(73, 13)
        Me.lblLimiteCredito.TabIndex = 0
        Me.lblLimiteCredito.Text = "Limite Crédito:"
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(141, 67)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(135, 21)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 15
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'txtDeuda
        '
        Me.txtDeuda.Color = System.Drawing.Color.Empty
        Me.txtDeuda.Decimales = True
        Me.txtDeuda.Indicaciones = Nothing
        Me.txtDeuda.Location = New System.Drawing.Point(141, 41)
        Me.txtDeuda.Name = "txtDeuda"
        Me.txtDeuda.Size = New System.Drawing.Size(135, 21)
        Me.txtDeuda.SoloLectura = True
        Me.txtDeuda.TabIndex = 3
        Me.txtDeuda.TabStop = False
        Me.txtDeuda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDeuda.Texto = "0"
        '
        'txtPlazoCheque
        '
        Me.txtPlazoCheque.Color = System.Drawing.Color.Empty
        Me.txtPlazoCheque.Decimales = True
        Me.txtPlazoCheque.Indicaciones = Nothing
        Me.txtPlazoCheque.Location = New System.Drawing.Point(141, 114)
        Me.txtPlazoCheque.Name = "txtPlazoCheque"
        Me.txtPlazoCheque.Size = New System.Drawing.Size(26, 21)
        Me.txtPlazoCheque.SoloLectura = False
        Me.txtPlazoCheque.TabIndex = 8
        Me.txtPlazoCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPlazoCheque.Texto = "0"
        '
        'txtPlazoCobro
        '
        Me.txtPlazoCobro.Color = System.Drawing.Color.Empty
        Me.txtPlazoCobro.Decimales = True
        Me.txtPlazoCobro.Indicaciones = Nothing
        Me.txtPlazoCobro.Location = New System.Drawing.Point(250, 114)
        Me.txtPlazoCobro.Name = "txtPlazoCobro"
        Me.txtPlazoCobro.Size = New System.Drawing.Size(26, 21)
        Me.txtPlazoCobro.SoloLectura = False
        Me.txtPlazoCobro.TabIndex = 13
        Me.txtPlazoCobro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPlazoCobro.Texto = "0"
        '
        'txtPlazoCredito
        '
        Me.txtPlazoCredito.Color = System.Drawing.Color.Empty
        Me.txtPlazoCredito.Decimales = True
        Me.txtPlazoCredito.Indicaciones = Nothing
        Me.txtPlazoCredito.Location = New System.Drawing.Point(141, 94)
        Me.txtPlazoCredito.Name = "txtPlazoCredito"
        Me.txtPlazoCredito.Size = New System.Drawing.Size(26, 21)
        Me.txtPlazoCredito.SoloLectura = False
        Me.txtPlazoCredito.TabIndex = 6
        Me.txtPlazoCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPlazoCredito.Texto = "0"
        '
        'txtDescuento
        '
        Me.txtDescuento.Color = System.Drawing.Color.Empty
        Me.txtDescuento.Decimales = True
        Me.txtDescuento.Indicaciones = Nothing
        Me.txtDescuento.Location = New System.Drawing.Point(250, 94)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(26, 21)
        Me.txtDescuento.SoloLectura = False
        Me.txtDescuento.TabIndex = 10
        Me.txtDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDescuento.Texto = "0"
        '
        'txtLimiteCredito
        '
        Me.txtLimiteCredito.Color = System.Drawing.Color.Empty
        Me.txtLimiteCredito.Decimales = True
        Me.txtLimiteCredito.Indicaciones = Nothing
        Me.txtLimiteCredito.Location = New System.Drawing.Point(141, 14)
        Me.txtLimiteCredito.Name = "txtLimiteCredito"
        Me.txtLimiteCredito.Size = New System.Drawing.Size(135, 21)
        Me.txtLimiteCredito.SoloLectura = False
        Me.txtLimiteCredito.TabIndex = 1
        Me.txtLimiteCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLimiteCredito.Texto = "0"
        '
        'tabDatosAdicionales
        '
        Me.tabDatosAdicionales.Controls.Add(Me.TableLayoutPanel2)
        Me.tabDatosAdicionales.Location = New System.Drawing.Point(4, 22)
        Me.tabDatosAdicionales.Name = "tabDatosAdicionales"
        Me.tabDatosAdicionales.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDatosAdicionales.Size = New System.Drawing.Size(859, 386)
        Me.tabDatosAdicionales.TabIndex = 1
        Me.tabDatosAdicionales.Text = "Datos Adicionales"
        Me.tabDatosAdicionales.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.OcxClienteContacto1, 0, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(853, 380)
        Me.TableLayoutPanel2.TabIndex = 7
        '
        'OcxClienteContacto1
        '
        Me.OcxClienteContacto1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxClienteContacto1.IDCliente = 0
        Me.OcxClienteContacto1.Iniciado = True
        Me.OcxClienteContacto1.Location = New System.Drawing.Point(3, 11)
        Me.OcxClienteContacto1.Name = "OcxClienteContacto1"
        Me.OcxClienteContacto1.Size = New System.Drawing.Size(847, 366)
        Me.OcxClienteContacto1.TabIndex = 6
        '
        'tabSucursales
        '
        Me.tabSucursales.Controls.Add(Me.OcxSucursalesClientes1)
        Me.tabSucursales.Location = New System.Drawing.Point(4, 22)
        Me.tabSucursales.Name = "tabSucursales"
        Me.tabSucursales.Size = New System.Drawing.Size(859, 386)
        Me.tabSucursales.TabIndex = 2
        Me.tabSucursales.Text = "Sucursales"
        Me.tabSucursales.UseVisualStyleBackColor = True
        '
        'OcxSucursalesClientes1
        '
        Me.OcxSucursalesClientes1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxSucursalesClientes1.IDCliente = 0
        Me.OcxSucursalesClientes1.IDPais = 0
        Me.OcxSucursalesClientes1.Iniciado = True
        Me.OcxSucursalesClientes1.Location = New System.Drawing.Point(0, 0)
        Me.OcxSucursalesClientes1.Name = "OcxSucursalesClientes1"
        Me.OcxSucursalesClientes1.Size = New System.Drawing.Size(859, 386)
        Me.OcxSucursalesClientes1.TabIndex = 0
        '
        'tabLocalizacion
        '
        Me.tabLocalizacion.Controls.Add(Me.OcxClientesMapa1)
        Me.tabLocalizacion.Location = New System.Drawing.Point(4, 22)
        Me.tabLocalizacion.Name = "tabLocalizacion"
        Me.tabLocalizacion.Size = New System.Drawing.Size(859, 386)
        Me.tabLocalizacion.TabIndex = 3
        Me.tabLocalizacion.Text = "Localización"
        Me.tabLocalizacion.UseVisualStyleBackColor = True
        '
        'OcxClientesMapa1
        '
        Me.OcxClientesMapa1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxClientesMapa1.dt = Nothing
        Me.OcxClientesMapa1.ID = 0
        Me.OcxClientesMapa1.Iniciado = False
        Me.OcxClientesMapa1.Latitud = New Decimal(New Integer() {25262551, 0, 0, -2147483648})
        Me.OcxClientesMapa1.Location = New System.Drawing.Point(0, 0)
        Me.OcxClientesMapa1.Longitud = New Decimal(New Integer() {57593911, 0, 0, -2147483648})
        Me.OcxClientesMapa1.Name = "OcxClientesMapa1"
        Me.OcxClientesMapa1.Size = New System.Drawing.Size(859, 386)
        Me.OcxClientesMapa1.TabIndex = 0
        '
        'tabFirma
        '
        Me.tabFirma.Controls.Add(Me.OcxRegistroFirmas1)
        Me.tabFirma.Location = New System.Drawing.Point(4, 22)
        Me.tabFirma.Name = "tabFirma"
        Me.tabFirma.Size = New System.Drawing.Size(859, 386)
        Me.tabFirma.TabIndex = 5
        Me.tabFirma.Text = "Firmas"
        Me.tabFirma.UseVisualStyleBackColor = True
        '
        'OcxRegistroFirmas1
        '
        Me.OcxRegistroFirmas1.IDCliente = 0
        Me.OcxRegistroFirmas1.Iniciado = False
        Me.OcxRegistroFirmas1.Location = New System.Drawing.Point(4, 3)
        Me.OcxRegistroFirmas1.Name = "OcxRegistroFirmas1"
        Me.OcxRegistroFirmas1.Size = New System.Drawing.Size(779, 350)
        Me.OcxRegistroFirmas1.TabIndex = 0
        '
        'tabPrecios
        '
        Me.tabPrecios.Controls.Add(Me.Panel2)
        Me.tabPrecios.Location = New System.Drawing.Point(4, 22)
        Me.tabPrecios.Name = "tabPrecios"
        Me.tabPrecios.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPrecios.Size = New System.Drawing.Size(859, 386)
        Me.tabPrecios.TabIndex = 6
        Me.tabPrecios.Text = "Precios"
        Me.tabPrecios.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.AutoScroll = True
        Me.Panel2.Controls.Add(Me.OcxClientePrecio1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(853, 380)
        Me.Panel2.TabIndex = 0
        '
        'OcxClientePrecio1
        '
        Me.OcxClientePrecio1.AutoScroll = True
        Me.OcxClientePrecio1.IDCliente = 0
        Me.OcxClientePrecio1.Location = New System.Drawing.Point(0, 39)
        Me.OcxClientePrecio1.Margin = New System.Windows.Forms.Padding(0)
        Me.OcxClientePrecio1.Name = "OcxClientePrecio1"
        Me.OcxClientePrecio1.RowCliente = Nothing
        Me.OcxClientePrecio1.Size = New System.Drawing.Size(886, 654)
        Me.OcxClientePrecio1.TabIndex = 0
        '
        'tabKMEstimado
        '
        Me.tabKMEstimado.BackColor = System.Drawing.SystemColors.Control
        Me.tabKMEstimado.Controls.Add(Me.OcxKMEstimadoMatriz1)
        Me.tabKMEstimado.Location = New System.Drawing.Point(4, 22)
        Me.tabKMEstimado.Name = "tabKMEstimado"
        Me.tabKMEstimado.Padding = New System.Windows.Forms.Padding(3)
        Me.tabKMEstimado.Size = New System.Drawing.Size(859, 386)
        Me.tabKMEstimado.TabIndex = 7
        Me.tabKMEstimado.Text = "KIlometraje Estimado Matriz"
        '
        'OcxKMEstimadoMatriz1
        '
        Me.OcxKMEstimadoMatriz1.IDCliente = 0
        Me.OcxKMEstimadoMatriz1.Iniciado = False
        Me.OcxKMEstimadoMatriz1.Location = New System.Drawing.Point(8, 6)
        Me.OcxKMEstimadoMatriz1.Name = "OcxKMEstimadoMatriz1"
        Me.OcxKMEstimadoMatriz1.Size = New System.Drawing.Size(284, 241)
        Me.OcxKMEstimadoMatriz1.TabIndex = 0
        '
        'tabKMEstimadoSucursal
        '
        Me.tabKMEstimadoSucursal.BackColor = System.Drawing.SystemColors.Control
        Me.tabKMEstimadoSucursal.Controls.Add(Me.OcxKMEstimadoSucursal1)
        Me.tabKMEstimadoSucursal.Location = New System.Drawing.Point(4, 22)
        Me.tabKMEstimadoSucursal.Name = "tabKMEstimadoSucursal"
        Me.tabKMEstimadoSucursal.Padding = New System.Windows.Forms.Padding(3)
        Me.tabKMEstimadoSucursal.Size = New System.Drawing.Size(859, 386)
        Me.tabKMEstimadoSucursal.TabIndex = 8
        Me.tabKMEstimadoSucursal.Text = "Kilometraje Estimado Sucursal"
        '
        'OcxKMEstimadoSucursal1
        '
        Me.OcxKMEstimadoSucursal1.IDCliente = 0
        Me.OcxKMEstimadoSucursal1.Iniciado = False
        Me.OcxKMEstimadoSucursal1.Location = New System.Drawing.Point(8, 6)
        Me.OcxKMEstimadoSucursal1.Name = "OcxKMEstimadoSucursal1"
        Me.OcxKMEstimadoSucursal1.Size = New System.Drawing.Size(642, 309)
        Me.OcxKMEstimadoSucursal1.TabIndex = 0
        '
        'btnBuscar
        '
        Me.btnBuscar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnBuscar.Location = New System.Drawing.Point(802, 13)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(58, 23)
        Me.btnBuscar.TabIndex = 4
        Me.btnBuscar.Text = "&Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'lblRUC
        '
        Me.lblRUC.AutoSize = True
        Me.lblRUC.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRUC.Location = New System.Drawing.Point(496, 17)
        Me.lblRUC.Name = "lblRUC"
        Me.lblRUC.Size = New System.Drawing.Size(33, 13)
        Me.lblRUC.TabIndex = 4
        Me.lblRUC.Text = "RUC:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.SystemColors.Control
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 493)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.StatusStrip1.Size = New System.Drawing.Size(867, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.tcGeneral, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 81.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(867, 493)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.OcxCBX1)
        Me.Panel1.Controls.Add(Me.btnConsultarRUC)
        Me.Panel1.Controls.Add(Me.chkCI)
        Me.Panel1.Controls.Add(Me.lblReferencia)
        Me.Panel1.Controls.Add(Me.txtReferencia)
        Me.Panel1.Controls.Add(Me.lblID)
        Me.Panel1.Controls.Add(Me.lblRazonSocial)
        Me.Panel1.Controls.Add(Me.txtRUC)
        Me.Panel1.Controls.Add(Me.btnBuscar)
        Me.Panel1.Controls.Add(Me.txtRazonSocial)
        Me.Panel1.Controls.Add(Me.lblRUC)
        Me.Panel1.Controls.Add(Me.txtID)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(861, 75)
        Me.Panel1.TabIndex = 0
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(437, 45)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(102, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Tipo de documento:"
        '
        'OcxCBX1
        '
        Me.OcxCBX1.CampoWhere = Nothing
        Me.OcxCBX1.CargarUnaSolaVez = False
        Me.OcxCBX1.DataDisplayMember = Nothing
        Me.OcxCBX1.DataFilter = Nothing
        Me.OcxCBX1.DataOrderBy = Nothing
        Me.OcxCBX1.DataSource = Nothing
        Me.OcxCBX1.DataValueMember = Nothing
        Me.OcxCBX1.dtSeleccionado = Nothing
        Me.OcxCBX1.FormABM = Nothing
        Me.OcxCBX1.Indicaciones = Nothing
        Me.OcxCBX1.Location = New System.Drawing.Point(545, 40)
        Me.OcxCBX1.Name = "OcxCBX1"
        Me.OcxCBX1.SeleccionMultiple = False
        Me.OcxCBX1.SeleccionObligatoria = False
        Me.OcxCBX1.Size = New System.Drawing.Size(59, 23)
        Me.OcxCBX1.SoloLectura = False
        Me.OcxCBX1.TabIndex = 9
        Me.OcxCBX1.Texto = ""
        '
        'btnConsultarRUC
        '
        Me.btnConsultarRUC.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnConsultarRUC.Location = New System.Drawing.Point(610, 12)
        Me.btnConsultarRUC.Name = "btnConsultarRUC"
        Me.btnConsultarRUC.Size = New System.Drawing.Size(88, 23)
        Me.btnConsultarRUC.TabIndex = 8
        Me.btnConsultarRUC.Text = "&Consultar RUC"
        Me.btnConsultarRUC.UseVisualStyleBackColor = True
        '
        'chkCI
        '
        Me.chkCI.BackColor = System.Drawing.Color.Transparent
        Me.chkCI.Color = System.Drawing.Color.Empty
        Me.chkCI.Location = New System.Drawing.Point(455, 13)
        Me.chkCI.Name = "chkCI"
        Me.chkCI.Size = New System.Drawing.Size(34, 21)
        Me.chkCI.SoloLectura = False
        Me.chkCI.TabIndex = 6
        Me.chkCI.Texto = "CI"
        Me.chkCI.Valor = False
        '
        'lblReferencia
        '
        Me.lblReferencia.AutoSize = True
        Me.lblReferencia.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblReferencia.Location = New System.Drawing.Point(2, 17)
        Me.lblReferencia.Name = "lblReferencia"
        Me.lblReferencia.Size = New System.Drawing.Size(27, 13)
        Me.lblReferencia.TabIndex = 0
        Me.lblReferencia.Text = "Ref:"
        '
        'txtReferencia
        '
        Me.txtReferencia.BackColor = System.Drawing.Color.White
        Me.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferencia.Color = System.Drawing.Color.Empty
        Me.txtReferencia.Indicaciones = "(F1) Para busqueda avanzada"
        Me.txtReferencia.Location = New System.Drawing.Point(29, 13)
        Me.txtReferencia.Multilinea = False
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Size = New System.Drawing.Size(58, 21)
        Me.txtReferencia.SoloLectura = False
        Me.txtReferencia.TabIndex = 0
        Me.txtReferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferencia.Texto = ""
        '
        'txtRUC
        '
        Me.txtRUC.BackColor = System.Drawing.Color.White
        Me.txtRUC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRUC.Color = System.Drawing.Color.Empty
        Me.txtRUC.Indicaciones = Nothing
        Me.txtRUC.Location = New System.Drawing.Point(529, 13)
        Me.txtRUC.Multilinea = False
        Me.txtRUC.Name = "txtRUC"
        Me.txtRUC.Size = New System.Drawing.Size(75, 21)
        Me.txtRUC.SoloLectura = False
        Me.txtRUC.TabIndex = 2
        Me.txtRUC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtRUC.Texto = ""
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.BackColor = System.Drawing.Color.White
        Me.txtRazonSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazonSocial.Color = System.Drawing.Color.Empty
        Me.txtRazonSocial.Indicaciones = "(F1) Para busqueda avanzada"
        Me.txtRazonSocial.Location = New System.Drawing.Point(160, 13)
        Me.txtRazonSocial.Multilinea = False
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.Size = New System.Drawing.Size(289, 21)
        Me.txtRazonSocial.SoloLectura = False
        Me.txtRazonSocial.TabIndex = 1
        Me.txtRazonSocial.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRazonSocial.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(747, 15)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(52, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 3
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(867, 515)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.KeyPreview = True
        Me.Name = "frmCliente"
        Me.Tag = "CLIENTE"
        Me.Text = "frmCliente"
        Me.tcGeneral.ResumeLayout(False)
        Me.tabDatosComerciales.ResumeLayout(False)
        Me.tabDatosComerciales.PerformLayout()
        Me.PnlCondicionVenta.ResumeLayout(False)
        Me.PnlCondicionVenta.PerformLayout()
        Me.tabCredito.ResumeLayout(False)
        Me.tabCredito.PerformLayout()
        Me.tabDatosAdicionales.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.tabSucursales.ResumeLayout(False)
        Me.tabLocalizacion.ResumeLayout(False)
        Me.tabFirma.ResumeLayout(False)
        Me.tabPrecios.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.tabKMEstimado.ResumeLayout(False)
        Me.tabKMEstimadoSucursal.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents lblRazonSocial As System.Windows.Forms.Label
    Friend WithEvents tcGeneral As System.Windows.Forms.TabControl
    Friend WithEvents tabDatosComerciales As System.Windows.Forms.TabPage
    Friend WithEvents tabDatosAdicionales As System.Windows.Forms.TabPage
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents tabSucursales As System.Windows.Forms.TabPage
    Friend WithEvents tabLocalizacion As System.Windows.Forms.TabPage
    Friend WithEvents lblRUC As System.Windows.Forms.Label
    Friend WithEvents lblTelefonos As System.Windows.Forms.Label
    Friend WithEvents lblDireccion As System.Windows.Forms.Label
    Friend WithEvents lblNombreFantasia As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents lblPais As System.Windows.Forms.Label
    Friend WithEvents lblZonaVenta As System.Windows.Forms.Label
    Friend WithEvents lblBarrio As System.Windows.Forms.Label
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents lblTipoCliente As System.Windows.Forms.Label
    Friend WithEvents lblListaPrecio As System.Windows.Forms.Label
    Friend WithEvents rdbContado As System.Windows.Forms.RadioButton
    Friend WithEvents lblCondicionVenta As System.Windows.Forms.Label
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents lblDistribuidor As System.Windows.Forms.Label
    Friend WithEvents lblCobrador As System.Windows.Forms.Label
    Friend WithEvents lblVendedor As System.Windows.Forms.Label
    Friend WithEvents lblPromotor As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents tabCredito As System.Windows.Forms.TabPage
    Friend WithEvents txtUsuarioAlta As System.Windows.Forms.TextBox
    Friend WithEvents txtFechaAlta As System.Windows.Forms.TextBox
    Friend WithEvents lblAlta As System.Windows.Forms.Label
    Friend WithEvents txtUsuarioModificacion As System.Windows.Forms.TextBox
    Friend WithEvents txtFechaModificacion As System.Windows.Forms.TextBox
    Friend WithEvents lblModificacion As System.Windows.Forms.Label
    Friend WithEvents txtFechaUltimaCompra As System.Windows.Forms.TextBox
    Friend WithEvents lblUltimaCompra As System.Windows.Forms.Label
    Friend WithEvents chkIVAExento As System.Windows.Forms.CheckBox
    Friend WithEvents lblLimiteCredito As System.Windows.Forms.Label
    Friend WithEvents txtLimiteCredito As ERP.ocxTXTNumeric
    Friend WithEvents lvlSaldo As System.Windows.Forms.Label
    Friend WithEvents txtSaldo As ERP.ocxTXTNumeric
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents txtRazonSocial As ERP.ocxTXTString
    Friend WithEvents txtRUC As ERP.ocxTXTString
    Friend WithEvents txtTelefonos As ERP.ocxTXTString
    Friend WithEvents txtDireccion As ERP.ocxTXTString
    Friend WithEvents txtNombreFantasia As ERP.ocxTXTString
    Friend WithEvents lblDescuentoSimbolo As System.Windows.Forms.Label
    Friend WithEvents txtDescuento As ERP.ocxTXTNumeric
    Friend WithEvents lblDesuento As System.Windows.Forms.Label
    Friend WithEvents txtPlazoCobro As ERP.ocxTXTNumeric
    Friend WithEvents lblPlazoCobro As System.Windows.Forms.Label
    Friend WithEvents txtPlazoCredito As ERP.ocxTXTNumeric
    Friend WithEvents lblPlazoCredito As System.Windows.Forms.Label
    Friend WithEvents txtPlazoCheque As ERP.ocxTXTNumeric
    Friend WithEvents lblPlazoCheque As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents cbxPais As ERP.ocxCBX
    Friend WithEvents cbxEstado As ERP.ocxCBX
    Friend WithEvents cbxTipoCliente As ERP.ocxCBX
    Friend WithEvents cbxListaPrecio As ERP.ocxCBX
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents cbxDistribuidor As ERP.ocxCBX
    Friend WithEvents cbxCobrador As ERP.ocxCBX
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents cbxPromotor As ERP.ocxCBX
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents cbxZonaVenta As ERP.ocxCBX
    Friend WithEvents cbxBarrio As ERP.ocxCBX
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents cbxDepartamento As ERP.ocxCBX
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents ocxCuenta As ERP.ocxTXTCuentaContable
    Friend WithEvents OcxSucursalesClientes1 As ERP.ocxSucursalesClientes
    'Friend WithEvents OcxDeuda1 As ERP.ocxDeuda
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblReferencia As System.Windows.Forms.Label
    Friend WithEvents txtReferencia As ERP.ocxTXTString
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OcxClienteContacto1 As ERP.ocxClienteContacto
    Friend WithEvents lblDeuda As System.Windows.Forms.Label
    Friend WithEvents txtDeuda As ERP.ocxTXTNumeric
    Friend WithEvents lklRecalcularCredito As System.Windows.Forms.LinkLabel
    Friend WithEvents chkCI As ERP.ocxCHK
    Friend WithEvents PnlCondicionVenta As System.Windows.Forms.Panel
    Friend WithEvents rdbCredito As System.Windows.Forms.RadioButton
    Friend WithEvents chkClienteVario As System.Windows.Forms.CheckBox
    Friend WithEvents cbxRuta As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxArea As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblCelular As System.Windows.Forms.Label
    Friend WithEvents txtCelulares As ERP.ocxTXTString
    Friend WithEvents chkJudicial As System.Windows.Forms.CheckBox
    Friend WithEvents cbxAbogado As ERP.ocxCBX
    Friend WithEvents tabFirma As System.Windows.Forms.TabPage
    Friend WithEvents OcxRegistroFirmas1 As ERP.ocxRegistroFirmas
    Friend WithEvents lklVerCatastro As System.Windows.Forms.LinkLabel
    Friend WithEvents OcxClientesMapa1 As ERP.ocxClientesMapa
    Friend WithEvents chkBoleta As ERP.ocxCHK
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents cbxCategoriaCliente As ocxCBX
    Friend WithEvents Label4 As Label
    Friend WithEvents tabPrecios As TabPage
    Friend WithEvents Panel2 As Panel
    Friend WithEvents OcxClientePrecio1 As ocxClientePrecio
    Friend WithEvents cbxClienteProducto As ocxCBX
    Friend WithEvents LblClienteProducto As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents btnConsultarRUC As Button
    Friend WithEvents cbxSituacion As ocxCBX
    Friend WithEvents Label5 As Label
    Friend WithEvents txtAniversario As ocxTXTDate
    Friend WithEvents Label3 As Label
    Friend WithEvents lblPaginaWeb As Label
    Friend WithEvents txtFax As ocxTXTString
    Friend WithEvents lblEmail As Label
    Friend WithEvents lblFax As Label
    Friend WithEvents txtPaginaWeb As TextBox
    Friend WithEvents txtEmail1 As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents OcxCBX1 As ocxCBX
    Friend WithEvents tabKMEstimado As TabPage
    Friend WithEvents OcxKMEstimadoMatriz1 As ocxKMEstimadoMatriz
    Friend WithEvents tabKMEstimadoSucursal As TabPage
    Friend WithEvents OcxKMEstimadoSucursal1 As ocxKMEstimadoSucursal
    Friend WithEvents Label7 As Label
    Friend WithEvents txtNroCasa As ocxTXTString
    Friend WithEvents cbxTipoOperacionTipoCliente As ocxCBX
    Friend WithEvents lblTipoOperacion As Label
End Class

﻿Public Class frmCotizacion

    'CLASES
    Dim CSistema As New CSistema


    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        'Funciones
        CargarInformacion()

        'Focus
        Me.ActiveControl = cbxMoneda
        cbxMoneda.Focus()
        txtFecha.Hoy()

    End Sub

    Sub CargarInformacion()

        'Cargas las monedas
        CSistema.SqlToComboBox(cbxMoneda, "Select ID, Descripcion From VMoneda Where ID<>1 Order By ID")

        'Cargamos la ultima cotizacion
        txtCotizacion.SetValue(CSistema.ExecuteScalar("Select dbo.FCotizacionAlDia(" & cbxMoneda.GetValue & ", 1)"))

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()
        If CSistema.ExecuteScalar("select count(*) from Cotizacion where fecha = '" & CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False) & "' and IDMoneda = " & cbxMoneda.GetValue) > 0 Then
            If MessageBox.Show("Ya existe cotizacion para la Fecha y Moneda seleccionada! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Validar
        'Moneda
        If cbxMoneda.GetValue = 0 Then
            Dim mensaje As String = "Seleccione correctamente la moneda!"
            ctrError.SetError(cbxMoneda, mensaje)
            ctrError.SetIconAlignment(cbxMoneda, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cotizacion
        If txtCotizacion.ObtenerValor = 0 Then
            Dim mensaje As String = "La cotizacion no es valida!"
            ctrError.SetError(txtCotizacion, mensaje)
            ctrError.SetIconAlignment(txtCotizacion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Procesar
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", txtCotizacion.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, True), ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpCotizacion", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            ctrError.Clear()

            MessageBox.Show(MensajeRetorno, "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub MostrarOpciones()
        Dim frm As New frmCotizacionHistorial
        Try
            frm.IdMoneda = cbxMoneda.cbx.SelectedValue
            frm.Moneda = cbxMoneda.txt.Text
        Catch ex As Exception

        End Try

        FGMostrarFormulario(Me, frm, "Cotizacion Historial", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Procesar(ERP.CSistema.NUMOperacionesABM.INS)
    End Sub

    Private Sub frmBanco_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Enter Then
            CSistema.SelectNextControl(Me, e.KeyCode)
        End If
    End Sub

    Private Sub frmBanco_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub cbxMoneda_Leave(sender As System.Object, e As System.EventArgs) Handles cbxMoneda.Leave
        txtCotizacion.SetValue(CSistema.ExecuteScalar("Select dbo.FCotizacionAlDia(" & cbxMoneda.GetValue & ", 1)"))
    End Sub

    Private Sub lklHistorial_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklHistorial.LinkClicked
        MostrarOpciones()
    End Sub
End Class
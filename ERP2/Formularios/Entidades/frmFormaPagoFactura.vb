﻿Public Class frmFormaPagoFactura
    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Controles
        CSistema.InicializaControles(Me)

        ocxCuenta.Conectar()
        'Variables
        vNuevo = False

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Focus
        dgvLista.Focus()

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtDescripcion)
        CSistema.CargaControl(vControles, txtReferencia)
        CSistema.CargaControl(vControles, chkCancelarAutomatico)
        CSistema.CargaControl(vControles, chkExigirAprobacion)
        CSistema.CargaControl(vControles, chkFacturaSinPedido)
        CSistema.CargaControl(vControles, ocxCuenta)
        CSistema.CargaControl(vControles, rdbActivo)
        CSistema.CargaControl(vControles, rdbDesactivado)
        CSistema.CargaControl(vControles, chkBloquearCondicion)
        CSistema.CargaControl(vControles, cbxCondicion)


        'Usuarios
        CSistema.SqlToDataGrid(dgvUsuario, "Select ID, Usuario, Nombre From VUsuario")
        dgvUsuario.Columns("ID").Visible = False
        dgvUsuario.Columns("Nombre").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        'Contado - Credito
        cbxCondicion.cbx.Items.Add("CONT")
        cbxCondicion.cbx.Items.Add("CRED")
        cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxCondicion.cbx.SelectedIndex = 0


        'Cargamos los registos en el lv
        Listar()

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If dgvLista.SelectedRows.Count = 0 Then
            ctrError.SetError(dgvLista, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgvLista, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = dgvLista.Rows(dgvLista.CurrentRow.Index).Cells(1).Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From vFormaPagoFactura Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            txtDescripcion.txt.Text = oRow("Descripcion").ToString
            txtReferencia.txt.Text = oRow("Referencia").ToString
            chkCancelarAutomatico.Checked = CBool(oRow("CancelarAutomatico").ToString)
            chkExigirAprobacion.Checked = CBool(oRow("Aprobacion").ToString)
            chkFacturaSinPedido.Checked = CBool(oRow("FacturacionSinPedido").ToString)
            ocxCuenta.SetValue(oRow("CodigoCuentaContable").ToString)

            If oRow("Condicion").ToString <> "" Then
                chkBloquearCondicion.Checked = True
                cbxCondicion.cbx.Text = oRow("Condicion").ToString
            Else
                chkBloquearCondicion.Checked = False
            End If

            If CBool(oRow("Estado")) = True Then
                rdbActivo.Checked = True
            Else
                rdbDesactivado.Checked = True
            End If

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ListarUsuarios()

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        CSistema.SqlToDataGrid(dgvLista, "Select 'Forma Pago'=Descripcion, ID, Referencia, 'Cuenta Contable'=CodigoCuentaContable From vFormaPagoFactura Order By 1")

        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If dgvLista.Columns.Count > 0 Then

            'Esto hacemos para que: 
            '1- Que el ID sea visible en la primera columna
            '2- Y para que cuando el usuario escriba en el lv, el control filtre por su descripcion.
            dgvLista.Columns(0).DisplayIndex = 1

            'Ahora seleccionamos automaticamente el registro especificado
            If dgvLista.Rows.Count > 0 Then
                If ID = 0 Then

                    dgvLista.Rows(0).Selected = True
                Else
                    For i As Integer = 0 To dgvLista.Rows.Count - 1
                        If dgvLista.Rows(i).Cells(1).Value.ToString = ID Then
                            dgvLista.Rows(i).Selected = True
                            Exit For
                        End If
                    Next

                End If
            End If

        End If
        ObtenerInformacion()
        dgvLista.Refresh()

    End Sub

    Sub ListarUsuarios()

        'Quitar todos los seleccionados
        For Each oRow As DataGridViewRow In dgvUsuario.Rows
            oRow.DefaultCellStyle.BackColor = Color.White
        Next

        If dgvLista.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim IDFormaPagoFactura As Integer = dgvLista.SelectedRows(0).Cells("ID").Value

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VFormaPagoFacturaUsuario Where IDFormaPagoFactura=" & IDFormaPagoFactura)

        For Each oRow As DataRow In dt.Rows
            For Each UsuarioRow As DataGridViewRow In dgvUsuario.Rows
                If UsuarioRow.Cells("ID").Value = oRow("IDUsuario") Then
                    UsuarioRow.DefaultCellStyle.BackColor = Color.LightGreen
                End If
            Next
        Next

    End Sub


    Sub InicializarControles()

        'TextBox
        txtDescripcion.txt.Clear()
        txtReferencia.txt.Clear()
        ocxCuenta.Clear()

        'CheckBox
        chkCancelarAutomatico.Checked = False
        chkExigirAprobacion.Checked = True

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From FormaPagoFactura"), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From FormaPagoFactura"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        txtDescripcion.Focus()

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Escritura de la Descripcion
        If txtDescripcion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar una descripcion valida!"
            ctrError.SetError(txtDescripcion, mensaje)
            ctrError.SetIconAlignment(txtDescripcion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Validar
        'Escritura de la txtReferencia
        If txtReferencia.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar una referencia valida!"
            ctrError.SetError(txtReferencia, mensaje)
            ctrError.SetIconAlignment(txtReferencia, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Validar
        'Escritura de la txtReferencia
        'If ocxCuenta.txt.Text.Trim.Length = 0 Then
        If ocxCuenta.txtCodigo.txt.TextLength = 0 Then
            Dim mensaje As String = "Debe ingresar una Cuenta Contable valida!"
            ctrError.SetError(ocxCuenta, mensaje)
            ctrError.SetIconAlignment(ocxCuenta, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgvLista.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgvLista, mensaje)
                ctrError.SetIconAlignment(dgvLista, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Referencia", txtReferencia.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CancelarAutomatico", chkCancelarAutomatico.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaContable", ocxCuenta.txtCodigo.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FacturacionSinPedido", chkFacturaSinPedido.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Aprobacion", chkExigirAprobacion.Checked.ToString, ParameterDirection.Input)
        If chkBloquearCondicion.Checked Then
            CSistema.SetSQLParameter(param, "@Condicion", cbxCondicion.cbx.Text, ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Condicion", "", ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpFormaPagoFactura", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            Listar(txtID.txt.Text)
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub


    Sub ActualizarFormaPagoFacturaUsuario(ByVal Insertar As Boolean)

        'Validar
        If dgvLista.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        If dgvUsuario.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim IDUsuario As Integer = dgvUsuario.SelectedRows(0).Cells("ID").Value
        Dim IDFormaPagoFactura As Integer = dgvLista.SelectedRows(0).Cells("ID").Value

        Dim SQL As String = "Exec SpActualizarFormaPagoFacturaUsuario "
        CSistema.ConcatenarParametro(SQL, "@IDUsuario", IDUsuario)
        CSistema.ConcatenarParametro(SQL, "@IDFormaPagoFactura", IDFormaPagoFactura)
        CSistema.ConcatenarParametro(SQL, "@Activar", Insertar)

        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)


        If dt Is Nothing Then
            MessageBox.Show("Error en el sistema! No se proceso.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            MessageBox.Show("No se proceso.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Dim Resultado As DataRow = dt.Rows(0)

        If Resultado("Procesado") = True Then
            ListarUsuarios()
        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        txtDescripcion.Focus()
        txtDescripcion.txt.SelectAll()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub dgvLista_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLista.Click, dgvUsuario.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub lvPaisLista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLista.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub frmMoneda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub HabilitarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HabilitarToolStripMenuItem.Click
        ActualizarFormaPagoFacturaUsuario(True)
    End Sub

    Private Sub DeshabilitarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeshabilitarToolStripMenuItem.Click
        ActualizarFormaPagoFacturaUsuario(False)
    End Sub

    Private Sub chkBloquearCondicion_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkBloquearCondicion.CheckedChanged
        If chkBloquearCondicion.Checked Then
            cbxCondicion.Enabled = True
        Else
            cbxCondicion.Enabled = False
        End If
    End Sub
End Class
﻿Imports ERP.Reporte
Public Class frmCliente1
    Dim CReporte As New CCliente
    'PROPIEDADES
    Private IDClienteValue As String
    Public Property IDCliente() As String
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As String)
            IDClienteValue = value
        End Set
    End Property

    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    Private UsuarioEsVendedorValue As Boolean
    Public Property UsuarioEsVendedor() As Boolean
        Get
            Return UsuarioEsVendedorValue
        End Get
        Set(ByVal value As Boolean)
            UsuarioEsVendedorValue = value
        End Set
    End Property

    Private UsuarioVendedorValue As String
    Public Property UsuarioVendedor() As String
        Get
            Return UsuarioVendedorValue
        End Get
        Set(ByVal value As String)
            UsuarioVendedorValue = value
        End Set
    End Property

    Private UsuarioIDVendedorValue As Integer
    Public Property UsuarioIDVendedor() As Integer
        Get
            Return UsuarioIDVendedorValue
        End Get
        Set(ByVal value As Integer)
            UsuarioIDVendedorValue = value
        End Set
    End Property

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control
    Dim MoverFormulario As Boolean
    Dim IDVendedor As Integer
    Dim RowSeleccionado As DataRow
    'FUNCIONES
    Sub Inicializar()

        'Formulario
        Me.AcceptButton = New Button

        'Variables()
        vNuevo = False
        If CadenaConexion Is Nothing Then
            CadenaConexion = VGCadenaConexion
        End If

        'RadioButton()
        rdbContado.Checked = True
        tcGeneral.BackColor = Color.FromArgb(188, 199, 216)
        tcGeneral.ForeColor = Color.FromArgb(44, 61, 90)
        tabPrecios.Visible = False

        ''Funciones()
        CargarInformacion()

        'Botones()
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Controles
        CSistema.InicializaControles(Me)
        CSistema.InicializaControles(tabDatosComerciales)
        ocxCuenta.Conectar()
        OcxSucursalesClientes1.CargarInformacion()
        CargarInformacionSucursal()
        CargarInformacionMapa()
        cbxClienteProducto.Enabled = False
        CargarInformacionKmEstimado()
        CargarInformacionKmEstimadoSucursal()

        'Focus
        txtID.Focus()

        ''Ir al ultimo
        'ManejoTecla(New KeyEventArgs(Keys.End))

        MoverFormulario = False

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)

        'Datos Comerciales
        CSistema.CargaControl(vControles, txtID)
        CSistema.CargaControl(vControles, txtRazonSocial)
        CSistema.CargaControl(vControles, txtRUC)
        'CSistema.CargaControl(vControles, cbxTipoContribuyente)

        CSistema.CargaControl(vControles, chkCI)
        CSistema.CargaControl(vControles, txtNombreFantasia)
        CSistema.CargaControl(vControles, txtDireccion)
        CSistema.CargaControl(vControles, txtTelefonos)
        CSistema.CargaControl(vControles, cbxEstado)
        CSistema.CargaControl(vControles, txtCelulares)

        'Localizaciones
        CSistema.CargaControl(vControles, cbxPais)
        CSistema.CargaControl(vControles, cbxDepartamento)
        CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxBarrio)
        CSistema.CargaControl(vControles, cbxZonaVenta)

        'Area y Ruta
        CSistema.CargaControl(vControles, cbxArea)
        CSistema.CargaControl(vControles, cbxRuta)
        CSistema.CargaControl(vControles, cbxClienteProducto)

        'Referencias
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxVendedor)
        CSistema.CargaControl(vControles, cbxPromotor)
        CSistema.CargaControl(vControles, cbxCobrador)
        CSistema.CargaControl(vControles, cbxDistribuidor)

        'Configuraciones
        CSistema.CargaControl(vControles, rdbContado)
        CSistema.CargaControl(vControles, rdbCredito)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, cbxListaPrecio)
        CSistema.CargaControl(vControles, cbxCategoriaCliente)
        CSistema.CargaControl(vControles, cbxTipoCliente)
        CSistema.CargaControl(vControles, chkIVAExento)
        CSistema.CargaControl(vControles, ocxCuenta)

        'Credito
        CSistema.CargaControl(vControles, txtLimiteCredito)
        CSistema.CargaControl(vControles, txtDescuento)
        CSistema.CargaControl(vControles, txtSaldo)
        CSistema.CargaControl(vControles, txtPlazoCredito)
        CSistema.CargaControl(vControles, txtPlazoCobro)
        CSistema.CargaControl(vControles, txtPlazoCheque)
        CSistema.CargaControl(vControles, chkBoleta)

        'Datos Adicionales
        CSistema.CargaControl(vControles, txtPaginaWeb)
        CSistema.CargaControl(vControles, txtEmail1)
        'CSistema.CargaControl(vControles, txtCorreo)

        CSistema.CargaControl(vControles, txtFax)
        CSistema.CargaControl(vControles, txtAniversario)

        CSistema.CargaControl(vControles, chkClienteVario)
        CSistema.CargaControl(vControles, cbxAbogado)

        CSistema.CargaControl(vControles, cbxSituacion)

        CSistema.CargaControl(vControles, txtNroCasa)

        'Listar Estado de Clientes
        CSistema.SqlToComboBox(cbxEstado.cbx, "Select ID, Descripcion From EstadoCliente Order By Orden Desc, Descripcion Asc", CadenaConexion)

        'Listar Paises
        CSistema.SqlToComboBox(cbxPais.cbx, "Select ID, Descripcion From Pais Order By Orden Desc, Descripcion Asc", CadenaConexion)

        'Listar Departamentos
        CSistema.SqlToComboBox(cbxDepartamento.cbx, "Select ID, Descripcion From Departamento Order By Orden Desc, Descripcion Asc", CadenaConexion)

        'Listar Ciudades
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select ID, Descripcion From Ciudad where estado = 1 Order By Orden Desc, Descripcion Asc", CadenaConexion)

        'Listar Barrios
        CSistema.SqlToComboBox(cbxBarrio.cbx, "Select ID, Descripcion From Barrio Order By Orden Desc, Descripcion Asc", CadenaConexion)

        'Vendedor
        CSistema.SqlToComboBox(cbxVendedor.cbx, "Select ID , Nombres From Vendedor where estado = 1 Order By Nombres Asc", CadenaConexion)
        'CSistema.SqlToComboBox(cbxVendedor.cbx, CData.GetTable("VVendedor", " IDSucursal=" & vgIDSucursal), "ID", "Nombres")

        'Lista de Precios
        CSistema.SqlToComboBox(cbxListaPrecio.cbx, "Select ID, Descripcion From ListaPrecio where IDSucursal=" & vgIDSucursal)

        '<Categoria de Clientes
        CSistema.SqlToComboBox(cbxCategoriaCliente.cbx, "Select ID, Descripcion From CategoriaCliente")

        'Monedas
        'CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Descripcion From Moneda  Order By 2", CadenaConexion)
        CSistema.SqlToComboBox(cbxMoneda.cbx, CData.GetTable("Moneda"), "ID", "Descripcion")
        cbxMoneda.SelectedValue(1)

        'Listar Areas
        CSistema.SqlToComboBox(cbxArea.cbx, "Select ID, Descripcion From Area Order By Descripcion Asc", CadenaConexion)

        'Listar Rutas
        CSistema.SqlToComboBox(cbxRuta.cbx, "Select ID, Descripcion From Ruta Order By Descripcion Asc", CadenaConexion)

        'Listar Abogados
        CSistema.SqlToComboBox(cbxAbogado.cbx, "Select ID, Descripcion From Abogado Order By Descripcion", CadenaConexion)

        'Listar Tipo Contribuyente


    End Sub

    Sub CargarInformacionSucursal()

        If OcxSucursalesClientes1.Iniciado = False Then
            OcxSucursalesClientes1.Inicializar()
        End If

    End Sub

    Sub CargarInformacionContacto()

        If OcxClienteContacto1.Iniciado = False Then
            OcxClienteContacto1.Inicializar()
        End If

    End Sub

    Sub CargarInformacionMapa()

        If OcxClientesMapa1.Iniciado = False Then
            OcxClientesMapa1.Inicializar()
        End If

    End Sub

    Sub CargarRegistroFirma(ID As Integer)

        If OcxRegistroFirmas1.Iniciado = False Then
            OcxRegistroFirmas1.IDCliente = ID
            OcxRegistroFirmas1.Inicializar()
        End If

    End Sub

    Sub CargarPrecios(ID As Integer)
        OcxClientePrecio1.IDCliente = ID
        OcxClientePrecio1.RowCliente = RowSeleccionado
        OcxClientePrecio1.CargarInformacion()


    End Sub

    Sub CargarInformacionKmEstimado()

        If OcxKMEstimadoMatriz1.Iniciado = False Then
            OcxKMEstimadoMatriz1.Inicializar()
        End If

    End Sub

    Sub CargarInformacionKmEstimadoSucursal()

        If OcxKMEstimadoSucursal1.Iniciado = False Then
            OcxKMEstimadoSucursal1.Inicializar()
        End If

    End Sub

    Sub ObtenerInformacion(ByVal Filtro As String, Optional ByVal PorReferencia As Boolean = False)

        'Validar
        'Obtener el ID Registro
        Dim ID As Integer

        If PorReferencia = False Then
            If IsNumeric(txtID.txt.Text) = False Then
                Exit Sub
            End If

            If CInt(txtID.txt.Text) = 0 Then
                Exit Sub
            End If
        End If

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VCliente " & Filtro, CadenaConexion)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count = 0 Then


            txtRazonSocial.Clear()
            txtRUC.Clear()

            CSistema.InicializaControles(Me)
            CSistema.InicializaControles(tabDatosComerciales)

            ctrError.SetError(txtID, "No se encontro ningun registro!")
            ctrError.SetIconAlignment(txtReferencia, ErrorIconAlignment.TopRight)

            txtReferencia.txt.SelectAll()
            txtReferencia.txt.Focus()

            Exit Sub

        Else

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)
            RowSeleccionado = oRow

            'Datos comerciales
            ID = oRow("ID").ToString
            txtID.txt.Text = oRow("ID").ToString
            txtReferencia.txt.Text = oRow("Referencia").ToString
            txtRazonSocial.txt.Text = oRow("RazonSocial").ToString
            txtRUC.txt.Text = oRow("RUC").ToString
            chkCI.Valor = oRow("CI")
            txtNombreFantasia.txt.Text = oRow("NombreFantasia").ToString
            txtDireccion.txt.Text = oRow("Direccion").ToString
            txtTelefonos.txt.Text = oRow("Telefono").ToString
            cbxEstado.cbx.Text = oRow("Estado").ToString
            IDVendedor = oRow("IDVendedor").ToString
            txtCelulares.txt.Text = oRow("Celular").ToString


            'Localizacion
            cbxPais.cbx.Text = oRow("Pais").ToString
            cbxDepartamento.cbx.Text = oRow("Departamento").ToString
            cbxCiudad.cbx.Text = oRow("Ciudad").ToString
            cbxBarrio.cbx.Text = oRow("Barrio").ToString
            cbxZonaVenta.cbx.Text = oRow("ZonaVenta").ToString

            'area y ruta
            cbxArea.cbx.Text = oRow("Area").ToString
            cbxRuta.cbx.Text = oRow("Ruta").ToString
            cbxClienteProducto.cbx.Text = oRow("ClienteProducto").ToString

            'Referencias
            cbxSucursal.cbx.Text = oRow("Sucursal").ToString
            cbxPromotor.cbx.Text = oRow("Promotor").ToString
            cbxAbogado.cbx.Text = oRow("Abogado").ToString
            chkJudicial.Checked = CBool(oRow("Judicial").ToString)

            If UsuarioEsVendedor = False Then
                cbxVendedor.cbx.Text = oRow("Vendedor").ToString
            Else
                If IDVendedor = UsuarioIDVendedor Then
                    cbxVendedor.cbx.Text = oRow("Vendedor").ToString
                Else
                    cbxVendedor.cbx.Text = ""
                End If
            End If

            cbxCobrador.cbx.Text = oRow("Cobrador").ToString
            cbxDistribuidor.cbx.Text = oRow("Distribuidor").ToString

            'Configuracion
            rdbContado.Checked = CBool(oRow("Contado").ToString)
            rdbCredito.Checked = CBool(oRow("Credito").ToString)
            cbxMoneda.cbx.Text = oRow("Moneda").ToString
            cbxListaPrecio.cbx.Text = oRow("ListaPrecio").ToString
            cbxCategoriaCliente.cbx.Text = oRow("CategoriaCLiente").ToString
            cbxTipoCliente.cbx.Text = oRow("TipoCliente").ToString
            chkIVAExento.Checked = CBool(oRow("IVAExento").ToString)
            ocxCuenta.SetValue(oRow("CodigoCuentaContable").ToString)

            'Estadisticos
            txtFechaAlta.Text = oRow("Alta").ToString
            txtUsuarioAlta.Text = oRow("UsuarioAlta").ToString
            txtFechaModificacion.Text = oRow("Modificacion").ToString
            txtUsuarioModificacion.Text = oRow("UsuarioModificacion").ToString
            txtFechaUltimaCompra.Text = oRow("Ult. Compra").ToString

            'Credito
            txtLimiteCredito.txt.Text = oRow("LimiteCredito").ToString
            txtDeuda.txt.Text = oRow("DeudaTotal").ToString
            txtSaldo.txt.Text = oRow("SaldoCredito").ToString
            txtDescuento.txt.Text = oRow("Descuento").ToString
            txtPlazoCredito.txt.Text = oRow("PlazoCredito").ToString
            txtPlazoCobro.txt.Text = oRow("PlazoCobro").ToString
            txtPlazoCheque.txt.Text = oRow("PlazoChequeDiferido").ToString
            chkBoleta.Valor = CBool(oRow("Boleta").ToString)

            'Adicionales
            CSistema.CargarRowEnControl(oRow, "PaginaWeb", txtPaginaWeb)
            CSistema.CargarRowEnControl(oRow, "Email", txtEmail1)
            CSistema.CargarRowEnControl(oRow, "Fax", txtFax)
            CSistema.CargarRowEnControl(oRow, "Aniversario", txtAniversario)

            txtNroCasa.txt.Text = oRow("NroCasa").ToString

            chkClienteVario.Checked = CBool(oRow("ClienteVario").ToString)

            cbxSituacion.cbx.Text = oRow("Situacion").ToString

            'Sucursales
            ObtenerInformacionSucursal(ID, oRow("IDPais").ToString)
            ObtenerInformacionContacto(ID)

            'Cargar Mapas
            ObtenerInformacionMapa(ID)
            ObtenerRegistroFirma(ID)
            CargarPrecios(ID)
            '  OcxClientesMapa1.Enabled = False

            ObtenerInformacionKMEstimado(ID)
            ObtenerInformacionKMEstimadoSucursal(ID)

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            txtReferencia.txt.SelectAll()
            txtReferencia.txt.Focus()

        End If

        ctrError.Clear()

    End Sub

    Sub ObtenerInformacionSucursal(ByVal ID As Integer, ByVal IDPais As String)

        If tcGeneral.SelectedTab.Name = "tabSucursales" Then
            OcxSucursalesClientes1.IDCliente = ID
            If IsNumeric(IDPais) = True Then
                OcxSucursalesClientes1.IDPais = IDPais
            End If

            OcxSucursalesClientes1.InicializarControles()
            OcxSucursalesClientes1.Listar()

        End If


    End Sub

    Sub ObtenerInformacionContacto(ByVal ID As Integer)

        If tcGeneral.SelectedTab.Name = "tabDatosAdicionales" Then
            OcxClienteContacto1.IDCliente = ID
            OcxClienteContacto1.InicializarControles()
            OcxClienteContacto1.Listar()

        End If


    End Sub
    Sub ObtenerRegistroFirma(ByVal ID As Integer)

        If tcGeneral.SelectedTab.Name = "tabFirma" Then
            OcxRegistroFirmas1.IDCliente = ID
            OcxRegistroFirmas1.Inicializar()
        End If


    End Sub

    Sub ObtenerInformacionMapa(ByVal ID As Integer)

        If tcGeneral.SelectedTab.Name = "tabLocalizacion" Then

            Dim sql As String = "Select Latitud, Longitud From VCliente Where ID=" & ID
            Dim dt As DataTable = CSistema.ExecuteToDataTable(sql, CadenaConexion)

            OcxClientesMapa1.ID = ID

            For Each oRow As DataRow In dt.Rows
                OcxClientesMapa1.Latitud = oRow("Latitud").ToString
                OcxClientesMapa1.Longitud = oRow("Longitud").ToString
            Next

            OcxClientesMapa1.CargarCliente()

        End If


    End Sub

    Sub ObtenerInformacionDeuda(ByVal ID As Integer)
        If tcGeneral.SelectedTab.Name = "tabCredito" Then
            Dim sql As String = "Select DeudaTotal From VCliente Where ID=" & ID
        End If
    End Sub

    Sub ObtenerInformacionSaldo(ByVal ID As Integer)
        If tcGeneral.SelectedTab.Name = "tabCredito" Then
            Dim sql As String = "Select SaldoCredito From VCliente Where ID=" & ID
        End If
    End Sub

    Sub ObtenerInformacionKMEstimado(ByVal ID As Integer)

        If tcGeneral.SelectedTab.Name = "tabKMEstimado" Then
            OcxKMEstimadoMatriz1.IDCliente = ID
            'OcxKMEstimadoMatriz1.InicializarControles()
            OcxKMEstimadoMatriz1.ObtenerInformacion()

        End If

    End Sub

    Sub ObtenerInformacionKMEstimadoSucursal(ByVal ID As Integer)

        If tcGeneral.SelectedTab.Name = "tabKMEstimadoSucursal" Then
            OcxKMEstimadoSucursal1.IDCliente = ID
            OcxKMEstimadoSucursal1.ObtenerInformacion()
            OcxKMEstimadoSucursal1.Listar()
        End If

    End Sub

    Sub ListarCliente()

        Dim frmBuscar As New frmClienteBuscar
        frmBuscar.Size = New Size(frmPrincipal2.Size.Width - 50, frmPrincipal2.Height - 130)
        frmBuscar.Location = New Point(5, 10)
        FGMostrarFormulario(Me, frmBuscar, "Busqueda de Clientes", FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        If frmBuscar.ID > 0 Then
            txtID.txt.Text = frmBuscar.ID
            ObtenerInformacion(" Where ID = " & frmBuscar.ID)
        End If

    End Sub

    Sub Nuevo()

        txtReferencia.Clear()
        txtRazonSocial.Clear()
        txtRUC.Clear()
        chkJudicial.Enabled = True
        cbxClienteProducto.Enabled = True
        CSistema.InicializaControles(Me)
        CSistema.InicializaControles(tabDatosComerciales)

        'Inicializar el limite de credito sino toma el ultimo cargado
        txtLimiteCredito.txt.Text = 0
        txtSaldo.txt.Text = 0
        txtDeuda.txt.Text = 0
        txtPlazoCredito.txt.Text = 0
        txtPlazoCobro.txt.Text = 0
        txtPlazoCheque.txt.Text = 0
        chkBoleta.Valor = False

        txtNroCasa.txt.Text = ""

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Cliente", CadenaConexion), Integer)

        rdbContado.Checked = True

        vNuevo = True
        txtReferencia.txt.Focus()
        btnBuscar.Enabled = False

        'Inicializar datos
        'Listar Paises
        CSistema.SqlToComboBox(cbxPais.cbx, "Select ID, Descripcion From Pais Order By Orden Desc, Descripcion Asc", CadenaConexion)
        cbxPais.cbx.SelectedIndex = 1
        'Listar Departamentos
        'CSistema.SqlToComboBox(cbxDepartamento.cbx, "Select ID, Descripcion From Departamento Order By Orden Desc, Descripcion Asc", CadenaConexion)
        'cbxDepartamento.cbx.SelectedIndex = 7
        'Listar Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Descripcion From Sucursal Order By Descripcion Asc", CadenaConexion)
        cbxSucursal.cbx.SelectedIndex = 0

        cbxVendedor.cbx.SelectedIndex = 1
        cbxListaPrecio.cbx.SelectedIndex = 1
        cbxTipoCliente.cbx.SelectedIndex = 1

    End Sub

    Sub Cancelar()

        txtReferencia.Clear()
        txtRazonSocial.Clear()
        txtRUC.Clear()
        chkJudicial.Enabled = False
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        txtID.txt.ReadOnly = False
        txtID.txt.Focus()
        vNuevo = False
        btnBuscar.Enabled = True

        'Ir al ultimo
        ManejoTecla(New KeyEventArgs(Keys.End))

    End Sub

    Sub Eliminar()

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)

    End Sub

    Function ValidarDocumento(ByVal operacion As ERP.CSistema.NUMOperacionesABM) As Boolean

        'Referencia
        If txtReferencia.txt.Text.Trim.Length = 0 Then

            CSistema.MostrarError("Ingrese una referencia valida!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            txtRazonSocial.txt.SelectAll()
            txtRazonSocial.Focus()
            Return False

        End If

        'Telefono
        If txtTelefonos.txt.Text.Trim.Length = 0 And txtCelulares.txt.Text.Trim.Length = 0 Then

            CSistema.MostrarError("Ingrese un telefono o celular valido!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            txtTelefonos.txt.SelectAll()
            txtTelefonos.Focus()
            Return False

        End If

        'direccion
        If txtDireccion.txt.Text.Trim.Length = 0 Then

            CSistema.MostrarError("Ingrese una direccion valida!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            txtDireccion.txt.SelectAll()
            txtDireccion.Focus()
            Return False

        End If

        ''Barrio
        'If cbxBarrio.txt.Text = "" Then
        '    CSistema.MostrarError("Ingrese una Barrio valido!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '    cbxBarrio.txt.SelectAll()
        '    cbxBarrio.Focus()
        '    Return False

        'End If


        ''Zona de venta
        'If cbxZonaVenta.txt.Text = "" Then
        '    CSistema.MostrarError("Ingrese una Zona de venta valida!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '    cbxZonaVenta.txt.SelectAll()
        '    cbxZonaVenta.Focus()
        '    Return False

        'End If

        ''Area
        'If cbxArea.txt.Text = "" Then
        '    CSistema.MostrarError("Ingrese una Area valida!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '    cbxArea.txt.SelectAll()
        '    cbxArea.Focus()
        '    Return False

        'End If

        'Lista de precio
        If cbxListaPrecio.txt.Text = "" Then
            CSistema.MostrarError("Ingrese una Lista de precio valida!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            cbxListaPrecio.txt.SelectAll()
            cbxListaPrecio.Focus()
            Return False
        End If

        'Tipo de cliente
        If cbxTipoCliente.txt.Text = "" Then
            CSistema.MostrarError("Ingrese un tipo de cliente valido!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            cbxTipoCliente.txt.SelectAll()
            cbxTipoCliente.Focus()
            Return False

        End If

        ''Ruta
        'If cbxRuta.txt.Text = "" Then
        '    CSistema.MostrarError("Ingrese una ruta valida!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '    cbxRuta.txt.SelectAll()
        '    cbxRuta.Focus()
        '    Return False

        'End If

        'Razon Social
        If txtRazonSocial.txt.Text.Trim.Length = 0 Then
            Dim Mensaje As String = "Ingrese la Razon Social!"
            ctrError.SetError(txtRazonSocial, Mensaje)
            ctrError.SetIconAlignment(txtRazonSocial, ErrorIconAlignment.TopRight)

            txtRazonSocial.txt.SelectAll()
            txtRazonSocial.Focus()

            tsslEstado.Text = "Atencion!!! " & Mensaje

            Return False

        End If

        'RUC
        If txtRUC.txt.Text.Trim.Length = 0 Then
            Dim Mensaje As String = "Ingrese el RUC!"
            ctrError.SetError(txtRUC, Mensaje)
            ctrError.SetIconAlignment(txtRUC, ErrorIconAlignment.TopRight)

            txtRUC.txt.SelectAll()
            txtRUC.Focus()

            tsslEstado.Text = "Atencion!!! " & Mensaje

            Return False

        End If

        'RUC Valido
        'Prueba 
        If chkCI.Valor = False Then
            If CSistema.ValidarDigitoVerificador(txtRUC.GetValue, True, True) = False Then

                Dim Mensaje As String = "El RUC no es correcto!"
                ctrError.SetError(txtRUC, Mensaje)
                ctrError.SetIconAlignment(txtRUC, ErrorIconAlignment.TopRight)

                txtRUC.txt.SelectAll()
                txtRUC.Focus()

                tsslEstado.Text = "Atencion!!! " & Mensaje

                Return False
            End If
        End If

        'Condicion de venta CREDITO exigir carga de Limite de Credito
        If rdbCredito.Checked = True Then
            If txtLimiteCredito.txt.Text = 0 Then
                CSistema.MostrarError("El Limite de credito no puede ser Cero!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                txtLimiteCredito.txt.SelectAll()
                txtLimiteCredito.Focus()
                Return False
            End If

        End If

        'Condicion de venta CREDITO exigir carga de Limite de Credito
        If chkJudicial.Checked = True Then
            If cbxAbogado.cbx.Text = "" Then
                CSistema.MostrarError("Seleccione un Abogado!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                cbxAbogado.Focus()
                Return False
            End If

        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Return False
            End If
        End If

        Return True

    End Function

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If


        'Procesar
        'Obtener el ID
        Dim ID As Integer
        If Operacion.ToString = "INS" Then
            ID = 0
        Else
            ID = txtID.txt.Text
        End If


        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        'Datos Obligatorios
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Referencia", txtReferencia.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@RazonSocial", txtRazonSocial.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@RUC", txtRUC.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CI", chkCI.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Datos Comerciales
        CSistema.SetSQLParameter(param, "@NombreFantasia", txtNombreFantasia.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Direccion", txtDireccion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Telefono", txtTelefonos.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Celular", txtCelulares.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDEstado", cbxEstado.cbx, ParameterDirection.Input)

        'Localizacion
        CSistema.SetSQLParameter(param, "@IDPais", cbxPais.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepartamento", cbxDepartamento.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCiudad", cbxCiudad.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDBarrio", cbxBarrio.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDZonaVenta", cbxZonaVenta.cbx, ParameterDirection.Input)

        'area y ruta
        CSistema.SetSQLParameter(param, "@IDArea", cbxArea.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDRuta", cbxRuta.cbx, ParameterDirection.Input)
        'Tipo de Cliente Producto 20-07-2021 SM
        CSistema.SetSQLParameter(param, "@IDClienteProducto", cbxClienteProducto.cbx, ParameterDirection.Input)

        'Referencia
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDPromotor", cbxPromotor.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDVendedor", cbxVendedor.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCobrador", cbxCobrador.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDistribuidor", cbxDistribuidor.cbx, ParameterDirection.Input)

        'Configuraciones
        CSistema.SetSQLParameter(param, "@IDListaPrecio", cbxListaPrecio.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCategoriaCliente", cbxCategoriaCliente.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoCliente", cbxTipoCliente.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Contado", rdbContado.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Credito", rdbCredito.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IVAExento", chkIVAExento.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CodigoCuentaContable", ocxCuenta.txtCodigo.GetValue, ParameterDirection.Input)

        'Credito
        CSistema.SetSQLParameter(param, "@LimiteCredito", txtLimiteCredito.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descuento", txtDescuento.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PlazoCredito", txtPlazoCredito.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PlazoCobro", txtPlazoCobro.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PlazoChequeDiferido", txtPlazoCheque.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Boleta", chkBoleta.Valor, ParameterDirection.Input)

        'Adicionales
        CSistema.SetSQLParameter(param, "@PaginaWeb", txtPaginaWeb.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Email", txtEmail1.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fax", txtFax.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Aniversario", CSistema.FormatoFechaBaseDatos(txtAniversario.txt.Text, True, False), ParameterDirection.Input)

        'Clientes varios
        CSistema.SetSQLParameter(param, "@ClienteVario", chkClienteVario.Checked, ParameterDirection.Input)

        'Judiciales
        CSistema.SetSQLParameter(param, "@Judicial", chkJudicial.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDAbogado", cbxAbogado.GetValue, ParameterDirection.Input)

        'Situacion
        CSistema.SetSQLParameter(param, "@IDSituacion", cbxSituacion.GetValue, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@NroCasa", txtNroCasa.txt.Text, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpCliente", False, False, MensajeRetorno, "", True, CadenaConexion) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            txtReferencia.txt.ReadOnly = False
            txtReferencia.txt.Focus()
            btnBuscar.Enabled = True

            If Operacion = ERP.CSistema.NUMOperacionesABM.INS Then

                ManejoTecla(New KeyEventArgs(Keys.End))
            End If

            Dim frm As New frmDescarga
            frm.ShowDialog()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
                ctrError.SetError(btnEliminar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopRight)
            Else
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            End If

        End If

    End Sub

    Sub ManejoTecla(ByVal e As KeyEventArgs)

        If e.KeyCode = Keys.Enter And vNuevo = False Then
            ObtenerInformacion(" Where Referencia = '" & txtReferencia.GetValue & "' ", True)
        ElseIf e.KeyCode = Keys.Enter And vNuevo = True Then
            txtRazonSocial.Focus()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtReferencia.txt.SelectAll()
            If UsuarioEsVendedor = False Then
                ObtenerInformacion(" Where ID = " & txtID.ObtenerValor)
            Else
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(ID), 1) From Cliente Where ID>" & txtID.ObtenerValor & " And IDVendedor=" & UsuarioIDVendedor, CadenaConexion), Integer)
                txtID.txt.Text = ID
                ObtenerInformacion(" Where ID = " & ID & " And IDVendedor=" & UsuarioIDVendedor)
            End If

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtReferencia.txt.SelectAll()
            If UsuarioEsVendedor = False Then
                ObtenerInformacion(" Where ID = " & txtID.ObtenerValor)
            Else
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Cliente Where ID<" & txtID.ObtenerValor & " And IDVendedor=" & UsuarioIDVendedor, CadenaConexion), Integer)
                txtID.txt.Text = ID
                ObtenerInformacion(" Where ID = " & ID & " And IDVendedor=" & UsuarioIDVendedor)
            End If

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            If UsuarioEsVendedor = False Then
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Cliente", CadenaConexion), Integer)
            Else
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Cliente Where IDVendedor=" & UsuarioIDVendedor, CadenaConexion), Integer)
            End If


            txtID.txt.Text = ID
            txtReferencia.txt.SelectAll()

            If UsuarioEsVendedor = False Then
                ObtenerInformacion(" Where ID = " & txtID.ObtenerValor)
            Else
                ObtenerInformacion(" Where ID = " & txtID.ObtenerValor & " And IDVendedor=" & UsuarioIDVendedor)
            End If

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            If UsuarioEsVendedor = False Then
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(ID), 1) From Cliente", CadenaConexion), Integer)
            Else
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(ID), 1) From Cliente Where IDVendedor=" & UsuarioIDVendedor, CadenaConexion), Integer)
            End If


            txtID.txt.Text = ID
            txtReferencia.txt.SelectAll()

            If UsuarioEsVendedor = False Then
                ObtenerInformacion(" Where ID = " & txtID.ObtenerValor)
            Else
                ObtenerInformacion(" Where ID = " & txtID.ObtenerValor & " And IDVendedor=" & UsuarioIDVendedor)
            End If

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub RecalcularDeuda()

        'Validar
        If MessageBox.Show("Desea recalcular la deuda?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
            Exit Sub
        End If

        Dim SQL As String = "Exec SpAcualizarSaldoCliente " & txtID.ObtenerValor
        Dim Resultado As DataTable = CSistema.ExecuteToDataTable(SQL, CadenaConexion)

        If Resultado Is Nothing Then
            MessageBox.Show("Ocurrio un error!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If Resultado.Rows.Count = 0 Then
            MessageBox.Show("Ocurrio un error!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        Dim oRow As DataRow = Resultado.Rows(0)
        If oRow("Procesado") = False Then
            MessageBox.Show(oRow("Mensaje").ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        MessageBox.Show(oRow("Mensaje").ToString, "Recalcular", MessageBoxButtons.OK, MessageBoxIcon.Information)
        txtDeuda.SetValue(oRow("Deuda").ToString)
        txtSaldo.SetValue(oRow("Saldo").ToString)

    End Sub

    Private Sub frmCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.F1 Then
            ListarCliente()
        End If

        If Me.ActiveControl.Name = "txtReferencia" Then
            Exit Sub
        End If

        If Me.ActiveControl.Name = "OcxSucursalesClientes1" Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub frmCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        'ManejoTecla(e)
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If


    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        chkJudicial.Enabled = True
        cbxAbogado.Enabled = chkJudicial.Checked
        txtID.txt.ReadOnly = True
        vNuevo = False
        txtRazonSocial.txt.Focus()
        cbxClienteProducto.Enabled = True
        btnBuscar.Enabled = False
    End Sub

    Private Sub cbxDepartamento_Editar(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxDepartamento.Editar

        Dim frm As New frmPaisDepartamentoCiudadBarrios
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpPaises"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpCiudades"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpBarrios"))

        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        'Listar Departamentos
        cbxCiudad.cbx.DataSource = Nothing
        CSistema.SqlToComboBox(cbxDepartamento.cbx, "Select ID, Descripcion From Departamento Where IDPais=" & cbxPais.cbx.SelectedValue & " Order By Orden Desc, Descripcion Asc", CadenaConexion)

    End Sub

    Private Sub cbxPais_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles cbxPais.DragOver

    End Sub

    Private Sub cbxPais_Editar(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxPais.Editar

        Dim frm As New frmPaisDepartamentoCiudadBarrios
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpDepartamentos"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpCiudades"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpBarrios"))

        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        'Listar Paises
        cbxDepartamento.cbx.DataSource = Nothing
        cbxCiudad.cbx.DataSource = Nothing
        CSistema.SqlToComboBox(cbxPais.cbx, "Select ID, Descripcion From Pais Order By Orden Desc, Descripcion Asc", CadenaConexion)

    End Sub
    Private Sub ListarCatastro()

        Dim Where As String = " Where ID = " & CSistema.FormatoMonedaBaseDatos(txtID.txt.Text, False)
        Dim OrderBy As String = ""
        Dim Titulo As String = " "
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        CReporte.ListarCatastro(frm, Where, "", vgUsuarioIdentificador, Titulo, "", "", True)
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        ListarCliente()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click, Button2.Click
        Me.Close()
    End Sub

    Private Sub cbxPais_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxPais.Load

    End Sub

    Private Sub cbxCiudad_Editar(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxCiudad.Editar

        Dim frm As New frmPaisDepartamentoCiudadBarrios
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpPaises"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpDepartamentos"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpBarrios"))

        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        'Listar Ciudades
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select ID, Descripcion From Ciudad Where IDDepartamento=" & cbxDepartamento.cbx.SelectedValue & " Order By Orden Desc, Descripcion Asc", CadenaConexion)

    End Sub

    Private Sub cbxBarrio_Editar(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxBarrio.Editar
        Dim frm As New frmPaisDepartamentoCiudadBarrios
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpPaises"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpDepartamentos"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpCiudades"))

        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        'Listar Barrios
        CSistema.SqlToComboBox(cbxBarrio.cbx, "Select ID, Descripcion From Barrio Order By Orden Desc, Descripcion Asc", CadenaConexion)

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click, Button1.Click
        Eliminar()
    End Sub

    Private Sub tcGeneral_Selected(ByVal sender As Object, ByVal e As System.Windows.Forms.TabControlEventArgs) Handles tcGeneral.Selected
        If e.TabPageIndex = 3 Then
            OcxSucursalesClientes1.Inicializar()
            OcxSucursalesClientes1.Listar()
            OcxSucursalesClientes1.Refresh()
            tcGeneral.TabPages(e.TabPageIndex).Refresh()
        End If

    End Sub

    Private Sub tcGeneral_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tcGeneral.SelectedIndexChanged

        If tcGeneral.SelectedTab.Name = "tabSucursales" Then
            CargarInformacionSucursal()
            ObtenerInformacionSucursal(txtID.ObtenerValor, cbxPais.cbx.SelectedValue)
        End If

        If tcGeneral.SelectedTab.Name = "tabDatosAdicionales" Then
            CargarInformacionContacto()
            ObtenerInformacionContacto(txtID.ObtenerValor)
        End If

        If tcGeneral.SelectedTab.Name = "tabCredito" Then
            ObtenerInformacionDeuda(txtID.ObtenerValor)
        End If

        If tcGeneral.SelectedTab.Name = "tabCredito" Then
            ObtenerInformacionSaldo(txtID.ObtenerValor)
        End If

        If tcGeneral.SelectedTab.Name = "tabLocalizacion" Then
            CargarInformacionMapa()
            ObtenerInformacionMapa(txtID.ObtenerValor)
        End If

        If tcGeneral.SelectedTab.Name = "tabFirma" Then
            CargarRegistroFirma(txtID.ObtenerValor)
            ObtenerRegistroFirma(txtID.ObtenerValor)
        End If

        If tcGeneral.SelectedTab.Name = "tabPrecios" Then
            CargarPrecios(txtID.ObtenerValor)
        End If

        If tcGeneral.SelectedTab.Name = "tabKMEstimado" Then
            CargarInformacionKmEstimado()
            ObtenerInformacionKMEstimado(txtID.ObtenerValor)
        End If

        If tcGeneral.SelectedTab.Name = "tabKMEstimadoSucursal" Then
            CargarInformacionKmEstimadoSucursal()
            ObtenerInformacionKMEstimadoSucursal(txtID.ObtenerValor)
        End If

    End Sub

    Private Sub txtReferencia_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtReferencia.TeclaPrecionada
        ManejoTecla(e)
    End Sub

    Private Sub Panel3_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        MoverFormulario = True
    End Sub

    Private Sub Panel3_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        If MoverFormulario = True Then
            Me.Location = MousePosition
        End If
    End Sub

    Private Sub Panel3_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        MoverFormulario = False
    End Sub

    Private Sub frmCliente_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        Me.Refresh()
    End Sub

    Private Sub rdbContado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbContado.CheckedChanged
        If PnlCondicionVenta.Enabled = True And rdbContado.Checked = True Then
            tabCredito.Enabled = False
        End If
    End Sub

    Private Sub rdbCredito_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbCredito.CheckedChanged
        If PnlCondicionVenta.Enabled = True And rdbCredito.Checked = True Then
            tabCredito.Enabled = True
        End If
    End Sub

    Private Sub lklRecalcularCredito_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklRecalcularCredito.LinkClicked
        RecalcularDeuda()
    End Sub

    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged

        If cbxSucursal.GetValue = 0 Then

            'Vendedor
            'CSistema.SqlToComboBox(cbxVendedor.cbx, CData.GetTable("VVendedor", " IDSucursal=" & vgIDSucursal), "ID", "Nombres")

            'Lista de Precios
            CSistema.SqlToComboBox(cbxListaPrecio.cbx, CData.GetTable("ListaPrecio", " IDSucursal=" & vgIDSucursal), "ID", "Descripcion")

            Exit Sub

        End If

        'Vendedor
        'CSistema.SqlToComboBox(cbxVendedor.cbx, CData.GetTable("VVendedor", " IDSucursal=" & cbxSucursal.GetValue), "ID", "Nombres")

        'Lista de Precios
        CSistema.SqlToComboBox(cbxListaPrecio.cbx, CData.GetTable("ListaPrecio", " IDSucursal=" & cbxSucursal.GetValue), "ID", "Descripcion")


        cbxVendedor.cbx.Text = ""
        cbxListaPrecio.cbx.Text = ""

    End Sub

    Private Sub chkJudicial_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkJudicial.CheckedChanged
        cbxAbogado.Enabled = chkJudicial.Checked
    End Sub

    Private Sub lklVerCatastro_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklVerCatastro.LinkClicked
        ListarCatastro()
    End Sub

    Private Sub OcxSucursalesClientes1_Load(sender As Object, e As EventArgs) Handles OcxSucursalesClientes1.Load

    End Sub
    'SC: 16-09-2021 Boton que consulta al RUC direccionado a la pagina.
    Private Sub btnConsultarRUC_Click(sender As Object, e As EventArgs) Handles btnConsultarRUC.Click
        Try
            System.Diagnostics.Process.Start("https://servicios.set.gov.py/eset-publico/perfilPublicoContribIService.do")
        Catch
            MessageBox.Show("Al abrir la pagina web no se encontro")
        End Try

    End Sub

    Private Sub cbxSituacion_TabIndexChanged(sender As Object, e As EventArgs) Handles cbxSituacion.TabIndexChanged
        chkJudicial.Checked = True
    End Sub

    Private Sub OcxClientePrecio1_Load(sender As Object, e As EventArgs) Handles OcxClientePrecio1.Load

    End Sub

    Private Sub frmCliente_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        'FA 20/06/2023
        LiberarMemoria()
    End Sub

    Private Sub cbxPais_PropertyChanged(sender As Object, e As EventArgs) Handles cbxPais.PropertyChanged
        CSistema.SqlToComboBox(cbxDepartamento.cbx, "Select ID, Descripcion From Departamento Where IDPais = " & cbxPais.cbx.SelectedValue & " Order By Orden Desc, Descripcion Asc", CadenaConexion)
        cbxDepartamento.cbx.Text = ""
    End Sub

    Private Sub cbxDepartamento_PropertyChanged(sender As Object, e As EventArgs) Handles cbxDepartamento.PropertyChanged
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select ID, Descripcion From Ciudad Where IDDepartamento = " & cbxDepartamento.cbx.SelectedValue & " Order By Orden Desc, Descripcion Asc", CadenaConexion)
        cbxCiudad.cbx.Text = ""
    End Sub

End Class
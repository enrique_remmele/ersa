﻿Public Class frmRuta

    'CLASES
    Dim CSistema As New CSistema


    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Focus
        lvLista.Focus()

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtDescripcion)
        CSistema.CargaControl(vControles, rdbActivo)
        CSistema.CargaControl(vControles, rdbDesactivado)

        'Cargamos los registos en el lv
        Listar()

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If lvLista.SelectedItems.Count = 0 Then
            ctrError.SetError(lvLista, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = lvLista.SelectedItems(0).SubItems(1).Text

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select ID, Descripcion, Estado, Sucursal, Area, ZonaVenta From vRuta Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            txtDescripcion.txt.Text = oRow("Descripcion").ToString
            cbxSucursal.cbx.Text = oRow("Sucursal").ToString
            cbxArea.cbx.Text = oRow("Area").ToString
            cbxZonaVenta.cbx.Text = oRow("ZonaVenta").ToString

            If CBool(oRow("Estado")) = True Then
                rdbActivo.Checked = True
            Else
                rdbDesactivado.Checked = True
            End If

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        CSistema.SqlToLv(lvLista, "Select 'Tipo'=Descripcion, ID, 'Estado'=(Case When Estado = 'True' Then 'OK' Else '-' End), Sucursal,Area, ZonaVenta From VRuta Order By 1")

        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If lvLista.Columns.Count > 0 Then

            'Esto hacemos para que: 
            '1- Que el ID sea visible en la primera columna
            '2- Y para que cuando el usuario escriba en el lv, el control filtre por su descripcion.
            lvLista.Columns(0).DisplayIndex = 1

            'Ahora seleccionamos automaticamente el registro especificado
            If lvLista.Items.Count > 0 Then
                If ID = 0 Then

                    lvLista.Items(0).Selected = True
                Else
                    For i As Integer = 0 To lvLista.Items.Count - 1
                        If lvLista.Items(i).SubItems(1).Text = ID Then
                            lvLista.Items(i).Selected = True
                            Exit For
                        End If
                    Next

                End If
            End If


        End If


        lvLista.Refresh()

    End Sub

    Sub InicializarControles()

        'TextBox
        txtDescripcion.txt.Clear()

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Ruta"), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Ruta"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        txtDescripcion.Focus()


    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Escritura de la Descripcion
        If txtDescripcion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar una descripcion valida!"
            ctrError.SetError(txtDescripcion, mensaje)
            ctrError.SetIconAlignment(txtDescripcion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Sucursal
        If cbxSucursal.Validar("Seleccione correctamente la sucursal", ctrError, cbxSucursal, tsslEstado) = False Then
            Exit Sub
        End If

        'Area
        If cbxArea.Validar("Seleccione correctamente el Area", ctrError, cbxArea, tsslEstado) = False Then
            Exit Sub
        End If

        'Zona de venta
        If cbxZonaVenta.Validar("Seleccione correctamente la Zona de venta", ctrError, cbxZonaVenta, tsslEstado) = False Then
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If lvLista.SelectedItems.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(lvLista, mensaje)
                ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDArea", cbxArea.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDZonaVenta", cbxZonaVenta.GetValue, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpRuta", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            Listar(txtID.txt.Text)
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno

            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub VerMapa()

        'Validar
        If lvLista.SelectedItems.Count = 0 Then
            Dim mensaje As String = "Seleccione un registro!"
            ctrError.SetError(txtDescripcion, mensaje)
            ctrError.SetIconAlignment(txtDescripcion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim ID As Integer = lvLista.SelectedItems(0).SubItems(1).Text
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From Ruta Where ID=" & ID).Copy


        Dim frm As New Form
        Dim ocx As New ocxZona

        frm.Controls.Add(ocx)

        ocx.Dock = DockStyle.Fill

        ocx.ID = ID
        ocx.ZonaVenta = dt.Rows(0)("Descripcion").ToString
        ocx.Poligono = dt.Rows(0)("Poligono").ToString
        ocx.Color = dt.Rows(0)("ColorPoligono").ToString

        ocx.Inicializar()


        frm.WindowState = FormWindowState.Maximized
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog()

    End Sub
    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        txtDescripcion.Focus()
        txtDescripcion.txt.SelectAll()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub lvLista_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvLista.Click
        ObtenerInformacion()
    End Sub

    Private Sub lvPaisLista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvLista.SelectedIndexChanged
        ObtenerInformacion()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmRuta_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub cbxSucursal_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        'Listar Departamentos
        If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
            Exit Sub
        End If
        cbxArea.cbx.Text = ""
        CSistema.SqlToComboBox(cbxArea.cbx, "Select ID, Descripcion From Area Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " Order By Descripcion Asc")
        cbxZonaVenta.cbx.Text = ""
        CSistema.SqlToComboBox(cbxZonaVenta.cbx, "Select ID, Descripcion From ZonaVenta Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " Order By Descripcion Asc")
    End Sub
End Class
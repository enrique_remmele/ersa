﻿
Public Class frmCuentaBancaria

    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control
    Dim vPathAlDia As String
    Dim vPathDiferido As String

    'FUNCIONES
    Sub Inicializar()

        'Formulario
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        CSistema.InicializaControles(Me)

        txtCuentaContable.Conectar()

        'Variables
        vNuevo = False

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Focus
        dgv.Focus()

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxBanco)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, cbxTipo)
        CSistema.CargaControl(vControles, txtCuenta)
        CSistema.CargaControl(vControles, txtNombre)
        CSistema.CargaControl(vControles, txtTitular)
        CSistema.CargaControl(vControles, txtFirma1)
        CSistema.CargaControl(vControles, txtFirma2)
        CSistema.CargaControl(vControles, txtFirma3)
        CSistema.CargaControl(vControles, txtCuentaContable)
        CSistema.CargaControl(vControles, rdbActivo)
        CSistema.CargaControl(vControles, rdbDesactivado)
        CSistema.CargaControl(vControles, txtApertura)
        CSistema.CargaControl(vControles, btnExaminarA)
        CSistema.CargaControl(vControles, btnExaminarD)
        CSistema.CargaControl(vControles, cbxUnidadNegocio)

        'Cargar combobox
        CSistema.SqlToComboBox(cbxBanco, "Select ID, Descripcion from Banco Order By 2")

        'Modena
        CSistema.SqlToComboBox(cbxMoneda, "Select ID, Referencia from Moneda Order By 2")

        'Tipo de Cuenta
        CSistema.SqlToComboBox(cbxTipo, "Select ID, Descripcion from TipoCuentaBancaria Order By 2")

        'Unidad de Negocio
        CSistema.SqlToComboBox(cbxUnidadNegocio, "Select ID, Descripcion from vUnidadNegocio Where Estado=1")

        'Cargamos los registos en el lv
        Listar()

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If dgv.SelectedRows.Count = 0 Then

            ctrError.SetError(dgv, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = dgv.SelectedRows(0).Cells("ID").Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VCuentaBancaria Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow

            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            cbxBanco.cbx.Text = oRow("Banco").ToString
            cbxMoneda.cbx.Text = oRow("Moneda").ToString
            cbxTipo.cbx.Text = oRow("TipoCuentaBancaria").ToString
            txtCuenta.txt.Text = oRow("Cuenta").ToString
            txtNombre.txt.Text = oRow("Nombre").ToString
            txtTitular.txt.Text = oRow("Titulares").ToString
            txtFirma1.txt.Text = oRow("Firma1").ToString
            txtFirma2.txt.Text = oRow("Firma2").ToString
            txtFirma3.txt.Text = oRow("Firma3").ToString
            txtAlDia.txt.Text = oRow("FormatoAlDia").ToString
            txtDiferido.txt.Text = oRow("FormatoDiferido").ToString
            cbxUnidadNegocio.cbx.Text = oRow("UnidadNegocio").ToString

            If CBool(oRow("Estado")) = True Then
                rdbActivo.Checked = True
            Else
                rdbDesactivado.Checked = True
            End If

            txtCuentaContable.SetValue(oRow("CodigoCuentaContable").ToString)
            txtObservacion.txt.Text = oRow("Observacion").ToString

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)


        End If

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        'Con este metodo "SqlToDataGrid" el sistema carga automaticamente la consulta SQL en el DataGrid asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el DataGrid.
        CSistema.SqlToDataGrid(dgv, "Select ID, Banco, Nombre, Cuenta, Moneda, 'Estado'=(Case When (Estado) = 'True' Then 'OK' Else '-' End),UnidadNegocio From VCuentaBancaria Order By 1")
        dgv.ReadOnly = True



    End Sub

    Sub InicializarControles()

        'TextBox
        txtCuenta.txt.Clear()
        txtNombre.txt.Clear()
        txtTitular.txt.Clear()
        txtFirma1.txt.Clear()
        txtFirma2.txt.Clear()
        txtFirma3.txt.Clear()
        txtAlDia.txt.Clear()
        txtDiferido.txt.Clear()

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From CuentaBancaria"), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From CuentaBancaria"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        txtCuenta.Focus()


    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgv.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgv, mensaje)
                ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Nombre", txtNombre.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Titulares", txtTitular.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Firma1", txtFirma1.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Firma2", txtFirma2.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Firma3", txtFirma3.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaBancaria", txtCuenta.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDBanco", cbxBanco.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoCuentaBancaria", cbxTipo.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@apertura", CSistema.FormatoFechaBaseDatos(txtApertura.GetValue, True, False), ParameterDirection.Input)

        If txtCuentaContable.Seleccionado = True Then
            CSistema.SetSQLParameter(param, "@CodigoCuentaContable", txtCuentaContable.Registro("Codigo"), ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FormatoAlDia", txtAlDia.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FormatoDiferido", txtDiferido.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUnidadNegocio", cbxUnidadNegocio.GetValue, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpCuentaBancaria", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            GuardarArchivos()
            Listar(txtID.txt.Text)
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno

            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub GuardarArchivos()

        If vPathAlDia <> "" Then
            If FileIO.FileSystem.FileExists(vPathAlDia) = True Then
                FileIO.FileSystem.CopyFile(vPathAlDia, VGCarpetaAplicacion & "\" & txtAlDia.txt.Text, True)
            End If
        End If

        If vPathDiferido <> "" Then
            If FileIO.FileSystem.FileExists(vPathDiferido) = True Then
                FileIO.FileSystem.CopyFile(vPathDiferido, VGCarpetaAplicacion & "\" & txtDiferido.txt.Text, True)
            End If
        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        txtCuenta.Focus()
        txtCuenta.txt.SelectAll()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub dgv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub lvPaisLista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ObtenerInformacion()
    End Sub

    Private Sub frmCuentaBancaria_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmMarca_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtObservacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtObservacion.Load

    End Sub
    Private Sub txtID_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtID.Load

    End Sub
    Private Sub cbxBanco_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxBanco.Load

    End Sub
    Private Sub cbxTipo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipo.Load

    End Sub
    Private Sub txtNombre_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNombre.Load

    End Sub
    Private Sub txtTitular_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTitular.Load

    End Sub
    Private Sub txtCuentaContable_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCuentaContable.Load

    End Sub
    Private Sub txtFirma1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFirma1.Load

    End Sub
    Private Sub btnExaminarD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExaminarD.Click
        OpenFileDialog1.Filter = "Archivos rpt|*.rpt"
        OpenFileDialog1.InitialDirectory = "C:\"
        OpenFileDialog1.ShowDialog()
        txtDiferido.txt.Text = OpenFileDialog1.SafeFileName()
        vPathDiferido = OpenFileDialog1.FileName

    End Sub

    Private Sub btnExaminarA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExaminarA.Click
        OpenFileDialog1.Filter = "Archivos rpt|*.rpt"
        OpenFileDialog1.InitialDirectory = "C:\"
        OpenFileDialog1.ShowDialog()
        txtAlDia.txt.Text = OpenFileDialog1.SafeFileName()
        vPathAlDia = OpenFileDialog1.FileName
    End Sub
End Class
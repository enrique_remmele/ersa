﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPorcentajeExcepcion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPorcentaje = New ERP.ocxTXTNumeric()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.lblPorcentaje = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnSalirProductoPrecio = New System.Windows.Forms.Button()
        Me.dgvProductoPrecio = New System.Windows.Forms.DataGridView()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnCancelarProductoPrecio = New System.Windows.Forms.Button()
        Me.rbDesactivadoProductoPrecio = New System.Windows.Forms.RadioButton()
        Me.btnGuardarProductoPrecio = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtImporteProductoPrecio = New ERP.ocxTXTNumeric()
        Me.btnEditarProductoPrecio = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnEliminarProductoPrecio = New System.Windows.Forms.Button()
        Me.rbActivoProductoPrecio = New System.Windows.Forms.RadioButton()
        Me.txtPorcentajeProductoPrecio = New ERP.ocxTXTNumeric()
        Me.btnNuevoProductoPrecio = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cbxUsuarioProductoPrecio = New ERP.ocxCBX()
        Me.cbxPresentacionProductoPrecio = New ERP.ocxCBX()
        Me.lblPresentacion = New System.Windows.Forms.Label()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.dgvProductoPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(138, 62)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 4
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(28, 64)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 167
        Me.lblEstado.Text = "Estado:"
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = True
        Me.txtImporte.Indicaciones = Nothing
        Me.txtImporte.Location = New System.Drawing.Point(272, 29)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(119, 22)
        Me.txtImporte.SoloLectura = False
        Me.txtImporte.TabIndex = 2
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(184, 34)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(86, 13)
        Me.Label3.TabIndex = 165
        Me.Label3.Text = "Importe Nominal:"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(77, 62)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 3
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.BackgroundColor = System.Drawing.Color.White
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv.GridColor = System.Drawing.Color.White
        Me.dgv.Location = New System.Drawing.Point(80, 124)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.Size = New System.Drawing.Size(430, 317)
        Me.dgv.TabIndex = 5
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(77, 89)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 6
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(147, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(15, 13)
        Me.Label2.TabIndex = 162
        Me.Label2.Text = "%"
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = Nothing
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = "Nombre"
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = "ID"
        Me.cbxUsuario.DataSource = ""
        Me.cbxUsuario.DataValueMember = "ID"
        Me.cbxUsuario.dtSeleccionado = Nothing
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(77, 1)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionMultiple = False
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(314, 21)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 0
        Me.cbxUsuario.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 160
        Me.Label1.Text = "Usuario:"
        '
        'txtPorcentaje
        '
        Me.txtPorcentaje.Color = System.Drawing.Color.Empty
        Me.txtPorcentaje.Decimales = True
        Me.txtPorcentaje.Indicaciones = Nothing
        Me.txtPorcentaje.Location = New System.Drawing.Point(77, 29)
        Me.txtPorcentaje.Name = "txtPorcentaje"
        Me.txtPorcentaje.Size = New System.Drawing.Size(63, 22)
        Me.txtPorcentaje.SoloLectura = False
        Me.txtPorcentaje.TabIndex = 1
        Me.txtPorcentaje.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentaje.Texto = "0"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 511)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(527, 22)
        Me.StatusStrip1.TabIndex = 159
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(352, 3)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 11
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(397, 89)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 10
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(316, 89)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 9
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(235, 89)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 8
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'lblPorcentaje
        '
        Me.lblPorcentaje.AutoSize = True
        Me.lblPorcentaje.Location = New System.Drawing.Point(10, 34)
        Me.lblPorcentaje.Name = "lblPorcentaje"
        Me.lblPorcentaje.Size = New System.Drawing.Size(61, 13)
        Me.lblPorcentaje.TabIndex = 152
        Me.lblPorcentaje.Text = "Porcentaje:"
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(154, 89)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 7
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(527, 511)
        Me.TabControl1.TabIndex = 168
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.TableLayoutPanel1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(519, 485)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Producto - Lista de Precio - Precio"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.TableLayoutPanel2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(519, 485)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Producto - Precio"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 77.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.dgv, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 121.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(513, 479)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnSalir)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(80, 447)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(430, 29)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'Panel1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.Panel1, 2)
        Me.Panel1.Controls.Add(Me.btnCancelar)
        Me.Panel1.Controls.Add(Me.rdbDesactivado)
        Me.Panel1.Controls.Add(Me.btnGuardar)
        Me.Panel1.Controls.Add(Me.lblEstado)
        Me.Panel1.Controls.Add(Me.lblPorcentaje)
        Me.Panel1.Controls.Add(Me.txtImporte)
        Me.Panel1.Controls.Add(Me.btnEditar)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.btnEliminar)
        Me.Panel1.Controls.Add(Me.rdbActivo)
        Me.Panel1.Controls.Add(Me.txtPorcentaje)
        Me.Panel1.Controls.Add(Me.btnNuevo)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.cbxUsuario)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(507, 115)
        Me.Panel1.TabIndex = 6
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 77.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel2, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.dgvProductoPrecio, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel2, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 149.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(513, 479)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.btnSalirProductoPrecio)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(80, 447)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(430, 29)
        Me.FlowLayoutPanel2.TabIndex = 0
        '
        'btnSalirProductoPrecio
        '
        Me.btnSalirProductoPrecio.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalirProductoPrecio.Location = New System.Drawing.Point(352, 3)
        Me.btnSalirProductoPrecio.Name = "btnSalirProductoPrecio"
        Me.btnSalirProductoPrecio.Size = New System.Drawing.Size(75, 23)
        Me.btnSalirProductoPrecio.TabIndex = 11
        Me.btnSalirProductoPrecio.Text = "Salir"
        Me.btnSalirProductoPrecio.UseVisualStyleBackColor = True
        '
        'dgvProductoPrecio
        '
        Me.dgvProductoPrecio.BackgroundColor = System.Drawing.Color.White
        Me.dgvProductoPrecio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProductoPrecio.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvProductoPrecio.GridColor = System.Drawing.Color.White
        Me.dgvProductoPrecio.Location = New System.Drawing.Point(80, 152)
        Me.dgvProductoPrecio.Name = "dgvProductoPrecio"
        Me.dgvProductoPrecio.ReadOnly = True
        Me.dgvProductoPrecio.Size = New System.Drawing.Size(430, 289)
        Me.dgvProductoPrecio.TabIndex = 5
        '
        'Panel2
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.Panel2, 2)
        Me.Panel2.Controls.Add(Me.lblPresentacion)
        Me.Panel2.Controls.Add(Me.cbxPresentacionProductoPrecio)
        Me.Panel2.Controls.Add(Me.btnCancelarProductoPrecio)
        Me.Panel2.Controls.Add(Me.rbDesactivadoProductoPrecio)
        Me.Panel2.Controls.Add(Me.btnGuardarProductoPrecio)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.txtImporteProductoPrecio)
        Me.Panel2.Controls.Add(Me.btnEditarProductoPrecio)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.btnEliminarProductoPrecio)
        Me.Panel2.Controls.Add(Me.rbActivoProductoPrecio)
        Me.Panel2.Controls.Add(Me.txtPorcentajeProductoPrecio)
        Me.Panel2.Controls.Add(Me.btnNuevoProductoPrecio)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.cbxUsuarioProductoPrecio)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(507, 143)
        Me.Panel2.TabIndex = 6
        '
        'btnCancelarProductoPrecio
        '
        Me.btnCancelarProductoPrecio.Location = New System.Drawing.Point(397, 115)
        Me.btnCancelarProductoPrecio.Name = "btnCancelarProductoPrecio"
        Me.btnCancelarProductoPrecio.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelarProductoPrecio.TabIndex = 10
        Me.btnCancelarProductoPrecio.Text = "&Cancelar"
        Me.btnCancelarProductoPrecio.UseVisualStyleBackColor = True
        '
        'rbDesactivadoProductoPrecio
        '
        Me.rbDesactivadoProductoPrecio.AutoSize = True
        Me.rbDesactivadoProductoPrecio.Location = New System.Drawing.Point(138, 88)
        Me.rbDesactivadoProductoPrecio.Name = "rbDesactivadoProductoPrecio"
        Me.rbDesactivadoProductoPrecio.Size = New System.Drawing.Size(85, 17)
        Me.rbDesactivadoProductoPrecio.TabIndex = 4
        Me.rbDesactivadoProductoPrecio.TabStop = True
        Me.rbDesactivadoProductoPrecio.Text = "Desactivado"
        Me.rbDesactivadoProductoPrecio.UseVisualStyleBackColor = True
        '
        'btnGuardarProductoPrecio
        '
        Me.btnGuardarProductoPrecio.Location = New System.Drawing.Point(154, 115)
        Me.btnGuardarProductoPrecio.Name = "btnGuardarProductoPrecio"
        Me.btnGuardarProductoPrecio.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardarProductoPrecio.TabIndex = 7
        Me.btnGuardarProductoPrecio.Text = "&Guardar"
        Me.btnGuardarProductoPrecio.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(28, 90)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 167
        Me.Label4.Text = "Estado:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(10, 59)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 152
        Me.Label5.Text = "Porcentaje:"
        '
        'txtImporteProductoPrecio
        '
        Me.txtImporteProductoPrecio.Color = System.Drawing.Color.Empty
        Me.txtImporteProductoPrecio.Decimales = True
        Me.txtImporteProductoPrecio.Indicaciones = Nothing
        Me.txtImporteProductoPrecio.Location = New System.Drawing.Point(272, 54)
        Me.txtImporteProductoPrecio.Name = "txtImporteProductoPrecio"
        Me.txtImporteProductoPrecio.Size = New System.Drawing.Size(119, 22)
        Me.txtImporteProductoPrecio.SoloLectura = False
        Me.txtImporteProductoPrecio.TabIndex = 2
        Me.txtImporteProductoPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteProductoPrecio.Texto = "0"
        '
        'btnEditarProductoPrecio
        '
        Me.btnEditarProductoPrecio.Location = New System.Drawing.Point(235, 115)
        Me.btnEditarProductoPrecio.Name = "btnEditarProductoPrecio"
        Me.btnEditarProductoPrecio.Size = New System.Drawing.Size(75, 23)
        Me.btnEditarProductoPrecio.TabIndex = 8
        Me.btnEditarProductoPrecio.Text = "&Editar"
        Me.btnEditarProductoPrecio.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(184, 59)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(86, 13)
        Me.Label6.TabIndex = 165
        Me.Label6.Text = "Importe Nominal:"
        '
        'btnEliminarProductoPrecio
        '
        Me.btnEliminarProductoPrecio.Location = New System.Drawing.Point(316, 115)
        Me.btnEliminarProductoPrecio.Name = "btnEliminarProductoPrecio"
        Me.btnEliminarProductoPrecio.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminarProductoPrecio.TabIndex = 9
        Me.btnEliminarProductoPrecio.Text = "E&liminar"
        Me.btnEliminarProductoPrecio.UseVisualStyleBackColor = True
        '
        'rbActivoProductoPrecio
        '
        Me.rbActivoProductoPrecio.AutoSize = True
        Me.rbActivoProductoPrecio.Location = New System.Drawing.Point(77, 88)
        Me.rbActivoProductoPrecio.Name = "rbActivoProductoPrecio"
        Me.rbActivoProductoPrecio.Size = New System.Drawing.Size(55, 17)
        Me.rbActivoProductoPrecio.TabIndex = 3
        Me.rbActivoProductoPrecio.TabStop = True
        Me.rbActivoProductoPrecio.Text = "Activo"
        Me.rbActivoProductoPrecio.UseVisualStyleBackColor = True
        '
        'txtPorcentajeProductoPrecio
        '
        Me.txtPorcentajeProductoPrecio.Color = System.Drawing.Color.Empty
        Me.txtPorcentajeProductoPrecio.Decimales = True
        Me.txtPorcentajeProductoPrecio.Indicaciones = Nothing
        Me.txtPorcentajeProductoPrecio.Location = New System.Drawing.Point(77, 54)
        Me.txtPorcentajeProductoPrecio.Name = "txtPorcentajeProductoPrecio"
        Me.txtPorcentajeProductoPrecio.Size = New System.Drawing.Size(63, 22)
        Me.txtPorcentajeProductoPrecio.SoloLectura = False
        Me.txtPorcentajeProductoPrecio.TabIndex = 1
        Me.txtPorcentajeProductoPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajeProductoPrecio.Texto = "0"
        '
        'btnNuevoProductoPrecio
        '
        Me.btnNuevoProductoPrecio.Location = New System.Drawing.Point(77, 115)
        Me.btnNuevoProductoPrecio.Name = "btnNuevoProductoPrecio"
        Me.btnNuevoProductoPrecio.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevoProductoPrecio.TabIndex = 6
        Me.btnNuevoProductoPrecio.Text = "&Nuevo"
        Me.btnNuevoProductoPrecio.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(25, 5)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(46, 13)
        Me.Label7.TabIndex = 160
        Me.Label7.Text = "Usuario:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(147, 59)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(15, 13)
        Me.Label8.TabIndex = 162
        Me.Label8.Text = "%"
        '
        'cbxUsuarioProductoPrecio
        '
        Me.cbxUsuarioProductoPrecio.CampoWhere = Nothing
        Me.cbxUsuarioProductoPrecio.CargarUnaSolaVez = False
        Me.cbxUsuarioProductoPrecio.DataDisplayMember = "Nombre"
        Me.cbxUsuarioProductoPrecio.DataFilter = Nothing
        Me.cbxUsuarioProductoPrecio.DataOrderBy = "ID"
        Me.cbxUsuarioProductoPrecio.DataSource = ""
        Me.cbxUsuarioProductoPrecio.DataValueMember = "ID"
        Me.cbxUsuarioProductoPrecio.dtSeleccionado = Nothing
        Me.cbxUsuarioProductoPrecio.FormABM = Nothing
        Me.cbxUsuarioProductoPrecio.Indicaciones = Nothing
        Me.cbxUsuarioProductoPrecio.Location = New System.Drawing.Point(77, 1)
        Me.cbxUsuarioProductoPrecio.Name = "cbxUsuarioProductoPrecio"
        Me.cbxUsuarioProductoPrecio.SeleccionMultiple = False
        Me.cbxUsuarioProductoPrecio.SeleccionObligatoria = False
        Me.cbxUsuarioProductoPrecio.Size = New System.Drawing.Size(314, 21)
        Me.cbxUsuarioProductoPrecio.SoloLectura = False
        Me.cbxUsuarioProductoPrecio.TabIndex = 0
        Me.cbxUsuarioProductoPrecio.Texto = ""
        '
        'cbxPresentacionProductoPrecio
        '
        Me.cbxPresentacionProductoPrecio.CampoWhere = Nothing
        Me.cbxPresentacionProductoPrecio.CargarUnaSolaVez = False
        Me.cbxPresentacionProductoPrecio.DataDisplayMember = Nothing
        Me.cbxPresentacionProductoPrecio.DataFilter = Nothing
        Me.cbxPresentacionProductoPrecio.DataOrderBy = Nothing
        Me.cbxPresentacionProductoPrecio.DataSource = Nothing
        Me.cbxPresentacionProductoPrecio.DataValueMember = Nothing
        Me.cbxPresentacionProductoPrecio.dtSeleccionado = Nothing
        Me.cbxPresentacionProductoPrecio.FormABM = Nothing
        Me.cbxPresentacionProductoPrecio.Indicaciones = Nothing
        Me.cbxPresentacionProductoPrecio.Location = New System.Drawing.Point(77, 25)
        Me.cbxPresentacionProductoPrecio.Name = "cbxPresentacionProductoPrecio"
        Me.cbxPresentacionProductoPrecio.SeleccionMultiple = False
        Me.cbxPresentacionProductoPrecio.SeleccionObligatoria = False
        Me.cbxPresentacionProductoPrecio.Size = New System.Drawing.Size(314, 23)
        Me.cbxPresentacionProductoPrecio.SoloLectura = False
        Me.cbxPresentacionProductoPrecio.TabIndex = 0
        Me.cbxPresentacionProductoPrecio.Texto = ""
        '
        'lblPresentacion
        '
        Me.lblPresentacion.AutoSize = True
        Me.lblPresentacion.Location = New System.Drawing.Point(-1, 30)
        Me.lblPresentacion.Name = "lblPresentacion"
        Me.lblPresentacion.Size = New System.Drawing.Size(72, 13)
        Me.lblPresentacion.TabIndex = 168
        Me.lblPresentacion.Text = "Presentacion:"
        '
        'frmPorcentajeExcepcion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(527, 533)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmPorcentajeExcepcion"
        Me.Text = "frmPorcentajeExcepcion"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        CType(Me.dgvProductoPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents rdbDesactivado As RadioButton
    Friend WithEvents lblEstado As Label
    Friend WithEvents txtImporte As ocxTXTNumeric
    Friend WithEvents Label3 As Label
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents rdbActivo As RadioButton
    Friend WithEvents dgv As DataGridView
    Friend WithEvents btnNuevo As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents cbxUsuario As ocxCBX
    Friend WithEvents Label1 As Label
    Friend WithEvents txtPorcentaje As ocxTXTNumeric
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents btnSalir As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnEliminar As Button
    Friend WithEvents btnEditar As Button
    Friend WithEvents lblPorcentaje As Label
    Friend WithEvents btnGuardar As Button
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents btnSalirProductoPrecio As Button
    Friend WithEvents dgvProductoPrecio As DataGridView
    Friend WithEvents Panel2 As Panel
    Friend WithEvents lblPresentacion As Label
    Friend WithEvents cbxPresentacionProductoPrecio As ocxCBX
    Friend WithEvents btnCancelarProductoPrecio As Button
    Friend WithEvents rbDesactivadoProductoPrecio As RadioButton
    Friend WithEvents btnGuardarProductoPrecio As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtImporteProductoPrecio As ocxTXTNumeric
    Friend WithEvents btnEditarProductoPrecio As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents btnEliminarProductoPrecio As Button
    Friend WithEvents rbActivoProductoPrecio As RadioButton
    Friend WithEvents txtPorcentajeProductoPrecio As ocxTXTNumeric
    Friend WithEvents btnNuevoProductoPrecio As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents cbxUsuarioProductoPrecio As ocxCBX
End Class

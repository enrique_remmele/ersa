﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificarAcuerdo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lklEliminar = New System.Windows.Forms.LinkLabel()
        Me.lklAgregar = New System.Windows.Forms.LinkLabel()
        Me.lvlista = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.txtImporte = New ERP.ocxTXTString()
        Me.txtPorcentaje = New ERP.ocxTXTString()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxMotivoAcuerdo = New ERP.ocxCBX()
        Me.txtVigenciaHasta = New ERP.ocxTXTDate()
        Me.txtVigenciaDesde = New ERP.ocxTXTDate()
        Me.txtNroAcuerdo = New ERP.ocxTXTString()
        Me.cbxPresentacion = New ERP.ocxCBX()
        Me.cbxFamilia = New ERP.ocxCBX()
        Me.txtpre = New ERP.ocxTXTString()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txtf = New ERP.ocxTXTString()
        Me.ExportarAExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblMotivoAcuerdo = New System.Windows.Forms.Label()
        Me.lblHasta = New System.Windows.Forms.Label()
        Me.lblDesde = New System.Windows.Forms.Label()
        Me.lblVigencia = New System.Windows.Forms.Label()
        Me.lblNroAcuerdo = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.rbActivado = New System.Windows.Forms.RadioButton()
        Me.rbDesactivado = New System.Windows.Forms.RadioButton()
        Me.ChkPresentacion = New System.Windows.Forms.CheckBox()
        Me.lblObservaciones = New System.Windows.Forms.Label()
        Me.ChkTodos = New System.Windows.Forms.CheckBox()
        Me.ChkFamilia = New System.Windows.Forms.CheckBox()
        Me.ChkProducto = New System.Windows.Forms.CheckBox()
        Me.ChkImporte = New System.Windows.Forms.CheckBox()
        Me.ChkPorcentaje = New System.Windows.Forms.CheckBox()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.gp2 = New System.Windows.Forms.GroupBox()
        Me.VerDetalleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gp2.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'lklEliminar
        '
        Me.lklEliminar.AutoSize = True
        Me.lklEliminar.Location = New System.Drawing.Point(33, 170)
        Me.lklEliminar.Name = "lklEliminar"
        Me.lklEliminar.Size = New System.Drawing.Size(43, 13)
        Me.lklEliminar.TabIndex = 146
        Me.lklEliminar.TabStop = True
        Me.lklEliminar.Text = "Eliminar"
        '
        'lklAgregar
        '
        Me.lklAgregar.AutoSize = True
        Me.lklAgregar.Location = New System.Drawing.Point(33, 146)
        Me.lklAgregar.Name = "lklAgregar"
        Me.lklAgregar.Size = New System.Drawing.Size(44, 13)
        Me.lklAgregar.TabIndex = 145
        Me.lklAgregar.TabStop = True
        Me.lklAgregar.Text = "Agregar"
        '
        'lvlista
        '
        Me.lvlista.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3})
        Me.lvlista.Location = New System.Drawing.Point(99, 126)
        Me.lvlista.Name = "lvlista"
        Me.lvlista.Size = New System.Drawing.Size(240, 77)
        Me.lvlista.TabIndex = 144
        Me.lvlista.UseCompatibleStateImageBehavior = False
        Me.lvlista.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "ID"
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Referencia"
        Me.ColumnHeader2.Width = 80
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Descripcion"
        Me.ColumnHeader3.Width = 89
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 308)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(755, 22)
        Me.StatusStrip1.TabIndex = 140
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'txtImporte
        '
        Me.txtImporte.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Indicaciones = Nothing
        Me.txtImporte.Location = New System.Drawing.Point(645, 88)
        Me.txtImporte.Multilinea = False
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(90, 21)
        Me.txtImporte.SoloLectura = False
        Me.txtImporte.TabIndex = 114
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtImporte.Texto = ""
        '
        'txtPorcentaje
        '
        Me.txtPorcentaje.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPorcentaje.Color = System.Drawing.Color.Empty
        Me.txtPorcentaje.Indicaciones = Nothing
        Me.txtPorcentaje.Location = New System.Drawing.Point(645, 55)
        Me.txtPorcentaje.Multilinea = False
        Me.txtPorcentaje.Name = "txtPorcentaje"
        Me.txtPorcentaje.Size = New System.Drawing.Size(90, 21)
        Me.txtPorcentaje.SoloLectura = False
        Me.txtPorcentaje.TabIndex = 112
        Me.txtPorcentaje.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPorcentaje.Texto = ""
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(359, 126)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(369, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 117
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxMotivoAcuerdo
        '
        Me.cbxMotivoAcuerdo.CampoWhere = Nothing
        Me.cbxMotivoAcuerdo.CargarUnaSolaVez = False
        Me.cbxMotivoAcuerdo.DataDisplayMember = Nothing
        Me.cbxMotivoAcuerdo.DataFilter = Nothing
        Me.cbxMotivoAcuerdo.DataOrderBy = Nothing
        Me.cbxMotivoAcuerdo.DataSource = Nothing
        Me.cbxMotivoAcuerdo.DataValueMember = Nothing
        Me.cbxMotivoAcuerdo.dtSeleccionado = Nothing
        Me.cbxMotivoAcuerdo.FormABM = Nothing
        Me.cbxMotivoAcuerdo.Indicaciones = Nothing
        Me.cbxMotivoAcuerdo.Location = New System.Drawing.Point(121, 59)
        Me.cbxMotivoAcuerdo.Name = "cbxMotivoAcuerdo"
        Me.cbxMotivoAcuerdo.SeleccionMultiple = False
        Me.cbxMotivoAcuerdo.SeleccionObligatoria = True
        Me.cbxMotivoAcuerdo.Size = New System.Drawing.Size(150, 21)
        Me.cbxMotivoAcuerdo.SoloLectura = False
        Me.cbxMotivoAcuerdo.TabIndex = 116
        Me.cbxMotivoAcuerdo.Texto = ""
        '
        'txtVigenciaHasta
        '
        Me.txtVigenciaHasta.AñoFecha = 0
        Me.txtVigenciaHasta.Color = System.Drawing.Color.Empty
        Me.txtVigenciaHasta.Fecha = New Date(2013, 2, 6, 9, 33, 15, 531)
        Me.txtVigenciaHasta.Location = New System.Drawing.Point(461, 70)
        Me.txtVigenciaHasta.MesFecha = 0
        Me.txtVigenciaHasta.Name = "txtVigenciaHasta"
        Me.txtVigenciaHasta.PermitirNulo = False
        Me.txtVigenciaHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtVigenciaHasta.SoloLectura = False
        Me.txtVigenciaHasta.TabIndex = 110
        '
        'txtVigenciaDesde
        '
        Me.txtVigenciaDesde.AñoFecha = 0
        Me.txtVigenciaDesde.Color = System.Drawing.Color.Empty
        Me.txtVigenciaDesde.Fecha = New Date(2013, 2, 6, 9, 33, 15, 531)
        Me.txtVigenciaDesde.Location = New System.Drawing.Point(351, 70)
        Me.txtVigenciaDesde.MesFecha = 0
        Me.txtVigenciaDesde.Name = "txtVigenciaDesde"
        Me.txtVigenciaDesde.PermitirNulo = False
        Me.txtVigenciaDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtVigenciaDesde.SoloLectura = False
        Me.txtVigenciaDesde.TabIndex = 109
        '
        'txtNroAcuerdo
        '
        Me.txtNroAcuerdo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroAcuerdo.Color = System.Drawing.Color.Empty
        Me.txtNroAcuerdo.Indicaciones = Nothing
        Me.txtNroAcuerdo.Location = New System.Drawing.Point(92, 17)
        Me.txtNroAcuerdo.Multilinea = False
        Me.txtNroAcuerdo.Name = "txtNroAcuerdo"
        Me.txtNroAcuerdo.Size = New System.Drawing.Size(90, 21)
        Me.txtNroAcuerdo.SoloLectura = False
        Me.txtNroAcuerdo.TabIndex = 105
        Me.txtNroAcuerdo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroAcuerdo.Texto = ""
        '
        'cbxPresentacion
        '
        Me.cbxPresentacion.CampoWhere = Nothing
        Me.cbxPresentacion.CargarUnaSolaVez = False
        Me.cbxPresentacion.DataDisplayMember = Nothing
        Me.cbxPresentacion.DataFilter = Nothing
        Me.cbxPresentacion.DataOrderBy = Nothing
        Me.cbxPresentacion.DataSource = Nothing
        Me.cbxPresentacion.DataValueMember = Nothing
        Me.cbxPresentacion.dtSeleccionado = Nothing
        Me.cbxPresentacion.FormABM = Nothing
        Me.cbxPresentacion.Indicaciones = Nothing
        Me.cbxPresentacion.Location = New System.Drawing.Point(163, 220)
        Me.cbxPresentacion.Name = "cbxPresentacion"
        Me.cbxPresentacion.SeleccionMultiple = False
        Me.cbxPresentacion.SeleccionObligatoria = True
        Me.cbxPresentacion.Size = New System.Drawing.Size(176, 21)
        Me.cbxPresentacion.SoloLectura = False
        Me.cbxPresentacion.TabIndex = 120
        Me.cbxPresentacion.Texto = ""
        '
        'cbxFamilia
        '
        Me.cbxFamilia.CampoWhere = Nothing
        Me.cbxFamilia.CargarUnaSolaVez = False
        Me.cbxFamilia.DataDisplayMember = Nothing
        Me.cbxFamilia.DataFilter = Nothing
        Me.cbxFamilia.DataOrderBy = Nothing
        Me.cbxFamilia.DataSource = Nothing
        Me.cbxFamilia.DataValueMember = Nothing
        Me.cbxFamilia.dtSeleccionado = Nothing
        Me.cbxFamilia.FormABM = Nothing
        Me.cbxFamilia.Indicaciones = Nothing
        Me.cbxFamilia.Location = New System.Drawing.Point(164, 247)
        Me.cbxFamilia.Name = "cbxFamilia"
        Me.cbxFamilia.SeleccionMultiple = False
        Me.cbxFamilia.SeleccionObligatoria = True
        Me.cbxFamilia.Size = New System.Drawing.Size(175, 21)
        Me.cbxFamilia.SoloLectura = False
        Me.cbxFamilia.TabIndex = 122
        Me.cbxFamilia.Texto = ""
        '
        'txtpre
        '
        Me.txtpre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtpre.Color = System.Drawing.Color.Empty
        Me.txtpre.Indicaciones = Nothing
        Me.txtpre.Location = New System.Drawing.Point(164, 220)
        Me.txtpre.Multilinea = False
        Me.txtpre.Name = "txtpre"
        Me.txtpre.Size = New System.Drawing.Size(175, 21)
        Me.txtpre.SoloLectura = True
        Me.txtpre.TabIndex = 147
        Me.txtpre.TabStop = False
        Me.txtpre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtpre.Texto = ""
        Me.txtpre.Visible = False
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'txtf
        '
        Me.txtf.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtf.Color = System.Drawing.Color.Empty
        Me.txtf.Indicaciones = Nothing
        Me.txtf.Location = New System.Drawing.Point(164, 247)
        Me.txtf.Multilinea = False
        Me.txtf.Name = "txtf"
        Me.txtf.Size = New System.Drawing.Size(175, 21)
        Me.txtf.SoloLectura = True
        Me.txtf.TabIndex = 148
        Me.txtf.TabStop = False
        Me.txtf.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtf.Texto = ""
        Me.txtf.Visible = False
        '
        'ExportarAExcelToolStripMenuItem
        '
        Me.ExportarAExcelToolStripMenuItem.Name = "ExportarAExcelToolStripMenuItem"
        Me.ExportarAExcelToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.ExportarAExcelToolStripMenuItem.Text = "Exportar a Excel"
        '
        'lblMotivoAcuerdo
        '
        Me.lblMotivoAcuerdo.AutoSize = True
        Me.lblMotivoAcuerdo.Location = New System.Drawing.Point(16, 62)
        Me.lblMotivoAcuerdo.Name = "lblMotivoAcuerdo"
        Me.lblMotivoAcuerdo.Size = New System.Drawing.Size(100, 13)
        Me.lblMotivoAcuerdo.TabIndex = 115
        Me.lblMotivoAcuerdo.Text = "Motivo de Acuerdo:"
        '
        'lblHasta
        '
        Me.lblHasta.AutoSize = True
        Me.lblHasta.Location = New System.Drawing.Point(458, 52)
        Me.lblHasta.Name = "lblHasta"
        Me.lblHasta.Size = New System.Drawing.Size(41, 13)
        Me.lblHasta.TabIndex = 134
        Me.lblHasta.Text = "Hasta :"
        '
        'lblDesde
        '
        Me.lblDesde.AutoSize = True
        Me.lblDesde.Location = New System.Drawing.Point(356, 52)
        Me.lblDesde.Name = "lblDesde"
        Me.lblDesde.Size = New System.Drawing.Size(44, 13)
        Me.lblDesde.TabIndex = 133
        Me.lblDesde.Text = "Desde :"
        '
        'lblVigencia
        '
        Me.lblVigencia.AutoSize = True
        Me.lblVigencia.Location = New System.Drawing.Point(280, 77)
        Me.lblVigencia.Name = "lblVigencia"
        Me.lblVigencia.Size = New System.Drawing.Size(54, 13)
        Me.lblVigencia.TabIndex = 132
        Me.lblVigencia.Text = "Vigencia :"
        '
        'lblNroAcuerdo
        '
        Me.lblNroAcuerdo.AutoSize = True
        Me.lblNroAcuerdo.Location = New System.Drawing.Point(16, 21)
        Me.lblNroAcuerdo.Name = "lblNroAcuerdo"
        Me.lblNroAcuerdo.Size = New System.Drawing.Size(70, 13)
        Me.lblNroAcuerdo.TabIndex = 131
        Me.lblNroAcuerdo.Text = "Nro Acuerdo:"
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Location = New System.Drawing.Point(653, 260)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 130
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.ImageIndex = 4
        Me.btnCancelar.Location = New System.Drawing.Point(557, 260)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(90, 23)
        Me.btnCancelar.TabIndex = 128
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGuardar.ImageIndex = 3
        Me.btnGuardar.Location = New System.Drawing.Point(461, 260)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(90, 23)
        Me.btnGuardar.TabIndex = 124
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'rbActivado
        '
        Me.rbActivado.AutoSize = True
        Me.rbActivado.Checked = True
        Me.rbActivado.Location = New System.Drawing.Point(6, 12)
        Me.rbActivado.Name = "rbActivado"
        Me.rbActivado.Size = New System.Drawing.Size(67, 17)
        Me.rbActivado.TabIndex = 0
        Me.rbActivado.TabStop = True
        Me.rbActivado.Text = "Activado"
        Me.rbActivado.UseVisualStyleBackColor = True
        '
        'rbDesactivado
        '
        Me.rbDesactivado.AutoSize = True
        Me.rbDesactivado.Location = New System.Drawing.Point(109, 12)
        Me.rbDesactivado.Name = "rbDesactivado"
        Me.rbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rbDesactivado.TabIndex = 1
        Me.rbDesactivado.Text = "Desactivado"
        Me.rbDesactivado.UseVisualStyleBackColor = True
        '
        'ChkPresentacion
        '
        Me.ChkPresentacion.AutoSize = True
        Me.ChkPresentacion.Location = New System.Drawing.Point(16, 220)
        Me.ChkPresentacion.Name = "ChkPresentacion"
        Me.ChkPresentacion.Size = New System.Drawing.Size(88, 17)
        Me.ChkPresentacion.TabIndex = 119
        Me.ChkPresentacion.Text = "Presentación"
        Me.ChkPresentacion.UseVisualStyleBackColor = True
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(356, 110)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(84, 13)
        Me.lblObservaciones.TabIndex = 139
        Me.lblObservaciones.Text = "Observaciones :"
        '
        'ChkTodos
        '
        Me.ChkTodos.AutoSize = True
        Me.ChkTodos.Location = New System.Drawing.Point(16, 275)
        Me.ChkTodos.Name = "ChkTodos"
        Me.ChkTodos.Size = New System.Drawing.Size(122, 17)
        Me.ChkTodos.TabIndex = 123
        Me.ChkTodos.Text = "Todos los productos"
        Me.ChkTodos.UseVisualStyleBackColor = True
        '
        'ChkFamilia
        '
        Me.ChkFamilia.AutoSize = True
        Me.ChkFamilia.Location = New System.Drawing.Point(16, 247)
        Me.ChkFamilia.Name = "ChkFamilia"
        Me.ChkFamilia.Size = New System.Drawing.Size(58, 17)
        Me.ChkFamilia.TabIndex = 121
        Me.ChkFamilia.Text = "Familia"
        Me.ChkFamilia.UseVisualStyleBackColor = True
        '
        'ChkProducto
        '
        Me.ChkProducto.AutoSize = True
        Me.ChkProducto.Location = New System.Drawing.Point(16, 126)
        Me.ChkProducto.Name = "ChkProducto"
        Me.ChkProducto.Size = New System.Drawing.Size(69, 17)
        Me.ChkProducto.TabIndex = 118
        Me.ChkProducto.Text = "Producto"
        Me.ChkProducto.UseVisualStyleBackColor = True
        '
        'ChkImporte
        '
        Me.ChkImporte.AutoSize = True
        Me.ChkImporte.Location = New System.Drawing.Point(554, 88)
        Me.ChkImporte.Name = "ChkImporte"
        Me.ChkImporte.Size = New System.Drawing.Size(67, 17)
        Me.ChkImporte.TabIndex = 113
        Me.ChkImporte.Text = "Importe :"
        Me.ChkImporte.UseVisualStyleBackColor = True
        '
        'ChkPorcentaje
        '
        Me.ChkPorcentaje.AutoSize = True
        Me.ChkPorcentaje.Location = New System.Drawing.Point(554, 58)
        Me.ChkPorcentaje.Name = "ChkPorcentaje"
        Me.ChkPorcentaje.Size = New System.Drawing.Size(83, 17)
        Me.ChkPorcentaje.TabIndex = 111
        Me.ChkPorcentaje.Text = "Porcentaje :"
        Me.ChkPorcentaje.UseVisualStyleBackColor = True
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(468, 180)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 13)
        Me.lblEstado.TabIndex = 136
        Me.lblEstado.Text = "Estado :"
        '
        'gp2
        '
        Me.gp2.Controls.Add(Me.rbActivado)
        Me.gp2.Controls.Add(Me.rbDesactivado)
        Me.gp2.Location = New System.Drawing.Point(520, 166)
        Me.gp2.Name = "gp2"
        Me.gp2.Size = New System.Drawing.Size(200, 33)
        Me.gp2.TabIndex = 138
        Me.gp2.TabStop = False
        '
        'VerDetalleToolStripMenuItem
        '
        Me.VerDetalleToolStripMenuItem.Name = "VerDetalleToolStripMenuItem"
        Me.VerDetalleToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.VerDetalleToolStripMenuItem.Text = "Ver Detalle"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VerDetalleToolStripMenuItem, Me.ExportarAExcelToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(158, 48)
        '
        'frmModificarAcuerdo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(755, 330)
        Me.Controls.Add(Me.lklEliminar)
        Me.Controls.Add(Me.lklAgregar)
        Me.Controls.Add(Me.lvlista)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.txtImporte)
        Me.Controls.Add(Me.txtPorcentaje)
        Me.Controls.Add(Me.txtObservacion)
        Me.Controls.Add(Me.cbxMotivoAcuerdo)
        Me.Controls.Add(Me.txtVigenciaHasta)
        Me.Controls.Add(Me.txtVigenciaDesde)
        Me.Controls.Add(Me.txtNroAcuerdo)
        Me.Controls.Add(Me.cbxPresentacion)
        Me.Controls.Add(Me.cbxFamilia)
        Me.Controls.Add(Me.txtpre)
        Me.Controls.Add(Me.txtf)
        Me.Controls.Add(Me.lblMotivoAcuerdo)
        Me.Controls.Add(Me.lblHasta)
        Me.Controls.Add(Me.lblDesde)
        Me.Controls.Add(Me.lblVigencia)
        Me.Controls.Add(Me.lblNroAcuerdo)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.ChkPresentacion)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.ChkTodos)
        Me.Controls.Add(Me.ChkFamilia)
        Me.Controls.Add(Me.ChkProducto)
        Me.Controls.Add(Me.ChkImporte)
        Me.Controls.Add(Me.ChkPorcentaje)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.gp2)
        Me.Name = "frmModificarAcuerdo"
        Me.Text = "frmModificarAcuerdo"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gp2.ResumeLayout(False)
        Me.gp2.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents lklEliminar As LinkLabel
    Friend WithEvents lklAgregar As LinkLabel
    Friend WithEvents lvlista As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents txtImporte As ocxTXTString
    Friend WithEvents txtPorcentaje As ocxTXTString
    Friend WithEvents txtObservacion As ocxTXTString
    Friend WithEvents cbxMotivoAcuerdo As ocxCBX
    Friend WithEvents txtVigenciaHasta As ocxTXTDate
    Friend WithEvents txtVigenciaDesde As ocxTXTDate
    Friend WithEvents txtNroAcuerdo As ocxTXTString
    Friend WithEvents cbxPresentacion As ocxCBX
    Friend WithEvents cbxFamilia As ocxCBX
    Friend WithEvents txtpre As ocxTXTString
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents txtf As ocxTXTString
    Friend WithEvents lblMotivoAcuerdo As Label
    Friend WithEvents lblHasta As Label
    Friend WithEvents lblDesde As Label
    Friend WithEvents lblVigencia As Label
    Friend WithEvents lblNroAcuerdo As Label
    Friend WithEvents btnSalir As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents ChkPresentacion As CheckBox
    Friend WithEvents lblObservaciones As Label
    Friend WithEvents ChkTodos As CheckBox
    Friend WithEvents ChkFamilia As CheckBox
    Friend WithEvents ChkProducto As CheckBox
    Friend WithEvents ChkImporte As CheckBox
    Friend WithEvents ChkPorcentaje As CheckBox
    Friend WithEvents lblEstado As Label
    Friend WithEvents gp2 As GroupBox
    Friend WithEvents rbActivado As RadioButton
    Friend WithEvents rbDesactivado As RadioButton
    Friend WithEvents ExportarAExcelToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents VerDetalleToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
End Class

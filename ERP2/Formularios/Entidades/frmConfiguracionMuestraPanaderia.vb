﻿Public Class frmConfiguracionMuestraPanaderia
    Public CSistema As New CSistema

    Dim dtConfiguracionMuestraPanaderia As DataTable

    Sub Inicializar()
        CargarInformacion()
    End Sub

    Sub CargarInformacion()

        dtConfiguracionMuestraPanaderia = CSistema.ExecuteToDataTable("Select * from ConfiguracionMuestraPanaderia")

        Dim IDOperacion As Integer = CSistema.ObtenerIDOperacion("frmMovimientoStock", "MOVIMIENTOS", "MOV")
        Dim IDTipoOperacino As Integer = CSistema.ExecuteScalar("Select ID from TipoOperacion where Salida = 1 and Entrada = 0 and IDOperacion =" & IDOperacion)

        CSistema.SqlToComboBox(cbxTipoComprobante, "Select id,Descripcion from TipoComprobante where IDOperacion = " & IDOperacion)

        CSistema.SqlToComboBox(cbxMotivo, "Select id,Descripcion from MotivoMovimiento where IDTipoMovimiento = " & IDTipoOperacino)

        If (Not dtConfiguracionMuestraPanaderia Is Nothing) And (dtConfiguracionMuestraPanaderia.Rows.Count > 0) Then
            cbxTipoComprobante.cbx.SelectedValue = dtConfiguracionMuestraPanaderia.Rows(0)("IDTipoComprobante")
            cbxMotivo.cbx.SelectedValue = dtConfiguracionMuestraPanaderia.Rows(0)("IDMotivo")
        End If

    End Sub

    Sub Guardar()

        If (dtConfiguracionMuestraPanaderia Is Nothing) Or (dtConfiguracionMuestraPanaderia.Rows.Count = 0) Then
            CSistema.ExecuteNonQuery("Insert into ConfiguracionMuestraPanaderia(IDTipoComprobante,IDMotivo) values (" & cbxTipoComprobante.cbx.SelectedValue & "," & cbxMotivo.cbx.SelectedValue & ")")

        Else
            CSistema.ExecuteNonQuery("Update ConfiguracionMuestraPanaderia Set IDTipoComprobante=" & cbxTipoComprobante.cbx.SelectedValue & ", IDMotivo=" & cbxMotivo.cbx.SelectedValue)
        End If

    End Sub

    Private Sub frmConfiguracionMuestraPanaderia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProducto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblCodigoBarra = New System.Windows.Forms.Label()
        Me.lblReferencia = New System.Windows.Forms.Label()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabIdentificacion = New System.Windows.Forms.TabPage()
        Me.chkInformatica = New ERP.ocxCHK()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblUnisal = New System.Windows.Forms.Label()
        Me.ChkDistribucion = New ERP.ocxCHK()
        Me.txtFechaModificacion = New System.Windows.Forms.MaskedTextBox()
        Me.txtFechaAlta = New System.Windows.Forms.MaskedTextBox()
        Me.txtUsuarioModificacion = New System.Windows.Forms.TextBox()
        Me.lblModificacion = New System.Windows.Forms.Label()
        Me.txtUsuarioAlta = New System.Windows.Forms.TextBox()
        Me.lblAlta = New System.Windows.Forms.Label()
        Me.chkDescargaCompra = New ERP.ocxCHK()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.chkMateriaPrima = New ERP.ocxCHK()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.chkMovimientoCombustible = New ERP.ocxCHK()
        Me.chkDescargaStock = New ERP.ocxCHK()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.chkVendible = New ERP.ocxCHK()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ocxCuentaDeudor = New ERP.ocxTXTCuentaContable()
        Me.lblCuentaContableDeudor = New System.Windows.Forms.Label()
        Me.lblPorcentaje = New System.Windows.Forms.Label()
        Me.txtMargenMinimo = New ERP.ocxTXTNumeric()
        Me.lblMargen = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.ocxCuentaCosto = New ERP.ocxTXTCuentaContable()
        Me.ocxCuentaVenta = New ERP.ocxTXTCuentaContable()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.ocxCuentaCompra = New ERP.ocxTXTCuentaContable()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.lblCuentaContableCosto = New System.Windows.Forms.Label()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.txtUnidadConversion = New ERP.ocxTXTString()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbxUnidadMedidaConversion = New ERP.ocxCBX()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPeso = New ERP.ocxTXTString()
        Me.chkControlarExistencia = New ERP.ocxCHK()
        Me.chkActivo = New ERP.ocxCHK()
        Me.cbxSubLinea2 = New ERP.ocxCBX()
        Me.lblSubLinea2 = New System.Windows.Forms.Label()
        Me.cbxSubLinea = New ERP.ocxCBX()
        Me.lblSubLinea = New System.Windows.Forms.Label()
        Me.lblCuentaVenta = New System.Windows.Forms.Label()
        Me.lblCuentaCompra = New System.Windows.Forms.Label()
        Me.txtVolumen = New ERP.ocxTXTString()
        Me.lblControlarExistencia = New System.Windows.Forms.Label()
        Me.lblActivo = New System.Windows.Forms.Label()
        Me.cbxImpuesto = New ERP.ocxCBX()
        Me.lblCostoPromedio = New System.Windows.Forms.Label()
        Me.txtCostoPromedio = New System.Windows.Forms.TextBox()
        Me.lblCostoCG = New System.Windows.Forms.Label()
        Me.txtCostoCG = New System.Windows.Forms.TextBox()
        Me.cbxProcedencia = New ERP.ocxCBX()
        Me.lblProcedencia = New System.Windows.Forms.Label()
        Me.lblCotizacion = New System.Windows.Forms.Label()
        Me.txtCotizacion = New System.Windows.Forms.TextBox()
        Me.lblUltimoCostoSinIVA = New System.Windows.Forms.Label()
        Me.txtUltimoCostoSinIVA = New System.Windows.Forms.TextBox()
        Me.lblUltimoCosto = New System.Windows.Forms.Label()
        Me.txtUltimoCosto = New System.Windows.Forms.TextBox()
        Me.txtCantidadUltimaSalida = New System.Windows.Forms.TextBox()
        Me.lblUltimaSalida = New System.Windows.Forms.Label()
        Me.txtFechaUltimaSalida = New System.Windows.Forms.TextBox()
        Me.txtCantidadUltimaEntrada = New System.Windows.Forms.TextBox()
        Me.lblUltimaEntrada = New System.Windows.Forms.Label()
        Me.txtFechaUltimaEntrada = New System.Windows.Forms.TextBox()
        Me.lblVolumen = New System.Windows.Forms.Label()
        Me.lblPeso = New System.Windows.Forms.Label()
        Me.txtUnidadPorCaja = New ERP.ocxTXTNumeric()
        Me.lblUnidadPorCaja = New System.Windows.Forms.Label()
        Me.cbxUnidadMedida = New ERP.ocxCBX()
        Me.lblUnidadMedida = New System.Windows.Forms.Label()
        Me.lblImpuesto = New System.Windows.Forms.Label()
        Me.cbxCategoria = New ERP.ocxCBX()
        Me.lblCategoria = New System.Windows.Forms.Label()
        Me.cbxDivision = New ERP.ocxCBX()
        Me.lblDivision = New System.Windows.Forms.Label()
        Me.cbxProveedor = New ERP.ocxCBX()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.cbxPresentacion = New ERP.ocxCBX()
        Me.lblPresentacion = New System.Windows.Forms.Label()
        Me.cbxMarca = New ERP.ocxCBX()
        Me.lblMarca = New System.Windows.Forms.Label()
        Me.cbxLinea = New ERP.ocxCBX()
        Me.lblLinea = New System.Windows.Forms.Label()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.lblTipoProducto = New System.Windows.Forms.Label()
        Me.tabExistencia = New System.Windows.Forms.TabPage()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.dgvExistencia = New System.Windows.Forms.DataGridView()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblSucursalDeposito = New System.Windows.Forms.Label()
        Me.lblExistenciaMinima = New System.Windows.Forms.Label()
        Me.txtExistenciaGeneral = New ERP.ocxTXTNumeric()
        Me.lblExistenciaCritica = New System.Windows.Forms.Label()
        Me.txtExistenciaCritica = New ERP.ocxTXTNumeric()
        Me.lblExistenciaGeneral = New System.Windows.Forms.Label()
        Me.txtExistenciaMinima = New ERP.ocxTXTNumeric()
        Me.btnActualizarExistencia = New System.Windows.Forms.Button()
        Me.cbxSucursalDeposito = New ERP.ocxCBX()
        Me.tabLotes = New System.Windows.Forms.TabPage()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.ListView2 = New System.Windows.Forms.ListView()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.OcxTXTDate1 = New ERP.ocxTXTDate()
        Me.OcxTXTString2 = New ERP.ocxTXTString()
        Me.tabUbicacion = New System.Windows.Forms.TabPage()
        Me.OcxProductoUbicacion1 = New ERP.ocxProductoUbicacion()
        Me.tabComision = New System.Windows.Forms.TabPage()
        Me.OcxComisionProducto1 = New ERP.ocxComisionProducto()
        Me.tabPrecios = New System.Windows.Forms.TabPage()
        Me.OcxListaPrecioProducto1 = New ERP.ocxListaPrecioProducto()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.txtCodigoBarra = New ERP.ocxTXTString()
        Me.txtReferencia = New ERP.ocxTXTString()
        Me.TabControl1.SuspendLayout()
        Me.tabIdentificacion.SuspendLayout()
        Me.tabExistencia.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.dgvExistencia, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.tabLotes.SuspendLayout()
        Me.tabUbicacion.SuspendLayout()
        Me.tabComision.SuspendLayout()
        Me.tabPrecios.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(643, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Codigo:"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(143, 11)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Text = "Descripción:"
        '
        'lblCodigoBarra
        '
        Me.lblCodigoBarra.AutoSize = True
        Me.lblCodigoBarra.Location = New System.Drawing.Point(440, 11)
        Me.lblCodigoBarra.Name = "lblCodigoBarra"
        Me.lblCodigoBarra.Size = New System.Drawing.Size(86, 13)
        Me.lblCodigoBarra.TabIndex = 4
        Me.lblCodigoBarra.Text = "Codigo de Barra:"
        '
        'lblReferencia
        '
        Me.lblReferencia.AutoSize = True
        Me.lblReferencia.Location = New System.Drawing.Point(7, 11)
        Me.lblReferencia.Name = "lblReferencia"
        Me.lblReferencia.Size = New System.Drawing.Size(62, 13)
        Me.lblReferencia.TabIndex = 0
        Me.lblReferencia.Text = "Referencia:"
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(757, 6)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(58, 23)
        Me.btnBuscar.TabIndex = 8
        Me.btnBuscar.Text = "&Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tabIdentificacion)
        Me.TabControl1.Controls.Add(Me.tabExistencia)
        Me.TabControl1.Controls.Add(Me.tabLotes)
        Me.TabControl1.Controls.Add(Me.tabUbicacion)
        Me.TabControl1.Controls.Add(Me.tabComision)
        Me.TabControl1.Controls.Add(Me.tabPrecios)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(3, 43)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(827, 439)
        Me.TabControl1.TabIndex = 1
        '
        'tabIdentificacion
        '
        Me.tabIdentificacion.Controls.Add(Me.chkInformatica)
        Me.tabIdentificacion.Controls.Add(Me.Label9)
        Me.tabIdentificacion.Controls.Add(Me.lblUnisal)
        Me.tabIdentificacion.Controls.Add(Me.ChkDistribucion)
        Me.tabIdentificacion.Controls.Add(Me.txtFechaModificacion)
        Me.tabIdentificacion.Controls.Add(Me.txtFechaAlta)
        Me.tabIdentificacion.Controls.Add(Me.txtUsuarioModificacion)
        Me.tabIdentificacion.Controls.Add(Me.lblModificacion)
        Me.tabIdentificacion.Controls.Add(Me.txtUsuarioAlta)
        Me.tabIdentificacion.Controls.Add(Me.lblAlta)
        Me.tabIdentificacion.Controls.Add(Me.chkDescargaCompra)
        Me.tabIdentificacion.Controls.Add(Me.Label8)
        Me.tabIdentificacion.Controls.Add(Me.chkMateriaPrima)
        Me.tabIdentificacion.Controls.Add(Me.Label7)
        Me.tabIdentificacion.Controls.Add(Me.chkMovimientoCombustible)
        Me.tabIdentificacion.Controls.Add(Me.chkDescargaStock)
        Me.tabIdentificacion.Controls.Add(Me.Label6)
        Me.tabIdentificacion.Controls.Add(Me.Label3)
        Me.tabIdentificacion.Controls.Add(Me.chkVendible)
        Me.tabIdentificacion.Controls.Add(Me.Label2)
        Me.tabIdentificacion.Controls.Add(Me.ocxCuentaDeudor)
        Me.tabIdentificacion.Controls.Add(Me.lblCuentaContableDeudor)
        Me.tabIdentificacion.Controls.Add(Me.lblPorcentaje)
        Me.tabIdentificacion.Controls.Add(Me.txtMargenMinimo)
        Me.tabIdentificacion.Controls.Add(Me.lblMargen)
        Me.tabIdentificacion.Controls.Add(Me.btnSalir)
        Me.tabIdentificacion.Controls.Add(Me.btnNuevo)
        Me.tabIdentificacion.Controls.Add(Me.btnEditar)
        Me.tabIdentificacion.Controls.Add(Me.ocxCuentaCosto)
        Me.tabIdentificacion.Controls.Add(Me.ocxCuentaVenta)
        Me.tabIdentificacion.Controls.Add(Me.btnGuardar)
        Me.tabIdentificacion.Controls.Add(Me.ocxCuentaCompra)
        Me.tabIdentificacion.Controls.Add(Me.btnCancelar)
        Me.tabIdentificacion.Controls.Add(Me.lblCuentaContableCosto)
        Me.tabIdentificacion.Controls.Add(Me.btnEliminar)
        Me.tabIdentificacion.Controls.Add(Me.txtUnidadConversion)
        Me.tabIdentificacion.Controls.Add(Me.Label4)
        Me.tabIdentificacion.Controls.Add(Me.cbxUnidadMedidaConversion)
        Me.tabIdentificacion.Controls.Add(Me.Label5)
        Me.tabIdentificacion.Controls.Add(Me.txtPeso)
        Me.tabIdentificacion.Controls.Add(Me.chkControlarExistencia)
        Me.tabIdentificacion.Controls.Add(Me.chkActivo)
        Me.tabIdentificacion.Controls.Add(Me.cbxSubLinea2)
        Me.tabIdentificacion.Controls.Add(Me.lblSubLinea2)
        Me.tabIdentificacion.Controls.Add(Me.cbxSubLinea)
        Me.tabIdentificacion.Controls.Add(Me.lblSubLinea)
        Me.tabIdentificacion.Controls.Add(Me.lblCuentaVenta)
        Me.tabIdentificacion.Controls.Add(Me.lblCuentaCompra)
        Me.tabIdentificacion.Controls.Add(Me.txtVolumen)
        Me.tabIdentificacion.Controls.Add(Me.lblControlarExistencia)
        Me.tabIdentificacion.Controls.Add(Me.lblActivo)
        Me.tabIdentificacion.Controls.Add(Me.cbxImpuesto)
        Me.tabIdentificacion.Controls.Add(Me.lblCostoPromedio)
        Me.tabIdentificacion.Controls.Add(Me.txtCostoPromedio)
        Me.tabIdentificacion.Controls.Add(Me.lblCostoCG)
        Me.tabIdentificacion.Controls.Add(Me.txtCostoCG)
        Me.tabIdentificacion.Controls.Add(Me.cbxProcedencia)
        Me.tabIdentificacion.Controls.Add(Me.lblProcedencia)
        Me.tabIdentificacion.Controls.Add(Me.lblCotizacion)
        Me.tabIdentificacion.Controls.Add(Me.txtCotizacion)
        Me.tabIdentificacion.Controls.Add(Me.lblUltimoCostoSinIVA)
        Me.tabIdentificacion.Controls.Add(Me.txtUltimoCostoSinIVA)
        Me.tabIdentificacion.Controls.Add(Me.lblUltimoCosto)
        Me.tabIdentificacion.Controls.Add(Me.txtUltimoCosto)
        Me.tabIdentificacion.Controls.Add(Me.txtCantidadUltimaSalida)
        Me.tabIdentificacion.Controls.Add(Me.lblUltimaSalida)
        Me.tabIdentificacion.Controls.Add(Me.txtFechaUltimaSalida)
        Me.tabIdentificacion.Controls.Add(Me.txtCantidadUltimaEntrada)
        Me.tabIdentificacion.Controls.Add(Me.lblUltimaEntrada)
        Me.tabIdentificacion.Controls.Add(Me.txtFechaUltimaEntrada)
        Me.tabIdentificacion.Controls.Add(Me.lblVolumen)
        Me.tabIdentificacion.Controls.Add(Me.lblPeso)
        Me.tabIdentificacion.Controls.Add(Me.txtUnidadPorCaja)
        Me.tabIdentificacion.Controls.Add(Me.lblUnidadPorCaja)
        Me.tabIdentificacion.Controls.Add(Me.cbxUnidadMedida)
        Me.tabIdentificacion.Controls.Add(Me.lblUnidadMedida)
        Me.tabIdentificacion.Controls.Add(Me.lblImpuesto)
        Me.tabIdentificacion.Controls.Add(Me.cbxCategoria)
        Me.tabIdentificacion.Controls.Add(Me.lblCategoria)
        Me.tabIdentificacion.Controls.Add(Me.cbxDivision)
        Me.tabIdentificacion.Controls.Add(Me.lblDivision)
        Me.tabIdentificacion.Controls.Add(Me.cbxProveedor)
        Me.tabIdentificacion.Controls.Add(Me.lblProveedor)
        Me.tabIdentificacion.Controls.Add(Me.cbxPresentacion)
        Me.tabIdentificacion.Controls.Add(Me.lblPresentacion)
        Me.tabIdentificacion.Controls.Add(Me.cbxMarca)
        Me.tabIdentificacion.Controls.Add(Me.lblMarca)
        Me.tabIdentificacion.Controls.Add(Me.cbxLinea)
        Me.tabIdentificacion.Controls.Add(Me.lblLinea)
        Me.tabIdentificacion.Controls.Add(Me.cbxTipoProducto)
        Me.tabIdentificacion.Controls.Add(Me.lblTipoProducto)
        Me.tabIdentificacion.Location = New System.Drawing.Point(4, 22)
        Me.tabIdentificacion.Name = "tabIdentificacion"
        Me.tabIdentificacion.Padding = New System.Windows.Forms.Padding(3)
        Me.tabIdentificacion.Size = New System.Drawing.Size(819, 413)
        Me.tabIdentificacion.TabIndex = 0
        Me.tabIdentificacion.Text = "Identificacion"
        Me.tabIdentificacion.UseVisualStyleBackColor = True
        '
        'chkInformatica
        '
        Me.chkInformatica.BackColor = System.Drawing.Color.Transparent
        Me.chkInformatica.Color = System.Drawing.Color.Empty
        Me.chkInformatica.Location = New System.Drawing.Point(173, 350)
        Me.chkInformatica.Name = "chkInformatica"
        Me.chkInformatica.Size = New System.Drawing.Size(15, 16)
        Me.chkInformatica.SoloLectura = False
        Me.chkInformatica.TabIndex = 93
        Me.chkInformatica.Texto = Nothing
        Me.chkInformatica.Valor = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(190, 349)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(59, 13)
        Me.Label9.TabIndex = 92
        Me.Label9.Text = "Informatica"
        '
        'lblUnisal
        '
        Me.lblUnisal.AutoSize = True
        Me.lblUnisal.Location = New System.Drawing.Point(64, 350)
        Me.lblUnisal.Name = "lblUnisal"
        Me.lblUnisal.Size = New System.Drawing.Size(62, 13)
        Me.lblUnisal.TabIndex = 91
        Me.lblUnisal.Text = "Distribucion"
        '
        'ChkDistribucion
        '
        Me.ChkDistribucion.BackColor = System.Drawing.Color.Transparent
        Me.ChkDistribucion.Color = System.Drawing.Color.Empty
        Me.ChkDistribucion.Location = New System.Drawing.Point(46, 350)
        Me.ChkDistribucion.Name = "ChkDistribucion"
        Me.ChkDistribucion.Size = New System.Drawing.Size(15, 16)
        Me.ChkDistribucion.SoloLectura = False
        Me.ChkDistribucion.TabIndex = 90
        Me.ChkDistribucion.Texto = Nothing
        Me.ChkDistribucion.Valor = False
        '
        'txtFechaModificacion
        '
        Me.txtFechaModificacion.Location = New System.Drawing.Point(427, 328)
        Me.txtFechaModificacion.Mask = "00/00/0000 90:00"
        Me.txtFechaModificacion.Name = "txtFechaModificacion"
        Me.txtFechaModificacion.ReadOnly = True
        Me.txtFechaModificacion.Size = New System.Drawing.Size(105, 20)
        Me.txtFechaModificacion.TabIndex = 89
        Me.txtFechaModificacion.ValidatingType = GetType(Date)
        '
        'txtFechaAlta
        '
        Me.txtFechaAlta.Location = New System.Drawing.Point(427, 307)
        Me.txtFechaAlta.Mask = "00/00/0000 90:00"
        Me.txtFechaAlta.Name = "txtFechaAlta"
        Me.txtFechaAlta.ReadOnly = True
        Me.txtFechaAlta.Size = New System.Drawing.Size(105, 20)
        Me.txtFechaAlta.TabIndex = 88
        Me.txtFechaAlta.ValidatingType = GetType(Date)
        '
        'txtUsuarioModificacion
        '
        Me.txtUsuarioModificacion.BackColor = System.Drawing.Color.White
        Me.txtUsuarioModificacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUsuarioModificacion.Location = New System.Drawing.Point(535, 328)
        Me.txtUsuarioModificacion.Name = "txtUsuarioModificacion"
        Me.txtUsuarioModificacion.ReadOnly = True
        Me.txtUsuarioModificacion.Size = New System.Drawing.Size(117, 20)
        Me.txtUsuarioModificacion.TabIndex = 85
        Me.txtUsuarioModificacion.TabStop = False
        Me.txtUsuarioModificacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblModificacion
        '
        Me.lblModificacion.AutoSize = True
        Me.lblModificacion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblModificacion.Location = New System.Drawing.Point(351, 332)
        Me.lblModificacion.Name = "lblModificacion"
        Me.lblModificacion.Size = New System.Drawing.Size(70, 13)
        Me.lblModificacion.TabIndex = 87
        Me.lblModificacion.Text = "Modificacion:"
        '
        'txtUsuarioAlta
        '
        Me.txtUsuarioAlta.BackColor = System.Drawing.Color.White
        Me.txtUsuarioAlta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUsuarioAlta.Location = New System.Drawing.Point(535, 307)
        Me.txtUsuarioAlta.Name = "txtUsuarioAlta"
        Me.txtUsuarioAlta.ReadOnly = True
        Me.txtUsuarioAlta.Size = New System.Drawing.Size(117, 20)
        Me.txtUsuarioAlta.TabIndex = 83
        Me.txtUsuarioAlta.TabStop = False
        Me.txtUsuarioAlta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblAlta
        '
        Me.lblAlta.AutoSize = True
        Me.lblAlta.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblAlta.Location = New System.Drawing.Point(393, 311)
        Me.lblAlta.Name = "lblAlta"
        Me.lblAlta.Size = New System.Drawing.Size(28, 13)
        Me.lblAlta.TabIndex = 86
        Me.lblAlta.Text = "Alta:"
        '
        'chkDescargaCompra
        '
        Me.chkDescargaCompra.BackColor = System.Drawing.Color.Transparent
        Me.chkDescargaCompra.Color = System.Drawing.Color.Empty
        Me.chkDescargaCompra.Location = New System.Drawing.Point(173, 330)
        Me.chkDescargaCompra.Name = "chkDescargaCompra"
        Me.chkDescargaCompra.Size = New System.Drawing.Size(15, 16)
        Me.chkDescargaCompra.SoloLectura = False
        Me.chkDescargaCompra.TabIndex = 81
        Me.chkDescargaCompra.Texto = Nothing
        Me.chkDescargaCompra.Valor = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(190, 329)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(107, 13)
        Me.Label8.TabIndex = 80
        Me.Label8.Text = "Descarga de Compra"
        '
        'chkMateriaPrima
        '
        Me.chkMateriaPrima.BackColor = System.Drawing.Color.Transparent
        Me.chkMateriaPrima.Color = System.Drawing.Color.Empty
        Me.chkMateriaPrima.Location = New System.Drawing.Point(173, 310)
        Me.chkMateriaPrima.Name = "chkMateriaPrima"
        Me.chkMateriaPrima.Size = New System.Drawing.Size(15, 16)
        Me.chkMateriaPrima.SoloLectura = False
        Me.chkMateriaPrima.TabIndex = 79
        Me.chkMateriaPrima.Texto = Nothing
        Me.chkMateriaPrima.Valor = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(190, 309)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(117, 13)
        Me.Label7.TabIndex = 78
        Me.Label7.Text = "Mat. Prima Balanceado"
        '
        'chkMovimientoCombustible
        '
        Me.chkMovimientoCombustible.BackColor = System.Drawing.Color.Transparent
        Me.chkMovimientoCombustible.Color = System.Drawing.Color.Empty
        Me.chkMovimientoCombustible.Location = New System.Drawing.Point(48, 330)
        Me.chkMovimientoCombustible.Name = "chkMovimientoCombustible"
        Me.chkMovimientoCombustible.Size = New System.Drawing.Size(15, 16)
        Me.chkMovimientoCombustible.SoloLectura = False
        Me.chkMovimientoCombustible.TabIndex = 77
        Me.chkMovimientoCombustible.Texto = Nothing
        Me.chkMovimientoCombustible.Valor = False
        '
        'chkDescargaStock
        '
        Me.chkDescargaStock.BackColor = System.Drawing.Color.Transparent
        Me.chkDescargaStock.Color = System.Drawing.Color.Empty
        Me.chkDescargaStock.Location = New System.Drawing.Point(48, 310)
        Me.chkDescargaStock.Name = "chkDescargaStock"
        Me.chkDescargaStock.Size = New System.Drawing.Size(15, 16)
        Me.chkDescargaStock.SoloLectura = False
        Me.chkDescargaStock.TabIndex = 76
        Me.chkDescargaStock.Texto = Nothing
        Me.chkDescargaStock.Valor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(65, 329)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 13)
        Me.Label6.TabIndex = 75
        Me.Label6.Text = "Combustible"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(65, 309)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(99, 13)
        Me.Label3.TabIndex = 74
        Me.Label3.Text = "Descarga de Stock"
        '
        'chkVendible
        '
        Me.chkVendible.BackColor = System.Drawing.Color.Transparent
        Me.chkVendible.Color = System.Drawing.Color.Empty
        Me.chkVendible.Location = New System.Drawing.Point(572, 120)
        Me.chkVendible.Name = "chkVendible"
        Me.chkVendible.Size = New System.Drawing.Size(18, 15)
        Me.chkVendible.SoloLectura = False
        Me.chkVendible.TabIndex = 73
        Me.chkVendible.Texto = Nothing
        Me.chkVendible.Valor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(512, 119)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 13)
        Me.Label2.TabIndex = 72
        Me.Label2.Text = "Vendible:"
        '
        'ocxCuentaDeudor
        '
        Me.ocxCuentaDeudor.AlturaMaxima = 40
        Me.ocxCuentaDeudor.Consulta = Nothing
        Me.ocxCuentaDeudor.IDCentroCosto = 0
        Me.ocxCuentaDeudor.IDUnidadNegocio = 0
        Me.ocxCuentaDeudor.ListarTodas = False
        Me.ocxCuentaDeudor.Location = New System.Drawing.Point(427, 273)
        Me.ocxCuentaDeudor.Name = "ocxCuentaDeudor"
        Me.ocxCuentaDeudor.Registro = Nothing
        Me.ocxCuentaDeudor.Resolucion173 = False
        Me.ocxCuentaDeudor.Seleccionado = False
        Me.ocxCuentaDeudor.Size = New System.Drawing.Size(330, 24)
        Me.ocxCuentaDeudor.SoloLectura = False
        Me.ocxCuentaDeudor.TabIndex = 49
        Me.ocxCuentaDeudor.Texto = Nothing
        Me.ocxCuentaDeudor.whereFiltro = Nothing
        '
        'lblCuentaContableDeudor
        '
        Me.lblCuentaContableDeudor.AutoSize = True
        Me.lblCuentaContableDeudor.Location = New System.Drawing.Point(329, 278)
        Me.lblCuentaContableDeudor.Name = "lblCuentaContableDeudor"
        Me.lblCuentaContableDeudor.Size = New System.Drawing.Size(92, 13)
        Me.lblCuentaContableDeudor.TabIndex = 48
        Me.lblCuentaContableDeudor.Text = "Cuenta C Deudor:"
        '
        'lblPorcentaje
        '
        Me.lblPorcentaje.AutoSize = True
        Me.lblPorcentaje.Location = New System.Drawing.Point(463, 119)
        Me.lblPorcentaje.Name = "lblPorcentaje"
        Me.lblPorcentaje.Size = New System.Drawing.Size(15, 13)
        Me.lblPorcentaje.TabIndex = 35
        Me.lblPorcentaje.Text = "%"
        '
        'txtMargenMinimo
        '
        Me.txtMargenMinimo.Color = System.Drawing.Color.Empty
        Me.txtMargenMinimo.Decimales = True
        Me.txtMargenMinimo.Indicaciones = Nothing
        Me.txtMargenMinimo.Location = New System.Drawing.Point(427, 114)
        Me.txtMargenMinimo.Name = "txtMargenMinimo"
        Me.txtMargenMinimo.Size = New System.Drawing.Size(34, 22)
        Me.txtMargenMinimo.SoloLectura = False
        Me.txtMargenMinimo.TabIndex = 34
        Me.txtMargenMinimo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMargenMinimo.Texto = "0"
        '
        'lblMargen
        '
        Me.lblMargen.AutoSize = True
        Me.lblMargen.Location = New System.Drawing.Point(352, 119)
        Me.lblMargen.Name = "lblMargen"
        Me.lblMargen.Size = New System.Drawing.Size(69, 13)
        Me.lblMargen.TabIndex = 33
        Me.lblMargen.Text = "Margen Min.:"
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(682, 379)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 71
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(171, 379)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 66
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(252, 379)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 67
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'ocxCuentaCosto
        '
        Me.ocxCuentaCosto.AlturaMaxima = 40
        Me.ocxCuentaCosto.Consulta = Nothing
        Me.ocxCuentaCosto.IDCentroCosto = 0
        Me.ocxCuentaCosto.IDUnidadNegocio = 0
        Me.ocxCuentaCosto.ListarTodas = False
        Me.ocxCuentaCosto.Location = New System.Drawing.Point(427, 247)
        Me.ocxCuentaCosto.Name = "ocxCuentaCosto"
        Me.ocxCuentaCosto.Registro = Nothing
        Me.ocxCuentaCosto.Resolucion173 = False
        Me.ocxCuentaCosto.Seleccionado = False
        Me.ocxCuentaCosto.Size = New System.Drawing.Size(330, 24)
        Me.ocxCuentaCosto.SoloLectura = False
        Me.ocxCuentaCosto.TabIndex = 47
        Me.ocxCuentaCosto.Texto = Nothing
        Me.ocxCuentaCosto.whereFiltro = Nothing
        '
        'ocxCuentaVenta
        '
        Me.ocxCuentaVenta.AlturaMaxima = 40
        Me.ocxCuentaVenta.Consulta = Nothing
        Me.ocxCuentaVenta.IDCentroCosto = 0
        Me.ocxCuentaVenta.IDUnidadNegocio = 0
        Me.ocxCuentaVenta.ListarTodas = False
        Me.ocxCuentaVenta.Location = New System.Drawing.Point(427, 221)
        Me.ocxCuentaVenta.Name = "ocxCuentaVenta"
        Me.ocxCuentaVenta.Registro = Nothing
        Me.ocxCuentaVenta.Resolucion173 = False
        Me.ocxCuentaVenta.Seleccionado = False
        Me.ocxCuentaVenta.Size = New System.Drawing.Size(330, 24)
        Me.ocxCuentaVenta.SoloLectura = False
        Me.ocxCuentaVenta.TabIndex = 45
        Me.ocxCuentaVenta.Texto = Nothing
        Me.ocxCuentaVenta.whereFiltro = Nothing
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(333, 379)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 68
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'ocxCuentaCompra
        '
        Me.ocxCuentaCompra.AlturaMaxima = 63
        Me.ocxCuentaCompra.Consulta = Nothing
        Me.ocxCuentaCompra.IDCentroCosto = 0
        Me.ocxCuentaCompra.IDUnidadNegocio = 0
        Me.ocxCuentaCompra.ListarTodas = False
        Me.ocxCuentaCompra.Location = New System.Drawing.Point(427, 195)
        Me.ocxCuentaCompra.Name = "ocxCuentaCompra"
        Me.ocxCuentaCompra.Registro = Nothing
        Me.ocxCuentaCompra.Resolucion173 = False
        Me.ocxCuentaCompra.Seleccionado = False
        Me.ocxCuentaCompra.Size = New System.Drawing.Size(330, 24)
        Me.ocxCuentaCompra.SoloLectura = False
        Me.ocxCuentaCompra.TabIndex = 43
        Me.ocxCuentaCompra.Texto = Nothing
        Me.ocxCuentaCompra.whereFiltro = Nothing
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(414, 379)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 69
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'lblCuentaContableCosto
        '
        Me.lblCuentaContableCosto.AutoSize = True
        Me.lblCuentaContableCosto.Location = New System.Drawing.Point(337, 252)
        Me.lblCuentaContableCosto.Name = "lblCuentaContableCosto"
        Me.lblCuentaContableCosto.Size = New System.Drawing.Size(84, 13)
        Me.lblCuentaContableCosto.TabIndex = 46
        Me.lblCuentaContableCosto.Text = "Cuenta C Costo:"
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(495, 379)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 70
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'txtUnidadConversion
        '
        Me.txtUnidadConversion.BackColor = System.Drawing.Color.White
        Me.txtUnidadConversion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUnidadConversion.Color = System.Drawing.Color.Empty
        Me.txtUnidadConversion.Indicaciones = Nothing
        Me.txtUnidadConversion.Location = New System.Drawing.Point(551, 32)
        Me.txtUnidadConversion.Multilinea = False
        Me.txtUnidadConversion.Name = "txtUnidadConversion"
        Me.txtUnidadConversion.Size = New System.Drawing.Size(32, 21)
        Me.txtUnidadConversion.SoloLectura = False
        Me.txtUnidadConversion.TabIndex = 28
        Me.txtUnidadConversion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUnidadConversion.Texto = "1"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(501, 37)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 27
        Me.Label4.Text = "Cantidad:"
        '
        'cbxUnidadMedidaConversion
        '
        Me.cbxUnidadMedidaConversion.CampoWhere = Nothing
        Me.cbxUnidadMedidaConversion.CargarUnaSolaVez = False
        Me.cbxUnidadMedidaConversion.DataDisplayMember = Nothing
        Me.cbxUnidadMedidaConversion.DataFilter = Nothing
        Me.cbxUnidadMedidaConversion.DataOrderBy = Nothing
        Me.cbxUnidadMedidaConversion.DataSource = Nothing
        Me.cbxUnidadMedidaConversion.DataValueMember = Nothing
        Me.cbxUnidadMedidaConversion.dtSeleccionado = Nothing
        Me.cbxUnidadMedidaConversion.FormABM = Nothing
        Me.cbxUnidadMedidaConversion.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxUnidadMedidaConversion.Location = New System.Drawing.Point(427, 33)
        Me.cbxUnidadMedidaConversion.Name = "cbxUnidadMedidaConversion"
        Me.cbxUnidadMedidaConversion.SeleccionMultiple = False
        Me.cbxUnidadMedidaConversion.SeleccionObligatoria = False
        Me.cbxUnidadMedidaConversion.Size = New System.Drawing.Size(73, 21)
        Me.cbxUnidadMedidaConversion.SoloLectura = False
        Me.cbxUnidadMedidaConversion.TabIndex = 26
        Me.cbxUnidadMedidaConversion.Texto = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(349, 37)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 13)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Conversion a:"
        '
        'txtPeso
        '
        Me.txtPeso.BackColor = System.Drawing.Color.White
        Me.txtPeso.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPeso.Color = System.Drawing.Color.Empty
        Me.txtPeso.Indicaciones = Nothing
        Me.txtPeso.Location = New System.Drawing.Point(427, 60)
        Me.txtPeso.Multilinea = False
        Me.txtPeso.Name = "txtPeso"
        Me.txtPeso.Size = New System.Drawing.Size(156, 21)
        Me.txtPeso.SoloLectura = False
        Me.txtPeso.TabIndex = 30
        Me.txtPeso.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPeso.Texto = ""
        '
        'chkControlarExistencia
        '
        Me.chkControlarExistencia.BackColor = System.Drawing.Color.Transparent
        Me.chkControlarExistencia.Color = System.Drawing.Color.Empty
        Me.chkControlarExistencia.Location = New System.Drawing.Point(427, 173)
        Me.chkControlarExistencia.Name = "chkControlarExistencia"
        Me.chkControlarExistencia.Size = New System.Drawing.Size(18, 21)
        Me.chkControlarExistencia.SoloLectura = False
        Me.chkControlarExistencia.TabIndex = 39
        Me.chkControlarExistencia.Texto = Nothing
        Me.chkControlarExistencia.Valor = False
        '
        'chkActivo
        '
        Me.chkActivo.BackColor = System.Drawing.Color.Transparent
        Me.chkActivo.Color = System.Drawing.Color.Empty
        Me.chkActivo.Location = New System.Drawing.Point(506, 173)
        Me.chkActivo.Name = "chkActivo"
        Me.chkActivo.Size = New System.Drawing.Size(18, 21)
        Me.chkActivo.SoloLectura = False
        Me.chkActivo.TabIndex = 41
        Me.chkActivo.Texto = Nothing
        Me.chkActivo.Valor = False
        '
        'cbxSubLinea2
        '
        Me.cbxSubLinea2.CampoWhere = Nothing
        Me.cbxSubLinea2.CargarUnaSolaVez = False
        Me.cbxSubLinea2.DataDisplayMember = Nothing
        Me.cbxSubLinea2.DataFilter = Nothing
        Me.cbxSubLinea2.DataOrderBy = Nothing
        Me.cbxSubLinea2.DataSource = Nothing
        Me.cbxSubLinea2.DataValueMember = Nothing
        Me.cbxSubLinea2.dtSeleccionado = Nothing
        Me.cbxSubLinea2.FormABM = Nothing
        Me.cbxSubLinea2.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSubLinea2.Location = New System.Drawing.Point(104, 87)
        Me.cbxSubLinea2.Name = "cbxSubLinea2"
        Me.cbxSubLinea2.SeleccionMultiple = False
        Me.cbxSubLinea2.SeleccionObligatoria = False
        Me.cbxSubLinea2.Size = New System.Drawing.Size(208, 21)
        Me.cbxSubLinea2.SoloLectura = False
        Me.cbxSubLinea2.TabIndex = 7
        Me.cbxSubLinea2.Texto = ""
        '
        'lblSubLinea2
        '
        Me.lblSubLinea2.AutoSize = True
        Me.lblSubLinea2.Location = New System.Drawing.Point(6, 91)
        Me.lblSubLinea2.Name = "lblSubLinea2"
        Me.lblSubLinea2.Size = New System.Drawing.Size(67, 13)
        Me.lblSubLinea2.TabIndex = 6
        Me.lblSubLinea2.Text = "Sub-Linea 2:"
        '
        'cbxSubLinea
        '
        Me.cbxSubLinea.CampoWhere = Nothing
        Me.cbxSubLinea.CargarUnaSolaVez = False
        Me.cbxSubLinea.DataDisplayMember = Nothing
        Me.cbxSubLinea.DataFilter = Nothing
        Me.cbxSubLinea.DataOrderBy = Nothing
        Me.cbxSubLinea.DataSource = Nothing
        Me.cbxSubLinea.DataValueMember = Nothing
        Me.cbxSubLinea.dtSeleccionado = Nothing
        Me.cbxSubLinea.FormABM = Nothing
        Me.cbxSubLinea.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSubLinea.Location = New System.Drawing.Point(104, 60)
        Me.cbxSubLinea.Name = "cbxSubLinea"
        Me.cbxSubLinea.SeleccionMultiple = False
        Me.cbxSubLinea.SeleccionObligatoria = False
        Me.cbxSubLinea.Size = New System.Drawing.Size(208, 21)
        Me.cbxSubLinea.SoloLectura = False
        Me.cbxSubLinea.TabIndex = 5
        Me.cbxSubLinea.Texto = ""
        '
        'lblSubLinea
        '
        Me.lblSubLinea.AutoSize = True
        Me.lblSubLinea.Location = New System.Drawing.Point(6, 64)
        Me.lblSubLinea.Name = "lblSubLinea"
        Me.lblSubLinea.Size = New System.Drawing.Size(58, 13)
        Me.lblSubLinea.TabIndex = 4
        Me.lblSubLinea.Text = "Sub-Linea:"
        '
        'lblCuentaVenta
        '
        Me.lblCuentaVenta.AutoSize = True
        Me.lblCuentaVenta.Location = New System.Drawing.Point(336, 226)
        Me.lblCuentaVenta.Name = "lblCuentaVenta"
        Me.lblCuentaVenta.Size = New System.Drawing.Size(85, 13)
        Me.lblCuentaVenta.TabIndex = 44
        Me.lblCuentaVenta.Text = "Cuenta C Venta:"
        '
        'lblCuentaCompra
        '
        Me.lblCuentaCompra.AutoSize = True
        Me.lblCuentaCompra.Location = New System.Drawing.Point(328, 200)
        Me.lblCuentaCompra.Name = "lblCuentaCompra"
        Me.lblCuentaCompra.Size = New System.Drawing.Size(93, 13)
        Me.lblCuentaCompra.TabIndex = 42
        Me.lblCuentaCompra.Text = "Cuenta C Compra:"
        '
        'txtVolumen
        '
        Me.txtVolumen.BackColor = System.Drawing.Color.White
        Me.txtVolumen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVolumen.Color = System.Drawing.Color.Empty
        Me.txtVolumen.Indicaciones = Nothing
        Me.txtVolumen.Location = New System.Drawing.Point(427, 87)
        Me.txtVolumen.Multilinea = False
        Me.txtVolumen.Name = "txtVolumen"
        Me.txtVolumen.Size = New System.Drawing.Size(156, 21)
        Me.txtVolumen.SoloLectura = False
        Me.txtVolumen.TabIndex = 32
        Me.txtVolumen.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtVolumen.Texto = ""
        '
        'lblControlarExistencia
        '
        Me.lblControlarExistencia.AutoSize = True
        Me.lblControlarExistencia.Location = New System.Drawing.Point(318, 172)
        Me.lblControlarExistencia.Name = "lblControlarExistencia"
        Me.lblControlarExistencia.Size = New System.Drawing.Size(103, 13)
        Me.lblControlarExistencia.TabIndex = 38
        Me.lblControlarExistencia.Text = "Controlar Existencia:"
        '
        'lblActivo
        '
        Me.lblActivo.AutoSize = True
        Me.lblActivo.Location = New System.Drawing.Point(463, 172)
        Me.lblActivo.Name = "lblActivo"
        Me.lblActivo.Size = New System.Drawing.Size(40, 13)
        Me.lblActivo.TabIndex = 40
        Me.lblActivo.Text = "Activo:"
        '
        'cbxImpuesto
        '
        Me.cbxImpuesto.CampoWhere = Nothing
        Me.cbxImpuesto.CargarUnaSolaVez = False
        Me.cbxImpuesto.DataDisplayMember = Nothing
        Me.cbxImpuesto.DataFilter = Nothing
        Me.cbxImpuesto.DataOrderBy = Nothing
        Me.cbxImpuesto.DataSource = Nothing
        Me.cbxImpuesto.DataValueMember = Nothing
        Me.cbxImpuesto.dtSeleccionado = Nothing
        Me.cbxImpuesto.FormABM = Nothing
        Me.cbxImpuesto.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxImpuesto.Location = New System.Drawing.Point(427, 141)
        Me.cbxImpuesto.Name = "cbxImpuesto"
        Me.cbxImpuesto.SeleccionMultiple = False
        Me.cbxImpuesto.SeleccionObligatoria = False
        Me.cbxImpuesto.Size = New System.Drawing.Size(156, 21)
        Me.cbxImpuesto.SoloLectura = False
        Me.cbxImpuesto.TabIndex = 37
        Me.cbxImpuesto.Texto = ""
        '
        'lblCostoPromedio
        '
        Me.lblCostoPromedio.AutoSize = True
        Me.lblCostoPromedio.Location = New System.Drawing.Point(586, 172)
        Me.lblCostoPromedio.Name = "lblCostoPromedio"
        Me.lblCostoPromedio.Size = New System.Drawing.Size(84, 13)
        Me.lblCostoPromedio.TabIndex = 64
        Me.lblCostoPromedio.Text = "Costo Promedio:"
        '
        'txtCostoPromedio
        '
        Me.txtCostoPromedio.BackColor = System.Drawing.Color.White
        Me.txtCostoPromedio.Location = New System.Drawing.Point(673, 168)
        Me.txtCostoPromedio.Name = "txtCostoPromedio"
        Me.txtCostoPromedio.ReadOnly = True
        Me.txtCostoPromedio.Size = New System.Drawing.Size(84, 20)
        Me.txtCostoPromedio.TabIndex = 65
        Me.txtCostoPromedio.TabStop = False
        Me.ToolTip1.SetToolTip(Me.txtCostoPromedio, "Fecha de Alta del Cliente")
        '
        'lblCostoCG
        '
        Me.lblCostoCG.AutoSize = True
        Me.lblCostoCG.Location = New System.Drawing.Point(615, 145)
        Me.lblCostoCG.Name = "lblCostoCG"
        Me.lblCostoCG.Size = New System.Drawing.Size(55, 13)
        Me.lblCostoCG.TabIndex = 62
        Me.lblCostoCG.Text = "Costo CG:"
        '
        'txtCostoCG
        '
        Me.txtCostoCG.BackColor = System.Drawing.Color.White
        Me.txtCostoCG.Location = New System.Drawing.Point(673, 141)
        Me.txtCostoCG.Name = "txtCostoCG"
        Me.txtCostoCG.ReadOnly = True
        Me.txtCostoCG.Size = New System.Drawing.Size(84, 20)
        Me.txtCostoCG.TabIndex = 63
        Me.txtCostoCG.TabStop = False
        Me.ToolTip1.SetToolTip(Me.txtCostoCG, "Fecha de Alta del Cliente")
        '
        'cbxProcedencia
        '
        Me.cbxProcedencia.CampoWhere = Nothing
        Me.cbxProcedencia.CargarUnaSolaVez = False
        Me.cbxProcedencia.DataDisplayMember = Nothing
        Me.cbxProcedencia.DataFilter = Nothing
        Me.cbxProcedencia.DataOrderBy = Nothing
        Me.cbxProcedencia.DataSource = Nothing
        Me.cbxProcedencia.DataValueMember = Nothing
        Me.cbxProcedencia.dtSeleccionado = Nothing
        Me.cbxProcedencia.FormABM = Nothing
        Me.cbxProcedencia.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxProcedencia.Location = New System.Drawing.Point(104, 249)
        Me.cbxProcedencia.Name = "cbxProcedencia"
        Me.cbxProcedencia.SeleccionMultiple = False
        Me.cbxProcedencia.SeleccionObligatoria = False
        Me.cbxProcedencia.Size = New System.Drawing.Size(208, 21)
        Me.cbxProcedencia.SoloLectura = False
        Me.cbxProcedencia.TabIndex = 19
        Me.cbxProcedencia.Texto = ""
        '
        'lblProcedencia
        '
        Me.lblProcedencia.AutoSize = True
        Me.lblProcedencia.Location = New System.Drawing.Point(6, 253)
        Me.lblProcedencia.Name = "lblProcedencia"
        Me.lblProcedencia.Size = New System.Drawing.Size(70, 13)
        Me.lblProcedencia.TabIndex = 18
        Me.lblProcedencia.Text = "Procedencia:"
        '
        'lblCotizacion
        '
        Me.lblCotizacion.AutoSize = True
        Me.lblCotizacion.Location = New System.Drawing.Point(611, 118)
        Me.lblCotizacion.Name = "lblCotizacion"
        Me.lblCotizacion.Size = New System.Drawing.Size(59, 13)
        Me.lblCotizacion.TabIndex = 60
        Me.lblCotizacion.Text = "Cotización:"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.BackColor = System.Drawing.Color.White
        Me.txtCotizacion.Location = New System.Drawing.Point(673, 114)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.ReadOnly = True
        Me.txtCotizacion.Size = New System.Drawing.Size(84, 20)
        Me.txtCotizacion.TabIndex = 61
        Me.txtCotizacion.TabStop = False
        Me.ToolTip1.SetToolTip(Me.txtCotizacion, "Fecha de Alta del Cliente")
        '
        'lblUltimoCostoSinIVA
        '
        Me.lblUltimoCostoSinIVA.AutoSize = True
        Me.lblUltimoCostoSinIVA.Location = New System.Drawing.Point(625, 91)
        Me.lblUltimoCostoSinIVA.Name = "lblUltimoCostoSinIVA"
        Me.lblUltimoCostoSinIVA.Size = New System.Drawing.Size(45, 13)
        Me.lblUltimoCostoSinIVA.TabIndex = 58
        Me.lblUltimoCostoSinIVA.Text = "Sin IVA:"
        '
        'txtUltimoCostoSinIVA
        '
        Me.txtUltimoCostoSinIVA.BackColor = System.Drawing.Color.White
        Me.txtUltimoCostoSinIVA.Location = New System.Drawing.Point(673, 87)
        Me.txtUltimoCostoSinIVA.Name = "txtUltimoCostoSinIVA"
        Me.txtUltimoCostoSinIVA.ReadOnly = True
        Me.txtUltimoCostoSinIVA.Size = New System.Drawing.Size(84, 20)
        Me.txtUltimoCostoSinIVA.TabIndex = 59
        Me.txtUltimoCostoSinIVA.TabStop = False
        Me.ToolTip1.SetToolTip(Me.txtUltimoCostoSinIVA, "Fecha de Alta del Cliente")
        '
        'lblUltimoCosto
        '
        Me.lblUltimoCosto.AutoSize = True
        Me.lblUltimoCosto.Location = New System.Drawing.Point(601, 64)
        Me.lblUltimoCosto.Name = "lblUltimoCosto"
        Me.lblUltimoCosto.Size = New System.Drawing.Size(69, 13)
        Me.lblUltimoCosto.TabIndex = 56
        Me.lblUltimoCosto.Text = "Ultimo Costo:"
        '
        'txtUltimoCosto
        '
        Me.txtUltimoCosto.BackColor = System.Drawing.Color.White
        Me.txtUltimoCosto.Location = New System.Drawing.Point(673, 60)
        Me.txtUltimoCosto.Name = "txtUltimoCosto"
        Me.txtUltimoCosto.ReadOnly = True
        Me.txtUltimoCosto.Size = New System.Drawing.Size(84, 20)
        Me.txtUltimoCosto.TabIndex = 57
        Me.txtUltimoCosto.TabStop = False
        Me.ToolTip1.SetToolTip(Me.txtUltimoCosto, "Fecha de Alta del Cliente")
        '
        'txtCantidadUltimaSalida
        '
        Me.txtCantidadUltimaSalida.BackColor = System.Drawing.Color.White
        Me.txtCantidadUltimaSalida.Location = New System.Drawing.Point(763, 33)
        Me.txtCantidadUltimaSalida.Name = "txtCantidadUltimaSalida"
        Me.txtCantidadUltimaSalida.ReadOnly = True
        Me.txtCantidadUltimaSalida.Size = New System.Drawing.Size(46, 20)
        Me.txtCantidadUltimaSalida.TabIndex = 55
        Me.txtCantidadUltimaSalida.TabStop = False
        Me.ToolTip1.SetToolTip(Me.txtCantidadUltimaSalida, "Fecha de Alta del Cliente")
        '
        'lblUltimaSalida
        '
        Me.lblUltimaSalida.AutoSize = True
        Me.lblUltimaSalida.Location = New System.Drawing.Point(599, 37)
        Me.lblUltimaSalida.Name = "lblUltimaSalida"
        Me.lblUltimaSalida.Size = New System.Drawing.Size(71, 13)
        Me.lblUltimaSalida.TabIndex = 53
        Me.lblUltimaSalida.Text = "Ultima Salida:"
        '
        'txtFechaUltimaSalida
        '
        Me.txtFechaUltimaSalida.BackColor = System.Drawing.Color.White
        Me.txtFechaUltimaSalida.Location = New System.Drawing.Point(673, 33)
        Me.txtFechaUltimaSalida.Name = "txtFechaUltimaSalida"
        Me.txtFechaUltimaSalida.ReadOnly = True
        Me.txtFechaUltimaSalida.Size = New System.Drawing.Size(84, 20)
        Me.txtFechaUltimaSalida.TabIndex = 54
        Me.txtFechaUltimaSalida.TabStop = False
        Me.ToolTip1.SetToolTip(Me.txtFechaUltimaSalida, "Fecha de Alta del Cliente")
        '
        'txtCantidadUltimaEntrada
        '
        Me.txtCantidadUltimaEntrada.BackColor = System.Drawing.Color.White
        Me.txtCantidadUltimaEntrada.Location = New System.Drawing.Point(763, 6)
        Me.txtCantidadUltimaEntrada.Name = "txtCantidadUltimaEntrada"
        Me.txtCantidadUltimaEntrada.ReadOnly = True
        Me.txtCantidadUltimaEntrada.Size = New System.Drawing.Size(46, 20)
        Me.txtCantidadUltimaEntrada.TabIndex = 52
        Me.txtCantidadUltimaEntrada.TabStop = False
        Me.ToolTip1.SetToolTip(Me.txtCantidadUltimaEntrada, "Fecha de Alta del Cliente")
        '
        'lblUltimaEntrada
        '
        Me.lblUltimaEntrada.AutoSize = True
        Me.lblUltimaEntrada.Location = New System.Drawing.Point(591, 10)
        Me.lblUltimaEntrada.Name = "lblUltimaEntrada"
        Me.lblUltimaEntrada.Size = New System.Drawing.Size(79, 13)
        Me.lblUltimaEntrada.TabIndex = 50
        Me.lblUltimaEntrada.Text = "Ultima Entrada:"
        '
        'txtFechaUltimaEntrada
        '
        Me.txtFechaUltimaEntrada.BackColor = System.Drawing.Color.White
        Me.txtFechaUltimaEntrada.Location = New System.Drawing.Point(673, 6)
        Me.txtFechaUltimaEntrada.Name = "txtFechaUltimaEntrada"
        Me.txtFechaUltimaEntrada.ReadOnly = True
        Me.txtFechaUltimaEntrada.Size = New System.Drawing.Size(84, 20)
        Me.txtFechaUltimaEntrada.TabIndex = 51
        Me.txtFechaUltimaEntrada.TabStop = False
        Me.ToolTip1.SetToolTip(Me.txtFechaUltimaEntrada, "Fecha de Alta del Cliente")
        '
        'lblVolumen
        '
        Me.lblVolumen.AutoSize = True
        Me.lblVolumen.Location = New System.Drawing.Point(370, 91)
        Me.lblVolumen.Name = "lblVolumen"
        Me.lblVolumen.Size = New System.Drawing.Size(51, 13)
        Me.lblVolumen.TabIndex = 31
        Me.lblVolumen.Text = "Volumen:"
        '
        'lblPeso
        '
        Me.lblPeso.AutoSize = True
        Me.lblPeso.Location = New System.Drawing.Point(387, 64)
        Me.lblPeso.Name = "lblPeso"
        Me.lblPeso.Size = New System.Drawing.Size(34, 13)
        Me.lblPeso.TabIndex = 29
        Me.lblPeso.Text = "Peso:"
        '
        'txtUnidadPorCaja
        '
        Me.txtUnidadPorCaja.Color = System.Drawing.Color.Empty
        Me.txtUnidadPorCaja.Decimales = True
        Me.txtUnidadPorCaja.Indicaciones = Nothing
        Me.txtUnidadPorCaja.Location = New System.Drawing.Point(549, 5)
        Me.txtUnidadPorCaja.Name = "txtUnidadPorCaja"
        Me.txtUnidadPorCaja.Size = New System.Drawing.Size(34, 22)
        Me.txtUnidadPorCaja.SoloLectura = False
        Me.txtUnidadPorCaja.TabIndex = 24
        Me.txtUnidadPorCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtUnidadPorCaja.Texto = "1"
        '
        'lblUnidadPorCaja
        '
        Me.lblUnidadPorCaja.AutoSize = True
        Me.lblUnidadPorCaja.Location = New System.Drawing.Point(501, 10)
        Me.lblUnidadPorCaja.Name = "lblUnidadPorCaja"
        Me.lblUnidadPorCaja.Size = New System.Drawing.Size(50, 13)
        Me.lblUnidadPorCaja.TabIndex = 23
        Me.lblUnidadPorCaja.Text = "Por Caja:"
        '
        'cbxUnidadMedida
        '
        Me.cbxUnidadMedida.CampoWhere = Nothing
        Me.cbxUnidadMedida.CargarUnaSolaVez = False
        Me.cbxUnidadMedida.DataDisplayMember = Nothing
        Me.cbxUnidadMedida.DataFilter = Nothing
        Me.cbxUnidadMedida.DataOrderBy = Nothing
        Me.cbxUnidadMedida.DataSource = Nothing
        Me.cbxUnidadMedida.DataValueMember = Nothing
        Me.cbxUnidadMedida.dtSeleccionado = Nothing
        Me.cbxUnidadMedida.FormABM = Nothing
        Me.cbxUnidadMedida.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxUnidadMedida.Location = New System.Drawing.Point(427, 6)
        Me.cbxUnidadMedida.Name = "cbxUnidadMedida"
        Me.cbxUnidadMedida.SeleccionMultiple = False
        Me.cbxUnidadMedida.SeleccionObligatoria = False
        Me.cbxUnidadMedida.Size = New System.Drawing.Size(73, 21)
        Me.cbxUnidadMedida.SoloLectura = False
        Me.cbxUnidadMedida.TabIndex = 22
        Me.cbxUnidadMedida.Texto = ""
        '
        'lblUnidadMedida
        '
        Me.lblUnidadMedida.AutoSize = True
        Me.lblUnidadMedida.Location = New System.Drawing.Point(324, 10)
        Me.lblUnidadMedida.Name = "lblUnidadMedida"
        Me.lblUnidadMedida.Size = New System.Drawing.Size(97, 13)
        Me.lblUnidadMedida.TabIndex = 21
        Me.lblUnidadMedida.Text = "Unidad de Medida:"
        '
        'lblImpuesto
        '
        Me.lblImpuesto.AutoSize = True
        Me.lblImpuesto.Location = New System.Drawing.Point(368, 145)
        Me.lblImpuesto.Name = "lblImpuesto"
        Me.lblImpuesto.Size = New System.Drawing.Size(53, 13)
        Me.lblImpuesto.TabIndex = 36
        Me.lblImpuesto.Text = "Impuesto:"
        '
        'cbxCategoria
        '
        Me.cbxCategoria.CampoWhere = Nothing
        Me.cbxCategoria.CargarUnaSolaVez = False
        Me.cbxCategoria.DataDisplayMember = Nothing
        Me.cbxCategoria.DataFilter = Nothing
        Me.cbxCategoria.DataOrderBy = Nothing
        Me.cbxCategoria.DataSource = Nothing
        Me.cbxCategoria.DataValueMember = Nothing
        Me.cbxCategoria.dtSeleccionado = Nothing
        Me.cbxCategoria.FormABM = Nothing
        Me.cbxCategoria.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxCategoria.Location = New System.Drawing.Point(104, 168)
        Me.cbxCategoria.Name = "cbxCategoria"
        Me.cbxCategoria.SeleccionMultiple = False
        Me.cbxCategoria.SeleccionObligatoria = False
        Me.cbxCategoria.Size = New System.Drawing.Size(208, 21)
        Me.cbxCategoria.SoloLectura = False
        Me.cbxCategoria.TabIndex = 13
        Me.cbxCategoria.Texto = ""
        '
        'lblCategoria
        '
        Me.lblCategoria.AutoSize = True
        Me.lblCategoria.Location = New System.Drawing.Point(6, 172)
        Me.lblCategoria.Name = "lblCategoria"
        Me.lblCategoria.Size = New System.Drawing.Size(55, 13)
        Me.lblCategoria.TabIndex = 12
        Me.lblCategoria.Text = "Categoria:"
        '
        'cbxDivision
        '
        Me.cbxDivision.CampoWhere = Nothing
        Me.cbxDivision.CargarUnaSolaVez = False
        Me.cbxDivision.DataDisplayMember = Nothing
        Me.cbxDivision.DataFilter = Nothing
        Me.cbxDivision.DataOrderBy = Nothing
        Me.cbxDivision.DataSource = Nothing
        Me.cbxDivision.DataValueMember = Nothing
        Me.cbxDivision.dtSeleccionado = Nothing
        Me.cbxDivision.FormABM = Nothing
        Me.cbxDivision.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxDivision.Location = New System.Drawing.Point(104, 222)
        Me.cbxDivision.Name = "cbxDivision"
        Me.cbxDivision.SeleccionMultiple = False
        Me.cbxDivision.SeleccionObligatoria = False
        Me.cbxDivision.Size = New System.Drawing.Size(208, 21)
        Me.cbxDivision.SoloLectura = False
        Me.cbxDivision.TabIndex = 17
        Me.cbxDivision.Texto = ""
        '
        'lblDivision
        '
        Me.lblDivision.AutoSize = True
        Me.lblDivision.Location = New System.Drawing.Point(6, 226)
        Me.lblDivision.Name = "lblDivision"
        Me.lblDivision.Size = New System.Drawing.Size(47, 13)
        Me.lblDivision.TabIndex = 16
        Me.lblDivision.Text = "Division:"
        '
        'cbxProveedor
        '
        Me.cbxProveedor.CampoWhere = Nothing
        Me.cbxProveedor.CargarUnaSolaVez = False
        Me.cbxProveedor.DataDisplayMember = Nothing
        Me.cbxProveedor.DataFilter = Nothing
        Me.cbxProveedor.DataOrderBy = Nothing
        Me.cbxProveedor.DataSource = Nothing
        Me.cbxProveedor.DataValueMember = Nothing
        Me.cbxProveedor.dtSeleccionado = Nothing
        Me.cbxProveedor.FormABM = Nothing
        Me.cbxProveedor.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxProveedor.Location = New System.Drawing.Point(104, 195)
        Me.cbxProveedor.Name = "cbxProveedor"
        Me.cbxProveedor.SeleccionMultiple = False
        Me.cbxProveedor.SeleccionObligatoria = False
        Me.cbxProveedor.Size = New System.Drawing.Size(208, 21)
        Me.cbxProveedor.SoloLectura = False
        Me.cbxProveedor.TabIndex = 15
        Me.cbxProveedor.Texto = ""
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(6, 199)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(59, 13)
        Me.lblProveedor.TabIndex = 14
        Me.lblProveedor.Text = "Proveedor:"
        '
        'cbxPresentacion
        '
        Me.cbxPresentacion.CampoWhere = Nothing
        Me.cbxPresentacion.CargarUnaSolaVez = False
        Me.cbxPresentacion.DataDisplayMember = Nothing
        Me.cbxPresentacion.DataFilter = Nothing
        Me.cbxPresentacion.DataOrderBy = Nothing
        Me.cbxPresentacion.DataSource = Nothing
        Me.cbxPresentacion.DataValueMember = Nothing
        Me.cbxPresentacion.dtSeleccionado = Nothing
        Me.cbxPresentacion.FormABM = Nothing
        Me.cbxPresentacion.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxPresentacion.Location = New System.Drawing.Point(104, 141)
        Me.cbxPresentacion.Name = "cbxPresentacion"
        Me.cbxPresentacion.SeleccionMultiple = False
        Me.cbxPresentacion.SeleccionObligatoria = False
        Me.cbxPresentacion.Size = New System.Drawing.Size(208, 21)
        Me.cbxPresentacion.SoloLectura = False
        Me.cbxPresentacion.TabIndex = 11
        Me.cbxPresentacion.Texto = ""
        '
        'lblPresentacion
        '
        Me.lblPresentacion.AutoSize = True
        Me.lblPresentacion.Location = New System.Drawing.Point(6, 145)
        Me.lblPresentacion.Name = "lblPresentacion"
        Me.lblPresentacion.Size = New System.Drawing.Size(72, 13)
        Me.lblPresentacion.TabIndex = 10
        Me.lblPresentacion.Text = "Presentacion:"
        '
        'cbxMarca
        '
        Me.cbxMarca.CampoWhere = Nothing
        Me.cbxMarca.CargarUnaSolaVez = False
        Me.cbxMarca.DataDisplayMember = Nothing
        Me.cbxMarca.DataFilter = Nothing
        Me.cbxMarca.DataOrderBy = Nothing
        Me.cbxMarca.DataSource = Nothing
        Me.cbxMarca.DataValueMember = Nothing
        Me.cbxMarca.dtSeleccionado = Nothing
        Me.cbxMarca.FormABM = Nothing
        Me.cbxMarca.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxMarca.Location = New System.Drawing.Point(104, 114)
        Me.cbxMarca.Name = "cbxMarca"
        Me.cbxMarca.SeleccionMultiple = False
        Me.cbxMarca.SeleccionObligatoria = False
        Me.cbxMarca.Size = New System.Drawing.Size(208, 21)
        Me.cbxMarca.SoloLectura = False
        Me.cbxMarca.TabIndex = 9
        Me.cbxMarca.Texto = ""
        '
        'lblMarca
        '
        Me.lblMarca.AutoSize = True
        Me.lblMarca.Location = New System.Drawing.Point(6, 118)
        Me.lblMarca.Name = "lblMarca"
        Me.lblMarca.Size = New System.Drawing.Size(40, 13)
        Me.lblMarca.TabIndex = 8
        Me.lblMarca.Text = "Marca:"
        '
        'cbxLinea
        '
        Me.cbxLinea.CampoWhere = Nothing
        Me.cbxLinea.CargarUnaSolaVez = False
        Me.cbxLinea.DataDisplayMember = Nothing
        Me.cbxLinea.DataFilter = Nothing
        Me.cbxLinea.DataOrderBy = Nothing
        Me.cbxLinea.DataSource = Nothing
        Me.cbxLinea.DataValueMember = Nothing
        Me.cbxLinea.dtSeleccionado = Nothing
        Me.cbxLinea.FormABM = Nothing
        Me.cbxLinea.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxLinea.Location = New System.Drawing.Point(104, 33)
        Me.cbxLinea.Name = "cbxLinea"
        Me.cbxLinea.SeleccionMultiple = False
        Me.cbxLinea.SeleccionObligatoria = False
        Me.cbxLinea.Size = New System.Drawing.Size(208, 21)
        Me.cbxLinea.SoloLectura = False
        Me.cbxLinea.TabIndex = 3
        Me.cbxLinea.Texto = ""
        '
        'lblLinea
        '
        Me.lblLinea.AutoSize = True
        Me.lblLinea.Location = New System.Drawing.Point(6, 37)
        Me.lblLinea.Name = "lblLinea"
        Me.lblLinea.Size = New System.Drawing.Size(36, 13)
        Me.lblLinea.TabIndex = 2
        Me.lblLinea.Text = "Linea:"
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = Nothing
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = Nothing
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = Nothing
        Me.cbxTipoProducto.DataSource = Nothing
        Me.cbxTipoProducto.DataValueMember = Nothing
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxTipoProducto.Location = New System.Drawing.Point(104, 6)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(208, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 1
        Me.cbxTipoProducto.Texto = ""
        '
        'lblTipoProducto
        '
        Me.lblTipoProducto.AutoSize = True
        Me.lblTipoProducto.Location = New System.Drawing.Point(6, 10)
        Me.lblTipoProducto.Name = "lblTipoProducto"
        Me.lblTipoProducto.Size = New System.Drawing.Size(92, 13)
        Me.lblTipoProducto.TabIndex = 0
        Me.lblTipoProducto.Text = "Tipo de Producto:"
        '
        'tabExistencia
        '
        Me.tabExistencia.Controls.Add(Me.TableLayoutPanel2)
        Me.tabExistencia.Location = New System.Drawing.Point(4, 22)
        Me.tabExistencia.Name = "tabExistencia"
        Me.tabExistencia.Padding = New System.Windows.Forms.Padding(3)
        Me.tabExistencia.Size = New System.Drawing.Size(819, 413)
        Me.tabExistencia.TabIndex = 1
        Me.tabExistencia.Text = "Existencia"
        Me.tabExistencia.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.dgvExistencia, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel2, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(813, 407)
        Me.TableLayoutPanel2.TabIndex = 11
        '
        'dgvExistencia
        '
        Me.dgvExistencia.AllowUserToAddRows = False
        Me.dgvExistencia.AllowUserToDeleteRows = False
        Me.dgvExistencia.AllowUserToOrderColumns = True
        Me.dgvExistencia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvExistencia.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvExistencia.Location = New System.Drawing.Point(3, 93)
        Me.dgvExistencia.MinimumSize = New System.Drawing.Size(22, 22)
        Me.dgvExistencia.Name = "dgvExistencia"
        Me.dgvExistencia.ReadOnly = True
        Me.dgvExistencia.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvExistencia.Size = New System.Drawing.Size(807, 311)
        Me.dgvExistencia.StandardTab = True
        Me.dgvExistencia.TabIndex = 10
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblSucursalDeposito)
        Me.Panel2.Controls.Add(Me.lblExistenciaMinima)
        Me.Panel2.Controls.Add(Me.txtExistenciaGeneral)
        Me.Panel2.Controls.Add(Me.lblExistenciaCritica)
        Me.Panel2.Controls.Add(Me.txtExistenciaCritica)
        Me.Panel2.Controls.Add(Me.lblExistenciaGeneral)
        Me.Panel2.Controls.Add(Me.txtExistenciaMinima)
        Me.Panel2.Controls.Add(Me.btnActualizarExistencia)
        Me.Panel2.Controls.Add(Me.cbxSucursalDeposito)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(807, 84)
        Me.Panel2.TabIndex = 11
        '
        'lblSucursalDeposito
        '
        Me.lblSucursalDeposito.AutoSize = True
        Me.lblSucursalDeposito.Location = New System.Drawing.Point(15, 9)
        Me.lblSucursalDeposito.Name = "lblSucursalDeposito"
        Me.lblSucursalDeposito.Size = New System.Drawing.Size(83, 13)
        Me.lblSucursalDeposito.TabIndex = 0
        Me.lblSucursalDeposito.Text = "Suc. - Deposito:"
        '
        'lblExistenciaMinima
        '
        Me.lblExistenciaMinima.AutoSize = True
        Me.lblExistenciaMinima.Location = New System.Drawing.Point(4, 37)
        Me.lblExistenciaMinima.Name = "lblExistenciaMinima"
        Me.lblExistenciaMinima.Size = New System.Drawing.Size(94, 13)
        Me.lblExistenciaMinima.TabIndex = 2
        Me.lblExistenciaMinima.Text = "Existencia Minima:"
        '
        'txtExistenciaGeneral
        '
        Me.txtExistenciaGeneral.Color = System.Drawing.Color.CornflowerBlue
        Me.txtExistenciaGeneral.Decimales = True
        Me.txtExistenciaGeneral.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExistenciaGeneral.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.txtExistenciaGeneral.Indicaciones = Nothing
        Me.txtExistenciaGeneral.Location = New System.Drawing.Point(421, 4)
        Me.txtExistenciaGeneral.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.txtExistenciaGeneral.Name = "txtExistenciaGeneral"
        Me.txtExistenciaGeneral.Size = New System.Drawing.Size(101, 26)
        Me.txtExistenciaGeneral.SoloLectura = True
        Me.txtExistenciaGeneral.TabIndex = 9
        Me.txtExistenciaGeneral.TabStop = False
        Me.txtExistenciaGeneral.Tag = "Existencia General de "
        Me.txtExistenciaGeneral.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtExistenciaGeneral.Texto = "0"
        '
        'lblExistenciaCritica
        '
        Me.lblExistenciaCritica.AutoSize = True
        Me.lblExistenciaCritica.Location = New System.Drawing.Point(8, 64)
        Me.lblExistenciaCritica.Name = "lblExistenciaCritica"
        Me.lblExistenciaCritica.Size = New System.Drawing.Size(90, 13)
        Me.lblExistenciaCritica.TabIndex = 4
        Me.lblExistenciaCritica.Text = "Existencia Critica:"
        '
        'txtExistenciaCritica
        '
        Me.txtExistenciaCritica.Color = System.Drawing.Color.Empty
        Me.txtExistenciaCritica.Decimales = True
        Me.txtExistenciaCritica.Indicaciones = Nothing
        Me.txtExistenciaCritica.Location = New System.Drawing.Point(104, 59)
        Me.txtExistenciaCritica.Name = "txtExistenciaCritica"
        Me.txtExistenciaCritica.Size = New System.Drawing.Size(77, 22)
        Me.txtExistenciaCritica.SoloLectura = False
        Me.txtExistenciaCritica.TabIndex = 5
        Me.txtExistenciaCritica.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtExistenciaCritica.Texto = "0"
        '
        'lblExistenciaGeneral
        '
        Me.lblExistenciaGeneral.AutoSize = True
        Me.lblExistenciaGeneral.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExistenciaGeneral.Location = New System.Drawing.Point(317, 9)
        Me.lblExistenciaGeneral.Name = "lblExistenciaGeneral"
        Me.lblExistenciaGeneral.Size = New System.Drawing.Size(98, 13)
        Me.lblExistenciaGeneral.TabIndex = 8
        Me.lblExistenciaGeneral.Text = "Existencia General:"
        '
        'txtExistenciaMinima
        '
        Me.txtExistenciaMinima.Color = System.Drawing.Color.Empty
        Me.txtExistenciaMinima.Decimales = True
        Me.txtExistenciaMinima.Indicaciones = Nothing
        Me.txtExistenciaMinima.Location = New System.Drawing.Point(104, 32)
        Me.txtExistenciaMinima.Name = "txtExistenciaMinima"
        Me.txtExistenciaMinima.Size = New System.Drawing.Size(77, 22)
        Me.txtExistenciaMinima.SoloLectura = False
        Me.txtExistenciaMinima.TabIndex = 3
        Me.txtExistenciaMinima.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtExistenciaMinima.Texto = "0"
        '
        'btnActualizarExistencia
        '
        Me.btnActualizarExistencia.Location = New System.Drawing.Point(187, 59)
        Me.btnActualizarExistencia.Name = "btnActualizarExistencia"
        Me.btnActualizarExistencia.Size = New System.Drawing.Size(92, 23)
        Me.btnActualizarExistencia.TabIndex = 6
        Me.btnActualizarExistencia.Text = "Actualizar"
        Me.btnActualizarExistencia.UseVisualStyleBackColor = True
        '
        'cbxSucursalDeposito
        '
        Me.cbxSucursalDeposito.CampoWhere = Nothing
        Me.cbxSucursalDeposito.CargarUnaSolaVez = False
        Me.cbxSucursalDeposito.DataDisplayMember = Nothing
        Me.cbxSucursalDeposito.DataFilter = Nothing
        Me.cbxSucursalDeposito.DataOrderBy = Nothing
        Me.cbxSucursalDeposito.DataSource = Nothing
        Me.cbxSucursalDeposito.DataValueMember = Nothing
        Me.cbxSucursalDeposito.dtSeleccionado = Nothing
        Me.cbxSucursalDeposito.FormABM = Nothing
        Me.cbxSucursalDeposito.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSucursalDeposito.Location = New System.Drawing.Point(104, 5)
        Me.cbxSucursalDeposito.Name = "cbxSucursalDeposito"
        Me.cbxSucursalDeposito.SeleccionMultiple = False
        Me.cbxSucursalDeposito.SeleccionObligatoria = False
        Me.cbxSucursalDeposito.Size = New System.Drawing.Size(175, 21)
        Me.cbxSucursalDeposito.SoloLectura = False
        Me.cbxSucursalDeposito.TabIndex = 1
        Me.cbxSucursalDeposito.Texto = ""
        '
        'tabLotes
        '
        Me.tabLotes.Controls.Add(Me.Button7)
        Me.tabLotes.Controls.Add(Me.Button8)
        Me.tabLotes.Controls.Add(Me.Button9)
        Me.tabLotes.Controls.Add(Me.Button10)
        Me.tabLotes.Controls.Add(Me.Button11)
        Me.tabLotes.Controls.Add(Me.ListView2)
        Me.tabLotes.Controls.Add(Me.Label28)
        Me.tabLotes.Controls.Add(Me.Label27)
        Me.tabLotes.Controls.Add(Me.OcxTXTDate1)
        Me.tabLotes.Controls.Add(Me.OcxTXTString2)
        Me.tabLotes.Location = New System.Drawing.Point(4, 22)
        Me.tabLotes.Name = "tabLotes"
        Me.tabLotes.Size = New System.Drawing.Size(819, 413)
        Me.tabLotes.TabIndex = 2
        Me.tabLotes.Text = "Lotes"
        Me.tabLotes.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(348, 59)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 23)
        Me.Button7.TabIndex = 48
        Me.Button7.Text = "&Cancelar"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(429, 59)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(75, 23)
        Me.Button8.TabIndex = 49
        Me.Button8.Text = "E&liminar"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(267, 59)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(75, 23)
        Me.Button9.TabIndex = 47
        Me.Button9.Text = "&Guardar"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(186, 59)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(75, 23)
        Me.Button10.TabIndex = 46
        Me.Button10.Text = "&Editar"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(105, 59)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(75, 23)
        Me.Button11.TabIndex = 45
        Me.Button11.Text = "&Nuevo"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'ListView2
        '
        Me.ListView2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListView2.Location = New System.Drawing.Point(105, 88)
        Me.ListView2.Name = "ListView2"
        Me.ListView2.Size = New System.Drawing.Size(0, 0)
        Me.ListView2.TabIndex = 44
        Me.ListView2.UseCompatibleStateImageBehavior = False
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(11, 37)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(68, 13)
        Me.Label28.TabIndex = 9
        Me.Label28.Text = "Vencimiento:"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(11, 10)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(31, 13)
        Me.Label27.TabIndex = 6
        Me.Label27.Text = "Lote:"
        '
        'OcxTXTDate1
        '
        Me.OcxTXTDate1.AñoFecha = 0
        Me.OcxTXTDate1.Color = System.Drawing.Color.Empty
        Me.OcxTXTDate1.Fecha = New Date(2013, 6, 5, 8, 10, 39, 15)
        Me.OcxTXTDate1.Location = New System.Drawing.Point(105, 33)
        Me.OcxTXTDate1.MesFecha = 0
        Me.OcxTXTDate1.Name = "OcxTXTDate1"
        Me.OcxTXTDate1.PermitirNulo = False
        Me.OcxTXTDate1.Size = New System.Drawing.Size(74, 20)
        Me.OcxTXTDate1.SoloLectura = False
        Me.OcxTXTDate1.TabIndex = 8
        '
        'OcxTXTString2
        '
        Me.OcxTXTString2.BackColor = System.Drawing.Color.White
        Me.OcxTXTString2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTString2.Color = System.Drawing.Color.Empty
        Me.OcxTXTString2.Indicaciones = "(F1) Para busqueda avanzada"
        Me.OcxTXTString2.Location = New System.Drawing.Point(104, 6)
        Me.OcxTXTString2.Multilinea = False
        Me.OcxTXTString2.Name = "OcxTXTString2"
        Me.OcxTXTString2.Size = New System.Drawing.Size(126, 21)
        Me.OcxTXTString2.SoloLectura = False
        Me.OcxTXTString2.TabIndex = 7
        Me.OcxTXTString2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.OcxTXTString2.Texto = ""
        '
        'tabUbicacion
        '
        Me.tabUbicacion.Controls.Add(Me.OcxProductoUbicacion1)
        Me.tabUbicacion.Location = New System.Drawing.Point(4, 22)
        Me.tabUbicacion.Name = "tabUbicacion"
        Me.tabUbicacion.Padding = New System.Windows.Forms.Padding(3)
        Me.tabUbicacion.Size = New System.Drawing.Size(819, 413)
        Me.tabUbicacion.TabIndex = 4
        Me.tabUbicacion.Text = "Ubicacion"
        Me.tabUbicacion.UseVisualStyleBackColor = True
        '
        'OcxProductoUbicacion1
        '
        Me.OcxProductoUbicacion1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxProductoUbicacion1.IDProducto = 0
        Me.OcxProductoUbicacion1.Location = New System.Drawing.Point(3, 3)
        Me.OcxProductoUbicacion1.Name = "OcxProductoUbicacion1"
        Me.OcxProductoUbicacion1.Size = New System.Drawing.Size(813, 407)
        Me.OcxProductoUbicacion1.TabIndex = 0
        '
        'tabComision
        '
        Me.tabComision.Controls.Add(Me.OcxComisionProducto1)
        Me.tabComision.Location = New System.Drawing.Point(4, 22)
        Me.tabComision.Name = "tabComision"
        Me.tabComision.Padding = New System.Windows.Forms.Padding(3)
        Me.tabComision.Size = New System.Drawing.Size(819, 413)
        Me.tabComision.TabIndex = 5
        Me.tabComision.Text = "Comision Producto"
        Me.tabComision.UseVisualStyleBackColor = True
        '
        'OcxComisionProducto1
        '
        Me.OcxComisionProducto1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxComisionProducto1.IDProducto = 0
        Me.OcxComisionProducto1.Location = New System.Drawing.Point(3, 3)
        Me.OcxComisionProducto1.Name = "OcxComisionProducto1"
        Me.OcxComisionProducto1.Size = New System.Drawing.Size(813, 407)
        Me.OcxComisionProducto1.TabIndex = 0
        '
        'tabPrecios
        '
        Me.tabPrecios.Controls.Add(Me.OcxListaPrecioProducto1)
        Me.tabPrecios.Location = New System.Drawing.Point(4, 22)
        Me.tabPrecios.Name = "tabPrecios"
        Me.tabPrecios.Size = New System.Drawing.Size(819, 413)
        Me.tabPrecios.TabIndex = 7
        Me.tabPrecios.Text = "Precios"
        Me.tabPrecios.UseVisualStyleBackColor = True
        '
        'OcxListaPrecioProducto1
        '
        Me.OcxListaPrecioProducto1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxListaPrecioProducto1.IDProducto = 0
        Me.OcxListaPrecioProducto1.Location = New System.Drawing.Point(0, 0)
        Me.OcxListaPrecioProducto1.Name = "OcxListaPrecioProducto1"
        Me.OcxListaPrecioProducto1.Referencia = Nothing
        Me.OcxListaPrecioProducto1.Size = New System.Drawing.Size(819, 413)
        Me.OcxListaPrecioProducto1.TabIndex = 0
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 485)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(833, 22)
        Me.StatusStrip1.TabIndex = 0
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TabControl1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(833, 485)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txtID)
        Me.Panel1.Controls.Add(Me.lblDescripcion)
        Me.Panel1.Controls.Add(Me.txtDescripcion)
        Me.Panel1.Controls.Add(Me.lblCodigoBarra)
        Me.Panel1.Controls.Add(Me.txtCodigoBarra)
        Me.Panel1.Controls.Add(Me.lblReferencia)
        Me.Panel1.Controls.Add(Me.txtReferencia)
        Me.Panel1.Controls.Add(Me.btnBuscar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(827, 34)
        Me.Panel1.TabIndex = 0
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(686, 7)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(58, 21)
        Me.txtID.SoloLectura = True
        Me.txtID.TabIndex = 7
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BackColor = System.Drawing.Color.White
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = "(F1) Para busqueda avanzada"
        Me.txtDescripcion.Location = New System.Drawing.Point(215, 7)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(223, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'txtCodigoBarra
        '
        Me.txtCodigoBarra.BackColor = System.Drawing.Color.White
        Me.txtCodigoBarra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoBarra.Color = System.Drawing.Color.Empty
        Me.txtCodigoBarra.Indicaciones = Nothing
        Me.txtCodigoBarra.Location = New System.Drawing.Point(532, 7)
        Me.txtCodigoBarra.Multilinea = False
        Me.txtCodigoBarra.Name = "txtCodigoBarra"
        Me.txtCodigoBarra.Size = New System.Drawing.Size(104, 21)
        Me.txtCodigoBarra.SoloLectura = False
        Me.txtCodigoBarra.TabIndex = 5
        Me.txtCodigoBarra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtCodigoBarra.Texto = ""
        '
        'txtReferencia
        '
        Me.txtReferencia.BackColor = System.Drawing.Color.White
        Me.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferencia.Color = System.Drawing.Color.Empty
        Me.txtReferencia.Indicaciones = Nothing
        Me.txtReferencia.Location = New System.Drawing.Point(72, 7)
        Me.txtReferencia.Multilinea = False
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Size = New System.Drawing.Size(66, 21)
        Me.txtReferencia.SoloLectura = False
        Me.txtReferencia.TabIndex = 1
        Me.txtReferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferencia.Texto = ""
        '
        'frmProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(833, 507)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.KeyPreview = True
        Me.Name = "frmProducto"
        Me.Tag = "PRODUCTO"
        Me.Text = "frmProducto"
        Me.TabControl1.ResumeLayout(False)
        Me.tabIdentificacion.ResumeLayout(False)
        Me.tabIdentificacion.PerformLayout()
        Me.tabExistencia.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.dgvExistencia, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.tabLotes.ResumeLayout(False)
        Me.tabLotes.PerformLayout()
        Me.tabUbicacion.ResumeLayout(False)
        Me.tabComision.ResumeLayout(False)
        Me.tabPrecios.ResumeLayout(False)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtCodigoBarra As ERP.ocxTXTString
    Friend WithEvents lblCodigoBarra As System.Windows.Forms.Label
    Friend WithEvents txtReferencia As ERP.ocxTXTString
    Friend WithEvents lblReferencia As System.Windows.Forms.Label
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tabIdentificacion As System.Windows.Forms.TabPage
    Friend WithEvents tabExistencia As System.Windows.Forms.TabPage
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents tabLotes As System.Windows.Forms.TabPage
    Friend WithEvents cbxCategoria As ERP.ocxCBX
    Friend WithEvents lblCategoria As System.Windows.Forms.Label
    Friend WithEvents cbxDivision As ERP.ocxCBX
    Friend WithEvents lblDivision As System.Windows.Forms.Label
    Friend WithEvents cbxProveedor As ERP.ocxCBX
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents cbxPresentacion As ERP.ocxCBX
    Friend WithEvents lblPresentacion As System.Windows.Forms.Label
    Friend WithEvents cbxMarca As ERP.ocxCBX
    Friend WithEvents lblMarca As System.Windows.Forms.Label
    Friend WithEvents cbxLinea As ERP.ocxCBX
    Friend WithEvents lblLinea As System.Windows.Forms.Label
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents lblTipoProducto As System.Windows.Forms.Label
    Friend WithEvents lblImpuesto As System.Windows.Forms.Label
    Friend WithEvents lblVolumen As System.Windows.Forms.Label
    Friend WithEvents lblPeso As System.Windows.Forms.Label
    Friend WithEvents txtUnidadPorCaja As ERP.ocxTXTNumeric
    Friend WithEvents lblUnidadPorCaja As System.Windows.Forms.Label
    Friend WithEvents cbxUnidadMedida As ERP.ocxCBX
    Friend WithEvents lblUnidadMedida As System.Windows.Forms.Label
    Friend WithEvents txtCantidadUltimaSalida As System.Windows.Forms.TextBox
    Friend WithEvents lblUltimaSalida As System.Windows.Forms.Label
    Friend WithEvents txtFechaUltimaSalida As System.Windows.Forms.TextBox
    Friend WithEvents txtCantidadUltimaEntrada As System.Windows.Forms.TextBox
    Friend WithEvents lblUltimaEntrada As System.Windows.Forms.Label
    Friend WithEvents txtFechaUltimaEntrada As System.Windows.Forms.TextBox
    Friend WithEvents lblUltimoCostoSinIVA As System.Windows.Forms.Label
    Friend WithEvents txtUltimoCostoSinIVA As System.Windows.Forms.TextBox
    Friend WithEvents lblUltimoCosto As System.Windows.Forms.Label
    Friend WithEvents txtUltimoCosto As System.Windows.Forms.TextBox
    Friend WithEvents lblCotizacion As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As System.Windows.Forms.TextBox
    Friend WithEvents txtExistenciaMinima As ERP.ocxTXTNumeric
    Friend WithEvents lblExistenciaMinima As System.Windows.Forms.Label
    Friend WithEvents cbxSucursalDeposito As ERP.ocxCBX
    Friend WithEvents lblSucursalDeposito As System.Windows.Forms.Label
    Friend WithEvents btnActualizarExistencia As System.Windows.Forms.Button
    Friend WithEvents txtExistenciaGeneral As ERP.ocxTXTNumeric
    Friend WithEvents lblExistenciaGeneral As System.Windows.Forms.Label
    Friend WithEvents txtExistenciaCritica As ERP.ocxTXTNumeric
    Friend WithEvents lblExistenciaCritica As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents OcxTXTDate1 As ERP.ocxTXTDate
    Friend WithEvents OcxTXTString2 As ERP.ocxTXTString
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents ListView2 As System.Windows.Forms.ListView
    Friend WithEvents cbxProcedencia As ERP.ocxCBX
    Friend WithEvents lblProcedencia As System.Windows.Forms.Label
    Friend WithEvents cbxImpuesto As ERP.ocxCBX
    Friend WithEvents lblCostoPromedio As System.Windows.Forms.Label
    Friend WithEvents txtCostoPromedio As System.Windows.Forms.TextBox
    Friend WithEvents lblCostoCG As System.Windows.Forms.Label
    Friend WithEvents txtCostoCG As System.Windows.Forms.TextBox
    Friend WithEvents lblControlarExistencia As System.Windows.Forms.Label
    Friend WithEvents lblActivo As System.Windows.Forms.Label
    Friend WithEvents txtVolumen As ERP.ocxTXTString
    Friend WithEvents ocxCuentaCompra As ERP.ocxTXTCuentaContable
    Friend WithEvents lblCuentaCompra As System.Windows.Forms.Label
    Friend WithEvents ocxCuentaVenta As ERP.ocxTXTCuentaContable
    Friend WithEvents lblCuentaVenta As System.Windows.Forms.Label
    Friend WithEvents cbxSubLinea2 As ERP.ocxCBX
    Friend WithEvents lblSubLinea2 As System.Windows.Forms.Label
    Friend WithEvents cbxSubLinea As ERP.ocxCBX
    Friend WithEvents lblSubLinea As System.Windows.Forms.Label
    Friend WithEvents tabUbicacion As System.Windows.Forms.TabPage
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkActivo As ERP.ocxCHK
    Friend WithEvents chkControlarExistencia As ERP.ocxCHK
    Friend WithEvents tabComision As System.Windows.Forms.TabPage
    Friend WithEvents OcxComisionProducto1 As ERP.ocxComisionProducto
    Friend WithEvents tabPrecios As System.Windows.Forms.TabPage
    Friend WithEvents OcxListaPrecioProducto1 As ERP.ocxListaPrecioProducto
    Friend WithEvents txtPeso As ERP.ocxTXTString
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbxUnidadMedidaConversion As ERP.ocxCBX
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtUnidadConversion As ERP.ocxTXTString
    Friend WithEvents ocxCuentaCosto As ERP.ocxTXTCuentaContable
    Friend WithEvents lblCuentaContableCosto As System.Windows.Forms.Label
    Friend WithEvents dgvExistencia As System.Windows.Forms.DataGridView
    Friend WithEvents lblPorcentaje As System.Windows.Forms.Label
    Friend WithEvents txtMargenMinimo As ERP.ocxTXTNumeric
    Friend WithEvents lblMargen As System.Windows.Forms.Label
    Friend WithEvents ocxCuentaDeudor As ERP.ocxTXTCuentaContable
    Friend WithEvents lblCuentaContableDeudor As System.Windows.Forms.Label
    Friend WithEvents chkVendible As ERP.ocxCHK
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkMateriaPrima As ERP.ocxCHK
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents chkMovimientoCombustible As ERP.ocxCHK
    Friend WithEvents chkDescargaStock As ERP.ocxCHK
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents OcxProductoUbicacion1 As ERP.ocxProductoUbicacion
    Friend WithEvents chkDescargaCompra As ERP.ocxCHK
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtUsuarioModificacion As System.Windows.Forms.TextBox
    Friend WithEvents lblModificacion As System.Windows.Forms.Label
    Friend WithEvents txtUsuarioAlta As System.Windows.Forms.TextBox
    Friend WithEvents lblAlta As System.Windows.Forms.Label
    Friend WithEvents txtFechaModificacion As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtFechaAlta As System.Windows.Forms.MaskedTextBox
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents lblUnisal As Label
    Friend WithEvents ChkDistribucion As ocxCHK
    Friend WithEvents chkInformatica As ocxCHK
    Friend WithEvents Label9 As Label
End Class

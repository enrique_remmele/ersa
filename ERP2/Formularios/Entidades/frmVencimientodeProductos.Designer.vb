﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVencimientodeProductos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cbxFamilia = New ERP.ocxCBX()
        Me.lblFamilia = New System.Windows.Forms.Label()
        Me.cbxPresentacion = New ERP.ocxCBX()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lblPresentacion = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.lblID = New System.Windows.Forms.Label()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.txtVencimiento = New System.Windows.Forms.NumericUpDown()
        Me.lblVencimiento = New System.Windows.Forms.Label()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.txtVencimiento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cbxFamilia
        '
        Me.cbxFamilia.CampoWhere = Nothing
        Me.cbxFamilia.CargarUnaSolaVez = False
        Me.cbxFamilia.DataDisplayMember = Nothing
        Me.cbxFamilia.DataFilter = Nothing
        Me.cbxFamilia.DataOrderBy = Nothing
        Me.cbxFamilia.DataSource = Nothing
        Me.cbxFamilia.DataValueMember = Nothing
        Me.cbxFamilia.dtSeleccionado = Nothing
        Me.cbxFamilia.FormABM = Nothing
        Me.cbxFamilia.Indicaciones = Nothing
        Me.cbxFamilia.Location = New System.Drawing.Point(106, 42)
        Me.cbxFamilia.Name = "cbxFamilia"
        Me.cbxFamilia.SeleccionMultiple = False
        Me.cbxFamilia.SeleccionObligatoria = False
        Me.cbxFamilia.Size = New System.Drawing.Size(237, 21)
        Me.cbxFamilia.SoloLectura = False
        Me.cbxFamilia.TabIndex = 25
        Me.cbxFamilia.Texto = ""
        '
        'lblFamilia
        '
        Me.lblFamilia.AutoSize = True
        Me.lblFamilia.Location = New System.Drawing.Point(20, 45)
        Me.lblFamilia.Name = "lblFamilia"
        Me.lblFamilia.Size = New System.Drawing.Size(42, 13)
        Me.lblFamilia.TabIndex = 24
        Me.lblFamilia.Text = "Familia:"
        '
        'cbxPresentacion
        '
        Me.cbxPresentacion.CampoWhere = Nothing
        Me.cbxPresentacion.CargarUnaSolaVez = False
        Me.cbxPresentacion.DataDisplayMember = Nothing
        Me.cbxPresentacion.DataFilter = Nothing
        Me.cbxPresentacion.DataOrderBy = Nothing
        Me.cbxPresentacion.DataSource = Nothing
        Me.cbxPresentacion.DataValueMember = Nothing
        Me.cbxPresentacion.dtSeleccionado = Nothing
        Me.cbxPresentacion.FormABM = Nothing
        Me.cbxPresentacion.Indicaciones = Nothing
        Me.cbxPresentacion.Location = New System.Drawing.Point(106, 69)
        Me.cbxPresentacion.Name = "cbxPresentacion"
        Me.cbxPresentacion.SeleccionMultiple = False
        Me.cbxPresentacion.SeleccionObligatoria = False
        Me.cbxPresentacion.Size = New System.Drawing.Size(237, 21)
        Me.cbxPresentacion.SoloLectura = False
        Me.cbxPresentacion.TabIndex = 27
        Me.cbxPresentacion.Texto = ""
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'lblPresentacion
        '
        Me.lblPresentacion.AutoSize = True
        Me.lblPresentacion.Location = New System.Drawing.Point(20, 72)
        Me.lblPresentacion.Name = "lblPresentacion"
        Me.lblPresentacion.Size = New System.Drawing.Size(72, 13)
        Me.lblPresentacion.TabIndex = 26
        Me.lblPresentacion.Text = "Presentacion:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(106, 13)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 21
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 356)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(531, 22)
        Me.StatusStrip1.TabIndex = 39
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(430, 321)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 38
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(349, 127)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 35
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(268, 127)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 34
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(187, 127)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 33
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(106, 127)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 32
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lvLista
        '
        Me.lvLista.Location = New System.Drawing.Point(106, 156)
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(399, 159)
        Me.lvLista.TabIndex = 37
        Me.lvLista.UseCompatibleStateImageBehavior = False
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(20, 18)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 20
        Me.lblID.Text = "ID:"
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(430, 127)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 36
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'txtVencimiento
        '
        Me.txtVencimiento.Location = New System.Drawing.Point(106, 99)
        Me.txtVencimiento.Maximum = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me.txtVencimiento.Name = "txtVencimiento"
        Me.txtVencimiento.Size = New System.Drawing.Size(40, 20)
        Me.txtVencimiento.TabIndex = 40
        '
        'lblVencimiento
        '
        Me.lblVencimiento.AutoSize = True
        Me.lblVencimiento.Location = New System.Drawing.Point(20, 101)
        Me.lblVencimiento.Name = "lblVencimiento"
        Me.lblVencimiento.Size = New System.Drawing.Size(68, 13)
        Me.lblVencimiento.TabIndex = 41
        Me.lblVencimiento.Text = "Vencimiento:"
        '
        'frmVencimientodeProductos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(531, 378)
        Me.Controls.Add(Me.lblVencimiento)
        Me.Controls.Add(Me.txtVencimiento)
        Me.Controls.Add(Me.cbxFamilia)
        Me.Controls.Add(Me.lblFamilia)
        Me.Controls.Add(Me.cbxPresentacion)
        Me.Controls.Add(Me.lblPresentacion)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.lvLista)
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.btnEliminar)
        Me.Name = "frmVencimientodeProductos"
        Me.Tag = "frmVencimientodeProductos"
        Me.Text = "frmVencimientodeProductos"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.txtVencimiento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbxFamilia As ocxCBX
    Friend WithEvents lblFamilia As Label
    Friend WithEvents cbxPresentacion As ocxCBX
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents lblPresentacion As Label
    Friend WithEvents txtID As ocxTXTNumeric
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents btnSalir As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnEditar As Button
    Friend WithEvents btnNuevo As Button
    Friend WithEvents lvLista As ListView
    Friend WithEvents lblID As Label
    Friend WithEvents btnEliminar As Button
    Friend WithEvents lblVencimiento As Label
    Friend WithEvents txtVencimiento As NumericUpDown
End Class

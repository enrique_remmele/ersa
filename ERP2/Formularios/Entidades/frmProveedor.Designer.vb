﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProveedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.lblID = New System.Windows.Forms.Label()
        Me.tabDatosAdicionales = New System.Windows.Forms.TabPage()
        Me.txtFax = New ERP.ocxTXTString()
        Me.OcxTXTString8 = New ERP.ocxTXTString()
        Me.OcxTXTString7 = New ERP.ocxTXTString()
        Me.OcxTXTString6 = New ERP.ocxTXTString()
        Me.OcxTXTNumeric7 = New ERP.ocxTXTNumeric()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.lvPaisLista = New System.Windows.Forms.ListView()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.lblFax = New System.Windows.Forms.Label()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.txtPaginaWeb = New System.Windows.Forms.TextBox()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.lblPaginaWeb = New System.Windows.Forms.Label()
        Me.tabDivisiones = New System.Windows.Forms.TabPage()
        Me.OcxTXTNumeric1 = New ERP.ocxTXTNumeric()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.OcxTXTString10 = New ERP.ocxTXTString()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblRUC = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtFechaAlta = New System.Windows.Forms.TextBox()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.tabLocalizacion = New System.Windows.Forms.TabPage()
        Me.OcxProveedoresMapa1 = New ERP.ocxProveedoresMapa()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.tcGeneral = New System.Windows.Forms.TabControl()
        Me.tabDatosComerciales = New System.Windows.Forms.TabPage()
        Me.chkDelExterior = New System.Windows.Forms.CheckBox()
        Me.chkAcopiador = New System.Windows.Forms.CheckBox()
        Me.chkSujetoRetencion = New System.Windows.Forms.CheckBox()
        Me.chkExportado = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxTipoCompra = New ERP.ocxCBX()
        Me.chkRetentor = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPlazo = New ERP.ocxTXTNumeric()
        Me.chkCredito = New System.Windows.Forms.CheckBox()
        Me.ocxCuentaCompra = New ERP.ocxTXTCuentaContable()
        Me.ocxCuentaVenta = New ERP.ocxTXTCuentaContable()
        Me.lblCuentaVenta = New System.Windows.Forms.Label()
        Me.lblCuentaCompra = New System.Windows.Forms.Label()
        Me.chkActivado = New System.Windows.Forms.CheckBox()
        Me.cbxTipoProveedor = New ERP.ocxCBX()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.cbxBarrio = New ERP.ocxCBX()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.cbxDepartamento = New ERP.ocxCBX()
        Me.cbxPais = New ERP.ocxCBX()
        Me.txtTelefonos = New ERP.ocxTXTString()
        Me.txtDireccion = New ERP.ocxTXTString()
        Me.txtNombreFantasia = New ERP.ocxTXTString()
        Me.txtUsuarioModificacion = New System.Windows.Forms.TextBox()
        Me.txtFechaModificacion = New System.Windows.Forms.TextBox()
        Me.lblModificacion = New System.Windows.Forms.Label()
        Me.txtFechaUltimaCompra = New System.Windows.Forms.TextBox()
        Me.lblUltimaCompra = New System.Windows.Forms.Label()
        Me.txtUsuarioAlta = New System.Windows.Forms.TextBox()
        Me.lblAlta = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblTipoProveedor = New System.Windows.Forms.Label()
        Me.lblBarrio = New System.Windows.Forms.Label()
        Me.lblCiudad = New System.Windows.Forms.Label()
        Me.lblDepartamento = New System.Windows.Forms.Label()
        Me.lblPais = New System.Windows.Forms.Label()
        Me.lblTelefonos = New System.Windows.Forms.Label()
        Me.lblDireccion = New System.Windows.Forms.Label()
        Me.lblNombreFantasia = New System.Windows.Forms.Label()
        Me.lblRazonSocial = New System.Windows.Forms.Label()
        Me.lblReferencia = New System.Windows.Forms.Label()
        Me.txtReferencia = New ERP.ocxTXTString()
        Me.txtRazonSocial = New ERP.ocxTXTString()
        Me.txtRUC = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblCodigoProveedor = New System.Windows.Forms.Label()
        Me.txtCodigoProveedor = New System.Windows.Forms.TextBox()
        Me.tabDatosAdicionales.SuspendLayout()
        Me.tabDivisiones.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabLocalizacion.SuspendLayout()
        Me.tcGeneral.SuspendLayout()
        Me.tabDatosComerciales.SuspendLayout()
        Me.SuspendLayout()
        '
        'ListView1
        '
        Me.ListView1.Location = New System.Drawing.Point(81, 14)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(399, 168)
        Me.ListView1.TabIndex = 0
        Me.ListView1.UseCompatibleStateImageBehavior = False
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(324, 188)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 23)
        Me.Button7.TabIndex = 4
        Me.Button7.Text = "&Cancelar"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(405, 188)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(75, 23)
        Me.Button8.TabIndex = 5
        Me.Button8.Text = "E&liminar"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(243, 188)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(75, 23)
        Me.Button9.TabIndex = 3
        Me.Button9.Text = "&Guardar"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(162, 188)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(75, 23)
        Me.Button10.TabIndex = 2
        Me.Button10.Text = "&Editar"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(81, 188)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(75, 23)
        Me.Button11.TabIndex = 1
        Me.Button11.Text = "&Nuevo"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(560, 11)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(43, 13)
        Me.lblID.TabIndex = 6
        Me.lblID.Text = "Codigo:"
        '
        'tabDatosAdicionales
        '
        Me.tabDatosAdicionales.Controls.Add(Me.txtFax)
        Me.tabDatosAdicionales.Controls.Add(Me.OcxTXTString8)
        Me.tabDatosAdicionales.Controls.Add(Me.OcxTXTString7)
        Me.tabDatosAdicionales.Controls.Add(Me.OcxTXTString6)
        Me.tabDatosAdicionales.Controls.Add(Me.OcxTXTNumeric7)
        Me.tabDatosAdicionales.Controls.Add(Me.Label38)
        Me.tabDatosAdicionales.Controls.Add(Me.lvPaisLista)
        Me.tabDatosAdicionales.Controls.Add(Me.Button2)
        Me.tabDatosAdicionales.Controls.Add(Me.Button3)
        Me.tabDatosAdicionales.Controls.Add(Me.Button4)
        Me.tabDatosAdicionales.Controls.Add(Me.Button5)
        Me.tabDatosAdicionales.Controls.Add(Me.Button6)
        Me.tabDatosAdicionales.Controls.Add(Me.Label37)
        Me.tabDatosAdicionales.Controls.Add(Me.Label36)
        Me.tabDatosAdicionales.Controls.Add(Me.TextBox5)
        Me.tabDatosAdicionales.Controls.Add(Me.Label35)
        Me.tabDatosAdicionales.Controls.Add(Me.Label34)
        Me.tabDatosAdicionales.Controls.Add(Me.Label33)
        Me.tabDatosAdicionales.Controls.Add(Me.lblFax)
        Me.tabDatosAdicionales.Controls.Add(Me.txtEmail)
        Me.tabDatosAdicionales.Controls.Add(Me.txtPaginaWeb)
        Me.tabDatosAdicionales.Controls.Add(Me.lblEmail)
        Me.tabDatosAdicionales.Controls.Add(Me.lblPaginaWeb)
        Me.tabDatosAdicionales.Location = New System.Drawing.Point(4, 22)
        Me.tabDatosAdicionales.Name = "tabDatosAdicionales"
        Me.tabDatosAdicionales.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDatosAdicionales.Size = New System.Drawing.Size(692, 300)
        Me.tabDatosAdicionales.TabIndex = 1
        Me.tabDatosAdicionales.Text = "Datos Adicionales"
        Me.tabDatosAdicionales.UseVisualStyleBackColor = True
        '
        'txtFax
        '
        Me.txtFax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFax.Color = System.Drawing.Color.Empty
        Me.txtFax.Indicaciones = Nothing
        Me.txtFax.Location = New System.Drawing.Point(573, 14)
        Me.txtFax.Multilinea = False
        Me.txtFax.Name = "txtFax"
        Me.txtFax.Size = New System.Drawing.Size(104, 21)
        Me.txtFax.SoloLectura = False
        Me.txtFax.TabIndex = 5
        Me.txtFax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtFax.Texto = ""
        '
        'OcxTXTString8
        '
        Me.OcxTXTString8.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTString8.Color = System.Drawing.Color.Empty
        Me.OcxTXTString8.Indicaciones = Nothing
        Me.OcxTXTString8.Location = New System.Drawing.Point(312, 238)
        Me.OcxTXTString8.Multilinea = False
        Me.OcxTXTString8.Name = "OcxTXTString8"
        Me.OcxTXTString8.Size = New System.Drawing.Size(125, 21)
        Me.OcxTXTString8.SoloLectura = False
        Me.OcxTXTString8.TabIndex = 20
        Me.OcxTXTString8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.OcxTXTString8.Texto = ""
        '
        'OcxTXTString7
        '
        Me.OcxTXTString7.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTString7.Color = System.Drawing.Color.Empty
        Me.OcxTXTString7.Indicaciones = Nothing
        Me.OcxTXTString7.Location = New System.Drawing.Point(81, 238)
        Me.OcxTXTString7.Multilinea = False
        Me.OcxTXTString7.Name = "OcxTXTString7"
        Me.OcxTXTString7.Size = New System.Drawing.Size(171, 21)
        Me.OcxTXTString7.SoloLectura = False
        Me.OcxTXTString7.TabIndex = 16
        Me.OcxTXTString7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.OcxTXTString7.Texto = ""
        '
        'OcxTXTString6
        '
        Me.OcxTXTString6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTString6.Color = System.Drawing.Color.Empty
        Me.OcxTXTString6.Indicaciones = Nothing
        Me.OcxTXTString6.Location = New System.Drawing.Point(81, 217)
        Me.OcxTXTString6.Multilinea = False
        Me.OcxTXTString6.Name = "OcxTXTString6"
        Me.OcxTXTString6.Size = New System.Drawing.Size(171, 21)
        Me.OcxTXTString6.SoloLectura = False
        Me.OcxTXTString6.TabIndex = 14
        Me.OcxTXTString6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.OcxTXTString6.Texto = ""
        '
        'OcxTXTNumeric7
        '
        Me.OcxTXTNumeric7.Color = System.Drawing.Color.Empty
        Me.OcxTXTNumeric7.Decimales = False
        Me.OcxTXTNumeric7.Indicaciones = Nothing
        Me.OcxTXTNumeric7.Location = New System.Drawing.Point(486, 217)
        Me.OcxTXTNumeric7.Name = "OcxTXTNumeric7"
        Me.OcxTXTNumeric7.Size = New System.Drawing.Size(44, 21)
        Me.OcxTXTNumeric7.SoloLectura = False
        Me.OcxTXTNumeric7.TabIndex = 22
        Me.OcxTXTNumeric7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.OcxTXTNumeric7.Texto = "0"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(446, 221)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(43, 13)
        Me.Label38.TabIndex = 21
        Me.Label38.Text = "Codigo:"
        '
        'lvPaisLista
        '
        Me.lvPaisLista.Location = New System.Drawing.Point(80, 61)
        Me.lvPaisLista.Name = "lvPaisLista"
        Me.lvPaisLista.Size = New System.Drawing.Size(450, 120)
        Me.lvPaisLista.TabIndex = 7
        Me.lvPaisLista.UseCompatibleStateImageBehavior = False
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(324, 187)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 11
        Me.Button2.Text = "&Cancelar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(405, 187)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 12
        Me.Button3.Text = "E&liminar"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(243, 187)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 10
        Me.Button4.Text = "&Guardar"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(162, 187)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 23)
        Me.Button5.TabIndex = 9
        Me.Button5.Text = "&Editar"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(81, 187)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(75, 23)
        Me.Button6.TabIndex = 8
        Me.Button6.Text = "&Nuevo"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(254, 242)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(52, 13)
        Me.Label37.TabIndex = 19
        Me.Label37.Text = "Telefono:"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(6, 242)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(38, 13)
        Me.Label36.TabIndex = 15
        Me.Label36.Text = "Cargo:"
        '
        'TextBox5
        '
        Me.TextBox5.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.TextBox5.Location = New System.Drawing.Point(312, 217)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(125, 20)
        Me.TextBox5.TabIndex = 18
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(271, 221)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(35, 13)
        Me.Label35.TabIndex = 17
        Me.Label35.Text = "Email:"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(6, 221)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(52, 13)
        Me.Label34.TabIndex = 13
        Me.Label34.Text = "Nombres:"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(6, 61)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(68, 13)
        Me.Label33.TabIndex = 6
        Me.Label33.Text = "Contactos:"
        '
        'lblFax
        '
        Me.lblFax.AutoSize = True
        Me.lblFax.Location = New System.Drawing.Point(540, 18)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(27, 13)
        Me.lblFax.TabIndex = 4
        Me.lblFax.Text = "Fax:"
        '
        'txtEmail
        '
        Me.txtEmail.BackColor = System.Drawing.Color.White
        Me.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtEmail.Location = New System.Drawing.Point(350, 14)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(180, 20)
        Me.txtEmail.TabIndex = 3
        '
        'txtPaginaWeb
        '
        Me.txtPaginaWeb.BackColor = System.Drawing.Color.White
        Me.txtPaginaWeb.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtPaginaWeb.Location = New System.Drawing.Point(81, 14)
        Me.txtPaginaWeb.Name = "txtPaginaWeb"
        Me.txtPaginaWeb.Size = New System.Drawing.Size(222, 20)
        Me.txtPaginaWeb.TabIndex = 1
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Location = New System.Drawing.Point(309, 18)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(35, 13)
        Me.lblEmail.TabIndex = 2
        Me.lblEmail.Text = "Email:"
        '
        'lblPaginaWeb
        '
        Me.lblPaginaWeb.AutoSize = True
        Me.lblPaginaWeb.Location = New System.Drawing.Point(6, 18)
        Me.lblPaginaWeb.Name = "lblPaginaWeb"
        Me.lblPaginaWeb.Size = New System.Drawing.Size(69, 13)
        Me.lblPaginaWeb.TabIndex = 0
        Me.lblPaginaWeb.Text = "Pagina Web:"
        '
        'tabDivisiones
        '
        Me.tabDivisiones.Controls.Add(Me.OcxTXTNumeric1)
        Me.tabDivisiones.Controls.Add(Me.Label3)
        Me.tabDivisiones.Controls.Add(Me.CheckBox1)
        Me.tabDivisiones.Controls.Add(Me.ListView1)
        Me.tabDivisiones.Controls.Add(Me.Button7)
        Me.tabDivisiones.Controls.Add(Me.Button8)
        Me.tabDivisiones.Controls.Add(Me.Button9)
        Me.tabDivisiones.Controls.Add(Me.Button10)
        Me.tabDivisiones.Controls.Add(Me.Button11)
        Me.tabDivisiones.Controls.Add(Me.Label39)
        Me.tabDivisiones.Controls.Add(Me.OcxTXTString10)
        Me.tabDivisiones.Location = New System.Drawing.Point(4, 22)
        Me.tabDivisiones.Name = "tabDivisiones"
        Me.tabDivisiones.Size = New System.Drawing.Size(692, 300)
        Me.tabDivisiones.TabIndex = 2
        Me.tabDivisiones.Text = "Divisiones"
        Me.tabDivisiones.UseVisualStyleBackColor = True
        '
        'OcxTXTNumeric1
        '
        Me.OcxTXTNumeric1.Color = System.Drawing.Color.Empty
        Me.OcxTXTNumeric1.Decimales = False
        Me.OcxTXTNumeric1.Indicaciones = Nothing
        Me.OcxTXTNumeric1.Location = New System.Drawing.Point(421, 217)
        Me.OcxTXTNumeric1.Name = "OcxTXTNumeric1"
        Me.OcxTXTNumeric1.Size = New System.Drawing.Size(58, 21)
        Me.OcxTXTNumeric1.SoloLectura = False
        Me.OcxTXTNumeric1.TabIndex = 49
        Me.OcxTXTNumeric1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.OcxTXTNumeric1.Texto = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(381, 221)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 48
        Me.Label3.Text = "Codigo:"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBox1.Location = New System.Drawing.Point(23, 239)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(71, 17)
        Me.CheckBox1.TabIndex = 47
        Me.CheckBox1.Text = "Activado:"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(28, 221)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(47, 13)
        Me.Label39.TabIndex = 6
        Me.Label39.Text = "Division:"
        '
        'OcxTXTString10
        '
        Me.OcxTXTString10.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTString10.Color = System.Drawing.Color.Empty
        Me.OcxTXTString10.Indicaciones = Nothing
        Me.OcxTXTString10.Location = New System.Drawing.Point(81, 217)
        Me.OcxTXTString10.Multilinea = False
        Me.OcxTXTString10.Name = "OcxTXTString10"
        Me.OcxTXTString10.Size = New System.Drawing.Size(294, 21)
        Me.OcxTXTString10.SoloLectura = False
        Me.OcxTXTString10.TabIndex = 7
        Me.OcxTXTString10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.OcxTXTString10.Texto = ""
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 410)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(727, 22)
        Me.StatusStrip1.TabIndex = 14
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'lblRUC
        '
        Me.lblRUC.AutoSize = True
        Me.lblRUC.Location = New System.Drawing.Point(448, 12)
        Me.lblRUC.Name = "lblRUC"
        Me.lblRUC.Size = New System.Drawing.Size(33, 13)
        Me.lblRUC.TabIndex = 4
        Me.lblRUC.Text = "RUC:"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(650, 7)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(58, 23)
        Me.btnBuscar.TabIndex = 8
        Me.btnBuscar.Text = "&Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'txtFechaAlta
        '
        Me.txtFechaAlta.BackColor = System.Drawing.Color.White
        Me.txtFechaAlta.Location = New System.Drawing.Point(545, 83)
        Me.txtFechaAlta.Name = "txtFechaAlta"
        Me.txtFechaAlta.ReadOnly = True
        Me.txtFechaAlta.Size = New System.Drawing.Size(77, 20)
        Me.txtFechaAlta.TabIndex = 16
        Me.txtFechaAlta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ToolTip1.SetToolTip(Me.txtFechaAlta, "Fecha de Alta del Cliente")
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(586, 271)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 46
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(412, 271)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 45
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'tabLocalizacion
        '
        Me.tabLocalizacion.Controls.Add(Me.OcxProveedoresMapa1)
        Me.tabLocalizacion.Location = New System.Drawing.Point(4, 22)
        Me.tabLocalizacion.Name = "tabLocalizacion"
        Me.tabLocalizacion.Size = New System.Drawing.Size(692, 300)
        Me.tabLocalizacion.TabIndex = 3
        Me.tabLocalizacion.Text = "Localizacion"
        Me.tabLocalizacion.UseVisualStyleBackColor = True
        '
        'OcxProveedoresMapa1
        '
        Me.OcxProveedoresMapa1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxProveedoresMapa1.dt = Nothing
        Me.OcxProveedoresMapa1.ID = 0
        Me.OcxProveedoresMapa1.Iniciado = False
        Me.OcxProveedoresMapa1.Latitud = New Decimal(New Integer() {25262551, 0, 0, -2147483648})
        Me.OcxProveedoresMapa1.Location = New System.Drawing.Point(0, 0)
        Me.OcxProveedoresMapa1.Longitud = New Decimal(New Integer() {57593911, 0, 0, -2147483648})
        Me.OcxProveedoresMapa1.Name = "OcxProveedoresMapa1"
        Me.OcxProveedoresMapa1.Size = New System.Drawing.Size(692, 300)
        Me.OcxProveedoresMapa1.TabIndex = 0
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(326, 271)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 44
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(245, 271)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 43
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(165, 271)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 42
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(86, 271)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 41
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'tcGeneral
        '
        Me.tcGeneral.Controls.Add(Me.tabDatosComerciales)
        Me.tcGeneral.Controls.Add(Me.tabDatosAdicionales)
        Me.tcGeneral.Controls.Add(Me.tabDivisiones)
        Me.tcGeneral.Controls.Add(Me.tabLocalizacion)
        Me.tcGeneral.Location = New System.Drawing.Point(12, 36)
        Me.tcGeneral.Name = "tcGeneral"
        Me.tcGeneral.SelectedIndex = 0
        Me.tcGeneral.Size = New System.Drawing.Size(700, 326)
        Me.tcGeneral.TabIndex = 9
        '
        'tabDatosComerciales
        '
        Me.tabDatosComerciales.Controls.Add(Me.txtCodigoProveedor)
        Me.tabDatosComerciales.Controls.Add(Me.lblCodigoProveedor)
        Me.tabDatosComerciales.Controls.Add(Me.chkDelExterior)
        Me.tabDatosComerciales.Controls.Add(Me.chkAcopiador)
        Me.tabDatosComerciales.Controls.Add(Me.btnNuevo)
        Me.tabDatosComerciales.Controls.Add(Me.btnEditar)
        Me.tabDatosComerciales.Controls.Add(Me.btnGuardar)
        Me.tabDatosComerciales.Controls.Add(Me.btnEliminar)
        Me.tabDatosComerciales.Controls.Add(Me.btnCancelar)
        Me.tabDatosComerciales.Controls.Add(Me.btnSalir)
        Me.tabDatosComerciales.Controls.Add(Me.chkSujetoRetencion)
        Me.tabDatosComerciales.Controls.Add(Me.chkExportado)
        Me.tabDatosComerciales.Controls.Add(Me.Label2)
        Me.tabDatosComerciales.Controls.Add(Me.cbxTipoCompra)
        Me.tabDatosComerciales.Controls.Add(Me.chkRetentor)
        Me.tabDatosComerciales.Controls.Add(Me.Label1)
        Me.tabDatosComerciales.Controls.Add(Me.txtPlazo)
        Me.tabDatosComerciales.Controls.Add(Me.chkCredito)
        Me.tabDatosComerciales.Controls.Add(Me.ocxCuentaCompra)
        Me.tabDatosComerciales.Controls.Add(Me.ocxCuentaVenta)
        Me.tabDatosComerciales.Controls.Add(Me.lblCuentaVenta)
        Me.tabDatosComerciales.Controls.Add(Me.lblCuentaCompra)
        Me.tabDatosComerciales.Controls.Add(Me.chkActivado)
        Me.tabDatosComerciales.Controls.Add(Me.cbxTipoProveedor)
        Me.tabDatosComerciales.Controls.Add(Me.cbxMoneda)
        Me.tabDatosComerciales.Controls.Add(Me.cbxSucursal)
        Me.tabDatosComerciales.Controls.Add(Me.cbxBarrio)
        Me.tabDatosComerciales.Controls.Add(Me.cbxCiudad)
        Me.tabDatosComerciales.Controls.Add(Me.cbxDepartamento)
        Me.tabDatosComerciales.Controls.Add(Me.cbxPais)
        Me.tabDatosComerciales.Controls.Add(Me.txtTelefonos)
        Me.tabDatosComerciales.Controls.Add(Me.txtDireccion)
        Me.tabDatosComerciales.Controls.Add(Me.txtNombreFantasia)
        Me.tabDatosComerciales.Controls.Add(Me.txtUsuarioModificacion)
        Me.tabDatosComerciales.Controls.Add(Me.txtFechaModificacion)
        Me.tabDatosComerciales.Controls.Add(Me.lblModificacion)
        Me.tabDatosComerciales.Controls.Add(Me.txtFechaUltimaCompra)
        Me.tabDatosComerciales.Controls.Add(Me.lblUltimaCompra)
        Me.tabDatosComerciales.Controls.Add(Me.txtUsuarioAlta)
        Me.tabDatosComerciales.Controls.Add(Me.txtFechaAlta)
        Me.tabDatosComerciales.Controls.Add(Me.lblAlta)
        Me.tabDatosComerciales.Controls.Add(Me.lblSucursal)
        Me.tabDatosComerciales.Controls.Add(Me.lblMoneda)
        Me.tabDatosComerciales.Controls.Add(Me.lblTipoProveedor)
        Me.tabDatosComerciales.Controls.Add(Me.lblBarrio)
        Me.tabDatosComerciales.Controls.Add(Me.lblCiudad)
        Me.tabDatosComerciales.Controls.Add(Me.lblDepartamento)
        Me.tabDatosComerciales.Controls.Add(Me.lblPais)
        Me.tabDatosComerciales.Controls.Add(Me.lblTelefonos)
        Me.tabDatosComerciales.Controls.Add(Me.lblDireccion)
        Me.tabDatosComerciales.Controls.Add(Me.lblNombreFantasia)
        Me.tabDatosComerciales.Location = New System.Drawing.Point(4, 22)
        Me.tabDatosComerciales.Name = "tabDatosComerciales"
        Me.tabDatosComerciales.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDatosComerciales.Size = New System.Drawing.Size(692, 300)
        Me.tabDatosComerciales.TabIndex = 0
        Me.tabDatosComerciales.Text = "Datos Comerciales"
        Me.tabDatosComerciales.UseVisualStyleBackColor = True
        '
        'chkDelExterior
        '
        Me.chkDelExterior.AutoSize = True
        Me.chkDelExterior.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkDelExterior.Location = New System.Drawing.Point(476, 55)
        Me.chkDelExterior.Name = "chkDelExterior"
        Me.chkDelExterior.Size = New System.Drawing.Size(83, 17)
        Me.chkDelExterior.TabIndex = 48
        Me.chkDelExterior.Text = "Del Exterior:"
        Me.chkDelExterior.UseVisualStyleBackColor = True
        '
        'chkAcopiador
        '
        Me.chkAcopiador.AutoSize = True
        Me.chkAcopiador.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkAcopiador.Location = New System.Drawing.Point(604, 228)
        Me.chkAcopiador.Name = "chkAcopiador"
        Me.chkAcopiador.Size = New System.Drawing.Size(74, 17)
        Me.chkAcopiador.TabIndex = 47
        Me.chkAcopiador.Text = "Acopiador"
        Me.chkAcopiador.UseVisualStyleBackColor = True
        '
        'chkSujetoRetencion
        '
        Me.chkSujetoRetencion.AutoSize = True
        Me.chkSujetoRetencion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkSujetoRetencion.Location = New System.Drawing.Point(488, 229)
        Me.chkSujetoRetencion.Name = "chkSujetoRetencion"
        Me.chkSujetoRetencion.Size = New System.Drawing.Size(100, 17)
        Me.chkSujetoRetencion.TabIndex = 40
        Me.chkSujetoRetencion.Text = "Sujeto a Reten:"
        Me.chkSujetoRetencion.UseVisualStyleBackColor = True
        '
        'chkExportado
        '
        Me.chkExportado.AutoSize = True
        Me.chkExportado.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkExportado.Location = New System.Drawing.Point(598, 205)
        Me.chkExportado.Name = "chkExportado"
        Me.chkExportado.Size = New System.Drawing.Size(80, 17)
        Me.chkExportado.TabIndex = 39
        Me.chkExportado.Text = "Exportador:"
        Me.chkExportado.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(239, 140)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 13)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "T. Compra:"
        '
        'cbxTipoCompra
        '
        Me.cbxTipoCompra.CampoWhere = Nothing
        Me.cbxTipoCompra.CargarUnaSolaVez = False
        Me.cbxTipoCompra.DataDisplayMember = Nothing
        Me.cbxTipoCompra.DataFilter = Nothing
        Me.cbxTipoCompra.DataOrderBy = Nothing
        Me.cbxTipoCompra.DataSource = Nothing
        Me.cbxTipoCompra.DataValueMember = Nothing
        Me.cbxTipoCompra.dtSeleccionado = Nothing
        Me.cbxTipoCompra.FormABM = Nothing
        Me.cbxTipoCompra.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxTipoCompra.Location = New System.Drawing.Point(305, 136)
        Me.cbxTipoCompra.Name = "cbxTipoCompra"
        Me.cbxTipoCompra.SeleccionMultiple = False
        Me.cbxTipoCompra.SeleccionObligatoria = False
        Me.cbxTipoCompra.Size = New System.Drawing.Size(157, 21)
        Me.cbxTipoCompra.SoloLectura = False
        Me.cbxTipoCompra.TabIndex = 28
        Me.cbxTipoCompra.Texto = ""
        '
        'chkRetentor
        '
        Me.chkRetentor.AutoSize = True
        Me.chkRetentor.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkRetentor.Location = New System.Drawing.Point(518, 203)
        Me.chkRetentor.Name = "chkRetentor"
        Me.chkRetentor.Size = New System.Drawing.Size(70, 17)
        Me.chkRetentor.TabIndex = 38
        Me.chkRetentor.Text = "Retentor:"
        Me.chkRetentor.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(601, 179)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(36, 13)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "Plazo:"
        '
        'txtPlazo
        '
        Me.txtPlazo.Color = System.Drawing.Color.Empty
        Me.txtPlazo.Decimales = False
        Me.txtPlazo.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtPlazo.Location = New System.Drawing.Point(639, 176)
        Me.txtPlazo.Name = "txtPlazo"
        Me.txtPlazo.Size = New System.Drawing.Size(39, 21)
        Me.txtPlazo.SoloLectura = False
        Me.txtPlazo.TabIndex = 35
        Me.txtPlazo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPlazo.Texto = "0"
        '
        'chkCredito
        '
        Me.chkCredito.AutoSize = True
        Me.chkCredito.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkCredito.Location = New System.Drawing.Point(526, 177)
        Me.chkCredito.Name = "chkCredito"
        Me.chkCredito.Size = New System.Drawing.Size(62, 17)
        Me.chkCredito.TabIndex = 33
        Me.chkCredito.Text = "Crédito:"
        Me.chkCredito.UseVisualStyleBackColor = True
        '
        'ocxCuentaCompra
        '
        Me.ocxCuentaCompra.AlturaMaxima = 90
        Me.ocxCuentaCompra.Consulta = Nothing
        Me.ocxCuentaCompra.IDCentroCosto = 0
        Me.ocxCuentaCompra.IDUnidadNegocio = 0
        Me.ocxCuentaCompra.ListarTodas = False
        Me.ocxCuentaCompra.Location = New System.Drawing.Point(109, 191)
        Me.ocxCuentaCompra.Name = "ocxCuentaCompra"
        Me.ocxCuentaCompra.Registro = Nothing
        Me.ocxCuentaCompra.Resolucion173 = False
        Me.ocxCuentaCompra.Seleccionado = False
        Me.ocxCuentaCompra.Size = New System.Drawing.Size(353, 20)
        Me.ocxCuentaCompra.SoloLectura = False
        Me.ocxCuentaCompra.TabIndex = 32
        Me.ocxCuentaCompra.Texto = Nothing
        Me.ocxCuentaCompra.whereFiltro = Nothing
        '
        'ocxCuentaVenta
        '
        Me.ocxCuentaVenta.AlturaMaxima = 62
        Me.ocxCuentaVenta.Consulta = Nothing
        Me.ocxCuentaVenta.IDCentroCosto = 0
        Me.ocxCuentaVenta.IDUnidadNegocio = 0
        Me.ocxCuentaVenta.ListarTodas = False
        Me.ocxCuentaVenta.Location = New System.Drawing.Point(109, 217)
        Me.ocxCuentaVenta.Name = "ocxCuentaVenta"
        Me.ocxCuentaVenta.Registro = Nothing
        Me.ocxCuentaVenta.Resolucion173 = False
        Me.ocxCuentaVenta.Seleccionado = False
        Me.ocxCuentaVenta.Size = New System.Drawing.Size(353, 20)
        Me.ocxCuentaVenta.SoloLectura = False
        Me.ocxCuentaVenta.TabIndex = 37
        Me.ocxCuentaVenta.Texto = Nothing
        Me.ocxCuentaVenta.whereFiltro = Nothing
        '
        'lblCuentaVenta
        '
        Me.lblCuentaVenta.AutoSize = True
        Me.lblCuentaVenta.Location = New System.Drawing.Point(6, 221)
        Me.lblCuentaVenta.Name = "lblCuentaVenta"
        Me.lblCuentaVenta.Size = New System.Drawing.Size(90, 13)
        Me.lblCuentaVenta.TabIndex = 36
        Me.lblCuentaVenta.Text = "Cuenta de Venta:"
        '
        'lblCuentaCompra
        '
        Me.lblCuentaCompra.AutoSize = True
        Me.lblCuentaCompra.Location = New System.Drawing.Point(6, 195)
        Me.lblCuentaCompra.Name = "lblCuentaCompra"
        Me.lblCuentaCompra.Size = New System.Drawing.Size(98, 13)
        Me.lblCuentaCompra.TabIndex = 31
        Me.lblCuentaCompra.Text = "Cuenta de Compra:"
        '
        'chkActivado
        '
        Me.chkActivado.AutoSize = True
        Me.chkActivado.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkActivado.Location = New System.Drawing.Point(488, 38)
        Me.chkActivado.Name = "chkActivado"
        Me.chkActivado.Size = New System.Drawing.Size(71, 17)
        Me.chkActivado.TabIndex = 6
        Me.chkActivado.Text = "Activado:"
        Me.chkActivado.UseVisualStyleBackColor = True
        '
        'cbxTipoProveedor
        '
        Me.cbxTipoProveedor.CampoWhere = Nothing
        Me.cbxTipoProveedor.CargarUnaSolaVez = False
        Me.cbxTipoProveedor.DataDisplayMember = Nothing
        Me.cbxTipoProveedor.DataFilter = Nothing
        Me.cbxTipoProveedor.DataOrderBy = Nothing
        Me.cbxTipoProveedor.DataSource = Nothing
        Me.cbxTipoProveedor.DataValueMember = Nothing
        Me.cbxTipoProveedor.dtSeleccionado = Nothing
        Me.cbxTipoProveedor.FormABM = Nothing
        Me.cbxTipoProveedor.Indicaciones = Nothing
        Me.cbxTipoProveedor.Location = New System.Drawing.Point(305, 94)
        Me.cbxTipoProveedor.Name = "cbxTipoProveedor"
        Me.cbxTipoProveedor.SeleccionMultiple = False
        Me.cbxTipoProveedor.SeleccionObligatoria = False
        Me.cbxTipoProveedor.Size = New System.Drawing.Size(157, 21)
        Me.cbxTipoProveedor.SoloLectura = False
        Me.cbxTipoProveedor.TabIndex = 14
        Me.cbxTipoProveedor.Texto = ""
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(305, 115)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(157, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 21
        Me.cbxMoneda.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(305, 73)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(157, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 10
        Me.cbxSucursal.Texto = ""
        '
        'cbxBarrio
        '
        Me.cbxBarrio.CampoWhere = Nothing
        Me.cbxBarrio.CargarUnaSolaVez = False
        Me.cbxBarrio.DataDisplayMember = Nothing
        Me.cbxBarrio.DataFilter = Nothing
        Me.cbxBarrio.DataOrderBy = Nothing
        Me.cbxBarrio.DataSource = Nothing
        Me.cbxBarrio.DataValueMember = Nothing
        Me.cbxBarrio.dtSeleccionado = Nothing
        Me.cbxBarrio.FormABM = Nothing
        Me.cbxBarrio.Indicaciones = Nothing
        Me.cbxBarrio.Location = New System.Drawing.Point(100, 137)
        Me.cbxBarrio.Name = "cbxBarrio"
        Me.cbxBarrio.SeleccionMultiple = False
        Me.cbxBarrio.SeleccionObligatoria = False
        Me.cbxBarrio.Size = New System.Drawing.Size(135, 21)
        Me.cbxBarrio.SoloLectura = False
        Me.cbxBarrio.TabIndex = 26
        Me.cbxBarrio.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(100, 115)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(135, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 19
        Me.cbxCiudad.Texto = ""
        '
        'cbxDepartamento
        '
        Me.cbxDepartamento.CampoWhere = Nothing
        Me.cbxDepartamento.CargarUnaSolaVez = False
        Me.cbxDepartamento.DataDisplayMember = Nothing
        Me.cbxDepartamento.DataFilter = Nothing
        Me.cbxDepartamento.DataOrderBy = Nothing
        Me.cbxDepartamento.DataSource = Nothing
        Me.cbxDepartamento.DataValueMember = Nothing
        Me.cbxDepartamento.dtSeleccionado = Nothing
        Me.cbxDepartamento.FormABM = Nothing
        Me.cbxDepartamento.Indicaciones = Nothing
        Me.cbxDepartamento.Location = New System.Drawing.Point(100, 94)
        Me.cbxDepartamento.Name = "cbxDepartamento"
        Me.cbxDepartamento.SeleccionMultiple = False
        Me.cbxDepartamento.SeleccionObligatoria = False
        Me.cbxDepartamento.Size = New System.Drawing.Size(135, 21)
        Me.cbxDepartamento.SoloLectura = False
        Me.cbxDepartamento.TabIndex = 12
        Me.cbxDepartamento.Texto = ""
        '
        'cbxPais
        '
        Me.cbxPais.CampoWhere = Nothing
        Me.cbxPais.CargarUnaSolaVez = False
        Me.cbxPais.DataDisplayMember = Nothing
        Me.cbxPais.DataFilter = Nothing
        Me.cbxPais.DataOrderBy = Nothing
        Me.cbxPais.DataSource = Nothing
        Me.cbxPais.DataValueMember = Nothing
        Me.cbxPais.dtSeleccionado = Nothing
        Me.cbxPais.FormABM = Nothing
        Me.cbxPais.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxPais.Location = New System.Drawing.Point(100, 73)
        Me.cbxPais.Name = "cbxPais"
        Me.cbxPais.SeleccionMultiple = False
        Me.cbxPais.SeleccionObligatoria = False
        Me.cbxPais.Size = New System.Drawing.Size(135, 21)
        Me.cbxPais.SoloLectura = False
        Me.cbxPais.TabIndex = 8
        Me.cbxPais.Texto = ""
        '
        'txtTelefonos
        '
        Me.txtTelefonos.BackColor = System.Drawing.Color.White
        Me.txtTelefonos.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefonos.Color = System.Drawing.Color.Empty
        Me.txtTelefonos.Indicaciones = Nothing
        Me.txtTelefonos.Location = New System.Drawing.Point(545, 15)
        Me.txtTelefonos.Multilinea = False
        Me.txtTelefonos.Name = "txtTelefonos"
        Me.txtTelefonos.Size = New System.Drawing.Size(133, 21)
        Me.txtTelefonos.SoloLectura = False
        Me.txtTelefonos.TabIndex = 3
        Me.txtTelefonos.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTelefonos.Texto = ""
        '
        'txtDireccion
        '
        Me.txtDireccion.BackColor = System.Drawing.Color.White
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Color = System.Drawing.Color.Empty
        Me.txtDireccion.Indicaciones = Nothing
        Me.txtDireccion.Location = New System.Drawing.Point(100, 36)
        Me.txtDireccion.Multilinea = False
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(362, 21)
        Me.txtDireccion.SoloLectura = False
        Me.txtDireccion.TabIndex = 5
        Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccion.Texto = ""
        '
        'txtNombreFantasia
        '
        Me.txtNombreFantasia.BackColor = System.Drawing.Color.White
        Me.txtNombreFantasia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreFantasia.Color = System.Drawing.Color.Empty
        Me.txtNombreFantasia.Indicaciones = Nothing
        Me.txtNombreFantasia.Location = New System.Drawing.Point(100, 13)
        Me.txtNombreFantasia.Multilinea = False
        Me.txtNombreFantasia.Name = "txtNombreFantasia"
        Me.txtNombreFantasia.Size = New System.Drawing.Size(362, 21)
        Me.txtNombreFantasia.SoloLectura = False
        Me.txtNombreFantasia.TabIndex = 1
        Me.txtNombreFantasia.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNombreFantasia.Texto = ""
        '
        'txtUsuarioModificacion
        '
        Me.txtUsuarioModificacion.BackColor = System.Drawing.Color.White
        Me.txtUsuarioModificacion.Location = New System.Drawing.Point(628, 104)
        Me.txtUsuarioModificacion.Name = "txtUsuarioModificacion"
        Me.txtUsuarioModificacion.ReadOnly = True
        Me.txtUsuarioModificacion.Size = New System.Drawing.Size(50, 20)
        Me.txtUsuarioModificacion.TabIndex = 24
        Me.txtUsuarioModificacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtFechaModificacion
        '
        Me.txtFechaModificacion.BackColor = System.Drawing.Color.White
        Me.txtFechaModificacion.Location = New System.Drawing.Point(545, 104)
        Me.txtFechaModificacion.Name = "txtFechaModificacion"
        Me.txtFechaModificacion.ReadOnly = True
        Me.txtFechaModificacion.Size = New System.Drawing.Size(77, 20)
        Me.txtFechaModificacion.TabIndex = 23
        Me.txtFechaModificacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblModificacion
        '
        Me.lblModificacion.AutoSize = True
        Me.lblModificacion.Location = New System.Drawing.Point(469, 108)
        Me.lblModificacion.Name = "lblModificacion"
        Me.lblModificacion.Size = New System.Drawing.Size(70, 13)
        Me.lblModificacion.TabIndex = 22
        Me.lblModificacion.Text = "Modificacion:"
        '
        'txtFechaUltimaCompra
        '
        Me.txtFechaUltimaCompra.BackColor = System.Drawing.Color.White
        Me.txtFechaUltimaCompra.Location = New System.Drawing.Point(545, 125)
        Me.txtFechaUltimaCompra.Name = "txtFechaUltimaCompra"
        Me.txtFechaUltimaCompra.ReadOnly = True
        Me.txtFechaUltimaCompra.Size = New System.Drawing.Size(77, 20)
        Me.txtFechaUltimaCompra.TabIndex = 30
        Me.txtFechaUltimaCompra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblUltimaCompra
        '
        Me.lblUltimaCompra.AutoSize = True
        Me.lblUltimaCompra.Location = New System.Drawing.Point(477, 132)
        Me.lblUltimaCompra.Name = "lblUltimaCompra"
        Me.lblUltimaCompra.Size = New System.Drawing.Size(62, 13)
        Me.lblUltimaCompra.TabIndex = 29
        Me.lblUltimaCompra.Text = "Ul. Compra:"
        '
        'txtUsuarioAlta
        '
        Me.txtUsuarioAlta.BackColor = System.Drawing.Color.White
        Me.txtUsuarioAlta.Location = New System.Drawing.Point(628, 83)
        Me.txtUsuarioAlta.Name = "txtUsuarioAlta"
        Me.txtUsuarioAlta.ReadOnly = True
        Me.txtUsuarioAlta.Size = New System.Drawing.Size(50, 20)
        Me.txtUsuarioAlta.TabIndex = 17
        Me.txtUsuarioAlta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblAlta
        '
        Me.lblAlta.AutoSize = True
        Me.lblAlta.Location = New System.Drawing.Point(511, 87)
        Me.lblAlta.Name = "lblAlta"
        Me.lblAlta.Size = New System.Drawing.Size(28, 13)
        Me.lblAlta.TabIndex = 15
        Me.lblAlta.Text = "Alta:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(248, 77)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 9
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(250, 119)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 20
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblTipoProveedor
        '
        Me.lblTipoProveedor.AutoSize = True
        Me.lblTipoProveedor.Location = New System.Drawing.Point(268, 98)
        Me.lblTipoProveedor.Name = "lblTipoProveedor"
        Me.lblTipoProveedor.Size = New System.Drawing.Size(31, 13)
        Me.lblTipoProveedor.TabIndex = 13
        Me.lblTipoProveedor.Text = "Tipo:"
        '
        'lblBarrio
        '
        Me.lblBarrio.AutoSize = True
        Me.lblBarrio.Location = New System.Drawing.Point(6, 141)
        Me.lblBarrio.Name = "lblBarrio"
        Me.lblBarrio.Size = New System.Drawing.Size(37, 13)
        Me.lblBarrio.TabIndex = 25
        Me.lblBarrio.Text = "Barrio:"
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(6, 119)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(43, 13)
        Me.lblCiudad.TabIndex = 18
        Me.lblCiudad.Text = "Ciudad:"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(6, 98)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(77, 13)
        Me.lblDepartamento.TabIndex = 11
        Me.lblDepartamento.Text = "Departamento:"
        '
        'lblPais
        '
        Me.lblPais.AutoSize = True
        Me.lblPais.Location = New System.Drawing.Point(6, 77)
        Me.lblPais.Name = "lblPais"
        Me.lblPais.Size = New System.Drawing.Size(30, 13)
        Me.lblPais.TabIndex = 7
        Me.lblPais.Text = "Pais:"
        '
        'lblTelefonos
        '
        Me.lblTelefonos.AutoSize = True
        Me.lblTelefonos.Location = New System.Drawing.Point(482, 19)
        Me.lblTelefonos.Name = "lblTelefonos"
        Me.lblTelefonos.Size = New System.Drawing.Size(57, 13)
        Me.lblTelefonos.TabIndex = 2
        Me.lblTelefonos.Text = "Telefonos:"
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.Location = New System.Drawing.Point(6, 40)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(55, 13)
        Me.lblDireccion.TabIndex = 4
        Me.lblDireccion.Text = "Direccion:"
        '
        'lblNombreFantasia
        '
        Me.lblNombreFantasia.AutoSize = True
        Me.lblNombreFantasia.Location = New System.Drawing.Point(6, 15)
        Me.lblNombreFantasia.Name = "lblNombreFantasia"
        Me.lblNombreFantasia.Size = New System.Drawing.Size(90, 13)
        Me.lblNombreFantasia.TabIndex = 0
        Me.lblNombreFantasia.Text = "Nombre Fantasia:"
        '
        'lblRazonSocial
        '
        Me.lblRazonSocial.AutoSize = True
        Me.lblRazonSocial.Location = New System.Drawing.Point(115, 12)
        Me.lblRazonSocial.Name = "lblRazonSocial"
        Me.lblRazonSocial.Size = New System.Drawing.Size(73, 13)
        Me.lblRazonSocial.TabIndex = 2
        Me.lblRazonSocial.Text = "Razón Social:"
        '
        'lblReferencia
        '
        Me.lblReferencia.AutoSize = True
        Me.lblReferencia.Location = New System.Drawing.Point(6, 12)
        Me.lblReferencia.Name = "lblReferencia"
        Me.lblReferencia.Size = New System.Drawing.Size(27, 13)
        Me.lblReferencia.TabIndex = 0
        Me.lblReferencia.Text = "Ref:"
        '
        'txtReferencia
        '
        Me.txtReferencia.BackColor = System.Drawing.Color.White
        Me.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferencia.Color = System.Drawing.Color.Empty
        Me.txtReferencia.Indicaciones = "(F1) Para busqueda avanzada"
        Me.txtReferencia.Location = New System.Drawing.Point(33, 8)
        Me.txtReferencia.Multilinea = False
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Size = New System.Drawing.Size(82, 21)
        Me.txtReferencia.SoloLectura = False
        Me.txtReferencia.TabIndex = 1
        Me.txtReferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferencia.Texto = ""
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.BackColor = System.Drawing.Color.White
        Me.txtRazonSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazonSocial.Color = System.Drawing.Color.Empty
        Me.txtRazonSocial.Indicaciones = "(F1) Para busqueda avanzada"
        Me.txtRazonSocial.Location = New System.Drawing.Point(188, 8)
        Me.txtRazonSocial.Multilinea = False
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.Size = New System.Drawing.Size(254, 21)
        Me.txtRazonSocial.SoloLectura = False
        Me.txtRazonSocial.TabIndex = 3
        Me.txtRazonSocial.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRazonSocial.Texto = ""
        '
        'txtRUC
        '
        Me.txtRUC.BackColor = System.Drawing.Color.White
        Me.txtRUC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRUC.Color = System.Drawing.Color.Empty
        Me.txtRUC.Indicaciones = Nothing
        Me.txtRUC.Location = New System.Drawing.Point(481, 8)
        Me.txtRUC.Multilinea = False
        Me.txtRUC.Name = "txtRUC"
        Me.txtRUC.Size = New System.Drawing.Size(78, 21)
        Me.txtRUC.SoloLectura = False
        Me.txtRUC.TabIndex = 5
        Me.txtRUC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtRUC.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(603, 7)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(39, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 7
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblCodigoProveedor
        '
        Me.lblCodigoProveedor.AutoSize = True
        Me.lblCodigoProveedor.Location = New System.Drawing.Point(6, 164)
        Me.lblCodigoProveedor.Name = "lblCodigoProveedor"
        Me.lblCodigoProveedor.Size = New System.Drawing.Size(95, 13)
        Me.lblCodigoProveedor.TabIndex = 15
        Me.lblCodigoProveedor.Text = "Codigo Proveedor:"
        '
        'txtCodigoProveedor
        '
        Me.txtCodigoProveedor.BackColor = System.Drawing.Color.White
        Me.txtCodigoProveedor.Location = New System.Drawing.Point(109, 161)
        Me.txtCodigoProveedor.Name = "txtCodigoProveedor"
        Me.txtCodigoProveedor.ReadOnly = True
        Me.txtCodigoProveedor.Size = New System.Drawing.Size(77, 20)
        Me.txtCodigoProveedor.TabIndex = 49
        Me.txtCodigoProveedor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'frmProveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(727, 432)
        Me.Controls.Add(Me.lblReferencia)
        Me.Controls.Add(Me.txtReferencia)
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.txtRazonSocial)
        Me.Controls.Add(Me.lblRUC)
        Me.Controls.Add(Me.txtRUC)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.tcGeneral)
        Me.Controls.Add(Me.lblRazonSocial)
        Me.Controls.Add(Me.txtID)
        Me.KeyPreview = True
        Me.Name = "frmProveedor"
        Me.Tag = "PROVEEDOR"
        Me.Text = "frmProveedor"
        Me.tabDatosAdicionales.ResumeLayout(False)
        Me.tabDatosAdicionales.PerformLayout()
        Me.tabDivisiones.ResumeLayout(False)
        Me.tabDivisiones.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabLocalizacion.ResumeLayout(False)
        Me.tcGeneral.ResumeLayout(False)
        Me.tabDatosComerciales.ResumeLayout(False)
        Me.tabDatosComerciales.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents tabDatosAdicionales As System.Windows.Forms.TabPage
    Friend WithEvents txtFax As ERP.ocxTXTString
    Friend WithEvents OcxTXTString8 As ERP.ocxTXTString
    Friend WithEvents OcxTXTString7 As ERP.ocxTXTString
    Friend WithEvents OcxTXTString6 As ERP.ocxTXTString
    Friend WithEvents OcxTXTNumeric7 As ERP.ocxTXTNumeric
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents lvPaisLista As System.Windows.Forms.ListView
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents txtPaginaWeb As System.Windows.Forms.TextBox
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents lblPaginaWeb As System.Windows.Forms.Label
    Friend WithEvents tabDivisiones As System.Windows.Forms.TabPage
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents OcxTXTString10 As ERP.ocxTXTString
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtRazonSocial As ERP.ocxTXTString
    Friend WithEvents lblRUC As System.Windows.Forms.Label
    Friend WithEvents txtRUC As ERP.ocxTXTString
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents tcGeneral As System.Windows.Forms.TabControl
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents tabLocalizacion As System.Windows.Forms.TabPage
    Friend WithEvents lblRazonSocial As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents tabDatosComerciales As System.Windows.Forms.TabPage
    Friend WithEvents chkActivado As System.Windows.Forms.CheckBox
    Friend WithEvents cbxTipoProveedor As ERP.ocxCBX
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents cbxBarrio As ERP.ocxCBX
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents cbxDepartamento As ERP.ocxCBX
    Friend WithEvents cbxPais As ERP.ocxCBX
    Friend WithEvents txtTelefonos As ERP.ocxTXTString
    Friend WithEvents txtDireccion As ERP.ocxTXTString
    Friend WithEvents txtNombreFantasia As ERP.ocxTXTString
    Friend WithEvents txtUsuarioModificacion As System.Windows.Forms.TextBox
    Friend WithEvents txtFechaModificacion As System.Windows.Forms.TextBox
    Friend WithEvents lblModificacion As System.Windows.Forms.Label
    Friend WithEvents txtFechaUltimaCompra As System.Windows.Forms.TextBox
    Friend WithEvents lblUltimaCompra As System.Windows.Forms.Label
    Friend WithEvents txtUsuarioAlta As System.Windows.Forms.TextBox
    Friend WithEvents txtFechaAlta As System.Windows.Forms.TextBox
    Friend WithEvents lblAlta As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents lblTipoProveedor As System.Windows.Forms.Label
    Friend WithEvents lblBarrio As System.Windows.Forms.Label
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents lblPais As System.Windows.Forms.Label
    Friend WithEvents lblTelefonos As System.Windows.Forms.Label
    Friend WithEvents lblDireccion As System.Windows.Forms.Label
    Friend WithEvents lblNombreFantasia As System.Windows.Forms.Label
    Friend WithEvents lblCuentaCompra As System.Windows.Forms.Label
    Friend WithEvents OcxTXTNumeric1 As ERP.ocxTXTNumeric
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents ocxCuentaCompra As ERP.ocxTXTCuentaContable
    Friend WithEvents ocxCuentaVenta As ERP.ocxTXTCuentaContable
    Friend WithEvents lblCuentaVenta As System.Windows.Forms.Label
    Friend WithEvents lblReferencia As System.Windows.Forms.Label
    Friend WithEvents txtReferencia As ERP.ocxTXTString
    Friend WithEvents chkCredito As System.Windows.Forms.CheckBox
    Friend WithEvents txtPlazo As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkRetentor As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxTipoCompra As ERP.ocxCBX
    Friend WithEvents chkSujetoRetencion As System.Windows.Forms.CheckBox
    Friend WithEvents chkExportado As System.Windows.Forms.CheckBox
    Friend WithEvents chkAcopiador As System.Windows.Forms.CheckBox
    Friend WithEvents OcxProveedoresMapa1 As ERP.ocxProveedoresMapa
    Friend WithEvents chkDelExterior As CheckBox
    Friend WithEvents txtCodigoProveedor As TextBox
    Friend WithEvents lblCodigoProveedor As Label
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAcuerdodeClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.lblDireccion = New System.Windows.Forms.Label()
        Me.lblVigencia = New System.Windows.Forms.Label()
        Me.lblNroAcuerdo = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.lblDesde = New System.Windows.Forms.Label()
        Me.lblHasta = New System.Windows.Forms.Label()
        Me.lblMotivoAcuerdo = New System.Windows.Forms.Label()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.rbDesactivado = New System.Windows.Forms.RadioButton()
        Me.rbActivado = New System.Windows.Forms.RadioButton()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.chkTSucursales = New System.Windows.Forms.CheckBox()
        Me.rbNC = New System.Windows.Forms.RadioButton()
        Me.rbFactura = New System.Windows.Forms.RadioButton()
        Me.ChkPorcentaje = New System.Windows.Forms.CheckBox()
        Me.gp2 = New System.Windows.Forms.GroupBox()
        Me.gp1 = New System.Windows.Forms.GroupBox()
        Me.ChkImporte = New System.Windows.Forms.CheckBox()
        Me.ChkProducto = New System.Windows.Forms.CheckBox()
        Me.ChkFamilia = New System.Windows.Forms.CheckBox()
        Me.ChkTodos = New System.Windows.Forms.CheckBox()
        Me.lblObservaciones = New System.Windows.Forms.Label()
        Me.ChkPresentacion = New System.Windows.Forms.CheckBox()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.VerDetalleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportarAExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.lvlista = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lklAgregar = New System.Windows.Forms.LinkLabel()
        Me.lklEliminar = New System.Windows.Forms.LinkLabel()
        Me.txtImporte = New ERP.ocxTXTString()
        Me.txtPorcentaje = New ERP.ocxTXTString()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxMotivoAcuerdo = New ERP.ocxCBX()
        Me.txtVigenciaHasta = New ERP.ocxTXTDate()
        Me.txtVigenciaDesde = New ERP.ocxTXTDate()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.txtNroAcuerdo = New ERP.ocxTXTString()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.txtDireccion = New ERP.ocxTXTString()
        Me.cbxPresentacion = New ERP.ocxCBX()
        Me.cbxFamilia = New ERP.ocxCBX()
        Me.txtpre = New ERP.ocxTXTString()
        Me.txtf = New ERP.ocxTXTString()
        Me.gp2.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(15, 102)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(42, 13)
        Me.lblCliente.TabIndex = 2
        Me.lblCliente.Text = "Cliente:"
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.Location = New System.Drawing.Point(14, 130)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(55, 13)
        Me.lblDireccion.TabIndex = 40
        Me.lblDireccion.Text = "Direccion:"
        '
        'lblVigencia
        '
        Me.lblVigencia.AutoSize = True
        Me.lblVigencia.Location = New System.Drawing.Point(267, 179)
        Me.lblVigencia.Name = "lblVigencia"
        Me.lblVigencia.Size = New System.Drawing.Size(54, 13)
        Me.lblVigencia.TabIndex = 51
        Me.lblVigencia.Text = "Vigencia :"
        '
        'lblNroAcuerdo
        '
        Me.lblNroAcuerdo.AutoSize = True
        Me.lblNroAcuerdo.Location = New System.Drawing.Point(152, 63)
        Me.lblNroAcuerdo.Name = "lblNroAcuerdo"
        Me.lblNroAcuerdo.Size = New System.Drawing.Size(70, 13)
        Me.lblNroAcuerdo.TabIndex = 49
        Me.lblNroAcuerdo.Text = "Nro Acuerdo:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(15, 63)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 42
        Me.lblID.Text = "ID:"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(549, 25)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 46
        Me.lblFecha.Text = "Fecha:"
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Location = New System.Drawing.Point(610, 425)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 47
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.ImageIndex = 4
        Me.btnCancelar.Location = New System.Drawing.Point(514, 425)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(90, 23)
        Me.btnCancelar.TabIndex = 45
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGuardar.ImageIndex = 3
        Me.btnGuardar.Location = New System.Drawing.Point(418, 425)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(90, 23)
        Me.btnGuardar.TabIndex = 25
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'lblDesde
        '
        Me.lblDesde.AutoSize = True
        Me.lblDesde.Location = New System.Drawing.Point(343, 154)
        Me.lblDesde.Name = "lblDesde"
        Me.lblDesde.Size = New System.Drawing.Size(44, 13)
        Me.lblDesde.TabIndex = 54
        Me.lblDesde.Text = "Desde :"
        '
        'lblHasta
        '
        Me.lblHasta.AutoSize = True
        Me.lblHasta.Location = New System.Drawing.Point(445, 154)
        Me.lblHasta.Name = "lblHasta"
        Me.lblHasta.Size = New System.Drawing.Size(41, 13)
        Me.lblHasta.TabIndex = 55
        Me.lblHasta.Text = "Hasta :"
        '
        'lblMotivoAcuerdo
        '
        Me.lblMotivoAcuerdo.AutoSize = True
        Me.lblMotivoAcuerdo.Location = New System.Drawing.Point(12, 220)
        Me.lblMotivoAcuerdo.Name = "lblMotivoAcuerdo"
        Me.lblMotivoAcuerdo.Size = New System.Drawing.Size(100, 13)
        Me.lblMotivoAcuerdo.TabIndex = 15
        Me.lblMotivoAcuerdo.Text = "Motivo de Acuerdo:"
        '
        'btnNuevo
        '
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.ImageIndex = 3
        Me.btnNuevo.Location = New System.Drawing.Point(226, 425)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(90, 23)
        Me.btnNuevo.TabIndex = 59
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'rbDesactivado
        '
        Me.rbDesactivado.AutoSize = True
        Me.rbDesactivado.Location = New System.Drawing.Point(109, 12)
        Me.rbDesactivado.Name = "rbDesactivado"
        Me.rbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rbDesactivado.TabIndex = 1
        Me.rbDesactivado.Text = "Desactivado"
        Me.rbDesactivado.UseVisualStyleBackColor = True
        '
        'rbActivado
        '
        Me.rbActivado.AutoSize = True
        Me.rbActivado.Checked = True
        Me.rbActivado.Location = New System.Drawing.Point(6, 12)
        Me.rbActivado.Name = "rbActivado"
        Me.rbActivado.Size = New System.Drawing.Size(67, 17)
        Me.rbActivado.TabIndex = 0
        Me.rbActivado.TabStop = True
        Me.rbActivado.Text = "Activado"
        Me.rbActivado.UseVisualStyleBackColor = True
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(433, 344)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 13)
        Me.lblEstado.TabIndex = 61
        Me.lblEstado.Text = "Estado :"
        '
        'btnEditar
        '
        Me.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEditar.ImageIndex = 3
        Me.btnEditar.Location = New System.Drawing.Point(322, 425)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(90, 23)
        Me.btnEditar.TabIndex = 64
        Me.btnEditar.Text = "&Modificar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'chkTSucursales
        '
        Me.chkTSucursales.AutoSize = True
        Me.chkTSucursales.Location = New System.Drawing.Point(18, 160)
        Me.chkTSucursales.Name = "chkTSucursales"
        Me.chkTSucursales.Size = New System.Drawing.Size(127, 17)
        Me.chkTSucursales.TabIndex = 8
        Me.chkTSucursales.Text = "Todas las Sucursales"
        Me.chkTSucursales.UseVisualStyleBackColor = True
        '
        'rbNC
        '
        Me.rbNC.AutoSize = True
        Me.rbNC.Checked = True
        Me.rbNC.Location = New System.Drawing.Point(23, 21)
        Me.rbNC.Name = "rbNC"
        Me.rbNC.Size = New System.Drawing.Size(99, 17)
        Me.rbNC.TabIndex = 0
        Me.rbNC.TabStop = True
        Me.rbNC.Text = "Nota de Credito"
        Me.rbNC.UseVisualStyleBackColor = True
        '
        'rbFactura
        '
        Me.rbFactura.AutoSize = True
        Me.rbFactura.Location = New System.Drawing.Point(151, 21)
        Me.rbFactura.Name = "rbFactura"
        Me.rbFactura.Size = New System.Drawing.Size(111, 17)
        Me.rbFactura.TabIndex = 3
        Me.rbFactura.Text = "Factura de Cliente"
        Me.rbFactura.UseVisualStyleBackColor = True
        '
        'ChkPorcentaje
        '
        Me.ChkPorcentaje.AutoSize = True
        Me.ChkPorcentaje.Location = New System.Drawing.Point(541, 160)
        Me.ChkPorcentaje.Name = "ChkPorcentaje"
        Me.ChkPorcentaje.Size = New System.Drawing.Size(83, 17)
        Me.ChkPorcentaje.TabIndex = 11
        Me.ChkPorcentaje.Text = "Porcentaje :"
        Me.ChkPorcentaje.UseVisualStyleBackColor = True
        '
        'gp2
        '
        Me.gp2.Controls.Add(Me.rbActivado)
        Me.gp2.Controls.Add(Me.rbDesactivado)
        Me.gp2.Location = New System.Drawing.Point(485, 330)
        Me.gp2.Name = "gp2"
        Me.gp2.Size = New System.Drawing.Size(200, 33)
        Me.gp2.TabIndex = 75
        Me.gp2.TabStop = False
        '
        'gp1
        '
        Me.gp1.Location = New System.Drawing.Point(13, 13)
        Me.gp1.Name = "gp1"
        Me.gp1.Size = New System.Drawing.Size(256, 27)
        Me.gp1.TabIndex = 1
        Me.gp1.TabStop = False
        '
        'ChkImporte
        '
        Me.ChkImporte.AutoSize = True
        Me.ChkImporte.Location = New System.Drawing.Point(541, 190)
        Me.ChkImporte.Name = "ChkImporte"
        Me.ChkImporte.Size = New System.Drawing.Size(67, 17)
        Me.ChkImporte.TabIndex = 13
        Me.ChkImporte.Text = "Importe :"
        Me.ChkImporte.UseVisualStyleBackColor = True
        '
        'ChkProducto
        '
        Me.ChkProducto.AutoSize = True
        Me.ChkProducto.Location = New System.Drawing.Point(13, 254)
        Me.ChkProducto.Name = "ChkProducto"
        Me.ChkProducto.Size = New System.Drawing.Size(69, 17)
        Me.ChkProducto.TabIndex = 18
        Me.ChkProducto.Text = "Producto"
        Me.ChkProducto.UseVisualStyleBackColor = True
        '
        'ChkFamilia
        '
        Me.ChkFamilia.AutoSize = True
        Me.ChkFamilia.Location = New System.Drawing.Point(13, 371)
        Me.ChkFamilia.Name = "ChkFamilia"
        Me.ChkFamilia.Size = New System.Drawing.Size(58, 17)
        Me.ChkFamilia.TabIndex = 22
        Me.ChkFamilia.Text = "Familia"
        Me.ChkFamilia.UseVisualStyleBackColor = True
        '
        'ChkTodos
        '
        Me.ChkTodos.AutoSize = True
        Me.ChkTodos.Location = New System.Drawing.Point(13, 399)
        Me.ChkTodos.Name = "ChkTodos"
        Me.ChkTodos.Size = New System.Drawing.Size(122, 17)
        Me.ChkTodos.TabIndex = 24
        Me.ChkTodos.Text = "Todos los productos"
        Me.ChkTodos.UseVisualStyleBackColor = True
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Location = New System.Drawing.Point(273, 217)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(84, 13)
        Me.lblObservaciones.TabIndex = 81
        Me.lblObservaciones.Text = "Observaciones :"
        '
        'ChkPresentacion
        '
        Me.ChkPresentacion.AutoSize = True
        Me.ChkPresentacion.Location = New System.Drawing.Point(13, 344)
        Me.ChkPresentacion.Name = "ChkPresentacion"
        Me.ChkPresentacion.Size = New System.Drawing.Size(88, 17)
        Me.ChkPresentacion.TabIndex = 20
        Me.ChkPresentacion.Text = "Presentación"
        Me.ChkPresentacion.UseVisualStyleBackColor = True
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VerDetalleToolStripMenuItem, Me.ExportarAExcelToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(158, 48)
        '
        'VerDetalleToolStripMenuItem
        '
        Me.VerDetalleToolStripMenuItem.Name = "VerDetalleToolStripMenuItem"
        Me.VerDetalleToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.VerDetalleToolStripMenuItem.Text = "Ver Detalle"
        '
        'ExportarAExcelToolStripMenuItem
        '
        Me.ExportarAExcelToolStripMenuItem.Name = "ExportarAExcelToolStripMenuItem"
        Me.ExportarAExcelToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.ExportarAExcelToolStripMenuItem.Text = "Exportar a Excel"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 461)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(746, 22)
        Me.StatusStrip1.TabIndex = 87
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(407, 374)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(277, 20)
        Me.flpRegistradoPor.TabIndex = 88
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(15, 102)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(59, 13)
        Me.lblProveedor.TabIndex = 91
        Me.lblProveedor.Text = "Proveedor:"
        '
        'lvlista
        '
        Me.lvlista.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lvlista.Location = New System.Drawing.Point(96, 254)
        Me.lvlista.Name = "lvlista"
        Me.lvlista.Size = New System.Drawing.Size(326, 77)
        Me.lvlista.TabIndex = 95
        Me.lvlista.UseCompatibleStateImageBehavior = False
        Me.lvlista.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "ID"
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Referencia"
        Me.ColumnHeader2.Width = 80
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Descripcion"
        Me.ColumnHeader3.Width = 89
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Porcentaje"
        Me.ColumnHeader4.Width = 90
        '
        'lklAgregar
        '
        Me.lklAgregar.AutoSize = True
        Me.lklAgregar.Location = New System.Drawing.Point(30, 274)
        Me.lklAgregar.Name = "lklAgregar"
        Me.lklAgregar.Size = New System.Drawing.Size(44, 13)
        Me.lklAgregar.TabIndex = 96
        Me.lklAgregar.TabStop = True
        Me.lklAgregar.Text = "Agregar"
        '
        'lklEliminar
        '
        Me.lklEliminar.AutoSize = True
        Me.lklEliminar.Location = New System.Drawing.Point(30, 298)
        Me.lklEliminar.Name = "lklEliminar"
        Me.lklEliminar.Size = New System.Drawing.Size(43, 13)
        Me.lklEliminar.TabIndex = 97
        Me.lklEliminar.TabStop = True
        Me.lklEliminar.Text = "Eliminar"
        '
        'txtImporte
        '
        Me.txtImporte.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Indicaciones = Nothing
        Me.txtImporte.Location = New System.Drawing.Point(632, 190)
        Me.txtImporte.Multilinea = False
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(90, 21)
        Me.txtImporte.SoloLectura = False
        Me.txtImporte.TabIndex = 14
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtImporte.Texto = ""
        '
        'txtPorcentaje
        '
        Me.txtPorcentaje.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPorcentaje.Color = System.Drawing.Color.Empty
        Me.txtPorcentaje.Indicaciones = Nothing
        Me.txtPorcentaje.Location = New System.Drawing.Point(632, 157)
        Me.txtPorcentaje.Multilinea = False
        Me.txtPorcentaje.Name = "txtPorcentaje"
        Me.txtPorcentaje.Size = New System.Drawing.Size(90, 21)
        Me.txtPorcentaje.SoloLectura = False
        Me.txtPorcentaje.TabIndex = 12
        Me.txtPorcentaje.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPorcentaje.Texto = ""
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(363, 217)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(359, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 17
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxMotivoAcuerdo
        '
        Me.cbxMotivoAcuerdo.CampoWhere = Nothing
        Me.cbxMotivoAcuerdo.CargarUnaSolaVez = False
        Me.cbxMotivoAcuerdo.DataDisplayMember = Nothing
        Me.cbxMotivoAcuerdo.DataFilter = Nothing
        Me.cbxMotivoAcuerdo.DataOrderBy = Nothing
        Me.cbxMotivoAcuerdo.DataSource = Nothing
        Me.cbxMotivoAcuerdo.DataValueMember = Nothing
        Me.cbxMotivoAcuerdo.dtSeleccionado = Nothing
        Me.cbxMotivoAcuerdo.FormABM = Nothing
        Me.cbxMotivoAcuerdo.Indicaciones = Nothing
        Me.cbxMotivoAcuerdo.Location = New System.Drawing.Point(117, 217)
        Me.cbxMotivoAcuerdo.Name = "cbxMotivoAcuerdo"
        Me.cbxMotivoAcuerdo.SeleccionMultiple = False
        Me.cbxMotivoAcuerdo.SeleccionObligatoria = True
        Me.cbxMotivoAcuerdo.Size = New System.Drawing.Size(150, 21)
        Me.cbxMotivoAcuerdo.SoloLectura = False
        Me.cbxMotivoAcuerdo.TabIndex = 16
        Me.cbxMotivoAcuerdo.Texto = ""
        '
        'txtVigenciaHasta
        '
        Me.txtVigenciaHasta.AñoFecha = 0
        Me.txtVigenciaHasta.Color = System.Drawing.Color.Empty
        Me.txtVigenciaHasta.Fecha = New Date(2013, 2, 6, 9, 33, 15, 531)
        Me.txtVigenciaHasta.Location = New System.Drawing.Point(448, 172)
        Me.txtVigenciaHasta.MesFecha = 0
        Me.txtVigenciaHasta.Name = "txtVigenciaHasta"
        Me.txtVigenciaHasta.PermitirNulo = False
        Me.txtVigenciaHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtVigenciaHasta.SoloLectura = False
        Me.txtVigenciaHasta.TabIndex = 10
        '
        'txtVigenciaDesde
        '
        Me.txtVigenciaDesde.AñoFecha = 0
        Me.txtVigenciaDesde.Color = System.Drawing.Color.Empty
        Me.txtVigenciaDesde.Fecha = New Date(2013, 2, 6, 9, 33, 15, 531)
        Me.txtVigenciaDesde.Location = New System.Drawing.Point(338, 172)
        Me.txtVigenciaDesde.MesFecha = 0
        Me.txtVigenciaDesde.Name = "txtVigenciaDesde"
        Me.txtVigenciaDesde.PermitirNulo = False
        Me.txtVigenciaDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtVigenciaDesde.SoloLectura = False
        Me.txtVigenciaDesde.TabIndex = 9
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(42, 59)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(88, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 44
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Enabled = False
        Me.txtFecha.Fecha = New Date(2013, 2, 6, 9, 33, 15, 531)
        Me.txtFecha.Location = New System.Drawing.Point(600, 21)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 4
        '
        'txtNroAcuerdo
        '
        Me.txtNroAcuerdo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroAcuerdo.Color = System.Drawing.Color.Empty
        Me.txtNroAcuerdo.Indicaciones = Nothing
        Me.txtNroAcuerdo.Location = New System.Drawing.Point(228, 59)
        Me.txtNroAcuerdo.Multilinea = False
        Me.txtNroAcuerdo.Name = "txtNroAcuerdo"
        Me.txtNroAcuerdo.Size = New System.Drawing.Size(90, 21)
        Me.txtNroAcuerdo.SoloLectura = False
        Me.txtNroAcuerdo.TabIndex = 5
        Me.txtNroAcuerdo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroAcuerdo.Texto = ""
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 61
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.ControlCorto = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(85, 95)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(588, 24)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 6
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 80
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(85, 93)
        Me.txtProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(588, 27)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 90
        '
        'txtDireccion
        '
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Color = System.Drawing.Color.Beige
        Me.txtDireccion.Indicaciones = Nothing
        Me.txtDireccion.Location = New System.Drawing.Point(85, 126)
        Me.txtDireccion.Multilinea = False
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(584, 21)
        Me.txtDireccion.SoloLectura = True
        Me.txtDireccion.TabIndex = 7
        Me.txtDireccion.TabStop = False
        Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccion.Texto = ""
        '
        'cbxPresentacion
        '
        Me.cbxPresentacion.CampoWhere = Nothing
        Me.cbxPresentacion.CargarUnaSolaVez = False
        Me.cbxPresentacion.DataDisplayMember = Nothing
        Me.cbxPresentacion.DataFilter = Nothing
        Me.cbxPresentacion.DataOrderBy = Nothing
        Me.cbxPresentacion.DataSource = Nothing
        Me.cbxPresentacion.DataValueMember = Nothing
        Me.cbxPresentacion.dtSeleccionado = Nothing
        Me.cbxPresentacion.FormABM = Nothing
        Me.cbxPresentacion.Indicaciones = Nothing
        Me.cbxPresentacion.Location = New System.Drawing.Point(160, 344)
        Me.cbxPresentacion.Name = "cbxPresentacion"
        Me.cbxPresentacion.SeleccionMultiple = False
        Me.cbxPresentacion.SeleccionObligatoria = True
        Me.cbxPresentacion.Size = New System.Drawing.Size(176, 21)
        Me.cbxPresentacion.SoloLectura = False
        Me.cbxPresentacion.TabIndex = 21
        Me.cbxPresentacion.Texto = ""
        '
        'cbxFamilia
        '
        Me.cbxFamilia.CampoWhere = Nothing
        Me.cbxFamilia.CargarUnaSolaVez = False
        Me.cbxFamilia.DataDisplayMember = Nothing
        Me.cbxFamilia.DataFilter = Nothing
        Me.cbxFamilia.DataOrderBy = Nothing
        Me.cbxFamilia.DataSource = Nothing
        Me.cbxFamilia.DataValueMember = Nothing
        Me.cbxFamilia.dtSeleccionado = Nothing
        Me.cbxFamilia.FormABM = Nothing
        Me.cbxFamilia.Indicaciones = Nothing
        Me.cbxFamilia.Location = New System.Drawing.Point(161, 371)
        Me.cbxFamilia.Name = "cbxFamilia"
        Me.cbxFamilia.SeleccionMultiple = False
        Me.cbxFamilia.SeleccionObligatoria = True
        Me.cbxFamilia.Size = New System.Drawing.Size(175, 21)
        Me.cbxFamilia.SoloLectura = False
        Me.cbxFamilia.TabIndex = 23
        Me.cbxFamilia.Texto = ""
        '
        'txtpre
        '
        Me.txtpre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtpre.Color = System.Drawing.Color.Empty
        Me.txtpre.Indicaciones = Nothing
        Me.txtpre.Location = New System.Drawing.Point(161, 344)
        Me.txtpre.Multilinea = False
        Me.txtpre.Name = "txtpre"
        Me.txtpre.Size = New System.Drawing.Size(175, 21)
        Me.txtpre.SoloLectura = True
        Me.txtpre.TabIndex = 98
        Me.txtpre.TabStop = False
        Me.txtpre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtpre.Texto = ""
        Me.txtpre.Visible = False
        '
        'txtf
        '
        Me.txtf.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtf.Color = System.Drawing.Color.Empty
        Me.txtf.Indicaciones = Nothing
        Me.txtf.Location = New System.Drawing.Point(161, 371)
        Me.txtf.Multilinea = False
        Me.txtf.Name = "txtf"
        Me.txtf.Size = New System.Drawing.Size(175, 21)
        Me.txtf.SoloLectura = True
        Me.txtf.TabIndex = 99
        Me.txtf.TabStop = False
        Me.txtf.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtf.Texto = ""
        Me.txtf.Visible = False
        '
        'frmAcuerdodeClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(746, 483)
        Me.Controls.Add(Me.lklEliminar)
        Me.Controls.Add(Me.lklAgregar)
        Me.Controls.Add(Me.lvlista)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.ChkPresentacion)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Controls.Add(Me.ChkTodos)
        Me.Controls.Add(Me.ChkFamilia)
        Me.Controls.Add(Me.ChkProducto)
        Me.Controls.Add(Me.ChkImporte)
        Me.Controls.Add(Me.ChkPorcentaje)
        Me.Controls.Add(Me.rbFactura)
        Me.Controls.Add(Me.rbNC)
        Me.Controls.Add(Me.txtImporte)
        Me.Controls.Add(Me.txtPorcentaje)
        Me.Controls.Add(Me.chkTSucursales)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.txtObservacion)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.cbxMotivoAcuerdo)
        Me.Controls.Add(Me.lblMotivoAcuerdo)
        Me.Controls.Add(Me.lblHasta)
        Me.Controls.Add(Me.lblDesde)
        Me.Controls.Add(Me.txtVigenciaHasta)
        Me.Controls.Add(Me.lblVigencia)
        Me.Controls.Add(Me.txtVigenciaDesde)
        Me.Controls.Add(Me.lblNroAcuerdo)
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.txtFecha)
        Me.Controls.Add(Me.txtNroAcuerdo)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.lblDireccion)
        Me.Controls.Add(Me.txtCliente)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.gp2)
        Me.Controls.Add(Me.gp1)
        Me.Controls.Add(Me.txtProveedor)
        Me.Controls.Add(Me.lblProveedor)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.cbxPresentacion)
        Me.Controls.Add(Me.cbxFamilia)
        Me.Controls.Add(Me.txtpre)
        Me.Controls.Add(Me.txtf)
        Me.Name = "frmAcuerdodeClientes"
        Me.Tag = "frmAcuerdodeClientes"
        Me.Text = "frmAcuerdodeClientes"
        Me.gp2.ResumeLayout(False)
        Me.gp2.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCliente As Label
    Friend WithEvents lblDireccion As Label
    Friend WithEvents txtDireccion As ocxTXTString
    Friend WithEvents txtVigenciaHasta As ocxTXTDate
    Friend WithEvents lblVigencia As Label
    Friend WithEvents txtVigenciaDesde As ocxTXTDate
    Friend WithEvents lblNroAcuerdo As Label
    Friend WithEvents lblID As Label
    Friend WithEvents lblFecha As Label
    Friend WithEvents txtID As ocxTXTNumeric
    Friend WithEvents txtFecha As ocxTXTDate
    Friend WithEvents txtNroAcuerdo As ocxTXTString
    Friend WithEvents btnSalir As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents lblDesde As Label
    Friend WithEvents lblHasta As Label
    Friend WithEvents lblMotivoAcuerdo As Label
    Friend WithEvents cbxMotivoAcuerdo As ocxCBX
    Friend WithEvents btnNuevo As Button
    Friend WithEvents txtObservacion As ocxTXTString
    Friend WithEvents rbDesactivado As RadioButton
    Friend WithEvents rbActivado As RadioButton
    Friend WithEvents lblEstado As Label
    Friend WithEvents btnEditar As Button
    Friend WithEvents chkTSucursales As CheckBox
    Friend WithEvents txtPorcentaje As ocxTXTString
    Friend WithEvents txtImporte As ocxTXTString
    Friend WithEvents rbNC As RadioButton
    Friend WithEvents rbFactura As RadioButton
    Friend WithEvents ChkPorcentaje As CheckBox
    Friend WithEvents gp2 As GroupBox
    Friend WithEvents gp1 As GroupBox
    Friend WithEvents ChkImporte As CheckBox
    Friend WithEvents ChkProducto As CheckBox
    Friend WithEvents ChkFamilia As CheckBox
    Friend WithEvents ChkTodos As CheckBox
    Friend WithEvents lblObservaciones As Label
    Friend WithEvents cbxFamilia As ocxCBX
    Friend WithEvents cbxPresentacion As ocxCBX
    Friend WithEvents ChkPresentacion As CheckBox
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents VerDetalleToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExportarAExcelToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents flpRegistradoPor As FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As Label
    Friend WithEvents lblUsuarioRegistro As Label
    Friend WithEvents lblFechaRegistro As Label
    Friend WithEvents lblProveedor As Label
    Friend WithEvents lvlista As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents lklAgregar As LinkLabel
    Friend WithEvents lklEliminar As LinkLabel
    Friend WithEvents txtpre As ocxTXTString
    Friend WithEvents txtf As ocxTXTString
    Public WithEvents txtCliente As ocxTXTCliente
    Public WithEvents txtProveedor As ocxTXTProveedor
    Friend WithEvents ColumnHeader4 As ColumnHeader
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransporte
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblChofer = New System.Windows.Forms.Label()
        Me.lblCamion = New System.Windows.Forms.Label()
        Me.lblDistribuidor = New System.Windows.Forms.Label()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.cbxDistribuidor = New ERP.ocxCBX()
        Me.cbxCamion = New ERP.ocxCBX()
        Me.cbxChofer = New ERP.ocxCBX()
        Me.cbxZona = New ERP.ocxCBX()
        Me.lblZonaVenta = New System.Windows.Forms.Label()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblChofer
        '
        Me.lblChofer.AutoSize = True
        Me.lblChofer.Location = New System.Drawing.Point(12, 140)
        Me.lblChofer.Name = "lblChofer"
        Me.lblChofer.Size = New System.Drawing.Size(41, 13)
        Me.lblChofer.TabIndex = 10
        Me.lblChofer.Text = "Chofer:"
        '
        'lblCamion
        '
        Me.lblCamion.AutoSize = True
        Me.lblCamion.Location = New System.Drawing.Point(12, 114)
        Me.lblCamion.Name = "lblCamion"
        Me.lblCamion.Size = New System.Drawing.Size(45, 13)
        Me.lblCamion.TabIndex = 8
        Me.lblCamion.Text = "Camion:"
        '
        'lblDistribuidor
        '
        Me.lblDistribuidor.AutoSize = True
        Me.lblDistribuidor.Location = New System.Drawing.Point(12, 88)
        Me.lblDistribuidor.Name = "lblDistribuidor"
        Me.lblDistribuidor.Size = New System.Drawing.Size(62, 13)
        Me.lblDistribuidor.TabIndex = 6
        Me.lblDistribuidor.Text = "Dsitribuidor:"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(98, 31)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(237, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(98, 4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 1
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 394)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(509, 22)
        Me.StatusStrip1.TabIndex = 19
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(44, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(422, 360)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 18
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(341, 166)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 15
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(422, 166)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 16
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(260, 166)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 14
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(179, 166)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 13
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(98, 166)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 12
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lvLista
        '
        Me.lvLista.Location = New System.Drawing.Point(98, 195)
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(399, 159)
        Me.lvLista.TabIndex = 17
        Me.lvLista.UseCompatibleStateImageBehavior = False
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(12, 35)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 2
        Me.lblDescripcion.Text = "Descripcion:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(12, 9)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'cbxDistribuidor
        '
        Me.cbxDistribuidor.CampoWhere = Nothing
        Me.cbxDistribuidor.DataDisplayMember = Nothing
        Me.cbxDistribuidor.DataFilter = Nothing
        Me.cbxDistribuidor.DataOrderBy = Nothing
        Me.cbxDistribuidor.DataSource = Nothing
        Me.cbxDistribuidor.DataValueMember = Nothing
        Me.cbxDistribuidor.FormABM = Nothing
        Me.cbxDistribuidor.Indicaciones = Nothing
        Me.cbxDistribuidor.Location = New System.Drawing.Point(98, 85)
        Me.cbxDistribuidor.Name = "cbxDistribuidor"
        Me.cbxDistribuidor.SeleccionObligatoria = False
        Me.cbxDistribuidor.Size = New System.Drawing.Size(237, 21)
        Me.cbxDistribuidor.SoloLectura = False
        Me.cbxDistribuidor.TabIndex = 7
        Me.cbxDistribuidor.Texto = ""
        '
        'cbxCamion
        '
        Me.cbxCamion.CampoWhere = Nothing
        Me.cbxCamion.DataDisplayMember = Nothing
        Me.cbxCamion.DataFilter = Nothing
        Me.cbxCamion.DataOrderBy = Nothing
        Me.cbxCamion.DataSource = Nothing
        Me.cbxCamion.DataValueMember = Nothing
        Me.cbxCamion.FormABM = Nothing
        Me.cbxCamion.Indicaciones = Nothing
        Me.cbxCamion.Location = New System.Drawing.Point(98, 112)
        Me.cbxCamion.Name = "cbxCamion"
        Me.cbxCamion.SeleccionObligatoria = False
        Me.cbxCamion.Size = New System.Drawing.Size(237, 21)
        Me.cbxCamion.SoloLectura = False
        Me.cbxCamion.TabIndex = 9
        Me.cbxCamion.Texto = ""
        '
        'cbxChofer
        '
        Me.cbxChofer.CampoWhere = Nothing
        Me.cbxChofer.DataDisplayMember = Nothing
        Me.cbxChofer.DataFilter = Nothing
        Me.cbxChofer.DataOrderBy = Nothing
        Me.cbxChofer.DataSource = Nothing
        Me.cbxChofer.DataValueMember = Nothing
        Me.cbxChofer.FormABM = Nothing
        Me.cbxChofer.Indicaciones = Nothing
        Me.cbxChofer.Location = New System.Drawing.Point(98, 139)
        Me.cbxChofer.Name = "cbxChofer"
        Me.cbxChofer.SeleccionObligatoria = False
        Me.cbxChofer.Size = New System.Drawing.Size(237, 21)
        Me.cbxChofer.SoloLectura = False
        Me.cbxChofer.TabIndex = 11
        Me.cbxChofer.Texto = ""
        '
        'cbxZona
        '
        Me.cbxZona.CampoWhere = Nothing
        Me.cbxZona.DataDisplayMember = Nothing
        Me.cbxZona.DataFilter = Nothing
        Me.cbxZona.DataOrderBy = Nothing
        Me.cbxZona.DataSource = Nothing
        Me.cbxZona.DataValueMember = Nothing
        Me.cbxZona.FormABM = Nothing
        Me.cbxZona.Indicaciones = Nothing
        Me.cbxZona.Location = New System.Drawing.Point(98, 58)
        Me.cbxZona.Name = "cbxZona"
        Me.cbxZona.SeleccionObligatoria = False
        Me.cbxZona.Size = New System.Drawing.Size(237, 21)
        Me.cbxZona.SoloLectura = False
        Me.cbxZona.TabIndex = 5
        Me.cbxZona.Texto = ""
        '
        'lblZonaVenta
        '
        Me.lblZonaVenta.AutoSize = True
        Me.lblZonaVenta.Location = New System.Drawing.Point(12, 61)
        Me.lblZonaVenta.Name = "lblZonaVenta"
        Me.lblZonaVenta.Size = New System.Drawing.Size(35, 13)
        Me.lblZonaVenta.TabIndex = 4
        Me.lblZonaVenta.Text = "Zona:"
        '
        'frmTransporte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(509, 416)
        Me.Controls.Add(Me.cbxZona)
        Me.Controls.Add(Me.lblZonaVenta)
        Me.Controls.Add(Me.cbxChofer)
        Me.Controls.Add(Me.cbxCamion)
        Me.Controls.Add(Me.cbxDistribuidor)
        Me.Controls.Add(Me.lblChofer)
        Me.Controls.Add(Me.lblCamion)
        Me.Controls.Add(Me.lblDistribuidor)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.lvLista)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.lblID)
        Me.Name = "frmTransporte"
        Me.Tag = "TRANSPORTE"
        Me.Text = "frmTransporte"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblChofer As System.Windows.Forms.Label
    Friend WithEvents lblCamion As System.Windows.Forms.Label
    Friend WithEvents lblDistribuidor As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lvLista As System.Windows.Forms.ListView
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents cbxChofer As ERP.ocxCBX
    Friend WithEvents cbxCamion As ERP.ocxCBX
    Friend WithEvents cbxDistribuidor As ERP.ocxCBX
    Friend WithEvents cbxZona As ERP.ocxCBX
    Friend WithEvents lblZonaVenta As System.Windows.Forms.Label
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPaisDepartamentoCiudadBarrios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.tbcGeneral = New System.Windows.Forms.TabControl()
        Me.tpPaises = New System.Windows.Forms.TabPage()
        Me.rdbPaisDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbPaisActivo = New System.Windows.Forms.RadioButton()
        Me.lblPaisEstado = New System.Windows.Forms.Label()
        Me.btnPaisCancelar = New System.Windows.Forms.Button()
        Me.txtPaisNacionalidad = New System.Windows.Forms.TextBox()
        Me.lblPaisNacionalidad = New System.Windows.Forms.Label()
        Me.btnPaisEliminar = New System.Windows.Forms.Button()
        Me.btnPaisGuardar = New System.Windows.Forms.Button()
        Me.btnPaisEditar = New System.Windows.Forms.Button()
        Me.btnPaisNuevo = New System.Windows.Forms.Button()
        Me.lvPaisLista = New System.Windows.Forms.ListView()
        Me.txtPaisDescripcion = New System.Windows.Forms.TextBox()
        Me.lblPaisDescripcion = New System.Windows.Forms.Label()
        Me.txtPaisID = New System.Windows.Forms.TextBox()
        Me.lblPaisID = New System.Windows.Forms.Label()
        Me.tpDepartamentos = New System.Windows.Forms.TabPage()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.btnDepartamentoCancelar = New System.Windows.Forms.Button()
        Me.btnDepartamentoEliminar = New System.Windows.Forms.Button()
        Me.cbxDepartamentoPais = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.rdbDepartamentoDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbDepartamentoActivo = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnDepartamentoGuardar = New System.Windows.Forms.Button()
        Me.btnDepartamentoEditar = New System.Windows.Forms.Button()
        Me.btnDepartamentoNuevo = New System.Windows.Forms.Button()
        Me.lvDepartamentos = New System.Windows.Forms.ListView()
        Me.txtDepartamentoDescripcion = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtDepartamentoID = New System.Windows.Forms.TextBox()
        Me.lblDepartamentoID = New System.Windows.Forms.Label()
        Me.tpCiudades = New System.Windows.Forms.TabPage()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblCiudadCodigo = New System.Windows.Forms.Label()
        Me.lblDepartamentoCiudad = New System.Windows.Forms.Label()
        Me.btnCiudadCancelar = New System.Windows.Forms.Button()
        Me.btnCiudadEliminar = New System.Windows.Forms.Button()
        Me.lblPaisCiudad = New System.Windows.Forms.Label()
        Me.rdbCiudadDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbCiudadActivo = New System.Windows.Forms.RadioButton()
        Me.lblCiudadEstado = New System.Windows.Forms.Label()
        Me.btnCiudadGuardar = New System.Windows.Forms.Button()
        Me.btnCiudadEditar = New System.Windows.Forms.Button()
        Me.btnCiudadNuevo = New System.Windows.Forms.Button()
        Me.lvCiudades = New System.Windows.Forms.ListView()
        Me.lblCiudadDescripcion = New System.Windows.Forms.Label()
        Me.txtCiudadID = New System.Windows.Forms.TextBox()
        Me.lblCiudadID = New System.Windows.Forms.Label()
        Me.tpBarrios = New System.Windows.Forms.TabPage()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnBarrioCancelar = New System.Windows.Forms.Button()
        Me.btnBarrioEliminar = New System.Windows.Forms.Button()
        Me.rdbBarrioDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbBarrioActivo = New System.Windows.Forms.RadioButton()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnBarrioGuardar = New System.Windows.Forms.Button()
        Me.btnBarrioEditar = New System.Windows.Forms.Button()
        Me.btnBarrioNuevo = New System.Windows.Forms.Button()
        Me.lvBarrio = New System.Windows.Forms.ListView()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtBarrioID = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.txtCodigoSet = New ERP.ocxTXTString()
        Me.txtCiudadCodigo = New ERP.ocxTXTString()
        Me.txtCiudadDescripcion = New ERP.ocxTXTString()
        Me.cbxCiudadDepartamento = New ERP.ocxCBX()
        Me.cbxCiudadPais = New ERP.ocxCBX()
        Me.txtCodigoBarrio = New ERP.ocxTXTString()
        Me.txtBarrioDescripcion = New ERP.ocxTXTString()
        Me.cbxPais = New ERP.ocxCBX()
        Me.cbxDepartamento = New ERP.ocxCBX()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.tbcGeneral.SuspendLayout()
        Me.tpPaises.SuspendLayout()
        Me.tpDepartamentos.SuspendLayout()
        Me.tpCiudades.SuspendLayout()
        Me.tpBarrios.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbcGeneral
        '
        Me.tbcGeneral.Controls.Add(Me.tpPaises)
        Me.tbcGeneral.Controls.Add(Me.tpDepartamentos)
        Me.tbcGeneral.Controls.Add(Me.tpCiudades)
        Me.tbcGeneral.Controls.Add(Me.tpBarrios)
        Me.tbcGeneral.Location = New System.Drawing.Point(0, 6)
        Me.tbcGeneral.Name = "tbcGeneral"
        Me.tbcGeneral.SelectedIndex = 0
        Me.tbcGeneral.Size = New System.Drawing.Size(522, 378)
        Me.tbcGeneral.TabIndex = 0
        '
        'tpPaises
        '
        Me.tpPaises.Controls.Add(Me.rdbPaisDesactivado)
        Me.tpPaises.Controls.Add(Me.rdbPaisActivo)
        Me.tpPaises.Controls.Add(Me.lblPaisEstado)
        Me.tpPaises.Controls.Add(Me.btnPaisCancelar)
        Me.tpPaises.Controls.Add(Me.txtPaisNacionalidad)
        Me.tpPaises.Controls.Add(Me.lblPaisNacionalidad)
        Me.tpPaises.Controls.Add(Me.btnPaisEliminar)
        Me.tpPaises.Controls.Add(Me.btnPaisGuardar)
        Me.tpPaises.Controls.Add(Me.btnPaisEditar)
        Me.tpPaises.Controls.Add(Me.btnPaisNuevo)
        Me.tpPaises.Controls.Add(Me.lvPaisLista)
        Me.tpPaises.Controls.Add(Me.txtPaisDescripcion)
        Me.tpPaises.Controls.Add(Me.lblPaisDescripcion)
        Me.tpPaises.Controls.Add(Me.txtPaisID)
        Me.tpPaises.Controls.Add(Me.lblPaisID)
        Me.tpPaises.Location = New System.Drawing.Point(4, 22)
        Me.tpPaises.Name = "tpPaises"
        Me.tpPaises.Padding = New System.Windows.Forms.Padding(3)
        Me.tpPaises.Size = New System.Drawing.Size(514, 352)
        Me.tpPaises.TabIndex = 0
        Me.tpPaises.Text = "Paises"
        Me.tpPaises.UseVisualStyleBackColor = True
        '
        'rdbPaisDesactivado
        '
        Me.rdbPaisDesactivado.AutoSize = True
        Me.rdbPaisDesactivado.Location = New System.Drawing.Point(156, 90)
        Me.rdbPaisDesactivado.Name = "rdbPaisDesactivado"
        Me.rdbPaisDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbPaisDesactivado.TabIndex = 8
        Me.rdbPaisDesactivado.TabStop = True
        Me.rdbPaisDesactivado.Text = "Desactivado"
        Me.rdbPaisDesactivado.UseVisualStyleBackColor = True
        '
        'rdbPaisActivo
        '
        Me.rdbPaisActivo.AutoSize = True
        Me.rdbPaisActivo.Location = New System.Drawing.Point(95, 90)
        Me.rdbPaisActivo.Name = "rdbPaisActivo"
        Me.rdbPaisActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbPaisActivo.TabIndex = 7
        Me.rdbPaisActivo.TabStop = True
        Me.rdbPaisActivo.Text = "Activo"
        Me.rdbPaisActivo.UseVisualStyleBackColor = True
        '
        'lblPaisEstado
        '
        Me.lblPaisEstado.AutoSize = True
        Me.lblPaisEstado.Location = New System.Drawing.Point(17, 92)
        Me.lblPaisEstado.Name = "lblPaisEstado"
        Me.lblPaisEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblPaisEstado.TabIndex = 6
        Me.lblPaisEstado.Text = "Estado:"
        '
        'btnPaisCancelar
        '
        Me.btnPaisCancelar.Location = New System.Drawing.Point(338, 124)
        Me.btnPaisCancelar.Name = "btnPaisCancelar"
        Me.btnPaisCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnPaisCancelar.TabIndex = 12
        Me.btnPaisCancelar.Text = "&Cancelar"
        Me.btnPaisCancelar.UseVisualStyleBackColor = True
        '
        'txtPaisNacionalidad
        '
        Me.txtPaisNacionalidad.BackColor = System.Drawing.Color.White
        Me.txtPaisNacionalidad.Location = New System.Drawing.Point(95, 64)
        Me.txtPaisNacionalidad.Name = "txtPaisNacionalidad"
        Me.txtPaisNacionalidad.Size = New System.Drawing.Size(187, 20)
        Me.txtPaisNacionalidad.TabIndex = 5
        '
        'lblPaisNacionalidad
        '
        Me.lblPaisNacionalidad.AutoSize = True
        Me.lblPaisNacionalidad.Location = New System.Drawing.Point(17, 67)
        Me.lblPaisNacionalidad.Name = "lblPaisNacionalidad"
        Me.lblPaisNacionalidad.Size = New System.Drawing.Size(72, 13)
        Me.lblPaisNacionalidad.TabIndex = 4
        Me.lblPaisNacionalidad.Text = "Nacionalidad:"
        '
        'btnPaisEliminar
        '
        Me.btnPaisEliminar.Location = New System.Drawing.Point(419, 124)
        Me.btnPaisEliminar.Name = "btnPaisEliminar"
        Me.btnPaisEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnPaisEliminar.TabIndex = 13
        Me.btnPaisEliminar.Text = "E&liminar"
        Me.btnPaisEliminar.UseVisualStyleBackColor = True
        '
        'btnPaisGuardar
        '
        Me.btnPaisGuardar.Location = New System.Drawing.Point(257, 124)
        Me.btnPaisGuardar.Name = "btnPaisGuardar"
        Me.btnPaisGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnPaisGuardar.TabIndex = 11
        Me.btnPaisGuardar.Text = "&Guardar"
        Me.btnPaisGuardar.UseVisualStyleBackColor = True
        '
        'btnPaisEditar
        '
        Me.btnPaisEditar.Location = New System.Drawing.Point(176, 124)
        Me.btnPaisEditar.Name = "btnPaisEditar"
        Me.btnPaisEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnPaisEditar.TabIndex = 10
        Me.btnPaisEditar.Text = "&Editar"
        Me.btnPaisEditar.UseVisualStyleBackColor = True
        '
        'btnPaisNuevo
        '
        Me.btnPaisNuevo.Location = New System.Drawing.Point(95, 124)
        Me.btnPaisNuevo.Name = "btnPaisNuevo"
        Me.btnPaisNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnPaisNuevo.TabIndex = 9
        Me.btnPaisNuevo.Text = "&Nuevo"
        Me.btnPaisNuevo.UseVisualStyleBackColor = True
        '
        'lvPaisLista
        '
        Me.lvPaisLista.Location = New System.Drawing.Point(95, 153)
        Me.lvPaisLista.Name = "lvPaisLista"
        Me.lvPaisLista.Size = New System.Drawing.Size(399, 188)
        Me.lvPaisLista.TabIndex = 14
        Me.lvPaisLista.UseCompatibleStateImageBehavior = False
        '
        'txtPaisDescripcion
        '
        Me.txtPaisDescripcion.BackColor = System.Drawing.Color.White
        Me.txtPaisDescripcion.Location = New System.Drawing.Point(95, 38)
        Me.txtPaisDescripcion.Name = "txtPaisDescripcion"
        Me.txtPaisDescripcion.Size = New System.Drawing.Size(187, 20)
        Me.txtPaisDescripcion.TabIndex = 3
        '
        'lblPaisDescripcion
        '
        Me.lblPaisDescripcion.AutoSize = True
        Me.lblPaisDescripcion.Location = New System.Drawing.Point(17, 42)
        Me.lblPaisDescripcion.Name = "lblPaisDescripcion"
        Me.lblPaisDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblPaisDescripcion.TabIndex = 2
        Me.lblPaisDescripcion.Text = "Descripción:"
        '
        'txtPaisID
        '
        Me.txtPaisID.BackColor = System.Drawing.Color.White
        Me.txtPaisID.Location = New System.Drawing.Point(95, 12)
        Me.txtPaisID.Name = "txtPaisID"
        Me.txtPaisID.ReadOnly = True
        Me.txtPaisID.Size = New System.Drawing.Size(61, 20)
        Me.txtPaisID.TabIndex = 1
        '
        'lblPaisID
        '
        Me.lblPaisID.AutoSize = True
        Me.lblPaisID.Location = New System.Drawing.Point(17, 15)
        Me.lblPaisID.Name = "lblPaisID"
        Me.lblPaisID.Size = New System.Drawing.Size(21, 13)
        Me.lblPaisID.TabIndex = 0
        Me.lblPaisID.Text = "ID:"
        '
        'tpDepartamentos
        '
        Me.tpDepartamentos.Controls.Add(Me.Label4)
        Me.tpDepartamentos.Controls.Add(Me.txtCodigo)
        Me.tpDepartamentos.Controls.Add(Me.btnDepartamentoCancelar)
        Me.tpDepartamentos.Controls.Add(Me.btnDepartamentoEliminar)
        Me.tpDepartamentos.Controls.Add(Me.cbxDepartamentoPais)
        Me.tpDepartamentos.Controls.Add(Me.Label2)
        Me.tpDepartamentos.Controls.Add(Me.rdbDepartamentoDesactivado)
        Me.tpDepartamentos.Controls.Add(Me.rdbDepartamentoActivo)
        Me.tpDepartamentos.Controls.Add(Me.Label1)
        Me.tpDepartamentos.Controls.Add(Me.btnDepartamentoGuardar)
        Me.tpDepartamentos.Controls.Add(Me.btnDepartamentoEditar)
        Me.tpDepartamentos.Controls.Add(Me.btnDepartamentoNuevo)
        Me.tpDepartamentos.Controls.Add(Me.lvDepartamentos)
        Me.tpDepartamentos.Controls.Add(Me.txtDepartamentoDescripcion)
        Me.tpDepartamentos.Controls.Add(Me.Label3)
        Me.tpDepartamentos.Controls.Add(Me.txtDepartamentoID)
        Me.tpDepartamentos.Controls.Add(Me.lblDepartamentoID)
        Me.tpDepartamentos.Location = New System.Drawing.Point(4, 22)
        Me.tpDepartamentos.Name = "tpDepartamentos"
        Me.tpDepartamentos.Padding = New System.Windows.Forms.Padding(3)
        Me.tpDepartamentos.Size = New System.Drawing.Size(514, 352)
        Me.tpDepartamentos.TabIndex = 1
        Me.tpDepartamentos.Text = "Departamentos"
        Me.tpDepartamentos.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(17, 93)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 13)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Codigo SET:"
        '
        'txtCodigo
        '
        Me.txtCodigo.BackColor = System.Drawing.Color.White
        Me.txtCodigo.Location = New System.Drawing.Point(95, 90)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(61, 20)
        Me.txtCodigo.TabIndex = 15
        '
        'btnDepartamentoCancelar
        '
        Me.btnDepartamentoCancelar.Location = New System.Drawing.Point(338, 141)
        Me.btnDepartamentoCancelar.Name = "btnDepartamentoCancelar"
        Me.btnDepartamentoCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnDepartamentoCancelar.TabIndex = 12
        Me.btnDepartamentoCancelar.Text = "&Cancelar"
        Me.btnDepartamentoCancelar.UseVisualStyleBackColor = True
        '
        'btnDepartamentoEliminar
        '
        Me.btnDepartamentoEliminar.Location = New System.Drawing.Point(419, 141)
        Me.btnDepartamentoEliminar.Name = "btnDepartamentoEliminar"
        Me.btnDepartamentoEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnDepartamentoEliminar.TabIndex = 13
        Me.btnDepartamentoEliminar.Text = "E&liminar"
        Me.btnDepartamentoEliminar.UseVisualStyleBackColor = True
        '
        'cbxDepartamentoPais
        '
        Me.cbxDepartamentoPais.BackColor = System.Drawing.Color.White
        Me.cbxDepartamentoPais.FormattingEnabled = True
        Me.cbxDepartamentoPais.Location = New System.Drawing.Point(95, 37)
        Me.cbxDepartamentoPais.Name = "cbxDepartamentoPais"
        Me.cbxDepartamentoPais.Size = New System.Drawing.Size(187, 21)
        Me.cbxDepartamentoPais.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(30, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Pais:"
        '
        'rdbDepartamentoDesactivado
        '
        Me.rdbDepartamentoDesactivado.AutoSize = True
        Me.rdbDepartamentoDesactivado.Location = New System.Drawing.Point(156, 116)
        Me.rdbDepartamentoDesactivado.Name = "rdbDepartamentoDesactivado"
        Me.rdbDepartamentoDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDepartamentoDesactivado.TabIndex = 8
        Me.rdbDepartamentoDesactivado.TabStop = True
        Me.rdbDepartamentoDesactivado.Text = "Desactivado"
        Me.rdbDepartamentoDesactivado.UseVisualStyleBackColor = True
        '
        'rdbDepartamentoActivo
        '
        Me.rdbDepartamentoActivo.AutoSize = True
        Me.rdbDepartamentoActivo.Location = New System.Drawing.Point(95, 116)
        Me.rdbDepartamentoActivo.Name = "rdbDepartamentoActivo"
        Me.rdbDepartamentoActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbDepartamentoActivo.TabIndex = 7
        Me.rdbDepartamentoActivo.TabStop = True
        Me.rdbDepartamentoActivo.Text = "Activo"
        Me.rdbDepartamentoActivo.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 118)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Estado:"
        '
        'btnDepartamentoGuardar
        '
        Me.btnDepartamentoGuardar.Location = New System.Drawing.Point(257, 141)
        Me.btnDepartamentoGuardar.Name = "btnDepartamentoGuardar"
        Me.btnDepartamentoGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnDepartamentoGuardar.TabIndex = 11
        Me.btnDepartamentoGuardar.Text = "&Guardar"
        Me.btnDepartamentoGuardar.UseVisualStyleBackColor = True
        '
        'btnDepartamentoEditar
        '
        Me.btnDepartamentoEditar.Location = New System.Drawing.Point(176, 141)
        Me.btnDepartamentoEditar.Name = "btnDepartamentoEditar"
        Me.btnDepartamentoEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnDepartamentoEditar.TabIndex = 10
        Me.btnDepartamentoEditar.Text = "&Editar"
        Me.btnDepartamentoEditar.UseVisualStyleBackColor = True
        '
        'btnDepartamentoNuevo
        '
        Me.btnDepartamentoNuevo.Location = New System.Drawing.Point(95, 141)
        Me.btnDepartamentoNuevo.Name = "btnDepartamentoNuevo"
        Me.btnDepartamentoNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnDepartamentoNuevo.TabIndex = 9
        Me.btnDepartamentoNuevo.Text = "&Nuevo"
        Me.btnDepartamentoNuevo.UseVisualStyleBackColor = True
        '
        'lvDepartamentos
        '
        Me.lvDepartamentos.Location = New System.Drawing.Point(95, 170)
        Me.lvDepartamentos.Name = "lvDepartamentos"
        Me.lvDepartamentos.Size = New System.Drawing.Size(399, 171)
        Me.lvDepartamentos.TabIndex = 14
        Me.lvDepartamentos.UseCompatibleStateImageBehavior = False
        '
        'txtDepartamentoDescripcion
        '
        Me.txtDepartamentoDescripcion.BackColor = System.Drawing.Color.White
        Me.txtDepartamentoDescripcion.Location = New System.Drawing.Point(95, 64)
        Me.txtDepartamentoDescripcion.Name = "txtDepartamentoDescripcion"
        Me.txtDepartamentoDescripcion.Size = New System.Drawing.Size(187, 20)
        Me.txtDepartamentoDescripcion.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(17, 67)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Descripción:"
        '
        'txtDepartamentoID
        '
        Me.txtDepartamentoID.BackColor = System.Drawing.Color.White
        Me.txtDepartamentoID.Location = New System.Drawing.Point(95, 12)
        Me.txtDepartamentoID.Name = "txtDepartamentoID"
        Me.txtDepartamentoID.ReadOnly = True
        Me.txtDepartamentoID.Size = New System.Drawing.Size(61, 20)
        Me.txtDepartamentoID.TabIndex = 1
        '
        'lblDepartamentoID
        '
        Me.lblDepartamentoID.AutoSize = True
        Me.lblDepartamentoID.Location = New System.Drawing.Point(17, 16)
        Me.lblDepartamentoID.Name = "lblDepartamentoID"
        Me.lblDepartamentoID.Size = New System.Drawing.Size(21, 13)
        Me.lblDepartamentoID.TabIndex = 0
        Me.lblDepartamentoID.Text = "ID:"
        '
        'tpCiudades
        '
        Me.tpCiudades.Controls.Add(Me.Label12)
        Me.tpCiudades.Controls.Add(Me.lblCiudadCodigo)
        Me.tpCiudades.Controls.Add(Me.lblDepartamentoCiudad)
        Me.tpCiudades.Controls.Add(Me.btnCiudadCancelar)
        Me.tpCiudades.Controls.Add(Me.btnCiudadEliminar)
        Me.tpCiudades.Controls.Add(Me.lblPaisCiudad)
        Me.tpCiudades.Controls.Add(Me.rdbCiudadDesactivado)
        Me.tpCiudades.Controls.Add(Me.rdbCiudadActivo)
        Me.tpCiudades.Controls.Add(Me.lblCiudadEstado)
        Me.tpCiudades.Controls.Add(Me.btnCiudadGuardar)
        Me.tpCiudades.Controls.Add(Me.btnCiudadEditar)
        Me.tpCiudades.Controls.Add(Me.btnCiudadNuevo)
        Me.tpCiudades.Controls.Add(Me.txtCodigoSet)
        Me.tpCiudades.Controls.Add(Me.txtCiudadCodigo)
        Me.tpCiudades.Controls.Add(Me.txtCiudadDescripcion)
        Me.tpCiudades.Controls.Add(Me.cbxCiudadDepartamento)
        Me.tpCiudades.Controls.Add(Me.cbxCiudadPais)
        Me.tpCiudades.Controls.Add(Me.lvCiudades)
        Me.tpCiudades.Controls.Add(Me.lblCiudadDescripcion)
        Me.tpCiudades.Controls.Add(Me.txtCiudadID)
        Me.tpCiudades.Controls.Add(Me.lblCiudadID)
        Me.tpCiudades.Location = New System.Drawing.Point(4, 22)
        Me.tpCiudades.Name = "tpCiudades"
        Me.tpCiudades.Size = New System.Drawing.Size(514, 352)
        Me.tpCiudades.TabIndex = 2
        Me.tpCiudades.Text = "Ciudades"
        Me.tpCiudades.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(17, 147)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(67, 13)
        Me.Label12.TabIndex = 19
        Me.Label12.Text = "Codigo SET:"
        '
        'lblCiudadCodigo
        '
        Me.lblCiudadCodigo.AutoSize = True
        Me.lblCiudadCodigo.Location = New System.Drawing.Point(17, 121)
        Me.lblCiudadCodigo.Name = "lblCiudadCodigo"
        Me.lblCiudadCodigo.Size = New System.Drawing.Size(43, 13)
        Me.lblCiudadCodigo.TabIndex = 8
        Me.lblCiudadCodigo.Text = "Codigo:"
        '
        'lblDepartamentoCiudad
        '
        Me.lblDepartamentoCiudad.AutoSize = True
        Me.lblDepartamentoCiudad.Location = New System.Drawing.Point(17, 68)
        Me.lblDepartamentoCiudad.Name = "lblDepartamentoCiudad"
        Me.lblDepartamentoCiudad.Size = New System.Drawing.Size(77, 13)
        Me.lblDepartamentoCiudad.TabIndex = 4
        Me.lblDepartamentoCiudad.Text = "Departamento:"
        '
        'btnCiudadCancelar
        '
        Me.btnCiudadCancelar.Location = New System.Drawing.Point(338, 190)
        Me.btnCiudadCancelar.Name = "btnCiudadCancelar"
        Me.btnCiudadCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCiudadCancelar.TabIndex = 16
        Me.btnCiudadCancelar.Text = "&Cancelar"
        Me.btnCiudadCancelar.UseVisualStyleBackColor = True
        '
        'btnCiudadEliminar
        '
        Me.btnCiudadEliminar.Location = New System.Drawing.Point(419, 190)
        Me.btnCiudadEliminar.Name = "btnCiudadEliminar"
        Me.btnCiudadEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnCiudadEliminar.TabIndex = 17
        Me.btnCiudadEliminar.Text = "E&liminar"
        Me.btnCiudadEliminar.UseVisualStyleBackColor = True
        '
        'lblPaisCiudad
        '
        Me.lblPaisCiudad.AutoSize = True
        Me.lblPaisCiudad.Location = New System.Drawing.Point(17, 41)
        Me.lblPaisCiudad.Name = "lblPaisCiudad"
        Me.lblPaisCiudad.Size = New System.Drawing.Size(30, 13)
        Me.lblPaisCiudad.TabIndex = 2
        Me.lblPaisCiudad.Text = "Pais:"
        '
        'rdbCiudadDesactivado
        '
        Me.rdbCiudadDesactivado.AutoSize = True
        Me.rdbCiudadDesactivado.Location = New System.Drawing.Point(155, 170)
        Me.rdbCiudadDesactivado.Name = "rdbCiudadDesactivado"
        Me.rdbCiudadDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbCiudadDesactivado.TabIndex = 12
        Me.rdbCiudadDesactivado.TabStop = True
        Me.rdbCiudadDesactivado.Text = "Desactivado"
        Me.rdbCiudadDesactivado.UseVisualStyleBackColor = True
        '
        'rdbCiudadActivo
        '
        Me.rdbCiudadActivo.AutoSize = True
        Me.rdbCiudadActivo.Location = New System.Drawing.Point(94, 170)
        Me.rdbCiudadActivo.Name = "rdbCiudadActivo"
        Me.rdbCiudadActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbCiudadActivo.TabIndex = 11
        Me.rdbCiudadActivo.TabStop = True
        Me.rdbCiudadActivo.Text = "Activo"
        Me.rdbCiudadActivo.UseVisualStyleBackColor = True
        '
        'lblCiudadEstado
        '
        Me.lblCiudadEstado.AutoSize = True
        Me.lblCiudadEstado.Location = New System.Drawing.Point(16, 172)
        Me.lblCiudadEstado.Name = "lblCiudadEstado"
        Me.lblCiudadEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblCiudadEstado.TabIndex = 10
        Me.lblCiudadEstado.Text = "Estado:"
        '
        'btnCiudadGuardar
        '
        Me.btnCiudadGuardar.Location = New System.Drawing.Point(257, 190)
        Me.btnCiudadGuardar.Name = "btnCiudadGuardar"
        Me.btnCiudadGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnCiudadGuardar.TabIndex = 15
        Me.btnCiudadGuardar.Text = "&Guardar"
        Me.btnCiudadGuardar.UseVisualStyleBackColor = True
        '
        'btnCiudadEditar
        '
        Me.btnCiudadEditar.Location = New System.Drawing.Point(176, 190)
        Me.btnCiudadEditar.Name = "btnCiudadEditar"
        Me.btnCiudadEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnCiudadEditar.TabIndex = 14
        Me.btnCiudadEditar.Text = "&Editar"
        Me.btnCiudadEditar.UseVisualStyleBackColor = True
        '
        'btnCiudadNuevo
        '
        Me.btnCiudadNuevo.Location = New System.Drawing.Point(95, 190)
        Me.btnCiudadNuevo.Name = "btnCiudadNuevo"
        Me.btnCiudadNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnCiudadNuevo.TabIndex = 13
        Me.btnCiudadNuevo.Text = "&Nuevo"
        Me.btnCiudadNuevo.UseVisualStyleBackColor = True
        '
        'lvCiudades
        '
        Me.lvCiudades.Location = New System.Drawing.Point(20, 216)
        Me.lvCiudades.Name = "lvCiudades"
        Me.lvCiudades.Size = New System.Drawing.Size(474, 131)
        Me.lvCiudades.TabIndex = 18
        Me.lvCiudades.UseCompatibleStateImageBehavior = False
        '
        'lblCiudadDescripcion
        '
        Me.lblCiudadDescripcion.AutoSize = True
        Me.lblCiudadDescripcion.Location = New System.Drawing.Point(17, 94)
        Me.lblCiudadDescripcion.Name = "lblCiudadDescripcion"
        Me.lblCiudadDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblCiudadDescripcion.TabIndex = 6
        Me.lblCiudadDescripcion.Text = "Descripción:"
        '
        'txtCiudadID
        '
        Me.txtCiudadID.BackColor = System.Drawing.Color.White
        Me.txtCiudadID.Location = New System.Drawing.Point(95, 12)
        Me.txtCiudadID.Name = "txtCiudadID"
        Me.txtCiudadID.ReadOnly = True
        Me.txtCiudadID.Size = New System.Drawing.Size(61, 20)
        Me.txtCiudadID.TabIndex = 1
        '
        'lblCiudadID
        '
        Me.lblCiudadID.AutoSize = True
        Me.lblCiudadID.Location = New System.Drawing.Point(17, 16)
        Me.lblCiudadID.Name = "lblCiudadID"
        Me.lblCiudadID.Size = New System.Drawing.Size(21, 13)
        Me.lblCiudadID.TabIndex = 0
        Me.lblCiudadID.Text = "ID:"
        '
        'tpBarrios
        '
        Me.tpBarrios.Controls.Add(Me.cbxCiudad)
        Me.tpBarrios.Controls.Add(Me.cbxDepartamento)
        Me.tpBarrios.Controls.Add(Me.cbxPais)
        Me.tpBarrios.Controls.Add(Me.txtCodigoBarrio)
        Me.tpBarrios.Controls.Add(Me.Label11)
        Me.tpBarrios.Controls.Add(Me.Label10)
        Me.tpBarrios.Controls.Add(Me.Label9)
        Me.tpBarrios.Controls.Add(Me.Label5)
        Me.tpBarrios.Controls.Add(Me.btnBarrioCancelar)
        Me.tpBarrios.Controls.Add(Me.btnBarrioEliminar)
        Me.tpBarrios.Controls.Add(Me.rdbBarrioDesactivado)
        Me.tpBarrios.Controls.Add(Me.rdbBarrioActivo)
        Me.tpBarrios.Controls.Add(Me.Label6)
        Me.tpBarrios.Controls.Add(Me.btnBarrioGuardar)
        Me.tpBarrios.Controls.Add(Me.btnBarrioEditar)
        Me.tpBarrios.Controls.Add(Me.btnBarrioNuevo)
        Me.tpBarrios.Controls.Add(Me.txtBarrioDescripcion)
        Me.tpBarrios.Controls.Add(Me.lvBarrio)
        Me.tpBarrios.Controls.Add(Me.Label7)
        Me.tpBarrios.Controls.Add(Me.txtBarrioID)
        Me.tpBarrios.Controls.Add(Me.Label8)
        Me.tpBarrios.Location = New System.Drawing.Point(4, 22)
        Me.tpBarrios.Name = "tpBarrios"
        Me.tpBarrios.Size = New System.Drawing.Size(514, 352)
        Me.tpBarrios.TabIndex = 3
        Me.tpBarrios.Text = "Barrios"
        Me.tpBarrios.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(17, 147)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(67, 13)
        Me.Label11.TabIndex = 25
        Me.Label11.Text = "Codigo SET:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(17, 39)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(30, 13)
        Me.Label10.TabIndex = 23
        Me.Label10.Text = "Pais:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(17, 66)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(77, 13)
        Me.Label9.TabIndex = 21
        Me.Label9.Text = "Departamento:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(17, 93)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 13)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Ciudad:"
        '
        'btnBarrioCancelar
        '
        Me.btnBarrioCancelar.Location = New System.Drawing.Point(338, 191)
        Me.btnBarrioCancelar.Name = "btnBarrioCancelar"
        Me.btnBarrioCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnBarrioCancelar.TabIndex = 10
        Me.btnBarrioCancelar.Text = "&Cancelar"
        Me.btnBarrioCancelar.UseVisualStyleBackColor = True
        '
        'btnBarrioEliminar
        '
        Me.btnBarrioEliminar.Location = New System.Drawing.Point(419, 191)
        Me.btnBarrioEliminar.Name = "btnBarrioEliminar"
        Me.btnBarrioEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnBarrioEliminar.TabIndex = 11
        Me.btnBarrioEliminar.Text = "E&liminar"
        Me.btnBarrioEliminar.UseVisualStyleBackColor = True
        '
        'rdbBarrioDesactivado
        '
        Me.rdbBarrioDesactivado.AutoSize = True
        Me.rdbBarrioDesactivado.Location = New System.Drawing.Point(156, 168)
        Me.rdbBarrioDesactivado.Name = "rdbBarrioDesactivado"
        Me.rdbBarrioDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbBarrioDesactivado.TabIndex = 6
        Me.rdbBarrioDesactivado.TabStop = True
        Me.rdbBarrioDesactivado.Text = "Desactivado"
        Me.rdbBarrioDesactivado.UseVisualStyleBackColor = True
        '
        'rdbBarrioActivo
        '
        Me.rdbBarrioActivo.AutoSize = True
        Me.rdbBarrioActivo.Location = New System.Drawing.Point(95, 168)
        Me.rdbBarrioActivo.Name = "rdbBarrioActivo"
        Me.rdbBarrioActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbBarrioActivo.TabIndex = 5
        Me.rdbBarrioActivo.TabStop = True
        Me.rdbBarrioActivo.Text = "Activo"
        Me.rdbBarrioActivo.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(17, 170)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(43, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Estado:"
        '
        'btnBarrioGuardar
        '
        Me.btnBarrioGuardar.Location = New System.Drawing.Point(257, 191)
        Me.btnBarrioGuardar.Name = "btnBarrioGuardar"
        Me.btnBarrioGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnBarrioGuardar.TabIndex = 9
        Me.btnBarrioGuardar.Text = "&Guardar"
        Me.btnBarrioGuardar.UseVisualStyleBackColor = True
        '
        'btnBarrioEditar
        '
        Me.btnBarrioEditar.Location = New System.Drawing.Point(176, 191)
        Me.btnBarrioEditar.Name = "btnBarrioEditar"
        Me.btnBarrioEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnBarrioEditar.TabIndex = 8
        Me.btnBarrioEditar.Text = "&Editar"
        Me.btnBarrioEditar.UseVisualStyleBackColor = True
        '
        'btnBarrioNuevo
        '
        Me.btnBarrioNuevo.Location = New System.Drawing.Point(95, 191)
        Me.btnBarrioNuevo.Name = "btnBarrioNuevo"
        Me.btnBarrioNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnBarrioNuevo.TabIndex = 7
        Me.btnBarrioNuevo.Text = "&Nuevo"
        Me.btnBarrioNuevo.UseVisualStyleBackColor = True
        '
        'lvBarrio
        '
        Me.lvBarrio.Location = New System.Drawing.Point(95, 220)
        Me.lvBarrio.Name = "lvBarrio"
        Me.lvBarrio.Size = New System.Drawing.Size(399, 124)
        Me.lvBarrio.TabIndex = 12
        Me.lvBarrio.UseCompatibleStateImageBehavior = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(17, 121)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 13)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Descripción:"
        '
        'txtBarrioID
        '
        Me.txtBarrioID.BackColor = System.Drawing.Color.White
        Me.txtBarrioID.Location = New System.Drawing.Point(95, 12)
        Me.txtBarrioID.Name = "txtBarrioID"
        Me.txtBarrioID.ReadOnly = True
        Me.txtBarrioID.Size = New System.Drawing.Size(61, 20)
        Me.txtBarrioID.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(17, 15)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(21, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "ID:"
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(443, 385)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 411)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(534, 22)
        Me.StatusStrip1.TabIndex = 2
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'txtCodigoSet
        '
        Me.txtCodigoSet.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoSet.Color = System.Drawing.Color.Empty
        Me.txtCodigoSet.Indicaciones = Nothing
        Me.txtCodigoSet.Location = New System.Drawing.Point(95, 143)
        Me.txtCodigoSet.Multilinea = False
        Me.txtCodigoSet.Name = "txtCodigoSet"
        Me.txtCodigoSet.Size = New System.Drawing.Size(82, 21)
        Me.txtCodigoSet.SoloLectura = False
        Me.txtCodigoSet.TabIndex = 20
        Me.txtCodigoSet.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCodigoSet.Texto = ""
        '
        'txtCiudadCodigo
        '
        Me.txtCiudadCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCiudadCodigo.Color = System.Drawing.Color.Empty
        Me.txtCiudadCodigo.Indicaciones = Nothing
        Me.txtCiudadCodigo.Location = New System.Drawing.Point(95, 117)
        Me.txtCiudadCodigo.Multilinea = False
        Me.txtCiudadCodigo.Name = "txtCiudadCodigo"
        Me.txtCiudadCodigo.Size = New System.Drawing.Size(82, 21)
        Me.txtCiudadCodigo.SoloLectura = False
        Me.txtCiudadCodigo.TabIndex = 9
        Me.txtCiudadCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCiudadCodigo.Texto = ""
        '
        'txtCiudadDescripcion
        '
        Me.txtCiudadDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCiudadDescripcion.Color = System.Drawing.Color.Empty
        Me.txtCiudadDescripcion.Indicaciones = Nothing
        Me.txtCiudadDescripcion.Location = New System.Drawing.Point(95, 90)
        Me.txtCiudadDescripcion.Multilinea = False
        Me.txtCiudadDescripcion.Name = "txtCiudadDescripcion"
        Me.txtCiudadDescripcion.Size = New System.Drawing.Size(189, 21)
        Me.txtCiudadDescripcion.SoloLectura = False
        Me.txtCiudadDescripcion.TabIndex = 7
        Me.txtCiudadDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCiudadDescripcion.Texto = ""
        '
        'cbxCiudadDepartamento
        '
        Me.cbxCiudadDepartamento.CampoWhere = Nothing
        Me.cbxCiudadDepartamento.CargarUnaSolaVez = False
        Me.cbxCiudadDepartamento.DataDisplayMember = Nothing
        Me.cbxCiudadDepartamento.DataFilter = Nothing
        Me.cbxCiudadDepartamento.DataOrderBy = Nothing
        Me.cbxCiudadDepartamento.DataSource = Nothing
        Me.cbxCiudadDepartamento.DataValueMember = Nothing
        Me.cbxCiudadDepartamento.dtSeleccionado = Nothing
        Me.cbxCiudadDepartamento.FormABM = Nothing
        Me.cbxCiudadDepartamento.Indicaciones = Nothing
        Me.cbxCiudadDepartamento.Location = New System.Drawing.Point(95, 65)
        Me.cbxCiudadDepartamento.Name = "cbxCiudadDepartamento"
        Me.cbxCiudadDepartamento.SeleccionMultiple = False
        Me.cbxCiudadDepartamento.SeleccionObligatoria = False
        Me.cbxCiudadDepartamento.Size = New System.Drawing.Size(189, 21)
        Me.cbxCiudadDepartamento.SoloLectura = False
        Me.cbxCiudadDepartamento.TabIndex = 5
        Me.cbxCiudadDepartamento.Texto = ""
        '
        'cbxCiudadPais
        '
        Me.cbxCiudadPais.CampoWhere = Nothing
        Me.cbxCiudadPais.CargarUnaSolaVez = False
        Me.cbxCiudadPais.DataDisplayMember = Nothing
        Me.cbxCiudadPais.DataFilter = Nothing
        Me.cbxCiudadPais.DataOrderBy = Nothing
        Me.cbxCiudadPais.DataSource = Nothing
        Me.cbxCiudadPais.DataValueMember = Nothing
        Me.cbxCiudadPais.dtSeleccionado = Nothing
        Me.cbxCiudadPais.FormABM = Nothing
        Me.cbxCiudadPais.Indicaciones = Nothing
        Me.cbxCiudadPais.Location = New System.Drawing.Point(95, 38)
        Me.cbxCiudadPais.Name = "cbxCiudadPais"
        Me.cbxCiudadPais.SeleccionMultiple = False
        Me.cbxCiudadPais.SeleccionObligatoria = False
        Me.cbxCiudadPais.Size = New System.Drawing.Size(189, 21)
        Me.cbxCiudadPais.SoloLectura = False
        Me.cbxCiudadPais.TabIndex = 3
        Me.cbxCiudadPais.Texto = ""
        '
        'txtCodigoBarrio
        '
        Me.txtCodigoBarrio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoBarrio.Color = System.Drawing.Color.Empty
        Me.txtCodigoBarrio.Indicaciones = Nothing
        Me.txtCodigoBarrio.Location = New System.Drawing.Point(95, 143)
        Me.txtCodigoBarrio.Multilinea = False
        Me.txtCodigoBarrio.Name = "txtCodigoBarrio"
        Me.txtCodigoBarrio.Size = New System.Drawing.Size(75, 21)
        Me.txtCodigoBarrio.SoloLectura = False
        Me.txtCodigoBarrio.TabIndex = 26
        Me.txtCodigoBarrio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCodigoBarrio.Texto = ""
        '
        'txtBarrioDescripcion
        '
        Me.txtBarrioDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBarrioDescripcion.Color = System.Drawing.Color.Empty
        Me.txtBarrioDescripcion.Indicaciones = Nothing
        Me.txtBarrioDescripcion.Location = New System.Drawing.Point(95, 117)
        Me.txtBarrioDescripcion.Multilinea = False
        Me.txtBarrioDescripcion.Name = "txtBarrioDescripcion"
        Me.txtBarrioDescripcion.Size = New System.Drawing.Size(197, 21)
        Me.txtBarrioDescripcion.SoloLectura = False
        Me.txtBarrioDescripcion.TabIndex = 3
        Me.txtBarrioDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBarrioDescripcion.Texto = ""
        '
        'cbxPais
        '
        Me.cbxPais.CampoWhere = Nothing
        Me.cbxPais.CargarUnaSolaVez = False
        Me.cbxPais.DataDisplayMember = Nothing
        Me.cbxPais.DataFilter = Nothing
        Me.cbxPais.DataOrderBy = Nothing
        Me.cbxPais.DataSource = Nothing
        Me.cbxPais.DataValueMember = Nothing
        Me.cbxPais.dtSeleccionado = Nothing
        Me.cbxPais.FormABM = Nothing
        Me.cbxPais.Indicaciones = Nothing
        Me.cbxPais.Location = New System.Drawing.Point(95, 38)
        Me.cbxPais.Name = "cbxPais"
        Me.cbxPais.SeleccionMultiple = False
        Me.cbxPais.SeleccionObligatoria = False
        Me.cbxPais.Size = New System.Drawing.Size(197, 21)
        Me.cbxPais.SoloLectura = False
        Me.cbxPais.TabIndex = 27
        Me.cbxPais.Texto = ""
        '
        'cbxDepartamento
        '
        Me.cbxDepartamento.CampoWhere = Nothing
        Me.cbxDepartamento.CargarUnaSolaVez = False
        Me.cbxDepartamento.DataDisplayMember = Nothing
        Me.cbxDepartamento.DataFilter = Nothing
        Me.cbxDepartamento.DataOrderBy = Nothing
        Me.cbxDepartamento.DataSource = Nothing
        Me.cbxDepartamento.DataValueMember = Nothing
        Me.cbxDepartamento.dtSeleccionado = Nothing
        Me.cbxDepartamento.FormABM = Nothing
        Me.cbxDepartamento.Indicaciones = Nothing
        Me.cbxDepartamento.Location = New System.Drawing.Point(95, 65)
        Me.cbxDepartamento.Name = "cbxDepartamento"
        Me.cbxDepartamento.SeleccionMultiple = False
        Me.cbxDepartamento.SeleccionObligatoria = False
        Me.cbxDepartamento.Size = New System.Drawing.Size(197, 21)
        Me.cbxDepartamento.SoloLectura = False
        Me.cbxDepartamento.TabIndex = 28
        Me.cbxDepartamento.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(95, 90)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(197, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 29
        Me.cbxCiudad.Texto = ""
        '
        'frmPaisDepartamentoCiudadBarrios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnSalir
        Me.ClientSize = New System.Drawing.Size(534, 433)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.tbcGeneral)
        Me.Name = "frmPaisDepartamentoCiudadBarrios"
        Me.Tag = "LOCALIDADES"
        Me.Text = "frmPaisDepartamentoCiudadBarrios"
        Me.tbcGeneral.ResumeLayout(False)
        Me.tpPaises.ResumeLayout(False)
        Me.tpPaises.PerformLayout()
        Me.tpDepartamentos.ResumeLayout(False)
        Me.tpDepartamentos.PerformLayout()
        Me.tpCiudades.ResumeLayout(False)
        Me.tpCiudades.PerformLayout()
        Me.tpBarrios.ResumeLayout(False)
        Me.tpBarrios.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbcGeneral As System.Windows.Forms.TabControl
    Friend WithEvents tpPaises As System.Windows.Forms.TabPage
    Friend WithEvents tpDepartamentos As System.Windows.Forms.TabPage
    Friend WithEvents tpCiudades As System.Windows.Forms.TabPage
    Friend WithEvents tpBarrios As System.Windows.Forms.TabPage
    Friend WithEvents lblPaisID As System.Windows.Forms.Label
    Friend WithEvents txtPaisID As System.Windows.Forms.TextBox
    Friend WithEvents txtPaisDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents lblPaisDescripcion As System.Windows.Forms.Label
    Friend WithEvents lvPaisLista As System.Windows.Forms.ListView
    Friend WithEvents btnPaisGuardar As System.Windows.Forms.Button
    Friend WithEvents btnPaisEditar As System.Windows.Forms.Button
    Friend WithEvents btnPaisNuevo As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnPaisEliminar As System.Windows.Forms.Button
    Friend WithEvents txtPaisNacionalidad As System.Windows.Forms.TextBox
    Friend WithEvents lblPaisNacionalidad As System.Windows.Forms.Label
    Friend WithEvents btnPaisCancelar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents rdbPaisDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbPaisActivo As System.Windows.Forms.RadioButton
    Friend WithEvents lblPaisEstado As System.Windows.Forms.Label
    Friend WithEvents cbxDepartamentoPais As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents rdbDepartamentoDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbDepartamentoActivo As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnDepartamentoGuardar As System.Windows.Forms.Button
    Friend WithEvents btnDepartamentoEditar As System.Windows.Forms.Button
    Friend WithEvents btnDepartamentoNuevo As System.Windows.Forms.Button
    Friend WithEvents lvDepartamentos As System.Windows.Forms.ListView
    Friend WithEvents txtDepartamentoDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtDepartamentoID As System.Windows.Forms.TextBox
    Friend WithEvents lblDepartamentoID As System.Windows.Forms.Label
    Friend WithEvents btnDepartamentoCancelar As System.Windows.Forms.Button
    Friend WithEvents btnDepartamentoEliminar As System.Windows.Forms.Button
    Friend WithEvents btnCiudadCancelar As System.Windows.Forms.Button
    Friend WithEvents btnCiudadEliminar As System.Windows.Forms.Button
    Friend WithEvents lblPaisCiudad As System.Windows.Forms.Label
    Friend WithEvents rdbCiudadDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbCiudadActivo As System.Windows.Forms.RadioButton
    Friend WithEvents lblCiudadEstado As System.Windows.Forms.Label
    Friend WithEvents btnCiudadGuardar As System.Windows.Forms.Button
    Friend WithEvents btnCiudadEditar As System.Windows.Forms.Button
    Friend WithEvents btnCiudadNuevo As System.Windows.Forms.Button
    Friend WithEvents lvCiudades As System.Windows.Forms.ListView
    Friend WithEvents lblCiudadDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtCiudadID As System.Windows.Forms.TextBox
    Friend WithEvents lblCiudadID As System.Windows.Forms.Label
    Friend WithEvents lblDepartamentoCiudad As System.Windows.Forms.Label
    Friend WithEvents cbxCiudadDepartamento As ERP.ocxCBX
    Friend WithEvents cbxCiudadPais As ERP.ocxCBX
    Friend WithEvents txtCiudadDescripcion As ERP.ocxTXTString
    Friend WithEvents btnBarrioCancelar As System.Windows.Forms.Button
    Friend WithEvents btnBarrioEliminar As System.Windows.Forms.Button
    Friend WithEvents rdbBarrioDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbBarrioActivo As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnBarrioGuardar As System.Windows.Forms.Button
    Friend WithEvents btnBarrioEditar As System.Windows.Forms.Button
    Friend WithEvents btnBarrioNuevo As System.Windows.Forms.Button
    Friend WithEvents lvBarrio As System.Windows.Forms.ListView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtBarrioID As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtBarrioDescripcion As ERP.ocxTXTString
    Friend WithEvents txtCiudadCodigo As ERP.ocxTXTString
    Friend WithEvents lblCiudadCodigo As System.Windows.Forms.Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtCodigo As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents txtCodigoBarrio As ocxTXTString
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txtCodigoSet As ocxTXTString
    Friend WithEvents cbxCiudad As ocxCBX
    Friend WithEvents cbxDepartamento As ocxCBX
    Friend WithEvents cbxPais As ocxCBX
End Class

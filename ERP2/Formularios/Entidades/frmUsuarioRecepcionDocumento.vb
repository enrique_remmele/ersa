﻿Public Class frmUsuarioRecepcionDocumento

    Dim CSistema As New CSistema

    Sub Inicializar()
        CargarInformacion()
    End Sub

    Sub CargarInformacion()

        CSistema.SqlToDataGrid(dgv, "Select IDUsuario, Nombre from vUsuarioRecepcionDocumento")

    End Sub

    Sub Guardar()
        Dim SQL As String = "exec SpUsuarioRecepcionDocumento @IDUsuario = " & cbxUsuario.cbx.SelectedValue & ", @Operacion = 'INS'"
        CSistema.ExecuteNonQuery(SQL)
        CargarInformacion()
    End Sub

    Sub Eliminar()
        Dim SQL As String = "exec SpUsuarioRecepcionDocumento @IDUsuario = " & dgv.SelectedRows(0).Cells(0).Value & ", @Operacion = 'DEL'"
        CSistema.ExecuteNonQuery(SQL)
        CargarInformacion()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmUsuarioRecepcionDocumento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar
    End Sub
End Class
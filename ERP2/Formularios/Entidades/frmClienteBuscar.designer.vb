﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClienteBuscar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsslCantidad = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.rdbRazonSocial = New System.Windows.Forms.RadioButton()
        Me.rdbReferencia = New System.Windows.Forms.RadioButton()
        Me.rdbRUC = New System.Windows.Forms.RadioButton()
        Me.rdbNombreFantasia = New System.Windows.Forms.RadioButton()
        Me.chkSucursal = New System.Windows.Forms.CheckBox()
        Me.cbxSucursal = New System.Windows.Forms.ComboBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.chkActivo = New ERP.ocxCHK()
        Me.dgvLista = New System.Windows.Forms.DataGridView()
        Me.StatusStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.tsslCantidad, Me.ToolStripStatusLabel2, Me.ToolStripStatusLabel3})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 474)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1035, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(58, 17)
        Me.ToolStripStatusLabel1.Text = "Cantidad:"
        '
        'tsslCantidad
        '
        Me.tsslCantidad.Name = "tsslCantidad"
        Me.tsslCantidad.Size = New System.Drawing.Size(13, 17)
        Me.tsslCantidad.Text = "0"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Margin = New System.Windows.Forms.Padding(20, 3, 0, 2)
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(207, 17)
        Me.ToolStripStatusLabel2.Text = "[Precione INICIO para volver a buscar]"
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.Margin = New System.Windows.Forms.Padding(20, 3, 0, 2)
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(276, 17)
        Me.ToolStripStatusLabel3.Text = "[Precione F2 para cambiar el metodo de busqueda]"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvLista, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1035, 474)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtDescripcion)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbRazonSocial)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbReferencia)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbRUC)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbNombreFantasia)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkSucursal)
        Me.FlowLayoutPanel1.Controls.Add(Me.cbxSucursal)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkActivo)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1020, 24)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(3, 3)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(156, 20)
        Me.txtDescripcion.TabIndex = 0
        '
        'rdbRazonSocial
        '
        Me.rdbRazonSocial.AutoSize = True
        Me.rdbRazonSocial.Location = New System.Drawing.Point(165, 3)
        Me.rdbRazonSocial.Name = "rdbRazonSocial"
        Me.rdbRazonSocial.Size = New System.Drawing.Size(88, 17)
        Me.rdbRazonSocial.TabIndex = 1
        Me.rdbRazonSocial.TabStop = True
        Me.rdbRazonSocial.Text = "Razón Social"
        Me.rdbRazonSocial.UseVisualStyleBackColor = True
        '
        'rdbReferencia
        '
        Me.rdbReferencia.AutoSize = True
        Me.rdbReferencia.Location = New System.Drawing.Point(259, 3)
        Me.rdbReferencia.Name = "rdbReferencia"
        Me.rdbReferencia.Size = New System.Drawing.Size(42, 17)
        Me.rdbReferencia.TabIndex = 2
        Me.rdbReferencia.TabStop = True
        Me.rdbReferencia.Text = "Ref"
        Me.rdbReferencia.UseVisualStyleBackColor = True
        '
        'rdbRUC
        '
        Me.rdbRUC.AutoSize = True
        Me.rdbRUC.Location = New System.Drawing.Point(307, 3)
        Me.rdbRUC.Name = "rdbRUC"
        Me.rdbRUC.Size = New System.Drawing.Size(48, 17)
        Me.rdbRUC.TabIndex = 3
        Me.rdbRUC.TabStop = True
        Me.rdbRUC.Text = "RUC"
        Me.rdbRUC.UseVisualStyleBackColor = True
        '
        'rdbNombreFantasia
        '
        Me.rdbNombreFantasia.AutoSize = True
        Me.rdbNombreFantasia.Location = New System.Drawing.Point(361, 3)
        Me.rdbNombreFantasia.Name = "rdbNombreFantasia"
        Me.rdbNombreFantasia.Size = New System.Drawing.Size(122, 17)
        Me.rdbNombreFantasia.TabIndex = 4
        Me.rdbNombreFantasia.TabStop = True
        Me.rdbNombreFantasia.Text = "Nombre de Fantasía"
        Me.rdbNombreFantasia.UseVisualStyleBackColor = True
        '
        'chkSucursal
        '
        Me.chkSucursal.AutoSize = True
        Me.chkSucursal.Location = New System.Drawing.Point(489, 3)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(70, 17)
        Me.chkSucursal.TabIndex = 5
        Me.chkSucursal.Text = "Sucursal:"
        Me.chkSucursal.UseVisualStyleBackColor = True
        '
        'cbxSucursal
        '
        Me.cbxSucursal.FormattingEnabled = True
        Me.cbxSucursal.Location = New System.Drawing.Point(565, 3)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.Size = New System.Drawing.Size(121, 21)
        Me.cbxSucursal.TabIndex = 6
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(692, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 21)
        Me.btnAceptar.TabIndex = 7
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancelar.Location = New System.Drawing.Point(773, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 21)
        Me.btnCancelar.TabIndex = 6
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'chkActivo
        '
        Me.chkActivo.BackColor = System.Drawing.Color.Transparent
        Me.chkActivo.Color = System.Drawing.Color.Empty
        Me.chkActivo.Location = New System.Drawing.Point(866, 3)
        Me.chkActivo.Margin = New System.Windows.Forms.Padding(15, 3, 3, 3)
        Me.chkActivo.Name = "chkActivo"
        Me.chkActivo.Size = New System.Drawing.Size(131, 20)
        Me.chkActivo.SoloLectura = False
        Me.chkActivo.TabIndex = 10
        Me.chkActivo.Texto = "Sólo Activos:"
        Me.chkActivo.Valor = False
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLista.Location = New System.Drawing.Point(3, 33)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.Size = New System.Drawing.Size(1029, 418)
        Me.dgvLista.TabIndex = 1
        '
        'frmClienteBuscar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancelar
        Me.ClientSize = New System.Drawing.Size(1035, 496)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.Name = "frmClienteBuscar"
        Me.Text = "frmClienteBuscar"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsslCantidad As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents rdbRazonSocial As System.Windows.Forms.RadioButton
    Friend WithEvents rdbRUC As System.Windows.Forms.RadioButton
    Friend WithEvents rdbReferencia As System.Windows.Forms.RadioButton
    Friend WithEvents rdbNombreFantasia As System.Windows.Forms.RadioButton
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents chkSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents cbxSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents dgvLista As System.Windows.Forms.DataGridView
    Friend WithEvents chkActivo As ERP.ocxCHK
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
End Class

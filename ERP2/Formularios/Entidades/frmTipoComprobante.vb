﻿Public Class frmTipoComprobante

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Controles
        CSistema.InicializaControles(Me)
        CSistema.InicializaControles(gbxSigno)

        'Variables
        vNuevo = False

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        CargarInformacion()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO)

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxTipo)
        CSistema.CargaControl(vControles, cbxOperacion)
        CSistema.CargaControl(vControles, txtDescripcion)
        CSistema.CargaControl(vControles, txtCodigo)
        CSistema.CargaControl(vControles, txtResumen)
        CSistema.CargaControl(vControles, rdbActivo)
        CSistema.CargaControl(vControles, rdbDesactivado)
        CSistema.CargaControl(vControles, chkAutonumerico)
        CSistema.CargaControl(vControles, chkComprobanteTimbrado)
        CSistema.CargaControl(vControles, chkComprobanteResta)
        CSistema.CargaControl(vControles, chkHabilitarDepositoBancario)
        CSistema.CargaControl(vControles, chkCalcularIVA)
        CSistema.CargaControl(vControles, chkIncluirEnLibro)
        CSistema.CargaControl(vControles, chkIVAIncluido)
        CSistema.CargaControl(vControles, rdbPositivo)
        CSistema.CargaControl(vControles, rdbNegativo)
        CSistema.CargaControl(vControles, txtHechaukaTipoDocumento)
        CSistema.CargaControl(vControles, txtHechaukaTimbradoReemplazar)
        CSistema.CargaControl(vControles, chkEsInterno)
        CSistema.CargaControl(vControles, chkExento)

        'Tipo de Comprobante
        cbxTipo.cbx.Items.Add("CLIENTE")
        cbxTipo.cbx.Items.Add("PROVEEDOR")

        cbxTipoFiltro.cbx.Items.Add("CLIENTE")
        cbxTipoFiltro.cbx.Items.Add("PROVEEDOR")

        'Operaciones
        Dim dt As DataTable = CData.GetTable("Operacion")
        CData.OrderDataTable(dt, "Descripcion")

        CSistema.SqlToComboBox(cbxOperacion.cbx, dt.Copy, "ID", "Descripcion")
        CSistema.SqlToComboBox(cbxOperacionFiltro.cbx, dt.Copy, "ID", "Descripcion")

        'JGR 20140910
        Me.ToolTip1.SetToolTip(chkEsInterno, "No genera asientos contables")

        'Cargamos los registos en el lv
        Listar()

    End Sub

    Sub ObtenerInformacion()

        Try
            'Validar
            'Si es que se selecciono el registro.
            If dgvLista.SelectedRows.Count = 0 Then
                Exit Sub
            End If

            'Obtener el ID Registro
            Dim ID As Integer

            ID = dgvLista.SelectedRows(0).Cells("ID").Value

            'Obtenemos la informacion actualizada desde la base de datos
            Dim dt As New DataTable
            dt = CSistema.ExecuteToDataTable("Select * From VTipoComprobante Where ID=" & ID)

            'Solo procesar si es que se encontro la fila asociada
            If dt.Rows.Count > 0 Then

                'Cargamos la fila "0" en un nuevo objeto DATAROW
                Dim oRow As DataRow
                oRow = dt.Rows(0)

                'Asignamos los valores a los controles correspondientes
                txtID.txt.Text = oRow("ID").ToString
                cbxTipo.cbx.Text = oRow("Tipo").ToString
                cbxOperacion.cbx.Text = oRow("Operacion").ToString
                txtDescripcion.txt.Text = oRow("Descripcion").ToString
                txtCodigo.txt.Text = oRow("Codigo").ToString
                txtResumen.txt.Text = oRow("Resumen").ToString

                If IsDBNull(oRow("Restar")) = False Then
                    chkComprobanteResta.Valor = oRow("Restar")
                End If

                If IsDBNull(oRow("Deposito")) = False Then
                    chkHabilitarDepositoBancario.Valor = oRow("Deposito")
                End If

                If CBool(oRow("Estado")) = True Then
                    rdbActivo.Checked = True
                Else
                    rdbDesactivado.Checked = True
                End If

                'Opciones
                chkAutonumerico.Valor = CBool(oRow("Autonumerico").ToString)
                chkComprobanteTimbrado.Valor = CBool(oRow("ComprobanteTimbrado").ToString)
                chkCalcularIVA.Valor = CBool(oRow("CalcularIVA").ToString)
                chkIVAIncluido.Valor = CBool(oRow("IVAIncluido").ToString)
                chkIncluirEnLibro.Valor = CBool(oRow("Libro").ToString)
                'JGR 20140910
                chkEsInterno.Valor = CBool(oRow("EsInterno").ToString)
                chkExento.Valor = CBool(oRow("Exento").ToString)

                If chkIncluirEnLibro.Valor = True Then
                    Dim Signo As String = oRow("Signo").ToString

                    If Signo = "+" Then
                        rdbPositivo.Checked = True
                    End If

                    If Signo = "-" Then
                        rdbNegativo.Checked = True
                    End If
                Else
                    rdbPositivo.Checked = False
                    rdbNegativo.Checked = False
                End If

                'Hechauka
                chkHechaukaTimbradoReemplazar.Valor = oRow("HechaukaTimbradoReemplazar")
                txtHechaukaTimbradoReemplazar.SetValue(oRow("HechaukaNumeroTimbrado"))
                txtHechaukaTipoDocumento.SetValue(oRow("HechaukaTipoDocumento"))

                'Configuramos los controles ABM como EDITAR
                EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR)

            End If

            ctrError.Clear()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Sub Listar(Optional ByVal Where As String = "")

        CSistema.SqlToDataGrid(dgvLista, "Select ID, Tipo, Operacion, Codigo, Descripcion From VTipoComprobante " & Where & " ORDER BY Operacion")

        If dgvLista.Columns.Count = 0 Then
            Exit Sub
        End If

        'Formato
        dgvLista.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub InicializarControles()

        'TextBox
        CSistema.InicializaControles(vControles)

        'RadioButton
        rdbActivo.Checked = True
        rdbPositivo.Checked = False
        rdbNegativo.Checked = False

        'Chec
        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From TipoComprobante"), Integer)
        Else
            Listar()
        End If

        'Error
        ctrError.Clear()

        'Foco
        cbxTipo.cbx.Focus()


    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Seleccion correcta del Tipo
        If cbxTipo.cbx.Text <> "CLIENTE" And cbxTipo.cbx.Text <> "PROVEEDOR" Then
            Dim mensaje As String = "Seleccione correctamente el tipo!"
            ctrError.SetError(cbxTipo, mensaje)
            ctrError.SetIconAlignment(cbxTipo, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion correcta del Operacion
        If IsNumeric(cbxOperacion.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente la Operacion!"
            ctrError.SetError(cbxOperacion, mensaje)
            ctrError.SetIconAlignment(cbxOperacion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Escritura de la Descripcion
        If txtDescripcion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar una descripcion valida!"
            ctrError.SetError(txtDescripcion, mensaje)
            ctrError.SetIconAlignment(txtDescripcion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Escritura de del Codigo
        If txtCodigo.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar un codigo valido!"
            ctrError.SetError(txtCodigo, mensaje)
            ctrError.SetIconAlignment(txtCodigo, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Libro
        If chkIncluirEnLibro.Valor = True Then
            If rdbNegativo.Checked = False And rdbPositivo.Checked = False Then
                Dim mensaje As String = "Debe seleccionar el signo en el libro!"
                ctrError.SetError(gbxSigno, mensaje)
                ctrError.SetIconAlignment(gbxSigno, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgvLista.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgvLista, mensaje)
                ctrError.SetIconAlignment(dgvLista, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Tipo", cbxTipo.cbx.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDOperacion", cbxOperacion.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Codigo", txtCodigo.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Resumen", txtResumen.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Restar", chkComprobanteResta.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Deposito", chkHabilitarDepositoBancario.Valor.ToString, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Autonumerico", chkAutonumerico.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ComprobanteTimbrado", chkComprobanteTimbrado.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CalcularIVA", chkCalcularIVA.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IVAIncluido", chkIVAIncluido.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Exento", chkExento.Valor.ToString, ParameterDirection.Input)
        'JGR 20140910
        CSistema.SetSQLParameter(param, "@EsInterno", chkEsInterno.Valor.ToString, ParameterDirection.Input)

        If cbxTipo.cbx.Text = "CLIENTE" And chkIncluirEnLibro.Valor = True Then
            CSistema.SetSQLParameter(param, "@LibroVenta", "True", ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@LibroVenta", "False", ParameterDirection.Input)
        End If

        If cbxTipo.cbx.Text = "PROVEEDOR" And chkIncluirEnLibro.Valor = True Then
            CSistema.SetSQLParameter(param, "@LibroCompra", "True", ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@LibroCompra", "False", ParameterDirection.Input)
        End If

        If chkIncluirEnLibro.Valor = True Then

            If rdbPositivo.Checked = True Then
                CSistema.SetSQLParameter(param, "@Signo", "+", ParameterDirection.Input)
            End If

            If rdbNegativo.Checked = True Then
                CSistema.SetSQLParameter(param, "@Signo", "-", ParameterDirection.Input)
            End If

        End If

        'Hechauka
        CSistema.SetSQLParameter(param, "@HechaukaTipoDocumento", txtHechaukaTipoDocumento.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@HechaukaTimbradoReemplazar", chkHechaukaTimbradoReemplazar.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@HechaukaNumeroTimbrado", txtHechaukaTimbradoReemplazar.GetValue, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpTipoComprobante", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO)
            ctrError.Clear()
            Listar(" Where ID=" & txtID.txt.Text)
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno

            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesABM)

        CSistema.ControlBotonesABM(Operacion, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles, btnImprimir)

    End Sub

    Sub ComprobantesFormaPagoDocumento()

        Dim oRow As DataRow = CData.GetRow(" ID = " & cbxOperacion.GetValue, "Operacion")

        chkComprobanteResta.Visible = False
        chkHabilitarDepositoBancario.Visible = False

        If oRow("FormName").ToString <> "frmSeleccionFormaPagoDocumento" Then
            Exit Sub
        End If

        chkComprobanteResta.Visible = True
        chkHabilitarDepositoBancario.Visible = True

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click

        'Establecemos los botones a Editando
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO)

        vNuevo = False

        'Foco
        txtDescripcion.Focus()
        txtDescripcion.txt.SelectAll()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub frmTipoComprobante_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub chkHechaukaTimbradoReemplazar_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkHechaukaTimbradoReemplazar.PropertyChanged
        txtHechaukaTimbradoReemplazar.Enabled = value
    End Sub

    Private Sub dgvLista_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLista.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub chkIVAIncluido_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkIVAIncluido.PropertyChanged
        If chkIncluirEnLibro.Valor = True Then
            gbxSigno.Enabled = True
        Else
            gbxSigno.Enabled = False
        End If
    End Sub

    Private Sub cbxTipoFiltro_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoFiltro.PropertyChanged
        Listar(" Where Tipo = '" & cbxTipoFiltro.cbx.Text & "' ")
    End Sub

    Private Sub cbxOperacionFiltro_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxOperacionFiltro.PropertyChanged
        Listar(" Where IDOperacion = " & cbxOperacionFiltro.GetValue & " ")
    End Sub

    Private Sub lklListarTodos_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklListarTodos.LinkClicked
        Listar("")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Listar(" Where Descripcion Like '%" & txtBuscar.GetValue & "%' ")
    End Sub

    Private Sub chkIncluirEnLibro_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkIncluirEnLibro.PropertyChanged
        gbxSigno.Enabled = chkIncluirEnLibro.Valor
    End Sub

    Private Sub cbxOperacion_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxOperacion.PropertyChanged
        ComprobantesFormaPagoDocumento()
    End Sub
End Class
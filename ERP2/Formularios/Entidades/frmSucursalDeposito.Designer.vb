﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSucursalDeposito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.tbcGeneral = New System.Windows.Forms.TabControl()
        Me.tpSucursal = New System.Windows.Forms.TabPage()
        Me.dgvSucursal = New System.Windows.Forms.DataGridView()
        Me.txtSucursalCodigoDistribuidor = New ERP.ocxTXTString()
        Me.lblCodigoDistribuidor = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSucursalCuentaContable = New ERP.ocxTXTString()
        Me.lblCuentaContable = New System.Windows.Forms.Label()
        Me.txtSucursalCodigo = New ERP.ocxTXTString()
        Me.lblSucursalCodigo = New System.Windows.Forms.Label()
        Me.txtSucursalReferencia = New ERP.ocxTXTString()
        Me.lblSucursalReferencia = New System.Windows.Forms.Label()
        Me.txtSucursalTelefono = New ERP.ocxTXTString()
        Me.lblSucursalTelefono = New System.Windows.Forms.Label()
        Me.txtSucursalDireccion = New ERP.ocxTXTString()
        Me.lblSucursalDireccion = New System.Windows.Forms.Label()
        Me.cbxSucursalCiudad = New ERP.ocxCBX()
        Me.lblSucursalCiudad = New System.Windows.Forms.Label()
        Me.cbxSucursalDepartamento = New ERP.ocxCBX()
        Me.cbxSucursalPais = New ERP.ocxCBX()
        Me.lblSucursalDepartamento = New System.Windows.Forms.Label()
        Me.lblSucursalPais = New System.Windows.Forms.Label()
        Me.txtSucursalDescripcion = New ERP.ocxTXTString()
        Me.btnSucursalCancelar = New System.Windows.Forms.Button()
        Me.btnSucursalEliminar = New System.Windows.Forms.Button()
        Me.rdbSucursalDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbSucursalActivo = New System.Windows.Forms.RadioButton()
        Me.lblSucursalEstado = New System.Windows.Forms.Label()
        Me.btnSucursalGuardar = New System.Windows.Forms.Button()
        Me.btnSucursalEditar = New System.Windows.Forms.Button()
        Me.btnSucursalNuevo = New System.Windows.Forms.Button()
        Me.lblSucursalDescripcion = New System.Windows.Forms.Label()
        Me.txtSucursalID = New System.Windows.Forms.TextBox()
        Me.lblSucursalID = New System.Windows.Forms.Label()
        Me.tpDeposito = New System.Windows.Forms.TabPage()
        Me.chkConsumoInformatica = New ERP.ocxCHK()
        Me.cbxSucursalDestino = New ERP.ocxCBX()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtCuentaContableCombustible = New ERP.ocxTXTCuentaContable()
        Me.dgvDeposito = New System.Windows.Forms.DataGridView()
        Me.chkDescargaCompra = New ERP.ocxCHK()
        Me.chkMateriaPrima = New ERP.ocxCHK()
        Me.chkConsumo = New ERP.ocxCHK()
        Me.chkDescargaStock = New ERP.ocxCHK()
        Me.chkCompra = New ERP.ocxCHK()
        Me.chkVenta = New ERP.ocxCHK()
        Me.cbxTipoDeposito = New ERP.ocxCBX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblCuentaContableMercaderia = New System.Windows.Forms.Label()
        Me.txtCuentaContableMercaderia = New ERP.ocxTXTCuentaContable()
        Me.cbxDepositoSucursal = New ERP.ocxCBX()
        Me.lblDepositoSucursal = New System.Windows.Forms.Label()
        Me.txtDepositoDescripcion = New ERP.ocxTXTString()
        Me.btnDepositoCancelar = New System.Windows.Forms.Button()
        Me.btnDepositoEliminar = New System.Windows.Forms.Button()
        Me.rdbDepositoDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbDepositoActivo = New System.Windows.Forms.RadioButton()
        Me.lblDepositoEstado = New System.Windows.Forms.Label()
        Me.btnDepositoGuardar = New System.Windows.Forms.Button()
        Me.btnDepositoEditar = New System.Windows.Forms.Button()
        Me.btnDepositoNuevo = New System.Windows.Forms.Button()
        Me.lblDepositoDescripcion = New System.Windows.Forms.Label()
        Me.txtDepositoID = New System.Windows.Forms.TextBox()
        Me.lblDepositoID = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txtNroCasa = New ERP.ocxTXTString()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbcGeneral.SuspendLayout()
        Me.tpSucursal.SuspendLayout()
        CType(Me.dgvSucursal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpDeposito.SuspendLayout()
        CType(Me.dgvDeposito, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbcGeneral
        '
        Me.tbcGeneral.Controls.Add(Me.tpSucursal)
        Me.tbcGeneral.Controls.Add(Me.tpDeposito)
        Me.tbcGeneral.Location = New System.Drawing.Point(12, 12)
        Me.tbcGeneral.Name = "tbcGeneral"
        Me.tbcGeneral.SelectedIndex = 0
        Me.tbcGeneral.Size = New System.Drawing.Size(494, 561)
        Me.tbcGeneral.TabIndex = 0
        '
        'tpSucursal
        '
        Me.tpSucursal.Controls.Add(Me.txtNroCasa)
        Me.tpSucursal.Controls.Add(Me.Label5)
        Me.tpSucursal.Controls.Add(Me.dgvSucursal)
        Me.tpSucursal.Controls.Add(Me.txtSucursalCodigoDistribuidor)
        Me.tpSucursal.Controls.Add(Me.lblCodigoDistribuidor)
        Me.tpSucursal.Controls.Add(Me.Label1)
        Me.tpSucursal.Controls.Add(Me.txtSucursalCuentaContable)
        Me.tpSucursal.Controls.Add(Me.lblCuentaContable)
        Me.tpSucursal.Controls.Add(Me.txtSucursalCodigo)
        Me.tpSucursal.Controls.Add(Me.lblSucursalCodigo)
        Me.tpSucursal.Controls.Add(Me.txtSucursalReferencia)
        Me.tpSucursal.Controls.Add(Me.lblSucursalReferencia)
        Me.tpSucursal.Controls.Add(Me.txtSucursalTelefono)
        Me.tpSucursal.Controls.Add(Me.lblSucursalTelefono)
        Me.tpSucursal.Controls.Add(Me.txtSucursalDireccion)
        Me.tpSucursal.Controls.Add(Me.lblSucursalDireccion)
        Me.tpSucursal.Controls.Add(Me.cbxSucursalCiudad)
        Me.tpSucursal.Controls.Add(Me.lblSucursalCiudad)
        Me.tpSucursal.Controls.Add(Me.cbxSucursalDepartamento)
        Me.tpSucursal.Controls.Add(Me.cbxSucursalPais)
        Me.tpSucursal.Controls.Add(Me.lblSucursalDepartamento)
        Me.tpSucursal.Controls.Add(Me.lblSucursalPais)
        Me.tpSucursal.Controls.Add(Me.txtSucursalDescripcion)
        Me.tpSucursal.Controls.Add(Me.btnSucursalCancelar)
        Me.tpSucursal.Controls.Add(Me.btnSucursalEliminar)
        Me.tpSucursal.Controls.Add(Me.rdbSucursalDesactivado)
        Me.tpSucursal.Controls.Add(Me.rdbSucursalActivo)
        Me.tpSucursal.Controls.Add(Me.lblSucursalEstado)
        Me.tpSucursal.Controls.Add(Me.btnSucursalGuardar)
        Me.tpSucursal.Controls.Add(Me.btnSucursalEditar)
        Me.tpSucursal.Controls.Add(Me.btnSucursalNuevo)
        Me.tpSucursal.Controls.Add(Me.lblSucursalDescripcion)
        Me.tpSucursal.Controls.Add(Me.txtSucursalID)
        Me.tpSucursal.Controls.Add(Me.lblSucursalID)
        Me.tpSucursal.Location = New System.Drawing.Point(4, 22)
        Me.tpSucursal.Name = "tpSucursal"
        Me.tpSucursal.Size = New System.Drawing.Size(486, 535)
        Me.tpSucursal.TabIndex = 3
        Me.tpSucursal.Text = "Sucursales"
        Me.tpSucursal.UseVisualStyleBackColor = True
        '
        'dgvSucursal
        '
        Me.dgvSucursal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSucursal.Location = New System.Drawing.Point(84, 341)
        Me.dgvSucursal.Name = "dgvSucursal"
        Me.dgvSucursal.ReadOnly = True
        Me.dgvSucursal.Size = New System.Drawing.Size(399, 191)
        Me.dgvSucursal.TabIndex = 31
        '
        'txtSucursalCodigoDistribuidor
        '
        Me.txtSucursalCodigoDistribuidor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSucursalCodigoDistribuidor.Color = System.Drawing.Color.Empty
        Me.txtSucursalCodigoDistribuidor.Indicaciones = Nothing
        Me.txtSucursalCodigoDistribuidor.Location = New System.Drawing.Point(242, 59)
        Me.txtSucursalCodigoDistribuidor.Multilinea = False
        Me.txtSucursalCodigoDistribuidor.Name = "txtSucursalCodigoDistribuidor"
        Me.txtSucursalCodigoDistribuidor.Size = New System.Drawing.Size(91, 21)
        Me.txtSucursalCodigoDistribuidor.SoloLectura = False
        Me.txtSucursalCodigoDistribuidor.TabIndex = 7
        Me.txtSucursalCodigoDistribuidor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSucursalCodigoDistribuidor.Texto = ""
        '
        'lblCodigoDistribuidor
        '
        Me.lblCodigoDistribuidor.AutoSize = True
        Me.lblCodigoDistribuidor.Location = New System.Drawing.Point(149, 63)
        Me.lblCodigoDistribuidor.Name = "lblCodigoDistribuidor"
        Me.lblCodigoDistribuidor.Size = New System.Drawing.Size(87, 13)
        Me.lblCodigoDistribuidor.TabIndex = 6
        Me.lblCodigoDistribuidor.Text = "Cod. Distribuidor:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(287, 266)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 12)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "* Resultado de Ejercicio"
        '
        'txtSucursalCuentaContable
        '
        Me.txtSucursalCuentaContable.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSucursalCuentaContable.Color = System.Drawing.Color.Empty
        Me.txtSucursalCuentaContable.Indicaciones = Nothing
        Me.txtSucursalCuentaContable.Location = New System.Drawing.Point(190, 262)
        Me.txtSucursalCuentaContable.Multilinea = False
        Me.txtSucursalCuentaContable.Name = "txtSucursalCuentaContable"
        Me.txtSucursalCuentaContable.Size = New System.Drawing.Size(91, 21)
        Me.txtSucursalCuentaContable.SoloLectura = False
        Me.txtSucursalCuentaContable.TabIndex = 21
        Me.txtSucursalCuentaContable.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSucursalCuentaContable.Texto = ""
        '
        'lblCuentaContable
        '
        Me.lblCuentaContable.AutoSize = True
        Me.lblCuentaContable.Location = New System.Drawing.Point(151, 266)
        Me.lblCuentaContable.Name = "lblCuentaContable"
        Me.lblCuentaContable.Size = New System.Drawing.Size(33, 13)
        Me.lblCuentaContable.TabIndex = 20
        Me.lblCuentaContable.Text = "C. C.:"
        '
        'txtSucursalCodigo
        '
        Me.txtSucursalCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSucursalCodigo.Color = System.Drawing.Color.Empty
        Me.txtSucursalCodigo.Indicaciones = Nothing
        Me.txtSucursalCodigo.Location = New System.Drawing.Point(82, 59)
        Me.txtSucursalCodigo.Multilinea = False
        Me.txtSucursalCodigo.Name = "txtSucursalCodigo"
        Me.txtSucursalCodigo.Size = New System.Drawing.Size(61, 21)
        Me.txtSucursalCodigo.SoloLectura = False
        Me.txtSucursalCodigo.TabIndex = 5
        Me.txtSucursalCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSucursalCodigo.Texto = ""
        '
        'lblSucursalCodigo
        '
        Me.lblSucursalCodigo.AutoSize = True
        Me.lblSucursalCodigo.Location = New System.Drawing.Point(3, 63)
        Me.lblSucursalCodigo.Name = "lblSucursalCodigo"
        Me.lblSucursalCodigo.Size = New System.Drawing.Size(43, 13)
        Me.lblSucursalCodigo.TabIndex = 4
        Me.lblSucursalCodigo.Text = "Codigo:"
        '
        'txtSucursalReferencia
        '
        Me.txtSucursalReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSucursalReferencia.Color = System.Drawing.Color.Empty
        Me.txtSucursalReferencia.Indicaciones = Nothing
        Me.txtSucursalReferencia.Location = New System.Drawing.Point(84, 262)
        Me.txtSucursalReferencia.Multilinea = False
        Me.txtSucursalReferencia.Name = "txtSucursalReferencia"
        Me.txtSucursalReferencia.Size = New System.Drawing.Size(61, 21)
        Me.txtSucursalReferencia.SoloLectura = False
        Me.txtSucursalReferencia.TabIndex = 19
        Me.txtSucursalReferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSucursalReferencia.Texto = ""
        '
        'lblSucursalReferencia
        '
        Me.lblSucursalReferencia.AutoSize = True
        Me.lblSucursalReferencia.Location = New System.Drawing.Point(5, 266)
        Me.lblSucursalReferencia.Name = "lblSucursalReferencia"
        Me.lblSucursalReferencia.Size = New System.Drawing.Size(62, 13)
        Me.lblSucursalReferencia.TabIndex = 18
        Me.lblSucursalReferencia.Text = "Referencia:"
        '
        'txtSucursalTelefono
        '
        Me.txtSucursalTelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSucursalTelefono.Color = System.Drawing.Color.Empty
        Me.txtSucursalTelefono.Indicaciones = Nothing
        Me.txtSucursalTelefono.Location = New System.Drawing.Point(82, 194)
        Me.txtSucursalTelefono.Multilinea = False
        Me.txtSucursalTelefono.Name = "txtSucursalTelefono"
        Me.txtSucursalTelefono.Size = New System.Drawing.Size(251, 21)
        Me.txtSucursalTelefono.SoloLectura = False
        Me.txtSucursalTelefono.TabIndex = 17
        Me.txtSucursalTelefono.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSucursalTelefono.Texto = ""
        '
        'lblSucursalTelefono
        '
        Me.lblSucursalTelefono.AutoSize = True
        Me.lblSucursalTelefono.Location = New System.Drawing.Point(3, 198)
        Me.lblSucursalTelefono.Name = "lblSucursalTelefono"
        Me.lblSucursalTelefono.Size = New System.Drawing.Size(52, 13)
        Me.lblSucursalTelefono.TabIndex = 16
        Me.lblSucursalTelefono.Text = "Telefono:"
        '
        'txtSucursalDireccion
        '
        Me.txtSucursalDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSucursalDireccion.Color = System.Drawing.Color.Empty
        Me.txtSucursalDireccion.Indicaciones = Nothing
        Me.txtSucursalDireccion.Location = New System.Drawing.Point(82, 167)
        Me.txtSucursalDireccion.Multilinea = False
        Me.txtSucursalDireccion.Name = "txtSucursalDireccion"
        Me.txtSucursalDireccion.Size = New System.Drawing.Size(318, 21)
        Me.txtSucursalDireccion.SoloLectura = False
        Me.txtSucursalDireccion.TabIndex = 15
        Me.txtSucursalDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSucursalDireccion.Texto = ""
        '
        'lblSucursalDireccion
        '
        Me.lblSucursalDireccion.AutoSize = True
        Me.lblSucursalDireccion.Location = New System.Drawing.Point(3, 171)
        Me.lblSucursalDireccion.Name = "lblSucursalDireccion"
        Me.lblSucursalDireccion.Size = New System.Drawing.Size(55, 13)
        Me.lblSucursalDireccion.TabIndex = 14
        Me.lblSucursalDireccion.Text = "Direccion:"
        '
        'cbxSucursalCiudad
        '
        Me.cbxSucursalCiudad.CampoWhere = Nothing
        Me.cbxSucursalCiudad.CargarUnaSolaVez = False
        Me.cbxSucursalCiudad.DataDisplayMember = Nothing
        Me.cbxSucursalCiudad.DataFilter = Nothing
        Me.cbxSucursalCiudad.DataOrderBy = Nothing
        Me.cbxSucursalCiudad.DataSource = Nothing
        Me.cbxSucursalCiudad.DataValueMember = Nothing
        Me.cbxSucursalCiudad.dtSeleccionado = Nothing
        Me.cbxSucursalCiudad.FormABM = Nothing
        Me.cbxSucursalCiudad.Indicaciones = Nothing
        Me.cbxSucursalCiudad.Location = New System.Drawing.Point(82, 140)
        Me.cbxSucursalCiudad.Name = "cbxSucursalCiudad"
        Me.cbxSucursalCiudad.SeleccionMultiple = False
        Me.cbxSucursalCiudad.SeleccionObligatoria = False
        Me.cbxSucursalCiudad.Size = New System.Drawing.Size(251, 21)
        Me.cbxSucursalCiudad.SoloLectura = False
        Me.cbxSucursalCiudad.TabIndex = 13
        Me.cbxSucursalCiudad.Texto = ""
        '
        'lblSucursalCiudad
        '
        Me.lblSucursalCiudad.AutoSize = True
        Me.lblSucursalCiudad.Location = New System.Drawing.Point(3, 144)
        Me.lblSucursalCiudad.Name = "lblSucursalCiudad"
        Me.lblSucursalCiudad.Size = New System.Drawing.Size(43, 13)
        Me.lblSucursalCiudad.TabIndex = 12
        Me.lblSucursalCiudad.Text = "Ciudad:"
        '
        'cbxSucursalDepartamento
        '
        Me.cbxSucursalDepartamento.CampoWhere = Nothing
        Me.cbxSucursalDepartamento.CargarUnaSolaVez = False
        Me.cbxSucursalDepartamento.DataDisplayMember = Nothing
        Me.cbxSucursalDepartamento.DataFilter = Nothing
        Me.cbxSucursalDepartamento.DataOrderBy = Nothing
        Me.cbxSucursalDepartamento.DataSource = Nothing
        Me.cbxSucursalDepartamento.DataValueMember = Nothing
        Me.cbxSucursalDepartamento.dtSeleccionado = Nothing
        Me.cbxSucursalDepartamento.FormABM = Nothing
        Me.cbxSucursalDepartamento.Indicaciones = Nothing
        Me.cbxSucursalDepartamento.Location = New System.Drawing.Point(82, 113)
        Me.cbxSucursalDepartamento.Name = "cbxSucursalDepartamento"
        Me.cbxSucursalDepartamento.SeleccionMultiple = False
        Me.cbxSucursalDepartamento.SeleccionObligatoria = False
        Me.cbxSucursalDepartamento.Size = New System.Drawing.Size(251, 21)
        Me.cbxSucursalDepartamento.SoloLectura = False
        Me.cbxSucursalDepartamento.TabIndex = 11
        Me.cbxSucursalDepartamento.Texto = ""
        '
        'cbxSucursalPais
        '
        Me.cbxSucursalPais.CampoWhere = Nothing
        Me.cbxSucursalPais.CargarUnaSolaVez = False
        Me.cbxSucursalPais.DataDisplayMember = Nothing
        Me.cbxSucursalPais.DataFilter = Nothing
        Me.cbxSucursalPais.DataOrderBy = Nothing
        Me.cbxSucursalPais.DataSource = Nothing
        Me.cbxSucursalPais.DataValueMember = Nothing
        Me.cbxSucursalPais.dtSeleccionado = Nothing
        Me.cbxSucursalPais.FormABM = Nothing
        Me.cbxSucursalPais.Indicaciones = Nothing
        Me.cbxSucursalPais.Location = New System.Drawing.Point(82, 86)
        Me.cbxSucursalPais.Name = "cbxSucursalPais"
        Me.cbxSucursalPais.SeleccionMultiple = False
        Me.cbxSucursalPais.SeleccionObligatoria = False
        Me.cbxSucursalPais.Size = New System.Drawing.Size(251, 21)
        Me.cbxSucursalPais.SoloLectura = False
        Me.cbxSucursalPais.TabIndex = 9
        Me.cbxSucursalPais.Texto = ""
        '
        'lblSucursalDepartamento
        '
        Me.lblSucursalDepartamento.AutoSize = True
        Me.lblSucursalDepartamento.Location = New System.Drawing.Point(3, 117)
        Me.lblSucursalDepartamento.Name = "lblSucursalDepartamento"
        Me.lblSucursalDepartamento.Size = New System.Drawing.Size(77, 13)
        Me.lblSucursalDepartamento.TabIndex = 10
        Me.lblSucursalDepartamento.Text = "Departamento:"
        '
        'lblSucursalPais
        '
        Me.lblSucursalPais.AutoSize = True
        Me.lblSucursalPais.Location = New System.Drawing.Point(3, 90)
        Me.lblSucursalPais.Name = "lblSucursalPais"
        Me.lblSucursalPais.Size = New System.Drawing.Size(30, 13)
        Me.lblSucursalPais.TabIndex = 8
        Me.lblSucursalPais.Text = "Pais:"
        '
        'txtSucursalDescripcion
        '
        Me.txtSucursalDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSucursalDescripcion.Color = System.Drawing.Color.Empty
        Me.txtSucursalDescripcion.Indicaciones = Nothing
        Me.txtSucursalDescripcion.Location = New System.Drawing.Point(82, 32)
        Me.txtSucursalDescripcion.Multilinea = False
        Me.txtSucursalDescripcion.Name = "txtSucursalDescripcion"
        Me.txtSucursalDescripcion.Size = New System.Drawing.Size(251, 21)
        Me.txtSucursalDescripcion.SoloLectura = False
        Me.txtSucursalDescripcion.TabIndex = 3
        Me.txtSucursalDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSucursalDescripcion.Texto = ""
        '
        'btnSucursalCancelar
        '
        Me.btnSucursalCancelar.Location = New System.Drawing.Point(327, 312)
        Me.btnSucursalCancelar.Name = "btnSucursalCancelar"
        Me.btnSucursalCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnSucursalCancelar.TabIndex = 29
        Me.btnSucursalCancelar.Text = "&Cancelar"
        Me.btnSucursalCancelar.UseVisualStyleBackColor = True
        '
        'btnSucursalEliminar
        '
        Me.btnSucursalEliminar.Location = New System.Drawing.Point(408, 312)
        Me.btnSucursalEliminar.Name = "btnSucursalEliminar"
        Me.btnSucursalEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnSucursalEliminar.TabIndex = 30
        Me.btnSucursalEliminar.Text = "E&liminar"
        Me.btnSucursalEliminar.UseVisualStyleBackColor = True
        '
        'rdbSucursalDesactivado
        '
        Me.rdbSucursalDesactivado.AutoSize = True
        Me.rdbSucursalDesactivado.Location = New System.Drawing.Point(145, 289)
        Me.rdbSucursalDesactivado.Name = "rdbSucursalDesactivado"
        Me.rdbSucursalDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbSucursalDesactivado.TabIndex = 25
        Me.rdbSucursalDesactivado.TabStop = True
        Me.rdbSucursalDesactivado.Text = "Desactivado"
        Me.rdbSucursalDesactivado.UseVisualStyleBackColor = True
        '
        'rdbSucursalActivo
        '
        Me.rdbSucursalActivo.AutoSize = True
        Me.rdbSucursalActivo.Location = New System.Drawing.Point(84, 289)
        Me.rdbSucursalActivo.Name = "rdbSucursalActivo"
        Me.rdbSucursalActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbSucursalActivo.TabIndex = 24
        Me.rdbSucursalActivo.TabStop = True
        Me.rdbSucursalActivo.Text = "Activo"
        Me.rdbSucursalActivo.UseVisualStyleBackColor = True
        '
        'lblSucursalEstado
        '
        Me.lblSucursalEstado.AutoSize = True
        Me.lblSucursalEstado.Location = New System.Drawing.Point(5, 291)
        Me.lblSucursalEstado.Name = "lblSucursalEstado"
        Me.lblSucursalEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblSucursalEstado.TabIndex = 23
        Me.lblSucursalEstado.Text = "Estado:"
        '
        'btnSucursalGuardar
        '
        Me.btnSucursalGuardar.Location = New System.Drawing.Point(246, 312)
        Me.btnSucursalGuardar.Name = "btnSucursalGuardar"
        Me.btnSucursalGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnSucursalGuardar.TabIndex = 28
        Me.btnSucursalGuardar.Text = "&Guardar"
        Me.btnSucursalGuardar.UseVisualStyleBackColor = True
        '
        'btnSucursalEditar
        '
        Me.btnSucursalEditar.Location = New System.Drawing.Point(165, 312)
        Me.btnSucursalEditar.Name = "btnSucursalEditar"
        Me.btnSucursalEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnSucursalEditar.TabIndex = 27
        Me.btnSucursalEditar.Text = "&Editar"
        Me.btnSucursalEditar.UseVisualStyleBackColor = True
        '
        'btnSucursalNuevo
        '
        Me.btnSucursalNuevo.Location = New System.Drawing.Point(84, 312)
        Me.btnSucursalNuevo.Name = "btnSucursalNuevo"
        Me.btnSucursalNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnSucursalNuevo.TabIndex = 26
        Me.btnSucursalNuevo.Text = "&Nuevo"
        Me.btnSucursalNuevo.UseVisualStyleBackColor = True
        '
        'lblSucursalDescripcion
        '
        Me.lblSucursalDescripcion.AutoSize = True
        Me.lblSucursalDescripcion.Location = New System.Drawing.Point(3, 36)
        Me.lblSucursalDescripcion.Name = "lblSucursalDescripcion"
        Me.lblSucursalDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblSucursalDescripcion.TabIndex = 2
        Me.lblSucursalDescripcion.Text = "Descripción:"
        '
        'txtSucursalID
        '
        Me.txtSucursalID.BackColor = System.Drawing.Color.White
        Me.txtSucursalID.Location = New System.Drawing.Point(82, 6)
        Me.txtSucursalID.Name = "txtSucursalID"
        Me.txtSucursalID.ReadOnly = True
        Me.txtSucursalID.Size = New System.Drawing.Size(61, 20)
        Me.txtSucursalID.TabIndex = 1
        '
        'lblSucursalID
        '
        Me.lblSucursalID.AutoSize = True
        Me.lblSucursalID.Location = New System.Drawing.Point(3, 10)
        Me.lblSucursalID.Name = "lblSucursalID"
        Me.lblSucursalID.Size = New System.Drawing.Size(21, 13)
        Me.lblSucursalID.TabIndex = 0
        Me.lblSucursalID.Text = "ID:"
        '
        'tpDeposito
        '
        Me.tpDeposito.Controls.Add(Me.chkConsumoInformatica)
        Me.tpDeposito.Controls.Add(Me.cbxSucursalDestino)
        Me.tpDeposito.Controls.Add(Me.Label4)
        Me.tpDeposito.Controls.Add(Me.Label3)
        Me.tpDeposito.Controls.Add(Me.txtCuentaContableCombustible)
        Me.tpDeposito.Controls.Add(Me.dgvDeposito)
        Me.tpDeposito.Controls.Add(Me.chkDescargaCompra)
        Me.tpDeposito.Controls.Add(Me.chkMateriaPrima)
        Me.tpDeposito.Controls.Add(Me.chkConsumo)
        Me.tpDeposito.Controls.Add(Me.chkDescargaStock)
        Me.tpDeposito.Controls.Add(Me.chkCompra)
        Me.tpDeposito.Controls.Add(Me.chkVenta)
        Me.tpDeposito.Controls.Add(Me.cbxTipoDeposito)
        Me.tpDeposito.Controls.Add(Me.Label2)
        Me.tpDeposito.Controls.Add(Me.lblCuentaContableMercaderia)
        Me.tpDeposito.Controls.Add(Me.txtCuentaContableMercaderia)
        Me.tpDeposito.Controls.Add(Me.cbxDepositoSucursal)
        Me.tpDeposito.Controls.Add(Me.lblDepositoSucursal)
        Me.tpDeposito.Controls.Add(Me.txtDepositoDescripcion)
        Me.tpDeposito.Controls.Add(Me.btnDepositoCancelar)
        Me.tpDeposito.Controls.Add(Me.btnDepositoEliminar)
        Me.tpDeposito.Controls.Add(Me.rdbDepositoDesactivado)
        Me.tpDeposito.Controls.Add(Me.rdbDepositoActivo)
        Me.tpDeposito.Controls.Add(Me.lblDepositoEstado)
        Me.tpDeposito.Controls.Add(Me.btnDepositoGuardar)
        Me.tpDeposito.Controls.Add(Me.btnDepositoEditar)
        Me.tpDeposito.Controls.Add(Me.btnDepositoNuevo)
        Me.tpDeposito.Controls.Add(Me.lblDepositoDescripcion)
        Me.tpDeposito.Controls.Add(Me.txtDepositoID)
        Me.tpDeposito.Controls.Add(Me.lblDepositoID)
        Me.tpDeposito.Location = New System.Drawing.Point(4, 22)
        Me.tpDeposito.Name = "tpDeposito"
        Me.tpDeposito.Size = New System.Drawing.Size(486, 494)
        Me.tpDeposito.TabIndex = 2
        Me.tpDeposito.Text = "Depositos"
        Me.tpDeposito.UseVisualStyleBackColor = True
        '
        'chkConsumoInformatica
        '
        Me.chkConsumoInformatica.BackColor = System.Drawing.Color.Transparent
        Me.chkConsumoInformatica.Color = System.Drawing.Color.Empty
        Me.chkConsumoInformatica.Location = New System.Drawing.Point(295, 115)
        Me.chkConsumoInformatica.Name = "chkConsumoInformatica"
        Me.chkConsumoInformatica.Size = New System.Drawing.Size(148, 21)
        Me.chkConsumoInformatica.SoloLectura = False
        Me.chkConsumoInformatica.TabIndex = 30
        Me.chkConsumoInformatica.Texto = "Consumo Informatica"
        Me.chkConsumoInformatica.Valor = False
        '
        'cbxSucursalDestino
        '
        Me.cbxSucursalDestino.CampoWhere = Nothing
        Me.cbxSucursalDestino.CargarUnaSolaVez = False
        Me.cbxSucursalDestino.DataDisplayMember = Nothing
        Me.cbxSucursalDestino.DataFilter = Nothing
        Me.cbxSucursalDestino.DataOrderBy = Nothing
        Me.cbxSucursalDestino.DataSource = Nothing
        Me.cbxSucursalDestino.DataValueMember = Nothing
        Me.cbxSucursalDestino.dtSeleccionado = Nothing
        Me.cbxSucursalDestino.FormABM = Nothing
        Me.cbxSucursalDestino.Indicaciones = Nothing
        Me.cbxSucursalDestino.Location = New System.Drawing.Point(82, 119)
        Me.cbxSucursalDestino.Name = "cbxSucursalDestino"
        Me.cbxSucursalDestino.SeleccionMultiple = False
        Me.cbxSucursalDestino.SeleccionObligatoria = False
        Me.cbxSucursalDestino.Size = New System.Drawing.Size(197, 21)
        Me.cbxSucursalDestino.SoloLectura = False
        Me.cbxSucursalDestino.TabIndex = 29
        Me.cbxSucursalDestino.Texto = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 123)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 13)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Suc Destino:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 196)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 13)
        Me.Label3.TabIndex = 26
        Me.Label3.Text = "C.C. Comb.:"
        '
        'txtCuentaContableCombustible
        '
        Me.txtCuentaContableCombustible.AlturaMaxima = 319
        Me.txtCuentaContableCombustible.Consulta = Nothing
        Me.txtCuentaContableCombustible.IDCentroCosto = 0
        Me.txtCuentaContableCombustible.IDUnidadNegocio = 0
        Me.txtCuentaContableCombustible.ListarTodas = False
        Me.txtCuentaContableCombustible.Location = New System.Drawing.Point(82, 190)
        Me.txtCuentaContableCombustible.Name = "txtCuentaContableCombustible"
        Me.txtCuentaContableCombustible.Registro = Nothing
        Me.txtCuentaContableCombustible.Resolucion173 = False
        Me.txtCuentaContableCombustible.Seleccionado = False
        Me.txtCuentaContableCombustible.Size = New System.Drawing.Size(399, 24)
        Me.txtCuentaContableCombustible.SoloLectura = False
        Me.txtCuentaContableCombustible.TabIndex = 27
        Me.txtCuentaContableCombustible.Texto = Nothing
        Me.txtCuentaContableCombustible.whereFiltro = Nothing
        '
        'dgvDeposito
        '
        Me.dgvDeposito.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDeposito.Location = New System.Drawing.Point(82, 271)
        Me.dgvDeposito.Name = "dgvDeposito"
        Me.dgvDeposito.ReadOnly = True
        Me.dgvDeposito.Size = New System.Drawing.Size(399, 220)
        Me.dgvDeposito.TabIndex = 25
        '
        'chkDescargaCompra
        '
        Me.chkDescargaCompra.BackColor = System.Drawing.Color.Transparent
        Me.chkDescargaCompra.Color = System.Drawing.Color.Empty
        Me.chkDescargaCompra.Location = New System.Drawing.Point(295, 90)
        Me.chkDescargaCompra.Name = "chkDescargaCompra"
        Me.chkDescargaCompra.Size = New System.Drawing.Size(148, 21)
        Me.chkDescargaCompra.SoloLectura = False
        Me.chkDescargaCompra.TabIndex = 24
        Me.chkDescargaCompra.Texto = "Descarga de Compra"
        Me.chkDescargaCompra.Valor = False
        '
        'chkMateriaPrima
        '
        Me.chkMateriaPrima.BackColor = System.Drawing.Color.Transparent
        Me.chkMateriaPrima.Color = System.Drawing.Color.Empty
        Me.chkMateriaPrima.Location = New System.Drawing.Point(295, 69)
        Me.chkMateriaPrima.Name = "chkMateriaPrima"
        Me.chkMateriaPrima.Size = New System.Drawing.Size(148, 21)
        Me.chkMateriaPrima.SoloLectura = False
        Me.chkMateriaPrima.TabIndex = 23
        Me.chkMateriaPrima.Texto = "Mat.Prima Balanceado"
        Me.chkMateriaPrima.Valor = False
        '
        'chkConsumo
        '
        Me.chkConsumo.BackColor = System.Drawing.Color.Transparent
        Me.chkConsumo.Color = System.Drawing.Color.Empty
        Me.chkConsumo.Location = New System.Drawing.Point(295, 48)
        Me.chkConsumo.Name = "chkConsumo"
        Me.chkConsumo.Size = New System.Drawing.Size(148, 21)
        Me.chkConsumo.SoloLectura = False
        Me.chkConsumo.TabIndex = 22
        Me.chkConsumo.Texto = "Consumo Combustible"
        Me.chkConsumo.Valor = False
        '
        'chkDescargaStock
        '
        Me.chkDescargaStock.BackColor = System.Drawing.Color.Transparent
        Me.chkDescargaStock.Color = System.Drawing.Color.Empty
        Me.chkDescargaStock.Location = New System.Drawing.Point(295, 27)
        Me.chkDescargaStock.Name = "chkDescargaStock"
        Me.chkDescargaStock.Size = New System.Drawing.Size(148, 17)
        Me.chkDescargaStock.SoloLectura = False
        Me.chkDescargaStock.TabIndex = 21
        Me.chkDescargaStock.Texto = "Descarga Stock"
        Me.chkDescargaStock.Valor = False
        '
        'chkCompra
        '
        Me.chkCompra.BackColor = System.Drawing.Color.Transparent
        Me.chkCompra.Color = System.Drawing.Color.Empty
        Me.chkCompra.Location = New System.Drawing.Point(386, 6)
        Me.chkCompra.Name = "chkCompra"
        Me.chkCompra.Size = New System.Drawing.Size(83, 21)
        Me.chkCompra.SoloLectura = False
        Me.chkCompra.TabIndex = 20
        Me.chkCompra.Texto = "Compra"
        Me.chkCompra.Valor = False
        '
        'chkVenta
        '
        Me.chkVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkVenta.Color = System.Drawing.Color.Empty
        Me.chkVenta.Location = New System.Drawing.Point(295, 6)
        Me.chkVenta.Name = "chkVenta"
        Me.chkVenta.Size = New System.Drawing.Size(63, 21)
        Me.chkVenta.SoloLectura = False
        Me.chkVenta.TabIndex = 19
        Me.chkVenta.Texto = "Venta"
        Me.chkVenta.Valor = False
        '
        'cbxTipoDeposito
        '
        Me.cbxTipoDeposito.CampoWhere = Nothing
        Me.cbxTipoDeposito.CargarUnaSolaVez = False
        Me.cbxTipoDeposito.DataDisplayMember = Nothing
        Me.cbxTipoDeposito.DataFilter = Nothing
        Me.cbxTipoDeposito.DataOrderBy = Nothing
        Me.cbxTipoDeposito.DataSource = Nothing
        Me.cbxTipoDeposito.DataValueMember = Nothing
        Me.cbxTipoDeposito.dtSeleccionado = Nothing
        Me.cbxTipoDeposito.FormABM = Nothing
        Me.cbxTipoDeposito.Indicaciones = Nothing
        Me.cbxTipoDeposito.Location = New System.Drawing.Point(82, 59)
        Me.cbxTipoDeposito.Name = "cbxTipoDeposito"
        Me.cbxTipoDeposito.SeleccionMultiple = False
        Me.cbxTipoDeposito.SeleccionObligatoria = False
        Me.cbxTipoDeposito.Size = New System.Drawing.Size(197, 21)
        Me.cbxTipoDeposito.SoloLectura = False
        Me.cbxTipoDeposito.TabIndex = 18
        Me.cbxTipoDeposito.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Tipo.Depo:"
        '
        'lblCuentaContableMercaderia
        '
        Me.lblCuentaContableMercaderia.AutoSize = True
        Me.lblCuentaContableMercaderia.Location = New System.Drawing.Point(3, 166)
        Me.lblCuentaContableMercaderia.Name = "lblCuentaContableMercaderia"
        Me.lblCuentaContableMercaderia.Size = New System.Drawing.Size(60, 13)
        Me.lblCuentaContableMercaderia.TabIndex = 6
        Me.lblCuentaContableMercaderia.Text = "C.C. Merc.:"
        '
        'txtCuentaContableMercaderia
        '
        Me.txtCuentaContableMercaderia.AlturaMaxima = 319
        Me.txtCuentaContableMercaderia.Consulta = Nothing
        Me.txtCuentaContableMercaderia.IDCentroCosto = 0
        Me.txtCuentaContableMercaderia.IDUnidadNegocio = 0
        Me.txtCuentaContableMercaderia.ListarTodas = False
        Me.txtCuentaContableMercaderia.Location = New System.Drawing.Point(82, 160)
        Me.txtCuentaContableMercaderia.Name = "txtCuentaContableMercaderia"
        Me.txtCuentaContableMercaderia.Registro = Nothing
        Me.txtCuentaContableMercaderia.Resolucion173 = False
        Me.txtCuentaContableMercaderia.Seleccionado = False
        Me.txtCuentaContableMercaderia.Size = New System.Drawing.Size(399, 24)
        Me.txtCuentaContableMercaderia.SoloLectura = False
        Me.txtCuentaContableMercaderia.TabIndex = 7
        Me.txtCuentaContableMercaderia.Texto = Nothing
        Me.txtCuentaContableMercaderia.whereFiltro = Nothing
        '
        'cbxDepositoSucursal
        '
        Me.cbxDepositoSucursal.CampoWhere = Nothing
        Me.cbxDepositoSucursal.CargarUnaSolaVez = False
        Me.cbxDepositoSucursal.DataDisplayMember = Nothing
        Me.cbxDepositoSucursal.DataFilter = Nothing
        Me.cbxDepositoSucursal.DataOrderBy = Nothing
        Me.cbxDepositoSucursal.DataSource = Nothing
        Me.cbxDepositoSucursal.DataValueMember = Nothing
        Me.cbxDepositoSucursal.dtSeleccionado = Nothing
        Me.cbxDepositoSucursal.FormABM = Nothing
        Me.cbxDepositoSucursal.Indicaciones = Nothing
        Me.cbxDepositoSucursal.Location = New System.Drawing.Point(82, 32)
        Me.cbxDepositoSucursal.Name = "cbxDepositoSucursal"
        Me.cbxDepositoSucursal.SeleccionMultiple = False
        Me.cbxDepositoSucursal.SeleccionObligatoria = False
        Me.cbxDepositoSucursal.Size = New System.Drawing.Size(197, 21)
        Me.cbxDepositoSucursal.SoloLectura = False
        Me.cbxDepositoSucursal.TabIndex = 3
        Me.cbxDepositoSucursal.Texto = ""
        '
        'lblDepositoSucursal
        '
        Me.lblDepositoSucursal.AutoSize = True
        Me.lblDepositoSucursal.Location = New System.Drawing.Point(3, 36)
        Me.lblDepositoSucursal.Name = "lblDepositoSucursal"
        Me.lblDepositoSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblDepositoSucursal.TabIndex = 2
        Me.lblDepositoSucursal.Text = "Sucursal:"
        '
        'txtDepositoDescripcion
        '
        Me.txtDepositoDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDepositoDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDepositoDescripcion.Indicaciones = Nothing
        Me.txtDepositoDescripcion.Location = New System.Drawing.Point(82, 88)
        Me.txtDepositoDescripcion.Multilinea = False
        Me.txtDepositoDescripcion.Name = "txtDepositoDescripcion"
        Me.txtDepositoDescripcion.Size = New System.Drawing.Size(197, 21)
        Me.txtDepositoDescripcion.SoloLectura = False
        Me.txtDepositoDescripcion.TabIndex = 5
        Me.txtDepositoDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDepositoDescripcion.Texto = ""
        '
        'btnDepositoCancelar
        '
        Me.btnDepositoCancelar.Location = New System.Drawing.Point(325, 244)
        Me.btnDepositoCancelar.Name = "btnDepositoCancelar"
        Me.btnDepositoCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnDepositoCancelar.TabIndex = 14
        Me.btnDepositoCancelar.Text = "&Cancelar"
        Me.btnDepositoCancelar.UseVisualStyleBackColor = True
        '
        'btnDepositoEliminar
        '
        Me.btnDepositoEliminar.Location = New System.Drawing.Point(406, 244)
        Me.btnDepositoEliminar.Name = "btnDepositoEliminar"
        Me.btnDepositoEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnDepositoEliminar.TabIndex = 15
        Me.btnDepositoEliminar.Text = "E&liminar"
        Me.btnDepositoEliminar.UseVisualStyleBackColor = True
        '
        'rdbDepositoDesactivado
        '
        Me.rdbDepositoDesactivado.AutoSize = True
        Me.rdbDepositoDesactivado.Location = New System.Drawing.Point(143, 221)
        Me.rdbDepositoDesactivado.Name = "rdbDepositoDesactivado"
        Me.rdbDepositoDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDepositoDesactivado.TabIndex = 10
        Me.rdbDepositoDesactivado.TabStop = True
        Me.rdbDepositoDesactivado.Text = "Desactivado"
        Me.rdbDepositoDesactivado.UseVisualStyleBackColor = True
        '
        'rdbDepositoActivo
        '
        Me.rdbDepositoActivo.AutoSize = True
        Me.rdbDepositoActivo.Location = New System.Drawing.Point(82, 221)
        Me.rdbDepositoActivo.Name = "rdbDepositoActivo"
        Me.rdbDepositoActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbDepositoActivo.TabIndex = 9
        Me.rdbDepositoActivo.TabStop = True
        Me.rdbDepositoActivo.Text = "Activo"
        Me.rdbDepositoActivo.UseVisualStyleBackColor = True
        '
        'lblDepositoEstado
        '
        Me.lblDepositoEstado.AutoSize = True
        Me.lblDepositoEstado.Location = New System.Drawing.Point(3, 223)
        Me.lblDepositoEstado.Name = "lblDepositoEstado"
        Me.lblDepositoEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblDepositoEstado.TabIndex = 8
        Me.lblDepositoEstado.Text = "Estado:"
        '
        'btnDepositoGuardar
        '
        Me.btnDepositoGuardar.Location = New System.Drawing.Point(244, 244)
        Me.btnDepositoGuardar.Name = "btnDepositoGuardar"
        Me.btnDepositoGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnDepositoGuardar.TabIndex = 13
        Me.btnDepositoGuardar.Text = "&Guardar"
        Me.btnDepositoGuardar.UseVisualStyleBackColor = True
        '
        'btnDepositoEditar
        '
        Me.btnDepositoEditar.Location = New System.Drawing.Point(163, 244)
        Me.btnDepositoEditar.Name = "btnDepositoEditar"
        Me.btnDepositoEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnDepositoEditar.TabIndex = 12
        Me.btnDepositoEditar.Text = "&Editar"
        Me.btnDepositoEditar.UseVisualStyleBackColor = True
        '
        'btnDepositoNuevo
        '
        Me.btnDepositoNuevo.Location = New System.Drawing.Point(82, 244)
        Me.btnDepositoNuevo.Name = "btnDepositoNuevo"
        Me.btnDepositoNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnDepositoNuevo.TabIndex = 11
        Me.btnDepositoNuevo.Text = "&Nuevo"
        Me.btnDepositoNuevo.UseVisualStyleBackColor = True
        '
        'lblDepositoDescripcion
        '
        Me.lblDepositoDescripcion.AutoSize = True
        Me.lblDepositoDescripcion.Location = New System.Drawing.Point(3, 92)
        Me.lblDepositoDescripcion.Name = "lblDepositoDescripcion"
        Me.lblDepositoDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDepositoDescripcion.TabIndex = 4
        Me.lblDepositoDescripcion.Text = "Descripción:"
        '
        'txtDepositoID
        '
        Me.txtDepositoID.BackColor = System.Drawing.Color.White
        Me.txtDepositoID.Location = New System.Drawing.Point(82, 6)
        Me.txtDepositoID.Name = "txtDepositoID"
        Me.txtDepositoID.ReadOnly = True
        Me.txtDepositoID.Size = New System.Drawing.Size(61, 20)
        Me.txtDepositoID.TabIndex = 1
        '
        'lblDepositoID
        '
        Me.lblDepositoID.AutoSize = True
        Me.lblDepositoID.Location = New System.Drawing.Point(3, 10)
        Me.lblDepositoID.Name = "lblDepositoID"
        Me.lblDepositoID.Size = New System.Drawing.Size(21, 13)
        Me.lblDepositoID.TabIndex = 0
        Me.lblDepositoID.Text = "ID:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 605)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(513, 22)
        Me.StatusStrip1.TabIndex = 2
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(427, 579)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'txtNroCasa
        '
        Me.txtNroCasa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroCasa.Color = System.Drawing.Color.Empty
        Me.txtNroCasa.Indicaciones = Nothing
        Me.txtNroCasa.Location = New System.Drawing.Point(82, 219)
        Me.txtNroCasa.Multilinea = False
        Me.txtNroCasa.Name = "txtNroCasa"
        Me.txtNroCasa.Size = New System.Drawing.Size(77, 21)
        Me.txtNroCasa.SoloLectura = False
        Me.txtNroCasa.TabIndex = 33
        Me.txtNroCasa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroCasa.Texto = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 223)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(57, 13)
        Me.Label5.TabIndex = 32
        Me.Label5.Text = "Nro. Casa:"
        '
        'frmSucursalDeposito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnSalir
        Me.ClientSize = New System.Drawing.Size(513, 627)
        Me.Controls.Add(Me.tbcGeneral)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnSalir)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmSucursalDeposito"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "SUCURSALES Y DEPOSITOS"
        Me.Text = "SUCURSALES Y DEPOSITOS"
        Me.tbcGeneral.ResumeLayout(False)
        Me.tpSucursal.ResumeLayout(False)
        Me.tpSucursal.PerformLayout()
        CType(Me.dgvSucursal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpDeposito.ResumeLayout(False)
        Me.tpDeposito.PerformLayout()
        CType(Me.dgvDeposito, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbcGeneral As System.Windows.Forms.TabControl
    Friend WithEvents tpDeposito As System.Windows.Forms.TabPage
    Friend WithEvents txtDepositoDescripcion As ERP.ocxTXTString
    Friend WithEvents btnDepositoCancelar As System.Windows.Forms.Button
    Friend WithEvents btnDepositoEliminar As System.Windows.Forms.Button
    Friend WithEvents rdbDepositoDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbDepositoActivo As System.Windows.Forms.RadioButton
    Friend WithEvents lblDepositoEstado As System.Windows.Forms.Label
    Friend WithEvents btnDepositoGuardar As System.Windows.Forms.Button
    Friend WithEvents btnDepositoEditar As System.Windows.Forms.Button
    Friend WithEvents btnDepositoNuevo As System.Windows.Forms.Button
    Friend WithEvents lblDepositoDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDepositoID As System.Windows.Forms.TextBox
    Friend WithEvents lblDepositoID As System.Windows.Forms.Label
    Friend WithEvents tpSucursal As System.Windows.Forms.TabPage
    Friend WithEvents txtSucursalDescripcion As ERP.ocxTXTString
    Friend WithEvents btnSucursalCancelar As System.Windows.Forms.Button
    Friend WithEvents btnSucursalEliminar As System.Windows.Forms.Button
    Friend WithEvents rdbSucursalDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbSucursalActivo As System.Windows.Forms.RadioButton
    Friend WithEvents lblSucursalEstado As System.Windows.Forms.Label
    Friend WithEvents btnSucursalGuardar As System.Windows.Forms.Button
    Friend WithEvents btnSucursalEditar As System.Windows.Forms.Button
    Friend WithEvents btnSucursalNuevo As System.Windows.Forms.Button
    Friend WithEvents lblSucursalDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtSucursalID As System.Windows.Forms.TextBox
    Friend WithEvents lblSucursalID As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtSucursalReferencia As ERP.ocxTXTString
    Friend WithEvents lblSucursalReferencia As System.Windows.Forms.Label
    Friend WithEvents txtSucursalTelefono As ERP.ocxTXTString
    Friend WithEvents lblSucursalTelefono As System.Windows.Forms.Label
    Friend WithEvents txtSucursalDireccion As ERP.ocxTXTString
    Friend WithEvents lblSucursalDireccion As System.Windows.Forms.Label
    Friend WithEvents cbxSucursalCiudad As ERP.ocxCBX
    Friend WithEvents lblSucursalCiudad As System.Windows.Forms.Label
    Friend WithEvents cbxSucursalDepartamento As ERP.ocxCBX
    Friend WithEvents cbxSucursalPais As ERP.ocxCBX
    Friend WithEvents lblSucursalDepartamento As System.Windows.Forms.Label
    Friend WithEvents lblSucursalPais As System.Windows.Forms.Label
    Friend WithEvents cbxDepositoSucursal As ERP.ocxCBX
    Friend WithEvents lblDepositoSucursal As System.Windows.Forms.Label
    Friend WithEvents txtSucursalCodigo As ERP.ocxTXTString
    Friend WithEvents lblSucursalCodigo As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSucursalCuentaContable As ERP.ocxTXTString
    Friend WithEvents lblCuentaContable As System.Windows.Forms.Label
    Friend WithEvents txtSucursalCodigoDistribuidor As ERP.ocxTXTString
    Friend WithEvents lblCodigoDistribuidor As System.Windows.Forms.Label
    Friend WithEvents lblCuentaContableMercaderia As System.Windows.Forms.Label
    Friend WithEvents txtCuentaContableMercaderia As ERP.ocxTXTCuentaContable
    Friend WithEvents cbxTipoDeposito As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkDescargaStock As ERP.ocxCHK
    Friend WithEvents chkCompra As ERP.ocxCHK
    Friend WithEvents chkVenta As ERP.ocxCHK
    Friend WithEvents chkConsumo As ERP.ocxCHK
    Friend WithEvents chkMateriaPrima As ERP.ocxCHK
    Friend WithEvents chkDescargaCompra As ERP.ocxCHK
    Friend WithEvents dgvDeposito As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSucursal As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As Label
    Friend WithEvents txtCuentaContableCombustible As ocxTXTCuentaContable
    Friend WithEvents cbxSucursalDestino As ocxCBX
    Friend WithEvents Label4 As Label
    Friend WithEvents chkConsumoInformatica As ocxCHK
    Friend WithEvents txtNroCasa As ocxTXTString
    Friend WithEvents Label5 As Label
End Class

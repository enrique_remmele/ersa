﻿Public Class frmFondoFijoCuentaContable
    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Focus
        dgv.Focus()

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        'CSistema.CargaControl(vControles, cbxGrupo)
        CSistema.CargaControl(vControles, ocxCuenta)
        CSistema.CargaControl(vControles, rdbActivo)
        CSistema.CargaControl(vControles, rdbDesactivado)

        ocxCuenta.Conectar()
        'Grupo de FF
        CSistema.SqlToComboBox(cbxGrupo.cbx, CData.GetTable("vGrupo").Copy, "ID", "Descripcion")

        'Cargamos los registos en el lv
        Listar(cbxGrupo.GetValue)

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        'Con este metodo "SqlToDataGrid" el sistema carga automaticamente la consulta SQL en el DataGrid asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el DataGrid.
        'CSistema.SqlToDataGrid(dgv, "Select IDFondoFijo, IDCuentaContable, Codigo, Descripcion, 'Estado'=(Case When Estado = 'True' Then 'Activo' Else 'Inactivo' End) From vFondoFijoCuentaContable where IDFondoFijo = " & ID)
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select IDFondoFijo, IDCuentaContable, Codigo, Descripcion, 'Estado'=(Case When Estado = 'True' Then 'OK' Else '-' End) From vFondoFijoCuentaContable where IDFondoFijo = " & ID)
        CSistema.dtToGrid(dgv, dt)
        dgv.Columns("IDFondoFijo").Visible = False
        dgv.Columns(1).Visible = False
        dgv.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.ReadOnly = True



    End Sub

    Sub ObtenerInformacion()
        If vControles Is Nothing Then
            CargarInformacion()
        End If
        'Validar
        'Si es que se selecciono el registro.
        If dgv.SelectedRows.Count = 0 Then
            ctrError.SetError(dgv, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim IDFondoFijo As Integer
        Dim IDCC As Integer
        IDFondoFijo = dgv.SelectedRows(0).Cells("IDFondoFijo").Value
        IDCC = dgv.SelectedRows(0).Cells("IDCuentaContable").Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VFondoFijoCuentaContable Where IDFondoFijo=" & IDFondoFijo & " and IdCuentaContable = " & IDCC)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            cbxGrupo.txt.Text = oRow("Grupo").ToString
            ocxCuenta.SeleccionarRegistro(oRow("Codigo").ToString)
            ocxCuenta.txtCodigo.Texto = oRow("Codigo").ToString
            ocxCuenta.txtDescripcion.Texto = oRow("Descripcion").ToString

            If CBool(oRow("Estado")) = True Then
                rdbActivo.Checked = True
            Else
                rdbDesactivado.Checked = True
            End If

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub InicializarControles()

        'TextBox
        'cbxGrupo.txt.Text.Clear()

        'RadioButton
        rdbActivo.Checked = True
        ocxCuenta.txtCodigo.Clear()
        ocxCuenta.txtDescripcion.Clear()

        'Funciones
        If vNuevo = False Then
            'cbxGrupo.cbx.SelectedIndex = 1
            'Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From vFondoFijoCuentaContable where IDFondoFijo = " & cbxGrupo.GetValue), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        ocxCuenta.txtCodigo.Focus()


    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Escritura de la Descripcion
        If ocxCuenta.txtCodigo.txt.Text = "" Then
            Dim mensaje As String = "Debe ingresar una Cuenta Contable valida!"
            ctrError.SetError(ocxCuenta.txtCodigo, mensaje)
            ctrError.SetIconAlignment(ocxCuenta.txtCodigo, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Sucursal
        If cbxGrupo.Validar("Seleccione correctamente el Grupo", ctrError, cbxGrupo, tsslEstado) = False Then
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgv.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgv, mensaje)
                ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@IDFondoFijo", cbxGrupo.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCuentaContable", ocxCuenta.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "spFondoFijoCuentaContable", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            Listar(cbxGrupo.GetValue)
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno

            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If
        cbxGrupo.Enabled = True
    End Sub

    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        ocxCuenta.txtCodigo.Enabled = True
        ocxCuenta.txtDescripcion.Enabled = True
        cbxGrupo.Enabled = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(sender As System.Object, e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        ocxCuenta.txtCodigo.Focus()
        ocxCuenta.txtCodigo.SelectAll()
        ocxCuenta.txtCodigo.Enabled = False
        ocxCuenta.txtDescripcion.Enabled = False
        cbxGrupo.Enabled = False
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub dgv_Click(sender As System.Object, e As System.EventArgs) Handles dgv.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub frmFondoFijoCuentaContable_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub cbxGrupo_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxGrupo.PropertyChanged
        Listar(cbxGrupo.GetValue)
    End Sub

    Private Sub cbxGrupo_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxGrupo.TeclaPrecionada
        Listar(cbxGrupo.GetValue)
    End Sub
End Class
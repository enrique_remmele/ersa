﻿Public Class frmCreditoCliente

    Dim CSistema As New CSistema
    Dim IDCliente As Integer

    Sub Inicializar()

        CargarInformacion()
        OcxClientesMapa1.btnEstablecer.Visible = False
        OcxClientesMapa1.btnGuardar.Visible = False
        OcxClientesMapa1.btnCancelar.Visible = False

        'Ir al ultimo
        ManejoTecla(New KeyEventArgs(Keys.End))

    End Sub

    Sub CargarInformacion()

        'Listar Estado de Clientes
        CSistema.SqlToComboBox(cbxEstado.cbx, "Select ID, Descripcion From EstadoCliente Order By Orden Desc, Descripcion Asc")

    End Sub

    Sub ObtenerInformacion(ByVal Campo As String, ByVal Valor As String)

        tsslEstado.Text = ""
        ctrError.Clear()

        Dim dtCliente As DataTable = CSistema.ExecuteToDataTable("Select * from vCliente where " & Campo & " = '" & Valor & "'")

        If dtCliente Is Nothing Then
            Dim Mensaje As String = "Cliente no registrado!"
            ctrError.SetError(txtReferencia, Mensaje)
            ctrError.SetIconAlignment(txtReferencia, ErrorIconAlignment.TopRight)

            tsslEstado.Text = "Atencion!!! " & Mensaje
            Exit Sub
        End If

        If dtCliente.Rows.Count = 0 Then
            Dim Mensaje As String = "Cliente no registrado!"
            ctrError.SetError(txtReferencia, Mensaje)
            ctrError.SetIconAlignment(txtReferencia, ErrorIconAlignment.TopRight)

            tsslEstado.Text = "Atencion!!! " & Mensaje
            Exit Sub
        End If

        Dim oRow As DataRow
        oRow = dtCliente.Rows(0)
        txtReferencia.txt.Text = oRow("Referencia").ToString
        txtRazonSocial.txt.Text = oRow("RazonSocial").ToString
        txtRUC.txt.Text = oRow("RUC").ToString
        rdbContado.Checked = CBool(oRow("Contado").ToString)
        rdbCredito.Checked = CBool(oRow("Credito").ToString)

        'Contacto
        txtNombreFantasia.txt.Text = oRow("NombreFantasia").ToString
        txtDireccion.txt.Text = oRow("Direccion").ToString
        txtTelefonos.txt.Text = oRow("Telefono").ToString
        cbxEstado.cbx.Text = oRow("Estado").ToString
        txtCelulares.txt.Text = oRow("Celular").ToString

        'Credito
        txtLimiteCredito.txt.Text = oRow("LimiteCredito").ToString
        txtDeuda.txt.Text = oRow("DeudaTotal").ToString
        txtSaldo.txt.Text = oRow("SaldoCredito").ToString
        txtDescuento.txt.Text = oRow("Descuento").ToString
        txtPlazoCredito.txt.Text = oRow("PlazoCredito").ToString
        txtPlazoCobro.txt.Text = oRow("PlazoCobro").ToString
        txtPlazoCheque.txt.Text = oRow("PlazoChequeDiferido").ToString

        chkBoleta.Valor = CBool(oRow("Boleta").ToString)

        txtLimiteCredito.Enabled = oRow("Credito")
        txtDeuda.Enabled = oRow("Credito")
        txtSaldo.Enabled = oRow("Credito")
        txtDescuento.Enabled = oRow("Credito")
        txtPlazoCredito.Enabled = oRow("Credito")
        txtPlazoCobro.Enabled = oRow("Credito")
        txtPlazoCheque.Enabled = oRow("Credito")
        chkBoleta.Enabled = oRow("Credito")

        If oRow("Credito") = False Then
            
            Dim Mensaje As String = "Cliente no habilitado para transacciones a credito!"
            ctrError.SetError(txtReferencia, Mensaje)
            ctrError.SetIconAlignment(txtReferencia, ErrorIconAlignment.TopRight)

            tsslEstado.Text = "Atencion!!! " & Mensaje

        End If

        Dim sql As String = "Select Latitud, Longitud From VCliente Where " & Campo & " = '" & Valor & "'"
        Dim dt As DataTable = CSistema.ExecuteToDataTable(sql)

        OcxClientesMapa1.ID = dtCliente.Rows(0)("ID")

        For Each dRow As DataRow In dt.Rows
            OcxClientesMapa1.Latitud = dRow("Latitud").ToString
            OcxClientesMapa1.Longitud = dRow("Longitud").ToString
        Next

        OcxClientesMapa1.CargarCliente()


        ctrError.Clear()

    End Sub

    Sub Guardar()

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Escritura de la Descripcion
        If IDCliente = 0 = False Then
            Dim mensaje As String = "Debe seleccionar un cliente valido!"
            ctrError.SetError(txtReferencia, mensaje)
            ctrError.SetIconAlignment(txtReferencia, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Condicion de venta CREDITO exigir carga de Limite de Credito
        If rdbCredito.Checked = True Then
            If txtLimiteCredito.txt.Text = 0 Then
                CSistema.MostrarError("El Limite de credito no puede ser Cero!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                txtLimiteCredito.txt.SelectAll()
                txtLimiteCredito.Focus()
                Exit Sub
            End If

        End If


        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@IDCliente", IDCliente, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@LimiteCredito", txtLimiteCredito.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descuento", txtDescuento.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PlazoCredito", txtPlazoCredito.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PlazoCobro", txtPlazoCobro.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PlazoChequeDiferido", txtPlazoCheque.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Boleta", chkBoleta.chk.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Contado", rdbContado.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Credito", rdbCredito.Checked.ToString, ParameterDirection.Input)
        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpAcualizarCreditoCliente", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            ctrError.Clear()
            ObtenerInformacion("Referencia", txtReferencia.Texto)
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub ListarCliente()

        Dim frmBuscar As New frmClienteBuscar
        frmBuscar.Size = New Size(frmPrincipal2.Size.Width - 50, frmPrincipal2.Height - 130)
        frmBuscar.Location = New Point(5, 10)
        FGMostrarFormulario(Me, frmBuscar, "Busqueda de Clientes", FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        If frmBuscar.ID > 0 Then
            IDCliente = frmBuscar.ID
            ObtenerInformacion("ID", IDCliente)
        End If

    End Sub

    Sub ManejoTecla(ByVal e As KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            ObtenerInformacion("Referencia", txtReferencia.GetValue)
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtReferencia.txt.SelectAll()
            ObtenerInformacion("ID", txtID.ObtenerValor)


        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtReferencia.txt.SelectAll()
            ObtenerInformacion("ID", txtID.ObtenerValor)

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Cliente"), Integer)

            txtID.txt.Text = ID
            txtReferencia.txt.SelectAll()

            ObtenerInformacion("ID", txtID.ObtenerValor)

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(ID), 1) From Cliente"), Integer)

            txtID.txt.Text = ID
            txtReferencia.txt.SelectAll()

            ObtenerInformacion("ID", txtID.ObtenerValor)

        End If

        If e.KeyCode = Keys.F1 Then
            ListarCliente()
        End If

    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub frmCreditoCliente_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub txtReferencia_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtReferencia.TeclaPrecionada
        ManejoTecla(e)
        'If e.KeyValue = Keys.Enter Then
        '    ObtenerInformacion("Referencia", txtReferencia.Texto)
        'End If
        'If e.KeyCode = Keys.F1 Then
        '    ListarCliente()
        'End If
    End Sub

    Private Sub frmCreditoCliente_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.F1 Then
            ListarCliente()
        End If
    End Sub

    Private Sub rdbCredito_CheckedChanged(sender As Object, e As EventArgs) Handles rdbCredito.CheckedChanged
        txtLimiteCredito.Enabled = rdbCredito.Checked
        txtDeuda.Enabled = rdbCredito.Checked
        txtSaldo.Enabled = rdbCredito.Checked
        txtDescuento.Enabled = rdbCredito.Checked
        txtPlazoCredito.Enabled = rdbCredito.Checked
        txtPlazoCobro.Enabled = rdbCredito.Checked
        txtPlazoCheque.Enabled = rdbCredito.Checked
        chkBoleta.Enabled = rdbCredito.Checked
    End Sub

    Private Sub lklRecalcularCredito_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lklRecalcularCredito.LinkClicked

    End Sub

    Private Sub lklVerCatastro_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lklVerCatastro.LinkClicked

    End Sub
End Class
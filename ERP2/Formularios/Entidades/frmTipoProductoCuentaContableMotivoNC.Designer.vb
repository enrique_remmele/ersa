﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTipoProductoCuentaContableMotivoNC
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblTipoProducto = New System.Windows.Forms.Label()
        Me.lblCuentaContableCosto = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.txtUsuarioModificacion = New System.Windows.Forms.TextBox()
        Me.txtFechaModificacion = New System.Windows.Forms.TextBox()
        Me.lblModificacion = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.txtCuentaContableAcuerdo = New ERP.ocxTXTCuentaContable()
        Me.txtCuentaContableDiferenciaPrecio = New ERP.ocxTXTCuentaContable()
        Me.txtCuentaContableDescuento = New ERP.ocxTXTCuentaContable()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblTipoProducto
        '
        Me.lblTipoProducto.AutoSize = True
        Me.lblTipoProducto.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblTipoProducto.Location = New System.Drawing.Point(12, 9)
        Me.lblTipoProducto.Name = "lblTipoProducto"
        Me.lblTipoProducto.Size = New System.Drawing.Size(107, 20)
        Me.lblTipoProducto.TabIndex = 0
        Me.lblTipoProducto.Text = "Tipo Producto"
        '
        'lblCuentaContableCosto
        '
        Me.lblCuentaContableCosto.AutoSize = True
        Me.lblCuentaContableCosto.Location = New System.Drawing.Point(9, 60)
        Me.lblCuentaContableCosto.Name = "lblCuentaContableCosto"
        Me.lblCuentaContableCosto.Size = New System.Drawing.Size(85, 13)
        Me.lblCuentaContableCosto.TabIndex = 12
        Me.lblCuentaContableCosto.Text = "C.C. Descuento:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 120)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 13)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "C.C. Dif. Precio:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(21, 90)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "C.C. Acuerdo:"
        '
        'btnCancelar
        '
        Me.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancelar.Location = New System.Drawing.Point(432, 214)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 20
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(351, 214)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 19
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'txtUsuarioModificacion
        '
        Me.txtUsuarioModificacion.BackColor = System.Drawing.Color.White
        Me.txtUsuarioModificacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUsuarioModificacion.Location = New System.Drawing.Point(195, 167)
        Me.txtUsuarioModificacion.Name = "txtUsuarioModificacion"
        Me.txtUsuarioModificacion.ReadOnly = True
        Me.txtUsuarioModificacion.Size = New System.Drawing.Size(117, 20)
        Me.txtUsuarioModificacion.TabIndex = 38
        Me.txtUsuarioModificacion.TabStop = False
        Me.txtUsuarioModificacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtFechaModificacion
        '
        Me.txtFechaModificacion.BackColor = System.Drawing.Color.White
        Me.txtFechaModificacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFechaModificacion.Location = New System.Drawing.Point(104, 167)
        Me.txtFechaModificacion.Name = "txtFechaModificacion"
        Me.txtFechaModificacion.ReadOnly = True
        Me.txtFechaModificacion.Size = New System.Drawing.Size(85, 20)
        Me.txtFechaModificacion.TabIndex = 37
        Me.txtFechaModificacion.TabStop = False
        Me.txtFechaModificacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblModificacion
        '
        Me.lblModificacion.AutoSize = True
        Me.lblModificacion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblModificacion.Location = New System.Drawing.Point(13, 171)
        Me.lblModificacion.Name = "lblModificacion"
        Me.lblModificacion.Size = New System.Drawing.Size(89, 13)
        Me.lblModificacion.TabIndex = 39
        Me.lblModificacion.Text = "Ult. Modificacion:"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 237)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(544, 22)
        Me.StatusStrip1.TabIndex = 40
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'txtCuentaContableAcuerdo
        '
        Me.txtCuentaContableAcuerdo.AlturaMaxima = 255
        Me.txtCuentaContableAcuerdo.Consulta = Nothing
        Me.txtCuentaContableAcuerdo.IDCentroCosto = 0
        Me.txtCuentaContableAcuerdo.IDUnidadNegocio = 0
        Me.txtCuentaContableAcuerdo.ListarTodas = False
        Me.txtCuentaContableAcuerdo.Location = New System.Drawing.Point(108, 84)
        Me.txtCuentaContableAcuerdo.Name = "txtCuentaContableAcuerdo"
        Me.txtCuentaContableAcuerdo.Registro = Nothing
        Me.txtCuentaContableAcuerdo.Resolucion173 = False
        Me.txtCuentaContableAcuerdo.Seleccionado = False
        Me.txtCuentaContableAcuerdo.Size = New System.Drawing.Size(399, 24)
        Me.txtCuentaContableAcuerdo.SoloLectura = False
        Me.txtCuentaContableAcuerdo.TabIndex = 17
        Me.txtCuentaContableAcuerdo.Texto = Nothing
        Me.txtCuentaContableAcuerdo.whereFiltro = Nothing
        '
        'txtCuentaContableDiferenciaPrecio
        '
        Me.txtCuentaContableDiferenciaPrecio.AlturaMaxima = 255
        Me.txtCuentaContableDiferenciaPrecio.Consulta = Nothing
        Me.txtCuentaContableDiferenciaPrecio.IDCentroCosto = 0
        Me.txtCuentaContableDiferenciaPrecio.IDUnidadNegocio = 0
        Me.txtCuentaContableDiferenciaPrecio.ListarTodas = False
        Me.txtCuentaContableDiferenciaPrecio.Location = New System.Drawing.Point(108, 114)
        Me.txtCuentaContableDiferenciaPrecio.Name = "txtCuentaContableDiferenciaPrecio"
        Me.txtCuentaContableDiferenciaPrecio.Registro = Nothing
        Me.txtCuentaContableDiferenciaPrecio.Resolucion173 = False
        Me.txtCuentaContableDiferenciaPrecio.Seleccionado = False
        Me.txtCuentaContableDiferenciaPrecio.Size = New System.Drawing.Size(399, 24)
        Me.txtCuentaContableDiferenciaPrecio.SoloLectura = False
        Me.txtCuentaContableDiferenciaPrecio.TabIndex = 16
        Me.txtCuentaContableDiferenciaPrecio.Texto = Nothing
        Me.txtCuentaContableDiferenciaPrecio.whereFiltro = Nothing
        '
        'txtCuentaContableDescuento
        '
        Me.txtCuentaContableDescuento.AlturaMaxima = 255
        Me.txtCuentaContableDescuento.Consulta = Nothing
        Me.txtCuentaContableDescuento.IDCentroCosto = 0
        Me.txtCuentaContableDescuento.IDUnidadNegocio = 0
        Me.txtCuentaContableDescuento.ListarTodas = False
        Me.txtCuentaContableDescuento.Location = New System.Drawing.Point(108, 54)
        Me.txtCuentaContableDescuento.Name = "txtCuentaContableDescuento"
        Me.txtCuentaContableDescuento.Registro = Nothing
        Me.txtCuentaContableDescuento.Resolucion173 = False
        Me.txtCuentaContableDescuento.Seleccionado = False
        Me.txtCuentaContableDescuento.Size = New System.Drawing.Size(399, 24)
        Me.txtCuentaContableDescuento.SoloLectura = False
        Me.txtCuentaContableDescuento.TabIndex = 13
        Me.txtCuentaContableDescuento.Texto = Nothing
        Me.txtCuentaContableDescuento.whereFiltro = Nothing
        '
        'frmTipoProductoCuentaContableMotivoNC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(544, 259)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.txtUsuarioModificacion)
        Me.Controls.Add(Me.txtFechaModificacion)
        Me.Controls.Add(Me.lblModificacion)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.txtCuentaContableAcuerdo)
        Me.Controls.Add(Me.txtCuentaContableDiferenciaPrecio)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblCuentaContableCosto)
        Me.Controls.Add(Me.txtCuentaContableDescuento)
        Me.Controls.Add(Me.lblTipoProducto)
        Me.Name = "frmTipoProductoCuentaContableMotivoNC"
        Me.Text = "frmTipoProductoCuentaContableMotivoNC"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblTipoProducto As System.Windows.Forms.Label
    Friend WithEvents lblCuentaContableCosto As System.Windows.Forms.Label
    Friend WithEvents txtCuentaContableDescuento As ERP.ocxTXTCuentaContable
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCuentaContableDiferenciaPrecio As ERP.ocxTXTCuentaContable
    Friend WithEvents txtCuentaContableAcuerdo As ERP.ocxTXTCuentaContable
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents txtUsuarioModificacion As System.Windows.Forms.TextBox
    Friend WithEvents txtFechaModificacion As System.Windows.Forms.TextBox
    Friend WithEvents lblModificacion As System.Windows.Forms.Label
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
End Class

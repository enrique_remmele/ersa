﻿Public Class frmOperacion

    'CLASES
    Dim CSistema As New CSistema


    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        'TextBox
        txtCodigo.txt.CharacterCasing = CharacterCasing.Normal


        'Funciones
        CargarOperaciones()
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Focus
        dgvLista.Focus()

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtDescripcion)
        CSistema.CargaControl(vControles, txtCodigo)
        CSistema.CargaControl(vControles, txtFormulario)

        'Cargamos los registos en el lv
        Listar()

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If dgvLista.SelectedRows.Count = 0 Then
            ctrError.SetError(dgvLista, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgvLista, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = dgvLista.SelectedRows(0).Cells(0).Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From Operacion Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            txtDescripcion.txt.Text = oRow("Descripcion").ToString
            txtCodigo.txt.Text = oRow("Codigo").ToString
            txtFormulario.txt.Text = oRow("FormName").ToString
            
            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        CSistema.SqlToDataGrid(dgvLista, "Select ID, Codigo, Descripcion From Operacion Order By 1")

        'Formato
        dgvLista.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        txtCantidad.SetValue(dgvLista.RowCount)

    End Sub

    Sub InicializarControles()

        'TextBox
        txtDescripcion.txt.Clear()
        txtCodigo.txt.Clear()
        txtFormulario.txt.Clear()
       
        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Operacion"), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Operacion"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        txtDescripcion.Focus()


    End Sub

    Sub CargarOperaciones()

        'INGRESOS
        'Ventas
        CSistema.ObtenerIDOperacion(frmVenta.Name, "VENTAS CLIENTES", "FATCLI")
        'Pedido
        CSistema.ObtenerIDOperacion(frmPedido.Name, "PEDIDO", "PED")
        'NOta de Credito
        CSistema.ObtenerIDOperacion(frmNotaCreditoCliente.Name, "NOTA CREDITO", "NCR")
        CSistema.ObtenerIDOperacion(frmAplicacionNotaCreditoCliente.Name, "APLICACION NOTA DE CREDITO", "ANC")
        CSistema.ObtenerIDOperacion(frmNotaCreditoTransferencia.Name, "TRANSFERENCIA NOTA DE CREDITO", "TNC")
        'Nota de Debito
        CSistema.ObtenerIDOperacion(frmNotaDebitoCliente.Name, "NOTA DEBITO", "NDB")
        CSistema.ObtenerIDOperacion(frmAplicacionNotaDebitoCliente.Name, "APLICACION NOTA DE DEBITO", "AND")
        'Lote
        CSistema.ObtenerIDOperacion(frmLoteDistribucion.Name, "LOTE DE DISTRIBUCION", "LOT")
        CSistema.ObtenerIDOperacion(frmDevolucionLote.Name, "DEVOLUCION DE LOTE", "DDL")
        CSistema.ObtenerIDOperacion(frmRendicionLote.Name, "RENDICION DE LOTE", "RLT")

        'COMPRAS
        CSistema.ObtenerIDOperacion(frmCompra.Name, "COMPRA DE MERCADERIA", "COMPROV")
        'NOta de Credito
        CSistema.ObtenerIDOperacion(frmNotaCreditoProveedor.Name, "NOTA CREDITO PROVEEDOR", "NCRP")
        'Nota de Debito
        CSistema.ObtenerIDOperacion(frmNotaDebitoProveedor.Name, "NOTA DEBITO PROVEEDOR", "NDRP")

        'STOCK
        CSistema.ObtenerIDOperacion(frmMovimientoStock.Name, "MOVIMIENTOS", "MOV")
        CSistema.ObtenerIDOperacion(frmCargaMercaderia.Name, "CARGA DE MERCADERIA", "CAR")
        CSistema.ObtenerIDOperacion(frmPlanillaInventario.Name, "PLANILLA DE INVENTARIO", "INV")
        CSistema.ObtenerIDOperacion(frmInventarioInicial.Name, "INVENTARIO INICIAL", "INV")

        'TESORERIA
        CSistema.ObtenerIDOperacion(frmAplicacionProveedoresAnticipo.Name, "COBRANZA CREDITO", "CCRE")
        CSistema.ObtenerIDOperacion(frmCobranzaContado.Name, "COBRANZA POR LOTE", "CLOT")
        'FondoFijo
        CSistema.ObtenerIDOperacion(frmVale.Name, "VALE", "VALE")
        'Cheques
        CSistema.ObtenerIDOperacion(frmChequeCliente.Name, "CHEQUE CLIENTE", "CHQC")
        CSistema.ObtenerIDOperacion(frmRechazoChequeCliente.Name, "RECHAZO CHEQUE CLIENTE", "RCC")
        CSistema.ObtenerIDOperacion(frmEfectivizacion.Name, "EFECTIVIZACION", "EFEC")
        CSistema.ObtenerIDOperacion(frmCanjeChequeCliente.Name, "CANJE CHEQUE CLIENTE", "CCC")
        CSistema.ObtenerIDOperacion(frmDescuentodeCheques.Name, "DESCUENTO CHEQUE", "DES")
        'Efectivo
        CSistema.ObtenerIDOperacion(frmSeleccionEfectivo.Name, "PAGOS Y COBROS EN EFECTIVO", "EFE")
        'Banco
        CSistema.ObtenerIDOperacion(frmDepositoBancario.Name, "DEPOSITO BANCARIO", "DEPB")
        CSistema.ObtenerIDOperacion(frmDebitoCreditoBancario.Name, "DEBITO CREDITO BANCARIO", "DCB")
        CSistema.ObtenerIDOperacion(frmConciliaciondeMovimientoBancaria.Name, "CONCILIACION BANCARIA ", "CNB")
        'Otros
        CSistema.ObtenerIDOperacion(frmOrdenPago.Name, "ORDEN DE PAGO", "OP")
        CSistema.ObtenerIDOperacion(frmRetencionIVA.Name, "RETENCION IVA", "RI")
        CSistema.ObtenerIDOperacion(frmGastos.Name, "GASTO", "GAS")

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub frmUsuario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub dgvLista_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLista.SelectionChanged
        ObtenerInformacion()
    End Sub

End Class
﻿Public Class frmProveedorBuscar

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'PROPIEDADES
    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private ConsultaValue As String
    Public Property Consulta() As String
        Get
            Return ConsultaValue
        End Get
        Set(ByVal value As String)
            ConsultaValue = value
        End Set
    End Property

    'VARIABLES

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'FUNCIONES
    'Listar Clientes
    Sub ListarProveedores()

        'If Consulta = "" Then
        Consulta = "Select ID, 'Razon Social'=RazonSocial, Referencia, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono From VProveedor "
        'End If

        Dim Where As String = ""
        Dim OrderBy As String = ""

        If chkActivo.chk.Checked = True Then
            'Filtrar por Fecha
            Where += IIf(Where = "", " WHERE ", " AND ")
            Where += " Estado='1' "
        End If

        If rdbRazonSocial.Checked = True Then
            Where += IIf(Where = "", " WHERE ", " AND ")
            Where += " RazonSocial Like '%" & txtDescripcion.Text.Trim & "%' "
            OrderBy = " Order By RazonSocial"
        End If

        If rdbReferencia.Checked = True Then
            Where += IIf(Where = "", " WHERE ", " AND ")
            Where += " Referencia Like '%" & txtDescripcion.Text.Trim & "%' "
            OrderBy = " Order By Referencia"
        End If

        If rdbRUC.Checked = True Then
            Where += IIf(Where = "", " WHERE ", " AND ")
            Where += " RUC Like '%" & txtDescripcion.Text.Trim & "%' "
            OrderBy = " Order By RUC"
        End If

        If rdbNombreFantasia.Checked = True Then
            Where += IIf(Where = "", " WHERE ", " AND ")
            Where += " NombreFantasia Like '%" & txtDescripcion.Text.Trim & "%' "
            OrderBy = " Order By NombreFantasia"
        End If

        If cbxSucursals.Enabled = True Then
            Where += IIf(Where = "", " WHERE ", " AND ")
            Where += " IDSucursal = '" & cbxSucursals.SelectedValue.ToString & "' "
        End If


        CSistema.SqlToLv(lvlista, Consulta & Where & OrderBy)
        Dim dt As DataTable = CSistema.ExecuteToDataTable(Consulta & Where & OrderBy)

        'Formato
        tsslCantidad.Text = lvlista.Items.Count

    End Sub

    'Guardar Informacion
    Sub GuardarInformacion()

        Dim Seleccion As Integer = 1

       If rdbRazonSocial.Checked = True Then
            Seleccion = 1
        End If

        If rdbReferencia.Checked = True Then
            Seleccion = 2
        End If

        If rdbRUC.Checked = True Then
            Seleccion = 3
        End If

        If rdbNombreFantasia.Checked = True Then
            Seleccion = 4
        End If

        If rdbRazonSocial.Checked = True Then
            Seleccion = 5
        End If

        'Seleccion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SELECCION", Seleccion)

    End Sub

    'Cargar Informacion
    Sub CargarInformacion()

        'Seleccion
        Dim Seleccion As Integer = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SELECCION", "1")

        Select Case Seleccion

            Case 1
                rdbRazonSocial.Checked = True
            Case 2
                rdbReferencia.Checked = True
            Case 3
                rdbRUC.Checked = True
            Case 4
                rdbNombreFantasia.Checked = True
            Case Else
                rdbRazonSocial.Checked = True
        End Select

        chkActivo.chk.Checked = True

        'Cargar las sucursales
        CSistema.SqlToComboBox(cbxSucursals, "Select ID, Codigo From Sucursal Order By ID ")
        cbxSucursals.SelectedValue = vgIDSucursal

        cbxSucursals.Enabled = False

        'Foco
        txtDescripcion.Focus()
        txtDescripcion.SelectAll()

    End Sub

    'Seleccion
    Sub Seleccionar()

        If lvlista.SelectedItems.Count > 0 Then
            ID = lvlista.SelectedItems(0).SubItems(0).Text
            Me.Visible = False
            RaiseEvent PropertyChanged(New Object, New EventArgs)
        End If

    End Sub

    Private Sub txtDescripcion_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDescripcion.KeyUp
        If e.KeyCode = Keys.Enter Then
            ListarProveedores()
        End If

    End Sub

    Private Sub frmClienteBuscar_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmClienteBuscar_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F2 Then


            If rdbRazonSocial.Checked = True Then
                rdbReferencia.Checked = True
                GoTo Siguiente
            End If

            If rdbReferencia.Checked = True Then
                rdbRUC.Checked = True
                GoTo Siguiente
            End If

            If rdbRUC.Checked = True Then
                rdbNombreFantasia.Checked = True
                GoTo Siguiente
            End If

            If rdbNombreFantasia.Checked = True Then
                rdbRazonSocial.Checked = True
                GoTo Siguiente
            End If


Siguiente:
            ListarProveedores()
            txtDescripcion.SelectAll()
            txtDescripcion.Focus()

        End If

        If e.KeyCode = Keys.Down Then
            If lvlista.Items.Count > 0 Then
                If lvlista.SelectedItems.Count = 0 Then
                    lvlista.Items(0).Selected = True
                End If

                lvlista.Focus()
            Else
                txtDescripcion.SelectAll()
            End If
        End If

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Private Sub frmClienteBuscar_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CargarInformacion()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        ListarProveedores()
    End Sub

    Private Sub lvlista_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvlista.DoubleClick
        Seleccionar()
    End Sub

    Private Sub lvlista_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvlista.KeyUp
        If e.KeyCode = Keys.Enter Then
            Seleccionar()
        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub chkSusursal_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkSusursals.CheckedChanged
        cbxSucursals.Enabled = chkSusursals.Checked
    End Sub
End Class
﻿Public Class frmMotivo


    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Variables
        vNuevo = False

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        CSistema.InicializaControles(Me)
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)


        'Focus
        dgv.Focus()

    End Sub

    Sub CargarInformacion()


        txtCuentaContable.Conectar()

        'Cargamos los paises
        CSistema.SqlToComboBox(cbxTipoOperacion.cbx, "Select ID, 'Descripcion' =  Operacion + ' - ' + Descripcion From VTipoOperacion Order By Operacion, Descripcion")

        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Descripcion From VSucursal Order by Descripcion")
        cbxSucursal.cbx.Text = ""

        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID,Descripcion From vTipoComprobante Order By Descripcion")
        cbxTipoComprobante.cbx.Text = ""

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(6)
        vControles(0) = txtDescripcion
        vControles(1) = cbxTipoOperacion
        vControles(2) = rdbActivo
        vControles(3) = rdbDesactivado
        vControles(4) = txtCuentaContable
        vControles(5) = cbxSucursal
        vControles(6) = cbxTipoComprobante

        'Cargamos los paises en el lv
        Listar()

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If dgv.SelectedRows.Count = 0 Then
            ctrError.SetError(dgv, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = dgv.SelectedRows(0).Cells("ID").Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select ID, Descripcion, 'TipoOperacion'=Operacion + ' - ' + Tipo,CodigoCuentaContable, Activo,Sucursal,TipoComprobante From  VMotivoMovimiento Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.Text = oRow("ID").ToString
            txtDescripcion.txt.Text = oRow("Descripcion").ToString
            cbxTipoOperacion.cbx.Text = oRow("TipoOperacion").ToString
            txtCuentaContable.LimpiarSeleccion()
            txtCuentaContable.SeleccionarRegistroCodigo(oRow("CodigoCuentaContable").ToString)

            cbxSucursal.cbx.Text = oRow("Sucursal").ToString
            cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString

            If CBool(oRow("Activo")) = True Then
                rdbActivo.Checked = True
            Else
                rdbDesactivado.Checked = True
            End If

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        'Con este metodo "SqlToDataGrid" el sistema carga automaticamente la consulta SQL en el DataGrid asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el DataGrid.
        CSistema.SqlToDataGrid(dgv, "Select ID, 'Motivo'=Descripcion, 'Tipo'=Operacion + ' - ' + Tipo,CodigoCuentaContable, Estado, Sucursal,TipoComprobante From VMotivoMovimiento Order By Operacion, Tipo, Descripcion, Sucursal")
        dgv.Columns("Motivo").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("Tipo").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.ReadOnly = True

    End Sub

    Sub InicializarControles()

        'TextBox
        txtDescripcion.txt.Clear()

        'RadioButton
        rdbActivo.Checked = True

        'ToolStripStatusLabel
        tsslEstado.Text = ""

        'Funciones
        If Me.vNuevo = True Then
            txtID.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From MotivoMovimiento"), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From MotivoMovimiento"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        cbxTipoOperacion.cbx.Focus()


    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        'Validar
        'Si es que selecciono correctamente el Pais
        If IsNumeric(cbxTipoOperacion.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el tipo de operacion al que corresponde!"
            ctrError.SetError(cbxTipoOperacion, mensaje)
            ctrError.SetIconAlignment(cbxTipoOperacion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Escritura de la Descripcion
        If txtDescripcion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar una descripcion valida!"
            ctrError.SetError(txtDescripcion, mensaje)
            ctrError.SetIconAlignment(txtDescripcion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If cbxSucursal.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente una Sucursal"
            ctrError.SetError(cbxSucursal, mensaje)
            ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgv.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgv, mensaje)
                ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(9) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        param(0) = CSistema.SetSQLParameter("@ID", ID, ParameterDirection.Input)
        param(1) = CSistema.SetSQLParameter("@Descripcion", txtDescripcion.txt.Text.Trim, ParameterDirection.Input)
        param(2) = CSistema.SetSQLParameter("@IDTipoMovimiento", cbxTipoOperacion.cbx.SelectedValue, ParameterDirection.Input)
        param(3) = CSistema.SetSQLParameter("@Operacion", Operacion.ToString, ParameterDirection.Input)
        param(4) = CSistema.SetSQLParameter("@Mensaje", "", ParameterDirection.Output, 200)
        param(5) = CSistema.SetSQLParameter("@Procesado", "", ParameterDirection.Output)
        param(6) = CSistema.SetSQLParameter("@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)

        If txtCuentaContable.Seleccionado Then
            param(7) = CSistema.SetSQLParameter("@CodigoCuentaContable", txtCuentaContable.Registro("Codigo").ToString, ParameterDirection.Input)
        Else
            param(7) = CSistema.SetSQLParameter("@CodigoCuentaContable", "''", ParameterDirection.Input)
        End If

        param(8) = CSistema.SetSQLParameter("@IDSucursal", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        param(9) = CSistema.SetSQLParameter("@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpMotivoMovimiento", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            Listar(txtID.Text)
            ctrError.Clear()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        txtDescripcion.Focus()
        txtDescripcion.txt.SelectAll()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        'InicializarControles()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub dgv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub frmLineas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()

    End Sub

    Private Sub dgv_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv.CellClick

    End Sub

    Private Sub cbxTipoOperacion_PropertyChanged(sender As Object, e As EventArgs) Handles cbxTipoOperacion.PropertyChanged

    End Sub
End Class
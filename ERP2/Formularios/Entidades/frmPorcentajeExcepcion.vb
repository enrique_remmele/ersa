﻿Public Class frmPorcentajeExcepcion
    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control
    Dim vNuevoProductoPrecio As Boolean
    Dim vControlesProductoPrecio() As Control

#Region "ProductoListaPrecio"

    'FUNCIONES
    Sub Inicializar()

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        'Funciones
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        'Botones
        EstablecerBotones(CSistema.NUMHabilitacionBotonesABM.INICIO)
        'Focus
        dgv.Focus()

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxUsuario)
        CSistema.CargaControl(vControles, txtPorcentaje)
        CSistema.CargaControl(vControles, txtImporte)
        CSistema.CargaControl(vControles, rdbActivo)
        CSistema.CargaControl(vControles, rdbDesactivado)

        CSistema.SqlToComboBox(cbxUsuario, "Select ID, Nombre from VUsuarioExcepcion")


        'Cargamos los registos en el lv
        Listar()

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If dgv.CurrentRow Is Nothing Then
            ctrError.SetError(dgv, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If


        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VPorcentajeExcepcion Where IDUsuario=" & dgv.CurrentRow.Cells("IDUsuario").Value)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            txtPorcentaje.txt.Text = CSistema.FormatoMoneda(oRow("Porcentaje").ToString, True)
            cbxUsuario.cbx.Text = oRow("NombreUsuario").ToString
            txtImporte.txt.Text = CSistema.FormatoMoneda(oRow("Importe").ToString, True)
            If CBool(oRow("Estado")) = True Then
                rdbActivo.Checked = True
            Else
                rdbDesactivado.Checked = True
            End If
            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar()

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        CSistema.SqlToDataGrid(dgv, "Select * From VPorcentajeExcepcion Order By NombreUsuario")

        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If dgv.Columns.Count = 0 Then
            Exit Sub
        End If

        For i As Integer = 0 To dgv.Columns.Count - 1
            dgv.Columns(i).Visible = False
        Next

        dgv.Columns("NombreUsuario").Visible = True
        dgv.Columns("Porcentaje").Visible = True
        dgv.Columns("Importe").Visible = True
        dgv.Columns("Estado").Visible = True

        dgv.Columns("NombreUsuario").DisplayIndex = 0
        dgv.Columns("Porcentaje").DisplayIndex = 1
        dgv.Columns("Importe").DisplayIndex = 2
        dgv.Columns("Estado").DisplayIndex = 3

        dgv.Columns("Porcentaje").DefaultCellStyle.Format = "N2"
        dgv.Columns("Importe").DefaultCellStyle.Format = "N2"


    End Sub

    Sub InicializarControles()

        'Error
        ctrError.Clear()

        'Foco
        txtPorcentaje.Focus()

        rdbActivo.Checked = True
        rdbDesactivado.Checked = False


    End Sub
    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesABM)

        CSistema.ControlBotonesABM(Operacion, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Escritura de la Descripcion
        If txtPorcentaje.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar un numero valido!"
            ctrError.SetError(txtPorcentaje, mensaje)
            ctrError.SetIconAlignment(txtPorcentaje, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If IsNumeric(txtPorcentaje.txt.Text) = False Then
            Dim mensaje As String = "Debe ingresar un numero valido!"
            ctrError.SetError(txtPorcentaje, mensaje)
            ctrError.SetIconAlignment(txtPorcentaje, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If txtImporte.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar un numero valido!"
            ctrError.SetError(txtImporte, mensaje)
            ctrError.SetIconAlignment(txtImporte, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If IsNumeric(txtImporte.txt.Text) = False Then
            Dim mensaje As String = "Debe ingresar un numero valido!"
            ctrError.SetError(txtImporte, mensaje)
            ctrError.SetIconAlignment(txtImporte, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        'Usuario
        If cbxUsuario.Validar("Seleccione correctamente un usuario", ctrError, cbxUsuario, tsslEstado) = False Then
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de ACTUALIZACION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgv.CurrentCell Is Nothing Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgv, mensaje)
                ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@IDUsuarioExcepcion", cbxUsuario.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Porcentaje", CSistema.FormatoNumeroBaseDatos(txtPorcentaje.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoNumeroBaseDatos(txtImporte.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpPorcentajeExcepcion", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            Listar()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno

            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub
    Sub Nuevo()
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        txtPorcentaje.Focus()
        txtPorcentaje.txt.SelectAll()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub frmListaPrecio_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub



    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub dgv_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles dgv.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub dgv_Click(sender As System.Object, e As System.EventArgs) Handles dgv.Click
        ObtenerInformacion()
    End Sub

#End Region

#Region "Producto Precio"

    'FUNCIONES
    Sub InicializarProductoPrecio()

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevoProductoPrecio = False

        'Funciones
        CargarInformacionProductoPrecio()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevoProductoPrecio, btnEditarProductoPrecio, btnCancelarProductoPrecio, btnGuardarProductoPrecio, btnEliminarProductoPrecio, vControlesProductoPrecio)
        'Botones
        EstablecerBotonesProductoPrecio(CSistema.NUMHabilitacionBotonesABM.INICIO)
        'Focus
        dgvProductoPrecio.Focus()

    End Sub

    Sub CargarInformacionProductoPrecio()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControlesProductoPrecio(-1)
        CSistema.CargaControl(vControlesProductoPrecio, cbxUsuarioProductoPrecio)
        CSistema.CargaControl(vControlesProductoPrecio, cbxPresentacionProductoPrecio)
        CSistema.CargaControl(vControlesProductoPrecio, txtPorcentajeProductoPrecio)
        CSistema.CargaControl(vControlesProductoPrecio, txtImporteProductoPrecio)
        CSistema.CargaControl(vControlesProductoPrecio, rbActivoProductoPrecio)
        CSistema.CargaControl(vControlesProductoPrecio, rbDesactivadoProductoPrecio)

        CSistema.SqlToComboBox(cbxUsuarioProductoPrecio, "Select ID, Nombre from VUsuarioExcepcion")
        CSistema.SqlToComboBox(cbxPresentacionProductoPrecio, "Select ID, Descripcion from Presentacion")


        'Cargamos los registos en el lv
        ListarProductoPrecio()

    End Sub

    Sub ObtenerInformacionProductoPrecio()

        'Validar
        'Si es que se selecciono el registro.
        If dgvProductoPrecio.CurrentRow Is Nothing Then
            ctrError.SetError(dgvProductoPrecio, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgvProductoPrecio, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevoProductoPrecio, btnEditarProductoPrecio, btnCancelarProductoPrecio, btnGuardarProductoPrecio, btnEliminarProductoPrecio, vControlesProductoPrecio)

            Exit Sub
        End If


        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VPorcentajeProductoPrecio Where IDUsuario=" & dgvProductoPrecio.CurrentRow.Cells("IDUsuario").Value & " and IDPresentacion=" & dgvProductoPrecio.CurrentRow.Cells("IDPresentacion").Value)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            txtPorcentajeProductoPrecio.txt.Text = CSistema.FormatoMoneda(oRow("Porcentaje").ToString, True)
            cbxUsuarioProductoPrecio.cbx.Text = oRow("NombreUsuario").ToString
            cbxPresentacionProductoPrecio.cbx.Text = oRow("Presentacion").ToString
            txtImporteProductoPrecio.txt.Text = CSistema.FormatoMoneda(oRow("Importe").ToString, True)
            If CBool(oRow("Estado")) = True Then
                rbActivoProductoPrecio.Checked = True
            Else
                rbDesactivadoProductoPrecio.Checked = True
            End If
            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevoProductoPrecio, btnEditarProductoPrecio, btnCancelarProductoPrecio, btnGuardarProductoPrecio, btnEliminarProductoPrecio, vControlesProductoPrecio)

        End If

        ctrError.Clear()

    End Sub

    Sub ListarProductoPrecio()

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        CSistema.SqlToDataGrid(dgvProductoPrecio, "Select * From VPorcentajeProductoPrecio Order By NombreUsuario")

        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If dgvProductoPrecio.Columns.Count = 0 Then
            Exit Sub
        End If

        For i As Integer = 0 To dgvProductoPrecio.Columns.Count - 1
            dgvProductoPrecio.Columns(i).Visible = False
        Next

        dgvProductoPrecio.Columns("NombreUsuario").Visible = True
        dgvProductoPrecio.Columns("Porcentaje").Visible = True
        dgvProductoPrecio.Columns("Presentacion").Visible = True
        dgvProductoPrecio.Columns("Importe").Visible = True
        dgvProductoPrecio.Columns("Estado").Visible = True

        dgvProductoPrecio.Columns("NombreUsuario").DisplayIndex = 0
        dgvProductoPrecio.Columns("Presentacion").DisplayIndex = 1
        dgvProductoPrecio.Columns("Porcentaje").DisplayIndex = 2
        dgvProductoPrecio.Columns("Importe").DisplayIndex = 3
        dgvProductoPrecio.Columns("Estado").DisplayIndex = 4

        dgvProductoPrecio.Columns("Porcentaje").DefaultCellStyle.Format = "N2"
        dgvProductoPrecio.Columns("Importe").DefaultCellStyle.Format = "N2"


    End Sub

    Sub InicializarControlesProductoPrecio()

        'Error
        ctrError.Clear()

        'Foco
        txtPorcentajeProductoPrecio.Focus()

        rbActivoProductoPrecio.Checked = True
        rbDesactivadoProductoPrecio.Checked = False


    End Sub
    Sub EstablecerBotonesProductoPrecio(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesABM)

        CSistema.ControlBotonesABM(Operacion, btnNuevoProductoPrecio, btnEditarProductoPrecio, btnCancelarProductoPrecio, btnGuardarProductoPrecio, btnEliminarProductoPrecio, vControlesProductoPrecio)

    End Sub

    Sub ProcesarProductoPrecio(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Escritura de la Descripcion
        If txtPorcentajeProductoPrecio.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar un numero valido!"
            ctrError.SetError(txtPorcentajeProductoPrecio, mensaje)
            ctrError.SetIconAlignment(txtPorcentajeProductoPrecio, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If IsNumeric(txtPorcentajeProductoPrecio.txt.Text) = False Then
            Dim mensaje As String = "Debe ingresar un numero valido!"
            ctrError.SetError(txtPorcentajeProductoPrecio, mensaje)
            ctrError.SetIconAlignment(txtPorcentajeProductoPrecio, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If txtImporteProductoPrecio.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar un numero valido!"
            ctrError.SetError(txtImporteProductoPrecio, mensaje)
            ctrError.SetIconAlignment(txtImporteProductoPrecio, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If IsNumeric(txtImporteProductoPrecio.txt.Text) = False Then
            Dim mensaje As String = "Debe ingresar un numero valido!"
            ctrError.SetError(txtImporteProductoPrecio, mensaje)
            ctrError.SetIconAlignment(txtImporteProductoPrecio, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        'Usuario
        If cbxUsuarioProductoPrecio.Validar("Seleccione correctamente un usuario", ctrError, cbxUsuarioProductoPrecio, tsslEstado) = False Then
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de ACTUALIZACION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgvProductoPrecio.CurrentCell Is Nothing Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgvProductoPrecio, mensaje)
                ctrError.SetIconAlignment(dgvProductoPrecio, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@IDUsuarioProductoPrecio", cbxUsuarioProductoPrecio.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDPresentacion", cbxPresentacionProductoPrecio.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Porcentaje", CSistema.FormatoNumeroBaseDatos(txtPorcentajeProductoPrecio.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoNumeroBaseDatos(txtImporteProductoPrecio.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rbActivoProductoPrecio.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpPorcentajeProductoPrecio", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevoProductoPrecio, btnEditarProductoPrecio, btnCancelarProductoPrecio, btnGuardarProductoPrecio, btnEliminarProductoPrecio, vControlesProductoPrecio)
            ctrError.Clear()
            ListarProductoPrecio()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno

            ctrError.SetError(btnGuardarProductoPrecio, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardarProductoPrecio, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnNuevoProductoPrecio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevoProductoPrecio.Click
        NuevoProductoPrecio()
    End Sub
    Sub NuevoProductoPrecio()
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevoProductoPrecio, btnEditarProductoPrecio, btnCancelarProductoPrecio, btnGuardarProductoPrecio, btnEliminarProductoPrecio, vControlesProductoPrecio)
        vNuevoProductoPrecio = True
        InicializarControlesProductoPrecio()
    End Sub

    Private Sub btnEditarProductoPrecio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditarProductoPrecio.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevoProductoPrecio, btnEditarProductoPrecio, btnCancelarProductoPrecio, btnGuardarProductoPrecio, btnEliminarProductoPrecio, vControlesProductoPrecio)

        vNuevoProductoPrecio = False

        'Foco
        cbxUsuarioProductoPrecio.SoloLectura = True
        cbxPresentacionProductoPrecio.SoloLectura = True

    End Sub

    Private Sub btnCancelarProductoPrecio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelarProductoPrecio.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevoProductoPrecio, btnEditarProductoPrecio, btnCancelarProductoPrecio, btnGuardarProductoPrecio, btnEliminarProductoPrecio, vControlesProductoPrecio)
        vNuevoProductoPrecio = False
        InicializarControlesProductoPrecio()
        ObtenerInformacionProductoPrecio()
    End Sub

    Private Sub btnGuardarProductoPrecio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardarProductoPrecio.Click
        If vNuevoProductoPrecio = True Then
            ProcesarProductoPrecio(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            ProcesarProductoPrecio(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminarProductoPrecio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarProductoPrecio.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevoProductoPrecio, btnEditarProductoPrecio, btnCancelarProductoPrecio, btnGuardarProductoPrecio, btnEliminarProductoPrecio, vControlesProductoPrecio)
        ProcesarProductoPrecio(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub frmListaPrecioProductoPrecio_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub



    Private Sub btnSalirProductoPrecio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalirProductoPrecio.Click
        Me.Close()
    End Sub

    Private Sub dgvProductoPrecio_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles dgvProductoPrecio.SelectionChanged
        ObtenerInformacionProductoPrecio()
    End Sub

    Private Sub dgvProductoPrecio_Click(sender As System.Object, e As System.EventArgs) Handles dgvProductoPrecio.Click
        ObtenerInformacionProductoPrecio()
    End Sub

#End Region

    Private Sub frmListaPrecio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
        InicializarProductoPrecio()
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfiguracionMuestraPanaderia
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.cbxMotivo = New ERP.ocxCBX()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Comprobante:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(35, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Motivo:"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(84, 23)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(231, 23)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 2
        Me.cbxTipoComprobante.Texto = ""
        '
        'cbxMotivo
        '
        Me.cbxMotivo.CampoWhere = Nothing
        Me.cbxMotivo.CargarUnaSolaVez = False
        Me.cbxMotivo.DataDisplayMember = Nothing
        Me.cbxMotivo.DataFilter = Nothing
        Me.cbxMotivo.DataOrderBy = Nothing
        Me.cbxMotivo.DataSource = Nothing
        Me.cbxMotivo.DataValueMember = Nothing
        Me.cbxMotivo.dtSeleccionado = Nothing
        Me.cbxMotivo.FormABM = Nothing
        Me.cbxMotivo.Indicaciones = Nothing
        Me.cbxMotivo.Location = New System.Drawing.Point(84, 57)
        Me.cbxMotivo.Name = "cbxMotivo"
        Me.cbxMotivo.SeleccionMultiple = False
        Me.cbxMotivo.SeleccionObligatoria = False
        Me.cbxMotivo.Size = New System.Drawing.Size(231, 23)
        Me.cbxMotivo.SoloLectura = False
        Me.cbxMotivo.TabIndex = 3
        Me.cbxMotivo.Texto = ""
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(240, 98)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 4
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'frmConfiguracionMuestraPanaderia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(329, 133)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.cbxMotivo)
        Me.Controls.Add(Me.cbxTipoComprobante)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmConfiguracionMuestraPanaderia"
        Me.Text = "frmConfiguracionMuestraPanaderia"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents cbxTipoComprobante As ocxCBX
    Friend WithEvents cbxMotivo As ocxCBX
    Friend WithEvents btnGuardar As Button
End Class

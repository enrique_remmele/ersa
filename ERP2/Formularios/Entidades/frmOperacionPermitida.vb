﻿Public Class frmOperacionPermitida

    Dim CSistema As New CSistema

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    Sub Inicializar()
        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False
        CargarInformacion()
        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'CSistema.CargaControl(vControles, cbxOperacion)
        CSistema.CargaControl(vControles, cbxTipoDocumento)
        CSistema.CargaControl(vControles, cbxTipoOperacion)
        CSistema.CargaControl(vControles, rdbActivo)
        CSistema.CargaControl(vControles, rdbDesactivado)

        'Operacion
        CSistema.SqlToComboBox(cbxOperacion, "select id, Descripcion from Operacion where FormName in ('frmMovimientoStock', 'frmDescargaStock', 'frmMovimientoMateriaPrima', 'frmMovimientoDescargaCompra', 'frmConsumoCombustible','frmMovimientoUnisal','frmConsumoInformatica')")
        cbxOperacion.cbx.SelectedIndex = 0

        Listar()


    End Sub

    Sub InicializarControles()

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From VOperacionPermitida"), Integer)
        Else
            Listar()
        End If

        'Error
        ctrError.Clear()

        'Foco
        cbxOperacion.cbx.Focus()

    End Sub

    Sub ObtenerInformacion()

        If dgv.SelectedRows.Count = 0 Then
            'MessageBox.Show("Seleccione correctamente un registro!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            Exit Sub
        End If

        Dim ID As Integer = 0
        Dim dtSeleccion As DataTable
        ID = dgv.SelectedRows(0).Cells(0).Value

        dtSeleccion = CSistema.ExecuteToDataTable("Select * from VOperacionPermitida where id = " & ID)
        txtID.Texto = ID
        cbxTipoDocumento.cbx.Text = dtSeleccion.Rows(0)("TipoComprobante").ToString
        cbxTipoOperacion.cbx.Text = dtSeleccion.Rows(0)("TipoOperacion").ToString

        If CBool(dtSeleccion.Rows(0)("Estado")) = True Then
            rdbActivo.Checked = True
            rdbDesactivado.Checked = False
        Else
            rdbActivo.Checked = False
            rdbDesactivado.Checked = True
        End If
        'Configuramos los controles ABM como EDITAR
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

    End Sub

    Sub Listar()

        Dim dt As DataTable

        dt = CSistema.ExecuteToDataTable("Select * from VOperacionPermitida where idOperacion = " & cbxOperacion.GetValue)
        CSistema.dtToGrid(dgv, dt)
        CSistema.DataGridColumnasVisibles(dgv, {"ID", "TipoComprobante", "TipoOperacion", "Estado"})
        dgv.Columns("TipoComprobante").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub Procesar(ByVal Operacion As ERP.CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgv.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgv, mensaje)
                ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDOperacion", cbxOperacion.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoOperacion", cbxTipoOperacion.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoDocumento.GetValue, ParameterDirection.Input)
        
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpOperacionPermitida", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            Listar()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If
    End Sub


    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(sender As System.Object, e As System.EventArgs) Handles btnEditar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        cbxOperacion.Focus()

    End Sub

    Private Sub cbxOperacion_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxOperacion.PropertyChanged
        'TipoDocumento
        CSistema.SqlToComboBox(cbxTipoDocumento, "select id, Descripcion from TipoComprobante where IDOperacion = " & cbxOperacion.GetValue)
        cbxTipoDocumento.cbx.SelectedIndex = 0
        Listar()

    End Sub

    Private Sub cbxTipoDocumento_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxTipoDocumento.PropertyChanged
        CSistema.SqlToComboBox(cbxTipoOperacion, "select id, Descripcion from TipoOperacion where IDOperacion = " & cbxOperacion.GetValue)
        cbxTipoOperacion.cbx.SelectedIndex = 0
    End Sub

    Private Sub dgv_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles dgv.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub frmOperacionPermitida_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
End Class
﻿Public Class frmProducto

    'PROPIEDADES
    Private IDProductoValue As String
    Public Property IDProducto() As String
        Get
            Return IDProductoValue
        End Get
        Set(ByVal value As String)
            IDProductoValue = value
        End Set
    End Property

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control
    Dim dtCuentasContablesObligatorias As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Variables
        vNuevo = False

        'Funciones
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Controles
        CSistema.InicializaControles(Me)
        CSistema.InicializaControles(tabIdentificacion)
        ocxCuentaCompra.Conectar()
        ocxCuentaVenta.Conectar()
        ocxCuentaCosto.Conectar()
        ocxCuentaDeudor.Conectar()
        OcxListaPrecioProducto1.Iniciarlizar()

        'btnbuscar habilitar
        btnBuscar.Enabled = True

        'Focus
        txtReferencia.Focus()

        'ir al ultimo registro
        ManejoTecla(New KeyEventArgs(Keys.End))
        BloquearCostos()
    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)

        'Datos principales
        CSistema.CargaControl(vControles, txtDescripcion)
        CSistema.CargaControl(vControles, txtCodigoBarra)
        'CSistema.CargaControl(vControles, btnBuscar)

        'Agrupadores
        CSistema.CargaControl(vControles, cbxTipoProducto)
        CSistema.CargaControl(vControles, cbxLinea)
        CSistema.CargaControl(vControles, cbxSubLinea)
        CSistema.CargaControl(vControles, cbxSubLinea2)
        CSistema.CargaControl(vControles, cbxMarca)
        CSistema.CargaControl(vControles, cbxPresentacion)
        CSistema.CargaControl(vControles, cbxCategoria)
        CSistema.CargaControl(vControles, cbxProveedor)
        CSistema.CargaControl(vControles, cbxDivision)
        CSistema.CargaControl(vControles, cbxProcedencia)
        CSistema.CargaControl(vControles, cbxUnidadMedidaConversion)

        'Unidad de Medida
        CSistema.CargaControl(vControles, cbxUnidadMedida)
        CSistema.CargaControl(vControles, txtUnidadPorCaja)
        CSistema.CargaControl(vControles, txtUnidadConversion)
        CSistema.CargaControl(vControles, txtPeso)
        CSistema.CargaControl(vControles, txtVolumen)


        'Configuraciones
        CSistema.CargaControl(vControles, txtMargenMinimo)
        CSistema.CargaControl(vControles, cbxImpuesto)
        CSistema.CargaControl(vControles, chkActivo)
        CSistema.CargaControl(vControles, chkControlarExistencia)
        CSistema.CargaControl(vControles, ocxCuentaCompra)
        CSistema.CargaControl(vControles, ocxCuentaVenta)
        CSistema.CargaControl(vControles, ocxCuentaCosto)
        CSistema.CargaControl(vControles, ocxCuentaDeudor)


        'Tipos de Producto
        CSistema.SqlToComboBox(cbxTipoProducto.cbx, "Select ID, Descripcion From TipoProducto Order By 2")

        'Tipos de Producto
        CSistema.SqlToComboBox(cbxPresentacion.cbx, "Select ID, Descripcion From Presentacion Order By 2")

        'Categoria
        CSistema.SqlToComboBox(cbxCategoria.cbx, "Select ID, Descripcion From Categoria Order By 2")

        'Proveedor
        CSistema.SqlToComboBox(cbxProveedor.cbx, "Select ID, RazonSocial + ' (' + Referencia + ')' From Proveedor Order By 2")

        'Procedencia
        CSistema.SqlToComboBox(cbxProcedencia.cbx, "Select ID, Descripcion From Pais Order By 2")

        'Unidad de Medida
        CSistema.SqlToComboBox(cbxUnidadMedida.cbx, "Select ID, Descripcion From UnidadMedida Order By 2")

        'Unidad de Medida a combertir
        CSistema.SqlToComboBox(cbxUnidadMedidaConversion.cbx, "Select ID, Descripcion From UnidadMedida Order By 2")

        'Impuesto
        CSistema.SqlToComboBox(cbxImpuesto.cbx, "Select ID, Descripcion From Impuesto Order By 1")

        OcxListaPrecioProducto1.Iniciarlizar()
        OcxComisionProducto1.Iniciarlizar()

    End Sub

    Sub ObtenerInformacion(ByVal Filtro As String, Optional ByVal PorReferencia As Boolean = False)

        'Validar
        'Obtener el ID Registro
        'Dim ID As Integer

        If PorReferencia = False Then
            If IsNumeric(txtID.txt.Text) = False Then
                Exit Sub
            End If
        End If

        If PorReferencia = True Then
            If txtReferencia.txt.Text = "" Then
                Exit Sub
            End If
        End If

        'Limpiar Listas
        dgvExistencia.DataSource = Nothing

        'Obtenemos la informacion actualizada desde la base de datos

        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select Top(1) * From VProducto " & Filtro)



        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count = 0 Then

            CSistema.InicializaControles(Me)
            CSistema.InicializaControles(tabIdentificacion)


            ctrError.SetError(txtID, "No se encontro ningun registro!")
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.TopRight)

            txtReferencia.txt.SelectAll()

            Exit Sub

        End If

        'Cargamos la fila "0" en un nuevo objeto DATAROW
        Dim oRow As DataRow
        oRow = dt.Rows(0)

        'Datos principales
        txtID.SetValue(oRow("ID").ToString)

        txtDescripcion.txt.Text = oRow("Descripcion").ToString
        txtCodigoBarra.txt.Text = oRow("CodigoBarra").ToString
        txtReferencia.txt.Text = oRow("Referencia").ToString

        'Agrupadores
        cbxTipoProducto.cbx.Text = oRow("TipoProducto").ToString
        cbxLinea.cbx.Text = oRow("Linea").ToString
        cbxSubLinea.cbx.Text = oRow("SubLinea").ToString
        cbxSubLinea2.cbx.Text = oRow("SubLinea2").ToString
        cbxMarca.cbx.Text = oRow("Marca").ToString
        cbxPresentacion.cbx.Text = oRow("Presentacion").ToString

        cbxCategoria.cbx.Text = oRow("Categoria").ToString
        cbxProveedor.cbx.Text = oRow("ProveedorReferencia").ToString
        cbxDivision.cbx.Text = oRow("Division").ToString
        cbxProcedencia.cbx.Text = oRow("Procedencia").ToString

        'Unidad de Medida
        cbxUnidadMedida.cbx.Text = oRow("Uni. Med.").ToString
        txtUnidadPorCaja.txt.Text = oRow("UnidadPorCaja").ToString
        cbxUnidadMedidaConversion.cbx.Text = oRow("Uni. Med. Conver.").ToString
        txtUnidadConversion.txt.Text = oRow("UnidadConvertir").ToString.Replace(".", ",")
        txtPeso.txt.Text = oRow("Peso").ToString.Replace(".", ",")
        txtVolumen.txt.Text = oRow("Volumen").ToString

        'Configuraciones
        Try
            txtMargenMinimo.SetValue(oRow("MargenMinimo").ToString)
        Catch ex As Exception

        End Try

        cbxImpuesto.cbx.Text = oRow("Impuesto").ToString
        chkActivo.Valor = CBool(oRow("Estado").ToString)
        chkVendible.Valor = CBool(oRow("Vendible").ToString)
        chkControlarExistencia.Valor = CBool(oRow("ControlarExistencia").ToString)
        ocxCuentaCompra.txtDescripcion.Text = oRow("CuentaCompra").ToString
        ocxCuentaVenta.SetValue(oRow("CodigoCuentaVenta").ToString)
        ocxCuentaCompra.SetValue(oRow("CodigoCuentaCompra").ToString)
        ocxCuentaCosto.SetValue(oRow("CodigoCuentaCosto").ToString)
        ocxCuentaDeudor.SetValue(oRow("CodigoCuentaDeudor").ToString)

        'Opciones de descarga
        chkMovimientoCombustible.Valor = CBool(oRow("ConsumoCombustible").ToString)
        chkMateriaPrima.Valor = CBool(oRow("MateriaPrima").ToString)
        chkDescargaCompra.Valor = CBool(oRow("DescargaCompra").ToString)
        chkDescargaStock.Valor = CBool(oRow("DescargaStock").ToString)
        ChkDistribucion.Valor = CBool(oRow("Distribucion").ToString)
        chkInformatica.Valor = CBool(oRow("Informatica").ToString)

        'Lista de Precio
        OcxListaPrecioProducto1.IDProducto = oRow("ID")
        OcxListaPrecioProducto1.ListarPreciosProductos()

        'lista de vendedores
        OcxComisionProducto1.IDProducto = oRow("ID")
        OcxComisionProducto1.ListarComisionProductos()

        'Existencia
        txtExistenciaGeneral.txt.Text = oRow("ExistenciaGeneral").ToString

        'Estadisticos
        txtUltimoCosto.Text = CSistema.FormatoNumero(oRow("UltimoCosto").ToString, False)
        txtUltimoCostoSinIVA.Text = CSistema.FormatoNumero(CSistema.CalcularSinIVA(oRow("IDImpuesto").ToString, oRow("UltimoCosto").ToString, False, True), False)
        txtCostoPromedio.Text = CSistema.FormatoNumero(oRow("CostoPromedio").ToString, False)
        txtCostoCG.Text = CSistema.FormatoNumero(oRow("CostoCG").ToString, False)
        txtFechaUltimaEntrada.Text = oRow("UltimaEntrada").ToString
        txtCantidadUltimaEntrada.Text = CSistema.FormatoNumero(oRow("CantidadEntrada").ToString, False)

        Dim dtLogSucesos As New DataTable
        dtLogSucesos = CSistema.ExecuteToDataTable("Select * from vLogSucesos where tabla = 'PRODUCTO' and Comprobante = '" & oRow("ID").ToString & "' order by id asc")

        Dim INSRow As DataRow() = dtLogSucesos.Select("IDTipoOperacionLog = 1").Clone
        Dim UPDRow As DataRow() = dtLogSucesos.Select("IDTipoOperacionLog = 2 and ID = MAX(ID)").Clone
        'LogSucesos
        If INSRow.Length = 0 Then
            txtUsuarioAlta.Text = "Admin"
            txtFechaAlta.Text = "01/01/2016"
        Else
            txtUsuarioAlta.Text = INSRow(0)("Usuario")
            txtFechaAlta.Text = INSRow(0)("Fecha")
        End If
        If UPDRow.Length = 0 Then
            txtUsuarioModificacion.Text = "Admin"
            txtFechaModificacion.Text = "01/01/2016"
        Else
            txtUsuarioModificacion.Text = UPDRow(0)("Usuario")
            txtFechaModificacion.Text = UPDRow(0)("Fecha")
        End If

        'Configuramos los controles ABM como EDITAR
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Listar Existencias
        ListarExistencias()

        'Zonas
        If TabControl1.SelectedTab.Name.ToUpper = "TABUBICACION" Then
            OcxProductoUbicacion1.IDProducto = oRow("ID")
            OcxProductoUbicacion1.Iniciarlizar()
        End If

        txtID.txt.SelectAll()

        ctrError.Clear()

    End Sub

    Sub ListarProducto()

        Dim frmBuscar As New frmProductoBuscar
        frmBuscar.WindowState = FormWindowState.Normal
        frmBuscar.StartPosition = FormStartPosition.CenterParent
        frmBuscar.ConExistencia = False
        frmBuscar.ShowDialog()
        If frmBuscar.ID > 0 Then
            txtID.txt.Text = frmBuscar.ID
            ObtenerInformacion(" Where ID = " & frmBuscar.ID)
        End If

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Descripcion
        If txtDescripcion.txt.Text.Trim.Length = 0 Then
            Dim Mensaje As String = "Ingrese la Descripcion!"
            ctrError.SetError(txtDescripcion, Mensaje)
            ctrError.SetIconAlignment(txtDescripcion, ErrorIconAlignment.TopRight)

            txtDescripcion.txt.SelectAll()
            txtDescripcion.Focus()

            tsslEstado.Text = "Atencion!!! " & Mensaje

            Exit Sub

        End If

        'Tipo de Producto
        If cbxTipoProducto.cbx.SelectedValue Is Nothing Then
            Dim Mensaje As String = "Ingrese el tipo de producto!"
            ctrError.SetError(cbxTipoProducto, Mensaje)
            ctrError.SetIconAlignment(cbxTipoProducto, ErrorIconAlignment.TopRight)

            cbxTipoProducto.Focus()

            tsslEstado.Text = "Atencion!!! " & Mensaje
        End If

        'SubTipo de Producto
        If cbxTipoProducto.cbx.SelectedValue = 1 Then
            If cbxLinea.cbx.SelectedValue Is Nothing Then
                Dim Mensaje As String = "Ingrese el Subtipo de producto!"
                ctrError.SetError(cbxTipoProducto, Mensaje)
                ctrError.SetIconAlignment(cbxLinea, ErrorIconAlignment.TopRight)

                cbxLinea.Focus()

                tsslEstado.Text = "Atencion!!! " & Mensaje
                Exit Sub
            End If
            'Familia de Producto
            If cbxSubLinea.cbx.SelectedValue Is Nothing Then
                Dim Mensaje As String = "Ingrese la Familia de producto!"
                ctrError.SetError(cbxTipoProducto, Mensaje)
                ctrError.SetIconAlignment(cbxSubLinea, ErrorIconAlignment.TopRight)

                cbxSubLinea.Focus()

                tsslEstado.Text = "Atencion!!! " & Mensaje
                Exit Sub
            End If
            'Produccion
            If cbxSubLinea2.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Ingrese la Produccion del producto"
                ctrError.SetError(cbxSubLinea2, ErrorIconAlignment.TopRight)

                cbxSubLinea2.Focus()

                tsslEstado.Text = "Atencion!!! " & mensaje
                Exit Sub
            End If

        End If

        'Referencia
        If Operacion <> ERP.CSistema.NUMOperacionesABM.DEL Then
            If txtReferencia.txt.Text.Trim.Length = 0 Then
                Dim Mensaje As String = "Ingrese una referencia!"
                ctrError.SetError(txtReferencia, Mensaje)
                ctrError.SetIconAlignment(txtReferencia, ErrorIconAlignment.TopRight)

                txtReferencia.txt.SelectAll()
                txtReferencia.Focus()

                tsslEstado.Text = "Atencion!!! " & Mensaje

                Exit Sub
            End If
        End If

        'Presentacion
        If Operacion <> ERP.CSistema.NUMOperacionesABM.DEL Then
            If cbxPresentacion.cbx.SelectedValue Is Nothing Then
                Dim Mensaje As String = "Ingrese la presentacion del producto!"
                ctrError.SetError(cbxPresentacion, Mensaje)
                ctrError.SetIconAlignment(cbxPresentacion, ErrorIconAlignment.TopRight)

                cbxPresentacion.Focus()

                tsslEstado.Text = "Atencion!!! " & Mensaje

                Exit Sub
            End If
        End If

        'Cuando el producto tenga tipo de producto DESCUENTOS no asignarle cuentas contables
        If cbxTipoProducto.cbx.SelectedValue <> 34 Then
            VerificarCuentasObligatorias()
        End If


        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        'Datos Obligatorio
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Identificadores
        CSistema.SetSQLParameter(param, "@CodigoBarra", txtCodigoBarra.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Referencia", txtReferencia.txt.Text, ParameterDirection.Input)

        'Agrupadores
        CSistema.SetSQLParameter(param, "@IDTipoProducto", cbxTipoProducto.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDLinea", cbxLinea.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSubLinea", cbxSubLinea.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSubLinea2", cbxSubLinea2.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMarca", cbxMarca.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDPresentacion", cbxPresentacion.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCategoria", cbxCategoria.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProveedor", cbxProveedor.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDivision", cbxDivision.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProcedencia", cbxProcedencia.cbx, ParameterDirection.Input)

        'Unidad de Medida
        CSistema.SetSQLParameter(param, "@IDUnidadMedida", cbxUnidadMedida.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@UnidadPorCaja", txtUnidadPorCaja.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUnidadMedidaConvertir", cbxUnidadMedidaConversion.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@UnidadConvertir", txtUnidadConversion.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Peso", CSistema.FormatoNumeroBaseDatos(txtPeso.GetValue, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Volumen", txtVolumen.txt.Text, ParameterDirection.Input)

        'Configuraciones
        CSistema.SetSQLParameter(param, "@MargenMinimo", CSistema.FormatoNumeroBaseDatos(txtMargenMinimo.ObtenerValor, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDImpuesto", cbxImpuesto.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", chkActivo.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Vendible", chkVendible.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ControlarExistencia", chkControlarExistencia.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaContableCompra", ocxCuentaCompra.txtCodigo.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaContableVenta", ocxCuentaVenta.txtCodigo.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaContableCosto", ocxCuentaCosto.txtCodigo.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaContableDeudor", ocxCuentaDeudor.txtCodigo.GetValue, ParameterDirection.Input)

        'Opciones de descarga
        CSistema.SetSQLParameter(param, "@ConsumoCombustible", chkMovimientoCombustible.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@MateriaPrima", chkMateriaPrima.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DescargaCompra", chkDescargaCompra.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Distribucion", ChkDistribucion.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Informatica", chkInformatica.Valor.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpProducto", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            txtReferencia.txt.ReadOnly = False
            txtReferencia.txt.Focus()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
                ctrError.SetError(btnEliminar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopRight)
            Else
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            End If

        End If
        ObtenerInformacion(" Where Referencia = '" & txtReferencia.GetValue & "' ")


    End Sub

    Sub ListarEnCascada()

        If cbxTipoProducto.cbx.Focused Then
            cbxLinea.SelectedValue(0)
            cbxSubLinea.SelectedValue(0)
            cbxSubLinea2.SelectedValue(0)
            cbxMarca.SelectedValue(0)
            cbxPresentacion.SelectedValue(0)
        End If

        If cbxLinea.cbx.Focused Then
            cbxSubLinea.SelectedValue(0)
            cbxSubLinea2.SelectedValue(0)
            cbxMarca.SelectedValue(0)
            cbxPresentacion.SelectedValue(0)
        End If

        If cbxSubLinea.cbx.Focused Then
            cbxSubLinea2.SelectedValue(0)
            cbxMarca.SelectedValue(0)
            cbxPresentacion.SelectedValue(0)
        End If

        If cbxSubLinea2.cbx.Focused Then
            cbxMarca.SelectedValue(0)
            cbxPresentacion.SelectedValue(0)
        End If

        If cbxMarca.cbx.Focused Then
            cbxPresentacion.SelectedValue(0)
        End If

    End Sub

    Sub Nuevo()

        ctrError.Clear()
        CSistema.InicializaControles(Me)
        CSistema.InicializaControles(tabIdentificacion)
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Producto"), Integer)
        vNuevo = True
        txtReferencia.txt.Focus()
        btnBuscar.Enabled = False
        txtReferencia.txt.Text = ""
        txtDescripcion.txt.Text = ""
        txtCodigoBarra.txt.Text = ""

        'Configuraciones
        cbxImpuesto.cbx.SelectedValue = 1
        chkActivo.Valor = False
        chkControlarExistencia.Valor = True

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada

        'If e.KeyCode = Keys.Enter Then
        '    ObtenerInformacion()
        'End If

        'If e.KeyCode = Keys.Up Then
        '    Dim ID As String
        '    ID = txtID.txt.Text

        '    If IsNumeric(ID) = False Then
        '        Exit Sub
        '    End If

        '    ID = CInt(ID) + 1
        '    txtID.txt.Text = ID
        '    txtID.txt.SelectAll()
        '    ObtenerInformacion()

        'End If

        'If e.KeyCode = Keys.Down Then
        '    Dim ID As String
        '    ID = txtID.txt.Text

        '    If IsNumeric(ID) = False Then
        '        Exit Sub
        '    End If

        '    If CInt(ID) = 1 Then
        '        Exit Sub
        '    End If

        '    ID = CInt(ID) - 1
        '    txtID.txt.Text = ID
        '    txtID.txt.SelectAll()
        '    ObtenerInformacion()

        'End If

        'If e.KeyCode = Keys.End Then

        '    Dim ID As Integer
        '    ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Producto"), Integer)

        '    txtID.txt.Text = ID
        '    txtID.txt.SelectAll()
        '    ObtenerInformacion()

        'End If

        'If e.KeyCode = Keys.Home Then

        '    Dim ID As Integer
        '    ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(ID), 1) From Producto"), Integer)

        '    txtID.txt.Text = ID
        '    txtID.txt.SelectAll()
        '    ObtenerInformacion()

        'End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        txtReferencia.txt.Focus()
        vNuevo = False
        btnBuscar.Enabled = True

        'Listar el ultimo registro
        ManejoTecla(New KeyEventArgs(Keys.End))
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        txtID.txt.ReadOnly = True
        btnNuevo.Enabled = False
        vNuevo = False

        txtDescripcion.txt.Focus()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        ListarProducto()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmProducto_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        'If e.KeyCode = Keys.F1 Then
        '    ListarProducto()
        'End If

        If e.KeyCode = Keys.Enter Then

            If Me.ActiveControl.Name = Me.OcxProductoUbicacion1.Name Then
                Exit Sub
            End If

            If txtID.txt.Focused = True Or txtID.Focused = True Then
                Exit Sub
            End If

            If txtReferencia.txt.Focused = True Or txtReferencia.Focused = True Then
                Exit Sub
            End If

            CSistema.SelectNextControl(Me, e.KeyCode)
        End If

    End Sub

    Private Sub frmProducto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
        InicializarExistencia()
    End Sub

    Private Sub cbxTipoProducto_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoProducto.PropertyChanged

        cbxLinea.cbx.Text = ""

        'Listar Lineas
        If IsNumeric(cbxTipoProducto.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxLinea.cbx, CData.GetTable("VTipoProductoLinea", " IDTipoProducto=" & cbxTipoProducto.GetValue), "IDLinea", "Linea")
        dtCuentasContablesObligatorias = CSistema.ExecuteToDataTable("Select * from vTipoProductoCuentaContableObligatoria where IDTipoProducto = " & cbxTipoProducto.GetValue)
        ListarEnCascada()

    End Sub

    Private Sub cbxProveedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxProveedor.PropertyChanged
        cbxDivision.cbx.Text = ""

        'Listar Lineas
        If IsNumeric(cbxProveedor.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxDivision.cbx, "Select ID, Descripcion From Division Where IDProveedor=" & cbxProveedor.cbx.SelectedValue & " Order By Descripcion ")

    End Sub

#Region "EXISTENCIA"

    Sub InicializarExistencia()

        'Funciones
        CargarInformacionExistencia()

    End Sub

    Sub CargarInformacionExistencia()

        'Sucursal-Deposito
        CSistema.SqlToComboBox(cbxSucursalDeposito.cbx, CData.GetTable("VDeposito").Copy, "ID", "Suc-Dep")


    End Sub

    Sub ListarExistencias()

        If TabControl1.SelectedTab.Name.ToUpper <> "TABEXISTENCIA" Then
            Exit Sub
        End If

        dgvExistencia.DataSource = Nothing

        InicializarExistencia()

        CSistema.SqlToDataGrid(dgvExistencia, "Select 'ID'=IDDeposito, ED.[Suc-Dep], ED.Existencia, 'Reservado'=" & vgOwnerDBFunction & ".FExistenciaProductoReservado(ED.IDProducto, ED.IDDeposito), 'Minima'=ED.ExistenciaMinima, 'Critica'=ED.ExistenciaCritica From VExistenciaDeposito ED Where ED.IDProducto=" & txtID.ObtenerValor & " Order By ED.Sucursal")

        'Formato
        dgvExistencia.Columns("Suc-Dep").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvExistencia.Columns("Existencia").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvExistencia.Columns("Reservado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvExistencia.Columns("Minima").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvExistencia.Columns("Critica").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgvExistencia.Columns("Existencia").DefaultCellStyle.Format = "N2"
        dgvExistencia.Columns("Reservado").DefaultCellStyle.Format = "N2"
        dgvExistencia.Columns("Minima").DefaultCellStyle.Format = "N2"
        dgvExistencia.Columns("Critica").DefaultCellStyle.Format = "N2"

        txtExistenciaGeneral.SetValue(CSistema.gridSumColumn(dgvExistencia, "Existencia"))


    End Sub

    Sub ObtenerInformacionExistencia()

        'Validar
        'Si es que se selecciono el registro.
        If dgvExistencia.SelectedRows.Count = 0 Then
            ctrError.SetError(dgvExistencia, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgvExistencia, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el ID Registro
        Dim IDDeposito As Integer

        IDDeposito = dgvExistencia.SelectedRows(0).Cells("ID").Value


        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select ED.[Suc-Dep], ED.ExistenciaMinima, ED.ExistenciaCritica From VExistenciaDeposito ED Where ED.IDProducto=" & txtID.txt.Text & " And ED.IDDeposito=" & IDDeposito)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            cbxSucursalDeposito.cbx.Text = oRow("Suc-Dep").ToString
            txtExistenciaMinima.txt.Text = oRow("ExistenciaMinima").ToString
            txtExistenciaCritica.txt.Text = oRow("ExistenciaCritica").ToString

        End If

        ctrError.Clear()

    End Sub

    Sub BloquearCostos()
        lblUltimoCosto.Visible = vgVerCosto
        txtUltimoCosto.Visible = vgVerCosto
        lblUltimoCostoSinIVA.Visible = vgVerCosto
        txtUltimoCostoSinIVA.Visible = vgVerCosto
        lblCostoCG.Visible = vgVerCosto
        txtCostoCG.Visible = vgVerCosto
        lblCostoPromedio.Visible = vgVerCosto
        txtCostoPromedio.Visible = vgVerCosto
    End Sub

    Sub ProcesarExistencia()

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Seleccion de Deposito
        If IsNumeric(cbxSucursalDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(cbxSucursalDeposito, mensaje)
            ctrError.SetIconAlignment(cbxSucursalDeposito, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Procesar
        'Obtener el IDProducto
        Dim IDProducto As Integer

        IDProducto = txtID.txt.Text

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@IDProducto", IDProducto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", cbxSucursalDeposito.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ExistenciaMinima", CSistema.FormatoMonedaBaseDatos(txtExistenciaMinima.ObtenerValor, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ExistenciaCritica", CSistema.FormatoMonedaBaseDatos(txtExistenciaCritica.ObtenerValor, True), ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpExistenciaDeposito", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            ctrError.Clear()
            ListarExistencias()

        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnActualizarExistencia, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnActualizarExistencia, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub VerificarCuentasObligatorias()

        If dtCuentasContablesObligatorias Is Nothing Then
            dtCuentasContablesObligatorias = CSistema.ExecuteToDataTable("Select * from vTipoProductoCuentaContableObligatoria where IDTipoProducto = " & cbxTipoProducto.GetValue)
        End If

        If dtCuentasContablesObligatorias.Rows.Count = 0 Then
            dtCuentasContablesObligatorias = CSistema.ExecuteToDataTable("Select * from vTipoProductoCuentaContableObligatoria where IDTipoProducto = " & cbxTipoProducto.GetValue)
        End If

        If CBool(dtCuentasContablesObligatorias.Rows(0)("CuentaContableVenta")) Then
            If ocxCuentaVenta.txtCodigo.Texto = "" Then
                MessageBox.Show("Debe seleccionar cuenta contable de venta", "Atencion", MessageBoxButtons.OK)
                chkActivo.chk.Checked = False
                ocxCuentaVenta.Focus()
                Exit Sub
            End If
        End If
        If CBool(dtCuentasContablesObligatorias.Rows(0)("CuentaContableDeudor")) Then
            If ocxCuentaDeudor.txtCodigo.Texto = "" Then
                MessageBox.Show("Debe seleccionar cuenta contable de deudor", "Atencion", MessageBoxButtons.OK)
                chkActivo.chk.Checked = False
                ocxCuentaDeudor.Focus()
                Exit Sub
            End If
        End If
        If CBool(dtCuentasContablesObligatorias.Rows(0)("CuentaContableCompra")) Then
            If ocxCuentaCompra.txtCodigo.Texto = "" Then
                MessageBox.Show("Debe seleccionar cuenta contable de compra", "Atencion", MessageBoxButtons.OK)
                chkActivo.chk.Checked = False
                ocxCuentaCompra.Focus()
                Exit Sub
            End If
        End If
        If CBool(dtCuentasContablesObligatorias.Rows(0)("CuentaContableCosto")) Then
            If ocxCuentaCosto.txtCodigo.Texto = "" Then
                MessageBox.Show("Debe seleccionar cuenta contable de costo", "Atencion", MessageBoxButtons.OK)
                chkActivo.chk.Checked = False
                ocxCuentaCosto.Focus()
                Exit Sub
            End If
        End If
    End Sub

    Private Sub btnActualizarExistencia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizarExistencia.Click
        ProcesarExistencia()
    End Sub

    Private Sub dgvExistencia_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvExistencia.SelectionChanged
        ObtenerInformacionExistencia()
    End Sub

#End Region

    Private Sub cbxTipoProducto_Editar(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxTipoProducto.Editar
        Dim frm As New frmTipoProducto
        CSistema.ShowForm(frm, Me)
    End Sub

    Private Sub cbxLinea_Editar(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxLinea.Editar
        'Dim frm As New frm
        'CSistema.ShowForm(frm, Me)
    End Sub

    Private Sub ocxCuenta_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocxCuentaCompra.ItemSeleccionado
        ocxCuentaVenta.Focus()
    End Sub

    Private Sub ocxCuentaVenta_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocxCuentaVenta.ItemSeleccionado
        btnGuardar.Focus()
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged
        ObtenerInformacion(" Where ID = " & txtID.ObtenerValor)
    End Sub

    Private Sub cbxLinea_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxLinea.PropertyChanged

        cbxSubLinea.cbx.Text = ""

        'Listar Lineas
        If IsNumeric(cbxTipoProducto.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxLinea.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxSubLinea.cbx, CData.GetTable("VLineaSubLinea", " IDTipoProducto=" & cbxTipoProducto.GetValue & " And IDLinea = " & cbxLinea.GetValue), "IDSubLinea", "SubLinea")

        ListarEnCascada()

    End Sub

    Private Sub cbxSubLinea_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSubLinea.PropertyChanged

        cbxSubLinea2.cbx.Text = ""

        'Listar Lineas
        If IsNumeric(cbxTipoProducto.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxLinea.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxSubLinea.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxSubLinea2.cbx, CData.GetTable("VSubLineaSubLinea2", " IDTipoProducto=" & cbxTipoProducto.GetValue & " And IDLinea = " & cbxLinea.GetValue & " And IDSubLinea = " & cbxSubLinea.GetValue), "IDSubLinea2", "SubLinea2")

        ListarEnCascada()

    End Sub

    Private Sub cbxSubLinea2_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSubLinea2.PropertyChanged

        cbxMarca.cbx.Text = ""

        'Listar Lineas
        If IsNumeric(cbxTipoProducto.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxLinea.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxSubLinea.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxSubLinea2.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxMarca.cbx, CData.GetTable("VSubLinea2Marca", " IDTipoProducto=" & cbxTipoProducto.GetValue & " And IDLinea = " & cbxLinea.GetValue & " And IDSubLinea = " & cbxSubLinea.GetValue & " And IDSubLinea2 = " & cbxSubLinea2.GetValue), "IDMarca", "Marca")

        ListarEnCascada()

    End Sub

    Private Sub cbxMarca_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxMarca.PropertyChanged

        cbxPresentacion.cbx.Text = ""

        'Listar Lineas
        If IsNumeric(cbxTipoProducto.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxLinea.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxSubLinea.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxSubLinea2.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxMarca.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxPresentacion.cbx, CData.GetTable("VMarcaPresentacion", " IDTipoProducto=" & cbxTipoProducto.GetValue & " And IDLinea = " & cbxLinea.GetValue & " And IDSubLinea = " & cbxSubLinea.GetValue & " And IDSubLinea2 = " & cbxSubLinea2.GetValue & " And IDMarca = " & cbxMarca.GetValue), "IDPresentacion", "Presentacion")

        ListarEnCascada()

    End Sub

    Private Sub txtPeso_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPeso.TeclaPrecionada

        'Verificar valor
        If IsNumeric(txtPeso.GetValue) = False Then
            txtPeso.txt.Text = "0"
        End If

    End Sub

    Private Sub txtReferencia_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtReferencia.TeclaPrecionada
        ManejoTecla(e)
    End Sub

    Sub ManejoTecla(ByVal e As KeyEventArgs)

        If e.KeyCode = Keys.Enter Then

            If vNuevo = True Then
                CSistema.SelectNextControl(Me, e.KeyCode)
                Exit Sub
            End If

            ObtenerInformacion(" Where Referencia = '" & txtReferencia.GetValue & "' ")

        End If

        If e.KeyCode = Keys.F1 Then
            ListarProducto()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            ObtenerInformacion(" Where ID = " & txtID.ObtenerValor)
            Exit Sub
        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            ObtenerInformacion(" Where ID = " & txtID.ObtenerValor)

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Producto"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            ObtenerInformacion(" Where ID = " & txtID.ObtenerValor)

        End If


        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(ID), 1) From Producto"), Integer)

            txtID.txt.Text = ID
            'txtID.txt.SelectAll()
            ObtenerInformacion(" Where ID = " & txtID.ObtenerValor)

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If
    End Sub

    Private Sub chkActivo_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkActivo.PropertyChanged
        If Not vNuevo Then
            Exit Sub
        End If
        If value Then
            VerificarCuentasObligatorias()
        End If

    End Sub

    Private Sub frmProducto_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        'FA 20/06/2023
        LiberarMemoria()
    End Sub

    Private Sub OcxListaPrecioProducto1_Load(sender As Object, e As EventArgs) Handles OcxListaPrecioProducto1.Load

    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTipoComprobante
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTipoComprobante))
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.lblCodigo = New System.Windows.Forms.Label()
        Me.lblResumen = New System.Windows.Forms.Label()
        Me.gbxSigno = New System.Windows.Forms.GroupBox()
        Me.rdbNegativo = New System.Windows.Forms.RadioButton()
        Me.rdbPositivo = New System.Windows.Forms.RadioButton()
        Me.txtResumen = New ERP.ocxTXTString()
        Me.txtCodigo = New ERP.ocxTXTString()
        Me.cbxOperacion = New ERP.ocxCBX()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.dgvLista = New System.Windows.Forms.DataGridView()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtBuscar = New ERP.ocxTXTString()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkHabilitarDepositoBancario = New ERP.ocxCHK()
        Me.chkComprobanteResta = New ERP.ocxCHK()
        Me.chkEsInterno = New ERP.ocxCHK()
        Me.chkHechaukaTimbradoReemplazar = New ERP.ocxCHK()
        Me.chkCalcularIVA = New ERP.ocxCHK()
        Me.chkIncluirEnLibro = New ERP.ocxCHK()
        Me.chkIVAIncluido = New ERP.ocxCHK()
        Me.chkComprobanteTimbrado = New ERP.ocxCHK()
        Me.chkAutonumerico = New ERP.ocxCHK()
        Me.txtHechaukaTimbradoReemplazar = New ERP.ocxTXTString()
        Me.txtHechaukaTipoDocumento = New ERP.ocxTXTNumeric()
        Me.lblHechaukaTipoDocumento = New System.Windows.Forms.Label()
        Me.lblHechauka = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblTipoFiltro = New System.Windows.Forms.Label()
        Me.cbxTipoFiltro = New ERP.ocxCBX()
        Me.lblOperacionFiltro = New System.Windows.Forms.Label()
        Me.cbxOperacionFiltro = New ERP.ocxCBX()
        Me.lklListarTodos = New System.Windows.Forms.LinkLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkExento = New ERP.ocxCHK()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.gbxSigno.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 572)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1003, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(149, 188)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 14
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(88, 188)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 13
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(16, 190)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 12
        Me.lblEstado.Text = "Estado:"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(16, 111)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 6
        Me.lblDescripcion.Text = "Descripción:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(16, 34)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(16, 57)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(31, 13)
        Me.lblTipo.TabIndex = 2
        Me.lblTipo.Text = "Tipo:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(16, 84)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 4
        Me.lblOperacion.Text = "Operacion:"
        '
        'lblCodigo
        '
        Me.lblCodigo.AutoSize = True
        Me.lblCodigo.Location = New System.Drawing.Point(16, 138)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(43, 13)
        Me.lblCodigo.TabIndex = 8
        Me.lblCodigo.Text = "Codigo:"
        '
        'lblResumen
        '
        Me.lblResumen.AutoSize = True
        Me.lblResumen.Location = New System.Drawing.Point(16, 165)
        Me.lblResumen.Name = "lblResumen"
        Me.lblResumen.Size = New System.Drawing.Size(55, 13)
        Me.lblResumen.TabIndex = 10
        Me.lblResumen.Text = "Resumen:"
        '
        'gbxSigno
        '
        Me.gbxSigno.BackColor = System.Drawing.SystemColors.Control
        Me.gbxSigno.Controls.Add(Me.rdbNegativo)
        Me.gbxSigno.Controls.Add(Me.rdbPositivo)
        Me.gbxSigno.Location = New System.Drawing.Point(116, 292)
        Me.gbxSigno.Name = "gbxSigno"
        Me.gbxSigno.Size = New System.Drawing.Size(147, 30)
        Me.gbxSigno.TabIndex = 22
        Me.gbxSigno.TabStop = False
        '
        'rdbNegativo
        '
        Me.rdbNegativo.AutoSize = True
        Me.rdbNegativo.ForeColor = System.Drawing.Color.Black
        Me.rdbNegativo.Location = New System.Drawing.Point(71, 9)
        Me.rdbNegativo.Name = "rdbNegativo"
        Me.rdbNegativo.Size = New System.Drawing.Size(68, 17)
        Me.rdbNegativo.TabIndex = 1
        Me.rdbNegativo.TabStop = True
        Me.rdbNegativo.Text = "Negativo"
        Me.rdbNegativo.UseVisualStyleBackColor = True
        '
        'rdbPositivo
        '
        Me.rdbPositivo.AutoSize = True
        Me.rdbPositivo.ForeColor = System.Drawing.Color.Black
        Me.rdbPositivo.Location = New System.Drawing.Point(3, 9)
        Me.rdbPositivo.Name = "rdbPositivo"
        Me.rdbPositivo.Size = New System.Drawing.Size(62, 17)
        Me.rdbPositivo.TabIndex = 0
        Me.rdbPositivo.TabStop = True
        Me.rdbPositivo.Text = "Positivo"
        Me.rdbPositivo.UseVisualStyleBackColor = True
        '
        'txtResumen
        '
        Me.txtResumen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtResumen.Color = System.Drawing.Color.Empty
        Me.txtResumen.Indicaciones = Nothing
        Me.txtResumen.Location = New System.Drawing.Point(94, 161)
        Me.txtResumen.Multilinea = False
        Me.txtResumen.Name = "txtResumen"
        Me.txtResumen.Size = New System.Drawing.Size(153, 21)
        Me.txtResumen.SoloLectura = False
        Me.txtResumen.TabIndex = 11
        Me.txtResumen.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtResumen.Texto = ""
        '
        'txtCodigo
        '
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Color = System.Drawing.Color.Empty
        Me.txtCodigo.Indicaciones = Nothing
        Me.txtCodigo.Location = New System.Drawing.Point(94, 134)
        Me.txtCodigo.Multilinea = False
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(65, 21)
        Me.txtCodigo.SoloLectura = False
        Me.txtCodigo.TabIndex = 9
        Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCodigo.Texto = ""
        '
        'cbxOperacion
        '
        Me.cbxOperacion.CampoWhere = Nothing
        Me.cbxOperacion.CargarUnaSolaVez = False
        Me.cbxOperacion.DataDisplayMember = Nothing
        Me.cbxOperacion.DataFilter = Nothing
        Me.cbxOperacion.DataOrderBy = Nothing
        Me.cbxOperacion.DataSource = Nothing
        Me.cbxOperacion.DataValueMember = Nothing
        Me.cbxOperacion.FormABM = Nothing
        Me.cbxOperacion.Indicaciones = Nothing
        Me.cbxOperacion.Location = New System.Drawing.Point(94, 80)
        Me.cbxOperacion.Name = "cbxOperacion"
        Me.cbxOperacion.SeleccionObligatoria = False
        Me.cbxOperacion.Size = New System.Drawing.Size(225, 21)
        Me.cbxOperacion.SoloLectura = False
        Me.cbxOperacion.TabIndex = 5
        Me.cbxOperacion.Texto = ""
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = Nothing
        Me.cbxTipo.CargarUnaSolaVez = False
        Me.cbxTipo.DataDisplayMember = Nothing
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = Nothing
        Me.cbxTipo.DataSource = Nothing
        Me.cbxTipo.DataValueMember = Nothing
        Me.cbxTipo.FormABM = Nothing
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(94, 53)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionObligatoria = False
        Me.cbxTipo.Size = New System.Drawing.Size(225, 21)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 3
        Me.cbxTipo.Texto = ""
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(94, 107)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(225, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 7
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(94, 29)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 1
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLista.Location = New System.Drawing.Point(3, 67)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.Size = New System.Drawing.Size(661, 465)
        Me.dgvLista.TabIndex = 2
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 336.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvLista, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1003, 572)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.txtBuscar)
        Me.FlowLayoutPanel3.Controls.Add(Me.Button1)
        Me.FlowLayoutPanel3.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtCantidad)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(661, 24)
        Me.FlowLayoutPanel3.TabIndex = 0
        '
        'txtBuscar
        '
        Me.txtBuscar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBuscar.Color = System.Drawing.Color.Empty
        Me.txtBuscar.Indicaciones = Nothing
        Me.txtBuscar.Location = New System.Drawing.Point(3, 3)
        Me.txtBuscar.Multilinea = False
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(368, 21)
        Me.txtBuscar.SoloLectura = False
        Me.txtBuscar.TabIndex = 0
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBuscar.Texto = ""
        '
        'Button1
        '
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.ImageIndex = 6
        Me.Button1.ImageList = Me.ImageList1
        Me.Button1.Location = New System.Drawing.Point(377, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(20, 21)
        Me.Button1.TabIndex = 4
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "Agregar.png")
        Me.ImageList1.Images.SetKeyName(1, "Editar.png")
        Me.ImageList1.Images.SetKeyName(2, "Eliminar.png")
        Me.ImageList1.Images.SetKeyName(3, "Guardar.png")
        Me.ImageList1.Images.SetKeyName(4, "Cancelar.png")
        Me.ImageList1.Images.SetKeyName(5, "Actualizar.png")
        Me.ImageList1.Images.SetKeyName(6, "Buscar.png")
        Me.ImageList1.Images.SetKeyName(7, "Cargar.png")
        Me.ImageList1.Images.SetKeyName(8, "Imprimir.png")
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(403, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 24)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Cant.:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = Nothing
        Me.txtCantidad.Location = New System.Drawing.Point(444, 3)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(63, 20)
        Me.txtCantidad.SoloLectura = True
        Me.txtCantidad.TabIndex = 3
        Me.txtCantidad.TabStop = False
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnNuevo)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnEditar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnEliminar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnImprimir)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 538)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(661, 31)
        Me.FlowLayoutPanel1.TabIndex = 3
        '
        'btnNuevo
        '
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.ImageIndex = 0
        Me.btnNuevo.ImageList = Me.ImageList1
        Me.btnNuevo.Location = New System.Drawing.Point(3, 3)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(82, 23)
        Me.btnNuevo.TabIndex = 0
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEditar.ImageIndex = 1
        Me.btnEditar.ImageList = Me.ImageList1
        Me.btnEditar.Location = New System.Drawing.Point(91, 3)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 1
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.ImageIndex = 2
        Me.btnEliminar.ImageList = Me.ImageList1
        Me.btnEliminar.Location = New System.Drawing.Point(172, 3)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(83, 23)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImprimir.ImageKey = "Imprimir.png"
        Me.btnImprimir.ImageList = Me.ImageList1
        Me.btnImprimir.Location = New System.Drawing.Point(261, 3)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(87, 23)
        Me.btnImprimir.TabIndex = 3
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.btnSalir)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnGuardar)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(670, 538)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(330, 31)
        Me.FlowLayoutPanel2.TabIndex = 5
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Location = New System.Drawing.Point(252, 3)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 2
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.ImageIndex = 4
        Me.btnCancelar.ImageList = Me.ImageList1
        Me.btnCancelar.Location = New System.Drawing.Point(156, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(90, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGuardar.ImageIndex = 3
        Me.btnGuardar.ImageList = Me.ImageList1
        Me.btnGuardar.Location = New System.Drawing.Point(60, 3)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(90, 23)
        Me.btnGuardar.TabIndex = 0
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkExento)
        Me.GroupBox1.Controls.Add(Me.chkHabilitarDepositoBancario)
        Me.GroupBox1.Controls.Add(Me.chkComprobanteResta)
        Me.GroupBox1.Controls.Add(Me.chkEsInterno)
        Me.GroupBox1.Controls.Add(Me.chkHechaukaTimbradoReemplazar)
        Me.GroupBox1.Controls.Add(Me.chkCalcularIVA)
        Me.GroupBox1.Controls.Add(Me.chkIncluirEnLibro)
        Me.GroupBox1.Controls.Add(Me.chkIVAIncluido)
        Me.GroupBox1.Controls.Add(Me.chkComprobanteTimbrado)
        Me.GroupBox1.Controls.Add(Me.chkAutonumerico)
        Me.GroupBox1.Controls.Add(Me.txtHechaukaTimbradoReemplazar)
        Me.GroupBox1.Controls.Add(Me.txtHechaukaTipoDocumento)
        Me.GroupBox1.Controls.Add(Me.lblHechaukaTipoDocumento)
        Me.GroupBox1.Controls.Add(Me.lblHechauka)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.gbxSigno)
        Me.GroupBox1.Controls.Add(Me.lblID)
        Me.GroupBox1.Controls.Add(Me.lblTipo)
        Me.GroupBox1.Controls.Add(Me.lblDescripcion)
        Me.GroupBox1.Controls.Add(Me.cbxTipo)
        Me.GroupBox1.Controls.Add(Me.cbxOperacion)
        Me.GroupBox1.Controls.Add(Me.txtDescripcion)
        Me.GroupBox1.Controls.Add(Me.txtResumen)
        Me.GroupBox1.Controls.Add(Me.lblOperacion)
        Me.GroupBox1.Controls.Add(Me.txtID)
        Me.GroupBox1.Controls.Add(Me.lblCodigo)
        Me.GroupBox1.Controls.Add(Me.lblResumen)
        Me.GroupBox1.Controls.Add(Me.rdbDesactivado)
        Me.GroupBox1.Controls.Add(Me.lblEstado)
        Me.GroupBox1.Controls.Add(Me.txtCodigo)
        Me.GroupBox1.Controls.Add(Me.rdbActivo)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(670, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.TableLayoutPanel1.SetRowSpan(Me.GroupBox1, 3)
        Me.GroupBox1.Size = New System.Drawing.Size(330, 529)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos"
        '
        'chkHabilitarDepositoBancario
        '
        Me.chkHabilitarDepositoBancario.BackColor = System.Drawing.Color.Transparent
        Me.chkHabilitarDepositoBancario.Color = System.Drawing.Color.Empty
        Me.chkHabilitarDepositoBancario.Location = New System.Drawing.Point(18, 351)
        Me.chkHabilitarDepositoBancario.Name = "chkHabilitarDepositoBancario"
        Me.chkHabilitarDepositoBancario.Size = New System.Drawing.Size(245, 17)
        Me.chkHabilitarDepositoBancario.SoloLectura = False
        Me.chkHabilitarDepositoBancario.TabIndex = 24
        Me.chkHabilitarDepositoBancario.Texto = "Habilitar para Deposito Bancario"
        Me.chkHabilitarDepositoBancario.Valor = False
        Me.chkHabilitarDepositoBancario.Visible = False
        '
        'chkComprobanteResta
        '
        Me.chkComprobanteResta.BackColor = System.Drawing.Color.Transparent
        Me.chkComprobanteResta.Color = System.Drawing.Color.Empty
        Me.chkComprobanteResta.Location = New System.Drawing.Point(18, 324)
        Me.chkComprobanteResta.Name = "chkComprobanteResta"
        Me.chkComprobanteResta.Size = New System.Drawing.Size(245, 21)
        Me.chkComprobanteResta.SoloLectura = False
        Me.chkComprobanteResta.TabIndex = 23
        Me.chkComprobanteResta.Texto = "Comprobante de Resta."
        Me.chkComprobanteResta.Valor = False
        Me.chkComprobanteResta.Visible = False
        '
        'chkEsInterno
        '
        Me.chkEsInterno.BackColor = System.Drawing.Color.Transparent
        Me.chkEsInterno.Color = System.Drawing.Color.Empty
        Me.chkEsInterno.Location = New System.Drawing.Point(207, 276)
        Me.chkEsInterno.Name = "chkEsInterno"
        Me.chkEsInterno.Size = New System.Drawing.Size(85, 17)
        Me.chkEsInterno.SoloLectura = False
        Me.chkEsInterno.TabIndex = 20
        Me.chkEsInterno.Texto = "Doc. interno"
        Me.ToolTip1.SetToolTip(Me.chkEsInterno, "No genera asientos contables")
        Me.chkEsInterno.Valor = False
        '
        'chkHechaukaTimbradoReemplazar
        '
        Me.chkHechaukaTimbradoReemplazar.BackColor = System.Drawing.Color.Transparent
        Me.chkHechaukaTimbradoReemplazar.Color = System.Drawing.Color.Empty
        Me.chkHechaukaTimbradoReemplazar.Location = New System.Drawing.Point(18, 427)
        Me.chkHechaukaTimbradoReemplazar.Name = "chkHechaukaTimbradoReemplazar"
        Me.chkHechaukaTimbradoReemplazar.Size = New System.Drawing.Size(188, 17)
        Me.chkHechaukaTimbradoReemplazar.SoloLectura = False
        Me.chkHechaukaTimbradoReemplazar.TabIndex = 28
        Me.chkHechaukaTimbradoReemplazar.Texto = "Reemplazar el nro de timbrado por:"
        Me.chkHechaukaTimbradoReemplazar.Valor = False
        '
        'chkCalcularIVA
        '
        Me.chkCalcularIVA.BackColor = System.Drawing.Color.Transparent
        Me.chkCalcularIVA.Color = System.Drawing.Color.Empty
        Me.chkCalcularIVA.Location = New System.Drawing.Point(18, 276)
        Me.chkCalcularIVA.Name = "chkCalcularIVA"
        Me.chkCalcularIVA.Size = New System.Drawing.Size(85, 17)
        Me.chkCalcularIVA.SoloLectura = False
        Me.chkCalcularIVA.TabIndex = 18
        Me.chkCalcularIVA.Texto = "Calcular IVA"
        Me.chkCalcularIVA.Valor = False
        '
        'chkIncluirEnLibro
        '
        Me.chkIncluirEnLibro.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirEnLibro.Color = System.Drawing.Color.Empty
        Me.chkIncluirEnLibro.Location = New System.Drawing.Point(18, 301)
        Me.chkIncluirEnLibro.Name = "chkIncluirEnLibro"
        Me.chkIncluirEnLibro.Size = New System.Drawing.Size(98, 17)
        Me.chkIncluirEnLibro.SoloLectura = False
        Me.chkIncluirEnLibro.TabIndex = 21
        Me.chkIncluirEnLibro.Texto = "Incluir en libro:"
        Me.chkIncluirEnLibro.Valor = False
        '
        'chkIVAIncluido
        '
        Me.chkIVAIncluido.BackColor = System.Drawing.Color.Transparent
        Me.chkIVAIncluido.Color = System.Drawing.Color.Empty
        Me.chkIVAIncluido.Location = New System.Drawing.Point(116, 276)
        Me.chkIVAIncluido.Name = "chkIVAIncluido"
        Me.chkIVAIncluido.Size = New System.Drawing.Size(85, 17)
        Me.chkIVAIncluido.SoloLectura = False
        Me.chkIVAIncluido.TabIndex = 19
        Me.chkIVAIncluido.Texto = "IVA Incluido"
        Me.chkIVAIncluido.Valor = False
        '
        'chkComprobanteTimbrado
        '
        Me.chkComprobanteTimbrado.BackColor = System.Drawing.Color.Transparent
        Me.chkComprobanteTimbrado.Color = System.Drawing.Color.Empty
        Me.chkComprobanteTimbrado.Location = New System.Drawing.Point(116, 253)
        Me.chkComprobanteTimbrado.Name = "chkComprobanteTimbrado"
        Me.chkComprobanteTimbrado.Size = New System.Drawing.Size(137, 17)
        Me.chkComprobanteTimbrado.SoloLectura = False
        Me.chkComprobanteTimbrado.TabIndex = 17
        Me.chkComprobanteTimbrado.Texto = "Comprobante timbrado"
        Me.chkComprobanteTimbrado.Valor = False
        '
        'chkAutonumerico
        '
        Me.chkAutonumerico.BackColor = System.Drawing.Color.Transparent
        Me.chkAutonumerico.Color = System.Drawing.Color.Empty
        Me.chkAutonumerico.Location = New System.Drawing.Point(18, 253)
        Me.chkAutonumerico.Name = "chkAutonumerico"
        Me.chkAutonumerico.Size = New System.Drawing.Size(84, 17)
        Me.chkAutonumerico.SoloLectura = False
        Me.chkAutonumerico.TabIndex = 16
        Me.chkAutonumerico.Texto = "Autonumerico"
        Me.chkAutonumerico.Valor = False
        '
        'txtHechaukaTimbradoReemplazar
        '
        Me.txtHechaukaTimbradoReemplazar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtHechaukaTimbradoReemplazar.Color = System.Drawing.Color.Empty
        Me.txtHechaukaTimbradoReemplazar.Enabled = False
        Me.txtHechaukaTimbradoReemplazar.Indicaciones = Nothing
        Me.txtHechaukaTimbradoReemplazar.Location = New System.Drawing.Point(212, 425)
        Me.txtHechaukaTimbradoReemplazar.Multilinea = False
        Me.txtHechaukaTimbradoReemplazar.Name = "txtHechaukaTimbradoReemplazar"
        Me.txtHechaukaTimbradoReemplazar.Size = New System.Drawing.Size(106, 21)
        Me.txtHechaukaTimbradoReemplazar.SoloLectura = False
        Me.txtHechaukaTimbradoReemplazar.TabIndex = 29
        Me.txtHechaukaTimbradoReemplazar.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtHechaukaTimbradoReemplazar.Texto = ""
        Me.txtHechaukaTimbradoReemplazar.ToolTip1 = Nothing
        '
        'txtHechaukaTipoDocumento
        '
        Me.txtHechaukaTipoDocumento.Color = System.Drawing.Color.Empty
        Me.txtHechaukaTipoDocumento.Decimales = True
        Me.txtHechaukaTipoDocumento.Indicaciones = Nothing
        Me.txtHechaukaTipoDocumento.Location = New System.Drawing.Point(93, 400)
        Me.txtHechaukaTipoDocumento.Name = "txtHechaukaTipoDocumento"
        Me.txtHechaukaTipoDocumento.Size = New System.Drawing.Size(49, 21)
        Me.txtHechaukaTipoDocumento.SoloLectura = False
        Me.txtHechaukaTipoDocumento.TabIndex = 27
        Me.txtHechaukaTipoDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtHechaukaTipoDocumento.Texto = "0"
        '
        'lblHechaukaTipoDocumento
        '
        Me.lblHechaukaTipoDocumento.AutoSize = True
        Me.lblHechaukaTipoDocumento.Location = New System.Drawing.Point(14, 404)
        Me.lblHechaukaTipoDocumento.Name = "lblHechaukaTipoDocumento"
        Me.lblHechaukaTipoDocumento.Size = New System.Drawing.Size(78, 13)
        Me.lblHechaukaTipoDocumento.TabIndex = 26
        Me.lblHechaukaTipoDocumento.Text = "T. Documento:"
        '
        'lblHechauka
        '
        Me.lblHechauka.AutoSize = True
        Me.lblHechauka.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHechauka.Location = New System.Drawing.Point(15, 380)
        Me.lblHechauka.Name = "lblHechauka"
        Me.lblHechauka.Size = New System.Drawing.Size(69, 13)
        Me.lblHechauka.TabIndex = 25
        Me.lblHechauka.Text = "Hechauka:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(16, 227)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(156, 13)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Opciones de Comprobante"
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.lblTipoFiltro)
        Me.FlowLayoutPanel4.Controls.Add(Me.cbxTipoFiltro)
        Me.FlowLayoutPanel4.Controls.Add(Me.lblOperacionFiltro)
        Me.FlowLayoutPanel4.Controls.Add(Me.cbxOperacionFiltro)
        Me.FlowLayoutPanel4.Controls.Add(Me.lklListarTodos)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(3, 33)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(661, 28)
        Me.FlowLayoutPanel4.TabIndex = 1
        '
        'lblTipoFiltro
        '
        Me.lblTipoFiltro.Location = New System.Drawing.Point(3, 0)
        Me.lblTipoFiltro.Name = "lblTipoFiltro"
        Me.lblTipoFiltro.Size = New System.Drawing.Size(31, 24)
        Me.lblTipoFiltro.TabIndex = 0
        Me.lblTipoFiltro.Text = "Tipo:"
        Me.lblTipoFiltro.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxTipoFiltro
        '
        Me.cbxTipoFiltro.CampoWhere = Nothing
        Me.cbxTipoFiltro.CargarUnaSolaVez = False
        Me.cbxTipoFiltro.DataDisplayMember = Nothing
        Me.cbxTipoFiltro.DataFilter = Nothing
        Me.cbxTipoFiltro.DataOrderBy = Nothing
        Me.cbxTipoFiltro.DataSource = Nothing
        Me.cbxTipoFiltro.DataValueMember = Nothing
        Me.cbxTipoFiltro.FormABM = Nothing
        Me.cbxTipoFiltro.Indicaciones = Nothing
        Me.cbxTipoFiltro.Location = New System.Drawing.Point(40, 3)
        Me.cbxTipoFiltro.Name = "cbxTipoFiltro"
        Me.cbxTipoFiltro.SeleccionObligatoria = False
        Me.cbxTipoFiltro.Size = New System.Drawing.Size(105, 21)
        Me.cbxTipoFiltro.SoloLectura = False
        Me.cbxTipoFiltro.TabIndex = 1
        Me.cbxTipoFiltro.Texto = ""
        '
        'lblOperacionFiltro
        '
        Me.lblOperacionFiltro.Location = New System.Drawing.Point(151, 0)
        Me.lblOperacionFiltro.Name = "lblOperacionFiltro"
        Me.lblOperacionFiltro.Size = New System.Drawing.Size(66, 24)
        Me.lblOperacionFiltro.TabIndex = 2
        Me.lblOperacionFiltro.Text = "Operacion:"
        Me.lblOperacionFiltro.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxOperacionFiltro
        '
        Me.cbxOperacionFiltro.CampoWhere = Nothing
        Me.cbxOperacionFiltro.CargarUnaSolaVez = False
        Me.cbxOperacionFiltro.DataDisplayMember = Nothing
        Me.cbxOperacionFiltro.DataFilter = Nothing
        Me.cbxOperacionFiltro.DataOrderBy = Nothing
        Me.cbxOperacionFiltro.DataSource = Nothing
        Me.cbxOperacionFiltro.DataValueMember = Nothing
        Me.cbxOperacionFiltro.FormABM = Nothing
        Me.cbxOperacionFiltro.Indicaciones = Nothing
        Me.cbxOperacionFiltro.Location = New System.Drawing.Point(223, 3)
        Me.cbxOperacionFiltro.Name = "cbxOperacionFiltro"
        Me.cbxOperacionFiltro.SeleccionObligatoria = False
        Me.cbxOperacionFiltro.Size = New System.Drawing.Size(238, 21)
        Me.cbxOperacionFiltro.SoloLectura = False
        Me.cbxOperacionFiltro.TabIndex = 3
        Me.cbxOperacionFiltro.Texto = ""
        '
        'lklListarTodos
        '
        Me.lklListarTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklListarTodos.Location = New System.Drawing.Point(467, 0)
        Me.lklListarTodos.Name = "lklListarTodos"
        Me.lklListarTodos.Size = New System.Drawing.Size(100, 24)
        Me.lklListarTodos.TabIndex = 4
        Me.lklListarTodos.TabStop = True
        Me.lklListarTodos.Text = "Listar todos"
        Me.lklListarTodos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkExento
        '
        Me.chkExento.BackColor = System.Drawing.Color.Transparent
        Me.chkExento.Color = System.Drawing.Color.Empty
        Me.chkExento.Location = New System.Drawing.Point(260, 253)
        Me.chkExento.Name = "chkExento"
        Me.chkExento.Size = New System.Drawing.Size(55, 17)
        Me.chkExento.SoloLectura = False
        Me.chkExento.TabIndex = 30
        Me.chkExento.Texto = "Exento"
        Me.chkExento.Valor = False
        '
        'frmTipoComprobante
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1003, 594)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmTipoComprobante"
        Me.Tag = "TIPO DE COMPROBANTES"
        Me.Text = "frmTipoComprobante"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.gbxSigno.ResumeLayout(False)
        Me.gbxSigno.PerformLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtResumen As ERP.ocxTXTString
    Friend WithEvents lblResumen As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As ERP.ocxTXTString
    Friend WithEvents lblCodigo As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents cbxOperacion As ERP.ocxCBX
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents cbxTipo As ERP.ocxCBX
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents rdbDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbActivo As System.Windows.Forms.RadioButton
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents gbxSigno As System.Windows.Forms.GroupBox
    Friend WithEvents rdbNegativo As System.Windows.Forms.RadioButton
    Friend WithEvents rdbPositivo As System.Windows.Forms.RadioButton
    Friend WithEvents dgvLista As System.Windows.Forms.DataGridView
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtBuscar As ERP.ocxTXTString
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As ERP.ocxTXTNumeric
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtHechaukaTimbradoReemplazar As ERP.ocxTXTString
    Friend WithEvents txtHechaukaTipoDocumento As ERP.ocxTXTNumeric
    Friend WithEvents lblHechaukaTipoDocumento As System.Windows.Forms.Label
    Friend WithEvents lblHechauka As System.Windows.Forms.Label
    Friend WithEvents chkCalcularIVA As ERP.ocxCHK
    Friend WithEvents chkIncluirEnLibro As ERP.ocxCHK
    Friend WithEvents chkIVAIncluido As ERP.ocxCHK
    Friend WithEvents chkComprobanteTimbrado As ERP.ocxCHK
    Friend WithEvents chkAutonumerico As ERP.ocxCHK
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lblTipoFiltro As System.Windows.Forms.Label
    Friend WithEvents cbxTipoFiltro As ERP.ocxCBX
    Friend WithEvents lblOperacionFiltro As System.Windows.Forms.Label
    Friend WithEvents cbxOperacionFiltro As ERP.ocxCBX
    Friend WithEvents lklListarTodos As System.Windows.Forms.LinkLabel
    Friend WithEvents chkHechaukaTimbradoReemplazar As ERP.ocxCHK
    Friend WithEvents chkEsInterno As ERP.ocxCHK
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents chkComprobanteResta As ERP.ocxCHK
    Friend WithEvents chkHabilitarDepositoBancario As ERP.ocxCHK
    Friend WithEvents chkExento As ERP.ocxCHK
End Class

﻿Public Class frmMeta
    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Focus
        lvLista.Focus()
        'Año
        nudAño.Minimum = Date.Now.Year - 10
        nudAño.Maximum = Date.Now.Year + 10
        nudAño.Value = Date.Now.Year
        'Mes
        cbxMes.SelectedIndex = Date.Now.Month - 1
    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, rdbActivo)
        CSistema.CargaControl(vControles, rdbDesactivado)
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxVendedor)
        CSistema.CargaControl(vControles, cbxTipoProducto)
        CSistema.CargaControl(vControles, cbxUnidadMedida)
        CSistema.CargaControl(vControles, cbxProducto)
        CSistema.CargaControl(vControles, cbxListaPrecio)
        CSistema.CargaControl(vControles, txtImporte)
        CSistema.CargaControl(vControles, txtMetaKg)
        CSistema.CargaControl(vControles, nudAño)
        CSistema.CargaControl(vControles, cbxMes)

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From VSucursal Order By 2")

        'Producto
        CSistema.SqlToComboBox(cbxProducto, "Select ID, ReferenciaProducto From VProducto Order By 2")

        'Vendedor
        CSistema.SqlToComboBox(cbxVendedor, "Select ID, Nombres From VVendedor Order By 2")

        'Tipo de Producto
        CSistema.SqlToComboBox(cbxTipoProducto, "Select ID, Descripcion From VTipoProducto Order By 2")

        'Unidad de medida
        CSistema.SqlToComboBox(cbxUnidadMedida, "Select ID, Referencia From UnidadMedida Order By 2")

        'Lista de precio
        CSistema.SqlToComboBox(cbxListaPrecio, "Select ID, Lista From vListaPrecio Order By 2")



        'Cargamos los registos en el lv
        Listar()

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If lvLista.SelectedRows.Count = 0 Then
            ctrError.SetError(lvLista, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = lvLista.SelectedRows(0).Cells("ID").Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VMeta Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            cbxSucursal.Texto = oRow("Sucursal").ToString
            cbxVendedor.Texto = oRow("Vendedor").ToString
            cbxTipoProducto.Texto = oRow("TipoProducto").ToString
            cbxUnidadMedida.Texto = oRow("UnidadMedida").ToString
            cbxProducto.Texto = oRow("ReferenciaProducto").ToString & "-" & oRow("Producto").ToString
            cbxListaPrecio.Texto = oRow("ListaPrecio").ToString
            txtImporte.txt.Text = oRow("Importe").ToString
            txtMetaKg.txt.Text = oRow("MetaKg").ToString
            cbxMes.Text = oRow("MesDescripcion").ToString
            nudAño.Value = oRow("Año").ToString

            If CBool(oRow("Estado")) = True Then
                rdbActivo.Checked = True
            Else
                rdbDesactivado.Checked = True
            End If

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        CSistema.SqlToDataGrid(lvLista, "Select ID,Sucursal,Vendedor,TipoProducto,UnidadMedida,Producto,ListaPrecio,Importe as Meta, 'Estado'=(Case When Estado = 'True' Then 'OK' Else '-' End) From VMeta Order By 1")

        If lvLista.Columns.Count = 0 Then
            Exit Sub
        End If

        'Formato
        lvLista.Columns("ID").Visible = False
        lvLista.Columns("Vendedor").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        'lvLista.Columns("UnidadMedida").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        lvLista.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        'lvLista.Columns("ListaPrecio").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        lvLista.Columns("Meta").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
    End Sub

    Sub InicializarControles()

        'TextBox

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Meta"), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Meta"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        cbxSucursal.Focus()
        txtImporte.txt.Text = 0
        txtMetaKg.txt.Text = 0

    End Sub

    Sub FiltrarProducto()
        cbxProducto.DataSource = Nothing
        Dim Where As String = ""
        If cbxTipoProducto.Texto <> "" Then
            Where = " where IDTipoProducto = " & cbxTipoProducto.GetValue
        End If

        'Producto
        CSistema.SqlToComboBox(cbxProducto, "Select ID, ReferenciaProducto From VProducto " & Where & " Order By 2")
    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Sucursal
        If cbxSucursal.txt.Text = "" Then
            Dim mensaje As String = "Debe ingresar Sucursal valida!"
            ctrError.SetError(cbxSucursal, mensaje)
            ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If
        'Vendedor
        If cbxVendedor.txt.Text = "" Then
            Dim mensaje As String = "Debe ingresar Vendedor valido!"
            ctrError.SetError(cbxVendedor, mensaje)
            ctrError.SetIconAlignment(cbxVendedor, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If
        'Tipo de Producto
        If cbxTipoProducto.txt.Text = "" Then
            Dim mensaje As String = "Debe ingresar Tipo valido!"
            ctrError.SetError(cbxTipoProducto, mensaje)
            ctrError.SetIconAlignment(cbxTipoProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If
        'Unidad de medida
        If cbxUnidadMedida.txt.Text = "" Then
            Dim mensaje As String = "Debe ingresar Unidad valida!"
            ctrError.SetError(cbxUnidadMedida, mensaje)
            ctrError.SetIconAlignment(cbxUnidadMedida, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If
        'Producto
        If cbxProducto.txt.Text = "" Then
            Dim mensaje As String = "Debe ingresar Producto valido!"
            ctrError.SetError(cbxProducto, mensaje)
            ctrError.SetIconAlignment(cbxProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If
        'Lista de Precio
        If cbxListaPrecio.txt.Text = "" Then
            Dim mensaje As String = "Debe ingresar Lista de precio valida!"
            ctrError.SetError(cbxListaPrecio, mensaje)
            ctrError.SetIconAlignment(cbxListaPrecio, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If
        'meta en Gs
        If txtImporte.ObtenerValor = 0 Then
            Dim mensaje As String = "Debe ingresar Importe valido!"
            ctrError.SetError(txtImporte, mensaje)
            ctrError.SetIconAlignment(txtImporte, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If
        'meta en kgs
        If txtMetaKg.ObtenerValor = 0 Then
            Dim mensaje As String = "Debe ingresar Meta en Kg valida!"
            ctrError.SetError(txtMetaKg, mensaje)
            ctrError.SetIconAlignment(txtMetaKg, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If lvLista.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(lvLista, mensaje)
                ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDVendedor", cbxVendedor.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoProducto", cbxTipoProducto.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUnidadMedida", cbxUnidadMedida.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProducto", cbxProducto.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDListaPrecio", cbxListaPrecio.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(txtImporte.txt.Text, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@MetaKg", CSistema.FormatoMonedaBaseDatos(txtMetaKg.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Año", nudAño.Value, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Mes", cbxMes.SelectedIndex + 1, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpMeta", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            Listar(txtID.txt.Text)
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()

    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click

        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        cbxSucursal.Focus()
        cbxSucursal.txt.SelectAll()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub



    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub lvLista_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvLista.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub frmMeta_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub cbxTipoProducto_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxTipoProducto.PropertyChanged
        FiltrarProducto()
    End Sub

    Private Sub frmMeta_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

End Class
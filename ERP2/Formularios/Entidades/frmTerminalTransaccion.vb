﻿Public Class frmTerminalTransaccion

    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vNuevoPE As Boolean
    Dim vControles() As Control
    Dim vControlesPE() As Control
    Dim dtPuntoExpedicion As DataTable
    Dim dtDeposito As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Variables
        vNuevo = False
        vNuevoPE = False

        'Funciones
        CargarInformacion()
        CargarInformacionPE()

        'Controles
        CSistema.InicializaControles(TabControl1.TabPages(0))
        CSistema.InicializaControles(TabControl1.TabPages(1))

        'Cargamos los registos en el lv
        Listar()
        ListarPE()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnPENuevo, New Button, btnPECancelar, btnPEGuardar, btnPEEliminar, vControlesPE)

        'Focus
        lvLista.Focus()



    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxDeposito)
        CSistema.CargaControl(vControles, txtDescripcion)
        CSistema.CargaControl(vControles, txtImpresora)

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Descripcion From Sucursal Order By 2")

        'Depositos
        dtDeposito = CSistema.ExecuteToDataTable("Select ID, Descripcion, IDSucursal From Deposito Order By 2").Copy

    End Sub

    Sub CargarInformacionPE()

        'Punto de Expedicion
        ReDim vControlesPE(-1)
        CSistema.CargaControl(vControlesPE, cbxPETerminal)
        CSistema.CargaControl(vControlesPE, cbxPEOperacion)
        CSistema.CargaControl(vControlesPE, cbxPEPuntoExpedicion)

        'Terminales
        CSistema.SqlToComboBox(cbxPETerminal.cbx, "Select ID, Descripcion From Terminal Order By 2")

        'Operaciones
        CSistema.SqlToComboBox(cbxPEOperacion.cbx, "Select ID, Descripcion From Operacion Order By 2")

        'Puntos de Expedicion
        CSistema.SqlToComboBox(cbxPEPuntoExpedicion.cbx, "Select ID, Descripcion From PuntoExpedicion Order By 2")

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If lvLista.SelectedItems.Count = 0 Then
            ctrError.SetError(lvLista, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = lvLista.SelectedItems(0).SubItems(1).Text

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VTerminal Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            cbxSucursal.cbx.Text = oRow("Sucursal").ToString
            cbxDeposito.cbx.Text = oRow("Deposito").ToString
            txtDescripcion.txt.Text = oRow("Descripcion").ToString
            txtImpresora.txt.Text = oRow("Impresora").ToString
            txtCodigo.Text = oRow("CodigoActivacion").ToString

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub ObtenerInformacionEP()

        'Validar
        'Si es que se selecciono el registro.
        If lvListaPE.SelectedItems.Count = 0 Then
            ctrError.SetError(lvListaPE, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(lvListaPE, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnPENuevo, New Button, btnPECancelar, btnPEGuardar, btnPEEliminar, vControlesPE)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim IDTerminal As Integer
        Dim IDOperacion As Integer
        Dim IDPuntoExpedicion As Integer

        IDTerminal = lvListaPE.SelectedItems(0).SubItems(3).Text
        IDOperacion = lvListaPE.SelectedItems(0).SubItems(4).Text
        IDPuntoExpedicion = lvListaPE.SelectedItems(0).SubItems(5).Text

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VTerminalPuntoExpedicion Where IDTerminal=" & IDTerminal & " And IDOperacion=" & IDOperacion & " And IDPuntoExpedicion=" & IDPuntoExpedicion & " ")

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            cbxPETerminal.cbx.Text = oRow("Terminal").ToString
            cbxPEOperacion.cbx.Text = oRow("Operacion").ToString
            cbxPEPuntoExpedicion.cbx.Text = oRow("PE").ToString

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnPENuevo, New Button, btnPECancelar, btnPEGuardar, btnPEEliminar, vControlesPE)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        CSistema.SqlToLv(lvLista, "Select Descripcion, ID, Sucursal, Deposito From VTerminal Order By Descripcion ")

        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If lvLista.Columns.Count > 0 Then

            'Esto hacemos para que: 
            '1- Que el ID sea visible en la primera columna
            '2- Y para que cuando el usuario escriba en el lv, el control filtre por su descripcion.
            lvLista.Columns(0).DisplayIndex = 1

            'Ahora seleccionamos automaticamente el registro especificado
            If lvLista.Items.Count > 0 Then
                If ID = 0 Then

                    lvLista.Items(0).Selected = True
                Else
                    For i As Integer = 0 To lvLista.Items.Count - 1
                        If lvLista.Items(i).SubItems(1).Text = ID Then
                            lvLista.Items(i).Selected = True
                            Exit For
                        End If
                    Next

                End If
            End If


        End If


        lvLista.Refresh()

    End Sub

    Sub ListarPE(Optional ByVal ID As Integer = 0)

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        CSistema.SqlToLv(lvListaPE, "Select Terminal, Operacion, PE, IDTerminal, IDOperacion, IDPuntoExpedicion From VTerminalPuntoExpedicion Order By Terminal, Operacion, PE ")

        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If lvListaPE.Columns.Count > 0 Then

           
            'Ahora seleccionamos automaticamente el registro especificado
            If lvListaPE.Items.Count > 0 Then
                If ID = 0 Then

                    lvListaPE.Items(0).Selected = True
                Else
                    For i As Integer = 0 To lvListaPE.Items.Count - 1
                        If lvListaPE.Items(i).SubItems(1).Text = ID Then
                            lvListaPE.Items(i).Selected = True
                            Exit For
                        End If
                    Next

                End If
            End If


        End If


        lvListaPE.Refresh()

    End Sub

    Sub InicializarControles()

        'TextBox
        CSistema.InicializaControles(vControles)

        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Terminal"), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Terminal"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        cbxSucursal.cbx.Focus()

    End Sub

    Sub InicializarControlesPE()

        'TextBox
        CSistema.InicializaControles(vControlesPE)

        'Funciones

        'Error
        ctrError.Clear()

        'Foco
        cbxPETerminal.cbx.Focus()

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Seleccion correcta de la sucursal
        If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            ctrError.SetError(cbxSucursal, mensaje)
            ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Deposito
        If IsNumeric(cbxDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(cbxDeposito, mensaje)
            ctrError.SetIconAlignment(cbxDeposito, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Escritura de la Descripcion
        If txtDescripcion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar una descripcion valida!"
            ctrError.SetError(txtDescripcion, mensaje)
            ctrError.SetIconAlignment(txtDescripcion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", cbxDeposito.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Impresora", txtImpresora.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpTerminal", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            Listar(txtID.txt.Text)

            'Actualizar Puntos de Expedicion
            'Puntos de Expedicion
            CSistema.SqlToComboBox(cbxPEPuntoExpedicion.cbx, "Select ID, Descripcion From PuntoExpedicion Order By 2")

        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub ProcesarPE(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Seleccion correcta de la sucursal
        If IsNumeric(cbxPETerminal.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente la terminal!"
            ctrError.SetError(cbxPETerminal, mensaje)
            ctrError.SetIconAlignment(cbxPETerminal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Operacion
        If IsNumeric(cbxPEOperacion.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el tipo de operacion!"
            ctrError.SetError(cbxPEOperacion, mensaje)
            ctrError.SetIconAlignment(cbxPEOperacion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Punto de Expedicion
        If IsNumeric(cbxPEPuntoExpedicion.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el punto de expedicion!"
            ctrError.SetError(cbxPEPuntoExpedicion, mensaje)
            ctrError.SetIconAlignment(cbxPEPuntoExpedicion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar

        Dim param(-1) As SqlClient.SqlParameter
        CSistema.SetSQLParameter(param, "@IDTerminalReferencia", cbxPETerminal.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDOperacion", cbxPEOperacion.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDPuntoExpedicion", cbxPEPuntoExpedicion.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpTerminalPuntoExpedicion", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnPENuevo, New Button, btnPECancelar, btnPEGuardar, btnPEEliminar, vControlesPE)
            ctrError.Clear()
            ListarPE()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnPEGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnPEGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        txtDescripcion.Focus()
        txtDescripcion.txt.SelectAll()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub lvLista_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvLista.Click
        ObtenerInformacion()
    End Sub

    Private Sub frmPuntoExpedicion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged

        cbxDeposito.cbx.DataSource = Nothing
        cbxDeposito.cbx.Text = ""

        If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        Try
            Dim dttemp As DataTable = CSistema.FiltrarDataTable(dtDeposito, " IDSucursal=" & cbxSucursal.cbx.SelectedValue).Copy
            CSistema.SqlToComboBox(cbxDeposito.cbx, dttemp)

        Catch ex As Exception

        End Try


    End Sub

    Private Sub lvLista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvLista.SelectedIndexChanged
        ObtenerInformacion()
    End Sub

    Private Sub btnPENuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPENuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnPENuevo, New Button, btnPECancelar, btnPEGuardar, btnPEEliminar, vControlesPE)
        vNuevoPE = True
        InicializarControlesPE()
    End Sub

    Private Sub btnPEGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPEGuardar.Click

        If vNuevoPE = True Then
            ProcesarPE(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            ProcesarPE(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnPECancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPECancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnPENuevo, New Button, btnPECancelar, btnPEGuardar, btnPEEliminar, vControlesPE)
        vNuevoPE = False
        InicializarControlesPE()
        ObtenerInformacionEP()
    End Sub

    Private Sub btnPEEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPEEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnPENuevo, New Button, btnPECancelar, btnPEGuardar, btnPEEliminar, vControlesPE)
        ProcesarPE(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub lvListaPE_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvListaPE.SelectedIndexChanged
        ObtenerInformacionEP()
    End Sub

End Class
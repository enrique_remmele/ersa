﻿Public Class frmCuentaBancariaSaldoDiario
    'CLASES
    Dim CSistema As New CSistema


    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        'Funciones
        CargarInformacion()

        'Focus
        Me.ActiveControl = cbxCuentaBancaria
        cbxCuentaBancaria.Focus()
        txtFecha.Hoy()

    End Sub

    Sub CargarInformacion()

        'Cargas las monedas
        CSistema.SqlToComboBox(cbxCuentaBancaria, "Select ID, Descripcion From VCuentaBancaria Order By Descripcion asc")

        'Cargamos la ultima cotizacion
        txtSaldo.txt.Text = 0

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()
        If CSistema.ExecuteScalar("select count(*) from CuentaBancariaSaldoDiario where fecha = '" & CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False) & "' and IDcuentaBancaria = " & cbxCuentaBancaria.GetValue) > 0 Then
            If MessageBox.Show("La Cuenta Bancaria tiene saldo en fecha seleccionada! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Validar
        'Moneda
        If cbxCuentaBancaria.GetValue = 0 Then
            Dim mensaje As String = "Seleccione correctamente la Cuenta Bancaria!"
            ctrError.SetError(cbxCuentaBancaria, mensaje)
            ctrError.SetIconAlignment(cbxCuentaBancaria, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Saldo
        'If txtSaldo.ObtenerValor = 0 Then
        '    Dim mensaje As String = "El Saldo no es valido!"
        '    ctrError.SetError(txtSaldo, mensaje)
        '    ctrError.SetIconAlignment(txtSaldo, ErrorIconAlignment.MiddleRight)
        '    tsslEstado.Text = mensaje
        '    Exit Sub
        'End If

        'Procesar
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDCuentaBancaria", cbxCuentaBancaria.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Saldo", CSistema.FormatoMonedaBaseDatos(txtSaldo.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, True), ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpCuentaBancariaSaldoDiario", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            ctrError.Clear()

            MessageBox.Show(MensajeRetorno, "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'Me.Close()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub MostrarOpciones()
        Dim frm As New frmCuentaBancariaSaldoHistorial
        Try
            frm.IDCuentaBancaria = cbxCuentaBancaria.GetValue
            frm.Moneda = cbxCuentaBancaria.txt.Text
        Catch ex As Exception

        End Try

        FGMostrarFormulario(Me, frm, "Saldo de Cuenta Bancaria", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Procesar(ERP.CSistema.NUMOperacionesABM.INS)
    End Sub

    Private Sub frmBanco_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Enter Then
            CSistema.SelectNextControl(Me, e.KeyCode)
        End If
    End Sub

    Private Sub frmBanco_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub cbxMoneda_Leave(sender As System.Object, e As System.EventArgs) Handles cbxCuentaBancaria.Leave
        'txtSaldo.SetValue(CSistema.ExecuteScalar("Select dbo.FCotizacionAlDia(" & cbxCuentaBancaria.GetValue & ", 1)"))
    End Sub

    Private Sub lklHistorial_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklHistorial.LinkClicked
        MostrarOpciones()
    End Sub
End Class
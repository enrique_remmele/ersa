﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProducto2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.lblCuentaVenta = New System.Windows.Forms.Label()
        Me.ListView2 = New System.Windows.Forms.ListView()
        Me.lblCuentaCompra = New System.Windows.Forms.Label()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.lblControlarExistencia = New System.Windows.Forms.Label()
        Me.lblActivo = New System.Windows.Forms.Label()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.lblCostoPromedio = New System.Windows.Forms.Label()
        Me.txtCostoPromedio = New System.Windows.Forms.TextBox()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.lblCostoCG = New System.Windows.Forms.Label()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.txtCostoCG = New System.Windows.Forms.TextBox()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.chkControlarExistencia = New System.Windows.Forms.CheckBox()
        Me.chkActivo = New System.Windows.Forms.CheckBox()
        Me.lblCotizacion = New System.Windows.Forms.Label()
        Me.txtCotizacion = New System.Windows.Forms.TextBox()
        Me.lblUltimoCostoSinIVA = New System.Windows.Forms.Label()
        Me.txtUltimoCostoSinIVA = New System.Windows.Forms.TextBox()
        Me.lblExistenciaMinima = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.lblUltimoCosto = New System.Windows.Forms.Label()
        Me.tabLotes = New System.Windows.Forms.TabPage()
        Me.OcxTXTDate1 = New ERP.ocxTXTDate()
        Me.OcxTXTString2 = New ERP.ocxTXTString()
        Me.lblSucursalDeposito = New System.Windows.Forms.Label()
        Me.txtUltimoCosto = New System.Windows.Forms.TextBox()
        Me.txtCantidadUltimaSalida = New System.Windows.Forms.TextBox()
        Me.lblUltimaSalida = New System.Windows.Forms.Label()
        Me.txtCantidadUltimaEntrada = New System.Windows.Forms.TextBox()
        Me.txtFechaUltimaSalida = New System.Windows.Forms.TextBox()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lblUltimaEntrada = New System.Windows.Forms.Label()
        Me.txtFechaUltimaEntrada = New System.Windows.Forms.TextBox()
        Me.lblVolumenPorCaja = New System.Windows.Forms.Label()
        Me.lblPesoPorCaja = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lblUnidadPorCaja = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblUnidadMedida = New System.Windows.Forms.Label()
        Me.lblImpuesto = New System.Windows.Forms.Label()
        Me.lvExistencia = New System.Windows.Forms.ListView()
        Me.lblDivision = New System.Windows.Forms.Label()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.tabIdentificacion = New System.Windows.Forms.TabPage()
        Me.OcxClasificador1 = New ERP.ocxClasificador()
        Me.ocxCuentaCompra = New ERP.ocxTXTCuentaContable()
        Me.txtVolumenPorCaja = New ERP.ocxTXTString()
        Me.cbxImpuesto = New ERP.ocxCBX()
        Me.txtPesoPorCaja = New ERP.ocxTXTNumeric()
        Me.txtUnidadPorCaja = New ERP.ocxTXTNumeric()
        Me.cbxUnidadMedida = New ERP.ocxCBX()
        Me.cbxDivision = New ERP.ocxCBX()
        Me.cbxProveedor = New ERP.ocxCBX()
        Me.ocxCuentaVenta = New ERP.ocxTXTCuentaContable()
        Me.btnActualizarExistencia = New System.Windows.Forms.Button()
        Me.lblMonedaPrecio = New System.Windows.Forms.Label()
        Me.tabPrecios = New System.Windows.Forms.TabPage()
        Me.btnCancelarPrecio = New System.Windows.Forms.Button()
        Me.btnEliminarPrecio = New System.Windows.Forms.Button()
        Me.btnGuardarPrecio = New System.Windows.Forms.Button()
        Me.btnEditarPrecio = New System.Windows.Forms.Button()
        Me.btnNuevoPrecio = New System.Windows.Forms.Button()
        Me.cbxMonedaPrecio = New ERP.ocxCBX()
        Me.lvListaPrecio = New System.Windows.Forms.ListView()
        Me.lblPrecioUnirario = New System.Windows.Forms.Label()
        Me.lblListaPrecio = New System.Windows.Forms.Label()
        Me.txtPrecioUnitario = New ERP.ocxTXTNumeric()
        Me.cbxListaPrecio = New ERP.ocxCBX()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabExistencia = New System.Windows.Forms.TabPage()
        Me.txtExistenciaCritica = New ERP.ocxTXTNumeric()
        Me.lblExistenciaCritica = New System.Windows.Forms.Label()
        Me.lblExistenciaGeneral = New System.Windows.Forms.Label()
        Me.txtExistenciaGeneral = New ERP.ocxTXTNumeric()
        Me.txtExistenciaMinima = New ERP.ocxTXTNumeric()
        Me.cbxSucursalDeposito = New ERP.ocxCBX()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblCodigoBarra = New System.Windows.Forms.Label()
        Me.lblReferencia = New System.Windows.Forms.Label()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.txtReferencia = New ERP.ocxTXTString()
        Me.txtCodigoBarra = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.tabLotes.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.tabIdentificacion.SuspendLayout()
        Me.tabPrecios.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tabExistencia.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(11, 10)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(31, 13)
        Me.Label27.TabIndex = 6
        Me.Label27.Text = "Lote:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Codigo:"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(11, 37)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(68, 13)
        Me.Label28.TabIndex = 9
        Me.Label28.Text = "Vencimiento:"
        '
        'lblCuentaVenta
        '
        Me.lblCuentaVenta.AutoSize = True
        Me.lblCuentaVenta.Location = New System.Drawing.Point(342, 225)
        Me.lblCuentaVenta.Name = "lblCuentaVenta"
        Me.lblCuentaVenta.Size = New System.Drawing.Size(85, 13)
        Me.lblCuentaVenta.TabIndex = 46
        Me.lblCuentaVenta.Text = "Cuenta C Venta:"
        '
        'ListView2
        '
        Me.ListView2.Location = New System.Drawing.Point(104, 88)
        Me.ListView2.Name = "ListView2"
        Me.ListView2.Size = New System.Drawing.Size(400, 229)
        Me.ListView2.TabIndex = 44
        Me.ListView2.UseCompatibleStateImageBehavior = False
        '
        'lblCuentaCompra
        '
        Me.lblCuentaCompra.AutoSize = True
        Me.lblCuentaCompra.Location = New System.Drawing.Point(334, 199)
        Me.lblCuentaCompra.Name = "lblCuentaCompra"
        Me.lblCuentaCompra.Size = New System.Drawing.Size(93, 13)
        Me.lblCuentaCompra.TabIndex = 16
        Me.lblCuentaCompra.Text = "Cuenta C Compra:"
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(267, 59)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(75, 23)
        Me.Button9.TabIndex = 47
        Me.Button9.Text = "&Guardar"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(186, 59)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(75, 23)
        Me.Button10.TabIndex = 46
        Me.Button10.Text = "&Editar"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'lblControlarExistencia
        '
        Me.lblControlarExistencia.AutoSize = True
        Me.lblControlarExistencia.Location = New System.Drawing.Point(324, 172)
        Me.lblControlarExistencia.Name = "lblControlarExistencia"
        Me.lblControlarExistencia.Size = New System.Drawing.Size(103, 13)
        Me.lblControlarExistencia.TabIndex = 28
        Me.lblControlarExistencia.Text = "Controlar Existencia:"
        '
        'lblActivo
        '
        Me.lblActivo.AutoSize = True
        Me.lblActivo.Location = New System.Drawing.Point(486, 172)
        Me.lblActivo.Name = "lblActivo"
        Me.lblActivo.Size = New System.Drawing.Size(40, 13)
        Me.lblActivo.TabIndex = 26
        Me.lblActivo.Text = "Activo:"
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(105, 59)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(75, 23)
        Me.Button11.TabIndex = 45
        Me.Button11.Text = "&Nuevo"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'lblCostoPromedio
        '
        Me.lblCostoPromedio.AutoSize = True
        Me.lblCostoPromedio.Location = New System.Drawing.Point(561, 172)
        Me.lblCostoPromedio.Name = "lblCostoPromedio"
        Me.lblCostoPromedio.Size = New System.Drawing.Size(84, 13)
        Me.lblCostoPromedio.TabIndex = 44
        Me.lblCostoPromedio.Text = "Costo Promedio:"
        '
        'txtCostoPromedio
        '
        Me.txtCostoPromedio.BackColor = System.Drawing.Color.White
        Me.txtCostoPromedio.Location = New System.Drawing.Point(648, 168)
        Me.txtCostoPromedio.Name = "txtCostoPromedio"
        Me.txtCostoPromedio.ReadOnly = True
        Me.txtCostoPromedio.Size = New System.Drawing.Size(84, 20)
        Me.txtCostoPromedio.TabIndex = 45
        Me.ToolTip1.SetToolTip(Me.txtCostoPromedio, "Fecha de Alta del Cliente")
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(738, 400)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 32
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(518, 400)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 30
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'lblCostoCG
        '
        Me.lblCostoCG.AutoSize = True
        Me.lblCostoCG.Location = New System.Drawing.Point(590, 145)
        Me.lblCostoCG.Name = "lblCostoCG"
        Me.lblCostoCG.Size = New System.Drawing.Size(55, 13)
        Me.lblCostoCG.TabIndex = 42
        Me.lblCostoCG.Text = "Costo CG:"
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(599, 400)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 31
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(437, 400)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 29
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(356, 400)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 28
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'txtCostoCG
        '
        Me.txtCostoCG.BackColor = System.Drawing.Color.White
        Me.txtCostoCG.Location = New System.Drawing.Point(648, 141)
        Me.txtCostoCG.Name = "txtCostoCG"
        Me.txtCostoCG.ReadOnly = True
        Me.txtCostoCG.Size = New System.Drawing.Size(84, 20)
        Me.txtCostoCG.TabIndex = 43
        Me.ToolTip1.SetToolTip(Me.txtCostoCG, "Fecha de Alta del Cliente")
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(429, 59)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(75, 23)
        Me.Button8.TabIndex = 49
        Me.Button8.Text = "E&liminar"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'chkControlarExistencia
        '
        Me.chkControlarExistencia.AutoSize = True
        Me.chkControlarExistencia.Location = New System.Drawing.Point(433, 171)
        Me.chkControlarExistencia.Name = "chkControlarExistencia"
        Me.chkControlarExistencia.Size = New System.Drawing.Size(15, 14)
        Me.chkControlarExistencia.TabIndex = 29
        Me.chkControlarExistencia.UseVisualStyleBackColor = True
        '
        'chkActivo
        '
        Me.chkActivo.AutoSize = True
        Me.chkActivo.Location = New System.Drawing.Point(532, 171)
        Me.chkActivo.Name = "chkActivo"
        Me.chkActivo.Size = New System.Drawing.Size(15, 14)
        Me.chkActivo.TabIndex = 27
        Me.chkActivo.UseVisualStyleBackColor = True
        '
        'lblCotizacion
        '
        Me.lblCotizacion.AutoSize = True
        Me.lblCotizacion.Location = New System.Drawing.Point(586, 118)
        Me.lblCotizacion.Name = "lblCotizacion"
        Me.lblCotizacion.Size = New System.Drawing.Size(59, 13)
        Me.lblCotizacion.TabIndex = 40
        Me.lblCotizacion.Text = "Cotización:"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.BackColor = System.Drawing.Color.White
        Me.txtCotizacion.Location = New System.Drawing.Point(648, 114)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.ReadOnly = True
        Me.txtCotizacion.Size = New System.Drawing.Size(84, 20)
        Me.txtCotizacion.TabIndex = 41
        Me.ToolTip1.SetToolTip(Me.txtCotizacion, "Fecha de Alta del Cliente")
        '
        'lblUltimoCostoSinIVA
        '
        Me.lblUltimoCostoSinIVA.AutoSize = True
        Me.lblUltimoCostoSinIVA.Location = New System.Drawing.Point(600, 91)
        Me.lblUltimoCostoSinIVA.Name = "lblUltimoCostoSinIVA"
        Me.lblUltimoCostoSinIVA.Size = New System.Drawing.Size(45, 13)
        Me.lblUltimoCostoSinIVA.TabIndex = 38
        Me.lblUltimoCostoSinIVA.Text = "Sin IVA:"
        '
        'txtUltimoCostoSinIVA
        '
        Me.txtUltimoCostoSinIVA.BackColor = System.Drawing.Color.White
        Me.txtUltimoCostoSinIVA.Location = New System.Drawing.Point(648, 87)
        Me.txtUltimoCostoSinIVA.Name = "txtUltimoCostoSinIVA"
        Me.txtUltimoCostoSinIVA.ReadOnly = True
        Me.txtUltimoCostoSinIVA.Size = New System.Drawing.Size(84, 20)
        Me.txtUltimoCostoSinIVA.TabIndex = 39
        Me.ToolTip1.SetToolTip(Me.txtUltimoCostoSinIVA, "Fecha de Alta del Cliente")
        '
        'lblExistenciaMinima
        '
        Me.lblExistenciaMinima.AutoSize = True
        Me.lblExistenciaMinima.Location = New System.Drawing.Point(11, 38)
        Me.lblExistenciaMinima.Name = "lblExistenciaMinima"
        Me.lblExistenciaMinima.Size = New System.Drawing.Size(94, 13)
        Me.lblExistenciaMinima.TabIndex = 2
        Me.lblExistenciaMinima.Text = "Existencia Minima:"
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(348, 59)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 23)
        Me.Button7.TabIndex = 48
        Me.Button7.Text = "&Cancelar"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'lblUltimoCosto
        '
        Me.lblUltimoCosto.AutoSize = True
        Me.lblUltimoCosto.Location = New System.Drawing.Point(576, 64)
        Me.lblUltimoCosto.Name = "lblUltimoCosto"
        Me.lblUltimoCosto.Size = New System.Drawing.Size(69, 13)
        Me.lblUltimoCosto.TabIndex = 36
        Me.lblUltimoCosto.Text = "Ultimo Costo:"
        '
        'tabLotes
        '
        Me.tabLotes.Controls.Add(Me.Button7)
        Me.tabLotes.Controls.Add(Me.Button8)
        Me.tabLotes.Controls.Add(Me.Button9)
        Me.tabLotes.Controls.Add(Me.Button10)
        Me.tabLotes.Controls.Add(Me.Button11)
        Me.tabLotes.Controls.Add(Me.ListView2)
        Me.tabLotes.Controls.Add(Me.Label28)
        Me.tabLotes.Controls.Add(Me.Label27)
        Me.tabLotes.Controls.Add(Me.OcxTXTDate1)
        Me.tabLotes.Controls.Add(Me.OcxTXTString2)
        Me.tabLotes.Location = New System.Drawing.Point(4, 22)
        Me.tabLotes.Name = "tabLotes"
        Me.tabLotes.Size = New System.Drawing.Size(790, 338)
        Me.tabLotes.TabIndex = 2
        Me.tabLotes.Text = "Lotes"
        Me.tabLotes.UseVisualStyleBackColor = True
        '
        'OcxTXTDate1
        '
        Me.OcxTXTDate1.Color = System.Drawing.Color.Empty
        Me.OcxTXTDate1.Fecha = New Date(2013, 2, 6, 9, 32, 55, 203)
        Me.OcxTXTDate1.Location = New System.Drawing.Point(105, 33)
        Me.OcxTXTDate1.Name = "OcxTXTDate1"
        Me.OcxTXTDate1.PermitirNulo = False
        Me.OcxTXTDate1.Size = New System.Drawing.Size(74, 20)
        Me.OcxTXTDate1.SoloLectura = False
        Me.OcxTXTDate1.TabIndex = 8
        '
        'OcxTXTString2
        '
        Me.OcxTXTString2.BackColor = System.Drawing.Color.White
        Me.OcxTXTString2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTString2.Color = System.Drawing.Color.Empty
        Me.OcxTXTString2.Indicaciones = "(F1) Para busqueda avanzada"
        Me.OcxTXTString2.Location = New System.Drawing.Point(104, 6)
        Me.OcxTXTString2.Multilinea = False
        Me.OcxTXTString2.Name = "OcxTXTString2"
        Me.OcxTXTString2.Size = New System.Drawing.Size(126, 21)
        Me.OcxTXTString2.SoloLectura = False
        Me.OcxTXTString2.TabIndex = 7
        Me.OcxTXTString2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.OcxTXTString2.Texto = ""
        '
        'lblSucursalDeposito
        '
        Me.lblSucursalDeposito.AutoSize = True
        Me.lblSucursalDeposito.Location = New System.Drawing.Point(11, 10)
        Me.lblSucursalDeposito.Name = "lblSucursalDeposito"
        Me.lblSucursalDeposito.Size = New System.Drawing.Size(83, 13)
        Me.lblSucursalDeposito.TabIndex = 0
        Me.lblSucursalDeposito.Text = "Suc. - Deposito:"
        '
        'txtUltimoCosto
        '
        Me.txtUltimoCosto.BackColor = System.Drawing.Color.White
        Me.txtUltimoCosto.Location = New System.Drawing.Point(648, 60)
        Me.txtUltimoCosto.Name = "txtUltimoCosto"
        Me.txtUltimoCosto.ReadOnly = True
        Me.txtUltimoCosto.Size = New System.Drawing.Size(84, 20)
        Me.txtUltimoCosto.TabIndex = 37
        Me.ToolTip1.SetToolTip(Me.txtUltimoCosto, "Fecha de Alta del Cliente")
        '
        'txtCantidadUltimaSalida
        '
        Me.txtCantidadUltimaSalida.BackColor = System.Drawing.Color.White
        Me.txtCantidadUltimaSalida.Location = New System.Drawing.Point(738, 33)
        Me.txtCantidadUltimaSalida.Name = "txtCantidadUltimaSalida"
        Me.txtCantidadUltimaSalida.ReadOnly = True
        Me.txtCantidadUltimaSalida.Size = New System.Drawing.Size(46, 20)
        Me.txtCantidadUltimaSalida.TabIndex = 35
        Me.ToolTip1.SetToolTip(Me.txtCantidadUltimaSalida, "Fecha de Alta del Cliente")
        '
        'lblUltimaSalida
        '
        Me.lblUltimaSalida.AutoSize = True
        Me.lblUltimaSalida.Location = New System.Drawing.Point(574, 37)
        Me.lblUltimaSalida.Name = "lblUltimaSalida"
        Me.lblUltimaSalida.Size = New System.Drawing.Size(71, 13)
        Me.lblUltimaSalida.TabIndex = 33
        Me.lblUltimaSalida.Text = "Ultima Salida:"
        '
        'txtCantidadUltimaEntrada
        '
        Me.txtCantidadUltimaEntrada.BackColor = System.Drawing.Color.White
        Me.txtCantidadUltimaEntrada.Location = New System.Drawing.Point(738, 6)
        Me.txtCantidadUltimaEntrada.Name = "txtCantidadUltimaEntrada"
        Me.txtCantidadUltimaEntrada.ReadOnly = True
        Me.txtCantidadUltimaEntrada.Size = New System.Drawing.Size(46, 20)
        Me.txtCantidadUltimaEntrada.TabIndex = 32
        Me.ToolTip1.SetToolTip(Me.txtCantidadUltimaEntrada, "Fecha de Alta del Cliente")
        '
        'txtFechaUltimaSalida
        '
        Me.txtFechaUltimaSalida.BackColor = System.Drawing.Color.White
        Me.txtFechaUltimaSalida.Location = New System.Drawing.Point(648, 33)
        Me.txtFechaUltimaSalida.Name = "txtFechaUltimaSalida"
        Me.txtFechaUltimaSalida.ReadOnly = True
        Me.txtFechaUltimaSalida.Size = New System.Drawing.Size(84, 20)
        Me.txtFechaUltimaSalida.TabIndex = 34
        Me.ToolTip1.SetToolTip(Me.txtFechaUltimaSalida, "Fecha de Alta del Cliente")
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(296, 400)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(54, 23)
        Me.btnNuevo.TabIndex = 27
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lblUltimaEntrada
        '
        Me.lblUltimaEntrada.AutoSize = True
        Me.lblUltimaEntrada.Location = New System.Drawing.Point(566, 10)
        Me.lblUltimaEntrada.Name = "lblUltimaEntrada"
        Me.lblUltimaEntrada.Size = New System.Drawing.Size(79, 13)
        Me.lblUltimaEntrada.TabIndex = 30
        Me.lblUltimaEntrada.Text = "Ultima Entrada:"
        '
        'txtFechaUltimaEntrada
        '
        Me.txtFechaUltimaEntrada.BackColor = System.Drawing.Color.White
        Me.txtFechaUltimaEntrada.Location = New System.Drawing.Point(648, 6)
        Me.txtFechaUltimaEntrada.Name = "txtFechaUltimaEntrada"
        Me.txtFechaUltimaEntrada.ReadOnly = True
        Me.txtFechaUltimaEntrada.Size = New System.Drawing.Size(84, 20)
        Me.txtFechaUltimaEntrada.TabIndex = 31
        Me.ToolTip1.SetToolTip(Me.txtFechaUltimaEntrada, "Fecha de Alta del Cliente")
        '
        'lblVolumenPorCaja
        '
        Me.lblVolumenPorCaja.AutoSize = True
        Me.lblVolumenPorCaja.Location = New System.Drawing.Point(334, 91)
        Me.lblVolumenPorCaja.Name = "lblVolumenPorCaja"
        Me.lblVolumenPorCaja.Size = New System.Drawing.Size(93, 13)
        Me.lblVolumenPorCaja.TabIndex = 22
        Me.lblVolumenPorCaja.Text = "Volumen por Caja:"
        '
        'lblPesoPorCaja
        '
        Me.lblPesoPorCaja.AutoSize = True
        Me.lblPesoPorCaja.Location = New System.Drawing.Point(351, 64)
        Me.lblPesoPorCaja.Name = "lblPesoPorCaja"
        Me.lblPesoPorCaja.Size = New System.Drawing.Size(76, 13)
        Me.lblPesoPorCaja.TabIndex = 20
        Me.lblPesoPorCaja.Text = "Peso por Caja:"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'lblUnidadPorCaja
        '
        Me.lblUnidadPorCaja.AutoSize = True
        Me.lblUnidadPorCaja.Location = New System.Drawing.Point(341, 37)
        Me.lblUnidadPorCaja.Name = "lblUnidadPorCaja"
        Me.lblUnidadPorCaja.Size = New System.Drawing.Size(86, 13)
        Me.lblUnidadPorCaja.TabIndex = 18
        Me.lblUnidadPorCaja.Text = "Unidad por Caja:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 426)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(829, 22)
        Me.StatusStrip1.TabIndex = 33
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'lblUnidadMedida
        '
        Me.lblUnidadMedida.AutoSize = True
        Me.lblUnidadMedida.Location = New System.Drawing.Point(330, 10)
        Me.lblUnidadMedida.Name = "lblUnidadMedida"
        Me.lblUnidadMedida.Size = New System.Drawing.Size(97, 13)
        Me.lblUnidadMedida.TabIndex = 16
        Me.lblUnidadMedida.Text = "Unidad de Medida:"
        '
        'lblImpuesto
        '
        Me.lblImpuesto.AutoSize = True
        Me.lblImpuesto.Location = New System.Drawing.Point(374, 145)
        Me.lblImpuesto.Name = "lblImpuesto"
        Me.lblImpuesto.Size = New System.Drawing.Size(53, 13)
        Me.lblImpuesto.TabIndex = 24
        Me.lblImpuesto.Text = "Impuesto:"
        '
        'lvExistencia
        '
        Me.lvExistencia.Location = New System.Drawing.Point(104, 88)
        Me.lvExistencia.Name = "lvExistencia"
        Me.lvExistencia.Size = New System.Drawing.Size(392, 216)
        Me.lvExistencia.TabIndex = 7
        Me.lvExistencia.UseCompatibleStateImageBehavior = False
        '
        'lblDivision
        '
        Me.lblDivision.AutoSize = True
        Me.lblDivision.Location = New System.Drawing.Point(380, 279)
        Me.lblDivision.Name = "lblDivision"
        Me.lblDivision.Size = New System.Drawing.Size(47, 13)
        Me.lblDivision.TabIndex = 12
        Me.lblDivision.Text = "Division:"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(368, 252)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(59, 13)
        Me.lblProveedor.TabIndex = 10
        Me.lblProveedor.Text = "Proveedor:"
        '
        'tabIdentificacion
        '
        Me.tabIdentificacion.Controls.Add(Me.OcxClasificador1)
        Me.tabIdentificacion.Controls.Add(Me.ocxCuentaCompra)
        Me.tabIdentificacion.Controls.Add(Me.lblCuentaVenta)
        Me.tabIdentificacion.Controls.Add(Me.lblCuentaCompra)
        Me.tabIdentificacion.Controls.Add(Me.txtVolumenPorCaja)
        Me.tabIdentificacion.Controls.Add(Me.lblControlarExistencia)
        Me.tabIdentificacion.Controls.Add(Me.lblActivo)
        Me.tabIdentificacion.Controls.Add(Me.cbxImpuesto)
        Me.tabIdentificacion.Controls.Add(Me.lblCostoPromedio)
        Me.tabIdentificacion.Controls.Add(Me.txtCostoPromedio)
        Me.tabIdentificacion.Controls.Add(Me.lblCostoCG)
        Me.tabIdentificacion.Controls.Add(Me.txtCostoCG)
        Me.tabIdentificacion.Controls.Add(Me.chkControlarExistencia)
        Me.tabIdentificacion.Controls.Add(Me.chkActivo)
        Me.tabIdentificacion.Controls.Add(Me.lblCotizacion)
        Me.tabIdentificacion.Controls.Add(Me.txtCotizacion)
        Me.tabIdentificacion.Controls.Add(Me.lblUltimoCostoSinIVA)
        Me.tabIdentificacion.Controls.Add(Me.txtUltimoCostoSinIVA)
        Me.tabIdentificacion.Controls.Add(Me.lblUltimoCosto)
        Me.tabIdentificacion.Controls.Add(Me.txtUltimoCosto)
        Me.tabIdentificacion.Controls.Add(Me.txtCantidadUltimaSalida)
        Me.tabIdentificacion.Controls.Add(Me.lblUltimaSalida)
        Me.tabIdentificacion.Controls.Add(Me.txtFechaUltimaSalida)
        Me.tabIdentificacion.Controls.Add(Me.txtCantidadUltimaEntrada)
        Me.tabIdentificacion.Controls.Add(Me.lblUltimaEntrada)
        Me.tabIdentificacion.Controls.Add(Me.txtFechaUltimaEntrada)
        Me.tabIdentificacion.Controls.Add(Me.lblVolumenPorCaja)
        Me.tabIdentificacion.Controls.Add(Me.txtPesoPorCaja)
        Me.tabIdentificacion.Controls.Add(Me.lblPesoPorCaja)
        Me.tabIdentificacion.Controls.Add(Me.txtUnidadPorCaja)
        Me.tabIdentificacion.Controls.Add(Me.lblUnidadPorCaja)
        Me.tabIdentificacion.Controls.Add(Me.cbxUnidadMedida)
        Me.tabIdentificacion.Controls.Add(Me.lblUnidadMedida)
        Me.tabIdentificacion.Controls.Add(Me.lblImpuesto)
        Me.tabIdentificacion.Controls.Add(Me.cbxDivision)
        Me.tabIdentificacion.Controls.Add(Me.lblDivision)
        Me.tabIdentificacion.Controls.Add(Me.cbxProveedor)
        Me.tabIdentificacion.Controls.Add(Me.lblProveedor)
        Me.tabIdentificacion.Controls.Add(Me.ocxCuentaVenta)
        Me.tabIdentificacion.Location = New System.Drawing.Point(4, 22)
        Me.tabIdentificacion.Name = "tabIdentificacion"
        Me.tabIdentificacion.Padding = New System.Windows.Forms.Padding(3)
        Me.tabIdentificacion.Size = New System.Drawing.Size(790, 338)
        Me.tabIdentificacion.TabIndex = 0
        Me.tabIdentificacion.Text = "Identificacion"
        Me.tabIdentificacion.UseVisualStyleBackColor = True
        '
        'OcxClasificador1
        '
        Me.OcxClasificador1.frmPadre = Nothing
        Me.OcxClasificador1.Location = New System.Drawing.Point(6, 6)
        Me.OcxClasificador1.Name = "OcxClasificador1"
        Me.OcxClasificador1.Size = New System.Drawing.Size(312, 326)
        Me.OcxClasificador1.StoreProcedure = Nothing
        Me.OcxClasificador1.StoreProcedureDetalle = Nothing
        Me.OcxClasificador1.TabIndex = 48
        Me.OcxClasificador1.Tabla = Nothing
        Me.OcxClasificador1.TablaDetalle = Nothing
        '
        'ocxCuentaCompra
        '
        Me.ocxCuentaCompra.AlturaMaxima = 63
        Me.ocxCuentaCompra.ListarTodas = False
        Me.ocxCuentaCompra.Location = New System.Drawing.Point(433, 195)
        Me.ocxCuentaCompra.SoloLectura = False
        Me.ocxCuentaCompra.Name = "ocxCuentaCompra"
        Me.ocxCuentaCompra.Registro = Nothing
        Me.ocxCuentaCompra.Resolucion173 = False
        Me.ocxCuentaCompra.Seleccionado = False
        Me.ocxCuentaCompra.Size = New System.Drawing.Size(299, 21)
        Me.ocxCuentaCompra.TabIndex = 17
        Me.ocxCuentaCompra.Texto = Nothing
        '
        'txtVolumenPorCaja
        '
        Me.txtVolumenPorCaja.BackColor = System.Drawing.Color.White
        Me.txtVolumenPorCaja.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVolumenPorCaja.Color = System.Drawing.Color.Empty
        Me.txtVolumenPorCaja.Indicaciones = Nothing
        Me.txtVolumenPorCaja.Location = New System.Drawing.Point(433, 87)
        Me.txtVolumenPorCaja.Multilinea = False
        Me.txtVolumenPorCaja.Name = "txtVolumenPorCaja"
        Me.txtVolumenPorCaja.Size = New System.Drawing.Size(114, 21)
        Me.txtVolumenPorCaja.SoloLectura = False
        Me.txtVolumenPorCaja.TabIndex = 23
        Me.txtVolumenPorCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtVolumenPorCaja.Texto = ""
        '
        'cbxImpuesto
        '
        Me.cbxImpuesto.CampoWhere = Nothing
        Me.cbxImpuesto.DataDisplayMember = Nothing
        Me.cbxImpuesto.DataFilter = Nothing
        Me.cbxImpuesto.DataOrderBy = Nothing
        Me.cbxImpuesto.DataSource = Nothing
        Me.cbxImpuesto.DataValueMember = Nothing
        Me.cbxImpuesto.FormABM = Nothing
        Me.cbxImpuesto.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxImpuesto.Location = New System.Drawing.Point(433, 141)
        Me.cbxImpuesto.Name = "cbxImpuesto"
        Me.cbxImpuesto.SeleccionObligatoria = False
        Me.cbxImpuesto.Size = New System.Drawing.Size(114, 21)
        Me.cbxImpuesto.SoloLectura = False
        Me.cbxImpuesto.TabIndex = 25
        Me.cbxImpuesto.Texto = ""
        '
        'txtPesoPorCaja
        '
        Me.txtPesoPorCaja.Color = System.Drawing.Color.Empty
        Me.txtPesoPorCaja.Decimales = True
        Me.txtPesoPorCaja.Indicaciones = Nothing
        Me.txtPesoPorCaja.Location = New System.Drawing.Point(433, 59)
        Me.txtPesoPorCaja.Name = "txtPesoPorCaja"
        Me.txtPesoPorCaja.Size = New System.Drawing.Size(114, 22)
        Me.txtPesoPorCaja.SoloLectura = False
        Me.txtPesoPorCaja.TabIndex = 21
        Me.txtPesoPorCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPesoPorCaja.Texto = "0"
        '
        'txtUnidadPorCaja
        '
        Me.txtUnidadPorCaja.Color = System.Drawing.Color.Empty
        Me.txtUnidadPorCaja.Decimales = True
        Me.txtUnidadPorCaja.Indicaciones = Nothing
        Me.txtUnidadPorCaja.Location = New System.Drawing.Point(433, 32)
        Me.txtUnidadPorCaja.Name = "txtUnidadPorCaja"
        Me.txtUnidadPorCaja.Size = New System.Drawing.Size(114, 22)
        Me.txtUnidadPorCaja.SoloLectura = False
        Me.txtUnidadPorCaja.TabIndex = 19
        Me.txtUnidadPorCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtUnidadPorCaja.Texto = "0"
        '
        'cbxUnidadMedida
        '
        Me.cbxUnidadMedida.CampoWhere = Nothing
        Me.cbxUnidadMedida.DataDisplayMember = Nothing
        Me.cbxUnidadMedida.DataFilter = Nothing
        Me.cbxUnidadMedida.DataOrderBy = Nothing
        Me.cbxUnidadMedida.DataSource = Nothing
        Me.cbxUnidadMedida.DataValueMember = Nothing
        Me.cbxUnidadMedida.FormABM = Nothing
        Me.cbxUnidadMedida.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxUnidadMedida.Location = New System.Drawing.Point(433, 6)
        Me.cbxUnidadMedida.Name = "cbxUnidadMedida"
        Me.cbxUnidadMedida.SeleccionObligatoria = False
        Me.cbxUnidadMedida.Size = New System.Drawing.Size(114, 21)
        Me.cbxUnidadMedida.SoloLectura = False
        Me.cbxUnidadMedida.TabIndex = 17
        Me.cbxUnidadMedida.Texto = ""
        '
        'cbxDivision
        '
        Me.cbxDivision.CampoWhere = Nothing
        Me.cbxDivision.DataDisplayMember = Nothing
        Me.cbxDivision.DataFilter = Nothing
        Me.cbxDivision.DataOrderBy = Nothing
        Me.cbxDivision.DataSource = Nothing
        Me.cbxDivision.DataValueMember = Nothing
        Me.cbxDivision.FormABM = Nothing
        Me.cbxDivision.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxDivision.Location = New System.Drawing.Point(433, 275)
        Me.cbxDivision.Name = "cbxDivision"
        Me.cbxDivision.SeleccionObligatoria = False
        Me.cbxDivision.Size = New System.Drawing.Size(208, 21)
        Me.cbxDivision.SoloLectura = False
        Me.cbxDivision.TabIndex = 13
        Me.cbxDivision.Texto = ""
        '
        'cbxProveedor
        '
        Me.cbxProveedor.CampoWhere = Nothing
        Me.cbxProveedor.DataDisplayMember = Nothing
        Me.cbxProveedor.DataFilter = Nothing
        Me.cbxProveedor.DataOrderBy = Nothing
        Me.cbxProveedor.DataSource = Nothing
        Me.cbxProveedor.DataValueMember = Nothing
        Me.cbxProveedor.FormABM = Nothing
        Me.cbxProveedor.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxProveedor.Location = New System.Drawing.Point(433, 248)
        Me.cbxProveedor.Name = "cbxProveedor"
        Me.cbxProveedor.SeleccionObligatoria = False
        Me.cbxProveedor.Size = New System.Drawing.Size(208, 21)
        Me.cbxProveedor.SoloLectura = False
        Me.cbxProveedor.TabIndex = 11
        Me.cbxProveedor.Texto = ""
        '
        'ocxCuentaVenta
        '
        Me.ocxCuentaVenta.AlturaMaxima = 40
        Me.ocxCuentaVenta.ListarTodas = False
        Me.ocxCuentaVenta.Location = New System.Drawing.Point(433, 221)
        Me.ocxCuentaVenta.SoloLectura = False
        Me.ocxCuentaVenta.Name = "ocxCuentaVenta"
        Me.ocxCuentaVenta.Registro = Nothing
        Me.ocxCuentaVenta.Resolucion173 = False
        Me.ocxCuentaVenta.Seleccionado = False
        Me.ocxCuentaVenta.Size = New System.Drawing.Size(299, 21)
        Me.ocxCuentaVenta.TabIndex = 47
        Me.ocxCuentaVenta.Texto = Nothing
        '
        'btnActualizarExistencia
        '
        Me.btnActualizarExistencia.Location = New System.Drawing.Point(187, 60)
        Me.btnActualizarExistencia.Name = "btnActualizarExistencia"
        Me.btnActualizarExistencia.Size = New System.Drawing.Size(92, 23)
        Me.btnActualizarExistencia.TabIndex = 6
        Me.btnActualizarExistencia.Text = "Actualizar"
        Me.btnActualizarExistencia.UseVisualStyleBackColor = True
        '
        'lblMonedaPrecio
        '
        Me.lblMonedaPrecio.AutoSize = True
        Me.lblMonedaPrecio.Location = New System.Drawing.Point(184, 38)
        Me.lblMonedaPrecio.Name = "lblMonedaPrecio"
        Me.lblMonedaPrecio.Size = New System.Drawing.Size(49, 13)
        Me.lblMonedaPrecio.TabIndex = 6
        Me.lblMonedaPrecio.Text = "Moneda:"
        '
        'tabPrecios
        '
        Me.tabPrecios.Controls.Add(Me.lblMonedaPrecio)
        Me.tabPrecios.Controls.Add(Me.btnCancelarPrecio)
        Me.tabPrecios.Controls.Add(Me.btnEliminarPrecio)
        Me.tabPrecios.Controls.Add(Me.btnGuardarPrecio)
        Me.tabPrecios.Controls.Add(Me.btnEditarPrecio)
        Me.tabPrecios.Controls.Add(Me.btnNuevoPrecio)
        Me.tabPrecios.Controls.Add(Me.cbxMonedaPrecio)
        Me.tabPrecios.Controls.Add(Me.lvListaPrecio)
        Me.tabPrecios.Controls.Add(Me.lblPrecioUnirario)
        Me.tabPrecios.Controls.Add(Me.lblListaPrecio)
        Me.tabPrecios.Controls.Add(Me.txtPrecioUnitario)
        Me.tabPrecios.Controls.Add(Me.cbxListaPrecio)
        Me.tabPrecios.Location = New System.Drawing.Point(4, 22)
        Me.tabPrecios.Name = "tabPrecios"
        Me.tabPrecios.Size = New System.Drawing.Size(790, 338)
        Me.tabPrecios.TabIndex = 3
        Me.tabPrecios.Text = "Precios"
        Me.tabPrecios.UseVisualStyleBackColor = True
        '
        'btnCancelarPrecio
        '
        Me.btnCancelarPrecio.Location = New System.Drawing.Point(347, 61)
        Me.btnCancelarPrecio.Name = "btnCancelarPrecio"
        Me.btnCancelarPrecio.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelarPrecio.TabIndex = 11
        Me.btnCancelarPrecio.Text = "&Cancelar"
        Me.btnCancelarPrecio.UseVisualStyleBackColor = True
        '
        'btnEliminarPrecio
        '
        Me.btnEliminarPrecio.Location = New System.Drawing.Point(428, 61)
        Me.btnEliminarPrecio.Name = "btnEliminarPrecio"
        Me.btnEliminarPrecio.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminarPrecio.TabIndex = 12
        Me.btnEliminarPrecio.Text = "E&liminar"
        Me.btnEliminarPrecio.UseVisualStyleBackColor = True
        '
        'btnGuardarPrecio
        '
        Me.btnGuardarPrecio.Location = New System.Drawing.Point(266, 61)
        Me.btnGuardarPrecio.Name = "btnGuardarPrecio"
        Me.btnGuardarPrecio.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardarPrecio.TabIndex = 10
        Me.btnGuardarPrecio.Text = "&Guardar"
        Me.btnGuardarPrecio.UseVisualStyleBackColor = True
        '
        'btnEditarPrecio
        '
        Me.btnEditarPrecio.Location = New System.Drawing.Point(185, 61)
        Me.btnEditarPrecio.Name = "btnEditarPrecio"
        Me.btnEditarPrecio.Size = New System.Drawing.Size(75, 23)
        Me.btnEditarPrecio.TabIndex = 9
        Me.btnEditarPrecio.Text = "&Editar"
        Me.btnEditarPrecio.UseVisualStyleBackColor = True
        '
        'btnNuevoPrecio
        '
        Me.btnNuevoPrecio.Location = New System.Drawing.Point(104, 61)
        Me.btnNuevoPrecio.Name = "btnNuevoPrecio"
        Me.btnNuevoPrecio.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevoPrecio.TabIndex = 8
        Me.btnNuevoPrecio.Text = "&Nuevo"
        Me.btnNuevoPrecio.UseVisualStyleBackColor = True
        '
        'cbxMonedaPrecio
        '
        Me.cbxMonedaPrecio.CampoWhere = Nothing
        Me.cbxMonedaPrecio.DataDisplayMember = Nothing
        Me.cbxMonedaPrecio.DataFilter = Nothing
        Me.cbxMonedaPrecio.DataOrderBy = Nothing
        Me.cbxMonedaPrecio.DataSource = Nothing
        Me.cbxMonedaPrecio.DataValueMember = Nothing
        Me.cbxMonedaPrecio.FormABM = Nothing
        Me.cbxMonedaPrecio.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxMonedaPrecio.Location = New System.Drawing.Point(239, 34)
        Me.cbxMonedaPrecio.Name = "cbxMonedaPrecio"
        Me.cbxMonedaPrecio.SeleccionObligatoria = False
        Me.cbxMonedaPrecio.Size = New System.Drawing.Size(99, 21)
        Me.cbxMonedaPrecio.SoloLectura = False
        Me.cbxMonedaPrecio.TabIndex = 7
        Me.cbxMonedaPrecio.Texto = ""
        '
        'lvListaPrecio
        '
        Me.lvListaPrecio.Location = New System.Drawing.Point(104, 90)
        Me.lvListaPrecio.Name = "lvListaPrecio"
        Me.lvListaPrecio.Size = New System.Drawing.Size(399, 232)
        Me.lvListaPrecio.TabIndex = 13
        Me.lvListaPrecio.UseCompatibleStateImageBehavior = False
        '
        'lblPrecioUnirario
        '
        Me.lblPrecioUnirario.AutoSize = True
        Me.lblPrecioUnirario.Location = New System.Drawing.Point(11, 38)
        Me.lblPrecioUnirario.Name = "lblPrecioUnirario"
        Me.lblPrecioUnirario.Size = New System.Drawing.Size(79, 13)
        Me.lblPrecioUnirario.TabIndex = 4
        Me.lblPrecioUnirario.Text = "Precio Unitario:"
        '
        'lblListaPrecio
        '
        Me.lblListaPrecio.AutoSize = True
        Me.lblListaPrecio.Location = New System.Drawing.Point(11, 10)
        Me.lblListaPrecio.Name = "lblListaPrecio"
        Me.lblListaPrecio.Size = New System.Drawing.Size(80, 13)
        Me.lblListaPrecio.TabIndex = 0
        Me.lblListaPrecio.Text = "Lista de Precio:"
        '
        'txtPrecioUnitario
        '
        Me.txtPrecioUnitario.Color = System.Drawing.Color.Empty
        Me.txtPrecioUnitario.Decimales = True
        Me.txtPrecioUnitario.Indicaciones = Nothing
        Me.txtPrecioUnitario.Location = New System.Drawing.Point(104, 33)
        Me.txtPrecioUnitario.Name = "txtPrecioUnitario"
        Me.txtPrecioUnitario.Size = New System.Drawing.Size(74, 22)
        Me.txtPrecioUnitario.SoloLectura = False
        Me.txtPrecioUnitario.TabIndex = 5
        Me.txtPrecioUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrecioUnitario.Texto = "0"
        '
        'cbxListaPrecio
        '
        Me.cbxListaPrecio.CampoWhere = Nothing
        Me.cbxListaPrecio.DataDisplayMember = Nothing
        Me.cbxListaPrecio.DataFilter = Nothing
        Me.cbxListaPrecio.DataOrderBy = Nothing
        Me.cbxListaPrecio.DataSource = Nothing
        Me.cbxListaPrecio.DataValueMember = Nothing
        Me.cbxListaPrecio.FormABM = Nothing
        Me.cbxListaPrecio.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxListaPrecio.Location = New System.Drawing.Point(104, 6)
        Me.cbxListaPrecio.Name = "cbxListaPrecio"
        Me.cbxListaPrecio.SeleccionObligatoria = False
        Me.cbxListaPrecio.Size = New System.Drawing.Size(234, 21)
        Me.cbxListaPrecio.SoloLectura = False
        Me.cbxListaPrecio.TabIndex = 1
        Me.cbxListaPrecio.Texto = ""
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tabIdentificacion)
        Me.TabControl1.Controls.Add(Me.tabPrecios)
        Me.TabControl1.Controls.Add(Me.tabExistencia)
        Me.TabControl1.Controls.Add(Me.tabLotes)
        Me.TabControl1.Location = New System.Drawing.Point(15, 30)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(798, 364)
        Me.TabControl1.TabIndex = 26
        '
        'tabExistencia
        '
        Me.tabExistencia.Controls.Add(Me.txtExistenciaCritica)
        Me.tabExistencia.Controls.Add(Me.lblExistenciaCritica)
        Me.tabExistencia.Controls.Add(Me.lblExistenciaGeneral)
        Me.tabExistencia.Controls.Add(Me.btnActualizarExistencia)
        Me.tabExistencia.Controls.Add(Me.txtExistenciaGeneral)
        Me.tabExistencia.Controls.Add(Me.lvExistencia)
        Me.tabExistencia.Controls.Add(Me.lblExistenciaMinima)
        Me.tabExistencia.Controls.Add(Me.lblSucursalDeposito)
        Me.tabExistencia.Controls.Add(Me.txtExistenciaMinima)
        Me.tabExistencia.Controls.Add(Me.cbxSucursalDeposito)
        Me.tabExistencia.Location = New System.Drawing.Point(4, 22)
        Me.tabExistencia.Name = "tabExistencia"
        Me.tabExistencia.Padding = New System.Windows.Forms.Padding(3)
        Me.tabExistencia.Size = New System.Drawing.Size(790, 338)
        Me.tabExistencia.TabIndex = 1
        Me.tabExistencia.Text = "Existencia"
        Me.tabExistencia.UseVisualStyleBackColor = True
        '
        'txtExistenciaCritica
        '
        Me.txtExistenciaCritica.Color = System.Drawing.Color.Empty
        Me.txtExistenciaCritica.Decimales = True
        Me.txtExistenciaCritica.Indicaciones = Nothing
        Me.txtExistenciaCritica.Location = New System.Drawing.Point(104, 60)
        Me.txtExistenciaCritica.Name = "txtExistenciaCritica"
        Me.txtExistenciaCritica.Size = New System.Drawing.Size(77, 22)
        Me.txtExistenciaCritica.SoloLectura = False
        Me.txtExistenciaCritica.TabIndex = 5
        Me.txtExistenciaCritica.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtExistenciaCritica.Texto = "0"
        '
        'lblExistenciaCritica
        '
        Me.lblExistenciaCritica.AutoSize = True
        Me.lblExistenciaCritica.Location = New System.Drawing.Point(11, 65)
        Me.lblExistenciaCritica.Name = "lblExistenciaCritica"
        Me.lblExistenciaCritica.Size = New System.Drawing.Size(90, 13)
        Me.lblExistenciaCritica.TabIndex = 4
        Me.lblExistenciaCritica.Text = "Existencia Critica:"
        '
        'lblExistenciaGeneral
        '
        Me.lblExistenciaGeneral.AutoSize = True
        Me.lblExistenciaGeneral.Location = New System.Drawing.Point(315, 315)
        Me.lblExistenciaGeneral.Name = "lblExistenciaGeneral"
        Me.lblExistenciaGeneral.Size = New System.Drawing.Size(98, 13)
        Me.lblExistenciaGeneral.TabIndex = 8
        Me.lblExistenciaGeneral.Text = "Existencia General:"
        '
        'txtExistenciaGeneral
        '
        Me.txtExistenciaGeneral.Color = System.Drawing.Color.Empty
        Me.txtExistenciaGeneral.Decimales = True
        Me.txtExistenciaGeneral.Indicaciones = Nothing
        Me.txtExistenciaGeneral.Location = New System.Drawing.Point(419, 310)
        Me.txtExistenciaGeneral.Name = "txtExistenciaGeneral"
        Me.txtExistenciaGeneral.Size = New System.Drawing.Size(77, 22)
        Me.txtExistenciaGeneral.SoloLectura = False
        Me.txtExistenciaGeneral.TabIndex = 9
        Me.txtExistenciaGeneral.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtExistenciaGeneral.Texto = "0"
        '
        'txtExistenciaMinima
        '
        Me.txtExistenciaMinima.Color = System.Drawing.Color.Empty
        Me.txtExistenciaMinima.Decimales = True
        Me.txtExistenciaMinima.Indicaciones = Nothing
        Me.txtExistenciaMinima.Location = New System.Drawing.Point(104, 33)
        Me.txtExistenciaMinima.Name = "txtExistenciaMinima"
        Me.txtExistenciaMinima.Size = New System.Drawing.Size(77, 22)
        Me.txtExistenciaMinima.SoloLectura = False
        Me.txtExistenciaMinima.TabIndex = 3
        Me.txtExistenciaMinima.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtExistenciaMinima.Texto = "0"
        '
        'cbxSucursalDeposito
        '
        Me.cbxSucursalDeposito.CampoWhere = Nothing
        Me.cbxSucursalDeposito.DataDisplayMember = Nothing
        Me.cbxSucursalDeposito.DataFilter = Nothing
        Me.cbxSucursalDeposito.DataOrderBy = Nothing
        Me.cbxSucursalDeposito.DataSource = Nothing
        Me.cbxSucursalDeposito.DataValueMember = Nothing
        Me.cbxSucursalDeposito.FormABM = Nothing
        Me.cbxSucursalDeposito.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSucursalDeposito.Location = New System.Drawing.Point(104, 6)
        Me.cbxSucursalDeposito.Name = "cbxSucursalDeposito"
        Me.cbxSucursalDeposito.SeleccionObligatoria = False
        Me.cbxSucursalDeposito.Size = New System.Drawing.Size(175, 21)
        Me.cbxSucursalDeposito.SoloLectura = False
        Me.cbxSucursalDeposito.TabIndex = 1
        Me.cbxSucursalDeposito.Texto = ""
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(765, 2)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(58, 23)
        Me.btnBuscar.TabIndex = 25
        Me.btnBuscar.Text = "&Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(125, 7)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 19
        Me.lblDescripcion.Text = "Descripción:"
        '
        'lblCodigoBarra
        '
        Me.lblCodigoBarra.AutoSize = True
        Me.lblCodigoBarra.Location = New System.Drawing.Point(426, 7)
        Me.lblCodigoBarra.Name = "lblCodigoBarra"
        Me.lblCodigoBarra.Size = New System.Drawing.Size(86, 13)
        Me.lblCodigoBarra.TabIndex = 21
        Me.lblCodigoBarra.Text = "Codigo de Barra:"
        '
        'lblReferencia
        '
        Me.lblReferencia.AutoSize = True
        Me.lblReferencia.Location = New System.Drawing.Point(628, 7)
        Me.lblReferencia.Name = "lblReferencia"
        Me.lblReferencia.Size = New System.Drawing.Size(62, 13)
        Me.lblReferencia.TabIndex = 23
        Me.lblReferencia.Text = "Referencia:"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BackColor = System.Drawing.Color.White
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = "(F1) Para busqueda avanzada"
        Me.txtDescripcion.Location = New System.Drawing.Point(197, 3)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(223, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 20
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'txtReferencia
        '
        Me.txtReferencia.BackColor = System.Drawing.Color.White
        Me.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferencia.Color = System.Drawing.Color.Empty
        Me.txtReferencia.Indicaciones = Nothing
        Me.txtReferencia.Location = New System.Drawing.Point(693, 3)
        Me.txtReferencia.Multilinea = False
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Size = New System.Drawing.Size(66, 21)
        Me.txtReferencia.SoloLectura = False
        Me.txtReferencia.TabIndex = 24
        Me.txtReferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferencia.Texto = ""
        '
        'txtCodigoBarra
        '
        Me.txtCodigoBarra.BackColor = System.Drawing.Color.White
        Me.txtCodigoBarra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoBarra.Color = System.Drawing.Color.Empty
        Me.txtCodigoBarra.Indicaciones = Nothing
        Me.txtCodigoBarra.Location = New System.Drawing.Point(518, 3)
        Me.txtCodigoBarra.Multilinea = False
        Me.txtCodigoBarra.Name = "txtCodigoBarra"
        Me.txtCodigoBarra.Size = New System.Drawing.Size(104, 21)
        Me.txtCodigoBarra.SoloLectura = False
        Me.txtCodigoBarra.TabIndex = 22
        Me.txtCodigoBarra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtCodigoBarra.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(61, 3)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(58, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 18
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'frmProducto2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(829, 448)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.txtReferencia)
        Me.Controls.Add(Me.txtCodigoBarra)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.lblCodigoBarra)
        Me.Controls.Add(Me.lblReferencia)
        Me.Controls.Add(Me.txtID)
        Me.Name = "frmProducto2"
        Me.Tag = "PRODUCTO"
        Me.Text = "frmProducto2"
        Me.tabLotes.ResumeLayout(False)
        Me.tabLotes.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.tabIdentificacion.ResumeLayout(False)
        Me.tabIdentificacion.PerformLayout()
        Me.tabPrecios.ResumeLayout(False)
        Me.tabPrecios.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.tabExistencia.ResumeLayout(False)
        Me.tabExistencia.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ocxCuentaCompra As ERP.ocxTXTCuentaContable
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents lblCuentaVenta As System.Windows.Forms.Label
    Friend WithEvents ListView2 As System.Windows.Forms.ListView
    Friend WithEvents lblCuentaCompra As System.Windows.Forms.Label
    Friend WithEvents txtVolumenPorCaja As ERP.ocxTXTString
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents lblControlarExistencia As System.Windows.Forms.Label
    Friend WithEvents lblActivo As System.Windows.Forms.Label
    Friend WithEvents cbxImpuesto As ERP.ocxCBX
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents lblCostoPromedio As System.Windows.Forms.Label
    Friend WithEvents OcxTXTDate1 As ERP.ocxTXTDate
    Friend WithEvents txtCostoPromedio As System.Windows.Forms.TextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents OcxTXTString2 As ERP.ocxTXTString
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents lblCostoCG As System.Windows.Forms.Label
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents txtCostoCG As System.Windows.Forms.TextBox
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents chkControlarExistencia As System.Windows.Forms.CheckBox
    Friend WithEvents chkActivo As System.Windows.Forms.CheckBox
    Friend WithEvents lblCotizacion As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As System.Windows.Forms.TextBox
    Friend WithEvents lblUltimoCostoSinIVA As System.Windows.Forms.Label
    Friend WithEvents txtUltimoCostoSinIVA As System.Windows.Forms.TextBox
    Friend WithEvents lblExistenciaMinima As System.Windows.Forms.Label
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents lblUltimoCosto As System.Windows.Forms.Label
    Friend WithEvents tabLotes As System.Windows.Forms.TabPage
    Friend WithEvents lblSucursalDeposito As System.Windows.Forms.Label
    Friend WithEvents txtExistenciaMinima As ERP.ocxTXTNumeric
    Friend WithEvents cbxSucursalDeposito As ERP.ocxCBX
    Friend WithEvents txtUltimoCosto As System.Windows.Forms.TextBox
    Friend WithEvents txtCantidadUltimaSalida As System.Windows.Forms.TextBox
    Friend WithEvents lblUltimaSalida As System.Windows.Forms.Label
    Friend WithEvents txtCantidadUltimaEntrada As System.Windows.Forms.TextBox
    Friend WithEvents txtFechaUltimaSalida As System.Windows.Forms.TextBox
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lblUltimaEntrada As System.Windows.Forms.Label
    Friend WithEvents txtFechaUltimaEntrada As System.Windows.Forms.TextBox
    Friend WithEvents lblVolumenPorCaja As System.Windows.Forms.Label
    Friend WithEvents txtPesoPorCaja As ERP.ocxTXTNumeric
    Friend WithEvents lblPesoPorCaja As System.Windows.Forms.Label
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtReferencia As ERP.ocxTXTString
    Friend WithEvents txtCodigoBarra As ERP.ocxTXTString
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tabIdentificacion As System.Windows.Forms.TabPage
    Friend WithEvents txtUnidadPorCaja As ERP.ocxTXTNumeric
    Friend WithEvents lblUnidadPorCaja As System.Windows.Forms.Label
    Friend WithEvents cbxUnidadMedida As ERP.ocxCBX
    Friend WithEvents lblUnidadMedida As System.Windows.Forms.Label
    Friend WithEvents lblImpuesto As System.Windows.Forms.Label
    Friend WithEvents cbxDivision As ERP.ocxCBX
    Friend WithEvents lblDivision As System.Windows.Forms.Label
    Friend WithEvents cbxProveedor As ERP.ocxCBX
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents ocxCuentaVenta As ERP.ocxTXTCuentaContable
    Friend WithEvents tabPrecios As System.Windows.Forms.TabPage
    Friend WithEvents lblMonedaPrecio As System.Windows.Forms.Label
    Friend WithEvents cbxMonedaPrecio As ERP.ocxCBX
    Friend WithEvents btnCancelarPrecio As System.Windows.Forms.Button
    Friend WithEvents btnEliminarPrecio As System.Windows.Forms.Button
    Friend WithEvents btnGuardarPrecio As System.Windows.Forms.Button
    Friend WithEvents btnEditarPrecio As System.Windows.Forms.Button
    Friend WithEvents btnNuevoPrecio As System.Windows.Forms.Button
    Friend WithEvents lvListaPrecio As System.Windows.Forms.ListView
    Friend WithEvents lblPrecioUnirario As System.Windows.Forms.Label
    Friend WithEvents lblListaPrecio As System.Windows.Forms.Label
    Friend WithEvents txtPrecioUnitario As ERP.ocxTXTNumeric
    Friend WithEvents cbxListaPrecio As ERP.ocxCBX
    Friend WithEvents tabExistencia As System.Windows.Forms.TabPage
    Friend WithEvents txtExistenciaCritica As ERP.ocxTXTNumeric
    Friend WithEvents lblExistenciaCritica As System.Windows.Forms.Label
    Friend WithEvents lblExistenciaGeneral As System.Windows.Forms.Label
    Friend WithEvents btnActualizarExistencia As System.Windows.Forms.Button
    Friend WithEvents txtExistenciaGeneral As ERP.ocxTXTNumeric
    Friend WithEvents lvExistencia As System.Windows.Forms.ListView
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblCodigoBarra As System.Windows.Forms.Label
    Friend WithEvents lblReferencia As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents OcxClasificador1 As ERP.ocxClasificador
End Class

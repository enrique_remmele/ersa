﻿Public Class frmInactivacionClientes
    'CLASES
    Dim CSistema As New CSistema

    Sub Inicializar()
        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True
        txtFechaDesde.PrimerDiaAño()
    End Sub
    Sub Procesar()

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer


        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        CSistema.SetSQLParameter(param, "@IdEstado", cbxEstado.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFechaDesde.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IdUsuario", vgIDUsuario, ParameterDirection.Input)

        IndiceOperacion = param.GetLength(0) - 1

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)


        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "spInactivaClientes", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnProcesar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnProcesar, ErrorIconAlignment.TopRight)
            Exit Sub

        Else
            MessageBox.Show("Registro Procesado!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()


        End If

    End Sub

    Private Sub frmInactivacionClientes_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Close()
    End Sub

    Private Sub btnProcesar_Click(sender As System.Object, e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub
End Class
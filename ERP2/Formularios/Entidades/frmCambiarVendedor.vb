﻿Public Class frmCambiarVendedor
    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vControles() As Control

    Sub Inicializar()
        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        txtFechaDesde.Enabled = False
        txtFechaHasta.Enabled = False
        CargarInformacion()

    End Sub
    Sub Procesar()
        If cbxVendedorActual.Texto = cbxVendedorNuevo.Texto Then
            MessageBox.Show("Los vendedores no pueden ser iguales", "Cambiar vendedor", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If chkActualizarVenta.Valor = True Then
            If txtFechaDesde.GetValue > txtFechaHasta.GetValue Then
                MessageBox.Show("Fecha hasta no debe ser mayor que Fecha Desde", "Cambiar vendedor", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer


        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        CSistema.SetSQLParameter(param, "@IdVendedorActual", cbxVendedorActual.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IdVendedorNuevo", cbxVendedorNuevo.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaDesde", CSistema.FormatoFechaBaseDatos(txtFechaDesde.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaHasta", CSistema.FormatoFechaBaseDatos(txtFechaHasta.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ActualizarVenta", chkActualizarVenta.Valor, ParameterDirection.Input)


        IndiceOperacion = param.GetLength(0) - 1

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)


        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "spCambiarVendedor", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnProcesar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnProcesar, ErrorIconAlignment.TopRight)
            Exit Sub

        Else
            MessageBox.Show("Registro Procesado!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()


        End If

    End Sub

    Sub CargarInformacion()
        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxVendedorActual)
        CSistema.CargaControl(vControles, cbxVendedorNuevo)
        CSistema.SqlToComboBox(cbxVendedorActual.cbx, "Select id, Nombres from VVendedor where Estado = 1 order by Nombres")
        CSistema.SqlToComboBox(cbxVendedorNuevo.cbx, "Select id, Nombres from VVendedor where Estado = 1 order by Nombres")

    End Sub

    Private Sub frmCambiarVendedor_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub frmCambiarVendedor_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub


    Private Sub btnProcesar_Click(sender As System.Object, e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

    Private Sub chxActualizarVenta_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkActualizarVenta.PropertyChanged
        txtFechaDesde.Enabled = chkActualizarVenta.Valor
        txtFechaHasta.Enabled = chkActualizarVenta.Valor

        If chkActualizarVenta.Valor = True Then
            txtFechaDesde.PrimerDiaMes()
            txtFechaHasta.Hoy()
        End If
        If chkActualizarVenta.Valor = False Then
            txtFechaDesde.ResetText()
            txtFechaHasta.ResetText()
        End If

    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Close()
    End Sub
End Class
﻿Public Class frmDepartamentoEmpresa

    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        'Funciones
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Focus
        dgvLista.Focus()

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtDepartamento)
        CSistema.CargaControl(vControles, cbxUsuario)
        CSistema.CargaControl(vControles, cbxSucursal)
        'CSistema.CargaControl(vControles, chkSeccion)

        CSistema.SqlToComboBox(cbxUsuario, "Select ID, Nombre from Usuario where estado = 1")

        CSistema.SqlToComboBox(cbxSucursal, "Select ID,Descripcion from vSucursal where estado = 1")
        'Cargamos los registos en el lv
        Listar()

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If dgvLista.SelectedRows.Count = 0 Then
            ctrError.SetError(dgvLista, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgvLista, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = dgvLista.SelectedRows(0).Cells(0).Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        'dt = CSistema.ExecuteToDataTable("Select ID, Descripcion, CodigoUnilever, Estado From TipoCliente Where ID=" & ID)
        dt = CSistema.ExecuteToDataTable("Select ID, Departamento, IDUsuarioJefe, Jefe, IDSucursal,Estado,Seccion From vDepartamentoEmpresa Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            txtDepartamento.txt.Text = oRow("Departamento").ToString
            cbxUsuario.SelectedValue(oRow("IDUsuarioJefe"))
            cbxSucursal.SelectedValue(oRow("IDSucursal"))

            If CBool(oRow("Estado")) = True Then
                rdbActivo.Checked = True
            Else
                rdbDesactivado.Checked = True
            End If

            If CBool(oRow("Seccion")) = True Then
                chkSeccion.chk.Checked = True
            Else
                chkSeccion.chk.Checked = False
            End If

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select ID, Departamento, Jefe, Sucursal From vDepartamentoEmpresa Order By 2")
        CSistema.dtToGrid(dgvLista, dt)
        'dgvLista.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub InicializarControles()

        'TextBox
        txtDepartamento.txt.Clear()
        cbxUsuario.cbx.Text = ""
        cbxSucursal.cbx.Text = ""

        'RadioButton

        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From DepartamentoEmpresa"), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From DepartamentoEmpresa"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        txtDepartamento.Focus()


    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Escritura de la Descripcion
        If txtDepartamento.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar una descripcion valida!"
            ctrError.SetError(txtDepartamento, mensaje)
            ctrError.SetIconAlignment(txtDepartamento, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgvLista.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgvLista, mensaje)
                ctrError.SetIconAlignment(dgvLista, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@CodigoUnilever", txtCodigoUnilever.txt.Text.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Departamento", txtDepartamento.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuarioJefe", cbxUsuario.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)

        'FA 07/03/2023
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Seccion", chkSeccion.chk.Checked, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpDepartamentoEmpresa", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            Listar(txtID.txt.Text)
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno

            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        txtDepartamento.Focus()
        txtDepartamento.txt.SelectAll()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub frmTipoCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub dgvLista_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLista.CellContentClick
        ObtenerInformacion()
    End Sub

    Private Sub dgvLista_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles dgvLista.SelectionChanged
        ObtenerInformacion()
    End Sub

End Class
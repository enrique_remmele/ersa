﻿Imports ERP.Reporte
Public Class frmCuentaBancariaSaldoHistorial
    Public IDCuentaBancaria As String
    Public Moneda As String
    Dim CReporte As New CReporteCuentaBancaria
    Dim CSistema As New CSistema

    Private Sub CargarInformacion()
        CSistema.SqlToComboBox(cbxCuentaBancaria, "Select ID, Descripcion From vCuentaBancaria Order By Descripcion")
        cbxCuentaBancaria.cbx.SelectedValue = IDCuentaBancaria

    End Sub

    Private Sub ListarInforme()

        Dim Where As String = "Where Fecha Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "' " & IIf(cbxCuentaBancaria.cbx.SelectedValue <> 0, " And IDCuentaBancaria = '" & cbxCuentaBancaria.GetValue & "'", "")
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Subtitulo As String = " DESDE " & txtFechaDesde.GetValueString & " HASTA " & txtFechaHasta.GetValueString & " MONEDA: " & cbxCuentaBancaria.txt.Text
        Dim Titulo As String = "SALDO DIARIO DE CUENTA BANCARIA "
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        CReporte.ListarCuentaBancariaSaldoDiario(frm, Where, "", vgUsuarioIdentificador, Titulo, Subtitulo, "", True)
    End Sub

    Private Sub frmCotizacionHistorial_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        CargarInformacion()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        ListarInforme()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Close()
    End Sub

    Private Sub frmCotizacionHistorial_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Down Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Or e.KeyCode = Keys.Up Then
            CSistema.SelectNextControl(Me, e.KeyCode)
        End If
    End Sub
End Class
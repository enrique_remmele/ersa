﻿Public Class frmCategoriaCliente

    Dim CSistema As New CSistema
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim DT As DataTable


    Sub Inicializar()
        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        'RadioButton
        rbActivado.Checked = True

        'Funciones
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnGuardar, btnCancelar, btnEliminar, vControles)


    End Sub

    Sub CargarInformacion()
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtDescripcion)
        CSistema.CargaControl(vControles, rbActivado)
        CSistema.CargaControl(vControles, rbDesactivado)
        Listar()
    End Sub

    Sub ObtenerInformacion()
        If dgv.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim id As Integer = dgv.SelectedRows(0).Cells("ID").Value
        For Each oRow As DataRow In DT.Select("id= " & id)
            txtID.Text = id
            txtDescripcion.Text = oRow("Descripcion")
            If CBool(oRow("Estado")) = True Then
                rbActivado.Checked = True
                rbDesactivado.Checked = False
            Else
                rbDesactivado.Checked = True
                rbActivado.Checked = False
            End If
        Next
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnGuardar, btnCancelar, btnEliminar, vControles)


    End Sub

    Sub InicializarControles()
        txtDescripcion.Text = ""
        rbActivado.Checked = True
        rbDesactivado.Checked = False
        If vNuevo = True Then
            txtID.Text = CSistema.ExecuteScalar("Select max(isnull(ID,0)+1) from CategoriaCliente")
        End If
    End Sub

    Sub Listar()

        DT = CSistema.ExecuteToDataTable("Select * from CategoriaCliente")
        CSistema.dtToGrid(dgv, DT)
        dgv.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.ReadOnly = True

    End Sub

    Sub Procesar(ByVal Operacion As String)

        'Validar
        'Escritura de la Descripcion
        If txtDescripcion.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar una descripción valida!"
            MessageBox.Show(mensaje, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtDescripcion.Focus()
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion <> "INS" Then
            If dgv.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                MessageBox.Show(mensaje, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                 Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = "DEL" Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer
        If vNuevo = False Then
            ID = txtID.Text
        Else
            ID = 0
        End If


        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rbActivado.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion, ParameterDirection.Input)
        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpCategoriaCliente", False, False, MensajeRetorno, "", True) = True Then
            MessageBox.Show(MensajeRetorno, "Informe", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            Listar()
        Else
            MessageBox.Show(MensajeRetorno, "Informe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmCategoriaCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()

    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnGuardar, btnCancelar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()

    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs)
        ObtenerInformacion()
    End Sub

    Private Sub btnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        vNuevo = False
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnGuardar, btnCancelar, btnEliminar, vControles)
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Select Case vNuevo
            Case True
                Procesar("INS")
            Case False
                Procesar("UPD")
        End Select
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Procesar("DEL")
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnGuardar, btnCancelar, btnEliminar, vControles)
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnGuardar, btnCancelar, btnEliminar, vControles)
    End Sub

    Private Sub dgv_SelectionChanged(sender As Object, e As EventArgs) Handles dgv.SelectionChanged
        ObtenerInformacion()
    End Sub
End Class
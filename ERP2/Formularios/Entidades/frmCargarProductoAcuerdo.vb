﻿Public Class frmCargarProductoAcuerdo

    'CLASES
    Dim CSistema As New CSistema
        Dim CData As New CData

        'PROPIEDADES
        Private RegistroValue As DataRow
        Public Property Registro() As DataRow
            Get
                Return RegistroValue
            End Get
            Set(ByVal value As DataRow)
                RegistroValue = value
            End Set
        End Property

        Private Proveedorvalue As Boolean
        Public Property Proveedor() As Boolean
            Get
                Return Proveedorvalue
            End Get
            Set(ByVal value As Boolean)
                Proveedorvalue = value
            End Set
        End Property

        Private ObservacionValue As String
        Public Property Observacion() As String
            Get
                Return ObservacionValue
            End Get
            Set(ByVal value As String)
                ObservacionValue = value
            End Set
        End Property

        Private ImporteValue As Decimal
        Public Property Importe() As Decimal
            Get
                Return ImporteValue
            End Get
            Set(ByVal value As Decimal)
                ImporteValue = value
            End Set
        End Property

        Private ProcesadoValue As Boolean
        Public Property Procesado() As Boolean
            Get
                Return ProcesadoValue
            End Get
            Set(ByVal value As Boolean)
                ProcesadoValue = value
            End Set
        End Property

        Public Property IDMoneda As Boolean
        Public Property Decimales As Boolean
    Public Property IDSucursal As Integer
    Public Property Porcentaje As Integer

    'FUNCIONES
    Sub Inicializar()

            Me.KeyPreview = True
            Me.AcceptButton = New Button
        Dim Dt As DataTable

        'Cambio para probar descuentos por productos
        'Dim dtDescuento As DataTable = CData.GetTable("VProducto", "ControlarExistencia='False'")
        'Dim dtDescuento As DataTable = CData.GetTable("VProducto", "TipoProducto='Descuentos'")

        If IDSucursal <> 5 Then
                'Dim dtDescuento As DataTable = CData.GetTable("VProducto", "IDTipoProducto in ('1', '2', '6')")
                If Proveedor = True Then
                Dt = CData.GetTable("VProducto", "IDTipoProducto in ('34')")
            Else
                Dt = CData.GetTable("VProducto", "IDTipoProducto in ('1', '2', '6')")
            End If
            Else
            'Dim dtDescuento As DataTable = CData.GetTable("VProducto", "vendible = True")
            Dt = CData.GetTable("VProducto", "vendible = True")
        End If
        'Dim dtDescuento As DataTable = CData.GetTable("VProducto")
        txtProducto.Conectar(Dt)
        'txtImporte.Decimales = Decimales

        Procesado = False

        'If IDMoneda = 2 Then
        '    txtImporte.Decimales = True
        'End If

    End Sub

    Sub Seleccionar()

        If txtProducto.Seleccionado = False Then
            CSistema.MostrarError("Seleccione un Producto!", ctrError, btnAceptar, tsslEstado)
            Exit Sub
        End If

        'If txtObservacion.txt.Text = "" Then
        '    CSistema.MostrarError("Debe ingresar alguna observación!", ctrError, btnAceptar, tsslEstado)
        '    Exit Sub
        'End If

        Registro = txtProducto.Registro
        If txtPorcentaje.txt.Text > 0 Then
            Porcentaje = txtPorcentaje.txt.Text
        Else
            Porcentaje = 0
        End If

        '    Observacion = txtObservacion.txt.Text.Trim
        '    Importe = txtImporte.ObtenerValor

        Procesado = True
        Me.Close()

    End Sub

    Private Sub frmCargarProductoAcuerdo_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmCargarProductoAcuerdo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Seleccionar()
    End Sub

    Private Sub txtDescuento_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado

    End Sub

    Private Sub Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancelar.Click
        Me.Close()

    End Sub

    Private Sub ChkPorcentaje_CheckedChanged(sender As Object, e As EventArgs) Handles ChkPorcentaje.CheckedChanged
        HabilitarPorcentaje()
    End Sub

    Sub HabilitarPorcentaje()
        'If vNuevo = True Then
        If ChkPorcentaje.Checked = True Then
            txtPorcentaje.Enabled = True
        Else
            txtPorcentaje.Enabled = False
        End If
        'End If
    End Sub

    '10-06-2021 - SC - Actualiza datos
    Sub frmCargarProductoAcuerdo_Activate()
        Me.Refresh()
    End Sub

End Class
﻿Public Class frmProductoSeleccionMultiple

    Dim CData As New CData

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'PROPIEDADES
    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property
    Public Property IDDeposito() As Integer


    Public Property Consulta() As String

    Public Property ConExistencia() As Boolean = True


    Private ColumnasNumericasValue As String()
    Public Property ColumnasNumericas() As String()
        Get
            Return ColumnasNumericasValue
        End Get
        Set(ByVal value As String())
            ColumnasNumericasValue = value
        End Set
    End Property

    Private ControlarExistenciaValue As Boolean
    Public Property ControlarExistencia() As Boolean
        Get
            Return ControlarExistenciaValue
        End Get
        Set(ByVal value As Boolean)
            ControlarExistenciaValue = value
        End Set
    End Property

    Public Property dt As DataTable
    Public Property dtSeleccionado As DataTable

    'VARIABLES

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'FUNCIONES

    'Cargar Informacion
    Sub CargarInformacion()

        Me.AcceptButton = New Button

        'Seleccion
        Dim Seleccion As Integer = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SELECCION", "1")

        Select Case Seleccion

            Case 1
                rdbDescripcion.Checked = True
            Case 2
                rdbCodigoBarra.Checked = True
            Case 3
                rdbReferencia.Checked = True
            Case 4
                rdbID.Checked = True
            Case Else
                rdbDescripcion.Checked = True
        End Select

        dt = CSistema.ExecuteToDataTable("Select 'Sel'=Convert(bit, 'False'), ID, Ref, Descripcion, 'Codigo de Barra'=CodigoBarra, 'Tipo de Producto'=TipoProducto From VProducto Where ID>0 ")

        dtSeleccionado = CSistema.ExecuteToDataTable("Select Top(0) 'Sel'=Convert(bit, 'False'), ID, Ref, Descripcion, 'Codigo de Barra'=CodigoBarra, 'Tipo de Producto'=TipoProducto From VProducto Where ID>0 ")

        chkActivo.chk.Checked = True
        chkExistencia.chk.Checked = ConExistencia
        'Foco
        txtDescripcion.Focus()
        txtDescripcion.SelectAll()

        Listar()

    End Sub

    'Guardar Informacion
    Sub GuardarInformacion()

        Dim Seleccion As Integer = 1

        If rdbDescripcion.Checked = True Then
            Seleccion = 1
        End If

        If rdbCodigoBarra.Checked = True Then
            Seleccion = 2
        End If

        If rdbReferencia.Checked = True Then
            Seleccion = 3
        End If

        If rdbID.Checked = True Then
            Seleccion = 4
        End If

        If rdbDescripcion.Checked = True Then
            Seleccion = 5
        End If

        'Seleccion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SELECCION", Seleccion)

    End Sub

    'Listar
    Sub Listar()

        Dim Where As String = ""
        Dim OrderBy As String = ""

        If rdbDescripcion.Checked = True Then
            CSistema.dtToGrid(dgw, CData.FiltrarDataTable(dt, " Descripcion Like '%" & txtDescripcion.Text.Trim & "%' "))
        End If

        If rdbCodigoBarra.Checked = True Then
            CSistema.dtToGrid(dgw, CData.FiltrarDataTable(dt, " CodigoBarra Like '%" & txtDescripcion.Text.Trim & "%' "))
        End If

        If rdbReferencia.Checked = True Then
            CSistema.dtToGrid(dgw, CData.FiltrarDataTable(dt, " Ref Like '%" & txtDescripcion.Text.Trim & "%' "))
        End If

        If rdbID.Checked = True Then
            CSistema.dtToGrid(dgw, CData.FiltrarDataTable(dt, " ID Like '%" & txtDescripcion.Text.Trim & "%' "))
        End If

        Try
            'Formato
            dgw.Columns(1).Visible = False
            dgw.Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

            If Not ColumnasNumericas Is Nothing Then
                For i As Integer = 0 To ColumnasNumericas.GetLength(0) - 1
                    dgw.Columns(ColumnasNumericas(i)).DefaultCellStyle.Format = "N0"
                    dgw.Columns(ColumnasNumericas(i)).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                Next
            End If
        Catch ex As Exception

        End Try

        tsslCantidad.Text = dgw.RowCount

    End Sub

    Sub ExistenciaDeposito()

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim ID As Integer = dgw.SelectedRows(0).Cells("ID").Value

        Dim SQL As String = "Select Sucursal, Deposito, Existencia From VExistenciaDeposito Where IDProducto=" & ID & " Order By Sucursal"
        dt = CSistema.ExecuteToDataTable(SQL)

        Dim frm As New Form
        frm.KeyPreview = True
        frm.Size = New Size(500, 200)

        Dim dgv As New DataGridView
        dgv.Name = "dgv"
        CSistema.dtToGrid(dgv, dt)
        frm.Controls.Add(dgv)
        dgv.Dock = DockStyle.Fill

        AddHandler frm.KeyUp, AddressOf FGCerrarFormulario
        AddHandler frm.Load, AddressOf InicializarExistenciaDeposito


        FGMostrarFormulario(Me, frm, "Existencia en Sucursales", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Sub InicializarExistenciaDeposito(ByVal sender As Object, ByVal e As System.EventArgs)


        For Each c As Control In sender.controls
            Dim dgv As DataGridView = CType(c, DataGridView)

            If dgv.Columns.Count > 0 Then

                'Formato
                dgv.Columns("Sucursal").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                dgv.Columns("Deposito").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

                dgv.Columns("Existencia").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                dgv.Columns("Existencia").DefaultCellStyle.Format = "N0"
                dgv.Columns("Existencia").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            End If

        Next

    End Sub

    Sub Marcar()

        If dgw.CurrentRow Is Nothing Then
            Exit Sub
        End If

        dtSeleccionado.Clear()

        For Each oRow As DataRow In dt.Rows
            If dgw.CurrentRow.Cells("ID").Value = oRow("ID") Then
                If dgw.CurrentRow.Cells("Sel").Value = False Then
                    oRow("Sel") = 1
                Else
                    oRow("Sel") = 0
                End If

            End If
        Next


        For Each dRow As DataRow In dt.Select("Sel= 1")

            Dim NewRow As DataRow = dtSeleccionado.NewRow

            For i As Integer = 0 To dtSeleccionado.Columns.Count - 1
                Dim NombreColumna As String = dtSeleccionado.Columns(i).ColumnName
                NewRow(NombreColumna) = dRow(NombreColumna)
            Next

            dtSeleccionado.Rows.Add(NewRow)

        Next

        
        Listar()

    End Sub

    Private Sub txtDescripcion_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDescripcion.KeyUp
        If e.KeyCode = Keys.Enter Then
            Listar()
        End If

    End Sub

    Private Sub frmProductoBuscar_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmProductoBuscar_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F2 Then

            If rdbDescripcion.Checked = True Then
                rdbCodigoBarra.Checked = True
                GoTo Siguiente
            End If

            If rdbCodigoBarra.Checked = True Then
                rdbReferencia.Checked = True
                GoTo Siguiente
            End If

            If rdbReferencia.Checked = True Then
                rdbID.Checked = True
                GoTo Siguiente
            End If

            If rdbID.Checked = True Then
                rdbDescripcion.Checked = True
                GoTo Siguiente
            End If

Siguiente:
            Listar()
            txtDescripcion.SelectAll()
            txtDescripcion.Focus()

        End If

        If e.KeyCode = Keys.Down Then
            If dgw.RowCount > 0 Then
                If dgw.SelectedRows.Count = 0 Then
                    dgw.SelectedRows(0).Selected = True
                End If

                dgw.Focus()
            Else
                txtDescripcion.SelectAll()
            End If
        End If

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Private Sub frmProductoBuscar_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CargarInformacion()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Space Then
            Marcar()
        End If

        If e.KeyCode = Keys.F1 Then
            ExistenciaDeposito()
        End If

    End Sub

    Private Sub ToolStripDropDownButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripDropDownButton1.Click
        ExistenciaDeposito()
    End Sub

    Private Sub btnAceptar_Click_1(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Me.Close()

    End Sub

    Private Sub dgw_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick
        Marcar()
    End Sub

    Private Sub btnCancelar_Click_1(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click

        dtSeleccionado.Reset()
        Me.Close()

    End Sub
End Class
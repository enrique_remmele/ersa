﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChoferProveedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.txtDireccion = New ERP.ocxTXTString()
        Me.lblDireccion = New System.Windows.Forms.Label()
        Me.txtCelular = New ERP.ocxTXTString()
        Me.lblCelular = New System.Windows.Forms.Label()
        Me.txtTelefono = New ERP.ocxTXTString()
        Me.lblTelefono = New System.Windows.Forms.Label()
        Me.txtNroDocumento = New ERP.ocxTXTString()
        Me.lblNroDocumento = New System.Windows.Forms.Label()
        Me.txtNombres = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lblNombres = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxCamion = New ERP.ocxCBX()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtEmail
        '
        Me.txtEmail.BackColor = System.Drawing.Color.White
        Me.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtEmail.Location = New System.Drawing.Point(98, 110)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(120, 20)
        Me.txtEmail.TabIndex = 113
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Location = New System.Drawing.Point(12, 113)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(35, 13)
        Me.lblEmail.TabIndex = 112
        Me.lblEmail.Text = "Email:"
        '
        'txtDireccion
        '
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Color = System.Drawing.Color.Empty
        Me.txtDireccion.Indicaciones = Nothing
        Me.txtDireccion.Location = New System.Drawing.Point(331, 83)
        Me.txtDireccion.Multilinea = False
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(241, 21)
        Me.txtDireccion.SoloLectura = False
        Me.txtDireccion.TabIndex = 111
        Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccion.Texto = ""
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.Location = New System.Drawing.Point(245, 87)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(55, 13)
        Me.lblDireccion.TabIndex = 110
        Me.lblDireccion.Text = "Direccion:"
        '
        'txtCelular
        '
        Me.txtCelular.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCelular.Color = System.Drawing.Color.Empty
        Me.txtCelular.Indicaciones = Nothing
        Me.txtCelular.Location = New System.Drawing.Point(98, 83)
        Me.txtCelular.Multilinea = False
        Me.txtCelular.Name = "txtCelular"
        Me.txtCelular.Size = New System.Drawing.Size(120, 21)
        Me.txtCelular.SoloLectura = False
        Me.txtCelular.TabIndex = 109
        Me.txtCelular.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCelular.Texto = ""
        '
        'lblCelular
        '
        Me.lblCelular.AutoSize = True
        Me.lblCelular.Location = New System.Drawing.Point(12, 87)
        Me.lblCelular.Name = "lblCelular"
        Me.lblCelular.Size = New System.Drawing.Size(42, 13)
        Me.lblCelular.TabIndex = 108
        Me.lblCelular.Text = "Celular:"
        '
        'txtTelefono
        '
        Me.txtTelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefono.Color = System.Drawing.Color.Empty
        Me.txtTelefono.Indicaciones = Nothing
        Me.txtTelefono.Location = New System.Drawing.Point(331, 55)
        Me.txtTelefono.Multilinea = False
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(120, 21)
        Me.txtTelefono.SoloLectura = False
        Me.txtTelefono.TabIndex = 107
        Me.txtTelefono.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTelefono.Texto = ""
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Location = New System.Drawing.Point(245, 59)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(52, 13)
        Me.lblTelefono.TabIndex = 106
        Me.lblTelefono.Text = "Telefono:"
        '
        'txtNroDocumento
        '
        Me.txtNroDocumento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroDocumento.Color = System.Drawing.Color.Empty
        Me.txtNroDocumento.Indicaciones = Nothing
        Me.txtNroDocumento.Location = New System.Drawing.Point(98, 55)
        Me.txtNroDocumento.Multilinea = False
        Me.txtNroDocumento.Name = "txtNroDocumento"
        Me.txtNroDocumento.Size = New System.Drawing.Size(120, 21)
        Me.txtNroDocumento.SoloLectura = False
        Me.txtNroDocumento.TabIndex = 105
        Me.txtNroDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroDocumento.Texto = ""
        '
        'lblNroDocumento
        '
        Me.lblNroDocumento.AutoSize = True
        Me.lblNroDocumento.Location = New System.Drawing.Point(12, 59)
        Me.lblNroDocumento.Name = "lblNroDocumento"
        Me.lblNroDocumento.Size = New System.Drawing.Size(88, 13)
        Me.lblNroDocumento.TabIndex = 104
        Me.lblNroDocumento.Text = "Nro. Documento:"
        '
        'txtNombres
        '
        Me.txtNombres.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombres.Color = System.Drawing.Color.Empty
        Me.txtNombres.Indicaciones = Nothing
        Me.txtNombres.Location = New System.Drawing.Point(98, 29)
        Me.txtNombres.Multilinea = False
        Me.txtNombres.Name = "txtNombres"
        Me.txtNombres.Size = New System.Drawing.Size(237, 21)
        Me.txtNombres.SoloLectura = False
        Me.txtNombres.TabIndex = 103
        Me.txtNombres.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNombres.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(98, 2)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 101
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 436)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(598, 22)
        Me.StatusStrip1.TabIndex = 124
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(422, 403)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 123
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(150, 171)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 116
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(12, 173)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 114
        Me.lblEstado.Text = "Estado:"
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(98, 171)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 115
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(341, 194)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 120
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(422, 194)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 121
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(260, 194)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 119
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(179, 194)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 118
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(98, 194)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 117
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lblNombres
        '
        Me.lblNombres.AutoSize = True
        Me.lblNombres.Location = New System.Drawing.Point(12, 33)
        Me.lblNombres.Name = "lblNombres"
        Me.lblNombres.Size = New System.Drawing.Size(52, 13)
        Me.lblNombres.TabIndex = 102
        Me.lblNombres.Text = "Nombres:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(12, 7)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 100
        Me.lblID.Text = "ID:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 140)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 125
        Me.Label1.Text = "Camion:"
        '
        'cbxCamion
        '
        Me.cbxCamion.CampoWhere = Nothing
        Me.cbxCamion.CargarUnaSolaVez = False
        Me.cbxCamion.DataDisplayMember = "Camion"
        Me.cbxCamion.DataFilter = Nothing
        Me.cbxCamion.DataOrderBy = "ID"
        Me.cbxCamion.DataSource = "VCamionProveedor"
        Me.cbxCamion.DataValueMember = "ID"
        Me.cbxCamion.dtSeleccionado = Nothing
        Me.cbxCamion.FormABM = Nothing
        Me.cbxCamion.Indicaciones = Nothing
        Me.cbxCamion.Location = New System.Drawing.Point(98, 140)
        Me.cbxCamion.Name = "cbxCamion"
        Me.cbxCamion.SeleccionMultiple = False
        Me.cbxCamion.SeleccionObligatoria = False
        Me.cbxCamion.Size = New System.Drawing.Size(353, 21)
        Me.cbxCamion.SoloLectura = False
        Me.cbxCamion.TabIndex = 126
        Me.cbxCamion.Texto = ""
        '
        'dgv
        '
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(98, 223)
        Me.dgv.Name = "dgv"
        Me.dgv.Size = New System.Drawing.Size(474, 174)
        Me.dgv.TabIndex = 127
        '
        'frmChoferProveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(598, 458)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.cbxCamion)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtEmail)
        Me.Controls.Add(Me.lblEmail)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.lblDireccion)
        Me.Controls.Add(Me.txtCelular)
        Me.Controls.Add(Me.lblCelular)
        Me.Controls.Add(Me.txtTelefono)
        Me.Controls.Add(Me.lblTelefono)
        Me.Controls.Add(Me.txtNroDocumento)
        Me.Controls.Add(Me.lblNroDocumento)
        Me.Controls.Add(Me.txtNombres)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.rdbDesactivado)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.rdbActivo)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.lblNombres)
        Me.Controls.Add(Me.lblID)
        Me.Name = "frmChoferProveedor"
        Me.Text = "frmChoferProveedor"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As ERP.ocxTXTString
    Friend WithEvents lblDireccion As System.Windows.Forms.Label
    Friend WithEvents txtCelular As ERP.ocxTXTString
    Friend WithEvents lblCelular As System.Windows.Forms.Label
    Friend WithEvents txtTelefono As ERP.ocxTXTString
    Friend WithEvents lblTelefono As System.Windows.Forms.Label
    Friend WithEvents txtNroDocumento As ERP.ocxTXTString
    Friend WithEvents lblNroDocumento As System.Windows.Forms.Label
    Friend WithEvents txtNombres As ERP.ocxTXTString
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents rdbDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents rdbActivo As System.Windows.Forms.RadioButton
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lblNombres As System.Windows.Forms.Label
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxCamion As ERP.ocxCBX
    Friend WithEvents dgv As DataGridView
End Class

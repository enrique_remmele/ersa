﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChequeraOpcionImprimir
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.cbxEstados = New ERP.ocxCBX()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxTipoInforme = New ERP.ocxCBX()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbxCantidad = New ERP.ocxCBX()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(169, 124)
        Me.Button1.Margin = New System.Windows.Forms.Padding(2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(85, 28)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Listar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(260, 124)
        Me.Button2.Margin = New System.Windows.Forms.Padding(2)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(85, 28)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Cerrar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'cbxEstados
        '
        Me.cbxEstados.CampoWhere = Nothing
        Me.cbxEstados.CargarUnaSolaVez = False
        Me.cbxEstados.DataDisplayMember = Nothing
        Me.cbxEstados.DataFilter = Nothing
        Me.cbxEstados.DataOrderBy = Nothing
        Me.cbxEstados.DataSource = Nothing
        Me.cbxEstados.DataValueMember = Nothing
        Me.cbxEstados.dtSeleccionado = Nothing
        Me.cbxEstados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxEstados.FormABM = Nothing
        Me.cbxEstados.Indicaciones = Nothing
        Me.cbxEstados.Location = New System.Drawing.Point(150, 40)
        Me.cbxEstados.Name = "cbxEstados"
        Me.cbxEstados.SeleccionMultiple = False
        Me.cbxEstados.SeleccionObligatoria = False
        Me.cbxEstados.Size = New System.Drawing.Size(195, 23)
        Me.cbxEstados.SoloLectura = False
        Me.cbxEstados.TabIndex = 2
        Me.cbxEstados.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(9, 42)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(134, 15)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Estado de los cheques:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(9, 13)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 15)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Tipo de Informe:"
        '
        'cbxTipoInforme
        '
        Me.cbxTipoInforme.CampoWhere = Nothing
        Me.cbxTipoInforme.CargarUnaSolaVez = False
        Me.cbxTipoInforme.DataDisplayMember = Nothing
        Me.cbxTipoInforme.DataFilter = Nothing
        Me.cbxTipoInforme.DataOrderBy = Nothing
        Me.cbxTipoInforme.DataSource = Nothing
        Me.cbxTipoInforme.DataValueMember = Nothing
        Me.cbxTipoInforme.dtSeleccionado = Nothing
        Me.cbxTipoInforme.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxTipoInforme.FormABM = Nothing
        Me.cbxTipoInforme.Indicaciones = Nothing
        Me.cbxTipoInforme.Location = New System.Drawing.Point(150, 11)
        Me.cbxTipoInforme.Name = "cbxTipoInforme"
        Me.cbxTipoInforme.SeleccionMultiple = False
        Me.cbxTipoInforme.SeleccionObligatoria = False
        Me.cbxTipoInforme.Size = New System.Drawing.Size(195, 23)
        Me.cbxTipoInforme.SoloLectura = False
        Me.cbxTipoInforme.TabIndex = 4
        Me.cbxTipoInforme.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(10, 73)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(119, 15)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Cantidad disponible:"
        '
        'cbxCantidad
        '
        Me.cbxCantidad.CampoWhere = Nothing
        Me.cbxCantidad.CargarUnaSolaVez = False
        Me.cbxCantidad.DataDisplayMember = Nothing
        Me.cbxCantidad.DataFilter = Nothing
        Me.cbxCantidad.DataOrderBy = Nothing
        Me.cbxCantidad.DataSource = Nothing
        Me.cbxCantidad.DataValueMember = Nothing
        Me.cbxCantidad.dtSeleccionado = Nothing
        Me.cbxCantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCantidad.FormABM = Nothing
        Me.cbxCantidad.Indicaciones = Nothing
        Me.cbxCantidad.Location = New System.Drawing.Point(151, 71)
        Me.cbxCantidad.Name = "cbxCantidad"
        Me.cbxCantidad.SeleccionMultiple = False
        Me.cbxCantidad.SeleccionObligatoria = False
        Me.cbxCantidad.Size = New System.Drawing.Size(195, 23)
        Me.cbxCantidad.SoloLectura = False
        Me.cbxCantidad.TabIndex = 6
        Me.cbxCantidad.Texto = ""
        '
        'frmChequeraOpcionImprimir
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(356, 166)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cbxCantidad)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbxTipoInforme)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbxEstados)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frmChequeraOpcionImprimir"
        Me.Text = "Opciones de impresión"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents cbxEstados As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxTipoInforme As ERP.ocxCBX
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbxCantidad As ERP.ocxCBX
End Class

﻿Imports ERP.Reporte
Public Class frmChequera
    'CLASES
    Dim CSistema As New CSistema
    Dim CReporte As New CReporteChequeras
    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Formulario
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Focus
        dgv.Focus()

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxBanco)
        CSistema.CargaControl(vControles, txtChequera)
        CSistema.CargaControl(vControles, rdbActivo)
        CSistema.CargaControl(vControles, rdbDesactivado)
        CSistema.CargaControl(vControles, cbxCuentaBancaria)
        CSistema.CargaControl(vControles, txtDesde)
        CSistema.CargaControl(vControles, txtHasta)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtUltimo)


        'Cargar combobox
        CSistema.SqlToComboBox(cbxBanco, "Select ID, Descripcion from Banco Order By 2")

        'Modena
        CSistema.SqlToComboBox(cbxCuentaBancaria, "Select ID, CuentaBancaria from CuentaBancaria Order By 2")


        'Cargamos los registos en el lv
        Listar()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        'Con este metodo "SqlToDataGrid" el sistema carga automaticamente la consulta SQL en el DataGrid asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el DataGrid.
        CSistema.SqlToDataGrid(dgv, "Select  ID, ReferenciaBanco as Banco, CuentaBancaria, NroComprobante,NroDesde, NroHasta, AUtilizar, 'Estado'=(Case When (Estado) = 'True' Then 'OK' Else '-' End) from vChequera Order By 1")
        dgv.ReadOnly = True


    End Sub

    Sub InicializarControles()

        'TextBox
        txtDesde.txt.Clear()
        txtHasta.txt.Clear()


        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Chequera"), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Chequera"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        txtChequera.Focus()


    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgv.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgv, mensaje)
                ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtChequera.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDBanco", cbxBanco.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCuentaBancaria", cbxCuentaBancaria.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroDesde", Replace(txtDesde.txt.Text, ".", ""), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroHasta", Replace(txtHasta.txt.Text, ".", ""), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Ultimo", Replace(txtUltimo.txt.Text, ".", ""), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpChequera", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            Listar(txtID.txt.Text)
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno

            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If dgv.SelectedRows.Count = 0 Then

            ctrError.SetError(dgv, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = dgv.SelectedRows(0).Cells("ID").Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VChequera Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow

            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            cbxBanco.cbx.Text = oRow("DescripcionBanco").ToString
            txtChequera.txt.Text = oRow("NroComprobante").ToString
            cbxCuentaBancaria.cbx.Text = oRow("CuentaBancaria").ToString
            txtDesde.txt.Text = oRow("NroDesde").ToString
            txtHasta.txt.Text = oRow("NroHasta").ToString
            txtObservacion.txt.Text = oRow("Observacion").ToString
            txtUltimo.txt.Text = oRow("Autilizar").ToString

            If CBool(oRow("Estado")) = True Then
                rdbActivo.Checked = True
            Else
                rdbDesactivado.Checked = True
            End If


            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)


        End If

        ctrError.Clear()

    End Sub

    Sub MostrarOpciones()

        Dim frm As New frmChequeraOpcionImprimir
        Try
            frm.Id = txtID.txt.Text
            frm.CuentaBancaria = cbxCuentaBancaria.GetValue
            frm.Desde = txtDesde.txt.Text
            frm.Hasta = txtHasta.txt.Text
            frm.Banco = cbxBanco.cbx.Text
        Catch ex As Exception

        End Try

        FGMostrarFormulario(Me, frm, "Opciones de impresión", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub
    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(sender As System.Object, e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        txtChequera.Focus()
        txtChequera.txt.SelectAll()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub dgv_Click(sender As System.Object, e As System.EventArgs) Handles dgv.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub frmChequera_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmChequera_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub cbxBanco_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxBanco.PropertyChanged
        'Listar Departamentos
        If IsNumeric(cbxBanco.cbx.SelectedValue) = False Then
            Exit Sub
        End If
        cbxCuentaBancaria.cbx.Text = ""
        CSistema.SqlToComboBox(cbxCuentaBancaria.cbx, "Select ID, CuentaBancaria From CuentaBancaria Where IDBanco=" & cbxBanco.cbx.SelectedValue & " Order By Cuentabancaria Asc")
    End Sub

    Private Sub btnListar_Click(sender As System.Object, e As System.EventArgs) Handles btnListar.Click
        MostrarOpciones()
    End Sub

    Private Sub cbxBanco_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxBanco.TeclaPrecionada
        'Listar Departamentos
        If IsNumeric(cbxBanco.cbx.SelectedValue) = False Then
            Exit Sub
        End If
        cbxCuentaBancaria.cbx.Text = ""
        CSistema.SqlToComboBox(cbxCuentaBancaria.cbx, "Select ID, CuentaBancaria From CuentaBancaria Where IDBanco=" & cbxBanco.cbx.SelectedValue & " Order By Cuentabancaria Asc")
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        CargarInformacion()
    End Sub
End Class
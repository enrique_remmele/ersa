﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProveedorBuscar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.rdbRazonSocial = New System.Windows.Forms.RadioButton()
        Me.rdbNombreFantasia = New System.Windows.Forms.RadioButton()
        Me.rdbRUC = New System.Windows.Forms.RadioButton()
        Me.rdbReferencia = New System.Windows.Forms.RadioButton()
        Me.lvlista = New System.Windows.Forms.ListView()
        Me.chkSusursals = New System.Windows.Forms.CheckBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsslCantidad = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.cbxSucursals = New System.Windows.Forms.ComboBox()
        Me.chkActivo = New ERP.ocxCHK()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(4, 4)
        Me.txtDescripcion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(207, 22)
        Me.txtDescripcion.TabIndex = 0
        '
        'rdbRazonSocial
        '
        Me.rdbRazonSocial.AutoSize = True
        Me.rdbRazonSocial.Location = New System.Drawing.Point(219, 4)
        Me.rdbRazonSocial.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbRazonSocial.Name = "rdbRazonSocial"
        Me.rdbRazonSocial.Size = New System.Drawing.Size(112, 21)
        Me.rdbRazonSocial.TabIndex = 1
        Me.rdbRazonSocial.TabStop = True
        Me.rdbRazonSocial.Text = "Razon Social"
        Me.rdbRazonSocial.UseVisualStyleBackColor = True
        '
        'rdbNombreFantasia
        '
        Me.rdbNombreFantasia.AutoSize = True
        Me.rdbNombreFantasia.Location = New System.Drawing.Point(398, 4)
        Me.rdbNombreFantasia.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbNombreFantasia.Name = "rdbNombreFantasia"
        Me.rdbNombreFantasia.Size = New System.Drawing.Size(157, 21)
        Me.rdbNombreFantasia.TabIndex = 3
        Me.rdbNombreFantasia.TabStop = True
        Me.rdbNombreFantasia.Text = "Nombre de Fantasia"
        Me.rdbNombreFantasia.UseVisualStyleBackColor = True
        '
        'rdbRUC
        '
        Me.rdbRUC.AutoSize = True
        Me.rdbRUC.Location = New System.Drawing.Point(563, 4)
        Me.rdbRUC.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbRUC.Name = "rdbRUC"
        Me.rdbRUC.Size = New System.Drawing.Size(58, 21)
        Me.rdbRUC.TabIndex = 4
        Me.rdbRUC.TabStop = True
        Me.rdbRUC.Text = "RUC"
        Me.rdbRUC.UseVisualStyleBackColor = True
        '
        'rdbReferencia
        '
        Me.rdbReferencia.AutoSize = True
        Me.rdbReferencia.Location = New System.Drawing.Point(339, 4)
        Me.rdbReferencia.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbReferencia.Name = "rdbReferencia"
        Me.rdbReferencia.Size = New System.Drawing.Size(51, 21)
        Me.rdbReferencia.TabIndex = 2
        Me.rdbReferencia.TabStop = True
        Me.rdbReferencia.Text = "Ref"
        Me.rdbReferencia.UseVisualStyleBackColor = True
        '
        'lvlista
        '
        Me.lvlista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvlista.GridLines = True
        Me.lvlista.Location = New System.Drawing.Point(4, 41)
        Me.lvlista.Margin = New System.Windows.Forms.Padding(4)
        Me.lvlista.Name = "lvlista"
        Me.lvlista.Size = New System.Drawing.Size(1320, 331)
        Me.lvlista.TabIndex = 1
        Me.lvlista.UseCompatibleStateImageBehavior = False
        Me.lvlista.View = System.Windows.Forms.View.Details
        '
        'chkSusursals
        '
        Me.chkSusursals.AutoSize = True
        Me.chkSusursals.Location = New System.Drawing.Point(629, 4)
        Me.chkSusursals.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSusursals.Name = "chkSusursals"
        Me.chkSusursals.Size = New System.Drawing.Size(89, 21)
        Me.chkSusursals.TabIndex = 5
        Me.chkSusursals.Text = "Sucursal:"
        Me.chkSusursals.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(874, 4)
        Me.btnAceptar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(100, 26)
        Me.btnAceptar.TabIndex = 7
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lvlista, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(4)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1328, 376)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtDescripcion)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbRazonSocial)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbReferencia)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbNombreFantasia)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbRUC)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkSusursals)
        Me.FlowLayoutPanel1.Controls.Add(Me.cbxSucursals)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkActivo)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(4, 4)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1320, 29)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'btnCancelar
        '
        Me.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancelar.Location = New System.Drawing.Point(982, 4)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(100, 26)
        Me.btnCancelar.TabIndex = 6
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(72, 20)
        Me.ToolStripStatusLabel1.Text = "Cantidad:"
        '
        'tsslCantidad
        '
        Me.tsslCantidad.Name = "tsslCantidad"
        Me.tsslCantidad.Size = New System.Drawing.Size(17, 20)
        Me.tsslCantidad.Text = "0"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.tsslCantidad})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 376)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(1328, 25)
        Me.StatusStrip1.TabIndex = 3
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'cbxSucursals
        '
        Me.cbxSucursals.FormattingEnabled = True
        Me.cbxSucursals.Location = New System.Drawing.Point(725, 3)
        Me.cbxSucursals.Name = "cbxSucursals"
        Me.cbxSucursals.Size = New System.Drawing.Size(142, 24)
        Me.cbxSucursals.TabIndex = 12
        '
        'chkActivo
        '
        Me.chkActivo.BackColor = System.Drawing.Color.Transparent
        Me.chkActivo.Color = System.Drawing.Color.Empty
        Me.chkActivo.Location = New System.Drawing.Point(1106, 4)
        Me.chkActivo.Margin = New System.Windows.Forms.Padding(20, 4, 4, 4)
        Me.chkActivo.Name = "chkActivo"
        Me.chkActivo.Size = New System.Drawing.Size(128, 21)
        Me.chkActivo.SoloLectura = False
        Me.chkActivo.TabIndex = 11
        Me.chkActivo.Texto = "Sólo Activos:"
        Me.chkActivo.Valor = False
        '
        'frmProveedorBuscar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1328, 401)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmProveedorBuscar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmProveedorBuscar"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents rdbRazonSocial As System.Windows.Forms.RadioButton
    Friend WithEvents rdbNombreFantasia As System.Windows.Forms.RadioButton
    Friend WithEvents rdbRUC As System.Windows.Forms.RadioButton
    Friend WithEvents rdbReferencia As System.Windows.Forms.RadioButton
    Friend WithEvents lvlista As System.Windows.Forms.ListView
    Friend WithEvents chkSusursals As System.Windows.Forms.CheckBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsslCantidad As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents chkActivo As ERP.ocxCHK
    Friend WithEvents chkSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents cbxSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents cbxSucursals As System.Windows.Forms.ComboBox
End Class

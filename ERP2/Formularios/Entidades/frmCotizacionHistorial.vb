﻿Imports ERP.Reporte
Public Class frmCotizacionHistorial

    Public IdMoneda As String
    Public Moneda As String
    Dim CReporte As New CReporteCotizacion
    Dim CSistema As New CSistema

    Private Sub CargarInformacion()
        CSistema.SqlToComboBox(cbxMoneda, "Select ID, Descripcion From VMoneda Where ID<>1 union select 'ID' = '0','Descripcion' = 'TODAS' Order By Descripcion")
        cbxMoneda.cbx.SelectedValue = IdMoneda

    End Sub

    Private Sub ListarInforme()

        Dim Where As String = "Where Fecha Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "' " & IIf(cbxMoneda.cbx.SelectedValue <> 0, " And IdMoneda = '" & cbxMoneda.cbx.SelectedValue & "'", "")
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Subtitulo As String = " DESDE " & txtFechaDesde.GetValueString & " HASTA " & txtFechaHasta.GetValueString & " MONEDA: " & cbxMoneda.txt.Text
        Dim Titulo As String = "COTIZACION DE MONEDA "
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        CReporte.ListarCotizacion(frm, Where, "", vgUsuarioIdentificador, Titulo, Subtitulo, "", True)
    End Sub

    Private Sub frmCotizacionHistorial_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        CargarInformacion()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        ListarInforme()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Close()
    End Sub

    Private Sub frmCotizacionHistorial_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Down Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Or e.KeyCode = Keys.Up Then
            CSistema.SelectNextControl(Me, e.KeyCode)
        End If
    End Sub
End Class
﻿Public Class frmPaisDepartamentoCiudadBarrios

    'CLASES
    Dim CSistema As New CSistema

#Region "PAIS"

    'VARIABLES
    Dim vPaisNuevo As Boolean
    Dim vControlesPais() As Control

    'FUNCIONES
    Sub InicializarPais()

        'TextBox
        txtPaisDescripcion.Clear()
        txtPaisNacionalidad.Clear()

        'ListView
        lvPaisLista.Clear()

        'Variables
        vPaisNuevo = False

        'RadioButton
        rdbPaisActivo.Checked = True

        'Funciones
        CargarInformacionPais()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnPaisNuevo, btnPaisEditar, btnPaisCancelar, btnPaisGuardar, btnPaisEliminar, vControlesPais)

        'Focus
        lvPaisLista.Focus()

    End Sub

    Sub CargarInformacionPais()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControlesPais(3)
        vControlesPais(0) = txtPaisDescripcion
        vControlesPais(1) = txtPaisNacionalidad
        vControlesPais(2) = rdbPaisActivo
        vControlesPais(3) = rdbPaisDesactivado

        'Cargamos los paises en el lv
        ListarPais()

    End Sub

    Sub ObtenerInformacionPais()

        'Validar
        'Si es que se selecciono el registro.
        If lvPaisLista.SelectedItems.Count = 0 Then
            ctrError.SetError(lvPaisLista, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(lvPaisLista, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnPaisNuevo, btnPaisEditar, btnPaisCancelar, btnPaisGuardar, btnPaisEliminar, vControlesPais)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = lvPaisLista.SelectedItems(0).SubItems(1).Text

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select ID, Descripcion, Nacionalidad, Estado From Pais Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtPaisID.Text = oRow("ID").ToString
            txtPaisDescripcion.Text = oRow("Descripcion").ToString
            txtPaisNacionalidad.Text = oRow("Nacionalidad").ToString

            If CBool(oRow("Estado")) = True Then
                rdbPaisActivo.Checked = True
            Else
                rdbPaisDesactivado.Checked = True
            End If

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnPaisNuevo, btnPaisEditar, btnPaisCancelar, btnPaisGuardar, btnPaisEliminar, vControlesPais)

        End If

        ctrError.Clear()

    End Sub

    Sub ListarPais(Optional ByVal IDPais As Integer = 0)

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        CSistema.SqlToLv(lvPaisLista, "Select 'Pais'=Descripcion, ID, Nacionalidad From Pais Order By 1")

        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If lvPaisLista.Columns.Count > 0 Then

            'Esto hacemos para que: 
            '1- Que el ID sea visible en la primera columna
            '2- Y para que cuando el usuario escriba en el lv, el control filtre por su descripcion.
            lvPaisLista.Columns(0).DisplayIndex = 1

            'Ahora seleccionamos automaticamente el registro especificado
            If IDPais = 0 Then
                'lvPaisLista.Items(0).Selected = True
            Else
                For i As Integer = 0 To lvPaisLista.Items.Count - 1
                    If lvPaisLista.Items(i).SubItems(1).Text = IDPais Then
                        lvPaisLista.Items(i).Selected = True
                        Exit For
                    End If
                Next

            End If

        End If


        lvPaisLista.Refresh()

    End Sub

    Sub InicializarControlesPais()

        'TextBox
        txtPaisDescripcion.Clear()
        txtPaisNacionalidad.Clear()

        'RadioButton
        rdbPaisActivo.Checked = True

        'ToolStripStatusLabel
        tsslEstado.Text = ""

        'Funciones
        If Me.vPaisNuevo = True Then
            txtPaisID.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Pais"), Integer)
        Else
            ListarPais(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Pais"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        txtPaisDescripcion.Focus()


    End Sub

    Sub ProcesarPais(ByVal Operacion As CSistema.NUMOperacionesABM)

        'Validar
        'Escritura de la Descripcion
        If txtPaisDescripcion.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar una descripcion valida!"
            ctrError.SetError(txtPaisDescripcion, mensaje)
            ctrError.SetIconAlignment(txtPaisDescripcion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If lvPaisLista.SelectedItems.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(lvPaisLista, mensaje)
                ctrError.SetIconAlignment(lvPaisLista, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtPaisID.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(6) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        param(0) = CSistema.SetSQLParameter("@ID", ID, ParameterDirection.Input)
        param(1) = CSistema.SetSQLParameter("@Descripcion", txtPaisDescripcion.Text.Trim, ParameterDirection.Input)
        param(2) = CSistema.SetSQLParameter("@Nacionalidad", txtPaisNacionalidad.Text.Trim, ParameterDirection.Input)
        param(3) = CSistema.SetSQLParameter("@Operacion", Operacion.ToString, ParameterDirection.Input)
        param(4) = CSistema.SetSQLParameter("@Mensaje", "", ParameterDirection.Output, 200)
        param(5) = CSistema.SetSQLParameter("@Procesado", "", ParameterDirection.Output)
        param(6) = CSistema.SetSQLParameter("@Estado", rdbPaisActivo.Checked.ToString, ParameterDirection.Input)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpPais", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnPaisNuevo, btnPaisEditar, btnPaisCancelar, btnPaisGuardar, btnPaisEliminar, vControlesPais)
            ListarPais(txtPaisID.Text)
            ctrError.Clear()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnPaisGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnPaisGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnPaisNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPaisNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnPaisNuevo, btnPaisEditar, btnPaisCancelar, btnPaisGuardar, btnPaisEliminar, vControlesPais)
        vPaisNuevo = True
        InicializarControlesPais()
    End Sub

    Private Sub btnPaisEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPaisEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnPaisNuevo, btnPaisEditar, btnPaisCancelar, btnPaisGuardar, btnPaisEliminar, vControlesPais)

        vPaisNuevo = False

        'Foco
        txtPaisDescripcion.Focus()
        txtPaisDescripcion.SelectAll()

    End Sub

    Private Sub btnPaisCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPaisCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnPaisNuevo, btnPaisEditar, btnPaisCancelar, btnPaisGuardar, btnPaisEliminar, vControlesPais)
        vPaisNuevo = False
        InicializarControlesPais()
        ObtenerInformacionPais()
    End Sub

    Private Sub btnPaisGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPaisGuardar.Click
        If vPaisNuevo = True Then
            ProcesarPais(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            ProcesarPais(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnPaisEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPaisEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnPaisNuevo, btnPaisEditar, btnPaisCancelar, btnPaisGuardar, btnPaisEliminar, vControlesPais)
        ProcesarPais(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub lvPaisLista_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvPaisLista.Click
        ObtenerInformacionPais()
    End Sub

    Private Sub lvPaisLista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvPaisLista.SelectedIndexChanged
        ObtenerInformacionPais()
    End Sub

#End Region

#Region "DEPARTAMENTO"

    'VARIABLES
    Dim vDepartamentoNuevo As Boolean
    Dim vControlesDepartamento() As Control

    'FUNCIONES
    Sub InicializarDepartamento()

        'TextBox
        txtDepartamentoDescripcion.Clear()
        'txtCodigoUnilever.Clear()

        'ListView
        lvDepartamentos.Clear()

        'Variables
        vDepartamentoNuevo = False

        'RadioButton
        rdbDepartamentoActivo.Checked = True

        'Funciones
        CargarInformacionDepartamento()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnDepartamentoNuevo, btnDepartamentoEditar, btnDepartamentoCancelar, btnDepartamentoGuardar, btnDepartamentoEliminar, vControlesDepartamento)

        'Focus
        lvDepartamentos.Focus()

    End Sub

    Sub CargarInformacionDepartamento()

        'Cargamos los paises
        CSistema.SqlToComboBox(cbxDepartamentoPais, "Select ID, Descripcion From Pais Order By Orden Desc, 2")

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControlesDepartamento(4)
        vControlesDepartamento(0) = txtDepartamentoDescripcion
        vControlesDepartamento(1) = cbxDepartamentoPais
        vControlesDepartamento(2) = rdbDepartamentoActivo
        vControlesDepartamento(3) = rdbDepartamentoDesactivado
        vControlesDepartamento(4) = txtCodigo
        'vControlesDepartamento(4) = txtCodigoUnilever
        'Cargamos los paises en el lv
        ListarDepartamentos()

    End Sub

    Sub ObtenerInformacionDepartamento()

        'Validar
        'Si es que se selecciono el registro.
        If lvDepartamentos.SelectedItems.Count = 0 Then
            ctrError.SetError(lvDepartamentos, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(lvDepartamentos, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnDepartamentoNuevo, btnDepartamentoEditar, btnDepartamentoCancelar, btnDepartamentoGuardar, btnDepartamentoEliminar, vControlesDepartamento)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = lvDepartamentos.SelectedItems(0).SubItems(1).Text

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        'dt = CSistema.ExecuteToDataTable("Select ID, Descripcion, IDPais, CodigoUnilever, Estado  From Departamento Where ID=" & ID)
        dt = CSistema.ExecuteToDataTable("Select ID, Descripcion, IDPais, Estado, Codigo From Departamento Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtDepartamentoID.Text = oRow("ID").ToString
            txtDepartamentoDescripcion.Text = oRow("Descripcion").ToString
            cbxDepartamentoPais.SelectedValue = oRow("IDPais").ToString
            'txtCodigoUnilever.Text = oRow("CodigoUnilever").ToString
            txtCodigo.Text = oRow("Codigo").ToString

            If CBool(oRow("Estado")) = True Then
                rdbDepartamentoActivo.Checked = True
            Else
                rdbDepartamentoDesactivado.Checked = True
            End If

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnDepartamentoNuevo, btnDepartamentoEditar, btnDepartamentoCancelar, btnDepartamentoGuardar, btnDepartamentoEliminar, vControlesDepartamento)

        End If

        ctrError.Clear()

    End Sub

    Sub ListarDepartamentos(Optional ByVal IDDepartamento As Integer = 0)

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        'CSistema.SqlToLv(lvDepartamentos, "Select 'Departamento'=D.Descripcion, D.ID, D.CodigoUnilever, 'Pais'=P.Descripcion  From Departamento D Join Pais P On D.IDPais=P.ID Order By P.Orden Desc, P.Descripcion, D.Descripcion")
        CSistema.SqlToLv(lvDepartamentos, "Select 'Departamento'=D.Descripcion, D.ID, 'Pais'=P.Descripcion, D.Codigo From Departamento D Join Pais P On D.IDPais=P.ID Order By P.Orden Desc, P.Descripcion, D.Descripcion")

        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If lvDepartamentos.Columns.Count > 0 Then

            'Esto hacemos para que: 
            '1- Que el ID sea visible en la primera columna
            '2- Y para que cuando el usuario escriba en el lv, el control filtre por su descripcion.
            lvDepartamentos.Columns(0).DisplayIndex = 1

            'Ahora seleccionamos automaticamente el registro especificado
            If IDDepartamento = 0 Then
                'lvDepartamentos.Items(0).Selected = True
            Else
                For i As Integer = 0 To lvDepartamentos.Items.Count - 1
                    If lvDepartamentos.Items(i).SubItems(1).Text = IDDepartamento Then
                        lvDepartamentos.Items(i).Selected = True
                        Exit For
                    End If
                Next

            End If

        End If


        lvDepartamentos.Refresh()

    End Sub

    Sub InicializarControlesDepartamento()

        'TextBox
        txtDepartamentoDescripcion.Clear()
        txtCodigo.Clear()
        'txtCodigoUnilever.Clear()

        'RadioButton
        rdbDepartamentoActivo.Checked = True

        'ToolStripStatusLabel
        tsslEstado.Text = ""

        'Funciones
        If Me.vDepartamentoNuevo = True Then
            txtDepartamentoID.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Departamento"), Integer)
        Else
            ListarDepartamentos(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Departamento"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        txtDepartamentoDescripcion.Focus()


    End Sub

    Sub ProcesarDepartamento(ByVal Operacion As CSistema.NUMOperacionesABM)

        'Validar
        'Si es que selecciono correctamente el Pais
        If IsNumeric(cbxDepartamentoPais.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el pais al que corresponde!"
            ctrError.SetError(cbxDepartamentoPais, mensaje)
            ctrError.SetIconAlignment(cbxDepartamentoPais, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Escritura de la Descripcion
        If txtDepartamentoDescripcion.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar una descripcion valida!"
            ctrError.SetError(txtPaisDescripcion, mensaje)
            ctrError.SetIconAlignment(txtPaisDescripcion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If lvDepartamentos.SelectedItems.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(lvDepartamentos, mensaje)
                ctrError.SetIconAlignment(lvDepartamentos, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtDepartamentoID.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(7) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        param(0) = CSistema.SetSQLParameter("@ID", ID, ParameterDirection.Input)
        param(1) = CSistema.SetSQLParameter("@Descripcion", txtDepartamentoDescripcion.Text.Trim, ParameterDirection.Input)
        param(2) = CSistema.SetSQLParameter("@IDPais", cbxDepartamentoPais.SelectedValue, ParameterDirection.Input)
        param(3) = CSistema.SetSQLParameter("@Codigo", txtCodigo.Text, ParameterDirection.Input)
        param(4) = CSistema.SetSQLParameter("@Operacion", Operacion.ToString, ParameterDirection.Input)
        param(5) = CSistema.SetSQLParameter("@Mensaje", "", ParameterDirection.Output, 200)
        param(6) = CSistema.SetSQLParameter("@Procesado", "", ParameterDirection.Output)
        'param(6) = CSistema.SetSQLParameter("@CodigoUnilever", txtCodigoUnilever.Text.Trim, ParameterDirection.Input)
        param(7) = CSistema.SetSQLParameter("@Estado", rdbDepartamentoActivo.Checked.ToString, ParameterDirection.Input)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpDepartamento", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnDepartamentoNuevo, btnPaisEditar, btnDepartamentoCancelar, btnDepartamentoGuardar, btnDepartamentoEliminar, vControlesDepartamento)
            ListarDepartamentos(txtDepartamentoID.Text)
            ctrError.Clear()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnDepartamentoGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnDepartamentoGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnDepartamentoNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDepartamentoNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnDepartamentoNuevo, btnDepartamentoEditar, btnDepartamentoCancelar, btnDepartamentoGuardar, btnDepartamentoEliminar, vControlesDepartamento)
        vDepartamentoNuevo = True
        InicializarControlesDepartamento()
    End Sub

    Private Sub btnDepartamentoEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDepartamentoEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnDepartamentoNuevo, btnDepartamentoEditar, btnDepartamentoCancelar, btnDepartamentoGuardar, btnDepartamentoEliminar, vControlesDepartamento)

        vDepartamentoNuevo = False

        'Foco
        txtDepartamentoDescripcion.Focus()
        txtDepartamentoDescripcion.SelectAll()

    End Sub

    Private Sub btnDepartamentoCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDepartamentoCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnDepartamentoNuevo, btnDepartamentoEditar, btnDepartamentoCancelar, btnDepartamentoGuardar, btnDepartamentoEliminar, vControlesDepartamento)
        vDepartamentoNuevo = False
        InicializarControlesDepartamento()
        ObtenerInformacionDepartamento()
    End Sub

    Private Sub btnDepartamentoGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDepartamentoGuardar.Click
        If vDepartamentoNuevo = True Then
            ProcesarDepartamento(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            ProcesarDepartamento(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnDepartamentoEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDepartamentoEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnDepartamentoNuevo, btnDepartamentoEditar, btnDepartamentoCancelar, btnDepartamentoGuardar, btnDepartamentoEliminar, vControlesDepartamento)
        ProcesarDepartamento(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub lvDepartamentos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvDepartamentos.Click
        ObtenerInformacionDepartamento()
    End Sub

    Private Sub lvDepartamentos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvDepartamentos.SelectedIndexChanged
        ObtenerInformacionDepartamento()
    End Sub

#End Region

#Region "CIUDAD"

    'VARIABLES
    Dim vCiudadNuevo As Boolean
    Dim vControlesCiudad() As Control

    'FUNCIONES
    Sub InicializarCiudad()

        'TextBox
        txtCiudadDescripcion.txt.Clear()
        'txtCiudadCodigoUnilever.txt.Clear()

        'ListView
        lvCiudades.Clear()

        'Variables
        vCiudadNuevo = False

        'RadioButton
        rdbCiudadActivo.Checked = True

        'Funciones
        CargarInformacionCiudad()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnCiudadNuevo, btnCiudadEditar, btnCiudadCancelar, btnCiudadGuardar, btnCiudadEliminar, vControlesCiudad)

        'Focus
        lvCiudades.Focus()

    End Sub

    Sub CargarInformacionCiudad()

        'Cargamos los paises
        CSistema.SqlToComboBox(cbxCiudadPais.cbx, "Select ID, Descripcion From Pais Order By Orden Desc, 2")

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControlesCiudad(-1)
        CSistema.CargaControl(vControlesCiudad, txtCiudadDescripcion)
        CSistema.CargaControl(vControlesCiudad, txtCiudadCodigo)
        CSistema.CargaControl(vControlesCiudad, cbxCiudadPais)
        CSistema.CargaControl(vControlesCiudad, cbxCiudadDepartamento)
        CSistema.CargaControl(vControlesCiudad, rdbCiudadActivo)
        CSistema.CargaControl(vControlesCiudad, rdbCiudadDesactivado)
        CSistema.CargaControl(vControlesCiudad, txtCodigoSet)
        'CSistema.CargaControl(vControlesCiudad, txtCiudadCodigoUnilever)

        'Cargamos los paises en el lv
        ListarCiudads()

    End Sub

    Sub ObtenerInformacionCiudad()

        'Validar
        'Si es que se selecciono el registro.
        If lvCiudades.SelectedItems.Count = 0 Then
            'ctrError.SetError(lvCiudades, "Seleccione correctamente un registro!")
            'ctrError.SetIconAlignment(lvCiudades, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            'CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnCiudadNuevo, btnCiudadEditar, btnCiudadCancelar, btnCiudadGuardar, btnCiudadEliminar, vControlesCiudad)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = lvCiudades.SelectedItems(0).SubItems(1).Text

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        'dt = CSistema.ExecuteToDataTable("Select ID, Descripcion, IDPais, IDDepartamento, Codigo, CodigoUnilever, Estado  From Ciudad Where ID=" & ID)
        dt = CSistema.ExecuteToDataTable("Select ID, Descripcion, IDPais, IDDepartamento, Codigo, Estado, CodigoSET  From Ciudad Where ID=" & ID)
        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtCiudadID.Text = oRow("ID").ToString
            txtCiudadDescripcion.txt.Text = oRow("Descripcion").ToString
            txtCiudadCodigo.txt.Text = oRow("Codigo").ToString
            'txtCiudadCodigoUnilever.txt.Text = oRow("CodigoUnilever").ToString
            cbxCiudadPais.cbx.SelectedValue = oRow("IDPais").ToString
            cbxCiudadDepartamento.cbx.SelectedValue = oRow("IDDepartamento").ToString

            txtCodigoSet.txt.Text = oRow("CodigoSET").ToString

            If CBool(oRow("Estado")) = True Then
                rdbCiudadActivo.Checked = True
            Else
                rdbCiudadDesactivado.Checked = True
            End If

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnCiudadNuevo, btnCiudadEditar, btnCiudadCancelar, btnCiudadGuardar, btnCiudadEliminar, vControlesCiudad)

        End If

        ctrError.Clear()

    End Sub

    Sub ListarCiudads(Optional ByVal IDCiudad As Integer = 0)

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        'CSistema.SqlToLv(lvCiudades, "Select 'Ciudad'=C.Descripcion, C.ID, C.Codigo, C.CodigoUnilever, 'Departamento'=D.Descripcion, 'Pais'=P.Descripcion  From Ciudad C Join Departamento D On C.IDDepartamento=D.ID Join Pais P On C.IDPais=P.ID Order By P.Orden Desc, D.Orden Desc, P.Descripcion, D.Descripcion, C.Descripcion")
        CSistema.SqlToLv(lvCiudades, "Select 'Ciudad'=C.Descripcion, C.ID, C.Codigo, 'Departamento'=D.Descripcion, 'Pais'=P.Descripcion, CodigoSET  From Ciudad C Join Departamento D On C.IDDepartamento=D.ID Join Pais P On C.IDPais=P.ID Order By P.Orden Desc, D.Orden Desc, P.Descripcion, D.Descripcion, C.Descripcion")

        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If lvCiudades.Columns.Count > 0 Then

            'Esto hacemos para que: 
            '1- Que el ID sea visible en la primera columna
            '2- Y para que cuando el usuario escriba en el lv, el control filtre por su descripcion.
            lvCiudades.Columns(0).DisplayIndex = 1

            'Ahora seleccionamos automaticamente el registro especificado
            If IDCiudad = 0 Then
                'lvCiudades.Items(0).Selected = True
            Else
                For i As Integer = 0 To lvCiudades.Items.Count - 1
                    If lvCiudades.Items(i).SubItems(1).Text = IDCiudad Then
                        lvCiudades.Items(i).Selected = True
                        Exit For
                    End If
                Next

            End If

        End If


        lvCiudades.Refresh()

    End Sub

    Sub InicializarControlesCiudad()

        'CSistema.InicializaControles(tpCiudades)

        'TextBox
        txtCiudadDescripcion.txt.Clear()
        txtCiudadCodigo.txt.Clear()
        txtCodigoSet.txt.Clear()

        'RadioButton
        rdbCiudadActivo.Checked = True

        'Funciones
        If Me.vCiudadNuevo = True Then
            txtCiudadID.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Ciudad"), Integer)
        Else
            ListarCiudads(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Ciudad"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        cbxCiudadPais.cbx.Focus()


    End Sub

    Sub ProcesarCiudad(ByVal Operacion As CSistema.NUMOperacionesABM)

        'Validar
        'Si es que selecciono correctamente el Pais
        If IsNumeric(cbxCiudadPais.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el pais al que corresponde!"
            ctrError.SetError(cbxCiudadPais, mensaje)
            ctrError.SetIconAlignment(cbxCiudadPais, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Si es que selecciono correctamente el Departamento
        If IsNumeric(cbxCiudadDepartamento.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el departamento al que corresponde!"
            ctrError.SetError(cbxCiudadDepartamento, mensaje)
            ctrError.SetIconAlignment(cbxCiudadDepartamento, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Escritura de la Descripcion
        If txtCiudadDescripcion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar una descripcion valida!"
            ctrError.SetError(txtCiudadDescripcion, mensaje)
            ctrError.SetIconAlignment(txtCiudadDescripcion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If lvCiudades.SelectedItems.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(lvCiudades, mensaje)
                ctrError.SetIconAlignment(lvCiudades, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtCiudadID.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtCiudadDescripcion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDPais", cbxCiudadPais.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepartamento", cbxCiudadDepartamento.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Codigo", txtCiudadCodigo.txt.Text.Trim, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@CodigoSET", txtCodigoSet.txt.Text.Trim, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Estado", rdbCiudadActivo.Checked.ToString, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@CodigoUnilever", txtCiudadCodigoUnilever.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpCiudad", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnCiudadNuevo, btnPaisEditar, btnCiudadCancelar, btnCiudadGuardar, btnCiudadEliminar, vControlesCiudad)
            ListarCiudads(txtCiudadID.Text)
            ctrError.Clear()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnCiudadGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnCiudadGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnCiudadNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCiudadNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnCiudadNuevo, btnCiudadEditar, btnCiudadCancelar, btnCiudadGuardar, btnCiudadEliminar, vControlesCiudad)
        vCiudadNuevo = True
        InicializarControlesCiudad()
    End Sub

    Private Sub btnCiudadEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCiudadEditar.Click

        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnCiudadNuevo, btnCiudadEditar, btnCiudadCancelar, btnCiudadGuardar, btnCiudadEliminar, vControlesCiudad)

        vCiudadNuevo = False

        'Foco
        txtCiudadDescripcion.txt.Focus()
        txtCiudadDescripcion.txt.SelectAll()

    End Sub

    Private Sub btnCiudadCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCiudadCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnCiudadNuevo, btnCiudadEditar, btnCiudadCancelar, btnCiudadGuardar, btnCiudadEliminar, vControlesCiudad)
        vCiudadNuevo = False
        InicializarControlesCiudad()
        ObtenerInformacionCiudad()
    End Sub

    Private Sub btnCiudadGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCiudadGuardar.Click

        If vCiudadNuevo = True Then
            ProcesarCiudad(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            ProcesarCiudad(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnCiudadEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCiudadEliminar.Click

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnCiudadNuevo, btnCiudadEditar, btnCiudadCancelar, btnCiudadGuardar, btnCiudadEliminar, vControlesCiudad)
        ProcesarCiudad(ERP.CSistema.NUMOperacionesABM.DEL)

    End Sub

    Private Sub lvCiudades_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvCiudades.Click
        ObtenerInformacionCiudad()
    End Sub

    Private Sub lvCiudades_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvCiudades.SelectedIndexChanged
        ObtenerInformacionCiudad()
    End Sub

    Private Sub cbxCiudadPais_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxCiudadPais.PropertyChanged
        cbxCiudadDepartamento.cbx.Text = ""

        'Listar Departamentos
        If IsNumeric(cbxCiudadPais.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxCiudadDepartamento.cbx, "Select ID, Descripcion From Departamento Where IDPais=" & cbxCiudadPais.cbx.SelectedValue & " Order By Orden Desc, Descripcion Asc")

    End Sub

#End Region

#Region "BARRIO"

    'VARIABLES
    Dim vBarrioNuevo As Boolean
    Dim vControlesBarrio() As Control

    'FUNCIONES
    Sub InicializarBarrio()

        'TextBox
        txtBarrioDescripcion.txt.Clear()

        'ListView
        lvBarrio.Clear()

        'Variables
        vBarrioNuevo = False

        'RadioButton
        rdbBarrioActivo.Checked = True

        'Funciones
        CargarInformacionBarrio()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnBarrioNuevo, btnBarrioEditar, btnBarrioCancelar, btnBarrioGuardar, btnBarrioEliminar, vControlesBarrio)

        'Focus
        lvBarrio.Focus()

    End Sub

    Sub CargarInformacionBarrio()

        'Cargamos los paises
        CSistema.SqlToComboBox(cbxPais, "Select ID, Descripcion From Pais Order By Orden Desc, 2")
        cbxPais.Text = ""
        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControlesBarrio(-1)
        CSistema.CargaControl(vControlesBarrio, txtBarrioDescripcion)
        CSistema.CargaControl(vControlesBarrio, rdbBarrioActivo)
        CSistema.CargaControl(vControlesBarrio, rdbBarrioDesactivado)
        CSistema.CargaControl(vControlesBarrio, cbxPais)
        CSistema.CargaControl(vControlesBarrio, cbxDepartamento)
        CSistema.CargaControl(vControlesBarrio, cbxCiudad)
        'Cargamos los paises en el lv
        ListarBarrios()

    End Sub

    Sub ObtenerInformacionBarrio()

        'Validar
        'Si es que se selecciono el registro.
        If lvBarrio.SelectedItems.Count = 0 Then
            ctrError.SetError(lvBarrio, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(lvBarrio, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnBarrioNuevo, btnBarrioEditar, btnBarrioCancelar, btnBarrioGuardar, btnBarrioEliminar, vControlesBarrio)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = lvBarrio.SelectedItems(0).SubItems(1).Text

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select ID, Descripcion, Estado, Pais, Departamento, Ciudad From vBarrio Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtBarrioID.Text = oRow("ID").ToString
            txtBarrioDescripcion.txt.Text = oRow("Descripcion").ToString

            cbxPais.cbx.Text = oRow("Pais").ToString
            cbxDepartamento.cbx.Text = oRow("Departamento").ToString
            cbxCiudad.cbx.Text = oRow("Ciudad").ToString

            If CBool(oRow("Estado")) = True Then
                rdbBarrioActivo.Checked = True
            Else
                rdbBarrioDesactivado.Checked = True
            End If

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnBarrioNuevo, btnBarrioEditar, btnBarrioCancelar, btnBarrioGuardar, btnBarrioEliminar, vControlesBarrio)

        End If

        ctrError.Clear()

    End Sub

    Sub ListarBarrios(Optional ByVal IDBarrio As Integer = 0)

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        CSistema.SqlToLv(lvBarrio, "Select 'Barrio'=B.Descripcion, B.ID, 'Estado'=(Case When Estado = 'True' Then 'OK' Else '-' End), Pais, Departamento, Ciudad From vBarrio B Order By B.Descripcion")

        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If lvBarrio.Columns.Count > 0 Then

            'Esto hacemos para que: 
            '1- Que el ID sea visible en la primera columna
            '2- Y para que cuando el usuario escriba en el lv, el control filtre por su descripcion.
            lvBarrio.Columns(0).DisplayIndex = 1

            'Ahora seleccionamos automaticamente el registro especificado
            If IDBarrio = 0 Then
                'lvBarrio.Items(0).Selected = True
            Else
                For i As Integer = 0 To lvBarrio.Items.Count - 1
                    If lvBarrio.Items(i).SubItems(1).Text = IDBarrio Then
                        lvBarrio.Items(i).Selected = True
                        Exit For
                    End If
                Next

            End If

        End If


        lvBarrio.Refresh()

    End Sub

    Sub InicializarControlesBarrio()

        'CSistema.InicializaControles(tpBarrioes)

        'TextBox
        txtBarrioDescripcion.txt.Clear()
        cbxPais.Text = ""
        cbxDepartamento.Text = ""
        cbxCiudad.Text = ""

        'RadioButton
        rdbBarrioActivo.Checked = True

        'Funciones
        If Me.vBarrioNuevo = True Then
            txtBarrioID.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Barrio"), Integer)
        Else
            ListarBarrios(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Barrio"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        txtBarrioDescripcion.txt.Focus()


    End Sub

    Sub ProcesarBarrio(ByVal Operacion As CSistema.NUMOperacionesABM)

        'Validar
        'Escritura de la Descripcion
        If txtBarrioDescripcion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar una descripcion valida!"
            ctrError.SetError(txtBarrioDescripcion, mensaje)
            ctrError.SetIconAlignment(txtBarrioDescripcion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If lvBarrio.SelectedItems.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(lvBarrio, mensaje)
                ctrError.SetIconAlignment(lvBarrio, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtBarrioID.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtBarrioDescripcion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbBarrioActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@IDPais", cbxPais.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepartamento", cbxDepartamento.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCiudad", cbxCiudad.cbx.SelectedValue, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpBarrio", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnBarrioNuevo, btnPaisEditar, btnBarrioCancelar, btnBarrioGuardar, btnBarrioEliminar, vControlesBarrio)
            ListarBarrios(txtBarrioID.Text)
            ctrError.Clear()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnBarrioGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnBarrioGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnBarrioNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBarrioNuevo.Click

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnBarrioNuevo, btnBarrioEditar, btnBarrioCancelar, btnBarrioGuardar, btnBarrioEliminar, vControlesBarrio)
        vBarrioNuevo = True
        InicializarControlesBarrio()
    End Sub

    Private Sub btnBarrioEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBarrioEditar.Click

        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnBarrioNuevo, btnBarrioEditar, btnBarrioCancelar, btnBarrioGuardar, btnBarrioEliminar, vControlesBarrio)

        vBarrioNuevo = False

        'Foco
        txtBarrioDescripcion.txt.Focus()
        txtBarrioDescripcion.txt.SelectAll()

    End Sub

    Private Sub btnBarrioCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBarrioCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnBarrioNuevo, btnBarrioEditar, btnBarrioCancelar, btnBarrioGuardar, btnBarrioEliminar, vControlesBarrio)
        vBarrioNuevo = False
        InicializarControlesBarrio()
        ObtenerInformacionBarrio()
    End Sub

    Private Sub btnBarrioGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBarrioGuardar.Click

        If vBarrioNuevo = True Then
            ProcesarBarrio(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            ProcesarBarrio(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnBarrioEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBarrioEliminar.Click

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnBarrioNuevo, btnBarrioEditar, btnBarrioCancelar, btnBarrioGuardar, btnBarrioEliminar, vControlesBarrio)
        ProcesarBarrio(ERP.CSistema.NUMOperacionesABM.DEL)

    End Sub

    Private Sub lvBarrio_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvBarrio.Click
        ObtenerInformacionBarrio()
    End Sub

    Private Sub lvBarrio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvBarrio.SelectedIndexChanged
        ObtenerInformacionBarrio()
    End Sub

    Private Sub cbxPais_PropertyChanged(sender As Object, e As EventArgs) Handles cbxPais.PropertyChanged
        cbxDepartamento.cbx.Text = ""
        CSistema.SqlToComboBox(cbxDepartamento.cbx, "Select ID, Descripcion From Departamento Where IDPais = " & cbxPais.cbx.SelectedValue & " Order By Orden Desc, Descripcion Asc")
        cbxDepartamento.cbx.Text = ""
    End Sub

    Private Sub cbxDepartamento_PropertyChanged(sender As Object, e As EventArgs) Handles cbxDepartamento.PropertyChanged
        cbxCiudad.cbx.Text = ""
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select ID, Descripcion From Ciudad Where IDDepartamento = " & cbxDepartamento.cbx.SelectedValue & " Order By Orden Desc, Descripcion Asc")
        cbxCiudad.cbx.Text = ""
    End Sub

#End Region

    Private Sub frmPaisDepartamentoCiudadBarrios_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        InicializarPais()
        InicializarDepartamento()
        InicializarCiudad()
        InicializarBarrio()

    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

End Class
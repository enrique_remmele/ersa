﻿Public Class frmProveedor

    'PROPIEDADES
    Private IDProveedorValue As String
    Public Property IDProveedor() As String
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As String)
            IDProveedorValue = value
        End Set
    End Property

    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Variables()
        vNuevo = False

        'RadioButton()

        'Funciones()
        CargarInformacion()

        'Botones()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO)

        'Controles
        CSistema.InicializaControles(Me)
        CSistema.InicializaControles(tabDatosComerciales)
        ocxCuentaCompra.Conectar()
        ocxCuentaVenta.Conectar()

        ManejarTecla(New KeyEventArgs(Keys.End))

    End Sub
    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesABM)

        CSistema.ControlBotonesABM(Operacion, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)

        'Datos Comerciales
        CSistema.CargaControl(vControles, txtID)
        CSistema.CargaControl(vControles, txtRazonSocial)
        CSistema.CargaControl(vControles, txtRUC)
        CSistema.CargaControl(vControles, txtNombreFantasia)
        CSistema.CargaControl(vControles, txtDireccion)
        CSistema.CargaControl(vControles, txtTelefonos)
        CSistema.CargaControl(vControles, chkActivado)
        CSistema.CargaControl(vControles, chkDelExterior)
        CSistema.CargaControl(vControles, cbxTipoCompra)
        CSistema.CargaControl(vControles, txtCodigoProveedor)

        'Localizaciones
        CSistema.CargaControl(vControles, cbxPais)
        CSistema.CargaControl(vControles, cbxDepartamento)
        CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxBarrio)

        'Referencias
        CSistema.CargaControl(vControles, cbxSucursal)

        'Configuraciones
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, cbxTipoProveedor)
        CSistema.CargaControl(vControles, ocxCuentaCompra)
        CSistema.CargaControl(vControles, ocxCuentaVenta)

        'Datos Adicionales
        CSistema.CargaControl(vControles, txtPaginaWeb)
        CSistema.CargaControl(vControles, txtEmail)
        CSistema.CargaControl(vControles, txtFax)

        'Crédito y Retentor
        CSistema.CargaControl(vControles, chkCredito)
        CSistema.CargaControl(vControles, txtPlazo)
        CSistema.CargaControl(vControles, chkRetentor)
        CSistema.CargaControl(vControles, chkSujetoRetencion)
        CSistema.CargaControl(vControles, chkExportado)
        CSistema.CargaControl(vControles, chkAcopiador)


        'Listar Paises
        CSistema.SqlToComboBox(cbxPais.cbx, "Select ID, Descripcion From Pais Order By Orden Desc, Descripcion Asc")

        'Listar Barrios
        CSistema.SqlToComboBox(cbxBarrio.cbx, "Select ID, Descripcion From Barrio Order By Orden Desc, Descripcion Asc")

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Descripcion From Sucursal Order By Descripcion")

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Descripcion From Moneda  Order By 2")

        'Tipo Proveedor
        CSistema.SqlToComboBox(cbxTipoProveedor.cbx, "Select ID, Descripcion From TipoProveedor Order By 2")

        'Tipo de compra
        cbxTipoCompra.cbx.Items.Add("INDISTINTO")
        cbxTipoCompra.cbx.Items.Add("DIRECTO")


      
    End Sub

    Sub ObtenerInformacion(ByVal Filtro As String, Optional ByVal PorReferencia As Boolean = False)

        'Validar
        'Obtener el ID Registro
        Dim ID As Integer

        If PorReferencia = False Then
            If IsNumeric(txtID.txt.Text) = False Then
                Exit Sub
            End If

            If CInt(txtID.txt.Text) = 0 Then
                Exit Sub
            End If
        End If

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VProveedor " & Filtro)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count = 0 Then

            ID = txtID.ObtenerValor

            CSistema.InicializaControles(Me)
            CSistema.InicializaControles(tabDatosComerciales)


            ctrError.SetError(txtID, "No se encontro ningun registro!")
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.TopRight)
            tsslEstado.Text = "No se encontro ningun registro!"


            txtID.txt.Text = ID
            txtReferencia.txt.SelectAll()
            txtReferencia.txt.Focus()

            Exit Sub

        Else

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Datos comerciales
            ID = oRow("ID").ToString
            txtReferencia.txt.Text = oRow("Referencia").ToString
            txtRazonSocial.txt.Text = oRow("RazonSocial").ToString
            txtRUC.txt.Text = oRow("RUC").ToString
            txtID.txt.Text = oRow("ID").ToString
            txtNombreFantasia.txt.Text = oRow("NombreFantasia").ToString
            txtDireccion.txt.Text = oRow("Direccion").ToString
            txtTelefonos.txt.Text = oRow("Telefono").ToString
            chkActivado.Checked = CBool(oRow("Estado").ToString)
            'SC:16-09-2021 Marca a los proveedores del exterior
            chkDelExterior.Checked = CBool(oRow("DelExterior").ToString)

            cbxTipoCompra.cbx.Text = ""
            txtCodigoProveedor.Text = oRow("CodigoProveedor").ToString

            If oRow("TipoCompra").ToString.Replace(" ", "") = "I" Then
                cbxTipoCompra.cbx.Text = "INDIRECTO"
            End If

            If oRow("TipoCompra").ToString.Replace(" ", "") = "D" Then
                cbxTipoCompra.cbx.Text = "DIRECTO"
            End If

            'Localizacion
            cbxPais.cbx.Text = oRow("Pais").ToString
            cbxDepartamento.cbx.Text = oRow("Departamento").ToString
            cbxCiudad.cbx.Text = oRow("Ciudad").ToString
            cbxBarrio.cbx.Text = oRow("Barrio").ToString

            'Referencias
            cbxSucursal.cbx.Text = oRow("Sucursal").ToString

            'Configuracion
            cbxMoneda.cbx.Text = oRow("Moneda").ToString
            cbxTipoProveedor.cbx.Text = oRow("TipoProveedor").ToString
            ocxCuentaCompra.SetValue(oRow("CuentaContableCompra").ToString)
            ocxCuentaVenta.SetValue(oRow("CuentaContableVenta").ToString)

            'Credito y Retentor
            chkCredito.Checked = CBool(oRow("Credito").ToString)
            txtPlazo.txt.Text = oRow("PlazoCredito").ToString
            chkRetentor.Checked = CBool(oRow("Retentor").ToString)
            chkSujetoRetencion.Checked = CBool(oRow("SujetoRetencion").ToString)
            chkExportado.Checked = CBool(oRow("Exportador").ToString)
            chkAcopiador.Checked = CBool(oRow("Acopiador").ToString)

            'Estadisticas
            txtUsuarioAlta.Text = oRow("UsuarioAlta").ToString
            txtFechaAlta.Text = oRow("Alta").ToString
            txtUsuarioModificacion.Text = oRow("UsuarioModificacion").ToString
            txtFechaModificacion.Text = oRow("Modificacion").ToString

            If IsDate(oRow("FechaUltimaCompra").ToString) = True Then
                txtFechaUltimaCompra.Text = CDate(oRow("FechaUltimaCompra").ToString).ToShortDateString
            Else
                txtFechaUltimaCompra.Text = ""
            End If


            'Adicionales
            CSistema.CargarRowEnControl(oRow, "PaginaWeb", txtPaginaWeb)
            CSistema.CargarRowEnControl(oRow, "Email", txtEmail)
            CSistema.CargarRowEnControl(oRow, "Fax", txtFax)

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            txtReferencia.txt.SelectAll()
            txtReferencia.txt.Focus()


            'Cargar Mapas
            ObtenerInformacionMapa(ID)
        End If

        ctrError.Clear()

    End Sub

    Sub ObtenerInformacionMapa(ByVal ID As Integer)

        Dim sql As String = "Select Latitud, Longitud From VProveedor Where ID=" & ID
        Dim dt As DataTable = CSistema.ExecuteToDataTable(sql, VGCadenaConexion)

        OcxProveedoresMapa1.Inicializar()
        OcxProveedoresMapa1.ID = ID

        OcxProveedoresMapa1.Latitud = dt.Rows(0)("Latitud").ToString
        OcxProveedoresMapa1.Longitud = dt.Rows(0)("Longitud").ToString


        OcxProveedoresMapa1.CargarPunto()



    End Sub


    Sub ListarProveedor()

        Dim frmBuscar As New frmProveedorBuscar
        frmBuscar.WindowState = FormWindowState.Normal
        frmBuscar.StartPosition = FormStartPosition.CenterParent
        frmBuscar.ShowDialog()
        If frmBuscar.ID > 0 Then
            txtID.txt.Text = frmBuscar.ID
            ObtenerInformacion(" Where ID = " & frmBuscar.ID)
        End If

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Razon Social
        If txtRazonSocial.txt.Text.Trim.Length = 0 Then
            Dim Mensaje As String = "Ingrese la Razon Social!"
            ctrError.SetError(txtRazonSocial, Mensaje)
            ctrError.SetIconAlignment(txtRazonSocial, ErrorIconAlignment.TopRight)

            txtRazonSocial.txt.SelectAll()
            txtRazonSocial.Focus()

            tsslEstado.Text = "Atencion!!! " & Mensaje

            Exit Sub

        End If

        'INICIO: SC 11-08-2021 Hasta aqui actualización para exigir de parte del sistema la carga de Plazo si el Chek de la Condición Crédito se activa
        If chkCredito.Checked = True Then
            If txtPlazo.txt.Text = 0 Then
                Dim mensaje As String = "ATENCION: Debe cargar el Plazo relacionado al credito!!!"
                ctrError.SetError(txtPlazo, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                txtPlazo.Focus()
                Exit Sub
            End If
        End If
        'FIN: SC 11-08-2021 Hasta aqui actualización para exigir de parte del sistema la carga de Plazo si el Chek de la Condición Crédito se activa

        'RUC
        If txtRUC.txt.Text.Trim.Length = 0 Then
            Dim Mensaje As String = "Ingrese el RUC!"
            ctrError.SetError(txtRUC, Mensaje)
            ctrError.SetIconAlignment(txtRUC, ErrorIconAlignment.TopRight)

            txtRUC.txt.SelectAll()
            txtRUC.Focus()

            tsslEstado.Text = "Atencion!!! " & Mensaje

            Exit Sub

        End If

        'RUC
        If CSistema.ValidarDigitoVerificador(txtRUC.GetValue.Trim) = False Then
            Exit Sub
        End If

        'RUC
        If txtReferencia.txt.Text.Trim.Length = 0 Then
            Dim Mensaje As String = "Ingrese la Referencia del proveedor!"
            ctrError.SetError(txtReferencia, Mensaje)
            ctrError.SetIconAlignment(txtReferencia, ErrorIconAlignment.TopRight)

            txtRUC.txt.SelectAll()
            txtRUC.Focus()

            tsslEstado.Text = "Atencion!!! " & Mensaje

            Exit Sub

        End If

        'Telefono
        If txtTelefonos.txt.Text.Trim.Length = 0 Then
            Dim Mensaje As String = "Ingrese numero de Telefono valido!"
            ctrError.SetError(txtTelefonos, Mensaje)
            ctrError.SetIconAlignment(txtTelefonos, ErrorIconAlignment.TopRight)

            txtTelefonos.txt.SelectAll()
            txtTelefonos.Focus()

            tsslEstado.Text = "Atencion!!! " & Mensaje

            Exit Sub

        End If
        'Direccion
        If txtDireccion.txt.Text.Trim.Length = 0 Then
            Dim Mensaje As String = "Ingrese direccion valida!"
            ctrError.SetError(txtTelefonos, Mensaje)
            ctrError.SetIconAlignment(txtDireccion, ErrorIconAlignment.TopRight)

            txtDireccion.txt.SelectAll()
            txtDireccion.Focus()

            tsslEstado.Text = "Atencion!!! " & Mensaje

            Exit Sub

        End If

        ''Tipo proveedor
        If cbxTipoProveedor.txt.Text = "" Then
            CSistema.MostrarError("Ingrese una Tipo de proveedor valido!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            cbxTipoProveedor.txt.SelectAll()
            cbxTipoProveedor.Focus()
            Exit Sub
        End If
        ''Pais
        If cbxPais.txt.Text = "" Then
            CSistema.MostrarError("Ingrese un Pais valido!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            cbxPais.txt.SelectAll()
            cbxPais.Focus()
            Exit Sub
        End If

        ''Departamento
        If cbxDepartamento.txt.Text = "" Then
            CSistema.MostrarError("Ingrese un Departamento valido!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            cbxDepartamento.txt.SelectAll()
            cbxDepartamento.Focus()
            Exit Sub
        End If

        ''Ciudad
        If cbxCiudad.txt.Text = "" Then
            CSistema.MostrarError("Ingrese una Ciudad valida!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            cbxCiudad.txt.SelectAll()
            cbxCiudad.Focus()
            Exit Sub
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        'Datos Obligatorio
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Referencia", txtReferencia.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@RazonSocial", txtRazonSocial.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@RUC", txtRUC.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TipoCompra", cbxTipoCompra.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CodigoProveedor", txtCodigoProveedor.Text.Trim, ParameterDirection.Input)

        If cbxTipoCompra.cbx.SelectedIndex = 0 Then

        End If

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Datos Comerciales
        CSistema.SetSQLParameter(param, "@NombreFantasia", txtNombreFantasia.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Direccion", txtDireccion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Telefono", txtTelefonos.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", chkActivado.Checked.ToString, ParameterDirection.Input)
        '16-09-2021 SC: Actualizacion para discriminar Proveedores del Exterior
        CSistema.SetSQLParameter(param, "@DelExterior", chkDelExterior.Checked.ToString, ParameterDirection.Input)

        'Localizacion
        CSistema.SetSQLParameter(param, "@IDPais", cbxPais.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepartamento", cbxDepartamento.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCiudad", cbxCiudad.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDBarrio", cbxBarrio.cbx, ParameterDirection.Input)

        'Referencia
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.cbx, ParameterDirection.Input)

        'Configuraciones
        CSistema.SetSQLParameter(param, "@IDTipoProveedor", cbxTipoProveedor.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaContableCompra", ocxCuentaCompra.txtCodigo.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaContableVenta", ocxCuentaVenta.txtCodigo.GetValue, ParameterDirection.Input)

        'Adicionales
        CSistema.SetSQLParameter(param, "@PaginaWeb", txtPaginaWeb.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Email", txtEmail.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fax", txtFax.txt.Text.Trim, ParameterDirection.Input)

        'Crédito y Retentor
        CSistema.SetSQLParameter(param, "@Credito", chkCredito.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PlazoCredito", txtPlazo.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Retentor", chkRetentor.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@SujetoRetencion", chkSujetoRetencion.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Exportador", chkExportado.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Acopiador", chkAcopiador.Checked.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpProveedor", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            txtReferencia.txt.ReadOnly = False
            txtReferencia.txt.Focus()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
                ctrError.SetError(btnEliminar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopRight)
            Else
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            End If

        End If

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If vNuevo = True Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then

            ObtenerInformacion(" Where Referencia = '" & txtReferencia.GetValue & "' ")
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            ObtenerInformacion(" Where ID = " & txtID.ObtenerValor)

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            ObtenerInformacion(" Where ID = " & txtID.ObtenerValor)

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Proveedor"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            ObtenerInformacion(" Where ID = " & txtID.ObtenerValor)

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(ID), 1) From Proveedor"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            ObtenerInformacion(" Where ID = " & txtID.ObtenerValor)

        End If

        'Nuevo

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub Nuevo()

        CSistema.InicializaControles(Me)
        CSistema.InicializaControles(tabDatosComerciales)
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO)
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Proveedor"), Integer)
        vNuevo = True
        tsslEstado.Text = ""
        ctrError.Clear()
        chkActivado.Checked = True
        'SC: 16-09-2021 Marca para proveedor del exterior
        chkDelExterior.Checked = False
        txtReferencia.txt.Focus()
        chkSujetoRetencion.Checked = True

    End Sub

    Sub Cancelar()

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        ManejarTecla(New KeyEventArgs(Keys.End))
        txtReferencia.txt.Focus()
        vNuevo = False

    End Sub

    Private Sub frmProveedor_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F1 Then
            ListarProveedor()
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then

            If ocxCuentaCompra.Focused = True Then
                Exit Sub
            End If

            If ocxCuentaVenta.Focused = True Then
                Exit Sub
            End If

            CSistema.SelectNextControl(Me, e.KeyCode)

        End If


    End Sub

    Private Sub frmCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        txtID.txt.ReadOnly = True
        vNuevo = False
        txtRazonSocial.txt.Focus()
        btnBuscar.Enabled = False
    End Sub

    Private Sub cbxDepartamento_Editar(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxDepartamento.Editar

        Dim frm As New frmPaisDepartamentoCiudadBarrios
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpPaises"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpCiudades"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpBarrios"))

        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        'Listar Departamentos
        cbxCiudad.cbx.DataSource = Nothing
        CSistema.SqlToComboBox(cbxDepartamento.cbx, "Select ID, Descripcion From Departamento Where IDPais=" & cbxPais.cbx.SelectedValue & " Order By Orden Desc, Descripcion Asc")

    End Sub

    Private Sub cbxDepartamento_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxDepartamento.PropertyChanged
        cbxCiudad.cbx.Text = ""

        'Listar Ciudades
        If IsNumeric(cbxDepartamento.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxDepartamento.cbx.Text = "" Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select ID, Descripcion From Ciudad Where IDDepartamento=" & cbxDepartamento.cbx.SelectedValue & " Order By Orden Desc, Descripcion Asc")

        cbxCiudad.cbx.Text = ""

    End Sub

    Private Sub cbxPais_Editar(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxPais.Editar

        Dim frm As New frmPaisDepartamentoCiudadBarrios
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpDepartamentos"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpCiudades"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpBarrios"))

        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        'Listar Paises
        cbxDepartamento.cbx.DataSource = Nothing
        cbxCiudad.cbx.DataSource = Nothing
        CSistema.SqlToComboBox(cbxPais.cbx, "Select ID, Descripcion From Pais Order By Orden Desc, Descripcion Asc")

    End Sub

    Private Sub cbxPais_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxPais.PropertyChanged
        cbxDepartamento.cbx.Text = ""

        'Listar Departamentos
        If IsNumeric(cbxPais.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxPais.cbx.Text = "" Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxDepartamento.cbx, "Select ID, Descripcion From Departamento Where IDPais=" & cbxPais.cbx.SelectedValue & " Order By Orden Desc, Descripcion Asc")

        cbxDepartamento.cbx.Text = ""

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        ListarProveedor()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub cbxCiudad_Editar(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxCiudad.Editar

        Dim frm As New frmPaisDepartamentoCiudadBarrios
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpPaises"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpDepartamentos"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpBarrios"))

        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        'Listar Ciudades
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select ID, Descripcion From Ciudad Where IDDepartamento=" & cbxDepartamento.cbx.SelectedValue & " Order By Orden Desc, Descripcion Asc")

    End Sub

    Private Sub cbxBarrio_Editar(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxBarrio.Editar
        Dim frm As New frmPaisDepartamentoCiudadBarrios
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpPaises"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpDepartamentos"))
        frm.tbcGeneral.TabPages.Remove(frm.tbcGeneral.TabPages("tpCiudades"))

        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        'Listar Barrios
        CSistema.SqlToComboBox(cbxBarrio.cbx, "Select ID, Descripcion From Barrio Order By Orden Desc, Descripcion Asc")

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR)
        vNuevo = False
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)

    End Sub

    Private Sub ocxCuentaCompra_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocxCuentaCompra.ItemSeleccionado
        If ocxCuentaCompra.Focused = False Then
            Exit Sub
        End If

        ocxCuentaVenta.Focus()
    End Sub

    Private Sub ocxCuentaVenta_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocxCuentaVenta.ItemSeleccionado
        If ocxCuentaVenta.Focused = False Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, Keys.Enter)

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        'ManejarTecla(e)
    End Sub

    Private Sub txtReferencia_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtReferencia.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    'INICIO: SC 09-08-2021 Desde aqui actualización para exigir de parte del sistema la carga de Plazo si el Chek de la Condición Crédito se activa
    Private Sub chkCredito_Leave(sender As Object, e As EventArgs) Handles chkCredito.Leave
        If chkCredito.Checked = True Then
            txtPlazo.Focus()
        End If

    End Sub

    Private Sub txtPlazo_Leave(sender As Object, e As EventArgs) Handles txtPlazo.Leave
        If chkCredito.Checked = True Then
            If txtPlazo.txt.Text = 0 Then
                Dim mensaje As String = "ATENCION: Debe cargar el Plazo relacionado al credito!!!"
                ctrError.SetError(txtPlazo, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                txtPlazo.Focus()
            Else

                ctrError.Clear()
                tsslEstado.Text = ""
            End If
        Else
            txtPlazo.txt.Text = 0
        End If
    End Sub

    Private Sub frmProveedor_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        'FA 20/06/2023
        LiberarMemoria()
    End Sub
    'FIN: SC 09-08-2021 Hasta aqui actualización para exigir de parte del sistema la carga de Plazo si el Chek de la Condición Crédito se activa

End Class
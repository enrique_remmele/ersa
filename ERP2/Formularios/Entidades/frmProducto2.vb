﻿Public Class frmProducto2

    'PROPIEDADES
    Private IDProductoValue As String
    Public Property IDProducto() As String
        Get
            Return IDProductoValue
        End Get
        Set(ByVal value As String)
            IDProductoValue = value
        End Set
    End Property

    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Variables
        vNuevo = False

        'RadioButton

        'Funciones
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Controles
        CSistema.InicializaControles(Me)
        CSistema.InicializaControles(tabIdentificacion)
        ocxCuentaCompra.Conectar()
        ocxCuentaVenta.Conectar()
        OcxClasificador1.Tabla = "VClasificacionProducto"
        OcxClasificador1.StoreProcedure = "SpClasificacionProducto"
        OcxClasificador1.TablaDetalle = "VProducto"
        OcxClasificador1.StoreProcedureDetalle = "SpProductoClasificacion"
        OcxClasificador1.frmPadre = Me
        OcxClasificador1.Inicializar()

        'Focus
        txtID.Focus()

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)

        'Datos principales
        CSistema.CargaControl(vControles, txtDescripcion)
        CSistema.CargaControl(vControles, txtCodigoBarra)
        CSistema.CargaControl(vControles, txtReferencia)

        'Agrupadores
        CSistema.CargaControl(vControles, cbxProveedor)
        CSistema.CargaControl(vControles, cbxDivision)

        'Unidad de Medida
        CSistema.CargaControl(vControles, cbxUnidadMedida)
        CSistema.CargaControl(vControles, txtUnidadPorCaja)
        CSistema.CargaControl(vControles, txtPesoPorCaja)
        CSistema.CargaControl(vControles, txtVolumenPorCaja)

        'Configuraciones
        CSistema.CargaControl(vControles, cbxImpuesto)
        CSistema.CargaControl(vControles, chkActivo)
        CSistema.CargaControl(vControles, chkControlarExistencia)
        CSistema.CargaControl(vControles, ocxCuentaCompra)

        'Proveedor
        CSistema.SqlToComboBox(cbxProveedor.cbx, "Select ID, RazonSocial From Proveedor Order By 2")

        'Unidad de Medida
        CSistema.SqlToComboBox(cbxUnidadMedida.cbx, "Select ID, Descripcion From UnidadMedida Order By 2")

        'Impuesto
        CSistema.SqlToComboBox(cbxImpuesto.cbx, "Select ID, Descripcion From Impuesto Order By 1")

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Obtener el ID Registro
        Dim ID As Integer

        If IsNumeric(txtID.txt.Text) = False Then
            Exit Sub
        End If

        If CInt(txtID.txt.Text) = 0 Then
            Exit Sub
        End If

        ID = txtID.txt.Text

        'Limpiar Listas
        lvExistencia.Items.Clear()
        lvListaPrecio.Items.Clear()

        'Inicializar Controles
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevoPrecio, btnEditarPrecio, btnCancelarPrecio, btnGuardarPrecio, btnEliminarPrecio, vControlesPrecio)
        vNuevoPrecio = False

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VProducto Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count = 0 Then

            CSistema.InicializaControles(Me)
            CSistema.InicializaControles(tabIdentificacion)


            ctrError.SetError(txtID, "No se encontro ningun registro!")
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.TopRight)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()

            Exit Sub

        Else

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Datos principales
            txtDescripcion.txt.Text = oRow("Descripcion").ToString
            txtCodigoBarra.txt.Text = oRow("CodigoBarra").ToString
            txtReferencia.txt.Text = oRow("Referencia").ToString

            'Agrupadores
            OcxClasificador1.SetValue(oRow("IDClasificacion"))
            cbxProveedor.cbx.Text = oRow("Proveedor").ToString
            cbxDivision.cbx.Text = oRow("Division").ToString

            'Unidad de Medida
            cbxUnidadMedida.cbx.Text = oRow("UnidadMedida").ToString
            txtUnidadPorCaja.txt.Text = oRow("UnidadPorCaja").ToString
            txtPesoPorCaja.txt.Text = oRow("PesoPorCaja").ToString
            txtVolumenPorCaja.txt.Text = oRow("VolumenPorCaja").ToString

            'Configuraciones
            cbxImpuesto.cbx.Text = oRow("Impuesto").ToString
            chkActivo.Checked = CBool(oRow("Estado").ToString)
            chkControlarExistencia.Checked = CBool(oRow("ControlarExistencia").ToString)
            ocxCuentaCompra.txtDescripcion.Text = oRow("CuentaCompra").ToString
            ocxCuentaVenta.txtDescripcion.Text = oRow("CuentaVenta").ToString

            'Existencia
            txtExistenciaGeneral.txt.Text = oRow("Existencia").ToString

            'Estadisticos
            txtUltimoCosto.Text = CSistema.FormatoNumero(oRow("UltimoCosto").ToString, False)
            txtUltimoCostoSinIVA.Text = CSistema.FormatoNumero(CSistema.CalcularSinIVA(oRow("IDImpuesto").ToString, oRow("UltimoCosto").ToString, False, True), False)
            txtCostoPromedio.Text = CSistema.FormatoNumero(oRow("CostoPromedio").ToString, False)
            txtFechaUltimaEntrada.Text = oRow("UltimaActualizacion").ToString

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            'Listar Listas de Precios
            ListarPrecios(ID)

            'Listar Existencias
            ListarExistencias()

            txtID.txt.SelectAll()

        End If

        ctrError.Clear()

    End Sub

    Sub ListarProducto()

        Dim frmBuscar As New frmProductoBuscar
        frmBuscar.WindowState = FormWindowState.Normal
        frmBuscar.StartPosition = FormStartPosition.CenterParent
        frmBuscar.ShowDialog()
        If frmBuscar.ID > 0 Then
            txtID.txt.Text = frmBuscar.ID
            ObtenerInformacion()
        End If

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Descripcion
        If txtDescripcion.txt.Text.Trim.Length = 0 Then
            Dim Mensaje As String = "Ingrese la Descripcion!"
            ctrError.SetError(txtDescripcion, Mensaje)
            ctrError.SetIconAlignment(txtDescripcion, ErrorIconAlignment.TopRight)

            txtDescripcion.txt.SelectAll()
            txtDescripcion.Focus()

            tsslEstado.Text = "Atencion!!! " & Mensaje

            Exit Sub

        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        'Datos Obligatorio
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Identificadores
        CSistema.SetSQLParameter(param, "@CodigoBarra", txtCodigoBarra.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Referencia", txtReferencia.txt.Text, ParameterDirection.Input)

        'Agrupadores
        If OcxClasificador1.GetValue > 0 Then
            CSistema.SetSQLParameter(param, "@IDClasificacion", OcxClasificador1.GetValue, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDProveedor", cbxProveedor.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDivision", cbxDivision.cbx, ParameterDirection.Input)

        'Unidad de Medida
        CSistema.SetSQLParameter(param, "@IDUnidadMedida", cbxUnidadMedida.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@UnidadPorCaja", txtUnidadPorCaja.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PesoPorCaja", txtPesoPorCaja.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "VolumenPorCaja", txtVolumenPorCaja.txt.Text, ParameterDirection.Input)

        'Configuraciones
        CSistema.SetSQLParameter(param, "@IDImpuesto", cbxImpuesto.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", chkActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ControlarExistencia", chkControlarExistencia.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCuentaContableCompra", ocxCuentaCompra, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCuentaContableVenta", ocxCuentaVenta, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpProducto", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            txtID.txt.ReadOnly = False
            txtID.txt.Focus()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
                ctrError.SetError(btnEliminar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopRight)
            Else
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            End If

        End If

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ObtenerInformacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            ObtenerInformacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            ObtenerInformacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Producto"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            ObtenerInformacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(ID), 1) From Producto"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            ObtenerInformacion()

        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        CSistema.InicializaControles(Me)
        CSistema.InicializaControles(tabIdentificacion)
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Producto"), Integer)
        vNuevo = True
        txtDescripcion.txt.Focus()

        'Configuraciones
        cbxImpuesto.cbx.SelectedValue = 1
        chkActivo.Checked = True
        chkControlarExistencia.Checked = True

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        txtID.txt.ReadOnly = False
        txtID.txt.Focus()
        vNuevo = False
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        txtID.txt.ReadOnly = True
        vNuevo = False
        txtDescripcion.txt.Focus()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        ListarProducto()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmProducto_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F1 Then
            ListarProducto()
        End If
    End Sub

    Private Sub frmProducto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
        InicializarPrecio()
        InicializarExistencia()
    End Sub

    Private Sub cbxProveedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxProveedor.PropertyChanged
        cbxDivision.cbx.Text = ""

        'Listar Lineas
        If IsNumeric(cbxProveedor.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxDivision.cbx, "Select ID, Descripcion From Division Where IDProveedor=" & cbxProveedor.cbx.SelectedValue & " Order By Descripcion ")

    End Sub

#Region "PRECIOS"

    'VARIABLES
    Dim vNuevoPrecio As Boolean
    Dim vControlesPrecio() As Control

    Sub InicializarPrecio()

        'Variables
        vNuevoPrecio = False

        'RadioButton

        'Funciones
        CargarInformacionPrecio()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevoPrecio, btnEditarPrecio, btnCancelarPrecio, btnGuardarPrecio, btnEliminarPrecio, vControlesPrecio)

        'Controles
        CSistema.InicializaControles(tabPrecios)

    End Sub

    Sub CargarInformacionPrecio()

        ReDim vControlesPrecio(-1)

        'Datos principales
        CSistema.CargaControl(vControlesPrecio, cbxListaPrecio)
        CSistema.CargaControl(vControlesPrecio, txtPrecioUnitario)
        CSistema.CargaControl(vControlesPrecio, cbxMonedaPrecio)

        'Lista de Precios
        CSistema.SqlToComboBox(cbxListaPrecio.cbx, "Select ID, Descripcion From ListaPrecio Order By 2")

        'Monedas
        CSistema.SqlToComboBox(cbxMonedaPrecio.cbx, "Select ID, Descripcion From Moneda Order By 1")

    End Sub

    Sub ListarPrecios(Optional ByVal ID As Integer = 0)

        InicializarPrecio()

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        CSistema.SqlToLv(lvListaPrecio, "Select ListaPrecio, 'ID'=IDListaPrecio, Precio, Moneda, Estado, Decimales From VProductoListaPrecio Where IDProducto=" & ID & " Order By 1")

        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If lvListaPrecio.Columns.Count > 0 Then

            'Esto hacemos para que: 
            '1- Que el ID sea visible en la primera columna
            '2- Y para que cuando el usuario escriba en el lv, el control filtre por su descripcion.
            lvListaPrecio.Columns(0).DisplayIndex = 1

            'Formato
            lvListaPrecio.Columns(5).Width = 0
            CSistema.FormatoMoneda(lvListaPrecio, 2, 5)

        End If


        lvListaPrecio.Refresh()

    End Sub

    Sub ObtenerInformacionPrecio()

        'Validar
        'Si es que se selecciono el registro.
        If lvListaPrecio.SelectedItems.Count = 0 Then
            ctrError.SetError(lvListaPrecio, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(lvListaPrecio, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevoPrecio, btnEditarPrecio, btnCancelarPrecio, btnGuardarPrecio, btnEliminarPrecio, vControlesPrecio)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim IDListaPrecio As Integer

        IDListaPrecio = lvListaPrecio.SelectedItems(0).SubItems(1).Text

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select ListaPrecio, Precio, Moneda From VProductoListaPrecio Where IDListaPrecio=" & IDListaPrecio & " And IDProducto=" & txtID.ObtenerValor)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            cbxListaPrecio.cbx.Text = oRow("ListaPrecio").ToString
            txtPrecioUnitario.txt.Text = oRow("Precio").ToString
            cbxMonedaPrecio.cbx.Text = oRow("Moneda").ToString

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevoPrecio, btnEditarPrecio, btnCancelarPrecio, btnGuardarPrecio, btnEliminarPrecio, vControlesPrecio)

        End If

        ctrError.Clear()

    End Sub

    Sub ProcesarPrecios(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Seleccion de Lista de Precio
        If IsNumeric(cbxListaPrecio.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente una lista de precio!"
            ctrError.SetError(cbxListaPrecio, mensaje)
            ctrError.SetIconAlignment(cbxListaPrecio, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Moneda
        If IsNumeric(cbxMonedaPrecio.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente una moneda!"
            ctrError.SetError(cbxMonedaPrecio, mensaje)
            ctrError.SetIconAlignment(cbxMonedaPrecio, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If lvListaPrecio.SelectedItems.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(lvListaPrecio, mensaje)
                ctrError.SetIconAlignment(lvListaPrecio, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el IDProducto
        Dim IDProducto As Integer

        IDProducto = txtID.txt.Text


        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@IDProducto", IDProducto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDListaPrecio", cbxListaPrecio.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Precio", txtPrecioUnitario.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMonedaPrecio.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpProductoListaPrecio", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevoPrecio, btnEditarPrecio, btnCancelarPrecio, btnGuardarPrecio, btnEliminarPrecio, vControlesPrecio)
            ctrError.Clear()
            ListarPrecios(IDProducto)
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, 90)
            ctrError.SetError(btnGuardarPrecio, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardarPrecio, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnNuevoPrecio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevoPrecio.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevoPrecio, btnEditarPrecio, btnCancelarPrecio, btnGuardarPrecio, btnEliminarPrecio, vControlesPrecio)
        vNuevoPrecio = True
        cbxListaPrecio.cbx.Focus()

        'Configuraciones
        cbxMonedaPrecio.cbx.SelectedValue = 1

    End Sub

    Private Sub btnEditarPrecio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditarPrecio.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevoPrecio, btnEditarPrecio, btnCancelarPrecio, btnGuardarPrecio, btnEliminarPrecio, vControlesPrecio)
        vNuevoPrecio = False
        cbxListaPrecio.cbx.Focus()
    End Sub

    Private Sub btnGuardarPrecio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardarPrecio.Click
        If vNuevoPrecio = True Then
            ProcesarPrecios(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            ProcesarPrecios(ERP.CSistema.NUMOperacionesABM.UPD)
        End If
    End Sub

    Private Sub btnCancelarPrecio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelarPrecio.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevoPrecio, btnEditarPrecio, btnCancelarPrecio, btnGuardarPrecio, btnEliminarPrecio, vControlesPrecio)
        cbxListaPrecio.cbx.Focus()
        vNuevoPrecio = False
    End Sub

    Private Sub btnEliminarPrecio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarPrecio.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevoPrecio, btnEditarPrecio, btnCancelarPrecio, btnGuardarPrecio, btnEliminarPrecio, vControlesPrecio)
        vNuevoPrecio = False
        ProcesarPrecios(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub lvListaPrecio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvListaPrecio.SelectedIndexChanged
        ObtenerInformacionPrecio()
    End Sub

#End Region

#Region "EXISTENCIA"

    Sub InicializarExistencia()

        'Funciones
        CargarInformacionExistencia()

    End Sub

    Sub CargarInformacionExistencia()

        'Sucursal-Deposito
        CSistema.SqlToComboBox(cbxSucursalDeposito.cbx, "Select ID, [Suc-Dep] From VDeposito Order By Sucursal, ID")

    End Sub

    Sub ListarExistencias()

        InicializarExistencia()

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        CSistema.SqlToLv(lvExistencia, "Select [Suc-Dep], 'ID'=IDDeposito, Existencia, 'Minima'=ExistenciaMinima, 'Critica'=ExistenciaCritica From VExistenciaDeposito Where IDProducto=" & txtID.txt.Text & " Order By Sucursal, ID")

        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If lvExistencia.Columns.Count > 0 Then

            'Esto hacemos para que: 
            '1- Que el ID sea visible en la primera columna
            '2- Y para que cuando el usuario escriba en el lv, el control filtre por su descripcion.
            lvExistencia.Columns(0).DisplayIndex = 1

            'Formato
            CSistema.FormatoMoneda(lvExistencia, 2)
            CSistema.FormatoMoneda(lvExistencia, 3)

        End If


        lvExistencia.Refresh()

    End Sub

    Sub ObtenerInformacionExistencia()

        'Validar
        'Si es que se selecciono el registro.
        If lvExistencia.SelectedItems.Count = 0 Then
            ctrError.SetError(lvExistencia, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(lvExistencia, ErrorIconAlignment.TopLeft)

            Exit Sub

        End If

        'Obtener el ID Registro
        Dim IDDeposito As Integer

        IDDeposito = lvExistencia.SelectedItems(0).SubItems(1).Text

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select [Suc-Dep], ExistenciaMinima, ExistenciaCritica From VExistenciaDeposito Where IDProducto=" & txtID.txt.Text & " And IDDeposito=" & IDDeposito)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            cbxSucursalDeposito.cbx.Text = oRow("Suc-Dep").ToString
            txtExistenciaMinima.txt.Text = oRow("ExistenciaMinima").ToString
            txtExistenciaCritica.txt.Text = oRow("ExistenciaCritica").ToString

        End If

        ctrError.Clear()

    End Sub

    Sub ProcesarExistencia()

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Seleccion de Deposito
        If IsNumeric(cbxSucursalDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(cbxSucursalDeposito, mensaje)
            ctrError.SetIconAlignment(cbxSucursalDeposito, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Procesar
        'Obtener el IDProducto
        Dim IDProducto As Integer

        IDProducto = txtID.txt.Text

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@IDProducto", IDProducto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", cbxSucursalDeposito.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ExistenciaMinima", txtExistenciaMinima.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ExistenciaCritica", txtExistenciaCritica.ObtenerValor, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpExistenciaDeposito", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            ctrError.Clear()
            ListarExistencias()

        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnActualizarExistencia, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnActualizarExistencia, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnActualizarExistencia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizarExistencia.Click
        ProcesarExistencia()
    End Sub

    Private Sub lvExistencia_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvExistencia.SelectedIndexChanged
        ObtenerInformacionExistencia()
    End Sub

#End Region

    Private Sub cbxTipoProducto_Editar(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Dim frm As New frmTipoProducto
        CSistema.ShowForm(frm, Me)
    End Sub

    Private Sub cbxLinea_Editar(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        'Dim frm As New frm
        'CSistema.ShowForm(frm, Me)
    End Sub

    Private Sub ocxCuenta_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocxCuentaCompra.ItemSeleccionado
        ocxCuentaVenta.Focus()
    End Sub

    Private Sub ocxCuentaVenta_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocxCuentaVenta.ItemSeleccionado
        btnGuardar.Focus()
    End Sub

    Private Sub tabPrecios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tabPrecios.Click

    End Sub
End Class
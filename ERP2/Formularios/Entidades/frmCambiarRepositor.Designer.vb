﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCambiarRepositor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnProcesar = New System.Windows.Forms.Button()
        Me.lblPromotorNuevo = New System.Windows.Forms.Label()
        Me.cbxRepositorNuevo = New ERP.ocxCBX()
        Me.lblPromotor = New System.Windows.Forms.Label()
        Me.cbxRepositorActual = New ERP.ocxCBX()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 135)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(380, 22)
        Me.StatusStrip1.TabIndex = 39
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(293, 95)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 38
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnProcesar
        '
        Me.btnProcesar.Location = New System.Drawing.Point(191, 95)
        Me.btnProcesar.Name = "btnProcesar"
        Me.btnProcesar.Size = New System.Drawing.Size(75, 23)
        Me.btnProcesar.TabIndex = 37
        Me.btnProcesar.Text = "Procesar"
        Me.btnProcesar.UseVisualStyleBackColor = True
        '
        'lblPromotorNuevo
        '
        Me.lblPromotorNuevo.AutoSize = True
        Me.lblPromotorNuevo.Location = New System.Drawing.Point(9, 57)
        Me.lblPromotorNuevo.Name = "lblPromotorNuevo"
        Me.lblPromotorNuevo.Size = New System.Drawing.Size(85, 13)
        Me.lblPromotorNuevo.TabIndex = 30
        Me.lblPromotorNuevo.Text = "Repositor nuevo"
        '
        'cbxRepositorNuevo
        '
        Me.cbxRepositorNuevo.CampoWhere = Nothing
        Me.cbxRepositorNuevo.CargarUnaSolaVez = False
        Me.cbxRepositorNuevo.DataDisplayMember = ""
        Me.cbxRepositorNuevo.DataFilter = Nothing
        Me.cbxRepositorNuevo.DataOrderBy = ""
        Me.cbxRepositorNuevo.DataSource = ""
        Me.cbxRepositorNuevo.DataValueMember = ""
        Me.cbxRepositorNuevo.dtSeleccionado = Nothing
        Me.cbxRepositorNuevo.FormABM = Nothing
        Me.cbxRepositorNuevo.Indicaciones = Nothing
        Me.cbxRepositorNuevo.Location = New System.Drawing.Point(105, 51)
        Me.cbxRepositorNuevo.Name = "cbxRepositorNuevo"
        Me.cbxRepositorNuevo.SeleccionMultiple = False
        Me.cbxRepositorNuevo.SeleccionObligatoria = False
        Me.cbxRepositorNuevo.Size = New System.Drawing.Size(263, 23)
        Me.cbxRepositorNuevo.SoloLectura = False
        Me.cbxRepositorNuevo.TabIndex = 31
        Me.cbxRepositorNuevo.Texto = ""
        '
        'lblPromotor
        '
        Me.lblPromotor.AutoSize = True
        Me.lblPromotor.Location = New System.Drawing.Point(11, 23)
        Me.lblPromotor.Name = "lblPromotor"
        Me.lblPromotor.Size = New System.Drawing.Size(84, 13)
        Me.lblPromotor.TabIndex = 28
        Me.lblPromotor.Text = "Repositor actual"
        '
        'cbxRepositorActual
        '
        Me.cbxRepositorActual.CampoWhere = Nothing
        Me.cbxRepositorActual.CargarUnaSolaVez = False
        Me.cbxRepositorActual.DataDisplayMember = "Nombres"
        Me.cbxRepositorActual.DataFilter = Nothing
        Me.cbxRepositorActual.DataOrderBy = "Nombres"
        Me.cbxRepositorActual.DataSource = "VPromotor"
        Me.cbxRepositorActual.DataValueMember = "ID"
        Me.cbxRepositorActual.dtSeleccionado = Nothing
        Me.cbxRepositorActual.FormABM = Nothing
        Me.cbxRepositorActual.Indicaciones = Nothing
        Me.cbxRepositorActual.Location = New System.Drawing.Point(105, 22)
        Me.cbxRepositorActual.Name = "cbxRepositorActual"
        Me.cbxRepositorActual.SeleccionMultiple = False
        Me.cbxRepositorActual.SeleccionObligatoria = False
        Me.cbxRepositorActual.Size = New System.Drawing.Size(263, 23)
        Me.cbxRepositorActual.SoloLectura = False
        Me.cbxRepositorActual.TabIndex = 29
        Me.cbxRepositorActual.Texto = ""
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmCambiarRepositor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(380, 157)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnProcesar)
        Me.Controls.Add(Me.lblPromotorNuevo)
        Me.Controls.Add(Me.cbxRepositorNuevo)
        Me.Controls.Add(Me.lblPromotor)
        Me.Controls.Add(Me.cbxRepositorActual)
        Me.Name = "frmCambiarRepositor"
        Me.Tag = "frmCambiarRepositor"
        Me.Text = "frmCambiarRepositor"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnProcesar As Button
    Friend WithEvents lblPromotorNuevo As Label
    Friend WithEvents cbxRepositorNuevo As ocxCBX
    Friend WithEvents lblPromotor As Label
    Friend WithEvents cbxRepositorActual As ocxCBX
    Friend WithEvents ctrError As ErrorProvider
End Class

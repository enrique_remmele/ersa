﻿Public Class frmSucursalDeposito

    'CLASES
    Dim CSistema As New CSistema
    Dim Editar As Boolean = False

    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

#Region "SUCURSAL"

    'VARIABLES
    Dim vSucursalNuevo As Boolean
    Dim vControlesSucursal() As Control

    'FUNCIONES
    Sub InicializarSucursal()

        'Variables
        vSucursalNuevo = False

        'RadioButton
        rdbSucursalActivo.Checked = True

        'Funciones
        CargarInformacionSucursal()
        CSistema.InicializaControles(vControlesSucursal)

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnSucursalNuevo, btnSucursalEditar, btnSucursalCancelar, btnSucursalGuardar, btnSucursalEliminar, vControlesSucursal)

        'Focus
        dgvSucursal.Focus()

    End Sub

    Sub CargarInformacionSucursal()

      
        'Cargamos los paises
        CSistema.SqlToComboBox(cbxSucursalPais.cbx, "Select ID, Descripcion From Pais Order By Orden Desc, 2", CadenaConexion)

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControlesSucursal(-1)
        CSistema.CargaControl(vControlesSucursal, txtSucursalDescripcion)
        CSistema.CargaControl(vControlesSucursal, txtSucursalCodigo)
        CSistema.CargaControl(vControlesSucursal, txtSucursalCodigoDistribuidor)
        CSistema.CargaControl(vControlesSucursal, cbxSucursalPais)
        CSistema.CargaControl(vControlesSucursal, cbxSucursalDepartamento)
        CSistema.CargaControl(vControlesSucursal, cbxSucursalCiudad)
        CSistema.CargaControl(vControlesSucursal, txtSucursalDireccion)
        CSistema.CargaControl(vControlesSucursal, txtSucursalTelefono)
        CSistema.CargaControl(vControlesSucursal, txtSucursalReferencia)
        CSistema.CargaControl(vControlesSucursal, txtSucursalCuentaContable)
        CSistema.CargaControl(vControlesSucursal, rdbSucursalActivo)
        CSistema.CargaControl(vControlesSucursal, rdbSucursalDesactivado)
        CSistema.CargaControl(vControlesSucursal, txtNroCasa)

        'Cargamos los paises en el lv
        ListarSucursales()

    End Sub

    Sub ObtenerInformacionSucursal()

        'Validar
        'Si es que se selecciono el registro.
        If dgvSucursal.SelectedRows.Count = 0 Then
            ctrError.SetError(dgvSucursal, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgvSucursal, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnSucursalNuevo, btnSucursalEditar, btnSucursalCancelar, btnSucursalGuardar, btnSucursalEliminar, vControlesSucursal)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = dgvSucursal.SelectedRows(0).Cells("ID").Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select ID, Descripcion, Codigo, CodigoDistribuidor, Pais, Departamento, Ciudad, Direccion, Telefono, Referencia,NroCasa, Estado, CuentaContable  From VSucursal Where ID=" & ID, CadenaConexion)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            CSistema.CargarRowEnControl(oRow, "ID", txtSucursalID)
            CSistema.CargarRowEnControl(oRow, "Descripcion", txtSucursalDescripcion)
            CSistema.CargarRowEnControl(oRow, "Codigo", txtSucursalCodigo)
            CSistema.CargarRowEnControl(oRow, "CodigoDistribuidor", txtSucursalCodigoDistribuidor)
            CSistema.CargarRowEnControl(oRow, "Pais", cbxSucursalPais)
            CSistema.CargarRowEnControl(oRow, "Departamento", cbxSucursalDepartamento)
            CSistema.CargarRowEnControl(oRow, "Ciudad", cbxSucursalCiudad)
            CSistema.CargarRowEnControl(oRow, "Direccion", txtSucursalDireccion)
            CSistema.CargarRowEnControl(oRow, "Telefono", txtSucursalTelefono.txt)
            CSistema.CargarRowEnControl(oRow, "Referencia", txtSucursalReferencia.txt)
            CSistema.CargarRowEnControl(oRow, "CuentaContable", txtSucursalCuentaContable.txt)
            CSistema.CargarRowEnControl(oRow, "NroCasa", txtNroCasa.txt)
            If oRow("Estado") Then
                rdbSucursalActivo.Checked = True
            Else
                rdbSucursalDesactivado.Checked = True
            End If

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnSucursalNuevo, btnSucursalEditar, btnSucursalCancelar, btnSucursalGuardar, btnSucursalEliminar, vControlesSucursal)

        End If

        ctrError.Clear()

    End Sub

    Sub ListarSucursales(Optional ByVal IDSucursal As Integer = 0)

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        CSistema.SqlToDataGrid(dgvSucursal, "Select 'Sucursal'=Descripcion, ID, Codigo, REF, Ciudad From VSucursal Order By Descripcion", CadenaConexion)
        dgvSucursal.Columns("Sucursal").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If dgvSucursal.Columns.Count > 0 Then

            'Esto hacemos para que: 
            '1- Que el ID sea visible en la primera columna
            '2- Y para que cuando el usuario escriba en el lv, el control filtre por su descripcion.
            dgvSucursal.Columns(0).DisplayIndex = 1

            'Ahora seleccionamos automaticamente el registro especificado
            'If dgvSucursal.Items.Count > 0 Then
            '    If IDSucursal = 0 Then
            '        dgvSucursal.Items(0).Selected = True
            '    Else
            '        For i As Integer = 0 To dgvSucursal.Items.Count - 1
            '            If dgvSucursal.Items(i).SubItems(1).Text = IDSucursal Then
            '                dgvSucursal.Items(i).Selected = True
            '                Exit For
            '            End If
            '        Next

            '    End If
            'End If


        End If


        dgvSucursal.Refresh()

    End Sub

    Sub InicializarControlesSucursal()

        CSistema.InicializaControles(vControlesSucursal)

        'TextBox
        txtSucursalDescripcion.txt.Clear()

        'RadioButton
        rdbSucursalActivo.Checked = True

        'Funciones
        If Me.vSucursalNuevo = True Then
            txtSucursalID.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Sucursal", CadenaConexion), Integer)
        Else
            ListarSucursales(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Sucursal", CadenaConexion), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        txtSucursalDescripcion.txt.Focus()


    End Sub

    Sub ProcesarSucursal(ByVal Operacion As CSistema.NUMOperacionesABM)

        'Validar

        'Escritura de la Descripcion
        If txtSucursalDescripcion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar una descripcion valida!"
            ctrError.SetError(txtSucursalDescripcion, mensaje)
            ctrError.SetIconAlignment(txtSucursalDescripcion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Escritura del Codigo
        If txtSucursalCodigo.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar un codigo valido!"
            ctrError.SetError(txtSucursalCodigo, mensaje)
            ctrError.SetIconAlignment(txtSucursalCodigo, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgvSucursal.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgvSucursal, mensaje)
                ctrError.SetIconAlignment(dgvSucursal, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtSucursalID.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtSucursalDescripcion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Codigo", txtSucursalCodigo.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CodigoDistribuidor", txtSucursalCodigoDistribuidor.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDPais", cbxSucursalPais.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepartamento", cbxSucursalDepartamento.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCiudad", cbxSucursalCiudad.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Direccion", txtSucursalDireccion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Telefono", txtSucursalTelefono.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Referencia", txtSucursalReferencia.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaContable", txtSucursalCuentaContable.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbSucursalActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@NroCasa", txtNroCasa.txt.Text, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpSucursal", False, False, MensajeRetorno, "", True, CadenaConexion) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnSucursalNuevo, btnSucursalEditar, btnSucursalCancelar, btnSucursalGuardar, btnSucursalEliminar, vControlesSucursal)
            ListarSucursales(txtSucursalID.Text)
            ctrError.Clear()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnSucursalGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnSucursalGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnSucursalNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSucursalNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnSucursalNuevo, btnSucursalEditar, btnSucursalCancelar, btnSucursalGuardar, btnSucursalEliminar, vControlesSucursal)
        vSucursalNuevo = True
        InicializarControlesSucursal()
    End Sub

    Private Sub btnSucursalEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSucursalEditar.Click

        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnSucursalNuevo, btnSucursalEditar, btnSucursalCancelar, btnSucursalGuardar, btnSucursalEliminar, vControlesSucursal)

        vSucursalNuevo = False

        'Foco
        txtSucursalDescripcion.txt.Focus()
        txtSucursalDescripcion.txt.SelectAll()

    End Sub

    Private Sub btnSucursalCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSucursalCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnSucursalNuevo, btnSucursalEditar, btnSucursalCancelar, btnSucursalGuardar, btnSucursalEliminar, vControlesSucursal)
        vSucursalNuevo = False
        InicializarControlesSucursal()
        ObtenerInformacionSucursal()
    End Sub

    Private Sub btnSucursalGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSucursalGuardar.Click

        If vSucursalNuevo = True Then
            ProcesarSucursal(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            ProcesarSucursal(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnSucursalEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSucursalEliminar.Click

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnSucursalNuevo, btnSucursalEditar, btnSucursalCancelar, btnSucursalGuardar, btnSucursalEliminar, vControlesSucursal)
        ProcesarSucursal(ERP.CSistema.NUMOperacionesABM.DEL)

    End Sub

    Private Sub dgvSucursales_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSucursal.Click
        ObtenerInformacionSucursal()
    End Sub

    Private Sub dgvSucursales_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvSucursal.SelectionChanged
        ObtenerInformacionSucursal()
    End Sub

    Private Sub cbxSucursalPais_PropertyChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxSucursalPais.PropertyChanged
        cbxSucursalDepartamento.cbx.Text = ""

        'Listar Departamentos
        If IsNumeric(cbxSucursalPais.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxSucursalDepartamento.cbx, "Select ID, Descripcion From Departamento Where IDPais=" & cbxSucursalPais.cbx.SelectedValue & " Order By Orden Desc, Descripcion Asc")
    End Sub

    Private Sub cbxSucursalDepartamento_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxSucursalDepartamento.PropertyChanged

        cbxSucursalCiudad.cbx.Text = ""

        If IsNumeric(cbxSucursalDepartamento.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxSucursalCiudad.cbx, "Select ID, Descripcion From Ciudad Where IDDepartamento=" & cbxSucursalDepartamento.cbx.SelectedValue & " Order By Orden Desc, Descripcion Asc")

    End Sub

#End Region

#Region "DEPOSITO"

    'VARIABLES
    Dim vDepositoNuevo As Boolean
    Dim vControlesDeposito() As Control

    'FUNCIONES
    Sub InicializarDeposito()

        'Variables
        vDepositoNuevo = False

        'RadioButton
        rdbDepositoActivo.Checked = True

        'Controles
        txtCuentaContableMercaderia.Conectar()
        txtCuentaContableCombustible.Conectar()

        'Funciones
        CargarInformacionDeposito()
        CSistema.InicializaControles(vControlesDeposito)

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnDepositoNuevo, btnDepositoEditar, btnDepositoCancelar, btnDepositoGuardar, btnDepositoEliminar, vControlesDeposito)

        'Focus
        dgvDeposito.Focus()

    End Sub

    Sub CargarInformacionDeposito()
        CargarComboSucursal()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControlesDeposito(-1)
        CSistema.CargaControl(vControlesDeposito, txtDepositoDescripcion)
        CSistema.CargaControl(vControlesDeposito, cbxDepositoSucursal)
        CSistema.CargaControl(vControlesDeposito, cbxTipoDeposito)
        CSistema.CargaControl(vControlesDeposito, rdbDepositoActivo)
        CSistema.CargaControl(vControlesDeposito, rdbDepositoDesactivado)
        CSistema.CargaControl(vControlesDeposito, txtCuentaContableMercaderia)
        CSistema.CargaControl(vControlesDeposito, txtCuentaContableCombustible)
        CSistema.CargaControl(vControlesDeposito, chkCompra)
        CSistema.CargaControl(vControlesDeposito, chkVenta)
        CSistema.CargaControl(vControlesDeposito, chkDescargaStock)
        CSistema.CargaControl(vControlesDeposito, chkConsumo)
        CSistema.CargaControl(vControlesDeposito, chkMateriaPrima)
        CSistema.CargaControl(vControlesDeposito, chkConsumoInformatica)
        CSistema.CargaControl(vControlesDeposito, cbxSucursalDestino)
        'Cargamos los paises en el lv
        ListarDepositos()

    End Sub

    Private Sub CargarComboSucursal()
        Try
            'Cargamos las Sucursales
            CSistema.SqlToComboBox(cbxDepositoSucursal.cbx, "Select ID, Descripcion + ' ' + Ref From VSucursal Order By 2", CadenaConexion)
            CSistema.SqlToComboBox(cbxSucursalDestino.cbx, "Select ID, Descripcion + ' ' + Ref From VSucursal Order By 2", CadenaConexion)
            CSistema.SqlToComboBox(cbxTipoDeposito.cbx, "Select ID, Descripcion From VTipoDeposito Order By 2", CadenaConexion)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub ObtenerInformacionDeposito()

        'Validar
        'Si es que se selecciono el registro.
        If dgvDeposito.SelectedRows.Count = 0 Then
            ctrError.SetError(dgvDeposito, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgvDeposito, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnDepositoNuevo, btnDepositoEditar, btnDepositoCancelar, btnDepositoGuardar, btnDepositoEliminar, vControlesDeposito)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = dgvDeposito.SelectedRows(0).Cells("ID").Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VDeposito Where ID=" & ID, CadenaConexion)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            CSistema.CargarRowEnControl(oRow, "ID", txtDepositoID)
            CSistema.CargarRowEnControl(oRow, "Deposito", txtDepositoDescripcion)
            CSistema.CargarRowEnControl(oRow, "Sucursal", cbxDepositoSucursal)
            CSistema.CargarRowEnControl(oRow, "TipoDeposito", cbxTipoDeposito)
            'CSistema.CargarRowEnControl(oRow, "Estado", rdbDepositoActivo)
            If oRow("Estado") = "OK" Then
                rdbDepositoActivo.Checked = True
            Else
                rdbDepositoDesactivado.Checked = True
            End If
            CSistema.CargarRowEnControl(oRow, "CuentaContableMercaderia", txtCuentaContableMercaderia)
            CSistema.CargarRowEnControl(oRow, "CuentaContableCombustible", txtCuentaContableCombustible)
            CSistema.CargarRowEnControl(oRow, "SucursalDestinoTransito", cbxSucursalDestino)

            chkVenta.Valor = CBool(oRow("Venta").ToString)
            chkCompra.Valor = CBool(oRow("Compra").ToString)
            chkDescargaStock.Valor = CBool(oRow("DescargaStock").ToString)
            chkConsumo.Valor = CBool(oRow("ConsumoCombustible").ToString)
            chkMateriaPrima.Valor = CBool(oRow("MovimientoMateriaPrima").ToString)
            chkDescargaCompra.Valor = CBool(oRow("DescargaCompra").ToString)
            chkConsumoInformatica.Valor = CBool(oRow("ConsumoInformatica").ToString)

            If Editar = False Then
                'Configuramos los controles ABM como EDITAR
                CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnDepositoNuevo, btnDepositoEditar, btnDepositoCancelar, btnDepositoGuardar, btnDepositoEliminar, vControlesDeposito)
            End If

        End If

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Sub ListarDepositos(Optional ByVal IDDeposito As Integer = 0)

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        CSistema.SqlToDataGrid(dgvDeposito, "Select Deposito, ID, Sucursal From VDeposito Order By Sucursal, Deposito", CadenaConexion)
        dgvDeposito.Columns("Deposito").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If dgvDeposito.Columns.Count > 0 Then

            'Esto hacemos para que: 
            '1- Que el ID sea visible en la primera columna
            '2- Y para que cuando el usuario escriba en el lv, el control filtre por su descripcion.
            dgvDeposito.Columns(0).DisplayIndex = 1

            'Ahora seleccionamos automaticamente el registro especificado
            'If dgvDeposito.Items.Count > 0 Then
            '    If IDDeposito = 0 Then
            '        dgvDeposito.Items(0).Selected = True
            '    Else
            '        For i As Integer = 0 To dgvDeposito.Items.Count - 1
            '            If dgvDeposito.Items(i).SubItems(1).Text = IDDeposito Then
            '                dgvDeposito.Items(i).Selected = True
            '                Exit For
            '            End If
            '        Next

            '    End If
            'End If


        End If


        dgvDeposito.Refresh()

    End Sub

    Sub InicializarControlesDeposito()

        CSistema.InicializaControles(vControlesDeposito)

        'TextBox
        txtDepositoDescripcion.txt.Clear()

        'RadioButton
        rdbDepositoActivo.Checked = True

        'Funciones
        If Me.vDepositoNuevo = True Then
            txtDepositoID.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Deposito", CadenaConexion), Integer)
        Else
            ListarDepositos(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Deposito", CadenaConexion), Integer))
        End If

        'Error
        ctrError.Clear()

        'Estado
        tsslEstado.Text = ""

        'Foco
        cbxDepositoSucursal.cbx.Focus()


    End Sub

    Sub ProcesarDeposito(ByVal Operacion As CSistema.NUMOperacionesABM)

        'Validar

        'Escritura de la Descripcion
        If txtDepositoDescripcion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar una descripcion valida!"
            ctrError.SetError(txtDepositoDescripcion, mensaje)
            ctrError.SetIconAlignment(txtDepositoDescripcion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Sucursal
        If IsNumeric(cbxDepositoSucursal.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Debe seleccionar una sucursal!"
            ctrError.SetError(cbxDepositoSucursal, mensaje)
            ctrError.SetIconAlignment(cbxDepositoSucursal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Tipo de deposito
        If IsNumeric(cbxTipoDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Debe seleccionar un Tipo de deposito!"
            ctrError.SetError(cbxTipoDeposito, mensaje)
            ctrError.SetIconAlignment(cbxTipoDeposito, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Sucursal
        If cbxDepositoSucursal.cbx.Text.Trim = "" Then
            Dim mensaje As String = "Debe seleccionar una sucursal!"
            ctrError.SetError(cbxDepositoSucursal, mensaje)
            ctrError.SetIconAlignment(cbxDepositoSucursal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Tipo de deposito
        If cbxTipoDeposito.cbx.Text.Trim = "" Then
            Dim mensaje As String = "Debe seleccionar un Tipo de deposito!"
            ctrError.SetError(cbxTipoDeposito, mensaje)
            ctrError.SetIconAlignment(cbxTipoDeposito, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgvDeposito.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgvDeposito, mensaje)
                ctrError.SetIconAlignment(dgvDeposito, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtDepositoID.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDepositoDescripcion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxDepositoSucursal.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoDeposito", cbxTipoDeposito.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbDepositoActivo.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaContableMercaderia", txtCuentaContableMercaderia.txtCodigo.GetValue, ParameterDirection.Input)


        If txtCuentaContableCombustible.Enabled Then
            CSistema.SetSQLParameter(param, "@CuentaContableCombustible", txtCuentaContableCombustible.txtCodigo.GetValue, ParameterDirection.Input)
        End If

        If cbxSucursalDestino.Enabled Then
            CSistema.SetSQLParameter(param, "@IDSucursalDestinoTransito", cbxSucursalDestino.cbx, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Compra", chkCompra.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Venta", chkVenta.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DescargaStock", chkDescargaStock.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ConsumoCombustible", chkConsumo.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@MovimientoMateriaPrima", chkMateriaPrima.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DescargaCompra", chkDescargaCompra.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ConsumoInformatica", chkConsumoInformatica.Valor, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpDeposito", False, False, MensajeRetorno, "", True, CadenaConexion) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnDepositoNuevo, btnDepositoEditar, btnDepositoCancelar, btnDepositoGuardar, btnDepositoEliminar, vControlesDeposito)
            ListarDepositos(txtDepositoID.Text)
            ctrError.Clear()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnDepositoGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnDepositoGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnDepositoNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDepositoNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnDepositoNuevo, btnDepositoEditar, btnDepositoCancelar, btnDepositoGuardar, btnDepositoEliminar, vControlesDeposito)
        vDepositoNuevo = True
        CargarComboSucursal()
        InicializarControlesDeposito()
    End Sub

    Private Sub btnDepositoEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDepositoEditar.Click

        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnDepositoNuevo, btnDepositoEditar, btnDepositoCancelar, btnDepositoGuardar, btnDepositoEliminar, vControlesDeposito)
        Editar = True
        ObtenerInformacionDeposito()
        vDepositoNuevo = False
        Editar = False
        'Foco
        txtDepositoDescripcion.txt.Focus()
        txtDepositoDescripcion.txt.SelectAll()

    End Sub

    Private Sub btnDepositoCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDepositoCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnDepositoNuevo, btnDepositoEditar, btnDepositoCancelar, btnDepositoGuardar, btnDepositoEliminar, vControlesDeposito)
        vDepositoNuevo = False
        InicializarControlesDeposito()
        ObtenerInformacionDeposito()
    End Sub

    Private Sub btnDepositoGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDepositoGuardar.Click

        If vDepositoNuevo = True Then
            ProcesarDeposito(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            ProcesarDeposito(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnDepositoEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDepositoEliminar.Click

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnDepositoNuevo, btnDepositoEditar, btnDepositoCancelar, btnDepositoGuardar, btnDepositoEliminar, vControlesDeposito)
        ProcesarDeposito(ERP.CSistema.NUMOperacionesABM.DEL)

    End Sub

    Private Sub dgvDepositoes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDeposito.Click
        ObtenerInformacionDeposito()
    End Sub

    Private Sub dgvDepositoes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvDeposito.SelectionChanged
        ObtenerInformacionDeposito()
    End Sub


#End Region

    Private Sub frmSucursalDeposito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        InicializarSucursal()
        InicializarDeposito()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub chkConsumo_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkConsumo.PropertyChanged
        txtCuentaContableCombustible.Enabled = value
    End Sub

    Private Sub cbxTipoDeposito_PropertyChanged(sender As Object, e As EventArgs) Handles cbxTipoDeposito.PropertyChanged
        If cbxTipoDeposito.cbx.Text = "TRANSITO" Or cbxTipoDeposito.cbx.Text = "TRANSITO DAÑADO" Then
            cbxSucursalDestino.Enabled = True
        Else
            cbxSucursalDestino.Enabled = False
        End If
    End Sub
End Class
﻿Imports ERP.Reporte
Public Class frmChequeraOpcionImprimir

    Dim CReporte As New CReporteChequeras
    Public Id As String
    Public CuentaBancaria As String
    Public Desde As String
    Public Hasta As String
    Public Banco As String

    Private Sub frmChequeraOpcionImprimir_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        CargarInformacion()
    End Sub

    Private Sub CargarInformacion()
        cbxEstados.cbx.Items.Add("Anulado")
        cbxEstados.cbx.Items.Add("En blanco")
        cbxEstados.cbx.Items.Add("Utilizado")
        cbxEstados.cbx.Items.Add("Todos")
        cbxEstados.cbx.SelectedIndex = 3

        cbxTipoInforme.cbx.Items.Add("General")
        cbxTipoInforme.cbx.Items.Add("Unitario")
        cbxTipoInforme.cbx.SelectedIndex = 0

        cbxCantidad.cbx.Items.Add("Todos")
        cbxCantidad.cbx.Items.Add("Con Saldo")
        cbxCantidad.cbx.Items.Add("Sin Saldo")
        cbxCantidad.cbx.SelectedIndex = 0
    End Sub

    Private Sub Listar()

        Dim Where As String = Id & "," & CuentaBancaria & ", '" & cbxEstados.cbx.Text & "'"
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Subtitulo As String = " Desde " & Desde & " Hasta " & Hasta
        Dim Titulo As String = "Chequera " & Banco & " - " & CuentaBancaria

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        CReporte.ListarChequera(frm, Where, "", vgUsuarioIdentificador, Titulo, Subtitulo, "NroCheque", True)

    End Sub

    Private Sub ListarGeneral()

        Dim Where As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Subtitulo As String = ""
        Dim Titulo As String = "Saldo de Chequeras "
        Dim whereDetalle As String = ""

        If cbxCantidad.cbx.SelectedIndex = 1 Then
            whereDetalle = " Saldo > 0"
        ElseIf cbxCantidad.cbx.SelectedIndex = 2 Then
            whereDetalle = " Saldo = 0"
        End If

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        CReporte.ListarChequeraTotal(frm, Where, whereDetalle, vgUsuarioIdentificador, Titulo, Subtitulo, "", True)

    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Close()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If cbxTipoInforme.cbx.SelectedIndex = 1 Then
            Listar()
        Else
            ListarGeneral()
        End If

    End Sub
End Class
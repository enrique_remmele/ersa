﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCambiarVendedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.lblVendedorNuevo = New System.Windows.Forms.Label()
        Me.lblFechaDesde = New System.Windows.Forms.Label()
        Me.lblFechaHasta = New System.Windows.Forms.Label()
        Me.btnProcesar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.chkActualizarVenta = New ERP.ocxCHK()
        Me.txtFechaHasta = New ERP.ocxTXTDate()
        Me.cbxVendedorNuevo = New ERP.ocxCBX()
        Me.txtFechaDesde = New ERP.ocxTXTDate()
        Me.cbxVendedorActual = New ERP.ocxCBX()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(12, 22)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(85, 13)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Text = "Vendedor actual"
        '
        'lblVendedorNuevo
        '
        Me.lblVendedorNuevo.AutoSize = True
        Me.lblVendedorNuevo.Location = New System.Drawing.Point(10, 56)
        Me.lblVendedorNuevo.Name = "lblVendedorNuevo"
        Me.lblVendedorNuevo.Size = New System.Drawing.Size(86, 13)
        Me.lblVendedorNuevo.TabIndex = 2
        Me.lblVendedorNuevo.Text = "Vendedor nuevo"
        '
        'lblFechaDesde
        '
        Me.lblFechaDesde.AutoSize = True
        Me.lblFechaDesde.Location = New System.Drawing.Point(7, 128)
        Me.lblFechaDesde.Name = "lblFechaDesde"
        Me.lblFechaDesde.Size = New System.Drawing.Size(71, 13)
        Me.lblFechaDesde.TabIndex = 5
        Me.lblFechaDesde.Text = "Fecha Desde"
        '
        'lblFechaHasta
        '
        Me.lblFechaHasta.AutoSize = True
        Me.lblFechaHasta.Location = New System.Drawing.Point(170, 128)
        Me.lblFechaHasta.Name = "lblFechaHasta"
        Me.lblFechaHasta.Size = New System.Drawing.Size(35, 13)
        Me.lblFechaHasta.TabIndex = 7
        Me.lblFechaHasta.Text = "Hasta"
        '
        'btnProcesar
        '
        Me.btnProcesar.Location = New System.Drawing.Point(188, 169)
        Me.btnProcesar.Name = "btnProcesar"
        Me.btnProcesar.Size = New System.Drawing.Size(75, 23)
        Me.btnProcesar.TabIndex = 9
        Me.btnProcesar.Text = "Procesar"
        Me.btnProcesar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(290, 169)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 10
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 194)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(380, 22)
        Me.StatusStrip1.TabIndex = 27
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'chkActualizarVenta
        '
        Me.chkActualizarVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkActualizarVenta.Color = System.Drawing.Color.Empty
        Me.chkActualizarVenta.Location = New System.Drawing.Point(10, 94)
        Me.chkActualizarVenta.Name = "chkActualizarVenta"
        Me.chkActualizarVenta.Size = New System.Drawing.Size(263, 21)
        Me.chkActualizarVenta.SoloLectura = False
        Me.chkActualizarVenta.TabIndex = 4
        Me.chkActualizarVenta.Texto = "Actualizar Venta"
        Me.chkActualizarVenta.Valor = False
        '
        'txtFechaHasta
        '
        Me.txtFechaHasta.AñoFecha = 0
        Me.txtFechaHasta.Color = System.Drawing.Color.Empty
        Me.txtFechaHasta.Fecha = New Date(CType(0, Long))
        Me.txtFechaHasta.Location = New System.Drawing.Point(205, 124)
        Me.txtFechaHasta.MesFecha = 0
        Me.txtFechaHasta.Name = "txtFechaHasta"
        Me.txtFechaHasta.PermitirNulo = False
        Me.txtFechaHasta.Size = New System.Drawing.Size(96, 21)
        Me.txtFechaHasta.SoloLectura = False
        Me.txtFechaHasta.TabIndex = 8
        '
        'cbxVendedorNuevo
        '
        Me.cbxVendedorNuevo.CampoWhere = Nothing
        Me.cbxVendedorNuevo.CargarUnaSolaVez = False
        Me.cbxVendedorNuevo.DataDisplayMember = "Nombres"
        Me.cbxVendedorNuevo.DataFilter = Nothing
        Me.cbxVendedorNuevo.DataOrderBy = "Nombres"
        Me.cbxVendedorNuevo.DataSource = "VVendedor"
        Me.cbxVendedorNuevo.DataValueMember = "ID"
        Me.cbxVendedorNuevo.dtSeleccionado = Nothing
        Me.cbxVendedorNuevo.FormABM = Nothing
        Me.cbxVendedorNuevo.Indicaciones = Nothing
        Me.cbxVendedorNuevo.Location = New System.Drawing.Point(106, 50)
        Me.cbxVendedorNuevo.Name = "cbxVendedorNuevo"
        Me.cbxVendedorNuevo.SeleccionMultiple = False
        Me.cbxVendedorNuevo.SeleccionObligatoria = False
        Me.cbxVendedorNuevo.Size = New System.Drawing.Size(263, 23)
        Me.cbxVendedorNuevo.SoloLectura = False
        Me.cbxVendedorNuevo.TabIndex = 3
        Me.cbxVendedorNuevo.Texto = ""
        '
        'txtFechaDesde
        '
        Me.txtFechaDesde.AñoFecha = 0
        Me.txtFechaDesde.Color = System.Drawing.Color.Empty
        Me.txtFechaDesde.Fecha = New Date(CType(0, Long))
        Me.txtFechaDesde.Location = New System.Drawing.Point(78, 124)
        Me.txtFechaDesde.MesFecha = 0
        Me.txtFechaDesde.Name = "txtFechaDesde"
        Me.txtFechaDesde.PermitirNulo = False
        Me.txtFechaDesde.Size = New System.Drawing.Size(92, 21)
        Me.txtFechaDesde.SoloLectura = False
        Me.txtFechaDesde.TabIndex = 6
        '
        'cbxVendedorActual
        '
        Me.cbxVendedorActual.CampoWhere = Nothing
        Me.cbxVendedorActual.CargarUnaSolaVez = False
        Me.cbxVendedorActual.DataDisplayMember = ""
        Me.cbxVendedorActual.DataFilter = Nothing
        Me.cbxVendedorActual.DataOrderBy = ""
        Me.cbxVendedorActual.DataSource = ""
        Me.cbxVendedorActual.DataValueMember = ""
        Me.cbxVendedorActual.dtSeleccionado = Nothing
        Me.cbxVendedorActual.FormABM = Nothing
        Me.cbxVendedorActual.Indicaciones = Nothing
        Me.cbxVendedorActual.Location = New System.Drawing.Point(106, 21)
        Me.cbxVendedorActual.Name = "cbxVendedorActual"
        Me.cbxVendedorActual.SeleccionMultiple = False
        Me.cbxVendedorActual.SeleccionObligatoria = False
        Me.cbxVendedorActual.Size = New System.Drawing.Size(263, 23)
        Me.cbxVendedorActual.SoloLectura = False
        Me.cbxVendedorActual.TabIndex = 0
        Me.cbxVendedorActual.Texto = ""
        '
        'frmCambiarVendedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(380, 216)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.chkActualizarVenta)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnProcesar)
        Me.Controls.Add(Me.lblFechaHasta)
        Me.Controls.Add(Me.lblFechaDesde)
        Me.Controls.Add(Me.txtFechaHasta)
        Me.Controls.Add(Me.lblVendedorNuevo)
        Me.Controls.Add(Me.cbxVendedorNuevo)
        Me.Controls.Add(Me.lblCliente)
        Me.Controls.Add(Me.txtFechaDesde)
        Me.Controls.Add(Me.cbxVendedorActual)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCambiarVendedor"
        Me.Tag = "frmCambiarVendedor"
        Me.Text = "Cambiar Vendedor"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxVendedorActual As ERP.ocxCBX
    Friend WithEvents txtFechaDesde As ERP.ocxTXTDate
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lblVendedorNuevo As System.Windows.Forms.Label
    Friend WithEvents cbxVendedorNuevo As ERP.ocxCBX
    Friend WithEvents txtFechaHasta As ERP.ocxTXTDate
    Friend WithEvents lblFechaDesde As System.Windows.Forms.Label
    Friend WithEvents lblFechaHasta As System.Windows.Forms.Label
    Friend WithEvents btnProcesar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents chkActualizarVenta As ERP.ocxCHK
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
End Class

﻿Public Class frmPuntoExpedicion

    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control
    Dim dtSucursal As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        'Funciones
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Focus
        dgvLista.Focus()

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, txtCantidadPorTalonario)
        CSistema.CargaControl(vControles, txtDescripcion)
        CSistema.CargaControl(vControles, txtTimbrado)
        CSistema.CargaControl(vControles, txtVencimiento)
        CSistema.CargaControl(vControles, txtDescripcion)
        CSistema.CargaControl(vControles, txtNumeracionDesde)
        CSistema.CargaControl(vControles, txtNumeracionHasta)
        CSistema.CargaControl(vControles, txtReferencia)
        CSistema.CargaControl(vControles, txtSucursal)
        CSistema.CargaControl(vControles, rdbActivo)
        CSistema.CargaControl(vControles, rdbDesactivado)
        CSistema.CargaControl(vControles, chkFacturaDirecta)
        CSistema.CargaControl(vControles, cbxDeposito)
        CSistema.CargaControl(vControles, txtVencimiento)

        'Tipos de Comprobantes
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, 'Tipo'=Codigo + ' - ' + Descripcion   From VTipoComprobante Where Proveedor='False' Order By 2")
        CSistema.SqlToComboBox(cbxTipoComprobanteConsulta.cbx, "Select ID, 'Tipo'=Codigo + ' - ' + Descripcion   From VTipoComprobante Where Proveedor='False' Order By 2")

        'Sucursales
        dtSucursal = CSistema.ExecuteToDataTable("Select ID, Referencia, Codigo From Sucursal Order By 2").Copy
        CSistema.SqlToComboBox(cbxSucursal.cbx, dtSucursal)

        'Cargamos los registos en el lv
        Listar()

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If dgvLista.SelectedRows.Count = 0 Then
            ctrError.SetError(dgvLista, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgvLista, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = dgvLista.SelectedRows(0).Cells(0).Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VPuntoExpedicion Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            cbxTipoComprobante.SelectedValue(oRow("IDTipoComprobante").ToString)
            cbxSucursal.cbx.Text = oRow("ReferenciaSucursal").ToString
            txtSucursal.txt.Text = oRow("Sucursal").ToString
            txtReferencia.txt.Text = oRow("ReferenciaPunto").ToString
            txtDescripcion.txt.Text = oRow("Descripcion").ToString
            txtTimbrado.txt.Text = oRow("Timbrado").ToString
            txtVencimiento.SetValue(CDate(oRow("Vencimiento").ToString))
            txtNumeracionDesde.txt.Text = oRow("NumeracionDesde").ToString
            txtNumeracionHasta.txt.Text = oRow("NumeracionHasta").ToString
            txtCantidadPorTalonario.txt.Text = oRow("CantidadPorTalonario").ToString
            rdbActivo.Checked = oRow("Estado")
            rdbDesactivado.Checked = Not oRow("Estado")
            chkFacturaDirecta.Valor = CBool(oRow("VentaDirecta"))
            'SC: 25-09-2021 Para Obtener Timbrado de Externos - UNISAL
            'chkExterno.Valor = CBool(oRow("Externo"))
            cbxDeposito.cbx.Text = oRow("DepositoVentaDirecta").ToString

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal Where As String = "")

        CSistema.SqlToDataGrid(dgvLista, "Select ID, Timbrado, Descripcion, 'Tipo Comprobante'=TipoComprobante, 'Prox. Comp.'=ProximoComprobante From VPuntoExpedicion " & Where & " Order By Descripcion ")

        'Formato
        dgvLista.Columns("Timbrado").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        ' txtCantidad.SetValue(dgvLista.RowCount)

    End Sub

    Sub InicializarControles()

        'TextBox
        CSistema.InicializaControles(vControles)

        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From PuntoExpedicion"), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From PuntoExpedicion"), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        cbxTipoComprobante.cbx.Focus()


    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        If Operacion <> ERP.CSistema.NUMOperacionesABM.DEL Then

            'Seleccion correcta del Tipo de Comprobante
            If IsNumeric(cbxTipoComprobante.cbx.SelectedValue) = False Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(cbxTipoComprobante, mensaje)
                ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'Sucursal
            If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
                Dim mensaje As String = "Seleccione correctamente la sucursal!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'Referencia
            If txtReferencia.txt.Text.Trim.Length = 0 Then
                Dim mensaje As String = "Debe ingresar una referencia valida!"
                ctrError.SetError(txtReferencia, mensaje)
                ctrError.SetIconAlignment(txtReferencia, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'Escritura de la Descripcion
            If txtDescripcion.txt.Text.Trim.Length = 0 Then
                Dim mensaje As String = "Debe ingresar una descripcion valida!"
                ctrError.SetError(txtDescripcion, mensaje)
                ctrError.SetIconAlignment(txtDescripcion, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'Numeraciones
            If CDec(txtNumeracionDesde.ObtenerValor) >= CDec(txtNumeracionHasta.ObtenerValor) Then
                Dim mensaje As String = "El rango de numeracion no es valido!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'Cantidad por Talonario
            If CDec(txtCantidadPorTalonario.ObtenerValor) = 0 Then
                Dim mensaje As String = "La cantidad por talonarios no es valido!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

        End If

       

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Referencia", txtReferencia.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Timbrado", txtTimbrado.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Vencimiento", CSistema.FormatoFechaBaseDatos(txtVencimiento.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NumeracionDesde", txtNumeracionDesde.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NumeracionHasta", txtNumeracionHasta.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CantidadPorTalonario", txtCantidadPorTalonario.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@VentaDirecta", chkFacturaDirecta.Valor, ParameterDirection.Input)
        'FA 19/09/2022 
        'CSistema.SetSQLParameter(param, "@InicioVigencia", CSistema.FormatoFechaBaseDatos(txtInicioVigencia.GetValue, True, False), ParameterDirection.Input)
        'SC: 25-09-2021 -Proyecto Unisal
        'CSistema.SetSQLParameter(param, "@Externo", chkExterno.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", cbxDeposito.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpPuntoExpedicion", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            Listar(" Where ID=" & txtID.txt.Text)
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        txtTimbrado.Focus()
        txtTimbrado.txt.SelectAll()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub dgvLista_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLista.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub frmPuntoExpedicion_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmPuntoExpedicion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged

        txtSucursal.txt.Clear()

        If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        For Each oRow As DataRow In dtSucursal.Select(" ID=" & cbxSucursal.cbx.SelectedValue)
            txtSucursal.txt.Text = oRow("Codigo").ToString
        Next

    End Sub

    Private Sub cbxTipoComprobanteConsulta_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxTipoComprobanteConsulta.PropertyChanged
        Listar(" Where IDTipoComprobante = '" & cbxTipoComprobanteConsulta.GetValue & "' ")
    End Sub

    Private Sub lklListarTodos_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklListarTodos.LinkClicked
        Listar("")
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub chkFacturaDirecta_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFacturaDirecta.PropertyChanged
        cbxDeposito.Enabled = value
        If value Then

            If cbxSucursal.cbx.Text = "" Then
                MessageBox.Show("Seleccione una sucursal", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                cbxSucursal.Focus()
                Exit Sub
            End If

            CSistema.SqlToComboBox(cbxDeposito, "Select ID,Descripcion from Deposito where Estado = 1 and Venta = 1 and IDSucursal = " & cbxSucursal.cbx.SelectedValue)
        End If

    End Sub
End Class
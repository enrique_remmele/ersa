﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdministrarCuentasContablesObligatoriasTipoProducto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTipoProducto = New System.Windows.Forms.Label()
        Me.chkCuentaContableCompra = New ERP.ocxCHK()
        Me.chkCuentaContableVenta = New ERP.ocxCHK()
        Me.chkCuentaContableCosto = New ERP.ocxCHK()
        Me.chkCuentaContableDeudor = New ERP.ocxCHK()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.lblUsuario = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblTipoProducto
        '
        Me.lblTipoProducto.AutoSize = True
        Me.lblTipoProducto.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoProducto.Location = New System.Drawing.Point(13, 13)
        Me.lblTipoProducto.Name = "lblTipoProducto"
        Me.lblTipoProducto.Size = New System.Drawing.Size(226, 20)
        Me.lblTipoProducto.TabIndex = 0
        Me.lblTipoProducto.Text = "Productos Molinos de Trigo"
        '
        'chkCuentaContableCompra
        '
        Me.chkCuentaContableCompra.BackColor = System.Drawing.Color.Transparent
        Me.chkCuentaContableCompra.Color = System.Drawing.Color.Empty
        Me.chkCuentaContableCompra.Location = New System.Drawing.Point(12, 50)
        Me.chkCuentaContableCompra.Name = "chkCuentaContableCompra"
        Me.chkCuentaContableCompra.Size = New System.Drawing.Size(144, 21)
        Me.chkCuentaContableCompra.SoloLectura = False
        Me.chkCuentaContableCompra.TabIndex = 1
        Me.chkCuentaContableCompra.Texto = "Cuenta Contable Compra"
        Me.chkCuentaContableCompra.Valor = False
        '
        'chkCuentaContableVenta
        '
        Me.chkCuentaContableVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkCuentaContableVenta.Color = System.Drawing.Color.Empty
        Me.chkCuentaContableVenta.Location = New System.Drawing.Point(162, 50)
        Me.chkCuentaContableVenta.Name = "chkCuentaContableVenta"
        Me.chkCuentaContableVenta.Size = New System.Drawing.Size(144, 21)
        Me.chkCuentaContableVenta.SoloLectura = False
        Me.chkCuentaContableVenta.TabIndex = 2
        Me.chkCuentaContableVenta.Texto = "Cuenta Contable Venta"
        Me.chkCuentaContableVenta.Valor = False
        '
        'chkCuentaContableCosto
        '
        Me.chkCuentaContableCosto.BackColor = System.Drawing.Color.Transparent
        Me.chkCuentaContableCosto.Color = System.Drawing.Color.Empty
        Me.chkCuentaContableCosto.Location = New System.Drawing.Point(12, 77)
        Me.chkCuentaContableCosto.Name = "chkCuentaContableCosto"
        Me.chkCuentaContableCosto.Size = New System.Drawing.Size(144, 21)
        Me.chkCuentaContableCosto.SoloLectura = False
        Me.chkCuentaContableCosto.TabIndex = 3
        Me.chkCuentaContableCosto.Texto = "Cuenta Contable Costo"
        Me.chkCuentaContableCosto.Valor = False
        '
        'chkCuentaContableDeudor
        '
        Me.chkCuentaContableDeudor.BackColor = System.Drawing.Color.Transparent
        Me.chkCuentaContableDeudor.Color = System.Drawing.Color.Empty
        Me.chkCuentaContableDeudor.Location = New System.Drawing.Point(162, 77)
        Me.chkCuentaContableDeudor.Name = "chkCuentaContableDeudor"
        Me.chkCuentaContableDeudor.Size = New System.Drawing.Size(144, 21)
        Me.chkCuentaContableDeudor.SoloLectura = False
        Me.chkCuentaContableDeudor.TabIndex = 4
        Me.chkCuentaContableDeudor.Texto = "Cuenta Contable Deudor"
        Me.chkCuentaContableDeudor.Valor = False
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(162, 138)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 5
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'lblUsuario
        '
        Me.lblUsuario.Location = New System.Drawing.Point(9, 120)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(147, 41)
        Me.lblUsuario.TabIndex = 6
        Me.lblUsuario.Text = "Ultimo Usuario en Modificar: Leonardo Recalde - Fecha: 26/08/2019"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(243, 138)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 7
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'frmAdministrarCuentasContablesObligatoriasTipoProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(327, 170)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.lblUsuario)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.chkCuentaContableDeudor)
        Me.Controls.Add(Me.chkCuentaContableCosto)
        Me.Controls.Add(Me.chkCuentaContableVenta)
        Me.Controls.Add(Me.chkCuentaContableCompra)
        Me.Controls.Add(Me.lblTipoProducto)
        Me.Name = "frmAdministrarCuentasContablesObligatoriasTipoProducto"
        Me.Text = "frmAdministrarCuentasContablesObligatoriasTipoProducto"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblTipoProducto As Label
    Friend WithEvents chkCuentaContableCompra As ocxCHK
    Friend WithEvents chkCuentaContableVenta As ocxCHK
    Friend WithEvents chkCuentaContableCosto As ocxCHK
    Friend WithEvents chkCuentaContableDeudor As ocxCHK
    Friend WithEvents btnGuardar As Button
    Friend WithEvents lblUsuario As Label
    Friend WithEvents btnCancelar As Button
End Class

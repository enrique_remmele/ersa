﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProductoSeleccionMultiple
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProductoSeleccionMultiple))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsslCantidad = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripDropDownButton1 = New System.Windows.Forms.ToolStripDropDownButton()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.rdbDescripcion = New System.Windows.Forms.RadioButton()
        Me.rdbCodigoBarra = New System.Windows.Forms.RadioButton()
        Me.rdbReferencia = New System.Windows.Forms.RadioButton()
        Me.rdbID = New System.Windows.Forms.RadioButton()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.chkActivo = New ERP.ocxCHK()
        Me.chkExistencia = New ERP.ocxCHK()
        Me.chkVendible = New ERP.ocxCHK()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.dgw, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.StatusStrip1, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(908, 580)
        Me.TableLayoutPanel1.TabIndex = 3
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Right
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(742, 551)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(163, 26)
        Me.FlowLayoutPanel2.TabIndex = 16
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(3, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 21)
        Me.btnAceptar.TabIndex = 5
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancelar.Location = New System.Drawing.Point(84, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 21)
        Me.btnCancelar.TabIndex = 6
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgw.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgw.Location = New System.Drawing.Point(3, 33)
        Me.dgw.MultiSelect = False
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(902, 486)
        Me.dgw.StandardTab = True
        Me.dgw.TabIndex = 15
        Me.dgw.Tag = "frmRemuneracion"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.tsslCantidad, Me.ToolStripDropDownButton1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 526)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(908, 22)
        Me.StatusStrip1.TabIndex = 3
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(58, 17)
        Me.ToolStripStatusLabel1.Text = "Cantidad:"
        '
        'tsslCantidad
        '
        Me.tsslCantidad.Name = "tsslCantidad"
        Me.tsslCantidad.Size = New System.Drawing.Size(13, 17)
        Me.tsslCantidad.Text = "0"
        '
        'ToolStripDropDownButton1
        '
        Me.ToolStripDropDownButton1.Image = CType(resources.GetObject("ToolStripDropDownButton1.Image"), System.Drawing.Image)
        Me.ToolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButton1.Name = "ToolStripDropDownButton1"
        Me.ToolStripDropDownButton1.Size = New System.Drawing.Size(110, 20)
        Me.ToolStripDropDownButton1.Text = "Existencia (F1)"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtDescripcion)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbDescripcion)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbCodigoBarra)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbReferencia)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbID)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnListar)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkActivo)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkExistencia)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkVendible)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(902, 24)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(3, 3)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(192, 20)
        Me.txtDescripcion.TabIndex = 0
        '
        'rdbDescripcion
        '
        Me.rdbDescripcion.AutoSize = True
        Me.rdbDescripcion.Location = New System.Drawing.Point(201, 3)
        Me.rdbDescripcion.Name = "rdbDescripcion"
        Me.rdbDescripcion.Size = New System.Drawing.Size(81, 17)
        Me.rdbDescripcion.TabIndex = 1
        Me.rdbDescripcion.TabStop = True
        Me.rdbDescripcion.Text = "Descripcion"
        Me.rdbDescripcion.UseVisualStyleBackColor = True
        '
        'rdbCodigoBarra
        '
        Me.rdbCodigoBarra.AutoSize = True
        Me.rdbCodigoBarra.Location = New System.Drawing.Point(288, 3)
        Me.rdbCodigoBarra.Name = "rdbCodigoBarra"
        Me.rdbCodigoBarra.Size = New System.Drawing.Size(101, 17)
        Me.rdbCodigoBarra.TabIndex = 2
        Me.rdbCodigoBarra.TabStop = True
        Me.rdbCodigoBarra.Text = "Codigo de Barra"
        Me.rdbCodigoBarra.UseVisualStyleBackColor = True
        '
        'rdbReferencia
        '
        Me.rdbReferencia.AutoSize = True
        Me.rdbReferencia.Location = New System.Drawing.Point(395, 3)
        Me.rdbReferencia.Name = "rdbReferencia"
        Me.rdbReferencia.Size = New System.Drawing.Size(77, 17)
        Me.rdbReferencia.TabIndex = 3
        Me.rdbReferencia.TabStop = True
        Me.rdbReferencia.Text = "Referencia"
        Me.rdbReferencia.UseVisualStyleBackColor = True
        '
        'rdbID
        '
        Me.rdbID.AutoSize = True
        Me.rdbID.Location = New System.Drawing.Point(478, 3)
        Me.rdbID.Name = "rdbID"
        Me.rdbID.Size = New System.Drawing.Size(36, 17)
        Me.rdbID.TabIndex = 4
        Me.rdbID.TabStop = True
        Me.rdbID.Text = "ID"
        Me.rdbID.UseVisualStyleBackColor = True
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(520, 3)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(75, 21)
        Me.btnListar.TabIndex = 5
        Me.btnListar.Text = "&Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'chkActivo
        '
        Me.chkActivo.BackColor = System.Drawing.Color.Transparent
        Me.chkActivo.Color = System.Drawing.Color.Empty
        Me.chkActivo.Location = New System.Drawing.Point(613, 3)
        Me.chkActivo.Margin = New System.Windows.Forms.Padding(15, 3, 3, 3)
        Me.chkActivo.Name = "chkActivo"
        Me.chkActivo.Size = New System.Drawing.Size(85, 20)
        Me.chkActivo.SoloLectura = False
        Me.chkActivo.TabIndex = 11
        Me.chkActivo.Texto = "Sólo Activos:"
        Me.chkActivo.Valor = False
        '
        'chkExistencia
        '
        Me.chkExistencia.BackColor = System.Drawing.Color.Transparent
        Me.chkExistencia.Color = System.Drawing.Color.Empty
        Me.chkExistencia.Location = New System.Drawing.Point(716, 3)
        Me.chkExistencia.Margin = New System.Windows.Forms.Padding(15, 3, 3, 3)
        Me.chkExistencia.Name = "chkExistencia"
        Me.chkExistencia.Size = New System.Drawing.Size(93, 20)
        Me.chkExistencia.SoloLectura = False
        Me.chkExistencia.TabIndex = 12
        Me.chkExistencia.Texto = "Con Existencia"
        Me.chkExistencia.Valor = False
        '
        'chkVendible
        '
        Me.chkVendible.BackColor = System.Drawing.Color.Transparent
        Me.chkVendible.Color = System.Drawing.Color.Empty
        Me.chkVendible.Location = New System.Drawing.Point(15, 30)
        Me.chkVendible.Margin = New System.Windows.Forms.Padding(15, 3, 3, 3)
        Me.chkVendible.Name = "chkVendible"
        Me.chkVendible.Size = New System.Drawing.Size(85, 20)
        Me.chkVendible.SoloLectura = False
        Me.chkVendible.TabIndex = 13
        Me.chkVendible.Texto = "Vendible:"
        Me.chkVendible.Valor = False
        '
        'frmProductoSeleccionMultiple
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(908, 580)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmProductoSeleccionMultiple"
        Me.Text = "frmProductoSeleccionMultiple"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsslCantidad As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripDropDownButton1 As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents rdbDescripcion As System.Windows.Forms.RadioButton
    Friend WithEvents rdbCodigoBarra As System.Windows.Forms.RadioButton
    Friend WithEvents rdbReferencia As System.Windows.Forms.RadioButton
    Friend WithEvents rdbID As System.Windows.Forms.RadioButton
    Friend WithEvents btnListar As System.Windows.Forms.Button
    Friend WithEvents chkActivo As ERP.ocxCHK
    Friend WithEvents chkExistencia As ERP.ocxCHK
    Friend WithEvents chkVendible As ERP.ocxCHK
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
End Class

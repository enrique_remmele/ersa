﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCreditoCliente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblID = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.PnlCondicionVenta = New System.Windows.Forms.Panel()
        Me.rdbContado = New System.Windows.Forms.RadioButton()
        Me.rdbCredito = New System.Windows.Forms.RadioButton()
        Me.lblCondicionVenta = New System.Windows.Forms.Label()
        Me.txtReferencia = New ERP.ocxTXTString()
        Me.txtRUC = New ERP.ocxTXTString()
        Me.txtRazonSocial = New ERP.ocxTXTString()
        Me.OcxClientesMapa1 = New ERP.ocxClientesMapa()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblCelular = New System.Windows.Forms.Label()
        Me.txtCelulares = New ERP.ocxTXTString()
        Me.cbxEstado = New ERP.ocxCBX()
        Me.txtTelefonos = New ERP.ocxTXTString()
        Me.txtDireccion = New ERP.ocxTXTString()
        Me.txtNombreFantasia = New ERP.ocxTXTString()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.lblTelefonos = New System.Windows.Forms.Label()
        Me.lblDireccion = New System.Windows.Forms.Label()
        Me.lblNombreFantasia = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.chkBoleta = New ERP.ocxCHK()
        Me.lklVerCatastro = New System.Windows.Forms.LinkLabel()
        Me.lvlSaldo = New System.Windows.Forms.Label()
        Me.lblDeuda = New System.Windows.Forms.Label()
        Me.lklRecalcularCredito = New System.Windows.Forms.LinkLabel()
        Me.lblPlazoCheque = New System.Windows.Forms.Label()
        Me.lblPlazoCobro = New System.Windows.Forms.Label()
        Me.lblPlazoCredito = New System.Windows.Forms.Label()
        Me.lblDescuentoSimbolo = New System.Windows.Forms.Label()
        Me.lblDesuento = New System.Windows.Forms.Label()
        Me.lblLimiteCredito = New System.Windows.Forms.Label()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.txtDeuda = New ERP.ocxTXTNumeric()
        Me.txtPlazoCheque = New ERP.ocxTXTNumeric()
        Me.txtPlazoCobro = New ERP.ocxTXTNumeric()
        Me.txtPlazoCredito = New ERP.ocxTXTNumeric()
        Me.txtDescuento = New ERP.ocxTXTNumeric()
        Me.txtLimiteCredito = New ERP.ocxTXTNumeric()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel1.SuspendLayout()
        Me.PnlCondicionVenta.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.lblID)
        Me.Panel1.Controls.Add(Me.txtID)
        Me.Panel1.Controls.Add(Me.PnlCondicionVenta)
        Me.Panel1.Controls.Add(Me.lblCondicionVenta)
        Me.Panel1.Controls.Add(Me.txtReferencia)
        Me.Panel1.Controls.Add(Me.txtRUC)
        Me.Panel1.Controls.Add(Me.txtRazonSocial)
        Me.Panel1.Controls.Add(Me.OcxClientesMapa1)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.lblCelular)
        Me.Panel1.Controls.Add(Me.txtCelulares)
        Me.Panel1.Controls.Add(Me.cbxEstado)
        Me.Panel1.Controls.Add(Me.txtTelefonos)
        Me.Panel1.Controls.Add(Me.txtDireccion)
        Me.Panel1.Controls.Add(Me.txtNombreFantasia)
        Me.Panel1.Controls.Add(Me.lblEstado)
        Me.Panel1.Controls.Add(Me.lblTelefonos)
        Me.Panel1.Controls.Add(Me.lblDireccion)
        Me.Panel1.Controls.Add(Me.lblNombreFantasia)
        Me.Panel1.Controls.Add(Me.StatusStrip1)
        Me.Panel1.Controls.Add(Me.btnGuardar)
        Me.Panel1.Controls.Add(Me.chkBoleta)
        Me.Panel1.Controls.Add(Me.lklVerCatastro)
        Me.Panel1.Controls.Add(Me.lvlSaldo)
        Me.Panel1.Controls.Add(Me.lblDeuda)
        Me.Panel1.Controls.Add(Me.lklRecalcularCredito)
        Me.Panel1.Controls.Add(Me.lblPlazoCheque)
        Me.Panel1.Controls.Add(Me.lblPlazoCobro)
        Me.Panel1.Controls.Add(Me.lblPlazoCredito)
        Me.Panel1.Controls.Add(Me.lblDescuentoSimbolo)
        Me.Panel1.Controls.Add(Me.lblDesuento)
        Me.Panel1.Controls.Add(Me.lblLimiteCredito)
        Me.Panel1.Controls.Add(Me.txtSaldo)
        Me.Panel1.Controls.Add(Me.txtDeuda)
        Me.Panel1.Controls.Add(Me.txtPlazoCheque)
        Me.Panel1.Controls.Add(Me.txtPlazoCobro)
        Me.Panel1.Controls.Add(Me.txtPlazoCredito)
        Me.Panel1.Controls.Add(Me.txtDescuento)
        Me.Panel1.Controls.Add(Me.txtLimiteCredito)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(765, 616)
        Me.Panel1.TabIndex = 1
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblID.Location = New System.Drawing.Point(507, 29)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(43, 13)
        Me.lblID.TabIndex = 81
        Me.lblID.Text = "Código:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(567, 26)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(52, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 80
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'PnlCondicionVenta
        '
        Me.PnlCondicionVenta.Controls.Add(Me.rdbContado)
        Me.PnlCondicionVenta.Controls.Add(Me.rdbCredito)
        Me.PnlCondicionVenta.Location = New System.Drawing.Point(130, 122)
        Me.PnlCondicionVenta.Name = "PnlCondicionVenta"
        Me.PnlCondicionVenta.Size = New System.Drawing.Size(135, 24)
        Me.PnlCondicionVenta.TabIndex = 79
        '
        'rdbContado
        '
        Me.rdbContado.AutoSize = True
        Me.rdbContado.BackColor = System.Drawing.Color.Transparent
        Me.rdbContado.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdbContado.Location = New System.Drawing.Point(69, 3)
        Me.rdbContado.Name = "rdbContado"
        Me.rdbContado.Size = New System.Drawing.Size(65, 17)
        Me.rdbContado.TabIndex = 1
        Me.rdbContado.TabStop = True
        Me.rdbContado.Text = "Contado"
        Me.rdbContado.UseVisualStyleBackColor = False
        '
        'rdbCredito
        '
        Me.rdbCredito.AutoSize = True
        Me.rdbCredito.BackColor = System.Drawing.Color.Transparent
        Me.rdbCredito.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdbCredito.Location = New System.Drawing.Point(4, 3)
        Me.rdbCredito.Name = "rdbCredito"
        Me.rdbCredito.Size = New System.Drawing.Size(58, 17)
        Me.rdbCredito.TabIndex = 0
        Me.rdbCredito.TabStop = True
        Me.rdbCredito.Text = "Credito"
        Me.rdbCredito.UseVisualStyleBackColor = False
        '
        'lblCondicionVenta
        '
        Me.lblCondicionVenta.AutoSize = True
        Me.lblCondicionVenta.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCondicionVenta.Location = New System.Drawing.Point(9, 127)
        Me.lblCondicionVenta.Name = "lblCondicionVenta"
        Me.lblCondicionVenta.Size = New System.Drawing.Size(69, 13)
        Me.lblCondicionVenta.TabIndex = 78
        Me.lblCondicionVenta.Text = "Cond. Venta:"
        '
        'txtReferencia
        '
        Me.txtReferencia.BackColor = System.Drawing.Color.White
        Me.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferencia.Color = System.Drawing.Color.Empty
        Me.txtReferencia.Indicaciones = "(F1) Para busqueda avanzada"
        Me.txtReferencia.Location = New System.Drawing.Point(52, 26)
        Me.txtReferencia.Multilinea = False
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Size = New System.Drawing.Size(58, 21)
        Me.txtReferencia.SoloLectura = False
        Me.txtReferencia.TabIndex = 75
        Me.txtReferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferencia.Texto = ""
        '
        'txtRUC
        '
        Me.txtRUC.BackColor = System.Drawing.Color.White
        Me.txtRUC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRUC.Color = System.Drawing.Color.Empty
        Me.txtRUC.Indicaciones = Nothing
        Me.txtRUC.Location = New System.Drawing.Point(399, 26)
        Me.txtRUC.Multilinea = False
        Me.txtRUC.Name = "txtRUC"
        Me.txtRUC.Size = New System.Drawing.Size(75, 21)
        Me.txtRUC.SoloLectura = True
        Me.txtRUC.TabIndex = 77
        Me.txtRUC.TabStop = False
        Me.txtRUC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtRUC.Texto = ""
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.BackColor = System.Drawing.Color.White
        Me.txtRazonSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazonSocial.Color = System.Drawing.Color.Empty
        Me.txtRazonSocial.Indicaciones = "(F1) Para busqueda avanzada"
        Me.txtRazonSocial.Location = New System.Drawing.Point(110, 26)
        Me.txtRazonSocial.Multilinea = False
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.Size = New System.Drawing.Size(289, 21)
        Me.txtRazonSocial.SoloLectura = True
        Me.txtRazonSocial.TabIndex = 76
        Me.txtRazonSocial.TabStop = False
        Me.txtRazonSocial.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRazonSocial.Texto = ""
        '
        'OcxClientesMapa1
        '
        Me.OcxClientesMapa1.dt = Nothing
        Me.OcxClientesMapa1.ID = 1
        Me.OcxClientesMapa1.Iniciado = False
        Me.OcxClientesMapa1.Latitud = New Decimal(New Integer() {25262551, 0, 0, -2147483648})
        Me.OcxClientesMapa1.Location = New System.Drawing.Point(12, 245)
        Me.OcxClientesMapa1.Longitud = New Decimal(New Integer() {57593911, 0, 0, -2147483648})
        Me.OcxClientesMapa1.Name = "OcxClientesMapa1"
        Me.OcxClientesMapa1.Size = New System.Drawing.Size(742, 343)
        Me.OcxClientesMapa1.TabIndex = 74
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 73
        Me.Label1.Text = "Cliente:"
        '
        'lblCelular
        '
        Me.lblCelular.AutoSize = True
        Me.lblCelular.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCelular.Location = New System.Drawing.Point(507, 99)
        Me.lblCelular.Name = "lblCelular"
        Me.lblCelular.Size = New System.Drawing.Size(53, 13)
        Me.lblCelular.TabIndex = 72
        Me.lblCelular.Text = "Celulares:"
        '
        'txtCelulares
        '
        Me.txtCelulares.BackColor = System.Drawing.Color.White
        Me.txtCelulares.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCelulares.Color = System.Drawing.Color.Empty
        Me.txtCelulares.Enabled = False
        Me.txtCelulares.Indicaciones = Nothing
        Me.txtCelulares.Location = New System.Drawing.Point(567, 95)
        Me.txtCelulares.Multilinea = False
        Me.txtCelulares.Name = "txtCelulares"
        Me.txtCelulares.Size = New System.Drawing.Size(187, 21)
        Me.txtCelulares.SoloLectura = False
        Me.txtCelulares.TabIndex = 71
        Me.txtCelulares.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCelulares.Texto = ""
        '
        'cbxEstado
        '
        Me.cbxEstado.CampoWhere = Nothing
        Me.cbxEstado.CargarUnaSolaVez = False
        Me.cbxEstado.DataDisplayMember = Nothing
        Me.cbxEstado.DataFilter = Nothing
        Me.cbxEstado.DataOrderBy = Nothing
        Me.cbxEstado.DataSource = Nothing
        Me.cbxEstado.DataValueMember = Nothing
        Me.cbxEstado.dtSeleccionado = Nothing
        Me.cbxEstado.Enabled = False
        Me.cbxEstado.FormABM = Nothing
        Me.cbxEstado.Indicaciones = Nothing
        Me.cbxEstado.Location = New System.Drawing.Point(567, 50)
        Me.cbxEstado.Name = "cbxEstado"
        Me.cbxEstado.SeleccionMultiple = False
        Me.cbxEstado.SeleccionObligatoria = True
        Me.cbxEstado.Size = New System.Drawing.Size(187, 21)
        Me.cbxEstado.SoloLectura = False
        Me.cbxEstado.TabIndex = 68
        Me.cbxEstado.Texto = ""
        '
        'txtTelefonos
        '
        Me.txtTelefonos.BackColor = System.Drawing.Color.White
        Me.txtTelefonos.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefonos.Color = System.Drawing.Color.Empty
        Me.txtTelefonos.Enabled = False
        Me.txtTelefonos.Indicaciones = Nothing
        Me.txtTelefonos.Location = New System.Drawing.Point(567, 74)
        Me.txtTelefonos.Multilinea = False
        Me.txtTelefonos.Name = "txtTelefonos"
        Me.txtTelefonos.Size = New System.Drawing.Size(187, 21)
        Me.txtTelefonos.SoloLectura = False
        Me.txtTelefonos.TabIndex = 65
        Me.txtTelefonos.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTelefonos.Texto = ""
        '
        'txtDireccion
        '
        Me.txtDireccion.BackColor = System.Drawing.Color.White
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Color = System.Drawing.Color.Empty
        Me.txtDireccion.Enabled = False
        Me.txtDireccion.Indicaciones = Nothing
        Me.txtDireccion.Location = New System.Drawing.Point(93, 95)
        Me.txtDireccion.Multilinea = False
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(347, 21)
        Me.txtDireccion.SoloLectura = False
        Me.txtDireccion.TabIndex = 67
        Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccion.Texto = ""
        '
        'txtNombreFantasia
        '
        Me.txtNombreFantasia.BackColor = System.Drawing.Color.White
        Me.txtNombreFantasia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreFantasia.Color = System.Drawing.Color.Empty
        Me.txtNombreFantasia.Enabled = False
        Me.txtNombreFantasia.Indicaciones = Nothing
        Me.txtNombreFantasia.Location = New System.Drawing.Point(93, 74)
        Me.txtNombreFantasia.Multilinea = False
        Me.txtNombreFantasia.Name = "txtNombreFantasia"
        Me.txtNombreFantasia.Size = New System.Drawing.Size(347, 21)
        Me.txtNombreFantasia.SoloLectura = False
        Me.txtNombreFantasia.TabIndex = 63
        Me.txtNombreFantasia.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNombreFantasia.Texto = ""
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblEstado.Location = New System.Drawing.Point(507, 54)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 70
        Me.lblEstado.Text = "Estado:"
        '
        'lblTelefonos
        '
        Me.lblTelefonos.AutoSize = True
        Me.lblTelefonos.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblTelefonos.Location = New System.Drawing.Point(507, 78)
        Me.lblTelefonos.Name = "lblTelefonos"
        Me.lblTelefonos.Size = New System.Drawing.Size(57, 13)
        Me.lblTelefonos.TabIndex = 66
        Me.lblTelefonos.Text = "Telefonos:"
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDireccion.Location = New System.Drawing.Point(6, 99)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(55, 13)
        Me.lblDireccion.TabIndex = 69
        Me.lblDireccion.Text = "Direccion:"
        '
        'lblNombreFantasia
        '
        Me.lblNombreFantasia.AutoSize = True
        Me.lblNombreFantasia.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNombreFantasia.Location = New System.Drawing.Point(6, 78)
        Me.lblNombreFantasia.Name = "lblNombreFantasia"
        Me.lblNombreFantasia.Size = New System.Drawing.Size(90, 13)
        Me.lblNombreFantasia.TabIndex = 64
        Me.lblNombreFantasia.Text = "Nombre Fantasia:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.SystemColors.Control
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 594)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.StatusStrip1.Size = New System.Drawing.Size(765, 22)
        Me.StatusStrip1.TabIndex = 37
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.SystemColors.Control
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnGuardar.Location = New System.Drawing.Point(679, 214)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 25)
        Me.btnGuardar.TabIndex = 36
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'chkBoleta
        '
        Me.chkBoleta.BackColor = System.Drawing.Color.Transparent
        Me.chkBoleta.Color = System.Drawing.Color.Empty
        Me.chkBoleta.Location = New System.Drawing.Point(405, 204)
        Me.chkBoleta.Name = "chkBoleta"
        Me.chkBoleta.Size = New System.Drawing.Size(263, 21)
        Me.chkBoleta.SoloLectura = False
        Me.chkBoleta.TabIndex = 35
        Me.chkBoleta.Texto = "Boleta contra Boleta"
        Me.chkBoleta.Valor = False
        '
        'lklVerCatastro
        '
        Me.lklVerCatastro.AutoSize = True
        Me.lklVerCatastro.Location = New System.Drawing.Point(271, 208)
        Me.lklVerCatastro.Name = "lklVerCatastro"
        Me.lklVerCatastro.Size = New System.Drawing.Size(65, 13)
        Me.lklVerCatastro.TabIndex = 34
        Me.lklVerCatastro.TabStop = True
        Me.lklVerCatastro.Text = "Ver Catastro"
        '
        'lvlSaldo
        '
        Me.lvlSaldo.AutoSize = True
        Me.lvlSaldo.Location = New System.Drawing.Point(9, 208)
        Me.lvlSaldo.Name = "lvlSaldo"
        Me.lvlSaldo.Size = New System.Drawing.Size(37, 13)
        Me.lvlSaldo.TabIndex = 32
        Me.lvlSaldo.Text = "Saldo:"
        '
        'lblDeuda
        '
        Me.lblDeuda.AutoSize = True
        Me.lblDeuda.Location = New System.Drawing.Point(9, 182)
        Me.lblDeuda.Name = "lblDeuda"
        Me.lblDeuda.Size = New System.Drawing.Size(42, 13)
        Me.lblDeuda.TabIndex = 20
        Me.lblDeuda.Text = "Deuda:"
        '
        'lklRecalcularCredito
        '
        Me.lklRecalcularCredito.AutoSize = True
        Me.lklRecalcularCredito.Location = New System.Drawing.Point(271, 182)
        Me.lklRecalcularCredito.Name = "lklRecalcularCredito"
        Me.lklRecalcularCredito.Size = New System.Drawing.Size(93, 13)
        Me.lklRecalcularCredito.TabIndex = 22
        Me.lklRecalcularCredito.TabStop = True
        Me.lklRecalcularCredito.Text = "Recalcular credito"
        '
        'lblPlazoCheque
        '
        Me.lblPlazoCheque.AutoSize = True
        Me.lblPlazoCheque.Location = New System.Drawing.Point(402, 182)
        Me.lblPlazoCheque.Name = "lblPlazoCheque"
        Me.lblPlazoCheque.Size = New System.Drawing.Size(115, 13)
        Me.lblPlazoCheque.TabIndex = 25
        Me.lblPlazoCheque.Text = "Plazo Cheque Diferido:"
        '
        'lblPlazoCobro
        '
        Me.lblPlazoCobro.AutoSize = True
        Me.lblPlazoCobro.Location = New System.Drawing.Point(559, 182)
        Me.lblPlazoCobro.Name = "lblPlazoCobro"
        Me.lblPlazoCobro.Size = New System.Drawing.Size(67, 13)
        Me.lblPlazoCobro.TabIndex = 30
        Me.lblPlazoCobro.Text = "Plazo Cobro:"
        '
        'lblPlazoCredito
        '
        Me.lblPlazoCredito.AutoSize = True
        Me.lblPlazoCredito.Location = New System.Drawing.Point(402, 155)
        Me.lblPlazoCredito.Name = "lblPlazoCredito"
        Me.lblPlazoCredito.Size = New System.Drawing.Size(72, 13)
        Me.lblPlazoCredito.TabIndex = 23
        Me.lblPlazoCredito.Text = "Plazo Credito:"
        '
        'lblDescuentoSimbolo
        '
        Me.lblDescuentoSimbolo.AutoSize = True
        Me.lblDescuentoSimbolo.Location = New System.Drawing.Point(664, 149)
        Me.lblDescuentoSimbolo.Name = "lblDescuentoSimbolo"
        Me.lblDescuentoSimbolo.Size = New System.Drawing.Size(15, 13)
        Me.lblDescuentoSimbolo.TabIndex = 29
        Me.lblDescuentoSimbolo.Text = "%"
        '
        'lblDesuento
        '
        Me.lblDesuento.AutoSize = True
        Me.lblDesuento.Location = New System.Drawing.Point(564, 155)
        Me.lblDesuento.Name = "lblDesuento"
        Me.lblDesuento.Size = New System.Drawing.Size(62, 13)
        Me.lblDesuento.TabIndex = 27
        Me.lblDesuento.Text = "Descuento:"
        '
        'lblLimiteCredito
        '
        Me.lblLimiteCredito.AutoSize = True
        Me.lblLimiteCredito.Location = New System.Drawing.Point(9, 155)
        Me.lblLimiteCredito.Name = "lblLimiteCredito"
        Me.lblLimiteCredito.Size = New System.Drawing.Size(73, 13)
        Me.lblLimiteCredito.TabIndex = 18
        Me.lblLimiteCredito.Text = "Limite Crédito:"
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(130, 204)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(135, 21)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 33
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'txtDeuda
        '
        Me.txtDeuda.Color = System.Drawing.Color.Empty
        Me.txtDeuda.Decimales = True
        Me.txtDeuda.Indicaciones = Nothing
        Me.txtDeuda.Location = New System.Drawing.Point(130, 178)
        Me.txtDeuda.Name = "txtDeuda"
        Me.txtDeuda.Size = New System.Drawing.Size(135, 21)
        Me.txtDeuda.SoloLectura = True
        Me.txtDeuda.TabIndex = 21
        Me.txtDeuda.TabStop = False
        Me.txtDeuda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDeuda.Texto = "0"
        '
        'txtPlazoCheque
        '
        Me.txtPlazoCheque.Color = System.Drawing.Color.Empty
        Me.txtPlazoCheque.Decimales = True
        Me.txtPlazoCheque.Indicaciones = Nothing
        Me.txtPlazoCheque.Location = New System.Drawing.Point(523, 178)
        Me.txtPlazoCheque.Name = "txtPlazoCheque"
        Me.txtPlazoCheque.Size = New System.Drawing.Size(26, 21)
        Me.txtPlazoCheque.SoloLectura = False
        Me.txtPlazoCheque.TabIndex = 26
        Me.txtPlazoCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPlazoCheque.Texto = "0"
        '
        'txtPlazoCobro
        '
        Me.txtPlazoCobro.Color = System.Drawing.Color.Empty
        Me.txtPlazoCobro.Decimales = True
        Me.txtPlazoCobro.Indicaciones = Nothing
        Me.txtPlazoCobro.Location = New System.Drawing.Point(632, 178)
        Me.txtPlazoCobro.Name = "txtPlazoCobro"
        Me.txtPlazoCobro.Size = New System.Drawing.Size(26, 21)
        Me.txtPlazoCobro.SoloLectura = False
        Me.txtPlazoCobro.TabIndex = 31
        Me.txtPlazoCobro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPlazoCobro.Texto = "0"
        '
        'txtPlazoCredito
        '
        Me.txtPlazoCredito.Color = System.Drawing.Color.Empty
        Me.txtPlazoCredito.Decimales = True
        Me.txtPlazoCredito.Indicaciones = Nothing
        Me.txtPlazoCredito.Location = New System.Drawing.Point(523, 151)
        Me.txtPlazoCredito.Name = "txtPlazoCredito"
        Me.txtPlazoCredito.Size = New System.Drawing.Size(26, 21)
        Me.txtPlazoCredito.SoloLectura = False
        Me.txtPlazoCredito.TabIndex = 24
        Me.txtPlazoCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPlazoCredito.Texto = "0"
        '
        'txtDescuento
        '
        Me.txtDescuento.Color = System.Drawing.Color.Empty
        Me.txtDescuento.Decimales = True
        Me.txtDescuento.Indicaciones = Nothing
        Me.txtDescuento.Location = New System.Drawing.Point(632, 151)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(26, 21)
        Me.txtDescuento.SoloLectura = False
        Me.txtDescuento.TabIndex = 28
        Me.txtDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDescuento.Texto = "0"
        '
        'txtLimiteCredito
        '
        Me.txtLimiteCredito.Color = System.Drawing.Color.Empty
        Me.txtLimiteCredito.Decimales = True
        Me.txtLimiteCredito.Indicaciones = Nothing
        Me.txtLimiteCredito.Location = New System.Drawing.Point(130, 151)
        Me.txtLimiteCredito.Name = "txtLimiteCredito"
        Me.txtLimiteCredito.Size = New System.Drawing.Size(135, 21)
        Me.txtLimiteCredito.SoloLectura = False
        Me.txtLimiteCredito.TabIndex = 19
        Me.txtLimiteCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLimiteCredito.Texto = "0"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmCreditoCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(765, 616)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmCreditoCliente"
        Me.Text = "frmCreditoCliente"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.PnlCondicionVenta.ResumeLayout(False)
        Me.PnlCondicionVenta.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkBoleta As ERP.ocxCHK
    Friend WithEvents lklVerCatastro As System.Windows.Forms.LinkLabel
    Friend WithEvents lvlSaldo As System.Windows.Forms.Label
    Friend WithEvents lblDeuda As System.Windows.Forms.Label
    Friend WithEvents lklRecalcularCredito As System.Windows.Forms.LinkLabel
    Friend WithEvents lblPlazoCheque As System.Windows.Forms.Label
    Friend WithEvents lblPlazoCobro As System.Windows.Forms.Label
    Friend WithEvents lblPlazoCredito As System.Windows.Forms.Label
    Friend WithEvents lblDescuentoSimbolo As System.Windows.Forms.Label
    Friend WithEvents lblDesuento As System.Windows.Forms.Label
    Friend WithEvents lblLimiteCredito As System.Windows.Forms.Label
    Friend WithEvents txtSaldo As ERP.ocxTXTNumeric
    Friend WithEvents txtDeuda As ERP.ocxTXTNumeric
    Friend WithEvents txtPlazoCheque As ERP.ocxTXTNumeric
    Friend WithEvents txtPlazoCobro As ERP.ocxTXTNumeric
    Friend WithEvents txtPlazoCredito As ERP.ocxTXTNumeric
    Friend WithEvents txtDescuento As ERP.ocxTXTNumeric
    Friend WithEvents txtLimiteCredito As ERP.ocxTXTNumeric
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents lblCelular As System.Windows.Forms.Label
    Friend WithEvents txtCelulares As ERP.ocxTXTString
    Friend WithEvents cbxEstado As ERP.ocxCBX
    Friend WithEvents txtTelefonos As ERP.ocxTXTString
    Friend WithEvents txtDireccion As ERP.ocxTXTString
    Friend WithEvents txtNombreFantasia As ERP.ocxTXTString
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lblTelefonos As System.Windows.Forms.Label
    Friend WithEvents lblDireccion As System.Windows.Forms.Label
    Friend WithEvents lblNombreFantasia As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents OcxClientesMapa1 As ERP.ocxClientesMapa
    Friend WithEvents txtReferencia As ERP.ocxTXTString
    Friend WithEvents txtRUC As ERP.ocxTXTString
    Friend WithEvents txtRazonSocial As ERP.ocxTXTString
    Friend WithEvents PnlCondicionVenta As Panel
    Friend WithEvents rdbContado As RadioButton
    Friend WithEvents rdbCredito As RadioButton
    Friend WithEvents lblCondicionVenta As Label
    Friend WithEvents lblID As Label
    Friend WithEvents txtID As ocxTXTNumeric
End Class

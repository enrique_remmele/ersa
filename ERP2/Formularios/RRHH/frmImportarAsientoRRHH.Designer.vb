﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmImportarAsientoRRHH
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblSaldo = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dgvDetalleAsiento = New System.Windows.Forms.DataGridView()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblCantidadDetalle = New System.Windows.Forms.Label()
        Me.txtCantidadDetalle = New ERP.ocxTXTNumeric()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblTotalDebito = New System.Windows.Forms.Label()
        Me.txtTotalDebito = New ERP.ocxTXTNumeric()
        Me.lblTotalCredito = New System.Windows.Forms.Label()
        Me.txtTotalCredito = New ERP.ocxTXTNumeric()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.btnExportar = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalSinConciliar = New ERP.ocxTXTNumeric()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtTotalConciliado = New ERP.ocxTXTNumeric()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ExportarAPlanillaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.btnListar = New System.Windows.Forms.Button()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.rbLiquidacionImportada = New System.Windows.Forms.RadioButton()
        Me.rbLiquidacionSinImportar = New System.Windows.Forms.RadioButton()
        Me.rbPreLiquidacion = New System.Windows.Forms.RadioButton()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtAño = New System.Windows.Forms.MaskedTextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbxPatronal = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbxMes = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.btnImportar = New System.Windows.Forms.Button()
        Me.btnEliminarImportados = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvDetalleAsiento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblSaldo
        '
        Me.lblSaldo.Location = New System.Drawing.Point(324, 0)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(57, 25)
        Me.lblSaldo.TabIndex = 7
        Me.lblSaldo.Text = "Saldo:"
        Me.lblSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Panel1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.Panel1, 2)
        Me.Panel1.Controls.Add(Me.dgvDetalleAsiento)
        Me.Panel1.Controls.Add(Me.TableLayoutPanel4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 393)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1004, 189)
        Me.Panel1.TabIndex = 6
        '
        'dgvDetalleAsiento
        '
        Me.dgvDetalleAsiento.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalleAsiento.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvDetalleAsiento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetalleAsiento.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvDetalleAsiento.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDetalleAsiento.Location = New System.Drawing.Point(0, 0)
        Me.dgvDetalleAsiento.Name = "dgvDetalleAsiento"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalleAsiento.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvDetalleAsiento.Size = New System.Drawing.Size(1004, 158)
        Me.dgvDetalleAsiento.TabIndex = 4
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 286.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.FlowLayoutPanel2, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.FlowLayoutPanel6, 1, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(0, 158)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(1004, 31)
        Me.TableLayoutPanel4.TabIndex = 3
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.lblCantidadDetalle)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtCantidadDetalle)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(280, 25)
        Me.FlowLayoutPanel2.TabIndex = 1
        '
        'lblCantidadDetalle
        '
        Me.lblCantidadDetalle.Location = New System.Drawing.Point(3, 0)
        Me.lblCantidadDetalle.Name = "lblCantidadDetalle"
        Me.lblCantidadDetalle.Size = New System.Drawing.Size(52, 25)
        Me.lblCantidadDetalle.TabIndex = 0
        Me.lblCantidadDetalle.Text = "Cantidad:"
        Me.lblCantidadDetalle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCantidadDetalle
        '
        Me.txtCantidadDetalle.Color = System.Drawing.Color.Empty
        Me.txtCantidadDetalle.Decimales = True
        Me.txtCantidadDetalle.Indicaciones = Nothing
        Me.txtCantidadDetalle.Location = New System.Drawing.Point(61, 3)
        Me.txtCantidadDetalle.Name = "txtCantidadDetalle"
        Me.txtCantidadDetalle.Size = New System.Drawing.Size(48, 22)
        Me.txtCantidadDetalle.SoloLectura = True
        Me.txtCantidadDetalle.TabIndex = 4
        Me.txtCantidadDetalle.TabStop = False
        Me.txtCantidadDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadDetalle.Texto = "0"
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.Controls.Add(Me.lblTotalDebito)
        Me.FlowLayoutPanel6.Controls.Add(Me.txtTotalDebito)
        Me.FlowLayoutPanel6.Controls.Add(Me.lblTotalCredito)
        Me.FlowLayoutPanel6.Controls.Add(Me.txtTotalCredito)
        Me.FlowLayoutPanel6.Controls.Add(Me.lblSaldo)
        Me.FlowLayoutPanel6.Controls.Add(Me.txtSaldo)
        Me.FlowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(289, 3)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(712, 25)
        Me.FlowLayoutPanel6.TabIndex = 0
        '
        'lblTotalDebito
        '
        Me.lblTotalDebito.Location = New System.Drawing.Point(3, 0)
        Me.lblTotalDebito.Name = "lblTotalDebito"
        Me.lblTotalDebito.Size = New System.Drawing.Size(50, 25)
        Me.lblTotalDebito.TabIndex = 0
        Me.lblTotalDebito.Text = "Debito:"
        Me.lblTotalDebito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTotalDebito
        '
        Me.txtTotalDebito.Color = System.Drawing.Color.Empty
        Me.txtTotalDebito.Decimales = True
        Me.txtTotalDebito.Indicaciones = Nothing
        Me.txtTotalDebito.Location = New System.Drawing.Point(59, 3)
        Me.txtTotalDebito.Name = "txtTotalDebito"
        Me.txtTotalDebito.Size = New System.Drawing.Size(95, 22)
        Me.txtTotalDebito.SoloLectura = True
        Me.txtTotalDebito.TabIndex = 4
        Me.txtTotalDebito.TabStop = False
        Me.txtTotalDebito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDebito.Texto = "0"
        '
        'lblTotalCredito
        '
        Me.lblTotalCredito.Location = New System.Drawing.Point(160, 0)
        Me.lblTotalCredito.Name = "lblTotalCredito"
        Me.lblTotalCredito.Size = New System.Drawing.Size(57, 25)
        Me.lblTotalCredito.TabIndex = 5
        Me.lblTotalCredito.Text = "Credito:"
        Me.lblTotalCredito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTotalCredito
        '
        Me.txtTotalCredito.Color = System.Drawing.Color.Empty
        Me.txtTotalCredito.Decimales = True
        Me.txtTotalCredito.Indicaciones = Nothing
        Me.txtTotalCredito.Location = New System.Drawing.Point(223, 3)
        Me.txtTotalCredito.Name = "txtTotalCredito"
        Me.txtTotalCredito.Size = New System.Drawing.Size(95, 22)
        Me.txtTotalCredito.SoloLectura = True
        Me.txtTotalCredito.TabIndex = 6
        Me.txtTotalCredito.TabStop = False
        Me.txtTotalCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCredito.Texto = "0"
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(387, 3)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(95, 22)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 8
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'btnExportar
        '
        Me.btnExportar.Location = New System.Drawing.Point(143, 220)
        Me.btnExportar.Name = "btnExportar"
        Me.btnExportar.Size = New System.Drawing.Size(84, 30)
        Me.btnExportar.TabIndex = 33
        Me.btnExportar.Text = "Exp. Excel"
        Me.btnExportar.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.AutoSize = True
        Me.FlowLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel4.Controls.Add(Me.txtTotalSinConciliar)
        Me.FlowLayoutPanel4.Controls.Add(Me.Label7)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtTotalConciliado)
        Me.FlowLayoutPanel4.Controls.Add(Me.Label6)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtCantidad)
        Me.FlowLayoutPanel4.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(109, 353)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(898, 34)
        Me.FlowLayoutPanel4.TabIndex = 2
        '
        'txtTotalSinConciliar
        '
        Me.txtTotalSinConciliar.Color = System.Drawing.Color.Empty
        Me.txtTotalSinConciliar.Decimales = True
        Me.txtTotalSinConciliar.Indicaciones = Nothing
        Me.txtTotalSinConciliar.Location = New System.Drawing.Point(862, 5)
        Me.txtTotalSinConciliar.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtTotalSinConciliar.Name = "txtTotalSinConciliar"
        Me.txtTotalSinConciliar.Size = New System.Drawing.Size(33, 22)
        Me.txtTotalSinConciliar.SoloLectura = True
        Me.txtTotalSinConciliar.TabIndex = 7
        Me.txtTotalSinConciliar.TabStop = False
        Me.txtTotalSinConciliar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalSinConciliar.Texto = "0"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(762, 8)
        Me.Label7.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(94, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Cant. Sin Verificar:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTotalConciliado
        '
        Me.txtTotalConciliado.Color = System.Drawing.Color.Empty
        Me.txtTotalConciliado.Decimales = True
        Me.txtTotalConciliado.Indicaciones = Nothing
        Me.txtTotalConciliado.Location = New System.Drawing.Point(723, 5)
        Me.txtTotalConciliado.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtTotalConciliado.Name = "txtTotalConciliado"
        Me.txtTotalConciliado.Size = New System.Drawing.Size(33, 22)
        Me.txtTotalConciliado.SoloLectura = True
        Me.txtTotalConciliado.TabIndex = 5
        Me.txtTotalConciliado.TabStop = False
        Me.txtTotalConciliado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalConciliado.Texto = "0"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(632, 8)
        Me.Label6.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(85, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Cant. Verificado:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = Nothing
        Me.txtCantidad.Location = New System.Drawing.Point(593, 5)
        Me.txtCantidad.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(33, 22)
        Me.txtCantidad.SoloLectura = True
        Me.txtCantidad.TabIndex = 10
        Me.txtCantidad.TabStop = False
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(535, 8)
        Me.Label1.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Cantidad:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ExportarAPlanillaToolStripMenuItem
        '
        Me.ExportarAPlanillaToolStripMenuItem.Name = "ExportarAPlanillaToolStripMenuItem"
        Me.ExportarAPlanillaToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ExportarAPlanillaToolStripMenuItem.Text = "Exportar a Planilla"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExportarAPlanillaToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(169, 26)
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(9, 220)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(84, 30)
        Me.btnListar.TabIndex = 4
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.rbLiquidacionImportada)
        Me.TabPage1.Controls.Add(Me.rbLiquidacionSinImportar)
        Me.TabPage1.Controls.Add(Me.rbPreLiquidacion)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.txtAño)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.cbxPatronal)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.cbxMes)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.TextBox1)
        Me.TabPage1.Controls.Add(Me.btnImportar)
        Me.TabPage1.Controls.Add(Me.btnEliminarImportados)
        Me.TabPage1.Controls.Add(Me.btnExportar)
        Me.TabPage1.Controls.Add(Me.btnListar)
        Me.TabPage1.Location = New System.Drawing.Point(4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(230, 573)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'rbLiquidacionImportada
        '
        Me.rbLiquidacionImportada.AutoSize = True
        Me.rbLiquidacionImportada.Location = New System.Drawing.Point(12, 60)
        Me.rbLiquidacionImportada.Margin = New System.Windows.Forms.Padding(0)
        Me.rbLiquidacionImportada.Name = "rbLiquidacionImportada"
        Me.rbLiquidacionImportada.Size = New System.Drawing.Size(129, 17)
        Me.rbLiquidacionImportada.TabIndex = 56
        Me.rbLiquidacionImportada.TabStop = True
        Me.rbLiquidacionImportada.Text = "Liquidacion Importada"
        Me.rbLiquidacionImportada.UseVisualStyleBackColor = True
        '
        'rbLiquidacionSinImportar
        '
        Me.rbLiquidacionSinImportar.AutoSize = True
        Me.rbLiquidacionSinImportar.Location = New System.Drawing.Point(12, 44)
        Me.rbLiquidacionSinImportar.Margin = New System.Windows.Forms.Padding(0)
        Me.rbLiquidacionSinImportar.Name = "rbLiquidacionSinImportar"
        Me.rbLiquidacionSinImportar.Size = New System.Drawing.Size(138, 17)
        Me.rbLiquidacionSinImportar.TabIndex = 55
        Me.rbLiquidacionSinImportar.TabStop = True
        Me.rbLiquidacionSinImportar.Text = "Liquidacion Sin Importar"
        Me.rbLiquidacionSinImportar.UseVisualStyleBackColor = True
        '
        'rbPreLiquidacion
        '
        Me.rbPreLiquidacion.AutoSize = True
        Me.rbPreLiquidacion.Location = New System.Drawing.Point(12, 29)
        Me.rbPreLiquidacion.Margin = New System.Windows.Forms.Padding(0)
        Me.rbPreLiquidacion.Name = "rbPreLiquidacion"
        Me.rbPreLiquidacion.Size = New System.Drawing.Size(98, 17)
        Me.rbPreLiquidacion.TabIndex = 54
        Me.rbPreLiquidacion.TabStop = True
        Me.rbPreLiquidacion.Text = "Pre Liquidacion"
        Me.rbPreLiquidacion.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 13)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(170, 13)
        Me.Label5.TabIndex = 53
        Me.Label5.Text = "Estado de Liquidacion a visualizar:"
        '
        'txtAño
        '
        Me.txtAño.Location = New System.Drawing.Point(68, 100)
        Me.txtAño.Mask = "0000"
        Me.txtAño.Name = "txtAño"
        Me.txtAño.Size = New System.Drawing.Size(70, 20)
        Me.txtAño.TabIndex = 51
        Me.txtAño.Text = "2023"
        Me.txtAño.ValidatingType = GetType(Date)
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(18, 158)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 13)
        Me.Label4.TabIndex = 50
        Me.Label4.Text = "Patronal:"
        '
        'cbxPatronal
        '
        Me.cbxPatronal.FormattingEnabled = True
        Me.cbxPatronal.Location = New System.Drawing.Point(68, 154)
        Me.cbxPatronal.Name = "cbxPatronal"
        Me.cbxPatronal.Size = New System.Drawing.Size(153, 21)
        Me.cbxPatronal.TabIndex = 49
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(18, 131)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(30, 13)
        Me.Label3.TabIndex = 47
        Me.Label3.Text = "Mes:"
        '
        'cbxMes
        '
        Me.cbxMes.FormattingEnabled = True
        Me.cbxMes.Location = New System.Drawing.Point(68, 127)
        Me.cbxMes.Name = "cbxMes"
        Me.cbxMes.Size = New System.Drawing.Size(153, 21)
        Me.cbxMes.TabIndex = 46
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(18, 104)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 13)
        Me.Label2.TabIndex = 45
        Me.Label2.Text = "Año:"
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(9, 413)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(207, 39)
        Me.TextBox1.TabIndex = 42
        Me.TextBox1.Text = "Para porder importar los Asientos, deben estar todos verificados."
        '
        'btnImportar
        '
        Me.btnImportar.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnImportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImportar.Location = New System.Drawing.Point(3, 376)
        Me.btnImportar.Name = "btnImportar"
        Me.btnImportar.Size = New System.Drawing.Size(224, 30)
        Me.btnImportar.TabIndex = 41
        Me.btnImportar.Text = "IMPORTAR ASIENTOS"
        Me.btnImportar.UseVisualStyleBackColor = False
        Me.btnImportar.Visible = False
        '
        'btnEliminarImportados
        '
        Me.btnEliminarImportados.BackColor = System.Drawing.Color.Red
        Me.btnEliminarImportados.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminarImportados.Location = New System.Drawing.Point(3, 510)
        Me.btnEliminarImportados.Name = "btnEliminarImportados"
        Me.btnEliminarImportados.Size = New System.Drawing.Size(224, 47)
        Me.btnEliminarImportados.TabIndex = 40
        Me.btnEliminarImportados.Text = "ELIMINAR ASIENTOS IMPORTADOS"
        Me.btnEliminarImportados.UseVisualStyleBackColor = False
        Me.btnEliminarImportados.Visible = False
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'Panel2
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.Panel2, 2)
        Me.Panel2.Controls.Add(Me.dgv)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1004, 344)
        Me.Panel2.TabIndex = 1
        '
        'dgv
        '
        Me.dgv.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv.Location = New System.Drawing.Point(0, 0)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgv.Size = New System.Drawing.Size(1004, 344)
        Me.dgv.TabIndex = 5
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 15)
        '
        'StatusStrip1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.StatusStrip1, 2)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 585)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1010, 20)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 904.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 244.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.StatusStrip1, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TabControl1, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 195.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1254, 605)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'TabControl1
        '
        Me.TabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(1013, 3)
        Me.TabControl1.Name = "TabControl1"
        Me.TableLayoutPanel1.SetRowSpan(Me.TabControl1, 4)
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(238, 599)
        Me.TabControl1.TabIndex = 4
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 353)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(100, 34)
        Me.FlowLayoutPanel1.TabIndex = 7
        '
        'frmImportarAsientoRRHH
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1254, 605)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmImportarAsientoRRHH"
        Me.Tag = "frmImportarAsientoRRHH"
        Me.Text = "frmImportarAsientoRRHH"
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvDetalleAsiento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel4.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblSaldo As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents dgv As DataGridView
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents btnExportar As Button
    Friend WithEvents btnListar As Button
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents txtTotalSinConciliar As ocxTXTNumeric
    Friend WithEvents Label7 As Label
    Friend WithEvents txtTotalConciliado As ocxTXTNumeric
    Friend WithEvents Label6 As Label
    Friend WithEvents dgvDetalleAsiento As DataGridView
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents lblCantidadDetalle As Label
    Friend WithEvents txtCantidadDetalle As ocxTXTNumeric
    Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
    Friend WithEvents lblTotalDebito As Label
    Friend WithEvents txtTotalDebito As ocxTXTNumeric
    Friend WithEvents lblTotalCredito As Label
    Friend WithEvents txtTotalCredito As ocxTXTNumeric
    Friend WithEvents txtSaldo As ocxTXTNumeric
    Friend WithEvents ExportarAPlanillaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents txtCantidad As ocxTXTNumeric
    Friend WithEvents Label1 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents cbxPatronal As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents cbxMes As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents btnImportar As Button
    Friend WithEvents btnEliminarImportados As Button
    Friend WithEvents txtAño As MaskedTextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents rbLiquidacionImportada As RadioButton
    Friend WithEvents rbLiquidacionSinImportar As RadioButton
    Friend WithEvents rbPreLiquidacion As RadioButton
End Class

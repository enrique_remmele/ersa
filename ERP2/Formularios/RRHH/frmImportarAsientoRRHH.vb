﻿Public Class frmImportarAsientoRRHH

    Dim CSistema As New CSistema
    Dim dt As DataTable
    Dim dtDetalle As DataTable
    Dim dtSeccionesERP As DataTable
    Dim dtDepartamentoERP As DataTable
    Dim dtCentroCostoERP As DataTable
    Dim dtUnidadNegocioERP As DataTable
    Dim IDTransaccion As Integer
    Dim FechaLiquidacion As DateTime

    Sub Inicializar()

        CSistema.SqlToComboBox(cbxMes, CSistema.RetornardtMeses)
        CSistema.SqlToComboBox(cbxPatronal, "select ID, Descripcion from RRHH.dbo.vPatronal where IDEmpresa = " & vgIDEmpresaRRHH)
        dtSeccionesERP = CSistema.ExecuteToDataTable("Select ID, Descripcion from Seccion")
        dtDepartamentoERP = CSistema.ExecuteToDataTable("Select ID, Departamento from DepartamentoEmpresa")
        dtCentroCostoERP = CSistema.ExecuteToDataTable("Select ID, Descripcion from CentroCosto")
        dtUnidadNegocioERP = CSistema.ExecuteToDataTable("Select ID, Descripcion from UnidadNegocio")
        cbxMes.SelectedValue = Today.Month
        txtAño.Text = Today.Year

    End Sub

    Sub Listar()

        FechaLiquidacion = New Date(txtAño.Text, cbxMes.SelectedValue, DateTime.DaysInMonth(txtAño.Text, cbxMes.SelectedValue))


        Dim consultadt As String = "Select 'Verificado'= cast(0 as bit), L.MesLiquidacion, 'UnidadNegocioERP'='','CentroCostoERP'='','DepartamentoERP'='','SeccionERP'='', A.* from RRHH.dbo.vAsiento A join RRHH.dbo.vLiquidacion L on A.IDLiquidacion = L.ID "
        Dim consultadtDetalle As String = "Select DA.* from RRHH.dbo.VDetalleAsiento DA join RRHH.dbo.vLiquidacion L on DA.IDLiquidacion = L.ID "
        Dim Where As String = " where isnull(L.anulado,0) = 0 and L.Mes = " & cbxMes.SelectedValue & " and L.Año = " & txtAño.Text & " and L.IDPatronal = " & cbxPatronal.SelectedValue

        If rbLiquidacionImportada.Checked Then
            Where = Where & " and Sincronizado = 1 and L.confirmado = 1 "
        End If

        If rbLiquidacionSinImportar.Checked Then
            Where = Where & " and Sincronizado = 0 and L.confirmado = 1 "
        End If

        If rbPreLiquidacion.Checked Then
            Where = Where & " and Sincronizado = 0 and isnull(L.confirmado,0) = 0 "
        End If


        dt = CSistema.ExecuteToDataTable(consultadt & Where, "", 1000)


        For Each oRow In dt.Rows
            If oRow("IDUnidadNegocioERP").ToString() <> "" Then
                oRow("UnidadNegocioERP") = dtUnidadNegocioERP.Select("ID =" & oRow("IDUnidadNegocioERP"))(0)("Descripcion")
            End If

            If oRow("IDCentroCostoERP").ToString() <> "" Then
                oRow("CentroCostoERP") = dtCentroCostoERP.Select("ID =" & oRow("IDCentroCostoERP"))(0)("Descripcion")
            End If

            If oRow("IDDepartamentoERP").ToString() <> "" Then
                oRow("DepartamentoERP") = dtDepartamentoERP.Select("ID =" & oRow("IDDepartamentoERP"))(0)("Departamento")
            End If

            If oRow("IDSeccionERP").ToString() <> "" Then
                oRow("SeccionERP") = dtSeccionesERP.Select("ID =" & oRow("IDSeccionERP"))(0)("Descripcion")
            End If
        Next


        dtDetalle = CSistema.ExecuteToDataTable(consultadtDetalle & Where & " order by IDEstructuraBase asc, Debito desc", "", 1000)

        CSistema.dtToGrid(dgv, dt)

        CSistema.DataGridColumnasVisibles(dgv, {"Verificado", "NumeroAsientoSincronizado", "UnidadNegocioERP", "SeccionERP", "Año", "MesLiquidacion", "Patronal", "CentroCostoERP", "DepartamentoERP", "ObservacionDetalle"})
        dgv.Columns("Verificado").DisplayIndex = 0
        dgv.Columns("NumeroAsientoSincronizado").DisplayIndex = 1
        dgv.Columns("SeccionERP").DisplayIndex = 2
        dgv.Columns("DepartamentoERP").DisplayIndex = 3
        dgv.Columns("CentroCostoERP").DisplayIndex = 4
        dgv.Columns("UnidadNegocioERP").DisplayIndex = 5
        dgv.Columns("Patronal").DisplayIndex = 6
        dgv.Columns("MesLiquidacion").DisplayIndex = 7
        dgv.Columns("Año").DisplayIndex = 8

        dgv.Columns("ObservacionDetalle").DisplayIndex = 9

        dgv.Columns("NumeroAsientoSincronizado").HeaderText = "Nro Asiento"
        dgv.Columns("UnidadNegocioERP").HeaderText = "Unidad de Negocio"
        dgv.Columns("SeccionERP").HeaderText = "Seccion"
        dgv.Columns("DepartamentoERP").HeaderText = "Departamento"
        dgv.Columns("MesLiquidacion").HeaderText = "Mes"
        dgv.Columns("CentroCostoERP").HeaderText = "Centro de Costo"
        dgv.Columns("ObservacionDetalle").HeaderText = "Observacion"


        CalcularTotales()

        ListarDetalle()

    End Sub

    Sub ListarDetalle()

        If dgv.SelectedRows.Count = 0 Then
            Exit Sub
        End If
        ctrError.Clear()

        dgvDetalleAsiento.DataSource = Nothing

        If dgv.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgv, Mensaje)
            ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        Dim IDAsiento As Integer = CInt(dgv.CurrentRow.Cells("ID").Value)
        Dim dt As DataTable = dtDetalle.Select("ID = " & IDAsiento).CopyToDataTable()
        dgvDetalleAsiento.DataSource = dt

        dgvDetalleAsiento.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        dgvDetalleAsiento.RowHeadersVisible = False
        dgvDetalleAsiento.BackgroundColor = Color.White

        CSistema.DataGridColumnasVisibles(dgvDetalleAsiento, {"CuentaContable", "Descripcion", "Debito", "Credito"})
        CSistema.DataGridColumnasNumericas(dgvDetalleAsiento, {"CuentaContable", "Debito", "Credito"})

        dgvDetalleAsiento.Columns("CuentaContable").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        dgvDetalleAsiento.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        dgvDetalleAsiento.Columns("CuentaContable").DisplayIndex = 0
        dgvDetalleAsiento.Columns("Descripcion").DisplayIndex = 1
        dgvDetalleAsiento.Columns("Debito").DisplayIndex = 2
        dgvDetalleAsiento.Columns("Credito").DisplayIndex = 3

        dgvDetalleAsiento.Columns(2).DefaultCellStyle.Format = "N0"
        dgvDetalleAsiento.Columns(3).DefaultCellStyle.Format = "N0"

        dgvDetalleAsiento.Sort(dgvDetalleAsiento.Columns("Debito"), System.ComponentModel.ListSortDirection.Descending)

        txtCantidadDetalle.txt.Text = dgvDetalleAsiento.Rows.Count
        txtTotalDebito.txt.Text = CSistema.dtSumColumn(dt, "Debito")
        txtTotalCredito.txt.Text = CSistema.dtSumColumn(dt, "Credito")

        txtSaldo.SetValue(CStr(txtTotalCredito.ObtenerValor - txtTotalDebito.ObtenerValor))
        CalcularTotales()
    End Sub

    Sub CalcularTotales()

        Dim Total As Decimal = 0
        Dim TotalVerificado As Decimal = 0
        Dim TotalSinVerificado As Decimal = 0

        Total = dgv.Rows.Count
        TotalVerificado = CSistema.gridCountRows(dgv, "Verificado", "True")
        TotalSinVerificado = CSistema.gridCountRows(dgv, "Verificado", "False")

        txtCantidad.txt.Text = Total.ToString()
        txtTotalConciliado.txt.Text = CSistema.FormatoMoneda(TotalVerificado, False)
        txtTotalSinConciliar.txt.Text = CSistema.FormatoMoneda(TotalSinVerificado, False)

        If rbLiquidacionSinImportar.Checked Then
            If Total = TotalVerificado And Total > 0 Then
                btnImportar.Visible = True
            Else
                btnImportar.Visible = False
            End If
        End If

        If rbLiquidacionImportada.Checked Then
            If Total = TotalVerificado And Total > 0 Then
                btnEliminarImportados.Visible = True
            Else
                btnEliminarImportados.Visible = False
            End If
        End If


    End Sub

    Sub Importar()

        Dim dv As DataView
        dv = New DataView(dgv.DataSource)

        Dim grid As New DataGridView

        grid.DataSource = dv
        CSistema.dtToGrid(grid, dv.ToTable())
        Dim dt As DataTable = grid.DataSource

        For Each row As DataRow In dt.Rows

            Dim param(-1) As SqlClient.SqlParameter

            'Entrada
            CSistema.SetSQLParameter(param, "@IDLiquidacion", row("IDLiquidacion"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@FechaLiquidacion", CSistema.FormatoFechaBaseDatos(FechaLiquidacion, True, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDAsientoRRHH", row("ID"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Detalle", row("ObservacionDetalle"), ParameterDirection.Input)

            If row("IDUnidadNegocioERP").ToString() <> "" Then
                CSistema.SetSQLParameter(param, "@IDUnidadNegocioSAIN", row("IDUnidadNegocioERP"), ParameterDirection.Input)
            End If

            If row("IDCentroCostoERP").ToString() <> "" Then
                CSistema.SetSQLParameter(param, "@IDCentroCostoSAIN", row("IDCentroCostoERP"), ParameterDirection.Input)
            End If

            If row("IDDepartamentoERP").ToString() <> "" Then
                CSistema.SetSQLParameter(param, "@IDDepartamentoSAIN", row("IDDepartamentoERP"), ParameterDirection.Input)
            End If

            If row("IDSeccionERP").ToString() <> "" Then
                CSistema.SetSQLParameter(param, "@IDSeccionSAIN", row("IDSeccionERP"), ParameterDirection.Input)
            End If

            CSistema.SetSQLParameter(param, "@IDSucursalSAIN", row("IDSucursalERP"), ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""
            If CSistema.ExecuteStoreProcedure(param, "SpAsientoRRHHImportar", False, True, MensajeRetorno) = False Then

            End If

        Next

        Listar()

    End Sub

    Sub EliminarImportados()
        Dim dv As DataView
        dv = New DataView(dgv.DataSource)

        Dim grid As New DataGridView

        grid.DataSource = dv
        CSistema.dtToGrid(grid, dv.ToTable())
        Dim dt As DataTable = grid.DataSource

        For Each row As DataRow In dt.Rows

            Dim param(-1) As SqlClient.SqlParameter

            'Entrada
            CSistema.SetSQLParameter(param, "IDTransaccion", row("IDTransaccionSincronizado"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDAsientoRRHH", row("ID"), ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""
            If CSistema.ExecuteStoreProcedure(param, "SpAsientoRRHHEliminarImportacion", False, True, MensajeRetorno) = False Then

            End If

        Next

        Listar()
    End Sub

    Private Sub frmConciliarAsiento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        CSistema.dtToExcel2(dt, "Asientos")
    End Sub

    Private Sub btnImportar_Click(sender As System.Object, e As System.EventArgs) Handles btnImportar.Click
        Importar()
    End Sub

    Private Sub btnEliminarImportados_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminarImportados.Click
        EliminarImportados()
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv.CellClick
        ListarDetalle()
    End Sub

    Private Sub DataGridView1_CellContentDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv.CellContentDoubleClick
        If dgv.CurrentCell.ColumnIndex = 0 Then
            dgv.CurrentCell.Value = Not dgv.CurrentCell.Value
        End If
        CalcularTotales()
    End Sub

    Private Sub rbPreLiquidacion_CheckedChanged(sender As Object, e As EventArgs) Handles rbPreLiquidacion.CheckedChanged, rbLiquidacionImportada.CheckedChanged, rbLiquidacionSinImportar.CheckedChanged
        CSistema.dtToGrid(dgv, Nothing)
        btnImportar.Visible = False
        btnEliminarImportados.Visible = False
        CalcularTotales()
    End Sub
End Class
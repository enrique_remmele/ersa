﻿Imports System.Windows.Forms
Imports System.Xml

Public Class frmPrincipal

    'EVENTOS

    'PROPIEDADES

    'CLASES
    Dim CArchivoInicio As New CArchivoInicio
    Dim CSistema As New CSistema

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        'Form
        'Titulo
        Dim Titulo As String
        Titulo = VGSoftwareNombre & " :: Versión: " & VGSoftwareVersion & " - Ultima actualización: " & VGSoftwareUltimaActualizacion & " - Licencia otorgada a: " & VGSoftwareLicenciaOtorgadaA
        Me.Text = Titulo

        'Identificaciones
        lblSucursal.Text = vgSucursal
        lblDeposito.Text = vgDeposito
        lblTerminal.Text = vgTerminal
        lblUsuario.Text = vgUsuario

        'Imagen
        Dim ImagenFondo As String = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "IMAGEN", "")
        If ImagenFondo <> "" Then
            If My.Computer.FileSystem.FileExists(ImagenFondo) = True Then
                Me.BackgroundImage = Image.FromFile(ImagenFondo)
            End If
        Else
            'Me.BackgroundImage = My.Resources.EXPRESS_FONDO
        End If

        'Fecha Hora
        VGFechaHoraSistema = CDate(CSistema.ExecuteScalar("Select GetDate()"))



    End Sub

    Sub MostrarFormulario(ByVal frm As Form, ByVal Nombre As String, Optional ByVal Estilo As FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable, Optional ByVal Localizacion As FormStartPosition = FormStartPosition.CenterScreen)

        'frm.Icon = My.Resources.Icon
        frm.Text = VGSoftwareNombre & " :: " & Nombre
        frm.FormBorderStyle = Estilo
        frm.MinimizeBox = True
        'frm.MaximizeBox = False
        frm.StartPosition = Localizacion
        frm.MdiParent = Me
        frm.Show()
        frm.BringToFront()

    End Sub

    Private Sub PaisCiudadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PaisCiudadToolStripMenuItem.Click
        MostrarFormulario(frmPaisDepartamentoCiudadBarrios, "ABM Localizaciones", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ClientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientesToolStripMenuItem.Click
        MostrarFormulario(frmCliente, "ABM Clientes", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ProveedoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProveedoresToolStripMenuItem.Click
        MostrarFormulario(frmProveedor, "ABM Proveedores", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen)
    End Sub

    Private Sub frmPrincipal_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DoubleClick

    End Sub

    Private Sub CambiarImagenDeFonoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CambiarImagenDeFonoToolStripMenuItem.Click
        OpenFileDialog1.ShowDialog()
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "IMAGEN", OpenFileDialog1.FileName)

        Dim ImagenFondo As String = OpenFileDialog1.FileName

        If ImagenFondo <> "" Then
            If My.Computer.FileSystem.FileExists(ImagenFondo) = True Then
                Me.BackgroundImage = Image.FromFile(ImagenFondo)
            End If
        End If

    End Sub

    Private Sub frmPrincipal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        VGFechaHoraSistema = DateAdd(DateInterval.Second, 1, VGFechaHoraSistema)
        tsslFecha.Text = VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString
    End Sub

    Private Sub PredeterminadoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PredeterminadoToolStripMenuItem.Click
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "IMAGEN", "")
        'Me.BackgroundImage = My.Resources.EXPRESS_FONDO
    End Sub

    Private Sub TipoClienteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoClienteToolStripMenuItem.Click
        MostrarFormulario(frmTipoCliente, "ABM Tipo de Clientes", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub PromotoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PromotoresToolStripMenuItem.Click
        MostrarFormulario(frmPromotor, "ABM Promotores", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub VendedoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VendedoresToolStripMenuItem.Click
        MostrarFormulario(frmVendedor, "ABM Vendedores", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub CobradoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CobradoresToolStripMenuItem.Click
        MostrarFormulario(frmCobrador, "ABM Cobradores", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub DistribuidoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DistribuidoresToolStripMenuItem.Click
        MostrarFormulario(frmDistribuidor, "ABM Distribuidores", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub EstadoDeClientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EstadoDeClientesToolStripMenuItem.Click
        MostrarFormulario(frmEstadoCliente, "ABM Estado de Clientes", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub Sucursales6ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Sucursales6ToolStripMenuItem.Click
        MostrarFormulario(frmSucursalDeposito, "ABM Sucursales y Depositos", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub frmPrincipal_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Me.Refresh()
    End Sub

    Private Sub MonedaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MonedaToolStripMenuItem.Click
        MostrarFormulario(frmMoneda, "ABM Monedas", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ProductosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProductosToolStripMenuItem.Click
        MostrarFormulario(frmProducto2, "ABM Producto", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen)
    End Sub

    Private Sub TipoDeProductosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoDeProductosToolStripMenuItem.Click
        MostrarFormulario(frmTipoProducto, "ABM Tipo de Productos", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub LineaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LineaToolStripMenuItem.Click
        MostrarFormulario(frmLineas, "ABM Lineas", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub MarcaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MarcaToolStripMenuItem.Click
        MostrarFormulario(frmMarca, "ABM Marcas", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem1.Click
        MostrarFormulario(frmPresentacion, "ABM Presentacion", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub CategoriasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CategoriasToolStripMenuItem.Click
        MostrarFormulario(frmCategoria, "ABM Categorias", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub UnidadesDeMedidaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UnidadesDeMedidaToolStripMenuItem.Click
        MostrarFormulario(frmUnidadMedida, "ABM Unidades de Medida", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub AdministrarToolStripMenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AdministrarToolStripMenuItem6.Click
        MostrarFormulario(frmListadoPrecios, "ABM Lista de Precios", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ToolStripMenuItem2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem2.Click
        MostrarFormulario(frmTipoComprobante, "Tipos de Comprobantes", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub MotivosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MotivosToolStripMenuItem.Click
        MostrarFormulario(frmMotivo, "Motivos de Registros", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub PlanDeCuentasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PlanDeCuentasToolStripMenuItem.Click
        MostrarFormulario(frmCuentaContable, "Plan de Cuentas", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen)
    End Sub

    Private Sub CuentasFijasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CuentasFijasToolStripMenuItem.Click
        MostrarFormulario(frmCuentaFija, "Cuentas Fijas", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen)
    End Sub

    Private Sub RegistrosDeCostosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MostrarFormulario(frmRegistroCostos, "Registro de Costos", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ConfiguracionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfiguracionesToolStripMenuItem.Click
        MostrarFormulario(frmConfiguraciones, "Configuracion del Sistema", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub NuevoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NuevoToolStripMenuItem.Click
        MostrarFormulario(frmVenta, "Venta", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub PuntosDeExpedicionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PuntosDeExpedicionToolStripMenuItem.Click
        MostrarFormulario(frmPuntoExpedicion, "Puntos de Expedicion", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub TerminalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TerminalesToolStripMenuItem.Click
        MostrarFormulario(frmTerminalTransaccion, "Terminales de transaccion", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub AdministrarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AdministrarToolStripMenuItem.Click
        MostrarFormulario(frmAdministrarVenta, "Administrar Ventas", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ConsultaDeFacturasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultaDeFacturasToolStripMenuItem.Click
        MostrarFormulario(frmConsultaVenta, "Consultar Ventas", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen)
    End Sub

    Private Sub IngresoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresoToolStripMenuItem.Click
        MostrarFormulario(frmChequeCliente, "Ingreso de Cheques de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ConsultaDeChequesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultaDeChequesToolStripMenuItem.Click
        MostrarFormulario(frmConsultaChequeCliente, "Consulta de Cheques de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub PruebaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PruebaToolStripMenuItem.Click
        MostrarFormulario(frmAplicacionProveedoresAnticipo, "Cobranza de Comprobantes Credito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ConsultaDeEfectivoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultaDeEfectivoToolStripMenuItem.Click
        MostrarFormulario(frmConsultaEfectivo, "Consulta de Efectivos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ConsultaDePagosConDocumentosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultaDePagosConDocumentosToolStripMenuItem.Click
        MostrarFormulario(frmConsultaFormaPagoDocumento, "Consulta de Pagos con Documentos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ComprasToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MostrarFormulario(frmCompra, "Compra", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub MovimientosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MovimientosToolStripMenuItem.Click
        MostrarFormulario(frmMovimientoStock, "Movimiento de Stock", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub NuevoToolStripMenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NuevoToolStripMenuItem5.Click
        MostrarFormulario(frmLoteDistribucion, "Lotes de Distribucion", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub CompraDeProductoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CompraDeProductoToolStripMenuItem.Click
        MostrarFormulario(frmCompra, "Compra de Productos", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub GastosToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GastosToolStripMenuItem1.Click
        MostrarFormulario(frmGastos, "Gastos", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ChoferesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChoferesToolStripMenuItem.Click
        MostrarFormulario(frmChofer, "Choferes", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ZonasDeVentaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZonasDeVentaToolStripMenuItem.Click
        MostrarFormulario(frmZonaVenta, "Zona de Ventas", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub CamionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CamionesToolStripMenuItem.Click
        MostrarFormulario(frmCamion, "Camiones para Distribucion", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub TransportistasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TransportistasToolStripMenuItem.Click
        MostrarFormulario(frmTransporte, "Predefinir Transportes de Distribucion", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ConsultaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultaToolStripMenuItem.Click
        MostrarFormulario(frmConsultaLoteDistribucion, "Consulta de Lote de Distribuciones", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen)
    End Sub

    Private Sub CargaDeMercaderiasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CargaDeMercaderiasToolStripMenuItem.Click
        MostrarFormulario(frmCargaMercaderia, "Control y Carga de Mercaderias", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub DevolucionDeLoteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DevolucionDeLoteToolStripMenuItem.Click
        MostrarFormulario(frmDevolucionLote, "Devolucion de Lote", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub RendicionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RendicionToolStripMenuItem.Click
        MostrarFormulario(frmRendicionLote, "Rendicion de Lotes de Ventas", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub CobranzasContadoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CobranzasContadoToolStripMenuItem.Click
        MostrarFormulario(frmCobranzaContado, "Cobranza por Lote", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub OrderDePagoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OrderDePagoToolStripMenuItem.Click
        MostrarFormulario(frmOrdenPago, "Orden de Pago", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub DepositoBancarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DepositoBancarioToolStripMenuItem.Click
        MostrarFormulario(frmDepositoBancario, "Deposito Bancario", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ImportarBDToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImportarBDToolStripMenuItem.Click
        MostrarFormulario(frmImportarDatos, "Importar BD", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub CuentasBancariasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CuentasBancariasToolStripMenuItem.Click
        MostrarFormulario(frmCuentaBancaria, "Cuentas Bancarias", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub DebitosYCreditosBancariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DebitosYCreditosBancariosToolStripMenuItem.Click
        MostrarFormulario(frmDebitoCreditoBancario, "Debito Credito Bancario", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub ActualizarDatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActualizarDatosToolStripMenuItem.Click
        Dim frm As New frmDescarga
        frm.ShowDialog()
    End Sub

    Private Sub ImpresionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImpresionToolStripMenuItem.Click
        MostrarFormulario(frmImpersion, "Debito Credito Bancario", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub

    Private Sub SalirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        MostrarArbol()
    End Sub

    Sub MostrarArbol()

        For Each menu1 As ToolStripMenuItem In Me.MainMenuStrip.Items

            Debug.Print(menu1.Text)

            If menu1.DropDownItems.Count > 0 Then
                MostrarArbol(menu1, 0)
            End If

        Next

    End Sub

    Sub MostrarArbol(ByVal menu1 As ToolStripMenuItem, ByVal Tab As Integer)

        Dim Espacio As String = ""
        For i As Integer = 0 To Tab
            Espacio = Espacio & vbTab
        Next


        For i As Integer = 0 To menu1.DropDownItems.Count - 1


            Dim Tipo As String = menu1.DropDownItems.Item(i).GetType.ToString()


            If Tipo <> "System.Windows.Forms.ToolStripMenuItem" Then
                GoTo siguiente
            End If

            Dim menu2 As ToolStripMenuItem = menu1.DropDownItems.Item(i)

            Debug.Print(Espacio & menu2.Text)

            If menu2.DropDownItems.Count > 0 Then
                MostrarArbol(menu2, Tab + 1)
            End If

siguiente:

        Next

    End Sub

    Private Sub AgrupadorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgrupadorToolStripMenuItem.Click
        MostrarFormulario(frmClasificacionProductoAgrupador, "Agrupador de Clasificaciones", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen)
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmPrincipal_Activate()
        Me.Refresh()
    End Sub
End Class

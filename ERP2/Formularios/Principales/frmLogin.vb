﻿
Public Class frmLogin

    Dim CSistema As CSistema
    Dim CarchivoInicio As CArchivoInicio
    Private strHostName As String = Nothing

    Private strIPAddress As String = Nothing



    Private UsuarioValue As String
    Public Property Usuario() As String
        Get
            Return UsuarioValue
        End Get
        Set(ByVal value As String)
            UsuarioValue = value
        End Set
    End Property

    Private PasswordValue As String
    Public Property Password() As String
        Get
            Return PasswordValue
        End Get
        Set(ByVal value As String)
            PasswordValue = value
        End Set
    End Property

    Private PermitirNuevaBDValue As Boolean
    Public Property PermitirNuevaBD() As Boolean
        Get
            Return PermitirNuevaBDValue
        End Get
        Set(ByVal value As Boolean)
            PermitirNuevaBDValue = value
        End Set
    End Property

    Overridable Sub Inicializar()
        Try
            CSistema = New CSistema
            CarchivoInicio = New CArchivoInicio

            Me.AcceptButton = New Button

            txtUsuario.AutoSize = False
            txtUsuario.Height = 22
            txtContraseña.AutoSize = False
            txtContraseña.Height = 22

            'Info
            lblInfoSistema.Text = VGSoftwareVersion
            ''lblInfoDesarrollador.Text = VGSoftwareDesarrolladorInfo
            lblSucursal.Text = vgSucursal
            lblDeposito.Text = vgDeposito
            lblTerminal.Text = vgTerminal
            lblInfoSistema.Text = VGSoftwareNombre & " - v." & VGSoftwareVersion & " - Últ. Actualización: " & VGSoftwareUltimaActualizacion
            lblLicenciaOtorgada.Text = vgLicenciaOtorgado

            'Configuraciones
            txtUsuario.Text = CarchivoInicio.IniGet(VGArchivoINI, "INICIO", "USUARIO", "")
            chkModoPedido.Checked = CarchivoInicio.IniGet(VGArchivoINI, "INICIO", "MODO PEDIDO", False)
            If txtUsuario.Text <> "" OrElse txtUsuario.Text <> Nothing Then
                txtContraseña.Select()
                txtContraseña.Focus()
            End If

            Me.Icon = My.Resources.SAIN

            GetIPAddress()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Overridable Sub Login()
        Try
            Dim SQL As String = "Exec SpAcceso "
            CSistema.ConcatenarParametro(SQL, "@Usuario", Usuario)
            CSistema.ConcatenarParametro(SQL, "@Password", Password)
            CSistema.ConcatenarParametro(SQL, "@IDTerminal", vgIDTerminal)
            CSistema.ConcatenarParametro(SQL, "@Version", VGSoftwareVersion)
            CSistema.ConcatenarParametro(SQL, "@IP", strIPAddress & " [" & strHostName & "]")

            'Verificamos que la cadena de conexion se haya cargado completamente
            If FGEStablecerCadenaConexion() = False Then
                MessageBox.Show("La cadena de conexion no esta configurada correctamente! No se podra establecer conexion con el servidor hasta que se solucione el problema.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If

            Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL, "", 10)
            If dt Is Nothing Then
                MessageBox.Show("¡No se puede acceser al sistema! Error de datos", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If

            If dt.Rows.Count = 0 Then
                MessageBox.Show("¡No se puede acceser al sistema! Error de datos", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If

            Dim oRow As DataRow = dt.Rows(0)

            If CBool(oRow("Procesado")) = False Then
                MessageBox.Show(oRow("Mensaje"), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txtUsuario.Focus()
                txtUsuario.SelectAll()
                Exit Sub
            End If

            vgIDUsuario = oRow("ID").ToString
            vgUsuario = oRow("Usuario").ToString
            vgIDPerfil = oRow("IDPerfil").ToString
            vgUsuarioIdentificador = oRow("Identificador").ToString
            vgPerfil = oRow("Perfil").ToString
            vgVerCosto = CBool(oRow("VerCosto").ToString)

            'Vendedor
            vgUsuarioEsVendedor = CSistema.RetornarValorBoolean(oRow("EsVendedor").ToString)
            vgUsuarioVendedor = oRow("Vendedor").ToString
            If vgUsuarioEsVendedor Then
                vgUsuarioIDVendedor = oRow("IDVendedor").ToString
            End If

            'Chofer
            vgUsuarioEsChofer = CSistema.RetornarValorBoolean(oRow("EsChofer").ToString)
            vgUsuarioChofer = oRow("Chofer").ToString
            If vgUsuarioEsChofer Then
                vgUsuarioIDChofer = oRow("IDChofer").ToString
            End If

            CarchivoInicio.IniWrite(VGArchivoINI, "INICIO", "USUARIO", txtUsuario.Text)

            If vgUsuarioEsVendedor AndAlso chkModoPedido.Checked Then
                'FGMostrarFormulario(Me, frmPedido, "Pedido", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, False)
                Dim frm As New frmPedido
                frm.Text = "Pedidos [Modo vendedor]"
                frm.Icon = My.Resources.SAIN
                Me.Visible = False
                FGEstiloFormulario(frm)
                My.Application.ApplicationContext.MainForm = frm
                frm.Show()
            Else
                Dim frm As New frmDescarga
                Me.Visible = False
                FGEstiloFormulario(frm)
                frm.ShowDialog()

                My.Application.ApplicationContext.MainForm = frmPrincipal2
                FGEstiloFormulario(frmPrincipal2)
                frmPrincipal2.Show()
            End If

            CarchivoInicio.IniWrite(VGArchivoINI, "INICIO", "MODO PEDIDO", chkModoPedido.Checked)

            Me.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        'GetIPAddress()

    End Sub

    Sub Configuracion()

        'Formulario de Servidor
        Dim frm As New Form
        Dim ocx As New ocxConexionBaseDeDatos
        ocx.HabilitarNuevaBaseDatos = PermitirNuevaBD
        frm.Controls.Add(ocx)
        ocx.Dock = DockStyle.Fill
        frm.Text = "Configuración de Datos"
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Size = New Size(ocx.Width + 48, ocx.Height + 46)
        frm.MaximizeBox = False
        frm.ShowDialog()

        If ocx.Procesado = True Then
            If FGEstablecerConexion() = False Then

            End If
        End If

    End Sub

    Private Sub frmLogin_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
   
    End Sub

    Private Sub frmLogin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        End
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Login()
    End Sub

    Private Sub GetIPAddress()
        Try
            

            strHostName = System.Net.Dns.GetHostName()

            strIPAddress = System.Net.Dns.GetHostByName(strHostName).AddressList(0).ToString()
        Catch 
        End Try

       

    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Configuracion()
    End Sub

    Private Sub txtUsuario_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.KeyUp
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            txtContraseña.Focus()
            txtContraseña.SelectAll()
        End If
    End Sub

    Private Sub txtUsuario_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUsuario.TextChanged
        Usuario = txtUsuario.Text
    End Sub

    Private Sub txtContraseña_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtContraseña.KeyUp
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Login()
        End If
    End Sub

    Private Sub txtContraseña_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtContraseña.TextChanged
        Password = txtContraseña.Text
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub FrmLogin_Activate()
        Me.Refresh()
    End Sub

    Private Sub chkModoPedido_CheckedChanged(sender As Object, e As EventArgs) Handles chkModoPedido.CheckedChanged

    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click

    End Sub

    Private Sub Panel2_Paint(sender As Object, e As PaintEventArgs) Handles Panel2.Paint

    End Sub
End Class
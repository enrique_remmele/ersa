﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrincipal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrincipal))
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.InicioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambiarUsuarioContraseñaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CerrarSesionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntidadesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PersonalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PersonasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VendedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CobradoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PromotoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DistribuidoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.OtrosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PaisCiudadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ZonasDeVentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadoDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.TipoDeProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LineaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MarcaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CategoriasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnidadesDeMedidaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.TransportistasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChoferesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CamionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModelosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TiposDeCamionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.TerminalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Sucursales6ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MonedaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CotizacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BancosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CuentasBancariasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.RegistrosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MotivosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PuntosDeExpedicionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdministrarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaDeFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoGeneralDeVentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.CuentasACobrarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasPorClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasPorProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentaAnualPorClienteImporteCantidadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasBajoElCostoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasYComisionesPorVendedorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasYDescuentosPorVendedorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LotesDeComprobantesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevoToolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RendicionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeRendicionPorLoteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemisionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PorLoteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevoToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoGeneralDeRemisionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotasDeCreditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PedidosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListasDePrecioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdministrarToolStripMenuItem6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DescuentosTacticosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComprasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CompraDeProductoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaDeComprasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComprasPorProductoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComprasPorProveedorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CuentasAPagarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.AnticiposToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GastosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AplicarAnticipoAGastosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MovimientosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaDeMercaderiasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DevolucionDeLoteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InventarioInicialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeMovimientosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenDeExistenciaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosAReponerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MovimientosPorProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanillaParaTomaDeInventarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TesoreriaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PruebaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CobranzasContadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OrderDePagoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DebitosYCreditosBancariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChequesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaDeChequesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DescuentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaDeEfectivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaDePagosConDocumentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DepositoBancarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PagoChequeClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DescuentoDeChequesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetalleGastoCreditoGastoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DepositoBancarioRechazadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContabilidadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanDeCuentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AsientosContablesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RRHToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HerramientasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CuentasFijasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfiguracionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImportarBDToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImpresionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SeguridadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel8 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsslFecha = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblSucursal = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel4 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblDeposito = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblTerminal = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel6 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblUsuario = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripSplitButton()
        Me.CambiarImagenDeFonoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PredeterminadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.AgrupadorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip.SuspendLayout()
        Me.StatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip
        '
        Me.MenuStrip.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InicioToolStripMenuItem, Me.EntidadesToolStripMenuItem, Me.VentasToolStripMenuItem, Me.ComprasToolStripMenuItem, Me.StockToolStripMenuItem, Me.TesoreriaToolStripMenuItem, Me.ContabilidadToolStripMenuItem, Me.RRHToolStripMenuItem, Me.HerramientasToolStripMenuItem, Me.SeguridadToolStripMenuItem})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Size = New System.Drawing.Size(815, 24)
        Me.MenuStrip.TabIndex = 5
        Me.MenuStrip.Text = "MenuStrip"
        '
        'InicioToolStripMenuItem
        '
        Me.InicioToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ActualizarDatosToolStripMenuItem, Me.CambiarUsuarioContraseñaToolStripMenuItem, Me.CerrarSesionToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.InicioToolStripMenuItem.Image = CType(resources.GetObject("InicioToolStripMenuItem.Image"), System.Drawing.Image)
        Me.InicioToolStripMenuItem.Name = "InicioToolStripMenuItem"
        Me.InicioToolStripMenuItem.Size = New System.Drawing.Size(66, 20)
        Me.InicioToolStripMenuItem.Text = "Inicio"
        '
        'ActualizarDatosToolStripMenuItem
        '
        Me.ActualizarDatosToolStripMenuItem.Name = "ActualizarDatosToolStripMenuItem"
        Me.ActualizarDatosToolStripMenuItem.Size = New System.Drawing.Size(252, 22)
        Me.ActualizarDatosToolStripMenuItem.Text = "Actualizar Datos"
        '
        'CambiarUsuarioContraseñaToolStripMenuItem
        '
        Me.CambiarUsuarioContraseñaToolStripMenuItem.Image = CType(resources.GetObject("CambiarUsuarioContraseñaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CambiarUsuarioContraseñaToolStripMenuItem.Name = "CambiarUsuarioContraseñaToolStripMenuItem"
        Me.CambiarUsuarioContraseñaToolStripMenuItem.Size = New System.Drawing.Size(252, 22)
        Me.CambiarUsuarioContraseñaToolStripMenuItem.Text = "Cambiar Usuario-Contraseña"
        '
        'CerrarSesionToolStripMenuItem
        '
        Me.CerrarSesionToolStripMenuItem.Image = CType(resources.GetObject("CerrarSesionToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CerrarSesionToolStripMenuItem.Name = "CerrarSesionToolStripMenuItem"
        Me.CerrarSesionToolStripMenuItem.Size = New System.Drawing.Size(252, 22)
        Me.CerrarSesionToolStripMenuItem.Text = "Cerrar Sesion"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Image = CType(resources.GetObject("SalirToolStripMenuItem.Image"), System.Drawing.Image)
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(252, 22)
        Me.SalirToolStripMenuItem.Text = "Salir"
        '
        'EntidadesToolStripMenuItem
        '
        Me.EntidadesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientesToolStripMenuItem, Me.ProveedoresToolStripMenuItem, Me.ProductosToolStripMenuItem, Me.PersonalesToolStripMenuItem, Me.PersonasToolStripMenuItem, Me.ToolStripSeparator3, Me.OtrosToolStripMenuItem, Me.ToolStripSeparator4, Me.TerminalesToolStripMenuItem, Me.Sucursales6ToolStripMenuItem, Me.ToolStripSeparator1, Me.MonedaToolStripMenuItem, Me.CotizacionesToolStripMenuItem, Me.BancosToolStripMenuItem, Me.CuentasBancariasToolStripMenuItem, Me.ToolStripSeparator6, Me.RegistrosToolStripMenuItem})
        Me.EntidadesToolStripMenuItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EntidadesToolStripMenuItem.Image = CType(resources.GetObject("EntidadesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.EntidadesToolStripMenuItem.Name = "EntidadesToolStripMenuItem"
        Me.EntidadesToolStripMenuItem.Size = New System.Drawing.Size(97, 20)
        Me.EntidadesToolStripMenuItem.Text = "Entidades"
        '
        'ClientesToolStripMenuItem
        '
        Me.ClientesToolStripMenuItem.Image = CType(resources.GetObject("ClientesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ClientesToolStripMenuItem.Name = "ClientesToolStripMenuItem"
        Me.ClientesToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.ClientesToolStripMenuItem.Text = "Clientes"
        '
        'ProveedoresToolStripMenuItem
        '
        Me.ProveedoresToolStripMenuItem.Image = CType(resources.GetObject("ProveedoresToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ProveedoresToolStripMenuItem.Name = "ProveedoresToolStripMenuItem"
        Me.ProveedoresToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.ProveedoresToolStripMenuItem.Text = "Proveedores"
        '
        'ProductosToolStripMenuItem
        '
        Me.ProductosToolStripMenuItem.Image = CType(resources.GetObject("ProductosToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ProductosToolStripMenuItem.Name = "ProductosToolStripMenuItem"
        Me.ProductosToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.ProductosToolStripMenuItem.Text = "Productos"
        '
        'PersonalesToolStripMenuItem
        '
        Me.PersonalesToolStripMenuItem.Image = CType(resources.GetObject("PersonalesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PersonalesToolStripMenuItem.Name = "PersonalesToolStripMenuItem"
        Me.PersonalesToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.PersonalesToolStripMenuItem.Text = "Personales"
        '
        'PersonasToolStripMenuItem
        '
        Me.PersonasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VendedoresToolStripMenuItem, Me.CobradoresToolStripMenuItem, Me.PromotoresToolStripMenuItem, Me.DistribuidoresToolStripMenuItem})
        Me.PersonasToolStripMenuItem.Name = "PersonasToolStripMenuItem"
        Me.PersonasToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.PersonasToolStripMenuItem.Text = "Personas"
        '
        'VendedoresToolStripMenuItem
        '
        Me.VendedoresToolStripMenuItem.Image = CType(resources.GetObject("VendedoresToolStripMenuItem.Image"), System.Drawing.Image)
        Me.VendedoresToolStripMenuItem.Name = "VendedoresToolStripMenuItem"
        Me.VendedoresToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.VendedoresToolStripMenuItem.Text = "Vendedores"
        '
        'CobradoresToolStripMenuItem
        '
        Me.CobradoresToolStripMenuItem.Image = CType(resources.GetObject("CobradoresToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CobradoresToolStripMenuItem.Name = "CobradoresToolStripMenuItem"
        Me.CobradoresToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.CobradoresToolStripMenuItem.Text = "Cobradores"
        '
        'PromotoresToolStripMenuItem
        '
        Me.PromotoresToolStripMenuItem.Image = CType(resources.GetObject("PromotoresToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PromotoresToolStripMenuItem.Name = "PromotoresToolStripMenuItem"
        Me.PromotoresToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.PromotoresToolStripMenuItem.Text = "Promotores"
        '
        'DistribuidoresToolStripMenuItem
        '
        Me.DistribuidoresToolStripMenuItem.Image = CType(resources.GetObject("DistribuidoresToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DistribuidoresToolStripMenuItem.Name = "DistribuidoresToolStripMenuItem"
        Me.DistribuidoresToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.DistribuidoresToolStripMenuItem.Text = "Distribuidores"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(216, 6)
        '
        'OtrosToolStripMenuItem
        '
        Me.OtrosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PaisCiudadToolStripMenuItem, Me.TipoClienteToolStripMenuItem, Me.ZonasDeVentaToolStripMenuItem, Me.EstadoDeClientesToolStripMenuItem, Me.ToolStripSeparator2, Me.TipoDeProductosToolStripMenuItem, Me.LineaToolStripMenuItem, Me.MarcaToolStripMenuItem, Me.ToolStripMenuItem1, Me.CategoriasToolStripMenuItem, Me.AgrupadorToolStripMenuItem, Me.UnidadesDeMedidaToolStripMenuItem, Me.ToolStripSeparator5, Me.TransportistasToolStripMenuItem, Me.ChoferesToolStripMenuItem, Me.CamionesToolStripMenuItem, Me.ModelosToolStripMenuItem, Me.TiposDeCamionesToolStripMenuItem})
        Me.OtrosToolStripMenuItem.Name = "OtrosToolStripMenuItem"
        Me.OtrosToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.OtrosToolStripMenuItem.Text = "Varios"
        '
        'PaisCiudadToolStripMenuItem
        '
        Me.PaisCiudadToolStripMenuItem.Image = CType(resources.GetObject("PaisCiudadToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PaisCiudadToolStripMenuItem.Name = "PaisCiudadToolStripMenuItem"
        Me.PaisCiudadToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.PaisCiudadToolStripMenuItem.Text = "Localidades"
        '
        'TipoClienteToolStripMenuItem
        '
        Me.TipoClienteToolStripMenuItem.Image = CType(resources.GetObject("TipoClienteToolStripMenuItem.Image"), System.Drawing.Image)
        Me.TipoClienteToolStripMenuItem.Name = "TipoClienteToolStripMenuItem"
        Me.TipoClienteToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.TipoClienteToolStripMenuItem.Text = "Tipo Cliente"
        '
        'ZonasDeVentaToolStripMenuItem
        '
        Me.ZonasDeVentaToolStripMenuItem.Image = CType(resources.GetObject("ZonasDeVentaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ZonasDeVentaToolStripMenuItem.Name = "ZonasDeVentaToolStripMenuItem"
        Me.ZonasDeVentaToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.ZonasDeVentaToolStripMenuItem.Text = "Zonas de Venta"
        '
        'EstadoDeClientesToolStripMenuItem
        '
        Me.EstadoDeClientesToolStripMenuItem.Name = "EstadoDeClientesToolStripMenuItem"
        Me.EstadoDeClientesToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.EstadoDeClientesToolStripMenuItem.Text = "Estado de Clientes"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(204, 6)
        '
        'TipoDeProductosToolStripMenuItem
        '
        Me.TipoDeProductosToolStripMenuItem.Image = CType(resources.GetObject("TipoDeProductosToolStripMenuItem.Image"), System.Drawing.Image)
        Me.TipoDeProductosToolStripMenuItem.Name = "TipoDeProductosToolStripMenuItem"
        Me.TipoDeProductosToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.TipoDeProductosToolStripMenuItem.Text = "Tipo de Productos"
        '
        'LineaToolStripMenuItem
        '
        Me.LineaToolStripMenuItem.Image = CType(resources.GetObject("LineaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.LineaToolStripMenuItem.Name = "LineaToolStripMenuItem"
        Me.LineaToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.LineaToolStripMenuItem.Text = "Lineas"
        '
        'MarcaToolStripMenuItem
        '
        Me.MarcaToolStripMenuItem.Image = CType(resources.GetObject("MarcaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.MarcaToolStripMenuItem.Name = "MarcaToolStripMenuItem"
        Me.MarcaToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.MarcaToolStripMenuItem.Text = "Marcas"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(207, 22)
        Me.ToolStripMenuItem1.Text = "Presentaciónes"
        '
        'CategoriasToolStripMenuItem
        '
        Me.CategoriasToolStripMenuItem.Name = "CategoriasToolStripMenuItem"
        Me.CategoriasToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.CategoriasToolStripMenuItem.Text = "Categorias"
        '
        'UnidadesDeMedidaToolStripMenuItem
        '
        Me.UnidadesDeMedidaToolStripMenuItem.Image = CType(resources.GetObject("UnidadesDeMedidaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.UnidadesDeMedidaToolStripMenuItem.Name = "UnidadesDeMedidaToolStripMenuItem"
        Me.UnidadesDeMedidaToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.UnidadesDeMedidaToolStripMenuItem.Text = "Unidades de Medida"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(204, 6)
        '
        'TransportistasToolStripMenuItem
        '
        Me.TransportistasToolStripMenuItem.Name = "TransportistasToolStripMenuItem"
        Me.TransportistasToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.TransportistasToolStripMenuItem.Text = "Transportistas"
        '
        'ChoferesToolStripMenuItem
        '
        Me.ChoferesToolStripMenuItem.Name = "ChoferesToolStripMenuItem"
        Me.ChoferesToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.ChoferesToolStripMenuItem.Text = "Choferes"
        '
        'CamionesToolStripMenuItem
        '
        Me.CamionesToolStripMenuItem.Image = CType(resources.GetObject("CamionesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CamionesToolStripMenuItem.Name = "CamionesToolStripMenuItem"
        Me.CamionesToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.CamionesToolStripMenuItem.Text = "Camiones"
        '
        'ModelosToolStripMenuItem
        '
        Me.ModelosToolStripMenuItem.Name = "ModelosToolStripMenuItem"
        Me.ModelosToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.ModelosToolStripMenuItem.Text = "Modelos"
        '
        'TiposDeCamionesToolStripMenuItem
        '
        Me.TiposDeCamionesToolStripMenuItem.Name = "TiposDeCamionesToolStripMenuItem"
        Me.TiposDeCamionesToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.TiposDeCamionesToolStripMenuItem.Text = "Tipos de Camiones"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(216, 6)
        '
        'TerminalesToolStripMenuItem
        '
        Me.TerminalesToolStripMenuItem.Image = CType(resources.GetObject("TerminalesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.TerminalesToolStripMenuItem.Name = "TerminalesToolStripMenuItem"
        Me.TerminalesToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.TerminalesToolStripMenuItem.Text = "Terminales"
        '
        'Sucursales6ToolStripMenuItem
        '
        Me.Sucursales6ToolStripMenuItem.Image = CType(resources.GetObject("Sucursales6ToolStripMenuItem.Image"), System.Drawing.Image)
        Me.Sucursales6ToolStripMenuItem.Name = "Sucursales6ToolStripMenuItem"
        Me.Sucursales6ToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.Sucursales6ToolStripMenuItem.Text = "Sucursales - Depositos"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(216, 6)
        '
        'MonedaToolStripMenuItem
        '
        Me.MonedaToolStripMenuItem.Image = CType(resources.GetObject("MonedaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.MonedaToolStripMenuItem.Name = "MonedaToolStripMenuItem"
        Me.MonedaToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.MonedaToolStripMenuItem.Text = "Monedas"
        '
        'CotizacionesToolStripMenuItem
        '
        Me.CotizacionesToolStripMenuItem.Image = CType(resources.GetObject("CotizacionesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CotizacionesToolStripMenuItem.Name = "CotizacionesToolStripMenuItem"
        Me.CotizacionesToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.CotizacionesToolStripMenuItem.Text = "Cotizaciones"
        '
        'BancosToolStripMenuItem
        '
        Me.BancosToolStripMenuItem.Image = CType(resources.GetObject("BancosToolStripMenuItem.Image"), System.Drawing.Image)
        Me.BancosToolStripMenuItem.Name = "BancosToolStripMenuItem"
        Me.BancosToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.BancosToolStripMenuItem.Text = "Bancos"
        '
        'CuentasBancariasToolStripMenuItem
        '
        Me.CuentasBancariasToolStripMenuItem.Name = "CuentasBancariasToolStripMenuItem"
        Me.CuentasBancariasToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.CuentasBancariasToolStripMenuItem.Text = "Cuentas Bancarias"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(216, 6)
        '
        'RegistrosToolStripMenuItem
        '
        Me.RegistrosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem2, Me.MotivosToolStripMenuItem, Me.PuntosDeExpedicionToolStripMenuItem})
        Me.RegistrosToolStripMenuItem.Image = CType(resources.GetObject("RegistrosToolStripMenuItem.Image"), System.Drawing.Image)
        Me.RegistrosToolStripMenuItem.Name = "RegistrosToolStripMenuItem"
        Me.RegistrosToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.RegistrosToolStripMenuItem.Text = "Registros"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Image = CType(resources.GetObject("ToolStripMenuItem2.Image"), System.Drawing.Image)
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(226, 22)
        Me.ToolStripMenuItem2.Text = "Tipos de Comprobantes"
        '
        'MotivosToolStripMenuItem
        '
        Me.MotivosToolStripMenuItem.Name = "MotivosToolStripMenuItem"
        Me.MotivosToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.MotivosToolStripMenuItem.Text = "Motivos"
        '
        'PuntosDeExpedicionToolStripMenuItem
        '
        Me.PuntosDeExpedicionToolStripMenuItem.Name = "PuntosDeExpedicionToolStripMenuItem"
        Me.PuntosDeExpedicionToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.PuntosDeExpedicionToolStripMenuItem.Text = "Puntos de Expedicion"
        '
        'VentasToolStripMenuItem
        '
        Me.VentasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FacturacionToolStripMenuItem, Me.LotesDeComprobantesToolStripMenuItem, Me.RemisionesToolStripMenuItem, Me.NotasDeCreditoToolStripMenuItem, Me.ToolStripMenuItem3, Me.PedidosToolStripMenuItem, Me.ListasDePrecioToolStripMenuItem})
        Me.VentasToolStripMenuItem.Name = "VentasToolStripMenuItem"
        Me.VentasToolStripMenuItem.Size = New System.Drawing.Size(69, 20)
        Me.VentasToolStripMenuItem.Text = "Ingresos"
        '
        'FacturacionToolStripMenuItem
        '
        Me.FacturacionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevoToolStripMenuItem, Me.AdministrarToolStripMenuItem, Me.ConsultaDeFacturasToolStripMenuItem, Me.ListadoGeneralDeVentasToolStripMenuItem, Me.ToolStripSeparator7, Me.CuentasACobrarToolStripMenuItem, Me.VentasPorClienteToolStripMenuItem, Me.VentasPorProductosToolStripMenuItem, Me.VentaAnualPorClienteImporteCantidadToolStripMenuItem, Me.VentasBajoElCostoToolStripMenuItem, Me.VentasYComisionesPorVendedorToolStripMenuItem, Me.VentasYDescuentosPorVendedorToolStripMenuItem})
        Me.FacturacionToolStripMenuItem.Name = "FacturacionToolStripMenuItem"
        Me.FacturacionToolStripMenuItem.Size = New System.Drawing.Size(224, 22)
        Me.FacturacionToolStripMenuItem.Text = "Ventas"
        '
        'NuevoToolStripMenuItem
        '
        Me.NuevoToolStripMenuItem.Name = "NuevoToolStripMenuItem"
        Me.NuevoToolStripMenuItem.Size = New System.Drawing.Size(325, 22)
        Me.NuevoToolStripMenuItem.Text = "Nuevo"
        '
        'AdministrarToolStripMenuItem
        '
        Me.AdministrarToolStripMenuItem.Name = "AdministrarToolStripMenuItem"
        Me.AdministrarToolStripMenuItem.Size = New System.Drawing.Size(325, 22)
        Me.AdministrarToolStripMenuItem.Text = "Administrar"
        '
        'ConsultaDeFacturasToolStripMenuItem
        '
        Me.ConsultaDeFacturasToolStripMenuItem.Name = "ConsultaDeFacturasToolStripMenuItem"
        Me.ConsultaDeFacturasToolStripMenuItem.Size = New System.Drawing.Size(325, 22)
        Me.ConsultaDeFacturasToolStripMenuItem.Text = "Consulta de Facturas"
        '
        'ListadoGeneralDeVentasToolStripMenuItem
        '
        Me.ListadoGeneralDeVentasToolStripMenuItem.Name = "ListadoGeneralDeVentasToolStripMenuItem"
        Me.ListadoGeneralDeVentasToolStripMenuItem.Size = New System.Drawing.Size(325, 22)
        Me.ListadoGeneralDeVentasToolStripMenuItem.Text = "Listado General de Ventas"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(322, 6)
        '
        'CuentasACobrarToolStripMenuItem
        '
        Me.CuentasACobrarToolStripMenuItem.Name = "CuentasACobrarToolStripMenuItem"
        Me.CuentasACobrarToolStripMenuItem.Size = New System.Drawing.Size(325, 22)
        Me.CuentasACobrarToolStripMenuItem.Text = "Cuentas a Cobrar"
        '
        'VentasPorClienteToolStripMenuItem
        '
        Me.VentasPorClienteToolStripMenuItem.Name = "VentasPorClienteToolStripMenuItem"
        Me.VentasPorClienteToolStripMenuItem.Size = New System.Drawing.Size(325, 22)
        Me.VentasPorClienteToolStripMenuItem.Text = "Ventas por Cliente"
        '
        'VentasPorProductosToolStripMenuItem
        '
        Me.VentasPorProductosToolStripMenuItem.Name = "VentasPorProductosToolStripMenuItem"
        Me.VentasPorProductosToolStripMenuItem.Size = New System.Drawing.Size(325, 22)
        Me.VentasPorProductosToolStripMenuItem.Text = "Ventas por Productos"
        '
        'VentaAnualPorClienteImporteCantidadToolStripMenuItem
        '
        Me.VentaAnualPorClienteImporteCantidadToolStripMenuItem.Name = "VentaAnualPorClienteImporteCantidadToolStripMenuItem"
        Me.VentaAnualPorClienteImporteCantidadToolStripMenuItem.Size = New System.Drawing.Size(325, 22)
        Me.VentaAnualPorClienteImporteCantidadToolStripMenuItem.Text = "Venta anual por cliente: Importe/Cantidad"
        '
        'VentasBajoElCostoToolStripMenuItem
        '
        Me.VentasBajoElCostoToolStripMenuItem.Name = "VentasBajoElCostoToolStripMenuItem"
        Me.VentasBajoElCostoToolStripMenuItem.Size = New System.Drawing.Size(325, 22)
        Me.VentasBajoElCostoToolStripMenuItem.Text = "Ventas bajo el costo"
        '
        'VentasYComisionesPorVendedorToolStripMenuItem
        '
        Me.VentasYComisionesPorVendedorToolStripMenuItem.Name = "VentasYComisionesPorVendedorToolStripMenuItem"
        Me.VentasYComisionesPorVendedorToolStripMenuItem.Size = New System.Drawing.Size(325, 22)
        Me.VentasYComisionesPorVendedorToolStripMenuItem.Text = "Ventas y comisiones por Vendedor"
        '
        'VentasYDescuentosPorVendedorToolStripMenuItem
        '
        Me.VentasYDescuentosPorVendedorToolStripMenuItem.Name = "VentasYDescuentosPorVendedorToolStripMenuItem"
        Me.VentasYDescuentosPorVendedorToolStripMenuItem.Size = New System.Drawing.Size(325, 22)
        Me.VentasYDescuentosPorVendedorToolStripMenuItem.Text = "Ventas y descuentos por Vendedor"
        '
        'LotesDeComprobantesToolStripMenuItem
        '
        Me.LotesDeComprobantesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevoToolStripMenuItem5, Me.RendicionToolStripMenuItem, Me.ConsultaToolStripMenuItem, Me.ListadoDeRendicionPorLoteToolStripMenuItem})
        Me.LotesDeComprobantesToolStripMenuItem.Name = "LotesDeComprobantesToolStripMenuItem"
        Me.LotesDeComprobantesToolStripMenuItem.Size = New System.Drawing.Size(224, 22)
        Me.LotesDeComprobantesToolStripMenuItem.Text = "Lotes de Comprobantes"
        '
        'NuevoToolStripMenuItem5
        '
        Me.NuevoToolStripMenuItem5.Name = "NuevoToolStripMenuItem5"
        Me.NuevoToolStripMenuItem5.Size = New System.Drawing.Size(258, 22)
        Me.NuevoToolStripMenuItem5.Text = "Cargar"
        '
        'RendicionToolStripMenuItem
        '
        Me.RendicionToolStripMenuItem.Name = "RendicionToolStripMenuItem"
        Me.RendicionToolStripMenuItem.Size = New System.Drawing.Size(258, 22)
        Me.RendicionToolStripMenuItem.Text = "Rendicion"
        '
        'ConsultaToolStripMenuItem
        '
        Me.ConsultaToolStripMenuItem.Name = "ConsultaToolStripMenuItem"
        Me.ConsultaToolStripMenuItem.Size = New System.Drawing.Size(258, 22)
        Me.ConsultaToolStripMenuItem.Text = "Consulta"
        '
        'ListadoDeRendicionPorLoteToolStripMenuItem
        '
        Me.ListadoDeRendicionPorLoteToolStripMenuItem.Name = "ListadoDeRendicionPorLoteToolStripMenuItem"
        Me.ListadoDeRendicionPorLoteToolStripMenuItem.Size = New System.Drawing.Size(258, 22)
        Me.ListadoDeRendicionPorLoteToolStripMenuItem.Text = "Listado de Rendicion por Lote"
        '
        'RemisionesToolStripMenuItem
        '
        Me.RemisionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PorLoteToolStripMenuItem, Me.NuevoToolStripMenuItem2, Me.ListadoGeneralDeRemisionesToolStripMenuItem})
        Me.RemisionesToolStripMenuItem.Name = "RemisionesToolStripMenuItem"
        Me.RemisionesToolStripMenuItem.Size = New System.Drawing.Size(224, 22)
        Me.RemisionesToolStripMenuItem.Text = "Remisiones"
        '
        'PorLoteToolStripMenuItem
        '
        Me.PorLoteToolStripMenuItem.Name = "PorLoteToolStripMenuItem"
        Me.PorLoteToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.PorLoteToolStripMenuItem.Text = "Por lote"
        '
        'NuevoToolStripMenuItem2
        '
        Me.NuevoToolStripMenuItem2.Name = "NuevoToolStripMenuItem2"
        Me.NuevoToolStripMenuItem2.Size = New System.Drawing.Size(177, 22)
        Me.NuevoToolStripMenuItem2.Text = "Simple"
        '
        'ListadoGeneralDeRemisionesToolStripMenuItem
        '
        Me.ListadoGeneralDeRemisionesToolStripMenuItem.Name = "ListadoGeneralDeRemisionesToolStripMenuItem"
        Me.ListadoGeneralDeRemisionesToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.ListadoGeneralDeRemisionesToolStripMenuItem.Text = "Listado General"
        '
        'NotasDeCreditoToolStripMenuItem
        '
        Me.NotasDeCreditoToolStripMenuItem.Name = "NotasDeCreditoToolStripMenuItem"
        Me.NotasDeCreditoToolStripMenuItem.Size = New System.Drawing.Size(224, 22)
        Me.NotasDeCreditoToolStripMenuItem.Text = "Notas de Credito"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(224, 22)
        Me.ToolStripMenuItem3.Text = "Notas de Debito"
        '
        'PedidosToolStripMenuItem
        '
        Me.PedidosToolStripMenuItem.Name = "PedidosToolStripMenuItem"
        Me.PedidosToolStripMenuItem.Size = New System.Drawing.Size(224, 22)
        Me.PedidosToolStripMenuItem.Text = "Pedidos"
        '
        'ListasDePrecioToolStripMenuItem
        '
        Me.ListasDePrecioToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AdministrarToolStripMenuItem6, Me.DescuentosTacticosToolStripMenuItem})
        Me.ListasDePrecioToolStripMenuItem.Name = "ListasDePrecioToolStripMenuItem"
        Me.ListasDePrecioToolStripMenuItem.Size = New System.Drawing.Size(224, 22)
        Me.ListasDePrecioToolStripMenuItem.Text = "Listas de Precio"
        '
        'AdministrarToolStripMenuItem6
        '
        Me.AdministrarToolStripMenuItem6.Name = "AdministrarToolStripMenuItem6"
        Me.AdministrarToolStripMenuItem6.Size = New System.Drawing.Size(208, 22)
        Me.AdministrarToolStripMenuItem6.Text = "Administrar"
        '
        'DescuentosTacticosToolStripMenuItem
        '
        Me.DescuentosTacticosToolStripMenuItem.Name = "DescuentosTacticosToolStripMenuItem"
        Me.DescuentosTacticosToolStripMenuItem.Size = New System.Drawing.Size(208, 22)
        Me.DescuentosTacticosToolStripMenuItem.Text = "Descuentos Tacticos"
        '
        'ComprasToolStripMenuItem
        '
        Me.ComprasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CompraDeProductoToolStripMenuItem, Me.ConsultaDeComprasToolStripMenuItem, Me.ComprasPorProductoToolStripMenuItem, Me.ComprasPorProveedorToolStripMenuItem, Me.CuentasAPagarToolStripMenuItem, Me.ToolStripSeparator9, Me.ToolStripMenuItem4, Me.ToolStripMenuItem5, Me.ToolStripSeparator8, Me.AnticiposToolStripMenuItem, Me.GastosToolStripMenuItem1, Me.AplicarAnticipoAGastosToolStripMenuItem})
        Me.ComprasToolStripMenuItem.Name = "ComprasToolStripMenuItem"
        Me.ComprasToolStripMenuItem.Size = New System.Drawing.Size(68, 20)
        Me.ComprasToolStripMenuItem.Text = "Egresos"
        '
        'CompraDeProductoToolStripMenuItem
        '
        Me.CompraDeProductoToolStripMenuItem.Name = "CompraDeProductoToolStripMenuItem"
        Me.CompraDeProductoToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.CompraDeProductoToolStripMenuItem.Text = "Compra de Productos"
        '
        'ConsultaDeComprasToolStripMenuItem
        '
        Me.ConsultaDeComprasToolStripMenuItem.Name = "ConsultaDeComprasToolStripMenuItem"
        Me.ConsultaDeComprasToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.ConsultaDeComprasToolStripMenuItem.Text = "Listado de Compras"
        '
        'ComprasPorProductoToolStripMenuItem
        '
        Me.ComprasPorProductoToolStripMenuItem.Name = "ComprasPorProductoToolStripMenuItem"
        Me.ComprasPorProductoToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.ComprasPorProductoToolStripMenuItem.Text = "Compras por Producto"
        '
        'ComprasPorProveedorToolStripMenuItem
        '
        Me.ComprasPorProveedorToolStripMenuItem.Name = "ComprasPorProveedorToolStripMenuItem"
        Me.ComprasPorProveedorToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.ComprasPorProveedorToolStripMenuItem.Text = "Compras por Proveedor"
        '
        'CuentasAPagarToolStripMenuItem
        '
        Me.CuentasAPagarToolStripMenuItem.Name = "CuentasAPagarToolStripMenuItem"
        Me.CuentasAPagarToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.CuentasAPagarToolStripMenuItem.Text = "Cuentas a Pagar"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(229, 6)
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(232, 22)
        Me.ToolStripMenuItem4.Text = "Notas de Credito"
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(232, 22)
        Me.ToolStripMenuItem5.Text = "Notas de Debito"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(229, 6)
        '
        'AnticiposToolStripMenuItem
        '
        Me.AnticiposToolStripMenuItem.Name = "AnticiposToolStripMenuItem"
        Me.AnticiposToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.AnticiposToolStripMenuItem.Text = "Anticipos"
        '
        'GastosToolStripMenuItem1
        '
        Me.GastosToolStripMenuItem1.Name = "GastosToolStripMenuItem1"
        Me.GastosToolStripMenuItem1.Size = New System.Drawing.Size(232, 22)
        Me.GastosToolStripMenuItem1.Text = "Gastos"
        '
        'AplicarAnticipoAGastosToolStripMenuItem
        '
        Me.AplicarAnticipoAGastosToolStripMenuItem.Name = "AplicarAnticipoAGastosToolStripMenuItem"
        Me.AplicarAnticipoAGastosToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.AplicarAnticipoAGastosToolStripMenuItem.Text = "Aplicar anticipo a Gastos"
        '
        'StockToolStripMenuItem
        '
        Me.StockToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MovimientosToolStripMenuItem, Me.CargaDeMercaderiasToolStripMenuItem, Me.DevolucionDeLoteToolStripMenuItem, Me.InventarioInicialToolStripMenuItem, Me.ListadoDeMovimientosToolStripMenuItem, Me.ResumenDeExistenciaToolStripMenuItem, Me.ProductosAReponerToolStripMenuItem, Me.MovimientosPorProductToolStripMenuItem, Me.PlanillaParaTomaDeInventarioToolStripMenuItem})
        Me.StockToolStripMenuItem.Image = CType(resources.GetObject("StockToolStripMenuItem.Image"), System.Drawing.Image)
        Me.StockToolStripMenuItem.Name = "StockToolStripMenuItem"
        Me.StockToolStripMenuItem.Size = New System.Drawing.Size(70, 20)
        Me.StockToolStripMenuItem.Text = "Stock"
        '
        'MovimientosToolStripMenuItem
        '
        Me.MovimientosToolStripMenuItem.Image = CType(resources.GetObject("MovimientosToolStripMenuItem.Image"), System.Drawing.Image)
        Me.MovimientosToolStripMenuItem.Name = "MovimientosToolStripMenuItem"
        Me.MovimientosToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.MovimientosToolStripMenuItem.Text = "Movimientos"
        '
        'CargaDeMercaderiasToolStripMenuItem
        '
        Me.CargaDeMercaderiasToolStripMenuItem.Name = "CargaDeMercaderiasToolStripMenuItem"
        Me.CargaDeMercaderiasToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.CargaDeMercaderiasToolStripMenuItem.Text = "Carga de Mercaderias"
        '
        'DevolucionDeLoteToolStripMenuItem
        '
        Me.DevolucionDeLoteToolStripMenuItem.Name = "DevolucionDeLoteToolStripMenuItem"
        Me.DevolucionDeLoteToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.DevolucionDeLoteToolStripMenuItem.Text = "Devolucion de Lote"
        '
        'InventarioInicialToolStripMenuItem
        '
        Me.InventarioInicialToolStripMenuItem.Name = "InventarioInicialToolStripMenuItem"
        Me.InventarioInicialToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.InventarioInicialToolStripMenuItem.Text = "Inventario Inicial"
        '
        'ListadoDeMovimientosToolStripMenuItem
        '
        Me.ListadoDeMovimientosToolStripMenuItem.Name = "ListadoDeMovimientosToolStripMenuItem"
        Me.ListadoDeMovimientosToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ListadoDeMovimientosToolStripMenuItem.Text = "Listado de Movimientos"
        '
        'ResumenDeExistenciaToolStripMenuItem
        '
        Me.ResumenDeExistenciaToolStripMenuItem.Name = "ResumenDeExistenciaToolStripMenuItem"
        Me.ResumenDeExistenciaToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ResumenDeExistenciaToolStripMenuItem.Text = "Resumen de Existencia"
        '
        'ProductosAReponerToolStripMenuItem
        '
        Me.ProductosAReponerToolStripMenuItem.Name = "ProductosAReponerToolStripMenuItem"
        Me.ProductosAReponerToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ProductosAReponerToolStripMenuItem.Text = "Productos a Reponer"
        '
        'MovimientosPorProductToolStripMenuItem
        '
        Me.MovimientosPorProductToolStripMenuItem.Name = "MovimientosPorProductToolStripMenuItem"
        Me.MovimientosPorProductToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.MovimientosPorProductToolStripMenuItem.Text = "Movimientos por Producto"
        '
        'PlanillaParaTomaDeInventarioToolStripMenuItem
        '
        Me.PlanillaParaTomaDeInventarioToolStripMenuItem.Name = "PlanillaParaTomaDeInventarioToolStripMenuItem"
        Me.PlanillaParaTomaDeInventarioToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.PlanillaParaTomaDeInventarioToolStripMenuItem.Text = "Planilla para toma de inventario"
        '
        'TesoreriaToolStripMenuItem
        '
        Me.TesoreriaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PruebaToolStripMenuItem, Me.CobranzasContadoToolStripMenuItem, Me.OrderDePagoToolStripMenuItem, Me.DebitosYCreditosBancariosToolStripMenuItem, Me.ChequesToolStripMenuItem, Me.ConsultaDeEfectivoToolStripMenuItem, Me.ConsultaDePagosConDocumentosToolStripMenuItem, Me.DepositoBancarioToolStripMenuItem, Me.PagoChequeClienteToolStripMenuItem, Me.DescuentoDeChequesToolStripMenuItem, Me.DetalleGastoCreditoGastoToolStripMenuItem, Me.DepositoBancarioRechazadoToolStripMenuItem})
        Me.TesoreriaToolStripMenuItem.Name = "TesoreriaToolStripMenuItem"
        Me.TesoreriaToolStripMenuItem.Size = New System.Drawing.Size(73, 20)
        Me.TesoreriaToolStripMenuItem.Text = "Tesoreria"
        '
        'PruebaToolStripMenuItem
        '
        Me.PruebaToolStripMenuItem.Name = "PruebaToolStripMenuItem"
        Me.PruebaToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.PruebaToolStripMenuItem.Text = "Cobranzas"
        '
        'CobranzasContadoToolStripMenuItem
        '
        Me.CobranzasContadoToolStripMenuItem.Name = "CobranzasContadoToolStripMenuItem"
        Me.CobranzasContadoToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.CobranzasContadoToolStripMenuItem.Text = "Cobranzas por Lote"
        '
        'OrderDePagoToolStripMenuItem
        '
        Me.OrderDePagoToolStripMenuItem.Name = "OrderDePagoToolStripMenuItem"
        Me.OrderDePagoToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.OrderDePagoToolStripMenuItem.Text = "Orden de Pago"
        '
        'DebitosYCreditosBancariosToolStripMenuItem
        '
        Me.DebitosYCreditosBancariosToolStripMenuItem.Name = "DebitosYCreditosBancariosToolStripMenuItem"
        Me.DebitosYCreditosBancariosToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.DebitosYCreditosBancariosToolStripMenuItem.Text = "Debitos y Creditos Bancarios"
        '
        'ChequesToolStripMenuItem
        '
        Me.ChequesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresoToolStripMenuItem, Me.ConsultaDeChequesToolStripMenuItem, Me.DescuentosToolStripMenuItem, Me.ModificarToolStripMenuItem})
        Me.ChequesToolStripMenuItem.Name = "ChequesToolStripMenuItem"
        Me.ChequesToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.ChequesToolStripMenuItem.Text = "Cheques"
        '
        'IngresoToolStripMenuItem
        '
        Me.IngresoToolStripMenuItem.Name = "IngresoToolStripMenuItem"
        Me.IngresoToolStripMenuItem.Size = New System.Drawing.Size(245, 22)
        Me.IngresoToolStripMenuItem.Text = "Ingreso de Cheque Clientes"
        '
        'ConsultaDeChequesToolStripMenuItem
        '
        Me.ConsultaDeChequesToolStripMenuItem.Name = "ConsultaDeChequesToolStripMenuItem"
        Me.ConsultaDeChequesToolStripMenuItem.Size = New System.Drawing.Size(245, 22)
        Me.ConsultaDeChequesToolStripMenuItem.Text = "Consulta de Cheques"
        '
        'DescuentosToolStripMenuItem
        '
        Me.DescuentosToolStripMenuItem.Name = "DescuentosToolStripMenuItem"
        Me.DescuentosToolStripMenuItem.Size = New System.Drawing.Size(245, 22)
        Me.DescuentosToolStripMenuItem.Text = "Descuentos"
        '
        'ModificarToolStripMenuItem
        '
        Me.ModificarToolStripMenuItem.Name = "ModificarToolStripMenuItem"
        Me.ModificarToolStripMenuItem.Size = New System.Drawing.Size(245, 22)
        Me.ModificarToolStripMenuItem.Text = "Modificar"
        '
        'ConsultaDeEfectivoToolStripMenuItem
        '
        Me.ConsultaDeEfectivoToolStripMenuItem.Name = "ConsultaDeEfectivoToolStripMenuItem"
        Me.ConsultaDeEfectivoToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.ConsultaDeEfectivoToolStripMenuItem.Text = "Consulta de Efectivo"
        '
        'ConsultaDePagosConDocumentosToolStripMenuItem
        '
        Me.ConsultaDePagosConDocumentosToolStripMenuItem.Name = "ConsultaDePagosConDocumentosToolStripMenuItem"
        Me.ConsultaDePagosConDocumentosToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.ConsultaDePagosConDocumentosToolStripMenuItem.Text = "Consulta de Pagos con Documentos"
        '
        'DepositoBancarioToolStripMenuItem
        '
        Me.DepositoBancarioToolStripMenuItem.Name = "DepositoBancarioToolStripMenuItem"
        Me.DepositoBancarioToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.DepositoBancarioToolStripMenuItem.Text = "Deposito Bancario"
        '
        'PagoChequeClienteToolStripMenuItem
        '
        Me.PagoChequeClienteToolStripMenuItem.Name = "PagoChequeClienteToolStripMenuItem"
        Me.PagoChequeClienteToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.PagoChequeClienteToolStripMenuItem.Text = "Pago Cheque Cliente"
        '
        'DescuentoDeChequesToolStripMenuItem
        '
        Me.DescuentoDeChequesToolStripMenuItem.Name = "DescuentoDeChequesToolStripMenuItem"
        Me.DescuentoDeChequesToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.DescuentoDeChequesToolStripMenuItem.Text = "Descuento de Cheques"
        '
        'DetalleGastoCreditoGastoToolStripMenuItem
        '
        Me.DetalleGastoCreditoGastoToolStripMenuItem.Name = "DetalleGastoCreditoGastoToolStripMenuItem"
        Me.DetalleGastoCreditoGastoToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.DetalleGastoCreditoGastoToolStripMenuItem.Text = "Detalle Gasto Debito Credito"
        '
        'DepositoBancarioRechazadoToolStripMenuItem
        '
        Me.DepositoBancarioRechazadoToolStripMenuItem.Name = "DepositoBancarioRechazadoToolStripMenuItem"
        Me.DepositoBancarioRechazadoToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.DepositoBancarioRechazadoToolStripMenuItem.Text = "Deposito Bancario Rechazado"
        '
        'ContabilidadToolStripMenuItem
        '
        Me.ContabilidadToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PlanDeCuentasToolStripMenuItem, Me.AsientosContablesToolStripMenuItem})
        Me.ContabilidadToolStripMenuItem.Name = "ContabilidadToolStripMenuItem"
        Me.ContabilidadToolStripMenuItem.Size = New System.Drawing.Size(91, 20)
        Me.ContabilidadToolStripMenuItem.Text = "Contabilidad"
        '
        'PlanDeCuentasToolStripMenuItem
        '
        Me.PlanDeCuentasToolStripMenuItem.Name = "PlanDeCuentasToolStripMenuItem"
        Me.PlanDeCuentasToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.PlanDeCuentasToolStripMenuItem.Text = "Plan de Cuentas"
        '
        'AsientosContablesToolStripMenuItem
        '
        Me.AsientosContablesToolStripMenuItem.Name = "AsientosContablesToolStripMenuItem"
        Me.AsientosContablesToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.AsientosContablesToolStripMenuItem.Text = "Asientos Contables"
        '
        'RRHToolStripMenuItem
        '
        Me.RRHToolStripMenuItem.Name = "RRHToolStripMenuItem"
        Me.RRHToolStripMenuItem.Size = New System.Drawing.Size(47, 20)
        Me.RRHToolStripMenuItem.Text = "RRH"
        '
        'HerramientasToolStripMenuItem
        '
        Me.HerramientasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CuentasFijasToolStripMenuItem, Me.ConfiguracionesToolStripMenuItem, Me.ImportarBDToolStripMenuItem, Me.ImpresionToolStripMenuItem})
        Me.HerramientasToolStripMenuItem.Name = "HerramientasToolStripMenuItem"
        Me.HerramientasToolStripMenuItem.Size = New System.Drawing.Size(97, 20)
        Me.HerramientasToolStripMenuItem.Text = "Herramientas"
        '
        'CuentasFijasToolStripMenuItem
        '
        Me.CuentasFijasToolStripMenuItem.Name = "CuentasFijasToolStripMenuItem"
        Me.CuentasFijasToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.CuentasFijasToolStripMenuItem.Text = "Cuentas Fijas"
        '
        'ConfiguracionesToolStripMenuItem
        '
        Me.ConfiguracionesToolStripMenuItem.Name = "ConfiguracionesToolStripMenuItem"
        Me.ConfiguracionesToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.ConfiguracionesToolStripMenuItem.Text = "Configuraciones"
        '
        'ImportarBDToolStripMenuItem
        '
        Me.ImportarBDToolStripMenuItem.Name = "ImportarBDToolStripMenuItem"
        Me.ImportarBDToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.ImportarBDToolStripMenuItem.Text = "Importar BD"
        '
        'ImpresionToolStripMenuItem
        '
        Me.ImpresionToolStripMenuItem.Name = "ImpresionToolStripMenuItem"
        Me.ImpresionToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.ImpresionToolStripMenuItem.Text = "Impresion"
        '
        'SeguridadToolStripMenuItem
        '
        Me.SeguridadToolStripMenuItem.Name = "SeguridadToolStripMenuItem"
        Me.SeguridadToolStripMenuItem.Size = New System.Drawing.Size(78, 20)
        Me.SeguridadToolStripMenuItem.Text = "Seguridad"
        '
        'StatusStrip
        '
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel8, Me.tsslFecha, Me.ToolStripStatusLabel2, Me.lblSucursal, Me.ToolStripStatusLabel4, Me.lblDeposito, Me.ToolStripStatusLabel, Me.lblTerminal, Me.ToolStripStatusLabel6, Me.lblUsuario, Me.ToolStripSplitButton1})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 431)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(815, 22)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'ToolStripStatusLabel8
        '
        Me.ToolStripStatusLabel8.BorderStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.ToolStripStatusLabel8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel8.Image = CType(resources.GetObject("ToolStripStatusLabel8.Image"), System.Drawing.Image)
        Me.ToolStripStatusLabel8.Name = "ToolStripStatusLabel8"
        Me.ToolStripStatusLabel8.Size = New System.Drawing.Size(62, 17)
        Me.ToolStripStatusLabel8.Text = "Fecha:"
        '
        'tsslFecha
        '
        Me.tsslFecha.Name = "tsslFecha"
        Me.tsslFecha.Size = New System.Drawing.Size(19, 17)
        Me.tsslFecha.Text = "00"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel2.Margin = New System.Windows.Forms.Padding(20, 3, 0, 2)
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(60, 17)
        Me.ToolStripStatusLabel2.Text = "Sucursal:"
        '
        'lblSucursal
        '
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(29, 17)
        Me.lblSucursal.Text = "ASU"
        '
        'ToolStripStatusLabel4
        '
        Me.ToolStripStatusLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel4.Margin = New System.Windows.Forms.Padding(20, 3, 0, 2)
        Me.ToolStripStatusLabel4.Name = "ToolStripStatusLabel4"
        Me.ToolStripStatusLabel4.Size = New System.Drawing.Size(57, 17)
        Me.ToolStripStatusLabel4.Text = "Deposito"
        '
        'lblDeposito
        '
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(35, 17)
        Me.lblDeposito.Text = "DEP1"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel.Margin = New System.Windows.Forms.Padding(20, 3, 0, 2)
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(59, 17)
        Me.ToolStripStatusLabel.Text = "Terminal:"
        '
        'lblTerminal
        '
        Me.lblTerminal.Name = "lblTerminal"
        Me.lblTerminal.Size = New System.Drawing.Size(51, 17)
        Me.lblTerminal.Text = "SERVER"
        '
        'ToolStripStatusLabel6
        '
        Me.ToolStripStatusLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel6.Margin = New System.Windows.Forms.Padding(20, 3, 0, 2)
        Me.ToolStripStatusLabel6.Name = "ToolStripStatusLabel6"
        Me.ToolStripStatusLabel6.Size = New System.Drawing.Size(54, 17)
        Me.ToolStripStatusLabel6.Text = "Usuario:"
        '
        'lblUsuario
        '
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(42, 17)
        Me.lblUsuario.Text = "ADMIN"
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripSplitButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CambiarImagenDeFonoToolStripMenuItem})
        Me.ToolStripSplitButton1.Image = CType(resources.GetObject("ToolStripSplitButton1.Image"), System.Drawing.Image)
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Margin = New System.Windows.Forms.Padding(50, 2, 0, 0)
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(84, 20)
        Me.ToolStripSplitButton1.Text = "Opciones"
        '
        'CambiarImagenDeFonoToolStripMenuItem
        '
        Me.CambiarImagenDeFonoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PredeterminadoToolStripMenuItem})
        Me.CambiarImagenDeFonoToolStripMenuItem.Image = CType(resources.GetObject("CambiarImagenDeFonoToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CambiarImagenDeFonoToolStripMenuItem.Name = "CambiarImagenDeFonoToolStripMenuItem"
        Me.CambiarImagenDeFonoToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.CambiarImagenDeFonoToolStripMenuItem.Text = "Cambiar imagen de fono"
        '
        'PredeterminadoToolStripMenuItem
        '
        Me.PredeterminadoToolStripMenuItem.Name = "PredeterminadoToolStripMenuItem"
        Me.PredeterminadoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.PredeterminadoToolStripMenuItem.Text = "Predeterminado"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = "Todas las imagenes |*.jpeg*.jpg*.bmp*.png*.gif|JPEG |*.jpeg|BMP |*.bmp|PNG |*.png" & _
            "|JPG |*.jpg"
        Me.OpenFileDialog1.Title = "Imagen de fondo"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'AgrupadorToolStripMenuItem
        '
        Me.AgrupadorToolStripMenuItem.Name = "AgrupadorToolStripMenuItem"
        Me.AgrupadorToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.AgrupadorToolStripMenuItem.Text = "Agrupador"
        '
        'frmPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(815, 453)
        Me.Controls.Add(Me.MenuStrip)
        Me.Controls.Add(Me.StatusStrip)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip
        Me.Name = "frmPrincipal"
        Me.Text = "frmPrincipal"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents InicioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambiarUsuarioContraseñaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CerrarSesionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntidadesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComprasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TesoreriaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContabilidadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RRHToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HerramientasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProveedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PersonalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TerminalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Sucursales6ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CotizacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BancosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CuentasBancariasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OtrosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SeguridadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PaisCiudadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FacturacionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NotasDeCreditoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemisionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PedidosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LotesDeComprobantesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListasDePrecioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NuevoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdministrarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultaDeFacturasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NuevoToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NuevoToolStripMenuItem5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RendicionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdministrarToolStripMenuItem6 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MovimientosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DescuentosTacticosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TipoClienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MonedaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ZonasDeVentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PersonasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VendedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CobradoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PromotoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DistribuidoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TipoDeProductosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MarcaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LineaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UnidadesDeMedidaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CamionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ModelosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TiposDeCamionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ToolStripSplitButton1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents CambiarImagenDeFonoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblTerminal As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblSucursal As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblDeposito As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel6 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblUsuario As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsslFecha As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents PredeterminadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel8 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel4 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents EstadoDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CategoriasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents RegistrosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MotivosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PruebaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanDeCuentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AsientosContablesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CuentasFijasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConfiguracionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PuntosDeExpedicionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OrderDePagoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DebitosYCreditosBancariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChequesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DescuentosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultaDeChequesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultaDeEfectivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultaDePagosConDocumentosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PorLoteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoGeneralDeRemisionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoGeneralDeVentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CuentasACobrarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasPorClienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasPorProductosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentaAnualPorClienteImporteCantidadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasBajoElCostoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasYComisionesPorVendedorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasYDescuentosPorVendedorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeRendicionPorLoteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InventarioInicialToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CompraDeProductoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultaDeComprasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComprasPorProductoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComprasPorProveedorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CuentasAPagarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AnticiposToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GastosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AplicarAnticipoAGastosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeMovimientosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResumenDeExistenciaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductosAReponerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MovimientosPorProductToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanillaParaTomaDeInventarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CobranzasContadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TransportistasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChoferesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargaDeMercaderiasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DevolucionDeLoteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ModificarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DepositoBancarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImportarBDToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImpresionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PagoChequeClienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DescuentoDeChequesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DetalleGastoCreditoGastoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DepositoBancarioRechazadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgrupadorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrincipal3
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrincipal3))
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.tiActualizarFechaHora = New System.Windows.Forms.Timer(Me.components)
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.tFechaHora = New System.Windows.Forms.Timer(Me.components)
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel8 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsslFecha = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblSucursal = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel4 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblDeposito = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblTerminal = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel6 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblUsuario = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripSplitButton()
        Me.CambiarImagenDeFonoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PredeterminadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = "Todas las imagenes |*.jpeg*.jpg*.bmp*.png*.gif|JPEG |*.jpeg|BMP |*.bmp|PNG |*.png" & _
            "|JPG |*.jpg"
        Me.OpenFileDialog1.Title = "Imagen de fondo"
        '
        'tiActualizarFechaHora
        '
        Me.tiActualizarFechaHora.Enabled = True
        Me.tiActualizarFechaHora.Interval = 60000
        '
        'tFechaHora
        '
        Me.tFechaHora.Enabled = True
        Me.tFechaHora.Interval = 1000
        '
        'StatusStrip
        '
        Me.StatusStrip.BackColor = System.Drawing.SystemColors.Control
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel8, Me.tsslFecha, Me.ToolStripStatusLabel2, Me.lblSucursal, Me.ToolStripStatusLabel4, Me.lblDeposito, Me.ToolStripStatusLabel, Me.lblTerminal, Me.ToolStripStatusLabel6, Me.lblUsuario, Me.ToolStripSplitButton1})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 488)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(908, 32)
        Me.StatusStrip.TabIndex = 13
        Me.StatusStrip.Text = "StatusStrip"
        '
        'ToolStripStatusLabel8
        '
        Me.ToolStripStatusLabel8.BorderStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.ToolStripStatusLabel8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel8.ForeColor = System.Drawing.Color.Black
        Me.ToolStripStatusLabel8.Image = CType(resources.GetObject("ToolStripStatusLabel8.Image"), System.Drawing.Image)
        Me.ToolStripStatusLabel8.Name = "ToolStripStatusLabel8"
        Me.ToolStripStatusLabel8.Size = New System.Drawing.Size(56, 27)
        Me.ToolStripStatusLabel8.Text = "Fecha:"
        '
        'tsslFecha
        '
        Me.tsslFecha.ForeColor = System.Drawing.Color.Black
        Me.tsslFecha.Name = "tsslFecha"
        Me.tsslFecha.Size = New System.Drawing.Size(19, 27)
        Me.tsslFecha.Text = "00"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel2.ForeColor = System.Drawing.Color.Black
        Me.ToolStripStatusLabel2.Margin = New System.Windows.Forms.Padding(20, 3, 0, 2)
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(51, 27)
        Me.ToolStripStatusLabel2.Text = "Sucursal:"
        '
        'lblSucursal
        '
        Me.lblSucursal.ForeColor = System.Drawing.Color.Black
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(29, 27)
        Me.lblSucursal.Text = "ASU"
        '
        'ToolStripStatusLabel4
        '
        Me.ToolStripStatusLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel4.ForeColor = System.Drawing.Color.Black
        Me.ToolStripStatusLabel4.Margin = New System.Windows.Forms.Padding(20, 3, 0, 2)
        Me.ToolStripStatusLabel4.Name = "ToolStripStatusLabel4"
        Me.ToolStripStatusLabel4.Size = New System.Drawing.Size(49, 27)
        Me.ToolStripStatusLabel4.Text = "Deposito"
        '
        'lblDeposito
        '
        Me.lblDeposito.ForeColor = System.Drawing.Color.Black
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(35, 27)
        Me.lblDeposito.Text = "DEP1"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel.ForeColor = System.Drawing.Color.Black
        Me.ToolStripStatusLabel.Margin = New System.Windows.Forms.Padding(20, 3, 0, 2)
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(50, 27)
        Me.ToolStripStatusLabel.Text = "Terminal:"
        '
        'lblTerminal
        '
        Me.lblTerminal.ForeColor = System.Drawing.Color.Black
        Me.lblTerminal.Name = "lblTerminal"
        Me.lblTerminal.Size = New System.Drawing.Size(51, 27)
        Me.lblTerminal.Text = "SERVER"
        '
        'ToolStripStatusLabel6
        '
        Me.ToolStripStatusLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel6.ForeColor = System.Drawing.Color.Black
        Me.ToolStripStatusLabel6.Margin = New System.Windows.Forms.Padding(20, 3, 0, 2)
        Me.ToolStripStatusLabel6.Name = "ToolStripStatusLabel6"
        Me.ToolStripStatusLabel6.Size = New System.Drawing.Size(46, 27)
        Me.ToolStripStatusLabel6.Text = "Usuario:"
        '
        'lblUsuario
        '
        Me.lblUsuario.ForeColor = System.Drawing.Color.Black
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(42, 27)
        Me.lblUsuario.Text = "ADMIN"
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CambiarImagenDeFonoToolStripMenuItem})
        Me.ToolStripSplitButton1.ForeColor = System.Drawing.Color.Black
        Me.ToolStripSplitButton1.Image = CType(resources.GetObject("ToolStripSplitButton1.Image"), System.Drawing.Image)
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Margin = New System.Windows.Forms.Padding(6)
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(84, 20)
        Me.ToolStripSplitButton1.Text = "Opciones"
        Me.ToolStripSplitButton1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CambiarImagenDeFonoToolStripMenuItem
        '
        Me.CambiarImagenDeFonoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PredeterminadoToolStripMenuItem})
        Me.CambiarImagenDeFonoToolStripMenuItem.Image = CType(resources.GetObject("CambiarImagenDeFonoToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CambiarImagenDeFonoToolStripMenuItem.Name = "CambiarImagenDeFonoToolStripMenuItem"
        Me.CambiarImagenDeFonoToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.CambiarImagenDeFonoToolStripMenuItem.Text = "Cambiar imagen de fondo"
        '
        'PredeterminadoToolStripMenuItem
        '
        Me.PredeterminadoToolStripMenuItem.Name = "PredeterminadoToolStripMenuItem"
        Me.PredeterminadoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.PredeterminadoToolStripMenuItem.Text = "Predeterminado"
        '
        'frmPrincipal3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(908, 520)
        Me.Controls.Add(Me.StatusStrip)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "frmPrincipal3"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmPrincipal3"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents tiActualizarFechaHora As System.Windows.Forms.Timer
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents tFechaHora As System.Windows.Forms.Timer
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel8 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsslFecha As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblSucursal As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel4 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblDeposito As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblTerminal As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel6 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblUsuario As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripSplitButton1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents CambiarImagenDeFonoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PredeterminadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPrincipal2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrincipal2))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem29 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem38 = New System.Windows.Forms.ToolStripSeparator()
        Me.CambiarDepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambiarUsuarioContraseñaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator41 = New System.Windows.Forms.ToolStripSeparator()
        Me.CerrarSesionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ENTIDADESToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreditoDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PersonalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PersonasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VendedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CobradoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PromotoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DistribuidoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AbogadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargarToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AsignacionDeLimiteDeDescuentoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SolicitudesDeExcepcionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosConPrecioModificableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformesToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExcepcionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaDePrecioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SolicitudesDeDescuentoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OtrosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PaisCiudadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ZonasDeVentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadoDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsmiClasificacion1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiClasificacion2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiClasificacion3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiClasificacion4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiClasificacion5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiClasificacion6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CategoriasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnidadesDeMedidaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ZonaEnDepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.TransportistasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChoferesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CamionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CategoriaDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator54 = New System.Windows.Forms.ToolStripSeparator()
        Me.VencimientoDeProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.TerminalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Sucursales6ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoDeDepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MonedaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CotizacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BancosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CuentasBancariasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaldoDiarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChequeraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.RegistrosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OperacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MotivosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MotivosDeAnulacionDeVentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem9 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MotivosDeAnulacionCobranzasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SubMotivosNotaDeCreditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem11 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MotivoDeRemisionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator38 = New System.Windows.Forms.ToolStripSeparator()
        Me.UsuariosYAutorizacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosParaAutorizarAnulacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PorcentajeDeExcepcionToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PorcentajeDeExcesoDeLineaDeCreditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AutorizadoresDeNotaDeCreditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosPermitidosCargaDeRecepcionDeDocumentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PuntosDeExpedicionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GruposYUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FormaDePagoFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AsignarCuentaContableAFondoFijoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AsignarCuentaContableARRHHToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosUnidadDeNegocioYCCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MetasDeVentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OperacionesPermitidasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AsignarCuentaContableIntegridadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TarjetaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcesadoraDeTarjetaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MotivoDevolucionNCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LlamadasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoDeLlamadaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResultadoDeLlamadaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AreasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RutasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambiarVendedorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InactivacionDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VariosProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CamionesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChoferesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambiarRepositorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturacionDePedidosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaDeComprobanteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AnulacionDeFacturaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SolicitudesDeAnulacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeComprobantesEmitidosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeComprobantesAnuladosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator15 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasPorClienteToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasAnualesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem8 = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasDeProductosInferiorAlCostoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComisionesPorVendedorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DescuentosPorVendedorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GráficosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GráficoVentaMesTotalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GráficoRankingClientesVentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GráficoProductosMásRentablesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GráficoProductosMásVendidoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasAExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeExcepcionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeDescuentosConcedidosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegistroDePreciosPorClienteProductoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.PedidosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaToolStripMenuItem8 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImportarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImprimirToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AprobacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AprobarExcesoDeLineaDeCreditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator19 = New System.Windows.Forms.ToolStripSeparator()
        Me.InformeToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformeAnticipacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DescuentosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaToolStripMenuItem13 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActividadesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TransferirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator44 = New System.Windows.Forms.ToolStripSeparator()
        Me.InformeToolStripMenuItem18 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator18 = New System.Windows.Forms.ToolStripSeparator()
        Me.NotaDeCreditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PedidoNotaDeCreditoMovilToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PedidosDeNotaDeCreditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AprobarPedidosDeNotasDeCredutoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcesarPedidoDeNotasDeCreditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator49 = New System.Windows.Forms.ToolStripSeparator()
        Me.CargaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.NCRDocumentoInternoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AplicarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TransferenciaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.InformeToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeNotasDeCreditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem33 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeAplicacionPendienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeNCConVentasAplicadasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator53 = New System.Windows.Forms.ToolStripSeparator()
        Me.ListadoDePedidoDeNotasDeCreditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PanelDePedidosDeDevolucionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotaDeDebitoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AplicarToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem30 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem32 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DevolucionesPorProductoClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.LoteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DevolucionDeLoteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RendicionToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.InformerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenDeLotesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HojaDeRutaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanillaDeCargaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetalleDeDevolucionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComprobantesSinLotesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KilosTransportadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformeToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TeleventasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformeTeleventaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanDeLlamadasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TransporteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaDeCamionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaDeCamionesPedidoDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformeToolStripMenuItem6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AbastecimientoPendienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PedClientesPendientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaParaAbastecimientoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformesGerencialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TotalGeneralToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReenvioComprobantesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AcuerdoDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaDeAcuerdosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeAcuerdosDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComprasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntradaPorComprasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem19 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoGeneralDeComprobantesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator13 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem13 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem14 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator14 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem15 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem16 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem17 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator16 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem18 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator24 = New System.Windows.Forms.ToolStripSeparator()
        Me.ActualizarTimbradoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MovimientosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaToolStripMenuItem6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DescargaDeStockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsumoCombustibleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsumoMateriaPrimaBalanceadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DescagaDeComprasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsumoInformaticaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DevolucionesDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator17 = New System.Windows.Forms.ToolStripSeparator()
        Me.InformeToolStripMenuItem10 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeProductoPorOperacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeMovimientosDeCombustiblesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeDevolucionesSinNCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaDeMercaderiasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaToolStripMenuItem7 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator30 = New System.Windows.Forms.ToolStripSeparator()
        Me.InformeToolStripMenuItem11 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ControlDeStockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EquiposDeConteoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VeremosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TomaDeInventarioFisicoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComprarativoEntreEquiposToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComprarativoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AjusteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator32 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem28 = New System.Windows.Forms.ToolStripMenuItem()
        Me.InventarioInicialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem26 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator31 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem27 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator29 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem36 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator47 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem23 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanillaParaTomaDeInventarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MovimientosPorProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosAReponerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenDeExistenciaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExistenciaValorizadaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KardexResumenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KardexDetalladoPorProductoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComparativoCostoKardexToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrigoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AcuerdoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TicketDeBasculaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AsignarAcuerdoATicketToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MacheoFacturaTicketToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator28 = New System.Windows.Forms.ToolStripSeparator()
        Me.InformesToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AcuerdoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TicketDeBasculaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MacheoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TicketDeBasculaSinCostoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TicketDeBasculaSinMachearAFechaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExtractoDePagosPorAcuerdoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OrdenDePedidoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PedidoParaFábricaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformeToolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CostoDeProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AsignarCostoAProductoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ControlDeCostosDeOperacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AjusteDeExistenciaKardexToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DistribucionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrecargaMovimientoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaDescargaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturaDistribucionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AprobadorPrecargaMovimientoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuarioAprobadorPrecargaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ABMSubMotivoMovimientoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TesoreriaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CobranzaCreditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaToolStripMenuItem9 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AprobadorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CobranzaContadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AplicacionesDeAnticipoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformeToolStripMenuItem12 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanillaDeCobranzasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator46 = New System.Windows.Forms.ToolStripSeparator()
        Me.ResumenDeCobranzasPorFacturasContadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformeTicketDeTarjetasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformeAcreditacionesBancariaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FormaDePagoCobranzaDepositoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator23 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DefinirFondoFijoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator43 = New System.Windows.Forms.ToolStripSeparator()
        Me.ValesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReposicionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarComprobanteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AplicarValeAGastoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator22 = New System.Windows.Forms.ToolStripSeparator()
        Me.EmitirListadoAReponerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RendicionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator27 = New System.Windows.Forms.ToolStripSeparator()
        Me.ValesPendientesDeRendicionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator42 = New System.Windows.Forms.ToolStripSeparator()
        Me.ChequesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaDeChequesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RechazarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EfectivizacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PagoChequeClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DescuentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VencimientoDeChequeDescontadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator20 = New System.Windows.Forms.ToolStripSeparator()
        Me.InformeToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeChequesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeCanjesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeDescuentosDeChequesDiferidosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeChequesRechazadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChequesDiferidosDepositadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeEstadoDeChequesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeChequesRechazadosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeChequesDescontadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EfectivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargarToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EfectivisarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TarjetasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TiposDeTarjetasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DepositosBancariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DebitosYCreditosBancariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConciliacionBancariaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CanjeDeRecaudacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator25 = New System.Windows.Forms.ToolStripSeparator()
        Me.CajaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem20 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaToolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EgresosARendirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AnticipoAProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator50 = New System.Windows.Forms.ToolStripSeparator()
        Me.PrepararPagosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RealizarPAgosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator51 = New System.Windows.Forms.ToolStripSeparator()
        Me.EntregaDeChequesOPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VencimientoDeChequesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator52 = New System.Windows.Forms.ToolStripSeparator()
        Me.RetencionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SolicitudesDeModificacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImprimirToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImprimirOrdenDePagoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AplicacionAnticipoAProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GastosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdministrarToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModifcarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GenerarCuotasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem10 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformePrestamoBancarioToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator26 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem21 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CuentasACobrarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InventarioDeDocumentosPendientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BusquedaDeClientesConFacturasAtrasadasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DistribucionDeSaldosDeDocumentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenDeMovimientosYSaldosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaldosAFechaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.InventarioDeDocumentosPendientesPorClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CuentasAPagarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem22 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem24 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentosPagadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem25 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaldosAFechaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InventarioDeDocumentosPendientesPorPlazoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BancoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExtractoDeMovimientosToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaldosBancariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaldoDiarioDeBancoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator35 = New System.Windows.Forms.ToolStripSeparator()
        Me.ResumenDiarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator21 = New System.Windows.Forms.ToolStripSeparator()
        Me.ListadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem34 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeDepositosBancariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeDebitosYCreditosBancariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeOrdenDePagoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeGastosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeRetencionesRecibidasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LIstadoDeChequesEmitidosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeRetencionesEmitidasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeRecibosGeneradosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeGastosAcuerdoComercialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.AsociarRetencionesDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformeFinancieroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresoTotalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ControlTesoreriaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContabilidadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CuentasFujasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AsientoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanDeCuentasContablesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdministrarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem31 = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnidadDeNegociosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CentroDeCostosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator36 = New System.Windows.Forms.ToolStripSeparator()
        Me.ComprobantesParaLibroVentaYCompraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConciliarAsientosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem35 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CierreDiarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CierreMensualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CierreEjercicioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcesosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambioGlobalDeCuentaContableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegenerarAsientosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RenumeracionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegenerarSaldosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CierreYReaperturaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KardexToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator40 = New System.Windows.Forms.ToolStripSeparator()
        Me.HechaukaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem40 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem41 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator39 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem42 = New System.Windows.Forms.ToolStripMenuItem()
        Me.LibroDiarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem43 = New System.Windows.Forms.ToolStripMenuItem()
        Me.LibroMayorDetalladoPorProductoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator45 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem44 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentosSinAsientosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator48 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem45 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem46 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EvolucionDeSaldosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BalanceRG49ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformesUnidadNegocioCCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LibroMayorToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenDeSaldosUnidadDeNegociosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ControlDeIntegridadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExistenciasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ControlDeUnidadNegocioYDepartamentoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarGastosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarFondoFijoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarCompraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarGastosRRHHToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator55 = New System.Windows.Forms.ToolStripSeparator()
        Me.ModificarModuloStockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Resolución49ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdministrarToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AsignacionDeCuentasRG49ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RRHHToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComprobantesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DepartamentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SeccionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImportarAsientoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalidadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MuestraParaPruebaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AprobarPedidosDeNotaDeCreditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformesToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeMuestrasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDePedidosDeDevolucionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfiguracionDeMuestraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExtractoDeMovimientosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeClientesQueNoCompranToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CumpleañosYAniversariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProveedoresToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExtractoDeMovimientosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExtractoDeMovimientosTodosLosProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExtractoDeMovimientoDeDocumentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RankingDeProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductoiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiseñadorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnileverToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ETLToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DMSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DMSMinutasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AuditoriaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExcepcionesDePreciosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegistroDeAperturasDeFechaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HerramientasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfiguracionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConexionAServidorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem39 = New System.Windows.Forms.ToolStripSeparator()
        Me.FechaDeFacturacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaSQLToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfiguracionDeImpresionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FormatoDeImpresiónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImportarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClasesWindowsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PedidoMovilToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FechaContabilidadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FechaAcuerdosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SeguridadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PerfilesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccesosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccesosSegúnPerfilToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LogToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReconfigurarMenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AyudaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComprobarSiExistenActualizacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfigurarActualizacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem37 = New System.Windows.Forms.ToolStripSeparator()
        Me.AcercaDeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CostosDeProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AsientosContablesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator34 = New System.Windows.Forms.ToolStripSeparator()
        Me.LibroIVAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LibroMayorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator37 = New System.Windows.Forms.ToolStripSeparator()
        Me.ComprobantesNoEmitidosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator33 = New System.Windows.Forms.ToolStripSeparator()
        Me.BalanceGeneralToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BalandeDeComprobacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.tFechaHora = New System.Windows.Forms.Timer(Me.components)
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.tiActualizarFechaHora = New System.Windows.Forms.Timer(Me.components)
        Me.BottomToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.miniToolStrip = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel8 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsslFecha = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblSucursal = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel4 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblDeposito = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblTerminal = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel6 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblUsuario = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripSplitButton()
        Me.AparienciaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PielesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambiarImagenDeFonoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PredeterminadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TopToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.RightToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.LeftToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.ContentPanel = New System.Windows.Forms.ToolStripContentPanel()
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblPerfil = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblBaseDeDatos = New System.Windows.Forms.ToolStripStatusLabel()
        Me.PagarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DevToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.StatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.MenuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.MenuStrip1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.GripMargin = New System.Windows.Forms.Padding(2)
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem29, Me.ENTIDADESToolStripMenuItem, Me.VentasToolStripMenuItem, Me.ComprasToolStripMenuItem, Me.StockToolStripMenuItem, Me.DistribucionToolStripMenuItem, Me.TesoreriaToolStripMenuItem, Me.ContabilidadToolStripMenuItem, Me.RRHHToolStripMenuItem, Me.CalidadToolStripMenuItem, Me.InformesToolStripMenuItem1, Me.HerramientasToolStripMenuItem, Me.SeguridadToolStripMenuItem, Me.AyudaToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.MenuStrip1.Size = New System.Drawing.Size(1449, 28)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem29
        '
        Me.ToolStripMenuItem29.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ActualizarDatosToolStripMenuItem, Me.ToolStripMenuItem38, Me.CambiarDepositoToolStripMenuItem, Me.CambiarUsuarioContraseñaToolStripMenuItem, Me.ToolStripSeparator41, Me.CerrarSesionToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.ToolStripMenuItem29.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripMenuItem29.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ToolStripMenuItem29.Name = "ToolStripMenuItem29"
        Me.ToolStripMenuItem29.Size = New System.Drawing.Size(63, 24)
        Me.ToolStripMenuItem29.Text = "Inicio"
        '
        'ActualizarDatosToolStripMenuItem
        '
        Me.ActualizarDatosToolStripMenuItem.Name = "ActualizarDatosToolStripMenuItem"
        Me.ActualizarDatosToolStripMenuItem.Size = New System.Drawing.Size(243, 26)
        Me.ActualizarDatosToolStripMenuItem.Tag = "frmDescargar"
        Me.ActualizarDatosToolStripMenuItem.Text = "Actualizar Datos"
        '
        'ToolStripMenuItem38
        '
        Me.ToolStripMenuItem38.Name = "ToolStripMenuItem38"
        Me.ToolStripMenuItem38.Size = New System.Drawing.Size(240, 6)
        '
        'CambiarDepositoToolStripMenuItem
        '
        Me.CambiarDepositoToolStripMenuItem.Name = "CambiarDepositoToolStripMenuItem"
        Me.CambiarDepositoToolStripMenuItem.Size = New System.Drawing.Size(243, 26)
        Me.CambiarDepositoToolStripMenuItem.Tag = "frmCambiarDepositoTerminal"
        Me.CambiarDepositoToolStripMenuItem.Text = "Cambiar Deposito"
        '
        'CambiarUsuarioContraseñaToolStripMenuItem
        '
        Me.CambiarUsuarioContraseñaToolStripMenuItem.Name = "CambiarUsuarioContraseñaToolStripMenuItem"
        Me.CambiarUsuarioContraseñaToolStripMenuItem.Size = New System.Drawing.Size(243, 26)
        Me.CambiarUsuarioContraseñaToolStripMenuItem.Tag = "frmCambiarUsuarioContraseña"
        Me.CambiarUsuarioContraseñaToolStripMenuItem.Text = "Cambiar Contraseña"
        '
        'ToolStripSeparator41
        '
        Me.ToolStripSeparator41.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ToolStripSeparator41.Name = "ToolStripSeparator41"
        Me.ToolStripSeparator41.Size = New System.Drawing.Size(240, 6)
        '
        'CerrarSesionToolStripMenuItem
        '
        Me.CerrarSesionToolStripMenuItem.Name = "CerrarSesionToolStripMenuItem"
        Me.CerrarSesionToolStripMenuItem.Size = New System.Drawing.Size(243, 26)
        Me.CerrarSesionToolStripMenuItem.Text = "Cerrar Sesión"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(243, 26)
        Me.SalirToolStripMenuItem.Text = "Salir"
        '
        'ENTIDADESToolStripMenuItem
        '
        Me.ENTIDADESToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientesToolStripMenuItem, Me.CreditoDeClientesToolStripMenuItem, Me.ProveedoresToolStripMenuItem, Me.ProductosToolStripMenuItem, Me.PersonalesToolStripMenuItem, Me.PersonasToolStripMenuItem, Me.ToolStripSeparator3, Me.ToolStripMenuItem6, Me.OtrosToolStripMenuItem, Me.ToolStripSeparator4, Me.TerminalesToolStripMenuItem, Me.Sucursales6ToolStripMenuItem, Me.TipoDeDepositoToolStripMenuItem, Me.ToolStripSeparator1, Me.MonedaToolStripMenuItem, Me.CotizacionesToolStripMenuItem, Me.BancosToolStripMenuItem, Me.CuentasBancariasToolStripMenuItem, Me.ChequeraToolStripMenuItem, Me.ToolStripSeparator6, Me.RegistrosToolStripMenuItem, Me.LlamadasToolStripMenuItem, Me.AreasToolStripMenuItem, Me.RutasToolStripMenuItem, Me.CambiarVendedorToolStripMenuItem, Me.InactivacionDeClientesToolStripMenuItem, Me.VariosProveedoresToolStripMenuItem, Me.CambiarRepositorToolStripMenuItem})
        Me.ENTIDADESToolStripMenuItem.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ENTIDADESToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ENTIDADESToolStripMenuItem.Name = "ENTIDADESToolStripMenuItem"
        Me.ENTIDADESToolStripMenuItem.Size = New System.Drawing.Size(95, 24)
        Me.ENTIDADESToolStripMenuItem.Text = "Entidades"
        '
        'ClientesToolStripMenuItem
        '
        Me.ClientesToolStripMenuItem.Name = "ClientesToolStripMenuItem"
        Me.ClientesToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.ClientesToolStripMenuItem.Tag = "frmCliente"
        Me.ClientesToolStripMenuItem.Text = "Clientes"
        '
        'CreditoDeClientesToolStripMenuItem
        '
        Me.CreditoDeClientesToolStripMenuItem.Name = "CreditoDeClientesToolStripMenuItem"
        Me.CreditoDeClientesToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.CreditoDeClientesToolStripMenuItem.Text = "Credito de Clientes"
        '
        'ProveedoresToolStripMenuItem
        '
        Me.ProveedoresToolStripMenuItem.Name = "ProveedoresToolStripMenuItem"
        Me.ProveedoresToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.ProveedoresToolStripMenuItem.Tag = "frmProveedor"
        Me.ProveedoresToolStripMenuItem.Text = "Proveedores"
        '
        'ProductosToolStripMenuItem
        '
        Me.ProductosToolStripMenuItem.Name = "ProductosToolStripMenuItem"
        Me.ProductosToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.ProductosToolStripMenuItem.Tag = "frmProducto"
        Me.ProductosToolStripMenuItem.Text = "Productos"
        '
        'PersonalesToolStripMenuItem
        '
        Me.PersonalesToolStripMenuItem.ForeColor = System.Drawing.SystemColors.GrayText
        Me.PersonalesToolStripMenuItem.Name = "PersonalesToolStripMenuItem"
        Me.PersonalesToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.PersonalesToolStripMenuItem.Tag = "frmPersonal"
        Me.PersonalesToolStripMenuItem.Text = "Personales"
        '
        'PersonasToolStripMenuItem
        '
        Me.PersonasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VendedoresToolStripMenuItem, Me.CobradoresToolStripMenuItem, Me.PromotoresToolStripMenuItem, Me.DistribuidoresToolStripMenuItem, Me.AbogadoToolStripMenuItem})
        Me.PersonasToolStripMenuItem.Name = "PersonasToolStripMenuItem"
        Me.PersonasToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.PersonasToolStripMenuItem.Text = "Personas"
        '
        'VendedoresToolStripMenuItem
        '
        Me.VendedoresToolStripMenuItem.Name = "VendedoresToolStripMenuItem"
        Me.VendedoresToolStripMenuItem.Size = New System.Drawing.Size(193, 26)
        Me.VendedoresToolStripMenuItem.Tag = "frmVendedor"
        Me.VendedoresToolStripMenuItem.Text = "Vendedores"
        '
        'CobradoresToolStripMenuItem
        '
        Me.CobradoresToolStripMenuItem.Name = "CobradoresToolStripMenuItem"
        Me.CobradoresToolStripMenuItem.Size = New System.Drawing.Size(193, 26)
        Me.CobradoresToolStripMenuItem.Tag = "frmCobrador"
        Me.CobradoresToolStripMenuItem.Text = "Cobradores"
        '
        'PromotoresToolStripMenuItem
        '
        Me.PromotoresToolStripMenuItem.Name = "PromotoresToolStripMenuItem"
        Me.PromotoresToolStripMenuItem.Size = New System.Drawing.Size(193, 26)
        Me.PromotoresToolStripMenuItem.Tag = "frmPromotor"
        Me.PromotoresToolStripMenuItem.Text = "Promotores"
        '
        'DistribuidoresToolStripMenuItem
        '
        Me.DistribuidoresToolStripMenuItem.Name = "DistribuidoresToolStripMenuItem"
        Me.DistribuidoresToolStripMenuItem.Size = New System.Drawing.Size(193, 26)
        Me.DistribuidoresToolStripMenuItem.Tag = "frmDistribuidor"
        Me.DistribuidoresToolStripMenuItem.Text = "Distribuidores"
        '
        'AbogadoToolStripMenuItem
        '
        Me.AbogadoToolStripMenuItem.Name = "AbogadoToolStripMenuItem"
        Me.AbogadoToolStripMenuItem.Size = New System.Drawing.Size(193, 26)
        Me.AbogadoToolStripMenuItem.Tag = "frmAbogados"
        Me.AbogadoToolStripMenuItem.Text = "Abogados"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.ToolStripSeparator3.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(262, 6)
        '
        'ToolStripMenuItem6
        '
        Me.ToolStripMenuItem6.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargarToolStripMenuItem1, Me.AsignacionDeLimiteDeDescuentoToolStripMenuItem, Me.SolicitudesDeExcepcionToolStripMenuItem, Me.ProductosConPrecioModificableToolStripMenuItem, Me.InformesToolStripMenuItem3, Me.SolicitudesDeDescuentoToolStripMenuItem})
        Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
        Me.ToolStripMenuItem6.Size = New System.Drawing.Size(265, 26)
        Me.ToolStripMenuItem6.Text = "Lista de Precio"
        '
        'CargarToolStripMenuItem1
        '
        Me.CargarToolStripMenuItem1.Name = "CargarToolStripMenuItem1"
        Me.CargarToolStripMenuItem1.Size = New System.Drawing.Size(350, 26)
        Me.CargarToolStripMenuItem1.Tag = "frmListadoPrecios"
        Me.CargarToolStripMenuItem1.Text = "Cargar"
        '
        'AsignacionDeLimiteDeDescuentoToolStripMenuItem
        '
        Me.AsignacionDeLimiteDeDescuentoToolStripMenuItem.Name = "AsignacionDeLimiteDeDescuentoToolStripMenuItem"
        Me.AsignacionDeLimiteDeDescuentoToolStripMenuItem.Size = New System.Drawing.Size(350, 26)
        Me.AsignacionDeLimiteDeDescuentoToolStripMenuItem.Tag = "frmAsignarLimiteDescuento"
        Me.AsignacionDeLimiteDeDescuentoToolStripMenuItem.Text = "Asignacion de Limite de Descuento"
        '
        'SolicitudesDeExcepcionToolStripMenuItem
        '
        Me.SolicitudesDeExcepcionToolStripMenuItem.Name = "SolicitudesDeExcepcionToolStripMenuItem"
        Me.SolicitudesDeExcepcionToolStripMenuItem.Size = New System.Drawing.Size(350, 26)
        Me.SolicitudesDeExcepcionToolStripMenuItem.Tag = "frmSolicitudExcepcion"
        Me.SolicitudesDeExcepcionToolStripMenuItem.Text = "Solicitudes de Descuento"
        '
        'ProductosConPrecioModificableToolStripMenuItem
        '
        Me.ProductosConPrecioModificableToolStripMenuItem.Name = "ProductosConPrecioModificableToolStripMenuItem"
        Me.ProductosConPrecioModificableToolStripMenuItem.Size = New System.Drawing.Size(350, 26)
        Me.ProductosConPrecioModificableToolStripMenuItem.Text = "Productos con Precio Modificable"
        '
        'InformesToolStripMenuItem3
        '
        Me.InformesToolStripMenuItem3.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExcepcionesToolStripMenuItem, Me.ListaDePrecioToolStripMenuItem})
        Me.InformesToolStripMenuItem3.Name = "InformesToolStripMenuItem3"
        Me.InformesToolStripMenuItem3.Size = New System.Drawing.Size(350, 26)
        Me.InformesToolStripMenuItem3.Text = "Informes"
        '
        'ExcepcionesToolStripMenuItem
        '
        Me.ExcepcionesToolStripMenuItem.Name = "ExcepcionesToolStripMenuItem"
        Me.ExcepcionesToolStripMenuItem.Size = New System.Drawing.Size(202, 26)
        Me.ExcepcionesToolStripMenuItem.Tag = "frmListaPrecioExcepciones"
        Me.ExcepcionesToolStripMenuItem.Text = "Excepciones"
        '
        'ListaDePrecioToolStripMenuItem
        '
        Me.ListaDePrecioToolStripMenuItem.Name = "ListaDePrecioToolStripMenuItem"
        Me.ListaDePrecioToolStripMenuItem.Size = New System.Drawing.Size(202, 26)
        Me.ListaDePrecioToolStripMenuItem.Tag = "frmListadoPrecioProducto"
        Me.ListaDePrecioToolStripMenuItem.Text = "Lista de Precio"
        '
        'SolicitudesDeDescuentoToolStripMenuItem
        '
        Me.SolicitudesDeDescuentoToolStripMenuItem.Name = "SolicitudesDeDescuentoToolStripMenuItem"
        Me.SolicitudesDeDescuentoToolStripMenuItem.Size = New System.Drawing.Size(350, 26)
        Me.SolicitudesDeDescuentoToolStripMenuItem.Tag = "frmSolicitudProductoPrecio"
        Me.SolicitudesDeDescuentoToolStripMenuItem.Text = "Solicitudes de Descuento "
        '
        'OtrosToolStripMenuItem
        '
        Me.OtrosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PaisCiudadToolStripMenuItem, Me.TipoClienteToolStripMenuItem, Me.ZonasDeVentaToolStripMenuItem, Me.EstadoDeClientesToolStripMenuItem, Me.ToolStripSeparator2, Me.tsmiClasificacion1, Me.tsmiClasificacion2, Me.tsmiClasificacion3, Me.tsmiClasificacion4, Me.tsmiClasificacion5, Me.tsmiClasificacion6, Me.CategoriasToolStripMenuItem, Me.UnidadesDeMedidaToolStripMenuItem, Me.ZonaEnDepositoToolStripMenuItem, Me.ToolStripSeparator5, Me.TransportistasToolStripMenuItem, Me.ChoferesToolStripMenuItem, Me.CamionesToolStripMenuItem, Me.CategoriaDeClientesToolStripMenuItem, Me.ToolStripSeparator54, Me.VencimientoDeProductosToolStripMenuItem})
        Me.OtrosToolStripMenuItem.Name = "OtrosToolStripMenuItem"
        Me.OtrosToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.OtrosToolStripMenuItem.Text = "Varios"
        '
        'PaisCiudadToolStripMenuItem
        '
        Me.PaisCiudadToolStripMenuItem.Name = "PaisCiudadToolStripMenuItem"
        Me.PaisCiudadToolStripMenuItem.Size = New System.Drawing.Size(283, 26)
        Me.PaisCiudadToolStripMenuItem.Tag = "frmPaisDepartamentoCiudadBarrios"
        Me.PaisCiudadToolStripMenuItem.Text = "Localidades"
        '
        'TipoClienteToolStripMenuItem
        '
        Me.TipoClienteToolStripMenuItem.Name = "TipoClienteToolStripMenuItem"
        Me.TipoClienteToolStripMenuItem.Size = New System.Drawing.Size(283, 26)
        Me.TipoClienteToolStripMenuItem.Tag = "frmTipoCliente"
        Me.TipoClienteToolStripMenuItem.Text = "Tipo Cliente"
        '
        'ZonasDeVentaToolStripMenuItem
        '
        Me.ZonasDeVentaToolStripMenuItem.Name = "ZonasDeVentaToolStripMenuItem"
        Me.ZonasDeVentaToolStripMenuItem.Size = New System.Drawing.Size(283, 26)
        Me.ZonasDeVentaToolStripMenuItem.Tag = "frmZonaVenta"
        Me.ZonasDeVentaToolStripMenuItem.Text = "Zonas de Venta"
        '
        'EstadoDeClientesToolStripMenuItem
        '
        Me.EstadoDeClientesToolStripMenuItem.Name = "EstadoDeClientesToolStripMenuItem"
        Me.EstadoDeClientesToolStripMenuItem.Size = New System.Drawing.Size(283, 26)
        Me.EstadoDeClientesToolStripMenuItem.Tag = "frmEstadoCliente"
        Me.EstadoDeClientesToolStripMenuItem.Text = "Estado de Clientes"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(280, 6)
        '
        'tsmiClasificacion1
        '
        Me.tsmiClasificacion1.Name = "tsmiClasificacion1"
        Me.tsmiClasificacion1.Size = New System.Drawing.Size(283, 26)
        Me.tsmiClasificacion1.Tag = "frmTipoProducto"
        Me.tsmiClasificacion1.Text = "Tipo de Productos"
        '
        'tsmiClasificacion2
        '
        Me.tsmiClasificacion2.Name = "tsmiClasificacion2"
        Me.tsmiClasificacion2.Size = New System.Drawing.Size(283, 26)
        Me.tsmiClasificacion2.Tag = "frmLineas"
        Me.tsmiClasificacion2.Text = "Lineas"
        '
        'tsmiClasificacion3
        '
        Me.tsmiClasificacion3.Name = "tsmiClasificacion3"
        Me.tsmiClasificacion3.Size = New System.Drawing.Size(283, 26)
        Me.tsmiClasificacion3.Tag = "frmSubLinea"
        Me.tsmiClasificacion3.Text = "Sub-Lineas"
        '
        'tsmiClasificacion4
        '
        Me.tsmiClasificacion4.Name = "tsmiClasificacion4"
        Me.tsmiClasificacion4.Size = New System.Drawing.Size(283, 26)
        Me.tsmiClasificacion4.Tag = "frmSubLinea2"
        Me.tsmiClasificacion4.Text = "Sub-Linea 2"
        '
        'tsmiClasificacion5
        '
        Me.tsmiClasificacion5.Name = "tsmiClasificacion5"
        Me.tsmiClasificacion5.Size = New System.Drawing.Size(283, 26)
        Me.tsmiClasificacion5.Tag = "frmMarca"
        Me.tsmiClasificacion5.Text = "Marcas"
        '
        'tsmiClasificacion6
        '
        Me.tsmiClasificacion6.Name = "tsmiClasificacion6"
        Me.tsmiClasificacion6.Size = New System.Drawing.Size(283, 26)
        Me.tsmiClasificacion6.Tag = "frmPresentacion"
        Me.tsmiClasificacion6.Text = "Presentaciones"
        '
        'CategoriasToolStripMenuItem
        '
        Me.CategoriasToolStripMenuItem.Name = "CategoriasToolStripMenuItem"
        Me.CategoriasToolStripMenuItem.Size = New System.Drawing.Size(283, 26)
        Me.CategoriasToolStripMenuItem.Tag = "frmCategoria"
        Me.CategoriasToolStripMenuItem.Text = "Categorias"
        '
        'UnidadesDeMedidaToolStripMenuItem
        '
        Me.UnidadesDeMedidaToolStripMenuItem.Name = "UnidadesDeMedidaToolStripMenuItem"
        Me.UnidadesDeMedidaToolStripMenuItem.Size = New System.Drawing.Size(283, 26)
        Me.UnidadesDeMedidaToolStripMenuItem.Tag = "frmUnidadMedida"
        Me.UnidadesDeMedidaToolStripMenuItem.Text = "Unidades de Medida"
        '
        'ZonaEnDepositoToolStripMenuItem
        '
        Me.ZonaEnDepositoToolStripMenuItem.Name = "ZonaEnDepositoToolStripMenuItem"
        Me.ZonaEnDepositoToolStripMenuItem.Size = New System.Drawing.Size(283, 26)
        Me.ZonaEnDepositoToolStripMenuItem.Tag = "frmZonaDeposito"
        Me.ZonaEnDepositoToolStripMenuItem.Text = "Zona en Deposito"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(280, 6)
        '
        'TransportistasToolStripMenuItem
        '
        Me.TransportistasToolStripMenuItem.Name = "TransportistasToolStripMenuItem"
        Me.TransportistasToolStripMenuItem.Size = New System.Drawing.Size(283, 26)
        Me.TransportistasToolStripMenuItem.Tag = "frmTransporte"
        Me.TransportistasToolStripMenuItem.Text = "Transportistas"
        '
        'ChoferesToolStripMenuItem
        '
        Me.ChoferesToolStripMenuItem.Name = "ChoferesToolStripMenuItem"
        Me.ChoferesToolStripMenuItem.Size = New System.Drawing.Size(283, 26)
        Me.ChoferesToolStripMenuItem.Tag = "frmChofer"
        Me.ChoferesToolStripMenuItem.Text = "Choferes"
        '
        'CamionesToolStripMenuItem
        '
        Me.CamionesToolStripMenuItem.Name = "CamionesToolStripMenuItem"
        Me.CamionesToolStripMenuItem.Size = New System.Drawing.Size(283, 26)
        Me.CamionesToolStripMenuItem.Tag = "frmCamion"
        Me.CamionesToolStripMenuItem.Text = "Camiones"
        '
        'CategoriaDeClientesToolStripMenuItem
        '
        Me.CategoriaDeClientesToolStripMenuItem.Name = "CategoriaDeClientesToolStripMenuItem"
        Me.CategoriaDeClientesToolStripMenuItem.Size = New System.Drawing.Size(283, 26)
        Me.CategoriaDeClientesToolStripMenuItem.Tag = "frmCategoriaCliente"
        Me.CategoriaDeClientesToolStripMenuItem.Text = "Categoria de Clientes"
        '
        'ToolStripSeparator54
        '
        Me.ToolStripSeparator54.Name = "ToolStripSeparator54"
        Me.ToolStripSeparator54.Size = New System.Drawing.Size(280, 6)
        '
        'VencimientoDeProductosToolStripMenuItem
        '
        Me.VencimientoDeProductosToolStripMenuItem.Name = "VencimientoDeProductosToolStripMenuItem"
        Me.VencimientoDeProductosToolStripMenuItem.Size = New System.Drawing.Size(283, 26)
        Me.VencimientoDeProductosToolStripMenuItem.Tag = "frmVencimientodeProductos"
        Me.VencimientoDeProductosToolStripMenuItem.Text = "Vencimiento de Productos"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(262, 6)
        '
        'TerminalesToolStripMenuItem
        '
        Me.TerminalesToolStripMenuItem.Name = "TerminalesToolStripMenuItem"
        Me.TerminalesToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.TerminalesToolStripMenuItem.Tag = "frmTerminalTransaccion"
        Me.TerminalesToolStripMenuItem.Text = "Terminales"
        '
        'Sucursales6ToolStripMenuItem
        '
        Me.Sucursales6ToolStripMenuItem.Name = "Sucursales6ToolStripMenuItem"
        Me.Sucursales6ToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.Sucursales6ToolStripMenuItem.Tag = "frmSucursalDeposito"
        Me.Sucursales6ToolStripMenuItem.Text = "Sucursales - Depositos"
        '
        'TipoDeDepositoToolStripMenuItem
        '
        Me.TipoDeDepositoToolStripMenuItem.Name = "TipoDeDepositoToolStripMenuItem"
        Me.TipoDeDepositoToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.TipoDeDepositoToolStripMenuItem.Tag = "frmTipoDeposito"
        Me.TipoDeDepositoToolStripMenuItem.Text = "Tipo de deposito"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(262, 6)
        '
        'MonedaToolStripMenuItem
        '
        Me.MonedaToolStripMenuItem.Name = "MonedaToolStripMenuItem"
        Me.MonedaToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.MonedaToolStripMenuItem.Tag = "frmMoneda"
        Me.MonedaToolStripMenuItem.Text = "Monedas"
        '
        'CotizacionesToolStripMenuItem
        '
        Me.CotizacionesToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CotizacionesToolStripMenuItem.Name = "CotizacionesToolStripMenuItem"
        Me.CotizacionesToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.CotizacionesToolStripMenuItem.Tag = "frmCotizacion"
        Me.CotizacionesToolStripMenuItem.Text = "Cotizaciones"
        '
        'BancosToolStripMenuItem
        '
        Me.BancosToolStripMenuItem.Name = "BancosToolStripMenuItem"
        Me.BancosToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.BancosToolStripMenuItem.Tag = "frmBanco"
        Me.BancosToolStripMenuItem.Text = "Bancos"
        '
        'CuentasBancariasToolStripMenuItem
        '
        Me.CuentasBancariasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SaldoDiarioToolStripMenuItem})
        Me.CuentasBancariasToolStripMenuItem.Name = "CuentasBancariasToolStripMenuItem"
        Me.CuentasBancariasToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.CuentasBancariasToolStripMenuItem.Tag = "frmCuentaBancaria"
        Me.CuentasBancariasToolStripMenuItem.Text = "Cuentas Bancarias"
        '
        'SaldoDiarioToolStripMenuItem
        '
        Me.SaldoDiarioToolStripMenuItem.Name = "SaldoDiarioToolStripMenuItem"
        Me.SaldoDiarioToolStripMenuItem.Size = New System.Drawing.Size(182, 26)
        Me.SaldoDiarioToolStripMenuItem.Tag = "frmCuentaBancariaSaldoDiario"
        Me.SaldoDiarioToolStripMenuItem.Text = "Saldo Diario"
        '
        'ChequeraToolStripMenuItem
        '
        Me.ChequeraToolStripMenuItem.Name = "ChequeraToolStripMenuItem"
        Me.ChequeraToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.ChequeraToolStripMenuItem.Tag = "frmChequera"
        Me.ChequeraToolStripMenuItem.Text = "Chequera"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(262, 6)
        '
        'RegistrosToolStripMenuItem
        '
        Me.RegistrosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OperacionesToolStripMenuItem, Me.ToolStripMenuItem2, Me.MotivosToolStripMenuItem, Me.MotivosDeAnulacionDeVentaToolStripMenuItem, Me.ToolStripMenuItem9, Me.MotivosDeAnulacionCobranzasToolStripMenuItem, Me.SubMotivosNotaDeCreditoToolStripMenuItem, Me.ToolStripMenuItem11, Me.MotivoDeRemisionToolStripMenuItem, Me.ToolStripSeparator38, Me.UsuariosYAutorizacionesToolStripMenuItem, Me.PuntosDeExpedicionToolStripMenuItem, Me.GruposYUsuarioToolStripMenuItem, Me.FormaDePagoFacturasToolStripMenuItem, Me.AsignarCuentaContableAFondoFijoToolStripMenuItem, Me.AsignarCuentaContableARRHHToolStripMenuItem, Me.UsuariosUnidadDeNegocioYCCToolStripMenuItem, Me.MetasDeVentaToolStripMenuItem, Me.OperacionesPermitidasToolStripMenuItem, Me.AsignarCuentaContableIntegridadToolStripMenuItem, Me.TarjetaToolStripMenuItem, Me.MotivoDevolucionNCToolStripMenuItem})
        Me.RegistrosToolStripMenuItem.Name = "RegistrosToolStripMenuItem"
        Me.RegistrosToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.RegistrosToolStripMenuItem.Text = "Registros"
        '
        'OperacionesToolStripMenuItem
        '
        Me.OperacionesToolStripMenuItem.Name = "OperacionesToolStripMenuItem"
        Me.OperacionesToolStripMenuItem.ShowShortcutKeys = False
        Me.OperacionesToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.OperacionesToolStripMenuItem.Tag = "frmOperacion"
        Me.OperacionesToolStripMenuItem.Text = "Operaciones"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Image = CType(resources.GetObject("ToolStripMenuItem2.Image"), System.Drawing.Image)
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(370, 26)
        Me.ToolStripMenuItem2.Tag = "frmTipoComprobante"
        Me.ToolStripMenuItem2.Text = "Tipos de Comprobantes"
        '
        'MotivosToolStripMenuItem
        '
        Me.MotivosToolStripMenuItem.Name = "MotivosToolStripMenuItem"
        Me.MotivosToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.MotivosToolStripMenuItem.Tag = "frmMotivo"
        Me.MotivosToolStripMenuItem.Text = "Motivos"
        '
        'MotivosDeAnulacionDeVentaToolStripMenuItem
        '
        Me.MotivosDeAnulacionDeVentaToolStripMenuItem.Name = "MotivosDeAnulacionDeVentaToolStripMenuItem"
        Me.MotivosDeAnulacionDeVentaToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.MotivosDeAnulacionDeVentaToolStripMenuItem.Tag = "frmMotivoAnulacionVenta"
        Me.MotivosDeAnulacionDeVentaToolStripMenuItem.Text = "Motivos de anulacion de venta"
        '
        'ToolStripMenuItem9
        '
        Me.ToolStripMenuItem9.Name = "ToolStripMenuItem9"
        Me.ToolStripMenuItem9.Size = New System.Drawing.Size(370, 26)
        Me.ToolStripMenuItem9.Tag = "frmMotivoAnulacionNotaCredito"
        Me.ToolStripMenuItem9.Text = "Motivos de anulacion  Nota de Credito"
        '
        'MotivosDeAnulacionCobranzasToolStripMenuItem
        '
        Me.MotivosDeAnulacionCobranzasToolStripMenuItem.Name = "MotivosDeAnulacionCobranzasToolStripMenuItem"
        Me.MotivosDeAnulacionCobranzasToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.MotivosDeAnulacionCobranzasToolStripMenuItem.Text = "Motivos de anulacion Cobranzas"
        '
        'SubMotivosNotaDeCreditoToolStripMenuItem
        '
        Me.SubMotivosNotaDeCreditoToolStripMenuItem.Name = "SubMotivosNotaDeCreditoToolStripMenuItem"
        Me.SubMotivosNotaDeCreditoToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.SubMotivosNotaDeCreditoToolStripMenuItem.Tag = "frmABMSubMotivoNC"
        Me.SubMotivosNotaDeCreditoToolStripMenuItem.Text = "Sub Motivos Nota de Credito"
        '
        'ToolStripMenuItem11
        '
        Me.ToolStripMenuItem11.Name = "ToolStripMenuItem11"
        Me.ToolStripMenuItem11.Size = New System.Drawing.Size(370, 26)
        Me.ToolStripMenuItem11.Text = "Motivo Reproceso"
        '
        'MotivoDeRemisionToolStripMenuItem
        '
        Me.MotivoDeRemisionToolStripMenuItem.Name = "MotivoDeRemisionToolStripMenuItem"
        Me.MotivoDeRemisionToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.MotivoDeRemisionToolStripMenuItem.Text = "Motivo de Remision"
        '
        'ToolStripSeparator38
        '
        Me.ToolStripSeparator38.Name = "ToolStripSeparator38"
        Me.ToolStripSeparator38.Size = New System.Drawing.Size(367, 6)
        '
        'UsuariosYAutorizacionesToolStripMenuItem
        '
        Me.UsuariosYAutorizacionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UsuariosParaAutorizarAnulacionToolStripMenuItem, Me.PorcentajeDeExcepcionToolStripMenuItem1, Me.PorcentajeDeExcesoDeLineaDeCreditoToolStripMenuItem, Me.AutorizadoresDeNotaDeCreditoToolStripMenuItem, Me.UsuariosPermitidosCargaDeRecepcionDeDocumentosToolStripMenuItem})
        Me.UsuariosYAutorizacionesToolStripMenuItem.Name = "UsuariosYAutorizacionesToolStripMenuItem"
        Me.UsuariosYAutorizacionesToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.UsuariosYAutorizacionesToolStripMenuItem.Text = "Usuarios y Autorizaciones"
        '
        'UsuariosParaAutorizarAnulacionToolStripMenuItem
        '
        Me.UsuariosParaAutorizarAnulacionToolStripMenuItem.Name = "UsuariosParaAutorizarAnulacionToolStripMenuItem"
        Me.UsuariosParaAutorizarAnulacionToolStripMenuItem.Size = New System.Drawing.Size(423, 26)
        Me.UsuariosParaAutorizarAnulacionToolStripMenuItem.Tag = "frmUsuarioAutorizadorAnulacion"
        Me.UsuariosParaAutorizarAnulacionToolStripMenuItem.Text = "Autorizadores de Anulacion"
        '
        'PorcentajeDeExcepcionToolStripMenuItem1
        '
        Me.PorcentajeDeExcepcionToolStripMenuItem1.Name = "PorcentajeDeExcepcionToolStripMenuItem1"
        Me.PorcentajeDeExcepcionToolStripMenuItem1.Size = New System.Drawing.Size(423, 26)
        Me.PorcentajeDeExcepcionToolStripMenuItem1.Tag = "frmPorcentajeExcepcion"
        Me.PorcentajeDeExcepcionToolStripMenuItem1.Text = "Autorizadores de Descuentos"
        '
        'PorcentajeDeExcesoDeLineaDeCreditoToolStripMenuItem
        '
        Me.PorcentajeDeExcesoDeLineaDeCreditoToolStripMenuItem.Name = "PorcentajeDeExcesoDeLineaDeCreditoToolStripMenuItem"
        Me.PorcentajeDeExcesoDeLineaDeCreditoToolStripMenuItem.Size = New System.Drawing.Size(423, 26)
        Me.PorcentajeDeExcesoDeLineaDeCreditoToolStripMenuItem.Tag = "frmPorcentajeExcesoLineaCredito"
        Me.PorcentajeDeExcesoDeLineaDeCreditoToolStripMenuItem.Text = "Autorizadores de Exceso de Linea de Credito"
        '
        'AutorizadoresDeNotaDeCreditoToolStripMenuItem
        '
        Me.AutorizadoresDeNotaDeCreditoToolStripMenuItem.Name = "AutorizadoresDeNotaDeCreditoToolStripMenuItem"
        Me.AutorizadoresDeNotaDeCreditoToolStripMenuItem.Size = New System.Drawing.Size(423, 26)
        Me.AutorizadoresDeNotaDeCreditoToolStripMenuItem.Tag = "frmUsuarioAutorizarPedidoNotaCredito"
        Me.AutorizadoresDeNotaDeCreditoToolStripMenuItem.Text = "Autorizadores de Nota de Credito"
        '
        'UsuariosPermitidosCargaDeRecepcionDeDocumentosToolStripMenuItem
        '
        Me.UsuariosPermitidosCargaDeRecepcionDeDocumentosToolStripMenuItem.Name = "UsuariosPermitidosCargaDeRecepcionDeDocumentosToolStripMenuItem"
        Me.UsuariosPermitidosCargaDeRecepcionDeDocumentosToolStripMenuItem.Size = New System.Drawing.Size(423, 26)
        Me.UsuariosPermitidosCargaDeRecepcionDeDocumentosToolStripMenuItem.Tag = "frmUsuarioRecepcionDocumento"
        Me.UsuariosPermitidosCargaDeRecepcionDeDocumentosToolStripMenuItem.Text = " Carga de Recepcion de documentos"
        '
        'PuntosDeExpedicionToolStripMenuItem
        '
        Me.PuntosDeExpedicionToolStripMenuItem.Name = "PuntosDeExpedicionToolStripMenuItem"
        Me.PuntosDeExpedicionToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.PuntosDeExpedicionToolStripMenuItem.Tag = "frmPuntoExpedicion"
        Me.PuntosDeExpedicionToolStripMenuItem.Text = "Puntos de Expedicion"
        '
        'GruposYUsuarioToolStripMenuItem
        '
        Me.GruposYUsuarioToolStripMenuItem.Name = "GruposYUsuarioToolStripMenuItem"
        Me.GruposYUsuarioToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.GruposYUsuarioToolStripMenuItem.Tag = "frmGrupo"
        Me.GruposYUsuarioToolStripMenuItem.Text = "Grupos y Usuario"
        '
        'FormaDePagoFacturasToolStripMenuItem
        '
        Me.FormaDePagoFacturasToolStripMenuItem.Name = "FormaDePagoFacturasToolStripMenuItem"
        Me.FormaDePagoFacturasToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.FormaDePagoFacturasToolStripMenuItem.Tag = "frmFormaPagoFactura"
        Me.FormaDePagoFacturasToolStripMenuItem.Text = "Forma de pago Facturas"
        '
        'AsignarCuentaContableAFondoFijoToolStripMenuItem
        '
        Me.AsignarCuentaContableAFondoFijoToolStripMenuItem.Name = "AsignarCuentaContableAFondoFijoToolStripMenuItem"
        Me.AsignarCuentaContableAFondoFijoToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.AsignarCuentaContableAFondoFijoToolStripMenuItem.Tag = "frmFondoFijoCuentaContable"
        Me.AsignarCuentaContableAFondoFijoToolStripMenuItem.Text = "Asignar Cuenta Contable a Fondo Fijo"
        '
        'AsignarCuentaContableARRHHToolStripMenuItem
        '
        Me.AsignarCuentaContableARRHHToolStripMenuItem.Name = "AsignarCuentaContableARRHHToolStripMenuItem"
        Me.AsignarCuentaContableARRHHToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.AsignarCuentaContableARRHHToolStripMenuItem.Tag = "frmRRHHCuentaContable"
        Me.AsignarCuentaContableARRHHToolStripMenuItem.Text = "Asignar Cuenta Contable a RRHH"
        '
        'UsuariosUnidadDeNegocioYCCToolStripMenuItem
        '
        Me.UsuariosUnidadDeNegocioYCCToolStripMenuItem.Name = "UsuariosUnidadDeNegocioYCCToolStripMenuItem"
        Me.UsuariosUnidadDeNegocioYCCToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.UsuariosUnidadDeNegocioYCCToolStripMenuItem.Tag = "frmUsuarioUnidadNegocio"
        Me.UsuariosUnidadDeNegocioYCCToolStripMenuItem.Text = "Usuarios - Unidad de Negocio y CC"
        '
        'MetasDeVentaToolStripMenuItem
        '
        Me.MetasDeVentaToolStripMenuItem.Name = "MetasDeVentaToolStripMenuItem"
        Me.MetasDeVentaToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.MetasDeVentaToolStripMenuItem.Tag = "frmMeta"
        Me.MetasDeVentaToolStripMenuItem.Text = "Metas de Venta"
        '
        'OperacionesPermitidasToolStripMenuItem
        '
        Me.OperacionesPermitidasToolStripMenuItem.Name = "OperacionesPermitidasToolStripMenuItem"
        Me.OperacionesPermitidasToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.OperacionesPermitidasToolStripMenuItem.Tag = "frmOperacionPermitida"
        Me.OperacionesPermitidasToolStripMenuItem.Text = "Operaciones Permitidas"
        '
        'AsignarCuentaContableIntegridadToolStripMenuItem
        '
        Me.AsignarCuentaContableIntegridadToolStripMenuItem.Name = "AsignarCuentaContableIntegridadToolStripMenuItem"
        Me.AsignarCuentaContableIntegridadToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.AsignarCuentaContableIntegridadToolStripMenuItem.Text = "Asignar Cuenta Contable Integridad"
        '
        'TarjetaToolStripMenuItem
        '
        Me.TarjetaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProcesadoraDeTarjetaToolStripMenuItem})
        Me.TarjetaToolStripMenuItem.Name = "TarjetaToolStripMenuItem"
        Me.TarjetaToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.TarjetaToolStripMenuItem.Text = "Tarjeta"
        '
        'ProcesadoraDeTarjetaToolStripMenuItem
        '
        Me.ProcesadoraDeTarjetaToolStripMenuItem.Name = "ProcesadoraDeTarjetaToolStripMenuItem"
        Me.ProcesadoraDeTarjetaToolStripMenuItem.Size = New System.Drawing.Size(261, 26)
        Me.ProcesadoraDeTarjetaToolStripMenuItem.Text = "Procesadora de Tarjeta"
        '
        'MotivoDevolucionNCToolStripMenuItem
        '
        Me.MotivoDevolucionNCToolStripMenuItem.Name = "MotivoDevolucionNCToolStripMenuItem"
        Me.MotivoDevolucionNCToolStripMenuItem.Size = New System.Drawing.Size(370, 26)
        Me.MotivoDevolucionNCToolStripMenuItem.Text = "Motivo Devolucion NC"
        '
        'LlamadasToolStripMenuItem
        '
        Me.LlamadasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TipoDeLlamadaToolStripMenuItem, Me.ResultadoDeLlamadaToolStripMenuItem})
        Me.LlamadasToolStripMenuItem.Name = "LlamadasToolStripMenuItem"
        Me.LlamadasToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.LlamadasToolStripMenuItem.Text = "Llamadas"
        '
        'TipoDeLlamadaToolStripMenuItem
        '
        Me.TipoDeLlamadaToolStripMenuItem.Name = "TipoDeLlamadaToolStripMenuItem"
        Me.TipoDeLlamadaToolStripMenuItem.Size = New System.Drawing.Size(246, 26)
        Me.TipoDeLlamadaToolStripMenuItem.Tag = "frmTipoLlamada"
        Me.TipoDeLlamadaToolStripMenuItem.Text = "Tipo de llamada"
        '
        'ResultadoDeLlamadaToolStripMenuItem
        '
        Me.ResultadoDeLlamadaToolStripMenuItem.Name = "ResultadoDeLlamadaToolStripMenuItem"
        Me.ResultadoDeLlamadaToolStripMenuItem.Size = New System.Drawing.Size(246, 26)
        Me.ResultadoDeLlamadaToolStripMenuItem.Tag = "frmResultadoLlamada"
        Me.ResultadoDeLlamadaToolStripMenuItem.Text = "Resultado de llamada"
        '
        'AreasToolStripMenuItem
        '
        Me.AreasToolStripMenuItem.Name = "AreasToolStripMenuItem"
        Me.AreasToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.AreasToolStripMenuItem.Tag = "frmArea"
        Me.AreasToolStripMenuItem.Text = "Areas"
        '
        'RutasToolStripMenuItem
        '
        Me.RutasToolStripMenuItem.Name = "RutasToolStripMenuItem"
        Me.RutasToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.RutasToolStripMenuItem.Text = "Rutas"
        '
        'CambiarVendedorToolStripMenuItem
        '
        Me.CambiarVendedorToolStripMenuItem.Name = "CambiarVendedorToolStripMenuItem"
        Me.CambiarVendedorToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.CambiarVendedorToolStripMenuItem.Tag = "frmCambiarVendedor"
        Me.CambiarVendedorToolStripMenuItem.Text = "Cambiar Vendedor"
        '
        'InactivacionDeClientesToolStripMenuItem
        '
        Me.InactivacionDeClientesToolStripMenuItem.Name = "InactivacionDeClientesToolStripMenuItem"
        Me.InactivacionDeClientesToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.InactivacionDeClientesToolStripMenuItem.Tag = "frmInactivacionClientes"
        Me.InactivacionDeClientesToolStripMenuItem.Text = "Inactivacion de Clientes"
        '
        'VariosProveedoresToolStripMenuItem
        '
        Me.VariosProveedoresToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CamionesToolStripMenuItem1, Me.ChoferesToolStripMenuItem1})
        Me.VariosProveedoresToolStripMenuItem.Name = "VariosProveedoresToolStripMenuItem"
        Me.VariosProveedoresToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.VariosProveedoresToolStripMenuItem.Text = "Varios - Proveedores"
        '
        'CamionesToolStripMenuItem1
        '
        Me.CamionesToolStripMenuItem1.Name = "CamionesToolStripMenuItem1"
        Me.CamionesToolStripMenuItem1.Size = New System.Drawing.Size(165, 26)
        Me.CamionesToolStripMenuItem1.Tag = "frmCamionProveedor"
        Me.CamionesToolStripMenuItem1.Text = "Camiones"
        '
        'ChoferesToolStripMenuItem1
        '
        Me.ChoferesToolStripMenuItem1.Name = "ChoferesToolStripMenuItem1"
        Me.ChoferesToolStripMenuItem1.Size = New System.Drawing.Size(165, 26)
        Me.ChoferesToolStripMenuItem1.Tag = "frmChoferProveedor"
        Me.ChoferesToolStripMenuItem1.Text = "Choferes"
        '
        'CambiarRepositorToolStripMenuItem
        '
        Me.CambiarRepositorToolStripMenuItem.Name = "CambiarRepositorToolStripMenuItem"
        Me.CambiarRepositorToolStripMenuItem.Size = New System.Drawing.Size(265, 26)
        Me.CambiarRepositorToolStripMenuItem.Tag = "frmCambiarRepositor"
        Me.CambiarRepositorToolStripMenuItem.Text = "Cambiar Repositor"
        '
        'VentasToolStripMenuItem
        '
        Me.VentasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FacturacionToolStripMenuItem, Me.FacturacionDePedidosToolStripMenuItem, Me.ConsultaDeComprobanteToolStripMenuItem, Me.AnulacionDeFacturaToolStripMenuItem, Me.SolicitudesDeAnulacionToolStripMenuItem, Me.InformesToolStripMenuItem, Me.ToolStripSeparator7, Me.PedidosToolStripMenuItem, Me.DescuentosToolStripMenuItem1, Me.ToolStripSeparator18, Me.NotaDeCreditoToolStripMenuItem, Me.NotaDeDebitoToolStripMenuItem, Me.ToolStripSeparator8, Me.LoteToolStripMenuItem, Me.ToolStripMenuItem3, Me.TeleventasToolStripMenuItem, Me.InformeTeleventaToolStripMenuItem, Me.TransporteToolStripMenuItem, Me.InformesGerencialesToolStripMenuItem, Me.ReenvioComprobantesToolStripMenuItem, Me.AcuerdoDeClientesToolStripMenuItem})
        Me.VentasToolStripMenuItem.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VentasToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.VentasToolStripMenuItem.Name = "VentasToolStripMenuItem"
        Me.VentasToolStripMenuItem.Size = New System.Drawing.Size(72, 24)
        Me.VentasToolStripMenuItem.Text = "Ventas"
        '
        'FacturacionToolStripMenuItem
        '
        Me.FacturacionToolStripMenuItem.Name = "FacturacionToolStripMenuItem"
        Me.FacturacionToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.FacturacionToolStripMenuItem.Tag = "frmVenta"
        Me.FacturacionToolStripMenuItem.Text = "Facturacion"
        '
        'FacturacionDePedidosToolStripMenuItem
        '
        Me.FacturacionDePedidosToolStripMenuItem.Name = "FacturacionDePedidosToolStripMenuItem"
        Me.FacturacionDePedidosToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.FacturacionDePedidosToolStripMenuItem.Tag = "frmFacturarPedidos"
        Me.FacturacionDePedidosToolStripMenuItem.Text = "Facturacion de Pedidos"
        Me.FacturacionDePedidosToolStripMenuItem.ToolTipText = "Facturacion e Impresion de Pedidos automatico"
        '
        'ConsultaDeComprobanteToolStripMenuItem
        '
        Me.ConsultaDeComprobanteToolStripMenuItem.Name = "ConsultaDeComprobanteToolStripMenuItem"
        Me.ConsultaDeComprobanteToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.ConsultaDeComprobanteToolStripMenuItem.Tag = "frmConsultaVenta"
        Me.ConsultaDeComprobanteToolStripMenuItem.Text = "Consulta de Comprobantes"
        '
        'AnulacionDeFacturaToolStripMenuItem
        '
        Me.AnulacionDeFacturaToolStripMenuItem.Name = "AnulacionDeFacturaToolStripMenuItem"
        Me.AnulacionDeFacturaToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.AnulacionDeFacturaToolStripMenuItem.Tag = "frmAdministrarVenta"
        Me.AnulacionDeFacturaToolStripMenuItem.Text = "Administrar Facturas"
        '
        'SolicitudesDeAnulacionToolStripMenuItem
        '
        Me.SolicitudesDeAnulacionToolStripMenuItem.Name = "SolicitudesDeAnulacionToolStripMenuItem"
        Me.SolicitudesDeAnulacionToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.SolicitudesDeAnulacionToolStripMenuItem.Tag = "frmSolicitudAnulacion"
        Me.SolicitudesDeAnulacionToolStripMenuItem.Text = "Solicitudes de Anulacion"
        '
        'InformesToolStripMenuItem
        '
        Me.InformesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListadoDeComprobantesEmitidosToolStripMenuItem, Me.ListadoDeComprobantesAnuladosToolStripMenuItem, Me.ToolStripSeparator15, Me.ToolStripMenuItem7, Me.VentasPorClienteToolStripMenuItem1, Me.VentasAnualesToolStripMenuItem, Me.ToolStripMenuItem8, Me.VentasDeProductosInferiorAlCostoToolStripMenuItem, Me.ComisionesPorVendedorToolStripMenuItem, Me.DescuentosPorVendedorToolStripMenuItem, Me.GráficosToolStripMenuItem, Me.VentasAExcelToolStripMenuItem, Me.ListadoDeExcepcionesToolStripMenuItem, Me.ListadoDeDescuentosConcedidosToolStripMenuItem, Me.RegistroDePreciosPorClienteProductoToolStripMenuItem})
        Me.InformesToolStripMenuItem.Name = "InformesToolStripMenuItem"
        Me.InformesToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.InformesToolStripMenuItem.Text = "Informes"
        '
        'ListadoDeComprobantesEmitidosToolStripMenuItem
        '
        Me.ListadoDeComprobantesEmitidosToolStripMenuItem.Name = "ListadoDeComprobantesEmitidosToolStripMenuItem"
        Me.ListadoDeComprobantesEmitidosToolStripMenuItem.Size = New System.Drawing.Size(401, 26)
        Me.ListadoDeComprobantesEmitidosToolStripMenuItem.Tag = "frmListadoFacturacionEmitida"
        Me.ListadoDeComprobantesEmitidosToolStripMenuItem.Text = "Listado de Comprobantes Emitidos"
        '
        'ListadoDeComprobantesAnuladosToolStripMenuItem
        '
        Me.ListadoDeComprobantesAnuladosToolStripMenuItem.Name = "ListadoDeComprobantesAnuladosToolStripMenuItem"
        Me.ListadoDeComprobantesAnuladosToolStripMenuItem.Size = New System.Drawing.Size(401, 26)
        Me.ListadoDeComprobantesAnuladosToolStripMenuItem.Tag = "frmListadoVentaAnulada"
        Me.ListadoDeComprobantesAnuladosToolStripMenuItem.Text = "Listado de Comprobantes Anulados"
        '
        'ToolStripSeparator15
        '
        Me.ToolStripSeparator15.Name = "ToolStripSeparator15"
        Me.ToolStripSeparator15.Size = New System.Drawing.Size(398, 6)
        '
        'ToolStripMenuItem7
        '
        Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
        Me.ToolStripMenuItem7.Size = New System.Drawing.Size(401, 26)
        Me.ToolStripMenuItem7.Tag = "frmListadoVentasPorProducto"
        Me.ToolStripMenuItem7.Text = "Ventas por Producto"
        '
        'VentasPorClienteToolStripMenuItem1
        '
        Me.VentasPorClienteToolStripMenuItem1.Name = "VentasPorClienteToolStripMenuItem1"
        Me.VentasPorClienteToolStripMenuItem1.Size = New System.Drawing.Size(401, 26)
        Me.VentasPorClienteToolStripMenuItem1.Tag = "frmListadoVentasPorCliente"
        Me.VentasPorClienteToolStripMenuItem1.Text = "Ventas por Cliente"
        '
        'VentasAnualesToolStripMenuItem
        '
        Me.VentasAnualesToolStripMenuItem.Name = "VentasAnualesToolStripMenuItem"
        Me.VentasAnualesToolStripMenuItem.Size = New System.Drawing.Size(401, 26)
        Me.VentasAnualesToolStripMenuItem.Tag = "frmListadoVentasTotalAnualProductoCliente"
        Me.VentasAnualesToolStripMenuItem.Text = "Ventas Anuales de Clientes"
        '
        'ToolStripMenuItem8
        '
        Me.ToolStripMenuItem8.ForeColor = System.Drawing.Color.Black
        Me.ToolStripMenuItem8.Name = "ToolStripMenuItem8"
        Me.ToolStripMenuItem8.Size = New System.Drawing.Size(401, 26)
        Me.ToolStripMenuItem8.Tag = "frmVentasPorClienteProveedor"
        Me.ToolStripMenuItem8.Text = "Ventas por Cliente / Proveedor (SKU)"
        '
        'VentasDeProductosInferiorAlCostoToolStripMenuItem
        '
        Me.VentasDeProductosInferiorAlCostoToolStripMenuItem.ForeColor = System.Drawing.SystemColors.GrayText
        Me.VentasDeProductosInferiorAlCostoToolStripMenuItem.Name = "VentasDeProductosInferiorAlCostoToolStripMenuItem"
        Me.VentasDeProductosInferiorAlCostoToolStripMenuItem.Size = New System.Drawing.Size(401, 26)
        Me.VentasDeProductosInferiorAlCostoToolStripMenuItem.Text = "Ventas de Productos Inferior al Costo"
        '
        'ComisionesPorVendedorToolStripMenuItem
        '
        Me.ComisionesPorVendedorToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ComisionesPorVendedorToolStripMenuItem.Name = "ComisionesPorVendedorToolStripMenuItem"
        Me.ComisionesPorVendedorToolStripMenuItem.Size = New System.Drawing.Size(401, 26)
        Me.ComisionesPorVendedorToolStripMenuItem.Tag = "frmComisionesporVendedor"
        Me.ComisionesPorVendedorToolStripMenuItem.Text = "Comisiones por Vendedor"
        '
        'DescuentosPorVendedorToolStripMenuItem
        '
        Me.DescuentosPorVendedorToolStripMenuItem.ForeColor = System.Drawing.SystemColors.GrayText
        Me.DescuentosPorVendedorToolStripMenuItem.Name = "DescuentosPorVendedorToolStripMenuItem"
        Me.DescuentosPorVendedorToolStripMenuItem.Size = New System.Drawing.Size(401, 26)
        Me.DescuentosPorVendedorToolStripMenuItem.Text = "Descuentos por Vendedor"
        '
        'GráficosToolStripMenuItem
        '
        Me.GráficosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GráficoVentaMesTotalToolStripMenuItem, Me.GráficoRankingClientesVentaToolStripMenuItem, Me.GráficoProductosMásRentablesToolStripMenuItem1, Me.GráficoProductosMásVendidoToolStripMenuItem1})
        Me.GráficosToolStripMenuItem.Name = "GráficosToolStripMenuItem"
        Me.GráficosToolStripMenuItem.Size = New System.Drawing.Size(401, 26)
        Me.GráficosToolStripMenuItem.Text = "Gráficos"
        '
        'GráficoVentaMesTotalToolStripMenuItem
        '
        Me.GráficoVentaMesTotalToolStripMenuItem.Name = "GráficoVentaMesTotalToolStripMenuItem"
        Me.GráficoVentaMesTotalToolStripMenuItem.Size = New System.Drawing.Size(277, 26)
        Me.GráficoVentaMesTotalToolStripMenuItem.Tag = "frmGraficoVentaMesTotal"
        Me.GráficoVentaMesTotalToolStripMenuItem.Text = "Venta Mes Total"
        '
        'GráficoRankingClientesVentaToolStripMenuItem
        '
        Me.GráficoRankingClientesVentaToolStripMenuItem.Name = "GráficoRankingClientesVentaToolStripMenuItem"
        Me.GráficoRankingClientesVentaToolStripMenuItem.Size = New System.Drawing.Size(277, 26)
        Me.GráficoRankingClientesVentaToolStripMenuItem.Tag = "frmGraficoRankingClientesVenta"
        Me.GráficoRankingClientesVentaToolStripMenuItem.Text = "Ranking Clientes Venta"
        '
        'GráficoProductosMásRentablesToolStripMenuItem1
        '
        Me.GráficoProductosMásRentablesToolStripMenuItem1.Name = "GráficoProductosMásRentablesToolStripMenuItem1"
        Me.GráficoProductosMásRentablesToolStripMenuItem1.Size = New System.Drawing.Size(277, 26)
        Me.GráficoProductosMásRentablesToolStripMenuItem1.Tag = "frmGraficoRankingProductoRentable"
        Me.GráficoProductosMásRentablesToolStripMenuItem1.Text = "Productos más Rentables"
        '
        'GráficoProductosMásVendidoToolStripMenuItem1
        '
        Me.GráficoProductosMásVendidoToolStripMenuItem1.Name = "GráficoProductosMásVendidoToolStripMenuItem1"
        Me.GráficoProductosMásVendidoToolStripMenuItem1.Size = New System.Drawing.Size(277, 26)
        Me.GráficoProductosMásVendidoToolStripMenuItem1.Tag = "frmGraficoRankingProductoMasVendido"
        Me.GráficoProductosMásVendidoToolStripMenuItem1.Text = "Productos más Vendido"
        '
        'VentasAExcelToolStripMenuItem
        '
        Me.VentasAExcelToolStripMenuItem.Name = "VentasAExcelToolStripMenuItem"
        Me.VentasAExcelToolStripMenuItem.Size = New System.Drawing.Size(401, 26)
        Me.VentasAExcelToolStripMenuItem.Tag = "frmVentasExcel"
        Me.VentasAExcelToolStripMenuItem.Text = "Ventas a Excel"
        '
        'ListadoDeExcepcionesToolStripMenuItem
        '
        Me.ListadoDeExcepcionesToolStripMenuItem.Name = "ListadoDeExcepcionesToolStripMenuItem"
        Me.ListadoDeExcepcionesToolStripMenuItem.Size = New System.Drawing.Size(401, 26)
        Me.ListadoDeExcepcionesToolStripMenuItem.Tag = "frmListadoExcepciones"
        Me.ListadoDeExcepcionesToolStripMenuItem.Text = "Listado de Excepciones"
        '
        'ListadoDeDescuentosConcedidosToolStripMenuItem
        '
        Me.ListadoDeDescuentosConcedidosToolStripMenuItem.Name = "ListadoDeDescuentosConcedidosToolStripMenuItem"
        Me.ListadoDeDescuentosConcedidosToolStripMenuItem.Size = New System.Drawing.Size(401, 26)
        Me.ListadoDeDescuentosConcedidosToolStripMenuItem.Tag = "frmListadoDetalladoDescuento"
        Me.ListadoDeDescuentosConcedidosToolStripMenuItem.Text = "Listado de Descuentos Concedidos"
        '
        'RegistroDePreciosPorClienteProductoToolStripMenuItem
        '
        Me.RegistroDePreciosPorClienteProductoToolStripMenuItem.Name = "RegistroDePreciosPorClienteProductoToolStripMenuItem"
        Me.RegistroDePreciosPorClienteProductoToolStripMenuItem.Size = New System.Drawing.Size(401, 26)
        Me.RegistroDePreciosPorClienteProductoToolStripMenuItem.Tag = "frmRegistroPrecioCliente"
        Me.RegistroDePreciosPorClienteProductoToolStripMenuItem.Text = "Registro de Precios por Cliente / Producto"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(318, 6)
        '
        'PedidosToolStripMenuItem
        '
        Me.PedidosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargaToolStripMenuItem2, Me.CargaToolStripMenuItem8, Me.ImportarToolStripMenuItem, Me.ImprimirToolStripMenuItem1, Me.AprobacionToolStripMenuItem, Me.AprobarExcesoDeLineaDeCreditoToolStripMenuItem, Me.ToolStripSeparator19, Me.InformeToolStripMenuItem2, Me.InformeAnticipacionToolStripMenuItem})
        Me.PedidosToolStripMenuItem.Name = "PedidosToolStripMenuItem"
        Me.PedidosToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.PedidosToolStripMenuItem.Text = "Pedidos"
        '
        'CargaToolStripMenuItem2
        '
        Me.CargaToolStripMenuItem2.Name = "CargaToolStripMenuItem2"
        Me.CargaToolStripMenuItem2.Size = New System.Drawing.Size(358, 26)
        Me.CargaToolStripMenuItem2.Tag = "frmPedido"
        Me.CargaToolStripMenuItem2.Text = "Carga"
        '
        'CargaToolStripMenuItem8
        '
        Me.CargaToolStripMenuItem8.Name = "CargaToolStripMenuItem8"
        Me.CargaToolStripMenuItem8.Size = New System.Drawing.Size(358, 26)
        Me.CargaToolStripMenuItem8.Tag = "frmPedidoNUEVO"
        Me.CargaToolStripMenuItem8.Text = "Carga"
        '
        'ImportarToolStripMenuItem
        '
        Me.ImportarToolStripMenuItem.Name = "ImportarToolStripMenuItem"
        Me.ImportarToolStripMenuItem.Size = New System.Drawing.Size(358, 26)
        Me.ImportarToolStripMenuItem.Tag = "frmImportarPedidos"
        Me.ImportarToolStripMenuItem.Text = "Importar"
        '
        'ImprimirToolStripMenuItem1
        '
        Me.ImprimirToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.GrayText
        Me.ImprimirToolStripMenuItem1.Name = "ImprimirToolStripMenuItem1"
        Me.ImprimirToolStripMenuItem1.Size = New System.Drawing.Size(358, 26)
        Me.ImprimirToolStripMenuItem1.Text = "Imprimir"
        '
        'AprobacionToolStripMenuItem
        '
        Me.AprobacionToolStripMenuItem.Name = "AprobacionToolStripMenuItem"
        Me.AprobacionToolStripMenuItem.Size = New System.Drawing.Size(358, 26)
        Me.AprobacionToolStripMenuItem.Tag = "frmAprobarPedido"
        Me.AprobacionToolStripMenuItem.Text = "Aprobar pedidos"
        '
        'AprobarExcesoDeLineaDeCreditoToolStripMenuItem
        '
        Me.AprobarExcesoDeLineaDeCreditoToolStripMenuItem.Name = "AprobarExcesoDeLineaDeCreditoToolStripMenuItem"
        Me.AprobarExcesoDeLineaDeCreditoToolStripMenuItem.Size = New System.Drawing.Size(358, 26)
        Me.AprobarExcesoDeLineaDeCreditoToolStripMenuItem.Tag = "frmAprobarExcesoLineaCredito"
        Me.AprobarExcesoDeLineaDeCreditoToolStripMenuItem.Text = "Aprobar Exceso de Linea de Credito"
        '
        'ToolStripSeparator19
        '
        Me.ToolStripSeparator19.Name = "ToolStripSeparator19"
        Me.ToolStripSeparator19.Size = New System.Drawing.Size(355, 6)
        '
        'InformeToolStripMenuItem2
        '
        Me.InformeToolStripMenuItem2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.InformeToolStripMenuItem2.Name = "InformeToolStripMenuItem2"
        Me.InformeToolStripMenuItem2.Size = New System.Drawing.Size(358, 26)
        Me.InformeToolStripMenuItem2.Tag = "frmPedidos"
        Me.InformeToolStripMenuItem2.Text = "Informe Pedidos"
        '
        'InformeAnticipacionToolStripMenuItem
        '
        Me.InformeAnticipacionToolStripMenuItem.Name = "InformeAnticipacionToolStripMenuItem"
        Me.InformeAnticipacionToolStripMenuItem.Size = New System.Drawing.Size(358, 26)
        Me.InformeAnticipacionToolStripMenuItem.Tag = "frmPedidoAnticipado"
        Me.InformeAnticipacionToolStripMenuItem.Text = "Informe Anticipacion"
        '
        'DescuentosToolStripMenuItem1
        '
        Me.DescuentosToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargaToolStripMenuItem13, Me.ActividadesToolStripMenuItem, Me.TransferirToolStripMenuItem, Me.ToolStripSeparator44, Me.InformeToolStripMenuItem18})
        Me.DescuentosToolStripMenuItem1.Name = "DescuentosToolStripMenuItem1"
        Me.DescuentosToolStripMenuItem1.Size = New System.Drawing.Size(321, 26)
        Me.DescuentosToolStripMenuItem1.Text = "Planilla de Descuentos Tacticos"
        '
        'CargaToolStripMenuItem13
        '
        Me.CargaToolStripMenuItem13.Name = "CargaToolStripMenuItem13"
        Me.CargaToolStripMenuItem13.Size = New System.Drawing.Size(192, 26)
        Me.CargaToolStripMenuItem13.Tag = "frmPlanillaDescuentoTactico"
        Me.CargaToolStripMenuItem13.Text = "Carga"
        '
        'ActividadesToolStripMenuItem
        '
        Me.ActividadesToolStripMenuItem.ForeColor = System.Drawing.SystemColors.GrayText
        Me.ActividadesToolStripMenuItem.Name = "ActividadesToolStripMenuItem"
        Me.ActividadesToolStripMenuItem.Size = New System.Drawing.Size(192, 26)
        Me.ActividadesToolStripMenuItem.Text = "Actividades"
        '
        'TransferirToolStripMenuItem
        '
        Me.TransferirToolStripMenuItem.ForeColor = System.Drawing.SystemColors.GrayText
        Me.TransferirToolStripMenuItem.Name = "TransferirToolStripMenuItem"
        Me.TransferirToolStripMenuItem.Size = New System.Drawing.Size(192, 26)
        Me.TransferirToolStripMenuItem.Text = "Transferencia"
        '
        'ToolStripSeparator44
        '
        Me.ToolStripSeparator44.Name = "ToolStripSeparator44"
        Me.ToolStripSeparator44.Size = New System.Drawing.Size(189, 6)
        '
        'InformeToolStripMenuItem18
        '
        Me.InformeToolStripMenuItem18.ForeColor = System.Drawing.SystemColors.GrayText
        Me.InformeToolStripMenuItem18.Name = "InformeToolStripMenuItem18"
        Me.InformeToolStripMenuItem18.Size = New System.Drawing.Size(192, 26)
        Me.InformeToolStripMenuItem18.Text = "Informe"
        '
        'ToolStripSeparator18
        '
        Me.ToolStripSeparator18.Name = "ToolStripSeparator18"
        Me.ToolStripSeparator18.Size = New System.Drawing.Size(318, 6)
        '
        'NotaDeCreditoToolStripMenuItem
        '
        Me.NotaDeCreditoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PedidoNotaDeCreditoMovilToolStripMenuItem, Me.PedidosDeNotaDeCreditoToolStripMenuItem, Me.AprobarPedidosDeNotasDeCredutoToolStripMenuItem, Me.ProcesarPedidoDeNotasDeCreditoToolStripMenuItem, Me.ToolStripSeparator49, Me.CargaToolStripMenuItem1, Me.NCRDocumentoInternoToolStripMenuItem, Me.AplicarToolStripMenuItem, Me.TransferenciaToolStripMenuItem, Me.ToolStripSeparator11, Me.InformeToolStripMenuItem1})
        Me.NotaDeCreditoToolStripMenuItem.Name = "NotaDeCreditoToolStripMenuItem"
        Me.NotaDeCreditoToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.NotaDeCreditoToolStripMenuItem.Text = "Nota de Credito"
        '
        'PedidoNotaDeCreditoMovilToolStripMenuItem
        '
        Me.PedidoNotaDeCreditoMovilToolStripMenuItem.Name = "PedidoNotaDeCreditoMovilToolStripMenuItem"
        Me.PedidoNotaDeCreditoMovilToolStripMenuItem.Size = New System.Drawing.Size(365, 26)
        Me.PedidoNotaDeCreditoMovilToolStripMenuItem.Tag = "frmPedidoNotaCreditoMovil"
        Me.PedidoNotaDeCreditoMovilToolStripMenuItem.Text = "Pedido Nota de Credito Movil"
        '
        'PedidosDeNotaDeCreditoToolStripMenuItem
        '
        Me.PedidosDeNotaDeCreditoToolStripMenuItem.Name = "PedidosDeNotaDeCreditoToolStripMenuItem"
        Me.PedidosDeNotaDeCreditoToolStripMenuItem.Size = New System.Drawing.Size(365, 26)
        Me.PedidosDeNotaDeCreditoToolStripMenuItem.Tag = "frmPedidoNotaCredito"
        Me.PedidosDeNotaDeCreditoToolStripMenuItem.Text = "Pedidos de Notas de Credito"
        '
        'AprobarPedidosDeNotasDeCredutoToolStripMenuItem
        '
        Me.AprobarPedidosDeNotasDeCredutoToolStripMenuItem.Name = "AprobarPedidosDeNotasDeCredutoToolStripMenuItem"
        Me.AprobarPedidosDeNotasDeCredutoToolStripMenuItem.Size = New System.Drawing.Size(365, 26)
        Me.AprobarPedidosDeNotasDeCredutoToolStripMenuItem.Tag = "frmAprobarPedidoNotaCredito"
        Me.AprobarPedidosDeNotasDeCredutoToolStripMenuItem.Text = "Aprobar Pedidos de Notas de Credito"
        '
        'ProcesarPedidoDeNotasDeCreditoToolStripMenuItem
        '
        Me.ProcesarPedidoDeNotasDeCreditoToolStripMenuItem.Name = "ProcesarPedidoDeNotasDeCreditoToolStripMenuItem"
        Me.ProcesarPedidoDeNotasDeCreditoToolStripMenuItem.Size = New System.Drawing.Size(365, 26)
        Me.ProcesarPedidoDeNotasDeCreditoToolStripMenuItem.Tag = "frmProcesarPedidoNotaCredito"
        Me.ProcesarPedidoDeNotasDeCreditoToolStripMenuItem.Text = "Procesar Pedido de Notas de Credito"
        '
        'ToolStripSeparator49
        '
        Me.ToolStripSeparator49.Name = "ToolStripSeparator49"
        Me.ToolStripSeparator49.Size = New System.Drawing.Size(362, 6)
        Me.ToolStripSeparator49.Visible = False
        '
        'CargaToolStripMenuItem1
        '
        Me.CargaToolStripMenuItem1.Name = "CargaToolStripMenuItem1"
        Me.CargaToolStripMenuItem1.Size = New System.Drawing.Size(365, 26)
        Me.CargaToolStripMenuItem1.Tag = "frmNotaCreditoCliente"
        Me.CargaToolStripMenuItem1.Text = "Cargar Notas de Credito"
        '
        'NCRDocumentoInternoToolStripMenuItem
        '
        Me.NCRDocumentoInternoToolStripMenuItem.Name = "NCRDocumentoInternoToolStripMenuItem"
        Me.NCRDocumentoInternoToolStripMenuItem.Size = New System.Drawing.Size(365, 26)
        Me.NCRDocumentoInternoToolStripMenuItem.Tag = "frmNotaCreditoCliente"
        Me.NCRDocumentoInternoToolStripMenuItem.Text = "Documento Interno"
        '
        'AplicarToolStripMenuItem
        '
        Me.AplicarToolStripMenuItem.Name = "AplicarToolStripMenuItem"
        Me.AplicarToolStripMenuItem.Size = New System.Drawing.Size(365, 26)
        Me.AplicarToolStripMenuItem.Tag = "frmAplicacionNotaCreditoCliente"
        Me.AplicarToolStripMenuItem.Text = "Aplicar"
        Me.AplicarToolStripMenuItem.ToolTipText = "Aplica la Nota de Credito a otra factura"
        '
        'TransferenciaToolStripMenuItem
        '
        Me.TransferenciaToolStripMenuItem.Name = "TransferenciaToolStripMenuItem"
        Me.TransferenciaToolStripMenuItem.Size = New System.Drawing.Size(365, 26)
        Me.TransferenciaToolStripMenuItem.Tag = "frmNotaCreditoTransferencia"
        Me.TransferenciaToolStripMenuItem.Text = "Transferencia"
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(362, 6)
        '
        'InformeToolStripMenuItem1
        '
        Me.InformeToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListadoDeNotasDeCreditoToolStripMenuItem, Me.ToolStripMenuItem33, Me.ListadoDeAplicacionPendienteToolStripMenuItem, Me.ListadoDeNCConVentasAplicadasToolStripMenuItem, Me.ToolStripSeparator53, Me.ListadoDePedidoDeNotasDeCreditoToolStripMenuItem, Me.PanelDePedidosDeDevolucionesToolStripMenuItem})
        Me.InformeToolStripMenuItem1.Name = "InformeToolStripMenuItem1"
        Me.InformeToolStripMenuItem1.Size = New System.Drawing.Size(365, 26)
        Me.InformeToolStripMenuItem1.Text = "Informe"
        '
        'ListadoDeNotasDeCreditoToolStripMenuItem
        '
        Me.ListadoDeNotasDeCreditoToolStripMenuItem.Name = "ListadoDeNotasDeCreditoToolStripMenuItem"
        Me.ListadoDeNotasDeCreditoToolStripMenuItem.Size = New System.Drawing.Size(373, 26)
        Me.ListadoDeNotasDeCreditoToolStripMenuItem.Tag = "frmListadoNotaCredito"
        Me.ListadoDeNotasDeCreditoToolStripMenuItem.Text = "Listado de Notas de Credito"
        '
        'ToolStripMenuItem33
        '
        Me.ToolStripMenuItem33.Name = "ToolStripMenuItem33"
        Me.ToolStripMenuItem33.Size = New System.Drawing.Size(373, 26)
        Me.ToolStripMenuItem33.Tag = "frmListadoNotaCreditoDetalle"
        Me.ToolStripMenuItem33.Text = "Listado de Notas de Credito Detallado"
        '
        'ListadoDeAplicacionPendienteToolStripMenuItem
        '
        Me.ListadoDeAplicacionPendienteToolStripMenuItem.Name = "ListadoDeAplicacionPendienteToolStripMenuItem"
        Me.ListadoDeAplicacionPendienteToolStripMenuItem.Size = New System.Drawing.Size(373, 26)
        Me.ListadoDeAplicacionPendienteToolStripMenuItem.Tag = "frmListadoNotaCreditoAplicar"
        Me.ListadoDeAplicacionPendienteToolStripMenuItem.Text = "Listado de Aplicacion Pendiente"
        '
        'ListadoDeNCConVentasAplicadasToolStripMenuItem
        '
        Me.ListadoDeNCConVentasAplicadasToolStripMenuItem.Name = "ListadoDeNCConVentasAplicadasToolStripMenuItem"
        Me.ListadoDeNCConVentasAplicadasToolStripMenuItem.Size = New System.Drawing.Size(373, 26)
        Me.ListadoDeNCConVentasAplicadasToolStripMenuItem.Tag = "frmListadoNotaCreditoVentaAplicada"
        Me.ListadoDeNCConVentasAplicadasToolStripMenuItem.Text = "Listado de NC con Ventas Aplicadas"
        '
        'ToolStripSeparator53
        '
        Me.ToolStripSeparator53.Name = "ToolStripSeparator53"
        Me.ToolStripSeparator53.Size = New System.Drawing.Size(370, 6)
        '
        'ListadoDePedidoDeNotasDeCreditoToolStripMenuItem
        '
        Me.ListadoDePedidoDeNotasDeCreditoToolStripMenuItem.Name = "ListadoDePedidoDeNotasDeCreditoToolStripMenuItem"
        Me.ListadoDePedidoDeNotasDeCreditoToolStripMenuItem.Size = New System.Drawing.Size(373, 26)
        Me.ListadoDePedidoDeNotasDeCreditoToolStripMenuItem.Tag = "frmListadoPedidoNotaCredito"
        Me.ListadoDePedidoDeNotasDeCreditoToolStripMenuItem.Text = "Listado de Pedido de Notas de Credito"
        '
        'PanelDePedidosDeDevolucionesToolStripMenuItem
        '
        Me.PanelDePedidosDeDevolucionesToolStripMenuItem.Name = "PanelDePedidosDeDevolucionesToolStripMenuItem"
        Me.PanelDePedidosDeDevolucionesToolStripMenuItem.Size = New System.Drawing.Size(373, 26)
        Me.PanelDePedidosDeDevolucionesToolStripMenuItem.Text = "Panel de pedidos de devoluciones"
        '
        'NotaDeDebitoToolStripMenuItem
        '
        Me.NotaDeDebitoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem4, Me.AplicarToolStripMenuItem1, Me.ToolStripSeparator12, Me.ToolStripMenuItem5})
        Me.NotaDeDebitoToolStripMenuItem.Name = "NotaDeDebitoToolStripMenuItem"
        Me.NotaDeDebitoToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.NotaDeDebitoToolStripMenuItem.Text = "Nota de Debito"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(148, 26)
        Me.ToolStripMenuItem4.Tag = "frmNotaDebitoCliente"
        Me.ToolStripMenuItem4.Text = "Carga"
        '
        'AplicarToolStripMenuItem1
        '
        Me.AplicarToolStripMenuItem1.Name = "AplicarToolStripMenuItem1"
        Me.AplicarToolStripMenuItem1.Size = New System.Drawing.Size(148, 26)
        Me.AplicarToolStripMenuItem1.Tag = "frmAplicacionNotaDebitoCliente"
        Me.AplicarToolStripMenuItem1.Text = "Aplicar"
        '
        'ToolStripSeparator12
        '
        Me.ToolStripSeparator12.Name = "ToolStripSeparator12"
        Me.ToolStripSeparator12.Size = New System.Drawing.Size(145, 6)
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem30, Me.ToolStripMenuItem32, Me.DevolucionesPorProductoClienteToolStripMenuItem})
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(148, 26)
        Me.ToolStripMenuItem5.Text = "Informe"
        '
        'ToolStripMenuItem30
        '
        Me.ToolStripMenuItem30.Name = "ToolStripMenuItem30"
        Me.ToolStripMenuItem30.Size = New System.Drawing.Size(354, 26)
        Me.ToolStripMenuItem30.Tag = "frmListadoNotaCredito"
        Me.ToolStripMenuItem30.Text = "Listado de Notas de Debito"
        '
        'ToolStripMenuItem32
        '
        Me.ToolStripMenuItem32.Name = "ToolStripMenuItem32"
        Me.ToolStripMenuItem32.Size = New System.Drawing.Size(354, 26)
        Me.ToolStripMenuItem32.Tag = "frmListadoNotaCreditoAplicar"
        Me.ToolStripMenuItem32.Text = "Listado de Aplicacion Pendiente"
        '
        'DevolucionesPorProductoClienteToolStripMenuItem
        '
        Me.DevolucionesPorProductoClienteToolStripMenuItem.Name = "DevolucionesPorProductoClienteToolStripMenuItem"
        Me.DevolucionesPorProductoClienteToolStripMenuItem.Size = New System.Drawing.Size(354, 26)
        Me.DevolucionesPorProductoClienteToolStripMenuItem.Tag = "frmListadoNotaDebitoDetalle"
        Me.DevolucionesPorProductoClienteToolStripMenuItem.Text = "Devoluciones por Producto / Cliente"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(318, 6)
        '
        'LoteToolStripMenuItem
        '
        Me.LoteToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargarToolStripMenuItem, Me.DevolucionDeLoteToolStripMenuItem, Me.RendicionToolStripMenuItem1, Me.ToolStripSeparator9, Me.InformerToolStripMenuItem})
        Me.LoteToolStripMenuItem.Name = "LoteToolStripMenuItem"
        Me.LoteToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.LoteToolStripMenuItem.Text = "Orden de Carga"
        '
        'CargarToolStripMenuItem
        '
        Me.CargarToolStripMenuItem.Name = "CargarToolStripMenuItem"
        Me.CargarToolStripMenuItem.Size = New System.Drawing.Size(172, 26)
        Me.CargarToolStripMenuItem.Tag = "frmLoteDistribucion"
        Me.CargarToolStripMenuItem.Text = "Cargar"
        '
        'DevolucionDeLoteToolStripMenuItem
        '
        Me.DevolucionDeLoteToolStripMenuItem.Name = "DevolucionDeLoteToolStripMenuItem"
        Me.DevolucionDeLoteToolStripMenuItem.Size = New System.Drawing.Size(172, 26)
        Me.DevolucionDeLoteToolStripMenuItem.Tag = "frmDevolucionLote"
        Me.DevolucionDeLoteToolStripMenuItem.Text = "Devolucion"
        '
        'RendicionToolStripMenuItem1
        '
        Me.RendicionToolStripMenuItem1.Name = "RendicionToolStripMenuItem1"
        Me.RendicionToolStripMenuItem1.Size = New System.Drawing.Size(172, 26)
        Me.RendicionToolStripMenuItem1.Tag = "frmRendicionLote"
        Me.RendicionToolStripMenuItem1.Text = "Rendicion"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(169, 6)
        '
        'InformerToolStripMenuItem
        '
        Me.InformerToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ResumenDeLotesToolStripMenuItem, Me.HojaDeRutaToolStripMenuItem, Me.PlanillaDeCargaToolStripMenuItem, Me.DetalleDeDevolucionesToolStripMenuItem, Me.ComprobantesSinLotesToolStripMenuItem, Me.KilosTransportadosToolStripMenuItem})
        Me.InformerToolStripMenuItem.Name = "InformerToolStripMenuItem"
        Me.InformerToolStripMenuItem.Size = New System.Drawing.Size(172, 26)
        Me.InformerToolStripMenuItem.Text = "Informes"
        '
        'ResumenDeLotesToolStripMenuItem
        '
        Me.ResumenDeLotesToolStripMenuItem.Name = "ResumenDeLotesToolStripMenuItem"
        Me.ResumenDeLotesToolStripMenuItem.Size = New System.Drawing.Size(266, 26)
        Me.ResumenDeLotesToolStripMenuItem.Tag = "frmListadoLoteEmitido"
        Me.ResumenDeLotesToolStripMenuItem.Text = "Resumen de Lotes"
        '
        'HojaDeRutaToolStripMenuItem
        '
        Me.HojaDeRutaToolStripMenuItem.Name = "HojaDeRutaToolStripMenuItem"
        Me.HojaDeRutaToolStripMenuItem.Size = New System.Drawing.Size(266, 26)
        Me.HojaDeRutaToolStripMenuItem.Tag = "frmHojaRuta"
        Me.HojaDeRutaToolStripMenuItem.Text = "Hoja de Ruta"
        '
        'PlanillaDeCargaToolStripMenuItem
        '
        Me.PlanillaDeCargaToolStripMenuItem.Name = "PlanillaDeCargaToolStripMenuItem"
        Me.PlanillaDeCargaToolStripMenuItem.Size = New System.Drawing.Size(266, 26)
        Me.PlanillaDeCargaToolStripMenuItem.Tag = "frmCargaPlanilla"
        Me.PlanillaDeCargaToolStripMenuItem.Text = "Planilla de Carga"
        '
        'DetalleDeDevolucionesToolStripMenuItem
        '
        Me.DetalleDeDevolucionesToolStripMenuItem.Name = "DetalleDeDevolucionesToolStripMenuItem"
        Me.DetalleDeDevolucionesToolStripMenuItem.Size = New System.Drawing.Size(266, 26)
        Me.DetalleDeDevolucionesToolStripMenuItem.Tag = "frmListadoLoteDetalleDevolucion"
        Me.DetalleDeDevolucionesToolStripMenuItem.Text = "Detalle de Devoluciones"
        Me.DetalleDeDevolucionesToolStripMenuItem.ToolTipText = "Lista de productos devueltos relacionados con las Notas de Credito"
        '
        'ComprobantesSinLotesToolStripMenuItem
        '
        Me.ComprobantesSinLotesToolStripMenuItem.Name = "ComprobantesSinLotesToolStripMenuItem"
        Me.ComprobantesSinLotesToolStripMenuItem.Size = New System.Drawing.Size(266, 26)
        Me.ComprobantesSinLotesToolStripMenuItem.Tag = "frmListadoComprobanteSinLote"
        Me.ComprobantesSinLotesToolStripMenuItem.Text = "Comprobantes sin Lote"
        '
        'KilosTransportadosToolStripMenuItem
        '
        Me.KilosTransportadosToolStripMenuItem.Name = "KilosTransportadosToolStripMenuItem"
        Me.KilosTransportadosToolStripMenuItem.Size = New System.Drawing.Size(266, 26)
        Me.KilosTransportadosToolStripMenuItem.Tag = "frmInformeKilosTransportados"
        Me.KilosTransportadosToolStripMenuItem.Text = "Kilos Transportados"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargaToolStripMenuItem4, Me.InformeToolStripMenuItem4})
        Me.ToolStripMenuItem3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(321, 26)
        Me.ToolStripMenuItem3.Tag = ""
        Me.ToolStripMenuItem3.Text = "Remision"
        '
        'CargaToolStripMenuItem4
        '
        Me.CargaToolStripMenuItem4.Name = "CargaToolStripMenuItem4"
        Me.CargaToolStripMenuItem4.Size = New System.Drawing.Size(148, 26)
        Me.CargaToolStripMenuItem4.Tag = "frmRemision"
        Me.CargaToolStripMenuItem4.Text = "Carga"
        '
        'InformeToolStripMenuItem4
        '
        Me.InformeToolStripMenuItem4.Name = "InformeToolStripMenuItem4"
        Me.InformeToolStripMenuItem4.Size = New System.Drawing.Size(148, 26)
        Me.InformeToolStripMenuItem4.Tag = "frmListadoRemision"
        Me.InformeToolStripMenuItem4.Text = "Informe"
        '
        'TeleventasToolStripMenuItem
        '
        Me.TeleventasToolStripMenuItem.Name = "TeleventasToolStripMenuItem"
        Me.TeleventasToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.TeleventasToolStripMenuItem.Tag = "FrmTeleventas"
        Me.TeleventasToolStripMenuItem.Text = "Televentas"
        '
        'InformeTeleventaToolStripMenuItem
        '
        Me.InformeTeleventaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PlanDeLlamadasToolStripMenuItem})
        Me.InformeTeleventaToolStripMenuItem.Name = "InformeTeleventaToolStripMenuItem"
        Me.InformeTeleventaToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.InformeTeleventaToolStripMenuItem.Text = "Informe Televenta"
        '
        'PlanDeLlamadasToolStripMenuItem
        '
        Me.PlanDeLlamadasToolStripMenuItem.Name = "PlanDeLlamadasToolStripMenuItem"
        Me.PlanDeLlamadasToolStripMenuItem.Size = New System.Drawing.Size(215, 26)
        Me.PlanDeLlamadasToolStripMenuItem.Tag = "frmPlanDeLlamadas"
        Me.PlanDeLlamadasToolStripMenuItem.Text = "Plan de llamadas"
        '
        'TransporteToolStripMenuItem
        '
        Me.TransporteToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargaDeCamionesToolStripMenuItem, Me.CargaDeCamionesPedidoDeClientesToolStripMenuItem, Me.InformeToolStripMenuItem6})
        Me.TransporteToolStripMenuItem.Name = "TransporteToolStripMenuItem"
        Me.TransporteToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.TransporteToolStripMenuItem.Text = "Transporte"
        '
        'CargaDeCamionesToolStripMenuItem
        '
        Me.CargaDeCamionesToolStripMenuItem.Name = "CargaDeCamionesToolStripMenuItem"
        Me.CargaDeCamionesToolStripMenuItem.Size = New System.Drawing.Size(377, 26)
        Me.CargaDeCamionesToolStripMenuItem.Tag = "frmAsignacionCamion"
        Me.CargaDeCamionesToolStripMenuItem.Text = "Carga de Camiones Abastecimiento"
        '
        'CargaDeCamionesPedidoDeClientesToolStripMenuItem
        '
        Me.CargaDeCamionesPedidoDeClientesToolStripMenuItem.Name = "CargaDeCamionesPedidoDeClientesToolStripMenuItem"
        Me.CargaDeCamionesPedidoDeClientesToolStripMenuItem.Size = New System.Drawing.Size(377, 26)
        Me.CargaDeCamionesPedidoDeClientesToolStripMenuItem.Tag = "frmAsignacionCamionPedidoCliente"
        Me.CargaDeCamionesPedidoDeClientesToolStripMenuItem.Text = "Carga de Camiones Pedido de clientes"
        '
        'InformeToolStripMenuItem6
        '
        Me.InformeToolStripMenuItem6.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AbastecimientoPendienteToolStripMenuItem, Me.PedClientesPendientesToolStripMenuItem, Me.CargaParaAbastecimientoToolStripMenuItem})
        Me.InformeToolStripMenuItem6.Name = "InformeToolStripMenuItem6"
        Me.InformeToolStripMenuItem6.Size = New System.Drawing.Size(377, 26)
        Me.InformeToolStripMenuItem6.Text = "Informe"
        '
        'AbastecimientoPendienteToolStripMenuItem
        '
        Me.AbastecimientoPendienteToolStripMenuItem.Name = "AbastecimientoPendienteToolStripMenuItem"
        Me.AbastecimientoPendienteToolStripMenuItem.Size = New System.Drawing.Size(325, 26)
        Me.AbastecimientoPendienteToolStripMenuItem.Tag = "frmListadoAbastecimientoCamion"
        Me.AbastecimientoPendienteToolStripMenuItem.Text = "Abastecimientos Pendientes"
        '
        'PedClientesPendientesToolStripMenuItem
        '
        Me.PedClientesPendientesToolStripMenuItem.Name = "PedClientesPendientesToolStripMenuItem"
        Me.PedClientesPendientesToolStripMenuItem.Size = New System.Drawing.Size(325, 26)
        Me.PedClientesPendientesToolStripMenuItem.Tag = "frmPedidoClientes"
        Me.PedClientesPendientesToolStripMenuItem.Text = "Pedidos de Clientes Pendientes"
        '
        'CargaParaAbastecimientoToolStripMenuItem
        '
        Me.CargaParaAbastecimientoToolStripMenuItem.Name = "CargaParaAbastecimientoToolStripMenuItem"
        Me.CargaParaAbastecimientoToolStripMenuItem.Size = New System.Drawing.Size(325, 26)
        Me.CargaParaAbastecimientoToolStripMenuItem.Tag = "frmListadoAbastecimientoSucursalDetallado"
        Me.CargaParaAbastecimientoToolStripMenuItem.Text = "Abastecimiento - Pedido Cliente"
        '
        'InformesGerencialesToolStripMenuItem
        '
        Me.InformesGerencialesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TotalGeneralToolStripMenuItem})
        Me.InformesGerencialesToolStripMenuItem.Name = "InformesGerencialesToolStripMenuItem"
        Me.InformesGerencialesToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.InformesGerencialesToolStripMenuItem.Text = "Informes Gerenciales"
        '
        'TotalGeneralToolStripMenuItem
        '
        Me.TotalGeneralToolStripMenuItem.Name = "TotalGeneralToolStripMenuItem"
        Me.TotalGeneralToolStripMenuItem.Size = New System.Drawing.Size(186, 26)
        Me.TotalGeneralToolStripMenuItem.Tag = "frmTotalGeneral"
        Me.TotalGeneralToolStripMenuItem.Text = "Total General"
        '
        'ReenvioComprobantesToolStripMenuItem
        '
        Me.ReenvioComprobantesToolStripMenuItem.Name = "ReenvioComprobantesToolStripMenuItem"
        Me.ReenvioComprobantesToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.ReenvioComprobantesToolStripMenuItem.Tag = "frmReenvioComprobantesElectronicos"
        Me.ReenvioComprobantesToolStripMenuItem.Text = "Re-envio de Comprobantes"
        '
        'AcuerdoDeClientesToolStripMenuItem
        '
        Me.AcuerdoDeClientesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargaDeAcuerdosToolStripMenuItem, Me.ListadoDeAcuerdosDeClientesToolStripMenuItem})
        Me.AcuerdoDeClientesToolStripMenuItem.Name = "AcuerdoDeClientesToolStripMenuItem"
        Me.AcuerdoDeClientesToolStripMenuItem.Size = New System.Drawing.Size(321, 26)
        Me.AcuerdoDeClientesToolStripMenuItem.Tag = "frmAcuerdodeClientes"
        Me.AcuerdoDeClientesToolStripMenuItem.Text = "Acuerdo de Clientes"
        '
        'CargaDeAcuerdosToolStripMenuItem
        '
        Me.CargaDeAcuerdosToolStripMenuItem.Name = "CargaDeAcuerdosToolStripMenuItem"
        Me.CargaDeAcuerdosToolStripMenuItem.Size = New System.Drawing.Size(327, 26)
        Me.CargaDeAcuerdosToolStripMenuItem.Text = "Carga de Acuerdos"
        '
        'ListadoDeAcuerdosDeClientesToolStripMenuItem
        '
        Me.ListadoDeAcuerdosDeClientesToolStripMenuItem.Name = "ListadoDeAcuerdosDeClientesToolStripMenuItem"
        Me.ListadoDeAcuerdosDeClientesToolStripMenuItem.Size = New System.Drawing.Size(327, 26)
        Me.ListadoDeAcuerdosDeClientesToolStripMenuItem.Tag = "frmListadoAcuerdodeClientes"
        Me.ListadoDeAcuerdosDeClientesToolStripMenuItem.Text = "Listado de Acuerdos de Clientes"
        '
        'ComprasToolStripMenuItem
        '
        Me.ComprasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EntradaPorComprasToolStripMenuItem, Me.ToolStripMenuItem19, Me.ToolStripSeparator13, Me.ToolStripMenuItem13, Me.ToolStripMenuItem16, Me.ToolStripSeparator24, Me.ActualizarTimbradoToolStripMenuItem})
        Me.ComprasToolStripMenuItem.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComprasToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ComprasToolStripMenuItem.Name = "ComprasToolStripMenuItem"
        Me.ComprasToolStripMenuItem.Size = New System.Drawing.Size(89, 24)
        Me.ComprasToolStripMenuItem.Text = "Compras"
        '
        'EntradaPorComprasToolStripMenuItem
        '
        Me.EntradaPorComprasToolStripMenuItem.Name = "EntradaPorComprasToolStripMenuItem"
        Me.EntradaPorComprasToolStripMenuItem.Size = New System.Drawing.Size(249, 26)
        Me.EntradaPorComprasToolStripMenuItem.Tag = "frmCompra"
        Me.EntradaPorComprasToolStripMenuItem.Text = "Entrada por Compras"
        '
        'ToolStripMenuItem19
        '
        Me.ToolStripMenuItem19.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListadoGeneralDeComprobantesToolStripMenuItem})
        Me.ToolStripMenuItem19.Name = "ToolStripMenuItem19"
        Me.ToolStripMenuItem19.Size = New System.Drawing.Size(249, 26)
        Me.ToolStripMenuItem19.Text = "Informes"
        '
        'ListadoGeneralDeComprobantesToolStripMenuItem
        '
        Me.ListadoGeneralDeComprobantesToolStripMenuItem.Name = "ListadoGeneralDeComprobantesToolStripMenuItem"
        Me.ListadoGeneralDeComprobantesToolStripMenuItem.Size = New System.Drawing.Size(278, 26)
        Me.ListadoGeneralDeComprobantesToolStripMenuItem.Tag = "frmListadoFacturaCompraEmitida"
        Me.ListadoGeneralDeComprobantesToolStripMenuItem.Text = "Listado de Comprobantes"
        '
        'ToolStripSeparator13
        '
        Me.ToolStripSeparator13.Name = "ToolStripSeparator13"
        Me.ToolStripSeparator13.Size = New System.Drawing.Size(246, 6)
        '
        'ToolStripMenuItem13
        '
        Me.ToolStripMenuItem13.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem14, Me.ToolStripSeparator14, Me.ToolStripMenuItem15})
        Me.ToolStripMenuItem13.Name = "ToolStripMenuItem13"
        Me.ToolStripMenuItem13.Size = New System.Drawing.Size(249, 26)
        Me.ToolStripMenuItem13.Text = "Nota de Credito"
        '
        'ToolStripMenuItem14
        '
        Me.ToolStripMenuItem14.Name = "ToolStripMenuItem14"
        Me.ToolStripMenuItem14.Size = New System.Drawing.Size(148, 26)
        Me.ToolStripMenuItem14.Tag = "frmNotaCreditoProveedor"
        Me.ToolStripMenuItem14.Text = "Carga"
        '
        'ToolStripSeparator14
        '
        Me.ToolStripSeparator14.Name = "ToolStripSeparator14"
        Me.ToolStripSeparator14.Size = New System.Drawing.Size(145, 6)
        '
        'ToolStripMenuItem15
        '
        Me.ToolStripMenuItem15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ToolStripMenuItem15.Name = "ToolStripMenuItem15"
        Me.ToolStripMenuItem15.Size = New System.Drawing.Size(148, 26)
        Me.ToolStripMenuItem15.Tag = "frmListarNotaCreditoProveedor"
        Me.ToolStripMenuItem15.Text = "Informe"
        '
        'ToolStripMenuItem16
        '
        Me.ToolStripMenuItem16.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem17, Me.ToolStripSeparator16, Me.ToolStripMenuItem18})
        Me.ToolStripMenuItem16.Name = "ToolStripMenuItem16"
        Me.ToolStripMenuItem16.Size = New System.Drawing.Size(249, 26)
        Me.ToolStripMenuItem16.Text = "Nota de Debito"
        '
        'ToolStripMenuItem17
        '
        Me.ToolStripMenuItem17.Name = "ToolStripMenuItem17"
        Me.ToolStripMenuItem17.Size = New System.Drawing.Size(148, 26)
        Me.ToolStripMenuItem17.Tag = "frmNotaDebitoProveedor"
        Me.ToolStripMenuItem17.Text = "Carga"
        '
        'ToolStripSeparator16
        '
        Me.ToolStripSeparator16.Name = "ToolStripSeparator16"
        Me.ToolStripSeparator16.Size = New System.Drawing.Size(145, 6)
        '
        'ToolStripMenuItem18
        '
        Me.ToolStripMenuItem18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ToolStripMenuItem18.Name = "ToolStripMenuItem18"
        Me.ToolStripMenuItem18.Size = New System.Drawing.Size(148, 26)
        Me.ToolStripMenuItem18.Tag = "frmListarNotaDebitoProveedor"
        Me.ToolStripMenuItem18.Text = "Informe"
        '
        'ToolStripSeparator24
        '
        Me.ToolStripSeparator24.Name = "ToolStripSeparator24"
        Me.ToolStripSeparator24.Size = New System.Drawing.Size(246, 6)
        '
        'ActualizarTimbradoToolStripMenuItem
        '
        Me.ActualizarTimbradoToolStripMenuItem.Name = "ActualizarTimbradoToolStripMenuItem"
        Me.ActualizarTimbradoToolStripMenuItem.Size = New System.Drawing.Size(249, 26)
        Me.ActualizarTimbradoToolStripMenuItem.Tag = "frmActualizarTimbrado"
        Me.ActualizarTimbradoToolStripMenuItem.Text = "Actualizar timbrado"
        '
        'StockToolStripMenuItem
        '
        Me.StockToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MovimientosToolStripMenuItem, Me.CargaDeMercaderiasToolStripMenuItem, Me.ControlDeStockToolStripMenuItem, Me.InventarioInicialToolStripMenuItem, Me.ToolStripSeparator29, Me.ToolStripMenuItem36, Me.ToolStripSeparator47, Me.ToolStripMenuItem23, Me.TrigoToolStripMenuItem, Me.OrdenDePedidoToolStripMenuItem, Me.CostoDeProductosToolStripMenuItem})
        Me.StockToolStripMenuItem.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StockToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.StockToolStripMenuItem.Name = "StockToolStripMenuItem"
        Me.StockToolStripMenuItem.Size = New System.Drawing.Size(64, 24)
        Me.StockToolStripMenuItem.Text = "Stock"
        '
        'MovimientosToolStripMenuItem
        '
        Me.MovimientosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargaToolStripMenuItem6, Me.DescargaDeStockToolStripMenuItem, Me.ConsumoCombustibleToolStripMenuItem, Me.ConsumoMateriaPrimaBalanceadoToolStripMenuItem, Me.DescagaDeComprasToolStripMenuItem, Me.ConsumoInformaticaToolStripMenuItem, Me.DevolucionesDeClientesToolStripMenuItem, Me.ModificarToolStripMenuItem1, Me.ToolStripSeparator17, Me.InformeToolStripMenuItem10, Me.ListadoDeProductoPorOperacionesToolStripMenuItem, Me.ListadoDeMovimientosDeCombustiblesToolStripMenuItem, Me.ListadoDeDevolucionesSinNCToolStripMenuItem})
        Me.MovimientosToolStripMenuItem.Name = "MovimientosToolStripMenuItem"
        Me.MovimientosToolStripMenuItem.Size = New System.Drawing.Size(243, 26)
        Me.MovimientosToolStripMenuItem.Text = "Movimientos"
        '
        'CargaToolStripMenuItem6
        '
        Me.CargaToolStripMenuItem6.Name = "CargaToolStripMenuItem6"
        Me.CargaToolStripMenuItem6.Size = New System.Drawing.Size(386, 26)
        Me.CargaToolStripMenuItem6.Tag = "frmMovimientoStock"
        Me.CargaToolStripMenuItem6.Text = "Carga de Comprobantes"
        '
        'DescargaDeStockToolStripMenuItem
        '
        Me.DescargaDeStockToolStripMenuItem.Name = "DescargaDeStockToolStripMenuItem"
        Me.DescargaDeStockToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.DescargaDeStockToolStripMenuItem.Tag = "frmDescargaStock"
        Me.DescargaDeStockToolStripMenuItem.Text = "Descarga de Stock"
        '
        'ConsumoCombustibleToolStripMenuItem
        '
        Me.ConsumoCombustibleToolStripMenuItem.Name = "ConsumoCombustibleToolStripMenuItem"
        Me.ConsumoCombustibleToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ConsumoCombustibleToolStripMenuItem.Tag = "frmConsumoCombustible"
        Me.ConsumoCombustibleToolStripMenuItem.Text = "Consumo Logistica"
        '
        'ConsumoMateriaPrimaBalanceadoToolStripMenuItem
        '
        Me.ConsumoMateriaPrimaBalanceadoToolStripMenuItem.Name = "ConsumoMateriaPrimaBalanceadoToolStripMenuItem"
        Me.ConsumoMateriaPrimaBalanceadoToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ConsumoMateriaPrimaBalanceadoToolStripMenuItem.Tag = "frmMovimientoMateriaPrima"
        Me.ConsumoMateriaPrimaBalanceadoToolStripMenuItem.Text = "Consumo Materia Prima"
        '
        'DescagaDeComprasToolStripMenuItem
        '
        Me.DescagaDeComprasToolStripMenuItem.Name = "DescagaDeComprasToolStripMenuItem"
        Me.DescagaDeComprasToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.DescagaDeComprasToolStripMenuItem.Tag = "frmMovimientoDescargaCompra"
        Me.DescagaDeComprasToolStripMenuItem.Text = "Descarga de Compras"
        '
        'ConsumoInformaticaToolStripMenuItem
        '
        Me.ConsumoInformaticaToolStripMenuItem.Name = "ConsumoInformaticaToolStripMenuItem"
        Me.ConsumoInformaticaToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ConsumoInformaticaToolStripMenuItem.Tag = "frmConsumoInformatica"
        Me.ConsumoInformaticaToolStripMenuItem.Text = "Consumo Informatica"
        '
        'DevolucionesDeClientesToolStripMenuItem
        '
        Me.DevolucionesDeClientesToolStripMenuItem.Name = "DevolucionesDeClientesToolStripMenuItem"
        Me.DevolucionesDeClientesToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.DevolucionesDeClientesToolStripMenuItem.Tag = "frmDevolucionSinNotaCredido"
        Me.DevolucionesDeClientesToolStripMenuItem.Text = "Devoluciones de Clientes"
        '
        'ModificarToolStripMenuItem1
        '
        Me.ModificarToolStripMenuItem1.Name = "ModificarToolStripMenuItem1"
        Me.ModificarToolStripMenuItem1.Size = New System.Drawing.Size(386, 26)
        Me.ModificarToolStripMenuItem1.Tag = "frmMovimientoModificar"
        Me.ModificarToolStripMenuItem1.Text = "Modificar"
        '
        'ToolStripSeparator17
        '
        Me.ToolStripSeparator17.Name = "ToolStripSeparator17"
        Me.ToolStripSeparator17.Size = New System.Drawing.Size(383, 6)
        '
        'InformeToolStripMenuItem10
        '
        Me.InformeToolStripMenuItem10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.InformeToolStripMenuItem10.Name = "InformeToolStripMenuItem10"
        Me.InformeToolStripMenuItem10.Size = New System.Drawing.Size(386, 26)
        Me.InformeToolStripMenuItem10.Tag = "frmListadoMovimientoProducto"
        Me.InformeToolStripMenuItem10.Text = "Listado de Comprobantes"
        '
        'ListadoDeProductoPorOperacionesToolStripMenuItem
        '
        Me.ListadoDeProductoPorOperacionesToolStripMenuItem.Name = "ListadoDeProductoPorOperacionesToolStripMenuItem"
        Me.ListadoDeProductoPorOperacionesToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ListadoDeProductoPorOperacionesToolStripMenuItem.Tag = "frmListadoOperacionProducto"
        Me.ListadoDeProductoPorOperacionesToolStripMenuItem.Text = "Listado de productos por operaciones"
        '
        'ListadoDeMovimientosDeCombustiblesToolStripMenuItem
        '
        Me.ListadoDeMovimientosDeCombustiblesToolStripMenuItem.Name = "ListadoDeMovimientosDeCombustiblesToolStripMenuItem"
        Me.ListadoDeMovimientosDeCombustiblesToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ListadoDeMovimientosDeCombustiblesToolStripMenuItem.Tag = "frmListadoMovimientoCombustible"
        Me.ListadoDeMovimientosDeCombustiblesToolStripMenuItem.Text = "Listado de Movimientos de Combustibles"
        '
        'ListadoDeDevolucionesSinNCToolStripMenuItem
        '
        Me.ListadoDeDevolucionesSinNCToolStripMenuItem.Name = "ListadoDeDevolucionesSinNCToolStripMenuItem"
        Me.ListadoDeDevolucionesSinNCToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ListadoDeDevolucionesSinNCToolStripMenuItem.Tag = "frmListadoDevolucionesSinNC"
        Me.ListadoDeDevolucionesSinNCToolStripMenuItem.Text = "Listado de Devoluciones sin NC"
        '
        'CargaDeMercaderiasToolStripMenuItem
        '
        Me.CargaDeMercaderiasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargaToolStripMenuItem7, Me.ToolStripSeparator30, Me.InformeToolStripMenuItem11})
        Me.CargaDeMercaderiasToolStripMenuItem.Name = "CargaDeMercaderiasToolStripMenuItem"
        Me.CargaDeMercaderiasToolStripMenuItem.Size = New System.Drawing.Size(243, 26)
        Me.CargaDeMercaderiasToolStripMenuItem.Text = "Picking"
        Me.CargaDeMercaderiasToolStripMenuItem.Visible = False
        '
        'CargaToolStripMenuItem7
        '
        Me.CargaToolStripMenuItem7.Name = "CargaToolStripMenuItem7"
        Me.CargaToolStripMenuItem7.Size = New System.Drawing.Size(148, 26)
        Me.CargaToolStripMenuItem7.Tag = "frmCargaMercaderia"
        Me.CargaToolStripMenuItem7.Text = "Carga"
        '
        'ToolStripSeparator30
        '
        Me.ToolStripSeparator30.Name = "ToolStripSeparator30"
        Me.ToolStripSeparator30.Size = New System.Drawing.Size(145, 6)
        '
        'InformeToolStripMenuItem11
        '
        Me.InformeToolStripMenuItem11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.InformeToolStripMenuItem11.Name = "InformeToolStripMenuItem11"
        Me.InformeToolStripMenuItem11.Size = New System.Drawing.Size(148, 26)
        Me.InformeToolStripMenuItem11.Tag = "frmListadoCargaMercaderia"
        Me.InformeToolStripMenuItem11.Text = "Informe"
        '
        'ControlDeStockToolStripMenuItem
        '
        Me.ControlDeStockToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EquiposDeConteoToolStripMenuItem, Me.VeremosToolStripMenuItem, Me.TomaDeInventarioFisicoToolStripMenuItem, Me.ComprarativoEntreEquiposToolStripMenuItem, Me.ComprarativoToolStripMenuItem, Me.AjusteToolStripMenuItem, Me.ToolStripSeparator32, Me.ToolStripMenuItem28})
        Me.ControlDeStockToolStripMenuItem.Name = "ControlDeStockToolStripMenuItem"
        Me.ControlDeStockToolStripMenuItem.Size = New System.Drawing.Size(243, 26)
        Me.ControlDeStockToolStripMenuItem.Text = "Control de Inventario"
        '
        'EquiposDeConteoToolStripMenuItem
        '
        Me.EquiposDeConteoToolStripMenuItem.Name = "EquiposDeConteoToolStripMenuItem"
        Me.EquiposDeConteoToolStripMenuItem.Size = New System.Drawing.Size(311, 26)
        Me.EquiposDeConteoToolStripMenuItem.Tag = "frmEquipoConteo"
        Me.EquiposDeConteoToolStripMenuItem.Text = "Equipos de Conteo"
        '
        'VeremosToolStripMenuItem
        '
        Me.VeremosToolStripMenuItem.Name = "VeremosToolStripMenuItem"
        Me.VeremosToolStripMenuItem.Size = New System.Drawing.Size(311, 26)
        Me.VeremosToolStripMenuItem.Tag = "frmPlanillaInventario"
        Me.VeremosToolStripMenuItem.Text = "Planilla de Inventario"
        '
        'TomaDeInventarioFisicoToolStripMenuItem
        '
        Me.TomaDeInventarioFisicoToolStripMenuItem.Name = "TomaDeInventarioFisicoToolStripMenuItem"
        Me.TomaDeInventarioFisicoToolStripMenuItem.Size = New System.Drawing.Size(311, 26)
        Me.TomaDeInventarioFisicoToolStripMenuItem.Tag = "frmTomaInventario"
        Me.TomaDeInventarioFisicoToolStripMenuItem.Text = "Toma de Inventario Fisico"
        '
        'ComprarativoEntreEquiposToolStripMenuItem
        '
        Me.ComprarativoEntreEquiposToolStripMenuItem.Name = "ComprarativoEntreEquiposToolStripMenuItem"
        Me.ComprarativoEntreEquiposToolStripMenuItem.Size = New System.Drawing.Size(311, 26)
        Me.ComprarativoEntreEquiposToolStripMenuItem.Tag = "frmComparativoInventarioEquipo"
        Me.ComprarativoEntreEquiposToolStripMenuItem.Text = "Comprarativo entre Equipos"
        '
        'ComprarativoToolStripMenuItem
        '
        Me.ComprarativoToolStripMenuItem.Name = "ComprarativoToolStripMenuItem"
        Me.ComprarativoToolStripMenuItem.Size = New System.Drawing.Size(311, 26)
        Me.ComprarativoToolStripMenuItem.Tag = "frmComparativoInventarioSistema"
        Me.ComprarativoToolStripMenuItem.Text = "Comprarativo Fisico / Sistema"
        '
        'AjusteToolStripMenuItem
        '
        Me.AjusteToolStripMenuItem.Name = "AjusteToolStripMenuItem"
        Me.AjusteToolStripMenuItem.Size = New System.Drawing.Size(311, 26)
        Me.AjusteToolStripMenuItem.Tag = "frmAjusteControlInventario"
        Me.AjusteToolStripMenuItem.Text = "Ajuste"
        Me.AjusteToolStripMenuItem.Visible = False
        '
        'ToolStripSeparator32
        '
        Me.ToolStripSeparator32.Name = "ToolStripSeparator32"
        Me.ToolStripSeparator32.Size = New System.Drawing.Size(308, 6)
        '
        'ToolStripMenuItem28
        '
        Me.ToolStripMenuItem28.ForeColor = System.Drawing.SystemColors.GrayText
        Me.ToolStripMenuItem28.Name = "ToolStripMenuItem28"
        Me.ToolStripMenuItem28.Size = New System.Drawing.Size(311, 26)
        Me.ToolStripMenuItem28.Text = "Informe"
        Me.ToolStripMenuItem28.Visible = False
        '
        'InventarioInicialToolStripMenuItem
        '
        Me.InventarioInicialToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem26, Me.ToolStripSeparator31, Me.ToolStripMenuItem27})
        Me.InventarioInicialToolStripMenuItem.Name = "InventarioInicialToolStripMenuItem"
        Me.InventarioInicialToolStripMenuItem.Size = New System.Drawing.Size(243, 26)
        Me.InventarioInicialToolStripMenuItem.Text = "Inventario Inicial"
        Me.InventarioInicialToolStripMenuItem.Visible = False
        '
        'ToolStripMenuItem26
        '
        Me.ToolStripMenuItem26.Name = "ToolStripMenuItem26"
        Me.ToolStripMenuItem26.Size = New System.Drawing.Size(148, 26)
        Me.ToolStripMenuItem26.Tag = "frmInventarioInicial"
        Me.ToolStripMenuItem26.Text = "Carga"
        '
        'ToolStripSeparator31
        '
        Me.ToolStripSeparator31.Name = "ToolStripSeparator31"
        Me.ToolStripSeparator31.Size = New System.Drawing.Size(145, 6)
        '
        'ToolStripMenuItem27
        '
        Me.ToolStripMenuItem27.ForeColor = System.Drawing.SystemColors.GrayText
        Me.ToolStripMenuItem27.Name = "ToolStripMenuItem27"
        Me.ToolStripMenuItem27.Size = New System.Drawing.Size(148, 26)
        Me.ToolStripMenuItem27.Text = "Informe"
        '
        'ToolStripSeparator29
        '
        Me.ToolStripSeparator29.Name = "ToolStripSeparator29"
        Me.ToolStripSeparator29.Size = New System.Drawing.Size(240, 6)
        '
        'ToolStripMenuItem36
        '
        Me.ToolStripMenuItem36.Name = "ToolStripMenuItem36"
        Me.ToolStripMenuItem36.Size = New System.Drawing.Size(243, 26)
        Me.ToolStripMenuItem36.Tag = "frmControlExistencia"
        Me.ToolStripMenuItem36.Text = "Recalcular Stock"
        '
        'ToolStripSeparator47
        '
        Me.ToolStripSeparator47.Name = "ToolStripSeparator47"
        Me.ToolStripSeparator47.Size = New System.Drawing.Size(240, 6)
        '
        'ToolStripMenuItem23
        '
        Me.ToolStripMenuItem23.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PlanillaParaTomaDeInventarioToolStripMenuItem, Me.MovimientosPorProductToolStripMenuItem, Me.ProductosAReponerToolStripMenuItem, Me.ResumenDeExistenciaToolStripMenuItem, Me.ExistenciaValorizadaToolStripMenuItem, Me.KardexResumenToolStripMenuItem, Me.KardexDetalladoPorProductoToolStripMenuItem, Me.ComparativoCostoKardexToolStripMenuItem})
        Me.ToolStripMenuItem23.Name = "ToolStripMenuItem23"
        Me.ToolStripMenuItem23.Size = New System.Drawing.Size(243, 26)
        Me.ToolStripMenuItem23.Text = "Informes"
        '
        'PlanillaParaTomaDeInventarioToolStripMenuItem
        '
        Me.PlanillaParaTomaDeInventarioToolStripMenuItem.Name = "PlanillaParaTomaDeInventarioToolStripMenuItem"
        Me.PlanillaParaTomaDeInventarioToolStripMenuItem.Size = New System.Drawing.Size(319, 26)
        Me.PlanillaParaTomaDeInventarioToolStripMenuItem.Tag = "frmInformeTomaInventario"
        Me.PlanillaParaTomaDeInventarioToolStripMenuItem.Text = "Planilla para toma de inventario"
        '
        'MovimientosPorProductToolStripMenuItem
        '
        Me.MovimientosPorProductToolStripMenuItem.Name = "MovimientosPorProductToolStripMenuItem"
        Me.MovimientosPorProductToolStripMenuItem.Size = New System.Drawing.Size(319, 26)
        Me.MovimientosPorProductToolStripMenuItem.Tag = "frmMovimientoProducto"
        Me.MovimientosPorProductToolStripMenuItem.Text = "Extracto de Movimientos"
        '
        'ProductosAReponerToolStripMenuItem
        '
        Me.ProductosAReponerToolStripMenuItem.Name = "ProductosAReponerToolStripMenuItem"
        Me.ProductosAReponerToolStripMenuItem.Size = New System.Drawing.Size(319, 26)
        Me.ProductosAReponerToolStripMenuItem.Tag = "frmExistenciaMinimaSobreMaxima"
        Me.ProductosAReponerToolStripMenuItem.Text = "Productos a Reponer"
        '
        'ResumenDeExistenciaToolStripMenuItem
        '
        Me.ResumenDeExistenciaToolStripMenuItem.Name = "ResumenDeExistenciaToolStripMenuItem"
        Me.ResumenDeExistenciaToolStripMenuItem.Size = New System.Drawing.Size(319, 26)
        Me.ResumenDeExistenciaToolStripMenuItem.Tag = "frmExistenciaMovimientoCalculado"
        Me.ResumenDeExistenciaToolStripMenuItem.Text = "Resumen de Existencia"
        '
        'ExistenciaValorizadaToolStripMenuItem
        '
        Me.ExistenciaValorizadaToolStripMenuItem.Name = "ExistenciaValorizadaToolStripMenuItem"
        Me.ExistenciaValorizadaToolStripMenuItem.Size = New System.Drawing.Size(319, 26)
        Me.ExistenciaValorizadaToolStripMenuItem.Tag = "frmExistenciaValorizada"
        Me.ExistenciaValorizadaToolStripMenuItem.Text = "Existencia Valorizada"
        '
        'KardexResumenToolStripMenuItem
        '
        Me.KardexResumenToolStripMenuItem.Name = "KardexResumenToolStripMenuItem"
        Me.KardexResumenToolStripMenuItem.Size = New System.Drawing.Size(319, 26)
        Me.KardexResumenToolStripMenuItem.Tag = "frmKardexProductoResumen"
        Me.KardexResumenToolStripMenuItem.Text = "Kardex Resumen"
        '
        'KardexDetalladoPorProductoToolStripMenuItem
        '
        Me.KardexDetalladoPorProductoToolStripMenuItem.Name = "KardexDetalladoPorProductoToolStripMenuItem"
        Me.KardexDetalladoPorProductoToolStripMenuItem.Size = New System.Drawing.Size(319, 26)
        Me.KardexDetalladoPorProductoToolStripMenuItem.Tag = "frmKardexProductoDetalle"
        Me.KardexDetalladoPorProductoToolStripMenuItem.Text = "Kardex Detallado por Producto"
        '
        'ComparativoCostoKardexToolStripMenuItem
        '
        Me.ComparativoCostoKardexToolStripMenuItem.Name = "ComparativoCostoKardexToolStripMenuItem"
        Me.ComparativoCostoKardexToolStripMenuItem.Size = New System.Drawing.Size(319, 26)
        Me.ComparativoCostoKardexToolStripMenuItem.Text = "Comparativo Costo Kardex"
        '
        'TrigoToolStripMenuItem
        '
        Me.TrigoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AcuerdoToolStripMenuItem, Me.TicketDeBasculaToolStripMenuItem, Me.AsignarAcuerdoATicketToolStripMenuItem, Me.MacheoFacturaTicketToolStripMenuItem, Me.ToolStripSeparator28, Me.InformesToolStripMenuItem2})
        Me.TrigoToolStripMenuItem.Name = "TrigoToolStripMenuItem"
        Me.TrigoToolStripMenuItem.Size = New System.Drawing.Size(243, 26)
        Me.TrigoToolStripMenuItem.Text = "Materia Prima"
        '
        'AcuerdoToolStripMenuItem
        '
        Me.AcuerdoToolStripMenuItem.Name = "AcuerdoToolStripMenuItem"
        Me.AcuerdoToolStripMenuItem.Size = New System.Drawing.Size(275, 26)
        Me.AcuerdoToolStripMenuItem.Tag = "frmAcuerdo"
        Me.AcuerdoToolStripMenuItem.Text = "Acuerdo"
        '
        'TicketDeBasculaToolStripMenuItem
        '
        Me.TicketDeBasculaToolStripMenuItem.Name = "TicketDeBasculaToolStripMenuItem"
        Me.TicketDeBasculaToolStripMenuItem.Size = New System.Drawing.Size(275, 26)
        Me.TicketDeBasculaToolStripMenuItem.Tag = "frmTicketBascula"
        Me.TicketDeBasculaToolStripMenuItem.Text = "Ticket de Bascula"
        '
        'AsignarAcuerdoATicketToolStripMenuItem
        '
        Me.AsignarAcuerdoATicketToolStripMenuItem.Name = "AsignarAcuerdoATicketToolStripMenuItem"
        Me.AsignarAcuerdoATicketToolStripMenuItem.Size = New System.Drawing.Size(275, 26)
        Me.AsignarAcuerdoATicketToolStripMenuItem.Tag = "frmAsignarAcuerdoTicket"
        Me.AsignarAcuerdoATicketToolStripMenuItem.Text = "Asignar Acuerdo a Ticket"
        '
        'MacheoFacturaTicketToolStripMenuItem
        '
        Me.MacheoFacturaTicketToolStripMenuItem.Name = "MacheoFacturaTicketToolStripMenuItem"
        Me.MacheoFacturaTicketToolStripMenuItem.Size = New System.Drawing.Size(275, 26)
        Me.MacheoFacturaTicketToolStripMenuItem.Tag = "frmMacheoFacturaTicket"
        Me.MacheoFacturaTicketToolStripMenuItem.Text = "Macheo Factura - Ticket"
        '
        'ToolStripSeparator28
        '
        Me.ToolStripSeparator28.Name = "ToolStripSeparator28"
        Me.ToolStripSeparator28.Size = New System.Drawing.Size(272, 6)
        '
        'InformesToolStripMenuItem2
        '
        Me.InformesToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AcuerdoToolStripMenuItem1, Me.TicketDeBasculaToolStripMenuItem1, Me.MacheoToolStripMenuItem, Me.TicketDeBasculaSinCostoToolStripMenuItem, Me.TicketDeBasculaSinMachearAFechaToolStripMenuItem, Me.ExtractoDePagosPorAcuerdoToolStripMenuItem})
        Me.InformesToolStripMenuItem2.Name = "InformesToolStripMenuItem2"
        Me.InformesToolStripMenuItem2.Size = New System.Drawing.Size(275, 26)
        Me.InformesToolStripMenuItem2.Text = "Informes"
        '
        'AcuerdoToolStripMenuItem1
        '
        Me.AcuerdoToolStripMenuItem1.Name = "AcuerdoToolStripMenuItem1"
        Me.AcuerdoToolStripMenuItem1.Size = New System.Drawing.Size(383, 26)
        Me.AcuerdoToolStripMenuItem1.Tag = "frmInformeAcuerdo"
        Me.AcuerdoToolStripMenuItem1.Text = "Acuerdo"
        '
        'TicketDeBasculaToolStripMenuItem1
        '
        Me.TicketDeBasculaToolStripMenuItem1.Name = "TicketDeBasculaToolStripMenuItem1"
        Me.TicketDeBasculaToolStripMenuItem1.Size = New System.Drawing.Size(383, 26)
        Me.TicketDeBasculaToolStripMenuItem1.Tag = "frmInformeTicketBascula"
        Me.TicketDeBasculaToolStripMenuItem1.Text = "Ticket de Bascula"
        '
        'MacheoToolStripMenuItem
        '
        Me.MacheoToolStripMenuItem.Name = "MacheoToolStripMenuItem"
        Me.MacheoToolStripMenuItem.Size = New System.Drawing.Size(383, 26)
        Me.MacheoToolStripMenuItem.Tag = "frmInfomeMacheoFacturaTicketBascula"
        Me.MacheoToolStripMenuItem.Text = "Macheo"
        '
        'TicketDeBasculaSinCostoToolStripMenuItem
        '
        Me.TicketDeBasculaSinCostoToolStripMenuItem.Name = "TicketDeBasculaSinCostoToolStripMenuItem"
        Me.TicketDeBasculaSinCostoToolStripMenuItem.Size = New System.Drawing.Size(383, 26)
        Me.TicketDeBasculaSinCostoToolStripMenuItem.Tag = "frmInformeTicketBasculaSinCostovb"
        Me.TicketDeBasculaSinCostoToolStripMenuItem.Text = "Ticket de Bascula Sin Costo"
        '
        'TicketDeBasculaSinMachearAFechaToolStripMenuItem
        '
        Me.TicketDeBasculaSinMachearAFechaToolStripMenuItem.Name = "TicketDeBasculaSinMachearAFechaToolStripMenuItem"
        Me.TicketDeBasculaSinMachearAFechaToolStripMenuItem.Size = New System.Drawing.Size(383, 26)
        Me.TicketDeBasculaSinMachearAFechaToolStripMenuItem.Tag = "frmInformeTicketBasculaSinMacheoAFecha"
        Me.TicketDeBasculaSinMachearAFechaToolStripMenuItem.Text = "Ticket de Bascula Sin Machear A Fecha"
        '
        'ExtractoDePagosPorAcuerdoToolStripMenuItem
        '
        Me.ExtractoDePagosPorAcuerdoToolStripMenuItem.Name = "ExtractoDePagosPorAcuerdoToolStripMenuItem"
        Me.ExtractoDePagosPorAcuerdoToolStripMenuItem.Size = New System.Drawing.Size(383, 26)
        Me.ExtractoDePagosPorAcuerdoToolStripMenuItem.Tag = "frmListadoGastoAcuerdo"
        Me.ExtractoDePagosPorAcuerdoToolStripMenuItem.Text = "Extracto de Pagos por Acuerdo"
        '
        'OrdenDePedidoToolStripMenuItem
        '
        Me.OrdenDePedidoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PedidoParaFábricaToolStripMenuItem, Me.InformeToolStripMenuItem5})
        Me.OrdenDePedidoToolStripMenuItem.Name = "OrdenDePedidoToolStripMenuItem"
        Me.OrdenDePedidoToolStripMenuItem.Size = New System.Drawing.Size(243, 26)
        Me.OrdenDePedidoToolStripMenuItem.Text = "Orden de Pedido"
        '
        'PedidoParaFábricaToolStripMenuItem
        '
        Me.PedidoParaFábricaToolStripMenuItem.Name = "PedidoParaFábricaToolStripMenuItem"
        Me.PedidoParaFábricaToolStripMenuItem.Size = New System.Drawing.Size(242, 26)
        Me.PedidoParaFábricaToolStripMenuItem.Tag = "frmOrdenDePedido"
        Me.PedidoParaFábricaToolStripMenuItem.Text = "Pedido para Fábrica"
        '
        'InformeToolStripMenuItem5
        '
        Me.InformeToolStripMenuItem5.Name = "InformeToolStripMenuItem5"
        Me.InformeToolStripMenuItem5.Size = New System.Drawing.Size(242, 26)
        Me.InformeToolStripMenuItem5.Tag = "frmListadoOrdenDePedido"
        Me.InformeToolStripMenuItem5.Text = "Informe"
        '
        'CostoDeProductosToolStripMenuItem
        '
        Me.CostoDeProductosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AsignarCostoAProductoToolStripMenuItem, Me.ControlDeCostosDeOperacionesToolStripMenuItem, Me.AjusteDeExistenciaKardexToolStripMenuItem})
        Me.CostoDeProductosToolStripMenuItem.Name = "CostoDeProductosToolStripMenuItem"
        Me.CostoDeProductosToolStripMenuItem.Size = New System.Drawing.Size(243, 26)
        Me.CostoDeProductosToolStripMenuItem.Text = "Costo de Productos"
        '
        'AsignarCostoAProductoToolStripMenuItem
        '
        Me.AsignarCostoAProductoToolStripMenuItem.Name = "AsignarCostoAProductoToolStripMenuItem"
        Me.AsignarCostoAProductoToolStripMenuItem.Size = New System.Drawing.Size(343, 26)
        Me.AsignarCostoAProductoToolStripMenuItem.Tag = "frmProductoCosto"
        Me.AsignarCostoAProductoToolStripMenuItem.Text = "Asignar Costo a Producto"
        '
        'ControlDeCostosDeOperacionesToolStripMenuItem
        '
        Me.ControlDeCostosDeOperacionesToolStripMenuItem.Name = "ControlDeCostosDeOperacionesToolStripMenuItem"
        Me.ControlDeCostosDeOperacionesToolStripMenuItem.Size = New System.Drawing.Size(343, 26)
        Me.ControlDeCostosDeOperacionesToolStripMenuItem.Text = "Control de Costos de Operaciones"
        '
        'AjusteDeExistenciaKardexToolStripMenuItem
        '
        Me.AjusteDeExistenciaKardexToolStripMenuItem.Name = "AjusteDeExistenciaKardexToolStripMenuItem"
        Me.AjusteDeExistenciaKardexToolStripMenuItem.Size = New System.Drawing.Size(343, 26)
        Me.AjusteDeExistenciaKardexToolStripMenuItem.Tag = "frmAjusteExistenciaSaldoKardex"
        Me.AjusteDeExistenciaKardexToolStripMenuItem.Text = "Ajuste de Existencia Kardex"
        '
        'DistribucionToolStripMenuItem
        '
        Me.DistribucionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PrecargaMovimientoToolStripMenuItem, Me.CargaDescargaToolStripMenuItem, Me.FacturaDistribucionToolStripMenuItem, Me.AprobadorPrecargaMovimientoToolStripMenuItem, Me.UsuarioAprobadorPrecargaToolStripMenuItem, Me.ABMSubMotivoMovimientoToolStripMenuItem})
        Me.DistribucionToolStripMenuItem.Name = "DistribucionToolStripMenuItem"
        Me.DistribucionToolStripMenuItem.Size = New System.Drawing.Size(110, 24)
        Me.DistribucionToolStripMenuItem.Text = "Distribucion"
        '
        'PrecargaMovimientoToolStripMenuItem
        '
        Me.PrecargaMovimientoToolStripMenuItem.Name = "PrecargaMovimientoToolStripMenuItem"
        Me.PrecargaMovimientoToolStripMenuItem.Size = New System.Drawing.Size(318, 26)
        Me.PrecargaMovimientoToolStripMenuItem.Tag = "frmPrecargaMovimiento"
        Me.PrecargaMovimientoToolStripMenuItem.Text = "PrecargaMovimiento"
        '
        'CargaDescargaToolStripMenuItem
        '
        Me.CargaDescargaToolStripMenuItem.Name = "CargaDescargaToolStripMenuItem"
        Me.CargaDescargaToolStripMenuItem.Size = New System.Drawing.Size(318, 26)
        Me.CargaDescargaToolStripMenuItem.Tag = "frmMovimientoDistribucion"
        Me.CargaDescargaToolStripMenuItem.Text = "Carga/Descarga Movimientos"
        '
        'FacturaDistribucionToolStripMenuItem
        '
        Me.FacturaDistribucionToolStripMenuItem.Name = "FacturaDistribucionToolStripMenuItem"
        Me.FacturaDistribucionToolStripMenuItem.Size = New System.Drawing.Size(318, 26)
        Me.FacturaDistribucionToolStripMenuItem.Tag = "frmRemisionExterna"
        Me.FacturaDistribucionToolStripMenuItem.Text = "Factura"
        '
        'AprobadorPrecargaMovimientoToolStripMenuItem
        '
        Me.AprobadorPrecargaMovimientoToolStripMenuItem.Name = "AprobadorPrecargaMovimientoToolStripMenuItem"
        Me.AprobadorPrecargaMovimientoToolStripMenuItem.Size = New System.Drawing.Size(318, 26)
        Me.AprobadorPrecargaMovimientoToolStripMenuItem.Tag = "frmAprobarPrecargaMovimiento"
        Me.AprobadorPrecargaMovimientoToolStripMenuItem.Text = "AprobadorPrecargaMovimiento"
        '
        'UsuarioAprobadorPrecargaToolStripMenuItem
        '
        Me.UsuarioAprobadorPrecargaToolStripMenuItem.Name = "UsuarioAprobadorPrecargaToolStripMenuItem"
        Me.UsuarioAprobadorPrecargaToolStripMenuItem.Size = New System.Drawing.Size(318, 26)
        Me.UsuarioAprobadorPrecargaToolStripMenuItem.Tag = "frmUsuarioAutorizarPrecargaMovimiento"
        Me.UsuarioAprobadorPrecargaToolStripMenuItem.Text = "UsuarioAprobadorPrecarga"
        '
        'ABMSubMotivoMovimientoToolStripMenuItem
        '
        Me.ABMSubMotivoMovimientoToolStripMenuItem.Name = "ABMSubMotivoMovimientoToolStripMenuItem"
        Me.ABMSubMotivoMovimientoToolStripMenuItem.Size = New System.Drawing.Size(318, 26)
        Me.ABMSubMotivoMovimientoToolStripMenuItem.Tag = "frmSubMotivoMovimiento"
        Me.ABMSubMotivoMovimientoToolStripMenuItem.Text = "ABM SubMotivo Movimiento"
        '
        'TesoreriaToolStripMenuItem
        '
        Me.TesoreriaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CobranzaCreditoToolStripMenuItem, Me.CobranzaContadoToolStripMenuItem, Me.AplicacionesDeAnticipoToolStripMenuItem, Me.InformeToolStripMenuItem12, Me.ToolStripSeparator23, Me.ToolStripMenuItem1, Me.ToolStripSeparator42, Me.ChequesToolStripMenuItem, Me.EfectivoToolStripMenuItem, Me.TarjetasToolStripMenuItem, Me.DepositosBancariosToolStripMenuItem, Me.DebitosYCreditosBancariosToolStripMenuItem, Me.ConciliacionBancariaToolStripMenuItem, Me.CanjeDeRecaudacionesToolStripMenuItem, Me.ToolStripSeparator25, Me.CajaToolStripMenuItem, Me.ToolStripMenuItem20, Me.GastosToolStripMenuItem1, Me.ToolStripMenuItem10, Me.ToolStripSeparator26, Me.ToolStripMenuItem21, Me.ToolStripSeparator10, Me.AsociarRetencionesDeClientesToolStripMenuItem, Me.InformeFinancieroToolStripMenuItem})
        Me.TesoreriaToolStripMenuItem.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TesoreriaToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TesoreriaToolStripMenuItem.Name = "TesoreriaToolStripMenuItem"
        Me.TesoreriaToolStripMenuItem.Size = New System.Drawing.Size(90, 24)
        Me.TesoreriaToolStripMenuItem.Text = "Tesoreria"
        '
        'CobranzaCreditoToolStripMenuItem
        '
        Me.CobranzaCreditoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargaToolStripMenuItem9, Me.AprobadorToolStripMenuItem})
        Me.CobranzaCreditoToolStripMenuItem.Name = "CobranzaCreditoToolStripMenuItem"
        Me.CobranzaCreditoToolStripMenuItem.Size = New System.Drawing.Size(329, 26)
        Me.CobranzaCreditoToolStripMenuItem.Tag = "frmCobranzaCredito"
        Me.CobranzaCreditoToolStripMenuItem.Text = "Cobranzas"
        '
        'CargaToolStripMenuItem9
        '
        Me.CargaToolStripMenuItem9.Name = "CargaToolStripMenuItem9"
        Me.CargaToolStripMenuItem9.Size = New System.Drawing.Size(169, 26)
        Me.CargaToolStripMenuItem9.Tag = "frmCobranzaCredito"
        Me.CargaToolStripMenuItem9.Text = "Carga"
        '
        'AprobadorToolStripMenuItem
        '
        Me.AprobadorToolStripMenuItem.Name = "AprobadorToolStripMenuItem"
        Me.AprobadorToolStripMenuItem.Size = New System.Drawing.Size(169, 26)
        Me.AprobadorToolStripMenuItem.Tag = "frmValidarCobranza"
        Me.AprobadorToolStripMenuItem.Text = "Aprobador"
        '
        'CobranzaContadoToolStripMenuItem
        '
        Me.CobranzaContadoToolStripMenuItem.Name = "CobranzaContadoToolStripMenuItem"
        Me.CobranzaContadoToolStripMenuItem.Size = New System.Drawing.Size(329, 26)
        Me.CobranzaContadoToolStripMenuItem.Tag = "frmCobranzaContado"
        Me.CobranzaContadoToolStripMenuItem.Text = "Cobranza por Lote"
        '
        'AplicacionesDeAnticipoToolStripMenuItem
        '
        Me.AplicacionesDeAnticipoToolStripMenuItem.Name = "AplicacionesDeAnticipoToolStripMenuItem"
        Me.AplicacionesDeAnticipoToolStripMenuItem.Size = New System.Drawing.Size(329, 26)
        Me.AplicacionesDeAnticipoToolStripMenuItem.Tag = "frmAplicacionVentasAnticipo"
        Me.AplicacionesDeAnticipoToolStripMenuItem.Text = "Aplicaciones de Anticipo"
        '
        'InformeToolStripMenuItem12
        '
        Me.InformeToolStripMenuItem12.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PlanillaDeCobranzasToolStripMenuItem, Me.ToolStripSeparator46, Me.ResumenDeCobranzasPorFacturasContadoToolStripMenuItem, Me.InformeTicketDeTarjetasToolStripMenuItem, Me.InformeAcreditacionesBancariaToolStripMenuItem, Me.FormaDePagoCobranzaDepositoToolStripMenuItem})
        Me.InformeToolStripMenuItem12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.InformeToolStripMenuItem12.Name = "InformeToolStripMenuItem12"
        Me.InformeToolStripMenuItem12.Size = New System.Drawing.Size(329, 26)
        Me.InformeToolStripMenuItem12.Text = "Informes"
        '
        'PlanillaDeCobranzasToolStripMenuItem
        '
        Me.PlanillaDeCobranzasToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PlanillaDeCobranzasToolStripMenuItem.Name = "PlanillaDeCobranzasToolStripMenuItem"
        Me.PlanillaDeCobranzasToolStripMenuItem.Size = New System.Drawing.Size(480, 26)
        Me.PlanillaDeCobranzasToolStripMenuItem.Tag = "frmPlanillaCobranza"
        Me.PlanillaDeCobranzasToolStripMenuItem.Text = "Planilla de Cobranzas"
        '
        'ToolStripSeparator46
        '
        Me.ToolStripSeparator46.Name = "ToolStripSeparator46"
        Me.ToolStripSeparator46.Size = New System.Drawing.Size(477, 6)
        '
        'ResumenDeCobranzasPorFacturasContadoToolStripMenuItem
        '
        Me.ResumenDeCobranzasPorFacturasContadoToolStripMenuItem.Name = "ResumenDeCobranzasPorFacturasContadoToolStripMenuItem"
        Me.ResumenDeCobranzasPorFacturasContadoToolStripMenuItem.Size = New System.Drawing.Size(480, 26)
        Me.ResumenDeCobranzasPorFacturasContadoToolStripMenuItem.Tag = "frmResumenCobranzaDiaria"
        Me.ResumenDeCobranzasPorFacturasContadoToolStripMenuItem.Text = "Resumen Diario de Cobranzas por Facturas Contado"
        '
        'InformeTicketDeTarjetasToolStripMenuItem
        '
        Me.InformeTicketDeTarjetasToolStripMenuItem.Name = "InformeTicketDeTarjetasToolStripMenuItem"
        Me.InformeTicketDeTarjetasToolStripMenuItem.Size = New System.Drawing.Size(480, 26)
        Me.InformeTicketDeTarjetasToolStripMenuItem.Tag = "frmTarjetas"
        Me.InformeTicketDeTarjetasToolStripMenuItem.Text = "Informe Ticket de Tarjetas"
        '
        'InformeAcreditacionesBancariaToolStripMenuItem
        '
        Me.InformeAcreditacionesBancariaToolStripMenuItem.Name = "InformeAcreditacionesBancariaToolStripMenuItem"
        Me.InformeAcreditacionesBancariaToolStripMenuItem.Size = New System.Drawing.Size(480, 26)
        Me.InformeAcreditacionesBancariaToolStripMenuItem.Tag = "frmAcreditaconBancaria"
        Me.InformeAcreditacionesBancariaToolStripMenuItem.Text = "Informe Acreditaciones Bancaria"
        '
        'FormaDePagoCobranzaDepositoToolStripMenuItem
        '
        Me.FormaDePagoCobranzaDepositoToolStripMenuItem.Name = "FormaDePagoCobranzaDepositoToolStripMenuItem"
        Me.FormaDePagoCobranzaDepositoToolStripMenuItem.Size = New System.Drawing.Size(480, 26)
        Me.FormaDePagoCobranzaDepositoToolStripMenuItem.Tag = "frmInformeFormaPagoCobranzaDeposito"
        Me.FormaDePagoCobranzaDepositoToolStripMenuItem.Text = "Forma de Pago - Cobranza - Deposito"
        '
        'ToolStripSeparator23
        '
        Me.ToolStripSeparator23.Name = "ToolStripSeparator23"
        Me.ToolStripSeparator23.Size = New System.Drawing.Size(326, 6)
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DefinirFondoFijoToolStripMenuItem, Me.ToolStripSeparator43, Me.ValesToolStripMenuItem, Me.ReposicionToolStripMenuItem, Me.ModificarComprobanteToolStripMenuItem, Me.AplicarValeAGastoToolStripMenuItem, Me.ToolStripSeparator22, Me.EmitirListadoAReponerToolStripMenuItem, Me.RendicionToolStripMenuItem, Me.ToolStripSeparator27, Me.ValesPendientesDeRendicionToolStripMenuItem, Me.InformeToolStripMenuItem})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(329, 26)
        Me.ToolStripMenuItem1.Text = "Fondo Fijo"
        '
        'DefinirFondoFijoToolStripMenuItem
        '
        Me.DefinirFondoFijoToolStripMenuItem.Name = "DefinirFondoFijoToolStripMenuItem"
        Me.DefinirFondoFijoToolStripMenuItem.Size = New System.Drawing.Size(318, 26)
        Me.DefinirFondoFijoToolStripMenuItem.Tag = "frmFondoFijo"
        Me.DefinirFondoFijoToolStripMenuItem.Text = "Establecer Fondo Fijo"
        '
        'ToolStripSeparator43
        '
        Me.ToolStripSeparator43.Name = "ToolStripSeparator43"
        Me.ToolStripSeparator43.Size = New System.Drawing.Size(315, 6)
        '
        'ValesToolStripMenuItem
        '
        Me.ValesToolStripMenuItem.Name = "ValesToolStripMenuItem"
        Me.ValesToolStripMenuItem.Size = New System.Drawing.Size(318, 26)
        Me.ValesToolStripMenuItem.Tag = "frmVale"
        Me.ValesToolStripMenuItem.Text = "Vales"
        '
        'ReposicionToolStripMenuItem
        '
        Me.ReposicionToolStripMenuItem.Name = "ReposicionToolStripMenuItem"
        Me.ReposicionToolStripMenuItem.Size = New System.Drawing.Size(318, 26)
        Me.ReposicionToolStripMenuItem.Tag = "frmGastos"
        Me.ReposicionToolStripMenuItem.Text = "Comprobantes"
        '
        'ModificarComprobanteToolStripMenuItem
        '
        Me.ModificarComprobanteToolStripMenuItem.Name = "ModificarComprobanteToolStripMenuItem"
        Me.ModificarComprobanteToolStripMenuItem.Size = New System.Drawing.Size(318, 26)
        Me.ModificarComprobanteToolStripMenuItem.Tag = "frmGastoModificar"
        Me.ModificarComprobanteToolStripMenuItem.Text = "Modificar Comprobante"
        '
        'AplicarValeAGastoToolStripMenuItem
        '
        Me.AplicarValeAGastoToolStripMenuItem.Name = "AplicarValeAGastoToolStripMenuItem"
        Me.AplicarValeAGastoToolStripMenuItem.Size = New System.Drawing.Size(318, 26)
        Me.AplicarValeAGastoToolStripMenuItem.Tag = "frmGastoAplicacionVales"
        Me.AplicarValeAGastoToolStripMenuItem.Text = "Aplicar Vale a Gasto"
        '
        'ToolStripSeparator22
        '
        Me.ToolStripSeparator22.Name = "ToolStripSeparator22"
        Me.ToolStripSeparator22.Size = New System.Drawing.Size(315, 6)
        '
        'EmitirListadoAReponerToolStripMenuItem
        '
        Me.EmitirListadoAReponerToolStripMenuItem.Name = "EmitirListadoAReponerToolStripMenuItem"
        Me.EmitirListadoAReponerToolStripMenuItem.Size = New System.Drawing.Size(318, 26)
        Me.EmitirListadoAReponerToolStripMenuItem.Tag = "frmComprobanteReponer"
        Me.EmitirListadoAReponerToolStripMenuItem.Text = "Emitir listado a Reponer"
        '
        'RendicionToolStripMenuItem
        '
        Me.RendicionToolStripMenuItem.Name = "RendicionToolStripMenuItem"
        Me.RendicionToolStripMenuItem.Size = New System.Drawing.Size(318, 26)
        Me.RendicionToolStripMenuItem.Tag = "frmRendicionFondoFijo"
        Me.RendicionToolStripMenuItem.Text = "Rendicion"
        '
        'ToolStripSeparator27
        '
        Me.ToolStripSeparator27.Name = "ToolStripSeparator27"
        Me.ToolStripSeparator27.Size = New System.Drawing.Size(315, 6)
        '
        'ValesPendientesDeRendicionToolStripMenuItem
        '
        Me.ValesPendientesDeRendicionToolStripMenuItem.Name = "ValesPendientesDeRendicionToolStripMenuItem"
        Me.ValesPendientesDeRendicionToolStripMenuItem.Size = New System.Drawing.Size(318, 26)
        Me.ValesPendientesDeRendicionToolStripMenuItem.Tag = "frmListadoVale"
        Me.ValesPendientesDeRendicionToolStripMenuItem.Text = "Vales Pendientes de Rendicion"
        '
        'InformeToolStripMenuItem
        '
        Me.InformeToolStripMenuItem.Name = "InformeToolStripMenuItem"
        Me.InformeToolStripMenuItem.Size = New System.Drawing.Size(318, 26)
        Me.InformeToolStripMenuItem.Tag = "frmListadoValeGasto"
        Me.InformeToolStripMenuItem.Text = "Vales - Gastos"
        '
        'ToolStripSeparator42
        '
        Me.ToolStripSeparator42.Name = "ToolStripSeparator42"
        Me.ToolStripSeparator42.Size = New System.Drawing.Size(326, 6)
        '
        'ChequesToolStripMenuItem
        '
        Me.ChequesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresoToolStripMenuItem, Me.ModificarToolStripMenuItem, Me.ConsultaDeChequesToolStripMenuItem, Me.RechazarToolStripMenuItem, Me.EfectivizacionToolStripMenuItem, Me.PagoChequeClienteToolStripMenuItem, Me.DescuentosToolStripMenuItem, Me.VencimientoDeChequeDescontadoToolStripMenuItem, Me.ToolStripSeparator20, Me.InformeToolStripMenuItem3})
        Me.ChequesToolStripMenuItem.Name = "ChequesToolStripMenuItem"
        Me.ChequesToolStripMenuItem.Size = New System.Drawing.Size(329, 26)
        Me.ChequesToolStripMenuItem.Text = "Cheques"
        '
        'IngresoToolStripMenuItem
        '
        Me.IngresoToolStripMenuItem.Name = "IngresoToolStripMenuItem"
        Me.IngresoToolStripMenuItem.Size = New System.Drawing.Size(358, 26)
        Me.IngresoToolStripMenuItem.Tag = "frmChequeCliente"
        Me.IngresoToolStripMenuItem.Text = "Cargar"
        '
        'ModificarToolStripMenuItem
        '
        Me.ModificarToolStripMenuItem.Name = "ModificarToolStripMenuItem"
        Me.ModificarToolStripMenuItem.Size = New System.Drawing.Size(358, 26)
        Me.ModificarToolStripMenuItem.Tag = "frmModificarChequeCliente"
        Me.ModificarToolStripMenuItem.Text = "Modificar"
        '
        'ConsultaDeChequesToolStripMenuItem
        '
        Me.ConsultaDeChequesToolStripMenuItem.Name = "ConsultaDeChequesToolStripMenuItem"
        Me.ConsultaDeChequesToolStripMenuItem.Size = New System.Drawing.Size(358, 26)
        Me.ConsultaDeChequesToolStripMenuItem.Tag = "frmConsultaChequeCliente"
        Me.ConsultaDeChequesToolStripMenuItem.Text = "Consultar"
        '
        'RechazarToolStripMenuItem
        '
        Me.RechazarToolStripMenuItem.Name = "RechazarToolStripMenuItem"
        Me.RechazarToolStripMenuItem.Size = New System.Drawing.Size(358, 26)
        Me.RechazarToolStripMenuItem.Tag = "frmRechazoChequeCliente"
        Me.RechazarToolStripMenuItem.Text = "Rechazar"
        '
        'EfectivizacionToolStripMenuItem
        '
        Me.EfectivizacionToolStripMenuItem.Name = "EfectivizacionToolStripMenuItem"
        Me.EfectivizacionToolStripMenuItem.Size = New System.Drawing.Size(358, 26)
        Me.EfectivizacionToolStripMenuItem.Tag = "frmEfectivizacion"
        Me.EfectivizacionToolStripMenuItem.Text = "Efectivizacion"
        '
        'PagoChequeClienteToolStripMenuItem
        '
        Me.PagoChequeClienteToolStripMenuItem.Name = "PagoChequeClienteToolStripMenuItem"
        Me.PagoChequeClienteToolStripMenuItem.Size = New System.Drawing.Size(358, 26)
        Me.PagoChequeClienteToolStripMenuItem.Tag = "frmCanjeChequeCliente"
        Me.PagoChequeClienteToolStripMenuItem.Text = "Canje de Cheque Rechazado"
        '
        'DescuentosToolStripMenuItem
        '
        Me.DescuentosToolStripMenuItem.Name = "DescuentosToolStripMenuItem"
        Me.DescuentosToolStripMenuItem.Size = New System.Drawing.Size(358, 26)
        Me.DescuentosToolStripMenuItem.Tag = "frmDescuentodeCheques"
        Me.DescuentosToolStripMenuItem.Text = "Descuentos de Cheques Diferidos"
        '
        'VencimientoDeChequeDescontadoToolStripMenuItem
        '
        Me.VencimientoDeChequeDescontadoToolStripMenuItem.Name = "VencimientoDeChequeDescontadoToolStripMenuItem"
        Me.VencimientoDeChequeDescontadoToolStripMenuItem.Size = New System.Drawing.Size(358, 26)
        Me.VencimientoDeChequeDescontadoToolStripMenuItem.Tag = "frmVencimientoChequeDescontado"
        Me.VencimientoDeChequeDescontadoToolStripMenuItem.Text = "Vencimiento de Cheque Descontado"
        '
        'ToolStripSeparator20
        '
        Me.ToolStripSeparator20.Name = "ToolStripSeparator20"
        Me.ToolStripSeparator20.Size = New System.Drawing.Size(355, 6)
        '
        'InformeToolStripMenuItem3
        '
        Me.InformeToolStripMenuItem3.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListadoDeChequesToolStripMenuItem, Me.ListadoDeCanjesToolStripMenuItem, Me.ListadoDeDescuentosDeChequesDiferidosToolStripMenuItem, Me.ListadoDeChequesRechazadosToolStripMenuItem, Me.ChequesDiferidosDepositadosToolStripMenuItem, Me.ListadoDeEstadoDeChequesToolStripMenuItem, Me.ListadoDeChequesRechazadosToolStripMenuItem1, Me.ListadoDeChequesDescontadosToolStripMenuItem})
        Me.InformeToolStripMenuItem3.Name = "InformeToolStripMenuItem3"
        Me.InformeToolStripMenuItem3.Size = New System.Drawing.Size(358, 26)
        Me.InformeToolStripMenuItem3.Text = "Informe"
        '
        'ListadoDeChequesToolStripMenuItem
        '
        Me.ListadoDeChequesToolStripMenuItem.Name = "ListadoDeChequesToolStripMenuItem"
        Me.ListadoDeChequesToolStripMenuItem.Size = New System.Drawing.Size(451, 26)
        Me.ListadoDeChequesToolStripMenuItem.Tag = "frmListadoCheque"
        Me.ListadoDeChequesToolStripMenuItem.Text = "Listado de Cheques"
        '
        'ListadoDeCanjesToolStripMenuItem
        '
        Me.ListadoDeCanjesToolStripMenuItem.Name = "ListadoDeCanjesToolStripMenuItem"
        Me.ListadoDeCanjesToolStripMenuItem.Size = New System.Drawing.Size(451, 26)
        Me.ListadoDeCanjesToolStripMenuItem.Tag = "frmListadoCanje"
        Me.ListadoDeCanjesToolStripMenuItem.Text = "Listado de Canjes"
        '
        'ListadoDeDescuentosDeChequesDiferidosToolStripMenuItem
        '
        Me.ListadoDeDescuentosDeChequesDiferidosToolStripMenuItem.Name = "ListadoDeDescuentosDeChequesDiferidosToolStripMenuItem"
        Me.ListadoDeDescuentosDeChequesDiferidosToolStripMenuItem.Size = New System.Drawing.Size(451, 26)
        Me.ListadoDeDescuentosDeChequesDiferidosToolStripMenuItem.Tag = "frmListadoIngresoChequesDiferidos"
        Me.ListadoDeDescuentosDeChequesDiferidosToolStripMenuItem.Text = "Listado de Cheques Diferidos contra Facturas"
        '
        'ListadoDeChequesRechazadosToolStripMenuItem
        '
        Me.ListadoDeChequesRechazadosToolStripMenuItem.Name = "ListadoDeChequesRechazadosToolStripMenuItem"
        Me.ListadoDeChequesRechazadosToolStripMenuItem.Size = New System.Drawing.Size(451, 26)
        Me.ListadoDeChequesRechazadosToolStripMenuItem.Tag = "frmSaldoChequesDiferidos"
        Me.ListadoDeChequesRechazadosToolStripMenuItem.Text = "Listado de Saldos Cheques Diferidos"
        '
        'ChequesDiferidosDepositadosToolStripMenuItem
        '
        Me.ChequesDiferidosDepositadosToolStripMenuItem.Name = "ChequesDiferidosDepositadosToolStripMenuItem"
        Me.ChequesDiferidosDepositadosToolStripMenuItem.Size = New System.Drawing.Size(451, 26)
        Me.ChequesDiferidosDepositadosToolStripMenuItem.Tag = "frmChequesDiferidosDepositados"
        Me.ChequesDiferidosDepositadosToolStripMenuItem.Text = "Listado de Cheques Diferidos Depositados"
        '
        'ListadoDeEstadoDeChequesToolStripMenuItem
        '
        Me.ListadoDeEstadoDeChequesToolStripMenuItem.Name = "ListadoDeEstadoDeChequesToolStripMenuItem"
        Me.ListadoDeEstadoDeChequesToolStripMenuItem.Size = New System.Drawing.Size(451, 26)
        Me.ListadoDeEstadoDeChequesToolStripMenuItem.Tag = "frmListadoEstadoCheques"
        Me.ListadoDeEstadoDeChequesToolStripMenuItem.Text = "Listado de Estado de Cheques"
        '
        'ListadoDeChequesRechazadosToolStripMenuItem1
        '
        Me.ListadoDeChequesRechazadosToolStripMenuItem1.Name = "ListadoDeChequesRechazadosToolStripMenuItem1"
        Me.ListadoDeChequesRechazadosToolStripMenuItem1.Size = New System.Drawing.Size(451, 26)
        Me.ListadoDeChequesRechazadosToolStripMenuItem1.Tag = "frmListadoChequesRechazados"
        Me.ListadoDeChequesRechazadosToolStripMenuItem1.Text = "Listado de Cheques Rechazados"
        '
        'ListadoDeChequesDescontadosToolStripMenuItem
        '
        Me.ListadoDeChequesDescontadosToolStripMenuItem.Name = "ListadoDeChequesDescontadosToolStripMenuItem"
        Me.ListadoDeChequesDescontadosToolStripMenuItem.Size = New System.Drawing.Size(451, 26)
        Me.ListadoDeChequesDescontadosToolStripMenuItem.Tag = "frmListadoChequesDescontados"
        Me.ListadoDeChequesDescontadosToolStripMenuItem.Text = "Listado de Vencimiento de Cheques descontados"
        '
        'EfectivoToolStripMenuItem
        '
        Me.EfectivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargarToolStripMenuItem2, Me.ConsultaToolStripMenuItem, Me.EfectivisarToolStripMenuItem})
        Me.EfectivoToolStripMenuItem.Name = "EfectivoToolStripMenuItem"
        Me.EfectivoToolStripMenuItem.Size = New System.Drawing.Size(329, 26)
        Me.EfectivoToolStripMenuItem.Text = "Efectivo"
        '
        'CargarToolStripMenuItem2
        '
        Me.CargarToolStripMenuItem2.Name = "CargarToolStripMenuItem2"
        Me.CargarToolStripMenuItem2.Size = New System.Drawing.Size(230, 26)
        Me.CargarToolStripMenuItem2.Tag = "frmEfectivo"
        Me.CargarToolStripMenuItem2.Text = "Cargar"
        '
        'ConsultaToolStripMenuItem
        '
        Me.ConsultaToolStripMenuItem.Name = "ConsultaToolStripMenuItem"
        Me.ConsultaToolStripMenuItem.Size = New System.Drawing.Size(230, 26)
        Me.ConsultaToolStripMenuItem.Tag = "frmConsultaEfectivo"
        Me.ConsultaToolStripMenuItem.Text = "Consulta"
        '
        'EfectivisarToolStripMenuItem
        '
        Me.EfectivisarToolStripMenuItem.Name = "EfectivisarToolStripMenuItem"
        Me.EfectivisarToolStripMenuItem.Size = New System.Drawing.Size(230, 26)
        Me.EfectivisarToolStripMenuItem.Tag = "frmHabilitarParaPagoEfectivo"
        Me.EfectivisarToolStripMenuItem.Text = "Habilitar para pago"
        Me.EfectivisarToolStripMenuItem.ToolTipText = "Habilita el efectivo para que se pueda seleccionar en la Orden de Pago"
        '
        'TarjetasToolStripMenuItem
        '
        Me.TarjetasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TiposDeTarjetasToolStripMenuItem})
        Me.TarjetasToolStripMenuItem.Name = "TarjetasToolStripMenuItem"
        Me.TarjetasToolStripMenuItem.Size = New System.Drawing.Size(329, 26)
        Me.TarjetasToolStripMenuItem.Text = "Tarjetas"
        '
        'TiposDeTarjetasToolStripMenuItem
        '
        Me.TiposDeTarjetasToolStripMenuItem.Name = "TiposDeTarjetasToolStripMenuItem"
        Me.TiposDeTarjetasToolStripMenuItem.Size = New System.Drawing.Size(210, 26)
        Me.TiposDeTarjetasToolStripMenuItem.Text = "Tipos de tarjetas"
        '
        'DepositosBancariosToolStripMenuItem
        '
        Me.DepositosBancariosToolStripMenuItem.Name = "DepositosBancariosToolStripMenuItem"
        Me.DepositosBancariosToolStripMenuItem.Size = New System.Drawing.Size(329, 26)
        Me.DepositosBancariosToolStripMenuItem.Tag = "frmDepositoBancario"
        Me.DepositosBancariosToolStripMenuItem.Text = "Depositos Bancarios"
        '
        'DebitosYCreditosBancariosToolStripMenuItem
        '
        Me.DebitosYCreditosBancariosToolStripMenuItem.Name = "DebitosYCreditosBancariosToolStripMenuItem"
        Me.DebitosYCreditosBancariosToolStripMenuItem.Size = New System.Drawing.Size(329, 26)
        Me.DebitosYCreditosBancariosToolStripMenuItem.Tag = "frmDebitoCreditoBancario"
        Me.DebitosYCreditosBancariosToolStripMenuItem.Text = "Debitos y Creditos Bancarios"
        '
        'ConciliacionBancariaToolStripMenuItem
        '
        Me.ConciliacionBancariaToolStripMenuItem.Name = "ConciliacionBancariaToolStripMenuItem"
        Me.ConciliacionBancariaToolStripMenuItem.Size = New System.Drawing.Size(329, 26)
        Me.ConciliacionBancariaToolStripMenuItem.Tag = "frmConciliaciondeMovimientoBancaria"
        Me.ConciliacionBancariaToolStripMenuItem.Text = "Conciliacion Bancaria"
        '
        'CanjeDeRecaudacionesToolStripMenuItem
        '
        Me.CanjeDeRecaudacionesToolStripMenuItem.Name = "CanjeDeRecaudacionesToolStripMenuItem"
        Me.CanjeDeRecaudacionesToolStripMenuItem.Size = New System.Drawing.Size(329, 26)
        Me.CanjeDeRecaudacionesToolStripMenuItem.Tag = "frmCanjeRecaudaciones"
        Me.CanjeDeRecaudacionesToolStripMenuItem.Text = "Canje de Recaudaciones"
        '
        'ToolStripSeparator25
        '
        Me.ToolStripSeparator25.Name = "ToolStripSeparator25"
        Me.ToolStripSeparator25.Size = New System.Drawing.Size(326, 6)
        '
        'CajaToolStripMenuItem
        '
        Me.CajaToolStripMenuItem.Name = "CajaToolStripMenuItem"
        Me.CajaToolStripMenuItem.Size = New System.Drawing.Size(329, 26)
        Me.CajaToolStripMenuItem.Tag = "frmAdministrarCaja"
        Me.CajaToolStripMenuItem.Text = "Caja"
        '
        'ToolStripMenuItem20
        '
        Me.ToolStripMenuItem20.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargaToolStripMenuItem5, Me.EgresosARendirToolStripMenuItem, Me.AnticipoAProveedoresToolStripMenuItem, Me.ToolStripSeparator50, Me.PrepararPagosToolStripMenuItem, Me.RealizarPAgosToolStripMenuItem, Me.ToolStripSeparator51, Me.EntregaDeChequesOPToolStripMenuItem, Me.VencimientoDeChequesToolStripMenuItem, Me.ToolStripSeparator52, Me.RetencionToolStripMenuItem, Me.SolicitudesDeModificacionToolStripMenuItem, Me.ImprimirToolStripMenuItem3, Me.ImprimirOrdenDePagoToolStripMenuItem, Me.AplicacionAnticipoAProveedoresToolStripMenuItem})
        Me.ToolStripMenuItem20.Name = "ToolStripMenuItem20"
        Me.ToolStripMenuItem20.Size = New System.Drawing.Size(329, 26)
        Me.ToolStripMenuItem20.Text = "Orden de Pago"
        '
        'CargaToolStripMenuItem5
        '
        Me.CargaToolStripMenuItem5.Name = "CargaToolStripMenuItem5"
        Me.CargaToolStripMenuItem5.Size = New System.Drawing.Size(342, 26)
        Me.CargaToolStripMenuItem5.Tag = "frmOrdenPago"
        Me.CargaToolStripMenuItem5.Text = "Cargar OP"
        '
        'EgresosARendirToolStripMenuItem
        '
        Me.EgresosARendirToolStripMenuItem.Name = "EgresosARendirToolStripMenuItem"
        Me.EgresosARendirToolStripMenuItem.Size = New System.Drawing.Size(342, 26)
        Me.EgresosARendirToolStripMenuItem.Tag = "frmOrdenPagoEgresosARendir"
        Me.EgresosARendirToolStripMenuItem.Text = "Egresos a Rendir"
        '
        'AnticipoAProveedoresToolStripMenuItem
        '
        Me.AnticipoAProveedoresToolStripMenuItem.Name = "AnticipoAProveedoresToolStripMenuItem"
        Me.AnticipoAProveedoresToolStripMenuItem.Size = New System.Drawing.Size(342, 26)
        Me.AnticipoAProveedoresToolStripMenuItem.Tag = "frmOrdenPagoAnticipo"
        Me.AnticipoAProveedoresToolStripMenuItem.Text = "Anticipo a Proveedores"
        '
        'ToolStripSeparator50
        '
        Me.ToolStripSeparator50.Name = "ToolStripSeparator50"
        Me.ToolStripSeparator50.Size = New System.Drawing.Size(339, 6)
        '
        'PrepararPagosToolStripMenuItem
        '
        Me.PrepararPagosToolStripMenuItem.Name = "PrepararPagosToolStripMenuItem"
        Me.PrepararPagosToolStripMenuItem.Size = New System.Drawing.Size(342, 26)
        Me.PrepararPagosToolStripMenuItem.Tag = "frmPrepararPago"
        Me.PrepararPagosToolStripMenuItem.Text = "Preparar Pagos"
        '
        'RealizarPAgosToolStripMenuItem
        '
        Me.RealizarPAgosToolStripMenuItem.Name = "RealizarPAgosToolStripMenuItem"
        Me.RealizarPAgosToolStripMenuItem.Size = New System.Drawing.Size(342, 26)
        Me.RealizarPAgosToolStripMenuItem.Tag = "frmPagoEgreso"
        Me.RealizarPAgosToolStripMenuItem.Text = "Realizar Pagos"
        '
        'ToolStripSeparator51
        '
        Me.ToolStripSeparator51.Name = "ToolStripSeparator51"
        Me.ToolStripSeparator51.Size = New System.Drawing.Size(339, 6)
        '
        'EntregaDeChequesOPToolStripMenuItem
        '
        Me.EntregaDeChequesOPToolStripMenuItem.Name = "EntregaDeChequesOPToolStripMenuItem"
        Me.EntregaDeChequesOPToolStripMenuItem.Size = New System.Drawing.Size(342, 26)
        Me.EntregaDeChequesOPToolStripMenuItem.Tag = "frmEntregaChequeOP"
        Me.EntregaDeChequesOPToolStripMenuItem.Text = "Entrega de Cheques OP"
        '
        'VencimientoDeChequesToolStripMenuItem
        '
        Me.VencimientoDeChequesToolStripMenuItem.Name = "VencimientoDeChequesToolStripMenuItem"
        Me.VencimientoDeChequesToolStripMenuItem.Size = New System.Drawing.Size(342, 26)
        Me.VencimientoDeChequesToolStripMenuItem.Tag = "frmVencimientoChequeEmitido"
        Me.VencimientoDeChequesToolStripMenuItem.Text = "Vencimiento de Cheques"
        '
        'ToolStripSeparator52
        '
        Me.ToolStripSeparator52.Name = "ToolStripSeparator52"
        Me.ToolStripSeparator52.Size = New System.Drawing.Size(339, 6)
        '
        'RetencionToolStripMenuItem
        '
        Me.RetencionToolStripMenuItem.Name = "RetencionToolStripMenuItem"
        Me.RetencionToolStripMenuItem.Size = New System.Drawing.Size(342, 26)
        Me.RetencionToolStripMenuItem.Tag = "frmRetencionIVA"
        Me.RetencionToolStripMenuItem.Text = "Retencion"
        '
        'SolicitudesDeModificacionToolStripMenuItem
        '
        Me.SolicitudesDeModificacionToolStripMenuItem.Name = "SolicitudesDeModificacionToolStripMenuItem"
        Me.SolicitudesDeModificacionToolStripMenuItem.Size = New System.Drawing.Size(342, 26)
        Me.SolicitudesDeModificacionToolStripMenuItem.Tag = "frmSolicitudEliminarComprobanteOrdenPagoEgreso"
        Me.SolicitudesDeModificacionToolStripMenuItem.Text = "Solicitudes de Modificacion"
        '
        'ImprimirToolStripMenuItem3
        '
        Me.ImprimirToolStripMenuItem3.Name = "ImprimirToolStripMenuItem3"
        Me.ImprimirToolStripMenuItem3.Size = New System.Drawing.Size(342, 26)
        Me.ImprimirToolStripMenuItem3.Tag = "frmImpresionCheque"
        Me.ImprimirToolStripMenuItem3.Text = "Imprimir Cheque"
        '
        'ImprimirOrdenDePagoToolStripMenuItem
        '
        Me.ImprimirOrdenDePagoToolStripMenuItem.Name = "ImprimirOrdenDePagoToolStripMenuItem"
        Me.ImprimirOrdenDePagoToolStripMenuItem.Size = New System.Drawing.Size(342, 26)
        Me.ImprimirOrdenDePagoToolStripMenuItem.Tag = "frmImpresionOrdenPago"
        Me.ImprimirOrdenDePagoToolStripMenuItem.Text = "Imprimir Orden de Pago"
        '
        'AplicacionAnticipoAProveedoresToolStripMenuItem
        '
        Me.AplicacionAnticipoAProveedoresToolStripMenuItem.Name = "AplicacionAnticipoAProveedoresToolStripMenuItem"
        Me.AplicacionAnticipoAProveedoresToolStripMenuItem.Size = New System.Drawing.Size(342, 26)
        Me.AplicacionAnticipoAProveedoresToolStripMenuItem.Tag = "frmAplicacionAnticipoProveedores"
        Me.AplicacionAnticipoAProveedoresToolStripMenuItem.Text = "Aplicacion Anticipo a Proveedores"
        '
        'GastosToolStripMenuItem1
        '
        Me.GastosToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargaToolStripMenuItem3, Me.AdministrarToolStripMenuItem1, Me.ModifcarToolStripMenuItem, Me.GenerarCuotasToolStripMenuItem})
        Me.GastosToolStripMenuItem1.Name = "GastosToolStripMenuItem1"
        Me.GastosToolStripMenuItem1.Size = New System.Drawing.Size(329, 26)
        Me.GastosToolStripMenuItem1.Text = "Gastos"
        '
        'CargaToolStripMenuItem3
        '
        Me.CargaToolStripMenuItem3.Name = "CargaToolStripMenuItem3"
        Me.CargaToolStripMenuItem3.Size = New System.Drawing.Size(205, 26)
        Me.CargaToolStripMenuItem3.Tag = "frmGastos"
        Me.CargaToolStripMenuItem3.Text = "Carga"
        '
        'AdministrarToolStripMenuItem1
        '
        Me.AdministrarToolStripMenuItem1.Name = "AdministrarToolStripMenuItem1"
        Me.AdministrarToolStripMenuItem1.Size = New System.Drawing.Size(205, 26)
        Me.AdministrarToolStripMenuItem1.Tag = "frmAdministrarEgresos"
        Me.AdministrarToolStripMenuItem1.Text = "Administrar"
        '
        'ModifcarToolStripMenuItem
        '
        Me.ModifcarToolStripMenuItem.Name = "ModifcarToolStripMenuItem"
        Me.ModifcarToolStripMenuItem.Size = New System.Drawing.Size(205, 26)
        Me.ModifcarToolStripMenuItem.Tag = "frmGastoModificar"
        Me.ModifcarToolStripMenuItem.Text = "Modificar"
        '
        'GenerarCuotasToolStripMenuItem
        '
        Me.GenerarCuotasToolStripMenuItem.Name = "GenerarCuotasToolStripMenuItem"
        Me.GenerarCuotasToolStripMenuItem.Size = New System.Drawing.Size(205, 26)
        Me.GenerarCuotasToolStripMenuItem.Tag = "frmGenerarCuotasGastos"
        Me.GenerarCuotasToolStripMenuItem.Text = "Generar cuotas"
        '
        'ToolStripMenuItem10
        '
        Me.ToolStripMenuItem10.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargaToolStripMenuItem, Me.InformePrestamoBancarioToolStripMenuItem2})
        Me.ToolStripMenuItem10.Name = "ToolStripMenuItem10"
        Me.ToolStripMenuItem10.Size = New System.Drawing.Size(329, 26)
        Me.ToolStripMenuItem10.Tag = "frmPrestamoBancario"
        Me.ToolStripMenuItem10.Text = "Prestamos Bancarios"
        '
        'CargaToolStripMenuItem
        '
        Me.CargaToolStripMenuItem.Name = "CargaToolStripMenuItem"
        Me.CargaToolStripMenuItem.Size = New System.Drawing.Size(148, 26)
        Me.CargaToolStripMenuItem.Text = "Cargar"
        '
        'InformePrestamoBancarioToolStripMenuItem2
        '
        Me.InformePrestamoBancarioToolStripMenuItem2.Name = "InformePrestamoBancarioToolStripMenuItem2"
        Me.InformePrestamoBancarioToolStripMenuItem2.Size = New System.Drawing.Size(148, 26)
        Me.InformePrestamoBancarioToolStripMenuItem2.Tag = "frmListadoPrestamoBancario"
        Me.InformePrestamoBancarioToolStripMenuItem2.Text = "Informe"
        '
        'ToolStripSeparator26
        '
        Me.ToolStripSeparator26.Name = "ToolStripSeparator26"
        Me.ToolStripSeparator26.Size = New System.Drawing.Size(326, 6)
        '
        'ToolStripMenuItem21
        '
        Me.ToolStripMenuItem21.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CuentasACobrarToolStripMenuItem, Me.CuentasAPagarToolStripMenuItem, Me.BancoToolStripMenuItem, Me.ToolStripSeparator35, Me.ResumenDiarioToolStripMenuItem, Me.ToolStripSeparator21, Me.ListadosToolStripMenuItem, Me.ToolStripMenuItem34, Me.ListadoDeDepositosBancariosToolStripMenuItem, Me.ListadoDeDebitosYCreditosBancariosToolStripMenuItem, Me.ListadoDeOrdenDePagoToolStripMenuItem, Me.ListadoDeGastosToolStripMenuItem, Me.ListadoDeRetencionesRecibidasToolStripMenuItem, Me.LIstadoDeChequesEmitidosToolStripMenuItem, Me.ListadoDeRetencionesEmitidasToolStripMenuItem, Me.ListadoDeRecibosGeneradosToolStripMenuItem, Me.ListadoDeGastosAcuerdoComercialToolStripMenuItem})
        Me.ToolStripMenuItem21.Name = "ToolStripMenuItem21"
        Me.ToolStripMenuItem21.Size = New System.Drawing.Size(329, 26)
        Me.ToolStripMenuItem21.Text = "Informes"
        '
        'CuentasACobrarToolStripMenuItem
        '
        Me.CuentasACobrarToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InventarioDeDocumentosPendientesToolStripMenuItem, Me.BusquedaDeClientesConFacturasAtrasadasToolStripMenuItem, Me.DistribucionDeSaldosDeDocumentosToolStripMenuItem, Me.ResumenDeMovimientosYSaldosToolStripMenuItem, Me.SaldosAFechaToolStripMenuItem1, Me.InventarioDeDocumentosPendientesPorClienteToolStripMenuItem})
        Me.CuentasACobrarToolStripMenuItem.Name = "CuentasACobrarToolStripMenuItem"
        Me.CuentasACobrarToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.CuentasACobrarToolStripMenuItem.Text = "Cuentas a Cobrar"
        '
        'InventarioDeDocumentosPendientesToolStripMenuItem
        '
        Me.InventarioDeDocumentosPendientesToolStripMenuItem.Name = "InventarioDeDocumentosPendientesToolStripMenuItem"
        Me.InventarioDeDocumentosPendientesToolStripMenuItem.Size = New System.Drawing.Size(452, 26)
        Me.InventarioDeDocumentosPendientesToolStripMenuItem.Tag = "frmInventariodeDocumentosPendientesCobrar"
        Me.InventarioDeDocumentosPendientesToolStripMenuItem.Text = "Inventario de Documentos Pendientes"
        '
        'BusquedaDeClientesConFacturasAtrasadasToolStripMenuItem
        '
        Me.BusquedaDeClientesConFacturasAtrasadasToolStripMenuItem.ForeColor = System.Drawing.SystemColors.GrayText
        Me.BusquedaDeClientesConFacturasAtrasadasToolStripMenuItem.Name = "BusquedaDeClientesConFacturasAtrasadasToolStripMenuItem"
        Me.BusquedaDeClientesConFacturasAtrasadasToolStripMenuItem.Size = New System.Drawing.Size(452, 26)
        Me.BusquedaDeClientesConFacturasAtrasadasToolStripMenuItem.Text = "Busqueda de Clientes con Facturas Atrasadas"
        '
        'DistribucionDeSaldosDeDocumentosToolStripMenuItem
        '
        Me.DistribucionDeSaldosDeDocumentosToolStripMenuItem.Name = "DistribucionDeSaldosDeDocumentosToolStripMenuItem"
        Me.DistribucionDeSaldosDeDocumentosToolStripMenuItem.Size = New System.Drawing.Size(452, 26)
        Me.DistribucionDeSaldosDeDocumentosToolStripMenuItem.Tag = "frmDistribuciondeSaldoDocumentoaCobrar"
        Me.DistribucionDeSaldosDeDocumentosToolStripMenuItem.Text = "Distribucion de Saldos de Documentos"
        '
        'ResumenDeMovimientosYSaldosToolStripMenuItem
        '
        Me.ResumenDeMovimientosYSaldosToolStripMenuItem.Name = "ResumenDeMovimientosYSaldosToolStripMenuItem"
        Me.ResumenDeMovimientosYSaldosToolStripMenuItem.Size = New System.Drawing.Size(452, 26)
        Me.ResumenDeMovimientosYSaldosToolStripMenuItem.Tag = "frmResumendeMovimientoSaldo"
        Me.ResumenDeMovimientosYSaldosToolStripMenuItem.Text = "Resumen de Movimientos y Saldos"
        '
        'SaldosAFechaToolStripMenuItem1
        '
        Me.SaldosAFechaToolStripMenuItem1.Name = "SaldosAFechaToolStripMenuItem1"
        Me.SaldosAFechaToolStripMenuItem1.Size = New System.Drawing.Size(452, 26)
        Me.SaldosAFechaToolStripMenuItem1.Text = "Saldos a Fecha"
        '
        'InventarioDeDocumentosPendientesPorClienteToolStripMenuItem
        '
        Me.InventarioDeDocumentosPendientesPorClienteToolStripMenuItem.Name = "InventarioDeDocumentosPendientesPorClienteToolStripMenuItem"
        Me.InventarioDeDocumentosPendientesPorClienteToolStripMenuItem.Size = New System.Drawing.Size(452, 26)
        Me.InventarioDeDocumentosPendientesPorClienteToolStripMenuItem.Text = "Inventario de Documentos Pendientes por Cliente"
        '
        'CuentasAPagarToolStripMenuItem
        '
        Me.CuentasAPagarToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem22, Me.ToolStripMenuItem24, Me.DocumentosPagadosToolStripMenuItem, Me.ToolStripMenuItem25, Me.SaldosAFechaToolStripMenuItem, Me.InventarioDeDocumentosPendientesPorPlazoToolStripMenuItem})
        Me.CuentasAPagarToolStripMenuItem.Name = "CuentasAPagarToolStripMenuItem"
        Me.CuentasAPagarToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.CuentasAPagarToolStripMenuItem.Text = "Cuentas a Pagar"
        '
        'ToolStripMenuItem22
        '
        Me.ToolStripMenuItem22.Name = "ToolStripMenuItem22"
        Me.ToolStripMenuItem22.Size = New System.Drawing.Size(444, 26)
        Me.ToolStripMenuItem22.Tag = "frmInventariodeDocumentosPendientesAPagar"
        Me.ToolStripMenuItem22.Text = "Inventario de Documentos Pendientes"
        '
        'ToolStripMenuItem24
        '
        Me.ToolStripMenuItem24.Name = "ToolStripMenuItem24"
        Me.ToolStripMenuItem24.Size = New System.Drawing.Size(444, 26)
        Me.ToolStripMenuItem24.Tag = "frmDistribuciondeSaldoDocumentoaPagar"
        Me.ToolStripMenuItem24.Text = "Distribucion de Saldos de Documentos"
        '
        'DocumentosPagadosToolStripMenuItem
        '
        Me.DocumentosPagadosToolStripMenuItem.Name = "DocumentosPagadosToolStripMenuItem"
        Me.DocumentosPagadosToolStripMenuItem.Size = New System.Drawing.Size(444, 26)
        Me.DocumentosPagadosToolStripMenuItem.Tag = "frmDocumentoPagado"
        Me.DocumentosPagadosToolStripMenuItem.Text = "Documentos Pagados"
        '
        'ToolStripMenuItem25
        '
        Me.ToolStripMenuItem25.Name = "ToolStripMenuItem25"
        Me.ToolStripMenuItem25.Size = New System.Drawing.Size(444, 26)
        Me.ToolStripMenuItem25.Tag = "frmMovimientoSaldoProveedor"
        Me.ToolStripMenuItem25.Text = "Resumen de Movimientos y Saldos"
        '
        'SaldosAFechaToolStripMenuItem
        '
        Me.SaldosAFechaToolStripMenuItem.Name = "SaldosAFechaToolStripMenuItem"
        Me.SaldosAFechaToolStripMenuItem.Size = New System.Drawing.Size(444, 26)
        Me.SaldosAFechaToolStripMenuItem.Tag = "frmConsultarSaldoProveedorFecha"
        Me.SaldosAFechaToolStripMenuItem.Text = "Saldos a Fecha"
        '
        'InventarioDeDocumentosPendientesPorPlazoToolStripMenuItem
        '
        Me.InventarioDeDocumentosPendientesPorPlazoToolStripMenuItem.Name = "InventarioDeDocumentosPendientesPorPlazoToolStripMenuItem"
        Me.InventarioDeDocumentosPendientesPorPlazoToolStripMenuItem.Size = New System.Drawing.Size(444, 26)
        Me.InventarioDeDocumentosPendientesPorPlazoToolStripMenuItem.Text = "Inventario de Documentos Pendientes Por Plazo"
        '
        'BancoToolStripMenuItem
        '
        Me.BancoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExtractoDeMovimientosToolStripMenuItem2, Me.SaldosBancariosToolStripMenuItem, Me.SaldoDiarioDeBancoToolStripMenuItem})
        Me.BancoToolStripMenuItem.Name = "BancoToolStripMenuItem"
        Me.BancoToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.BancoToolStripMenuItem.Text = "Banco"
        '
        'ExtractoDeMovimientosToolStripMenuItem2
        '
        Me.ExtractoDeMovimientosToolStripMenuItem2.Name = "ExtractoDeMovimientosToolStripMenuItem2"
        Me.ExtractoDeMovimientosToolStripMenuItem2.Size = New System.Drawing.Size(268, 26)
        Me.ExtractoDeMovimientosToolStripMenuItem2.Tag = "frmExtractoMovimientoBancario"
        Me.ExtractoDeMovimientosToolStripMenuItem2.Text = "Extracto de Movimientos"
        '
        'SaldosBancariosToolStripMenuItem
        '
        Me.SaldosBancariosToolStripMenuItem.Name = "SaldosBancariosToolStripMenuItem"
        Me.SaldosBancariosToolStripMenuItem.Size = New System.Drawing.Size(268, 26)
        Me.SaldosBancariosToolStripMenuItem.Text = "Saldos Bancarios"
        '
        'SaldoDiarioDeBancoToolStripMenuItem
        '
        Me.SaldoDiarioDeBancoToolStripMenuItem.Name = "SaldoDiarioDeBancoToolStripMenuItem"
        Me.SaldoDiarioDeBancoToolStripMenuItem.Size = New System.Drawing.Size(268, 26)
        Me.SaldoDiarioDeBancoToolStripMenuItem.Tag = "frmSaldoBancariaDiario"
        Me.SaldoDiarioDeBancoToolStripMenuItem.Text = "Saldo Diario De Banco"
        '
        'ToolStripSeparator35
        '
        Me.ToolStripSeparator35.Name = "ToolStripSeparator35"
        Me.ToolStripSeparator35.Size = New System.Drawing.Size(383, 6)
        '
        'ResumenDiarioToolStripMenuItem
        '
        Me.ResumenDiarioToolStripMenuItem.ForeColor = System.Drawing.SystemColors.GrayText
        Me.ResumenDiarioToolStripMenuItem.Name = "ResumenDiarioToolStripMenuItem"
        Me.ResumenDiarioToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ResumenDiarioToolStripMenuItem.Text = "Resumen Diario"
        '
        'ToolStripSeparator21
        '
        Me.ToolStripSeparator21.Name = "ToolStripSeparator21"
        Me.ToolStripSeparator21.Size = New System.Drawing.Size(383, 6)
        '
        'ListadosToolStripMenuItem
        '
        Me.ListadosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ListadosToolStripMenuItem.Name = "ListadosToolStripMenuItem"
        Me.ListadosToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ListadosToolStripMenuItem.Tag = "frmListadoCobranzaCredito"
        Me.ListadosToolStripMenuItem.Text = "Listado de Cobranzas"
        '
        'ToolStripMenuItem34
        '
        Me.ToolStripMenuItem34.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ToolStripMenuItem34.Name = "ToolStripMenuItem34"
        Me.ToolStripMenuItem34.Size = New System.Drawing.Size(386, 26)
        Me.ToolStripMenuItem34.Tag = "frmListadoCobranzaContado"
        Me.ToolStripMenuItem34.Text = "Listado de Cobranzas por Lote"
        '
        'ListadoDeDepositosBancariosToolStripMenuItem
        '
        Me.ListadoDeDepositosBancariosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ListadoDeDepositosBancariosToolStripMenuItem.Name = "ListadoDeDepositosBancariosToolStripMenuItem"
        Me.ListadoDeDepositosBancariosToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ListadoDeDepositosBancariosToolStripMenuItem.Tag = "frmListadoDepositoBancario"
        Me.ListadoDeDepositosBancariosToolStripMenuItem.Text = "Listado de Depositos Bancarios"
        '
        'ListadoDeDebitosYCreditosBancariosToolStripMenuItem
        '
        Me.ListadoDeDebitosYCreditosBancariosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ListadoDeDebitosYCreditosBancariosToolStripMenuItem.Name = "ListadoDeDebitosYCreditosBancariosToolStripMenuItem"
        Me.ListadoDeDebitosYCreditosBancariosToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ListadoDeDebitosYCreditosBancariosToolStripMenuItem.Tag = "frmListadoDebitoCreditoBancario"
        Me.ListadoDeDebitosYCreditosBancariosToolStripMenuItem.Text = "Listado de Debitos y Creditos Bancarios"
        '
        'ListadoDeOrdenDePagoToolStripMenuItem
        '
        Me.ListadoDeOrdenDePagoToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ListadoDeOrdenDePagoToolStripMenuItem.Name = "ListadoDeOrdenDePagoToolStripMenuItem"
        Me.ListadoDeOrdenDePagoToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ListadoDeOrdenDePagoToolStripMenuItem.Tag = "frmListadoOrdenPago"
        Me.ListadoDeOrdenDePagoToolStripMenuItem.Text = "Listado de Orden de Pago"
        '
        'ListadoDeGastosToolStripMenuItem
        '
        Me.ListadoDeGastosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ListadoDeGastosToolStripMenuItem.Name = "ListadoDeGastosToolStripMenuItem"
        Me.ListadoDeGastosToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ListadoDeGastosToolStripMenuItem.Tag = "frmListadoGasto"
        Me.ListadoDeGastosToolStripMenuItem.Text = "Listado de Gastos"
        '
        'ListadoDeRetencionesRecibidasToolStripMenuItem
        '
        Me.ListadoDeRetencionesRecibidasToolStripMenuItem.Name = "ListadoDeRetencionesRecibidasToolStripMenuItem"
        Me.ListadoDeRetencionesRecibidasToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ListadoDeRetencionesRecibidasToolStripMenuItem.Tag = "frmListadoRetencionRecibida"
        Me.ListadoDeRetencionesRecibidasToolStripMenuItem.Text = "Listado de Retenciones Recibidas"
        '
        'LIstadoDeChequesEmitidosToolStripMenuItem
        '
        Me.LIstadoDeChequesEmitidosToolStripMenuItem.Name = "LIstadoDeChequesEmitidosToolStripMenuItem"
        Me.LIstadoDeChequesEmitidosToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.LIstadoDeChequesEmitidosToolStripMenuItem.Tag = "frmListadoChequesEmitidos"
        Me.LIstadoDeChequesEmitidosToolStripMenuItem.Text = "LIstado de Cheques Emitidos"
        '
        'ListadoDeRetencionesEmitidasToolStripMenuItem
        '
        Me.ListadoDeRetencionesEmitidasToolStripMenuItem.Name = "ListadoDeRetencionesEmitidasToolStripMenuItem"
        Me.ListadoDeRetencionesEmitidasToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ListadoDeRetencionesEmitidasToolStripMenuItem.Tag = "frmRetencionEmitida"
        Me.ListadoDeRetencionesEmitidasToolStripMenuItem.Text = "Listado de Retenciones Emitidas"
        '
        'ListadoDeRecibosGeneradosToolStripMenuItem
        '
        Me.ListadoDeRecibosGeneradosToolStripMenuItem.Name = "ListadoDeRecibosGeneradosToolStripMenuItem"
        Me.ListadoDeRecibosGeneradosToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ListadoDeRecibosGeneradosToolStripMenuItem.Tag = "frmListadoRecibosGenerados"
        Me.ListadoDeRecibosGeneradosToolStripMenuItem.Text = "Listado de Recibos Generados"
        '
        'ListadoDeGastosAcuerdoComercialToolStripMenuItem
        '
        Me.ListadoDeGastosAcuerdoComercialToolStripMenuItem.Name = "ListadoDeGastosAcuerdoComercialToolStripMenuItem"
        Me.ListadoDeGastosAcuerdoComercialToolStripMenuItem.Size = New System.Drawing.Size(386, 26)
        Me.ListadoDeGastosAcuerdoComercialToolStripMenuItem.Tag = "frmListadoGastoAcuerdoComercial"
        Me.ListadoDeGastosAcuerdoComercialToolStripMenuItem.Text = "Listado de Gastos / Acuerdo Comercial"
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(326, 6)
        '
        'AsociarRetencionesDeClientesToolStripMenuItem
        '
        Me.AsociarRetencionesDeClientesToolStripMenuItem.Name = "AsociarRetencionesDeClientesToolStripMenuItem"
        Me.AsociarRetencionesDeClientesToolStripMenuItem.Size = New System.Drawing.Size(329, 26)
        Me.AsociarRetencionesDeClientesToolStripMenuItem.Tag = "frmAsociarRetencionCliente"
        Me.AsociarRetencionesDeClientesToolStripMenuItem.Text = "Asociar Retenciones de Clientes"
        '
        'InformeFinancieroToolStripMenuItem
        '
        Me.InformeFinancieroToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresoTotalToolStripMenuItem, Me.ControlTesoreriaToolStripMenuItem})
        Me.InformeFinancieroToolStripMenuItem.Name = "InformeFinancieroToolStripMenuItem"
        Me.InformeFinancieroToolStripMenuItem.Size = New System.Drawing.Size(329, 26)
        Me.InformeFinancieroToolStripMenuItem.Text = "Informe Financiero"
        '
        'IngresoTotalToolStripMenuItem
        '
        Me.IngresoTotalToolStripMenuItem.Name = "IngresoTotalToolStripMenuItem"
        Me.IngresoTotalToolStripMenuItem.Size = New System.Drawing.Size(216, 26)
        Me.IngresoTotalToolStripMenuItem.Tag = "frmInformeFinancieroIngresos"
        Me.IngresoTotalToolStripMenuItem.Text = "Ingreso Total"
        '
        'ControlTesoreriaToolStripMenuItem
        '
        Me.ControlTesoreriaToolStripMenuItem.Name = "ControlTesoreriaToolStripMenuItem"
        Me.ControlTesoreriaToolStripMenuItem.Size = New System.Drawing.Size(216, 26)
        Me.ControlTesoreriaToolStripMenuItem.Tag = "frmInformeControlTesoreria"
        Me.ControlTesoreriaToolStripMenuItem.Text = "Control Tesoreria"
        '
        'ContabilidadToolStripMenuItem
        '
        Me.ContabilidadToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CuentasFujasToolStripMenuItem, Me.AsientoToolStripMenuItem, Me.PlanDeCuentasContablesToolStripMenuItem, Me.UnidadDeNegociosToolStripMenuItem, Me.CentroDeCostosToolStripMenuItem, Me.ToolStripSeparator36, Me.ComprobantesParaLibroVentaYCompraToolStripMenuItem, Me.ConciliarAsientosToolStripMenuItem, Me.ToolStripMenuItem35, Me.ProcesosToolStripMenuItem, Me.ToolStripSeparator40, Me.HechaukaToolStripMenuItem, Me.ToolStripMenuItem40, Me.InformesUnidadNegocioCCToolStripMenuItem, Me.ControlDeIntegridadToolStripMenuItem, Me.ControlDeUnidadNegocioYDepartamentoToolStripMenuItem, Me.Resolución49ToolStripMenuItem})
        Me.ContabilidadToolStripMenuItem.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContabilidadToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ContabilidadToolStripMenuItem.Name = "ContabilidadToolStripMenuItem"
        Me.ContabilidadToolStripMenuItem.Size = New System.Drawing.Size(113, 24)
        Me.ContabilidadToolStripMenuItem.Text = "Contabilidad"
        '
        'CuentasFujasToolStripMenuItem
        '
        Me.CuentasFujasToolStripMenuItem.Name = "CuentasFujasToolStripMenuItem"
        Me.CuentasFujasToolStripMenuItem.Size = New System.Drawing.Size(409, 26)
        Me.CuentasFujasToolStripMenuItem.Tag = "frmCuentaFijas"
        Me.CuentasFujasToolStripMenuItem.Text = "Cuentas Fijas para Asientos"
        '
        'AsientoToolStripMenuItem
        '
        Me.AsientoToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.AsientoToolStripMenuItem.Name = "AsientoToolStripMenuItem"
        Me.AsientoToolStripMenuItem.Size = New System.Drawing.Size(409, 26)
        Me.AsientoToolStripMenuItem.Tag = "frmConsultaAsientoContable"
        Me.AsientoToolStripMenuItem.Text = "Asiento"
        '
        'PlanDeCuentasContablesToolStripMenuItem
        '
        Me.PlanDeCuentasContablesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AdministrarToolStripMenuItem, Me.ToolStripMenuItem31})
        Me.PlanDeCuentasContablesToolStripMenuItem.Name = "PlanDeCuentasContablesToolStripMenuItem"
        Me.PlanDeCuentasContablesToolStripMenuItem.Size = New System.Drawing.Size(409, 26)
        Me.PlanDeCuentasContablesToolStripMenuItem.Tag = ""
        Me.PlanDeCuentasContablesToolStripMenuItem.Text = "Plan de Cuentas"
        '
        'AdministrarToolStripMenuItem
        '
        Me.AdministrarToolStripMenuItem.Name = "AdministrarToolStripMenuItem"
        Me.AdministrarToolStripMenuItem.Size = New System.Drawing.Size(303, 26)
        Me.AdministrarToolStripMenuItem.Tag = "frmCuentaContable"
        Me.AdministrarToolStripMenuItem.Text = "Administrar"
        '
        'ToolStripMenuItem31
        '
        Me.ToolStripMenuItem31.Name = "ToolStripMenuItem31"
        Me.ToolStripMenuItem31.Size = New System.Drawing.Size(303, 26)
        Me.ToolStripMenuItem31.Tag = "frmCambiarPlanCuenta"
        Me.ToolStripMenuItem31.Text = "Establecer el Plan de Cuenta"
        '
        'UnidadDeNegociosToolStripMenuItem
        '
        Me.UnidadDeNegociosToolStripMenuItem.Name = "UnidadDeNegociosToolStripMenuItem"
        Me.UnidadDeNegociosToolStripMenuItem.Size = New System.Drawing.Size(409, 26)
        Me.UnidadDeNegociosToolStripMenuItem.Tag = "frmUnidadDeNegocioAsiento"
        Me.UnidadDeNegociosToolStripMenuItem.Text = "Unidad de Negocio"
        '
        'CentroDeCostosToolStripMenuItem
        '
        Me.CentroDeCostosToolStripMenuItem.Name = "CentroDeCostosToolStripMenuItem"
        Me.CentroDeCostosToolStripMenuItem.Size = New System.Drawing.Size(409, 26)
        Me.CentroDeCostosToolStripMenuItem.Tag = "frmCentroCostoAsiento"
        Me.CentroDeCostosToolStripMenuItem.Text = "Centro de Costos"
        '
        'ToolStripSeparator36
        '
        Me.ToolStripSeparator36.Name = "ToolStripSeparator36"
        Me.ToolStripSeparator36.Size = New System.Drawing.Size(406, 6)
        '
        'ComprobantesParaLibroVentaYCompraToolStripMenuItem
        '
        Me.ComprobantesParaLibroVentaYCompraToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ComprobantesParaLibroVentaYCompraToolStripMenuItem.Name = "ComprobantesParaLibroVentaYCompraToolStripMenuItem"
        Me.ComprobantesParaLibroVentaYCompraToolStripMenuItem.Size = New System.Drawing.Size(409, 26)
        Me.ComprobantesParaLibroVentaYCompraToolStripMenuItem.Tag = "frmConsultaComprobanteLibroIVA"
        Me.ComprobantesParaLibroVentaYCompraToolStripMenuItem.Text = "Comprobantes para libro venta y compra"
        '
        'ConciliarAsientosToolStripMenuItem
        '
        Me.ConciliarAsientosToolStripMenuItem.Name = "ConciliarAsientosToolStripMenuItem"
        Me.ConciliarAsientosToolStripMenuItem.Size = New System.Drawing.Size(409, 26)
        Me.ConciliarAsientosToolStripMenuItem.Tag = "frmConciliarAsiento"
        Me.ConciliarAsientosToolStripMenuItem.Text = "Conciliar Asientos"
        '
        'ToolStripMenuItem35
        '
        Me.ToolStripMenuItem35.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CierreDiarioToolStripMenuItem, Me.CierreMensualToolStripMenuItem, Me.CierreEjercicioToolStripMenuItem})
        Me.ToolStripMenuItem35.Name = "ToolStripMenuItem35"
        Me.ToolStripMenuItem35.Size = New System.Drawing.Size(409, 26)
        Me.ToolStripMenuItem35.Tag = "frmCuentaFijas"
        Me.ToolStripMenuItem35.Text = "Cierre Contable"
        '
        'CierreDiarioToolStripMenuItem
        '
        Me.CierreDiarioToolStripMenuItem.Name = "CierreDiarioToolStripMenuItem"
        Me.CierreDiarioToolStripMenuItem.Size = New System.Drawing.Size(207, 26)
        Me.CierreDiarioToolStripMenuItem.Tag = "frmCierreDiario"
        Me.CierreDiarioToolStripMenuItem.Text = "Cierre Diario"
        '
        'CierreMensualToolStripMenuItem
        '
        Me.CierreMensualToolStripMenuItem.Name = "CierreMensualToolStripMenuItem"
        Me.CierreMensualToolStripMenuItem.Size = New System.Drawing.Size(207, 26)
        Me.CierreMensualToolStripMenuItem.Tag = "frmCierreMensual"
        Me.CierreMensualToolStripMenuItem.Text = "Cierre Mensual"
        '
        'CierreEjercicioToolStripMenuItem
        '
        Me.CierreEjercicioToolStripMenuItem.Name = "CierreEjercicioToolStripMenuItem"
        Me.CierreEjercicioToolStripMenuItem.Size = New System.Drawing.Size(207, 26)
        Me.CierreEjercicioToolStripMenuItem.Tag = "frmCierreEjercicio"
        Me.CierreEjercicioToolStripMenuItem.Text = "Cierre Ejercicio"
        '
        'ProcesosToolStripMenuItem
        '
        Me.ProcesosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CambioGlobalDeCuentaContableToolStripMenuItem, Me.RegenerarAsientosToolStripMenuItem, Me.RenumeracionToolStripMenuItem, Me.RegenerarSaldosToolStripMenuItem, Me.CierreYReaperturaToolStripMenuItem, Me.KardexToolStripMenuItem})
        Me.ProcesosToolStripMenuItem.Name = "ProcesosToolStripMenuItem"
        Me.ProcesosToolStripMenuItem.Size = New System.Drawing.Size(409, 26)
        Me.ProcesosToolStripMenuItem.Text = "Procesos"
        '
        'CambioGlobalDeCuentaContableToolStripMenuItem
        '
        Me.CambioGlobalDeCuentaContableToolStripMenuItem.Name = "CambioGlobalDeCuentaContableToolStripMenuItem"
        Me.CambioGlobalDeCuentaContableToolStripMenuItem.Size = New System.Drawing.Size(347, 26)
        Me.CambioGlobalDeCuentaContableToolStripMenuItem.Tag = "frmCambioGlobalCuentaContable"
        Me.CambioGlobalDeCuentaContableToolStripMenuItem.Text = "Cambio Global de Cuenta Contable"
        '
        'RegenerarAsientosToolStripMenuItem
        '
        Me.RegenerarAsientosToolStripMenuItem.Name = "RegenerarAsientosToolStripMenuItem"
        Me.RegenerarAsientosToolStripMenuItem.Size = New System.Drawing.Size(347, 26)
        Me.RegenerarAsientosToolStripMenuItem.Tag = "frmRegererarAsiento"
        Me.RegenerarAsientosToolStripMenuItem.Text = "Regenerar Asientos"
        '
        'RenumeracionToolStripMenuItem
        '
        Me.RenumeracionToolStripMenuItem.Name = "RenumeracionToolStripMenuItem"
        Me.RenumeracionToolStripMenuItem.Size = New System.Drawing.Size(347, 26)
        Me.RenumeracionToolStripMenuItem.Tag = "frmRenumeracionAsiento"
        Me.RenumeracionToolStripMenuItem.Text = "Renumeracion de Asientos"
        '
        'RegenerarSaldosToolStripMenuItem
        '
        Me.RegenerarSaldosToolStripMenuItem.Name = "RegenerarSaldosToolStripMenuItem"
        Me.RegenerarSaldosToolStripMenuItem.Size = New System.Drawing.Size(347, 26)
        Me.RegenerarSaldosToolStripMenuItem.Tag = "frmRecalcularPlanCuentaSaldo"
        Me.RegenerarSaldosToolStripMenuItem.Text = "Recalcular Saldos"
        '
        'CierreYReaperturaToolStripMenuItem
        '
        Me.CierreYReaperturaToolStripMenuItem.Enabled = False
        Me.CierreYReaperturaToolStripMenuItem.Name = "CierreYReaperturaToolStripMenuItem"
        Me.CierreYReaperturaToolStripMenuItem.Size = New System.Drawing.Size(347, 26)
        Me.CierreYReaperturaToolStripMenuItem.Tag = "frmAperturaCierre"
        Me.CierreYReaperturaToolStripMenuItem.Text = "Reapertura"
        '
        'KardexToolStripMenuItem
        '
        Me.KardexToolStripMenuItem.Name = "KardexToolStripMenuItem"
        Me.KardexToolStripMenuItem.Size = New System.Drawing.Size(347, 26)
        Me.KardexToolStripMenuItem.Text = "Kardex"
        '
        'ToolStripSeparator40
        '
        Me.ToolStripSeparator40.Name = "ToolStripSeparator40"
        Me.ToolStripSeparator40.Size = New System.Drawing.Size(406, 6)
        '
        'HechaukaToolStripMenuItem
        '
        Me.HechaukaToolStripMenuItem.Name = "HechaukaToolStripMenuItem"
        Me.HechaukaToolStripMenuItem.Size = New System.Drawing.Size(409, 26)
        Me.HechaukaToolStripMenuItem.Tag = "frmHechaukaLibroVentas1"
        Me.HechaukaToolStripMenuItem.Text = "Hechauka"
        '
        'ToolStripMenuItem40
        '
        Me.ToolStripMenuItem40.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem41, Me.ToolStripSeparator39, Me.ToolStripMenuItem42, Me.LibroDiarioToolStripMenuItem, Me.ToolStripMenuItem43, Me.LibroMayorDetalladoPorProductoToolStripMenuItem, Me.ToolStripSeparator45, Me.ToolStripMenuItem44, Me.DocumentosSinAsientosToolStripMenuItem, Me.ToolStripSeparator48, Me.ToolStripMenuItem45, Me.ToolStripMenuItem46, Me.EvolucionDeSaldosToolStripMenuItem, Me.BalanceRG49ToolStripMenuItem})
        Me.ToolStripMenuItem40.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ToolStripMenuItem40.Name = "ToolStripMenuItem40"
        Me.ToolStripMenuItem40.Size = New System.Drawing.Size(409, 26)
        Me.ToolStripMenuItem40.Text = "Informes"
        '
        'ToolStripMenuItem41
        '
        Me.ToolStripMenuItem41.Name = "ToolStripMenuItem41"
        Me.ToolStripMenuItem41.Size = New System.Drawing.Size(288, 26)
        Me.ToolStripMenuItem41.Tag = "frmAsientoDiario"
        Me.ToolStripMenuItem41.Text = "Asientos Contables"
        '
        'ToolStripSeparator39
        '
        Me.ToolStripSeparator39.Name = "ToolStripSeparator39"
        Me.ToolStripSeparator39.Size = New System.Drawing.Size(285, 6)
        '
        'ToolStripMenuItem42
        '
        Me.ToolStripMenuItem42.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ToolStripMenuItem42.Name = "ToolStripMenuItem42"
        Me.ToolStripMenuItem42.Size = New System.Drawing.Size(288, 26)
        Me.ToolStripMenuItem42.Tag = "frmLibroIVA"
        Me.ToolStripMenuItem42.Text = "Libro IVA"
        '
        'LibroDiarioToolStripMenuItem
        '
        Me.LibroDiarioToolStripMenuItem.Name = "LibroDiarioToolStripMenuItem"
        Me.LibroDiarioToolStripMenuItem.Size = New System.Drawing.Size(288, 26)
        Me.LibroDiarioToolStripMenuItem.Tag = "frmLibroDiario"
        Me.LibroDiarioToolStripMenuItem.Text = "Libro Diario"
        '
        'ToolStripMenuItem43
        '
        Me.ToolStripMenuItem43.Name = "ToolStripMenuItem43"
        Me.ToolStripMenuItem43.Size = New System.Drawing.Size(288, 26)
        Me.ToolStripMenuItem43.Tag = "frmLibroMayor"
        Me.ToolStripMenuItem43.Text = "Libro Mayor"
        '
        'LibroMayorDetalladoPorProductoToolStripMenuItem
        '
        Me.LibroMayorDetalladoPorProductoToolStripMenuItem.Name = "LibroMayorDetalladoPorProductoToolStripMenuItem"
        Me.LibroMayorDetalladoPorProductoToolStripMenuItem.Size = New System.Drawing.Size(288, 26)
        Me.LibroMayorDetalladoPorProductoToolStripMenuItem.Tag = "frmLibroMayorDetalle"
        Me.LibroMayorDetalladoPorProductoToolStripMenuItem.Text = "Libro Mayor Detallado"
        '
        'ToolStripSeparator45
        '
        Me.ToolStripSeparator45.Name = "ToolStripSeparator45"
        Me.ToolStripSeparator45.Size = New System.Drawing.Size(285, 6)
        '
        'ToolStripMenuItem44
        '
        Me.ToolStripMenuItem44.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ToolStripMenuItem44.Name = "ToolStripMenuItem44"
        Me.ToolStripMenuItem44.Size = New System.Drawing.Size(288, 26)
        Me.ToolStripMenuItem44.Tag = "frmComprobantesNoEmitidos"
        Me.ToolStripMenuItem44.Text = "Comprobantes no Emitidos"
        '
        'DocumentosSinAsientosToolStripMenuItem
        '
        Me.DocumentosSinAsientosToolStripMenuItem.Name = "DocumentosSinAsientosToolStripMenuItem"
        Me.DocumentosSinAsientosToolStripMenuItem.Size = New System.Drawing.Size(288, 26)
        Me.DocumentosSinAsientosToolStripMenuItem.Tag = "frmDocumentoSinAsiento"
        Me.DocumentosSinAsientosToolStripMenuItem.Text = "Documentos sin Asientos"
        '
        'ToolStripSeparator48
        '
        Me.ToolStripSeparator48.Name = "ToolStripSeparator48"
        Me.ToolStripSeparator48.Size = New System.Drawing.Size(285, 6)
        '
        'ToolStripMenuItem45
        '
        Me.ToolStripMenuItem45.Name = "ToolStripMenuItem45"
        Me.ToolStripMenuItem45.Size = New System.Drawing.Size(288, 26)
        Me.ToolStripMenuItem45.Tag = "frmBalanceGeneralEstadoResultado"
        Me.ToolStripMenuItem45.Text = "Balance General"
        '
        'ToolStripMenuItem46
        '
        Me.ToolStripMenuItem46.Name = "ToolStripMenuItem46"
        Me.ToolStripMenuItem46.Size = New System.Drawing.Size(288, 26)
        Me.ToolStripMenuItem46.Tag = "frmBalanceComprobacion"
        Me.ToolStripMenuItem46.Text = "Balance de Comprobacion"
        '
        'EvolucionDeSaldosToolStripMenuItem
        '
        Me.EvolucionDeSaldosToolStripMenuItem.Name = "EvolucionDeSaldosToolStripMenuItem"
        Me.EvolucionDeSaldosToolStripMenuItem.Size = New System.Drawing.Size(288, 26)
        Me.EvolucionDeSaldosToolStripMenuItem.Text = "Evolucion de Saldos"
        '
        'BalanceRG49ToolStripMenuItem
        '
        Me.BalanceRG49ToolStripMenuItem.Name = "BalanceRG49ToolStripMenuItem"
        Me.BalanceRG49ToolStripMenuItem.Size = New System.Drawing.Size(288, 26)
        Me.BalanceRG49ToolStripMenuItem.Text = "Balance RG 49"
        '
        'InformesUnidadNegocioCCToolStripMenuItem
        '
        Me.InformesUnidadNegocioCCToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LibroMayorToolStripMenuItem1, Me.ResumenDeSaldosUnidadDeNegociosToolStripMenuItem})
        Me.InformesUnidadNegocioCCToolStripMenuItem.Name = "InformesUnidadNegocioCCToolStripMenuItem"
        Me.InformesUnidadNegocioCCToolStripMenuItem.Size = New System.Drawing.Size(409, 26)
        Me.InformesUnidadNegocioCCToolStripMenuItem.Text = "Informes Unidad Negocio - CC"
        '
        'LibroMayorToolStripMenuItem1
        '
        Me.LibroMayorToolStripMenuItem1.Name = "LibroMayorToolStripMenuItem1"
        Me.LibroMayorToolStripMenuItem1.Size = New System.Drawing.Size(389, 26)
        Me.LibroMayorToolStripMenuItem1.Tag = "frmLibroMayorUNCC"
        Me.LibroMayorToolStripMenuItem1.Text = "Libro Mayor"
        '
        'ResumenDeSaldosUnidadDeNegociosToolStripMenuItem
        '
        Me.ResumenDeSaldosUnidadDeNegociosToolStripMenuItem.Name = "ResumenDeSaldosUnidadDeNegociosToolStripMenuItem"
        Me.ResumenDeSaldosUnidadDeNegociosToolStripMenuItem.Size = New System.Drawing.Size(389, 26)
        Me.ResumenDeSaldosUnidadDeNegociosToolStripMenuItem.Tag = "frmResumenUnidadNegocio1"
        Me.ResumenDeSaldosUnidadDeNegociosToolStripMenuItem.Text = "Resumen de Saldos Unidad de Negocios"
        '
        'ControlDeIntegridadToolStripMenuItem
        '
        Me.ControlDeIntegridadToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExistenciasToolStripMenuItem})
        Me.ControlDeIntegridadToolStripMenuItem.Name = "ControlDeIntegridadToolStripMenuItem"
        Me.ControlDeIntegridadToolStripMenuItem.Size = New System.Drawing.Size(409, 26)
        Me.ControlDeIntegridadToolStripMenuItem.Text = "Control de Integridad"
        '
        'ExistenciasToolStripMenuItem
        '
        Me.ExistenciasToolStripMenuItem.Name = "ExistenciasToolStripMenuItem"
        Me.ExistenciasToolStripMenuItem.Size = New System.Drawing.Size(174, 26)
        Me.ExistenciasToolStripMenuItem.Tag = "frmControlIntegridad"
        Me.ExistenciasToolStripMenuItem.Text = "Existencias"
        '
        'ControlDeUnidadNegocioYDepartamentoToolStripMenuItem
        '
        Me.ControlDeUnidadNegocioYDepartamentoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ModificarGastosToolStripMenuItem, Me.ModificarFondoFijoToolStripMenuItem, Me.ModificarCompraToolStripMenuItem, Me.ModificarGastosRRHHToolStripMenuItem, Me.ToolStripSeparator55, Me.ModificarModuloStockToolStripMenuItem})
        Me.ControlDeUnidadNegocioYDepartamentoToolStripMenuItem.Name = "ControlDeUnidadNegocioYDepartamentoToolStripMenuItem"
        Me.ControlDeUnidadNegocioYDepartamentoToolStripMenuItem.Size = New System.Drawing.Size(409, 26)
        Me.ControlDeUnidadNegocioYDepartamentoToolStripMenuItem.Text = "Control de Unidad Negocio y Departamento"
        '
        'ModificarGastosToolStripMenuItem
        '
        Me.ModificarGastosToolStripMenuItem.Name = "ModificarGastosToolStripMenuItem"
        Me.ModificarGastosToolStripMenuItem.Size = New System.Drawing.Size(264, 26)
        Me.ModificarGastosToolStripMenuItem.Tag = "frmGastoContabilidad"
        Me.ModificarGastosToolStripMenuItem.Text = "Modificar Gastos"
        '
        'ModificarFondoFijoToolStripMenuItem
        '
        Me.ModificarFondoFijoToolStripMenuItem.Name = "ModificarFondoFijoToolStripMenuItem"
        Me.ModificarFondoFijoToolStripMenuItem.Size = New System.Drawing.Size(264, 26)
        Me.ModificarFondoFijoToolStripMenuItem.Tag = "frmGastoContabilidad"
        Me.ModificarFondoFijoToolStripMenuItem.Text = "Modificar Fondo Fijo"
        '
        'ModificarCompraToolStripMenuItem
        '
        Me.ModificarCompraToolStripMenuItem.Name = "ModificarCompraToolStripMenuItem"
        Me.ModificarCompraToolStripMenuItem.Size = New System.Drawing.Size(264, 26)
        Me.ModificarCompraToolStripMenuItem.Tag = "frmCompraContabilidad"
        Me.ModificarCompraToolStripMenuItem.Text = "Modificar Compra"
        '
        'ModificarGastosRRHHToolStripMenuItem
        '
        Me.ModificarGastosRRHHToolStripMenuItem.Name = "ModificarGastosRRHHToolStripMenuItem"
        Me.ModificarGastosRRHHToolStripMenuItem.Size = New System.Drawing.Size(264, 26)
        Me.ModificarGastosRRHHToolStripMenuItem.Tag = "frmGastoContabilidad"
        Me.ModificarGastosRRHHToolStripMenuItem.Text = "Modificar Gastos RRHH"
        '
        'ToolStripSeparator55
        '
        Me.ToolStripSeparator55.Name = "ToolStripSeparator55"
        Me.ToolStripSeparator55.Size = New System.Drawing.Size(261, 6)
        '
        'ModificarModuloStockToolStripMenuItem
        '
        Me.ModificarModuloStockToolStripMenuItem.Name = "ModificarModuloStockToolStripMenuItem"
        Me.ModificarModuloStockToolStripMenuItem.Size = New System.Drawing.Size(264, 26)
        Me.ModificarModuloStockToolStripMenuItem.Tag = "frmModificarMovimiento"
        Me.ModificarModuloStockToolStripMenuItem.Text = "Modificar Modulo Stock"
        '
        'Resolución49ToolStripMenuItem
        '
        Me.Resolución49ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AdministrarToolStripMenuItem2, Me.AsignacionDeCuentasRG49ToolStripMenuItem})
        Me.Resolución49ToolStripMenuItem.Name = "Resolución49ToolStripMenuItem"
        Me.Resolución49ToolStripMenuItem.Size = New System.Drawing.Size(409, 26)
        Me.Resolución49ToolStripMenuItem.Text = "Resolución 49"
        '
        'AdministrarToolStripMenuItem2
        '
        Me.AdministrarToolStripMenuItem2.Name = "AdministrarToolStripMenuItem2"
        Me.AdministrarToolStripMenuItem2.Size = New System.Drawing.Size(312, 26)
        Me.AdministrarToolStripMenuItem2.Tag = "frmResolucion49"
        Me.AdministrarToolStripMenuItem2.Text = "Administrar"
        '
        'AsignacionDeCuentasRG49ToolStripMenuItem
        '
        Me.AsignacionDeCuentasRG49ToolStripMenuItem.Name = "AsignacionDeCuentasRG49ToolStripMenuItem"
        Me.AsignacionDeCuentasRG49ToolStripMenuItem.Size = New System.Drawing.Size(312, 26)
        Me.AsignacionDeCuentasRG49ToolStripMenuItem.Tag = "frmAsignarCuentaRG49"
        Me.AsignacionDeCuentasRG49ToolStripMenuItem.Text = "Asignacion de Cuentas RG 49"
        '
        'RRHHToolStripMenuItem
        '
        Me.RRHHToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ComprobantesToolStripMenuItem, Me.DepartamentosToolStripMenuItem, Me.SeccionToolStripMenuItem, Me.ImportarAsientoToolStripMenuItem})
        Me.RRHHToolStripMenuItem.Name = "RRHHToolStripMenuItem"
        Me.RRHHToolStripMenuItem.Size = New System.Drawing.Size(67, 24)
        Me.RRHHToolStripMenuItem.Text = "RRHH"
        '
        'ComprobantesToolStripMenuItem
        '
        Me.ComprobantesToolStripMenuItem.Name = "ComprobantesToolStripMenuItem"
        Me.ComprobantesToolStripMenuItem.Size = New System.Drawing.Size(211, 26)
        Me.ComprobantesToolStripMenuItem.Tag = "frmGastos"
        Me.ComprobantesToolStripMenuItem.Text = "Comprobantes"
        '
        'DepartamentosToolStripMenuItem
        '
        Me.DepartamentosToolStripMenuItem.Name = "DepartamentosToolStripMenuItem"
        Me.DepartamentosToolStripMenuItem.Size = New System.Drawing.Size(211, 26)
        Me.DepartamentosToolStripMenuItem.Tag = "frmDepartamentoEmpresa"
        Me.DepartamentosToolStripMenuItem.Text = "Departamentos"
        '
        'SeccionToolStripMenuItem
        '
        Me.SeccionToolStripMenuItem.Name = "SeccionToolStripMenuItem"
        Me.SeccionToolStripMenuItem.Size = New System.Drawing.Size(211, 26)
        Me.SeccionToolStripMenuItem.Text = "Seccion"
        '
        'ImportarAsientoToolStripMenuItem
        '
        Me.ImportarAsientoToolStripMenuItem.Name = "ImportarAsientoToolStripMenuItem"
        Me.ImportarAsientoToolStripMenuItem.Size = New System.Drawing.Size(211, 26)
        Me.ImportarAsientoToolStripMenuItem.Tag = "frmImportarAsientoRRHH"
        Me.ImportarAsientoToolStripMenuItem.Text = "Importar Asiento"
        '
        'CalidadToolStripMenuItem
        '
        Me.CalidadToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MuestraParaPruebaToolStripMenuItem, Me.AprobarPedidosDeNotaDeCreditoToolStripMenuItem, Me.InformesToolStripMenuItem4, Me.ConfiguracionDeMuestraToolStripMenuItem})
        Me.CalidadToolStripMenuItem.Name = "CalidadToolStripMenuItem"
        Me.CalidadToolStripMenuItem.Size = New System.Drawing.Size(78, 24)
        Me.CalidadToolStripMenuItem.Text = "Calidad"
        '
        'MuestraParaPruebaToolStripMenuItem
        '
        Me.MuestraParaPruebaToolStripMenuItem.Name = "MuestraParaPruebaToolStripMenuItem"
        Me.MuestraParaPruebaToolStripMenuItem.Size = New System.Drawing.Size(323, 26)
        Me.MuestraParaPruebaToolStripMenuItem.Tag = "frmMuestraParaPrueba"
        Me.MuestraParaPruebaToolStripMenuItem.Text = "Muestra Para Prueba"
        '
        'AprobarPedidosDeNotaDeCreditoToolStripMenuItem
        '
        Me.AprobarPedidosDeNotaDeCreditoToolStripMenuItem.Name = "AprobarPedidosDeNotaDeCreditoToolStripMenuItem"
        Me.AprobarPedidosDeNotaDeCreditoToolStripMenuItem.Size = New System.Drawing.Size(323, 26)
        Me.AprobarPedidosDeNotaDeCreditoToolStripMenuItem.Tag = "frmAprobarPedidoDevolucion"
        Me.AprobarPedidosDeNotaDeCreditoToolStripMenuItem.Text = "Aprobar Pedidos de Devolucion"
        '
        'InformesToolStripMenuItem4
        '
        Me.InformesToolStripMenuItem4.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListadoDeMuestrasToolStripMenuItem, Me.ListadoDePedidosDeDevolucionToolStripMenuItem})
        Me.InformesToolStripMenuItem4.Name = "InformesToolStripMenuItem4"
        Me.InformesToolStripMenuItem4.Size = New System.Drawing.Size(323, 26)
        Me.InformesToolStripMenuItem4.Text = "Informes"
        '
        'ListadoDeMuestrasToolStripMenuItem
        '
        Me.ListadoDeMuestrasToolStripMenuItem.Name = "ListadoDeMuestrasToolStripMenuItem"
        Me.ListadoDeMuestrasToolStripMenuItem.Size = New System.Drawing.Size(339, 26)
        Me.ListadoDeMuestrasToolStripMenuItem.Text = "Listado de Muestras"
        '
        'ListadoDePedidosDeDevolucionToolStripMenuItem
        '
        Me.ListadoDePedidosDeDevolucionToolStripMenuItem.Name = "ListadoDePedidosDeDevolucionToolStripMenuItem"
        Me.ListadoDePedidosDeDevolucionToolStripMenuItem.Size = New System.Drawing.Size(339, 26)
        Me.ListadoDePedidosDeDevolucionToolStripMenuItem.Text = "Listado de Pedidos de Devolucion"
        '
        'ConfiguracionDeMuestraToolStripMenuItem
        '
        Me.ConfiguracionDeMuestraToolStripMenuItem.Name = "ConfiguracionDeMuestraToolStripMenuItem"
        Me.ConfiguracionDeMuestraToolStripMenuItem.Size = New System.Drawing.Size(323, 26)
        Me.ConfiguracionDeMuestraToolStripMenuItem.Tag = "frmConfiguracionMuestraPanaderia"
        Me.ConfiguracionDeMuestraToolStripMenuItem.Text = "Configuracion de Muestra"
        '
        'InformesToolStripMenuItem1
        '
        Me.InformesToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientesToolStripMenuItem1, Me.ProveedoresToolStripMenuItem1, Me.ProductoiToolStripMenuItem, Me.DiseñadorToolStripMenuItem, Me.UnileverToolStripMenuItem, Me.AuditoriaToolStripMenuItem})
        Me.InformesToolStripMenuItem1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InformesToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.InformesToolStripMenuItem1.Name = "InformesToolStripMenuItem1"
        Me.InformesToolStripMenuItem1.Size = New System.Drawing.Size(87, 24)
        Me.InformesToolStripMenuItem1.Text = "Informes"
        '
        'ClientesToolStripMenuItem1
        '
        Me.ClientesToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExtractoDeMovimientosToolStripMenuItem, Me.ListadoDeClientesToolStripMenuItem, Me.ListadoDeClientesQueNoCompranToolStripMenuItem, Me.CumpleañosYAniversariosToolStripMenuItem})
        Me.ClientesToolStripMenuItem1.Name = "ClientesToolStripMenuItem1"
        Me.ClientesToolStripMenuItem1.Size = New System.Drawing.Size(184, 26)
        Me.ClientesToolStripMenuItem1.Text = "Clientes"
        '
        'ExtractoDeMovimientosToolStripMenuItem
        '
        Me.ExtractoDeMovimientosToolStripMenuItem.Name = "ExtractoDeMovimientosToolStripMenuItem"
        Me.ExtractoDeMovimientosToolStripMenuItem.Size = New System.Drawing.Size(354, 26)
        Me.ExtractoDeMovimientosToolStripMenuItem.Tag = "frmExtractoMovimientoCliente"
        Me.ExtractoDeMovimientosToolStripMenuItem.Text = "Extracto de Movimientos"
        '
        'ListadoDeClientesToolStripMenuItem
        '
        Me.ListadoDeClientesToolStripMenuItem.Name = "ListadoDeClientesToolStripMenuItem"
        Me.ListadoDeClientesToolStripMenuItem.Size = New System.Drawing.Size(354, 26)
        Me.ListadoDeClientesToolStripMenuItem.Tag = "frmListadoCliente"
        Me.ListadoDeClientesToolStripMenuItem.Text = "Listado de Clientes"
        '
        'ListadoDeClientesQueNoCompranToolStripMenuItem
        '
        Me.ListadoDeClientesQueNoCompranToolStripMenuItem.Name = "ListadoDeClientesQueNoCompranToolStripMenuItem"
        Me.ListadoDeClientesQueNoCompranToolStripMenuItem.Size = New System.Drawing.Size(354, 26)
        Me.ListadoDeClientesQueNoCompranToolStripMenuItem.Tag = "frmListadoClienteNoCompra"
        Me.ListadoDeClientesQueNoCompranToolStripMenuItem.Text = "Listado de Clientes que no compran"
        '
        'CumpleañosYAniversariosToolStripMenuItem
        '
        Me.CumpleañosYAniversariosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CumpleañosYAniversariosToolStripMenuItem.Name = "CumpleañosYAniversariosToolStripMenuItem"
        Me.CumpleañosYAniversariosToolStripMenuItem.Size = New System.Drawing.Size(354, 26)
        Me.CumpleañosYAniversariosToolStripMenuItem.Text = "Cumpleaños y Aniversarios"
        '
        'ProveedoresToolStripMenuItem1
        '
        Me.ProveedoresToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExtractoDeMovimientosToolStripMenuItem1, Me.ListadoDeProveedoresToolStripMenuItem, Me.ExtractoDeMovimientosTodosLosProveedoresToolStripMenuItem, Me.ExtractoDeMovimientoDeDocumentosToolStripMenuItem, Me.RankingDeProveedoresToolStripMenuItem})
        Me.ProveedoresToolStripMenuItem1.Name = "ProveedoresToolStripMenuItem1"
        Me.ProveedoresToolStripMenuItem1.Size = New System.Drawing.Size(184, 26)
        Me.ProveedoresToolStripMenuItem1.Text = "Proveedores"
        '
        'ExtractoDeMovimientosToolStripMenuItem1
        '
        Me.ExtractoDeMovimientosToolStripMenuItem1.Name = "ExtractoDeMovimientosToolStripMenuItem1"
        Me.ExtractoDeMovimientosToolStripMenuItem1.Size = New System.Drawing.Size(455, 26)
        Me.ExtractoDeMovimientosToolStripMenuItem1.Tag = "frmExtractoMovimientoProveedor"
        Me.ExtractoDeMovimientosToolStripMenuItem1.Text = "Extracto de Movimientos"
        '
        'ListadoDeProveedoresToolStripMenuItem
        '
        Me.ListadoDeProveedoresToolStripMenuItem.Name = "ListadoDeProveedoresToolStripMenuItem"
        Me.ListadoDeProveedoresToolStripMenuItem.Size = New System.Drawing.Size(455, 26)
        Me.ListadoDeProveedoresToolStripMenuItem.Tag = "frmListadoProveedor"
        Me.ListadoDeProveedoresToolStripMenuItem.Text = "Listado de Proveedores"
        '
        'ExtractoDeMovimientosTodosLosProveedoresToolStripMenuItem
        '
        Me.ExtractoDeMovimientosTodosLosProveedoresToolStripMenuItem.Name = "ExtractoDeMovimientosTodosLosProveedoresToolStripMenuItem"
        Me.ExtractoDeMovimientosTodosLosProveedoresToolStripMenuItem.Size = New System.Drawing.Size(455, 26)
        Me.ExtractoDeMovimientosTodosLosProveedoresToolStripMenuItem.Tag = "frmExtractoMovimientoProveedorTodos"
        Me.ExtractoDeMovimientosTodosLosProveedoresToolStripMenuItem.Text = "Extracto de Movimientos de todos los proveedores"
        '
        'ExtractoDeMovimientoDeDocumentosToolStripMenuItem
        '
        Me.ExtractoDeMovimientoDeDocumentosToolStripMenuItem.Name = "ExtractoDeMovimientoDeDocumentosToolStripMenuItem"
        Me.ExtractoDeMovimientoDeDocumentosToolStripMenuItem.Size = New System.Drawing.Size(455, 26)
        Me.ExtractoDeMovimientoDeDocumentosToolStripMenuItem.Tag = "frmExtractoMovimientoDocumentoProveedor"
        Me.ExtractoDeMovimientoDeDocumentosToolStripMenuItem.Text = "Extracto de Movimiento de Documentos"
        '
        'RankingDeProveedoresToolStripMenuItem
        '
        Me.RankingDeProveedoresToolStripMenuItem.Name = "RankingDeProveedoresToolStripMenuItem"
        Me.RankingDeProveedoresToolStripMenuItem.Size = New System.Drawing.Size(455, 26)
        Me.RankingDeProveedoresToolStripMenuItem.Tag = "frmRankingProveedores"
        Me.RankingDeProveedoresToolStripMenuItem.Text = "Ranking de Proveedores"
        '
        'ProductoiToolStripMenuItem
        '
        Me.ProductoiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListadoDeProductosToolStripMenuItem})
        Me.ProductoiToolStripMenuItem.Name = "ProductoiToolStripMenuItem"
        Me.ProductoiToolStripMenuItem.Size = New System.Drawing.Size(184, 26)
        Me.ProductoiToolStripMenuItem.Text = "Producto"
        '
        'ListadoDeProductosToolStripMenuItem
        '
        Me.ListadoDeProductosToolStripMenuItem.Name = "ListadoDeProductosToolStripMenuItem"
        Me.ListadoDeProductosToolStripMenuItem.Size = New System.Drawing.Size(246, 26)
        Me.ListadoDeProductosToolStripMenuItem.Tag = "frmListadoProducto"
        Me.ListadoDeProductosToolStripMenuItem.Text = "Listado de Productos"
        '
        'DiseñadorToolStripMenuItem
        '
        Me.DiseñadorToolStripMenuItem.Name = "DiseñadorToolStripMenuItem"
        Me.DiseñadorToolStripMenuItem.Size = New System.Drawing.Size(184, 26)
        Me.DiseñadorToolStripMenuItem.Tag = "frmFastReport"
        Me.DiseñadorToolStripMenuItem.Text = "Diseñador"
        Me.DiseñadorToolStripMenuItem.Visible = False
        '
        'UnileverToolStripMenuItem
        '
        Me.UnileverToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ETLToolStripMenuItem, Me.DMSToolStripMenuItem, Me.DMSMinutasToolStripMenuItem})
        Me.UnileverToolStripMenuItem.Name = "UnileverToolStripMenuItem"
        Me.UnileverToolStripMenuItem.Size = New System.Drawing.Size(184, 26)
        Me.UnileverToolStripMenuItem.Text = "Unilever"
        Me.UnileverToolStripMenuItem.Visible = False
        '
        'ETLToolStripMenuItem
        '
        Me.ETLToolStripMenuItem.Name = "ETLToolStripMenuItem"
        Me.ETLToolStripMenuItem.Size = New System.Drawing.Size(172, 26)
        Me.ETLToolStripMenuItem.Tag = "frmETL"
        Me.ETLToolStripMenuItem.Text = "ETL"
        '
        'DMSToolStripMenuItem
        '
        Me.DMSToolStripMenuItem.Name = "DMSToolStripMenuItem"
        Me.DMSToolStripMenuItem.Size = New System.Drawing.Size(172, 26)
        Me.DMSToolStripMenuItem.Tag = "frmDMS"
        Me.DMSToolStripMenuItem.Text = "DMS"
        '
        'DMSMinutasToolStripMenuItem
        '
        Me.DMSMinutasToolStripMenuItem.Name = "DMSMinutasToolStripMenuItem"
        Me.DMSMinutasToolStripMenuItem.Size = New System.Drawing.Size(172, 26)
        Me.DMSMinutasToolStripMenuItem.Tag = "frmDMSCubo"
        Me.DMSMinutasToolStripMenuItem.Text = "DMS Cubo"
        '
        'AuditoriaToolStripMenuItem
        '
        Me.AuditoriaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientesToolStripMenuItem2, Me.ProductosToolStripMenuItem1, Me.ExcepcionesDePreciosToolStripMenuItem, Me.DocumentosToolStripMenuItem, Me.RegistroDeAperturasDeFechaToolStripMenuItem})
        Me.AuditoriaToolStripMenuItem.Name = "AuditoriaToolStripMenuItem"
        Me.AuditoriaToolStripMenuItem.Size = New System.Drawing.Size(184, 26)
        Me.AuditoriaToolStripMenuItem.Text = "Auditoria"
        '
        'ClientesToolStripMenuItem2
        '
        Me.ClientesToolStripMenuItem2.Name = "ClientesToolStripMenuItem2"
        Me.ClientesToolStripMenuItem2.Size = New System.Drawing.Size(324, 26)
        Me.ClientesToolStripMenuItem2.Tag = "frmAuditoriaClientes"
        Me.ClientesToolStripMenuItem2.Text = "Clientes"
        '
        'ProductosToolStripMenuItem1
        '
        Me.ProductosToolStripMenuItem1.Name = "ProductosToolStripMenuItem1"
        Me.ProductosToolStripMenuItem1.Size = New System.Drawing.Size(324, 26)
        Me.ProductosToolStripMenuItem1.Tag = "frmAuditoriaProductos"
        Me.ProductosToolStripMenuItem1.Text = "Productos"
        '
        'ExcepcionesDePreciosToolStripMenuItem
        '
        Me.ExcepcionesDePreciosToolStripMenuItem.Name = "ExcepcionesDePreciosToolStripMenuItem"
        Me.ExcepcionesDePreciosToolStripMenuItem.Size = New System.Drawing.Size(324, 26)
        Me.ExcepcionesDePreciosToolStripMenuItem.Tag = "frmAuditoriaExcepcionesPrecios"
        Me.ExcepcionesDePreciosToolStripMenuItem.Text = "Excepciones de Precios"
        '
        'DocumentosToolStripMenuItem
        '
        Me.DocumentosToolStripMenuItem.Name = "DocumentosToolStripMenuItem"
        Me.DocumentosToolStripMenuItem.Size = New System.Drawing.Size(324, 26)
        Me.DocumentosToolStripMenuItem.Tag = "frmAuditoriaDocumentos"
        Me.DocumentosToolStripMenuItem.Text = "Documentos"
        '
        'RegistroDeAperturasDeFechaToolStripMenuItem
        '
        Me.RegistroDeAperturasDeFechaToolStripMenuItem.Name = "RegistroDeAperturasDeFechaToolStripMenuItem"
        Me.RegistroDeAperturasDeFechaToolStripMenuItem.Size = New System.Drawing.Size(324, 26)
        Me.RegistroDeAperturasDeFechaToolStripMenuItem.Text = "Registro de Aperturas de Fecha"
        '
        'HerramientasToolStripMenuItem
        '
        Me.HerramientasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConfiguracionesToolStripMenuItem, Me.ConexionAServidorToolStripMenuItem, Me.ToolStripMenuItem39, Me.FechaDeFacturacionToolStripMenuItem, Me.ConsultaSQLToolStripMenuItem, Me.ConfiguracionDeImpresionToolStripMenuItem, Me.ImportarDatosToolStripMenuItem, Me.ClasesWindowsToolStripMenuItem, Me.PedidoMovilToolStripMenuItem, Me.FechaContabilidadToolStripMenuItem, Me.FechaAcuerdosToolStripMenuItem})
        Me.HerramientasToolStripMenuItem.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HerramientasToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.HerramientasToolStripMenuItem.Name = "HerramientasToolStripMenuItem"
        Me.HerramientasToolStripMenuItem.Size = New System.Drawing.Size(120, 24)
        Me.HerramientasToolStripMenuItem.Text = "Herramientas"
        '
        'ConfiguracionesToolStripMenuItem
        '
        Me.ConfiguracionesToolStripMenuItem.Name = "ConfiguracionesToolStripMenuItem"
        Me.ConfiguracionesToolStripMenuItem.Size = New System.Drawing.Size(295, 26)
        Me.ConfiguracionesToolStripMenuItem.Tag = "frmConfiguraciones"
        Me.ConfiguracionesToolStripMenuItem.Text = "Configuraciones"
        '
        'ConexionAServidorToolStripMenuItem
        '
        Me.ConexionAServidorToolStripMenuItem.Name = "ConexionAServidorToolStripMenuItem"
        Me.ConexionAServidorToolStripMenuItem.Size = New System.Drawing.Size(295, 26)
        Me.ConexionAServidorToolStripMenuItem.Tag = "frmConexionServidor"
        Me.ConexionAServidorToolStripMenuItem.Text = "Conexion a Servidor"
        '
        'ToolStripMenuItem39
        '
        Me.ToolStripMenuItem39.Name = "ToolStripMenuItem39"
        Me.ToolStripMenuItem39.Size = New System.Drawing.Size(292, 6)
        '
        'FechaDeFacturacionToolStripMenuItem
        '
        Me.FechaDeFacturacionToolStripMenuItem.Name = "FechaDeFacturacionToolStripMenuItem"
        Me.FechaDeFacturacionToolStripMenuItem.Size = New System.Drawing.Size(295, 26)
        Me.FechaDeFacturacionToolStripMenuItem.Tag = "frmFechaFacturacion"
        Me.FechaDeFacturacionToolStripMenuItem.Text = "Fecha de Facturacion"
        '
        'ConsultaSQLToolStripMenuItem
        '
        Me.ConsultaSQLToolStripMenuItem.Name = "ConsultaSQLToolStripMenuItem"
        Me.ConsultaSQLToolStripMenuItem.Size = New System.Drawing.Size(295, 26)
        Me.ConsultaSQLToolStripMenuItem.Tag = "frmConsultaSQL"
        Me.ConsultaSQLToolStripMenuItem.Text = "Consulta SQL"
        '
        'ConfiguracionDeImpresionToolStripMenuItem
        '
        Me.ConfiguracionDeImpresionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FacturaToolStripMenuItem, Me.FormatoDeImpresiónToolStripMenuItem})
        Me.ConfiguracionDeImpresionToolStripMenuItem.Name = "ConfiguracionDeImpresionToolStripMenuItem"
        Me.ConfiguracionDeImpresionToolStripMenuItem.Size = New System.Drawing.Size(295, 26)
        Me.ConfiguracionDeImpresionToolStripMenuItem.Tag = ""
        Me.ConfiguracionDeImpresionToolStripMenuItem.Text = "Configuracion de Impresion"
        '
        'FacturaToolStripMenuItem
        '
        Me.FacturaToolStripMenuItem.Name = "FacturaToolStripMenuItem"
        Me.FacturaToolStripMenuItem.Size = New System.Drawing.Size(252, 26)
        Me.FacturaToolStripMenuItem.Tag = "frmImpresionFactura"
        Me.FacturaToolStripMenuItem.Text = "Factura"
        '
        'FormatoDeImpresiónToolStripMenuItem
        '
        Me.FormatoDeImpresiónToolStripMenuItem.Name = "FormatoDeImpresiónToolStripMenuItem"
        Me.FormatoDeImpresiónToolStripMenuItem.Size = New System.Drawing.Size(252, 26)
        Me.FormatoDeImpresiónToolStripMenuItem.Tag = "frmImpresion"
        Me.FormatoDeImpresiónToolStripMenuItem.Text = "Formato de Impresión"
        '
        'ImportarDatosToolStripMenuItem
        '
        Me.ImportarDatosToolStripMenuItem.Name = "ImportarDatosToolStripMenuItem"
        Me.ImportarDatosToolStripMenuItem.Size = New System.Drawing.Size(295, 26)
        Me.ImportarDatosToolStripMenuItem.Text = "Importar Datos"
        '
        'ClasesWindowsToolStripMenuItem
        '
        Me.ClasesWindowsToolStripMenuItem.Name = "ClasesWindowsToolStripMenuItem"
        Me.ClasesWindowsToolStripMenuItem.Size = New System.Drawing.Size(295, 26)
        Me.ClasesWindowsToolStripMenuItem.Tag = "frmClasesWindows"
        Me.ClasesWindowsToolStripMenuItem.Text = "Clases Windows"
        '
        'PedidoMovilToolStripMenuItem
        '
        Me.PedidoMovilToolStripMenuItem.Name = "PedidoMovilToolStripMenuItem"
        Me.PedidoMovilToolStripMenuItem.Size = New System.Drawing.Size(295, 26)
        Me.PedidoMovilToolStripMenuItem.Text = "PedidoMovil"
        '
        'FechaContabilidadToolStripMenuItem
        '
        Me.FechaContabilidadToolStripMenuItem.Name = "FechaContabilidadToolStripMenuItem"
        Me.FechaContabilidadToolStripMenuItem.Size = New System.Drawing.Size(295, 26)
        Me.FechaContabilidadToolStripMenuItem.Tag = "frmFechaOperacionesContabilidad"
        Me.FechaContabilidadToolStripMenuItem.Text = "Fecha Contabilidad"
        '
        'FechaAcuerdosToolStripMenuItem
        '
        Me.FechaAcuerdosToolStripMenuItem.Name = "FechaAcuerdosToolStripMenuItem"
        Me.FechaAcuerdosToolStripMenuItem.Size = New System.Drawing.Size(295, 26)
        Me.FechaAcuerdosToolStripMenuItem.Tag = "frmFechaOperacionesAcuerdos"
        Me.FechaAcuerdosToolStripMenuItem.Text = "Fecha Acuerdos"
        '
        'SeguridadToolStripMenuItem
        '
        Me.SeguridadToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PerfilesToolStripMenuItem, Me.UsuariosToolStripMenuItem, Me.AccesosToolStripMenuItem, Me.AccesosSegúnPerfilToolStripMenuItem, Me.LogToolStripMenuItem, Me.ReconfigurarMenuToolStripMenuItem})
        Me.SeguridadToolStripMenuItem.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SeguridadToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.SeguridadToolStripMenuItem.Name = "SeguridadToolStripMenuItem"
        Me.SeguridadToolStripMenuItem.Size = New System.Drawing.Size(98, 24)
        Me.SeguridadToolStripMenuItem.Text = "Seguridad"
        '
        'PerfilesToolStripMenuItem
        '
        Me.PerfilesToolStripMenuItem.Name = "PerfilesToolStripMenuItem"
        Me.PerfilesToolStripMenuItem.Size = New System.Drawing.Size(245, 26)
        Me.PerfilesToolStripMenuItem.Tag = "frmPerfil"
        Me.PerfilesToolStripMenuItem.Text = "Perfiles"
        '
        'UsuariosToolStripMenuItem
        '
        Me.UsuariosToolStripMenuItem.Name = "UsuariosToolStripMenuItem"
        Me.UsuariosToolStripMenuItem.Size = New System.Drawing.Size(245, 26)
        Me.UsuariosToolStripMenuItem.Tag = "frmUsuario"
        Me.UsuariosToolStripMenuItem.Text = "Usuarios"
        '
        'AccesosToolStripMenuItem
        '
        Me.AccesosToolStripMenuItem.Name = "AccesosToolStripMenuItem"
        Me.AccesosToolStripMenuItem.Size = New System.Drawing.Size(245, 26)
        Me.AccesosToolStripMenuItem.Tag = "frmAccesos"
        Me.AccesosToolStripMenuItem.Text = "Accesos"
        '
        'AccesosSegúnPerfilToolStripMenuItem
        '
        Me.AccesosSegúnPerfilToolStripMenuItem.Name = "AccesosSegúnPerfilToolStripMenuItem"
        Me.AccesosSegúnPerfilToolStripMenuItem.Size = New System.Drawing.Size(245, 26)
        Me.AccesosSegúnPerfilToolStripMenuItem.Text = "Accesos según perfil"
        '
        'LogToolStripMenuItem
        '
        Me.LogToolStripMenuItem.Name = "LogToolStripMenuItem"
        Me.LogToolStripMenuItem.Size = New System.Drawing.Size(245, 26)
        Me.LogToolStripMenuItem.Tag = "frmLogAccesos"
        Me.LogToolStripMenuItem.Text = "Log de Sucesos"
        '
        'ReconfigurarMenuToolStripMenuItem
        '
        Me.ReconfigurarMenuToolStripMenuItem.Name = "ReconfigurarMenuToolStripMenuItem"
        Me.ReconfigurarMenuToolStripMenuItem.Size = New System.Drawing.Size(245, 26)
        Me.ReconfigurarMenuToolStripMenuItem.Text = "Reconfigurar Menu"
        '
        'AyudaToolStripMenuItem
        '
        Me.AyudaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ActualizacionesToolStripMenuItem, Me.ToolStripMenuItem37, Me.AcercaDeToolStripMenuItem, Me.DevToolStripMenuItem})
        Me.AyudaToolStripMenuItem.Name = "AyudaToolStripMenuItem"
        Me.AyudaToolStripMenuItem.Size = New System.Drawing.Size(70, 24)
        Me.AyudaToolStripMenuItem.Text = "Ayuda"
        '
        'ActualizacionesToolStripMenuItem
        '
        Me.ActualizacionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ComprobarSiExistenActualizacionesToolStripMenuItem, Me.ConfigurarActualizacionesToolStripMenuItem})
        Me.ActualizacionesToolStripMenuItem.Name = "ActualizacionesToolStripMenuItem"
        Me.ActualizacionesToolStripMenuItem.Size = New System.Drawing.Size(224, 26)
        Me.ActualizacionesToolStripMenuItem.Text = "Actualizaciones"
        '
        'ComprobarSiExistenActualizacionesToolStripMenuItem
        '
        Me.ComprobarSiExistenActualizacionesToolStripMenuItem.Name = "ComprobarSiExistenActualizacionesToolStripMenuItem"
        Me.ComprobarSiExistenActualizacionesToolStripMenuItem.Size = New System.Drawing.Size(363, 26)
        Me.ComprobarSiExistenActualizacionesToolStripMenuItem.Text = "Comprobar si existen actualizaciones"
        '
        'ConfigurarActualizacionesToolStripMenuItem
        '
        Me.ConfigurarActualizacionesToolStripMenuItem.Name = "ConfigurarActualizacionesToolStripMenuItem"
        Me.ConfigurarActualizacionesToolStripMenuItem.Size = New System.Drawing.Size(363, 26)
        Me.ConfigurarActualizacionesToolStripMenuItem.Tag = "frmAccesoActualizacion"
        Me.ConfigurarActualizacionesToolStripMenuItem.Text = "Configurar actualizaciones"
        '
        'ToolStripMenuItem37
        '
        Me.ToolStripMenuItem37.Name = "ToolStripMenuItem37"
        Me.ToolStripMenuItem37.Size = New System.Drawing.Size(221, 6)
        '
        'AcercaDeToolStripMenuItem
        '
        Me.AcercaDeToolStripMenuItem.Name = "AcercaDeToolStripMenuItem"
        Me.AcercaDeToolStripMenuItem.Size = New System.Drawing.Size(224, 26)
        Me.AcercaDeToolStripMenuItem.Text = "Acerca de"
        '
        'CostosDeProductosToolStripMenuItem
        '
        Me.CostosDeProductosToolStripMenuItem.Name = "CostosDeProductosToolStripMenuItem"
        Me.CostosDeProductosToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.CostosDeProductosToolStripMenuItem.Tag = "frmProductoCosto"
        Me.CostosDeProductosToolStripMenuItem.Text = "Costos de Productos"
        '
        'AsientosContablesToolStripMenuItem
        '
        Me.AsientosContablesToolStripMenuItem.Name = "AsientosContablesToolStripMenuItem"
        Me.AsientosContablesToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.AsientosContablesToolStripMenuItem.Tag = "frmAsientoDiario"
        Me.AsientosContablesToolStripMenuItem.Text = "Asientos Contables"
        '
        'ToolStripSeparator34
        '
        Me.ToolStripSeparator34.Name = "ToolStripSeparator34"
        Me.ToolStripSeparator34.Size = New System.Drawing.Size(240, 6)
        '
        'LibroIVAToolStripMenuItem
        '
        Me.LibroIVAToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LibroIVAToolStripMenuItem.Name = "LibroIVAToolStripMenuItem"
        Me.LibroIVAToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.LibroIVAToolStripMenuItem.Tag = "frmLibroIVA"
        Me.LibroIVAToolStripMenuItem.Text = "Libro IVA"
        '
        'LibroMayorToolStripMenuItem
        '
        Me.LibroMayorToolStripMenuItem.Name = "LibroMayorToolStripMenuItem"
        Me.LibroMayorToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.LibroMayorToolStripMenuItem.Tag = "frmLibroMayor"
        Me.LibroMayorToolStripMenuItem.Text = "Libro Mayor"
        '
        'ToolStripSeparator37
        '
        Me.ToolStripSeparator37.Name = "ToolStripSeparator37"
        Me.ToolStripSeparator37.Size = New System.Drawing.Size(240, 6)
        '
        'ComprobantesNoEmitidosToolStripMenuItem
        '
        Me.ComprobantesNoEmitidosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ComprobantesNoEmitidosToolStripMenuItem.Name = "ComprobantesNoEmitidosToolStripMenuItem"
        Me.ComprobantesNoEmitidosToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.ComprobantesNoEmitidosToolStripMenuItem.Tag = "frmComprobantesNoEmitidos"
        Me.ComprobantesNoEmitidosToolStripMenuItem.Text = "Comprobantes no Emitidos"
        '
        'ToolStripSeparator33
        '
        Me.ToolStripSeparator33.Name = "ToolStripSeparator33"
        Me.ToolStripSeparator33.Size = New System.Drawing.Size(240, 6)
        '
        'BalanceGeneralToolStripMenuItem
        '
        Me.BalanceGeneralToolStripMenuItem.Name = "BalanceGeneralToolStripMenuItem"
        Me.BalanceGeneralToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.BalanceGeneralToolStripMenuItem.Tag = "frmBalanceGeneralEstadoResultado"
        Me.BalanceGeneralToolStripMenuItem.Text = "Balance General"
        '
        'BalandeDeComprobacionToolStripMenuItem
        '
        Me.BalandeDeComprobacionToolStripMenuItem.Name = "BalandeDeComprobacionToolStripMenuItem"
        Me.BalandeDeComprobacionToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.BalandeDeComprobacionToolStripMenuItem.Tag = "frmBalanceComprobacion"
        Me.BalandeDeComprobacionToolStripMenuItem.Text = "Balande de Comprobacion"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = "Todas las imagenes |*.jpeg*.jpg*.bmp*.png*.gif|JPEG |*.jpeg|BMP |*.bmp|PNG |*.png" &
    "|JPG |*.jpg"
        Me.OpenFileDialog1.Title = "Imagen de fondo"
        '
        'tFechaHora
        '
        Me.tFechaHora.Enabled = True
        Me.tFechaHora.Interval = 1000
        '
        'tiActualizarFechaHora
        '
        Me.tiActualizarFechaHora.Enabled = True
        Me.tiActualizarFechaHora.Interval = 60000
        '
        'BottomToolStripPanel
        '
        Me.BottomToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.BottomToolStripPanel.Name = "BottomToolStripPanel"
        Me.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.BottomToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.BottomToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'miniToolStrip
        '
        Me.miniToolStrip.AutoSize = False
        Me.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me.miniToolStrip.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.miniToolStrip.Location = New System.Drawing.Point(152, 24)
        Me.miniToolStrip.Name = "miniToolStrip"
        Me.miniToolStrip.Size = New System.Drawing.Size(150, 22)
        Me.miniToolStrip.TabIndex = 8
        '
        'ToolStripStatusLabel8
        '
        Me.ToolStripStatusLabel8.BorderStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.ToolStripStatusLabel8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel8.ForeColor = System.Drawing.Color.Black
        Me.ToolStripStatusLabel8.Image = CType(resources.GetObject("ToolStripStatusLabel8.Image"), System.Drawing.Image)
        Me.ToolStripStatusLabel8.Name = "ToolStripStatusLabel8"
        Me.ToolStripStatusLabel8.Size = New System.Drawing.Size(71, 30)
        Me.ToolStripStatusLabel8.Text = "Fecha:"
        '
        'tsslFecha
        '
        Me.tsslFecha.ForeColor = System.Drawing.Color.Black
        Me.tsslFecha.Name = "tsslFecha"
        Me.tsslFecha.Size = New System.Drawing.Size(25, 30)
        Me.tsslFecha.Text = "00"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel2.ForeColor = System.Drawing.Color.Black
        Me.ToolStripStatusLabel2.Margin = New System.Windows.Forms.Padding(10, 3, 0, 2)
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(67, 31)
        Me.ToolStripStatusLabel2.Text = "Sucursal:"
        '
        'lblSucursal
        '
        Me.lblSucursal.ForeColor = System.Drawing.Color.Black
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(37, 30)
        Me.lblSucursal.Text = "ASU"
        '
        'ToolStripStatusLabel4
        '
        Me.ToolStripStatusLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel4.ForeColor = System.Drawing.Color.Black
        Me.ToolStripStatusLabel4.Margin = New System.Windows.Forms.Padding(10, 3, 0, 2)
        Me.ToolStripStatusLabel4.Name = "ToolStripStatusLabel4"
        Me.ToolStripStatusLabel4.Size = New System.Drawing.Size(68, 31)
        Me.ToolStripStatusLabel4.Text = "Depósito:"
        '
        'lblDeposito
        '
        Me.lblDeposito.ForeColor = System.Drawing.Color.Black
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(44, 30)
        Me.lblDeposito.Text = "DEP1"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel.ForeColor = System.Drawing.Color.Black
        Me.ToolStripStatusLabel.Margin = New System.Windows.Forms.Padding(10, 3, 0, 2)
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(67, 31)
        Me.ToolStripStatusLabel.Text = "Terminal:"
        '
        'lblTerminal
        '
        Me.lblTerminal.ForeColor = System.Drawing.Color.Black
        Me.lblTerminal.Name = "lblTerminal"
        Me.lblTerminal.Size = New System.Drawing.Size(60, 30)
        Me.lblTerminal.Text = "SERVER"
        '
        'ToolStripStatusLabel6
        '
        Me.ToolStripStatusLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel6.ForeColor = System.Drawing.Color.Black
        Me.ToolStripStatusLabel6.Margin = New System.Windows.Forms.Padding(10, 3, 0, 2)
        Me.ToolStripStatusLabel6.Name = "ToolStripStatusLabel6"
        Me.ToolStripStatusLabel6.Size = New System.Drawing.Size(61, 31)
        Me.ToolStripStatusLabel6.Text = "Usuario:"
        '
        'lblUsuario
        '
        Me.lblUsuario.ForeColor = System.Drawing.Color.Black
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(58, 30)
        Me.lblUsuario.Text = "ADMIN"
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AparienciaToolStripMenuItem, Me.CambiarImagenDeFonoToolStripMenuItem})
        Me.ToolStripSplitButton1.ForeColor = System.Drawing.Color.Black
        Me.ToolStripSplitButton1.Image = CType(resources.GetObject("ToolStripSplitButton1.Image"), System.Drawing.Image)
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Margin = New System.Windows.Forms.Padding(10, 6, 6, 6)
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(110, 24)
        Me.ToolStripSplitButton1.Text = "Opciones"
        Me.ToolStripSplitButton1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'AparienciaToolStripMenuItem
        '
        Me.AparienciaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PielesToolStripMenuItem})
        Me.AparienciaToolStripMenuItem.Image = CType(resources.GetObject("AparienciaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.AparienciaToolStripMenuItem.Name = "AparienciaToolStripMenuItem"
        Me.AparienciaToolStripMenuItem.Size = New System.Drawing.Size(267, 26)
        Me.AparienciaToolStripMenuItem.Text = "Apariencia"
        '
        'PielesToolStripMenuItem
        '
        Me.PielesToolStripMenuItem.Name = "PielesToolStripMenuItem"
        Me.PielesToolStripMenuItem.Size = New System.Drawing.Size(130, 26)
        Me.PielesToolStripMenuItem.Text = "Pieles"
        '
        'CambiarImagenDeFonoToolStripMenuItem
        '
        Me.CambiarImagenDeFonoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PredeterminadoToolStripMenuItem})
        Me.CambiarImagenDeFonoToolStripMenuItem.Image = CType(resources.GetObject("CambiarImagenDeFonoToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CambiarImagenDeFonoToolStripMenuItem.Name = "CambiarImagenDeFonoToolStripMenuItem"
        Me.CambiarImagenDeFonoToolStripMenuItem.Size = New System.Drawing.Size(267, 26)
        Me.CambiarImagenDeFonoToolStripMenuItem.Text = "Cambiar imagen de fondo"
        '
        'PredeterminadoToolStripMenuItem
        '
        Me.PredeterminadoToolStripMenuItem.Name = "PredeterminadoToolStripMenuItem"
        Me.PredeterminadoToolStripMenuItem.Size = New System.Drawing.Size(199, 26)
        Me.PredeterminadoToolStripMenuItem.Text = "Predeterminado"
        '
        'TopToolStripPanel
        '
        Me.TopToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopToolStripPanel.Name = "TopToolStripPanel"
        Me.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.TopToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.TopToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'RightToolStripPanel
        '
        Me.RightToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.RightToolStripPanel.Name = "RightToolStripPanel"
        Me.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.RightToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.RightToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'LeftToolStripPanel
        '
        Me.LeftToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.LeftToolStripPanel.Name = "LeftToolStripPanel"
        Me.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.LeftToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.LeftToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'ContentPanel
        '
        Me.ContentPanel.AutoScroll = True
        Me.ContentPanel.Size = New System.Drawing.Size(150, 175)
        '
        'StatusStrip
        '
        Me.StatusStrip.BackColor = System.Drawing.SystemColors.Control
        Me.StatusStrip.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel8, Me.tsslFecha, Me.ToolStripStatusLabel2, Me.lblSucursal, Me.ToolStripStatusLabel4, Me.lblDeposito, Me.ToolStripStatusLabel, Me.lblTerminal, Me.ToolStripStatusLabel3, Me.lblPerfil, Me.ToolStripStatusLabel6, Me.lblUsuario, Me.ToolStripStatusLabel1, Me.lblBaseDeDatos, Me.ToolStripSplitButton1})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 886)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
        Me.StatusStrip.Size = New System.Drawing.Size(1449, 36)
        Me.StatusStrip.TabIndex = 8
        Me.StatusStrip.Text = "StatusStrip"
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel3.ForeColor = System.Drawing.Color.Black
        Me.ToolStripStatusLabel3.Margin = New System.Windows.Forms.Padding(10, 3, 0, 2)
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(44, 31)
        Me.ToolStripStatusLabel3.Text = "Perfil:"
        '
        'lblPerfil
        '
        Me.lblPerfil.ForeColor = System.Drawing.Color.Black
        Me.lblPerfil.Name = "lblPerfil"
        Me.lblPerfil.Size = New System.Drawing.Size(58, 30)
        Me.lblPerfil.Text = "ADMIN"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.ForeColor = System.Drawing.Color.Black
        Me.ToolStripStatusLabel1.Margin = New System.Windows.Forms.Padding(10, 3, 0, 2)
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(107, 31)
        Me.ToolStripStatusLabel1.Text = "Base de Datos:"
        '
        'lblBaseDeDatos
        '
        Me.lblBaseDeDatos.ForeColor = System.Drawing.Color.Black
        Me.lblBaseDeDatos.Name = "lblBaseDeDatos"
        Me.lblBaseDeDatos.Size = New System.Drawing.Size(42, 30)
        Me.lblBaseDeDatos.Text = "SAIN"
        '
        'PagarToolStripMenuItem
        '
        Me.PagarToolStripMenuItem.Name = "PagarToolStripMenuItem"
        Me.PagarToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.PagarToolStripMenuItem.Text = "Pagar"
        '
        'DevToolStripMenuItem
        '
        Me.DevToolStripMenuItem.Name = "DevToolStripMenuItem"
        Me.DevToolStripMenuItem.Size = New System.Drawing.Size(224, 26)
        Me.DevToolStripMenuItem.Text = "dev"
        '
        'frmPrincipal2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1449, 922)
        Me.Controls.Add(Me.StatusStrip)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "frmPrincipal2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "x"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ENTIDADESToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComprasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TesoreriaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContabilidadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HerramientasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SeguridadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProveedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PersonalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PersonasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VendedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CobradoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PromotoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DistribuidoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents OtrosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PaisCiudadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipoClienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ZonasDeVentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstadoDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents UnidadesDeMedidaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TransportistasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChoferesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CamionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TerminalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Sucursales6ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MonedaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CotizacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BancosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CuentasBancariasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents RegistrosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MotivosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PuntosDeExpedicionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FacturacionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AnulacionDeFacturaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents InformesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NotaDeCreditoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NotaDeDebitoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LoteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RendicionToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents InformerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultaDeComprobanteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeComprobantesEmitidosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeComprobantesAnuladosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents InformeToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResumenDeLotesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HojaDeRutaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanillaDeCargaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExtractoDeMovimientosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProveedoresToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator15 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem7 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem8 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasDeProductosInferiorAlCostoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComisionesPorVendedorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DescuentosPorVendedorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem6 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargarToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntradaPorComprasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem19 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoGeneralDeComprobantesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem16 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem17 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator16 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem18 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem13 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem14 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator14 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem15 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExtractoDeMovimientosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DevolucionDeLoteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MovimientosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargaDeMercaderiasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InventarioInicialToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PedidosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargaToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator19 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents InformeToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator18 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents DetalleDeDevolucionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PerfilesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsuariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccesosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LogToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImprimirToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FacturacionDePedidosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChequesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultaDeChequesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DescuentosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ModificarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PagoChequeClienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RechazarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator20 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents InformeToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeChequesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AplicarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeNotasDeCreditoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeAplicacionPendienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CobranzaCreditoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CobranzaContadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator23 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ListadoDeCanjesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeDescuentosDeChequesDiferidosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeChequesRechazadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EfectivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargaToolStripMenuItem6 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator17 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents InformeToolStripMenuItem10 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargaToolStripMenuItem7 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator30 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents InformeToolStripMenuItem11 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ControlDeStockToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VeremosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem26 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator31 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem27 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator29 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CostosDeProductosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem23 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanillaParaTomaDeInventarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MovimientosPorProductToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductosAReponerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResumenDeExistenciaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EfectivisarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator25 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem20 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargaToolStripMenuItem5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GastosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargaToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator26 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem21 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CuentasACobrarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InventarioDeDocumentosPendientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BusquedaDeClientesConFacturasAtrasadasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DistribucionDeSaldosDeDocumentosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResumenDeMovimientosYSaldosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CuentasAPagarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem22 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem24 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocumentosPagadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem25 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EquiposDeConteoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TomaDeInventarioFisicoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComprarativoEntreEquiposToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComprarativoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AjusteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator32 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem28 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExistenciaValorizadaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformeToolStripMenuItem12 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanillaDeCobranzasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DepositosBancariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DebitosYCreditosBancariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConciliacionBancariaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator38 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents DescuentosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComprobantesSinLotesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BancoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExtractoDeMovimientosToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaldosBancariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator35 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ResumenDiarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AsientoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanDeCuentasContablesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CuentasFujasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator36 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ComprobantesParaLibroVentaYCompraToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator40 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ComprobantesNoEmitidosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeClientesQueNoCompranToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CumpleañosYAniversariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeProveedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductoiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeProductosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FechaDeFacturacionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem29 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambiarUsuarioContraseñaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CerrarSesionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConfiguracionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents tFechaHora As System.Windows.Forms.Timer
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents ToolStripSeparator41 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TransferenciaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReposicionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator43 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ValesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator42 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CategoriasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiClasificacion6 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiClasificacion5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiClasificacion2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiClasificacion1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DiseñadorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargaToolStripMenuItem13 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActividadesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator44 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents InformeToolStripMenuItem18 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TransferirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiClasificacion3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiClasificacion4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultaSQLToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tiActualizarFechaHora As System.Windows.Forms.Timer
    Friend WithEvents ZonaEnDepositoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReconfigurarMenuToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miniToolStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel8 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsslFecha As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblSucursal As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel4 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblDeposito As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblTerminal As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel6 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblUsuario As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripSplitButton1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents CambiarImagenDeFonoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PredeterminadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents ModifcarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AplicarToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem30 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem32 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem33 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GráficosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GráficoVentaMesTotalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GráficoRankingClientesVentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GráficoProductosMásRentablesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GráficoProductosMásVendidoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DevolucionesPorProductoClienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChequesDiferidosDepositadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImprimirToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImprimirOrdenDePagoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem34 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeDepositosBancariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeDebitosYCreditosBancariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeOrdenDePagoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeGastosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator21 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LibroIVAToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DefinirFondoFijoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator22 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents EmitirListadoAReponerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RendicionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AsientosContablesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConfiguracionDeImpresionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FacturaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BottomToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents TopToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents RightToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents LeftToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents ContentPanel As System.Windows.Forms.ToolStripContentPanel
    Friend WithEvents ImportarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem35 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CierreDiarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CierreMensualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CierreEjercicioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator24 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ActualizarTimbradoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RetencionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HechaukaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UnileverToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ETLToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DMSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImportarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LibroMayorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator27 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ValesPendientesDeRendicionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BalanceGeneralToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcesosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambioGlobalDeCuentaContableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegenerarAsientosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RenumeracionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegenerarSaldosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator34 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator37 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator33 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BalandeDeComprobacionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClasesWindowsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdministrarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem31 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GruposYUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargarToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EfectivizacionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ModificarComprobanteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator46 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ResumenDeCobranzasPorFacturasContadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem36 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator47 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents DMSMinutasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasPorClienteToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OperacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AyudaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem37 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AcercaDeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComprobarSiExistenActualizacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConfigurarActualizacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem38 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem39 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TarjetasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TiposDeTarjetasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem40 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem41 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator39 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem42 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem43 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator45 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem44 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator48 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem45 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem46 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasAnualesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblBaseDeDatos As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ConexionAServidorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AparienciaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LibroDiarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NCRDocumentoInternoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeRetencionesRecibidasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PielesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AsociarRetencionesDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocumentosSinAsientosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CajaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TeleventasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LlamadasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipoDeLlamadaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResultadoDeLlamadaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FormaDePagoFacturasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AreasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RutasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrepararPagosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AprobacionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntregaDeChequesOPToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RealizarPAgosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrestamosBancariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambiarVendedorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PagarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem10 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AnticipoAProveedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EgresosARendirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformePrestamoBancarioToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CentroDeCostosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformeTeleventaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanDeLlamadasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LIstadoDeChequesEmitidosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasAExcelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChequeraToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AsignarCuentaContableAFondoFijoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdministrarToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeExcepcionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeRetencionesEmitidasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AplicarValeAGastoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ModificarToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipoDeDepositoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeProductoPorOperacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GenerarCuotasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MotivosDeAnulacionDeVentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeEstadoDeChequesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DescargaDeStockToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExtractoDeMovimientosTodosLosProveedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VencimientoDeChequesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformeTicketDeTarjetasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem9 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblPerfil As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents AuditoriaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExcepcionesDePreciosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocumentosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AbogadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InactivacionDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsumoCombustibleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UnidadDeNegociosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsumoMateriaPrimaBalanceadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsuariosUnidadDeNegocioYCCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExtractoDeMovimientoDeDocumentosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaldoDiarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaldoDiarioDeBancoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KilosTransportadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformeAcreditacionesBancariaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DescagaDeComprasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformeToolStripMenuItem4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FormaDePagoCobranzaDepositoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformeFinancieroToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresoTotalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ControlTesoreriaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeNCConVentasAplicadasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CierreYReaperturaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TrigoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AcuerdoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TicketDeBasculaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformesUnidadNegocioCCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LibroMayorToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VariosProveedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CamionesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChoferesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MacheoFacturaTicketToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator28 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents InformesToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AcuerdoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TicketDeBasculaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MacheoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KardexToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VencimientoDeChequeDescontadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OrdenDePedidoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PedidoParaFábricaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RankingDeProveedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformeToolStripMenuItem5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeDescuentosConcedidosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AsignarAcuerdoATicketToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CostoDeProductosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AsignarCostoAProductoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TransporteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargaDeCamionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AsignacionDeLimiteDeDescuentoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargaDeCamionesPedidoDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformeToolStripMenuItem6 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AbastecimientoPendienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PedClientesPendientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeChequesRechazadosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargaParaAbastecimientoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TicketDeBasculaSinCostoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformeAnticipacionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MetasDeVentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformesGerencialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TotalGeneralToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeChequesDescontadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResumenDeSaldosUnidadDeNegociosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AprobarExcesoDeLineaDeCreditoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeRecibosGeneradosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PedidosDeNotaDeCreditoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator49 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AprobarPedidosDeNotasDeCredutoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcesarPedidoDeNotasDeCreditoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OperacionesPermitidasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CreditoDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RRHHToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComprobantesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AsignarCuentaContableARRHHToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ControlDeCostosDeOperacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeMovimientosDeCombustiblesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TicketDeBasculaSinMachearAFechaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SolicitudesDeExcepcionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CategoriaDeClientesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FormatoDeImpresiónToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PedidoNotaDeCreditoMovilToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PedidoMovilToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SolicitudesDeAnulacionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UsuariosYAutorizacionesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UsuariosParaAutorizarAnulacionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PorcentajeDeExcepcionToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents PorcentajeDeExcesoDeLineaDeCreditoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SubMotivosNotaDeCreditoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AutorizadoresDeNotaDeCreditoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MotivosDeAnulacionCobranzasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProductosConPrecioModificableToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents InformesToolStripMenuItem3 As ToolStripMenuItem
    Friend WithEvents ExcepcionesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListaDePrecioToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LibroMayorDetalladoPorProductoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents KardexResumenToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents KardexDetalladoPorProductoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CalidadToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MuestraParaPruebaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents InformesToolStripMenuItem4 As ToolStripMenuItem
    Friend WithEvents ListadoDeMuestrasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AprobarPedidosDeNotaDeCreditoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListadoDePedidosDeDevolucionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConfiguracionDeMuestraToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CambiarDepositoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator50 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator51 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator52 As ToolStripSeparator
    Friend WithEvents SolicitudesDeModificacionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CargaToolStripMenuItem4 As ToolStripMenuItem
    Friend WithEvents AccesosSegúnPerfilToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SaldosAFechaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SaldosAFechaToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator53 As ToolStripSeparator
    Friend WithEvents ListadoDePedidoDeNotasDeCreditoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PanelDePedidosDeDevolucionesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RegistroDeAperturasDeFechaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AplicacionesDeAnticipoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CanjeDeRecaudacionesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UsuariosPermitidosCargaDeRecepcionDeDocumentosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConciliarAsientosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DevolucionesDeClientesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AjusteDeExistenciaKardexToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator54 As ToolStripSeparator
    Friend WithEvents DepartamentosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExtractoDePagosPorAcuerdoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListadoDeDevolucionesSinNCToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SolicitudesDeDescuentoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CargaToolStripMenuItem8 As ToolStripMenuItem
    Friend WithEvents RegistroDePreciosPorClienteProductoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ControlDeIntegridadToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EvolucionDeSaldosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExistenciasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AplicacionAnticipoAProveedoresToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DistribucionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CargaDescargaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FacturaDistribucionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrecargaMovimientoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AprobadorPrecargaMovimientoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UsuarioAprobadorPrecargaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ABMSubMotivoMovimientoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents InventarioDeDocumentosPendientesPorPlazoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents InventarioDeDocumentosPendientesPorClienteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AsignarCuentaContableIntegridadToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TarjetaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProcesadoraDeTarjetaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents VencimientoDeProductosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CambiarRepositorToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ControlDeUnidadNegocioYDepartamentoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ModificarGastosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ModificarFondoFijoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ModificarCompraToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ModificarGastosRRHHToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SeccionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsumoInformaticaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FechaContabilidadToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Resolución49ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BalanceRG49ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AdministrarToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents AsignacionDeCuentasRG49ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CargaToolStripMenuItem9 As ToolStripMenuItem
    Friend WithEvents AprobadorToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ComparativoCostoKardexToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MotivoDeRemisionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ImportarAsientoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReenvioComprobantesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MotivoDevolucionNCToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FechaAcuerdosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListadoDeGastosAcuerdoComercialToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AcuerdoDeClientesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CargaDeAcuerdosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListadoDeAcuerdosDeClientesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator55 As ToolStripSeparator
    Friend WithEvents ModificarModuloStockToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem11 As ToolStripMenuItem
    Friend WithEvents DevToolStripMenuItem As ToolStripMenuItem
End Class

﻿Public Class frmCambiarDepositoTerminal
    Dim CSistema As New CSistema

    Sub Inicializar()

        CSistema.SqlToComboBox(cbxDeposito, "Select ID, [Suc-Dep] from vDeposito where idtipodeposito = 1 and estado = 'OK'")

        cbxDeposito.cbx.SelectedValue = vgIDDeposito
        txtDescripcion.txt.Text = vgTerminal

    End Sub

    Sub Guardar()

        If MessageBox.Show("Al realizar esta operacion, el sistema se cerrara. Desea continuar?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.No Then
            Exit Sub
        End If

        'Deposito
        If IsNumeric(cbxDeposito.cbx.SelectedValue) = False Then
            MessageBox.Show("Seleccione correctamente el deposito!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", cbxDeposito.cbx, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        CSistema.ExecuteStoreProcedure(param, "SpCambiarDepositoTerminal", False, False, MensajeRetorno, "", False)

        End

    End Sub

    Private Sub frmCambiarDepositoTerminal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmCambiarDepositoTerminal_Activate()
        Me.Refresh()
    End Sub
End Class
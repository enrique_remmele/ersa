﻿Imports System.Xml


Public Class frmPrincipal2

    'EVENTOS

    'PROPIEDADES

    'CLASES
    Dim CArchivoInicio As New CArchivoInicio
    Dim CSistema As New CSistema

    'VARIABLES
    Private CerrandoSesion As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Form
        'Titulo
        Dim Titulo As String
        Titulo = VGSoftwareNombre & " :: Versión: " & VGSoftwareVersion & " - Ultima actualización: " & VGSoftwareUltimaActualizacion & " - " & vgLicenciaOtorgado
        Me.Text = Titulo
        Me.Icon = My.Resources.SAIN

        'Identificaciones
        lblSucursal.Text = vgSucursal
        lblDeposito.Text = vgDeposito
        lblTerminal.Text = vgTerminal
        lblUsuario.Text = vgUsuario
        lblPerfil.Text = vgPerfil
        lblBaseDeDatos.Text = String.Format("{0}\{1}", vgNombreServidor, vgNombreBaseDatos)


        'Imagen
        Dim ImagenFondo As String = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "IMAGEN", "")
        If ImagenFondo <> "" Then
            If My.Computer.FileSystem.FileExists(ImagenFondo) = True Then
                Me.BackgroundImage = Image.FromFile(ImagenFondo)
            End If
        Else
            Me.BackgroundImage = Nothing 'My.Resources.EXPRESS_FONDO
        End If

        'Fecha Hora
        VGFechaHoraSistema = CDate(CSistema.ExecuteScalar("Select GetDate()"))

        'Configurar Menu
        FGConfigurarMenu(Me.MenuStrip1)

        'Menu de Clasificacion de Producto
        'Clasificacion 1
        tsmiClasificacion1.Text = vgConfiguraciones("ProductoClasificacion1").ToString
        'Clasificacion 2
        tsmiClasificacion2.Text = vgConfiguraciones("ProductoClasificacion2").ToString
        'Clasificacion 3
        tsmiClasificacion3.Text = vgConfiguraciones("ProductoClasificacion3").ToString
        'Clasificacion 4
        tsmiClasificacion4.Text = vgConfiguraciones("ProductoClasificacion4").ToString
        'Clasificacion 5
        tsmiClasificacion5.Text = vgConfiguraciones("ProductoClasificacion5").ToString
        'Clasificacion 6
        tsmiClasificacion6.Text = vgConfiguraciones("ProductoClasificacion6").ToString
        'Clasificacion 7
        CategoriasToolStripMenuItem.Text = vgConfiguraciones("ProductoClasificacion7").ToString
        '
        'Dim xml As XmlTextReader

        'If vgActualizacionVersion < VGSoftwareVersion Then
        '    CSistema.ExecuteScalar()
        'End If
        If CSistema.ExecuteScalar("Select ProductoPrecio from Configuraciones where IDSucursal = " & vgIDSucursal) Then
            SolicitudesDeExcepcionToolStripMenuItem.Visible = False
            CargaToolStripMenuItem2.Visible = False

            SolicitudesDeDescuentoToolStripMenuItem.Visible = True
            CargaToolStripMenuItem8.Visible = True
        Else
            SolicitudesDeExcepcionToolStripMenuItem.Visible = True
            CargaToolStripMenuItem2.Visible = True

            SolicitudesDeDescuentoToolStripMenuItem.Visible = False
            CargaToolStripMenuItem8.Visible = False
        End If




    End Sub

    Sub MostrarArbol()

        Dim Index As Integer = 1

        Dim dt As New DataTable
        dt.Columns.Add("Codigo")
        dt.Columns.Add("Descripcion")
        dt.Columns.Add("Nivel")
        dt.Columns.Add("NombreControl")
        dt.Columns.Add("CodigoPadre")
        dt.Columns.Add("Tag")

        For Each menu1 As ToolStripMenuItem In Me.MainMenuStrip.Items

            Dim NewRow As DataRow = dt.NewRow

            NewRow("Codigo") = CSistema.FormatDobleDigito(Index.ToString)
            NewRow("Descripcion") = menu1.Text
            NewRow("Nivel") = 1
            NewRow("NombreControl") = menu1.Name
            NewRow("CodigoPadre") = "0"

            If menu1.Tag Is Nothing Then
                NewRow("Tag") = ""
            Else
                NewRow("Tag") = menu1.Tag.ToString
            End If
            dt.Rows.Add(NewRow)

            Debug.Print(Index.ToString & ". " & menu1.Text)

            If menu1.DropDownItems.Count > 0 Then
                Dim Codigo As String = Index.ToString
                MostrarArbol(menu1, 0, dt, Codigo)
            End If

            Index = Index + 1

        Next

        Dim frm As New Form
        Dim lv As New ListView
        frm.Controls.Add(lv)
        lv.Dock = DockStyle.Fill

        CSistema.dtToLv(lv, dt)

        frm.ShowDialog(Me)

        Dim SQL As String = ""

        CSistema.ExecuteNonQuery("Delete From Menu")

        For Each oRow As DataRow In dt.Rows

            'Debug.Print(oRow("Descripcion"))

            Dim param As New DataTable

            CSistema.SetSQLParameter(param, "Codigo", oRow("Codigo"))
            CSistema.SetSQLParameter(param, "Descripcion", oRow("Descripcion"))
            CSistema.SetSQLParameter(param, "Nivel", oRow("Nivel"))
            CSistema.SetSQLParameter(param, "NombreControl", oRow("NombreControl"))
            CSistema.SetSQLParameter(param, "CodigoPadre", oRow("CodigoPadre"))
            CSistema.SetSQLParameter(param, "Tag", oRow("Tag"))

            SQL = CSistema.InsertSQL(param, "Menu")

            Dim RowCount As Integer = CSistema.ExecuteNonQuery(SQL)

        Next


    End Sub

    Sub MostrarArbol(ByVal menu1 As ToolStripMenuItem, ByVal Tab As Integer, ByVal dt As DataTable, Optional ByVal Codigo As String = "", Optional ByVal Indice As Integer = 0, Optional ByVal CodigoPadre As String = "1")

        Dim Espacio As String = ""

        For i As Integer = 0 To Tab
            Espacio = Espacio & vbTab
        Next

        CodigoPadre = Codigo

        For i As Integer = 0 To menu1.DropDownItems.Count - 1

            Dim Tipo As String = menu1.DropDownItems.Item(i).GetType.ToString()

            If Tipo <> "System.Windows.Forms.ToolStripMenuItem" Then
                GoTo siguiente
            End If

            Dim menu2 As ToolStripMenuItem = menu1.DropDownItems.Item(i)

            Indice = Indice + 1
            Codigo = CodigoPadre & CSistema.FormatDobleDigito(Indice.ToString)

            'Debug.Print(Espacio & Codigo & " " & menu2.Text & " " & Tab + 1 & " " & menu2.Name & " " & menu2.Tag & " " & CodigoPadre)
            Debug.Print(Espacio & Codigo & " " & menu2.Text)

            Dim NewRow As DataRow = dt.NewRow

            NewRow("Codigo") = Codigo
            NewRow("Descripcion") = menu2.Text
            NewRow("Nivel") = Tab + 2
            NewRow("NombreControl") = menu2.Name
            NewRow("CodigoPadre") = CodigoPadre

            If menu2.Tag Is Nothing Then
                NewRow("Tag") = ""
            Else
                NewRow("Tag") = menu2.Tag.ToString
            End If

            dt.Rows.Add(NewRow)

            If menu2.DropDownItems.Count > 0 Then
                'Codigo = Codigo & (Indice).ToString
                MostrarArbol(menu2, Tab + 1, dt, Codigo, 0, Codigo)
            End If

siguiente:

        Next

    End Sub

    'Sub MostrarFormulario(ByVal toolstrip As ToolStripMenuItem, ByVal Titulo As String, ByVal tipo As FormBorderStyle, ByVal posicion As FormStartPosition, ByVal ShowDialog As Boolean)

    '    FGMostrarFormulario(Me, toolstrip.Tag, Titulo, tipo, posicion, ShowDialog)

    'End Sub

    Private Sub SalirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()

    End Sub

    Private Sub Salir()
        Try
            'Guarda en una lista los formularios a cerrar
            Dim formsToRemove As New List(Of Object)
            For Each frm As Form In My.Application.OpenForms
                If frm.Name <> Me.Name AndAlso frm.Name <> "frmLogin" Then
                    formsToRemove.Add(frm)
                End If
            Next
            'Cierra los formularios 
            For Each frm As Form In formsToRemove
                My.Application.OpenForms.Item(frm.Name).Close()
            Next

        Catch
        End Try

    End Sub

    Private Sub ActualizarDatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActualizarDatosToolStripMenuItem.Click
        Dim frm As New frmDescarga
        frm.ShowDialog()
        LiberarMemoria()
    End Sub

    Private Sub CambiarImagenDeFonoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CambiarImagenDeFonoToolStripMenuItem.Click
        Try
            OpenFileDialog1.ShowDialog()
            CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "IMAGEN", OpenFileDialog1.FileName)

            Dim ImagenFondo As String = OpenFileDialog1.FileName

            If ImagenFondo <> "" Then
                If My.Computer.FileSystem.FileExists(ImagenFondo) = True Then
                    Me.BackgroundImage = Image.FromFile(ImagenFondo)
                End If
            End If
        Catch
        End Try


    End Sub

    Private Sub frmPrincipal2_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If CerrandoSesion = False Then
            If MessageBox.Show("¿Desea salir del sistema?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                FGConfigurarMenu(Me.MenuStrip1)
                e.Cancel = True
                Exit Sub
            End If
        End If
        LiberarMemoria()
        Salir()

    End Sub

    Private Sub frmPrincipal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tFechaHora.Tick
        VGFechaHoraSistema = DateAdd(DateInterval.Second, 1, VGFechaHoraSistema)
        tsslFecha.Text = VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString
    End Sub

    Private Sub tiActualizarFechaHora_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tiActualizarFechaHora.Tick
        'Try
        '    VGFechaHoraSistema = CSistema.ExecuteScalar("Select GetDate()")
        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub PredeterminadoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PredeterminadoToolStripMenuItem.Click
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "IMAGEN", "")
        Me.BackgroundImage = Nothing ' My.Resources.EXPRESS_FONDO
    End Sub

    Private Sub CerrarSesionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CerrarSesionToolStripMenuItem.Click
        CerrandoSesion = True

        My.Application.ApplicationContext.MainForm = frmLogin

        frmLogin.Show()
        frmLogin.BringToFront()

        Me.Close() 'ver Sub Salir()
        CerrandoSesion = False

    End Sub

    Private Sub ClientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientesToolStripMenuItem.Click
        Dim frm As New frmCliente
        frm.UsuarioEsVendedor = vgUsuarioEsVendedor
        frm.UsuarioVendedor = vgUsuarioVendedor
        frm.UsuarioIDVendedor = vgUsuarioIDVendedor
        FGMostrarFormulario(Me, frm, "Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ProveedoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProveedoresToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmProveedor, "Proveedores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ProductosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProductosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmProducto, "Productos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub VendedoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VendedoresToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmVendedor, "Vendedores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CobradoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CobradoresToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCobrador, "Cobradores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub PromotoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PromotoresToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPromotor, "Promotores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub DistribuidoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DistribuidoresToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmDistribuidor, "Distribuidores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CargarToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CargarToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmListadoPrecios, "Listas de Precio", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub PaisCiudadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PaisCiudadToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPaisDepartamentoCiudadBarrios, "Localidades", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub TipoClienteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoClienteToolStripMenuItem.Click
        'FGMostrarFormulario(Me, frmTipoCliente, "Tipo de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
        frmTipoCliente.Show()
    End Sub

    Private Sub ZonasDeVentaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZonasDeVentaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmZonaVenta, "Zona de Ventas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub EstadoDeClientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EstadoDeClientesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmEstadoCliente, "Estado de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub UnidadesDeMedidaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UnidadesDeMedidaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmUnidadMedida, "Unidad de Medida", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub TransportistasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TransportistasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmTransporte, "Transportes de Distribución", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ChoferesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChoferesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmChofer, "Choferes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CamionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CamionesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCamion, "Camiones", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub TerminalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TerminalesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmTerminalTransaccion, "Terminales", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub Sucursales6ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Sucursales6ToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmSucursalDeposito, "Sucursales y Depósitos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub MonedaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MonedaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmMoneda, "Monedas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CotizacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CotizacionesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCotizacion, "Cotizaciones", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub BancosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BancosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmBanco, "Bancos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CuentasBancariasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CuentasBancariasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCuentaBancaria, "Cuentas Bancarias", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CargaToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CargaToolStripMenuItem1.Click
        frmNotaCreditoCliente.tipoComprobante = "NCR" 'JGr 20140816
        FGMostrarFormulario(Me, frmNotaCreditoCliente, "Nota de Crédito Cliente", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub ToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem2.Click
        FGMostrarFormulario(Me, frmTipoComprobante, "Tipos de Comprobantes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub MotivosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MotivosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmMotivo, "Motivos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub PuntosDeExpedicionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PuntosDeExpedicionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPuntoExpedicion, "Puntos de Expredición", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub FacturacionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FacturacionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmVenta, "Facturación", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
        frmVenta.Inicializar()
    End Sub

    Private Sub ConsultaDeComprobanteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultaDeComprobanteToolStripMenuItem.Click
        Dim frm As New frmConsultaVenta
        frm.Size = New Size(Me.ClientSize.Width - 75, Me.ClientSize.Height - 75)
        FGMostrarFormulario(Me, frm, "Consultas de Facturas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AnulacionDeFacturaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AnulacionDeFacturaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAdministrarVenta, "Anulación de Facturas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, True)
    End Sub

    Private Sub CargarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CargarToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmLoteDistribucion, "Lote de Distribucion de Facturas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub DevolucionDeLoteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DevolucionDeLoteToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmDevolucionLote, "Devolución de Mercaderías por Lote", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub RendicionToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RendicionToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmRendicionLote, "Rendición de Lote", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub EntradaPorComprasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntradaPorComprasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCompra, "Entrada de Mercaderías - Compras", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CargaToolStripMenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CargaToolStripMenuItem6.Click
        FGMostrarFormulario(Me, frmMovimientoStock, "Movimiento de Stock", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CargaToolStripMenuItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CargaToolStripMenuItem7.Click
        FGMostrarFormulario(Me, frmCargaMercaderia, "Picking - Carga de Mercaderías por Lote", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub IngresoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmChequeCliente, "Cheques de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ConsultaDeChequesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultaDeChequesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmConsultaChequeCliente, "Consulta de Cheques de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub RechazarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RechazarToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmRechazoChequeCliente, "Rechazar Cheques de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub PagoChequeClienteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PagoChequeClienteToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCanjeChequeCliente, "Canje de Cheques de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub DescuentosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DescuentosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmDescuentodeCheques, "Descuento de Cheques Diferidos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub ConsultaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmConsultaEfectivo, "Consulta de Efectivos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CargaToolStripMenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CargaToolStripMenuItem5.Click
        Dim frm As New frmOrdenPago
        frm.CargarAlIniciar = True
        frm.TipoPago = frmOrdenPago.ENUMTipoPago.Todos
        FGMostrarFormulario(Me, frm, "Orden de Pago", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CargaToolStripMenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CargaToolStripMenuItem3.Click
        Dim frm As New frmGastos
        frm.CajaChica = False
        FGMostrarFormulario(Me, frm, "Gastos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub ConfiguracionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfiguracionesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmConfiguraciones, "Configuraciones", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub TipoDeProductosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiClasificacion1.Click
        FGMostrarFormulario(Me, frmTipoProducto, vgConfiguraciones("ProductoClasificacion1").ToString, Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub LineaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiClasificacion2.Click
        FGMostrarFormulario(Me, frmLineas, vgConfiguraciones("ProductoClasificacion2").ToString, Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub MarcaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiClasificacion5.Click
        FGMostrarFormulario(Me, frmMarca, vgConfiguraciones("ProductoClasificacion5").ToString, Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem31_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiClasificacion6.Click
        FGMostrarFormulario(Me, frmPresentacion, vgConfiguraciones("ProductoClasificacion6").ToString, Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeComprobantesEmitidosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeComprobantesEmitidosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoFacturacionEmitida, "Listado de Facturas emitidas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CuentasFujasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CuentasFujasToolStripMenuItem.Click
        'FGMostrarFormulario(Me, frmCuentaFija, "Cuentas Fijas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
        'frmCuentaFija.Show(Me)
        Try
            FGMostrarFormulario(Me, frmCuentaFijas, "Cuentas Fijas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CargaToolStripMenuItem13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CargaToolStripMenuItem13.Click
        FGMostrarFormulario(Me, frmPlanillaDescuentoTactico, "Planilla de Descuentas Tacticos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub SubLineasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiClasificacion3.Click
        FGMostrarFormulario(Me, frmSubLinea, vgConfiguraciones("ProductoClasificacion3").ToString, Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub SubLinea2ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiClasificacion4.Click
        FGMostrarFormulario(Me, frmSubLinea2, vgConfiguraciones("ProductoClasificacion4").ToString, Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ConsultaSQLToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultaSQLToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmConsultasSQL, "Consultas SQL", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CargaToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CargaToolStripMenuItem2.Click
        If CSistema.ExecuteScalar("Select ProductoPrecio from Configuraciones where IDSucursal = " & vgIDSucursal) Then

        Else
            FGMostrarFormulario(Me, frmPedido, "Pedido", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False)
        End If

    End Sub

    Private Sub FacturacionDePedidosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FacturacionDePedidosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmFacturarPedidos, "Facturacion de Pedidos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, True)
    End Sub

    Private Sub ZonaEnDepositoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZonaEnDepositoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmZonaDeposito, "Zonas de Depositos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub EquiposDeConteoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EquiposDeConteoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmEquipoConteo, "Equipos de Conteo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub VeremosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VeremosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPlanillaInventario, "Planilla de Inventario", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub TomaDeInventarioFisicoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TomaDeInventarioFisicoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmTomaInventario, "Toma de Inventario", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ComprarativoEntreEquiposToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComprarativoEntreEquiposToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmComparativoInventarioEquipo, "Comparativo entre Equipos de Conteo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ComprarativoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComprarativoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmComparativoInventarioSistema, "Comparativo entre Fisico y Sistema", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AjusteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AjusteToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAjusteControlInventario, "Ajuste de inventario", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AccesosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccesosToolStripMenuItem.Click
        FGMostrarFormulario(Me, sender.Tag, "Accesos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ReconfigurarMenuToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReconfigurarMenuToolStripMenuItem.Click
        MostrarArbol()
    End Sub

    Private Sub ListadoDeComprobantesAnuladosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeComprobantesAnuladosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoVentaAnulada, "Listado de Facturas Anuladas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub PlanillaDeCobranzasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PlanillaDeCobranzasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPlanillaCobranza, "Planilla de Cobranzas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ModificarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmModificarChequeCliente, "Modificar Cheque Cliente", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub EfectivisarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EfectivisarToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmHabilitarParaPagoEfectivo, "Habilitar Para Pago Efectivo ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub FechaDeFacturacionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FechaDeFacturacionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmFechaFacturacion, "Fecha de Facturacion", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub PerfilesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PerfilesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPerfil, "Perfiles de Acceso", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub UsuariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UsuariosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmUsuario, "Perfiles de Acceso", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CambiarUsuarioContraseñaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CambiarUsuarioContraseñaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCambiarUsuarioContraseña, "Cambiar contraseña", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub LogToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LogToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmLogAccesos, "Log de Accesos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ModifcarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModifcarToolStripMenuItem.Click
        Dim frm As New frmGastoModificar
        frm.CajaChica = False
        FGMostrarFormulario(Me, frm, "Modificar Gastos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AplicarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AplicarToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAplicacionNotaCreditoCliente, "Nota de Credito - Aplicaciones a Facturas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub TransferenciaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TransferenciaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmNotaCreditoTransferencia, "Nota de Credito - Transferencias de Saldos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeNotasDeCreditoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeNotasDeCreditoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoNotaCredito, "Listado de Notas de Credito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeAplicacionPendienteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeAplicacionPendienteToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoNotaCreditoAplicar, "Listado de Notas de Credito - Aplicaciones", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem4.Click
        FGMostrarFormulario(Me, frmNotaDebitoCliente, "Nota de Debito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AplicarToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AplicarToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmAplicacionNotaDebitoCliente, "Nota de Debito - Aplicaciones", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem33_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem33.Click
        FGMostrarFormulario(Me, frmListadoNotaCreditoDetalle, "Listado de Notas de Credito - Detallado", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub PlanillaParaTomaDeInventarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PlanillaParaTomaDeInventarioToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmInformeTomaInventario, "Planilla de Toma de Inventario", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub MovimientosPorProductToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MovimientosPorProductToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmMovimientoProducto, "Extracto de Movimientos de Producto", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub ProductosAReponerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProductosAReponerToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmExistenciaMinimaSobreMaxima, "Existencias", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub ResumenDeExistenciaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResumenDeExistenciaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmExistenciaMovimientoCalculado, "Resumen de Existencia", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub ExistenciaValorizadaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExistenciaValorizadaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmExistenciaValorizada, "Existencia Valorizada", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub DetalleDeDevolucionesToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DetalleDeDevolucionesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoLoteDetalleDevolucion, "Listado Detalle Devolución Lotes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub GráficoVentaMesTotalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GráficoVentaMesTotalToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmGraficoVentaMesTotal, "Gráfico Venta Mes Total", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub GráficoRankingClientesVentaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GráficoRankingClientesVentaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmGraficoRankingClientesVenta, "Gráfico Ranking Clientes Venta", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub GráficoProductosMásRentablesToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GráficoProductosMásRentablesToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmGraficoRankingProductoRentable, "Gráfico Ranking Producto más rentable", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub GráficoProductosMásVendidoToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GráficoProductosMásVendidoToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmGraficoRankingProductoMasVendido, "Gráfico Ranking Producto más vendido", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem30_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem30.Click
        FGMostrarFormulario(Me, frmListadoNotaDebito, "Listado de Notas Debito ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem32_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem32.Click
        FGMostrarFormulario(Me, frmListadoNotaDebitoAplicar, "Listado de Notas Debito no Aplicados ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub DevolucionesPorProductoClienteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DevolucionesPorProductoClienteToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoNotaDebitoDetalle, "Listado de Notas Debito Detallado ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ResumenDeLotesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResumenDeLotesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoLoteEmitido, "Listado Lotes Emitidos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub HojaDeRutaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HojaDeRutaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmHojaRuta, "Hoja de Ruta", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub PlanillaDeCargaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PlanillaDeCargaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCargaPlanilla, "Carga de Planilla", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ComprobantesSinLotesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComprobantesSinLotesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoComprobanteSinLote, "Listado de Comprobantes sin Lotes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoGeneralDeComprobantesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoGeneralDeComprobantesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoFacturaCompraEmitida, "Listado Facturas Compras Emitidas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem17_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem17.Click
        FGMostrarFormulario(Me, frmNotaDebitoProveedor, "Nota de Debito :: Proveedores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem15.Click
        FGMostrarFormulario(Me, frmListarNotaCreditoProveedor, "Listado de Nota de Credito :: Proveedores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem18.Click
        FGMostrarFormulario(Me, frmListarNotaDebitoProveedor, "Listado de Nota de Debito :: Proveedores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem14.Click
        FGMostrarFormulario(Me, frmNotaCreditoProveedor, "Nota de Credito :: Proveedores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeChequesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeChequesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoCheque, "Informe Cheque Cliente ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeChequesRechazadosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeChequesRechazadosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmSaldoChequesDiferidos, "Informe Saldo Cheques Diferidos ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ChequesDiferidosDepositadosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChequesDiferidosDepositadosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmChequesDiferidosDepositados, "Informe  Cheques Diferidos Depositados ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeCanjesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeCanjesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoCanje, "Informe Listado Canje ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeDescuentosDeChequesDiferidosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeDescuentosDeChequesDiferidosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoIngresoChequesDiferidos, "Informe Listado Cheques Diferidos ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ImprimirToolStripMenuItem3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImprimirToolStripMenuItem3.Click
        FGMostrarFormulario(Me, frmImpresionCheque, "Imprimir Cheque", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ImprimirOrdenDePagoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImprimirOrdenDePagoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmImpresionOrdenPago, "Imprimir Orden de Pago", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CobranzaCreditoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CobranzaCreditoToolStripMenuItem.Click
        Dim frm As New frmCobranzaCredito
        frm.Inicializar()
        frm.PropietarioBD = vgConfiguraciones("PropietarioBD")
        FGMostrarFormulario(Me, frm, "Cobranzas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CobranzaContadoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CobranzaContadoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCobranzaContado, "Cobranza por Lotes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub DepositosBancariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DepositosBancariosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmDepositoBancario, "Depositos Bancarios", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub DebitosYCreditosBancariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DebitosYCreditosBancariosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmDebitoCreditoBancario, "Debitos y Creditos Bancarios", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
        frmDebitoCreditoBancario.Inicializar()
    End Sub

    Private Sub ConciliacionBancariaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConciliacionBancariaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmConciliaciondeMovimientoBancaria, "Concilacion Movimiento Bancario ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub InventarioDeDocumentosPendientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InventarioDeDocumentosPendientesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmInventariodeDocumentosPendientesCobrar, "Inventario de Documentos Pendientes ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub BusquedaDeClientesConFacturasAtrasadasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BusquedaDeClientesConFacturasAtrasadasToolStripMenuItem.Click

    End Sub

    Private Sub DistribucionDeSaldosDeDocumentosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DistribucionDeSaldosDeDocumentosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmDistribuciondeSaldoDocumentoaCobrar, "Distribucion de Saldo de Documentos a Cobrar ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ResumenDeMovimientosYSaldosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResumenDeMovimientosYSaldosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmResumendeMovimientoSaldo, "Resumen de Movimientos y Saldos ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem22_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem22.Click
        FGMostrarFormulario(Me, frmInventariodeDocumentosPendientesAPagar, "Inventario de documentos pendientes a Pagar", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem24_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem24.Click
        FGMostrarFormulario(Me, frmDistribuciondeSaldoDocumentoaPagar, "Distribuidor de saldos de documentos a pagar", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub DocumentosPagadosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DocumentosPagadosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmDocumentoPagado, "Documentos pagados", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem25.Click
        FGMostrarFormulario(Me, frmMovimientoSaldoProveedor, "Resumen de movimientos y saldos de proveedores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ExtractoDeMovimientosToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExtractoDeMovimientosToolStripMenuItem2.Click
        FGMostrarFormulario(Me, frmExtractoMovimientoBancario, "Extracto de Movimientos Bancarios ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub SaldosBancariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaldosBancariosToolStripMenuItem.Click
        CSistema.ExecuteNonQuery("Delete SaldoBanco")
        Dim CReporte As New ERP.Reporte.CReporteBanco
        CReporte.SaldoBanco()
    End Sub

    Private Sub ResumenDiarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResumenDiarioToolStripMenuItem.Click

    End Sub

    Private Sub ExtractoDeMovimientosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExtractoDeMovimientosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmExtractoMovimientoCliente, "Extracto de movimientos de clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeClientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeClientesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoCliente, "Indice general de clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeClientesQueNoCompranToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeClientesQueNoCompranToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoClienteNoCompra, "Listado de Clientes que no compran en un periodo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ExtractoDeMovimientosToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExtractoDeMovimientosToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmExtractoMovimientoProveedor, "Extracto de movimientos de proveedores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeProveedoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeProveedoresToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoProveedor, "Indice general de proveedores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeProductosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeProductosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoProducto, "Indice general de productos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub DefinirFondoFijoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DefinirFondoFijoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmFondoFijo, "Establecer Fondo Fijo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ValesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ValesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmVale, "Vales", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ReposicionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReposicionToolStripMenuItem.Click
        Dim frm As New frmGastos
        frm.CajaChica = True
        FGMostrarFormulario(Me, frm, "Fondo Fijo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub EmitirListadoAReponerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmitirListadoAReponerToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmComprobanteReponer, "Comprobantes a Reponer", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub RendicionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RendicionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmRendicionFondoFijo, "Rendicion de Fondo Fijo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub DiseñadorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DiseñadorToolStripMenuItem.Click

    End Sub

    Private Sub AsientosContablesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AsientosContablesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAsientoDiario, "Asientos Contables", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub FacturaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FacturaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmSeleccionFormatoImpresion, "Impresion de Documentos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AsientoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AsientoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmConsultaAsientoContable, "Asientos contables", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CierreDiarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CierreDiarioToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCierreDiario, "Asientos contables :: Cierre Diario", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CierreMensualToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CierreMensualToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCierreMensual, "Asientos contables :: Cierre Mensual", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CierreEjercicioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CierreEjercicioToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCierreEjercicio, "Asientos contables :: Cierre del Ejercicio", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub LibroIVAToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LibroIVAToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmLibroIVA, "Libro IVA", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ComprobantesParaLibroVentaYCompraToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComprobantesParaLibroVentaYCompraToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmConsultaComprobanteLibroIVA, "Comprobanres de Libro IVA", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ActualizarTimbradoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActualizarTimbradoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmActualizarTimbrado, "Actualizar timbrado", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub InformeToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InformeToolStripMenuItem2.Click
        FGMostrarFormulario(Me, frmPedidos, "Informe de Pedidos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub RenumeracionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RenumeracionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmRenumeracionAsiento, "Renumeracion de Asientos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub RegenerarAsientosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegenerarAsientosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmRegererarAsiento, "Regenerar Asientos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub RetencionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RetencionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmRetencionIVA, "Retenciones", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub LibroVentasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FGMostrarFormulario(Me, frmHechaukaLibroVentas, "Libro de Ventas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub LibroComprasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FGMostrarFormulario(Me, frmHechaukaLibrodeCompra, "Libro de Compras", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub LibroRetencionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FGMostrarFormulario(Me, frmHechaukaRenta, "Libro de Renta", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeGastosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeGastosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoGasto, "Listado de Gastos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ETLToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ETLToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmETL, "Informe ETL", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CambioGlobalDeCuentaContableToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CambioGlobalDeCuentaContableToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCambioGlobalCuentaContable, "Cambio de Cuenta Contable Global", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ImportarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImportarToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmImportarPedidos, "Importar pedidos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub DMSToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DMSToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmDMS, "Informe DMS", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub InformeToolStripMenuItem10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InformeToolStripMenuItem10.Click
        FGMostrarFormulario(Me, frmListadoMovimientoProducto, "Listado de Comprobantes de Stock", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub InformeToolStripMenuItem11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InformeToolStripMenuItem11.Click
        FGMostrarFormulario(Me, frmListadoCargaMercaderia, "Listado de Carga de Mercaderias (Picking)", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub LibroMayorToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles LibroMayorToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmLibroMayor, "Listado Mayor", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ValesPendientesDeRendicionToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ValesPendientesDeRendicionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoVale, "Listado de Vales", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListadosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoCobranzaCredito, "Listado de Cobranzas Credito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem34_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripMenuItem34.Click
        FGMostrarFormulario(Me, frmListadoCobranzaContado, "Listado de Cobranzas Contado", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeDepositosBancariosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListadoDeDepositosBancariosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoDepositoBancario, "Listado de Depositos Bancarios", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeDebitosYCreditosBancariosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListadoDeDebitosYCreditosBancariosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoDebitoCreditoBancario, "Listado de Debitos/Creditos Bancarios", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeOrdenDePagoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListadoDeOrdenDePagoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoOrdenPago, "Listado de Ordenes de Pago", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ComisionesPorVendedorToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ComisionesPorVendedorToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmComisionesporVendedor, "Comisiones de Vendedores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub BalanceGeneralToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles BalanceGeneralToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmBalanceGeneralEstadoResultado, "Balance General y Estado de Resultado", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub RegenerarSaldosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RegenerarSaldosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmRecalcularPlanCuentaSaldo, "Regenerar Saldos de Cuentas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub BalandeDeComprobacionToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles BalandeDeComprobacionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmBalanceComprobacion, "Balance de Comprobacion", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ComprobantesNoEmitidosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComprobantesNoEmitidosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmComprobantesNoEmitidos, "Comprobantes no Emitidos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ClasesWindowsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClasesWindowsToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmClasesWindows, "Clases de Windows", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AdministrarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AdministrarToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCuentaContable, "Cuentas Contables", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem31_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem31.Click
        FGMostrarFormulario(Me, frmCambiarPlanCuenta, "Establece Plan de Cuenta", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem26_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem26.Click
        FGMostrarFormulario(Me, frmInventarioInicial, "Inventario Inicial", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub GruposYUsuarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GruposYUsuarioToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmGrupo, "Grupos y Usuarios", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem7.Click
        FGMostrarFormulario(Me, frmListadoVentasPorProducto, "Informe :: Ventas por Producto", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub CargarToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CargarToolStripMenuItem2.Click
        FGMostrarFormulario(Me, frmEfectivo, "Carga de Efectivo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub EfectivizacionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EfectivizacionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmEfectivizacion, "Efectivizacion de Cheque", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub ModificarComprobanteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarComprobanteToolStripMenuItem.Click
        Dim frm As New frmGastoModificar
        frm.CajaChica = True
        FGMostrarFormulario(Me, frm, "Modificar Comprobantes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ResumenDeCobranzasPorFacturasContadoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResumenDeCobranzasPorFacturasContadoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmResumenCobranzaDiaria, "Resumen Diario de Cobranzas por Facturas Contado", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem36_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem36.Click
        FGMostrarFormulario(Me, frmControlExistencia, "Recalcular Stock", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub DMSMinutasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DMSMinutasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmDMSCubo, "DMS Cubo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub TotalPorProveedorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FGMostrarFormulario(Me, frmVentasPorClienteProveedor, "Ventas por Cliente/Proveedor", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub VentasPorClienteToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VentasPorClienteToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmListadoVentasPorCliente, "Ventas por Cliente", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub OperacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OperacionesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmOperacion, "Operacion", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ImportarDatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImportarDatosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmImportarDatosOtros, "Importar Datos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, True)
    End Sub

    Private Sub CategoriasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CategoriasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCategoria, vgConfiguraciones("ProductoClasificacion7").ToString, Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ConfigurarActualizacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfigurarActualizacionesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAccesoActualizacion, "Configurar actualizaciones", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ComprobarSiExistenActualizacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComprobarSiExistenActualizacionesToolStripMenuItem.Click
        FGActualizacion(True, True)
    End Sub

    Private Sub VentasAnualesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VentasAnualesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoVentasTotalAnualProductoCliente, "Ventas Anuales", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem41_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem41.Click
        FGMostrarFormulario(Me, frmAsientoDiario, "Asientos Contables", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AcercaDeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AcercaDeToolStripMenuItem.Click
        Dim f As New frmBienvenida
        f.Text = "Acerca de"
        f.ShowIcon = False
        f.ControlBox = True
        f.MaximizeBox = False
        f.MinimizeBox = False
        f.ShowDialog()
    End Sub

    Private Sub ToolStripMenuItem42_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem42.Click
        FGMostrarFormulario(Me, frmLibroIVA, "Libro IVA", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem43_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem43.Click
        FGMostrarFormulario(Me, frmLibroMayor, "Listado Mayor", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem44_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem44.Click
        FGMostrarFormulario(Me, frmComprobantesNoEmitidos, "Comprobantes no Emitidos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem45_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem45.Click
        FGMostrarFormulario(Me, frmBalanceGeneralEstadoResultado, "Balance General y Estado de Resultado", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem46_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem46.Click
        FGMostrarFormulario(Me, frmBalanceComprobacion, "Balance de Comprobacion", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem8.Click
        FGMostrarFormulario(Me, frmVentasPorClienteProveedor, "Ventas por Cliente / Proveedor", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub TiposDeTarjetasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TiposDeTarjetasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmTipoTarjeta, "Tipos de Tarjeta", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ConexionAServidorToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ConexionAServidorToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmConexionServidor, "Base de Datos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AparienciaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AparienciaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmApariencia, "Apariencia", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub LibroDiarioToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles LibroDiarioToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmLibroDiario, "Listado Diario", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub NCRDocumentoInternoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles NCRDocumentoInternoToolStripMenuItem.Click
        frmNotaCreditoCliente.tipoComprobante = "NCIC" 'JGr 20140816
        FGMostrarFormulario(Me, frmNotaCreditoCliente, "Nota de Crédito de Cliente - [Documento interno]", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeRetencionesRecibidasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListadoDeRetencionesRecibidasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoRetencionRecibida, "Listado de Retenciones Recibidas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AsociarRetencionesDeClientesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AsociarRetencionesDeClientesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAsociarRetencionCliente, "Asociar Retenciones de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub HechaukaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles HechaukaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmHechaukaLibroVentas1, "Generar archivo para hechauka", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
        'FGMostrarFormulario(Me, frmHechaukaLibroVentas, "Generar archivo para hechauka", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub DocumentosSinAsientosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DocumentosSinAsientosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmDocumentoSinAsiento, "Documentos sin Asientos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CajaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CajaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAdministrarCaja, "Cajas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub TeleventasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles TeleventasToolStripMenuItem.Click
        Dim frm As New FrmTeleventas
        frm.Inicializar()
        'frm.PropietarioBD = vgConfiguraciones("PropietarioBD")
        FGMostrarFormulario(Me, frm, "Televentas", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False)
        'FGMostrarFormulario(Me, FrmTeleventas, "Televentas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub TipoDeLlamadaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles TipoDeLlamadaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmTipoLlamada, "Tipo de llamada", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ResultadoDeLlamadaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ResultadoDeLlamadaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmResultadoLlamada, "Resultado de llamada", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    'Private Sub AprobacionDePedidosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AprobacionDePedidosToolStripMenuItem.Click
    '    FGMostrarFormulario(Me, frmAprobacionPedido, "Aprobacion de Pedidos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    'End Sub

    Private Sub FormaDePagoFacturasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles FormaDePagoFacturasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmFormaPagoFactura, "Forma de pago Facturas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AreasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AreasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmArea, "Areas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub RutasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RutasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmRuta, "Rutas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub PrepararPagosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PrepararPagosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPrepararPago, "Preparacion de Pagos a Proveedores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, True)
    End Sub
    Private Sub AprobacionToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AprobacionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAprobarPedido, "Aprobacion de pedidos.", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, True)
    End Sub

    Private Sub EntregaDeChequesOPToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntregaDeChequesOPToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmEntregaChequeOP, "Entrega de Cheques OP", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub RealizarPAgosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RealizarPAgosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPagoEgreso, "Realizar los Pagos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, True)
    End Sub

    Private Sub CambiarVendedorToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CambiarVendedorToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCambiarVendedor, "Cambio de Vendedor de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, False)
    End Sub

    Private Sub CostosDeProductosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CostosDeProductosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmProductoCosto, "Costos de Productos", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub CargaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CargaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPrestamoBancario, "Prestamo Bancario", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub AnticipoAProveedoresToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AnticipoAProveedoresToolStripMenuItem.Click
        Dim frm As New frmOrdenPagoAnticipo
        frm.CargarAlIniciar = True
        FGMostrarFormulario(Me, frm, "Orden de Pago - Anticipo a Proveedores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AplicacionAnticipoAProveedoresToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AplicacionAnticipoAProveedoresToolStripMenuItem.Click
        Dim frm As New frmAplicacionAnticipoProveedores
        frm.CargarAlIniciar = True
        FGMostrarFormulario(Me, frm, "Aplicacion de Anticipo a Proveedores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)

    End Sub

    Private Sub EgresosARendirToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles EgresosARendirToolStripMenuItem.Click
        Dim frm As New frmOrdenPagoEgresosARendir
        frm.CargarAlIniciar = True
        FGMostrarFormulario(Me, frm, "Orden de Pago - Egresos a Rendir", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub InformePrestamoBancarioToolStripMenuItem2_Click(sender As Object, e As System.EventArgs) Handles InformePrestamoBancarioToolStripMenuItem2.Click
        Dim frm As New frmListadoPrestamoBancario
        FGMostrarFormulario(Me, frm, "Listado de Prestamos Bancarios", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CentroDeCostosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CentroDeCostosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCentroCostoAsiento, "Centro de Costos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub PlanDeLlamadasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PlanDeLlamadasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPlanDeLlamadas, "Plan de Llamadas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub LIstadoDeChequesEmitidosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles LIstadoDeChequesEmitidosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoChequesEmitidos, "Listado de Cheques Emitidos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub VentasAExcelToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles VentasAExcelToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmVentasExcel, "Listado de Ventas para Pasar a Excel", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub ChequeraToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ChequeraToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmChequera, "Chequeras", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub AsignarCuentaContableAFondoFijoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AsignarCuentaContableAFondoFijoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmFondoFijoCuentaContable, "Asignacion de Cuenta Contable a Fondo Fijo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub AdministrarToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles AdministrarToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmAdministrarEgresos, "Administrar Egresos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, True)
    End Sub

    Private Sub InformeToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles InformeToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoValeGasto, "Listado de Vales-Gastos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeExcepcionesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListadoDeExcepcionesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoExcepciones, "Listado de Excepciones Recibidas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeRetencionesEmitidasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListadoDeRetencionesEmitidasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmRetencionEmitida, "Listado de Retenciones Emitidas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AplicarValeAGastoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AplicarValeAGastoToolStripMenuItem.Click
        Dim frm As New frmGastoAplicacionVales
        frm.CajaChica = True
        FGMostrarFormulario(Me, frm, "Aplicar vale a Gasto", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ModificarToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles ModificarToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmMovimientoModificar, "Movimientos - Modificar", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub TipoDeDepositoToolStripMenuItem_Click_1(sender As System.Object, e As System.EventArgs) Handles TipoDeDepositoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmTipoDeposito, "Tipo de deposito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub ListadoDeProductoPorOperacionesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListadoDeProductoPorOperacionesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoOperacionProducto, "Listado de productos por operaciones", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub GenerarCuotasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles GenerarCuotasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmGenerarCuotasGastos, "Generar Cuotas Gastos con saldo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub MotivosDeAnulacionDeVentaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles MotivosDeAnulacionDeVentaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmMotivoAnulacionVenta, "Motivos de anulacion de venta", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub ListadoDeEstadoDeChequesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListadoDeEstadoDeChequesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoEstadoCheques, "Listado de estado de cheques", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub DescargaDeStockToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DescargaDeStockToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmDescargaStock, "Descarga de Stock", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ExtractoDeMovimientosTodosLosProveedoresToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExtractoDeMovimientosTodosLosProveedoresToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmExtractoMovimientoProveedorTodos, "Extracto de movimientos de todos los proveedores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub VencimientoDeChequesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles VencimientoDeChequesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmVencimientoChequeEmitido, "Vencimiento de Cheques diferidos emitidos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub InformeTicketDeTarjetasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles InformeTicketDeTarjetasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmTarjetas, "Inventario de Tarjetas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ToolStripMenuItem9_Click_1(sender As System.Object, e As System.EventArgs) Handles ToolStripMenuItem9.Click
        FGMostrarFormulario(Me, frmMotivoAnulacionNotaCredito, "Motivos de anulacion de Nota de Credito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub ClientesToolStripMenuItem2_Click(sender As System.Object, e As System.EventArgs) Handles ClientesToolStripMenuItem2.Click
        FGMostrarFormulario(Me, frmAuditoriaClientes, "Auditoria de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ProductosToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles ProductosToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmAuditoriaProductos, "Auditoria de Productos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ExcepcionesDePreciosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExcepcionesDePreciosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAuditoriaExcepcionesPrecios, "Auditoria de Excepciones de Precios", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub DocumentosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DocumentosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAuditoriaDocumentos, "Auditoria de Documentos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AbogadoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AbogadoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAbogados, "Abogados", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub InactivacionDeClientesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles InactivacionDeClientesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmInactivacionClientes, "Inactivacion de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub UnidadDeNegociosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles UnidadDeNegociosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmUnidadDeNegocioAsiento, "Unidad de Negocio", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ConsumoCombustibleToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ConsumoCombustibleToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmConsumoCombustible, "Consumo de Logistica", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub ConsumoMateriaPrimaBalanceadoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ConsumoMateriaPrimaBalanceadoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmMovimientoMateriaPrima, "Materia Prima", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub UsuariosUnidadDeNegocioYCCToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles UsuariosUnidadDeNegocioYCCToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmUsuarioUnidadNegocio, "Usuario - Unid.Negocio - CC", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ExtractoDeMovimientoDeDocumentosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExtractoDeMovimientoDeDocumentosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmExtractoMovimientoDocumentoProveedor, "Extracto por Documento de Proveedor", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub SaldoDiarioToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SaldoDiarioToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCuentaBancariaSaldoDiario, "Saldo Diario de Cuenta Bancaria", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub SaldoDiarioDeBancoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SaldoDiarioDeBancoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmSaldoBancariaDiario, "Cuenta Bancaria Saldo Diario ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub KilosTransportadosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles KilosTransportadosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmInformeKilosTransportados, "Listado de Kgs Transporados ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)

    End Sub

    Private Sub InformeAcreditacionesBancariaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles InformeAcreditacionesBancariaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAcreditaconBancaria, "Acreditacion Bancaria", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub DescagaDeComprasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DescagaDeComprasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmMovimientoDescargaCompra, "Descarga de Compras", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub InformeToolStripMenuItem4_Click(sender As System.Object, e As System.EventArgs) Handles InformeToolStripMenuItem4.Click
        FGMostrarFormulario(Me, frmListadoRemision, "Listado de Remisiones ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub FormaDePagoCobranzaDepositoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles FormaDePagoCobranzaDepositoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmInformeFormaPagoCobranzaDeposito, "Forma de Pago", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub IngresoTotalToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles IngresoTotalToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmInformeFinancieroIngresos, "Ingreso Total", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ControlTesoreriaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ControlTesoreriaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmInformeControlTesoreria, "Control de Tesoreria", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeNCConVentasAplicadasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListadoDeNCConVentasAplicadasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoNotaCreditoVentaAplicada, "Listado de NC con venta aplicada", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CierreYReaperturaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CierreYReaperturaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAperturaCierre, "Cierre y Reapertura", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AcuerdoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AcuerdoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAcuerdo, "Acuerdo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub LibroMayorToolStripMenuItem1_Click_1(sender As System.Object, e As System.EventArgs) Handles LibroMayorToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmLibroMayorUNCC, "Listado Mayor UN - CC", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub TicketDeBasculaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles TicketDeBasculaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmTicketBascula, "Ticket de Bascula", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CamionesToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles CamionesToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmCamionProveedor, "Camion del Proveedor", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ChoferesToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles ChoferesToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmChoferProveedor, "Choferes del Proveedor", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub MacheoFacturaTicketToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles MacheoFacturaTicketToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmMacheoFacturaTicket, "Macheo Factura - Ticket", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub KardexToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles KardexToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmKardex, "Kardex", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AcuerdoToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles AcuerdoToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmInformeAcuerdo, "Listado Acuerdo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub TicketDeBasculaToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles TicketDeBasculaToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmInformeTicketBascula, "Listado Ticket de Bascula", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub VencimientoDeChequeDescontadoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles VencimientoDeChequeDescontadoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmVencimientoChequeDescontado, "Vencimiento de Cheques descontados", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub PedidoParaFábricaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PedidoParaFábricaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmOrdenDePedido, "Orden de Pedido", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub RankingDeProveedoresToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RankingDeProveedoresToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmRankingProveedores, "Ranking de proveedores", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub InformeToolStripMenuItem5_Click(sender As System.Object, e As System.EventArgs) Handles InformeToolStripMenuItem5.Click
        FGMostrarFormulario(Me, frmListadoOrdenDePedido, "Listado de Orden de Pedido", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeDescuentosConcedidosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListadoDeDescuentosConcedidosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoDetalladoDescuento, "Listado de Descuentos Concedidos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AsignarAcuerdoATicketToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AsignarAcuerdoATicketToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAsignarAcuerdoTicket, "Asignar Acuerdo a Ticket de Bascula", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AsignarCostoAProductoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AsignarCostoAProductoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmProductoCosto, "Asignar Costo al Producto", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CargaDeCamionesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CargaDeCamionesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAsignacionCamion, "Carga de Camiones Abastecimiento", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AsignacionDeLimiteDeDescuentoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AsignacionDeLimiteDeDescuentoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAsignarLimiteDescuento, "Limite de Descuentos Usuario - Lista de Precio", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CargaDeCamionesPedidoDeClientesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CargaDeCamionesPedidoDeClientesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAsignacionCamionPedidoCliente, "Carga de Camiones Pedido de clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub PedClientesPendientesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PedClientesPendientesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPedidoClientes, "Informe de Pedidos de Clientes - Carga Camiones", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeChequesRechazadosToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles ListadoDeChequesRechazadosToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmListadoChequesRechazados, "Informe de Cheques Rechazados ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AbastecimientoPendienteToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AbastecimientoPendienteToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoAbastecimientoCamion, "Informe Abastecimiento a Sucursal", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub TicketDeBasculaSinCostoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles TicketDeBasculaSinCostoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmInformeTicketBasculaSinCostovb, "Listado Ticket de Bascula", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub InformeAnticipacionToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles InformeAnticipacionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPedidoAnticipado, "Informe de Pedidos anticipados", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub MetasDeVentaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles MetasDeVentaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmMeta, "Metas de Venta", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CumpleañosYAniversariosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CumpleañosYAniversariosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAniversariosyCumpleaños, "Aniversarios y Cumpleaños", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub MacheoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles MacheoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmInfomeMacheoFacturaTicketBascula, "Listado de Macheo Factura Ticket Bascula", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub TotalGeneralToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles TotalGeneralToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmTotalGeneral, "Informe Gerencial - Totl General", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeChequesDescontadosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListadoDeChequesDescontadosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoChequesDescontados, "Listado de Vencimiento de Cheques Descontados", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ResumenDeSaldosUnidadDeNegociosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ResumenDeSaldosUnidadDeNegociosToolStripMenuItem.Click
        'FGMostrarFormulario(Me, frmResumenUnidadNegocio, "Resumen de Unidad de Negocio", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, True)
        FGMostrarFormulario(Me, frmResumenUnidadNegocio1, "Resumen de Unidad de Negocio", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub AprobarExcesoDeLineaDeCreditoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AprobarExcesoDeLineaDeCreditoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAprobarExcesoLineaCredito, "Aprobacion de pedidos con exceso de Credito.", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False, True, True)
    End Sub

    'Private Sub HabilitacionDePorcentajeDeDescuentoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles HabilitacionDePorcentajeDeDescuentoToolStripMenuItem.Click
    '    
    'End Sub

    Private Sub ListadoDeRecibosGeneradosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListadoDeRecibosGeneradosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoRecibosGenerados, "Listado de Recibos Generados", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub


    'Private Sub ABMSubMotivosDeNotasDeCreditoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ABMSubMotivosDeNotasDeCreditoToolStripMenuItem.Click
    '    FGMostrarFormulario(Me, frmABMSubMotivoNC, "Sub Motivo de Nota de Credito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    'End Sub

    Private Sub PedidosDeNotaDeCreditoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PedidosDeNotaDeCreditoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPedidoNotaCredito, "Pedidos de Notas de Credito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    'Private Sub ABMUsuariosParaAutorizarToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ABMUsuariosParaAutorizarToolStripMenuItem.Click
    '    FGMostrarFormulario(Me, frmUsuarioAutorizarPedidoNotaCredito, "Usuarios para Autorizar Pedidos de Notas de Credito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    'End Sub

    Private Sub AprobarPedidosDeNotasDeCredutoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AprobarPedidosDeNotasDeCredutoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAprobarPedidoNotaCredito, "Aprobar Pedidos de Notas de Crédito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, True)
    End Sub

    Private Sub ProcesarPedidoDeNotasDeCreditoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ProcesarPedidoDeNotasDeCreditoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmProcesarPedidoNotaCredito, "Procesar Pedidos de Notas de Crédito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub OperacionesPermitidasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles OperacionesPermitidasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmOperacionPermitida, "Operaciones Permitidas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CreditoDeClientesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CreditoDeClientesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCreditoCliente, "Credito de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CargaParaAbastecimientoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CargaParaAbastecimientoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoAbastecimientoSucursalDetallado, "Listado Abastecimientos Detallados", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ComprobantesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ComprobantesToolStripMenuItem.Click
        Dim frm As New frmGastos
        frm.CajaChica = False
        frm.RRHH = True
        FGMostrarFormulario(Me, frm, "Carga de Comprobantes de Recursos Humanos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)

    End Sub

    Private Sub AsignarCuentaContableARRHHToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AsignarCuentaContableARRHHToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmRRHHCuentaContable, "Asignar Cuenta Contable a RRHH", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ControlDeCostosDeOperacionesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ControlDeCostosDeOperacionesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmControlCosto, "Control de Costos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListadoDeMovimientosDeCombustiblesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListadoDeMovimientosDeCombustiblesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoMovimientoCombustible, "Movimientos de Combustibles", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub TicketDeBasculaSinMachearAFechaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TicketDeBasculaSinMachearAFechaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmInformeTicketBasculaSinMacheoAFecha, "Ticket de Bascula Sin Machear a Fecha", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub SolicitudesDeExcepcionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SolicitudesDeExcepcionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmSolicitudExcepcion, "Solicitudes de Descuento", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)

    End Sub

    'Private Sub PorcentajeDeExcepcionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PorcentajeDeExcepcionToolStripMenuItem.Click
    '    FGMostrarFormulario(Me, frmPorcentajeExcepcion, "Porcentaje de Excepcion", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    'End Sub

    Private Sub CategoriaDeClientesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CategoriaDeClientesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCategoriaCliente, "Categoria de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub FormatoDeImpresiónToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FormatoDeImpresiónToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmImpresion, "Formato de Impresión", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False, True)
    End Sub

    Private Sub PedidoNotaDeCreditoMovilToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PedidoNotaDeCreditoMovilToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPedidoNotaCreditoMovil, "Pedido Nota de Credito Movil", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False, True)
    End Sub

    Private Sub PedidoMovilToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PedidoMovilToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPedidoMovil, "Pedido Movil", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False, False)
    End Sub

    Private Sub SolicitudesDeAnulacionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SolicitudesDeAnulacionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmSolicitudAnulacion, "Solicitud de Anulacion", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False, False)
    End Sub

    Private Sub PorcentajeDeExcesoDeLineaDeCreditoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PorcentajeDeExcesoDeLineaDeCreditoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPorcentajeExcesoLineaCredito, "Porcentaje de Exceso de Linea de Credito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub PorcentajeDeExcepcionToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles PorcentajeDeExcepcionToolStripMenuItem1.Click
        FGMostrarFormulario(Me, frmPorcentajeExcepcion, "Porcentaje de Descuento", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub UsuariosParaAutorizarAnulacionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UsuariosParaAutorizarAnulacionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmUsuarioAutorizadorAnulacion, "Autorizador de Anulacion", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ProductosConPrecioModificableToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ProductosConPrecioModificableToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmProductoModificarPrecio, "Productos con precio modificable", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AutorizadoresDeNotaDeCreditoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AutorizadoresDeNotaDeCreditoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmUsuarioAutorizarPedidoNotaCredito, "Usuarios para Autorizar Pedidos de Notas de Credito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub MotivosDeAnulacionCobranzasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MotivosDeAnulacionCobranzasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmMotivoAnulacionCobranza, "Motivo de Anulacion de Cobranzas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub SubMotivosNotaDeCreditoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SubMotivosNotaDeCreditoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmABMSubMotivoNC, "Sub Motivo de Nota de Credito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ListaDePrecioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListaDePrecioToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoPrecioProducto, "Listado de Precios", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub LibroMayorDetalladoPorProductoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LibroMayorDetalladoPorProductoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmLibroMayorDetalle, "Listado Mayor Detallado", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub


    Private Sub ExcepcionesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExcepcionesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListaPrecioExcepciones, "Excepciones de Lista de Precios", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub KardexResumenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KardexResumenToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmKardexProductoResumen, "Informe Kardex Resumido", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub KardexDetalladoPorProductoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KardexDetalladoPorProductoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmKardexProductoDetalle, "Informe Kardex Detallado", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub MuestraParaPruebaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MuestraParaPruebaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmMuestraParaPrueba, "Muestra Para Prueba", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ConfiguracionDeMuestraToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConfiguracionDeMuestraToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmConfiguracionMuestraPanaderia, "Configuracion Muestra Panaderia", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AprobarPedidosDeNotaDeCreditoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AprobarPedidosDeNotaDeCreditoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAprobarPedidoDevolucion, "Aprobar Pedido de Devolucion", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CambiarDepositoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CambiarDepositoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCambiarDepositoTerminal, "Cambiar Terminal", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub SolicitudesDeModificacionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SolicitudesDeModificacionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmSolicitudEliminarComprobanteOrdenPagoEgreso, "Solicitudes de Modificacion OP", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CargaToolStripMenuItem4_Click(sender As Object, e As EventArgs) Handles CargaToolStripMenuItem4.Click
        FGMostrarFormulario(Me, frmRemision, "Carga de Nota de Remision", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub AccesosSegúnPerfilToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AccesosSegúnPerfilToolStripMenuItem.Click
        FGMostrarFormulario(Me, FrmAccesosSegunFormulario, "Accesos Según Formulario", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub SaldosAFechaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SaldosAFechaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmConsultarSaldoProveedorFecha, "Saldo Proveedor a Fecha", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub ListadoDePedidoDeNotasDeCreditoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListadoDePedidoDeNotasDeCreditoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoPedidoNotaCredito, "Listado Pedido Notas Credito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub RegistroDeAperturasDeFechaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RegistroDeAperturasDeFechaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAuditoriaUsuarioOperacionFueraRango, "Auditoria de Apertura de Fecha", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub AplicacionesDeAnticipoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AplicacionesDeAnticipoToolStripMenuItem.Click
        Dim frm As New frmAplicacionVentasAnticipo
        'Ocultar botones
        'frm.btnAnular.Visible = False
        'frm.btnAsiento.Visible = False
        frm.btnBusquedaAvanzada.Visible = True
        frm.btnCancelar.Visible = False
        frm.btnGuardar.Visible = False
        frm.btnNuevo.Visible = False
        frm.IDTransaccionVisualizar = 0
        frm.IDCliente = 0
        FGMostrarFormulario(Me, frm, "Aplicacion de Ventas a Anticipo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub CanjeDeRecaudacionesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CanjeDeRecaudacionesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCanjeRecaudaciones, "Canje de Recaudaciones", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub UsuariosPermitidosCargaDeRecepcionDeDocumentosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UsuariosPermitidosCargaDeRecepcionDeDocumentosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmUsuarioRecepcionDocumento, "Usuarios Recepcion de Documentos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub ConciliarAsientosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConciliarAsientosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmConciliarAsiento, "Conciliar Asientos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub DevolucionesDeClientesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DevolucionesDeClientesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmDevolucionSinNotaCredido, "Devoluciones de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub AjusteDeExistenciaKardexToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AjusteDeExistenciaKardexToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAjusteExistenciaSaldoKardex, "Ajuste de Saldo de Existencia en Kardex", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub DepartamentosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DepartamentosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmDepartamentoEmpresa, "Departamentos de la empresa", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub ExtractoDePagosPorAcuerdoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExtractoDePagosPorAcuerdoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoGastoAcuerdo, "Extracto de Pagos por Acuerdo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub ListadoDeDevolucionesSinNCToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListadoDeDevolucionesSinNCToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoDevolucionesSinNC, "Listado de Devoluciones sin NC", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, False)
    End Sub

    Private Sub SolicitudesDeDescuentoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SolicitudesDeDescuentoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmSolicitudProductoPrecio, "Solicitudes de Descuento", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CargaToolStripMenuItem8_Click(sender As Object, e As EventArgs) Handles CargaToolStripMenuItem8.Click
        'El formulario utilizado para carga es PEDIDONUEVO
        FGMostrarFormulario(Me, frmPedidoNUEVO, "Pedido", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub RegistroDePreciosPorClienteProductoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RegistroDePreciosPorClienteProductoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmRegistroPrecioCliente, "Registro de Precios por Cliente / Producto", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False)

    End Sub

    Private Sub EvolucionDeSaldosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EvolucionDeSaldosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmGraficoContable, "Evolucion de Saldos", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False, True, True)
    End Sub

    Private Sub ExistenciasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExistenciasToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmControlIntegridad, "Control de integridad de cuentas de existencia", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False)
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmPrincipal2_Activate()
        Me.Refresh()
    End Sub

    Private Sub CargaDescargaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CargaDescargaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmMovimientoDistribucion, "Movimiento de Stock Distribucion", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub PrecargaMovimientoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrecargaMovimientoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmPrecargaMovimiento, "PrecargaMovimiento de Stock Distribucion", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AprobadorPrecargaMovimientoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AprobadorPrecargaMovimientoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAprobarPrecargaMovimiento, "Aprobador PrecargaMovimiento de Stock Distribucion", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False)

    End Sub

    Private Sub UsuarioAprobadorPrecargaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UsuarioAprobadorPrecargaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmUsuarioAutorizarPrecargaMovimiento, "Usuario Aprobador PrecargaMovimiento de Stock Distribucion", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ABMSubMotivoMovimientoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ABMSubMotivoMovimientoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmSubMotivoMovimiento, "ABM SubMotivo Movimiento", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False)

    End Sub

    Private Sub FacturaDistribucionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FacturaDistribucionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmRemisionExterna, "Factura para Cliente de Distribucion", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False)

    End Sub

    Private Sub InventarioDeDocumentosPendientesPorPlazoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InventarioDeDocumentosPendientesPorPlazoToolStripMenuItem.Click
        'FGMostrarFormulario(Me, frmInventariodeDocumentosPendientesAPagar, "Inventario de documentos pendientes a Pagar", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
        FGMostrarFormulario(Me, frmInventarioDocumentosPendienteaPagarPorPlazo, "Inventario de documentos pendientes a Pagar por Plazo", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)

    End Sub

    Private Sub InventarioDeDocumentosPendientesPorClienteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InventarioDeDocumentosPendientesPorClienteToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmInventariodeDocumentosPendientesCobrarPorCliente, "Inventario de Documentos Pendientes a Cobrar Por Cliente ", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)

    End Sub

    Private Sub AsignarCuentaContableIntegridadToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AsignarCuentaContableIntegridadToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmIntegridadCuentaContable, "Asignacion de Cuenta Contable para Control de Integridad", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub ProcesadoraDeTarjetaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ProcesadoraDeTarjetaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmProcesadoraTarjeta, "Procesadora de Tarjeta", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub VencimientoDeProductosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VencimientoDeProductosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmVencimientodeProductos, "Vencimiento de Productos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CambiarRepositorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CambiarRepositorToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCambiarRepositor, "Cambio de Repositor de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, False)
    End Sub

    Private Sub ModificarGastosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModificarGastosToolStripMenuItem.Click
        Dim frm As New frmGastoContabilidad
        frm.CajaChica = False
        FGMostrarFormulario(Me, frm, "Control Unidad Negocio y Departamento", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, False)
    End Sub

    Private Sub ModificarFondoFijoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModificarFondoFijoToolStripMenuItem.Click
        Dim frm As New frmGastoContabilidad
        frm.CajaChica = True
        FGMostrarFormulario(Me, frm, "Control Unidad Negocio y Departamento", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, False)
    End Sub

    Private Sub ModificarCompraToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModificarCompraToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmCompraContabilidad, "Control Unidad Negocio y Departamento", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, False)
    End Sub

    Private Sub ModificarGastosRRHHToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModificarGastosRRHHToolStripMenuItem.Click
        Dim frm As New frmGastoContabilidad
        frm.CajaChica = False
        frm.RRHH = True
        FGMostrarFormulario(Me, frm, "Control Unidad Negocio y Departamento", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, False)
    End Sub

    Private Sub SeccionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SeccionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmSeccion, "Seccion", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, False)
    End Sub

    Private Sub ConsumoInformaticaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsumoInformaticaToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmConsumoInformatica, "Consumo Informatica", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True, False)
    End Sub

    Private Sub FechaContabilidadToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FechaContabilidadToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmFechaOperacionesContabilidad, "Fecha Contabilidad", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub AdministrarToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles AdministrarToolStripMenuItem2.Click
        FGMostrarFormulario(Me, frmresolucion49, "Resolucion 49", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub AsignacionDeCuentasRG49ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AsignacionDeCuentasRG49ToolStripMenuItem.Click
        'FGMostrarFormulario(Me, frmAsignarCuentaRG49, "Asignacion de Cuentas RG 49", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    'Private Sub CargaToolStripMenuItem9_Click(sender As Object, e As EventArgs) Handles CargaToolStripMenuItem9.Click
    '    Dim frm As New frmCobranzaCredito
    '    frm.Inicializar()
    '    frm.PropietarioBD = vgConfiguraciones("PropietarioBD")
    '    FGMostrarFormulario(Me, frm, "Cobranzas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    'End Sub

    Private Sub AprobadorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AprobadorToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmValidarCobranza, "Aprobador de Cobranzas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub MotivoDeRemisionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MotivoDeRemisionToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmMotivoRemision, "Motivo de Remision", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ImportarAsientoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImportarAsientoToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmImportarAsientoRRHH, "Importar Asiento", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    'Private Sub AcuerdoDeClientesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AcuerdoDeClientesToolStripMenuItem.Click
    '    FGMostrarFormulario(Me, frmAcuerdodeClientes, "Acuerdo de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    'End Sub

    Private Sub ListadoDeAcuerdosDeClientesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListadoDeAcuerdosDeClientesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoAcuerdodeClientes, "Listado de Acuerdo de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CargaToolStripMenuItem9_Click(sender As Object, e As EventArgs) Handles CargaToolStripMenuItem9.Click
        'Dim frm As New frmCobranzaCredito
        'frm.Inicializar()
        'frm.PropietarioBD = vgConfiguraciones("PropietarioBD")
        'FGMostrarFormulario(Me, frm, "Cobranzas", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ReenvioComprobantesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReenvioComprobantesToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmReenvioComprobantesElectronicos, "Re-envio de Comprobantes Electronicos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub MotivoDevolucionNCToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MotivoDevolucionNCToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmMotivoDevolucionNCCarga, "Motivos de Devolucion NC", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub FechaAcuerdosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FechaAcuerdosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmFechaOperacionesAcuerdos, "Fecha Rango de Acuerdos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub ListadoDeGastosAcuerdoComercialToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListadoDeGastosAcuerdoComercialToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmListadoGastoAcuerdoComercial, "Listado de Gastos / Acuerdos Comerciales", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub CargaDeAcuerdosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CargaDeAcuerdosToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmAcuerdodeClientes, "Acuerdo de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
    End Sub

    Private Sub ModificarModuloStockToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModificarModuloStockToolStripMenuItem.Click
        FGMostrarFormulario(Me, frmModificarMovimiento, "Modificar Movimiento", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub

    Private Sub ToolStripMenuItem11_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem11.Click
        FGMostrarFormulario(Me, frmMotivoReproceso, "Motivo Reproceso", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False, True)
    End Sub


End Class

﻿Public Class frmInicioSesion

    Dim CDecode As New ERP2ED.CDecode

    ' TODO: inserte el código para realizar autenticación personalizada usando el nombre de usuario y la contraseña proporcionada 
    ' (Consulte http://go.microsoft.com/fwlink/?LinkId=35339).  
    ' El objeto principal personalizado se puede adjuntar al objeto principal del subproceso actual como se indica a continuación: 
    '     My.User.CurrentPrincipal = CustomPrincipal
    ' donde CustomPrincipal es la implementación de IPrincipal utilizada para realizar la autenticación. 
    ' Posteriormente, My.User devolverá la información de identidad encapsulada en el objeto CustomPrincipal
    ' como el nombre de usuario, nombre para mostrar, etc.

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click

        'Descargar Datos
        Dim frm As New frmDescarga
        Me.Visible = False
        frm.ShowDialog()

        My.Application.ApplicationContext.MainForm = frmPrincipal2
        frmPrincipal2.Show()

        Me.Close()

        'CDecode.InClearText = txtUsuario.Text.Trim
        'CDecode.Encrypt()
        'txtPassword.Text = CDecode.CryptedText

    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click

        CDecode.InClearText = txtUsuario.Text.Trim
        CDecode.Decrypt()
        txtPassword.Text = CDecode.CryptedText


    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmInicioSesion_Activate()
        Me.Refresh()
    End Sub

End Class

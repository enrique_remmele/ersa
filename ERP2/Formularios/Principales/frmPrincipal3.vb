﻿Public Class frmPrincipal3

    'EVENTOS

    'PROPIEDADES
    Dim WithEvents OcxMenu1 As New ocxMenu

    'CLASES
    Dim CArchivoInicio As New CArchivoInicio
    Dim CSistema As New CSistema

    'VARIABLES
    Dim dtMenu As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Form
        'Titulo
        Dim Titulo As String
        Titulo = VGSoftwareNombre & " :: Versión: " & VGSoftwareVersion & " - Ultima actualización: " & VGSoftwareUltimaActualizacion & " - " & vgLicenciaOtorgado
        Me.Text = Titulo

        'Identificaciones
        lblSucursal.Text = vgSucursal
        lblDeposito.Text = vgDeposito
        lblTerminal.Text = vgTerminal
        lblUsuario.Text = vgUsuario

        'Imagen
        Dim ImagenFondo As String = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "IMAGEN", "")
        If ImagenFondo <> "" Then
            If My.Computer.FileSystem.FileExists(ImagenFondo) = True Then
                Me.BackgroundImage = Image.FromFile(ImagenFondo)
            End If
        Else
            ' Me.BackgroundImage = My.Resources.EXPRESS_FONDO
        End If

        'Fecha Hora
        VGFechaHoraSistema = CDate(CSistema.ExecuteScalar("Select GetDate()"))

        'Menu
        Me.Controls.Add(OcxMenu1)
        OcxMenu1.Dock = DockStyle.Top
        OcxMenu1.AutoSize = True

        CargarMenu()

        FGConfigurarMenu(OcxMenu1.MenuStrip1)

        'Menu de Clasificacion de Producto
        'Clasificacion 1
        'tsmiClasificacion1.Text = vgConfiguraciones("ProductoClasificacion1").ToString
        ''Clasificacion 2
        'tsmiClasificacion2.Text = vgConfiguraciones("ProductoClasificacion2").ToString
        ''Clasificacion 3
        'tsmiClasificacion3.Text = vgConfiguraciones("ProductoClasificacion3").ToString
        ''Clasificacion 4
        'tsmiClasificacion4.Text = vgConfiguraciones("ProductoClasificacion4").ToString
        ''Clasificacion 5
        'tsmiClasificacion5.Text = vgConfiguraciones("ProductoClasificacion5").ToString
        ''Clasificacion 6
        'tsmiClasificacion6.Text = vgConfiguraciones("ProductoClasificacion6").ToString

    End Sub

    Sub MostrarArbol()

        Dim Index As Integer = 1

        Dim dt As New DataTable
        dt.Columns.Add("Codigo")
        dt.Columns.Add("Descripcion")
        dt.Columns.Add("Nivel")
        dt.Columns.Add("NombreControl")
        dt.Columns.Add("CodigoPadre")
        dt.Columns.Add("Tag")

        For Each menu1 As ToolStripMenuItem In Me.MainMenuStrip.Items

            Dim NewRow As DataRow = dt.NewRow

            NewRow("Codigo") = CSistema.FormatDobleDigito(Index.ToString)
            NewRow("Descripcion") = menu1.Text
            NewRow("Nivel") = 1
            NewRow("NombreControl") = menu1.Name
            NewRow("CodigoPadre") = "0"

            If menu1.Tag Is Nothing Then
                NewRow("Tag") = ""
            Else
                NewRow("Tag") = menu1.Tag.ToString
            End If
            dt.Rows.Add(NewRow)

            Debug.Print(Index.ToString & ". " & menu1.Text)

            If menu1.DropDownItems.Count > 0 Then
                Dim Codigo As String = Index.ToString
                MostrarArbol(menu1, 0, dt, Codigo)
            End If

            Index = Index + 1

        Next

        Dim frm As New Form
        Dim lv As New ListView
        frm.Controls.Add(lv)
        lv.Dock = DockStyle.Fill

        CSistema.dtToLv(lv, dt)

        frm.ShowDialog(Me)

        Dim SQL As String = ""

        CSistema.ExecuteNonQuery("Delete From Menu")

        For Each oRow As DataRow In dt.Rows

            Debug.Print(oRow("Descripcion"))

            Dim param As New DataTable

            CSistema.SetSQLParameter(param, "Codigo", oRow("Codigo"))
            CSistema.SetSQLParameter(param, "Descripcion", oRow("Descripcion"))
            CSistema.SetSQLParameter(param, "Nivel", oRow("Nivel"))
            CSistema.SetSQLParameter(param, "NombreControl", oRow("NombreControl"))
            CSistema.SetSQLParameter(param, "CodigoPadre", oRow("CodigoPadre"))
            CSistema.SetSQLParameter(param, "Tag", oRow("Tag"))

            SQL = CSistema.InsertSQL(param, "Menu")

            Dim RowCount As Integer = CSistema.ExecuteNonQuery(SQL)

        Next


    End Sub

    Sub CargarMenu()

        'dtMenu = CSistema.ExecuteToDataTable("Select * From Menu")
        dtMenu = vgSAIN.Tables("Menu").Copy
        Dim CData As New CData
        'CData.OrderDataTable(dtMenu, "Nivel, Codigo, ID", True)

        'Cargar la Cabecera
        For Each oRow As DataRow In dtMenu.Select("Nivel = 1")

            Dim SubMenu1 As New ocxSubMenu
            SubMenu1.EstablecerValores(oRow("Formulario").ToString, oRow("Nombre").ToString, oRow("Descripcion").ToString, oRow("Inicializar"))
            OcxMenu1.MenuStrip1.Items.Add(SubMenu1.SubMenu)

            If dtMenu.Select("CodigoPadre='" & oRow("Codigo").ToString & "' ").Count > 0 Then
                CargarMenu(oRow("Codigo").ToString, SubMenu1)
            End If

        Next

    End Sub

    Sub CargarMenu(ByVal Codigo As String, ByVal SubMenu As ocxSubMenu)

        'Cargar la Cabecera
        For Each oRow As DataRow In dtMenu.Select("CodigoPadre = '" & Codigo & "' ")

            Dim SubMenu1 As New ocxSubMenu
            SubMenu1.EstablecerValores(oRow("Formulario").ToString, oRow("Nombre").ToString, oRow("Descripcion").ToString, oRow("Inicializar"))
            SubMenu.SubMenu.DropDownItems.Add(SubMenu1.SubMenu)

            If dtMenu.Select("CodigoPadre='" & oRow("Codigo").ToString & "' ").Count > 0 Then
                CargarMenu(oRow("Codigo").ToString, SubMenu1)
            End If

        Next

    End Sub

    Sub MostrarArbol(ByVal menu1 As ToolStripMenuItem, ByVal Tab As Integer, ByVal dt As DataTable, Optional ByVal Codigo As String = "", Optional ByVal Indice As Integer = 0, Optional ByVal CodigoPadre As String = "1")

        Dim Espacio As String = ""

        For i As Integer = 0 To Tab
            Espacio = Espacio & vbTab
        Next

        CodigoPadre = Codigo

        For i As Integer = 0 To menu1.DropDownItems.Count - 1


            Dim Tipo As String = menu1.DropDownItems.Item(i).GetType.ToString()


            If Tipo <> "System.Windows.Forms.ToolStripMenuItem" Then
                GoTo siguiente
            End If

            Dim menu2 As ToolStripMenuItem = menu1.DropDownItems.Item(i)

            Indice = Indice + 1
            Codigo = CodigoPadre & CSistema.FormatDobleDigito(Indice.ToString)

            Debug.Print(Espacio & Codigo & " " & menu2.Text & " " & Tab + 1 & " " & menu2.Name & " " & menu2.Tag & " " & CodigoPadre)

            Dim NewRow As DataRow = dt.NewRow

            NewRow("Codigo") = Codigo
            NewRow("Descripcion") = menu2.Text
            NewRow("Nivel") = Tab + 2
            NewRow("NombreControl") = menu2.Name
            NewRow("CodigoPadre") = CodigoPadre

            If menu2.Tag Is Nothing Then
                NewRow("Tag") = ""
            Else
                NewRow("Tag") = menu2.Tag.ToString
            End If

            dt.Rows.Add(NewRow)

            If menu2.DropDownItems.Count > 0 Then
                'Codigo = Codigo & (Indice).ToString
                MostrarArbol(menu2, Tab + 1, dt, Codigo, 0, Codigo)
            End If

siguiente:

        Next

    End Sub

    Private Sub frmPrincipal3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub CambiarImagenDeFonoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CambiarImagenDeFonoToolStripMenuItem.Click
        OpenFileDialog1.ShowDialog()
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "IMAGEN", OpenFileDialog1.FileName)

        Dim ImagenFondo As String = OpenFileDialog1.FileName

        If ImagenFondo <> "" Then
            If My.Computer.FileSystem.FileExists(ImagenFondo) = True Then
                Me.BackgroundImage = Image.FromFile(ImagenFondo)
            End If
        End If

    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmPrincipal3_Activate()
        Me.Refresh()
    End Sub
End Class
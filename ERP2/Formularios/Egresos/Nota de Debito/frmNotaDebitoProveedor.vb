﻿Public Class frmNotaDebitoProveedor

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Dim CAsiento As New CAsientoNotaDebitoProveedor
    Dim CData As New CData

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property
    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property

    'EVENTOS

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dtCompra As New DataTable
    Dim dtDetalleCompra As New DataTable
    Dim dtNotaDebitoCompra As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim dtTerminalPuntoExpedicion As DataTable
    Dim vHabilitar As Boolean
    Dim vConComprobantes As Boolean
    Dim vObservacionDevolucion As String
    Dim vObservacionDescuento As String

    Dim Proveedor As Boolean = True

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Controles
        txtProducto.Conectar()
        txtProveedor.Conectar()
        Dim dtDescuento As DataTable = CData.GetTable("VProducto", "ControlarExistencia='False'")
        OcxImpuesto1.Inicializar()
        OcxImpuesto1.dg.ReadOnly = True

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "NOTA DEBITO PROVEEDOR", "NDRP")
        vNuevo = True
        vHabilitar = True
        vConComprobantes = True

        'Otros
        flpRegistradoPor.Visible = False
        gbxCabecera.BringToFront()
        pnlDevolucion.Dock = DockStyle.Fill
        pnlDescuento.Dock = DockStyle.Fill

        'Variables
        vObservacionDescuento = "DESCUENTO CONCEDIDO"
        vObservacionDevolucion = "DEVOLUCION DE MERCADERIAS"

        'Funciones
        CargarInformacion()


        'Clases
        CAsiento.InicializarAsiento()

        'Botones
        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

        Dim ID As Integer
        ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VNotaDebitoProveedor Where IDSucursal=" & cbxCiudad.cbx.SelectedValue & " "), Integer)
        txtID.txt.Text = ID
        CargarOperacion()

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Combrobante


        'Cabecera
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, cbxDeposito)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, txtCotizacion)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtProveedor)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtTimbrado)
        CSistema.CargaControl(vControles, txtVtoTimbrado)
        CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, rdbDescuentos)
        CSistema.CargaControl(vControles, rdbDevolucion)
        CSistema.CargaControl(vControles, chkEsFE)


        'Detalle
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, txtObservacionProducto)
        CSistema.CargaControl(vControles, txtCantidad)
        CSistema.CargaControl(vControles, txtCostoUnitario)
        CSistema.CargaControl(vControles, txtImporte)
        CSistema.CargaControl(vControles, lvLista)

        'Decuento
        CSistema.CargaControl(vControles, lklAgregarDetalleDescuento)
        CSistema.CargaControl(vControles, lklEliminarDetalleDescuento)
        CSistema.CargaControl(vControles, lklAgregarComprobanteDescuento)
        CSistema.CargaControl(vControles, lklEliminarComprobanteDescuento)
        CSistema.CargaControl(vControles, lvListaDescuento)
        CSistema.CargaControl(vControles, dgw)

        'Detalles
        dtDetalle = CSistema.ExecuteToDataTable("Select Top(0) *, 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0  From VDetalleNotaDebitoProveedor").Clone
        dtDetalleCompra = CSistema.ExecuteToDataTable("Select *, 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0 From DetalleCompra").Clone
        dtDescuento = CData.GetStructure("VDescuento", "Select Top(0) * From VDescuento")
        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CARGAR CONTROLES

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Ciudades
        CSistema.SqlToComboBox(cbxCiudad.cbx, CData.GetTable("VSucursal", " ID = " & vgIDSucursal), "IDCiudad", "CodigoCiudad")

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal", " ID = " & vgIDSucursal), "ID", "Descripcion")

        'Deposito
        CSistema.SqlToComboBox(cbxDeposito.cbx, CData.GetTable("VDeposito", " IDSucursal = " & vgIDSucursal), "ID", "Deposito")
        cbxDeposito.cbx.Text = vgDeposito

        'Monedas
        cbxMoneda.Conectar()
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Impuestos
        OcxImpuesto1.EstablecerSoloLectura()

        'Radio Button
        rdbDevolucion.Checked = True

    End Sub

    Sub GuardarInformacion()

        ''Ciudad
        'CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        ''Tipo de Comprobante
        'CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        ''Sucursal
        'CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

        ''Deposito
        'CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DEPOSITO", cbxDeposito.cbx.Text)

    End Sub

    Sub Nuevo()

        'Configurar botones
        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

        ''Limpiar detalle
        dtDetalle.Rows.Clear()
        dtDescuento.Rows.Clear()
        dtCompra.Rows.Clear()
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)
        OcxImpuesto1.CargarValores(dtDetalle)
        dtNotaDebitoCompra.Rows.Clear()
        chkEsFE.chk.Checked = False

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True
        CAsiento.Inicializar()



        rdbDevolucion.Checked = True
        txtObservacion.SetValue(vObservacionDevolucion)

        'Cabecera
        txtFecha.txt.Text = ""
        cbxComprobante.SeleccionObligatoria = False
        cbxComprobante.cbx.Text = ""
        txtComprobante.txt.Text = ""
        cbxTipoComprobante.cbx.Text = ""
        txtTimbrado.txt.Text = ""
        txtProveedor.Clear()

        txtCostoUnitario.SoloLectura = True
        txtImporte.SoloLectura = True
        flpRegistradoPor.Visible = False

        rdbDescuentos.Enabled = True
        rdbDevolucion.Enabled = True

        'Limpiar Detalle
        lvLista.Items.Clear()
        cbxComprobante.cbx.Text = " "

        ''Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From NotaDebitoProveedor Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ),1)"), Integer)


        'limpiar Detalle Descuento
        lvListaDescuento.Items.Clear()
        dgw.Rows.Clear()
        txtTotalDescuento.txt.Text = ""
        txtSaldo.txt.Text = ""
        txtDescontados.txt.Text = ""
        txtCantidadCobrado.txt.Text = ""
        cbxComprobante.SoloLectura = False
        txtObservacion.SoloLectura = False
        dgw.ReadOnly = False
        txtObservacionProducto.SoloLectura = False
        txtProducto.txt.Text = ""

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Seleccion de Cliente
        If txtProveedor.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If CSistema.ControlarNroDocumento(txtComprobante.txt.Text) = False Then
            Exit Function
        End If

        'Timbrado
        If txtTimbrado.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un número de timbrado!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If CSistema.ControlarTimbrado(txtTimbrado.txt.Text) = False Then
            Exit Function
        End If

        If txtProveedor.Registro Is Nothing Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If IsNumeric(txtProveedor.Registro("ID").ToString) = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Moneda
        If IsNumeric(cbxMoneda.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el tipo de moneda!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Deposito
        If IsNumeric(cbxDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Saldo
        If rdbDescuentos.Checked = True Then
            'If txtSaldo.txt.Text <> 0 Then
            '    Dim mensaje As String = "El saldo debe ser Cero!"
            '    ctrError.SetError(txtSaldo, mensaje)
            '    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            '    tsslEstado.Text = mensaje
            '    Exit Function
            'End If

        End If

        'Existencia en Detalle
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If dtDetalle.Rows.Count = 0 Then
                Dim mensaje As String = "El documento debe tener por lo menos 1 detalle!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            'Total
            If CDec(OcxImpuesto1.txtTotal.ObtenerValor) <= 0 Then
                Dim mensaje As String = "El importe del documento no es valido!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If




            'Validar el Asiento
            If CAsiento.ObtenerSaldo <> 0 Then
                CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Function
            End If

        End If

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        ValidarDocumento = True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroTimbrado", txtTimbrado.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@VencimientoTimbrado", CSistema.FormatoFechaBaseDatos(txtVtoTimbrado.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoOperacion", cbxDeposito.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descuento", rdbDescuentos.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Devolucion", rdbDevolucion.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@EsFE", chkEsFE.chk.Checked, ParameterDirection.Input)


        'Saldo
        If rdbDevolucion.Checked = True Then
            CSistema.SetSQLParameter(param, "@Saldo", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "Total")), ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Saldo", CSistema.FormatoMonedaBaseDatos(txtSaldo.ObtenerValor), ParameterDirection.Input)
        End If

        'Otros


        'Moneda
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.cbx, ParameterDirection.Input)
        If txtCotizacion.ObtenerValor = 0 Then
            CSistema.SetSQLParameter(param, "@Cotizacion", "1".ToString, ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Cotizacion", txtCotizacion.ObtenerValor, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text.Trim, ParameterDirection.Input)

        'Totales
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "Total")), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalImpuesto")), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalDiscriminado")), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDescuento", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalDescuento")), ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        'Capturamos el index de la Operacion para un posible proceso posterior
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpNotaDebitoProveedor", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From NotaDebitoProveedor Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpNotaDebitoProveedor", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        Dim Procesar As Boolean = True


        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)
            CargarOperacion(IDTransaccion)


        End If

        'Guardar detalle
        If IDTransaccion > 0 Then

            'Guardar 
            Procesar = InsertarDetalle(IDTransaccion)

        End If

        Try

            ReDim param(-1)

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            'Aplicar
            If CSistema.ExecuteStoreProcedure(param, "SpNotaDebitoProveedorProcesar", False, False, MensajeRetorno) = False Then

            End If

        Catch ex As Exception

        End Try

        'Si es nuevo
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then

            'Cargamos el DetalleImpuesto
            CDetalleImpuesto.Guardar(IDTransaccion)

            'Cargamos el asiento
            CAsiento.IDTransaccion = IDTransaccion
            GenerarAsiento()
            CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        End If



        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)
        CargarOperacion(IDTransaccion)



    End Sub

    Sub Eliminar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpNotaDebitoProveedor", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From NotaDebitoProveedor Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpNotaDebitoProveedor", False, False, MensajeRetorno)
            End If

            Exit Sub

        End If

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.ANULAR_ELIMINAR, btnNuevo, btnGuardar, btnCancelar, btnEliminar, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

        ' Cargar el ultimo registro
        Dim ID As Integer
        ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VNotaDebitoProveedor Where IDSucursal=" & cbxCiudad.cbx.SelectedValue & " "), Integer)
        txtID.txt.Text = ID
        CargarOperacion()

    End Sub
    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = "Nota de Debito: " & cbxTipoComprobante.cbx.Text & "  -  " & txtProveedor.txtRazonSocial.Text

            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From vNotaDebitoProveedor Where Numero=" & txtID.ObtenerValor & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else

            'Validar
            If cbxCiudad.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la ciudad de operacion!"
                ctrError.SetError(cbxCiudad, mensaje)
                ctrError.SetIconAlignment(cbxCiudad, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(cbxTipoComprobante, mensaje)
                ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If txtProveedor.Seleccionado = False Then
                Dim mensaje As String = "Seleccione correctamente el cliente!"
                ctrError.SetError(txtProveedor, mensaje)
                ctrError.SetIconAlignment(txtProveedor, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = "Compra: " & cbxTipoComprobante.cbx.Text & " - " & txtProveedor.txtRazonSocial.Text & ""

            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento
            frm.CAsiento.Bloquear = CAsiento.Bloquear

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
            CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            CAsiento.NroCaja = frm.CAsiento.NroCaja
            CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                VisualizarAsiento()
            End If

        End If

    End Sub

    Sub GenerarAsiento()


        'EstablecerCabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = cbxCiudad.cbx.SelectedValue
        oRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
        oRow("Fecha") = txtFecha.GetValue
        oRow("IDMoneda") = cbxMoneda.GetValue
        oRow("Cotizacion") = txtCotizacion.ObtenerValor
        oRow("IDTipoComprobante") = cbxTipoComprobante.cbx.SelectedValue
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = txtComprobante.GetValue
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.GetValue
        oRow("Detalle") = txtObservacion.txt.Text
        oRow("Total") = OcxImpuesto1.txtTotal.ObtenerValor

        oRow("GastosMultiples") = False

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)

        OcxImpuesto1.Generar()
        CAsiento.dtImpuesto = OcxImpuesto1.dtImpuesto.Copy
        CAsiento.dtDetalleProductos = dtDetalle
        CAsiento.IDProveedor = txtProveedor.Registro("ID").ToString
        CAsiento.Generar()

    End Sub

    Sub CargarDescuento(ByVal Venta As DataRow)

        'Variables
        Dim vIDTransaccionVenta As Integer = Venta("IDTransaccion")
        Dim vIDProducto As Integer = Venta("ID")
        Dim vID As Integer = 0

        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Select * From VDescuento Where IDTransaccion=" & vIDTransaccionVenta & " And IDProducto=" & vIDProducto & " And ID=" & vID & " ").Copy

        If dttemp Is Nothing Then
            Exit Sub
        End If

        'Agregar
        For Each oRow As DataRow In dttemp.Rows
            Dim NewRow As DataRow = dtDescuento.NewRow
            NewRow.ItemArray = oRow.ItemArray

            If NewRow("Descuento") = 0 Then
                GoTo siguiente
            End If

            'Descuento 
            Dim Descuento As Decimal = NewRow("Descuento")
            Dim DescuentoDiscriminado As Decimal = 0
            Dim TotalDescuentoDiscriminado As Decimal = 0

            NewRow("Cantidad") = txtCantidad.ObtenerValor
            NewRow("Total") = txtCantidad.ObtenerValor * Descuento

            CSistema.CalcularIVA(Venta("IDImpuesto"), Descuento, False, True, DescuentoDiscriminado)
            NewRow("DescuentoDiscriminado") = DescuentoDiscriminado

            TotalDescuentoDiscriminado = NewRow("Cantidad") * DescuentoDiscriminado
            NewRow("TotalDescuentoDiscriminado") = TotalDescuentoDiscriminado

            dtDescuento.Rows.Add(NewRow.ItemArray)

siguiente:

        Next

    End Sub

    Sub EliminarDescuentos()

        For Each item As ListViewItem In lvLista.SelectedItems
            Dim IDProducto As Integer = item.Text
            Dim ID As Integer = item.Index

            For i As Integer = dtDescuento.Rows.Count - 1 To 0 Step -1
                Dim oRow As DataRow = dtDescuento.Rows(i)
                If oRow("IDProducto") = IDProducto And oRow("ID") = ID Then
                    dtDescuento.Rows.RemoveAt(i)
                End If
            Next

        Next

    End Sub



    Sub Buscar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

        'Otros
        Dim frm As New frmConsultaNotaCredito
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog(Me)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalle = True

        Seleccionardescuento()
        If rdbDevolucion.Checked = True Then
            For Each oRow As DataRow In dtDetalle.Rows

                Dim sql As String = ""
                Dim sql2 As String = ""


                sql = "Insert Into DetalleNotaDebitoProveedor (IDTransaccion, IDProducto, ID, IDTransaccionCompra, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, CostoUnitario, DescuentoUnitario, PorcentajeDescuento, Total, TotalImpuesto, TotalCostoImpuesto, TotalCosto,  TotalDiscriminado, TotalCostoDiscriminado, Caja, CantidadCaja) Values(" & IDTransaccion & "," & oRow("IDProducto").ToString & ", " & oRow("ID").ToString & "," & oRow("IDTransaccionCompra").ToString & "," & oRow("IDDeposito").ToString & ",'" & oRow("Descripcion").ToString & "'," & oRow("IDImpuesto").ToString & "," & CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PrecioUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("CostoUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("DescuentoUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PorcentajeDescuento").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCostoImpuesto").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCosto").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCostoDiscriminado").ToString) & ", '" & oRow("Caja").ToString & "'," & CSistema.FormatoNumeroBaseDatos(oRow("CantidadCaja").ToString) & " )"
                sql2 = "Insert Into NotaDebitoProveedorCompra (IDTransaccionNotaDebitoProveedor, IDTransaccionEgreso, Importe, Cobrado, Descontado, Saldo) Values (" & IDTransaccion & "," & oRow("IDTransaccionCompra").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Descontado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString) & ") "


                If CSistema.ExecuteNonQuery(sql) = 0 Then
                    Return False
                End If


                If CSistema.ExecuteNonQuery(sql2) = 0 Then
                    Return False
                End If

            Next

        End If
        ' Else
        If rdbDescuentos.Checked = True Then


            For Each oRow As DataRow In dtNotaDebitoCompra.Select(" Sel = 'True' ")
                Dim sql As String
                'sql = "Insert Into NotaDebitoProveedorCompra (IDTransaccionNotaDebitoProveedor, IDTransaccionEgreso, Importe,Cobrado,Descontado, Saldo, Tipo) Values (" & IDTransaccion & "," & oRow("IDTransaccion").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ",0,0," & CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString) & ",'" & oRow("Tipo").ToString & "') "
                sql = "Insert Into NotaDebitoProveedorCompra (IDTransaccionNotaDebitoProveedor, IDTransaccionEgreso, Importe,Cobrado,Descontado, Saldo) Values (" & IDTransaccion & "," & oRow("IDTransaccion").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & ",0,0," & CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString) & ") "

                If CSistema.ExecuteNonQuery(sql) = 0 Then
                    Return False
                End If
            Next
            For Each oRow As DataRow In dtDetalle.Rows
                Dim sql As String = ""
                sql = "Insert Into DetalleNotaDebitoProveedor (IDTransaccion, IDProducto, ID , IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, CostoUnitario, DescuentoUnitario, PorcentajeDescuento, Total, TotalImpuesto, TotalCostoImpuesto, TotalCosto, TotalDescuento, TotalDiscriminado, TotalCostoDiscriminado, Caja, CantidadCaja) Values(" & IDTransaccion & "," & oRow("IDProducto").ToString & ", " & oRow("ID").ToString & "," & oRow("IDDeposito").ToString & ",'" & oRow("Descripcion").ToString & "'," & oRow("IDImpuesto").ToString & "," & CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PrecioUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("CostoUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("DescuentoUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PorcentajeDescuento").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCostoImpuesto").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCosto").ToString) & "," & oRow("TotalDescuento").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCostoDiscriminado").ToString) & ", '" & oRow("Caja").ToString & "'," & CSistema.FormatoNumeroBaseDatos(oRow("CantidadCaja").ToString) & " )"

                If CSistema.ExecuteNonQuery(sql) = 0 Then
                    Return False
                End If
            Next

        End If


        'End If
    End Function

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        cbxComprobante.SoloLectura = True
        txtObservacion.SoloLectura = True

        txtObservacionProducto.SoloLectura = True

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()


        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From NotaDebitoProveedor Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA, btnNuevo, btnGuardar, btnCancelar, btnEliminar, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VNotaDebitoProveedor Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select *, 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0 From VDetalleNotaDebitoProveedor Where IDTransaccion=" & IDTransaccion & " Order By ID").Copy

        'Cargamos la cabecera
        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        txtProveedor.SetValue(oRow("IDProveedor").ToString)
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtTimbrado.txt.Text = oRow("NroTimbrado").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        cbxDeposito.txt.Text = oRow("Deposito").ToString
        cbxMoneda.txt.Text = oRow("Moneda").ToString
        txtCotizacion.txt.Text = oRow("Cotizacion").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        rdbDescuentos.Checked = oRow("Descuento").ToString
        rdbDevolucion.Checked = oRow("Devolucion").ToString
        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString


        'Cargamos el detalle
        ListarDetalle()
        ListarDetalleDescuento()
        CargarComprobanteDescuentofacturados(IDTransaccion)
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)
        CalcularTotales()
        dgw.ReadOnly = True

        calcularTotalDescuento()

        'Inicializamos el Asiento
        CAsiento.Limpiar()

        txtSaldo.txt.Text = ""

        'If Me.TabControl1.SelectedIndex = 1 And Me.rdbDevolucion.Checked = True Then
        '    Me.TabControl1.SelectedIndex = 0
        'End If
        'If Me.TabControl1.SelectedIndex = 0 And Me.rdbDescuentos.Checked = True Then
        '    Me.TabControl1.SelectedIndex = 1
        'End If

    End Sub

    Sub Cancelar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)


        cbxComprobante.SoloLectura = True
        txtObservacionProducto.SoloLectura = True
        'txtProducto.txt.sololectura = True


        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        CargarOperacion()

    End Sub

    Sub CalcularImporte(ByVal Decimales As Boolean, ByVal Redondear As Boolean, ByVal PorTotal As Boolean)

        ctrError.Clear()
        tsslEstado.Text = ""

        Dim Cantidad As Decimal
        Dim PrecioUnitario As Decimal
        Dim Importe As Decimal

        Cantidad = txtCantidad.ObtenerValor

        'Validar
        If Cantidad = 0 Then
            Dim mensaje As String = "No se puede calcular los montos si la cantidad es 0!"
            ctrError.SetError(lvLista, mensaje)
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If PorTotal = True Then
            PrecioUnitario = txtImporte.ObtenerValor / Cantidad
        Else
            Importe = txtCostoUnitario.ObtenerValor * Cantidad
        End If

        If Decimales = False Then
            PrecioUnitario = CInt(Math.Round(PrecioUnitario, 0))
            Importe = CLng(Math.Round(Importe, 0))
        Else
            If Redondear = True Then
                PrecioUnitario = CInt(Math.Round(PrecioUnitario, 0))
                Importe = CInt(Math.Round(Importe, 0))
            End If
        End If

        If PorTotal = True Then
            txtCostoUnitario.txt.Text = PrecioUnitario.ToString
        Else
            txtImporte.txt.Text = Importe.ToString
        End If

    End Sub

    Sub CalcularTotales()

        OcxImpuesto1.CargarValores(CDetalleImpuesto.dt)

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        lvLista.Items.Clear()

        'Variables
        Dim Total As Decimal = 0
        Dim Saldo As Decimal = 0
        'Cargamos registro por registro
        For Each oRow As DataRow In dtDetalle.Rows

            Dim item As ListViewItem = lvLista.Items.Add(oRow("IDProducto").ToString)

            Dim Descripcion As String
            If oRow("Observacion").ToString.Trim.Length > 0 Then
                Descripcion = oRow("Descripcion").ToString.Trim & " - " & oRow("Observacion").ToString.Trim
            Else
                Descripcion = oRow("Descripcion").ToString.Trim

            End If
            item.SubItems.Add(oRow("Referencia").ToString)
            item.SubItems.Add(oRow("Descripcion").ToString)
            item.SubItems.Add(oRow("Comprobante").ToString)
            item.SubItems.Add(oRow("Ref Imp").ToString)
            item.SubItems.Add(CSistema.FormatoNumero(oRow("Cantidad").ToString))
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("PrecioUnitario").ToString))
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("Total").ToString))


        Next

        Bloquear()

    End Sub

    Sub CargarProducto()

        If vConComprobantes = True Then
            CargarProductoConComprobante()
        Else
            CargarProductoSinComprobante()
        End If

    End Sub

    Sub CargarProductoConComprobante()

        'Validar
        'Seleccion de Producto
        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Deposito
        If IsNumeric(cbxDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If cbxDeposito.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cantidad
        If IsNumeric(txtCantidad.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtCantidad.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim CantidadProducto As Decimal = txtCantidad.ObtenerValor
        Dim CantidadCaja As Integer = CantidadProducto / txtProducto.Registro("UnidadPorCaja")

        'Obtenemos la cantidad que existe en el comprobante
        Dim CantidadComprobante As Decimal = txtProducto.Registro("SaldoCantidad").ToString

        'Obtenemos la cantidad de productos incluyendo los que estan en el detalle
        For Each oRow As DataRow In dtDetalle.Rows

            If cbxComprobante.GetValue = oRow("IDtransaccionCompra") And txtProducto.Registro("ID") = oRow("IDProducto") Then
                CSistema.MostrarError("No puede ingresar el mismo producto 2 veces", ctrError, txtProducto, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Sub
            End If

        Next

        If CantidadProducto > CantidadComprobante Then
            CSistema.MostrarError("La cantidad no puede ser mayor al del comprobante (" & CSistema.FormatoNumero(CantidadComprobante.ToString) & ")", ctrError, txtCantidad, tsslEstado, ErrorIconAlignment.MiddleRight)
            Exit Sub
        End If

        'Cargamos el registro en el detalle
        dtDetalle.Columns("IDTransaccionCompra").AllowDBNull = True

        Dim dRow As DataRow = dtDetalle.NewRow()
        Dim CompraRow As DataRow = dtDetalleCompra.Select("IDTransaccion= " & cbxComprobante.GetValue & " And ID = " & txtProducto.Registro("ID") & " ")(0)

        Try

            'Obtener Valores
            'Productos
            dRow("IDProducto") = txtProducto.Registro("ID").ToString
            dRow("ID") = dtDetalle.Rows.Count
            dRow("IDTransaccionCompra") = cbxComprobante.GetValue

            If txtObservacionProducto.txt.Text.Trim.Length = 0 Then
                dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString
            Else
                dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString & " - " & txtObservacionProducto.txt.Text
            End If
            dRow("Referencia") = txtProducto.Registro("Ref")
            dRow("CodigoBarra") = CompraRow("CodigoBarra").ToString
            dRow("Observacion") = txtObservacionProducto.txt.Text

            'Deposito
            dRow("IDDeposito") = cbxDeposito.cbx.SelectedValue
            dRow("Deposito") = cbxDeposito.cbx.Text

            'Cantidad y Precios
            dRow("Cantidad") = CantidadProducto

            Dim PrecioUnitario As Decimal
            PrecioUnitario = CompraRow("PrecioUnitario")

            dRow("PrecioUnitario") = PrecioUnitario
            dRow("Pre. Uni.") = PrecioUnitario
            dRow("Total") = PrecioUnitario * CantidadProducto

            'Impuestos
            dRow("IDImpuesto") = CompraRow("IDImpuesto").ToString
            dRow("Impuesto") = CompraRow("Impuesto").ToString
            dRow("Ref Imp") = CompraRow("Ref Imp").ToString
            dRow("TotalImpuesto") = 0
            dRow("TotalDiscriminado") = 0
            CSistema.CalcularIVA(dRow("IDImpuesto").ToString, CDec(dRow("Total").ToString), False, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))

            'Costo
            dRow("CostoUnitario") = CompraRow("Costo").ToString
            dRow("TotalCosto") = CantidadProducto * CompraRow("Costo").ToString
            dRow("TotalCostoImpuesto") = 0
            dRow("TotalCostoDiscriminado") = dRow("TotalCosto")
            CSistema.CalcularIVA(CompraRow("IDImpuesto").ToString, CDec(dRow("TotalCosto").ToString), False, True, 0, dRow("TotalCostoImpuesto"))


            'Venta
            dRow("IDTransaccionCompra") = cbxComprobante.GetValue
            dRow("Comprobante") = cbxComprobante.cbx.Text



            'Cantidad Caja
            dRow("Caja") = 0
            dRow("CantidadCaja") = CantidadCaja

            'Agregamos al detalle
            dtDetalle.Rows.Add(dRow)




            ListarDetalle()
            CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)
            CalcularTotales()

            'Inicializamos los valores
            txtObservacionProducto.txt.Clear()
            txtCantidad.txt.Text = "0"
            txtImporte.txt.Text = "0"
            txtCostoUnitario.txt.Text = "0"
            txtProducto.txt.Clear()

            'Retorno de Foco
            txtProducto.txt.SelectAll()
            txtProducto.txt.Focus()

            ctrError.Clear()
            tsslEstado.Text = ""

        Catch ex As Exception

        End Try


    End Sub

    Sub CargarProductoSinComprobante()

        'Validar
        'Seleccion de Producto
        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Deposito
        If IsNumeric(cbxDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If cbxDeposito.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cantidad
        If IsNumeric(txtCantidad.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtCantidad.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim CantidadProducto As Decimal = txtCantidad.ObtenerValor
        Dim CantidadCaja As Integer = CantidadProducto / txtProducto.Registro("UnidadPorCaja")




        If vHabilitar = True Then

            'Obtenemos la cantidad que existe en el comprobante
            Dim CantidadComprobante As Decimal = txtProducto.Registro("SaldoCantidad").ToString

            'Obtenemos la cantidad de productos incluyendo los que estan en el detalle
            For Each oRow As DataRow In dtDetalle.Rows

                If cbxComprobante.GetValue = oRow("IDtransaccionCompra") And txtProducto.Registro("ID") = oRow("IDProducto") Then
                    CSistema.MostrarError("No puede ingresar el mismo producto 2 veces", ctrError, txtProducto, tsslEstado, ErrorIconAlignment.MiddleRight)
                    Exit Sub
                End If

            Next

            If CantidadProducto > CantidadComprobante Then
                CSistema.MostrarError("La cantidad no puede ser mayor al del comprobante (" & CSistema.FormatoNumero(CantidadComprobante.ToString) & ")", ctrError, txtCantidad, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Sub
            End If


        End If


        'Cargamos el registro en el detalle
        dtDetalle.Columns("IDTransaccionCompra").AllowDBNull = True

        Dim dRow As DataRow = dtDetalle.NewRow()

        'Obtener Valores
        'Productos
        dRow("IDProducto") = txtProducto.Registro("ID").ToString
        dRow("ID") = dtDetalle.Rows.Count
        dRow("IDTransaccionCompra") = cbxComprobante.GetValue

        If txtObservacionProducto.txt.Text.Trim.Length = 0 Then
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString
        Else
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString & " - " & txtObservacionProducto.txt.Text
        End If

        dRow("CodigoBarra") = txtProducto.Registro("CodigoBarra").ToString
        dRow("Observacion") = txtObservacionProducto.txt.Text

        'Deposito
        dRow("IDDeposito") = cbxDeposito.cbx.SelectedValue
        dRow("Deposito") = cbxDeposito.cbx.Text

        'Cantidad y Precios
        dRow("Cantidad") = CantidadProducto

        Dim PrecioUnitario As Decimal

        If vHabilitar = True Then
            PrecioUnitario = CDec(txtProducto.Registro("PrecioUnitario").ToString)
        Else
            PrecioUnitario = txtCostoUnitario.txt.Text
        End If

        dRow("PrecioUnitario") = PrecioUnitario
        dRow("Pre. Uni.") = PrecioUnitario
        dRow("Total") = PrecioUnitario * CantidadProducto

        'Impuestos
        dRow("IDImpuesto") = txtProducto.Registro("IDImpuesto").ToString
        dRow("Impuesto") = txtProducto.Registro("Impuesto").ToString
        dRow("Ref Imp") = txtProducto.Registro("Ref Imp").ToString
        dRow("TotalImpuesto") = 0
        dRow("TotalDiscriminado") = 0
        CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("Total").ToString), False, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))

        'Costo
        dRow("CostoUnitario") = txtProducto.Registro("Costo").ToString
        dRow("TotalCosto") = CantidadProducto * txtProducto.Registro("Costo").ToString
        dRow("TotalCostoImpuesto") = 0
        dRow("TotalCostoDiscriminado") = 0
        CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("TotalCosto").ToString), False, True, dRow("TotalCostoDiscriminado"), dRow("TotalCostoImpuesto"))

        'Descuento
        dRow("TotalDescuento") = 0
        dRow("DescuentoUnitario") = 0
        dRow("PorcentajeDescuento") = 0
        dRow("TotalDescuentoDiscriminado") = 0

        'Totales
        dRow("TotalBruto") = 0
        dRow("TotalSinDescuento") = dRow("Total") - dRow("TotalDescuento")
        dRow("TotalSinImpuesto") = dRow("Total") - dRow("TotalImpuesto")
        dRow("TotalNeto") = 0
        dRow("TotalNetoConDescuentoNeto") = dRow("TotalDiscriminado") + dRow("TotalDescuentoDiscriminado")

        'Venta
        dRow("IDTransaccionVenta") = cbxComprobante.GetValue
        dRow("Comprobante") = cbxComprobante.cbx.Text

        Try
            dRow("Cobrado") = dtCompra.Select(" IDTransaccion = " & cbxComprobante.GetValue)(0)("Cobrado")
            dRow("Descontado") = dtCompra.Select(" IDTransaccion = " & cbxComprobante.GetValue)(0)("Descontado")
            dRow("Saldo") = dtCompra.Select(" IDTransaccion = " & cbxComprobante.GetValue)(0)("Saldo")
        Catch ex As Exception

        End Try

        'Cantidad Caja
        dRow("Caja") = 0
        dRow("CantidadCaja") = CantidadCaja

        'Agregamos al detalle
        dtDetalle.Rows.Add(dRow)

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)
        CalcularTotales()

        'Inicializamos los valores
        txtObservacionProducto.txt.Clear()
        txtCantidad.txt.Text = "0"
        txtImporte.txt.Text = "0"
        txtCostoUnitario.txt.Text = "0"
        txtProducto.txt.Clear()

        'Retorno de Foco
        txtProducto.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Sub CargarDescuentos()

        'Validar
        'Cliente
        If txtProveedor.Seleccionado = False Then

            MsgBox("Selecione un cliente", vbExclamation)

            Exit Sub

        End If

        Dim frm As New frmNotaCreditoCargarDescuento
        frm.IDMoneda = cbxMoneda.GetValue
        frm.Decimales = CData.GetRow("ID=" & cbxMoneda.GetValue, "VMoneda")("Decimales")
        frm.Proveedor = Proveedor
        FGMostrarFormulario(Me, frm, "Cargar detalle", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)


        If frm.Procesado = False Then
            Exit Sub
        End If

        'Cargamos el registro en el detalle
        Dim dRow As DataRow = dtDetalle.NewRow()


        'Obtener Valores
        'Productos
        dRow("IDProducto") = frm.Registro("ID").ToString
        dRow("ID") = dtDetalle.Rows.Count
        dRow("Descripcion") = frm.Registro("Descripcion").ToString
        dRow("CodigoBarra") = frm.Registro("CodigoBarra").ToString
        dRow("Observacion") = frm.Observacion

        'Deposito
        dRow("IDDeposito") = cbxDeposito.cbx.SelectedValue
        dRow("Deposito") = cbxDeposito.cbx.Text

        'Cantidad y Precios
        dRow("Total") = frm.Importe

        'Impuestos
        dRow("IDImpuesto") = frm.Registro("IDImpuesto").ToString
        dRow("Impuesto") = frm.Registro("Impuesto").ToString
        dRow("Ref Imp") = frm.Registro("Ref Imp").ToString
        dRow("TotalImpuesto") = 0
        dRow("TotalDiscriminado") = 0
        CSistema.CalcularIVA(dRow("IDImpuesto"), CDec(dRow("Total").ToString), False, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))

        'Costo
        dRow("CostoUnitario") = 0
        dRow("TotalCosto") = 0
        dRow("TotalCostoImpuesto") = 0
        dRow("TotalCostoDiscriminado") = 0

        'Descuento
        dRow("TotalDescuento") = 0
        dRow("DescuentoUnitario") = 0
        dRow("PorcentajeDescuento") = 0


        'Totales


        'Cantidad Caja
        dRow("Caja") = False
        dRow("CantidadCaja") = 1

        'Agregamos al detalle
        dtDetalle.Rows.Add(dRow)

        ListarDetalleDescuento()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)
        CalcularTotales()

        'Inicializamos los valores
        txtObservacionProducto.txt.Clear()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Sub ListarDetalleDescuento()

        'Limpiamos todo el detalle
        lvListaDescuento.Items.Clear()

        'Variables
        Dim Total As Decimal = 0

        'Cargamos registro por registro
        For Each oRow As DataRow In dtDetalle.Rows

            Dim item As ListViewItem = lvListaDescuento.Items.Add(oRow("IDProducto").ToString)

            Dim Descripcion As String
            If oRow("Observacion").ToString.Trim.Length > 0 Then
                Descripcion = oRow("Descripcion").ToString.Trim & " - " & oRow("Observacion").ToString.Trim
            Else
                Descripcion = oRow("Descripcion").ToString.Trim
            End If

            item.SubItems.Add(Descripcion)
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("Total").ToString))
            Total = Total + CDec(oRow("Total").ToString)

        Next

        txtTotalDescuento.txt.Text = Total

        If vNuevo Then
            calcularTotalDescuento()
        End If

        Bloquear()


    End Sub

    Sub EliminarProductoDevolucion()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If lvLista.SelectedItems.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(lvLista, mensaje)
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Eliminamos los descuentos
        ' EliminarDescuentos()

        dtDetalle.Rows(lvLista.SelectedIndices(0)).Delete()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtDetalle.Rows
            If Not oRow.RowState = DataRowState.Deleted Then

                'Renumerar descuentos
                For Each dRow As DataRow In dtDescuento.Select(" IDProducto=" & oRow("IDProducto") & " And ID=" & oRow("ID") & " ")
                    dRow("ID") = Index
                Next

                oRow("ID") = Index
                Index = Index + 1
            End If
        Next

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)
        CalcularTotales()
        Bloquear()

    End Sub

    Sub EliminarProductoDescuento()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If lvListaDescuento.SelectedItems.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(lvListaDescuento, mensaje)
            ctrError.SetIconAlignment(lvListaDescuento, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        dtDetalle.Rows(lvListaDescuento.SelectedIndices(0)).Delete()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtDetalle.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        ListarDetalleDescuento()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)
        CalcularTotales()

        Bloquear()

    End Sub

    Sub EliminarComprobanteDescuento()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If dgw.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgw, mensaje)
            ctrError.SetIconAlignment(lvListaDescuento, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        For Each oRow As DataGridViewRow In dgw.SelectedRows
            Dim vIDTransaccion As Integer

            vIDTransaccion = oRow.Cells("colIDTransaccion").Value
            For Each dtRow As DataRow In dtNotaDebitoCompra.Select(" IDTransaccion = " & vIDTransaccion)
                dtRow("Sel") = False

            Next
        Next

        ListarComprobanteDescuento()


    End Sub

    Sub CargarComprobante()

        If txtProveedor.Seleccionado = False Then
            Exit Sub
        End If

        CargarComprobanteDevolucion()
        ObtenerComprobanteDescuento()


    End Sub

    Sub CargarComprobanteDevolucion()
        If vNuevo = False Then
            If txtProveedor.Seleccionado = True Then
                Exit Sub
            End If
        End If

        dtCompra = CSistema.ExecuteToDataTable("Select IDTransaccion, NroComprobante, Saldo From Compra Where Cancelado = 'False' and IDProveedor =" & txtProveedor.Registro("ID").ToString).Copy
        dtDetalleCompra = CSistema.ExecuteToDataTable("Select P.ID, P.Descripcion, P.CodigoBarra, P.Ref, P.[Ref Imp], P.UnidadPorCaja, P.IDImpuesto, P.Impuesto, P.Costo, DC.IDTransaccion, DC.PrecioUnitario, DC.Cantidad, 'SaldoCantidad'=DC.Cantidad - (IsNull((Select Sum(DNCP.Cantidad) From DetalleNotaDebitoProveedor DNCP Join NotaDebitoProveedor NCP On  DNCP.IDTransaccion=NCP.IDTransaccion Where DNCP.IDProducto=DC.IDProducto And DNCP.IDTransaccionCompra =DC.IDTransaccion And NCP.Anulado='False'), 0)), DC.Total, C.Saldo From Compra C Join DetalleCompra DC On DC.IDTransaccion=C.IDTransaccion Join VProducto P On DC.IDProducto=P.ID Where C.Cancelado = 'False' and C.IDProveedor =" & txtProveedor.Registro("ID").ToString).Copy



        CSistema.SqlToComboBox(cbxComprobante.cbx, dtCompra, "IDTransaccion", "NroComprobante")


    End Sub

    Sub ObtenerComprobanteDescuento()

        If txtProveedor.Registro Is Nothing Then
            Exit Sub
        End If

        'dtNotaDebitoCompra = CSistema.ExecuteToDataTable("Select IDTransaccion, Comprobante,[Cod.], Proveedor, Total,  Saldo, Condicion,Fec,[Fec. Venc.], 'Sel'='False', 'Importe'=0, 'Cancelar'='False' From VCompra where Cancelado = 'False' And Saldo>0 and IDProveedor =" & txtProveedor.Registro("ID").ToString).Copy
        'dtNotaDebitoCompra = CSistema.ExecuteToDataTable("Select IDTransaccion, NroComprobante,Referencia, Proveedor, Total,  Saldo, Condicion,Fecha,FechaVencimiento, 'Sel'='False', 'Importe'=0, 'Cancelar'='False' From vEgreso where Cancelado = 'False' And Saldo>0 and IDProveedor =" & txtProveedor.Registro("ID").ToString).Copy
        'FA 18/01/2024 Debe de estirar todas las facturas ya sea con saldo o no 
        'dtNotaDebitoCompra = CSistema.ExecuteToDataTable("Select IDTransaccion, Comprobante,[Cod.], Proveedor, Total,  Saldo, Condicion,Fec,[Fec. Venc.], 'Sel'='False', 'Importe'=0, 'Cancelar'='False' From VEgreso where Cancelado = 'False' And Saldo>0 and IDProveedor =" & txtProveedor.Registro("ID").ToString).Copy
        dtNotaDebitoCompra = CSistema.ExecuteToDataTable("Select IDTransaccion, Comprobante,[Cod.], Proveedor, Total,  Saldo, Condicion,Fec,[Fec. Venc.], 'Sel'='False', 'Importe'=0, 'Cancelar'='False' From VEgreso where IDProveedor =" & txtProveedor.Registro("ID").ToString).Copy

    End Sub

    Sub CargarComprobanteDescuento()

        Dim frm As New frmNotaCreditoCargarDocumentoDescuento
        frm.dt = dtNotaDebitoCompra
        FGMostrarFormulario(Me, frm, "Comprobantes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        dtNotaDebitoCompra = frm.dt
        ListarComprobanteDescuento()

    End Sub

    Sub ListarComprobanteDescuento()

        'Limpiar la grilla
        dgw.Rows.Clear()

        Dim dt As DataTable = dtNotaDebitoCompra.Copy
        For Each oRow As DataRow In dt.Select("Sel='True'")

            Dim Registro(11) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Sel").ToString
            Registro(2) = oRow("Comprobante").ToString
            Registro(3) = oRow("Cod.").ToString
            Registro(4) = oRow("Proveedor").ToString
            Registro(5) = oRow("Condicion").ToString
            Registro(6) = oRow("Fec. Venc.").ToString
            Registro(7) = CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(8) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            Registro(9) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(10) = oRow("Cancelar").ToString

            'Sumar el total de la deuda
            dgw.Rows.Add(Registro)

        Next

        calcularTotalDescuento()

    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Columns(e.ColumnIndex).Name = "colImporte" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value

            Else
                'Controlar Saldo
                Dim Saldo As Decimal = dgw.CurrentRow.Cells("colSaldo").Value
                Dim Importe As Decimal = dgw.CurrentRow.Cells("colImporte").Value

                If Importe > Saldo Then
                    Dim mensaje As String = "El importe no puede ser mayor al saldo!"
                    CSistema.MostrarError(mensaje, ctrError, dgw, tsslEstado)
                    dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value
                    GoTo siguiente
                End If

                dgw.CurrentCell.Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells("colImporte").Value)

            End If

siguiente:

            calcularTotalDescuento()

        End If

    End Sub

    Sub CargarComprobanteDescuentofacturados(ByVal IDTransaccion As Integer)

        If txtProveedor.Registro Is Nothing Then
            Exit Sub
        End If


        dtNotaDebitoCompra = CSistema.ExecuteToDataTable("Select IDTransaccion, Comprobante,[Cod.], Total, NCV.Cobrado, NCV.Descontado, NCV.Saldo, NCV.Importe, Condicion, Fec, [Fec. Venc.],V.Tipo, 'Sel'='True', 'Cancelar'='False' From VEgreso V Join NotaDebitoProveedorCompra  NCV On V.IDTransaccion=NCV.IDTransaccionEgreso   where IDTransaccionNotaDebitoProveedor = " & IDTransaccion & " And IDProveedor =" & txtProveedor.Registro("ID").ToString).Copy

        'Limpiar la grilla
        dgw.Rows.Clear()
        'Dim TotalSaldo As Decimal = 0

        For Each oRow As DataRow In dtNotaDebitoCompra.Rows

            Dim Registro(11) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Sel").ToString
            Registro(2) = oRow("Comprobante").ToString
            Registro(3) = oRow("Cod.").ToString
            Registro(4) = oRow("Condicion").ToString
            Registro(5) = oRow("Fec. Venc.").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(7) = CSistema.FormatoMoneda(oRow("Cobrado").ToString)
            Registro(8) = CSistema.FormatoMoneda(oRow("Descontado").ToString)
            Registro(9) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            Registro(10) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(11) = oRow("Cancelar").ToString

            'Sumar el total de la deuda

            'TotalSaldo = CDec(oRow("Saldo").ToString) - txtTotalDescuento.txt.Text
            dgw.Rows.Add(Registro)

        Next

        ' txtDescuento.txt.ReadOnly = True
        'dgw.ReadOnly = True




    End Sub



    Sub calcularTotalDescuento()

        Dim TotalSaldo As Decimal = 0
        Dim Total As Decimal = 0
        Dim TotalComprobantes As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows

            If oRow.Cells(1).Value = True Then
                Total = Total + oRow.Cells(9).Value
                TotalComprobantes = TotalComprobantes + 1
            End If

        Next
        TotalSaldo = txtTotalDescuento.ObtenerValor - Total
        txtDescontados.SetValue(Total)
        txtCantidadCobrado.SetValue(TotalComprobantes)
        txtSaldo.SetValue(TotalSaldo)


    End Sub



    Sub HabilitarConComprobante(ByRef Comprobante As Boolean)

        If Comprobante = True Then
            cbxComprobante.cbx.Text = ""
            cbxComprobante.SoloLectura = False
            CargarComprobanteDevolucion()
            txtCostoUnitario.SoloLectura = True
            txtImporte.SoloLectura = True

            'Descuento
            lklAgregarComprobanteDescuento.Enabled = True
            lklEliminarComprobanteDescuento.Enabled = True

        Else
            cbxComprobante.SeleccionObligatoria = False
            cbxComprobante.cbx.Text = ""
            cbxComprobante.SoloLectura = True
            txtProducto.Conectar()
            txtCostoUnitario.SoloLectura = False
            txtImporte.SoloLectura = False

            'Descuento
            lklAgregarComprobanteDescuento.Enabled = False
            lklEliminarComprobanteDescuento.Enabled = False

        End If

    End Sub

    Sub Seleccionardescuento()

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells(0).Value

            For Each oRow1 As DataRow In dtNotaDebitoCompra.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Sel") = oRow.Cells("colSel").Value
                oRow1("Importe") = CSistema.FormatoMonedaBaseDatos(oRow.Cells("colImporte").Value)
                oRow1("Cancelar") = oRow.Cells("colcancelar").Value
            Next

        Next

    End Sub

    Sub MoverRegistro(ByVal tecla As KeyEventArgs)

        If vNuevo = True Then
            Exit Sub
        End If

        If tecla.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If tecla.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If tecla.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If tecla.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Num), 1) From VNotaDebitoProveedor"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If tecla.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Num), 1) From VNotaDebitoProveedor"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

    End Sub

    Sub Bloquear()

        If dtDetalle.Rows.Count > 0 Then
            rdbDescuentos.Enabled = False
            rdbDevolucion.Enabled = False
            cbxDeposito.SoloLectura = True
            txtProveedor.SoloLectura = True
        Else
            rdbDescuentos.Enabled = True
            rdbDevolucion.Enabled = True
            cbxDeposito.SoloLectura = False
            txtProveedor.SoloLectura = False
        End If


        For i As Integer = 0 To dgw.Rows.Count - 1
            If dgw.Rows(i).Cells("colSel").Value = True Then
                rdbDescuentos.Enabled = False
                rdbDevolucion.Enabled = False

                Exit For
            End If
        Next



    End Sub

    Private Sub frmNotaCreditoProveedor_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmNotaCreditoProveedor_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Enter Then

            If txtCantidad.txt.Focused = True Then
                Exit Sub
            End If

            If txtImporte.txt.Focused = True Then
                Exit Sub
            End If

            If dgw.Focused = True Then
                Exit Sub
            End If

            CSistema.SelectNextControl(Me, e.KeyCode)

        End If

    End Sub

    Private Sub frmNotaCreditoProveedor_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub txtCantidad_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.TeclaPrecionada
        If txtCantidad.txt.Focused = False Then
            Exit Sub
        End If

        CalcularImporte(False, True, False)

        If e.KeyCode = Keys.Enter Then
            If vHabilitar = True Then
                CargarProducto()
            Else
                txtCostoUnitario.txt.Focus()
            End If

        End If
    End Sub

    Private Sub txtCostoUnitario_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCostoUnitario.TeclaPrecionada

        'If txtCostoUnitario.txt.Focused = False Then
        '    Exit Sub
        'End If

        CalcularImporte(False, True, False)

        If e.KeyCode = Keys.Enter Then
            If vHabilitar = True Then
                CargarProducto()
            Else
                txtImporte.txt.Focus()
            End If

        End If
    End Sub

    Private Sub txtProducto_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then

            If vConComprobantes = True Then
                txtCostoUnitario.txt.Text = txtProducto.Registro("PrecioUnitario").ToString
            Else
                txtCostoUnitario.txt.Text = txtProducto.Registro("Precio").ToString
            End If
            CalcularImporte(False, True, False)
            txtObservacionProducto.Focus()

        End If

    End Sub

    Private Sub txtProveedor_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProveedor.ItemSeleccionado

        If vNuevo = True Then
            CargarComprobante()
            txtFecha.txt.Focus()
        End If

    End Sub

    'Private Sub cbxComprobante_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxComprobante.Leave

    '    If chkAplicar.Valor = True Then
    '        dtDetalleVenta = CSistema.ExecuteToDataTable("Select * From VDetalleVentaParaNotaCredito V Where IDTransaccion=" & cbxComprobante.GetValue & " ").Copy
    '    Else
    '        dtDetalleVenta = CSistema.ExecuteToDataTable("Select * From VDetalleVentaParaNotaCredito V Where V.Cancelado='False' And V.IDCliente =" & txtProveedor.Registro("ID").ToString).Copy
    '    End If

    '    Dim dttemp As DataTable = CData.FiltrarDataTable(dtDetalleVenta, " IDTransaccion = " & cbxComprobante.GetValue)
    '    txtProducto.Conectar(dttemp)

    'End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvLista.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProductoDevolucion()
        End If

    End Sub



    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        MoverRegistro(e)
    End Sub

    Private Sub txtImporte_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtImporte.TeclaPrecionada
        If txtImporte.txt.Focused = False Then
            Exit Sub
        End If

        CalcularImporte(False, True, True)


        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If

    End Sub

    Private Sub rdbDescuentos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbDescuentos.CheckedChanged
        If rdbDescuentos.Checked = True Then
            ObtenerComprobanteDescuento()
            pnlDevolucion.Visible = False
            pnlDescuento.Visible = True

            If vNuevo = True Then
                If txtObservacion.GetValue = vObservacionDescuento Or txtObservacion.GetValue = vObservacionDevolucion Then
                    txtObservacion.SetValue(vObservacionDescuento)
                End If
            End If
        End If
    End Sub

    Private Sub lvListaDescuento_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvListaDescuento.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProductoDescuento()
        End If
    End Sub

    Private Sub rdbDevolucion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbDevolucion.CheckedChanged

        If rdbDevolucion.Checked = True Then
            pnlDevolucion.Visible = True
            pnlDescuento.Visible = False

            If vNuevo = True Then
                If txtObservacion.GetValue = vObservacionDescuento Or txtObservacion.GetValue = vObservacionDevolucion Then
                    txtObservacion.SetValue(vObservacionDevolucion)
                End If
            End If

        End If

    End Sub


    Private Sub cbxCiudad_PropertyChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

        cbxSucursal.cbx.Text = ""
        cbxDeposito.cbx.Text = ""

        If cbxCiudad.Validar() = False Then
            Exit Sub
        End If

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal", " IDCiudad = " & cbxCiudad.GetValue), "ID", "Descripcion")
        cbxSucursal.SelectedValue(vgIDSucursal)

    End Sub

    Private Sub cbxSucursal_PropertyChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

        cbxDeposito.cbx.Text = ""

        If cbxSucursal.Validar() = False Then
            Exit Sub
        End If

        'Deposito
        CSistema.SqlToComboBox(cbxDeposito.cbx, CData.GetTable("VDeposito", " IDSucursal = " & cbxSucursal.GetValue), "ID", "Deposito")
        cbxDeposito.SelectedValue(vgIDDeposito)

    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Close()

    End Sub

    Private Sub lklAgregarDescuento_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklAgregarDetalleDescuento.LinkClicked
        CargarDescuentos()
    End Sub

    Private Sub LinkLabel3_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklAgregarComprobanteDescuento.LinkClicked
        CargarComprobanteDescuento()
    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarDetalleDescuento.LinkClicked
        EliminarProductoDescuento()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarComprobanteDescuento.LinkClicked
        EliminarComprobanteDescuento()
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarComprobanteDescuento()
        End If
    End Sub

    Private Sub VerDetalleToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VerDetalleToolStripMenuItem.Click

        If lvLista.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim Filtro As String
        Filtro = " ID=" & lvLista.SelectedItems(0).Index & " And IDProducto=" & lvLista.SelectedItems(0).Text & " "

        Dim dttemp As DataTable = dtDetalle.Copy
        dttemp = CData.FiltrarDataTable(dttemp, Filtro)
        CSistema.MostrarPropiedades(Me, "Detalle", "", "", dttemp)

    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        CSistema.dtToExcel(dtDetalle)
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub


    Private Sub cbxComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxComprobante.PropertyChanged
        Dim dttemp As DataTable = CData.FiltrarDataTable(dtDetalleCompra, " IDTransaccion = " & cbxComprobante.GetValue)
        txtProducto.txt.Text = ""
        txtProducto.Conectar(dttemp)

    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Eliminar(ERP.CSistema.NUMOperacionesRegistro.DEL)
    End Sub
End Class








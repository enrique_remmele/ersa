﻿Public Class frmConsultaGasto

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property
    Private CajaChicaValue As Boolean
    Public Property CajaChica As Boolean
        Get
            Return CajaChicaValue
        End Get
        Set(ByVal value As Boolean)
            CajaChicaValue = value
        End Set
    End Property
    Private dtGastoValue As DataTable
    Property dtGasto As DataTable
        Get
            Return dtGastoValue
        End Get
        Set(ByVal value As DataTable)
            dtGastoValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim Consulta As String
    Dim Where As String

    'Inicializar
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'TextBox
        txtCantidadOperacion.txt.ResetText()
        txtComprobante.txt.Clear()
        txtOperacion.txt.ResetText()
        txtTotalOperacion.txt.ResetText()

        'CheckBox
        'chkComprobante.Checked = False
        chkCondicion.Checked = False
        chkUsuario.Checked = False
        chkSucursal.Checked = False

        'ComboBox
        cbxCondicion.Enabled = False
        cbxUsuario.Enabled = False
        cbxSucursal.Enabled = False

        'RadioButton

        'ListView
        dgwOperacion.Rows.Clear()

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        CargarInformacion()

        'Foco
        txtOperacion.txt.Focus()
        txtOperacion.txt.SelectAll()

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'Condicion
        cbxCondicion.Items.Add("CONT")
        cbxCondicion.Items.Add("CRED")
        cbxCondicion.DropDownStyle = ComboBoxStyle.DropDownList

        'Usuarios
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxUsuario, "Select ID, Usuario From Usuario Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

        'Condicion
        chkCondicion.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CONDICION ACTIVO", "False")
        cbxCondicion.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CONDICION", "")

        ''Comprobante
        '' chkComprobante.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "COMPROBANTE ACTIVO", "False")
        'cbxComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "COMPROBANTE", "")
        'txtComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "NROCOMPROBANTE", "")

        'Usuario
        chkUsuario.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "USUARIO ACTIVO", "False")
        cbxUsuario.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "USUARIO", "")


    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCRUSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Condicion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CONDICION ACTIVO", chkCondicion.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CONDICION", cbxCondicion.Text)

        ''Comprobante
        '' CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "COMPROBANTE ACTIVO", chkComprobante.Checked.ToString)
        'CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "COMPROBANTE", cbxComprobante.Text)
        'CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "NROCOMPROBANTE", txtComprobante.Text)

        'Usuario
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "USUARIO ACTIVO", chkUsuario.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "USUARIO", cbxUsuario.Text)

    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Operaciones
    Sub ListarOperacionesConCajaChica(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "")

        ctrError.Clear()

        Dim dt As DataTable


        Where = Condicion

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la Sucursal!") = False Then
            Exit Sub
        End If

        'Condicion
        If chkCondicion.Checked = True Then
            If Where = "" Then
                Where = " Where (Condicion='" & cbxCondicion.Text & "') "
            Else
                Where = Where & " And (Condicion='" & cbxCondicion.Text & "') "
            End If
        End If

        'Usuario
        If EstablecerCondicion(cbxUsuario, chkUsuario, "IDUsuario", "Seleccione correctamente el usuario!") = False Then
            Exit Sub
        End If

        ''Solo por numero
        If Numero > 0 Then
            Where = " Where Numero=" & Numero & " "
        End If

        dt = CSistema.ExecuteToDataTable("Select IDTransaccion, Suc, 'Caja'=NroCaja, Num,'Fec.Comp.'=Fec, 'Tipo'=TipoComprobante, Comprobante, Proveedor, Observacion, 'Mon'=Moneda, Total, Usuario From VGastoFondoFijo" & Where)

        CSistema.dtToGrid(dgwOperacion, dt)

        If dgwOperacion.Columns.Count = False Then
            Exit Sub
        End If

        'Formato
        dgwOperacion.Columns("IDtransaccion").Visible = False
        dgwOperacion.Columns("Observacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgwOperacion.Columns("Suc").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgwOperacion.Columns("Caja").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgwOperacion.Columns("Num").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgwOperacion.Columns("Mon").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader

        dgwOperacion.Columns("Proveedor").AutoSizeMode = DataGridViewAutoSizeColumnMode.None
        dgwOperacion.Columns("Proveedor").Width = 150

        dgwOperacion.Columns("Fec.Comp.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgwOperacion.Columns("Tipo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgwOperacion.Columns("Total").DefaultCellStyle.Format = "N0"
        dgwOperacion.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        txtTotalOperacion.SetValue(CSistema.dtSumColumn(dtGasto, "Total"))
        txtCantidadOperacion.SetValue(dgwOperacion.RowCount)


    End Sub

    Sub ListarOperacionesSinCajaChica(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "")

        ctrError.Clear()


        Where = Condicion

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la Sucursal!") = False Then
            Exit Sub
        End If

        'Condicion
        If chkCondicion.Checked = True Then
            If Where = "" Then
                Where = " Where (Condicion='" & cbxCondicion.Text & "') "
            Else
                Where = Where & " And (Condicion='" & cbxCondicion.Text & "') "
            End If
        End If

        'Usuario
        If EstablecerCondicion(cbxUsuario, chkUsuario, "IDUsuario", "Seleccione correctamente el usuario!") = False Then
            Exit Sub
        End If

        ''Solo por numero
        If Numero > 0 Then
            Where = " Where Numero=" & Numero & " "
        End If

        dtGasto = CSistema.ExecuteToDataTable("Select IDTransaccion, Suc, 'Caja'=NroCaja, Num,'Fec.Comp.'=Fec, 'Tipo'=TipoComprobante, Comprobante, Proveedor, Observacion, 'Mon'=Moneda, Total, Usuario From VGastoTipoComprobante" & Where)

        CSistema.dtToGrid(dgwOperacion, dtGasto)

        'Formato
        dgwOperacion.Columns("IDtransaccion").Visible = False
        dgwOperacion.Columns("Observacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgwOperacion.Columns("Suc").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgwOperacion.Columns("Caja").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgwOperacion.Columns("Num").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgwOperacion.Columns("Mon").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader

        dgwOperacion.Columns("Proveedor").AutoSizeMode = DataGridViewAutoSizeColumnMode.None
        dgwOperacion.Columns("Proveedor").Width = 150

        dgwOperacion.Columns("Fec.Comp.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgwOperacion.Columns("Tipo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgwOperacion.Columns("Total").DefaultCellStyle.Format = "N0"
        dgwOperacion.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight


        txtTotalOperacion.SetValue(CSistema.dtSumColumn(dtGasto, "Total"))
        txtCantidadOperacion.SetValue(dgwOperacion.RowCount)


    End Sub

    Sub ListarOperacionesxComprobanteConCajaChica(Optional ByVal Numero As String = "", Optional ByVal Condicion As String = "")

        ctrError.Clear()


        Where = Condicion

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la Sucursal!") = False Then
            Exit Sub
        End If

        'Condicion
        If chkCondicion.Checked = True Then
            If Where = "" Then
                Where = " Where (Condicion='" & cbxCondicion.Text & "') "
            Else
                Where = Where & " And (Condicion='" & cbxCondicion.Text & "') "
            End If
        End If

        'Usuario
        If EstablecerCondicion(cbxUsuario, chkUsuario, "IDUsuario", "Seleccione correctamente el usuario!") = False Then
            Exit Sub
        End If

        Where = " Where NroComprobante='" & Numero & "'"

        dtGasto = CSistema.ExecuteToDataTable("Select IDTransaccion, Suc, 'Caja'=NroCaja, Num,'Fec.Comp.'=Fec, 'Tipo'=TipoComprobante, Comprobante, Proveedor, Observacion, 'Mon'=Moneda, Total, Usuario From VGastoFondoFijo " & Where)

         CSistema.dtToGrid(dgwOperacion, dtGasto)

        'Formato
        dgwOperacion.Columns("IDtransaccion").Visible = False
        dgwOperacion.Columns("Observacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgwOperacion.Columns("Suc").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgwOperacion.Columns("Caja").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgwOperacion.Columns("Num").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgwOperacion.Columns("Mon").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader

        dgwOperacion.Columns("Proveedor").AutoSizeMode = DataGridViewAutoSizeColumnMode.None
        dgwOperacion.Columns("Proveedor").Width = 150

        dgwOperacion.Columns("Fec.Comp.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgwOperacion.Columns("Tipo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgwOperacion.Columns("Total").DefaultCellStyle.Format = "N0"
        dgwOperacion.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        txtTotalOperacion.SetValue(CSistema.dtSumColumn(dtGasto, "Total"))
        txtCantidadOperacion.SetValue(dgwOperacion.RowCount)

    End Sub

    Sub ListarOperacionesxComprobanteSinCajaChica(Optional ByVal Numero As String = "", Optional ByVal Condicion As String = "")

        ctrError.Clear()


        Where = Condicion

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la Sucursal!") = False Then
            Exit Sub
        End If

        'Condicion
        If chkCondicion.Checked = True Then
            If Where = "" Then
                Where = " Where (Condicion='" & cbxCondicion.Text & "') "
            Else
                Where = Where & " And (Condicion='" & cbxCondicion.Text & "') "
            End If
        End If

        'Usuario
        If EstablecerCondicion(cbxUsuario, chkUsuario, "IDUsuario", "Seleccione correctamente el usuario!") = False Then
            Exit Sub
        End If

        Where = " Where NroComprobante='" & Numero & "'"

        dtGasto = CSistema.ExecuteToDataTable("Select IDTransaccion, Suc, 'Caja'=NroCaja, Num,'Fec.Comp.'=Fec, 'Tipo'=TipoComprobante, Comprobante, Proveedor, Observacion, 'Mon'=Moneda, Total, Usuario From VGastoTipoComprobante " & Where)

        CSistema.dtToGrid(dgwOperacion, dtGasto)

        'Formato
        dgwOperacion.Columns("IDtransaccion").Visible = False
        dgwOperacion.Columns("Observacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgwOperacion.Columns("Suc").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgwOperacion.Columns("Caja").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgwOperacion.Columns("Num").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgwOperacion.Columns("Mon").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader

        dgwOperacion.Columns("Proveedor").AutoSizeMode = DataGridViewAutoSizeColumnMode.None
        dgwOperacion.Columns("Proveedor").Width = 150

        dgwOperacion.Columns("Fec.Comp.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgwOperacion.Columns("Tipo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgwOperacion.Columns("Total").DefaultCellStyle.Format = "N0"
        dgwOperacion.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        txtTotalOperacion.SetValue(CSistema.dtSumColumn(dtGasto, "Total"))
        txtCantidadOperacion.SetValue(dgwOperacion.RowCount)

    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    

    Sub SeleccionarRegistro()

        'Validar
        If dgwOperacion.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgwOperacion.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgwOperacion, Mensaje)
            ctrError.SetIconAlignment(dgwOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        IDTransaccion = dgwOperacion.SelectedRows(0).Cells("IDTransaccion").Value

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub frmConsultaCompra_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaCompra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub lvOperacion_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        SeleccionarRegistro()
    End Sub

    Private Sub chkUsuario_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUsuario.CheckedChanged
        HabilitarControles(chkUsuario, cbxUsuario)
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub


    Private Sub btnRegistroGeneral_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistroGeneral.Click
        If CajaChica = True Then
            ListarOperacionesConCajaChica()
        Else
            ListarOperacionesSinCajaChica()
        End If
    End Sub

    Private Sub dtbRegistrosPorFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtbRegistrosPorFecha.Click
        Where = " Where (Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "') "
        If CajaChica = True Then
            ListarOperacionesConCajaChica(0, Where)
        Else
            ListarOperacionesSinCajaChica(0, Where)
        End If
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            If CajaChica = True Then
                ListarOperacionesConCajaChica(txtOperacion.ObtenerValor)
                txtOperacion.txt.Focus()
                txtOperacion.txt.SelectAll()
            End If
            If CajaChica = False Then
                ListarOperacionesSinCajaChica(txtOperacion.ObtenerValor)
                txtOperacion.txt.Focus()
                txtOperacion.txt.SelectAll()
            End If
        End If
    End Sub

    Private Sub frmConsultaMovimiento_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
          CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub chkCondicion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCondicion.CheckedChanged
        HabilitarControles(chkCondicion, cbxCondicion)
    End Sub

    Private Sub dtpDesde_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpDesde.ValueChanged
        dtpHasta.Value = dtpDesde.Text
    End Sub

    Private Sub txtComprobante_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        
    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        SeleccionarRegistro()
    End Sub

    Private Sub txtComprobante_TeclaPrecionada1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            If CajaChica = True Then
                ListarOperacionesxComprobanteConCajaChica(txtComprobante.GetValue)
                txtComprobante.Focus()
                txtComprobante.txt.SelectAll()
            End If
            If CajaChica = False Then
                ListarOperacionesxComprobanteSinCajaChica(txtComprobante.GetValue)
                txtComprobante.Focus()
                txtComprobante.txt.SelectAll()
            End If
        End If
    End Sub

    Private Sub dgwOperacion_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgwOperacion.DoubleClick
        SeleccionarRegistro()
    End Sub

    Private Sub dgwOperacion_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgwOperacion.KeyUp

    End Sub

    Private Sub txtNroCaja_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtNroCaja.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            If CajaChica = True Then
                ListarOperacionesConCajaChica(0, " Where NroCaja=" & txtNroCaja.ObtenerValor)
                txtNroCaja.txt.Focus()
                txtNroCaja.txt.SelectAll()
            End If
            If CajaChica = False Then
                ListarOperacionesSinCajaChica(0, " Where NroCaja=" & txtNroCaja.ObtenerValor)
                txtNroCaja.txt.Focus()
                txtNroCaja.txt.SelectAll()
            End If
        End If
    End Sub
End Class
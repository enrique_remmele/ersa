﻿Public Class frmValeAResindir

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property

    Public Property IDSucursal As Integer
    Public Property IDGrupo As Integer

    'FUNCIONES
    Sub Inicializar()

        'Form

        'Fechas
        txtDesde.PrimerDiaMes()
        txtHasta.UltimoDiaMes()

        'Funciones
        CargarInformacion()
        Listar()
        Cargar()

        'Foco
        dgw.Focus()

    End Sub

    Sub CargarInformacion()

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo From VSucursal ")

        'Grupo
        CSistema.SqlToComboBox(cbxGrupo.cbx, "Select IDGrupo, Grupo From vGrupoUsuario Where Usuario ='" & vgUsuario & "'")

        cbxSucursal.SelectedValue(IDSucursal)
        cbxGrupo.SelectedValue(IDGrupo)

    End Sub

    Sub Listar()

        'Limpiar la grilla
        dgw.Rows.Clear()
        Dim TotalDeuda As Decimal = 0

        For Each oRow As DataRow In dt.Select("IDSucursal=" & cbxSucursal.GetValue & " And IDGrupo=" & cbxGrupo.GetValue & " And (Fecha >= '" & txtDesde.GetValue & "' And Fecha <= '" & txtHasta.GetValue & "') ")

            Dim Registro(10) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Sel").ToString
            Registro(2) = oRow("Suc").ToString
            Registro(3) = oRow("Fec").ToString
            Registro(4) = oRow("Motivo").ToString
            Registro(5) = oRow("Nombre").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(7) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            Registro(8) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(9) = oRow("Cancelar").ToString

            'Sumar el total de la deuda
            TotalDeuda = TotalDeuda + CDec(oRow("Saldo").ToString)

            dgw.Rows.Add(Registro)

        Next

        txtTotalComprobante.SetValue(TotalDeuda)
        txtCantidadComprobante.SetValue(dt.Rows.Count)


    End Sub

    Sub Seleccionar()

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells(0).Value

            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Sel") = oRow.Cells("colSel").Value
                oRow1("Importe") = oRow.Cells("colImporte").Value
                oRow1("Cancelar") = oRow.Cells("colCancelar").Value
            Next

        Next

        Me.Close()

    End Sub

    Sub Cargar()

        For Each oRow As DataRow In dt.Rows
            For Each oRow1 As DataGridViewRow In dgw.Rows
                If oRow("IDTransaccion") = oRow1.Cells(0).Value Then
                    oRow1.Cells(1).Value = CBool(oRow("Sel").ToString)
                    oRow1.Cells(7).Value = CSistema.FormatoMoneda(oRow("Saldo").ToString)
                    oRow1.Cells(8).Value = CSistema.FormatoMoneda(oRow("Importe").ToString)
                    oRow1.Cells(9).Value = CBool(oRow("Cancelar").ToString)
                End If
            Next
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim TotalPagado As Integer = 0
        Dim CantidadPagado As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Cells("colSel").Value = True Then
                TotalPagado = TotalPagado + oRow.Cells("colImporte").Value
                CantidadPagado = CantidadPagado + 1
            End If
        Next

        txtTotalPagado.SetValue(TotalPagado)
        txtCantidadPagado.SetValue(CantidadPagado)

        txtTotalComprobante.SetValue(CSistema.gridSumColumn(dgw, "colTotal"))
        txtCantidadComprobante.SetValue(dgw.RowCount)

    End Sub

    Sub PintarCelda()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(1).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next

    End Sub

    Private Sub frmCobranzaCreditoSeleccionarVentas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick

        ctrError.Clear()
        tsslEstado.Text = ""

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        If e.ColumnIndex = dgw.Columns.Item("colSel").Index Then

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSel").Value = False Then
                        oRow.Cells("colSel").Value = True
                        oRow.Cells("colSel").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = False
                        oRow.Cells("colCancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                    Else
                        oRow.Cells("colSel").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = False
                        oRow.Cells("colCancelar").ReadOnly = True
                        oRow.Cells("colImporte").Value = oRow.Cells("colSaldo").Value
                        oRow.Cells("colSel").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

        End If

    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Columns(e.ColumnIndex).Name = "colImporte" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value

            Else
                dgw.CurrentCell.Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells("colImporte").Value)
            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Space Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSel").Value = False Then
                        oRow.Cells("colSel").Value = True
                        oRow.Cells("colSel").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = False
                        oRow.Cells("colCancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("colImporte")

                    Else
                        oRow.Cells("colSel").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colCancelar").ReadOnly = True
                        oRow.Cells("colImporte").Value = oRow.Cells("colSaldo").Value
                        oRow.Cells("colSel").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If

        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSel").Value = False Then
                        oRow.Cells("colSel").Value = True
                        oRow.Cells("colSel").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = False
                        oRow.Cells("colCancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                    Else
                        oRow.Cells("colSel").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colCancelar").ReadOnly = True
                        oRow.Cells("colImporte").Value = oRow.Cells("colSaldo").Value
                        oRow.Cells("colSel").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For


                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colImporte")
            End If

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If

    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Index = e.RowIndex Then
                If oRow.Cells("colSel").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                End If

                oRow.DefaultCellStyle.Font = f1

                oRow.Cells("colImporte").Selected = True

            Else

                oRow.DefaultCellStyle.Font = f2

                If oRow.Cells("colSel").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.White
                Else
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                End If
            End If
        Next
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp

        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells("colImporte")
        End If

    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeleccionarTodoToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = True
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuitarTodaSeleccionToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = False
        Next

        PintarCelda()
        CalcularTotales()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Seleccionar()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked

    End Sub

    Private Sub btnListar_Click(sender As System.Object, e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

End Class






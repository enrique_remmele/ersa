﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCuotasSaldos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CopiarEsteImporteALosDemasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblCantidadCuotas = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.txtCuotas = New ERP.ocxTXTNumeric()
        Me.txtSumatoria = New ERP.ocxTXTNumeric()
        Me.txtVencimientiPrimeraCuota = New ERP.ocxTXTDate()
        Me.lblSumatoria = New System.Windows.Forms.Label()
        Me.txtTotal = New ERP.ocxTXTNumeric()
        Me.btnAplicar = New System.Windows.Forms.Button()
        Me.lblVencimientiPrimeraCuota = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.BackgroundColor = System.Drawing.Color.White
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.ContextMenuStrip = Me.ContextMenuStrip1
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv.Location = New System.Drawing.Point(3, 40)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.RowHeadersVisible = False
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgv.Size = New System.Drawing.Size(525, 282)
        Me.dgv.StandardTab = True
        Me.dgv.TabIndex = 1
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CopiarEsteImporteALosDemasToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(244, 26)
        '
        'CopiarEsteImporteALosDemasToolStripMenuItem
        '
        Me.CopiarEsteImporteALosDemasToolStripMenuItem.Name = "CopiarEsteImporteALosDemasToolStripMenuItem"
        Me.CopiarEsteImporteALosDemasToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.CopiarEsteImporteALosDemasToolStripMenuItem.Text = "Copiar este importe a los demas"
        '
        'lblCantidadCuotas
        '
        Me.lblCantidadCuotas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCantidadCuotas.Location = New System.Drawing.Point(225, 4)
        Me.lblCantidadCuotas.Margin = New System.Windows.Forms.Padding(4)
        Me.lblCantidadCuotas.Name = "lblCantidadCuotas"
        Me.lblCantidadCuotas.Size = New System.Drawing.Size(65, 22)
        Me.lblCantidadCuotas.TabIndex = 2
        Me.lblCantidadCuotas.Text = "Cuotas:"
        Me.lblCantidadCuotas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(447, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 5
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(366, 3)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 4
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'txtCuotas
        '
        Me.txtCuotas.Color = System.Drawing.Color.Empty
        Me.txtCuotas.Decimales = True
        Me.txtCuotas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCuotas.Indicaciones = Nothing
        Me.txtCuotas.Location = New System.Drawing.Point(298, 4)
        Me.txtCuotas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtCuotas.Name = "txtCuotas"
        Me.txtCuotas.Size = New System.Drawing.Size(35, 22)
        Me.txtCuotas.SoloLectura = False
        Me.txtCuotas.TabIndex = 3
        Me.txtCuotas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCuotas.Texto = "0"
        '
        'txtSumatoria
        '
        Me.txtSumatoria.Color = System.Drawing.Color.Empty
        Me.txtSumatoria.Decimales = True
        Me.txtSumatoria.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSumatoria.Indicaciones = Nothing
        Me.txtSumatoria.Location = New System.Drawing.Point(262, 4)
        Me.txtSumatoria.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtSumatoria.Name = "txtSumatoria"
        Me.txtSumatoria.Size = New System.Drawing.Size(97, 22)
        Me.txtSumatoria.SoloLectura = True
        Me.txtSumatoria.TabIndex = 3
        Me.txtSumatoria.TabStop = False
        Me.txtSumatoria.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSumatoria.Texto = "0"
        '
        'txtVencimientiPrimeraCuota
        '
        Me.txtVencimientiPrimeraCuota.Color = System.Drawing.Color.Empty
        Me.txtVencimientiPrimeraCuota.Fecha = New Date(CType(0, Long))
        Me.txtVencimientiPrimeraCuota.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVencimientiPrimeraCuota.Location = New System.Drawing.Point(138, 4)
        Me.txtVencimientiPrimeraCuota.Margin = New System.Windows.Forms.Padding(3, 4, 3, 3)
        Me.txtVencimientiPrimeraCuota.Name = "txtVencimientiPrimeraCuota"
        Me.txtVencimientiPrimeraCuota.PermitirNulo = True
        Me.txtVencimientiPrimeraCuota.Size = New System.Drawing.Size(80, 21)
        Me.txtVencimientiPrimeraCuota.SoloLectura = False
        Me.txtVencimientiPrimeraCuota.TabIndex = 1
        '
        'lblSumatoria
        '
        Me.lblSumatoria.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSumatoria.Location = New System.Drawing.Point(171, 4)
        Me.lblSumatoria.Margin = New System.Windows.Forms.Padding(4)
        Me.lblSumatoria.Name = "lblSumatoria"
        Me.lblSumatoria.Size = New System.Drawing.Size(83, 22)
        Me.lblSumatoria.TabIndex = 2
        Me.lblSumatoria.Text = "Sumatoria:"
        Me.lblSumatoria.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTotal
        '
        Me.txtTotal.Color = System.Drawing.Color.Empty
        Me.txtTotal.Decimales = True
        Me.txtTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.Indicaciones = Nothing
        Me.txtTotal.Location = New System.Drawing.Point(66, 4)
        Me.txtTotal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(97, 22)
        Me.txtTotal.SoloLectura = True
        Me.txtTotal.TabIndex = 1
        Me.txtTotal.TabStop = False
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Texto = "0"
        '
        'btnAplicar
        '
        Me.btnAplicar.Location = New System.Drawing.Point(340, 3)
        Me.btnAplicar.Name = "btnAplicar"
        Me.btnAplicar.Size = New System.Drawing.Size(75, 23)
        Me.btnAplicar.TabIndex = 4
        Me.btnAplicar.Text = "Aplicar"
        Me.btnAplicar.UseVisualStyleBackColor = True
        '
        'lblVencimientiPrimeraCuota
        '
        Me.lblVencimientiPrimeraCuota.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVencimientiPrimeraCuota.Location = New System.Drawing.Point(4, 4)
        Me.lblVencimientiPrimeraCuota.Margin = New System.Windows.Forms.Padding(4)
        Me.lblVencimientiPrimeraCuota.Name = "lblVencimientiPrimeraCuota"
        Me.lblVencimientiPrimeraCuota.Size = New System.Drawing.Size(127, 22)
        Me.lblVencimientiPrimeraCuota.TabIndex = 0
        Me.lblVencimientiPrimeraCuota.Text = "Venc. 1ra. Cuota:"
        Me.lblVencimientiPrimeraCuota.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dgv, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(531, 360)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnGuardar)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtSumatoria)
        Me.FlowLayoutPanel2.Controls.Add(Me.lblSumatoria)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtTotal)
        Me.FlowLayoutPanel2.Controls.Add(Me.lblTotal)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 328)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(525, 29)
        Me.FlowLayoutPanel2.TabIndex = 2
        '
        'lblTotal
        '
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(10, 4)
        Me.lblTotal.Margin = New System.Windows.Forms.Padding(4)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(48, 22)
        Me.lblTotal.TabIndex = 0
        Me.lblTotal.Text = "Total:"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.lblVencimientiPrimeraCuota)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtVencimientiPrimeraCuota)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblCantidadCuotas)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtCuotas)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnAplicar)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(525, 31)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'frmCuotasSaldos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(531, 360)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmCuotasSaldos"
        Me.Text = "frmCuotasSaldos"
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents CopiarEsteImporteALosDemasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblCantidadCuotas As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents txtCuotas As ERP.ocxTXTNumeric
    Friend WithEvents txtSumatoria As ERP.ocxTXTNumeric
    Friend WithEvents txtVencimientiPrimeraCuota As ERP.ocxTXTDate
    Friend WithEvents lblSumatoria As System.Windows.Forms.Label
    Friend WithEvents txtTotal As ERP.ocxTXTNumeric
    Friend WithEvents btnAplicar As System.Windows.Forms.Button
    Friend WithEvents lblVencimientiPrimeraCuota As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
End Class

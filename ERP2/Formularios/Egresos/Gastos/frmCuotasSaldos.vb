﻿Public Class frmCuotasSaldos
    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Public Property Cuotas As Integer
    Public Property IDTransaccion As Integer
    Public Property Procesado As Boolean
    Public Property Total As Decimal
    Public Property FechaInicial As Date
    Public Property dt As DataTable
    Public Property SoloConsulta As Boolean

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Funciones
        If dt Is Nothing Then
            dt = CSistema.ExecuteToDataTable("Select Top(0) * From VCuota")
        End If

        'Propiedades
        txtCuotas.SetValue(Cuotas)
        txtTotal.SetValue(Total)
        txtVencimientiPrimeraCuota.SetValue(FechaInicial)

        Listar()

        'Focus
        If dgv.Rows.Count > 0 Then
            Me.ActiveControl = dgv
            Me.dgv.Focus()
        Else
            Me.ActiveControl = txtVencimientiPrimeraCuota
            Me.txtVencimientiPrimeraCuota.Focus()
        End If

        If SoloConsulta = True Then

            btnAplicar.Visible = False
            txtCuotas.Visible = False
            lblSumatoria.Visible = False
            txtSumatoria.Visible = False
            btnGuardar.Visible = False
            lblCantidadCuotas.Visible = False
            lblVencimientiPrimeraCuota.Visible = False
            txtVencimientiPrimeraCuota.Visible = False

            dgv.Columns("Vencimiento").ReadOnly = True
            dgv.Columns("Saldo").ReadOnly = True
            dgv.Columns("Importe").ReadOnly = True

            PintarPagados()

        End If

    End Sub

    Sub Aplicar(Optional ValorPrimeraCuota As Decimal = 0)

        'Validar
        If txtCuotas.ObtenerValor = 0 Then
            Exit Sub
        End If

        dt.Rows.Clear()

        Dim vPrimeraCuota As Decimal = 0
        'Si no se pasa la primera cuota explicitamente, calcular
        If ValorPrimeraCuota = 0 Then
            vPrimeraCuota = PrimeraCuota()
        Else
            vPrimeraCuota = ValorPrimeraCuota
        End If

        Dim Importe As Decimal = Total - vPrimeraCuota
        Importe = Importe / (txtCuotas.ObtenerValor - 1)

        Dim Vencimiento As Date = txtVencimientiPrimeraCuota.GetValue

        For i As Integer = 1 To txtCuotas.ObtenerValor
            Dim Newrow As DataRow = dt.NewRow
            Newrow("IDTransaccion") = IDTransaccion
            Newrow("ID") = i.ToString

            If i = 1 Then
                Newrow("Importe") = vPrimeraCuota
                Newrow("Saldo") = vPrimeraCuota
            Else
                Newrow("Importe") = Importe
                Newrow("Saldo") = Importe
            End If

            Newrow("Vencimiento") = Vencimiento
            Newrow("Cancelado") = False

            dt.Rows.Add(Newrow)

            'Sumamos un mes
            Vencimiento = DateAdd(DateInterval.Month, 1, Vencimiento)

        Next

        Listar()

        If dgv.Rows.Count > 0 Then
            Me.ActiveControl = dgv
            dgv.Focus()
            dgv.CurrentCell = dgv.Rows(0).Cells("Importe")
        End If



    End Sub

    Sub Listar()

        dgv.DataSource = Nothing

        If dt Is Nothing Then
            Exit Sub
        End If

        CSistema.dtToGrid(dgv, dt)
        dgv.SelectionMode = DataGridViewSelectionMode.CellSelect

        'Formato
        dgv.Columns("IDTransaccion").Visible = False
        CSistema.DataGridColumnasNumericas(dgv, {"Importe", "Saldo"})
        dgv.Columns("Vencimiento").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv.Columns("Vencimiento").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("Saldo").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("Importe").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("ID").AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet
        dgv.Columns("ID").Width = 30


        'Solo lectura
        dgv.Columns("ID").ReadOnly = True
        dgv.Columns("Saldo").ReadOnly = True
        dgv.Columns("Cancelado").ReadOnly = True

        'Nombres de Columnas
        dgv.Columns("ID").HeaderText = "Nro."
        dgv.Columns("Cancelado").HeaderText = "Canc."

        'Total
        txtSumatoria.SetValue(CSistema.gridSumColumn(dgv, "Importe"))

    End Sub

    Sub Aceptar()

        'Validar los importes
        Dim TotalCuotas As Decimal = CSistema.gridSumColumn(dgv, "Importe")

        If TotalCuotas <> Total Then
            MessageBox.Show("Los importes no son correctos! Tiene diferencia con el total del documento.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        'Pasar lo de la grilla al datatable
        For Each oRow As DataGridViewRow In dgv.Rows
            For Each oRow1 As DataRow In dt.Select("ID=" & oRow.Cells("ID").Value)
                oRow1("Importe") = oRow.Cells("Importe").Value
                oRow1("Saldo") = oRow.Cells("Saldo").Value
            Next
        Next

        Cuotas = txtCuotas.ObtenerValor
        Procesado = True
        Me.Close()

    End Sub

    Sub Cancelar()

        Procesado = False
        Me.Close()

    End Sub

    Function PrimeraCuota(Optional Importe As Decimal = 0) As Decimal

        'Por un lado, sacamos la primera parte
        Dim vprimera As Decimal
        Dim vdemas As Decimal
        Dim vcuotas As Decimal = txtCuotas.ObtenerValor
        Dim vTotal As Decimal = Total

        If Importe = 0 Then
            vprimera = Total / vcuotas
            'Verificamos si el valor es redondo
            If vprimera = Int(vprimera) Then
                Return vprimera
            End If
        Else
            vprimera = Importe
        End If

        'Vamos a quitarle al total la primera parte
        vTotal = vTotal - vprimera

        'Ahora calculamos las demas cuotas
        vdemas = vTotal / (vcuotas - 1)

        'Nos fijamos si los demas ya estan redondeados
        If vdemas = Int(vdemas) Then
            Return vprimera
        Else
            vprimera = Math.Ceiling(vprimera)
            vprimera = vprimera + 1
            vprimera = PrimeraCuota(vprimera)
        End If

        Return vprimera

    End Function

    Sub CopiarATodos()

        'Validar
        If dgv.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        Dim Importe As Decimal = dgv.SelectedCells(0).Value

        For Each oRow As DataGridViewRow In dgv.Rows
            oRow.Cells("Importe").Value = Importe
            oRow.Cells("Saldo").Value = Importe
        Next

        'Total
        txtSumatoria.SetValue(CSistema.gridSumColumn(dgv, "Importe"))

    End Sub

    Sub PintarPagados()

        For Each orow As DataGridViewRow In dgv.Rows
            If orow.Cells("Cancelado").Value = True Then
                orow.DefaultCellStyle.BackColor = Color.LightGreen
            End If
        Next

    End Sub

    Private Sub frmCuota_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        'If dgv.Focused = True Then
        '    Exit Sub
        'End If

        'CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub frmCuota_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAplicar_Click(sender As System.Object, e As System.EventArgs) Handles btnAplicar.Click
        Aplicar()
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        Aceptar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub txtCuotas_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtCuotas.TeclaPrecionada
        Cuotas = txtCuotas.ObtenerValor

        If e.KeyCode = Keys.Enter Then
            If txtCuotas.ObtenerValor > 0 Then
                btnAplicar.Focus()
            End If
        End If

    End Sub

    Private Sub CopiarEsteImporteALosDemasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CopiarEsteImporteALosDemasToolStripMenuItem.Click
        CopiarATodos()
    End Sub

    Private Sub dgv_CellEndEdit(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellEndEdit
        txtSumatoria.SetValue(CSistema.gridSumColumn(dgv, "Importe"))

        If e.RowIndex = 0 Then

            If dgv.Columns(e.ColumnIndex).Name = "Importe" Then
                Aplicar(dgv.Rows(e.RowIndex).Cells("Importe").Value)
            End If

        End If

    End Sub

    Private Sub txtVencimientiPrimeraCuota_Leave(sender As System.Object, e As System.EventArgs) Handles txtVencimientiPrimeraCuota.Leave
        FechaInicial = txtVencimientiPrimeraCuota.GetValue
    End Sub

    Private Sub txtVencimientiPrimeraCuota_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtVencimientiPrimeraCuota.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            txtCuotas.Focus()
        End If
    End Sub
End Class
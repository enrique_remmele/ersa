﻿Public Class frmGenerarCuotasGastos
    ' CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Public CAsiento As New CAsientoGasto
    Dim IDTransaccionGasto As Integer

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private IDTransaccionARendirValue As Integer
    Public Property IDTransaccionARendir() As Integer
        Get
            Return IDTransaccionARendirValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionARendirValue = value
        End Set
    End Property

    Private CajaChicaValue As Boolean
    Public Property CajaChica As Boolean
        Get
            Return CajaChicaValue
        End Get
        Set(ByVal value As Boolean)
            CajaChicaValue = value
        End Set
    End Property

    Private NumeroValue As Integer
    Public Property Numero As Integer
        Get
            Return NumeroValue
        End Get
        Set(ByVal value As Integer)
            NumeroValue = value
        End Set
    End Property

    Public Property dtCuota As DataTable
    Public Property Cuota As Integer

    'EVENTOS

    'VARIABLES
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim dtVales As New DataTable

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        txtProveedor.Conectar()

        'Otros
        txtVencimiento.Enabled = False
        OcxImpuesto1.Inicializar()
        CDetalleImpuesto.Inicializar()
        txtCotizacion.Inicializar()
        flpRegistradoPor.Visible = False

        'Propiedades
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "CUOTAS SALDOS DE GASTOS ", "CUOG")
        vNuevo = True

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        MostrarCajaChica()

        'Foco
        Me.ActiveControl = txtID
        txtID.txt.Focus()

        If IDTransaccion <> 0 Then
            CargarOperacion(IDTransaccion)
        Else
            txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        End If

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, txtProveedor)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, cbxCondicion)
        CSistema.CargaControl(vControles, txtVencimiento)
        'CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxGrupo)
        CSistema.CargaControl(vControles, txtCotizacion)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, cbxRemision)
        CSistema.CargaControl(vControles, cbxTipoIVA)
        CSistema.CargaControl(vControles, chkCheque)
        CSistema.CargaControl(vControles, chkEfectivo)
        CSistema.CargaControl(vControles, lklAgregarVale)
        CSistema.CargaControl(vControles, lklEliminarVale)
        CSistema.CargaControl(vControles, txtTimbrado)
        CSistema.CargaControl(vControles, txtVtoTimbrado)
        CSistema.CargaControl(vControles, cbxDeposito)
        CSistema.CargaControl(vControles, cbxCamion)
        CSistema.CargaControl(vControles, cbxChofer)

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CONDICION
        cbxCondicion.cbx.Items.Add("CONTADO")
        cbxCondicion.cbx.Items.Add("CREDITO")
        cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'TIPO IVA
        cbxTipoIVA.cbx.Items.Add("DIRECTO")
        cbxTipoIVA.cbx.Items.Add("INDISTINTO")
        cbxTipoIVA.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipoIVA.cbx.SelectedIndex = 0

        'CARGAR CONTROLES
        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal"), "ID", "Codigo")

        'Grupo
        'JGR 20140809 Verificar esta carga. Posiblemente hay que cargar todos los items pero a la hora de guardar controlar si el grupo corresponde al usuario
        'CSistema.SqlToComboBox(cbxGrupo.cbx, CData.GetTable("vGrupoUsuario", "IDUsuario=" & vgIDUsuario), "IDGrupo", "Grupo")
        CSistema.SqlToComboBox(cbxGrupo.cbx, "Select IDGrupo as ID, Grupo From vGrupoUsuario GROUP BY IDGrupo, Grupo order by ID ")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, CData.GetTable("VTipoComprobante", "IDOperacion=" & IDOperacion), "ID", "Codigo")

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Condicion
        cbxCondicion.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CONDICION", "CONTADO")

        'Grupo
        cbxGrupo.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "GRUPO", "")

        'Moneda
        txtCotizacion.cbxMoneda.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MONEDA", "")

        'Forma de Pago Cheque
        chkCheque.Valor = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CHEQUE", "True")

        'Forma de Pago Efectivo
        chkEfectivo.Valor = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "Efectivo", "False")

        'Impuestos
        OcxImpuesto1.EstablecerSoloLectura()

        'Configuraciones
        'Deposito
        lblDeposito.Visible = CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDeposito").ToString)
        cbxDeposito.Visible = CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDeposito").ToString)

        'Distribucion
        pnlDistribucion.Visible = CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDistribucion").ToString)

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, New Button, btnBusquedaAvanzada, btnAsiento, vControles)

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            If vNuevo = False Then
                If CajaChica = True Then
                    CargarOperacionConFondoFijo()
                Else
                    CargarOperacion()
                End If
            End If
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CajaChica = True Then
                ID = CSistema.ExecuteScalar("Select Min(Numero) From VGastoFondoFijo Where IDSucursal=" & cbxSucursal.GetValue & " And Numero>" & ID & " ")
                txtID.txt.Text = ID
                txtID.txt.SelectAll()
                CargarOperacionConFondoFijo()
            Else
                ID = CSistema.ExecuteScalar("Select IsNull((Select Min(Numero) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.GetValue & " And Numero>" & ID & "), 0) ")
                txtID.txt.Text = ID
                txtID.txt.SelectAll()
                CargarOperacion()
            End If

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            'Buscar el Maximo menor de lo ingresado
            If CajaChica = True Then
                ID = CSistema.ExecuteScalar("Select Max(Numero) From VGastoFondoFijo Where IDSucursal=" & cbxSucursal.GetValue & " And Numero<" & ID & " ")
                txtID.txt.Text = ID
                txtID.txt.SelectAll()
                CargarOperacionConFondoFijo()
            Else
                    ID = CSistema.ExecuteScalar("Select Max(Numero) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.GetValue & " And Numero<" & ID & " ")
                    txtID.txt.Text = ID
                    txtID.txt.SelectAll()
                    CargarOperacion()
                End If

        End If

        If e.KeyCode = Keys.End Then

            'If vNuevo = True Then
            '    Exit Sub
            'End If

            Dim ID As Integer
            If CajaChica = True Then
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From GastoFondoFijo Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)
            Else
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And CajaChica='False' "), Integer)
            End If


            txtID.txt.Text = ID
            txtID.txt.SelectAll()

            If CajaChica = True Then
                CargarOperacionConFondoFijo()
            Else
                CargarOperacion()
            End If

        End If

        If e.KeyCode = Keys.Home Then

            'If vNuevo = True Then
            '    Exit Sub
            'End If

            Dim ID As Integer
            If CajaChica = True Then
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From GastoFondoFijo Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)
            Else
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And CajaChica='False' "), Integer)
            End If


            txtID.txt.Text = ID
            txtID.txt.SelectAll()

            If CajaChica = True Then
                CargarOperacionConFondoFijo()
            Else
                CargarOperacion()
            End If

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If


    End Sub

    Sub GuardarInformacion()

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Condicion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CONDICION", cbxCondicion.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

        'Grupo0
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "GRUPO", cbxGrupo.cbx.Text)

        'Moneda
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", txtCotizacion.cbxMoneda.cbx.Text)

        'Forma de Pago Cheque
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CHEQUE", chkCheque.Valor.ToString)

        'Forma de Pago Efectivo
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "Efectivo", chkEfectivo.Valor.ToString)

    End Sub

    Function SeleccionarRegistro(vTabla As String) As Integer

        SeleccionarRegistro = 0

        Dim vIDTransaccion As Integer

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select IDTransaccion, Sucursal, Numero, [Cod.], Comprobante, Proveedor, Fecha, Grupo, FechaTransaccion, Usuario From " & vTabla & " Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ")

        If dt.Rows.Count = 0 Then
            'vIDTransaccion = 0
        End If

        If dt.Rows.Count = 1 Then
            vIDTransaccion = dt.Rows(0)("IDTransaccion").ToString
        End If

        If dt.Rows.Count > 1 Then
            MessageBox.Show("El sistema encontro que hay una duplicacion de numeracion.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)

            Dim frm As New frmGastosDuplicados
            frm.dt = dt
            FGMostrarFormulario(Me, frm, "Duplicacion de Numeracion", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

            If frm.Procesado = False Then
                vIDTransaccion = 0
            End If

            If frm.Procesado = True Then
                vIDTransaccion = frm.IDTransaccion
            End If

        End If

        SeleccionarRegistro = vIDTransaccion

    End Function


    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        vNuevo = False

        txtID.SoloLectura = False
        txtID.txt.Focus()

        'Ultimo Registro
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Validar
        'Tipo Comprobante
        If cbxTipoComprobante.Validar("Seleccione correctamente el tipo de comprobante!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            CSistema.MostrarError("Ingrese un número de comprobante!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Sucursal
        If cbxSucursal.Validar("Seleccione correctamente la sucursal!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Total
        If CDec(OcxImpuesto1.txtTotal.ObtenerValor) <= 0 Then
            Dim mensaje As String = "El importe del documento no es válido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If


        'Timbrado
        If CData.GetTable("VTipoComprobante", " ID=" & cbxTipoComprobante.GetValue)(0)("ComprobanteTimbrado") = True Then
            If txtTimbrado.txt.Text.Trim.Length = 0 Then
                Dim mensaje As String = "Ingrese un número de timbrado!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If CSistema.ControlarTimbrado(txtTimbrado.txt.Text) = False Then
                Exit Function
            End If

            If CSistema.ControlarNroDocumento(txtComprobante.txt.Text) = False Then
                Exit Function
            End If

        End If

        'Forma de Pago
        If chkCheque.Valor = False And chkEfectivo.Valor = False Then
            CSistema.MostrarError("Seleccione una forma de pago!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Validar el Asiento
        If CAsiento.dtAsiento.Rows.Count = 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Validar el Asiento
        If CAsiento.dtDetalleAsiento.Rows.Count = 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        If CAsiento.ObtenerSaldo <> 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        If CAsiento.ObtenerTotal = 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Caja
        If CAsiento.CajaHabilitada = False Then
            CSistema.MostrarError("La caja no esta habilidada!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Si va a eliminar
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        For Each oRow As DataRow In dtVales.Select("Sel='True'")
            If CDec(oRow("Importe").ToString) > CDec(oRow("Saldo").ToString) Then
                Dim mensaje As String = "El importe del vale no debe exceder al saldo!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If
        Next

        'Fecha
        'If CSistema.ControlFechaOperacion(Me, IDOperacion, "Guardar un gasto con fecha anterior", "", txtComprobante.GetValue, txtFecha.GetValue, OcxImpuesto1.txtTotal.ObtenerValor) = False Then
        '    Exit Function
        'End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        'If ValidarDocumento(Operacion) = False Then
        '    Exit Sub
        'End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IDTransaccion As Integer
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)


        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccionGasto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", Numero, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", "1", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroTimbrado", txtTimbrado.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaVencimientoTimbrado", txtVtoTimbrado.GetValueString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDGrupo", cbxGrupo.cbx, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        If cbxTipoIVA.cbx.Text = "DIRECTO" Then
            CSistema.SetSQLParameter(param, "@Directo", "True", ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Directo", "False", ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IncluirLibro", chkIncluirLibro.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Efectivo", chkEfectivo.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cheque", chkCheque.Valor.ToString, ParameterDirection.Input)

        If CajaChica = True Then
            CSistema.SetSQLParameter(param, "@CajaChica", "True", ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@CajaChica", "False", ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Cuota", Cuota, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", "CUOTA", ParameterDirection.Input)

        IndiceOperacion = param.GetLength(0) - 1

        'Moneda
        CSistema.SetSQLParameter(param, "@IDMoneda", txtCotizacion.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(txtCotizacion.txtCotizacion.ObtenerValor), ParameterDirection.Input)

        'Credito
        If cbxCondicion.cbx.SelectedIndex = 0 Then
            CSistema.SetSQLParameter(param, "@Credito", "False".ToString, ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Credito", "True".ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@FechaVencimiento", CSistema.FormatoFechaBaseDatos(txtVencimiento.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        End If

        'Distribucion
        If CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDeposito").ToString) = True Then
            CSistema.SetSQLParameter(param, "@IDDepositoOperacion", cbxDeposito.cbx, ParameterDirection.Input)
        End If

        If CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDistribucion").ToString) = True Then
            CSistema.SetSQLParameter(param, "@IDCamion", cbxCamion.cbx, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDChofer", cbxChofer.cbx, ParameterDirection.Input)
        End If

        'Totales
        'Totales
        CSistema.SetSQLParameter(param, "@Total", OcxImpuesto1.TotalBD, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalImpuesto", OcxImpuesto1.TotalImpuestoBD, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", OcxImpuesto1.TotalDiscriminadoBD, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpGasto", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From Gasto Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpGasto", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        Dim Procesar As Boolean = True

        'Cuotas
        If IDTransaccion > 0 And Cuota > 0 Then
            'Insertamos las cuotas
            InsertarCuotas(ERP.CSistema.NUMOperacionesRegistro.INS, IDTransaccionGasto)
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccionGasto)

        txtID.SoloLectura = False
        Cancelar()

    End Sub

    Sub CalcularTotales()

        OcxImpuesto1.CargarValores(CDetalleImpuesto.dt)

    End Sub



    Function InsertarCuotas(ByVal Operacion As CSistema.NUMOperacionesRegistro, ByVal vIDTransaccion As Integer) As Boolean

        InsertarCuotas = True

        For Each oRow As DataRow In dtCuota.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", vIDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", oRow("ID").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Saldo", CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Vencimiento", CSistema.FormatoFechaBaseDatos(oRow("Vencimiento").ToString, True, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cancelado", oRow("Cancelado").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpCuota", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
                Return False
            End If

        Next

    End Function

    Sub CargaValeARendir()

        Dim frm As New frmValeAResindir
        dtVales = CSistema.ExecuteToDataTable("Select *, 'Sel'= 'False','Cancelar'= 'False' from VVale Where ARendir= 'True' And Cancelado= 'False' And Anulado='False'").Copy
        frm.dt = dtVales
        frm.IDGrupo = cbxGrupo.GetValue
        frm.IDSucursal = cbxSucursal.GetValue
        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Aplicacion de Vales", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False, False)

        dtVales = frm.dt

        CargarVale()

    End Sub

    Sub MostrarCajaChica()

        If CajaChica = True Then
            gbxVale.Visible = True
            cbxCondicion.cbx.SelectedIndex = 1
        End If

        If CajaChica = False Then
            gbxVale.Visible = False
        End If

    End Sub

    Sub CargarVale()

        lvVale.Items.Clear()

        Dim Total As Decimal = 0

        For Each oRow As DataRow In dtVales.Rows
            If oRow("Sel") = True Then

                Dim item As ListViewItem = New ListViewItem(oRow("IDTransaccion").ToString)
                item.SubItems.Add(oRow("Fec").ToString)
                item.SubItems.Add(oRow("Motivo").ToString)
                item.SubItems.Add(oRow("Nombre").ToString)
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Total").ToString))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Saldo").ToString))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Importe").ToString))
                Total = Total + CDec(oRow("Importe").ToString)

                lvVale.Items.Add(item)

            End If
        Next

        txtTotalComprobante.SetValue(Total)
        txtCantidadComprobante.SetValue(lvVale.Items.Count)

        'CalcularTotales()

    End Sub

    Sub EliminarVale()

        For Each item As ListViewItem In lvVale.SelectedItems

            For Each oRow As DataRow In dtVales.Select(" IDTransaccion = " & item.Text & " ")
                'CAsientoContableOrdenPago.EliminarVenta(oRow("Importe").ToString, oRow("IDTipoComprobante").ToString, oRow("IDMoneda").ToString, oRow("Credito").ToString)
                oRow("Sel") = False
                Exit For
            Next

        Next

        CargarVale()

    End Sub

    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'Otros

        Dim frm As New frmConsultaGasto
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.CajaChica = CajaChica
        frm.ShowDialog(Me)

        If frm.IDTransaccion > 0 Then
            If CajaChica = True Then
                CargarOperacionConFondoFijo(frm.IDTransaccion)
            Else
                CargarOperacion(frm.IDTransaccion)
            End If
        End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            'IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From GastoTipoComprobante Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )")
            IDTransaccion = SeleccionarRegistro("VGastoTipoComprobante")
            IDTransaccionGasto = SeleccionarRegistro("VGastoTipoComprobante")
        Else
            IDTransaccion = vIDTransaccion
            IDTransaccionGasto = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Listamos todos los grupos para la consulta
        CSistema.SqlToComboBox(cbxGrupo.cbx, "Select IDGrupo as ID, Grupo From vGrupoUsuario GROUP BY IDGrupo, Grupo order by ID ")

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VGastoTipoComprobante Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        txtID.txt.Text = oRow("Numero").ToString
        txtProveedor.SetValue(oRow("IDProveedor").ToString)
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtFecha.SetValue(oRow("Fecha"))
        txtTimbrado.txt.Text = oRow("NroTimbrado").ToString
        cbxGrupo.cbx.Text = oRow("Grupo").ToString
        txtVtoTimbrado.SetValue(oRow("FechaVencimientoTimbrado").ToString)

        'Distribucion
        cbxDeposito.cbx.Text = oRow("Deposito").ToString
        cbxCamion.cbx.Text = oRow("Camion").ToString
        cbxChofer.cbx.Text = oRow("Chofer").ToString

        'Condicion
        If CBool(oRow("CREDITO").ToString) = False Then
            txtVencimiento.txt.Clear()
            cbxCondicion.cbx.SelectedIndex = 0
        Else
            txtVencimiento.SetValue(CDate(oRow("FechaVencimiento").ToString))
            'calcular plazo
            cbxCondicion.cbx.SelectedIndex = 1
        End If

        cbxSucursal.txt.Text = oRow("Sucursal").ToString

        'Cotizacion
        txtCotizacion.SetValue(oRow("Moneda").ToString, oRow("Cotizacion").ToString)
        txtObservacion.txt.Text = oRow("Observacion").ToString

        If CBool(oRow("Directo").ToString) = False Then
            cbxTipoIVA.cbx.SelectedIndex = 1
        Else
            cbxTipoIVA.cbx.SelectedIndex = 0
        End If

        chkIncluirLibro.Valor = oRow("IncluirLibro")

        Try
            chkEfectivo.Valor = oRow("Efectivo").ToString
            chkCheque.Valor = oRow("Cheque")
        Catch ex As Exception
            chkEfectivo.Valor = False
            chkCheque.Valor = True
        End Try

        'Cuota
        If oRow("Cuota") > 0 Then
            chkCuota.Valor = True
            dtCuota = CSistema.ExecuteToDataTable("Select * From VCuota Where IDTransaccion=" & IDTransaccion)
        Else
            chkCuota.Valor = False
        End If

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        'Cargamos el detalle
        OcxImpuesto1.CargarValores(IDTransaccion)

        'Cargamos GastoVale
        dtVales = CSistema.ExecuteToDataTable("Select * ,'Sel'='True' from VGastoVale Where IDTransaccionGasto= " & IDTransaccion).Copy
        CargarVale()

        'Inicializamos el Asiento
        ' CAsiento.Limpiar()

    End Sub

    Sub CargarOperacionConFondoFijo(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            'IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From GastoFondoFijo Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )")
            IDTransaccion = SeleccionarRegistro("VGastoFondoFijo")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VGastoFondoFijo Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnicos."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        txtID.txt.Text = oRow("Numero").ToString
        txtProveedor.SetValue(oRow("IDProveedor").ToString)
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtFecha.SetValue(oRow("Fecha"))
        txtTimbrado.SetValue(oRow("NroTimbrado"))
        'cbxGrupo.SoloLectura = False
        cbxGrupo.cbx.Text = oRow("Grupo").ToString

        'Condicion
        If CBool(oRow("CREDITO").ToString) = False Then
            txtVencimiento.txt.Clear()
            cbxCondicion.cbx.SelectedIndex = 0
        Else
            txtVencimiento.SetValue(CDate(oRow("FechaVencimiento").ToString))
            'calcular plazo
            cbxCondicion.cbx.SelectedIndex = 1
        End If

        cbxSucursal.txt.Text = oRow("Sucursal").ToString

        'Cotizacion
        txtCotizacion.SetValue(oRow("Moneda").ToString, oRow("Cotizacion").ToString)
        txtObservacion.txt.Text = oRow("Observacion").ToString

        If CBool(oRow("Directo").ToString) = False Then
            cbxTipoIVA.cbx.SelectedIndex = 1
        Else
            cbxTipoIVA.cbx.SelectedIndex = 0
        End If

        chkIncluirLibro.Valor = oRow("IncluirLibro")
        chkEfectivo.Valor = oRow("Efectivo")
        chkCheque.Valor = oRow("Cheque")

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        'Cargamos el detalle
        OcxImpuesto1.CargarValores(IDTransaccion)

        'Cargamos GastoVale
        dtVales = CSistema.ExecuteToDataTable("Select * ,'Sel'='True' from VGastoVale Where IDTransaccionGasto= " & IDTransaccion).Copy
        CargarVale()

        'Inicializamos el Asiento
        ' CAsiento.Limpiar()

    End Sub

    Sub LibroCompra()

        Dim dttemp As DataTable = CData.GetTable("VTipoComprobante", "ID=" & cbxTipoComprobante.GetValue)

        If dttemp Is Nothing Then
            Exit Sub
        End If

        If dttemp.Rows.Count = 0 Then
            Exit Sub
        End If

        chkIncluirLibro.Valor = dttemp.Rows(0)("Libro")

    End Sub

    Sub VizualizarCuota()

        Dim frm As New frmCuotasSaldos
        'frm.Total = OcxImpuesto1.Total
        frm.Total = CSistema.ExecuteScalar("Select Isnull(saldo,0) From gasto Where IDTransaccion = " & IDTransaccion)
        frm.FechaInicial = txtFecha.GetValue
        frm.dt = dtCuota
        frm.Cuotas = Cuota
        'If vNuevo = True Then
        '    frm.SoloConsulta = False

        '    Select Case cbxCondicion.cbx.SelectedIndex
        '        Case 0 ' CONTADO
        '            frm.FechaInicial = txtFecha.GetValue
        '        Case 1 ' CREDITO
        '            frm.FechaInicial = txtVencimiento.GetValue
        '        Case Else ' SIN DEFINIR
        '            frm.FechaInicial = txtFecha.GetValue
        '    End Select

        'Else
        '    frm.SoloConsulta = True
        'End If
        frm.SoloConsulta = False

        FGMostrarFormulario(Me, frm, "Cuotas", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        dtCuota = frm.dt
        Cuota = frm.Cuotas
        If Cuota > 0 Then
            btnGuardar.Enabled = True
        Else
            btnGuardar.Enabled = False
        End If
    End Sub

    Private Sub frmGasto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub frmGasto_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmGasto_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Enter Then

            If cbxCondicion.Focused = True Then
                Exit Sub
            End If

            CSistema.SelectNextControl(Me, e.KeyCode)
        End If

    End Sub

    Private Sub txtProveedor_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtProveedor.KeyUp

        If e.KeyCode = Keys.Escape Then
            txtProveedor.LimpiarSeleccion()
            txtProveedor.OcultarLista()
        End If

    End Sub

    Private Sub txtProveedor_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProveedor.Leave
        txtProveedor.OcultarLista()
    End Sub

    Private Sub txtProveedor_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProveedor.ItemSeleccionado
        cbxTipoComprobante.cbx.Focus()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.UPD)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada

        If txtID.SoloLectura = True Then
            Exit Sub
        End If

        ManejarTecla(e)
    End Sub

    Private Sub cbxTipoComprobante_Editar(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxTipoComprobante.Editar

        Dim frm As New frmTipoComprobante
        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        cbxTipoComprobante.cbx.DataSource = Nothing
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=20")

    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub cbxSucursal_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxSucursal.Leave

        If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxSucursal.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged

        If IDTransaccion = 0 Then
            txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        End If

        cbxDeposito.DataSource = Nothing

        If cbxSucursal.GetValue = 0 Then
            Exit Sub
        End If

        Dim dt As DataTable = CData.GetTable("VDeposito", " IDSucursal=" & cbxSucursal.GetValue)
        CSistema.SqlToComboBox(cbxDeposito.cbx, dt, "ID", "Deposito")

    End Sub

    Private Sub cbxTipoComprobante_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoComprobante.Leave
        LibroCompra()
    End Sub

    Private Sub cbxTipoComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoComprobante.PropertyChanged

    End Sub

    Private Sub cbxSucursal_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxSucursal.TeclaPrecionada

    End Sub

    Private Sub txtCotizacion_CambioCotizacion() Handles txtCotizacion.CambioCotizacion

    End Sub

    Private Sub txtCotizacion_CambioMoneda() Handles txtCotizacion.CambioMoneda
        OcxImpuesto1.SetIDMoneda(txtCotizacion.Registro("ID"))
    End Sub

    Private Sub chkCuota_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCuota.PropertyChanged
        btnCuota.Enabled = value
    End Sub

    Private Sub btnCuota_Click(sender As System.Object, e As System.EventArgs) Handles btnCuota.Click
        VizualizarCuota()
    End Sub
End Class
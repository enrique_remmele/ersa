﻿Public Class frmGastosDuplicados

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Public Property IDTransaccion As Integer
    Public Property Procesado As Boolean
    Public Property dt As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Funciones
        Listar()

    End Sub

    Sub Listar()

        'Listar
        CSistema.dtToGrid(dgv, dt)

        'Formato


    End Sub

    Sub Seleccionar()

        If dgv.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        IDTransaccion = dgv.SelectedRows(0).Cells("IDTransaccion").Value
        Procesado = True
        Me.Close()

    End Sub

    Sub Cancelar()

        IDTransaccion = 0
        Procesado = False
        Me.Close()

    End Sub

    Private Sub frmGastosDuplicados_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSeleccionar_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

End Class
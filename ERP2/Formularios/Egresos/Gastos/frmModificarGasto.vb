﻿Imports System.Web.Services.Description

Public Class frmModificarGasto

    Dim Csistema As New CSistema
    Dim CData As New CData
    Private dt As Object
    Public Property Condicion As String
    Public Property Vencimiento As String
    Public Property Plazo As String
    Public Property Observacion As String
    Public Property IDOperacion As Integer
    Public Property IDTransaccion As Integer
    Public Property Timbrado As String
    Public Property VtoTimbrado As String
    Public Property EsFE As Boolean
    Public Property UnidadNegocio As String
    Public Property Departamento As String
    Public Property GastosVarios As Boolean
    Public Property IDSucursal As Integer
    Dim Seccion As Boolean = False

    Sub Inicializar()
        Cargar()
    End Sub

    Sub Cargar()

        cbxCondicion.cbx.Items.Add("Contado")
        cbxCondicion.cbx.Items.Add("Credito")
        cbxCondicion.cbx.Text = Condicion
        txtVencimiento.txt.Text = Vencimiento
        txtObservacion.txt.Text = Observacion
        txtTimbrado.txt.Text = Timbrado
        txtVtoTimbrado.txt.Text = VtoTimbrado

        chkGastosVarios.Checked = GastosVarios
        chkGastosVarios.Enabled = False
        If IDSucursal = 1 Then
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " IDSucursal in (1,4)")
            Csistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")

        Else
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " IDSucursal=" & IDSucursal)
            Csistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
        End If
        Csistema.SqlToComboBox(cbxUnidadNegocio.cbx, CData.GetTable("vUnidadNegocio"), "ID", "Descripcion")
        If GastosVarios = True Then
            cbxUnidadNegocio.cbx.Enabled = False
            cbxUnidadNegocio.cbx.Text = ""
        Else
                cbxUnidadNegocio.cbx.Enabled = True
            cbxUnidadNegocio.cbx.Text = UnidadNegocio
        End If



        cbxDepartamento.cbx.Text = Departamento
        'Csistema.SqlToComboBox(cbxDepartamento.cbx, CData.GetTable("vDepartamentoEmpresa"), "ID", "Descripcion")

        ''ChkFE.Checked = EsFE
    End Sub

    Function ValidarDocumento() As Boolean

        ValidarDocumento = False

        'Unidad de negocio
        If chkGastosVarios.Checked = False Then
            If cbxUnidadNegocio.cbx.Text.Trim = "" Then
                Dim mensaje As String = "Seleccione correctamente la Unidad de Negocio"
                MsgBox(mensaje)
                Exit Function
            End If
        End If

        'Departamento
        If cbxDepartamento.cbx.Text.Trim = "" Then
            Dim mensaje As String = "Seleccione correctamente Departamento"
            MsgBox(mensaje)
            Exit Function
        End If

        If Seccion = True Then
            If cbxSeccion.cbx.Text.Trim = "" Then
                Dim mensaje As String = "Seleccione correctamente la Seccion"
                MsgBox(mensaje)
                Exit Function
            End If

        End If

        Return True

    End Function

    Sub Guardar()

        If ValidarDocumento() = False Then
            Exit Sub
        End If

        Dim Credito As Boolean
        If cbxCondicion.cbx.Text = "Credito" Then
            Credito = True
        Else
            Credito = False
        End If

        Try
            Dim param(-1) As SqlClient.SqlParameter

            Csistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
            If cbxCondicion.cbx.SelectedIndex = 0 Then
                Csistema.SetSQLParameter(param, "@Credito", "False", ParameterDirection.Input)
            Else
                Csistema.SetSQLParameter(param, "@Credito", "True", ParameterDirection.Input)
                Csistema.SetSQLParameter(param, "@FechaVencimiento", Csistema.FormatoFechaBaseDatos(txtVencimiento.txt.Text, True, False), ParameterDirection.Input)

            End If

            Csistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@NroTimbrado", txtTimbrado.txt.Text, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@FechaVencimientoTimbrado", Csistema.FormatoFechaBaseDatos(txtVtoTimbrado.txt.Text, True, False), ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@Operacion", "UPD2", ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDterminal", vgIDTerminal, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
            'Se agrega cuando se carga para diferenciar Factura Electrónica
            If ChkFE.Checked = True Then
                Csistema.SetSQLParameter(param, "@EsFE", ChkFE.Checked, ParameterDirection.Input)
            End If

            If chkGastosVarios.Checked = False Then
                Csistema.SetSQLParameter(param, "@IDUnidadNegocio", cbxUnidadNegocio.cbx.SelectedValue, ParameterDirection.Input)
                Csistema.SetSQLParameter(param, "@IDDepartamentoEmpresa", cbxDepartamento.cbx.SelectedValue, ParameterDirection.Input)
            Else
                Csistema.SetSQLParameter(param, "@IDDepartamentoEmpresa", cbxDepartamento.cbx.SelectedValue, ParameterDirection.Input)
            End If

            If Seccion = True Then
                Csistema.SetSQLParameter(param, "@IDSeccion", cbxSeccion.cbx.SelectedValue, ParameterDirection.Input)
            End If

            Csistema.SetSQLParameter(param, "@IDTransaccionSalida", 0, ParameterDirection.Output)

            'Informacion de Salida
            Csistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            Csistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Aplicar Modificaciones
            If Csistema.ExecuteStoreProcedure(param, "SpGasto", False, False, MensajeRetorno) = False Then
                '12-07-2021 comentado por error en tipos de datos, de igual forma ejecuta el proceso
                'MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

            Me.Close()

        Catch ex As Exception

        End Try
    End Sub


    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()

    End Sub

    Private Sub frmModificarCompra_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub cbxDepartamento_PropertyChanged(sender As Object, e As EventArgs) Handles cbxDepartamento.PropertyChanged
        Seccion = Csistema.ExecuteScalar("Select Seccion From vDepartamentoEmpresa where ID = " & cbxDepartamento.cbx.SelectedValue)
        If Seccion = True Then
            Dim dtSeccion As DataTable = CData.GetTable("vSeccion", " IDDepartamentoEmpresa=" & cbxDepartamento.cbx.SelectedValue)
            Csistema.SqlToComboBox(cbxSeccion.cbx, dtSeccion, "ID", "Descripcion")
            cbxSeccion.cbx.Text = ""
            cbxSeccion.SoloLectura = False
        Else
            cbxSeccion.SoloLectura = True
            cbxSeccion.cbx.Text = ""
        End If
    End Sub
End Class
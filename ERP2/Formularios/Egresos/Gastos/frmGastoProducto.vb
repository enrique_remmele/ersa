﻿Public Class frmGastoProducto
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CAsiento As New CAsientoMovimiento
    Dim CData As New CData

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue

        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private ObservacionValue As String
    Public Property Observacion() As String
        Get
            Return ObservacionValue

        End Get
        Set(ByVal value As String)
            ObservacionValue = value
        End Set
    End Property

    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue

        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property
    Private IDAcuerdoValue As Integer
    Public Property IDAcuerdo() As Integer
        Get
            Return IDAcuerdoValue

        End Get
        Set(ByVal value As Integer)
            IDAcuerdoValue = value
        End Set
    End Property
    Private CantidadValue As Double
    Public Property Cantidad() As Integer
        Get
            Return CantidadValue

        End Get
        Set(ByVal value As Integer)
            CantidadValue = value
        End Set
    End Property

    Private CCaRecibirValue As String
    Public Property CCaRecibir() As String
        Get
            Return CCaRecibirValue

        End Get
        Set(ByVal value As String)
            CCaRecibirValue = value
        End Set
    End Property

    Public Property FechaPago As Date
    Public Property ImporteCuota As Decimal

    'Variables
    Dim dt As DataTable

    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True


        'CargarControles()

    End Sub

    'Sub CargarControles()

    '    dt = CSistema.ExecuteToDataTable("Select * From VGastoProducto Where IDTransaccion=" & IDTransaccion).Copy

    '    'Cargamos
    '    If dt Is Nothing Then
    '        Dim mensaje As String = "Error en la consulta! Problemas tecnicos."
    '        MessageBox.Show(mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '        Exit Sub
    '    End If

    '    Dim oRow As DataRow = dt.Rows(0)

    '    Dim FechaPago As Date = CDate(oRow("FechaPago"))

    '    txtPrecio.txt.Text = oRow("ImporteAPagar").ToString
    '    txtCantidad.txt.Text = oRow("PagosVarios").ToString
    '    txtObservacion.txt.Text = oRow("ObservacionPago").ToString

    '    'Nro Cuota
    '    txtAcuerdo.Texto = NroCuota

    'End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        'Validar
        'Monto pagado
        If IsNumeric(txtPrecio.ObtenerValor) = False Then
            Dim mensaje As String = "El monto pagado no es correcto!"
            ctrError.SetError(txtPrecio, mensaje)
            ctrError.SetIconAlignment(txtPrecio, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Pagos varios
        If IsNumeric(txtCantidad.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Guardamos
        Cantidad = CSistema.FormatoMonedaBaseDatos(txtCantidad.txt.Text, True)
        Observacion = txtObservacion.txt.Text
        'Dim Sql As String = ""
        'Sql = "Insert into GastoProducto values(" & IDTransaccion & "," & vIDAcuerdo & "," & CSistema.FormatoMonedaBaseDatos(txtCantidad.txt.Text, True) & ")"
        'Dim RowCount As Integer = CSistema.ExecuteNonQuery(Sql, VGCadenaConexion)

        'If RowCount = 0 Then
        '    CSistema.MostrarError("Error de sistema!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Sub
        'End If

        Me.Close()

    End Sub

    Sub CargarAcuerdo(Optional ByVal Numero As String = "")
        ctrError.Clear()
        tsslEstado.Text = ""
        Dim dtAcuerdo As DataTable
        If Numero <> "" Then
            txtAcuerdo.SoloLectura = True
            txtCantidad.SoloLectura = True
            txtObservacion.SoloLectura = True
            Dim dtGasto As DataTable
            dtGasto = CSistema.ExecuteToDataTable("Select * From GastoProducto Where IDTransaccionGasto = " & IDTransaccion)
            Dim oRowGasto As DataRow = dtGasto.Rows(0)
            txtCantidad.Texto = oRowGasto("cantidad").ToString
            txtObservacion.Texto = oRowGasto("Observacion").ToString
            txtAcuerdo.Texto = oRowGasto("NroAcuerdo").ToString
            dtGasto.Clear()
            btnGuardar.Visible = False
        Else
            txtAcuerdo.SoloLectura = False
            txtCantidad.SoloLectura = False
            txtObservacion.SoloLectura = False
            btnGuardar.Visible = True
        End If

        If txtAcuerdo.txt.Text = "" Then
            Dim mensaje As String = "Ingrese Nro de Comprobante del Acuerdo!"
            ctrError.SetError(txtAcuerdo, mensaje)
            ctrError.SetIconAlignment(txtAcuerdo, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If
        If Numero <> "" Then
            dtAcuerdo = CSistema.ExecuteToDataTable("Select * From vAcuerdo Where NroAcuerdo = '" & txtAcuerdo.txt.Text & "'")
        Else
            dtAcuerdo = CSistema.ExecuteToDataTable("Select * From vAcuerdo Where NroAcuerdo = '" & txtAcuerdo.txt.Text & "' and IDProveedor = " & IDProveedor)
        End If
        'Cargamos la cabecera
        If (dtAcuerdo Is Nothing) Or (dtAcuerdo.Rows.Count = 0) Then
            Dim mensaje As String = "El acuerdo no pertenece al Proveedor o no existe."
            ctrError.SetError(txtAcuerdo, mensaje)
            ctrError.SetIconAlignment(txtAcuerdo, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim oRow As DataRow = dtAcuerdo.Rows(0)
        txtMoneda.Texto = oRow("Moneda").ToString
        txtPrecio.txt.Text = oRow("Precio").ToString
        txtProducto.Texto = oRow("Producto").ToString
        IDAcuerdo = oRow("NroAcuerdo").ToString
        CCaRecibir = oRow("CCaRecibir").ToString
        dtAcuerdo.Clear()
    End Sub
    Sub CargarOperacion(Optional ByVal Numero As String = "")
        ctrError.Clear()
        tsslEstado.Text = ""
        Dim dtAcuerdo As DataTable
        If Numero <> "" Then
            txtAcuerdo.SoloLectura = True
            txtCantidad.SoloLectura = True
            txtObservacion.SoloLectura = True
            Dim dtGasto As DataTable
            dtGasto = CSistema.ExecuteToDataTable("Select * From GastoProducto Where IDTransaccionGasto = " & IDTransaccion)
            Dim oRowGasto As DataRow = dtGasto.Rows(0)
            txtCantidad.Texto = oRowGasto("cantidad").ToString
            txtObservacion.Texto = oRowGasto("Observacion").ToString
            txtAcuerdo.Texto = oRowGasto("NroAcuerdo").ToString
            dtGasto.Clear()
            btnGuardar.Visible = False
        Else
            txtAcuerdo.SoloLectura = False
            txtCantidad.SoloLectura = False
            txtObservacion.SoloLectura = False
            btnGuardar.Visible = True
        End If

        dtAcuerdo = CSistema.ExecuteToDataTable("Select * From vAcuerdo Where NroAcuerdo = '" & txtAcuerdo.txt.Text & "'")

        Dim oRow As DataRow = dtAcuerdo.Rows(0)
        txtMoneda.Texto = oRow("Moneda").ToString
        txtPrecio.txt.Text = oRow("Precio").ToString
        txtProducto.Texto = oRow("Producto").ToString
        IDAcuerdo = oRow("NroAcuerdo").ToString
        CCaRecibir = oRow("CCaRecibir").ToString
        dtAcuerdo.Clear()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.UPD)
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtAcuerdo_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtAcuerdo.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarAcuerdo()
        End If
    End Sub

    Private Sub frmGastoProducto_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
End Class
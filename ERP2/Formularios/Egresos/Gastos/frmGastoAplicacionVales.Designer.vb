﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGastoAplicacionVales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblRemision = New System.Windows.Forms.Label()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblVencimiento = New System.Windows.Forms.Label()
        Me.lblCondicion = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.colTotal = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colSaldo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colNombre = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colImporte = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colMotivo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lklAgregarVale = New System.Windows.Forms.LinkLabel()
        Me.lklEliminarVale = New System.Windows.Forms.LinkLabel()
        Me.colFecha = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtVtoTimbrado = New ERP.ocxTXTDate()
        Me.txtTimbrado = New ERP.ocxTXTString()
        Me.cbxGrupo = New ERP.ocxCBX()
        Me.cbxRemision = New ERP.ocxCBX()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.txtVencimiento = New ERP.ocxTXTDate()
        Me.cbxCondicion = New ERP.ocxCBX()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.txtCotizacion = New ERP.ocxCotizacion()
        Me.gbxVale = New System.Windows.Forms.GroupBox()
        Me.txtCantidadComprobante = New ERP.ocxTXTNumeric()
        Me.lvVale = New System.Windows.Forms.ListView()
        Me.colIDTransaccion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txtTotalComprobante = New ERP.ocxTXTNumeric()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.txtTotalIva = New ERP.ocxTXTNumeric()
        Me.txtTotalGasto = New ERP.ocxTXTNumeric()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.gbxCabecera.SuspendLayout()
        Me.gbxVale.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(383, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Vto. Timbrado:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(242, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Timbrado:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 70)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Grupo:"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(553, 99)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 24
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblRemision
        '
        Me.lblRemision.AutoSize = True
        Me.lblRemision.Location = New System.Drawing.Point(549, 70)
        Me.lblRemision.Name = "lblRemision"
        Me.lblRemision.Size = New System.Drawing.Size(53, 13)
        Me.lblRemision.TabIndex = 20
        Me.lblRemision.Text = "Remisión:"
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblVencimiento
        '
        Me.lblVencimiento.AutoSize = True
        Me.lblVencimiento.Location = New System.Drawing.Point(615, 43)
        Me.lblVencimiento.Name = "lblVencimiento"
        Me.lblVencimiento.Size = New System.Drawing.Size(38, 13)
        Me.lblVencimiento.TabIndex = 12
        Me.lblVencimiento.Text = "Venc.:"
        '
        'lblCondicion
        '
        Me.lblCondicion.AutoSize = True
        Me.lblCondicion.Location = New System.Drawing.Point(413, 43)
        Me.lblCondicion.Name = "lblCondicion"
        Me.lblCondicion.Size = New System.Drawing.Size(57, 13)
        Me.lblCondicion.TabIndex = 10
        Me.lblCondicion.Text = "Condición:"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(256, 43)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 8
        Me.lblFecha.Text = "Fecha:"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(237, 16)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(59, 13)
        Me.lblProveedor.TabIndex = 3
        Me.lblProveedor.Text = "Proveedor:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(6, 99)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 22
        Me.lblObservacion.Text = "Observación:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(6, 43)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 5
        Me.lblComprobante.Text = "Comprobante:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(6, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operación:"
        '
        'colTotal
        '
        Me.colTotal.Text = "Total"
        Me.colTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colTotal.Width = 75
        '
        'colSaldo
        '
        Me.colSaldo.Text = "Saldo"
        Me.colSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colSaldo.Width = 75
        '
        'colNombre
        '
        Me.colNombre.Text = "Nombre"
        Me.colNombre.Width = 120
        '
        'colImporte
        '
        Me.colImporte.Text = "Importe"
        Me.colImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colImporte.Width = 75
        '
        'colMotivo
        '
        Me.colMotivo.Text = "Motivo"
        Me.colMotivo.Width = 100
        '
        'lklAgregarVale
        '
        Me.lklAgregarVale.AutoSize = True
        Me.lklAgregarVale.Location = New System.Drawing.Point(6, 16)
        Me.lklAgregarVale.Name = "lklAgregarVale"
        Me.lklAgregarVale.Size = New System.Drawing.Size(109, 13)
        Me.lklAgregarVale.TabIndex = 0
        Me.lklAgregarVale.TabStop = True
        Me.lklAgregarVale.Text = "Agregar comprobante"
        '
        'lklEliminarVale
        '
        Me.lklEliminarVale.AutoSize = True
        Me.lklEliminarVale.Location = New System.Drawing.Point(118, 16)
        Me.lklEliminarVale.Name = "lklEliminarVale"
        Me.lklEliminarVale.Size = New System.Drawing.Size(108, 13)
        Me.lklEliminarVale.TabIndex = 1
        Me.lklEliminarVale.TabStop = True
        Me.lklEliminarVale.Text = "Eliminar comprobante"
        '
        'colFecha
        '
        Me.colFecha.Text = "Fecha"
        Me.colFecha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.txtProveedor)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.txtVtoTimbrado)
        Me.gbxCabecera.Controls.Add(Me.Label2)
        Me.gbxCabecera.Controls.Add(Me.Label3)
        Me.gbxCabecera.Controls.Add(Me.txtTimbrado)
        Me.gbxCabecera.Controls.Add(Me.cbxGrupo)
        Me.gbxCabecera.Controls.Add(Me.Label1)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.cbxRemision)
        Me.gbxCabecera.Controls.Add(Me.lblRemision)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.txtVencimiento)
        Me.gbxCabecera.Controls.Add(Me.lblVencimiento)
        Me.gbxCabecera.Controls.Add(Me.cbxCondicion)
        Me.gbxCabecera.Controls.Add(Me.lblCondicion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblProveedor)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtCotizacion)
        Me.gbxCabecera.Location = New System.Drawing.Point(8, 1)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(766, 136)
        Me.gbxCabecera.TabIndex = 20
        Me.gbxCabecera.TabStop = False
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 149
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(297, 9)
        Me.txtProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(455, 27)
        Me.txtProveedor.SoloLectura = True
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 4
        Me.txtProveedor.TabStop = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(75, 12)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(63, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 26
        Me.cbxSucursal.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(139, 12)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(97, 20)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtVtoTimbrado
        '
        Me.txtVtoTimbrado.Color = System.Drawing.Color.Empty
        Me.txtVtoTimbrado.Fecha = New Date(2013, 6, 20, 10, 18, 15, 593)
        Me.txtVtoTimbrado.Location = New System.Drawing.Point(460, 66)
        Me.txtVtoTimbrado.Name = "txtVtoTimbrado"
        Me.txtVtoTimbrado.PermitirNulo = False
        Me.txtVtoTimbrado.Size = New System.Drawing.Size(86, 20)
        Me.txtVtoTimbrado.SoloLectura = True
        Me.txtVtoTimbrado.TabIndex = 19
        Me.txtVtoTimbrado.TabStop = False
        '
        'txtTimbrado
        '
        Me.txtTimbrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTimbrado.Color = System.Drawing.Color.Empty
        Me.txtTimbrado.Indicaciones = Nothing
        Me.txtTimbrado.Location = New System.Drawing.Point(297, 66)
        Me.txtTimbrado.Multilinea = False
        Me.txtTimbrado.Name = "txtTimbrado"
        Me.txtTimbrado.Size = New System.Drawing.Size(80, 21)
        Me.txtTimbrado.SoloLectura = True
        Me.txtTimbrado.TabIndex = 17
        Me.txtTimbrado.TabStop = False
        Me.txtTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTimbrado.Texto = ""
        '
        'cbxGrupo
        '
        Me.cbxGrupo.CampoWhere = Nothing
        Me.cbxGrupo.CargarUnaSolaVez = False
        Me.cbxGrupo.DataDisplayMember = Nothing
        Me.cbxGrupo.DataFilter = Nothing
        Me.cbxGrupo.DataOrderBy = Nothing
        Me.cbxGrupo.DataSource = Nothing
        Me.cbxGrupo.DataValueMember = Nothing
        Me.cbxGrupo.FormABM = Nothing
        Me.cbxGrupo.Indicaciones = Nothing
        Me.cbxGrupo.Location = New System.Drawing.Point(75, 66)
        Me.cbxGrupo.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxGrupo.Name = "cbxGrupo"
        Me.cbxGrupo.SeleccionObligatoria = True
        Me.cbxGrupo.Size = New System.Drawing.Size(161, 21)
        Me.cbxGrupo.SoloLectura = True
        Me.cbxGrupo.TabIndex = 15
        Me.cbxGrupo.TabStop = False
        Me.cbxGrupo.Texto = ""
        '
        'cbxRemision
        '
        Me.cbxRemision.CampoWhere = Nothing
        Me.cbxRemision.CargarUnaSolaVez = False
        Me.cbxRemision.DataDisplayMember = Nothing
        Me.cbxRemision.DataFilter = Nothing
        Me.cbxRemision.DataOrderBy = Nothing
        Me.cbxRemision.DataSource = Nothing
        Me.cbxRemision.DataValueMember = Nothing
        Me.cbxRemision.FormABM = Nothing
        Me.cbxRemision.Indicaciones = Nothing
        Me.cbxRemision.Location = New System.Drawing.Point(608, 66)
        Me.cbxRemision.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxRemision.Name = "cbxRemision"
        Me.cbxRemision.SeleccionObligatoria = True
        Me.cbxRemision.Size = New System.Drawing.Size(144, 21)
        Me.cbxRemision.SoloLectura = True
        Me.cbxRemision.TabIndex = 21
        Me.cbxRemision.TabStop = False
        Me.cbxRemision.Texto = ""
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(76, 95)
        Me.txtObservacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(470, 21)
        Me.txtObservacion.SoloLectura = True
        Me.txtObservacion.TabIndex = 23
        Me.txtObservacion.TabStop = False
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'txtVencimiento
        '
        Me.txtVencimiento.Color = System.Drawing.Color.Empty
        Me.txtVencimiento.Fecha = New Date(2013, 6, 20, 10, 18, 15, 593)
        Me.txtVencimiento.Location = New System.Drawing.Point(653, 40)
        Me.txtVencimiento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtVencimiento.Name = "txtVencimiento"
        Me.txtVencimiento.PermitirNulo = False
        Me.txtVencimiento.Size = New System.Drawing.Size(99, 20)
        Me.txtVencimiento.SoloLectura = True
        Me.txtVencimiento.TabIndex = 13
        Me.txtVencimiento.TabStop = False
        '
        'cbxCondicion
        '
        Me.cbxCondicion.CampoWhere = Nothing
        Me.cbxCondicion.CargarUnaSolaVez = False
        Me.cbxCondicion.DataDisplayMember = Nothing
        Me.cbxCondicion.DataFilter = Nothing
        Me.cbxCondicion.DataOrderBy = Nothing
        Me.cbxCondicion.DataSource = Nothing
        Me.cbxCondicion.DataValueMember = Nothing
        Me.cbxCondicion.FormABM = Nothing
        Me.cbxCondicion.Indicaciones = Nothing
        Me.cbxCondicion.Location = New System.Drawing.Point(470, 39)
        Me.cbxCondicion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.SeleccionObligatoria = True
        Me.cbxCondicion.Size = New System.Drawing.Size(145, 21)
        Me.cbxCondicion.SoloLectura = True
        Me.cbxCondicion.TabIndex = 11
        Me.cbxCondicion.TabStop = False
        Me.cbxCondicion.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 6, 20, 10, 18, 15, 593)
        Me.txtFecha.Location = New System.Drawing.Point(296, 40)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(117, 20)
        Me.txtFecha.SoloLectura = True
        Me.txtFecha.TabIndex = 9
        Me.txtFecha.TabStop = False
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(76, 39)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(63, 21)
        Me.cbxTipoComprobante.SoloLectura = True
        Me.cbxTipoComprobante.TabIndex = 6
        Me.cbxTipoComprobante.TabStop = False
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(139, 39)
        Me.txtComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(97, 21)
        Me.txtComprobante.SoloLectura = True
        Me.txtComprobante.TabIndex = 7
        Me.txtComprobante.TabStop = False
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'txtCotizacion
        '
        Me.txtCotizacion.dt = Nothing
        Me.txtCotizacion.Location = New System.Drawing.Point(608, 89)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Registro = Nothing
        Me.txtCotizacion.Saltar = False
        Me.txtCotizacion.Seleccionado = True
        Me.txtCotizacion.Size = New System.Drawing.Size(144, 32)
        Me.txtCotizacion.SoloLectura = True
        Me.txtCotizacion.TabIndex = 25
        '
        'gbxVale
        '
        Me.gbxVale.Controls.Add(Me.Label6)
        Me.gbxVale.Controls.Add(Me.txtTotalGasto)
        Me.gbxVale.Controls.Add(Me.txtCantidadComprobante)
        Me.gbxVale.Controls.Add(Me.Label5)
        Me.gbxVale.Controls.Add(Me.lvVale)
        Me.gbxVale.Controls.Add(Me.txtTotalIva)
        Me.gbxVale.Controls.Add(Me.Label4)
        Me.gbxVale.Controls.Add(Me.txtTotalComprobante)
        Me.gbxVale.Controls.Add(Me.lklAgregarVale)
        Me.gbxVale.Controls.Add(Me.lklEliminarVale)
        Me.gbxVale.Location = New System.Drawing.Point(8, 139)
        Me.gbxVale.Name = "gbxVale"
        Me.gbxVale.Size = New System.Drawing.Size(766, 159)
        Me.gbxVale.TabIndex = 28
        Me.gbxVale.TabStop = False
        '
        'txtCantidadComprobante
        '
        Me.txtCantidadComprobante.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobante.Decimales = True
        Me.txtCantidadComprobante.Indicaciones = Nothing
        Me.txtCantidadComprobante.Location = New System.Drawing.Point(446, 129)
        Me.txtCantidadComprobante.Name = "txtCantidadComprobante"
        Me.txtCantidadComprobante.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadComprobante.SoloLectura = True
        Me.txtCantidadComprobante.TabIndex = 4
        Me.txtCantidadComprobante.TabStop = False
        Me.txtCantidadComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobante.Texto = "0"
        '
        'lvVale
        '
        Me.lvVale.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colIDTransaccion, Me.colFecha, Me.colMotivo, Me.colNombre, Me.colTotal, Me.colSaldo, Me.colImporte})
        Me.lvVale.FullRowSelect = True
        Me.lvVale.GridLines = True
        Me.lvVale.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvVale.HideSelection = False
        Me.lvVale.Location = New System.Drawing.Point(3, 32)
        Me.lvVale.MultiSelect = False
        Me.lvVale.Name = "lvVale"
        Me.lvVale.Size = New System.Drawing.Size(631, 92)
        Me.lvVale.TabIndex = 2
        Me.lvVale.UseCompatibleStateImageBehavior = False
        Me.lvVale.View = System.Windows.Forms.View.Details
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.Text = "IDTransaccion"
        Me.colIDTransaccion.Width = 0
        '
        'txtTotalComprobante
        '
        Me.txtTotalComprobante.Color = System.Drawing.Color.Empty
        Me.txtTotalComprobante.Decimales = True
        Me.txtTotalComprobante.Indicaciones = Nothing
        Me.txtTotalComprobante.Location = New System.Drawing.Point(354, 129)
        Me.txtTotalComprobante.Name = "txtTotalComprobante"
        Me.txtTotalComprobante.Size = New System.Drawing.Size(86, 22)
        Me.txtTotalComprobante.SoloLectura = True
        Me.txtTotalComprobante.TabIndex = 3
        Me.txtTotalComprobante.TabStop = False
        Me.txtTotalComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalComprobante.Texto = "0"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(8, 304)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(456, 20)
        Me.flpRegistradoPor.TabIndex = 29
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 371)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(787, 22)
        Me.StatusStrip1.TabIndex = 39
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(343, 338)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 34
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(688, 338)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 38
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(586, 338)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 37
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(14, 338)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(126, 23)
        Me.btnBusquedaAvanzada.TabIndex = 32
        Me.btnBusquedaAvanzada.Text = "&Busqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(505, 338)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 36
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(424, 338)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 35
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'txtTotalIva
        '
        Me.txtTotalIva.Color = System.Drawing.Color.Empty
        Me.txtTotalIva.Decimales = True
        Me.txtTotalIva.Indicaciones = Nothing
        Me.txtTotalIva.Location = New System.Drawing.Point(643, 48)
        Me.txtTotalIva.Name = "txtTotalIva"
        Me.txtTotalIva.Size = New System.Drawing.Size(104, 22)
        Me.txtTotalIva.SoloLectura = True
        Me.txtTotalIva.TabIndex = 5
        Me.txtTotalIva.TabStop = False
        Me.txtTotalIva.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalIva.Texto = "0"
        '
        'txtTotalGasto
        '
        Me.txtTotalGasto.Color = System.Drawing.Color.Empty
        Me.txtTotalGasto.Decimales = True
        Me.txtTotalGasto.Indicaciones = Nothing
        Me.txtTotalGasto.Location = New System.Drawing.Point(643, 101)
        Me.txtTotalGasto.Name = "txtTotalGasto"
        Me.txtTotalGasto.Size = New System.Drawing.Size(104, 22)
        Me.txtTotalGasto.SoloLectura = True
        Me.txtTotalGasto.TabIndex = 27
        Me.txtTotalGasto.TabStop = False
        Me.txtTotalGasto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalGasto.Texto = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(640, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(89, 16)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "TOTAL IVA:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(640, 82)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(107, 16)
        Me.Label5.TabIndex = 40
        Me.Label5.Text = "TOTAL NETO:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(238, 132)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(113, 16)
        Me.Label6.TabIndex = 41
        Me.Label6.Text = "TOTAL VALES:"
        '
        'frmGastoAplicacionVales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(787, 393)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Controls.Add(Me.gbxVale)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnModificar)
        Me.Name = "frmGastoAplicacionVales"
        Me.Tag = "frmGastoAplicacionVales"
        Me.Text = "frmGastoAplicacionVales"
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.gbxVale.ResumeLayout(False)
        Me.gbxVale.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents txtVtoTimbrado As ERP.ocxTXTDate
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtTimbrado As ERP.ocxTXTString
    Friend WithEvents cbxGrupo As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents cbxRemision As ERP.ocxCBX
    Friend WithEvents lblRemision As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents txtVencimiento As ERP.ocxTXTDate
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblVencimiento As System.Windows.Forms.Label
    Friend WithEvents cbxCondicion As ERP.ocxCBX
    Friend WithEvents lblCondicion As System.Windows.Forms.Label
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As ERP.ocxCotizacion
    Friend WithEvents colTotal As System.Windows.Forms.ColumnHeader
    Friend WithEvents colSaldo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colNombre As System.Windows.Forms.ColumnHeader
    Friend WithEvents colImporte As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtTotalComprobante As ERP.ocxTXTNumeric
    Friend WithEvents colMotivo As System.Windows.Forms.ColumnHeader
    Friend WithEvents lklAgregarVale As System.Windows.Forms.LinkLabel
    Friend WithEvents lklEliminarVale As System.Windows.Forms.LinkLabel
    Friend WithEvents colFecha As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents txtProveedor As ERP.ocxTXTProveedor
    Friend WithEvents gbxVale As System.Windows.Forms.GroupBox
    Friend WithEvents txtCantidadComprobante As ERP.ocxTXTNumeric
    Friend WithEvents lvVale As System.Windows.Forms.ListView
    Friend WithEvents colIDTransaccion As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents txtTotalGasto As ERP.ocxTXTNumeric
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtTotalIva As ERP.ocxTXTNumeric
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
End Class

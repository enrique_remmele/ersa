﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmModificarGasto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblCondicion = New System.Windows.Forms.Label()
        Me.lblVencimiento = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxCondicion = New ERP.ocxCBX()
        Me.txtVencimiento = New ERP.ocxTXTDate()
        Me.txtVtoTimbrado = New ERP.ocxTXTDate()
        Me.txtTimbrado = New ERP.ocxTXTString()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ChkFE = New System.Windows.Forms.CheckBox()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbxDepartamento = New ERP.ocxCBX()
        Me.chkGastosVarios = New System.Windows.Forms.CheckBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cbxSeccion = New ERP.ocxCBX()
        Me.SuspendLayout()
        '
        'lblCondicion
        '
        Me.lblCondicion.AutoSize = True
        Me.lblCondicion.Location = New System.Drawing.Point(24, 38)
        Me.lblCondicion.Name = "lblCondicion"
        Me.lblCondicion.Size = New System.Drawing.Size(60, 13)
        Me.lblCondicion.TabIndex = 0
        Me.lblCondicion.Text = "Condición :"
        '
        'lblVencimiento
        '
        Me.lblVencimiento.AutoSize = True
        Me.lblVencimiento.Location = New System.Drawing.Point(199, 38)
        Me.lblVencimiento.Name = "lblVencimiento"
        Me.lblVencimiento.Size = New System.Drawing.Size(71, 13)
        Me.lblVencimiento.TabIndex = 1
        Me.lblVencimiento.Text = "Vencimiento :"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(11, 112)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(73, 13)
        Me.lblObservacion.TabIndex = 3
        Me.lblObservacion.Text = "Observación :"
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(335, 292)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 8
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(423, 292)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 10
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(97, 112)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(440, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 12
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxCondicion
        '
        Me.cbxCondicion.CampoWhere = Nothing
        Me.cbxCondicion.CargarUnaSolaVez = False
        Me.cbxCondicion.DataDisplayMember = Nothing
        Me.cbxCondicion.DataFilter = Nothing
        Me.cbxCondicion.DataOrderBy = Nothing
        Me.cbxCondicion.DataSource = Nothing
        Me.cbxCondicion.DataValueMember = Nothing
        Me.cbxCondicion.dtSeleccionado = Nothing
        Me.cbxCondicion.FormABM = Nothing
        Me.cbxCondicion.Indicaciones = Nothing
        Me.cbxCondicion.Location = New System.Drawing.Point(97, 38)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.SeleccionMultiple = False
        Me.cbxCondicion.SeleccionObligatoria = False
        Me.cbxCondicion.Size = New System.Drawing.Size(88, 21)
        Me.cbxCondicion.SoloLectura = False
        Me.cbxCondicion.TabIndex = 11
        Me.cbxCondicion.Texto = ""
        '
        'txtVencimiento
        '
        Me.txtVencimiento.AñoFecha = 0
        Me.txtVencimiento.Color = System.Drawing.Color.Empty
        Me.txtVencimiento.Fecha = New Date(CType(0, Long))
        Me.txtVencimiento.Location = New System.Drawing.Point(276, 38)
        Me.txtVencimiento.MesFecha = 0
        Me.txtVencimiento.Name = "txtVencimiento"
        Me.txtVencimiento.PermitirNulo = True
        Me.txtVencimiento.Size = New System.Drawing.Size(84, 21)
        Me.txtVencimiento.SoloLectura = False
        Me.txtVencimiento.TabIndex = 9
        '
        'txtVtoTimbrado
        '
        Me.txtVtoTimbrado.AñoFecha = 0
        Me.txtVtoTimbrado.Color = System.Drawing.Color.Empty
        Me.txtVtoTimbrado.Fecha = New Date(CType(0, Long))
        Me.txtVtoTimbrado.Location = New System.Drawing.Point(300, 75)
        Me.txtVtoTimbrado.MesFecha = 0
        Me.txtVtoTimbrado.Name = "txtVtoTimbrado"
        Me.txtVtoTimbrado.PermitirNulo = True
        Me.txtVtoTimbrado.Size = New System.Drawing.Size(84, 21)
        Me.txtVtoTimbrado.SoloLectura = False
        Me.txtVtoTimbrado.TabIndex = 20
        '
        'txtTimbrado
        '
        Me.txtTimbrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTimbrado.Color = System.Drawing.Color.Empty
        Me.txtTimbrado.Indicaciones = Nothing
        Me.txtTimbrado.Location = New System.Drawing.Point(97, 74)
        Me.txtTimbrado.Multilinea = False
        Me.txtTimbrado.Name = "txtTimbrado"
        Me.txtTimbrado.Size = New System.Drawing.Size(93, 22)
        Me.txtTimbrado.SoloLectura = False
        Me.txtTimbrado.TabIndex = 19
        Me.txtTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTimbrado.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(27, 79)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 13)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Timbrado :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(207, 79)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Vto Timbrado :"
        '
        'ChkFE
        '
        Me.ChkFE.AutoSize = True
        Me.ChkFE.Location = New System.Drawing.Point(366, 42)
        Me.ChkFE.Name = "ChkFE"
        Me.ChkFE.Size = New System.Drawing.Size(182, 17)
        Me.ChkFE.TabIndex = 23
        Me.ChkFE.Text = "Es Fact Electrónica / Fact Virtual"
        Me.ChkFE.UseVisualStyleBackColor = True
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = Nothing
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = Nothing
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = Nothing
        Me.cbxUnidadNegocio.DataSource = Nothing
        Me.cbxUnidadNegocio.DataValueMember = Nothing
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(97, 162)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(263, 21)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 24
        Me.cbxUnidadNegocio.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(5, 170)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(79, 13)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "U. de Negocio:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 205)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 13)
        Me.Label4.TabIndex = 27
        Me.Label4.Text = "Departamento:"
        '
        'cbxDepartamento
        '
        Me.cbxDepartamento.CampoWhere = Nothing
        Me.cbxDepartamento.CargarUnaSolaVez = False
        Me.cbxDepartamento.DataDisplayMember = Nothing
        Me.cbxDepartamento.DataFilter = Nothing
        Me.cbxDepartamento.DataOrderBy = Nothing
        Me.cbxDepartamento.DataSource = Nothing
        Me.cbxDepartamento.DataValueMember = Nothing
        Me.cbxDepartamento.dtSeleccionado = Nothing
        Me.cbxDepartamento.FormABM = Nothing
        Me.cbxDepartamento.Indicaciones = Nothing
        Me.cbxDepartamento.Location = New System.Drawing.Point(97, 197)
        Me.cbxDepartamento.Name = "cbxDepartamento"
        Me.cbxDepartamento.SeleccionMultiple = False
        Me.cbxDepartamento.SeleccionObligatoria = False
        Me.cbxDepartamento.Size = New System.Drawing.Size(263, 21)
        Me.cbxDepartamento.SoloLectura = False
        Me.cbxDepartamento.TabIndex = 26
        Me.cbxDepartamento.Texto = ""
        '
        'chkGastosVarios
        '
        Me.chkGastosVarios.AutoSize = True
        Me.chkGastosVarios.Location = New System.Drawing.Point(97, 139)
        Me.chkGastosVarios.Name = "chkGastosVarios"
        Me.chkGastosVarios.Size = New System.Drawing.Size(208, 17)
        Me.chkGastosVarios.TabIndex = 28
        Me.chkGastosVarios.Text = "Asignacion de Unidad y Departamento"
        Me.chkGastosVarios.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(35, 240)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 13)
        Me.Label5.TabIndex = 30
        Me.Label5.Text = "Seccion:"
        '
        'cbxSeccion
        '
        Me.cbxSeccion.CampoWhere = Nothing
        Me.cbxSeccion.CargarUnaSolaVez = False
        Me.cbxSeccion.DataDisplayMember = Nothing
        Me.cbxSeccion.DataFilter = Nothing
        Me.cbxSeccion.DataOrderBy = Nothing
        Me.cbxSeccion.DataSource = Nothing
        Me.cbxSeccion.DataValueMember = Nothing
        Me.cbxSeccion.dtSeleccionado = Nothing
        Me.cbxSeccion.FormABM = Nothing
        Me.cbxSeccion.Indicaciones = Nothing
        Me.cbxSeccion.Location = New System.Drawing.Point(95, 232)
        Me.cbxSeccion.Name = "cbxSeccion"
        Me.cbxSeccion.SeleccionMultiple = False
        Me.cbxSeccion.SeleccionObligatoria = False
        Me.cbxSeccion.Size = New System.Drawing.Size(263, 21)
        Me.cbxSeccion.SoloLectura = False
        Me.cbxSeccion.TabIndex = 29
        Me.cbxSeccion.Texto = ""
        '
        'frmModificarGasto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(552, 323)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cbxSeccion)
        Me.Controls.Add(Me.chkGastosVarios)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cbxDepartamento)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cbxUnidadNegocio)
        Me.Controls.Add(Me.ChkFE)
        Me.Controls.Add(Me.txtVtoTimbrado)
        Me.Controls.Add(Me.txtTimbrado)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtObservacion)
        Me.Controls.Add(Me.cbxCondicion)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.txtVencimiento)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.lblObservacion)
        Me.Controls.Add(Me.lblVencimiento)
        Me.Controls.Add(Me.lblCondicion)
        Me.Name = "frmModificarGasto"
        Me.Text = "frmModificarCompra"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblCondicion As Label
    Friend WithEvents lblVencimiento As Label
    Friend WithEvents lblObservacion As Label
    Friend WithEvents btnGuardar As Button
    Friend WithEvents txtVencimiento As ocxTXTDate
    Friend WithEvents btnSalir As Button
    Friend WithEvents cbxCondicion As ocxCBX
    Friend WithEvents txtObservacion As ocxTXTString
    Friend WithEvents txtVtoTimbrado As ocxTXTDate
    Friend WithEvents txtTimbrado As ocxTXTString
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents ChkFE As CheckBox
    Friend WithEvents cbxUnidadNegocio As ocxCBX
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents cbxDepartamento As ocxCBX
    Friend WithEvents chkGastosVarios As CheckBox
    Friend WithEvents Label5 As Label
    Friend WithEvents cbxSeccion As ocxCBX
End Class

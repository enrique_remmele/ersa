﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGenerarCuotasGastos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblFormaPago = New System.Windows.Forms.Label()
        Me.gbxVale = New System.Windows.Forms.GroupBox()
        Me.lvVale = New System.Windows.Forms.ListView()
        Me.colIDTransaccion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colFecha = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colMotivo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colNombre = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colTotal = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colSaldo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colImporte = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lklAgregarVale = New System.Windows.Forms.LinkLabel()
        Me.lklEliminarVale = New System.Windows.Forms.LinkLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.lblTipoIVA = New System.Windows.Forms.Label()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.pnlDistribucion = New System.Windows.Forms.Panel()
        Me.lblDistribucion = New System.Windows.Forms.Label()
        Me.lblDeposito = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblRemision = New System.Windows.Forms.Label()
        Me.lblVencimiento = New System.Windows.Forms.Label()
        Me.lblCondicion = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.btnCuota = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.txtCantidadComprobante = New ERP.ocxTXTNumeric()
        Me.txtTotalComprobante = New ERP.ocxTXTNumeric()
        Me.chkCuota = New ERP.ocxCHK()
        Me.chkEfectivo = New ERP.ocxCHK()
        Me.chkCheque = New ERP.ocxCHK()
        Me.OcxImpuesto1 = New ERP.ocxImpuesto()
        Me.cbxTipoIVA = New ERP.ocxCBX()
        Me.cbxCamion = New ERP.ocxCBX()
        Me.cbxChofer = New ERP.ocxCBX()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.txtVtoTimbrado = New ERP.ocxTXTDate()
        Me.txtTimbrado = New ERP.ocxTXTString()
        Me.cbxGrupo = New ERP.ocxCBX()
        Me.cbxRemision = New ERP.ocxCBX()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtVencimiento = New ERP.ocxTXTDate()
        Me.cbxCondicion = New ERP.ocxCBX()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtCotizacion = New ERP.ocxCotizacion()
        Me.chkIncluirLibro = New ERP.ocxCHK()
        Me.flpRegistradoPor.SuspendLayout()
        Me.gbxVale.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxCabecera.SuspendLayout()
        Me.pnlDistribucion.SuspendLayout()
        Me.SuspendLayout()
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(340, 345)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(451, 20)
        Me.flpRegistradoPor.TabIndex = 29
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'lblFormaPago
        '
        Me.lblFormaPago.AutoSize = True
        Me.lblFormaPago.Enabled = False
        Me.lblFormaPago.Location = New System.Drawing.Point(176, 173)
        Me.lblFormaPago.Name = "lblFormaPago"
        Me.lblFormaPago.Size = New System.Drawing.Size(82, 13)
        Me.lblFormaPago.TabIndex = 24
        Me.lblFormaPago.Text = "Forma de Pago:"
        '
        'gbxVale
        '
        Me.gbxVale.Controls.Add(Me.txtCantidadComprobante)
        Me.gbxVale.Controls.Add(Me.lvVale)
        Me.gbxVale.Controls.Add(Me.txtTotalComprobante)
        Me.gbxVale.Controls.Add(Me.lklAgregarVale)
        Me.gbxVale.Controls.Add(Me.lklEliminarVale)
        Me.gbxVale.Location = New System.Drawing.Point(28, 206)
        Me.gbxVale.Name = "gbxVale"
        Me.gbxVale.Size = New System.Drawing.Size(305, 159)
        Me.gbxVale.TabIndex = 28
        Me.gbxVale.TabStop = False
        '
        'lvVale
        '
        Me.lvVale.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colIDTransaccion, Me.colFecha, Me.colMotivo, Me.colNombre, Me.colTotal, Me.colSaldo, Me.colImporte})
        Me.lvVale.Enabled = False
        Me.lvVale.FullRowSelect = True
        Me.lvVale.GridLines = True
        Me.lvVale.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvVale.HideSelection = False
        Me.lvVale.Location = New System.Drawing.Point(0, 35)
        Me.lvVale.MultiSelect = False
        Me.lvVale.Name = "lvVale"
        Me.lvVale.Size = New System.Drawing.Size(296, 92)
        Me.lvVale.TabIndex = 2
        Me.lvVale.UseCompatibleStateImageBehavior = False
        Me.lvVale.View = System.Windows.Forms.View.Details
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.Text = "IDTransaccion"
        Me.colIDTransaccion.Width = 0
        '
        'colFecha
        '
        Me.colFecha.Text = "Fecha"
        Me.colFecha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.colFecha.Width = 53
        '
        'colMotivo
        '
        Me.colMotivo.Text = "Motivo"
        Me.colMotivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colMotivo.Width = 0
        '
        'colNombre
        '
        Me.colNombre.Text = "Nombre"
        Me.colNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colNombre.Width = 74
        '
        'colTotal
        '
        Me.colTotal.Text = "Total"
        Me.colTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colTotal.Width = 55
        '
        'colSaldo
        '
        Me.colSaldo.Text = "Saldo"
        Me.colSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colSaldo.Width = 55
        '
        'colImporte
        '
        Me.colImporte.Text = "Importe"
        Me.colImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colImporte.Width = 55
        '
        'lklAgregarVale
        '
        Me.lklAgregarVale.AutoSize = True
        Me.lklAgregarVale.Enabled = False
        Me.lklAgregarVale.Location = New System.Drawing.Point(6, 16)
        Me.lklAgregarVale.Name = "lklAgregarVale"
        Me.lklAgregarVale.Size = New System.Drawing.Size(109, 13)
        Me.lklAgregarVale.TabIndex = 0
        Me.lklAgregarVale.TabStop = True
        Me.lklAgregarVale.Text = "Agregar comprobante"
        '
        'lklEliminarVale
        '
        Me.lklEliminarVale.AutoSize = True
        Me.lklEliminarVale.Enabled = False
        Me.lklEliminarVale.Location = New System.Drawing.Point(118, 16)
        Me.lklEliminarVale.Name = "lklEliminarVale"
        Me.lklEliminarVale.Size = New System.Drawing.Size(108, 13)
        Me.lklEliminarVale.TabIndex = 1
        Me.lklEliminarVale.TabStop = True
        Me.lklEliminarVale.Text = "Eliminar comprobante"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 437)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(854, 22)
        Me.StatusStrip1.TabIndex = 39
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblTipoIVA
        '
        Me.lblTipoIVA.AutoSize = True
        Me.lblTipoIVA.Enabled = False
        Me.lblTipoIVA.Location = New System.Drawing.Point(31, 173)
        Me.lblTipoIVA.Name = "lblTipoIVA"
        Me.lblTipoIVA.Size = New System.Drawing.Size(55, 13)
        Me.lblTipoIVA.TabIndex = 21
        Me.lblTipoIVA.Text = "T. de Imp:"
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(340, 389)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 34
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        Me.btnAsiento.Visible = False
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(103, 389)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(126, 23)
        Me.btnBusquedaAvanzada.TabIndex = 32
        Me.btnBusquedaAvanzada.Text = "&Búsqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(708, 389)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 38
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(22, 389)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 31
        Me.btnAnular.Text = "E&liminar"
        Me.btnAnular.UseVisualStyleBackColor = True
        Me.btnAnular.Visible = False
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(502, 389)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 36
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(421, 389)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 35
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        Me.btnNuevo.Visible = False
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.pnlDistribucion)
        Me.gbxCabecera.Controls.Add(Me.cbxDeposito)
        Me.gbxCabecera.Controls.Add(Me.lblDeposito)
        Me.gbxCabecera.Controls.Add(Me.txtProveedor)
        Me.gbxCabecera.Controls.Add(Me.txtVtoTimbrado)
        Me.gbxCabecera.Controls.Add(Me.Label2)
        Me.gbxCabecera.Controls.Add(Me.Label3)
        Me.gbxCabecera.Controls.Add(Me.txtTimbrado)
        Me.gbxCabecera.Controls.Add(Me.cbxGrupo)
        Me.gbxCabecera.Controls.Add(Me.Label1)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.cbxRemision)
        Me.gbxCabecera.Controls.Add(Me.lblRemision)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtVencimiento)
        Me.gbxCabecera.Controls.Add(Me.lblVencimiento)
        Me.gbxCabecera.Controls.Add(Me.cbxCondicion)
        Me.gbxCabecera.Controls.Add(Me.lblCondicion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblProveedor)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtCotizacion)
        Me.gbxCabecera.Location = New System.Drawing.Point(21, 6)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(771, 159)
        Me.gbxCabecera.TabIndex = 20
        Me.gbxCabecera.TabStop = False
        '
        'pnlDistribucion
        '
        Me.pnlDistribucion.Controls.Add(Me.lblDistribucion)
        Me.pnlDistribucion.Controls.Add(Me.cbxCamion)
        Me.pnlDistribucion.Controls.Add(Me.cbxChofer)
        Me.pnlDistribucion.Location = New System.Drawing.Point(242, 120)
        Me.pnlDistribucion.Name = "pnlDistribucion"
        Me.pnlDistribucion.Size = New System.Drawing.Size(512, 29)
        Me.pnlDistribucion.TabIndex = 31
        '
        'lblDistribucion
        '
        Me.lblDistribucion.AutoSize = True
        Me.lblDistribucion.Location = New System.Drawing.Point(3, 7)
        Me.lblDistribucion.Name = "lblDistribucion"
        Me.lblDistribucion.Size = New System.Drawing.Size(87, 13)
        Me.lblDistribucion.TabIndex = 0
        Me.lblDistribucion.Text = "Camion / Chofer:"
        '
        'lblDeposito
        '
        Me.lblDeposito.AutoSize = True
        Me.lblDeposito.Location = New System.Drawing.Point(20, 128)
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(52, 13)
        Me.lblDeposito.TabIndex = 29
        Me.lblDeposito.Text = "Deposito:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(385, 71)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "Vto. Timbrado:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(244, 71)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Timbrado:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(33, 71)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Grupo:"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(555, 100)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 27
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblRemision
        '
        Me.lblRemision.AutoSize = True
        Me.lblRemision.Location = New System.Drawing.Point(551, 71)
        Me.lblRemision.Name = "lblRemision"
        Me.lblRemision.Size = New System.Drawing.Size(53, 13)
        Me.lblRemision.TabIndex = 23
        Me.lblRemision.Text = "Remisión:"
        '
        'lblVencimiento
        '
        Me.lblVencimiento.AutoSize = True
        Me.lblVencimiento.Location = New System.Drawing.Point(617, 44)
        Me.lblVencimiento.Name = "lblVencimiento"
        Me.lblVencimiento.Size = New System.Drawing.Size(38, 13)
        Me.lblVencimiento.TabIndex = 15
        Me.lblVencimiento.Text = "Venc.:"
        '
        'lblCondicion
        '
        Me.lblCondicion.AutoSize = True
        Me.lblCondicion.Location = New System.Drawing.Point(415, 44)
        Me.lblCondicion.Name = "lblCondicion"
        Me.lblCondicion.Size = New System.Drawing.Size(57, 13)
        Me.lblCondicion.TabIndex = 13
        Me.lblCondicion.Text = "Condición:"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(258, 44)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 11
        Me.lblFecha.Text = "Fecha:"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(239, 17)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(59, 13)
        Me.lblProveedor.TabIndex = 6
        Me.lblProveedor.Text = "Proveedor:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(40, 100)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(32, 13)
        Me.lblObservacion.TabIndex = 25
        Me.lblObservacion.Text = "Obs.:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(23, 44)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(49, 13)
        Me.lblComprobante.TabIndex = 8
        Me.lblComprobante.Text = "Compro.:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(13, 17)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 3
        Me.lblOperacion.Text = "Operación:"
        '
        'btnCuota
        '
        Me.btnCuota.Enabled = False
        Me.btnCuota.Location = New System.Drawing.Point(235, 389)
        Me.btnCuota.Name = "btnCuota"
        Me.btnCuota.Size = New System.Drawing.Size(75, 23)
        Me.btnCuota.TabIndex = 33
        Me.btnCuota.Text = "C&uota"
        Me.btnCuota.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(583, 389)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 37
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'txtCantidadComprobante
        '
        Me.txtCantidadComprobante.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobante.Decimales = True
        Me.txtCantidadComprobante.Indicaciones = Nothing
        Me.txtCantidadComprobante.Location = New System.Drawing.Point(251, 132)
        Me.txtCantidadComprobante.Name = "txtCantidadComprobante"
        Me.txtCantidadComprobante.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadComprobante.SoloLectura = True
        Me.txtCantidadComprobante.TabIndex = 4
        Me.txtCantidadComprobante.TabStop = False
        Me.txtCantidadComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobante.Texto = "0"
        '
        'txtTotalComprobante
        '
        Me.txtTotalComprobante.Color = System.Drawing.Color.Empty
        Me.txtTotalComprobante.Decimales = True
        Me.txtTotalComprobante.Indicaciones = Nothing
        Me.txtTotalComprobante.Location = New System.Drawing.Point(159, 133)
        Me.txtTotalComprobante.Name = "txtTotalComprobante"
        Me.txtTotalComprobante.Size = New System.Drawing.Size(86, 22)
        Me.txtTotalComprobante.SoloLectura = True
        Me.txtTotalComprobante.TabIndex = 3
        Me.txtTotalComprobante.TabStop = False
        Me.txtTotalComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalComprobante.Texto = "0"
        '
        'chkCuota
        '
        Me.chkCuota.BackColor = System.Drawing.SystemColors.Control
        Me.chkCuota.Color = System.Drawing.SystemColors.Control
        Me.chkCuota.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCuota.Location = New System.Drawing.Point(26, 366)
        Me.chkCuota.Name = "chkCuota"
        Me.chkCuota.Size = New System.Drawing.Size(211, 20)
        Me.chkCuota.SoloLectura = False
        Me.chkCuota.TabIndex = 30
        Me.chkCuota.Texto = "Comprobante a pagar en cuotas"
        Me.chkCuota.Valor = False
        '
        'chkEfectivo
        '
        Me.chkEfectivo.BackColor = System.Drawing.SystemColors.Control
        Me.chkEfectivo.Color = System.Drawing.SystemColors.Control
        Me.chkEfectivo.Enabled = False
        Me.chkEfectivo.Location = New System.Drawing.Point(258, 191)
        Me.chkEfectivo.Name = "chkEfectivo"
        Me.chkEfectivo.Size = New System.Drawing.Size(81, 20)
        Me.chkEfectivo.SoloLectura = False
        Me.chkEfectivo.TabIndex = 26
        Me.chkEfectivo.Texto = "Efectivo"
        Me.chkEfectivo.Valor = False
        '
        'chkCheque
        '
        Me.chkCheque.BackColor = System.Drawing.SystemColors.Control
        Me.chkCheque.Color = System.Drawing.SystemColors.Control
        Me.chkCheque.Enabled = False
        Me.chkCheque.Location = New System.Drawing.Point(258, 169)
        Me.chkCheque.Name = "chkCheque"
        Me.chkCheque.Size = New System.Drawing.Size(81, 20)
        Me.chkCheque.SoloLectura = False
        Me.chkCheque.TabIndex = 25
        Me.chkCheque.Texto = "Cheque"
        Me.chkCheque.Valor = False
        '
        'OcxImpuesto1
        '
        Me.OcxImpuesto1.Decimales = False
        Me.OcxImpuesto1.dtImpuesto = Nothing
        Me.OcxImpuesto1.Enabled = False
        Me.OcxImpuesto1.IDMoneda = 0
        Me.OcxImpuesto1.Location = New System.Drawing.Point(340, 170)
        Me.OcxImpuesto1.Margin = New System.Windows.Forms.Padding(4)
        Me.OcxImpuesto1.Name = "OcxImpuesto1"
        Me.OcxImpuesto1.Size = New System.Drawing.Size(451, 171)
        Me.OcxImpuesto1.TabIndex = 27
        Me.OcxImpuesto1.TotalRetencionIVA = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'cbxTipoIVA
        '
        Me.cbxTipoIVA.CampoWhere = Nothing
        Me.cbxTipoIVA.CargarUnaSolaVez = False
        Me.cbxTipoIVA.DataDisplayMember = Nothing
        Me.cbxTipoIVA.DataFilter = Nothing
        Me.cbxTipoIVA.DataOrderBy = Nothing
        Me.cbxTipoIVA.DataSource = Nothing
        Me.cbxTipoIVA.DataValueMember = Nothing
        Me.cbxTipoIVA.Enabled = False
        Me.cbxTipoIVA.FormABM = Nothing
        Me.cbxTipoIVA.Indicaciones = Nothing
        Me.cbxTipoIVA.Location = New System.Drawing.Point(86, 169)
        Me.cbxTipoIVA.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoIVA.Name = "cbxTipoIVA"
        Me.cbxTipoIVA.SeleccionObligatoria = True
        Me.cbxTipoIVA.Size = New System.Drawing.Size(90, 21)
        Me.cbxTipoIVA.SoloLectura = False
        Me.cbxTipoIVA.TabIndex = 22
        Me.cbxTipoIVA.Texto = ""
        '
        'cbxCamion
        '
        Me.cbxCamion.CampoWhere = Nothing
        Me.cbxCamion.CargarUnaSolaVez = False
        Me.cbxCamion.DataDisplayMember = "Descripcion"
        Me.cbxCamion.DataFilter = Nothing
        Me.cbxCamion.DataOrderBy = "Descripcion"
        Me.cbxCamion.DataSource = "VCamion"
        Me.cbxCamion.DataValueMember = "ID"
        Me.cbxCamion.FormABM = Nothing
        Me.cbxCamion.Indicaciones = Nothing
        Me.cbxCamion.Location = New System.Drawing.Point(90, 3)
        Me.cbxCamion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCamion.Name = "cbxCamion"
        Me.cbxCamion.SeleccionObligatoria = False
        Me.cbxCamion.Size = New System.Drawing.Size(201, 21)
        Me.cbxCamion.SoloLectura = False
        Me.cbxCamion.TabIndex = 1
        Me.cbxCamion.Texto = ""
        '
        'cbxChofer
        '
        Me.cbxChofer.CampoWhere = Nothing
        Me.cbxChofer.CargarUnaSolaVez = False
        Me.cbxChofer.DataDisplayMember = "Nombres"
        Me.cbxChofer.DataFilter = Nothing
        Me.cbxChofer.DataOrderBy = "Nombres"
        Me.cbxChofer.DataSource = "VChofer"
        Me.cbxChofer.DataValueMember = "ID"
        Me.cbxChofer.FormABM = Nothing
        Me.cbxChofer.Indicaciones = Nothing
        Me.cbxChofer.Location = New System.Drawing.Point(295, 3)
        Me.cbxChofer.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxChofer.Name = "cbxChofer"
        Me.cbxChofer.SeleccionObligatoria = False
        Me.cbxChofer.Size = New System.Drawing.Size(213, 21)
        Me.cbxChofer.SoloLectura = False
        Me.cbxChofer.TabIndex = 2
        Me.cbxChofer.Texto = ""
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = Nothing
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "Deposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = Nothing
        Me.cbxDeposito.DataSource = Nothing
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(78, 124)
        Me.cbxDeposito.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(161, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 30
        Me.cbxDeposito.Texto = ""
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 80
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(299, 10)
        Me.txtProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(455, 27)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 7
        '
        'txtVtoTimbrado
        '
        Me.txtVtoTimbrado.Color = System.Drawing.Color.Empty
        Me.txtVtoTimbrado.Fecha = New Date(2013, 6, 20, 10, 18, 15, 593)
        Me.txtVtoTimbrado.Location = New System.Drawing.Point(462, 67)
        Me.txtVtoTimbrado.Name = "txtVtoTimbrado"
        Me.txtVtoTimbrado.PermitirNulo = False
        Me.txtVtoTimbrado.Size = New System.Drawing.Size(86, 20)
        Me.txtVtoTimbrado.SoloLectura = False
        Me.txtVtoTimbrado.TabIndex = 22
        '
        'txtTimbrado
        '
        Me.txtTimbrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTimbrado.Color = System.Drawing.Color.Empty
        Me.txtTimbrado.Indicaciones = Nothing
        Me.txtTimbrado.Location = New System.Drawing.Point(299, 67)
        Me.txtTimbrado.Multilinea = False
        Me.txtTimbrado.Name = "txtTimbrado"
        Me.txtTimbrado.Size = New System.Drawing.Size(80, 21)
        Me.txtTimbrado.SoloLectura = False
        Me.txtTimbrado.TabIndex = 20
        Me.txtTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTimbrado.Texto = ""
        '
        'cbxGrupo
        '
        Me.cbxGrupo.CampoWhere = Nothing
        Me.cbxGrupo.CargarUnaSolaVez = False
        Me.cbxGrupo.DataDisplayMember = Nothing
        Me.cbxGrupo.DataFilter = Nothing
        Me.cbxGrupo.DataOrderBy = Nothing
        Me.cbxGrupo.DataSource = Nothing
        Me.cbxGrupo.DataValueMember = Nothing
        Me.cbxGrupo.FormABM = Nothing
        Me.cbxGrupo.Indicaciones = Nothing
        Me.cbxGrupo.Location = New System.Drawing.Point(78, 67)
        Me.cbxGrupo.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxGrupo.Name = "cbxGrupo"
        Me.cbxGrupo.SeleccionObligatoria = True
        Me.cbxGrupo.Size = New System.Drawing.Size(161, 21)
        Me.cbxGrupo.SoloLectura = False
        Me.cbxGrupo.TabIndex = 18
        Me.cbxGrupo.Texto = ""
        '
        'cbxRemision
        '
        Me.cbxRemision.CampoWhere = Nothing
        Me.cbxRemision.CargarUnaSolaVez = False
        Me.cbxRemision.DataDisplayMember = Nothing
        Me.cbxRemision.DataFilter = Nothing
        Me.cbxRemision.DataOrderBy = Nothing
        Me.cbxRemision.DataSource = Nothing
        Me.cbxRemision.DataValueMember = Nothing
        Me.cbxRemision.FormABM = Nothing
        Me.cbxRemision.Indicaciones = Nothing
        Me.cbxRemision.Location = New System.Drawing.Point(610, 67)
        Me.cbxRemision.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxRemision.Name = "cbxRemision"
        Me.cbxRemision.SeleccionObligatoria = True
        Me.cbxRemision.Size = New System.Drawing.Size(144, 21)
        Me.cbxRemision.SoloLectura = False
        Me.cbxRemision.TabIndex = 24
        Me.cbxRemision.Texto = ""
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(78, 96)
        Me.txtObservacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(470, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 26
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(78, 13)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(63, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.Texto = ""
        '
        'txtVencimiento
        '
        Me.txtVencimiento.Color = System.Drawing.Color.Empty
        Me.txtVencimiento.Fecha = New Date(2013, 6, 20, 10, 18, 15, 593)
        Me.txtVencimiento.Location = New System.Drawing.Point(655, 40)
        Me.txtVencimiento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtVencimiento.Name = "txtVencimiento"
        Me.txtVencimiento.PermitirNulo = False
        Me.txtVencimiento.Size = New System.Drawing.Size(99, 20)
        Me.txtVencimiento.SoloLectura = False
        Me.txtVencimiento.TabIndex = 16
        '
        'cbxCondicion
        '
        Me.cbxCondicion.CampoWhere = Nothing
        Me.cbxCondicion.CargarUnaSolaVez = False
        Me.cbxCondicion.DataDisplayMember = Nothing
        Me.cbxCondicion.DataFilter = Nothing
        Me.cbxCondicion.DataOrderBy = Nothing
        Me.cbxCondicion.DataSource = Nothing
        Me.cbxCondicion.DataValueMember = Nothing
        Me.cbxCondicion.FormABM = Nothing
        Me.cbxCondicion.Indicaciones = Nothing
        Me.cbxCondicion.Location = New System.Drawing.Point(472, 40)
        Me.cbxCondicion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.SeleccionObligatoria = True
        Me.cbxCondicion.Size = New System.Drawing.Size(145, 21)
        Me.cbxCondicion.SoloLectura = False
        Me.cbxCondicion.TabIndex = 14
        Me.cbxCondicion.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 6, 20, 10, 18, 15, 593)
        Me.txtFecha.Location = New System.Drawing.Point(298, 40)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(117, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 12
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(78, 40)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(63, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 9
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(141, 40)
        Me.txtComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(97, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 10
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(141, 13)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(97, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 5
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.dt = Nothing
        Me.txtCotizacion.Location = New System.Drawing.Point(606, 90)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Registro = Nothing
        Me.txtCotizacion.Saltar = False
        Me.txtCotizacion.Seleccionado = True
        Me.txtCotizacion.Size = New System.Drawing.Size(148, 32)
        Me.txtCotizacion.SoloLectura = False
        Me.txtCotizacion.TabIndex = 28
        '
        'chkIncluirLibro
        '
        Me.chkIncluirLibro.BackColor = System.Drawing.SystemColors.Control
        Me.chkIncluirLibro.Color = System.Drawing.SystemColors.Control
        Me.chkIncluirLibro.Enabled = False
        Me.chkIncluirLibro.Location = New System.Drawing.Point(86, 191)
        Me.chkIncluirLibro.Name = "chkIncluirLibro"
        Me.chkIncluirLibro.Size = New System.Drawing.Size(90, 20)
        Me.chkIncluirLibro.SoloLectura = False
        Me.chkIncluirLibro.TabIndex = 23
        Me.chkIncluirLibro.Texto = "Incluir en Libro"
        Me.chkIncluirLibro.Valor = False
        '
        'frmGenerarCuotasGastos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(854, 459)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.lblFormaPago)
        Me.Controls.Add(Me.gbxVale)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.lblTipoIVA)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.chkCuota)
        Me.Controls.Add(Me.chkEfectivo)
        Me.Controls.Add(Me.chkCheque)
        Me.Controls.Add(Me.OcxImpuesto1)
        Me.Controls.Add(Me.cbxTipoIVA)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Controls.Add(Me.btnCuota)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.chkIncluirLibro)
        Me.Name = "frmGenerarCuotasGastos"
        Me.Tag = "frmGenerarCuotasGastos"
        Me.Text = "frmGenerarCuotasGastos"
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.gbxVale.ResumeLayout(False)
        Me.gbxVale.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.pnlDistribucion.ResumeLayout(False)
        Me.pnlDistribucion.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents txtCantidadComprobante As ERP.ocxTXTNumeric
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblFormaPago As System.Windows.Forms.Label
    Friend WithEvents gbxVale As System.Windows.Forms.GroupBox
    Friend WithEvents lvVale As System.Windows.Forms.ListView
    Friend WithEvents colIDTransaccion As System.Windows.Forms.ColumnHeader
    Friend WithEvents colFecha As System.Windows.Forms.ColumnHeader
    Friend WithEvents colMotivo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colNombre As System.Windows.Forms.ColumnHeader
    Friend WithEvents colTotal As System.Windows.Forms.ColumnHeader
    Friend WithEvents colSaldo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colImporte As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtTotalComprobante As ERP.ocxTXTNumeric
    Friend WithEvents lklAgregarVale As System.Windows.Forms.LinkLabel
    Friend WithEvents lklEliminarVale As System.Windows.Forms.LinkLabel
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lblTipoIVA As System.Windows.Forms.Label
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents chkCuota As ERP.ocxCHK
    Friend WithEvents chkEfectivo As ERP.ocxCHK
    Friend WithEvents chkCheque As ERP.ocxCHK
    Friend WithEvents OcxImpuesto1 As ERP.ocxImpuesto
    Friend WithEvents cbxTipoIVA As ERP.ocxCBX
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents pnlDistribucion As System.Windows.Forms.Panel
    Friend WithEvents lblDistribucion As System.Windows.Forms.Label
    Friend WithEvents cbxCamion As ERP.ocxCBX
    Friend WithEvents cbxChofer As ERP.ocxCBX
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents lblDeposito As System.Windows.Forms.Label
    Friend WithEvents txtProveedor As ERP.ocxTXTProveedor
    Friend WithEvents txtVtoTimbrado As ERP.ocxTXTDate
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtTimbrado As ERP.ocxTXTString
    Friend WithEvents cbxGrupo As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents cbxRemision As ERP.ocxCBX
    Friend WithEvents lblRemision As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtVencimiento As ERP.ocxTXTDate
    Friend WithEvents lblVencimiento As System.Windows.Forms.Label
    Friend WithEvents cbxCondicion As ERP.ocxCBX
    Friend WithEvents lblCondicion As System.Windows.Forms.Label
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As ERP.ocxCotizacion
    Friend WithEvents btnCuota As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents chkIncluirLibro As ERP.ocxCHK
End Class

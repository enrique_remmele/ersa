﻿Public Class frmGastoAplicacionVales

    ' CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Public CAsiento As New CAsientoGasto

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private IDTransaccionARendirValue As Integer
    Public Property IDTransaccionARendir() As Integer
        Get
            Return IDTransaccionARendirValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionARendirValue = value
        End Set
    End Property

    Private CajaChicaValue As Boolean
    Public Property CajaChica As Boolean
        Get
            Return CajaChicaValue
        End Get
        Set(ByVal value As Boolean)
            CajaChicaValue = value
        End Set
    End Property

    Private NumeroValue As Integer
    Public Property Numero As Integer
        Get
            Return NumeroValue
        End Get
        Set(ByVal value As Integer)
            NumeroValue = value
        End Set
    End Property

    Public Property dtCuota As DataTable
    Public Property Cuota As Integer

    'EVENTOS

    'VARIABLES
    Dim vControles() As Control
    Dim vModificando As Boolean
    Dim dtVales As New DataTable
    Dim dt As New DataTable
    Dim Total As Decimal

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Controles
        txtProveedor.Conectar()

        'Otros
        flpRegistradoPor.Visible = False
        txtCotizacion.Inicializar()

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(frmGastos.Name, "GASTO", "GAS")

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        MostrarCajaChica()
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        'CSistema.CargaControl(vControles, txtProveedor)
        'CSistema.CargaControl(vControles, cbxTipoComprobante)
        'CSistema.CargaControl(vControles, txtComprobante)
        'CSistema.CargaControl(vControles, txtFecha)
        'CSistema.CargaControl(vControles, txtVtoTimbrado)
        'CSistema.CargaControl(vControles, cbxCondicion)
        'CSistema.CargaControl(vControles, txtVencimiento)
        'CSistema.CargaControl(vControles, cbxGrupo)
        'CSistema.CargaControl(vControles, txtCotizacion)
        'CSistema.CargaControl(vControles, txtObservacion)
        'CSistema.CargaControl(vControles, cbxRemision)
        'CSistema.CargaControl(vControles, cbxTipoIVA)
        'CSistema.CargaControl(vControles, chkCheque)
        'CSistema.CargaControl(vControles, chkEfectivo)
        CSistema.CargaControl(vControles, lklAgregarVale)
        CSistema.CargaControl(vControles, lklEliminarVale)
        'CSistema.CargaControl(vControles, txtTimbrado)
        'CSistema.CargaControl(vControles, txtVtoTimbrado)
        'CSistema.CargaControl(vControles, cbxDeposito)
        'CSistema.CargaControl(vControles, cbxCamion)
        'CSistema.CargaControl(vControles, cbxChofer)

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CONDICION
        cbxCondicion.cbx.Items.Add("CONTADO")
        cbxCondicion.cbx.Items.Add("CREDITO")
        cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR CONTROLES
        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal"), "ID", "Codigo")

        'Grupo
        CSistema.SqlToComboBox(cbxGrupo.cbx, "Select IDGrupo, Grupo  From vGrupoUsuario Where IDUsuario=" & vgIDUsuario)

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'CARGAR LA ULTIMA CONFIGURACION
        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Condicion
        cbxCondicion.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CONDICION", "CONTADO")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Grupo
        cbxGrupo.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "GRUPO", "")

        'Moneda
        txtCotizacion.cbxMoneda.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MONEDA", "")
    End Sub

    Sub GuardarInformacion()

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Condicion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CONDICION", cbxCondicion.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

        'Grupo0
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "GRUPO", cbxGrupo.cbx.Text)

        'Moneda
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", txtCotizacion.cbxMoneda.cbx.Text)

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            If CajaChica = True Then
                CargarOperacionConFondoFijo()
            Else
                CargarOperacion()
            End If
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            If CajaChica = True Then
                CargarOperacionConFondoFijo()
            Else
                CargarOperacion()
            End If

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()

            If CajaChica = True Then
                CargarOperacionConFondoFijo()
            Else
                CargarOperacion()
            End If

        End If



        If e.KeyCode = Keys.End Then

            'If vNuevo = True Then
            '    Exit Sub
            'End If

            Dim ID As Integer
            If CajaChica = True Then
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From GastoFondoFijo Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)
            Else
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And CajaChica='False' "), Integer)
            End If


            txtID.txt.Text = ID
            txtID.txt.SelectAll()

            If CajaChica = True Then
                CargarOperacionConFondoFijo()
            Else
                CargarOperacion()
            End If

        End If

        If e.KeyCode = Keys.Home Then

            'If vNuevo = True Then
            '    Exit Sub
            'End If

            Dim ID As Integer
            If CajaChica = True Then
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From GastoFondoFijo Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)
            Else
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And CajaChica='False' "), Integer)
            End If


            txtID.txt.Text = ID
            txtID.txt.SelectAll()

            If CajaChica = True Then
                CargarOperacionConFondoFijo()
            Else
                CargarOperacion()
            End If

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub CargarOperacionConFondoFijo(Optional ByVal vIDTransaccion As Integer = 0)

        vModificando = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = SeleccionarRegistro("VGastoFondoFijo")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)


        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VGastoFondoFijo Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        txtID.txt.Text = oRow("Numero").ToString
        txtProveedor.SetValue(oRow("IDProveedor").ToString)
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtFecha.SetValue(oRow("Fecha"))
        txtTimbrado.txt.Text = oRow("NroTimbrado").ToString
        txtVtoTimbrado.SetValue(oRow("FechaVencimientoTimbrado").ToString)
        cbxGrupo.cbx.Text = oRow("Grupo").ToString

        'Condicion
        If CBool(oRow("CREDITO").ToString) = False Then
            txtVencimiento.txt.Clear()
            cbxCondicion.cbx.SelectedIndex = 0
        Else
            txtVencimiento.SetValue(CDate(oRow("FechaVencimiento").ToString))
            'calcular plazo
            cbxCondicion.cbx.SelectedIndex = 1
        End If

        cbxSucursal.txt.Text = oRow("Sucursal").ToString

        'Cotizacion
        txtCotizacion.SetValue(oRow("Moneda").ToString, oRow("Cotizacion").ToString)
        txtObservacion.txt.Text = oRow("Observacion").ToString
        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString


        'Verificar si es que la moneda admite decimales
        Dim Decimales As Boolean = False

        Dim dt1 As DataTable = CData.GetTable("VMoneda", " referencia='" & oRow("Moneda").ToString & "'")

        Decimales = dt1.Rows(0)("Decimales")

        txtTotalGasto.txt.Text = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
        txtTotalIva.txt.Text = CSistema.FormatoMoneda(oRow("TotalImpuesto").ToString, Decimales)

        'Cargamos GastoVale
        lvVale.Items.Clear()
        dtVales = CSistema.ExecuteToDataTable("Select * ,'Sel'='True' from VGastoVale Where IDTransaccionGasto= " & IDTransaccion).Copy
        CargarVale()

        'Inicializamos el Asiento
        ' CAsiento.Limpiar()
        'Total = OcxImpuesto1.txtTotal.txt.Text
        Total = txtTotalGasto.txt.Text


        CAsiento.CargarOperacion(IDTransaccion)

        txtID.txt.Focus()
        txtID.txt.SelectAll()

    End Sub

    Function SeleccionarRegistro(vTabla As String) As Integer

        SeleccionarRegistro = 0

        Dim vIDTransaccion As Integer

        'IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From GastoFondoFijo Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )")
        'Error de numeracion, un misterio
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select IDTransaccion, Sucursal, Numero, [Cod.], Comprobante, Proveedor, Fecha, Grupo, FechaTransaccion, Usuario From " & vTabla & " Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ")

        If dt.Rows.Count = 0 Then
            vIDTransaccion = 0
        End If

        If dt.Rows.Count = 1 Then
            vIDTransaccion = dt.Rows(0)("IDTransaccion").ToString
        End If

        If dt.Rows.Count > 1 Then
            MessageBox.Show("El sistema encontro que hay una duplicacion de numeracion.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)

            Dim frm As New frmGastosDuplicados
            frm.dt = dt
            FGMostrarFormulario(Me, frm, "Duplicacion de Numeracion", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

            If frm.Procesado = False Then
                vIDTransaccion = 0
            End If

            If frm.Procesado = True Then
                vIDTransaccion = frm.IDTransaccion
            End If

        End If

        SeleccionarRegistro = vIDTransaccion

    End Function

    Sub Nuevo()

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        cbxSucursal.Enabled = True

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'If Total <> CDec(OcxImpuesto1.txtTotal.txt.Text) Then
        If Total <> CDec(txtTotalGasto.txt.Text) Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Validar
        'Tipo Comprobante
        If cbxTipoComprobante.Validar("Seleccione correctamente el tipo de comprobante!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            CSistema.MostrarError("Ingrese un numero de comprobante!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If


        'Timbrado
        If CData.GetTable("VTipoComprobante", " ID=" & cbxTipoComprobante.GetValue)(0)("ComprobanteTimbrado") = True Then
            If txtTimbrado.txt.Text.Trim.Length = 0 Then
                Dim mensaje As String = "Ingrese un número de timbrado!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If CSistema.ControlarTimbrado(txtTimbrado.txt.Text) = False Then
                Exit Function
            End If

            If CSistema.ControlarNroDocumento(txtComprobante.txt.Text) = False Then
                Exit Function
            End If

        End If

        'Sucursal
        If cbxSucursal.Validar("Seleccione correctamente la sucursal!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Total
        'If CDec(OcxImpuesto1.txtTotal.ObtenerValor) <= 0 Then
        If CDec(txtTotalGasto.ObtenerValor) <= 0 Then
            Dim mensaje As String = "El importe del documento no es valido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Validar el Asiento
        If CAsiento.dtAsiento.Rows.Count = 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Validar el Asiento
        If CAsiento.dtDetalleAsiento.Rows.Count = 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Validar el Asiento
        If CAsiento.ObtenerSaldo <> 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        If CAsiento.ObtenerTotal = 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Si va a eliminar
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If
        Dim MensajeRetorno As String = ""
        'Cargamos el asiento
        'CAsiento.IDTransaccion = IDTransaccion
        'CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        Dim Procesar As Boolean = True

        If CajaChica = True Then

            'Guardar detalle
            If IDTransaccion > 0 Then

                If Operacion = ERP.CSistema.NUMOperacionesRegistro.UPD Then

                    'Insertamos el Detalle
                    InsertarDetalle(ERP.CSistema.NUMOperacionesRegistro.INS, IDTransaccion)

                    If lvVale.Items.Count = 0 Then
                        InsertarDetalleFondoFijo(IDTransaccion)
                    End If
                End If
            End If

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False
        Cancelar()
        cbxSucursal.Enabled = True
    End Sub
    Function InsertarDetalleFondoFijo(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalleFondoFijo = True

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDGrupo", cbxGrupo.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTransaccionGasto", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(txtTotalGasto.ObtenerValor), ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Insertar el detalle
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpDetalleRendicionFondoFijo", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            Return False
        End If


    End Function

    Sub Modificar()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        ctrError.Clear()
        tsslEstado.Text = ""

        btnAsiento.Enabled = True
        vModificando = True

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Bloquear Origen
        cbxSucursal.Enabled = False

        cbxTipoComprobante.Focus()
        'dtVales = CSistema.ExecuteToDataTable("Select *, 'Sel'= 'False','Cancelar'= 'False' from VVale Where ARendir= 'True' And Cancelado= 'False' And Anulado='False'").Copy
    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        'If Total <> OcxImpuesto1.txtTotal.ObtenerValor Then
        If Total <> txtTotalGasto.ObtenerValor Then
            CAsiento.InicializarAsiento()
            CAsiento.Generado = False
            vModificando = True
        End If

        'Si es nuevo
        If vModificando = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "Compra: " & txtID.txt.Text & "  -  " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text & "  -  " & txtProveedor.txtRazonSocial.Text

            'Para que muestre solo las cuenta contables que estan asignadas al fondo fijo
            If CajaChica = True Then
                Dim dtCCFF As New DataTable
                Dim CuentasContables As String = "ID = 0 "
                dtCCFF = CSistema.ExecuteToDataTable("Select IDCuentaContable from FondoFijoCuentaContable where IdFondoFijo = (select ID from Grupo where descripcion = '" & cbxGrupo.txt.Text & "') ").Copy

                For Each oRow1 As DataRow In dtCCFF.Rows
                    Dim param As New DataTable
                    CuentasContables = CuentasContables & " or ID =" & oRow1("IDCuentaContable").ToString & "    "
                Next
                frm.Filtrar = True
                frm.whereFiltro = CuentasContables
            Else
                frm.Filtrar = False
                frm.whereFiltro = ""
            End If

            'Dim SQL As String

            If CajaChica = False Then
                'SQL = "Select IsNull((Select IDTransaccion From GastoTipoComprobante Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ), 0 )"
                IDTransaccion = SeleccionarRegistro("VGastoTipoComprobante")
            Else
                'SQL = "Select IsNull((Select IDTransaccion From GastoFondoFijo Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ), 0 )"
                IDTransaccion = SeleccionarRegistro("VGastoFondoFijo")
            End If

            'Dim IDTransaccion As Integer = CSistema.ExecuteScalar(SQL)
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)

        Else

            If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(cbxTipoComprobante, mensaje)
                ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If txtProveedor.Seleccionado = False Then
                Dim mensaje As String = "Seleccione correctamente el proveedor!"
                ctrError.SetError(txtProveedor, mensaje)
                ctrError.SetIconAlignment(txtProveedor, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "Compra: " & txtID.txt.Text & "  -  " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text & "  -  " & txtProveedor.txtRazonSocial.Text

            'Para que muestre solo las cuenta contables que estan asignadas al fondo fijo
            If CajaChica = True Then
                Dim dtCCFF As New DataTable
                Dim CuentasContables As String = "ID = 0 "
                dtCCFF = CSistema.ExecuteToDataTable("Select IDCuentaContable from FondoFijoCuentaContable where IdFondoFijo =" & cbxGrupo.GetValue & " ").Copy

                For Each oRow1 As DataRow In dtCCFF.Rows
                    Dim param As New DataTable
                    CuentasContables = CuentasContables & " or ID =" & oRow1("IDCuentaContable").ToString & "    "
                Next
                frm.Filtrar = True
                frm.whereFiltro = CuentasContables
            Else
                frm.Filtrar = False
                frm.whereFiltro = ""
            End If

            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
            CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            CAsiento.NroCaja = frm.CAsiento.NroCaja
            CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            Dim oRow As DataRow = frm.CAsiento.dtAsiento.Rows(0)
            Total = oRow("Total").ToString

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                CAsiento.dtAsiento.Clear()
                CAsiento.dtDetalleAsiento.Clear()
                CAsiento.dtImpuesto.Clear()
                GenerarAsiento(True)
                VisualizarAsiento()
            End If

        End If


    End Sub

    Sub GenerarAsiento(Optional VolverAGenerar As Boolean = False)

        'EstablecerCabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = CData.GetTable("VSucursal", " ID = " & cbxSucursal.GetValue).Rows(0)("IDCiudad").ToString
        oRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
        oRow("Fecha") = txtFecha.GetValue
        oRow("IDMoneda") = txtCotizacion.Registro("ID")
        oRow("Cotizacion") = txtCotizacion.GetValue
        oRow("IDTipoComprobante") = cbxTipoComprobante.cbx.SelectedValue
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = txtComprobante.txt.Text
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text
        oRow("Detalle") = txtObservacion.txt.Text
        'oRow("Total") = OcxImpuesto1.txtTotal.ObtenerValor
        oRow("Total") = txtTotalGasto.ObtenerValor
        oRow("CajaChica") = CajaChica

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)


        'Solo establecer la primera vez, esto es para el detalle
        If CAsiento.Generado = True Then
            Exit Sub
        End If

        'CAsiento.dtImpuesto = OcxImpuesto1.dtImpuesto.Copy
        CAsiento.IDProveedor = txtProveedor.Registro("ID").ToString

        If VolverAGenerar = True Then
            CAsiento.Generar()
        End If

    End Sub

    Function InsertarDetalle(ByVal Operacion As CSistema.NUMOperacionesRegistro, ByVal vIDTransaccion As Integer) As Boolean

        InsertarDetalle = True


        For Each oRow As DataRow In dtVales.Select("Sel='True'")

            Dim param(-1) As SqlClient.SqlParameter


            CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDGrupo", cbxGrupo.cbx.SelectedValue, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccionVale", (oRow("IDTransaccion").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccionGasto", vIDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Saldo", CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpGastoVale", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
                Return False
            End If

        Next

    End Function

    Sub CargaValeARendir()
        Dim frm As New frmValeAResindir
        dtVales = CSistema.ExecuteToDataTable("Select *, 'Sel'= 'False','Cancelar'= 'False' from VVale Where ARendir= 'True' And Cancelado= 'False' And Anulado='False'").Copy
        frm.Text = " Seleccionar Seleccionar Vales "
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtVales
        frm.Inicializar()
        frm.ShowDialog(Me)
        ' dtVales = frm.dt

        CargarVale()

    End Sub

    Sub MostrarCajaChica()
        If CajaChica = True Then
            gbxVale.Visible = True
        End If

        If CajaChica = False Then
            gbxVale.Visible = False
        End If
    End Sub

    Sub CargarVale()

        'lvVale.Items.Clear()

        Dim Total As Decimal = 0

        For Each oRow As DataRow In dtVales.Rows
            If oRow("Sel") = True Then

                Dim item As ListViewItem = New ListViewItem(oRow("IDTransaccion").ToString)
                item.SubItems.Add(oRow("Fec").ToString)
                item.SubItems.Add(oRow("Motivo").ToString)
                item.SubItems.Add(oRow("Nombre").ToString)
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Total").ToString))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Saldo").ToString))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Importe").ToString))
                Total = Total + CDec(oRow("Importe").ToString)

                lvVale.Items.Add(item)

            End If
        Next

        txtTotalComprobante.SetValue(Total)
        txtCantidadComprobante.SetValue(lvVale.Items.Count)

    End Sub

    Sub EliminarVale()

        For Each item As ListViewItem In lvVale.SelectedItems

            For Each oRow As DataRow In dtVales.Select(" IDTransaccion = " & item.Text & " ")
                'CAsientoContableOrdenPago.EliminarVenta(oRow("Importe").ToString, oRow("IDTipoComprobante").ToString, oRow("IDMoneda").ToString, oRow("Credito").ToString)

                If CajaChica = True Then

                    'Guardar detalle
                    If IDTransaccion > 0 Then
                        oRow("Sel") = True
                        InsertarDetalle(ERP.CSistema.NUMOperacionesRegistro.DEL, IDTransaccion)
                    End If

                End If
                oRow("Sel") = False
                Exit For
            Next

        Next

        CargarVale()

    End Sub

    Sub Buscar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO, New Button, btnGuardar, btnCancelar, New Button, New Button, btnBusquedaAvanzada, btnAsiento, vControles, btnModificar)
        'Otros

        Dim frm As New frmConsultaGasto
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.CajaChica = CajaChica
        frm.ShowDialog(Me)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        'vNuevo = True
        vModificando = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            'IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From GastoTipoComprobante Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ), 0 )")
            IDTransaccion = SeleccionarRegistro("VGastoTipoComprobante")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dt.Rows.Clear()

        dt = CSistema.ExecuteToDataTable("Select * From VGastoTipoComprobante Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas técnicos."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        txtID.txt.Text = oRow("Numero").ToString
        txtProveedor.SetValue(oRow("IDProveedor").ToString)
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtFecha.SetValue(oRow("Fecha"))
        txtTimbrado.txt.Text = oRow("NroTimbrado").ToString
        cbxGrupo.cbx.Text = oRow("Grupo").ToString
        txtVtoTimbrado.SetValue(oRow("FechaVencimientoTimbrado").ToString)

        'Condicion
        If CBool(oRow("CREDITO").ToString) = False Then
            txtVencimiento.txt.Clear()
            cbxCondicion.cbx.SelectedIndex = 0
        Else
            txtVencimiento.SetValue(CDate(oRow("FechaVencimiento").ToString))
            'calcular plazo
            cbxCondicion.cbx.SelectedIndex = 1
        End If

        cbxSucursal.cbx.Text = oRow("Sucursal").ToString

        'Cotizacion
        txtCotizacion.SetValue(oRow("Moneda").ToString, oRow("Cotizacion").ToString)
        txtObservacion.txt.Text = oRow("Observacion").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        txtTotalGasto.txt.Text = oRow("Total").ToString
        txtTotalIva.txt.Text = oRow("TotalImpuesto").ToString

        CAsiento.CargarOperacion(IDTransaccion)

        txtID.txt.Focus()
        txtID.txt.SelectAll()


    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnModificar, btnGuardar, btnCancelar, New Button, New Button, btnBusquedaAvanzada, btnAsiento, vControles, New Button)

    End Sub

    Private Sub frmGasto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub frmGasto_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmGasto_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub txtProveedor_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProveedor.Leave
        txtProveedor.OcultarLista()
    End Sub

    Private Sub txtProveedor_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProveedor.ItemSeleccionado
        cbxTipoComprobante.cbx.Focus()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.UPD)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Eliminar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub cbxTipoComprobante_Editar(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxTipoComprobante.Editar

        Dim frm As New frmTipoComprobante
        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        cbxTipoComprobante.cbx.DataSource = Nothing
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=20")

    End Sub

    Private Sub cbxSucursal_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxSucursal.Leave
        If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxSucursal.cbx.Text.Trim = "" Then
            Exit Sub
        End If
    End Sub

    Private Sub cbxSucursal_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxSucursal.TeclaPrecionada
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklAgregarVale.LinkClicked
        CargaValeARendir()
    End Sub

    Private Sub lklEliminarVenta_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarVale.LinkClicked
        EliminarVale()
        Cancelar()
    End Sub

    Private Sub cbxTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoComprobante.PropertyChanged
        If dt.Rows.Count = 0 Then
            Exit Sub
        End If
        For Each oRow As DataRow In dt.Rows
            If oRow("TipoComprobante") <> cbxTipoComprobante.cbx.Text Then
                CAsiento.InicializarAsiento()
                CAsiento.Generado = False
                vModificando = True
            Else
                vModificando = False
            End If
        Next
    End Sub

    Private Sub txtObservacion_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtObservacion.KeyUp
        If dt.Rows.Count = 0 Then
            Exit Sub
        End If
        For Each oRow As DataRow In dt.Rows
            If oRow("Observacion") <> txtObservacion.txt.Text Then
                CAsiento.InicializarAsiento()
                CAsiento.Generado = False
                vModificando = True
            Else
                vModificando = False
            End If
        Next
    End Sub

    Private Sub txtComprobante_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtComprobante.KeyUp
        If dt.Rows.Count = 0 Then
            Exit Sub
        End If
        For Each oRow As DataRow In dt.Rows
            If oRow("Comprobante") <> txtComprobante.txt.Text Then
                CAsiento.InicializarAsiento()
                CAsiento.Generado = False
                vModificando = True
            Else
                vModificando = False
            End If
        Next
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmValeAResindir
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SeleccionarTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuitarTodaSeleccionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtCantidadPagado = New ERP.ocxTXTNumeric()
        Me.txtTotalPagado = New ERP.ocxTXTNumeric()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colSuc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMotivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCancelar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblTotalCobrado = New System.Windows.Forms.Label()
        Me.txtCantidadComprobante = New ERP.ocxTXTNumeric()
        Me.txtTotalComprobante = New ERP.ocxTXTNumeric()
        Me.lblContado = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.cbxGrupo = New ERP.ocxCBX()
        Me.lblGrupo = New System.Windows.Forms.Label()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.Button2)
        Me.FlowLayoutPanel4.Controls.Add(Me.Button1)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(0, 420)
        Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(896, 30)
        Me.FlowLayoutPanel4.TabIndex = 3
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(818, 3)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "&Cancelar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(727, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(85, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "&Aceptar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SeleccionarTodoToolStripMenuItem, Me.QuitarTodaSeleccionToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(187, 48)
        '
        'SeleccionarTodoToolStripMenuItem
        '
        Me.SeleccionarTodoToolStripMenuItem.Name = "SeleccionarTodoToolStripMenuItem"
        Me.SeleccionarTodoToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.SeleccionarTodoToolStripMenuItem.Text = "Seleccionar todo"
        '
        'QuitarTodaSeleccionToolStripMenuItem
        '
        Me.QuitarTodaSeleccionToolStripMenuItem.Name = "QuitarTodaSeleccionToolStripMenuItem"
        Me.QuitarTodaSeleccionToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.QuitarTodaSeleccionToolStripMenuItem.Text = "Quitar toda seleccion"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 448)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(896, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'txtCantidadPagado
        '
        Me.txtCantidadPagado.Color = System.Drawing.Color.Empty
        Me.txtCantidadPagado.Decimales = True
        Me.txtCantidadPagado.Indicaciones = Nothing
        Me.txtCantidadPagado.Location = New System.Drawing.Point(400, 3)
        Me.txtCantidadPagado.Name = "txtCantidadPagado"
        Me.txtCantidadPagado.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadPagado.SoloLectura = False
        Me.txtCantidadPagado.TabIndex = 5
        Me.txtCantidadPagado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadPagado.Texto = "0"
        '
        'txtTotalPagado
        '
        Me.txtTotalPagado.Color = System.Drawing.Color.Empty
        Me.txtTotalPagado.Decimales = True
        Me.txtTotalPagado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalPagado.Indicaciones = Nothing
        Me.txtTotalPagado.Location = New System.Drawing.Point(311, 3)
        Me.txtTotalPagado.Name = "txtTotalPagado"
        Me.txtTotalPagado.Size = New System.Drawing.Size(83, 22)
        Me.txtTotalPagado.SoloLectura = True
        Me.txtTotalPagado.TabIndex = 4
        Me.txtTotalPagado.TabStop = False
        Me.txtTotalPagado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalPagado.Texto = "0"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(3, 0)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(87, 13)
        Me.LinkLabel1.TabIndex = 0
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Seleccionar todo"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.dgw, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(896, 470)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle8
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colSel, Me.colSuc, Me.colFecha, Me.colMotivo, Me.colNombre, Me.colTotal, Me.colSaldo, Me.colImporte, Me.colCancelar})
        Me.dgw.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgw.Location = New System.Drawing.Point(3, 40)
        Me.dgw.Name = "dgw"
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(890, 349)
        Me.dgw.TabIndex = 1
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colSel
        '
        Me.colSel.HeaderText = "Sel"
        Me.colSel.Name = "colSel"
        Me.colSel.ReadOnly = True
        Me.colSel.Width = 30
        '
        'colSuc
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colSuc.DefaultCellStyle = DataGridViewCellStyle9
        Me.colSuc.HeaderText = "Suc"
        Me.colSuc.Name = "colSuc"
        Me.colSuc.ReadOnly = True
        Me.colSuc.ToolTipText = "Comprobante de Venta"
        Me.colSuc.Width = 50
        '
        'colFecha
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.NullValue = "---"
        Me.colFecha.DefaultCellStyle = DataGridViewCellStyle10
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.ReadOnly = True
        Me.colFecha.Width = 78
        '
        'colMotivo
        '
        Me.colMotivo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.NullValue = "---"
        Me.colMotivo.DefaultCellStyle = DataGridViewCellStyle11
        Me.colMotivo.HeaderText = "Motivo"
        Me.colMotivo.Name = "colMotivo"
        Me.colMotivo.ReadOnly = True
        '
        'colNombre
        '
        Me.colNombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colNombre.HeaderText = "Nombre"
        Me.colNombre.Name = "colNombre"
        '
        'colTotal
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.Format = "N0"
        DataGridViewCellStyle12.NullValue = "0"
        Me.colTotal.DefaultCellStyle = DataGridViewCellStyle12
        Me.colTotal.HeaderText = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.ReadOnly = True
        Me.colTotal.Width = 78
        '
        'colSaldo
        '
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "N0"
        DataGridViewCellStyle13.NullValue = "0"
        Me.colSaldo.DefaultCellStyle = DataGridViewCellStyle13
        Me.colSaldo.HeaderText = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.ReadOnly = True
        Me.colSaldo.Width = 78
        '
        'colImporte
        '
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "N0"
        DataGridViewCellStyle14.NullValue = "0"
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle14
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        '
        'colCancelar
        '
        Me.colCancelar.HeaderText = "Cancel"
        Me.colCancelar.Name = "colCancelar"
        Me.colCancelar.ReadOnly = True
        Me.colCancelar.ToolTipText = "Cancelar manualmente el comprobante"
        Me.colCancelar.Width = 35
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.LinkLabel1, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.FlowLayoutPanel1, 1, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(0, 392)
        Me.TableLayoutPanel3.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(896, 28)
        Me.TableLayoutPanel3.TabIndex = 2
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtCantidadPagado)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalPagado)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblTotalCobrado)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtCantidadComprobante)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalComprobante)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblContado)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(448, 0)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(448, 28)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'lblTotalCobrado
        '
        Me.lblTotalCobrado.Location = New System.Drawing.Point(244, 0)
        Me.lblTotalCobrado.Name = "lblTotalCobrado"
        Me.lblTotalCobrado.Size = New System.Drawing.Size(61, 22)
        Me.lblTotalCobrado.TabIndex = 3
        Me.lblTotalCobrado.Text = "Pagado:"
        Me.lblTotalCobrado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCantidadComprobante
        '
        Me.txtCantidadComprobante.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobante.Decimales = True
        Me.txtCantidadComprobante.Indicaciones = Nothing
        Me.txtCantidadComprobante.Location = New System.Drawing.Point(193, 3)
        Me.txtCantidadComprobante.Name = "txtCantidadComprobante"
        Me.txtCantidadComprobante.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadComprobante.SoloLectura = False
        Me.txtCantidadComprobante.TabIndex = 2
        Me.txtCantidadComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobante.Texto = "0"
        '
        'txtTotalComprobante
        '
        Me.txtTotalComprobante.Color = System.Drawing.Color.Empty
        Me.txtTotalComprobante.Decimales = True
        Me.txtTotalComprobante.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalComprobante.Indicaciones = Nothing
        Me.txtTotalComprobante.Location = New System.Drawing.Point(104, 3)
        Me.txtTotalComprobante.Name = "txtTotalComprobante"
        Me.txtTotalComprobante.Size = New System.Drawing.Size(83, 22)
        Me.txtTotalComprobante.SoloLectura = True
        Me.txtTotalComprobante.TabIndex = 1
        Me.txtTotalComprobante.TabStop = False
        Me.txtTotalComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalComprobante.Texto = "0"
        '
        'lblContado
        '
        Me.lblContado.Location = New System.Drawing.Point(39, 0)
        Me.lblContado.Name = "lblContado"
        Me.lblContado.Size = New System.Drawing.Size(59, 22)
        Me.lblContado.TabIndex = 0
        Me.lblContado.Text = "Total:"
        Me.lblContado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.lblSucursal)
        Me.FlowLayoutPanel3.Controls.Add(Me.cbxSucursal)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblFecha)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtDesde)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtHasta)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblGrupo)
        Me.FlowLayoutPanel3.Controls.Add(Me.cbxGrupo)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnListar)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(890, 31)
        Me.FlowLayoutPanel3.TabIndex = 0
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(3, 9)
        Me.lblSucursal.Margin = New System.Windows.Forms.Padding(3, 9, 3, 0)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Text = "Sucursal:"
        Me.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(61, 5)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4, 5, 4, 4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(81, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 20, 10, 18, 15, 593)
        Me.txtDesde.Location = New System.Drawing.Point(196, 6)
        Me.txtDesde.Margin = New System.Windows.Forms.Padding(4, 6, 4, 4)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(79, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 3
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(149, 9)
        Me.lblFecha.Margin = New System.Windows.Forms.Padding(3, 9, 3, 0)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Text = "Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 20, 10, 18, 15, 593)
        Me.txtHasta.Location = New System.Drawing.Point(283, 6)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(4, 6, 4, 4)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(79, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 4
        '
        'cbxGrupo
        '
        Me.cbxGrupo.CampoWhere = Nothing
        Me.cbxGrupo.CargarUnaSolaVez = False
        Me.cbxGrupo.DataDisplayMember = "Grupo"
        Me.cbxGrupo.DataFilter = Nothing
        Me.cbxGrupo.DataOrderBy = "Grupo"
        Me.cbxGrupo.DataSource = Nothing
        Me.cbxGrupo.DataValueMember = "IDGrupo"
        Me.cbxGrupo.FormABM = Nothing
        Me.cbxGrupo.Indicaciones = Nothing
        Me.cbxGrupo.Location = New System.Drawing.Point(414, 5)
        Me.cbxGrupo.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.cbxGrupo.Name = "cbxGrupo"
        Me.cbxGrupo.SeleccionObligatoria = False
        Me.cbxGrupo.Size = New System.Drawing.Size(224, 21)
        Me.cbxGrupo.SoloLectura = False
        Me.cbxGrupo.TabIndex = 6
        Me.cbxGrupo.Texto = ""
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(369, 9)
        Me.lblGrupo.Margin = New System.Windows.Forms.Padding(3, 9, 3, 0)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(39, 13)
        Me.lblGrupo.TabIndex = 5
        Me.lblGrupo.Text = "Grupo:"
        Me.lblGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(644, 4)
        Me.btnListar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 3)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(111, 23)
        Me.btnListar.TabIndex = 7
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'frmValeAResindir
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(896, 470)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmValeAResindir"
        Me.Text = "frmValeAResindir"
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SeleccionarTodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuitarTodaSeleccionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents txtCantidadPagado As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalPagado As ERP.ocxTXTNumeric
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblTotalCobrado As System.Windows.Forms.Label
    Friend WithEvents txtCantidadComprobante As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalComprobante As ERP.ocxTXTNumeric
    Friend WithEvents lblContado As System.Windows.Forms.Label
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents colIDTransaccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSel As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colSuc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colMotivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSaldo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCancelar As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
    Friend WithEvents cbxGrupo As ERP.ocxCBX
    Friend WithEvents btnListar As System.Windows.Forms.Button
End Class

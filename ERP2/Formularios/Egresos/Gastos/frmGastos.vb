﻿Public Class frmGastos

    ' CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Public CAsiento As New CAsientoGasto
    Dim vCantidadProducto As Double = 0
    Dim vIDAcuerdo As Integer
    Dim vObservacionProducto As String
    Dim vCCProductoArecibir As String
    Dim Inicializado As Boolean = False
    Dim UnidadNegocio As String
    Dim Departamento As String
    Dim Seccion As Boolean = False

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private IDTransaccionARendirValue As Integer
    Public Property IDTransaccionARendir() As Integer
        Get
            Return IDTransaccionARendirValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionARendirValue = value
        End Set
    End Property

    Private CajaChicaValue As Boolean
    Public Property CajaChica As Boolean
        Get
            Return CajaChicaValue
        End Get
        Set(ByVal value As Boolean)
            CajaChicaValue = value
        End Set
    End Property

    Private NumeroValue As Integer
    Public Property Numero As Integer
        Get
            Return NumeroValue
        End Get
        Set(ByVal value As Integer)
            NumeroValue = value
        End Set
    End Property

    Public Property dtCuota As DataTable
    Public Property dtProveedor As DataTable
    Public Property Cuota As Integer
    Public Property RRHH As Boolean = False

    'SC: 01-09-2021 - Variables
    Dim TotalCreditoAsiento As Decimal = 0
    Dim TotalDebitoAsiento As Decimal = 0
    Dim SaldoAsiento As Decimal = 0
    Dim TotalOperativa As Decimal = 0
    Dim TotalDebitoDetalleAsiento As Decimal = 0
    Dim TotalCreditoDetalleAsiento As Decimal = 0
    Dim DesbalanceoAsiento As Boolean = False

    'EVENTOS

    'VARIABLES
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim dtVales As New DataTable

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        'txtProveedor.Conectar()
        'SC:20/04/2022 se reemplaza por linea de abajo para solo listar aquellos proveedores con estado activo
        txtProveedor.Conectar("Select * From VProveedor Where Estado='True' Order By RazonSocial ")

        'Otros
        txtVencimiento.Enabled = False
        OcxImpuesto1.Inicializar()
        CDetalleImpuesto.Inicializar()
        txtCotizacion.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
        txtCotizacion.Inicializar()
        flpRegistradoPor.Visible = False

        'SC 09-08-2021
        txtPlazo.Enabled = False

        'Propiedades
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "GASTO", "GAS")
        vNuevo = True

        'SC: 01-09-2021
        DesbalanceoAsiento = False

        'Funciones
        CargarInformacion()
        
        'Clases
        CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        MostrarCajaChica()

        'Foco
        Me.ActiveControl = txtID
        txtID.txt.Focus()

        Inicializado = True

        'If IDSucursal > 0 Then
        '    cbxSucursal.cbx.SelectedValue = IDSucursal
        '    cbxSucursal.cbx.Text = (CData.GetTable("VSucursal").Select("ID = " & IDSucursal)(0)("Codigo"))
        '    ' CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal"), "ID", "Codigo")

        'End If


        If IDTransaccion <> 0 Then
            If CajaChica Then
                CargarOperacionConFondoFijo(IDTransaccion)
            Else
                CargarOperacion(IDTransaccion)
            End If

        Else
            txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        End If

        If CajaChica = False Then
            chkAcuerdoAsociado.Visible = True
            ChkAcuerdoComercial.Visible = True
            cbxAcuerdoComercial.Visible = True
        Else
            chkAcuerdoAsociado.Visible = False
            ChkAcuerdoComercial.Visible = False
            cbxAcuerdoComercial.Visible = False
        End If



    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, txtProveedor)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, cbxCondicion)
        CSistema.CargaControl(vControles, txtVencimiento)
        'CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxGrupo)
        CSistema.CargaControl(vControles, txtCotizacion)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, cbxDepartamentoEmpresa)
        CSistema.CargaControl(vControles, cbxTipoIVA)
        CSistema.CargaControl(vControles, chkCheque)
        CSistema.CargaControl(vControles, chkEfectivo)
        CSistema.CargaControl(vControles, lklAgregarVale)
        CSistema.CargaControl(vControles, lklEliminarVale)
        CSistema.CargaControl(vControles, txtTimbrado)
        CSistema.CargaControl(vControles, txtVtoTimbrado)
        CSistema.CargaControl(vControles, cbxDeposito)
        CSistema.CargaControl(vControles, cbxCamion)
        CSistema.CargaControl(vControles, cbxChofer)
        CSistema.CargaControl(vControles, chkAcuerdoAsociado)
        CSistema.CargaControl(vControles, ChkAcuerdoComercial)
        CSistema.CargaControl(vControles, cbxAcuerdoComercial)
        CSistema.CargaControl(vControles, chkCuota)
        CSistema.CargaControl(vControles, chkIncluirLibro)
        CSistema.CargaControl(vControles, ChkFE)
        'FA 22/11/2022 - Plan de cuenta
        CSistema.CargaControl(vControles, cbxUnidadNegocio)
        CSistema.CargaControl(vControles, cbxSeccion)

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CONDICION
        cbxCondicion.cbx.Items.Add("CONTADO")
        cbxCondicion.cbx.Items.Add("CREDITO")
        cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'TIPO IVA
        cbxTipoIVA.cbx.Items.Add("DIRECTO")
        cbxTipoIVA.cbx.Items.Add("INDISTINTO")
        cbxTipoIVA.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipoIVA.cbx.SelectedIndex = 0

        'CARGAR CONTROLES
        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal"), "ID", "Codigo")

        'FA 22/11/2022 - Plan de cuenta
        CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, CData.GetTable("vUnidadNegocio", "Estado = 1"), "ID", "Descripcion")

        'Grupo
        'JGR 20140809 Verificar esta carga. Posiblemente hay que cargar todos los items pero a la hora de guardar controlar si el grupo corresponde al usuario
        CSistema.SqlToComboBox(cbxGrupo.cbx, "Select IDGrupo as ID, Grupo From vGrupoUsuario GROUP BY IDGrupo, Grupo order by ID ")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, CData.GetTable("VTipoComprobante", "IDOperacion=" & IDOperacion), "ID", "Codigo")

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Condicion
        cbxCondicion.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CONDICION", "CONTADO")

        'Grupo
        cbxGrupo.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "GRUPO", "")

        'Moneda
        txtCotizacion.cbxMoneda.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MONEDA", "")

        'Forma de Pago Cheque
        chkCheque.Valor = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CHEQUE", "True")

        'Forma de Pago Efectivo
        chkEfectivo.Valor = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "Efectivo", "False")

        'Acuerdo Comercial
        'CSistema.SqlToComboBox(cbxAcuerdoComercial.cbx, CData.GetTable("VAcuerdodeClientes", "Estado='Activo'"), "ID", "NroAcuerdo")

        'Impuestos
        OcxImpuesto1.EstablecerSoloLectura()

        'Configuraciones
        'Deposito
        lblDeposito.Visible = CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDeposito").ToString)
        cbxDeposito.Visible = CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDeposito").ToString)

        'Distribucion
        pnlDistribucion.Visible = CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDistribucion").ToString)

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, New Button, btnBusquedaAvanzada, btnAsiento, vControles, btnModificar)

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            If vNuevo = False Then
                If CajaChica = True Then
                    CargarOperacionConFondoFijo()
                Else
                    CargarOperacion()
                End If
            End If
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CajaChica = True Then
                ID = CSistema.ExecuteScalar("Select IsNull((Select Min(Numero) From VGastoFondoFijo Where IDSucursal=" & cbxSucursal.GetValue & " And Numero>" & ID & " ),0)")
                txtID.txt.Text = ID
                txtID.txt.SelectAll()
                CargarOperacionConFondoFijo()
            Else
                If RRHH Then
                    ID = CSistema.ExecuteScalar("Select IsNull((Select Min(Numero) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.GetValue & " And Numero>" & ID & " And RRHH = 'True'), 0) ")
                Else
                    ID = CSistema.ExecuteScalar("Select IsNull((Select Min(Numero) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.GetValue & " And Numero>" & ID & "And RRHH = 'False'), 0) ")
                End If
                txtID.txt.Text = ID
                txtID.txt.SelectAll()
                CargarOperacion()
            End If

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            'Buscar el Maximo menor de lo ingresado
            If CajaChica = True Then
                ID = CSistema.ExecuteScalar("Select ISNULL((Select Max(Numero) From VGastoFondoFijo Where IDSucursal=" & cbxSucursal.GetValue & " And Numero<" & ID & " ),0)")
                txtID.txt.Text = ID
                txtID.txt.SelectAll()
                CargarOperacionConFondoFijo()
            Else
                If RRHH Then
                    ID = CSistema.ExecuteScalar("Select ISNULL((Select Max(Numero) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.GetValue & " And Numero<" & ID & " And RRHH = 'True'),0) ")
                Else
                    ID = CSistema.ExecuteScalar("Select ISNUll((Select Max(Numero) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.GetValue & " And Numero<" & ID & " And RRHH = 'False'),0) ")
                End If

                txtID.txt.Text = ID
                txtID.txt.SelectAll()
                CargarOperacion()
            End If

        End If

        If e.KeyCode = Keys.End Then

            'If vNuevo = True Then
            '    Exit Sub
            'End If

            Dim ID As Integer
            If CajaChica = True Then
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From GastoFondoFijo Where IDSucursal=" & cbxSucursal.GetValue & " "), Integer)
            Else
                If RRHH Then
                    ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.GetValue & " And CajaChica='False' And RRHH = 'True' "), Integer)
                Else
                    ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.GetValue & " And CajaChica='False' And RRHH = 'False' "), Integer)
                End If

            End If


            txtID.txt.Text = ID
            txtID.txt.SelectAll()

            If CajaChica = True Then
                CargarOperacionConFondoFijo()
            Else
                CargarOperacion()
            End If

        End If

        If e.KeyCode = Keys.Home Then

            'If vNuevo = True Then
            '    Exit Sub
            'End If

            Dim ID As Integer
            If CajaChica = True Then
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From GastoFondoFijo Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)
            Else
                If RRHH Then
                    ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And CajaChica='False' and RRHH = 'True' "), Integer)
                Else
                    ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And CajaChica='False' and RRHH = 'False' "), Integer)
                End If

            End If


            txtID.txt.Text = ID
            txtID.txt.SelectAll()

            If CajaChica = True Then
                CargarOperacionConFondoFijo()
            Else
                CargarOperacion()
            End If

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub GuardarInformacion()

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Condicion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CONDICION", cbxCondicion.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

        'Grupo0
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "GRUPO", cbxGrupo.cbx.Text)

        'Moneda
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", txtCotizacion.cbxMoneda.cbx.Text)

        'Forma de Pago Cheque
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CHEQUE", chkCheque.Valor.ToString)

        'Forma de Pago Efectivo
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "Efectivo", chkEfectivo.Valor.ToString)

    End Sub

    Function SeleccionarRegistro(vTabla As String) As Integer

        SeleccionarRegistro = 0

        Dim vIDTransaccion As Integer

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select IDTransaccion, Sucursal, Numero, [Cod.], Comprobante, Proveedor, Fecha, Grupo, FechaTransaccion, Usuario From " & vTabla & " Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " and RRHH = '" & RRHH.ToString & "' ")

        If dt.Rows.Count = 0 Then
            vIDTransaccion = 0
        End If

        If dt.Rows.Count = 1 Then
            vIDTransaccion = dt.Rows(0)("IDTransaccion").ToString
        End If

        If dt.Rows.Count > 1 Then
            MessageBox.Show("El sistema encontro que hay una duplicacion de numeracion.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)

            Dim frm As New frmGastosDuplicados
            frm.dt = dt
            FGMostrarFormulario(Me, frm, "Duplicacion de Numeracion", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

            If frm.Procesado = False Then
                vIDTransaccion = 0
            End If

            If frm.Procesado = True Then
                vIDTransaccion = frm.IDTransaccion
            End If

        End If

        SeleccionarRegistro = vIDTransaccion

    End Function

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        ctrError.Clear()
        tsslEstado.Text = ""

        'Limpiar detalle

        'Otros
        flpRegistradoPor.Visible = False

        IDTransaccion = 0
        CAsiento.Limpiar()
        CAsiento.InicializarAsiento()
        OcxImpuesto1.IDMoneda = txtCotizacion.cbxMoneda.GetValue
        OcxImpuesto1.Reestablecer()
        CDetalleImpuesto.dt.Rows.Clear()
        cbxCondicion.cbx.SelectedIndex = 0
        txtVencimiento.Enabled = False


        vNuevo = True

        'SC: 01-09-2021
        DesbalanceoAsiento = False
        TotalCreditoAsiento = 0
        TotalDebitoAsiento = 0
        SaldoAsiento = 0

        'FA 23/11/2022 - Plan de cuenta
        cbxUnidadNegocio.cbx.Text = ""
        cbxDepartamentoEmpresa.cbx.Text = ""
        chkGastoMultiple.Enabled = True

        'Cabecera
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()
        txtProveedor.Clear()
        ChkFE.Checked = False
        txtFecha.Clear()
        txtVencimiento.Clear()
        txtTimbrado.Clear()
        txtVtoTimbrado.Clear()
        txtCotizacion.Cargar()
        ChkAcuerdoComercial.Checked = False
        cbxAcuerdoComercial.Enabled = False
        cbxAcuerdoComercial.cbx.Text = ""

        'Distribucion
        cbxDeposito.cbx.Text = ""
        cbxCamion.cbx.Text = ""
        cbxChofer.cbx.Text = ""

        'Otros
        chkCuota.Valor = False
        chkCuota.chk.Checked = False
        Cuota = 0

        'Bloquear Nro de Operacion
        txtID.SoloLectura = True

        chkGastoMultiple.Checked = False
        'Limpiar el asiento
        CAsiento.Inicializar()

        'Poner el foco en el proveedor
        txtProveedor.LimpiarSeleccion()
        txtProveedor.Focus()

        dtVales = CSistema.ExecuteToDataTable("Select *, 'Sel'= 'False','Cancelar'= 'False' from VVale Where ARendir= 'True' And Cancelado= 'False' And Anulado='False'").Copy
        dtCuota = Nothing

        'Limpiar lvvale
        lvVale.Items.Clear()

        'Foco
        cbxSucursal.Focus()

        If CajaChica = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From GastoFondoFijo Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1)"), Integer)
            'SC: 23/08/2021 - Si es FondoFijo establecer Condicion como CONTADO por defecto
            cbxCondicion.cbx.SelectedIndex = 0
        Else
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From GastoTipoComprobante Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1)"), Integer)
        End If

        'Volver a poner los grupos habilitados
        CSistema.SqlToComboBox(cbxGrupo.cbx, CData.GetTable("vGrupoUsuario", "IDUsuario=" & vgIDUsuario), "ID", "Grupo")

        Numero = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From Gasto Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1)"), Integer)

        'Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " IDSucursal=" & cbxSucursal.GetValue)
        'CSistema.SqlToComboBox(cbxDepartamentoEmpresa.cbx, dtDepartamentos, "ID", "Departamento")
        'cbxDepartamentoEmpresa.cbx.Text = ""
        If cbxSucursal.GetValue = 1 Then
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True'")
            'Dim dtDepartamentos As DataTable = CData.GetTable("select ID,Descripcion from vdepartamentoempresa Where Estado= 'True' order by Descripcion")
            CSistema.SqlToComboBox(cbxDepartamentoEmpresa.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamentoEmpresa.cbx.Text = ""
        Else
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True' and IDSucursal=" & cbxSucursal.GetValue)
            CSistema.SqlToComboBox(cbxDepartamentoEmpresa.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamentoEmpresa.cbx.Text = ""
        End If

        txtProveedor.Focus()
        chkAcuerdoAsociado.Valor = False
    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        vNuevo = False

        txtID.SoloLectura = False
        txtID.txt.Focus()

        'Ultimo Registro
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Validar
        'Tipo Comprobante
        If cbxTipoComprobante.Validar("Seleccione correctamente el tipo de comprobante!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            CSistema.MostrarError("Ingrese un número de comprobante!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Sucursal
        If cbxSucursal.Validar("Seleccione correctamente la sucursal!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        

        'Total
        If CDec(OcxImpuesto1.txtTotal.ObtenerValor) <= 0 Then
            Dim mensaje As String = "El importe del documento no es válido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If


        'Timbrado
        If CData.GetTable("VTipoComprobante", " ID=" & cbxTipoComprobante.GetValue)(0)("ComprobanteTimbrado") = True Then
            If txtTimbrado.txt.Text.Trim.Length = 0 Then
                Dim mensaje As String = "Ingrese un número de timbrado!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If CSistema.ControlarTimbrado(txtTimbrado.txt.Text) = False Then
                Exit Function
            End If

            If CSistema.ControlarNroDocumento(txtComprobante.txt.Text) = False Then
                Exit Function
            End If

        End If
        
        'Forma de Pago
        If chkCheque.Valor = False And chkEfectivo.Valor = False Then
            CSistema.MostrarError("Seleccione una forma de pago!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Cuota
        If (chkCuota.Valor = True) And (Cuota = 0) Then
            CSistema.MostrarError("No hay cuotas cargadas!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        If chkAcuerdoAsociado.chk.Checked = False Then
            'Validar el Asiento
            If CAsiento.dtAsiento.Rows.Count = 0 Then
                CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Function
            End If

            'Validar el Asiento
            If CAsiento.dtDetalleAsiento.Rows.Count = 0 Then
                CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Function
            End If

            If CAsiento.ObtenerSaldo <> 0 Then
                CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Function
            End If

            If CAsiento.ObtenerTotal = 0 Then
                CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Function
            End If
        End If

        'Caja
        'If CAsiento.CajaHabilitada = False Then
        '    CSistema.MostrarError("La caja no esta habilidada!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Function
        'End If

        'Unidad de negocio
        If chkGastoMultiple.Checked = False Then
            If cbxUnidadNegocio.cbx.Text.Trim = "" Then
                Dim mensaje As String = "Seleccione correctamente la Unidad de Negocio"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If
        End If

        'Departamento
        If cbxDepartamentoEmpresa.cbx.Text.Trim = "" Then
            Dim mensaje As String = "Seleccione correctamente Departamento"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If Seccion = True Then
            If cbxSeccion.cbx.Text.Trim = "" Then
                Dim mensaje As String = "Seleccione correctamente la Seccion"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

        End If

        'Acuerdo Comercial
        If ChkAcuerdoComercial.Checked = True And cbxAcuerdoComercial.cbx.Text = "" Then
            CSistema.MostrarError("Seleccionar un acuerdo!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Si va a eliminar
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        For Each oRow As DataRow In dtVales.Select("Sel='True'")
            If CDec(oRow("Importe").ToString) > CDec(oRow("Saldo").ToString) Then
                Dim mensaje As String = "El importe del vale no debe exceder al saldo!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If
        Next

        'Fecha
        'If CSistema.ControlFechaOperacion(Me, IDOperacion, "Guardar un gasto con fecha anterior", "", txtComprobante.GetValue, txtFecha.GetValue, OcxImpuesto1.txtTotal.ObtenerValor) = False Then
        '    Exit Function
        'End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        GenerarAsiento()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IDTransaccion As Integer
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Numero", Numero, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroTimbrado", txtTimbrado.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaVencimientoTimbrado", txtVtoTimbrado.GetValueString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDGrupo", cbxGrupo.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepartamentoEmpresa", cbxDepartamentoEmpresa.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@GastoMultiple", chkGastoMultiple.Checked, ParameterDirection.Input)
        If chkGastoMultiple.Checked = False Then
            CSistema.SetSQLParameter(param, "@IDUnidadNegocio", cbxUnidadNegocio.cbx.SelectedValue, ParameterDirection.Input)
        End If

        If Seccion = True Then
            CSistema.SetSQLParameter(param, "@IDSeccion", cbxSeccion.cbx.SelectedValue, ParameterDirection.Input)
        End If

        'Se agrega cuando se carga para diferenciar Factura Electrónica
        If chkFE.Checked = True Then
            CSistema.SetSQLParameter(param, "@EsFE", chkFE.Checked, ParameterDirection.Input)
        End If
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        If cbxTipoIVA.cbx.Text = "DIRECTO" Then
            CSistema.SetSQLParameter(param, "@Directo", "True", ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Directo", "False", ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IncluirLibro", chkIncluirLibro.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Efectivo", chkEfectivo.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cheque", chkCheque.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@RRHH", RRHH.ToString, ParameterDirection.Input)
        If CajaChica = True Then
            CSistema.SetSQLParameter(param, "@CajaChica", "True", ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDAcuerdoComercial", "", ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@CajaChica", "False", ParameterDirection.Input)
            If ChkAcuerdoComercial.Checked = True Then
                CSistema.SetSQLParameter(param, "@IDAcuerdoComercial", cbxAcuerdoComercial.cbx.SelectedValue, ParameterDirection.Input)
            End If

        End If
        If chkCuota.Valor Then
            CSistema.SetSQLParameter(param, "@Cuota", Cuota, ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Cuota", 0, ParameterDirection.Input)
        End If


        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        IndiceOperacion = param.GetLength(0) - 1

        'Moneda
        CSistema.SetSQLParameter(param, "@IDMoneda", txtCotizacion.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(txtCotizacion.txtCotizacion.ObtenerValor), ParameterDirection.Input)

        'Credito
        If cbxCondicion.cbx.SelectedIndex = 0 Then
            CSistema.SetSQLParameter(param, "@Credito", "False".ToString, ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Credito", "True".ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@FechaVencimiento", CSistema.FormatoFechaBaseDatos(txtVencimiento.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        End If

        'Distribucion
        If CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDeposito").ToString) = True Then
            CSistema.SetSQLParameter(param, "@IDDepositoOperacion", cbxDeposito.cbx, ParameterDirection.Input)
        End If

        If CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDistribucion").ToString) = True Then
            CSistema.SetSQLParameter(param, "@IDCamion", cbxCamion.cbx, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDChofer", cbxChofer.cbx, ParameterDirection.Input)
        End If

        'Totales
        CSistema.SetSQLParameter(param, "@Total", OcxImpuesto1.TotalBD, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalImpuesto", OcxImpuesto1.TotalImpuestoBD, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", OcxImpuesto1.TotalDiscriminadoBD, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpGasto", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion:  " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From Gasto Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpGasto", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        'Si es nuevo
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then

            'Cargamos el DetalleImpuesto
            OcxImpuesto1.Generar(IDTransaccion)
            CDetalleImpuesto.dt = OcxImpuesto1.dtImpuesto
            CDetalleImpuesto.IDMoneda = OcxImpuesto1.IDMoneda
            CDetalleImpuesto.Decimales = OcxImpuesto1.Decimales
            CDetalleImpuesto.Guardar(IDTransaccion)

            'Cargamos el asiento
            If chkAcuerdoAsociado.chk.Checked = False Then
                CAsiento.IDTransaccion = IDTransaccion
                CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

            End If

            If chkAcuerdoAsociado.Valor = True And vCantidadProducto <> 0 And vIDAcuerdo <> 0 Then
                Dim Sql As String = ""
                Sql = "Insert into GastoProducto values(" & IDTransaccion & "," & vIDAcuerdo & "," & CSistema.FormatoMonedaBaseDatos(vCantidadProducto, True) & ",'" & vObservacionProducto & "')"
                Dim RowCount As Integer = CSistema.ExecuteNonQuery(Sql, VGCadenaConexion)
                Sql = "Exec SpAsientoGastoMateriaPrima " & IDTransaccion
                CSistema.ExecuteNonQuery(Sql, VGCadenaConexion)

            End If

            If ChkAcuerdoComercial.Checked = True Then
                AsociarGastoAcuerdo(IDTransaccion)
            End If

        End If

        Dim Procesar As Boolean = True

        If CajaChica = True Then

            'Guardar detalle
            If IDTransaccion > 0 Then

                If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then

                    'Insertamos el Detalle
                    InsertarDetalle(ERP.CSistema.NUMOperacionesRegistro.INS, IDTransaccion)

                    If lvVale.Items.Count = 0 Then
                        InsertarDetalleFondoFijo(IDTransaccion)
                    End If


                End If

            End If

        End If

        'Cuotas
        If IDTransaccion > 0 And Cuota > 0 Then

            If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then

                'Insertamos las cuotas
                InsertarCuotas(ERP.CSistema.NUMOperacionesRegistro.INS, IDTransaccion)

            End If

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        'SC: 01-09-2021 -Valida Asiento Desbalanceado al GUARDAR
        TotalCreditoAsiento = CSistema.ExecuteScalar("Select IsNull((Select Credito From VAsiento Where IDTransaccion = " & IDTransaccion & "), 0 )")
        TotalDebitoAsiento = CSistema.ExecuteScalar("Select IsNull((Select Debito From VAsiento Where IDTransaccion = " & IDTransaccion & "), 0 )")
        SaldoAsiento = CSistema.ExecuteScalar("Select IsNull((Select Saldo From VAsiento Where IDTransaccion = " & IDTransaccion & "), 0 )")

        TotalDebitoDetalleAsiento = CSistema.ExecuteScalar("Select IsNull((Select sum(Debito) From VDetalleAsiento Where IDTransaccion = " & IDTransaccion & "and Debito <> 0), 0 )")
        TotalCreditoDetalleAsiento = CSistema.ExecuteScalar("Select IsNull((Select sum(Credito) From VDetalleAsiento Where IDTransaccion = " & IDTransaccion & "and Credito <> 0), 0 )")


        TotalOperativa = txtCotizacion.txtCotizacion.ObtenerValor * CSistema.FormatoMoneda3Decimales(OcxImpuesto1.Total, True)

        If TotalCreditoAsiento <> TotalDebitoAsiento Then

            DesbalanceoAsiento = True
        ElseIf SaldoAsiento <> 0 Then

            DesbalanceoAsiento = True
        ElseIf TotalDebitoAsiento <> TotalOperativa Then

            DesbalanceoAsiento = True
        ElseIf TotalCreditoAsiento <> TotalOperativa Then

            DesbalanceoAsiento = True
        ElseIf TotalCreditoDetalleAsiento <> TotalOperativa Then

            DesbalanceoAsiento = True
        ElseIf TotalDebitoDetalleAsiento <> TotalOperativa Then

            DesbalanceoAsiento = True
        End If

        If DesbalanceoAsiento = True Then
            Dim mensaje As String = "ASIENTO DESBALANCEADO. FAVOR VERIFICAR! "
            MessageBox.Show(mensaje, "ATENCION", MessageBoxButtons.OK, MessageBoxIcon.Error)
            tsslEstado.Text = mensaje
            VisualizarAsiento()
            'Exit Sub

        End If

        txtID.SoloLectura = False
        Cancelar()

    End Sub

    Sub CalcularTotales()

        OcxImpuesto1.CargarValores(CDetalleImpuesto.dt)

    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "Compra: " & txtID.txt.Text & "  -  " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text & "  -  " & txtProveedor.txtRazonSocial.Text

            'Dim SQL As String
            If CajaChica = False Then
                'SQL = "Select IsNull((Select IDTransaccion From GastoTipoComprobante Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ), 0 )"
                IDTransaccion = SeleccionarRegistro("VGastoTipoComprobante")
            Else
                'SQL = "Select IsNull((Select IDTransaccion From GastoFondoFijo Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ), 0 )"
                IDTransaccion = SeleccionarRegistro("VGastoFondoFijo")
            End If

            'Dim IDTransaccion As Integer = CSistema.ExecuteScalar(SQL)
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)

        Else

            If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(cbxTipoComprobante, mensaje)
                ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If txtProveedor.Seleccionado = False Then
                Dim mensaje As String = "Seleccione correctamente el proveedor!"
                ctrError.SetError(txtProveedor, mensaje)
                ctrError.SetIconAlignment(txtProveedor, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "Compra: " & txtID.txt.Text & "  -  " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text & "  -  " & txtProveedor.txtRazonSocial.Text
            frm.GastoMultiple = chkGastoMultiple.Checked

            'Para que muestre solo las cuenta contables que estan asignadas al fondo fijo
            If CajaChica = True Then
                Dim dtCCFF As New DataTable
                Dim CuentasContables As String = "ID = 0 "
                dtCCFF = CSistema.ExecuteToDataTable("Select IDCuentaContable from FondoFijoCuentaContable where IdFondoFijo =" & cbxGrupo.GetValue & " ").Copy

                For Each oRow As DataRow In dtCCFF.Rows
                    Dim param As New DataTable
                    CuentasContables = CuentasContables & " or ID =" & oRow("IDCuentaContable").ToString & "    "
                Next
                frm.Filtrar = True
                frm.whereFiltro = CuentasContables
            Else
                If RRHH Then
                    Dim dtCCFF As New DataTable
                    Dim CuentasContables As String = "ID = 0 "
                    dtCCFF = CSistema.ExecuteToDataTable("Select IDCuentaContable from RRHHCuentaContable")

                    For Each oRow As DataRow In dtCCFF.Rows
                        Dim param As New DataTable
                        CuentasContables = CuentasContables & " or ID =" & oRow("IDCuentaContable").ToString & "    "
                    Next
                    frm.Filtrar = True
                    frm.whereFiltro = CuentasContables
                Else
                    frm.Filtrar = False
                    frm.whereFiltro = ""
                End If

            End If

            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
            CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            CAsiento.NroCaja = frm.CAsiento.NroCaja
            CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                CAsiento.dtAsiento.Clear()
                CAsiento.dtDetalleAsiento.Clear()
                CAsiento.dtImpuesto.Clear()
                VisualizarAsiento()
            End If

        End If


    End Sub

    Sub GenerarAsiento()

        If chkAcuerdoAsociado.chk.Checked Then
            Exit Sub
        End If
        'EstablecerCabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = CData.GetTable("VSucursal", " ID = " & cbxSucursal.GetValue).Rows(0)("IDCiudad").ToString
        oRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
        oRow("Fecha") = txtFecha.GetValue
        oRow("IDMoneda") = txtCotizacion.Registro("ID")
        oRow("Cotizacion") = txtCotizacion.txtCotizacion.ObtenerValor
        oRow("IDTipoComprobante") = cbxTipoComprobante.cbx.SelectedValue
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = txtComprobante.txt.Text
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text
        oRow("Detalle") = txtObservacion.txt.Text

        'SC: 20-09-2021 Guaraniza cabecera asiento
        If txtCotizacion.txtCotizacion.ObtenerValor <> 1 Then
            oRow("Total") = txtCotizacion.txtCotizacion.ObtenerValor * CSistema.FormatoMoneda3Decimales(OcxImpuesto1.Total, True)
            oRow("Debito") = txtCotizacion.txtCotizacion.ObtenerValor * CSistema.FormatoMoneda3Decimales(OcxImpuesto1.Total, True)
            oRow("Credito") = txtCotizacion.txtCotizacion.ObtenerValor * CSistema.FormatoMoneda3Decimales(OcxImpuesto1.Total, True)

        Else
            'SC - Agregue estos 2 que faltaban, igualados al total - 01-09-2021
            oRow("Total") = OcxImpuesto1.txtTotal.ObtenerValor
            oRow("Debito") = OcxImpuesto1.txtTotal.ObtenerValor
            oRow("Credito") = OcxImpuesto1.txtTotal.ObtenerValor
        End If

        oRow("CajaChica") = CajaChica
        oRow("GastosMultiples") = chkGastoMultiple.Checked
        If chkGastoMultiple.Checked = True Then
            oRow("IDUnidadNegocio") = 0
            oRow("IDCentroCosto") = 0
            oRow("IDDepartamento") = cbxDepartamentoEmpresa.GetValue
            oRow("IDSeccion") = cbxSeccion.GetValue
        Else
            oRow("IDUnidadNegocio") = cbxUnidadNegocio.GetValue
            oRow("IDDepartamento") = cbxDepartamentoEmpresa.GetValue
            oRow("IDSeccion") = cbxSeccion.GetValue
        End If

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)

        OcxImpuesto1.Generar()
        CAsiento.dtImpuesto = OcxImpuesto1.dtImpuesto.Copy
        CAsiento.IDProveedor = txtProveedor.Registro("ID").ToString
        CAsiento.IDSucursal = cbxSucursal.GetValue
        CAsiento.IDGrupo = cbxGrupo.GetValue
        If chkAcuerdoAsociado.Valor = True And vCantidadProducto <> 0 Then
            CAsiento.CCaRecibir = vCCProductoArecibir
        Else
            CAsiento.CCaRecibir = ""
        End If
        'CAsiento.ImporteGasto =
        CAsiento.Generar()


    End Sub

    Function InsertarDetalle(ByVal Operacion As CSistema.NUMOperacionesRegistro, ByVal vIDTransaccion As Integer) As Boolean

        InsertarDetalle = True

        For Each oRow As DataRow In dtVales.Select("Sel='True'")

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDGrupo", cbxGrupo.cbx.SelectedValue, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccionVale", (oRow("IDTransaccion").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccionGasto", vIDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Saldo", CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpGastoVale", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
                Return False
            End If

        Next

    End Function

    Function InsertarDetalleFondoFijo(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalleFondoFijo = True

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDGrupo", cbxGrupo.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTransaccionGasto", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(CSistema.gridSumColumn(OcxImpuesto1.dg, "colTotal")), ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Insertar el detalle
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpDetalleRendicionFondoFijo", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            Return False
        End If


    End Function

    Function InsertarCuotas(ByVal Operacion As CSistema.NUMOperacionesRegistro, ByVal vIDTransaccion As Integer) As Boolean

        InsertarCuotas = True

        For Each oRow As DataRow In dtCuota.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", vIDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", oRow("ID").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Saldo", CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Vencimiento", CSistema.FormatoFechaBaseDatos(oRow("Vencimiento").ToString, True, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cancelado", oRow("Cancelado").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpCuota", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
                Return False
            End If

        Next

    End Function

    Sub Eliminar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If


        If CajaChica = True Then

            'Guardar detalle
            If IDTransaccion > 0 Then

                If Operacion = ERP.CSistema.NUMOperacionesRegistro.DEL Then

                    'Insertamos el Detalle
                    If lvVale.Items.Count <> 0 Then
                        InsertarDetalle(ERP.CSistema.NUMOperacionesRegistro.DEL, IDTransaccion)
                    End If

                End If

            End If

        End If


        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.DEL.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Eliminar
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpGasto", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub CargaValeARendir()

        Dim frm As New frmValeAResindir
        dtVales = CSistema.ExecuteToDataTable("Select *, 'Sel'= 'False','Cancelar'= 'False' from VVale Where ARendir= 'True' And Cancelado= 'False' And Anulado='False'").Copy
        frm.dt = dtVales
        frm.IDGrupo = cbxGrupo.GetValue
        frm.IDSucursal = cbxSucursal.GetValue
        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Aplicacion de Vales", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False, False)

        dtVales = frm.dt

        CargarVale()

    End Sub

    Sub MostrarCajaChica()

        If CajaChica = True Then
            gbxVale.Visible = True
            cbxCondicion.cbx.SelectedIndex = 1
        End If

        If CajaChica = False Then
            gbxVale.Visible = False
            chkAcuerdoAsociado.Visible = True
        End If

    End Sub

    Sub CargarVale()

        lvVale.Items.Clear()

        Dim Total As Decimal = 0

        For Each oRow As DataRow In dtVales.Rows
            If oRow("Sel") = True Then

                Dim item As ListViewItem = New ListViewItem(oRow("IDTransaccion").ToString)
                item.SubItems.Add(oRow("Fec").ToString)
                item.SubItems.Add(oRow("Motivo").ToString)
                item.SubItems.Add(oRow("Nombre").ToString)
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Total").ToString))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Saldo").ToString))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Importe").ToString))
                Total = Total + CDec(oRow("Importe").ToString)

                lvVale.Items.Add(item)

            End If
        Next

        txtTotalComprobante.SetValue(Total)
        txtCantidadComprobante.SetValue(lvVale.Items.Count)

        'CalcularTotales()

    End Sub

    Sub EliminarVale()

        For Each item As ListViewItem In lvVale.SelectedItems

            For Each oRow As DataRow In dtVales.Select(" IDTransaccion = " & item.Text & " ")
                'CAsientoContableOrdenPago.EliminarVenta(oRow("Importe").ToString, oRow("IDTipoComprobante").ToString, oRow("IDMoneda").ToString, oRow("Credito").ToString)
                oRow("Sel") = False
                Exit For
            Next

        Next

        CargarVale()

    End Sub

    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'Otros

        Dim frm As New frmConsultaGasto
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.CajaChica = CajaChica
        frm.ShowDialog(Me)

        If frm.IDTransaccion > 0 Then
            If CajaChica = True Then
                CargarOperacionConFondoFijo(frm.IDTransaccion)
            Else
                CargarOperacion(frm.IDTransaccion)
            End If
        End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            'IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From GastoTipoComprobante Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )")
            IDTransaccion = SeleccionarRegistro("VGastoTipoComprobante")
            vIDTransaccion = IDTransaccion
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Listamos todos los grupos para la consulta
        CSistema.SqlToComboBox(cbxGrupo.cbx, "Select IDGrupo as ID, Grupo From vGrupoUsuario GROUP BY IDGrupo, Grupo order by ID ")

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VGastoTipoComprobante Where IDTransaccion=" & IDTransaccion) ' & " And IDSucursal = " & cbxSucursal.GetValue)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        cbxSucursal.cbx.SelectedValue = oRow("IDSucursal")
        cbxSucursal.cbx.Text = (CData.GetTable("VSucursal").Select("ID = " & oRow("IDSucursal"))(0)("Codigo"))

        txtID.txt.Text = oRow("Numero").ToString
        'txtProveedor.SetValue(oRow("IDProveedor").ToString)

        'SC: 19/04/2022 - Carga campo por campo, porque la vista de Proveedor solo trae 'ACTIVO'
        txtProveedor.txtID.txt.Text = oRow("IDProveedor").ToString
        txtProveedor.txtRazonSocial.txt.Text = oRow("Proveedor").ToString
        txtProveedor.txtReferencia.txt.Text = oRow("Referencia").ToString
        txtProveedor.txtRUC.txt.Text = oRow("Ruc").ToString
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        'Es Factura Electrónica
        If oRow("FacturaElectronica") <> 0 Then
            ChkFE.Checked = True
        Else
            ChkFE.Checked = False
        End If
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtFecha.SetValue(oRow("Fecha"))
        txtTimbrado.txt.Text = oRow("NroTimbrado").ToString
        cbxGrupo.cbx.Text = oRow("Grupo").ToString
        txtVtoTimbrado.SetValue(oRow("FechaVencimientoTimbrado").ToString)

        'Distribucion
        cbxDeposito.cbx.Text = oRow("Deposito").ToString
        cbxCamion.cbx.Text = oRow("Camion").ToString
        cbxChofer.cbx.Text = oRow("Chofer").ToString

        cbxDepartamentoEmpresa.txt.Text = oRow("DepartamentoEmpresa").ToString
        cbxUnidadNegocio.txt.Text = oRow("UnidadNegocio").ToString
        chkGastoMultiple.Checked = oRow("GastoMultiple").ToString

        Departamento = oRow("DepartamentoEmpresa").ToString
        UnidadNegocio = oRow("UnidadNegocio").ToString
        cbxSeccion.cbx.Text = oRow("Seccion").ToString

        'Condicion
        If CBool(oRow("CREDITO").ToString) = False Then
            txtVencimiento.txt.Clear()
            cbxCondicion.cbx.SelectedIndex = 0
        Else
            txtVencimiento.SetValue(CDate(oRow("FechaVencimiento").ToString))
            'calcular plazo
            cbxCondicion.cbx.SelectedIndex = 1
        End If

        'Cotizacion
        txtCotizacion.SetValue(oRow("Moneda").ToString, oRow("Cotizacion").ToString)
        txtObservacion.txt.Text = oRow("Observacion").ToString

        'If ChkAcuerdoComercial.Checked = True Then
        '    cbxAcuerdoComercial.cbx.Text = oRow("NroAcuerdoComercial").ToString
        'Else
        '    cbxAcuerdoComercial.cbx.Text = ""
        'End If
        If oRow("NroAcuerdoComercial").ToString <> 0 Then
            ChkAcuerdoComercial.Checked = True
            cbxAcuerdoComercial.cbx.Text = oRow("NroAcuerdoComercial").ToString
        Else
            ChkAcuerdoComercial.Checked = False
            cbxAcuerdoComercial.cbx.Text = ""
        End If

        If CBool(oRow("Directo").ToString) = False Then
            cbxTipoIVA.cbx.SelectedIndex = 1
        Else
            cbxTipoIVA.cbx.SelectedIndex = 0
        End If

        chkIncluirLibro.Valor = oRow("IncluirLibro")

        If chkGastoMultiple.Checked = True Then
            lblDepartamento.Visible = False
            lblDepartamentoSolicitante.Visible = True
        Else
            lblDepartamento.Visible = True
            lblDepartamentoSolicitante.Visible = False
        End If

        Try
            chkEfectivo.Valor = oRow("Efectivo").ToString
            chkCheque.Valor = oRow("Cheque")
        Catch ex As Exception
            chkEfectivo.Valor = False
            chkCheque.Valor = True
        End Try

        'Cuota
        If oRow("Cuota") > 0 Then
            chkCuota.Valor = True
            dtCuota = CSistema.ExecuteToDataTable("Select * From VCuota Where IDTransaccion=" & vIDTransaccion)
        Else
            chkCuota.Valor = False
        End If

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        'Cargamos el detalle
        OcxImpuesto1.CargarValores(vIDTransaccion)

        'Cargamos GastoVale
        dtVales = CSistema.ExecuteToDataTable("Select * ,'Sel'='True' from VGastoVale Where IDTransaccionGasto= " & vIDTransaccion).Copy
        CargarVale()

        'Inicializamos el Asiento
        ' CAsiento.Limpiar()
        chkAcuerdoAsociado.Valor = CType(CSistema.ExecuteScalar("select 'valor'=(case when count(*) = 0 then 'False' else 'True' end) from GastoProducto where IDTransaccionGasto =" & IDTransaccion), Boolean)
        chkGastoMultiple.Enabled = False
    End Sub

    Sub CargarOperacionConFondoFijo(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            'IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From GastoFondoFijo Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )")
            IDTransaccion = SeleccionarRegistro("VGastoFondoFijo")
            vIDTransaccion = IDTransaccion
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
        'Grupo
        CSistema.SqlToComboBox(cbxGrupo.cbx, "Select IDGrupo as ID, Grupo From vGrupoUsuario GROUP BY IDGrupo, Grupo order by ID ")

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VGastoFondoFijo Where IDTransaccion=" & IDTransaccion) '& " And IDSucursal = " & cbxSucursal.GetValue)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnicos."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        cbxSucursal.cbx.SelectedValue = oRow("IDSucursal")
        cbxSucursal.cbx.Text = (CData.GetTable("VSucursal").Select("ID = " & oRow("IDSucursal"))(0)("Codigo"))

        txtID.txt.Text = oRow("Numero").ToString
        txtProveedor.SetValue(oRow("IDProveedor").ToString)
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        'Es Factura Electrónica
        If oRow("FacturaElectronica") <> 0 Then
            ChkFE.Checked = True
        Else
            ChkFE.Checked = False
        End If
        txtFecha.SetValue(oRow("Fecha"))
        txtTimbrado.SetValue(oRow("NroTimbrado"))
        'cbxGrupo.SoloLectura = False
        txtTimbrado.txt.Text = oRow("NroTimbrado").ToString
        cbxGrupo.cbx.Text = oRow("Grupo").ToString
        txtVtoTimbrado.SetValue(oRow("FechaVencimientoTimbrado").ToString)

        'Condicion
        If CBool(oRow("CREDITO").ToString) = False Then
            txtVencimiento.txt.Clear()
            cbxCondicion.cbx.SelectedIndex = 0
        Else
            txtVencimiento.SetValue(CDate(oRow("FechaVencimiento").ToString))
            'calcular plazo
            cbxCondicion.cbx.SelectedIndex = 1
        End If

        'cbxSucursal.txt.Text = oRow("Sucursal").ToString

        'Cotizacion
        txtCotizacion.SetValue(oRow("Moneda").ToString, oRow("Cotizacion").ToString)
        txtObservacion.txt.Text = oRow("Observacion").ToString
        cbxUnidadNegocio.txt.Text = oRow("UnidadNegocio").ToString
        cbxDepartamentoEmpresa.txt.Text = oRow("DepartamentoEmpresa").ToString
        cbxSeccion.cbx.Text = oRow("Seccion").ToString

        Departamento = oRow("DepartamentoEmpresa").ToString
        UnidadNegocio = oRow("UnidadNegocio").ToString

        If CBool(oRow("Directo").ToString) = False Then
            cbxTipoIVA.cbx.SelectedIndex = 1
        Else
            cbxTipoIVA.cbx.SelectedIndex = 0
        End If

        chkIncluirLibro.Valor = oRow("IncluirLibro")
        chkEfectivo.Valor = oRow("Efectivo")
        chkCheque.Valor = oRow("Cheque")

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        'Cargamos el detalle
        OcxImpuesto1.CargarValores(vIDTransaccion)

        'Cargamos GastoVale
        dtVales = CSistema.ExecuteToDataTable("Select * ,'Sel'='True' from VGastoVale Where IDTransaccionGasto= " & vIDTransaccion).Copy
        CargarVale()

        'Inicializamos el Asiento
        ' CAsiento.Limpiar()

    End Sub

    Sub LibroCompra()

        Dim dttemp As DataTable = CData.GetTable("VTipoComprobante", "ID=" & cbxTipoComprobante.GetValue)

        If dttemp Is Nothing Then
            Exit Sub
        End If

        If dttemp.Rows.Count = 0 Then
            Exit Sub
        End If

        chkIncluirLibro.Valor = dttemp.Rows(0)("Libro")

    End Sub

    Sub VizualizarCuota()

        Dim frm As New frmCuota
        frm.Total = OcxImpuesto1.Total
        frm.FechaInicial = txtFecha.GetValue
        frm.dt = dtCuota
        frm.Cuotas = Cuota
        frm.IDMoneda = txtCotizacion.cbxMoneda.GetValue
        If vNuevo = True Then
            frm.SoloConsulta = False

            Select Case cbxCondicion.cbx.SelectedIndex
                Case 0 ' CONTADO
                    frm.FechaInicial = txtFecha.GetValue
                Case 1 ' CREDITO
                    frm.FechaInicial = txtVencimiento.GetValue
                Case Else ' SIN DEFINIR
                    frm.FechaInicial = txtFecha.GetValue
            End Select

        Else
            frm.SoloConsulta = True
        End If

        FGMostrarFormulario(Me, frm, "Cuotas", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        dtCuota = frm.dt
        Cuota = frm.Cuotas

    End Sub
    Sub CargarProducto()

        'If txtProveedor.Seleccionado = False Then
        '    Exit Sub
        'End If

        If txtProveedor.txtID.txt.Text = "" Then
            Exit Sub
        End If

        Dim frm As New frmGastoProducto
        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.IDTransaccion = IDTransaccion
        If vNuevo = False Then
            frm.CargarOperacion(IDTransaccion)
        End If
        'frm.IDProveedor = txtProveedor.Registro("ID").ToString
        frm.IDProveedor = txtProveedor.txtID.txt.Text
        FGMostrarFormulario(Me, frm, "Productos: ", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)
        vCantidadProducto = frm.Cantidad
        vCCProductoArecibir = frm.CCaRecibir
        vIDAcuerdo = frm.IDAcuerdo
        vObservacionProducto = frm.Observacion
        txtID.Focus()

    End Sub

    Private Sub frmGasto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub frmGasto_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
        'FA 20/06/2023
        LiberarMemoria()
    End Sub

    Private Sub frmGasto_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Enter Then

            If cbxCondicion.Focused = True Then
                Exit Sub
            End If

            CSistema.SelectNextControl(Me, e.KeyCode)
        End If

    End Sub

    Private Sub txtProveedor_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtProveedor.KeyUp

        If e.KeyCode = Keys.Escape Then
            txtProveedor.LimpiarSeleccion()
            txtProveedor.OcultarLista()
        End If

    End Sub

    Private Sub txtProveedor_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProveedor.Leave
        txtProveedor.OcultarLista()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If CSistema.ExecuteScalar("select count(*) from Cotizacion where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False) & "' and IDMoneda = " & txtCotizacion.Registro("ID")) = 0 And txtCotizacion.Registro("ID") > 1 Then
            MessageBox.Show("No se ha fijado cotizacion para la moneda seleccionada.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
        End If
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Eliminar(ERP.CSistema.NUMOperacionesRegistro.DEL)
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada

        If txtID.SoloLectura = True Then
            Exit Sub
        End If

        ManejarTecla(e)
    End Sub

    Private Sub cbxTipoComprobante_Editar(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxTipoComprobante.Editar

        Dim frm As New frmTipoComprobante
        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        cbxTipoComprobante.cbx.DataSource = Nothing
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=20")

    End Sub

    Private Sub cbxSucursal_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxSucursal.Leave
        If Inicializado = False Then
            Exit Sub
        End If
        If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxSucursal.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        If Inicializado = False Then
            Exit Sub
        End If
        'If IDTransaccion = 0 Then
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        'End If

        cbxDeposito.DataSource = Nothing
        cbxDepartamentoEmpresa.DataSource = Nothing

        If cbxSucursal.GetValue = 0 Then
            Exit Sub
        End If

        Dim dt As DataTable = CData.GetTable("VDeposito", " IDSucursal=" & cbxSucursal.GetValue)
        CSistema.SqlToComboBox(cbxDeposito.cbx, dt, "ID", "Deposito")


        Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " IDSucursal=" & cbxSucursal.GetValue)
        CSistema.SqlToComboBox(cbxDepartamentoEmpresa.cbx, dtDepartamentos, "ID", "Departamento")

    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklAgregarVale.LinkClicked
        CargaValeARendir()
    End Sub

    Private Sub lklEliminarVenta_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarVale.LinkClicked

        EliminarVale()

    End Sub

    Private Sub cbxCondicion_Leave(sender As System.Object, e As System.EventArgs) Handles cbxCondicion.Leave


        If cbxCondicion.cbx.SelectedIndex = 0 Then
            txtVencimiento.Enabled = False
            txtVencimiento.txt.Text = txtFecha.GetValue
            cbxGrupo.Focus()
        Else

            txtVencimiento.Enabled = True
            txtVencimiento.Focus()
        End If

    End Sub

    Private Sub cbxTipoComprobante_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoComprobante.Leave
        LibroCompra()
    End Sub

    Private Sub txtCotizacion_CambioMoneda() Handles txtCotizacion.CambioMoneda
        txtCotizacion.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
        txtCotizacion.Recargar()
        OcxImpuesto1.SetIDMoneda(txtCotizacion.Registro("ID"))
    End Sub

    Private Sub chkCuota_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCuota.PropertyChanged
        btnCuota.Enabled = value
    End Sub

    Private Sub btnCuota_Click(sender As System.Object, e As System.EventArgs) Handles btnCuota.Click
        VizualizarCuota()
    End Sub

    'SC 09-08-2021 - Se comenta porque se anexa a otra rutina las lineas de abajo
    'Private Sub txtFecha_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtFecha.TeclaPrecionada
    '    txtCotizacion.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
    '    txtCotizacion.Recargar()
    '    OcxImpuesto1.SetIDMoneda(txtCotizacion.Registro("ID"))
    'End Sub

    Private Sub chkAcuerdoAsociado_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkAcuerdoAsociado.PropertyChanged
        If chkAcuerdoAsociado.chk.Checked = True Then
            btnVerProducto.Visible = True
            ChkAcuerdoComercial.Enabled = False
            cbxAcuerdoComercial.Enabled = False
        Else
            btnVerProducto.Visible = False
            ChkAcuerdoComercial.Enabled = True
            cbxAcuerdoComercial.Enabled = True
        End If
    End Sub

    Private Sub btnVerProducto_Click(sender As System.Object, e As System.EventArgs) Handles btnVerProducto.Click
        CargarProducto()
    End Sub

    Private Sub ChkAcuerdoComercial_CheckedChanged(sender As Object, e As EventArgs) Handles ChkAcuerdoComercial.CheckedChanged
        If ChkAcuerdoComercial.Checked = True Then
            chkAcuerdoAsociado.Enabled = False
            cbxAcuerdoComercial.Enabled = True
            If vNuevo = True Then
                'Agregado para traer Acuerdos asociados con el proveedor
                'CSistema.SqlToComboBox(cbxAcuerdoComercial, "select ID, NroAcuerdo From VAcuerdodeClientes where Estado = 'Activo' and IDProveedor =" & txtProveedor.Registro("ID").ToString & " Order by NroAcuerdo Desc")
                CSistema.SqlToComboBox(cbxAcuerdoComercial, "select ID, NroAcuerdo From VAcuerdodeClientes where Estado = 'Activo' and (datediff(day, VigenciaHasta, getdate())) <= 90 and IDProveedor =" & txtProveedor.Registro("ID").ToString & " Order by NroAcuerdo Desc")
            End If
        Else
            chkAcuerdoAsociado.Enabled = True
            cbxAcuerdoComercial.Enabled = False
            cbxAcuerdoComercial.cbx.Text = ""

        End If
    End Sub

    'Private Sub chkAcuerdoComercial_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean)
    '    If ChkAcuerdoComercial.Checked = True Then
    '        chkAcuerdoAsociado.Visible = False
    '    Else
    '        chkAcuerdoAsociado.Visible = True
    '    End If
    'End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click

        Dim frm As New frmModificarGasto
        frm.Condicion = cbxCondicion.cbx.Text
        frm.Vencimiento = txtVencimiento.txt.Text
        frm.Observacion = txtObservacion.txt.Text
        frm.IDTransaccion = IDTransaccion
        frm.IDOperacion = IDOperacion
        frm.Timbrado = txtTimbrado.txt.Text
        frm.VtoTimbrado = txtVtoTimbrado.txt.Text
        'frm.ChkFE.Checked = True
        If ChkFE.Checked = True Then
            frm.ChkFE.Checked = True
        Else
            frm.ChkFE.Checked = False
        End If
        frm.IDSucursal = cbxSucursal.cbx.SelectedValue

        If chkGastoMultiple.Checked = True Then
            frm.GastosVarios = True
        Else
            frm.GastosVarios = False
        End If

        frm.UnidadNegocio = UnidadNegocio
        frm.Departamento = Departamento

        FGMostrarFormulario(Me, frm, "Modificar Gasto", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False, False)
        CargarOperacion()
    End Sub

    'SC 09-08-2021 Nueva rutina para calcular fecha de vencimiento segun plazo cuando condición sea CREDITO
    Private Sub txtFecha_Leave(sender As Object, e As EventArgs) Handles txtFecha.Leave
        'Rutina para calcular fecha plazo automáticamente
        CalcularPlazo()
        cbxCondicion.Focus()

    End Sub

    'SC 09-08-2021 Nueva rutina para calcular fecha de vencimiento segun plazo cuando condición sea CREDITO
    Private Sub txtFecha_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtFecha.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            If txtVencimiento.Enabled = False And txtPlazo.Enabled = False Then
                cbxGrupo.Focus()
            Else
                txtVencimiento.Focus()
            End If
        End If

        If e.KeyCode = Keys.Tab Then
            If txtVencimiento.Enabled = False And txtPlazo.Enabled = False Then
                cbxGrupo.Focus()
            Else
                txtVencimiento.Focus()
            End If
        End If
        txtCotizacion.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
        txtCotizacion.Recargar()
        OcxImpuesto1.SetIDMoneda(txtCotizacion.Registro("ID"))
    End Sub

    'SC 09-08-2021 Nueva rutina para calcular fecha de vencimiento segun plazo cuando condición sea CREDITO
    Sub CalcularPlazo()
        txtVencimiento.SetValue(DateAdd(DateInterval.Day, CDec(txtPlazo.ObtenerValor), txtFecha.GetValue))
        txtVencimiento.Enabled = True
    End Sub

    'SC 09-08-2021 Nueva rutina para calcular fecha de vencimiento segun plazo cuando condición sea CREDITO
    Private Sub txtProveedor_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProveedor.ItemSeleccionado
        If vNuevo = True Then
            ObtenerInformacionProveedor()

        End If
    End Sub

    'SC 09-08-2021 Nueva rutina para calcular fecha de vencimiento segun plazo cuando condición sea CREDITO
    Sub ObtenerInformacionProveedor()
        Dim oRow As DataRow = txtProveedor.Registro

        'Configuraciones
        cbxCondicion.cbx.Text = oRow("CONDICION").ToString
        If cbxCondicion.cbx.SelectedIndex = 0 Then
            cbxCondicion.Enabled = False
            cbxCondicion.SoloLectura = True
        Else
            cbxCondicion.Enabled = True
            cbxCondicion.SoloLectura = False
        End If
        txtPlazo.txt.Text = oRow("PlazoCredito").ToString

        'SC: 23/08/2021 - Si es FondoFijo establecer Condicion como CONTADO por defecto
        If CajaChica = True Then
            cbxCondicion.cbx.SelectedIndex = 0
        End If

    End Sub

    Private Sub txtVencimiento_Leave(sender As Object, e As EventArgs) Handles txtVencimiento.Leave
        cbxGrupo.Focus()
    End Sub

    Private Sub chkGastoMultiple_CheckedChanged(sender As Object, e As EventArgs) Handles chkGastoMultiple.CheckedChanged
        If vNuevo = True Then
            If chkGastoMultiple.Checked = True Then
                cbxUnidadNegocio.SoloLectura = True
                lblDepartamento.Visible = False
                lblDepartamentoSolicitante.Visible = True
                'cbxDepartamentoEmpresa.SoloLectura = True
            Else
                cbxUnidadNegocio.SoloLectura = False
                lblDepartamento.Visible = True
                lblDepartamentoSolicitante.Visible = False
                'cbxDepartamentoEmpresa.SoloLectura = False
            End If
        End If
    End Sub

    Private Sub cbxDepartamentoEmpresa_PropertyChanged(sender As Object, e As EventArgs) Handles cbxDepartamentoEmpresa.PropertyChanged
        Seccion = CSistema.ExecuteScalar("Select Seccion From vDepartamentoEmpresa where ID = " & cbxDepartamentoEmpresa.cbx.SelectedValue)
        If Seccion = True Then
            Dim dtSeccion As DataTable = CData.GetTable("vSeccion", " IDDepartamentoEmpresa=" & cbxDepartamentoEmpresa.cbx.SelectedValue)
            CSistema.SqlToComboBox(cbxSeccion.cbx, dtSeccion, "ID", "Descripcion")
            cbxSeccion.cbx.Text = ""
            cbxSeccion.SoloLectura = False
        Else
            cbxSeccion.SoloLectura = True
            cbxSeccion.cbx.Text = ""
        End If

    End Sub

    Function AsociarGastoAcuerdo(ByVal IDTransaccion As Integer) As Boolean

        AsociarGastoAcuerdo = True

        Dim sql As String
        Dim IDAcuerdoComercial = 0
        IDAcuerdoComercial = cbxAcuerdoComercial.cbx.SelectedValue
        sql = "Insert Into GastoAcuerdoComercial (IDTransaccionGasto, IDAcuerdoComercial) Values (" & IDTransaccion & "," & IDAcuerdoComercial & ") "
        If CSistema.ExecuteNonQuery(sql) = 0 Then
            Return False
        End If

    End Function

End Class
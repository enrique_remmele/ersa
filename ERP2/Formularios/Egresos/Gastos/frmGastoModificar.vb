﻿Public Class frmGastoModificar

    ' CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Public CAsiento As New CAsientoGasto
    Dim Seccion As Boolean = False

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private IDTransaccionARendirValue As Integer
    Public Property IDTransaccionARendir() As Integer
        Get
            Return IDTransaccionARendirValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionARendirValue = value
        End Set
    End Property

    Private CajaChicaValue As Boolean
    Public Property CajaChica As Boolean
        Get
            Return CajaChicaValue
        End Get
        Set(ByVal value As Boolean)
            CajaChicaValue = value
        End Set
    End Property

    Private NumeroValue As Integer
    Public Property Numero As Integer
        Get
            Return NumeroValue
        End Get
        Set(ByVal value As Integer)
            NumeroValue = value
        End Set
    End Property

    Public Property dtCuota As DataTable
    Public Property Cuota As Integer
    Public Property RRHH As Boolean = False

    'EVENTOS

    'VARIABLES
    Dim vControles() As Control
    Dim vModificando As Boolean
    Dim dtVales As New DataTable
    Dim dt As New DataTable
    Dim Total As Decimal

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Controles
        txtProveedor.Conectar()

        'Otros
        'txtVencimiento.Enabled = False
        OcxImpuesto1.Inicializar()
        CDetalleImpuesto.Inicializar()
        flpRegistradoPor.Visible = False
        txtCotizacion.Inicializar()

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(frmGastos.Name, "GASTO", "GAS")

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        MostrarCajaChica()

        OcxImpuesto1.dg.ReadOnly = True

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, txtProveedor)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtVtoTimbrado)
        CSistema.CargaControl(vControles, cbxCondicion)
        CSistema.CargaControl(vControles, txtVencimiento)
        CSistema.CargaControl(vControles, cbxGrupo)
        CSistema.CargaControl(vControles, txtCotizacion)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, cbxDepartamentoEmpresa)
        CSistema.CargaControl(vControles, cbxTipoIVA)
        CSistema.CargaControl(vControles, chkCheque)
        CSistema.CargaControl(vControles, chkEfectivo)
        CSistema.CargaControl(vControles, lklAgregarVale)
        CSistema.CargaControl(vControles, lklEliminarVale)
        CSistema.CargaControl(vControles, txtTimbrado)
        CSistema.CargaControl(vControles, txtVtoTimbrado)
        CSistema.CargaControl(vControles, cbxDeposito)
        CSistema.CargaControl(vControles, cbxCamion)
        CSistema.CargaControl(vControles, cbxChofer)
        CSistema.CargaControl(vControles, chkCuota)
        CSistema.CargaControl(vControles, chkIncluirLibro)
        CSistema.CargaControl(vControles, ChkFE)
        CSistema.CargaControl(vControles, chkGastoMultiple)
        CSistema.CargaControl(vControles, cbxUnidadNegocio)
        CSistema.CargaControl(vControles, cbxSeccion)

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CONDICION
        cbxCondicion.cbx.Items.Add("CONTADO")
        cbxCondicion.cbx.Items.Add("CREDITO")
        cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'TIPO IVA
        cbxTipoIVA.cbx.Items.Add("DIRECTO")
        cbxTipoIVA.cbx.Items.Add("INDISTINTO")
        cbxTipoIVA.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipoIVA.cbx.SelectedIndex = 0

        'CARGAR CONTROLES
        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal"), "ID", "Codigo")

        CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, CData.GetTable("vUnidadNegocio"), "ID", "Descripcion")
        'cbxUnidadNegocio.cbx.Text = ""
        'Grupo
        'CSistema.SqlToComboBox(cbxGrupo.cbx, "Select IDGrupo, Grupo  From vGrupoUsuario Where IDUsuario=" & vgIDUsuario)
        '30/12/2021 - SC: Agregamos la busqueda general
        CSistema.SqlToComboBox(cbxGrupo.cbx, "Select IDGrupo as ID, Grupo From vGrupoUsuario GROUP BY IDGrupo, Grupo order by ID ")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'CARGAR LA ULTIMA CONFIGURACION
        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Condicion
        cbxCondicion.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CONDICION", "CONTADO")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Grupo
        cbxGrupo.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "GRUPO", "")

        'Moneda
        txtCotizacion.cbxMoneda.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MONEDA", "")

        'Forma de Pago Cheque
        chkCheque.Valor = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CHEQUE", "True")

        'Forma de Pago Efectivo
        chkEfectivo.Valor = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "Efectivo", "False")

        'Impuestos
        OcxImpuesto1.EstablecerSoloLectura()

        'Configuraciones
        'Deposito
        lblDeposito.Visible = CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDeposito").ToString)
        cbxDeposito.Visible = CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDeposito").ToString)

        'Distribucion
        pnlDistribucion.Visible = CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDistribucion").ToString)

    End Sub

    Sub GuardarInformacion()

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Condicion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CONDICION", cbxCondicion.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

        'Grupo0
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "GRUPO", cbxGrupo.cbx.Text)

        'Moneda
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", txtCotizacion.cbxMoneda.cbx.Text)

        'Forma de Pago Cheque
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CHEQUE", chkCheque.Valor.ToString)

        'Forma de Pago Efectivo
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "Efectivo", chkEfectivo.Valor.ToString)

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            If CajaChica = True Then
                CargarOperacionConFondoFijo()
            Else
                CargarOperacion()
            End If
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            If CajaChica = True Then
                CargarOperacionConFondoFijo()
            Else
                If RRHH Then
                    ID = CSistema.ExecuteScalar("Select IsNull((Select Min(Numero) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.GetValue & " And Numero>" & ID & " And RRHH = 'True'), 0) ")
                Else
                    ID = CSistema.ExecuteScalar("Select IsNull((Select Min(Numero) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.GetValue & " And Numero>" & ID & "And RRHH = 'False'), 0) ")
                End If
                txtID.txt.Text = ID
                txtID.txt.SelectAll()
                CargarOperacion()
            End If

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()

            If CajaChica = True Then
                CargarOperacionConFondoFijo()
            Else
                CargarOperacion()
            End If

        End If



        If e.KeyCode = Keys.End Then

            'If vNuevo = True Then
            '    Exit Sub
            'End If

            Dim ID As Integer
            If CajaChica = True Then
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From GastoFondoFijo Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)
            Else
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And CajaChica='False' "), Integer)
            End If


            txtID.txt.Text = ID
            txtID.txt.SelectAll()

            If CajaChica = True Then
                CargarOperacionConFondoFijo()
            Else
                CargarOperacion()
            End If

        End If

        If e.KeyCode = Keys.Home Then

            'If vNuevo = True Then
            '    Exit Sub
            'End If

            Dim ID As Integer
            If CajaChica = True Then
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From GastoFondoFijo Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)
            Else
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VGastoTipoComprobante Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And CajaChica='False' "), Integer)
            End If


            txtID.txt.Text = ID
            txtID.txt.SelectAll()

            If CajaChica = True Then
                CargarOperacionConFondoFijo()
            Else
                CargarOperacion()
            End If

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub CargarOperacionConFondoFijo(Optional ByVal vIDTransaccion As Integer = 0)

        vModificando = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = SeleccionarRegistro("VGastoFondoFijo")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)


        dt = CSistema.ExecuteToDataTable("Select * From VGastoFondoFijo Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        txtID.txt.Text = oRow("Numero").ToString
        txtProveedor.SetValue(oRow("IDProveedor").ToString)
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtFecha.SetValue(oRow("Fecha"))
        txtTimbrado.txt.Text = oRow("NroTimbrado").ToString
        txtVtoTimbrado.SetValue(oRow("FechaVencimientoTimbrado").ToString)
        cbxGrupo.cbx.Text = oRow("Grupo").ToString

        cbxDepartamentoEmpresa.txt.Text = oRow("DepartamentoEmpresa").ToString
        cbxUnidadNegocio.txt.Text = oRow("UnidadNegocio").ToString
        chkGastoMultiple.Checked = oRow("GastoMultiple").ToString
        cbxSeccion.txt.Text = oRow("Seccion").ToString


        'Condicion
        If CBool(oRow("CREDITO").ToString) = False Then
            txtVencimiento.txt.Clear()
            cbxCondicion.cbx.SelectedIndex = 0
        Else
            txtVencimiento.SetValue(CDate(oRow("FechaVencimiento").ToString))
            'calcular plazo
            cbxCondicion.cbx.SelectedIndex = 1
        End If

        cbxSucursal.txt.Text = oRow("Sucursal").ToString

        'Cotizacion
        txtCotizacion.SetValue(oRow("Moneda").ToString, oRow("Cotizacion").ToString)
        txtObservacion.txt.Text = oRow("Observacion").ToString

        If CBool(oRow("Directo").ToString) = False Then
            cbxTipoIVA.cbx.SelectedIndex = 1
        Else
            cbxTipoIVA.cbx.SelectedIndex = 0
        End If

        chkIncluirLibro.Valor = oRow("IncluirLibro")
        chkEfectivo.Valor = oRow("Efectivo")
        chkCheque.Valor = oRow("Cheque")

        If chkGastoMultiple.Checked = True Then
            lblDepartamento.Visible = False
            lblDepartamentoSolicitante.Visible = True
        Else
            lblDepartamento.Visible = True
            lblDepartamentoSolicitante.Visible = False
        End If

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        OcxImpuesto1.CargarValores(IDTransaccion)

        'Cargamos GastoVale
        dtVales = CSistema.ExecuteToDataTable("Select * ,'Sel'='True' from VGastoVale Where IDTransaccionGasto= " & IDTransaccion).Copy
        CargarVale()

        'Inicializamos el Asiento
        ' CAsiento.Limpiar()
        Total = OcxImpuesto1.txtTotal.txt.Text

        CAsiento.CargarOperacion(IDTransaccion)

        txtID.txt.Focus()
        txtID.txt.SelectAll()
        'cbxUnidadNegocio.SoloLectura = True

    End Sub

    Function SeleccionarRegistro(vTabla As String) As Integer

        SeleccionarRegistro = 0

        Dim vIDTransaccion As Integer

        'IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From GastoFondoFijo Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )")
        'Error de numeracion, un misterio
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select IDTransaccion, Sucursal, Numero, [Cod.], Comprobante, Proveedor, Fecha, Grupo, FechaTransaccion, Usuario From " & vTabla & " Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ")

        If dt.Rows.Count = 0 Then
            vIDTransaccion = 0
        End If

        If dt.Rows.Count = 1 Then
            vIDTransaccion = dt.Rows(0)("IDTransaccion").ToString
        End If

        If dt.Rows.Count > 1 Then
            MessageBox.Show("El sistema encontro que hay una duplicacion de numeracion.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)

            Dim frm As New frmGastosDuplicados
            frm.dt = dt
            FGMostrarFormulario(Me, frm, "Duplicacion de Numeracion", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

            If frm.Procesado = False Then
                vIDTransaccion = 0
            End If

            If frm.Procesado = True Then
                vIDTransaccion = frm.IDTransaccion
            End If

        End If

        SeleccionarRegistro = vIDTransaccion

    End Function

    Sub Nuevo()

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        OcxImpuesto1.dg.ReadOnly = True

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        cbxSucursal.Enabled = True

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        If Total <> CDec(OcxImpuesto1.txtTotal.txt.Text) Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Validar
        'Tipo Comprobante
        If cbxTipoComprobante.Validar("Seleccione correctamente el tipo de comprobante!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            CSistema.MostrarError("Ingrese un numero de comprobante!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Timbrado
        If CData.GetTable("VTipoComprobante", " ID=" & cbxTipoComprobante.GetValue)(0)("ComprobanteTimbrado") = True Then
            If txtTimbrado.txt.Text.Trim.Length = 0 Then
                Dim mensaje As String = "Ingrese un número de timbrado!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If CSistema.ControlarTimbrado(txtTimbrado.txt.Text) = False Then
                Exit Function
            End If

            If CSistema.ControlarNroDocumento(txtComprobante.txt.Text) = False Then
                Exit Function
            End If

        End If

        'Sucursal
        If cbxSucursal.Validar("Seleccione correctamente la sucursal!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Total
        If CDec(OcxImpuesto1.txtTotal.ObtenerValor) <= 0 Then
            Dim mensaje As String = "El importe del documento no es valido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Forma de Pago
        If chkCheque.Valor = False And chkEfectivo.Valor = False Then
            CSistema.MostrarError("Seleccione una forma de pago!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Cuota
        If (chkCuota.Valor = True) And (Cuota = 0) Then
            CSistema.MostrarError("No hay cuotas cargadas!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Validar el Asiento
        If CAsiento.dtAsiento.Rows.Count = 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Validar el Asiento
        If CAsiento.dtDetalleAsiento.Rows.Count = 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Validar el Asiento
        If CAsiento.ObtenerSaldo <> 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        If CAsiento.ObtenerTotal = 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Si va a eliminar
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        'Unidad de negocio
        If chkGastoMultiple.Checked = False Then
            If cbxUnidadNegocio.cbx.Text.Trim = "" Then
                Dim mensaje As String = "Seleccione correctamente la Unidad de Negocio"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If
        End If

        'Departamento
        If cbxDepartamentoEmpresa.cbx.Text.Trim = "" Then
            Dim mensaje As String = "Seleccione correctamente Departamento"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If Seccion = True Then
            If cbxSeccion.cbx.Text.Trim = "" Then
                Dim mensaje As String = "Seleccione correctamente la Seccion"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

        End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDGrupo", cbxGrupo.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroTimbrado", txtTimbrado.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaVencimientoTimbrado", txtVtoTimbrado.GetValueString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepartamentoEmpresa", cbxDepartamentoEmpresa.cbx.SelectedValue, ParameterDirection.Input)
        If chkGastoMultiple.Checked = False Then
            CSistema.SetSQLParameter(param, "@IDUnidadNegocio", cbxUnidadNegocio.cbx.SelectedValue, ParameterDirection.Input)
        End If

        If Seccion = True Then
            CSistema.SetSQLParameter(param, "@IDSeccion", cbxSeccion.cbx.SelectedValue, ParameterDirection.Input)
        End If

        If ChkFE.Checked = True Then
            CSistema.SetSQLParameter(param, "@EsFE", ChkFE.Checked, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        If cbxTipoIVA.cbx.Text = "DIRECTO" Then
            CSistema.SetSQLParameter(param, "@Directo", "True", ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Directo", "False", ParameterDirection.Input)
        End If

        If CajaChica = True Then
            CSistema.SetSQLParameter(param, "@CajaChica", "True", ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@CajaChica", "False", ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Cuota", Cuota, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IncluirLibro", chkIncluirLibro.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cheque", chkCheque.Valor.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Efectivo", chkEfectivo.Valor.ToString, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", "UPD", ParameterDirection.Input)

        IndiceOperacion = param.GetLength(0) - 1

        'Moneda
        CSistema.SetSQLParameter(param, "@IDMoneda", txtCotizacion.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", txtCotizacion.txtCotizacion.ObtenerValor, ParameterDirection.Input)

        'Credito
        If cbxCondicion.cbx.SelectedIndex = 0 Then
            CSistema.SetSQLParameter(param, "@Credito", "False".ToString, ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Credito", "True".ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@FechaVencimiento", CSistema.FormatoFechaBaseDatos(txtVencimiento.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        End If

        'Distribucion
        If CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDeposito").ToString) = True Then
            CSistema.SetSQLParameter(param, "@IDDepositoOperacion", cbxDeposito.cbx, ParameterDirection.Input)
        End If

        If CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDistribucion").ToString) = True Then
            CSistema.SetSQLParameter(param, "@IDCamion", cbxCamion.cbx, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDChofer", cbxChofer.cbx, ParameterDirection.Input)
        End If

        'Totales
        'Totales
        'CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(CSistema.gridSumColumn(OcxImpuesto1.dg, "colTotal")), ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(CSistema.gridSumColumn(OcxImpuesto1.dg, "colTotalImpuesto")), ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(CSistema.gridSumColumn(OcxImpuesto1.dg, "colTotalDiscriminado")), ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Total", OcxImpuesto1.TotalBD, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalImpuesto", OcxImpuesto1.TotalImpuestoBD, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", OcxImpuesto1.TotalDiscriminadoBD, ParameterDirection.Input)


        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpGasto", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            Exit Sub

        End If

        MessageBox.Show(MensajeRetorno, "MODIFICACION")

        'Cargamos el DetalleImpuesto
        OcxImpuesto1.Generar(IDTransaccion)
        CDetalleImpuesto.dt = OcxImpuesto1.dtImpuesto
        CDetalleImpuesto.Guardar(IDTransaccion)

        'Cargamos el asiento
        CAsiento.IDTransaccion = IDTransaccion
        CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        'Cuotas
        If IDTransaccion > 0 And Cuota > 0 Then

            If Operacion = ERP.CSistema.NUMOperacionesRegistro.UPD Then

                'Insertamos las cuotas
                InsertarCuotas(ERP.CSistema.NUMOperacionesRegistro.INS, IDTransaccion)

            End If

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False
        Cancelar()
        cbxSucursal.Enabled = True
    End Sub

    Sub Modificar()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        ctrError.Clear()
        tsslEstado.Text = ""

        btnAsiento.Enabled = True
        OcxImpuesto1.dg.ReadOnly = False
        vModificando = True

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Bloquear Origen
        cbxSucursal.Enabled = False

        cbxTipoComprobante.Focus()
        CDetalleImpuesto.Decimales = txtCotizacion.Registro("Decimales")
        'dtVales = CSistema.ExecuteToDataTable("Select *, 'Sel'= 'False','Cancelar'= 'False' from VVale Where ARendir= 'True' And Cancelado= 'False' And Anulado='False'").Copy
        chkGastoMultiple.Enabled = True
        If chkGastoMultiple.Checked = True Then
            cbxUnidadNegocio.SoloLectura = True
            cbxUnidadNegocio.cbx.Text = ""
        Else
            cbxUnidadNegocio.SoloLectura = False
            cbxUnidadNegocio.cbx.Text = ""
        End If
    End Sub

    Sub CalcularTotales()

        OcxImpuesto1.CargarValores(CDetalleImpuesto.dt)

    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""


        OcxImpuesto1.Generar(IDTransaccion)
        CDetalleImpuesto.dt = OcxImpuesto1.dtImpuesto

        If Total <> OcxImpuesto1.txtTotal.ObtenerValor Then
            CAsiento.InicializarAsiento()
            CAsiento.Generado = False
            vModificando = True
        End If

        'Si es nuevo
        If vModificando = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "Compra: " & txtID.txt.Text & "  -  " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text & "  -  " & txtProveedor.txtRazonSocial.Text
            'frm.GastoMultiple = chkGastoMultiple.Checked
            'Para que muestre solo las cuenta contables que estan asignadas al fondo fijo
            If CajaChica = True Then
                Dim dtCCFF As New DataTable
                Dim CuentasContables As String = "ID = 0 "
                dtCCFF = CSistema.ExecuteToDataTable("Select IDCuentaContable from FondoFijoCuentaContable where IdFondoFijo = (select ID from Grupo where descripcion = '" & cbxGrupo.txt.Text & "') ").Copy

                For Each oRow1 As DataRow In dtCCFF.Rows
                    Dim param As New DataTable
                    CuentasContables = CuentasContables & " or ID =" & oRow1("IDCuentaContable").ToString & "    "
                Next
                frm.Filtrar = True
                frm.whereFiltro = CuentasContables
            Else
                frm.Filtrar = False
                frm.whereFiltro = ""
            End If

            'Dim SQL As String

            If CajaChica = False Then
                'SQL = "Select IsNull((Select IDTransaccion From GastoTipoComprobante Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ), 0 )"
                IDTransaccion = SeleccionarRegistro("VGastoTipoComprobante")
            Else
                'SQL = "Select IsNull((Select IDTransaccion From GastoFondoFijo Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ), 0 )"
                IDTransaccion = SeleccionarRegistro("VGastoFondoFijo")
            End If

            'Dim IDTransaccion As Integer = CSistema.ExecuteScalar(SQL)
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)

        Else

            If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(cbxTipoComprobante, mensaje)
                ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If txtProveedor.Seleccionado = False Then
                Dim mensaje As String = "Seleccione correctamente el proveedor!"
                ctrError.SetError(txtProveedor, mensaje)
                ctrError.SetIconAlignment(txtProveedor, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "Compra: " & txtID.txt.Text & "  -  " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text & "  -  " & txtProveedor.txtRazonSocial.Text
            frm.GastoMultiple = chkGastoMultiple.Checked
            'Para que muestre solo las cuenta contables que estan asignadas al fondo fijo
            If CajaChica = True Then
                Dim dtCCFF As New DataTable
                Dim CuentasContables As String = "ID = 0 "
                dtCCFF = CSistema.ExecuteToDataTable("Select IDCuentaContable from FondoFijoCuentaContable where IdFondoFijo =" & cbxGrupo.GetValue & " ").Copy

                For Each oRow1 As DataRow In dtCCFF.Rows
                    Dim param As New DataTable
                    CuentasContables = CuentasContables & " or ID =" & oRow1("IDCuentaContable").ToString & "    "
                Next
                frm.Filtrar = True
                frm.whereFiltro = CuentasContables
            Else
                frm.Filtrar = False
                frm.whereFiltro = ""
            End If

            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
            CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            CAsiento.NroCaja = frm.CAsiento.NroCaja
            CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            Dim oRow As DataRow = frm.CAsiento.dtAsiento.Rows(0)
            Total = oRow("Total").ToString

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                CAsiento.dtAsiento.Clear()
                CAsiento.dtDetalleAsiento.Clear()
                CAsiento.dtImpuesto.Clear()
                GenerarAsiento(True)
                VisualizarAsiento()
            End If

        End If


    End Sub

    Sub GenerarAsiento(Optional VolverAGenerar As Boolean = False)

        'EstablecerCabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = CData.GetTable("VSucursal", " ID = " & cbxSucursal.GetValue).Rows(0)("IDCiudad").ToString
        oRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
        oRow("Fecha") = txtFecha.GetValue
        oRow("IDMoneda") = txtCotizacion.Registro("ID")
        oRow("Cotizacion") = txtCotizacion.GetValue
        oRow("IDTipoComprobante") = cbxTipoComprobante.cbx.SelectedValue
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = txtComprobante.txt.Text
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text
        oRow("Detalle") = txtObservacion.txt.Text
        oRow("Total") = OcxImpuesto1.txtTotal.ObtenerValor
        oRow("CajaChica") = CajaChica

        oRow("GastosMultiples") = chkGastoMultiple.Checked
        If chkGastoMultiple.Checked = True Then
            oRow("IDUnidadNegocio") = 0
            oRow("IDCentroCosto") = 0
            oRow("IDDepartamento") = cbxDepartamentoEmpresa.GetValue
        Else
            oRow("IDUnidadNegocio") = cbxUnidadNegocio.GetValue

            oRow("IDDepartamento") = cbxDepartamentoEmpresa.GetValue
        End If

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)


        'Solo establecer la primera vez, esto es para el detalle
        If CAsiento.Generado = True Then
            Exit Sub
        End If

        OcxImpuesto1.Generar()
        CAsiento.dtImpuesto = OcxImpuesto1.dtImpuesto.Copy
        CAsiento.IDProveedor = txtProveedor.Registro("ID").ToString

        If VolverAGenerar = True Then
            CAsiento.Generar()
        End If

    End Sub

    Function InsertarDetalle(ByVal Operacion As CSistema.NUMOperacionesRegistro, ByVal vIDTransaccion As Integer) As Boolean

        InsertarDetalle = True


        For Each oRow As DataRow In dtVales.Select("Sel='True'")

            Dim param(-1) As SqlClient.SqlParameter


            CSistema.SetSQLParameter(param, "@vIDSucursal", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@vIDGrupo", cbxGrupo.cbx.SelectedValue, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@vIDTransaccionVale", (oRow("IDTransaccion").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@vIDTransaccionGasto", vIDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@vImporte", CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpGastoVale", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
                Return False
            End If

        Next

    End Function

    Function InsertarCuotas(ByVal Operacion As CSistema.NUMOperacionesRegistro, ByVal vIDTransaccion As Integer) As Boolean

        InsertarCuotas = True

        For Each oRow As DataRow In dtCuota.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", vIDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", oRow("ID").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Saldo", CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Vencimiento", CSistema.FormatoFechaBaseDatos(oRow("Vencimiento").ToString, True, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cancelado", oRow("Cancelado").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpCuota", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
                Return False
            End If

        Next

    End Function

    Sub Eliminar()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnEliminar, mensaje)
            ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.DEL.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Eliminar
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpGasto", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnEliminar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
    End Sub

    Sub CargaValeARendir()
        Dim frm As New frmValeAResindir
        dtVales = CSistema.ExecuteToDataTable("Select *, 'Sel'= 'False','Cancelar'= 'False' from VVale Where ARendir= 'True' And Cancelado= 'False' And Anulado='False'").Copy
        frm.Text = " Seleccionar Seleccionar Vales "
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtVales
        frm.Inicializar()
        frm.ShowDialog(Me)
        ' dtVales = frm.dt

        CargarVale()

    End Sub

    Sub MostrarCajaChica()
        If CajaChica = True Then
            gbxVale.Visible = True
        End If

        If CajaChica = False Then
            gbxVale.Visible = False
        End If
    End Sub

    Sub CargarVale()

        lvVale.Items.Clear()

        Dim Total As Decimal = 0

        For Each oRow As DataRow In dtVales.Rows
            If oRow("Sel") = True Then

                Dim item As ListViewItem = New ListViewItem(oRow("IDTransaccion").ToString)
                item.SubItems.Add(oRow("Fec").ToString)
                item.SubItems.Add(oRow("Motivo").ToString)
                item.SubItems.Add(oRow("Nombre").ToString)
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Total").ToString))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Saldo").ToString))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Importe").ToString))
                Total = Total + CDec(oRow("Importe").ToString)

                lvVale.Items.Add(item)

            End If
        Next

        txtTotalComprobante.SetValue(Total)
        txtCantidadComprobante.SetValue(lvVale.Items.Count)

        'CalcularTotales()

    End Sub

    Sub EliminarVale()

        For Each item As ListViewItem In lvVale.SelectedItems

            For Each oRow As DataRow In dtVales.Select(" IDTransaccion = " & item.Text & " ")
                'CAsientoContableOrdenPago.EliminarVenta(oRow("Importe").ToString, oRow("IDTipoComprobante").ToString, oRow("IDMoneda").ToString, oRow("Credito").ToString)
                oRow("Sel") = False
                Exit For
            Next

        Next

        CargarVale()

    End Sub

    Sub Buscar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO, New Button, btnGuardar, btnCancelar, btnEliminar, New Button, btnBusquedaAvanzada, btnAsiento, vControles, btnModificar)
        'Otros

        Dim frm As New frmConsultaGasto
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.CajaChica = CajaChica
        frm.ShowDialog(Me)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        'vNuevo = True
        vModificando = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            'IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From GastoTipoComprobante Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ), 0 )")
            IDTransaccion = SeleccionarRegistro("VGastoTipoComprobante")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dt.Rows.Clear()
        '30/12/2021 - SC: Agregamos la busqueda general
        'Listamos todos los grupos para la consulta
        CSistema.SqlToComboBox(cbxGrupo.cbx, "Select IDGrupo as ID, Grupo From vGrupoUsuario GROUP BY IDGrupo, Grupo order by ID ")

        dt = CSistema.ExecuteToDataTable("Select * From VGastoTipoComprobante Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas técnicos."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        txtID.txt.Text = oRow("Numero").ToString
        txtProveedor.SetValue(oRow("IDProveedor").ToString)
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        'Es Factura Electrónica
        If oRow("FacturaElectronica") <> 0 Then
            ChkFE.Checked = True
        Else
            ChkFE.Checked = False
        End If
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtFecha.SetValue(oRow("Fecha"))
        txtTimbrado.txt.Text = oRow("NroTimbrado").ToString
        cbxGrupo.cbx.Text = oRow("Grupo").ToString
        txtVtoTimbrado.SetValue(oRow("FechaVencimientoTimbrado").ToString)

        cbxDepartamentoEmpresa.txt.Text = oRow("DepartamentoEmpresa").ToString
        cbxUnidadNegocio.txt.Text = oRow("UnidadNegocio").ToString
        chkGastoMultiple.Checked = oRow("GastoMultiple").ToString
        cbxSeccion.txt.Text = oRow("Seccion").ToString

        'cbxUnidadNegocio.SoloLectura = True
        'Condicion
        If CBool(oRow("CREDITO").ToString) = False Then
            txtVencimiento.txt.Clear()
            cbxCondicion.cbx.SelectedIndex = 0
        Else
            txtVencimiento.SetValue(CDate(oRow("FechaVencimiento").ToString))
            'calcular plazo
            cbxCondicion.cbx.SelectedIndex = 1
        End If

        cbxSucursal.cbx.Text = oRow("Sucursal").ToString

        'Cotizacion
        txtCotizacion.SetValue(oRow("Moneda").ToString, oRow("Cotizacion").ToString)
        txtObservacion.txt.Text = oRow("Observacion").ToString

        'Distribucion
        cbxDeposito.cbx.Text = oRow("Deposito").ToString
        cbxCamion.cbx.Text = oRow("Camion").ToString
        cbxChofer.cbx.Text = oRow("Chofer").ToString

        If CBool(oRow("Directo").ToString) = False Then
            cbxTipoIVA.cbx.SelectedIndex = 1
        Else
            cbxTipoIVA.cbx.SelectedIndex = 0
        End If

        chkIncluirLibro.Valor = oRow("IncluirLibro")
        'chkEfectivo.Valor = oRow("Efectivo")
        'chkCheque.Valor = oRow("Cheque")

        'Cuota
        Cuota = oRow("Cuota")
        If oRow("Cuota") > 0 Then
            chkCuota.Valor = True
            dtCuota = CSistema.ExecuteToDataTable("Select * From VCuota Where IDTransaccion=" & IDTransaccion)
        Else
            chkCuota.Valor = False
        End If

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        'Cargamos el detalle
        OcxImpuesto1.CargarValores(IDTransaccion)

        Total = OcxImpuesto1.txtTotal.txt.Text

        CAsiento.CargarOperacion(IDTransaccion)

        txtID.txt.Focus()
        txtID.txt.SelectAll()


    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnModificar, btnGuardar, btnCancelar, btnEliminar, New Button, btnBusquedaAvanzada, btnAsiento, vControles, New Button)

    End Sub

    Sub VizualizarCuota()

        Dim frm As New frmCuota
        frm.Total = OcxImpuesto1.Total
        frm.FechaInicial = txtFecha.GetValue
        frm.dt = dtCuota
        frm.Cuotas = Cuota
        If vModificando = True Then
            frm.SoloConsulta = False

            Select Case cbxCondicion.cbx.SelectedIndex
                Case 0 ' CONTADO
                    frm.FechaInicial = txtFecha.GetValue
                Case 1 ' CREDITO
                    frm.FechaInicial = txtVencimiento.GetValue
                Case Else ' SIN DEFINIR
                    frm.FechaInicial = txtFecha.GetValue
            End Select

        Else
            frm.SoloConsulta = True
        End If

        FGMostrarFormulario(Me, frm, "Cuotas", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        dtCuota = frm.dt
        Cuota = frm.Cuotas

    End Sub

    Private Sub frmGasto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub frmGasto_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmGasto_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub txtProveedor_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProveedor.Leave
        txtProveedor.OcultarLista()
    End Sub

    Private Sub txtProveedor_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProveedor.ItemSeleccionado
        cbxTipoComprobante.cbx.Focus()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If CSistema.ExecuteScalar("select count(*) from Cotizacion where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False) & "' and IDMoneda = " & txtCotizacion.Registro("ID")) = 0 And txtCotizacion.Registro("ID") > 1 Then
            MessageBox.Show("No se ha fijado cotizacion para la moneda seleccionada.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
        End If
        Guardar(ERP.CSistema.NUMOperacionesRegistro.UPD)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub cbxTipoComprobante_Editar(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxTipoComprobante.Editar

        Dim frm As New frmTipoComprobante
        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        cbxTipoComprobante.cbx.DataSource = Nothing
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=20")

    End Sub

    Private Sub cbxSucursal_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxSucursal.Leave
        If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxSucursal.cbx.Text.Trim = "" Then
            Exit Sub
        End If
    End Sub

    Private Sub cbxSucursal_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxSucursal.TeclaPrecionada
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklAgregarVale.LinkClicked
        CargaValeARendir()
    End Sub

    Private Sub lklEliminarVenta_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarVale.LinkClicked
        EliminarVale()
    End Sub

    Private Sub cbxTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoComprobante.PropertyChanged
        If dt.Rows.Count = 0 Then
            Exit Sub
        End If
        For Each oRow As DataRow In dt.Rows
            If oRow("TipoComprobante") <> cbxTipoComprobante.cbx.Text Then
                CAsiento.InicializarAsiento()
                CAsiento.Generado = False
                vModificando = True
            Else
                vModificando = False
            End If
        Next
    End Sub

    Private Sub txtObservacion_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtObservacion.KeyUp
        If dt.Rows.Count = 0 Then
            Exit Sub
        End If
        For Each oRow As DataRow In dt.Rows
            If oRow("Observacion") <> txtObservacion.txt.Text Then
                CAsiento.InicializarAsiento()
                CAsiento.Generado = False
                vModificando = True
            Else
                vModificando = False
            End If
        Next
    End Sub

    Private Sub txtComprobante_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtComprobante.KeyUp
        If dt.Rows.Count = 0 Then
            Exit Sub
        End If
        For Each oRow As DataRow In dt.Rows
            If oRow("Comprobante") <> txtComprobante.txt.Text Then
                CAsiento.InicializarAsiento()
                CAsiento.Generado = False
                vModificando = True
            Else
                vModificando = False
            End If
        Next
    End Sub

    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged

        cbxDeposito.DataSource = Nothing
        cbxDepartamentoEmpresa.DataSource = Nothing

        If cbxSucursal.GetValue = 0 Then
            Exit Sub
        End If

        Dim dt As DataTable = CData.GetTable("VDeposito", " IDSucursal=" & cbxSucursal.GetValue)
        CSistema.SqlToComboBox(cbxDeposito.cbx, dt, "ID", "Deposito")

        Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " IDSucursal=" & cbxSucursal.GetValue)
        CSistema.SqlToComboBox(cbxDepartamentoEmpresa.cbx, dtDepartamentos, "ID", "Departamento")

    End Sub

    Private Sub txtCotizacion_CambioMoneda() Handles txtCotizacion.CambioMoneda
        OcxImpuesto1.SetIDMoneda(txtCotizacion.Registro("ID"))
    End Sub

    Private Sub chkCuota_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCuota.PropertyChanged
        btnCuota.Enabled = value
        If value = False Then
            Cuota = 0
        End If

    End Sub

    Private Sub btnCuota_Click(sender As System.Object, e As System.EventArgs) Handles btnCuota.Click
        VizualizarCuota()
    End Sub

    Private Sub chkGastoMultiple_CheckedChanged(sender As Object, e As EventArgs) Handles chkGastoMultiple.CheckedChanged
        If chkGastoMultiple.Checked = True Then
            'cbxUnidadNegocio.SoloLectura = True
            lblDepartamento.Visible = False
            lblDepartamentoSolicitante.Visible = True
            'cbxDepartamentoEmpresa.SoloLectura = True
        Else
            'cbxUnidadNegocio.SoloLectura = False
            lblDepartamento.Visible = True
            lblDepartamentoSolicitante.Visible = False
            'cbxDepartamentoEmpresa.SoloLectura = False
        End If
    End Sub

    Private Sub cbxDepartamentoEmpresa_PropertyChanged(sender As Object, e As EventArgs) Handles cbxDepartamentoEmpresa.PropertyChanged
        Seccion = CSistema.ExecuteScalar("Select Seccion From vDepartamentoEmpresa where ID = " & cbxDepartamentoEmpresa.cbx.SelectedValue)
        If Seccion = True Then
            Dim dtSeccion As DataTable = CData.GetTable("vSeccion", " IDDepartamentoEmpresa=" & cbxDepartamentoEmpresa.cbx.SelectedValue)
            CSistema.SqlToComboBox(cbxSeccion.cbx, dtSeccion, "ID", "Descripcion")
            cbxSeccion.cbx.Text = ""
            cbxSeccion.SoloLectura = False
        Else
            cbxSeccion.SoloLectura = True
            cbxSeccion.cbx.Text = ""
        End If
    End Sub
End Class


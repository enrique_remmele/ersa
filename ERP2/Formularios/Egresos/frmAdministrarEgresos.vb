﻿Public Class frmAdministrarEgresos

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CData As New CData
    Dim dtConsulta As New DataTable


    'PROPIEDADES
    Public Property IDTransaccion As Integer
    Public Property IDOperacion As Integer

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Controles
        OcxImpuesto1.Inicializar()

        'Funciones
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal"), "ID", "Codigo")

        'Proveedor
        Dim vdt As DataTable = CData.GetTable("VProveedor")
        CData.OrderDataTable(vdt, "RazonSocial")
        CSistema.SqlToComboBox(cbxProveedor.cbx, vdt, "ID", "RazonSocial")

        'Filtros
        'Tipo
        CSistema.SqlToComboBox(cbxTipo.cbx, "Select Distinct IDOperacion, Operacion From VEgresos")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, CData.GetTable("VTipoComprobante"), "ID", "Descripcion")

        'Grupo
        CSistema.SqlToComboBox(cbxGrupo.cbx, CData.GetTable("VGrupo"), "ID", "Descripcion")

        'Moneda
        CSistema.SqlToComboBox(cbxMoneda.cbx, CData.GetTable("VMoneda"), "ID", "Descripcion")

        'Usuario
        CSistema.SqlToComboBox(cbxUsuario.cbx, CData.GetTable("VUsuario"), "ID", "Usuario")

    End Sub

    Sub GuardarInformacion()

    End Sub

    Sub ListarComprobantes(Optional vCondicion As String = "")

        'Dim Consulta As String = "Select *, 'TotalGuaranies'=isnull(Total,0)*isnull(Cotizacion,1),'SaldoGuaranies'=isnull(Saldo,0)*isnull(Cotizacion,1) From VEgresos "
        Dim Consulta As String = "Select *, 'TotalGuaranies'=Case when IDMoneda = 1 Then isnull(Total,0) else 0 end,'SaldoGuaranies'=Case when IDMoneda = 1 Then isnull(Saldo,0) else 0 end,'TotalDolares'=Case when IDMoneda = 2 Then isnull(Total,0) else 0 end,'SaldoDolares'=Case when IDMoneda = 2 Then isnull(Saldo,0) else 0 end From VEgresos "
        Dim Where As String = " Where 0=0 "
        Dim OrderBy As String = " Order By Comprobante "
        Dim HabilitarRRHH As Boolean = CSistema.FuncionPermitida(vgIDPerfil, Me, "MostrarDatosRRHH")

        If HabilitarRRHH = False Then
            Where = Where & " And RRHH = 'False' "
        End If

        'Si es normal
        If vCondicion = "" Then
            Dim TipoFecha As String = ""

            If rdbFechaDocumento.Checked Then TipoFecha = "Fecha"
            If rdbFechaRegistro.Checked Then TipoFecha = "FechaRegistro"
            If rdbFechaVencimiento.Checked Then TipoFecha = "FechaVencimiento"

            If dtpDesde.Text = dtpHasta.Text Then
                Where = Where & " And cast(" & TipoFecha & " as date)= cast('" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' as date)"
            Else
                Where = Where & " And cast(" & TipoFecha & " as date) Between cast('" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "'  as date) And cast('" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' as date) "
            End If


            If chkFecha2.Checked Then

                If rdbFechaDocumento2.Checked Then TipoFecha = "Fecha"
                If rdbFechaRegistro2.Checked Then TipoFecha = "FechaRegistro"
                If rdbFechaVencimiento2.Checked Then TipoFecha = "FechaVencimiento"
                If txtDesde2.Text = txtHasta2.Text Then
                    Where = Where & " And cast(" & TipoFecha & " as date)= cast('" & CSistema.FormatoFechaBaseDatos(txtDesde2, True, False) & "' as date)"
                Else
                    Where = Where & " And cast(" & TipoFecha & " as date) Between cast('" & CSistema.FormatoFechaBaseDatos(txtDesde2, True, False) & "' as date) And cast('" & CSistema.FormatoFechaBaseDatos(txtHasta2, True, False) & "' as date)"
                End If



            End If


        Else
            Where = Where & vCondicion
        End If

        'Filtros
        cbxSucursal.EstablecerCondicion(Where)
        cbxProveedor.EstablecerCondicion(Where)
        cbxTipo.EstablecerCondicion(Where)
        cbxTipoComprobante.EstablecerCondicion(Where)
        cbxGrupo.EstablecerCondicion(Where)
        cbxMoneda.EstablecerCondicion(Where)
        cbxUsuario.EstablecerCondicion(Where)

        'Condicion
        If rdbCondicionContado.Checked Then Where = Where & " And Credito='False' "
        If rdbCondicionCredito.Checked Then Where = Where & " And Credito='True' "

        'Estado
        If rdbEstadoCancelados.Checked Then Where = Where & " And (Saldo=0 Or Cancelado='True') "
        If rdbEstadoPendientes.Checked Then Where = Where & " And (Saldo<>0 Or Cancelado='False') "


        Dim CantidadGS As Integer = CSistema.ExecuteScalar("Select count(IDTransaccion) From VEgresos " & Where & "And IDMoneda = 1")
        Dim CantidadUSD As Integer = CSistema.ExecuteScalar("Select count(IDTransaccion) From VEgresos " & Where & "And IDMoneda = 2")

        'Listar 
        dtConsulta = CSistema.ExecuteToDataTable(Consulta & Where & OrderBy, "", 1000)
        CSistema.dtToGrid(dgvComprobantes, dtConsulta)

        If dgvComprobantes.ColumnCount = 0 Then
            Exit Sub
        End If

        'Formato
        dgvComprobantes.Columns("Proveedor").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvComprobantes.Columns("Grupo").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        'Ocultar columnas
        Dim ColumnasVisibles() As String = {"CodigoOperacion", "Suc", "Numero", "TipoComprobante", "Comprobante", "Referencia", "Proveedor", "Fecha", "Grupo", "Condicion", "FechaVencimiento", "Moneda", "Cotizacion", "Total", "Saldo", "NumeroOrdenPago"}
        Dim ColumnasNumericas() As String = {"Cotizacion", "Total", "Saldo"}
        Dim ColumnasCentradas() As String = {"Fecha", "Condicion", "FechaVencimiento", "Moneda"}
        CSistema.DataGridColumnasVisibles(dgvComprobantes, ColumnasVisibles)

        'Si la moneda seleccionada es dolar, mostrar decimales
        Dim decimales As Boolean = True
        Dim CantidadDecimal As Integer = 2
        If cbxMoneda.Enabled = True Then decimales = CSistema.MonedaDecimal(cbxMoneda.GetValue)

        'Formato
        For Each c As DataGridViewColumn In dgvComprobantes.Columns

            'Formatos numericos
            If ColumnasNumericas.Contains(c.Name) = True Then
                c.DefaultCellStyle.Format = "N" & CantidadDecimal
                c.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End If

        Next


        CSistema.DataGridColumnasNumericas(dgvComprobantes, ColumnasNumericas, decimales)
        CSistema.DataGridColumnasCentradas(dgvComprobantes, ColumnasCentradas)

        'Nombres de Columnas
        dgvComprobantes.Columns("CodigoOperacion").HeaderText = "Ope."
        dgvComprobantes.Columns("TipoComprobante").HeaderText = "T. Comp."
        dgvComprobantes.Columns("Numero").HeaderText = "Nro."
        dgvComprobantes.Columns("Referencia").HeaderText = "Ref."
        dgvComprobantes.Columns("Condicion").HeaderText = "Cond."
        dgvComprobantes.Columns("FechaVencimiento").HeaderText = "Venc."
        dgvComprobantes.Columns("Moneda").HeaderText = "Mon."
        dgvComprobantes.Columns("Cotizacion").HeaderText = "Cotiz."
        dgvComprobantes.Columns("NumeroOrdenPago").HeaderText = "OP"

        'Anchor de Columnas
        dgvComprobantes.Columns("CodigoOperacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgvComprobantes.Columns("Suc").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgvComprobantes.Columns("Numero").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgvComprobantes.Columns("TipoComprobante").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgvComprobantes.Columns("Condicion").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgvComprobantes.Columns("Moneda").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgvComprobantes.Columns("Cotizacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        dgvComprobantes.Columns("NumeroOrdenPago").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader

        'Cantidad
        txtCantidadOperacion.SetValue(CantidadGS)
        txtCantidadUSD.SetValue(CantidadUSD)
        Dim total As Decimal = CSistema.dtSumColumn(dtConsulta, "TotalGuaranies")
        Dim Saldo As Decimal = CSistema.dtSumColumn(dtConsulta, "SaldoGuaranies")
        Dim totalDolares As Decimal = CSistema.dtSumColumn(dtConsulta, "TotalDolares")
        Dim SaldoDolares As Decimal = CSistema.dtSumColumn(dtConsulta, "SaldoDolares")
        txtTotalGuaranies.SetValue(total)
        txtSaldoGuaranies.SetValue(Saldo)
        TxtTotalDolares.SetValue(totalDolares)
        TxtSaldoDolares.SetValue(SaldoDolares)

    End Sub

    Sub VerAsiento()

        If dgvComprobantes.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione un registro!"
            ctrError.SetError(btnAsiento, mensaje)
            ctrError.SetIconAlignment(btnAsiento, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim Comprobante As String
        Dim Cliente As String
        Dim IDTransaccion As Integer

        IDTransaccion = dgvComprobantes.SelectedRows(0).Cells("IDTransaccion").Value
        Comprobante = dgvComprobantes.SelectedRows(0).Cells("Comprobante").Value
        Cliente = dgvComprobantes.SelectedRows(0).Cells("Proveedor").Value

        Dim frm As New frmVisualizarAsiento
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.Text = "Egreso: " & Comprobante & "  -  " & Cliente
        frm.IDTransaccion = IDTransaccion

        'Mostramos
        frm.ShowDialog(Me)

    End Sub

    Sub Eliminar()

    End Sub

    Sub ListarExtracto()

        Dim SQL As String = "Select IDTransaccion, Operacion, Fecha, Documento, [Detalle/Concepto], Debito, Credito, Saldo From VExtractoMovimientoProveedorFactura Where ComprobanteAsociado=" & IDTransaccion & " Order By Fecha "
        Dim vdt As DataTable = CSistema.ExecuteToDataTable(SQL)
        Dim SaldoAnterior As Decimal = 0
        For Each oRow As DataRow In vdt.Rows

            SaldoAnterior = (SaldoAnterior + oRow("Credito")) - oRow("Debito")
            oRow("Saldo") = SaldoAnterior

        Next

        CSistema.dtToGrid(dgvExtracto, vdt)

        'Formato
        Dim IDMoneda As Integer = dgvComprobantes.SelectedRows(0).Cells("IDMoneda").Value
        CSistema.DataGridColumnasNumericas(dgvExtracto, {"Debito", "Credito", "Saldo"}, CSistema.MonedaDecimal(IDMoneda))
        dgvExtracto.Columns("IDTransaccion").HeaderText = "Nro. Trans."
        dgvExtracto.Columns("Detalle/Concepto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill


    End Sub

    Sub ListarDetalle()

    End Sub

    Sub SeleccionarRegistro()

        If dgvComprobantes.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        IDTransaccion = dgvComprobantes.SelectedRows(0).Cells("IDTransaccion").Value

        'Totales
        OcxImpuesto1.SetIDMoneda(dgvComprobantes.SelectedRows(0).Cells("IDMoneda").Value)
        OcxImpuesto1.CargarValores(IDTransaccion)

        'Registro
        txtIDTransaccion.Text = dgvComprobantes.SelectedRows(0).Cells("IDTransaccion").Value
        txtUsuario.Text = dgvComprobantes.SelectedRows(0).Cells("UsuarioRegistro").Value
        txtSucursal.Text = dgvComprobantes.SelectedRows(0).Cells("SucursalTransaccion").Value
        txtTerminal.Text = dgvComprobantes.SelectedRows(0).Cells("TerminalTransaccion").Value
        Dim FechaRegistro As Date = dgvComprobantes.SelectedRows(0).Cells("FechaRegistro").Value
        txtFechaHora.Text = FechaRegistro.ToShortDateString & " " & FechaRegistro.ToShortTimeString
        txtObservacion.Text = dgvComprobantes.SelectedRows(0).Cells("Observacion").Value
        txtTimbrado.Text = dgvComprobantes.SelectedRows(0).Cells("NroTimbrado").Value
        txtVencimientoTimbrado.Text = dgvComprobantes.SelectedRows(0).Cells("FechaVencimientoTimbrado").Value

        'Extracto
        ListarExtracto()

    End Sub

    Sub RepararSaldo()

        'Validar
        If dgvComprobantes.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim IDTransaccion As Integer = dgvComprobantes.SelectedRows(0).Cells("IDTransaccion").Value
        Dim SaldoActual As Decimal = dgvComprobantes.SelectedRows(0).Cells("Saldo").Value
        Dim SaldoCalculado As Decimal = dgvExtracto.Rows(dgvExtracto.Rows.Count - 1).Cells("Saldo").Value
        Dim TipoDocumento As String = dgvComprobantes.SelectedRows(0).Cells("Operacion").Value

        'Si los saldos son iguales, no hace falta reparar
        If SaldoActual = SaldoCalculado Then
            MessageBox.Show("Los saldos calculado y del sistema son iguales! No es necesario una reparacion.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        'Consultar
        If MessageBox.Show("Esta seguro/a de reemplazar el saldo: " & CSistema.FormatoNumero(SaldoCalculado) & " por " & CSistema.FormatoNumero(SaldoActual) & "?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim Tabla As String = ""
        Dim Cancelado As Boolean = False

        Select Case TipoDocumento.ToUpper
            Case "GASTO"
                Tabla = "Gasto"
            Case "COMPRA DE MERCADERIA"
                Tabla = "Compra"
            Case "FONDO FIJO"
                Tabla = "Gasto"
        End Select

        If SaldoCalculado = 0 Then
            Cancelado = True
        End If

        Dim SQL As String = "Update " & Tabla & " Set Saldo=" & CSistema.FormatoNumeroBaseDatos(SaldoCalculado, True) & ", Cancelado='" & Cancelado.ToString & "' Where IDTransaccion=" & IDTransaccion

        If CSistema.ExecuteNonQuery(SQL) = 0 Then
            MessageBox.Show("No se pudo realizar la operacion!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        MessageBox.Show("Registro actualizado!", "Saldo", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    Sub ExtractoProveedor()

        Dim IDProveedor As Integer = dgvComprobantes.SelectedRows(0).Cells("IDProveedor").Value
        Dim Desde As Date = dgvComprobantes.SelectedRows(0).Cells("Fecha").Value
        Dim Hasta As Date = Now

        Dim frm As New frmExtractoMovimientoProveedor
        frm.Precargar = True
        frm.IDProveedor = IDProveedor
        frm.Desde = Desde
        frm.Hasta = Hasta
        FGMostrarFormulario(Me, frm, "Extracto Proveedor", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub
    Sub VerDetalle()

        If dgvComprobantes.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione un registro!"
            ctrError.SetError(btnVerDetalle, mensaje)
            ctrError.SetIconAlignment(btnVerDetalle, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim IDTransaccion As Integer
        IDTransaccion = dgvComprobantes.SelectedRows(0).Cells("IDTransaccion").Value

        If dgvComprobantes.SelectedRows(0).Cells("CodigoOperacion").Value = "COMPROV" Then
            Dim frmCompra As New frmCompra
            frmCompra.IDTransaccion = IDTransaccion
            frmCompra.ShowDialog()
        ElseIf dgvComprobantes.SelectedRows(0).Cells("CodigoOperacion").Value = "GAS" Then
            Dim frmCompra As New frmGastos
            frmCompra.IDTransaccion = IDTransaccion
            frmCompra.ShowDialog()
        ElseIf dgvComprobantes.SelectedRows(0).Cells("CodigoOperacion").Value = "F.F." Then
            Dim frmCompra As New frmGastos
            frmCompra.CajaChica = True
            frmCompra.IDTransaccion = IDTransaccion
            frmCompra.ShowDialog()
        End If
        

    End Sub

    Private Sub frmAdministrarEgresos_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmAdministrarEgresos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(sender As System.Object, e As System.EventArgs) Handles btnListar.Click
        ListarComprobantes()
    End Sub

    Private Sub chkSucursal_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkSucursal.CheckedChanged
        cbxSucursal.Enabled = sender.Checked
    End Sub

    Private Sub chkProveedor_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkProveedor.CheckedChanged
        cbxProveedor.Enabled = sender.Checked
    End Sub

    Private Sub chkTipo_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkTipo.CheckedChanged
        cbxTipo.Enabled = sender.Checked
    End Sub

    Private Sub chkTipoComprobante_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkTipoComprobante.CheckedChanged
        cbxTipoComprobante.Enabled = sender.Checked
    End Sub

    Private Sub chkGrupo_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkGrupo.CheckedChanged
        cbxGrupo.Enabled = sender.Checked
    End Sub

    Private Sub chkMoneda_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkMoneda.CheckedChanged
        cbxMoneda.Enabled = sender.Checked
    End Sub

    Private Sub chkUsuario_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkUsuario.CheckedChanged
        cbxUsuario.Enabled = sender.Checked
    End Sub

    Private Sub lklSemana_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklSemana.LinkClicked
        dtpDesde.Value = DateAdd(DateInterval.Day, -(Now.DayOfWeek - 1), Now)
        dtpHasta.Value = DateAdd(DateInterval.Day, 7 - (Now.DayOfWeek), Now)
        ListarComprobantes()
    End Sub

    Private Sub lklMes_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklMes.LinkClicked
        dtpDesde.Value = "01/" & CSistema.FormatDobleDigito(Now.Month) & "/" & Now.Year
        dtpHasta.Value = Date.DaysInMonth(Now.Year, Now.Month) & "/" & CSistema.FormatDobleDigito(Now.Month) & "/" & Now.Year
        ListarComprobantes()
    End Sub

    Private Sub lklMesAnterior_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklMesAnterior.LinkClicked
        dtpDesde.Value = "01/" & CSistema.FormatDobleDigito(Now.Month - 1) & "/" & Now.Year
        dtpHasta.Value = Date.DaysInMonth(Now.Year, Now.Month - 1) & "/" & CSistema.FormatDobleDigito(Now.Month - 1) & "/" & Now.Year
        ListarComprobantes()
    End Sub

    Private Sub lklAño_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklAño.LinkClicked
        dtpDesde.Value = "01/01/" & Now.Year
        dtpHasta.Value = "31/12/" & Now.Year
        ListarComprobantes()
    End Sub

    Private Sub txtComprobante_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarComprobantes(" And Comprobante Like '%" & txtComprobante.GetValue & "%' ")
        End If
    End Sub

    Private Sub txtNumero_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtNumero.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarComprobantes(" And Numero = " & txtNumero.GetValue & " ")
        End If
    End Sub

    Private Sub txtNumeroOP_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtNumeroOP.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarComprobantes(" And NumeroOrdenPago = " & txtNumeroOP.GetValue & " ")
        End If
    End Sub

    Private Sub dgvComprobantes_SelectionChanged(sender As Object, e As System.EventArgs) Handles dgvComprobantes.SelectionChanged
        SeleccionarRegistro()
    End Sub

    Private Sub btnRepararSaldo_Click(sender As System.Object, e As System.EventArgs) Handles btnRepararSaldo.Click
        RepararSaldo()
    End Sub

    Private Sub lklExtractoProveedor_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklExtractoProveedor.LinkClicked
        ExtractoProveedor()
    End Sub

    Private Sub btnSaldosIncorrectos_Click(sender As System.Object, e As System.EventArgs) Handles btnSaldosIncorrectos.Click
        Dim Where As String = " And Saldo<>[dbo].[FSaldoEgreso](IDTransaccion)"
        ListarComprobantes(Where)
    End Sub

    Private Sub btnVerDetalle_Click(sender As System.Object, e As System.EventArgs) Handles btnVerDetalle.Click
        VerDetalle()
    End Sub

    Private Sub btnAsiento_Click(sender As System.Object, e As System.EventArgs) Handles btnAsiento.Click
        VerAsiento()
    End Sub

    Private Sub txtComprobante_Load(sender As Object, e As EventArgs) Handles txtComprobante.Load

    End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        If dtConsulta Is Nothing Then
            Exit Sub
        End If
        If dtConsulta.Rows.Count = 0 Then
            Exit Sub
        End If
        CSistema.dtToExcel2(dtConsulta, "Egresos")
    End Sub

    Private Sub chkFecha2_CheckedChanged(sender As Object, e As EventArgs) Handles chkFecha2.CheckedChanged
        rdbFechaDocumento2.Enabled = chkFecha2.Checked
        rdbFechaVencimiento2.Enabled = chkFecha2.Checked
        rdbFechaRegistro2.Enabled = chkFecha2.Checked
        txtDesde2.Enabled = chkFecha2.Checked
        txtHasta2.Enabled = chkFecha2.Checked
    End Sub
End Class
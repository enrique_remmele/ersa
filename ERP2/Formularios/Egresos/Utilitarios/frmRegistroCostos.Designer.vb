﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRegistroCostos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblPeriodoActual = New System.Windows.Forms.Label()
        Me.lblProducto = New System.Windows.Forms.Label()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblPromedio = New System.Windows.Forms.Label()
        Me.lblPeriodoAnterior = New System.Windows.Forms.Label()
        Me.lblPeriodoGeneral = New System.Windows.Forms.Label()
        Me.lblFechaInicio = New System.Windows.Forms.Label()
        Me.lblInformaciones = New System.Windows.Forms.Label()
        Me.lblUltimaActualizacion = New System.Windows.Forms.Label()
        Me.lblUltimoCosto = New System.Windows.Forms.Label()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.lnkUltimos3Periodos = New System.Windows.Forms.LinkLabel()
        Me.lnkPeriodosDelAño = New System.Windows.Forms.LinkLabel()
        Me.lblInforme = New System.Windows.Forms.Label()
        Me.lnkUltimoPeriodo = New System.Windows.Forms.LinkLabel()
        Me.lnkPeriodosPorFecha = New System.Windows.Forms.LinkLabel()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.lblCerrado = New System.Windows.Forms.Label()
        Me.lblUsuarioCierre = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.gbxDetalle = New System.Windows.Forms.GroupBox()
        Me.gbxInforme = New System.Windows.Forms.GroupBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnCerrarPeriodoProducto = New System.Windows.Forms.Button()
        Me.btnCerrarPeriodo = New System.Windows.Forms.Button()
        Me.btnEliminarPeriodo = New System.Windows.Forms.Button()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.txtFechaInicio = New ERP.ocxTXTString()
        Me.txtUltimaActualizacion = New ERP.ocxTXTString()
        Me.txtCerrado = New ERP.ocxTXTString()
        Me.txtUsuarioCierre = New ERP.ocxTXTString()
        Me.txtCantidadPeriodoActual = New ERP.ocxTXTNumeric()
        Me.txtTotalPeriodoActual = New ERP.ocxTXTNumeric()
        Me.txtPromedioPeriodoActual = New ERP.ocxTXTNumeric()
        Me.txtEstado = New ERP.ocxTXTString()
        Me.txtCantidadPeriodoAnterior = New ERP.ocxTXTNumeric()
        Me.txtTotalPeriodoAnterior = New ERP.ocxTXTNumeric()
        Me.txtPromedioPeriodoAnterior = New ERP.ocxTXTNumeric()
        Me.txtCantidadPeriodoGeneral = New ERP.ocxTXTNumeric()
        Me.txtTotalPeriodoGeneral = New ERP.ocxTXTNumeric()
        Me.txtPromedioPeriodoGeneral = New ERP.ocxTXTNumeric()
        Me.txtUltimoCosto = New ERP.ocxTXTNumeric()
        Me.gbxDetalle.SuspendLayout()
        Me.gbxInforme.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblPeriodoActual
        '
        Me.lblPeriodoActual.AutoSize = True
        Me.lblPeriodoActual.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodoActual.Location = New System.Drawing.Point(240, 37)
        Me.lblPeriodoActual.Name = "lblPeriodoActual"
        Me.lblPeriodoActual.Size = New System.Drawing.Size(76, 13)
        Me.lblPeriodoActual.TabIndex = 14
        Me.lblPeriodoActual.Text = "Periodo Actual"
        '
        'lblProducto
        '
        Me.lblProducto.AutoSize = True
        Me.lblProducto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProducto.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProducto.Location = New System.Drawing.Point(9, 11)
        Me.lblProducto.Name = "lblProducto"
        Me.lblProducto.Size = New System.Drawing.Size(62, 13)
        Me.lblProducto.TabIndex = 0
        Me.lblProducto.Text = "Producto:"
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCantidad.Location = New System.Drawing.Point(331, 16)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(61, 13)
        Me.lblCantidad.TabIndex = 11
        Me.lblCantidad.Text = "Cantidad:"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(433, 16)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(40, 13)
        Me.lblTotal.TabIndex = 12
        Me.lblTotal.Text = "Total:"
        '
        'lblPromedio
        '
        Me.lblPromedio.AutoSize = True
        Me.lblPromedio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPromedio.Location = New System.Drawing.Point(510, 16)
        Me.lblPromedio.Name = "lblPromedio"
        Me.lblPromedio.Size = New System.Drawing.Size(63, 13)
        Me.lblPromedio.TabIndex = 13
        Me.lblPromedio.Text = "Promedio:"
        '
        'lblPeriodoAnterior
        '
        Me.lblPeriodoAnterior.AutoSize = True
        Me.lblPeriodoAnterior.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodoAnterior.Location = New System.Drawing.Point(234, 59)
        Me.lblPeriodoAnterior.Name = "lblPeriodoAnterior"
        Me.lblPeriodoAnterior.Size = New System.Drawing.Size(82, 13)
        Me.lblPeriodoAnterior.TabIndex = 18
        Me.lblPeriodoAnterior.Text = "Periodo Anterior"
        '
        'lblPeriodoGeneral
        '
        Me.lblPeriodoGeneral.AutoSize = True
        Me.lblPeriodoGeneral.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodoGeneral.Location = New System.Drawing.Point(272, 81)
        Me.lblPeriodoGeneral.Name = "lblPeriodoGeneral"
        Me.lblPeriodoGeneral.Size = New System.Drawing.Size(44, 13)
        Me.lblPeriodoGeneral.TabIndex = 22
        Me.lblPeriodoGeneral.Text = "General"
        '
        'lblFechaInicio
        '
        Me.lblFechaInicio.AutoSize = True
        Me.lblFechaInicio.Location = New System.Drawing.Point(6, 37)
        Me.lblFechaInicio.Name = "lblFechaInicio"
        Me.lblFechaInicio.Size = New System.Drawing.Size(83, 13)
        Me.lblFechaInicio.TabIndex = 1
        Me.lblFechaInicio.Text = "Fecha de Inicio:"
        '
        'lblInformaciones
        '
        Me.lblInformaciones.AutoSize = True
        Me.lblInformaciones.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInformaciones.Location = New System.Drawing.Point(6, 16)
        Me.lblInformaciones.Name = "lblInformaciones"
        Me.lblInformaciones.Size = New System.Drawing.Size(90, 13)
        Me.lblInformaciones.TabIndex = 0
        Me.lblInformaciones.Text = "Informaciones:"
        '
        'lblUltimaActualizacion
        '
        Me.lblUltimaActualizacion.AutoSize = True
        Me.lblUltimaActualizacion.Location = New System.Drawing.Point(6, 59)
        Me.lblUltimaActualizacion.Name = "lblUltimaActualizacion"
        Me.lblUltimaActualizacion.Size = New System.Drawing.Size(92, 13)
        Me.lblUltimaActualizacion.TabIndex = 3
        Me.lblUltimaActualizacion.Text = "Ult. Actualizacion:"
        '
        'lblUltimoCosto
        '
        Me.lblUltimoCosto.AutoSize = True
        Me.lblUltimoCosto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUltimoCosto.Location = New System.Drawing.Point(441, 125)
        Me.lblUltimoCosto.Name = "lblUltimoCosto"
        Me.lblUltimoCosto.Size = New System.Drawing.Size(66, 13)
        Me.lblUltimoCosto.TabIndex = 26
        Me.lblUltimoCosto.Text = "Ultimo Costo"
        '
        'lvLista
        '
        Me.lvLista.Location = New System.Drawing.Point(6, 53)
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(566, 102)
        Me.lvLista.TabIndex = 7
        Me.lvLista.UseCompatibleStateImageBehavior = False
        '
        'dtpDesde
        '
        Me.dtpDesde.CustomFormat = "dd/MM/yyyy"
        Me.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDesde.Location = New System.Drawing.Point(272, 33)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(87, 20)
        Me.dtpDesde.TabIndex = 4
        '
        'dtpHasta
        '
        Me.dtpHasta.CustomFormat = "dd/MM/yyyy"
        Me.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpHasta.Location = New System.Drawing.Point(365, 33)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(87, 20)
        Me.dtpHasta.TabIndex = 5
        '
        'lnkUltimos3Periodos
        '
        Me.lnkUltimos3Periodos.AutoSize = True
        Me.lnkUltimos3Periodos.Location = New System.Drawing.Point(85, 37)
        Me.lnkUltimos3Periodos.Name = "lnkUltimos3Periodos"
        Me.lnkUltimos3Periodos.Size = New System.Drawing.Size(93, 13)
        Me.lnkUltimos3Periodos.TabIndex = 2
        Me.lnkUltimos3Periodos.TabStop = True
        Me.lnkUltimos3Periodos.Text = "Ultimos 3 periodos"
        '
        'lnkPeriodosDelAño
        '
        Me.lnkPeriodosDelAño.AutoSize = True
        Me.lnkPeriodosDelAño.Location = New System.Drawing.Point(180, 37)
        Me.lnkPeriodosDelAño.Name = "lnkPeriodosDelAño"
        Me.lnkPeriodosDelAño.Size = New System.Drawing.Size(86, 13)
        Me.lnkPeriodosDelAño.TabIndex = 3
        Me.lnkPeriodosDelAño.TabStop = True
        Me.lnkPeriodosDelAño.Text = "Periodos del año"
        '
        'lblInforme
        '
        Me.lblInforme.AutoSize = True
        Me.lblInforme.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.lblInforme.Location = New System.Drawing.Point(6, 16)
        Me.lblInforme.Name = "lblInforme"
        Me.lblInforme.Size = New System.Drawing.Size(59, 13)
        Me.lblInforme.TabIndex = 0
        Me.lblInforme.Text = "Informes:"
        '
        'lnkUltimoPeriodo
        '
        Me.lnkUltimoPeriodo.AutoSize = True
        Me.lnkUltimoPeriodo.Location = New System.Drawing.Point(6, 37)
        Me.lnkUltimoPeriodo.Name = "lnkUltimoPeriodo"
        Me.lnkUltimoPeriodo.Size = New System.Drawing.Size(74, 13)
        Me.lnkUltimoPeriodo.TabIndex = 1
        Me.lnkUltimoPeriodo.TabStop = True
        Me.lnkUltimoPeriodo.Text = "Ultimo periodo"
        '
        'lnkPeriodosPorFecha
        '
        Me.lnkPeriodosPorFecha.AutoSize = True
        Me.lnkPeriodosPorFecha.Location = New System.Drawing.Point(458, 37)
        Me.lnkPeriodosPorFecha.Name = "lnkPeriodosPorFecha"
        Me.lnkPeriodosPorFecha.Size = New System.Drawing.Size(96, 13)
        Me.lnkPeriodosPorFecha.TabIndex = 6
        Me.lnkPeriodosPorFecha.TabStop = True
        Me.lnkPeriodosPorFecha.Text = "Periodos por fecha"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(6, 81)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 5
        Me.lblEstado.Text = "Estado:"
        '
        'lblCerrado
        '
        Me.lblCerrado.AutoSize = True
        Me.lblCerrado.Location = New System.Drawing.Point(6, 103)
        Me.lblCerrado.Name = "lblCerrado"
        Me.lblCerrado.Size = New System.Drawing.Size(47, 13)
        Me.lblCerrado.TabIndex = 7
        Me.lblCerrado.Text = "Cerrado:"
        '
        'lblUsuarioCierre
        '
        Me.lblUsuarioCierre.AutoSize = True
        Me.lblUsuarioCierre.Location = New System.Drawing.Point(6, 125)
        Me.lblUsuarioCierre.Name = "lblUsuarioCierre"
        Me.lblUsuarioCierre.Size = New System.Drawing.Size(56, 13)
        Me.lblUsuarioCierre.TabIndex = 9
        Me.lblUsuarioCierre.Text = "Usr Cierre:"
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(501, 386)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 7
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'gbxDetalle
        '
        Me.gbxDetalle.Controls.Add(Me.lblInformaciones)
        Me.gbxDetalle.Controls.Add(Me.txtFechaInicio)
        Me.gbxDetalle.Controls.Add(Me.lblPeriodoActual)
        Me.gbxDetalle.Controls.Add(Me.txtUltimaActualizacion)
        Me.gbxDetalle.Controls.Add(Me.lblCantidad)
        Me.gbxDetalle.Controls.Add(Me.txtCerrado)
        Me.gbxDetalle.Controls.Add(Me.lblTotal)
        Me.gbxDetalle.Controls.Add(Me.lblPromedio)
        Me.gbxDetalle.Controls.Add(Me.txtUsuarioCierre)
        Me.gbxDetalle.Controls.Add(Me.txtCantidadPeriodoActual)
        Me.gbxDetalle.Controls.Add(Me.lblUsuarioCierre)
        Me.gbxDetalle.Controls.Add(Me.txtTotalPeriodoActual)
        Me.gbxDetalle.Controls.Add(Me.lblCerrado)
        Me.gbxDetalle.Controls.Add(Me.txtPromedioPeriodoActual)
        Me.gbxDetalle.Controls.Add(Me.txtEstado)
        Me.gbxDetalle.Controls.Add(Me.lblPeriodoAnterior)
        Me.gbxDetalle.Controls.Add(Me.lblEstado)
        Me.gbxDetalle.Controls.Add(Me.txtCantidadPeriodoAnterior)
        Me.gbxDetalle.Controls.Add(Me.txtTotalPeriodoAnterior)
        Me.gbxDetalle.Controls.Add(Me.txtPromedioPeriodoAnterior)
        Me.gbxDetalle.Controls.Add(Me.lblPeriodoGeneral)
        Me.gbxDetalle.Controls.Add(Me.txtCantidadPeriodoGeneral)
        Me.gbxDetalle.Controls.Add(Me.txtTotalPeriodoGeneral)
        Me.gbxDetalle.Controls.Add(Me.txtPromedioPeriodoGeneral)
        Me.gbxDetalle.Controls.Add(Me.lblFechaInicio)
        Me.gbxDetalle.Controls.Add(Me.lblUltimaActualizacion)
        Me.gbxDetalle.Controls.Add(Me.txtUltimoCosto)
        Me.gbxDetalle.Controls.Add(Me.lblUltimoCosto)
        Me.gbxDetalle.Location = New System.Drawing.Point(3, 223)
        Me.gbxDetalle.Name = "gbxDetalle"
        Me.gbxDetalle.Size = New System.Drawing.Size(581, 151)
        Me.gbxDetalle.TabIndex = 3
        Me.gbxDetalle.TabStop = False
        '
        'gbxInforme
        '
        Me.gbxInforme.Controls.Add(Me.lblInforme)
        Me.gbxInforme.Controls.Add(Me.lvLista)
        Me.gbxInforme.Controls.Add(Me.dtpDesde)
        Me.gbxInforme.Controls.Add(Me.lnkPeriodosPorFecha)
        Me.gbxInforme.Controls.Add(Me.dtpHasta)
        Me.gbxInforme.Controls.Add(Me.lnkUltimoPeriodo)
        Me.gbxInforme.Controls.Add(Me.lnkUltimos3Periodos)
        Me.gbxInforme.Controls.Add(Me.lnkPeriodosDelAño)
        Me.gbxInforme.Location = New System.Drawing.Point(3, 53)
        Me.gbxInforme.Name = "gbxInforme"
        Me.gbxInforme.Size = New System.Drawing.Size(581, 164)
        Me.gbxInforme.TabIndex = 2
        Me.gbxInforme.TabStop = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 418)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(592, 22)
        Me.StatusStrip1.TabIndex = 8
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'btnCerrarPeriodoProducto
        '
        Me.btnCerrarPeriodoProducto.Location = New System.Drawing.Point(8, 386)
        Me.btnCerrarPeriodoProducto.Name = "btnCerrarPeriodoProducto"
        Me.btnCerrarPeriodoProducto.Size = New System.Drawing.Size(158, 23)
        Me.btnCerrarPeriodoProducto.TabIndex = 4
        Me.btnCerrarPeriodoProducto.Text = "Cerrar el periodo del producto"
        Me.btnCerrarPeriodoProducto.UseVisualStyleBackColor = True
        '
        'btnCerrarPeriodo
        '
        Me.btnCerrarPeriodo.Location = New System.Drawing.Point(172, 386)
        Me.btnCerrarPeriodo.Name = "btnCerrarPeriodo"
        Me.btnCerrarPeriodo.Size = New System.Drawing.Size(97, 23)
        Me.btnCerrarPeriodo.TabIndex = 5
        Me.btnCerrarPeriodo.Text = "Cerrar periodo"
        Me.btnCerrarPeriodo.UseVisualStyleBackColor = True
        '
        'btnEliminarPeriodo
        '
        Me.btnEliminarPeriodo.Location = New System.Drawing.Point(275, 386)
        Me.btnEliminarPeriodo.Name = "btnEliminarPeriodo"
        Me.btnEliminarPeriodo.Size = New System.Drawing.Size(106, 23)
        Me.btnEliminarPeriodo.TabIndex = 6
        Me.btnEliminarPeriodo.Text = "Eliminar periodo"
        Me.btnEliminarPeriodo.UseVisualStyleBackColor = True
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 181
        Me.txtProducto.Location = New System.Drawing.Point(9, 27)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.Size = New System.Drawing.Size(446, 20)
        Me.txtProducto.TabIndex = 1
        '
        'txtFechaInicio
        '
        Me.txtFechaInicio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFechaInicio.Indicaciones = Nothing
        Me.txtFechaInicio.Location = New System.Drawing.Point(104, 33)
        Me.txtFechaInicio.Name = "txtFechaInicio"
        Me.txtFechaInicio.Size = New System.Drawing.Size(74, 21)
        Me.txtFechaInicio.SoloLectura = False
        Me.txtFechaInicio.TabIndex = 2
        Me.txtFechaInicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFechaInicio.Texto = ""
        '
        'txtUltimaActualizacion
        '
        Me.txtUltimaActualizacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUltimaActualizacion.Indicaciones = Nothing
        Me.txtUltimaActualizacion.Location = New System.Drawing.Point(104, 55)
        Me.txtUltimaActualizacion.Name = "txtUltimaActualizacion"
        Me.txtUltimaActualizacion.Size = New System.Drawing.Size(74, 21)
        Me.txtUltimaActualizacion.SoloLectura = False
        Me.txtUltimaActualizacion.TabIndex = 4
        Me.txtUltimaActualizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUltimaActualizacion.Texto = ""
        '
        'txtCerrado
        '
        Me.txtCerrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCerrado.Indicaciones = Nothing
        Me.txtCerrado.Location = New System.Drawing.Point(104, 99)
        Me.txtCerrado.Name = "txtCerrado"
        Me.txtCerrado.Size = New System.Drawing.Size(74, 21)
        Me.txtCerrado.SoloLectura = False
        Me.txtCerrado.TabIndex = 8
        Me.txtCerrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCerrado.Texto = ""
        '
        'txtUsuarioCierre
        '
        Me.txtUsuarioCierre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUsuarioCierre.Indicaciones = Nothing
        Me.txtUsuarioCierre.Location = New System.Drawing.Point(104, 121)
        Me.txtUsuarioCierre.Name = "txtUsuarioCierre"
        Me.txtUsuarioCierre.Size = New System.Drawing.Size(123, 21)
        Me.txtUsuarioCierre.SoloLectura = False
        Me.txtUsuarioCierre.TabIndex = 10
        Me.txtUsuarioCierre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUsuarioCierre.Texto = ""
        '
        'txtCantidadPeriodoActual
        '
        Me.txtCantidadPeriodoActual.Decimales = True
        Me.txtCantidadPeriodoActual.Indicaciones = Nothing
        Me.txtCantidadPeriodoActual.Location = New System.Drawing.Point(322, 32)
        Me.txtCantidadPeriodoActual.Name = "txtCantidadPeriodoActual"
        Me.txtCantidadPeriodoActual.Size = New System.Drawing.Size(78, 22)
        Me.txtCantidadPeriodoActual.SoloLectura = False
        Me.txtCantidadPeriodoActual.TabIndex = 15
        Me.txtCantidadPeriodoActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadPeriodoActual.Texto = "0"
        '
        'txtTotalPeriodoActual
        '
        Me.txtTotalPeriodoActual.Decimales = True
        Me.txtTotalPeriodoActual.Indicaciones = Nothing
        Me.txtTotalPeriodoActual.Location = New System.Drawing.Point(400, 32)
        Me.txtTotalPeriodoActual.Name = "txtTotalPeriodoActual"
        Me.txtTotalPeriodoActual.Size = New System.Drawing.Size(107, 22)
        Me.txtTotalPeriodoActual.SoloLectura = False
        Me.txtTotalPeriodoActual.TabIndex = 16
        Me.txtTotalPeriodoActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalPeriodoActual.Texto = "0"
        '
        'txtPromedioPeriodoActual
        '
        Me.txtPromedioPeriodoActual.Decimales = True
        Me.txtPromedioPeriodoActual.Indicaciones = Nothing
        Me.txtPromedioPeriodoActual.Location = New System.Drawing.Point(507, 32)
        Me.txtPromedioPeriodoActual.Name = "txtPromedioPeriodoActual"
        Me.txtPromedioPeriodoActual.Size = New System.Drawing.Size(68, 22)
        Me.txtPromedioPeriodoActual.SoloLectura = False
        Me.txtPromedioPeriodoActual.TabIndex = 17
        Me.txtPromedioPeriodoActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPromedioPeriodoActual.Texto = "0"
        '
        'txtEstado
        '
        Me.txtEstado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstado.Indicaciones = Nothing
        Me.txtEstado.Location = New System.Drawing.Point(104, 77)
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.Size = New System.Drawing.Size(123, 21)
        Me.txtEstado.SoloLectura = False
        Me.txtEstado.TabIndex = 6
        Me.txtEstado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEstado.Texto = ""
        '
        'txtCantidadPeriodoAnterior
        '
        Me.txtCantidadPeriodoAnterior.Decimales = True
        Me.txtCantidadPeriodoAnterior.Indicaciones = Nothing
        Me.txtCantidadPeriodoAnterior.Location = New System.Drawing.Point(322, 54)
        Me.txtCantidadPeriodoAnterior.Name = "txtCantidadPeriodoAnterior"
        Me.txtCantidadPeriodoAnterior.Size = New System.Drawing.Size(78, 22)
        Me.txtCantidadPeriodoAnterior.SoloLectura = False
        Me.txtCantidadPeriodoAnterior.TabIndex = 19
        Me.txtCantidadPeriodoAnterior.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadPeriodoAnterior.Texto = "0"
        '
        'txtTotalPeriodoAnterior
        '
        Me.txtTotalPeriodoAnterior.Decimales = True
        Me.txtTotalPeriodoAnterior.Indicaciones = Nothing
        Me.txtTotalPeriodoAnterior.Location = New System.Drawing.Point(400, 54)
        Me.txtTotalPeriodoAnterior.Name = "txtTotalPeriodoAnterior"
        Me.txtTotalPeriodoAnterior.Size = New System.Drawing.Size(107, 22)
        Me.txtTotalPeriodoAnterior.SoloLectura = False
        Me.txtTotalPeriodoAnterior.TabIndex = 20
        Me.txtTotalPeriodoAnterior.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalPeriodoAnterior.Texto = "0"
        '
        'txtPromedioPeriodoAnterior
        '
        Me.txtPromedioPeriodoAnterior.Decimales = True
        Me.txtPromedioPeriodoAnterior.Indicaciones = Nothing
        Me.txtPromedioPeriodoAnterior.Location = New System.Drawing.Point(507, 54)
        Me.txtPromedioPeriodoAnterior.Name = "txtPromedioPeriodoAnterior"
        Me.txtPromedioPeriodoAnterior.Size = New System.Drawing.Size(68, 22)
        Me.txtPromedioPeriodoAnterior.SoloLectura = False
        Me.txtPromedioPeriodoAnterior.TabIndex = 21
        Me.txtPromedioPeriodoAnterior.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPromedioPeriodoAnterior.Texto = "0"
        '
        'txtCantidadPeriodoGeneral
        '
        Me.txtCantidadPeriodoGeneral.Decimales = True
        Me.txtCantidadPeriodoGeneral.Indicaciones = Nothing
        Me.txtCantidadPeriodoGeneral.Location = New System.Drawing.Point(322, 76)
        Me.txtCantidadPeriodoGeneral.Name = "txtCantidadPeriodoGeneral"
        Me.txtCantidadPeriodoGeneral.Size = New System.Drawing.Size(78, 22)
        Me.txtCantidadPeriodoGeneral.SoloLectura = False
        Me.txtCantidadPeriodoGeneral.TabIndex = 23
        Me.txtCantidadPeriodoGeneral.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadPeriodoGeneral.Texto = "0"
        '
        'txtTotalPeriodoGeneral
        '
        Me.txtTotalPeriodoGeneral.Decimales = True
        Me.txtTotalPeriodoGeneral.Indicaciones = Nothing
        Me.txtTotalPeriodoGeneral.Location = New System.Drawing.Point(400, 76)
        Me.txtTotalPeriodoGeneral.Name = "txtTotalPeriodoGeneral"
        Me.txtTotalPeriodoGeneral.Size = New System.Drawing.Size(107, 22)
        Me.txtTotalPeriodoGeneral.SoloLectura = False
        Me.txtTotalPeriodoGeneral.TabIndex = 24
        Me.txtTotalPeriodoGeneral.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalPeriodoGeneral.Texto = "0"
        '
        'txtPromedioPeriodoGeneral
        '
        Me.txtPromedioPeriodoGeneral.Decimales = True
        Me.txtPromedioPeriodoGeneral.Indicaciones = Nothing
        Me.txtPromedioPeriodoGeneral.Location = New System.Drawing.Point(507, 76)
        Me.txtPromedioPeriodoGeneral.Name = "txtPromedioPeriodoGeneral"
        Me.txtPromedioPeriodoGeneral.Size = New System.Drawing.Size(68, 22)
        Me.txtPromedioPeriodoGeneral.SoloLectura = False
        Me.txtPromedioPeriodoGeneral.TabIndex = 25
        Me.txtPromedioPeriodoGeneral.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPromedioPeriodoGeneral.Texto = "0"
        '
        'txtUltimoCosto
        '
        Me.txtUltimoCosto.Decimales = True
        Me.txtUltimoCosto.Indicaciones = Nothing
        Me.txtUltimoCosto.Location = New System.Drawing.Point(507, 120)
        Me.txtUltimoCosto.Name = "txtUltimoCosto"
        Me.txtUltimoCosto.Size = New System.Drawing.Size(68, 22)
        Me.txtUltimoCosto.SoloLectura = False
        Me.txtUltimoCosto.TabIndex = 27
        Me.txtUltimoCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtUltimoCosto.Texto = "0"
        '
        'frmRegistroCostos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(592, 440)
        Me.Controls.Add(Me.txtProducto)
        Me.Controls.Add(Me.btnEliminarPeriodo)
        Me.Controls.Add(Me.btnCerrarPeriodo)
        Me.Controls.Add(Me.btnCerrarPeriodoProducto)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.gbxInforme)
        Me.Controls.Add(Me.gbxDetalle)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.lblProducto)
        Me.Name = "frmRegistroCostos"
        Me.Text = "frmRegistroCostos"
        Me.gbxDetalle.ResumeLayout(False)
        Me.gbxDetalle.PerformLayout()
        Me.gbxInforme.ResumeLayout(False)
        Me.gbxInforme.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents lblPeriodoActual As System.Windows.Forms.Label
    Friend WithEvents lblProducto As System.Windows.Forms.Label
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lblPromedio As System.Windows.Forms.Label
    Friend WithEvents txtCantidadPeriodoActual As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalPeriodoActual As ERP.ocxTXTNumeric
    Friend WithEvents txtPromedioPeriodoActual As ERP.ocxTXTNumeric
    Friend WithEvents txtPromedioPeriodoAnterior As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalPeriodoAnterior As ERP.ocxTXTNumeric
    Friend WithEvents txtCantidadPeriodoAnterior As ERP.ocxTXTNumeric
    Friend WithEvents lblPeriodoAnterior As System.Windows.Forms.Label
    Friend WithEvents txtPromedioPeriodoGeneral As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalPeriodoGeneral As ERP.ocxTXTNumeric
    Friend WithEvents txtCantidadPeriodoGeneral As ERP.ocxTXTNumeric
    Friend WithEvents lblPeriodoGeneral As System.Windows.Forms.Label
    Friend WithEvents lblFechaInicio As System.Windows.Forms.Label
    Friend WithEvents lblInformaciones As System.Windows.Forms.Label
    Friend WithEvents lblUltimaActualizacion As System.Windows.Forms.Label
    Friend WithEvents txtUltimoCosto As ERP.ocxTXTNumeric
    Friend WithEvents lblUltimoCosto As System.Windows.Forms.Label
    Friend WithEvents lvLista As System.Windows.Forms.ListView
    Friend WithEvents dtpDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents lnkUltimos3Periodos As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkPeriodosDelAño As System.Windows.Forms.LinkLabel
    Friend WithEvents lblInforme As System.Windows.Forms.Label
    Friend WithEvents lnkUltimoPeriodo As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkPeriodosPorFecha As System.Windows.Forms.LinkLabel
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents txtEstado As ERP.ocxTXTString
    Friend WithEvents lblCerrado As System.Windows.Forms.Label
    Friend WithEvents txtUsuarioCierre As ERP.ocxTXTString
    Friend WithEvents lblUsuarioCierre As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents txtCerrado As ERP.ocxTXTString
    Friend WithEvents txtUltimaActualizacion As ERP.ocxTXTString
    Friend WithEvents txtFechaInicio As ERP.ocxTXTString
    Friend WithEvents gbxDetalle As System.Windows.Forms.GroupBox
    Friend WithEvents gbxInforme As System.Windows.Forms.GroupBox
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnCerrarPeriodo As System.Windows.Forms.Button
    Friend WithEvents btnCerrarPeriodoProducto As System.Windows.Forms.Button
    Friend WithEvents btnEliminarPeriodo As System.Windows.Forms.Button
End Class

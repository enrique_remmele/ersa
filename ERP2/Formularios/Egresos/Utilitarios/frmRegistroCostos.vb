﻿Public Class frmRegistroCostos

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES

    'EVENTOS

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        'Inicializar Controles
        CSistema.InicializaControles(gbxDetalle)

        'DateTimePiker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Controles
        txtProducto.Conectar()

    End Sub

    Sub Listar(ByVal Where As String, Optional ByVal Top As String = "")

        ctrError.Clear()
        tsslEstado.Text = ""

        'Verificamos que se haya seleccionado un producto
        'Seleccion de Producto
        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim IDProducto As Integer = txtProducto.Registro("ID").ToString

        Dim sql As String
        If Top = "" Then
            sql = " ID, Producto, Inicio, [Ult. Act.], Estado, Cierre, Usuario From VRegistroCosto Where IDProducto=" & IDProducto & " "
        Else
            sql = Top & " ID, Producto, Inicio, [Ult. Act.], Estado, Cierre, Usuario From VRegistroCosto Where IDProducto=" & IDProducto & " "
        End If


        Dim OrderBy As String = " Order By FechaInicio Desc "

        'Listar
        CSistema.SqlToLv(lvLista, sql & Where & OrderBy)

        'Formato

    End Sub

    Sub ObtenerInformacion()

        LimpiarDetalle()
        ctrError.Clear()
        tsslEstado.Text = ""

        'Seleccion
        If lvLista.SelectedItems.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvLista, mensaje)
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Obtenemos la informacion
        Dim ID As Integer
        Dim dt As DataTable

        ID = lvLista.SelectedItems(0).Text
        dt = CSistema.ExecuteToDataTable("Select * From VRegistroCosto Where ID=" & ID).Copy

        For Each oRow As DataRow In dt.Rows

            txtFechaInicio.txt.Text = oRow("Inicio").ToString
            txtUltimaActualizacion.txt.Text = oRow("Ult. Act.").ToString
            txtEstado.txt.Text = oRow("Estado").ToString
            txtCerrado.txt.Text = oRow("Cierre").ToString
            txtUsuarioCierre.txt.Text = oRow("Usuario").ToString

            'Montos
            txtCantidadPeriodoActual.txt.Text = oRow("ActualCantidad").ToString
            txtTotalPeriodoActual.txt.Text = oRow("ActualTotalCosto").ToString
            txtPromedioPeriodoActual.txt.Text = oRow("ActualCostoPromedio").ToString

            txtCantidadPeriodoAnterior.txt.Text = oRow("AnteriorCantidad").ToString
            txtTotalPeriodoAnterior.txt.Text = oRow("AnteriorTotalCosto").ToString
            txtPromedioPeriodoAnterior.txt.Text = oRow("AnteriorCostoPromedio").ToString

            txtCantidadPeriodoGeneral.txt.Text = oRow("GeneralCantidad").ToString
            txtTotalPeriodoGeneral.txt.Text = oRow("GeneralTotalCosto").ToString
            txtPromedioPeriodoGeneral.txt.Text = oRow("GeneralCostoPromedio").ToString

            txtUltimoCosto.txt.Text = oRow("UltimoCosto").ToString

        Next

    End Sub

    Sub LimpiarDetalle()

    End Sub

    Sub CerrarPeriodoProducto()

    End Sub

    Sub CerrarPeriodo()

    End Sub

    Sub EliminarPeriodo()

    End Sub

    Private Sub frmRegistroCostos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub lnkUltimoPeriodo_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkUltimoPeriodo.LinkClicked
        Listar("", "Select Top(1) ")
    End Sub

    Private Sub lnkUltimos3Periodos_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkUltimos3Periodos.LinkClicked
        Listar("", "Select Top(3) ")
    End Sub

    Private Sub lnkPeriodosDelAño_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkPeriodosDelAño.LinkClicked
        Listar(" And Year(FechaInicio) = Year(GetDate())  ")
    End Sub

    Private Sub lnkPeriodosPorFecha_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkPeriodosPorFecha.LinkClicked
        Listar(" And (FechaInicio Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' )")
    End Sub

    Private Sub btnCerrarPeriodoProducto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrarPeriodoProducto.Click
        CerrarPeriodoProducto()
    End Sub

    Private Sub btnCerrarPeriodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrarPeriodo.Click
        CerrarPeriodo()
    End Sub

    Private Sub btnEliminarPeriodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarPeriodo.Click
        EliminarPeriodo()
    End Sub

    Private Sub txtProducto_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then
            lnkUltimoPeriodo.Focus()
        End If
    End Sub

    Private Sub lvLista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvLista.SelectedIndexChanged
        ObtenerInformacion()
    End Sub

End Class
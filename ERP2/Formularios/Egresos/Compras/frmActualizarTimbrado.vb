﻿Public Class frmActualizarTimbrado

    'CLASES
    Dim CSistema As New CSistema

    Sub Inicializar()

        txtProveedor.Conectar()

    End Sub

    Sub Procesar()

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento() = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        CSistema.SetSQLParameter(param, "@NroTimbrado", txtTimbrado.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NumeroTimbradoRemplazar", txtTimbradoReemplazar.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@VtoTimbrado", CSistema.FormatoFechaBaseDatos(txtVtoTimbrado.txt.Text, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.txtID.ObtenerValor, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", "UPD", ParameterDirection.Input)

        IndiceOperacion = param.GetLength(0) - 1

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)


        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpActualizarTimbrado", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnProcesar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnProcesar, ErrorIconAlignment.TopRight)
            Exit Sub

        Else

            MessageBox.Show("Registro guardado!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()


        End If


    End Sub

    Function ValidarDocumento() As Boolean

        ValidarDocumento = False

        'Validar

        'Cliente
        If txtProveedor.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente un cliente!"
            ctrError.SetError(btnProcesar, mensaje)
            ctrError.SetIconAlignment(btnProcesar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Timbrado
        If txtTimbrado.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de timbrado!"
            ctrError.SetError(btnProcesar, mensaje)
            ctrError.SetIconAlignment(btnProcesar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If txtVtoTimbrado.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de timbrado a reemplazar!"
            ctrError.SetError(btnProcesar, mensaje)
            ctrError.SetIconAlignment(btnProcesar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If
      
        Return True

    End Function

    Private Sub frmActualizarTimbrado_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmModificarCompra
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblCondicion = New System.Windows.Forms.Label()
        Me.lblVencimiento = New System.Windows.Forms.Label()
        Me.lblPlazo = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxCondicion = New ERP.ocxCBX()
        Me.txtVencimiento = New ERP.ocxTXTDate()
        Me.txtPlazo = New ERP.ocxTXTString()
        Me.txtVtoTimbrado = New ERP.ocxTXTDate()
        Me.txtTimbrado = New ERP.ocxTXTString()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chkfe = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbxDepartamento = New ERP.ocxCBX()
        Me.SuspendLayout()
        '
        'lblCondicion
        '
        Me.lblCondicion.AutoSize = True
        Me.lblCondicion.Location = New System.Drawing.Point(23, 21)
        Me.lblCondicion.Name = "lblCondicion"
        Me.lblCondicion.Size = New System.Drawing.Size(60, 13)
        Me.lblCondicion.TabIndex = 0
        Me.lblCondicion.Text = "Condición :"
        '
        'lblVencimiento
        '
        Me.lblVencimiento.AutoSize = True
        Me.lblVencimiento.Location = New System.Drawing.Point(214, 21)
        Me.lblVencimiento.Name = "lblVencimiento"
        Me.lblVencimiento.Size = New System.Drawing.Size(71, 13)
        Me.lblVencimiento.TabIndex = 1
        Me.lblVencimiento.Text = "Vencimiento :"
        '
        'lblPlazo
        '
        Me.lblPlazo.AutoSize = True
        Me.lblPlazo.Location = New System.Drawing.Point(424, 21)
        Me.lblPlazo.Name = "lblPlazo"
        Me.lblPlazo.Size = New System.Drawing.Size(39, 13)
        Me.lblPlazo.TabIndex = 2
        Me.lblPlazo.Text = "Plazo :"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(23, 85)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(73, 13)
        Me.lblObservacion.TabIndex = 3
        Me.lblObservacion.Text = "Observación :"
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(401, 157)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 8
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(489, 157)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 10
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(102, 85)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(462, 24)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 12
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxCondicion
        '
        Me.cbxCondicion.CampoWhere = Nothing
        Me.cbxCondicion.CargarUnaSolaVez = False
        Me.cbxCondicion.DataDisplayMember = Nothing
        Me.cbxCondicion.DataFilter = Nothing
        Me.cbxCondicion.DataOrderBy = Nothing
        Me.cbxCondicion.DataSource = Nothing
        Me.cbxCondicion.DataValueMember = Nothing
        Me.cbxCondicion.dtSeleccionado = Nothing
        Me.cbxCondicion.FormABM = Nothing
        Me.cbxCondicion.Indicaciones = Nothing
        Me.cbxCondicion.Location = New System.Drawing.Point(89, 13)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.SeleccionMultiple = False
        Me.cbxCondicion.SeleccionObligatoria = False
        Me.cbxCondicion.Size = New System.Drawing.Size(88, 21)
        Me.cbxCondicion.SoloLectura = False
        Me.cbxCondicion.TabIndex = 11
        Me.cbxCondicion.Texto = ""
        '
        'txtVencimiento
        '
        Me.txtVencimiento.AñoFecha = 0
        Me.txtVencimiento.Color = System.Drawing.Color.Empty
        Me.txtVencimiento.Fecha = New Date(CType(0, Long))
        Me.txtVencimiento.Location = New System.Drawing.Point(288, 13)
        Me.txtVencimiento.MesFecha = 0
        Me.txtVencimiento.Name = "txtVencimiento"
        Me.txtVencimiento.PermitirNulo = True
        Me.txtVencimiento.Size = New System.Drawing.Size(84, 21)
        Me.txtVencimiento.SoloLectura = False
        Me.txtVencimiento.TabIndex = 9
        '
        'txtPlazo
        '
        Me.txtPlazo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPlazo.Color = System.Drawing.Color.Empty
        Me.txtPlazo.Indicaciones = Nothing
        Me.txtPlazo.Location = New System.Drawing.Point(482, 12)
        Me.txtPlazo.Multilinea = False
        Me.txtPlazo.Name = "txtPlazo"
        Me.txtPlazo.Size = New System.Drawing.Size(82, 22)
        Me.txtPlazo.SoloLectura = False
        Me.txtPlazo.TabIndex = 4
        Me.txtPlazo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPlazo.Texto = ""
        '
        'txtVtoTimbrado
        '
        Me.txtVtoTimbrado.AñoFecha = 0
        Me.txtVtoTimbrado.Color = System.Drawing.Color.Empty
        Me.txtVtoTimbrado.Fecha = New Date(CType(0, Long))
        Me.txtVtoTimbrado.Location = New System.Drawing.Point(480, 51)
        Me.txtVtoTimbrado.MesFecha = 0
        Me.txtVtoTimbrado.Name = "txtVtoTimbrado"
        Me.txtVtoTimbrado.PermitirNulo = True
        Me.txtVtoTimbrado.Size = New System.Drawing.Size(84, 21)
        Me.txtVtoTimbrado.SoloLectura = False
        Me.txtVtoTimbrado.TabIndex = 16
        '
        'txtTimbrado
        '
        Me.txtTimbrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTimbrado.Color = System.Drawing.Color.Empty
        Me.txtTimbrado.Indicaciones = Nothing
        Me.txtTimbrado.Location = New System.Drawing.Point(288, 50)
        Me.txtTimbrado.Multilinea = False
        Me.txtTimbrado.Name = "txtTimbrado"
        Me.txtTimbrado.Size = New System.Drawing.Size(82, 22)
        Me.txtTimbrado.SoloLectura = False
        Me.txtTimbrado.TabIndex = 15
        Me.txtTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTimbrado.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(214, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 13)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Timbrado :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(387, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Vto Timbrado :"
        '
        'chkfe
        '
        Me.chkfe.AutoSize = True
        Me.chkfe.Location = New System.Drawing.Point(26, 55)
        Me.chkfe.Name = "chkfe"
        Me.chkfe.Size = New System.Drawing.Size(182, 17)
        Me.chkfe.TabIndex = 17
        Me.chkfe.Text = "Es Fact Electrónica / Fact Virtual"
        Me.chkfe.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(19, 123)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 13)
        Me.Label4.TabIndex = 32
        Me.Label4.Text = "Departamento:"
        '
        'cbxDepartamento
        '
        Me.cbxDepartamento.CampoWhere = Nothing
        Me.cbxDepartamento.CargarUnaSolaVez = False
        Me.cbxDepartamento.DataDisplayMember = Nothing
        Me.cbxDepartamento.DataFilter = Nothing
        Me.cbxDepartamento.DataOrderBy = Nothing
        Me.cbxDepartamento.DataSource = Nothing
        Me.cbxDepartamento.DataValueMember = Nothing
        Me.cbxDepartamento.dtSeleccionado = Nothing
        Me.cbxDepartamento.FormABM = Nothing
        Me.cbxDepartamento.Indicaciones = Nothing
        Me.cbxDepartamento.Location = New System.Drawing.Point(102, 115)
        Me.cbxDepartamento.Name = "cbxDepartamento"
        Me.cbxDepartamento.SeleccionMultiple = False
        Me.cbxDepartamento.SeleccionObligatoria = False
        Me.cbxDepartamento.Size = New System.Drawing.Size(263, 21)
        Me.cbxDepartamento.SoloLectura = False
        Me.cbxDepartamento.TabIndex = 31
        Me.cbxDepartamento.Texto = ""
        '
        'frmModificarCompra
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(580, 189)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cbxDepartamento)
        Me.Controls.Add(Me.chkfe)
        Me.Controls.Add(Me.txtVtoTimbrado)
        Me.Controls.Add(Me.txtTimbrado)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtObservacion)
        Me.Controls.Add(Me.cbxCondicion)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.txtVencimiento)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.txtPlazo)
        Me.Controls.Add(Me.lblObservacion)
        Me.Controls.Add(Me.lblPlazo)
        Me.Controls.Add(Me.lblVencimiento)
        Me.Controls.Add(Me.lblCondicion)
        Me.Name = "frmModificarCompra"
        Me.Text = "frmModificarCompra"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblCondicion As Label
    Friend WithEvents lblVencimiento As Label
    Friend WithEvents lblPlazo As Label
    Friend WithEvents lblObservacion As Label
    Friend WithEvents txtPlazo As ocxTXTString
    Friend WithEvents btnGuardar As Button
    Friend WithEvents txtVencimiento As ocxTXTDate
    Friend WithEvents btnSalir As Button
    Friend WithEvents cbxCondicion As ocxCBX
    Friend WithEvents txtObservacion As ocxTXTString
    Friend WithEvents txtVtoTimbrado As ocxTXTDate
    Friend WithEvents txtTimbrado As ocxTXTString
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents chkfe As CheckBox
    Friend WithEvents Label4 As Label
    Friend WithEvents cbxDepartamento As ocxCBX
End Class

﻿Public Class frmCambioCostoCompra

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private ProductoValue As DataRow
    Public Property Producto() As DataRow
        Get
            Return ProductoValue
        End Get
        Set(ByVal value As DataRow)
            ProductoValue = value
        End Set
    End Property

    Private CostoActualValue As Decimal
    Public Property CostoActual() As Decimal
        Get
            Return CostoActualValue
        End Get
        Set(ByVal value As Decimal)
            CostoActualValue = value
        End Set
    End Property

    Private AceptadoValue As Boolean
    Public Property Aceptado() As Boolean
        Get
            Return AceptadoValue
        End Get
        Set(ByVal value As Boolean)
            AceptadoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        If Producto Is Nothing Then
            MessageBox.Show("Error interno! Vuelva a intentarlo.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Aceptado = False
            Me.Close()
        End If

        txtProducto.SetValue(Producto("Descripcion").ToString)
        txtUltimoCosto.SetValue(Producto("UltimoCosto").ToString)
        txtCostoActual.SetValue(CostoActual)

        Dim Diferencia As Decimal = txtUltimoCosto.ObtenerValor - txtCostoActual.ObtenerValor
        txtDiferencia.SetValue(Diferencia)

        btnCancelar.Focus()

    End Sub

    Private Sub frmCambioCostoCompra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Me.Aceptado = True
        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Aceptado = False
        Me.Close()
    End Sub
End Class
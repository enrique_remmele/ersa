﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaCompra
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkSucursal = New System.Windows.Forms.CheckBox()
        Me.cbxSucursal = New System.Windows.Forms.ComboBox()
        Me.btnRegistrosGenerales = New System.Windows.Forms.Button()
        Me.chkUsuario = New System.Windows.Forms.CheckBox()
        Me.cbxUsuario = New System.Windows.Forms.ComboBox()
        Me.chkCondicion = New System.Windows.Forms.CheckBox()
        Me.cbxCondicion = New System.Windows.Forms.ComboBox()
        Me.dtbRegistrosPorFecha = New System.Windows.Forms.Button()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.btnRegistrosDelMes = New System.Windows.Forms.Button()
        Me.btnRegistrosDelDia = New System.Windows.Forms.Button()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.cbxComprobante = New ERP.ocxCBX()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.txtOperacion = New ERP.ocxTXTNumeric()
        Me.txtComprobante = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtCantidadOperacion = New ERP.ocxTXTNumeric()
        Me.lblCantidadOperacion = New System.Windows.Forms.Label()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.txtCantidadDetalle = New ERP.ocxTXTNumeric()
        Me.lblCantidadDetalle = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.lblTotalDetalle = New System.Windows.Forms.Label()
        Me.txtTotalDetalle = New ERP.ocxTXTNumeric()
        Me.lvDetalle = New System.Windows.Forms.ListView()
        Me.lvOperacion = New System.Windows.Forms.ListView()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblTotalOperacion = New System.Windows.Forms.Label()
        Me.txtTotalOperacion = New ERP.ocxTXTNumeric()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 220.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(754, 425)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkSucursal)
        Me.GroupBox1.Controls.Add(Me.cbxSucursal)
        Me.GroupBox1.Controls.Add(Me.btnRegistrosGenerales)
        Me.GroupBox1.Controls.Add(Me.chkUsuario)
        Me.GroupBox1.Controls.Add(Me.cbxUsuario)
        Me.GroupBox1.Controls.Add(Me.chkCondicion)
        Me.GroupBox1.Controls.Add(Me.cbxCondicion)
        Me.GroupBox1.Controls.Add(Me.dtbRegistrosPorFecha)
        Me.GroupBox1.Controls.Add(Me.dtpHasta)
        Me.GroupBox1.Controls.Add(Me.dtpDesde)
        Me.GroupBox1.Controls.Add(Me.lblFecha)
        Me.GroupBox1.Controls.Add(Me.btnRegistrosDelMes)
        Me.GroupBox1.Controls.Add(Me.btnRegistrosDelDia)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(537, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(214, 419)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'chkSucursal
        '
        Me.chkSucursal.AutoSize = True
        Me.chkSucursal.Location = New System.Drawing.Point(6, 13)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(70, 17)
        Me.chkSucursal.TabIndex = 0
        Me.chkSucursal.Text = "Sucursal:"
        Me.chkSucursal.UseVisualStyleBackColor = True
        '
        'cbxSucursal
        '
        Me.cbxSucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormattingEnabled = True
        Me.cbxSucursal.Location = New System.Drawing.Point(6, 36)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.Size = New System.Drawing.Size(204, 21)
        Me.cbxSucursal.TabIndex = 1
        '
        'btnRegistrosGenerales
        '
        Me.btnRegistrosGenerales.Location = New System.Drawing.Point(6, 228)
        Me.btnRegistrosGenerales.Name = "btnRegistrosGenerales"
        Me.btnRegistrosGenerales.Size = New System.Drawing.Size(204, 23)
        Me.btnRegistrosGenerales.TabIndex = 11
        Me.btnRegistrosGenerales.Text = "Registro General"
        Me.btnRegistrosGenerales.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegistrosGenerales.UseVisualStyleBackColor = True
        '
        'chkUsuario
        '
        Me.chkUsuario.AutoSize = True
        Me.chkUsuario.Location = New System.Drawing.Point(6, 115)
        Me.chkUsuario.Name = "chkUsuario"
        Me.chkUsuario.Size = New System.Drawing.Size(65, 17)
        Me.chkUsuario.TabIndex = 7
        Me.chkUsuario.Text = "Usuario:"
        Me.chkUsuario.UseVisualStyleBackColor = True
        '
        'cbxUsuario
        '
        Me.cbxUsuario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxUsuario.Enabled = False
        Me.cbxUsuario.FormattingEnabled = True
        Me.cbxUsuario.Location = New System.Drawing.Point(6, 138)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.Size = New System.Drawing.Size(204, 21)
        Me.cbxUsuario.TabIndex = 8
        '
        'chkCondicion
        '
        Me.chkCondicion.AutoSize = True
        Me.chkCondicion.Location = New System.Drawing.Point(6, 63)
        Me.chkCondicion.Name = "chkCondicion"
        Me.chkCondicion.Size = New System.Drawing.Size(76, 17)
        Me.chkCondicion.TabIndex = 2
        Me.chkCondicion.Text = "Condicion:"
        Me.chkCondicion.UseVisualStyleBackColor = True
        '
        'cbxCondicion
        '
        Me.cbxCondicion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxCondicion.Enabled = False
        Me.cbxCondicion.FormattingEnabled = True
        Me.cbxCondicion.Location = New System.Drawing.Point(6, 86)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.Size = New System.Drawing.Size(204, 21)
        Me.cbxCondicion.TabIndex = 3
        '
        'dtbRegistrosPorFecha
        '
        Me.dtbRegistrosPorFecha.Location = New System.Drawing.Point(6, 345)
        Me.dtbRegistrosPorFecha.Name = "dtbRegistrosPorFecha"
        Me.dtbRegistrosPorFecha.Size = New System.Drawing.Size(204, 23)
        Me.dtbRegistrosPorFecha.TabIndex = 15
        Me.dtbRegistrosPorFecha.Text = "Registros por Fecha"
        Me.dtbRegistrosPorFecha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.dtbRegistrosPorFecha.UseVisualStyleBackColor = True
        '
        'dtpHasta
        '
        Me.dtpHasta.Location = New System.Drawing.Point(6, 314)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(204, 20)
        Me.dtpHasta.TabIndex = 14
        '
        'dtpDesde
        '
        Me.dtpDesde.Location = New System.Drawing.Point(6, 288)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(204, 20)
        Me.dtpDesde.TabIndex = 13
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(6, 272)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(145, 13)
        Me.lblFecha.TabIndex = 12
        Me.lblFecha.Text = "Fecha del Comprobante:"
        '
        'btnRegistrosDelMes
        '
        Me.btnRegistrosDelMes.Location = New System.Drawing.Point(6, 199)
        Me.btnRegistrosDelMes.Name = "btnRegistrosDelMes"
        Me.btnRegistrosDelMes.Size = New System.Drawing.Size(204, 23)
        Me.btnRegistrosDelMes.TabIndex = 10
        Me.btnRegistrosDelMes.Text = "Registros del Mes"
        Me.btnRegistrosDelMes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegistrosDelMes.UseVisualStyleBackColor = True
        '
        'btnRegistrosDelDia
        '
        Me.btnRegistrosDelDia.Location = New System.Drawing.Point(6, 170)
        Me.btnRegistrosDelDia.Name = "btnRegistrosDelDia"
        Me.btnRegistrosDelDia.Size = New System.Drawing.Size(204, 23)
        Me.btnRegistrosDelDia.TabIndex = 9
        Me.btnRegistrosDelDia.Text = "Registros del Dia"
        Me.btnRegistrosDelDia.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegistrosDelDia.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel4, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.lvDetalle, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.lvOperacion, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel5, 0, 2)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 5
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(528, 419)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 4
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 98.00995!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.99005!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Panel3, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel2, 2, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(522, 30)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.cbxComprobante)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.lblOperacion)
        Me.Panel3.Controls.Add(Me.txtOperacion)
        Me.Panel3.Controls.Add(Me.txtComprobante)
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(371, 24)
        Me.Panel3.TabIndex = 0
        '
        'cbxComprobante
        '
        Me.cbxComprobante.CampoWhere = Nothing
        Me.cbxComprobante.DataDisplayMember = Nothing
        Me.cbxComprobante.DataFilter = Nothing
        Me.cbxComprobante.DataOrderBy = Nothing
        Me.cbxComprobante.DataSource = Nothing
        Me.cbxComprobante.DataValueMember = Nothing
        Me.cbxComprobante.FormABM = Nothing
        Me.cbxComprobante.Indicaciones = Nothing
        Me.cbxComprobante.Location = New System.Drawing.Point(193, 1)
        Me.cbxComprobante.Name = "cbxComprobante"
        Me.cbxComprobante.SeleccionObligatoria = False
        Me.cbxComprobante.Size = New System.Drawing.Size(90, 21)
        Me.cbxComprobante.SoloLectura = False
        Me.cbxComprobante.TabIndex = 8
        Me.cbxComprobante.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(121, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Comprobante:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(-1, 6)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'txtOperacion
        '
        Me.txtOperacion.Color = System.Drawing.Color.Empty
        Me.txtOperacion.Decimales = True
        Me.txtOperacion.Indicaciones = Nothing
        Me.txtOperacion.Location = New System.Drawing.Point(59, 1)
        Me.txtOperacion.Name = "txtOperacion"
        Me.txtOperacion.Size = New System.Drawing.Size(56, 22)
        Me.txtOperacion.SoloLectura = False
        Me.txtOperacion.TabIndex = 3
        Me.txtOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOperacion.Texto = "0"
        '
        'txtComprobante
        '
        Me.txtComprobante.Location = New System.Drawing.Point(286, 1)
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(85, 20)
        Me.txtComprobante.TabIndex = 6
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txtCantidadOperacion)
        Me.Panel2.Controls.Add(Me.lblCantidadOperacion)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(387, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(123, 24)
        Me.Panel2.TabIndex = 1
        '
        'txtCantidadOperacion
        '
        Me.txtCantidadOperacion.Color = System.Drawing.Color.Empty
        Me.txtCantidadOperacion.Decimales = True
        Me.txtCantidadOperacion.Indicaciones = Nothing
        Me.txtCantidadOperacion.Location = New System.Drawing.Point(78, 0)
        Me.txtCantidadOperacion.Name = "txtCantidadOperacion"
        Me.txtCantidadOperacion.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadOperacion.SoloLectura = False
        Me.txtCantidadOperacion.TabIndex = 4
        Me.txtCantidadOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadOperacion.Texto = "0"
        '
        'lblCantidadOperacion
        '
        Me.lblCantidadOperacion.AutoSize = True
        Me.lblCantidadOperacion.Location = New System.Drawing.Point(3, 6)
        Me.lblCantidadOperacion.Name = "lblCantidadOperacion"
        Me.lblCantidadOperacion.Size = New System.Drawing.Size(74, 13)
        Me.lblCantidadOperacion.TabIndex = 0
        Me.lblCantidadOperacion.Text = "Cant Registro:"
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 4
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 49.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 101.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Panel6, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel5, 2, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.txtTotalDetalle, 3, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 385)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(522, 31)
        Me.TableLayoutPanel4.TabIndex = 2
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.txtCantidadDetalle)
        Me.Panel6.Controls.Add(Me.lblCantidadDetalle)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(3, 3)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(180, 25)
        Me.Panel6.TabIndex = 0
        '
        'txtCantidadDetalle
        '
        Me.txtCantidadDetalle.Color = System.Drawing.Color.Empty
        Me.txtCantidadDetalle.Decimales = True
        Me.txtCantidadDetalle.Indicaciones = Nothing
        Me.txtCantidadDetalle.Location = New System.Drawing.Point(98, 2)
        Me.txtCantidadDetalle.Name = "txtCantidadDetalle"
        Me.txtCantidadDetalle.Size = New System.Drawing.Size(48, 22)
        Me.txtCantidadDetalle.SoloLectura = False
        Me.txtCantidadDetalle.TabIndex = 4
        Me.txtCantidadDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadDetalle.Texto = "0"
        '
        'lblCantidadDetalle
        '
        Me.lblCantidadDetalle.AutoSize = True
        Me.lblCantidadDetalle.Location = New System.Drawing.Point(3, 7)
        Me.lblCantidadDetalle.Name = "lblCantidadDetalle"
        Me.lblCantidadDetalle.Size = New System.Drawing.Size(94, 13)
        Me.lblCantidadDetalle.TabIndex = 0
        Me.lblCantidadDetalle.Text = "Cantidad Registro:"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.lblTotalDetalle)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(375, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(43, 25)
        Me.Panel5.TabIndex = 1
        '
        'lblTotalDetalle
        '
        Me.lblTotalDetalle.AutoSize = True
        Me.lblTotalDetalle.Location = New System.Drawing.Point(4, 7)
        Me.lblTotalDetalle.Name = "lblTotalDetalle"
        Me.lblTotalDetalle.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalDetalle.TabIndex = 0
        Me.lblTotalDetalle.Text = "Total:"
        '
        'txtTotalDetalle
        '
        Me.txtTotalDetalle.Color = System.Drawing.Color.Empty
        Me.txtTotalDetalle.Decimales = True
        Me.txtTotalDetalle.Indicaciones = Nothing
        Me.txtTotalDetalle.Location = New System.Drawing.Point(424, 3)
        Me.txtTotalDetalle.Name = "txtTotalDetalle"
        Me.txtTotalDetalle.Size = New System.Drawing.Size(95, 22)
        Me.txtTotalDetalle.SoloLectura = False
        Me.txtTotalDetalle.TabIndex = 4
        Me.txtTotalDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDetalle.Texto = "0"
        '
        'lvDetalle
        '
        Me.lvDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvDetalle.Location = New System.Drawing.Point(3, 230)
        Me.lvDetalle.Name = "lvDetalle"
        Me.lvDetalle.Size = New System.Drawing.Size(522, 149)
        Me.lvDetalle.TabIndex = 3
        Me.lvDetalle.UseCompatibleStateImageBehavior = False
        '
        'lvOperacion
        '
        Me.lvOperacion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvOperacion.Location = New System.Drawing.Point(3, 39)
        Me.lvOperacion.Name = "lvOperacion"
        Me.lvOperacion.Size = New System.Drawing.Size(522, 149)
        Me.lvOperacion.TabIndex = 1
        Me.lvOperacion.UseCompatibleStateImageBehavior = False
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 4
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 49.0!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 101.0!))
        Me.TableLayoutPanel5.Controls.Add(Me.Panel1, 2, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.txtTotalOperacion, 3, 0)
        Me.TableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(3, 194)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(522, 30)
        Me.TableLayoutPanel5.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblTotalOperacion)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(375, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(43, 24)
        Me.Panel1.TabIndex = 0
        '
        'lblTotalOperacion
        '
        Me.lblTotalOperacion.AutoSize = True
        Me.lblTotalOperacion.Location = New System.Drawing.Point(4, 4)
        Me.lblTotalOperacion.Name = "lblTotalOperacion"
        Me.lblTotalOperacion.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalOperacion.TabIndex = 0
        Me.lblTotalOperacion.Text = "Total:"
        '
        'txtTotalOperacion
        '
        Me.txtTotalOperacion.Color = System.Drawing.Color.Empty
        Me.txtTotalOperacion.Decimales = True
        Me.txtTotalOperacion.Indicaciones = Nothing
        Me.txtTotalOperacion.Location = New System.Drawing.Point(424, 3)
        Me.txtTotalOperacion.Name = "txtTotalOperacion"
        Me.txtTotalOperacion.Size = New System.Drawing.Size(95, 22)
        Me.txtTotalOperacion.SoloLectura = False
        Me.txtTotalOperacion.TabIndex = 4
        Me.txtTotalOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalOperacion.Texto = "0"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmConsultaCompra
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(754, 425)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmConsultaCompra"
        Me.Text = "frmConsultaCompra"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents cbxSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents btnRegistrosGenerales As System.Windows.Forms.Button
    Friend WithEvents chkUsuario As System.Windows.Forms.CheckBox
    Friend WithEvents cbxUsuario As System.Windows.Forms.ComboBox
    Friend WithEvents txtComprobante As System.Windows.Forms.TextBox
    Friend WithEvents dtbRegistrosPorFecha As System.Windows.Forms.Button
    Friend WithEvents dtpHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents btnRegistrosDelMes As System.Windows.Forms.Button
    Friend WithEvents btnRegistrosDelDia As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblCantidadOperacion As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtOperacion As ERP.ocxTXTNumeric
    Friend WithEvents txtCantidadOperacion As ERP.ocxTXTNumeric
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidadDetalle As ERP.ocxTXTNumeric
    Friend WithEvents lblCantidadDetalle As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents lblTotalDetalle As System.Windows.Forms.Label
    Friend WithEvents txtTotalDetalle As ERP.ocxTXTNumeric
    Friend WithEvents lvDetalle As System.Windows.Forms.ListView
    Friend WithEvents lvOperacion As System.Windows.Forms.ListView
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblTotalOperacion As System.Windows.Forms.Label
    Friend WithEvents txtTotalOperacion As ERP.ocxTXTNumeric
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents chkCondicion As System.Windows.Forms.CheckBox
    Friend WithEvents cbxCondicion As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxComprobante As ERP.ocxCBX
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
End Class

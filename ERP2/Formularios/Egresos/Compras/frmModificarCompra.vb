﻿Public Class frmModificarCompra

    Dim Csistema As New CSistema
    Dim CData As New CData
    Public Property Condicion As String
    Public Property Vencimiento As String
    Public Property Plazo As String
    Public Property Observacion As String
    Public Property IDOperacion As Integer
    Public Property IDTransaccion As Integer
    Public Property Timbrado As String
    Public Property VtoTimbrado As String
    Public Property UnidadNegocio As String
    Public Property Departamento As String
    Public Property GastosVarios As Boolean
    Public Property IDSucursal As Integer

    Sub Inicializar()
        Cargar()
    End Sub

    Sub Cargar()
        cbxCondicion.cbx.Items.Add("Contado")
        cbxCondicion.cbx.Items.Add("Credito")
        cbxCondicion.cbx.Text = Condicion
        txtVencimiento.txt.Text = Vencimiento
        txtPlazo.txt.Text = Plazo
        txtObservacion.txt.Text = Observacion
        txtTimbrado.txt.Text = Timbrado
        txtVtoTimbrado.txt.Text = VtoTimbrado

        'chkGastosVarios.Checked = GastosVarios
        'chkGastosVarios.Enabled = False
        If IDSucursal = 1 Then
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " IDSucursal in (1,4)")
            Csistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
        Else
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " IDSucursal=" & IDSucursal)
            Csistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
        End If

        'Csistema.SqlToComboBox(cbxUnidadNegocio.cbx, CData.GetTable("vUnidadNegocio"), "ID", "Descripcion")
        'If GastosVarios = True Then
        '    cbxUnidadNegocio.cbx.Enabled = False
        '    cbxUnidadNegocio.cbx.Text = ""
        'Else
        '    cbxUnidadNegocio.cbx.Enabled = True
        '    cbxUnidadNegocio.cbx.Text = UnidadNegocio
        'End If

        cbxDepartamento.cbx.Text = Departamento

    End Sub

    Sub Guardar()
        Dim Credito As Boolean
        If cbxCondicion.cbx.Text = "Credito" Then
            Credito = True
        Else
            Credito = False
        End If

        Try
            Dim param(-1) As SqlClient.SqlParameter

            Csistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@Credito", Credito, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@FechaVencimiento", Csistema.FormatoFechaBaseDatos(txtVencimiento.txt.Text, True, False), ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@NroTimbrado", txtTimbrado.txt.Text, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@VtoTimbrado", Csistema.FormatoFechaBaseDatos(txtVtoTimbrado.txt.Text, True, False), ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@Operacion", "UPD", ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDterminal", vgIDTerminal, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDTransaccionSalida", 0, ParameterDirection.Output)

            If chkfe.Checked = True Then
                Csistema.SetSQLParameter(param, "@EsFe", chkfe.Checked, ParameterDirection.Input)
            End If

            'If chkGastosVarios.Checked = False Then
            '    Csistema.SetSQLParameter(param, "@IDUnidadNegocio", cbxUnidadNegocio.cbx.SelectedValue, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDDepartamentoEmpresa", cbxDepartamento.cbx.SelectedValue, ParameterDirection.Input)
            'Else
            '    Csistema.SetSQLParameter(param, "@IDDepartamentoEmpresa", cbxDepartamento.cbx.SelectedValue, ParameterDirection.Input)
            'End If

            'Informacion de Salida
            Csistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            Csistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Aplicar Modificaciones
            If Csistema.ExecuteStoreProcedure(param, "SpCompra", False, False, MensajeRetorno) = False Then
                MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

            Me.Close()

        Catch ex As Exception

        End Try
    End Sub


    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()

    End Sub

    Private Sub frmModificarCompra_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub cbxCondicion_PropertyChanged(sender As Object, e As EventArgs) Handles cbxCondicion.PropertyChanged
        If cbxCondicion.cbx.Text = "Contado" Then
            txtPlazo.txt.Text = 0
            txtPlazo.Enabled = False
        Else
            txtPlazo.Enabled = True
        End If
    End Sub
End Class
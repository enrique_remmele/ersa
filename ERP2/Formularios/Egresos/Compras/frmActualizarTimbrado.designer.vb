﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmActualizarTimbrado
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnProcesar = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.txtVtoTimbrado = New ERP.ocxTXTDate()
        Me.txtTimbradoReemplazar = New ERP.ocxTXTString()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.txtTimbrado = New ERP.ocxTXTString()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(4, 10)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(59, 13)
        Me.lblProveedor.TabIndex = 5
        Me.lblProveedor.Text = "Proveedor:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 13)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Nro. Timbrado:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtProveedor)
        Me.GroupBox1.Controls.Add(Me.txtVtoTimbrado)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtTimbradoReemplazar)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.lblProveedor)
        Me.GroupBox1.Controls.Add(Me.txtTimbrado)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(495, 98)
        Me.GroupBox1.TabIndex = 24
        Me.GroupBox1.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(280, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Vto. Timbrado:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(138, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 13)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "Reemplazar Por:"
        '
        'btnProcesar
        '
        Me.btnProcesar.Location = New System.Drawing.Point(333, 103)
        Me.btnProcesar.Name = "btnProcesar"
        Me.btnProcesar.Size = New System.Drawing.Size(75, 23)
        Me.btnProcesar.TabIndex = 25
        Me.btnProcesar.Text = "&Procesar"
        Me.btnProcesar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 127)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(507, 22)
        Me.StatusStrip1.TabIndex = 26
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'txtVtoTimbrado
        '
        Me.txtVtoTimbrado.Color = System.Drawing.Color.Empty
        Me.txtVtoTimbrado.Fecha = New Date(2013, 5, 20, 14, 20, 16, 53)
        Me.txtVtoTimbrado.Location = New System.Drawing.Point(282, 74)
        Me.txtVtoTimbrado.Name = "txtVtoTimbrado"
        Me.txtVtoTimbrado.PermitirNulo = False
        Me.txtVtoTimbrado.Size = New System.Drawing.Size(74, 20)
        Me.txtVtoTimbrado.SoloLectura = False
        Me.txtVtoTimbrado.TabIndex = 27
        '
        'txtTimbradoReemplazar
        '
        Me.txtTimbradoReemplazar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTimbradoReemplazar.Color = System.Drawing.Color.Empty
        Me.txtTimbradoReemplazar.Indicaciones = Nothing
        Me.txtTimbradoReemplazar.Location = New System.Drawing.Point(141, 75)
        Me.txtTimbradoReemplazar.Multilinea = False
        Me.txtTimbradoReemplazar.Name = "txtTimbradoReemplazar"
        Me.txtTimbradoReemplazar.Size = New System.Drawing.Size(124, 21)
        Me.txtTimbradoReemplazar.SoloLectura = False
        Me.txtTimbradoReemplazar.TabIndex = 25
        Me.txtTimbradoReemplazar.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTimbradoReemplazar.Texto = ""
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 80
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(6, 29)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(480, 24)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 6
        '
        'txtTimbrado
        '
        Me.txtTimbrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTimbrado.Color = System.Drawing.Color.Empty
        Me.txtTimbrado.Indicaciones = Nothing
        Me.txtTimbrado.Location = New System.Drawing.Point(7, 75)
        Me.txtTimbrado.Multilinea = False
        Me.txtTimbrado.Name = "txtTimbrado"
        Me.txtTimbrado.Size = New System.Drawing.Size(124, 21)
        Me.txtTimbrado.SoloLectura = False
        Me.txtTimbrado.TabIndex = 23
        Me.txtTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTimbrado.Texto = ""
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(413, 103)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 27
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'frmActualizarTimbrado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(507, 149)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnProcesar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmActualizarTimbrado"
        Me.Text = "frmActualizarTimbrado"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtProveedor As ERP.ocxTXTProveedor
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTimbrado As ERP.ocxTXTString
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtVtoTimbrado As ERP.ocxTXTDate
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtTimbradoReemplazar As ERP.ocxTXTString
    Friend WithEvents btnProcesar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCambioCostoCompra
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtProducto = New ERP.ocxTXTString()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtUltimoCosto = New ERP.ocxTXTNumeric()
        Me.txtCostoActual = New ERP.ocxTXTNumeric()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtDiferencia = New ERP.ocxTXTNumeric()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(327, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "- El costo unitario introducido es diferente al ultimo costo de compra!"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Producto:"
        '
        'txtProducto
        '
        Me.txtProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProducto.Color = System.Drawing.Color.Empty
        Me.txtProducto.Indicaciones = Nothing
        Me.txtProducto.Location = New System.Drawing.Point(82, 12)
        Me.txtProducto.Multilinea = False
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(245, 21)
        Me.txtProducto.SoloLectura = True
        Me.txtProducto.TabIndex = 2
        Me.txtProducto.TabStop = False
        Me.txtProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtProducto.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 44)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Ultimo Costo:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 71)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Costo Actual:"
        '
        'txtUltimoCosto
        '
        Me.txtUltimoCosto.Color = System.Drawing.Color.Empty
        Me.txtUltimoCosto.Decimales = True
        Me.txtUltimoCosto.Indicaciones = Nothing
        Me.txtUltimoCosto.Location = New System.Drawing.Point(81, 39)
        Me.txtUltimoCosto.Name = "txtUltimoCosto"
        Me.txtUltimoCosto.Size = New System.Drawing.Size(120, 22)
        Me.txtUltimoCosto.SoloLectura = True
        Me.txtUltimoCosto.TabIndex = 4
        Me.txtUltimoCosto.TabStop = False
        Me.txtUltimoCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtUltimoCosto.Texto = "0"
        '
        'txtCostoActual
        '
        Me.txtCostoActual.Color = System.Drawing.Color.Empty
        Me.txtCostoActual.Decimales = True
        Me.txtCostoActual.Indicaciones = Nothing
        Me.txtCostoActual.Location = New System.Drawing.Point(82, 66)
        Me.txtCostoActual.Name = "txtCostoActual"
        Me.txtCostoActual.Size = New System.Drawing.Size(120, 22)
        Me.txtCostoActual.SoloLectura = True
        Me.txtCostoActual.TabIndex = 6
        Me.txtCostoActual.TabStop = False
        Me.txtCostoActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCostoActual.Texto = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 187)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(91, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Desea continuar?"
        '
        'txtDiferencia
        '
        Me.txtDiferencia.Color = System.Drawing.Color.Empty
        Me.txtDiferencia.Decimales = True
        Me.txtDiferencia.Indicaciones = Nothing
        Me.txtDiferencia.Location = New System.Drawing.Point(81, 94)
        Me.txtDiferencia.Name = "txtDiferencia"
        Me.txtDiferencia.Size = New System.Drawing.Size(120, 22)
        Me.txtDiferencia.SoloLectura = True
        Me.txtDiferencia.TabIndex = 8
        Me.txtDiferencia.TabStop = False
        Me.txtDiferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDiferencia.Texto = "0"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(5, 99)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 13)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Diferencia:"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(15, 214)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 3
        Me.btnAceptar.Text = "Si"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(96, 214)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 4
        Me.btnCancelar.Text = "No"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtProducto)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtDiferencia)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtUltimoCosto)
        Me.GroupBox1.Controls.Add(Me.txtCostoActual)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 34)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(334, 131)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'frmCambioCostoCompra
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(351, 253)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmCambioCostoCompra"
        Me.Text = "frmCambioCostoCompra"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtProducto As ERP.ocxTXTString
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtUltimoCosto As ERP.ocxTXTNumeric
    Friend WithEvents txtCostoActual As ERP.ocxTXTNumeric
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtDiferencia As ERP.ocxTXTNumeric
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
End Class

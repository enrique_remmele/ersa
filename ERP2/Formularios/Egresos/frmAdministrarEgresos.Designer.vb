﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdministrarEgresos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim lblTotalDolares As System.Windows.Forms.Label
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.dgvComprobantes = New System.Windows.Forms.DataGridView()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ExportarAPlanillaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btnExportar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkFecha2 = New System.Windows.Forms.CheckBox()
        Me.txtHasta2 = New System.Windows.Forms.DateTimePicker()
        Me.txtDesde2 = New System.Windows.Forms.DateTimePicker()
        Me.FlowLayoutPanel7 = New System.Windows.Forms.FlowLayoutPanel()
        Me.rdbFechaDocumento2 = New System.Windows.Forms.RadioButton()
        Me.rdbFechaRegistro2 = New System.Windows.Forms.RadioButton()
        Me.rdbFechaVencimiento2 = New System.Windows.Forms.RadioButton()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.rdbEstadoAmbos = New System.Windows.Forms.RadioButton()
        Me.rdbEstadoPendientes = New System.Windows.Forms.RadioButton()
        Me.rdbEstadoCancelados = New System.Windows.Forms.RadioButton()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.rdbCondicionAmbos = New System.Windows.Forms.RadioButton()
        Me.rdbCondicionCredito = New System.Windows.Forms.RadioButton()
        Me.rdbCondicionContado = New System.Windows.Forms.RadioButton()
        Me.lblCondicion = New System.Windows.Forms.Label()
        Me.chkUsuario = New System.Windows.Forms.CheckBox()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.chkMoneda = New System.Windows.Forms.CheckBox()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.chkGrupo = New System.Windows.Forms.CheckBox()
        Me.cbxGrupo = New ERP.ocxCBX()
        Me.chkTipoComprobante = New System.Windows.Forms.CheckBox()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.lblFiltros = New System.Windows.Forms.Label()
        Me.lklAño = New System.Windows.Forms.LinkLabel()
        Me.lklMesAnterior = New System.Windows.Forms.LinkLabel()
        Me.lklMes = New System.Windows.Forms.LinkLabel()
        Me.lklSemana = New System.Windows.Forms.LinkLabel()
        Me.chkTipo = New System.Windows.Forms.CheckBox()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.rdbFechaDocumento = New System.Windows.Forms.RadioButton()
        Me.rdbFechaRegistro = New System.Windows.Forms.RadioButton()
        Me.rdbFechaVencimiento = New System.Windows.Forms.RadioButton()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.lblFechaReporte = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.btnSaldosIncorrectos = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.chkSucursal = New System.Windows.Forms.CheckBox()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.lblNumero = New System.Windows.Forms.Label()
        Me.txtNumero = New ERP.ocxTXTString()
        Me.lblOrdenPago = New System.Windows.Forms.Label()
        Me.txtNumeroOP = New ERP.ocxTXTString()
        Me.chkProveedor = New System.Windows.Forms.CheckBox()
        Me.cbxProveedor = New ERP.ocxCBX()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnVerDetalle = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnRepararSaldo = New System.Windows.Forms.Button()
        Me.lklExtractoProveedor = New System.Windows.Forms.LinkLabel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtTotalGuaranies = New ERP.ocxTXTNumeric()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtSaldoGuaranies = New ERP.ocxTXTNumeric()
        Me.lblCantidadOperacion = New System.Windows.Forms.Label()
        Me.txtCantidadOperacion = New ERP.ocxTXTNumeric()
        Me.TxtTotalDolares = New ERP.ocxTXTNumeric()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TxtSaldoDolares = New ERP.ocxTXTNumeric()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCantidadUSD = New ERP.ocxTXTNumeric()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.OcxImpuesto1 = New ERP.ocxImpuesto()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtObservacion = New System.Windows.Forms.TextBox()
        Me.txtVencimientoTimbrado = New System.Windows.Forms.TextBox()
        Me.txtTimbrado = New System.Windows.Forms.TextBox()
        Me.txtFechaHora = New System.Windows.Forms.TextBox()
        Me.txtTerminal = New System.Windows.Forms.TextBox()
        Me.txtSucursal = New System.Windows.Forms.TextBox()
        Me.txtUsuario = New System.Windows.Forms.TextBox()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.txtIDTransaccion = New System.Windows.Forms.TextBox()
        Me.lblVencimientoTimbrado = New System.Windows.Forms.Label()
        Me.lblFechaHora = New System.Windows.Forms.Label()
        Me.lblTimbrado = New System.Windows.Forms.Label()
        Me.lblTerminal = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblUsuario = New System.Windows.Forms.Label()
        Me.lblTransaccion = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.dgvExtracto = New System.Windows.Forms.DataGridView()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        lblTotalDolares = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvComprobantes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.FlowLayoutPanel7.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.dgvExtracto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTotalDolares
        '
        lblTotalDolares.AutoSize = True
        lblTotalDolares.Location = New System.Drawing.Point(3, 38)
        lblTotalDolares.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
        lblTotalDolares.Name = "lblTotalDolares"
        lblTotalDolares.Size = New System.Drawing.Size(85, 13)
        lblTotalDolares.TabIndex = 8
        lblTotalDolares.Text = "    Total Dolares:"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 603.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 244.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.StatusStrip1, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TabControl1, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.TabControl2, 0, 3)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 141.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1327, 622)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'StatusStrip1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.StatusStrip1, 2)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 602)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1083, 20)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 15)
        '
        'Panel2
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.Panel2, 2)
        Me.Panel2.Controls.Add(Me.dgvComprobantes)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 38)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1077, 356)
        Me.Panel2.TabIndex = 1
        '
        'dgvComprobantes
        '
        Me.dgvComprobantes.AllowUserToAddRows = False
        Me.dgvComprobantes.AllowUserToDeleteRows = False
        Me.dgvComprobantes.BackgroundColor = System.Drawing.Color.White
        Me.dgvComprobantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvComprobantes.ContextMenuStrip = Me.ContextMenuStrip1
        Me.dgvComprobantes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvComprobantes.Location = New System.Drawing.Point(0, 0)
        Me.dgvComprobantes.MultiSelect = False
        Me.dgvComprobantes.Name = "dgvComprobantes"
        Me.dgvComprobantes.ReadOnly = True
        Me.dgvComprobantes.RowHeadersVisible = False
        Me.dgvComprobantes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvComprobantes.Size = New System.Drawing.Size(1077, 356)
        Me.dgvComprobantes.StandardTab = True
        Me.dgvComprobantes.TabIndex = 0
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExportarAPlanillaToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(169, 26)
        '
        'ExportarAPlanillaToolStripMenuItem
        '
        Me.ExportarAPlanillaToolStripMenuItem.Name = "ExportarAPlanillaToolStripMenuItem"
        Me.ExportarAPlanillaToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ExportarAPlanillaToolStripMenuItem.Text = "Exportar a Planilla"
        '
        'TabControl1
        '
        Me.TabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(1086, 3)
        Me.TabControl1.Name = "TabControl1"
        Me.TableLayoutPanel1.SetRowSpan(Me.TabControl1, 5)
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(238, 616)
        Me.TabControl1.TabIndex = 4
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btnExportar)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.chkFecha2)
        Me.TabPage1.Controls.Add(Me.txtHasta2)
        Me.TabPage1.Controls.Add(Me.txtDesde2)
        Me.TabPage1.Controls.Add(Me.FlowLayoutPanel7)
        Me.TabPage1.Controls.Add(Me.FlowLayoutPanel6)
        Me.TabPage1.Controls.Add(Me.lblEstado)
        Me.TabPage1.Controls.Add(Me.FlowLayoutPanel2)
        Me.TabPage1.Controls.Add(Me.lblCondicion)
        Me.TabPage1.Controls.Add(Me.chkUsuario)
        Me.TabPage1.Controls.Add(Me.cbxUsuario)
        Me.TabPage1.Controls.Add(Me.chkMoneda)
        Me.TabPage1.Controls.Add(Me.cbxMoneda)
        Me.TabPage1.Controls.Add(Me.chkGrupo)
        Me.TabPage1.Controls.Add(Me.cbxGrupo)
        Me.TabPage1.Controls.Add(Me.chkTipoComprobante)
        Me.TabPage1.Controls.Add(Me.cbxTipoComprobante)
        Me.TabPage1.Controls.Add(Me.lblFiltros)
        Me.TabPage1.Controls.Add(Me.lklAño)
        Me.TabPage1.Controls.Add(Me.lklMesAnterior)
        Me.TabPage1.Controls.Add(Me.lklMes)
        Me.TabPage1.Controls.Add(Me.lklSemana)
        Me.TabPage1.Controls.Add(Me.chkTipo)
        Me.TabPage1.Controls.Add(Me.cbxTipo)
        Me.TabPage1.Controls.Add(Me.dtpHasta)
        Me.TabPage1.Controls.Add(Me.dtpDesde)
        Me.TabPage1.Controls.Add(Me.FlowLayoutPanel5)
        Me.TabPage1.Controls.Add(Me.lblFecha)
        Me.TabPage1.Controls.Add(Me.btnListar)
        Me.TabPage1.Location = New System.Drawing.Point(4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(230, 590)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btnExportar
        '
        Me.btnExportar.Location = New System.Drawing.Point(125, 152)
        Me.btnExportar.Name = "btnExportar"
        Me.btnExportar.Size = New System.Drawing.Size(100, 30)
        Me.btnExportar.TabIndex = 33
        Me.btnExportar.Text = "Exportar"
        Me.btnExportar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExportar.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(58, 128)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 32
        Me.Label3.Text = "Hasta:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(58, 107)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 13)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "Desde:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(61, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Hasta:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(61, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Desde:"
        '
        'chkFecha2
        '
        Me.chkFecha2.AutoSize = True
        Me.chkFecha2.Location = New System.Drawing.Point(4, 84)
        Me.chkFecha2.Name = "chkFecha2"
        Me.chkFecha2.Size = New System.Drawing.Size(56, 17)
        Me.chkFecha2.TabIndex = 28
        Me.chkFecha2.Text = "2 Fec:"
        Me.chkFecha2.UseVisualStyleBackColor = True
        '
        'txtHasta2
        '
        Me.txtHasta2.Enabled = False
        Me.txtHasta2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtHasta2.Location = New System.Drawing.Point(121, 128)
        Me.txtHasta2.Name = "txtHasta2"
        Me.txtHasta2.Size = New System.Drawing.Size(103, 20)
        Me.txtHasta2.TabIndex = 27
        '
        'txtDesde2
        '
        Me.txtDesde2.Enabled = False
        Me.txtDesde2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtDesde2.Location = New System.Drawing.Point(121, 107)
        Me.txtDesde2.Name = "txtDesde2"
        Me.txtDesde2.Size = New System.Drawing.Size(103, 20)
        Me.txtDesde2.TabIndex = 26
        '
        'FlowLayoutPanel7
        '
        Me.FlowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FlowLayoutPanel7.Controls.Add(Me.rdbFechaDocumento2)
        Me.FlowLayoutPanel7.Controls.Add(Me.rdbFechaRegistro2)
        Me.FlowLayoutPanel7.Controls.Add(Me.rdbFechaVencimiento2)
        Me.FlowLayoutPanel7.Location = New System.Drawing.Point(60, 82)
        Me.FlowLayoutPanel7.Name = "FlowLayoutPanel7"
        Me.FlowLayoutPanel7.Size = New System.Drawing.Size(164, 20)
        Me.FlowLayoutPanel7.TabIndex = 25
        '
        'rdbFechaDocumento2
        '
        Me.rdbFechaDocumento2.AutoSize = True
        Me.rdbFechaDocumento2.Checked = True
        Me.rdbFechaDocumento2.Enabled = False
        Me.rdbFechaDocumento2.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbFechaDocumento2.Location = New System.Drawing.Point(2, 1)
        Me.rdbFechaDocumento2.Margin = New System.Windows.Forms.Padding(2, 1, 3, 3)
        Me.rdbFechaDocumento2.Name = "rdbFechaDocumento2"
        Me.rdbFechaDocumento2.Size = New System.Drawing.Size(56, 16)
        Me.rdbFechaDocumento2.TabIndex = 0
        Me.rdbFechaDocumento2.TabStop = True
        Me.rdbFechaDocumento2.Text = "Docum."
        Me.rdbFechaDocumento2.UseVisualStyleBackColor = True
        '
        'rdbFechaRegistro2
        '
        Me.rdbFechaRegistro2.AutoSize = True
        Me.rdbFechaRegistro2.Enabled = False
        Me.rdbFechaRegistro2.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbFechaRegistro2.Location = New System.Drawing.Point(62, 1)
        Me.rdbFechaRegistro2.Margin = New System.Windows.Forms.Padding(1, 1, 3, 3)
        Me.rdbFechaRegistro2.Name = "rdbFechaRegistro2"
        Me.rdbFechaRegistro2.Size = New System.Drawing.Size(43, 16)
        Me.rdbFechaRegistro2.TabIndex = 1
        Me.rdbFechaRegistro2.Text = "Reg."
        Me.rdbFechaRegistro2.UseVisualStyleBackColor = True
        '
        'rdbFechaVencimiento2
        '
        Me.rdbFechaVencimiento2.AutoSize = True
        Me.rdbFechaVencimiento2.Enabled = False
        Me.rdbFechaVencimiento2.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbFechaVencimiento2.Location = New System.Drawing.Point(109, 1)
        Me.rdbFechaVencimiento2.Margin = New System.Windows.Forms.Padding(1, 1, 3, 3)
        Me.rdbFechaVencimiento2.Name = "rdbFechaVencimiento2"
        Me.rdbFechaVencimiento2.Size = New System.Drawing.Size(48, 16)
        Me.rdbFechaVencimiento2.TabIndex = 2
        Me.rdbFechaVencimiento2.Text = "Venc."
        Me.rdbFechaVencimiento2.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FlowLayoutPanel6.Controls.Add(Me.rdbEstadoAmbos)
        Me.FlowLayoutPanel6.Controls.Add(Me.rdbEstadoPendientes)
        Me.FlowLayoutPanel6.Controls.Add(Me.rdbEstadoCancelados)
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(7, 268)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(218, 20)
        Me.FlowLayoutPanel6.TabIndex = 12
        '
        'rdbEstadoAmbos
        '
        Me.rdbEstadoAmbos.AutoSize = True
        Me.rdbEstadoAmbos.Checked = True
        Me.rdbEstadoAmbos.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbEstadoAmbos.Location = New System.Drawing.Point(2, 1)
        Me.rdbEstadoAmbos.Margin = New System.Windows.Forms.Padding(2, 1, 3, 3)
        Me.rdbEstadoAmbos.Name = "rdbEstadoAmbos"
        Me.rdbEstadoAmbos.Size = New System.Drawing.Size(53, 16)
        Me.rdbEstadoAmbos.TabIndex = 0
        Me.rdbEstadoAmbos.TabStop = True
        Me.rdbEstadoAmbos.Text = "Ambos"
        Me.rdbEstadoAmbos.UseVisualStyleBackColor = True
        '
        'rdbEstadoPendientes
        '
        Me.rdbEstadoPendientes.AutoSize = True
        Me.rdbEstadoPendientes.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbEstadoPendientes.Location = New System.Drawing.Point(59, 1)
        Me.rdbEstadoPendientes.Margin = New System.Windows.Forms.Padding(1, 1, 3, 3)
        Me.rdbEstadoPendientes.Name = "rdbEstadoPendientes"
        Me.rdbEstadoPendientes.Size = New System.Drawing.Size(69, 16)
        Me.rdbEstadoPendientes.TabIndex = 1
        Me.rdbEstadoPendientes.Text = "Pendientes"
        Me.rdbEstadoPendientes.UseVisualStyleBackColor = True
        '
        'rdbEstadoCancelados
        '
        Me.rdbEstadoCancelados.AutoSize = True
        Me.rdbEstadoCancelados.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbEstadoCancelados.Location = New System.Drawing.Point(132, 1)
        Me.rdbEstadoCancelados.Margin = New System.Windows.Forms.Padding(1, 1, 3, 3)
        Me.rdbEstadoCancelados.Name = "rdbEstadoCancelados"
        Me.rdbEstadoCancelados.Size = New System.Drawing.Size(72, 16)
        Me.rdbEstadoCancelados.TabIndex = 2
        Me.rdbEstadoCancelados.Text = "Cancelados"
        Me.rdbEstadoCancelados.UseVisualStyleBackColor = True
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstado.Location = New System.Drawing.Point(7, 248)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(50, 13)
        Me.lblEstado.TabIndex = 11
        Me.lblEstado.Text = "Estado:"
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FlowLayoutPanel2.Controls.Add(Me.rdbCondicionAmbos)
        Me.FlowLayoutPanel2.Controls.Add(Me.rdbCondicionCredito)
        Me.FlowLayoutPanel2.Controls.Add(Me.rdbCondicionContado)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(7, 225)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(218, 20)
        Me.FlowLayoutPanel2.TabIndex = 10
        '
        'rdbCondicionAmbos
        '
        Me.rdbCondicionAmbos.AutoSize = True
        Me.rdbCondicionAmbos.Checked = True
        Me.rdbCondicionAmbos.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbCondicionAmbos.Location = New System.Drawing.Point(2, 1)
        Me.rdbCondicionAmbos.Margin = New System.Windows.Forms.Padding(2, 1, 3, 3)
        Me.rdbCondicionAmbos.Name = "rdbCondicionAmbos"
        Me.rdbCondicionAmbos.Size = New System.Drawing.Size(53, 16)
        Me.rdbCondicionAmbos.TabIndex = 0
        Me.rdbCondicionAmbos.TabStop = True
        Me.rdbCondicionAmbos.Text = "Ambos"
        Me.rdbCondicionAmbos.UseVisualStyleBackColor = True
        '
        'rdbCondicionCredito
        '
        Me.rdbCondicionCredito.AutoSize = True
        Me.rdbCondicionCredito.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbCondicionCredito.Location = New System.Drawing.Point(59, 1)
        Me.rdbCondicionCredito.Margin = New System.Windows.Forms.Padding(1, 1, 3, 3)
        Me.rdbCondicionCredito.Name = "rdbCondicionCredito"
        Me.rdbCondicionCredito.Size = New System.Drawing.Size(53, 16)
        Me.rdbCondicionCredito.TabIndex = 1
        Me.rdbCondicionCredito.Text = "Credito"
        Me.rdbCondicionCredito.UseVisualStyleBackColor = True
        '
        'rdbCondicionContado
        '
        Me.rdbCondicionContado.AutoSize = True
        Me.rdbCondicionContado.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbCondicionContado.Location = New System.Drawing.Point(116, 1)
        Me.rdbCondicionContado.Margin = New System.Windows.Forms.Padding(1, 1, 3, 3)
        Me.rdbCondicionContado.Name = "rdbCondicionContado"
        Me.rdbCondicionContado.Size = New System.Drawing.Size(58, 16)
        Me.rdbCondicionContado.TabIndex = 2
        Me.rdbCondicionContado.Text = "Contado"
        Me.rdbCondicionContado.UseVisualStyleBackColor = True
        '
        'lblCondicion
        '
        Me.lblCondicion.AutoSize = True
        Me.lblCondicion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCondicion.Location = New System.Drawing.Point(7, 205)
        Me.lblCondicion.Name = "lblCondicion"
        Me.lblCondicion.Size = New System.Drawing.Size(67, 13)
        Me.lblCondicion.TabIndex = 9
        Me.lblCondicion.Text = "Condicion:"
        '
        'chkUsuario
        '
        Me.chkUsuario.AutoSize = True
        Me.chkUsuario.Location = New System.Drawing.Point(7, 475)
        Me.chkUsuario.Name = "chkUsuario"
        Me.chkUsuario.Size = New System.Drawing.Size(65, 17)
        Me.chkUsuario.TabIndex = 22
        Me.chkUsuario.Text = "Usuario:"
        Me.chkUsuario.UseVisualStyleBackColor = True
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = "IDUsuarioRegistro"
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = Nothing
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = Nothing
        Me.cbxUsuario.DataSource = Nothing
        Me.cbxUsuario.DataValueMember = Nothing
        Me.cbxUsuario.dtSeleccionado = Nothing
        Me.cbxUsuario.Enabled = False
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(7, 492)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionMultiple = False
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(216, 23)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 23
        Me.cbxUsuario.Texto = ""
        '
        'chkMoneda
        '
        Me.chkMoneda.AutoSize = True
        Me.chkMoneda.Location = New System.Drawing.Point(7, 435)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(68, 17)
        Me.chkMoneda.TabIndex = 20
        Me.chkMoneda.Text = "Moneda:"
        Me.chkMoneda.UseVisualStyleBackColor = True
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = Nothing
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(7, 452)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(216, 23)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 21
        Me.cbxMoneda.Texto = ""
        '
        'chkGrupo
        '
        Me.chkGrupo.AutoSize = True
        Me.chkGrupo.Location = New System.Drawing.Point(7, 395)
        Me.chkGrupo.Name = "chkGrupo"
        Me.chkGrupo.Size = New System.Drawing.Size(58, 17)
        Me.chkGrupo.TabIndex = 18
        Me.chkGrupo.Text = "Grupo:"
        Me.chkGrupo.UseVisualStyleBackColor = True
        '
        'cbxGrupo
        '
        Me.cbxGrupo.CampoWhere = "IDGrupo"
        Me.cbxGrupo.CargarUnaSolaVez = False
        Me.cbxGrupo.DataDisplayMember = Nothing
        Me.cbxGrupo.DataFilter = Nothing
        Me.cbxGrupo.DataOrderBy = Nothing
        Me.cbxGrupo.DataSource = Nothing
        Me.cbxGrupo.DataValueMember = Nothing
        Me.cbxGrupo.dtSeleccionado = Nothing
        Me.cbxGrupo.Enabled = False
        Me.cbxGrupo.FormABM = Nothing
        Me.cbxGrupo.Indicaciones = Nothing
        Me.cbxGrupo.Location = New System.Drawing.Point(7, 412)
        Me.cbxGrupo.Name = "cbxGrupo"
        Me.cbxGrupo.SeleccionMultiple = False
        Me.cbxGrupo.SeleccionObligatoria = False
        Me.cbxGrupo.Size = New System.Drawing.Size(216, 23)
        Me.cbxGrupo.SoloLectura = False
        Me.cbxGrupo.TabIndex = 19
        Me.cbxGrupo.Texto = ""
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.AutoSize = True
        Me.chkTipoComprobante.Location = New System.Drawing.Point(7, 355)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(131, 17)
        Me.chkTipoComprobante.TabIndex = 16
        Me.chkTipoComprobante.Text = "Tipo de Comprobante:"
        Me.chkTipoComprobante.UseVisualStyleBackColor = True
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = "IDTipoComprobante"
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(7, 372)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(216, 23)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 17
        Me.cbxTipoComprobante.Texto = ""
        '
        'lblFiltros
        '
        Me.lblFiltros.AutoSize = True
        Me.lblFiltros.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFiltros.Location = New System.Drawing.Point(4, 294)
        Me.lblFiltros.Name = "lblFiltros"
        Me.lblFiltros.Size = New System.Drawing.Size(45, 13)
        Me.lblFiltros.TabIndex = 13
        Me.lblFiltros.Text = "Filtros:"
        '
        'lklAño
        '
        Me.lklAño.AutoSize = True
        Me.lklAño.Location = New System.Drawing.Point(193, 186)
        Me.lklAño.Name = "lklAño"
        Me.lklAño.Size = New System.Drawing.Size(26, 13)
        Me.lklAño.TabIndex = 8
        Me.lklAño.TabStop = True
        Me.lklAño.Text = "Año"
        '
        'lklMesAnterior
        '
        Me.lklMesAnterior.AutoSize = True
        Me.lklMesAnterior.Location = New System.Drawing.Point(118, 186)
        Me.lklMesAnterior.Name = "lklMesAnterior"
        Me.lklMesAnterior.Size = New System.Drawing.Size(65, 13)
        Me.lklMesAnterior.TabIndex = 7
        Me.lklMesAnterior.TabStop = True
        Me.lklMesAnterior.Text = "Mes anterior"
        '
        'lklMes
        '
        Me.lklMes.AutoSize = True
        Me.lklMes.Location = New System.Drawing.Point(72, 186)
        Me.lklMes.Name = "lklMes"
        Me.lklMes.Size = New System.Drawing.Size(27, 13)
        Me.lklMes.TabIndex = 6
        Me.lklMes.TabStop = True
        Me.lklMes.Text = "Mes"
        '
        'lklSemana
        '
        Me.lklSemana.AutoSize = True
        Me.lklSemana.Location = New System.Drawing.Point(7, 186)
        Me.lklSemana.Name = "lklSemana"
        Me.lklSemana.Size = New System.Drawing.Size(46, 13)
        Me.lklSemana.TabIndex = 5
        Me.lklSemana.TabStop = True
        Me.lklSemana.Text = "Semana"
        '
        'chkTipo
        '
        Me.chkTipo.AutoSize = True
        Me.chkTipo.Location = New System.Drawing.Point(7, 315)
        Me.chkTipo.Name = "chkTipo"
        Me.chkTipo.Size = New System.Drawing.Size(50, 17)
        Me.chkTipo.TabIndex = 14
        Me.chkTipo.Text = "Tipo:"
        Me.chkTipo.UseVisualStyleBackColor = True
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = "IDOperacion"
        Me.cbxTipo.CargarUnaSolaVez = False
        Me.cbxTipo.DataDisplayMember = Nothing
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = Nothing
        Me.cbxTipo.DataSource = Nothing
        Me.cbxTipo.DataValueMember = Nothing
        Me.cbxTipo.dtSeleccionado = Nothing
        Me.cbxTipo.Enabled = False
        Me.cbxTipo.FormABM = Nothing
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(7, 332)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionMultiple = False
        Me.cbxTipo.SeleccionObligatoria = False
        Me.cbxTipo.Size = New System.Drawing.Size(216, 23)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 15
        Me.cbxTipo.Texto = ""
        '
        'dtpHasta
        '
        Me.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHasta.Location = New System.Drawing.Point(120, 53)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(105, 20)
        Me.dtpHasta.TabIndex = 3
        '
        'dtpDesde
        '
        Me.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDesde.Location = New System.Drawing.Point(120, 30)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(105, 20)
        Me.dtpDesde.TabIndex = 2
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FlowLayoutPanel5.Controls.Add(Me.rdbFechaDocumento)
        Me.FlowLayoutPanel5.Controls.Add(Me.rdbFechaRegistro)
        Me.FlowLayoutPanel5.Controls.Add(Me.rdbFechaVencimiento)
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(61, 5)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(164, 20)
        Me.FlowLayoutPanel5.TabIndex = 1
        '
        'rdbFechaDocumento
        '
        Me.rdbFechaDocumento.AutoSize = True
        Me.rdbFechaDocumento.Checked = True
        Me.rdbFechaDocumento.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbFechaDocumento.Location = New System.Drawing.Point(2, 1)
        Me.rdbFechaDocumento.Margin = New System.Windows.Forms.Padding(2, 1, 3, 3)
        Me.rdbFechaDocumento.Name = "rdbFechaDocumento"
        Me.rdbFechaDocumento.Size = New System.Drawing.Size(56, 16)
        Me.rdbFechaDocumento.TabIndex = 0
        Me.rdbFechaDocumento.TabStop = True
        Me.rdbFechaDocumento.Text = "Docum."
        Me.rdbFechaDocumento.UseVisualStyleBackColor = True
        '
        'rdbFechaRegistro
        '
        Me.rdbFechaRegistro.AutoSize = True
        Me.rdbFechaRegistro.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbFechaRegistro.Location = New System.Drawing.Point(62, 1)
        Me.rdbFechaRegistro.Margin = New System.Windows.Forms.Padding(1, 1, 3, 3)
        Me.rdbFechaRegistro.Name = "rdbFechaRegistro"
        Me.rdbFechaRegistro.Size = New System.Drawing.Size(43, 16)
        Me.rdbFechaRegistro.TabIndex = 1
        Me.rdbFechaRegistro.Text = "Reg."
        Me.rdbFechaRegistro.UseVisualStyleBackColor = True
        '
        'rdbFechaVencimiento
        '
        Me.rdbFechaVencimiento.AutoSize = True
        Me.rdbFechaVencimiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbFechaVencimiento.Location = New System.Drawing.Point(109, 1)
        Me.rdbFechaVencimiento.Margin = New System.Windows.Forms.Padding(1, 1, 3, 3)
        Me.rdbFechaVencimiento.Name = "rdbFechaVencimiento"
        Me.rdbFechaVencimiento.Size = New System.Drawing.Size(48, 16)
        Me.rdbFechaVencimiento.TabIndex = 2
        Me.rdbFechaVencimiento.Text = "Venc."
        Me.rdbFechaVencimiento.UseVisualStyleBackColor = True
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(6, 8)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(55, 13)
        Me.lblFecha.TabIndex = 0
        Me.lblFecha.Text = "Fecha de:"
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(7, 152)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(112, 30)
        Me.btnListar.TabIndex = 4
        Me.btnListar.Text = "Listar"
        Me.btnListar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.lblFechaReporte)
        Me.TabPage2.Controls.Add(Me.DateTimePicker1)
        Me.TabPage2.Controls.Add(Me.DateTimePicker2)
        Me.TabPage2.Controls.Add(Me.btnSaldosIncorrectos)
        Me.TabPage2.Location = New System.Drawing.Point(4, 4)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(230, 590)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Reportes"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'lblFechaReporte
        '
        Me.lblFechaReporte.AutoSize = True
        Me.lblFechaReporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaReporte.Location = New System.Drawing.Point(3, 9)
        Me.lblFechaReporte.Name = "lblFechaReporte"
        Me.lblFechaReporte.Size = New System.Drawing.Size(46, 13)
        Me.lblFechaReporte.TabIndex = 0
        Me.lblFechaReporte.Text = "Fecha:"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(6, 57)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(219, 20)
        Me.DateTimePicker1.TabIndex = 2
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Location = New System.Drawing.Point(6, 31)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(219, 20)
        Me.DateTimePicker2.TabIndex = 1
        '
        'btnSaldosIncorrectos
        '
        Me.btnSaldosIncorrectos.Location = New System.Drawing.Point(6, 83)
        Me.btnSaldosIncorrectos.Name = "btnSaldosIncorrectos"
        Me.btnSaldosIncorrectos.Size = New System.Drawing.Size(219, 30)
        Me.btnSaldosIncorrectos.TabIndex = 3
        Me.btnSaldosIncorrectos.Text = "Saldos incorrectos"
        Me.btnSaldosIncorrectos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSaldosIncorrectos.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.FlowLayoutPanel1, 2)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkSucursal)
        Me.FlowLayoutPanel1.Controls.Add(Me.cbxSucursal)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblComprobante)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtComprobante)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblNumero)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtNumero)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblOrdenPago)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtNumeroOP)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkProveedor)
        Me.FlowLayoutPanel1.Controls.Add(Me.cbxProveedor)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1077, 29)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'chkSucursal
        '
        Me.chkSucursal.AutoSize = True
        Me.chkSucursal.Location = New System.Drawing.Point(3, 6)
        Me.chkSucursal.Margin = New System.Windows.Forms.Padding(3, 6, 3, 3)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(51, 17)
        Me.chkSucursal.TabIndex = 0
        Me.chkSucursal.Text = "Suc.:"
        Me.chkSucursal.UseVisualStyleBackColor = True
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(60, 3)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(54, 23)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(117, 6)
        Me.lblComprobante.Margin = New System.Windows.Forms.Padding(0, 6, 0, 0)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 2
        Me.lblComprobante.Text = "Comprobante:"
        Me.lblComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(193, 4)
        Me.txtComprobante.Margin = New System.Windows.Forms.Padding(3, 4, 3, 3)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(97, 20)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 3
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'lblNumero
        '
        Me.lblNumero.AutoSize = True
        Me.lblNumero.Location = New System.Drawing.Point(293, 6)
        Me.lblNumero.Margin = New System.Windows.Forms.Padding(0, 6, 0, 0)
        Me.lblNumero.Name = "lblNumero"
        Me.lblNumero.Size = New System.Drawing.Size(56, 13)
        Me.lblNumero.TabIndex = 4
        Me.lblNumero.Text = "Nro. Ope.:"
        Me.lblNumero.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNumero
        '
        Me.txtNumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumero.Color = System.Drawing.Color.Empty
        Me.txtNumero.Indicaciones = Nothing
        Me.txtNumero.Location = New System.Drawing.Point(352, 4)
        Me.txtNumero.Margin = New System.Windows.Forms.Padding(3, 4, 3, 3)
        Me.txtNumero.Multilinea = False
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.Size = New System.Drawing.Size(43, 20)
        Me.txtNumero.SoloLectura = False
        Me.txtNumero.TabIndex = 5
        Me.txtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNumero.Texto = ""
        '
        'lblOrdenPago
        '
        Me.lblOrdenPago.AutoSize = True
        Me.lblOrdenPago.Location = New System.Drawing.Point(398, 6)
        Me.lblOrdenPago.Margin = New System.Windows.Forms.Padding(0, 6, 0, 0)
        Me.lblOrdenPago.Name = "lblOrdenPago"
        Me.lblOrdenPago.Size = New System.Drawing.Size(67, 13)
        Me.lblOrdenPago.TabIndex = 6
        Me.lblOrdenPago.Text = "Orden Pago:"
        Me.lblOrdenPago.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNumeroOP
        '
        Me.txtNumeroOP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumeroOP.Color = System.Drawing.Color.Empty
        Me.txtNumeroOP.Indicaciones = Nothing
        Me.txtNumeroOP.Location = New System.Drawing.Point(468, 4)
        Me.txtNumeroOP.Margin = New System.Windows.Forms.Padding(3, 4, 3, 3)
        Me.txtNumeroOP.Multilinea = False
        Me.txtNumeroOP.Name = "txtNumeroOP"
        Me.txtNumeroOP.Size = New System.Drawing.Size(43, 20)
        Me.txtNumeroOP.SoloLectura = False
        Me.txtNumeroOP.TabIndex = 7
        Me.txtNumeroOP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNumeroOP.Texto = ""
        '
        'chkProveedor
        '
        Me.chkProveedor.AutoSize = True
        Me.chkProveedor.Location = New System.Drawing.Point(534, 6)
        Me.chkProveedor.Margin = New System.Windows.Forms.Padding(20, 6, 3, 3)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(78, 17)
        Me.chkProveedor.TabIndex = 8
        Me.chkProveedor.Text = "Proveedor:"
        Me.chkProveedor.UseVisualStyleBackColor = True
        '
        'cbxProveedor
        '
        Me.cbxProveedor.CampoWhere = "IDProveedor"
        Me.cbxProveedor.CargarUnaSolaVez = False
        Me.cbxProveedor.DataDisplayMember = Nothing
        Me.cbxProveedor.DataFilter = Nothing
        Me.cbxProveedor.DataOrderBy = Nothing
        Me.cbxProveedor.DataSource = Nothing
        Me.cbxProveedor.DataValueMember = Nothing
        Me.cbxProveedor.dtSeleccionado = Nothing
        Me.cbxProveedor.Enabled = False
        Me.cbxProveedor.FormABM = Nothing
        Me.cbxProveedor.Indicaciones = Nothing
        Me.cbxProveedor.Location = New System.Drawing.Point(618, 3)
        Me.cbxProveedor.Name = "cbxProveedor"
        Me.cbxProveedor.SeleccionMultiple = False
        Me.cbxProveedor.SeleccionObligatoria = False
        Me.cbxProveedor.Size = New System.Drawing.Size(251, 23)
        Me.cbxProveedor.SoloLectura = False
        Me.cbxProveedor.TabIndex = 9
        Me.cbxProveedor.Texto = ""
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.btnVerDetalle)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnAsiento)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnEliminar)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnRepararSaldo)
        Me.FlowLayoutPanel3.Controls.Add(Me.lklExtractoProveedor)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 400)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(474, 58)
        Me.FlowLayoutPanel3.TabIndex = 1
        '
        'btnVerDetalle
        '
        Me.btnVerDetalle.Location = New System.Drawing.Point(3, 3)
        Me.btnVerDetalle.Name = "btnVerDetalle"
        Me.btnVerDetalle.Size = New System.Drawing.Size(75, 23)
        Me.btnVerDetalle.TabIndex = 0
        Me.btnVerDetalle.Text = "Ver Detalle"
        Me.btnVerDetalle.UseVisualStyleBackColor = True
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(84, 3)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 1
        Me.btnAsiento.Text = "Ver Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(165, 3)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnRepararSaldo
        '
        Me.btnRepararSaldo.Location = New System.Drawing.Point(246, 3)
        Me.btnRepararSaldo.Name = "btnRepararSaldo"
        Me.btnRepararSaldo.Size = New System.Drawing.Size(103, 23)
        Me.btnRepararSaldo.TabIndex = 3
        Me.btnRepararSaldo.Text = "Reparar Saldo"
        Me.btnRepararSaldo.UseVisualStyleBackColor = True
        '
        'lklExtractoProveedor
        '
        Me.lklExtractoProveedor.AutoSize = True
        Me.lklExtractoProveedor.Location = New System.Drawing.Point(355, 6)
        Me.lklExtractoProveedor.Margin = New System.Windows.Forms.Padding(3, 6, 3, 0)
        Me.lklExtractoProveedor.Name = "lklExtractoProveedor"
        Me.lklExtractoProveedor.Size = New System.Drawing.Size(113, 13)
        Me.lklExtractoProveedor.TabIndex = 4
        Me.lklExtractoProveedor.TabStop = True
        Me.lklExtractoProveedor.Text = "Extracto de Proveedor"
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.AutoSize = True
        Me.FlowLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel4.Controls.Add(Me.Label5)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtTotalGuaranies)
        Me.FlowLayoutPanel4.Controls.Add(Me.Label6)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtSaldoGuaranies)
        Me.FlowLayoutPanel4.Controls.Add(Me.lblCantidadOperacion)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtCantidadOperacion)
        Me.FlowLayoutPanel4.Controls.Add(lblTotalDolares)
        Me.FlowLayoutPanel4.Controls.Add(Me.TxtTotalDolares)
        Me.FlowLayoutPanel4.Controls.Add(Me.Label8)
        Me.FlowLayoutPanel4.Controls.Add(Me.TxtSaldoDolares)
        Me.FlowLayoutPanel4.Controls.Add(Me.Label7)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtCantidadUSD)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(483, 400)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(597, 58)
        Me.FlowLayoutPanel4.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 8)
        Me.Label5.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(85, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Total Guaranies:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTotalGuaranies
        '
        Me.txtTotalGuaranies.Color = System.Drawing.Color.Empty
        Me.txtTotalGuaranies.Decimales = True
        Me.txtTotalGuaranies.Indicaciones = Nothing
        Me.txtTotalGuaranies.Location = New System.Drawing.Point(94, 5)
        Me.txtTotalGuaranies.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtTotalGuaranies.Name = "txtTotalGuaranies"
        Me.txtTotalGuaranies.Size = New System.Drawing.Size(150, 22)
        Me.txtTotalGuaranies.SoloLectura = False
        Me.txtTotalGuaranies.TabIndex = 3
        Me.txtTotalGuaranies.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalGuaranies.Texto = "0"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(250, 8)
        Me.Label6.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(88, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Saldo Guaranies:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSaldoGuaranies
        '
        Me.txtSaldoGuaranies.Color = System.Drawing.Color.Empty
        Me.txtSaldoGuaranies.Decimales = True
        Me.txtSaldoGuaranies.Indicaciones = Nothing
        Me.txtSaldoGuaranies.Location = New System.Drawing.Point(344, 5)
        Me.txtSaldoGuaranies.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtSaldoGuaranies.Name = "txtSaldoGuaranies"
        Me.txtSaldoGuaranies.Size = New System.Drawing.Size(125, 22)
        Me.txtSaldoGuaranies.SoloLectura = False
        Me.txtSaldoGuaranies.TabIndex = 5
        Me.txtSaldoGuaranies.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoGuaranies.Texto = "0"
        '
        'lblCantidadOperacion
        '
        Me.lblCantidadOperacion.AutoSize = True
        Me.lblCantidadOperacion.Location = New System.Drawing.Point(475, 8)
        Me.lblCantidadOperacion.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
        Me.lblCantidadOperacion.Name = "lblCantidadOperacion"
        Me.lblCantidadOperacion.Size = New System.Drawing.Size(62, 13)
        Me.lblCantidadOperacion.TabIndex = 0
        Me.lblCantidadOperacion.Text = "   Cant. GS:"
        Me.lblCantidadOperacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCantidadOperacion
        '
        Me.txtCantidadOperacion.Color = System.Drawing.Color.Empty
        Me.txtCantidadOperacion.Decimales = True
        Me.txtCantidadOperacion.Indicaciones = Nothing
        Me.txtCantidadOperacion.Location = New System.Drawing.Point(543, 5)
        Me.txtCantidadOperacion.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtCantidadOperacion.Name = "txtCantidadOperacion"
        Me.txtCantidadOperacion.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadOperacion.SoloLectura = False
        Me.txtCantidadOperacion.TabIndex = 1
        Me.txtCantidadOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadOperacion.Texto = "0"
        '
        'TxtTotalDolares
        '
        Me.TxtTotalDolares.Color = System.Drawing.Color.Empty
        Me.TxtTotalDolares.Decimales = True
        Me.TxtTotalDolares.Indicaciones = Nothing
        Me.TxtTotalDolares.Location = New System.Drawing.Point(94, 35)
        Me.TxtTotalDolares.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.TxtTotalDolares.Name = "TxtTotalDolares"
        Me.TxtTotalDolares.Size = New System.Drawing.Size(150, 22)
        Me.TxtTotalDolares.SoloLectura = False
        Me.TxtTotalDolares.TabIndex = 9
        Me.TxtTotalDolares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TxtTotalDolares.Texto = "0"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(250, 38)
        Me.Label8.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(88, 13)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "   Saldo Dolares :"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TxtSaldoDolares
        '
        Me.TxtSaldoDolares.Color = System.Drawing.Color.Empty
        Me.TxtSaldoDolares.Decimales = True
        Me.TxtSaldoDolares.Indicaciones = Nothing
        Me.TxtSaldoDolares.Location = New System.Drawing.Point(344, 35)
        Me.TxtSaldoDolares.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.TxtSaldoDolares.Name = "TxtSaldoDolares"
        Me.TxtSaldoDolares.Size = New System.Drawing.Size(125, 22)
        Me.TxtSaldoDolares.SoloLectura = False
        Me.TxtSaldoDolares.TabIndex = 11
        Me.TxtSaldoDolares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TxtSaldoDolares.Texto = "0"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(475, 38)
        Me.Label7.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(61, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Cant. USD:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCantidadUSD
        '
        Me.txtCantidadUSD.Color = System.Drawing.Color.Empty
        Me.txtCantidadUSD.Decimales = True
        Me.txtCantidadUSD.Indicaciones = Nothing
        Me.txtCantidadUSD.Location = New System.Drawing.Point(542, 35)
        Me.txtCantidadUSD.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtCantidadUSD.Name = "txtCantidadUSD"
        Me.txtCantidadUSD.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadUSD.SoloLectura = False
        Me.txtCantidadUSD.TabIndex = 7
        Me.txtCantidadUSD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadUSD.Texto = "0"
        '
        'TabControl2
        '
        Me.TabControl2.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TableLayoutPanel1.SetColumnSpan(Me.TabControl2, 2)
        Me.TabControl2.Controls.Add(Me.TabPage5)
        Me.TabControl2.Controls.Add(Me.TabPage3)
        Me.TabControl2.Controls.Add(Me.TabPage4)
        Me.TabControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl2.Location = New System.Drawing.Point(3, 464)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(1077, 135)
        Me.TabControl2.TabIndex = 3
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.TableLayoutPanel2)
        Me.TabPage5.Location = New System.Drawing.Point(4, 4)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(1069, 109)
        Me.TabPage5.TabIndex = 2
        Me.TabPage5.Text = "Registro"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 420.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 649.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.OcxImpuesto1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.GroupBox2, 1, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1069, 109)
        Me.TableLayoutPanel2.TabIndex = 3
        '
        'OcxImpuesto1
        '
        Me.OcxImpuesto1.Decimales = False
        Me.OcxImpuesto1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxImpuesto1.dtImpuesto = Nothing
        Me.OcxImpuesto1.IDMoneda = 0
        Me.OcxImpuesto1.Location = New System.Drawing.Point(3, 3)
        Me.OcxImpuesto1.Name = "OcxImpuesto1"
        Me.OcxImpuesto1.Size = New System.Drawing.Size(414, 103)
        Me.OcxImpuesto1.TabIndex = 0
        Me.OcxImpuesto1.TotalRetencionIVA = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtObservacion)
        Me.GroupBox2.Controls.Add(Me.txtVencimientoTimbrado)
        Me.GroupBox2.Controls.Add(Me.txtTimbrado)
        Me.GroupBox2.Controls.Add(Me.txtFechaHora)
        Me.GroupBox2.Controls.Add(Me.txtTerminal)
        Me.GroupBox2.Controls.Add(Me.txtSucursal)
        Me.GroupBox2.Controls.Add(Me.txtUsuario)
        Me.GroupBox2.Controls.Add(Me.lblObservacion)
        Me.GroupBox2.Controls.Add(Me.txtIDTransaccion)
        Me.GroupBox2.Controls.Add(Me.lblVencimientoTimbrado)
        Me.GroupBox2.Controls.Add(Me.lblFechaHora)
        Me.GroupBox2.Controls.Add(Me.lblTimbrado)
        Me.GroupBox2.Controls.Add(Me.lblTerminal)
        Me.GroupBox2.Controls.Add(Me.lblSucursal)
        Me.GroupBox2.Controls.Add(Me.lblUsuario)
        Me.GroupBox2.Controls.Add(Me.lblTransaccion)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Location = New System.Drawing.Point(423, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(643, 103)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Registro"
        '
        'txtObservacion
        '
        Me.txtObservacion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtObservacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservacion.Location = New System.Drawing.Point(254, 60)
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.ReadOnly = True
        Me.txtObservacion.Size = New System.Drawing.Size(239, 35)
        Me.txtObservacion.TabIndex = 15
        '
        'txtVencimientoTimbrado
        '
        Me.txtVencimientoTimbrado.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtVencimientoTimbrado.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVencimientoTimbrado.Location = New System.Drawing.Point(422, 42)
        Me.txtVencimientoTimbrado.Name = "txtVencimientoTimbrado"
        Me.txtVencimientoTimbrado.ReadOnly = True
        Me.txtVencimientoTimbrado.Size = New System.Drawing.Size(71, 11)
        Me.txtVencimientoTimbrado.TabIndex = 11
        Me.txtVencimientoTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtTimbrado
        '
        Me.txtTimbrado.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTimbrado.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTimbrado.Location = New System.Drawing.Point(422, 23)
        Me.txtTimbrado.Name = "txtTimbrado"
        Me.txtTimbrado.ReadOnly = True
        Me.txtTimbrado.Size = New System.Drawing.Size(71, 11)
        Me.txtTimbrado.TabIndex = 5
        Me.txtTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtFechaHora
        '
        Me.txtFechaHora.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtFechaHora.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFechaHora.Location = New System.Drawing.Point(79, 61)
        Me.txtFechaHora.Name = "txtFechaHora"
        Me.txtFechaHora.ReadOnly = True
        Me.txtFechaHora.Size = New System.Drawing.Size(118, 11)
        Me.txtFechaHora.TabIndex = 13
        Me.txtFechaHora.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtTerminal
        '
        Me.txtTerminal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTerminal.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTerminal.Location = New System.Drawing.Point(254, 42)
        Me.txtTerminal.Name = "txtTerminal"
        Me.txtTerminal.ReadOnly = True
        Me.txtTerminal.Size = New System.Drawing.Size(106, 11)
        Me.txtTerminal.TabIndex = 9
        Me.txtTerminal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSucursal
        '
        Me.txtSucursal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSucursal.Location = New System.Drawing.Point(79, 42)
        Me.txtSucursal.Name = "txtSucursal"
        Me.txtSucursal.ReadOnly = True
        Me.txtSucursal.Size = New System.Drawing.Size(118, 11)
        Me.txtSucursal.TabIndex = 7
        Me.txtSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtUsuario
        '
        Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUsuario.Location = New System.Drawing.Point(254, 23)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.ReadOnly = True
        Me.txtUsuario.Size = New System.Drawing.Size(106, 11)
        Me.txtUsuario.TabIndex = 3
        Me.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(216, 60)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(32, 13)
        Me.lblObservacion.TabIndex = 14
        Me.lblObservacion.Text = "Obs.:"
        '
        'txtIDTransaccion
        '
        Me.txtIDTransaccion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtIDTransaccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDTransaccion.Location = New System.Drawing.Point(79, 23)
        Me.txtIDTransaccion.Name = "txtIDTransaccion"
        Me.txtIDTransaccion.ReadOnly = True
        Me.txtIDTransaccion.Size = New System.Drawing.Size(118, 11)
        Me.txtIDTransaccion.TabIndex = 1
        Me.txtIDTransaccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblVencimientoTimbrado
        '
        Me.lblVencimientoTimbrado.AutoSize = True
        Me.lblVencimientoTimbrado.Location = New System.Drawing.Point(378, 41)
        Me.lblVencimientoTimbrado.Name = "lblVencimientoTimbrado"
        Me.lblVencimientoTimbrado.Size = New System.Drawing.Size(38, 13)
        Me.lblVencimientoTimbrado.TabIndex = 10
        Me.lblVencimientoTimbrado.Text = "Venc.:"
        '
        'lblFechaHora
        '
        Me.lblFechaHora.AutoSize = True
        Me.lblFechaHora.Location = New System.Drawing.Point(5, 60)
        Me.lblFechaHora.Name = "lblFechaHora"
        Me.lblFechaHora.Size = New System.Drawing.Size(68, 13)
        Me.lblFechaHora.TabIndex = 12
        Me.lblFechaHora.Text = "Fecha/Hora:"
        '
        'lblTimbrado
        '
        Me.lblTimbrado.AutoSize = True
        Me.lblTimbrado.Location = New System.Drawing.Point(362, 22)
        Me.lblTimbrado.Name = "lblTimbrado"
        Me.lblTimbrado.Size = New System.Drawing.Size(54, 13)
        Me.lblTimbrado.TabIndex = 4
        Me.lblTimbrado.Text = "Timbrado:"
        '
        'lblTerminal
        '
        Me.lblTerminal.AutoSize = True
        Me.lblTerminal.Location = New System.Drawing.Point(198, 41)
        Me.lblTerminal.Name = "lblTerminal"
        Me.lblTerminal.Size = New System.Drawing.Size(50, 13)
        Me.lblTerminal.TabIndex = 8
        Me.lblTerminal.Text = "Terminal:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(22, 41)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 6
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblUsuario
        '
        Me.lblUsuario.AutoSize = True
        Me.lblUsuario.Location = New System.Drawing.Point(202, 22)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuario.TabIndex = 2
        Me.lblUsuario.Text = "Usuario:"
        '
        'lblTransaccion
        '
        Me.lblTransaccion.AutoSize = True
        Me.lblTransaccion.Location = New System.Drawing.Point(10, 22)
        Me.lblTransaccion.Name = "lblTransaccion"
        Me.lblTransaccion.Size = New System.Drawing.Size(63, 13)
        Me.lblTransaccion.TabIndex = 0
        Me.lblTransaccion.Text = "Nro. Trans.:"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.dgvExtracto)
        Me.TabPage3.Location = New System.Drawing.Point(4, 4)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1069, 109)
        Me.TabPage3.TabIndex = 0
        Me.TabPage3.Text = "Extracto"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'dgvExtracto
        '
        Me.dgvExtracto.AllowUserToAddRows = False
        Me.dgvExtracto.AllowUserToDeleteRows = False
        Me.dgvExtracto.BackgroundColor = System.Drawing.Color.White
        Me.dgvExtracto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvExtracto.ContextMenuStrip = Me.ContextMenuStrip1
        Me.dgvExtracto.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvExtracto.Location = New System.Drawing.Point(3, 3)
        Me.dgvExtracto.MultiSelect = False
        Me.dgvExtracto.Name = "dgvExtracto"
        Me.dgvExtracto.ReadOnly = True
        Me.dgvExtracto.RowHeadersVisible = False
        Me.dgvExtracto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvExtracto.Size = New System.Drawing.Size(1063, 103)
        Me.dgvExtracto.StandardTab = True
        Me.dgvExtracto.TabIndex = 0
        '
        'TabPage4
        '
        Me.TabPage4.Location = New System.Drawing.Point(4, 4)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(1069, 109)
        Me.TabPage4.TabIndex = 1
        Me.TabPage4.Text = "Detalle"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmAdministrarEgresos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1327, 622)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmAdministrarEgresos"
        Me.Text = "frmAdministrarEgresos"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvComprobantes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.FlowLayoutPanel7.ResumeLayout(False)
        Me.FlowLayoutPanel7.PerformLayout()
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel6.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel5.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.PerformLayout()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel4.PerformLayout()
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.dgvExtracto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents dgvComprobantes As System.Windows.Forms.DataGridView
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ExportarAPlanillaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents btnListar As System.Windows.Forms.Button
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnVerDetalle As System.Windows.Forms.Button
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents FlowLayoutPanel5 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents rdbFechaDocumento As System.Windows.Forms.RadioButton
    Friend WithEvents rdbFechaRegistro As System.Windows.Forms.RadioButton
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents cbxProveedor As ERP.ocxCBX
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblNumero As System.Windows.Forms.Label
    Friend WithEvents lblFiltros As System.Windows.Forms.Label
    Friend WithEvents lklAño As System.Windows.Forms.LinkLabel
    Friend WithEvents lklMesAnterior As System.Windows.Forms.LinkLabel
    Friend WithEvents lklMes As System.Windows.Forms.LinkLabel
    Friend WithEvents lklSemana As System.Windows.Forms.LinkLabel
    Friend WithEvents chkTipo As System.Windows.Forms.CheckBox
    Friend WithEvents cbxTipo As ERP.ocxCBX
    Friend WithEvents dtpHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkMoneda As System.Windows.Forms.CheckBox
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents chkSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkGrupo As System.Windows.Forms.CheckBox
    Friend WithEvents cbxGrupo As ERP.ocxCBX
    Friend WithEvents chkTipoComprobante As System.Windows.Forms.CheckBox
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents rdbFechaVencimiento As System.Windows.Forms.RadioButton
    Friend WithEvents chkProveedor As System.Windows.Forms.CheckBox
    Friend WithEvents lblOrdenPago As System.Windows.Forms.Label
    Friend WithEvents chkUsuario As System.Windows.Forms.CheckBox
    Friend WithEvents cbxUsuario As ERP.ocxCBX
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents rdbCondicionAmbos As System.Windows.Forms.RadioButton
    Friend WithEvents rdbCondicionCredito As System.Windows.Forms.RadioButton
    Friend WithEvents rdbCondicionContado As System.Windows.Forms.RadioButton
    Friend WithEvents lblCondicion As System.Windows.Forms.Label
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents dgvExtracto As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents FlowLayoutPanel6 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents rdbEstadoAmbos As System.Windows.Forms.RadioButton
    Friend WithEvents rdbEstadoPendientes As System.Windows.Forms.RadioButton
    Friend WithEvents rdbEstadoCancelados As System.Windows.Forms.RadioButton
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents txtNumero As ERP.ocxTXTString
    Friend WithEvents txtNumeroOP As ERP.ocxTXTString
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OcxImpuesto1 As ERP.ocxImpuesto
    Friend WithEvents lblTerminal As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblUsuario As System.Windows.Forms.Label
    Friend WithEvents lblTransaccion As System.Windows.Forms.Label
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblVencimientoTimbrado As System.Windows.Forms.Label
    Friend WithEvents lblTimbrado As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents txtVencimientoTimbrado As System.Windows.Forms.TextBox
    Friend WithEvents txtTimbrado As System.Windows.Forms.TextBox
    Friend WithEvents txtFechaHora As System.Windows.Forms.TextBox
    Friend WithEvents txtTerminal As System.Windows.Forms.TextBox
    Friend WithEvents txtSucursal As System.Windows.Forms.TextBox
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents txtIDTransaccion As System.Windows.Forms.TextBox
    Friend WithEvents lblFechaHora As System.Windows.Forms.Label
    Friend WithEvents btnRepararSaldo As System.Windows.Forms.Button
    Friend WithEvents lklExtractoProveedor As System.Windows.Forms.LinkLabel
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnSaldosIncorrectos As System.Windows.Forms.Button
    Friend WithEvents lblFechaReporte As System.Windows.Forms.Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents chkFecha2 As CheckBox
    Friend WithEvents txtHasta2 As DateTimePicker
    Friend WithEvents txtDesde2 As DateTimePicker
    Friend WithEvents FlowLayoutPanel7 As FlowLayoutPanel
    Friend WithEvents rdbFechaDocumento2 As RadioButton
    Friend WithEvents rdbFechaRegistro2 As RadioButton
    Friend WithEvents rdbFechaVencimiento2 As RadioButton
    Friend WithEvents btnExportar As Button
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents Label5 As Label
    Friend WithEvents txtTotalGuaranies As ocxTXTNumeric
    Friend WithEvents Label6 As Label
    Friend WithEvents txtSaldoGuaranies As ocxTXTNumeric
    Friend WithEvents lblCantidadOperacion As Label
    Friend WithEvents txtCantidadOperacion As ocxTXTNumeric
    Friend WithEvents TxtTotalDolares As ocxTXTNumeric
    Friend WithEvents Label8 As Label
    Friend WithEvents TxtSaldoDolares As ocxTXTNumeric
    Friend WithEvents Label7 As Label
    Friend WithEvents txtCantidadUSD As ocxTXTNumeric
End Class

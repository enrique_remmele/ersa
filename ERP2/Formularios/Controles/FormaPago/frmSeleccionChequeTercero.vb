﻿Public Class frmSeleccionChequeTercero

    'CLASE
    Dim CSistema As New CSistema

    'PRIPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = ID
        End Set
    End Property

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        'Cliente
        txtCliente.Conectar()

        'Funciones
        'CargarCheques()

        'Foco
        txtCliente.txtID.Focus()

    End Sub

    Sub ListarCheques()

        'Limpiar la grilla
        dgw.Rows.Clear()

        dt = CSistema.ExecuteToDataTable("Select *, 'Sel'='False' From VChequesClienteFormaPago Where IDCliente=" & txtCliente.Registro("ID").ToString).Copy

        Dim TotalCheques As Decimal = 0

        For Each oRow As DataRow In dt.Rows
            Dim Registro(11) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = ID
            Registro(2) = oRow("Sel").ToString
            Registro(3) = oRow("Ciudad").ToString
            Registro(4) = oRow("Banco").ToString
            Registro(5) = oRow("NroCheque").ToString
            Registro(6) = oRow("CuentaBancaria").ToString
            Registro(7) = oRow("Fecha").ToString
            Registro(8) = oRow("Moneda").ToString
            Registro(9) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(10) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            Registro(11) = CSistema.FormatoMoneda(oRow("Importe").ToString)

            'Sumar el total del saldo
            TotalCheques = TotalCheques + CDec(oRow("Saldo").ToString)

            dgw.Rows.Add(Registro)

        Next

        txtTotalCheque.SetValue(TotalCheques)
        txtCantidadCheque.SetValue(dt.Rows.Count)

    End Sub

    Sub SeleccionarCheque()

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells(0).Value

            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & IDTransaccion)

                If oRow.Cells("colSel").Value = True Then
                    'Validar que el importe ingresado sea menor al saldo
                    If oRow.Cells("colImporte").Value > oRow1("Saldo") Then
                        Dim mensaje As String = "El Importe debe ser menor al saldo!"
                        ctrError.SetError(dgw, mensaje)
                        ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                        tsslEstado.Text = mensaje
                        Exit Sub
                    End If

                    oRow1("Sel") = oRow.Cells("colSel").Value
                    oRow1("Importe") = oRow.Cells("colImporte").Value

                    Me.Close()

                End If
            Next

        Next


    End Sub

    Sub CargarCheques()

        For Each oRow As DataRow In dt.Rows
            For Each oRow1 As DataGridViewRow In dgw.Rows
                If oRow("IDTransaccion") = oRow1.Cells(0).Value Then
                    oRow1.Cells(1).Value = CBool(oRow("Sel").ToString)
                    oRow1.Cells(9).Value = CSistema.FormatoMoneda(oRow("Importe").ToString)
                End If
            Next
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim ImporteSeleccionado As Decimal = 0
        Dim CantidadSeleccionado As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows

            If oRow.Cells(1).Value = True Then
                ImporteSeleccionado = ImporteSeleccionado + oRow.Cells("colImporte").Value
                CantidadSeleccionado = CantidadSeleccionado + 1
            End If

        Next

        txtTotalSeleccionado.SetValue(ImporteSeleccionado)
        txtCantidadSeleccionado.SetValue(CantidadSeleccionado)

    End Sub

    Sub PintarCelda()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(1).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next

        dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub

    Private Sub dgwTextBox_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim columna As Integer = dgw.CurrentCell.ColumnIndex

        If columna = 10 Then

            If (Char.IsNumber(e.KeyChar) Or e.KeyChar = Microsoft.VisualBasic.ChrW(46) Or e.KeyChar = Microsoft.VisualBasic.ChrW(127) Or e.KeyChar = Microsoft.VisualBasic.ChrW(8)) Then
                e.Handled = False

                If Asc(e.KeyChar) = Keys.Enter Then
                    If CSistema.FormatoNumero(dgw.Columns.Item("colImporte").ToString) > CSistema.FormatoNumero(dgw.Columns.Item("colSaldo").ToString) Then
                        Dim mensaje As String = "El Importe debe ser menor al saldo!"
                        ctrError.SetError(dgw, mensaje)
                        ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                        tsslEstado.Text = mensaje
                    End If
                End If


            Else
                e.Handled = True
            End If

        End If

    End Sub

    Private Sub dgw_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgw.EditingControlShowing

        If (dgw.CurrentCell.ColumnIndex = 10) Then

            Dim oTexbox As TextBox = CType(e.Control, TextBox)
            AddHandler oTexbox.KeyPress, AddressOf dgwTextBox_Keypress

        End If

    End Sub


    Private Sub frmSeleccionChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick
        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

        If e.ColumnIndex = dgw.Columns.Item("colSel").Index Then

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSel").Value = False Then
                        oRow.Cells("colSel").Value = True
                        oRow.Cells("colSel").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("colImporte")

                    Else
                        oRow.Cells("colSel").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colImporte").Value = oRow.Cells("colSaldo").Value
                        oRow.Cells("colSel").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

        End If

    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit
        If e.ColumnIndex = 10 Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value

            Else
                dgw.CurrentCell.Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells("colImporte").Value)
            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSel").Value = False Then
                        oRow.Cells("colSel").Value = True
                        oRow.Cells("colSel").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("colImporte")

                    Else
                        oRow.Cells("colSel").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colImporte").Value = oRow.Cells("colSaldo").Value
                        oRow.Cells("colSel").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If
    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Index = e.RowIndex Then

                oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                oRow.DefaultCellStyle.Font = f1

            Else

                oRow.DefaultCellStyle.Font = f2

                If oRow.Cells("colSel").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.White
                Else
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                End If

            End If
        Next
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Tab Then

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells("colImporte")

        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        SeleccionarCheque()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado
        ListarCheques()
        CargarCheques()
    End Sub


End Class
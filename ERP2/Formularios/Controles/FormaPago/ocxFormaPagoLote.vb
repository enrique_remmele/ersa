﻿Public Class ocxFormaPagoLote

    'CLASES
    Dim CSistema As New CSistema
    Protected CData As New CData

    'ENUMERACIONES
    Enum ENUMFormaPago
        Efectivo = 2
        Cheque = 3
        ChequeTercero = 4
        Documento = 5
    End Enum

    'EVENTOS
    Public Event ImporteModificado(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ListarCheques(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ItemModificado(ByVal sender As Object, ByVal e As EventArgs, ByVal oRow As DataRow)
    Public Event RegistroInsertado(ByVal sender As Object, ByVal e As EventArgs, ByVal oRow As DataRow, ByVal Tipo As ENUMFormaPago)
    Public Event RegistroEliminado(ByVal sender As Object, ByVal e As EventArgs, ByVal oRow As DataRow, ByVal Tipo As ENUMFormaPago)

    'PROPIEDADES
    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value

            btnCheque.Enabled = Not value
            btnDocumentos.Enabled = Not value
            btnEfectivo.Enabled = Not value

            lklEliminarFormaPago.Enabled = Not value
            lklNuevoChequeCliente.Enabled = Not value

        End Set
    End Property

    Private dtFormaPagoValue As DataTable
    Public Property dtFormaPago() As DataTable
        Get
            Return dtFormaPagoValue
        End Get
        Set(ByVal value As DataTable)
            dtFormaPagoValue = value
        End Set
    End Property

    Private dtChequesValue As DataTable
    Public Property dtCheques() As DataTable
        Get
            Return dtChequesValue
        End Get
        Set(ByVal value As DataTable)
            dtChequesValue = value
        End Set
    End Property

    Private dtChequesTerceroValue As DataTable
    Public Property dtChequesTercero() As DataTable
        Get
            Return dtChequesTerceroValue
        End Get
        Set(ByVal value As DataTable)
            dtChequesTerceroValue = value
        End Set
    End Property

    Private dtEfectivoValue As DataTable
    Public Property dtEfectivo() As DataTable
        Get
            Return dtEfectivoValue
        End Get
        Set(ByVal value As DataTable)
            dtEfectivoValue = value
        End Set
    End Property

    Private dtDocumentoValue As DataTable
    Public Property dtDocumento() As DataTable
        Get
            Return dtDocumentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDocumentoValue = value
        End Set
    End Property

    Private TotalValue As Decimal
    Public Property Total() As Decimal

        Get
            Return TotalValue
        End Get
        Set(ByVal value As Decimal)
            TotalValue = value
        End Set
    End Property

    Private SaldoValue As Decimal
    Public Property Saldo() As Decimal

        Get
            Return SaldoValue
        End Get
        Set(ByVal value As Decimal)
            SaldoValue = value
        End Set
    End Property

    Private ComprobanteValue As String
    Public Property Comprobante() As String
        Get
            Return ComprobanteValue
        End Get
        Set(ByVal value As String)
            ComprobanteValue = value
        End Set
    End Property

    Private FechaValue As Date
    Public Property Fecha() As Date
        Get
            Return FechaValue
        End Get
        Set(ByVal value As Date)
            FechaValue = value
        End Set
    End Property

    Private TipoValue As String
    Public Property Tipo() As String
        Get
            Return TipoValue
        End Get
        Set(ByVal value As String)
            TipoValue = value
        End Set
    End Property

    Private FormValue As Form
    Public Property Form() As Form
        Get
            Return FormValue
        End Get
        Set(ByVal value As Form)
            FormValue = value
        End Set
    End Property

    Private RestarValue As Boolean
    Public Property Restar() As Boolean
        Get
            Return RestarValue
        End Get
        Set(ByVal value As Boolean)
            RestarValue = value
        End Set
    End Property


    'FUNCIONES
    Sub Inicializar()

        '

        'Funciones
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Crear estructura de Cheque
        dtCheques = CSistema.ExecuteToDataTable("Select Top(0) * From VChequesClienteFormaPago ").Clone

        'Crear estructura de Cheque de Tercero
        dtChequesTercero = dtCheques.Clone

        'Crear estructura de Efectivo
        dtEfectivo = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleEfectivo ").Clone

        'Crear estructura de Documentos
        dtDocumento = CSistema.ExecuteToDataTable("Select Top(0) * From VFormaPagoDocumento ").Clone

        'Crear estructura de la Forma de Pago
        dtFormaPago = CSistema.ExecuteToDataTable("Select Top(0) * From VFormaPagoCobranzaCredito Order By ID ASC").Clone

    End Sub

    Sub Reset()

        dtFormaPago.Rows.Clear()
        dtEfectivo.Rows.Clear()
        dtCheques.Rows.Clear()
        dtDocumento.Rows.Clear()
        ListarFormaPago()

    End Sub

    Sub SeleccionarChequeTercero()

        Dim frm As New frmSeleccionChequeTercero
        frm.Text = "Seleccionar cheques de terceros"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtChequesTercero.Clone
        frm.ID = dtChequesTercero.Rows.Count + 1
        frm.ShowDialog(Me)
        'dtChequesTercero = frm.dt.Clone


        If frm.dt Is Nothing Then
            Exit Sub
        End If

        'Cargamos los Seleccionados
        For Each oRow As DataRow In frm.dt.Rows

            If CBool(oRow("Sel").ToString) = True Then
                dtChequesTercero.ImportRow(oRow)
                RaiseEvent RegistroInsertado(New Object, New EventArgs, oRow, ENUMFormaPago.ChequeTercero)
            End If

        Next

        ListarFormaPago()

    End Sub

    Sub SeleccionarEfectivo()

        Dim frm As New frmSeleccionEfectivo
        frm.Text = "Seleccionar el tipo de efectivo"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Fecha = Fecha
        frm.Comprobante = Comprobante
        frm.Saldo = Saldo
        frm.dt = dtEfectivo.Clone
        frm.ID = dtEfectivo.Rows.Count + 1
        frm.Tipo = Tipo
        frm.ShowDialog(Me)

        If frm.dt.Rows.Count > 0 Then
            dtEfectivo.ImportRow(frm.dt.Rows(0))
            RaiseEvent RegistroInsertado(New Object, New EventArgs, frm.dt.Rows(0), ENUMFormaPago.Efectivo)
        End If

        ListarFormaPago()

    End Sub

    Sub SeleccionarDocumento()

        Dim frm As New frmSeleccionFormaPagoDocumento
        frm.Text = "Documentos de Pago y/o Cobro"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Fecha = Fecha
        frm.Saldo = Saldo
        frm.Comprobante = Comprobante
        frm.dt = dtDocumento.Clone
        frm.ID = dtDocumento.Rows.Count + 1
        frm.ShowDialog(Me)

        If frm.dt.Rows.Count > 0 Then
            dtDocumento.ImportRow(frm.dt.Rows(0))
            RaiseEvent RegistroInsertado(New Object, New EventArgs, frm.dt.Rows(0), ENUMFormaPago.Documento)
        End If

        ListarFormaPago()

    End Sub

    Sub ListarFormaPago()

        dgw.Rows.Clear()
        dtFormaPago.Rows.Clear()

        'Agregar el Efectivo
        For Each oRow As DataRow In dtEfectivo.Rows

            Dim NewRow As DataRow = dtFormaPago.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("ID") = dtFormaPago.Rows.Count + 1
            NewRow("FormaPago") = "EFECTIVO"
            NewRow("FormaPago") = oRow("TipoComprobante").ToString
            NewRow("Moneda") = oRow("Moneda").ToString
            NewRow("ImporteMoneda") = oRow("ImporteMoneda").ToString
            NewRow("Cambio") = oRow("Cotizacion").ToString
            NewRow("Banco") = "---"
            NewRow("Comprobante") = oRow("Comprobante").ToString
            NewRow("Tipo") = "---"
            NewRow("Importe") = oRow("Importe").ToString
            NewRow("Restar") = False

            dtFormaPago.Rows.Add(NewRow)

        Next

        'Agregar los Cheques
        For Each oRow As DataRow In dtCheques.Rows

            If CBool(oRow("Sel").ToString) = True Then
                Dim NewRow As DataRow = dtFormaPago.NewRow

                NewRow("IDTransaccion") = 0
                NewRow("ID") = dtFormaPago.Rows.Count + 1
                NewRow("FormaPago") = "CHEQUE"
                NewRow("Moneda") = oRow("Moneda").ToString
                NewRow("ImporteMoneda") = oRow("Importe").ToString
                NewRow("Cambio") = oRow("Cotizacion").ToString
                NewRow("Banco") = oRow("Banco").ToString
                NewRow("Comprobante") = oRow("NroCheque").ToString
                NewRow("Tipo") = oRow("Tipo").ToString
                NewRow("FechaEmision") = oRow("Fecha").ToString
                NewRow("Vencimiento") = oRow("FechaVencimiento").ToString
                NewRow("Importe") = oRow("Importe").ToString
                NewRow("Restar") = False

                dtFormaPago.Rows.Add(NewRow)

            End If

        Next

        'Agregar los Cheques de Terceros
        For Each oRow As DataRow In dtChequesTercero.Rows

            If CBool(oRow("Sel").ToString) = True Then
                Dim NewRow As DataRow = dtFormaPago.NewRow

                NewRow("IDTransaccion") = 0
                NewRow("ID") = dtFormaPago.Rows.Count + 1
                NewRow("FormaPago") = "CHEQUE TERCERO"
                NewRow("Moneda") = oRow("Moneda").ToString
                NewRow("ImporteMoneda") = oRow("Importe").ToString
                NewRow("Cambio") = oRow("Cotizacion").ToString
                NewRow("Banco") = oRow("Banco").ToString
                NewRow("Comprobante") = oRow("NroCheque").ToString
                NewRow("Tipo") = oRow("Tipo").ToString
                NewRow("FechaEmision") = oRow("Fecha").ToString
                NewRow("Vencimiento") = oRow("FechaVencimiento").ToString
                NewRow("Importe") = oRow("Importe").ToString
                NewRow("Restar") = False

                dtFormaPago.Rows.Add(NewRow)

            End If

        Next

        'Agregar el Documentos

        For Each oRow As DataRow In dtDocumento.Rows

            Dim NewRow As DataRow = dtFormaPago.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("ID") = dtFormaPago.Rows.Count + 1
            NewRow("FormaPago") = "DOCUMENTO"
            NewRow("FormaPago") = oRow("TipoComprobante").ToString
            NewRow("Moneda") = oRow("Moneda").ToString
            NewRow("ImporteMoneda") = oRow("ImporteMoneda").ToString
            NewRow("Cambio") = oRow("Cotizacion").ToString
            NewRow("Banco") = "---"
            NewRow("Comprobante") = oRow("Comprobante").ToString
            NewRow("Tipo") = "---"
            NewRow("Importe") = oRow("Importe").ToString
            NewRow("Restar") = CBool(oRow("Restar").ToString)

            dtFormaPago.Rows.Add(NewRow)

        Next

        'Cargar en la Lista
        For Each oRow As DataRow In dtFormaPago.Rows

            Dim Registro(11) As String
            Registro(0) = oRow("ID").ToString
            Registro(1) = oRow("FormaPago").ToString
            Registro(2) = oRow("Moneda").ToString
            Registro(3) = CSistema.FormatoMoneda(oRow("ImporteMoneda").ToString)
            Registro(4) = CSistema.FormatoMoneda(oRow("Cambio").ToString)
            Registro(5) = oRow("Banco").ToString
            Registro(6) = oRow("Comprobante").ToString
            Registro(7) = oRow("Tipo").ToString
            Registro(8) = oRow("FechaEmision").ToString
            Registro(9) = oRow("Vencimiento").ToString
            Registro(10) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(11) = CSistema.FormatoMoneda(oRow("IDTransaccion").ToString)

            dgw.Rows.Add(Registro)

        Next

        CalcularTotales()
        RaiseEvent ImporteModificado(New Object, New EventArgs)

    End Sub

    Public Sub ListarFormaPago(ByVal IDTransaccion As Integer)

        dgw.Rows.Clear()
        dtFormaPago.Rows.Clear()

        dtFormaPago = CSistema.ExecuteToDataTable("Select * From VFormaPagoCobranzaCredito Where IDTransaccion=" & IDTransaccion & " Order By ID ASC ").Copy

        'Cargar en la Lista
        For Each oRow As DataRow In dtFormaPago.Rows

            Dim Registro(11) As String
            Registro(0) = oRow("ID").ToString
            Registro(1) = oRow("TipoComprobante").ToString
            Registro(2) = oRow("Moneda").ToString
            Registro(3) = CSistema.FormatoMoneda(oRow("ImporteMoneda").ToString)
            Registro(4) = CSistema.FormatoMoneda(oRow("Cambio").ToString)
            Registro(5) = oRow("Banco").ToString
            Registro(6) = oRow("Comprobante").ToString
            Registro(7) = oRow("Tipo").ToString
            Registro(8) = oRow("FechaEmision").ToString
            Registro(9) = oRow("Vencimiento").ToString
            Registro(10) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(11) = CSistema.FormatoMoneda(oRow("IDTransaccion").ToString)

            dgw.Rows.Add(Registro)

        Next

        CalcularTotales()
        RaiseEvent ImporteModificado(New Object, New EventArgs)

    End Sub

    Sub EliminarFormaPago()

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim ID As Integer = dgw.SelectedRows(0).Cells(0).Value

        For Each oRow As DataRow In dtFormaPago.Select(" ID=" & ID)

            Dim TipoComprobante As String = oRow("FormaPago").ToString
            Dim Comprobante As String = oRow("Comprobante").ToString

            'CHEQUES
            If TipoComprobante = "CHEQUE" Then
                Dim NroCheque As String = oRow("Comprobante").ToString
                Dim ChequeRow As DataRow = dtCheques.Select("NroCheque='" & NroCheque & "'")(0)
                ChequeRow("Sel") = False
                RaiseEvent RegistroEliminado(New Object, New EventArgs, ChequeRow, ENUMFormaPago.Cheque)
            End If

            'CHEQUES TERCERO
            If TipoComprobante = "CHEQUE TERCERO" Then
                Dim NroCheque As String = oRow("Comprobante").ToString
                Dim ChequeRow As DataRow = dtChequesTercero.Select("NroCheque='" & NroCheque & "'")(0)
                ChequeRow("Sel") = False
                RaiseEvent RegistroEliminado(New Object, New EventArgs, ChequeRow, ENUMFormaPago.ChequeTercero)
            End If

            'EFECTIVO
            If dtEfectivo.Select("TipoComprobante='" & TipoComprobante & "' And Comprobante='" & Comprobante & "'").GetLength(0) > 0 Then
                Dim EfectivoRow As DataRow = dtEfectivo.Select("ID=" & ID)(0)
                RaiseEvent RegistroEliminado(New Object, New EventArgs, EfectivoRow, ENUMFormaPago.Efectivo)
                dtEfectivo.Rows.Remove(EfectivoRow)
            End If

            'DOCUMENTOS
            If dtDocumento.Select("TipoComprobante='" & TipoComprobante & "' And Comprobante='" & Comprobante & "'").GetLength(0) > 0 Then
                Dim DocumentoRow As DataRow = dtDocumento.Select("TipoComprobante='" & TipoComprobante & "' And Comprobante='" & Comprobante & "'")(0)
                RaiseEvent RegistroEliminado(New Object, New EventArgs, DocumentoRow, ENUMFormaPago.Documento)
                dtDocumento.Rows.Remove(DocumentoRow)
            End If

        Next

        ListarFormaPago()

    End Sub


    Sub CalcularTotales()

        Dim tmp As Decimal = 0

        'Sumar
        For Each oRow As DataRow In dtFormaPago.Rows
            If CBool(oRow("Restar")) = True Then
                tmp = tmp - CDec(oRow("Importe").ToString)
            Else
                tmp = tmp + CDec(oRow("Importe").ToString)
            End If
        Next

        txtTotalFormaPago.SetValue(tmp)
        Total = tmp

    End Sub

    Sub ModificarFormaPago()

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        If dgw.SelectedRows(0).Cells(11).Value = 0 Then
            Exit Sub
        End If

        Dim mnuContextMenu As New ContextMenuStrip()
        mnuContextMenu.Items.Add("Modificar")
        dgw.ContextMenuStrip = mnuContextMenu

        AddHandler mnuContextMenu.Click, AddressOf SeleccionarDocumentoModificar

    End Sub

    Sub SeleccionarDocumentoModificar()

        Dim frm As New frmSeleccionModificarFormaPagoDocumento
        frm.Text = "Modificar Documentos de Pago y/o Cobro"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen


        Dim vIDTransaccion As Integer = dgw.SelectedRows(0).Cells(11).Value
        Dim vID As Integer = dgw.SelectedRows(0).Cells(0).Value
        Dim FormaPago As String = ""
        Dim Where As String = ""


        If vIDTransaccion = 0 Then
            Exit Sub
        End If


        dtFormaPago = CSistema.ExecuteToDataTable("Select * From VFormaPagoCobranzaCredito Where IDTransaccion=" & vIDTransaccion & " And ID=" & vID)

        If dtFormaPago.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dtFormaPago.Rows(0)

        FormaPago = oRow("FormaPago").ToString

        If FormaPago = "Documento" Then
            frm.IDTransaccion = vIDTransaccion
            frm.txtID.txt.Text = vID
            frm.ShowDialog(Me)

            ListarFormaPago(vIDTransaccion)
        Else
            Exit Sub
        End If

    End Sub

    Private Sub btnCheque_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheque.Click
        SeleccionarChequeTercero()
    End Sub

    Private Sub btnEfectivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEfectivo.Click
        SeleccionarEfectivo()
    End Sub

    Private Sub lklEliminarFormaPago_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarFormaPago.LinkClicked
        EliminarFormaPago()
    End Sub

    Private Sub lklNuevoChequeCliente_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklNuevoChequeCliente.LinkClicked
        FGMostrarFormulario(Form, frmChequeCliente, "Ingreso de Cheques de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True)
        RaiseEvent ListarCheques(sender, e)
    End Sub

    Private Sub btnDocumentos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDocumentos.Click
        SeleccionarDocumento()
    End Sub

    Private Sub dgw_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgw.MouseDown
        ModificarFormaPago()
    End Sub


End Class

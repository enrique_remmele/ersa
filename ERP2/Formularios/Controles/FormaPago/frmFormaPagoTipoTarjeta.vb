﻿Public Class frmFormaPagoTipoTarjeta

    'CLASE
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private ComprobanteValue As String
    Public Property Comprobante() As String
        Get
            Return ComprobanteValue
        End Get
        Set(ByVal value As String)
            ComprobanteValue = value
        End Set
    End Property

    Private FechaValue As Date
    Public Property Fecha() As Date
        Get
            Return FechaValue
        End Get
        Set(ByVal value As Date)
            FechaValue = value
        End Set
    End Property

    Private SaldoValue As Decimal
    Public Property Saldo() As Decimal
        Get
            Return SaldoValue
        End Get
        Set(ByVal value As Decimal)
            SaldoValue = value
        End Set
    End Property

    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private TipoValue As String
    Public Property Tipo() As String
        Get
            Return TipoValue
        End Get
        Set(ByVal value As String)
            TipoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "PAGOS Y COBROS CON TARJETA", "TAR")

        CargarInformacion()
        txtComprobante.txt.Text = Comprobante
        txtID.SetValue(ID)
        txtFecha.SetValue(Fecha)
        txtImporteMoneda.SetValue(Saldo)
        txtImporte.SetValue(CSistema.Cotizador(txtCotizacion.ObtenerValor, txtImporteMoneda.ObtenerValor, cbxMoneda.cbx.SelectedValue))

        'Foco
        cbxTipoComprobante.cbx.Focus()

    End Sub

    Sub CargarInformacion()

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Tipo de Tarjeta
        CSistema.SqlToComboBox(cbxTipoTarjeta.cbx, "Select ID, Descripcion From TipoTarjeta ORDER BY Descripcion")

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Referencia From Moneda")
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        txtCotizacion.SetValue(1)

    End Sub

    Sub Aceptar()
        Try
            Dim oRow As DataRow = dt.NewRow
            oRow("IDTransaccion") = 0
            oRow("ID") = ID
            oRow("IDTipoComprobante") = cbxTipoComprobante.GetValue
            oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
            oRow("Comprobante") = txtComprobante.txt.Text
            oRow("Fecha") = txtFecha.GetValue
            oRow("Importe") = txtImporte.ObtenerValor
            oRow("IDMoneda") = cbxMoneda.cbx.SelectedValue
            oRow("Moneda") = cbxMoneda.cbx.Text
            oRow("ImporteMoneda") = txtImporteMoneda.ObtenerValor
            oRow("Cotizacion") = txtCotizacion.ObtenerValor
            oRow("Observacion") = txtObservacion.txt.Text
            oRow("IDTipoTarjeta") = cbxTipoTarjeta.GetValue
            oRow("TerminacionTarjeta") = txtTerminacionTarjeta.txt.Text
            If IsDBNull(txtFecha.GetValue) = False AndAlso txtFecha.GetValue <> Date.MinValue Then
                oRow("FechaVencimientoTarjeta") = txtFecha.GetValue
            Else
                oRow("FechaVencimientoTarjeta") = DBNull.Value
            End If
            oRow("Boleta") = txtBoleta.GetValue
            dt.Rows.Add(oRow)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


        Me.Close()

    End Sub

    Private Sub txtImporte_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtImporteMoneda.TeclaPrecionada
        'If txtImporte.txt.Focused = True Then
        '    Exit Sub
        'End If

        txtImporte.SetValue(CSistema.Cotizador(txtCotizacion.ObtenerValor, txtImporteMoneda.ObtenerValor, cbxMoneda.cbx.SelectedValue))

    End Sub

    Private Sub frmSeleccionEfectivo_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmSeleccionEfectivo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Aceptar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub cbxTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoComprobante.PropertyChanged
        txtTipoDocumento.txt.Text = CSistema.ExecuteScalar("Select Descripcion From TipoComprobante Where ID = " & cbxTipoComprobante.GetValue)
    End Sub

End Class
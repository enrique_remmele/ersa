﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxFormaPagoLote
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.btnDocumentos = New System.Windows.Forms.Button()
        Me.btnCheque = New System.Windows.Forms.Button()
        Me.btnEfectivo = New System.Windows.Forms.Button()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.lklNuevoChequeCliente = New System.Windows.Forms.LinkLabel()
        Me.lklEliminarFormaPago = New System.Windows.Forms.LinkLabel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalFormaPago = New ERP.ocxTXTNumeric()
        Me.lblTotalFormaPago = New System.Windows.Forms.Label()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFormaPago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporteMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCambio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBanco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFechaEmision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colVencimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.dgw, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(717, 238)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 4
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.btnDocumentos, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.btnCheque, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.btnEfectivo, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(711, 27)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'btnDocumentos
        '
        Me.btnDocumentos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnDocumentos.Location = New System.Drawing.Point(203, 3)
        Me.btnDocumentos.Name = "btnDocumentos"
        Me.btnDocumentos.Size = New System.Drawing.Size(94, 21)
        Me.btnDocumentos.TabIndex = 3
        Me.btnDocumentos.Text = "Documentos"
        Me.btnDocumentos.UseVisualStyleBackColor = True
        '
        'btnCheque
        '
        Me.btnCheque.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnCheque.Location = New System.Drawing.Point(103, 3)
        Me.btnCheque.Name = "btnCheque"
        Me.btnCheque.Size = New System.Drawing.Size(94, 21)
        Me.btnCheque.TabIndex = 2
        Me.btnCheque.Text = "Cheque"
        Me.btnCheque.UseVisualStyleBackColor = True
        '
        'btnEfectivo
        '
        Me.btnEfectivo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnEfectivo.Location = New System.Drawing.Point(3, 3)
        Me.btnEfectivo.Name = "btnEfectivo"
        Me.btnEfectivo.Size = New System.Drawing.Size(94, 21)
        Me.btnEfectivo.TabIndex = 1
        Me.btnEfectivo.Text = "Efectivo"
        Me.btnEfectivo.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 4
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 97.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 159.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.lklNuevoChequeCliente, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.lklEliminarFormaPago, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.FlowLayoutPanel1, 3, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 205)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(711, 30)
        Me.TableLayoutPanel3.TabIndex = 1
        '
        'lklNuevoChequeCliente
        '
        Me.lklNuevoChequeCliente.AutoSize = True
        Me.lklNuevoChequeCliente.Location = New System.Drawing.Point(143, 0)
        Me.lklNuevoChequeCliente.Name = "lklNuevoChequeCliente"
        Me.lklNuevoChequeCliente.Size = New System.Drawing.Size(78, 13)
        Me.lklNuevoChequeCliente.TabIndex = 7
        Me.lklNuevoChequeCliente.TabStop = True
        Me.lklNuevoChequeCliente.Text = "Nuevo cheque"
        '
        'lklEliminarFormaPago
        '
        Me.lklEliminarFormaPago.AutoSize = True
        Me.lklEliminarFormaPago.Location = New System.Drawing.Point(3, 0)
        Me.lklEliminarFormaPago.Name = "lklEliminarFormaPago"
        Me.lklEliminarFormaPago.Size = New System.Drawing.Size(114, 13)
        Me.lklEliminarFormaPago.TabIndex = 6
        Me.lklEliminarFormaPago.TabStop = True
        Me.lklEliminarFormaPago.Text = "Eliminar forma de pago"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalFormaPago)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblTotalFormaPago)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(399, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(309, 24)
        Me.FlowLayoutPanel1.TabIndex = 9
        '
        'txtTotalFormaPago
        '
        Me.txtTotalFormaPago.Color = System.Drawing.Color.Empty
        Me.txtTotalFormaPago.Decimales = True
        Me.txtTotalFormaPago.Indicaciones = Nothing
        Me.txtTotalFormaPago.Location = New System.Drawing.Point(160, 3)
        Me.txtTotalFormaPago.Name = "txtTotalFormaPago"
        Me.txtTotalFormaPago.Size = New System.Drawing.Size(146, 20)
        Me.txtTotalFormaPago.SoloLectura = True
        Me.txtTotalFormaPago.TabIndex = 10
        Me.txtTotalFormaPago.TabStop = False
        Me.txtTotalFormaPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalFormaPago.Texto = "0"
        '
        'lblTotalFormaPago
        '
        Me.lblTotalFormaPago.AutoSize = True
        Me.lblTotalFormaPago.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTotalFormaPago.Location = New System.Drawing.Point(120, 0)
        Me.lblTotalFormaPago.Name = "lblTotalFormaPago"
        Me.lblTotalFormaPago.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalFormaPago.TabIndex = 11
        Me.lblTotalFormaPago.Text = "Total:"
        Me.lblTotalFormaPago.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        Me.dgw.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colID, Me.colFormaPago, Me.colMoneda, Me.colImporteMoneda, Me.colCambio, Me.colBanco, Me.colComprobante, Me.colTipo, Me.colFechaEmision, Me.colVencimiento, Me.colImporte, Me.colIDTransaccion})
        Me.dgw.Location = New System.Drawing.Point(3, 36)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(711, 163)
        Me.dgw.TabIndex = 15
        Me.dgw.TabStop = False
        '
        'colID
        '
        Me.colID.HeaderText = "ID"
        Me.colID.Name = "colID"
        Me.colID.ReadOnly = True
        Me.colID.Width = 20
        '
        'colFormaPago
        '
        Me.colFormaPago.HeaderText = "FormaPago"
        Me.colFormaPago.Name = "colFormaPago"
        Me.colFormaPago.ReadOnly = True
        Me.colFormaPago.Width = 80
        '
        'colMoneda
        '
        Me.colMoneda.HeaderText = "Mon"
        Me.colMoneda.Name = "colMoneda"
        Me.colMoneda.ReadOnly = True
        Me.colMoneda.Width = 30
        '
        'colImporteMoneda
        '
        Me.colImporteMoneda.HeaderText = "Importe"
        Me.colImporteMoneda.Name = "colImporteMoneda"
        Me.colImporteMoneda.ReadOnly = True
        Me.colImporteMoneda.Width = 70
        '
        'colCambio
        '
        Me.colCambio.HeaderText = "Cambio"
        Me.colCambio.Name = "colCambio"
        Me.colCambio.ReadOnly = True
        Me.colCambio.Width = 25
        '
        'colBanco
        '
        Me.colBanco.HeaderText = "Banco"
        Me.colBanco.Name = "colBanco"
        Me.colBanco.ReadOnly = True
        Me.colBanco.Width = 55
        '
        'colComprobante
        '
        Me.colComprobante.HeaderText = "Comprobante"
        Me.colComprobante.Name = "colComprobante"
        Me.colComprobante.ReadOnly = True
        Me.colComprobante.Width = 80
        '
        'colTipo
        '
        Me.colTipo.HeaderText = "Tipo"
        Me.colTipo.Name = "colTipo"
        Me.colTipo.ReadOnly = True
        Me.colTipo.Width = 60
        '
        'colFechaEmision
        '
        Me.colFechaEmision.HeaderText = "Fecha Emis."
        Me.colFechaEmision.Name = "colFechaEmision"
        Me.colFechaEmision.ReadOnly = True
        Me.colFechaEmision.Width = 90
        '
        'colVencimiento
        '
        Me.colVencimiento.HeaderText = "Vencimiento"
        Me.colVencimiento.Name = "colVencimiento"
        Me.colVencimiento.ReadOnly = True
        Me.colVencimiento.Width = 90
        '
        'colImporte
        '
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Width = 5
        '
        'ocxFormaPagoLote
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxFormaPagoLote"
        Me.Size = New System.Drawing.Size(717, 238)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnEfectivo As System.Windows.Forms.Button
    Friend WithEvents btnCheque As System.Windows.Forms.Button
    Friend WithEvents btnDocumentos As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lklEliminarFormaPago As System.Windows.Forms.LinkLabel
    Friend WithEvents lklNuevoChequeCliente As System.Windows.Forms.LinkLabel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotalFormaPago As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalFormaPago As System.Windows.Forms.Label
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents colID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFormaPago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colMoneda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporteMoneda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCambio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBanco As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFechaEmision As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colVencimiento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIDTransaccion As System.Windows.Forms.DataGridViewTextBoxColumn

End Class

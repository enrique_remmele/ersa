﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeleccionChequeCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.lblDeudaTotal = New System.Windows.Forms.Label()
        Me.lblTotalCobrado = New System.Windows.Forms.Label()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.txtCantidadCheque = New ERP.ocxTXTNumeric()
        Me.txtTotalSeleccionado = New ERP.ocxTXTNumeric()
        Me.txtCantidadSeleccionado = New ERP.ocxTXTNumeric()
        Me.txtTotalCheque = New ERP.ocxTXTNumeric()
        Me.txtCliente = New ERP.ocxTXTString()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colCiu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBanco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCuentaBancaria = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCancelar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 217)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(785, 22)
        Me.StatusStrip1.TabIndex = 12
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Cliente:"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(641, 185)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "&Cancelar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(560, 185)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "&Aceptar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colSel, Me.colCiu, Me.colBanco, Me.colNro, Me.colCuentaBancaria, Me.colFecha, Me.colMoneda, Me.colTotal, Me.colSaldo, Me.colImporte, Me.colCancelar})
        Me.dgw.Location = New System.Drawing.Point(15, 32)
        Me.dgw.Name = "dgw"
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgw.Size = New System.Drawing.Size(758, 119)
        Me.dgw.TabIndex = 2
        Me.dgw.TabStop = False
        '
        'lblDeudaTotal
        '
        Me.lblDeudaTotal.AutoSize = True
        Me.lblDeudaTotal.Location = New System.Drawing.Point(491, 162)
        Me.lblDeudaTotal.Name = "lblDeudaTotal"
        Me.lblDeudaTotal.Size = New System.Drawing.Size(75, 13)
        Me.lblDeudaTotal.TabIndex = 9
        Me.lblDeudaTotal.Text = "Seleccionado:"
        '
        'lblTotalCobrado
        '
        Me.lblTotalCobrado.AutoSize = True
        Me.lblTotalCobrado.Location = New System.Drawing.Point(301, 162)
        Me.lblTotalCobrado.Name = "lblTotalCobrado"
        Me.lblTotalCobrado.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalCobrado.TabIndex = 6
        Me.lblTotalCobrado.Text = "Total:"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(12, 157)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(87, 13)
        Me.LinkLabel1.TabIndex = 5
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Seleccionar todo"
        '
        'txtCantidadCheque
        '
        Me.txtCantidadCheque.Color = System.Drawing.Color.Empty
        Me.txtCantidadCheque.Decimales = True
        Me.txtCantidadCheque.Indicaciones = Nothing
        Me.txtCantidadCheque.Location = New System.Drawing.Point(440, 157)
        Me.txtCantidadCheque.Name = "txtCantidadCheque"
        Me.txtCantidadCheque.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadCheque.SoloLectura = False
        Me.txtCantidadCheque.TabIndex = 8
        Me.txtCantidadCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadCheque.Texto = "0"
        '
        'txtTotalSeleccionado
        '
        Me.txtTotalSeleccionado.Color = System.Drawing.Color.Empty
        Me.txtTotalSeleccionado.Decimales = True
        Me.txtTotalSeleccionado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalSeleccionado.Indicaciones = Nothing
        Me.txtTotalSeleccionado.Location = New System.Drawing.Point(566, 157)
        Me.txtTotalSeleccionado.Name = "txtTotalSeleccionado"
        Me.txtTotalSeleccionado.Size = New System.Drawing.Size(105, 22)
        Me.txtTotalSeleccionado.SoloLectura = True
        Me.txtTotalSeleccionado.TabIndex = 10
        Me.txtTotalSeleccionado.TabStop = False
        Me.txtTotalSeleccionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalSeleccionado.Texto = "0"
        '
        'txtCantidadSeleccionado
        '
        Me.txtCantidadSeleccionado.Color = System.Drawing.Color.Empty
        Me.txtCantidadSeleccionado.Decimales = True
        Me.txtCantidadSeleccionado.Indicaciones = Nothing
        Me.txtCantidadSeleccionado.Location = New System.Drawing.Point(671, 157)
        Me.txtCantidadSeleccionado.Name = "txtCantidadSeleccionado"
        Me.txtCantidadSeleccionado.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadSeleccionado.SoloLectura = False
        Me.txtCantidadSeleccionado.TabIndex = 11
        Me.txtCantidadSeleccionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadSeleccionado.Texto = "0"
        '
        'txtTotalCheque
        '
        Me.txtTotalCheque.Color = System.Drawing.Color.Empty
        Me.txtTotalCheque.Decimales = True
        Me.txtTotalCheque.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalCheque.Indicaciones = Nothing
        Me.txtTotalCheque.Location = New System.Drawing.Point(335, 157)
        Me.txtTotalCheque.Name = "txtTotalCheque"
        Me.txtTotalCheque.Size = New System.Drawing.Size(105, 22)
        Me.txtTotalCheque.SoloLectura = True
        Me.txtTotalCheque.TabIndex = 7
        Me.txtTotalCheque.TabStop = False
        Me.txtTotalCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCheque.Texto = "0"
        '
        'txtCliente
        '
        Me.txtCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCliente.Color = System.Drawing.Color.Empty
        Me.txtCliente.Indicaciones = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(68, 5)
        Me.txtCliente.Multilinea = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(330, 21)
        Me.txtCliente.SoloLectura = True
        Me.txtCliente.TabIndex = 1
        Me.txtCliente.TabStop = False
        Me.txtCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCliente.Texto = ""
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colSel
        '
        Me.colSel.HeaderText = "Sel"
        Me.colSel.Name = "colSel"
        Me.colSel.ReadOnly = True
        Me.colSel.Width = 30
        '
        'colCiu
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colCiu.DefaultCellStyle = DataGridViewCellStyle2
        Me.colCiu.HeaderText = "Ciudad"
        Me.colCiu.Name = "colCiu"
        Me.colCiu.ReadOnly = True
        Me.colCiu.ToolTipText = "Ciudad"
        Me.colCiu.Width = 50
        '
        'colBanco
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colBanco.DefaultCellStyle = DataGridViewCellStyle3
        Me.colBanco.HeaderText = "Banco"
        Me.colBanco.Name = "colBanco"
        Me.colBanco.ReadOnly = True
        Me.colBanco.ToolTipText = "Banco"
        Me.colBanco.Width = 92
        '
        'colNro
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colNro.DefaultCellStyle = DataGridViewCellStyle4
        Me.colNro.HeaderText = "Nro."
        Me.colNro.Name = "colNro"
        Me.colNro.ReadOnly = True
        Me.colNro.ToolTipText = "Numero de  Cheque"
        Me.colNro.Width = 80
        '
        'colCuentaBancaria
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colCuentaBancaria.DefaultCellStyle = DataGridViewCellStyle5
        Me.colCuentaBancaria.HeaderText = "Nro. C.B."
        Me.colCuentaBancaria.Name = "colCuentaBancaria"
        Me.colCuentaBancaria.ReadOnly = True
        Me.colCuentaBancaria.ToolTipText = "Numero de Cuenta Bancaria"
        Me.colCuentaBancaria.Width = 80
        '
        'colFecha
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colFecha.DefaultCellStyle = DataGridViewCellStyle6
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.Name = "colFecha"
        Me.colFecha.ReadOnly = True
        Me.colFecha.ToolTipText = "Fecha de Emision"
        Me.colFecha.Width = 55
        '
        'colMoneda
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colMoneda.DefaultCellStyle = DataGridViewCellStyle7
        Me.colMoneda.HeaderText = "Moneda"
        Me.colMoneda.Name = "colMoneda"
        Me.colMoneda.ReadOnly = True
        Me.colMoneda.Width = 50
        '
        'colTotal
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N0"
        DataGridViewCellStyle8.NullValue = "0"
        Me.colTotal.DefaultCellStyle = DataGridViewCellStyle8
        Me.colTotal.HeaderText = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.ReadOnly = True
        Me.colTotal.ToolTipText = "Importe Total del Cheque"
        Me.colTotal.Width = 75
        '
        'colSaldo
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.Format = "N0"
        DataGridViewCellStyle9.NullValue = "0"
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black
        Me.colSaldo.DefaultCellStyle = DataGridViewCellStyle9
        Me.colSaldo.HeaderText = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.ReadOnly = True
        '
        'colImporte
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N0"
        DataGridViewCellStyle10.NullValue = "0"
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle10
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        '
        'colCancelar
        '
        Me.colCancelar.HeaderText = "Canc."
        Me.colCancelar.Name = "colCancelar"
        Me.colCancelar.Width = 35
        '
        'frmSeleccionChequeCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(785, 239)
        Me.Controls.Add(Me.LinkLabel1)
        Me.Controls.Add(Me.txtCantidadCheque)
        Me.Controls.Add(Me.txtTotalSeleccionado)
        Me.Controls.Add(Me.lblDeudaTotal)
        Me.Controls.Add(Me.txtCantidadSeleccionado)
        Me.Controls.Add(Me.txtTotalCheque)
        Me.Controls.Add(Me.lblTotalCobrado)
        Me.Controls.Add(Me.dgw)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.txtCliente)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmSeleccionChequeCliente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmSeleccionChequeCliente"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents txtCliente As ERP.ocxTXTString
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents txtCantidadCheque As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalSeleccionado As ERP.ocxTXTNumeric
    Friend WithEvents lblDeudaTotal As System.Windows.Forms.Label
    Friend WithEvents txtCantidadSeleccionado As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalCheque As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalCobrado As System.Windows.Forms.Label
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents colIDTransaccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSel As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colCiu As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBanco As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCuentaBancaria As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colMoneda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSaldo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCancelar As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class

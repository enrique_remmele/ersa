﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeleccionEfectivo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblCambio = New System.Windows.Forms.Label()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.gbxDatos = New System.Windows.Forms.GroupBox()
        Me.txtTipoDocumento = New ERP.ocxTXTString()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.txtImporteMoneda = New ERP.ocxTXTNumeric()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.txtFecha = New ERP.ocxTXTDate()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.gbxDatos.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblCambio
        '
        Me.lblCambio.AutoSize = True
        Me.lblCambio.Location = New System.Drawing.Point(144, 71)
        Me.lblCambio.Name = "lblCambio"
        Me.lblCambio.Size = New System.Drawing.Size(45, 13)
        Me.lblCambio.TabIndex = 11
        Me.lblCambio.Text = "Cambio:"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(228, 71)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(45, 13)
        Me.lblImporte.TabIndex = 13
        Me.lblImporte.Text = "Importe:"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(41, 71)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 9
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(233, 44)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 5
        Me.lblFecha.Text = "Fecha:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(17, 44)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 3
        Me.lblComprobante.Text = "Comprobante:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(393, 137)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(312, 137)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 1
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 181)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(472, 22)
        Me.StatusStrip1.TabIndex = 3
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(20, 98)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 16
        Me.lblObservacion.Text = "Observacion:"
        '
        'gbxDatos
        '
        Me.gbxDatos.Controls.Add(Me.txtTipoDocumento)
        Me.gbxDatos.Controls.Add(Me.Label1)
        Me.gbxDatos.Controls.Add(Me.lblID)
        Me.gbxDatos.Controls.Add(Me.txtID)
        Me.gbxDatos.Controls.Add(Me.lblComprobante)
        Me.gbxDatos.Controls.Add(Me.txtObservacion)
        Me.gbxDatos.Controls.Add(Me.lblMoneda)
        Me.gbxDatos.Controls.Add(Me.lblObservacion)
        Me.gbxDatos.Controls.Add(Me.cbxMoneda)
        Me.gbxDatos.Controls.Add(Me.txtCotizacion)
        Me.gbxDatos.Controls.Add(Me.lblImporte)
        Me.gbxDatos.Controls.Add(Me.txtImporteMoneda)
        Me.gbxDatos.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxDatos.Controls.Add(Me.lblCambio)
        Me.gbxDatos.Controls.Add(Me.txtComprobante)
        Me.gbxDatos.Controls.Add(Me.txtImporte)
        Me.gbxDatos.Controls.Add(Me.lblFecha)
        Me.gbxDatos.Controls.Add(Me.txtFecha)
        Me.gbxDatos.Location = New System.Drawing.Point(0, 1)
        Me.gbxDatos.Name = "gbxDatos"
        Me.gbxDatos.Size = New System.Drawing.Size(469, 130)
        Me.gbxDatos.TabIndex = 0
        Me.gbxDatos.TabStop = False
        '
        'txtTipoDocumento
        '
        Me.txtTipoDocumento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoDocumento.Color = System.Drawing.Color.Empty
        Me.txtTipoDocumento.Indicaciones = Nothing
        Me.txtTipoDocumento.Location = New System.Drawing.Point(142, 14)
        Me.txtTipoDocumento.Multilinea = False
        Me.txtTipoDocumento.Name = "txtTipoDocumento"
        Me.txtTipoDocumento.Size = New System.Drawing.Size(313, 21)
        Me.txtTipoDocumento.SoloLectura = False
        Me.txtTipoDocumento.TabIndex = 2
        Me.txtTipoDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTipoDocumento.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(1, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Tipo Documento:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(346, 44)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 7
        Me.lblID.Text = "ID:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(373, 40)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(82, 21)
        Me.txtID.SoloLectura = True
        Me.txtID.TabIndex = 8
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(91, 94)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(364, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 17
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(91, 67)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(53, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 10
        Me.cbxMoneda.Texto = ""
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(189, 67)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(39, 21)
        Me.txtCotizacion.SoloLectura = False
        Me.txtCotizacion.TabIndex = 12
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "0"
        '
        'txtImporteMoneda
        '
        Me.txtImporteMoneda.Color = System.Drawing.Color.Empty
        Me.txtImporteMoneda.Decimales = False
        Me.txtImporteMoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteMoneda.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporteMoneda.Location = New System.Drawing.Point(273, 67)
        Me.txtImporteMoneda.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtImporteMoneda.Name = "txtImporteMoneda"
        Me.txtImporteMoneda.Size = New System.Drawing.Size(91, 21)
        Me.txtImporteMoneda.SoloLectura = False
        Me.txtImporteMoneda.TabIndex = 14
        Me.txtImporteMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteMoneda.Texto = "0"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(91, 14)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(53, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 1
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(91, 40)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(137, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 4
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = False
        Me.txtImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporte.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporte.Location = New System.Drawing.Point(364, 67)
        Me.txtImporte.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(91, 21)
        Me.txtImporte.SoloLectura = True
        Me.txtImporte.TabIndex = 15
        Me.txtImporte.TabStop = False
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 6, 27, 15, 21, 5, 127)
        Me.txtFecha.Location = New System.Drawing.Point(273, 40)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(67, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 6
        '
        'frmSeleccionEfectivo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(472, 203)
        Me.Controls.Add(Me.gbxDatos)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmSeleccionEfectivo"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Efectivo"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.gbxDatos.ResumeLayout(False)
        Me.gbxDatos.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtImporte As ERP.ocxTXTNumeric
    Friend WithEvents lblCambio As System.Windows.Forms.Label
    Friend WithEvents txtImporteMoneda As ERP.ocxTXTNumeric
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents gbxDatos As System.Windows.Forms.GroupBox
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents txtTipoDocumento As ERP.ocxTXTString
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificarDetalleDescuentoVenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvDescuento = New System.Windows.Forms.DataGridView()
        Me.lblDescuento = New System.Windows.Forms.Label()
        Me.txtPorcentajeDescuento = New ERP.ocxTXTNumeric()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAplicar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtDescuento = New ERP.ocxTXTNumeric()
        Me.gbxDescuento = New System.Windows.Forms.GroupBox()
        Me.lblPrecioUnitario = New System.Windows.Forms.Label()
        Me.txtPrecioUnitario = New ERP.ocxTXTNumeric()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        CType(Me.dgvDescuento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDescuento.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvDescuento
        '
        Me.dgvDescuento.AllowUserToAddRows = False
        Me.dgvDescuento.AllowUserToDeleteRows = False
        Me.dgvDescuento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDescuento.Location = New System.Drawing.Point(6, 19)
        Me.dgvDescuento.Name = "dgvDescuento"
        Me.dgvDescuento.ReadOnly = True
        Me.dgvDescuento.Size = New System.Drawing.Size(416, 132)
        Me.dgvDescuento.TabIndex = 0
        '
        'lblDescuento
        '
        Me.lblDescuento.AutoSize = True
        Me.lblDescuento.Location = New System.Drawing.Point(6, 154)
        Me.lblDescuento.Name = "lblDescuento"
        Me.lblDescuento.Size = New System.Drawing.Size(38, 13)
        Me.lblDescuento.TabIndex = 1
        Me.lblDescuento.Text = "Desc.:"
        '
        'txtPorcentajeDescuento
        '
        Me.txtPorcentajeDescuento.Color = System.Drawing.Color.Empty
        Me.txtPorcentajeDescuento.Decimales = True
        Me.txtPorcentajeDescuento.Indicaciones = Nothing
        Me.txtPorcentajeDescuento.Location = New System.Drawing.Point(6, 170)
        Me.txtPorcentajeDescuento.Name = "txtPorcentajeDescuento"
        Me.txtPorcentajeDescuento.Size = New System.Drawing.Size(60, 21)
        Me.txtPorcentajeDescuento.SoloLectura = False
        Me.txtPorcentajeDescuento.TabIndex = 2
        Me.txtPorcentajeDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajeDescuento.Texto = "0"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(347, 168)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 10
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAplicar
        '
        Me.btnAplicar.Location = New System.Drawing.Point(266, 168)
        Me.btnAplicar.Name = "btnAplicar"
        Me.btnAplicar.Size = New System.Drawing.Size(75, 23)
        Me.btnAplicar.TabIndex = 9
        Me.btnAplicar.Text = "Aplicar"
        Me.btnAplicar.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(182, 154)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Descuento:"
        '
        'txtDescuento
        '
        Me.txtDescuento.Color = System.Drawing.Color.Empty
        Me.txtDescuento.Decimales = True
        Me.txtDescuento.Indicaciones = Nothing
        Me.txtDescuento.Location = New System.Drawing.Point(182, 170)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(72, 21)
        Me.txtDescuento.SoloLectura = True
        Me.txtDescuento.TabIndex = 8
        Me.txtDescuento.TabStop = False
        Me.txtDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDescuento.Texto = "0"
        '
        'gbxDescuento
        '
        Me.gbxDescuento.Controls.Add(Me.lblCantidad)
        Me.gbxDescuento.Controls.Add(Me.txtCantidad)
        Me.gbxDescuento.Controls.Add(Me.dgvDescuento)
        Me.gbxDescuento.Controls.Add(Me.lblDescuento)
        Me.gbxDescuento.Controls.Add(Me.txtPorcentajeDescuento)
        Me.gbxDescuento.Controls.Add(Me.btnCancelar)
        Me.gbxDescuento.Controls.Add(Me.btnAplicar)
        Me.gbxDescuento.Controls.Add(Me.Label3)
        Me.gbxDescuento.Controls.Add(Me.txtDescuento)
        Me.gbxDescuento.Controls.Add(Me.lblPrecioUnitario)
        Me.gbxDescuento.Controls.Add(Me.txtPrecioUnitario)
        Me.gbxDescuento.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxDescuento.Location = New System.Drawing.Point(0, 0)
        Me.gbxDescuento.Name = "gbxDescuento"
        Me.gbxDescuento.Size = New System.Drawing.Size(432, 205)
        Me.gbxDescuento.TabIndex = 0
        Me.gbxDescuento.TabStop = False
        Me.gbxDescuento.Text = "DECUENTOS:"
        '
        'lblPrecioUnitario
        '
        Me.lblPrecioUnitario.AutoSize = True
        Me.lblPrecioUnitario.Location = New System.Drawing.Point(103, 154)
        Me.lblPrecioUnitario.Name = "lblPrecioUnitario"
        Me.lblPrecioUnitario.Size = New System.Drawing.Size(51, 13)
        Me.lblPrecioUnitario.TabIndex = 5
        Me.lblPrecioUnitario.Text = "Pre. Uni.:"
        '
        'txtPrecioUnitario
        '
        Me.txtPrecioUnitario.Color = System.Drawing.Color.Empty
        Me.txtPrecioUnitario.Decimales = True
        Me.txtPrecioUnitario.Indicaciones = Nothing
        Me.txtPrecioUnitario.Location = New System.Drawing.Point(103, 170)
        Me.txtPrecioUnitario.Name = "txtPrecioUnitario"
        Me.txtPrecioUnitario.Size = New System.Drawing.Size(78, 21)
        Me.txtPrecioUnitario.SoloLectura = True
        Me.txtPrecioUnitario.TabIndex = 6
        Me.txtPrecioUnitario.TabStop = False
        Me.txtPrecioUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrecioUnitario.Texto = "0"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(184, 169)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 10
        Me.Button2.Text = "Aplicar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(67, 154)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(35, 13)
        Me.lblCantidad.TabIndex = 3
        Me.lblCantidad.Text = "Cant.:"
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = Nothing
        Me.txtCantidad.Location = New System.Drawing.Point(67, 170)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(35, 21)
        Me.txtCantidad.SoloLectura = True
        Me.txtCantidad.TabIndex = 4
        Me.txtCantidad.TabStop = False
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'frmModificarDetalleDescuentoVenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(432, 205)
        Me.Controls.Add(Me.gbxDescuento)
        Me.Name = "frmModificarDetalleDescuentoVenta"
        Me.Text = "frmModificarDetalleDescuentoVenta"
        CType(Me.dgvDescuento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDescuento.ResumeLayout(False)
        Me.gbxDescuento.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvDescuento As System.Windows.Forms.DataGridView
    Friend WithEvents lblDescuento As System.Windows.Forms.Label
    Friend WithEvents txtPorcentajeDescuento As ERP.ocxTXTNumeric
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAplicar As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtDescuento As ERP.ocxTXTNumeric
    Friend WithEvents gbxDescuento As System.Windows.Forms.GroupBox
    Friend WithEvents lblPrecioUnitario As System.Windows.Forms.Label
    Friend WithEvents txtPrecioUnitario As ERP.ocxTXTNumeric
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As ERP.ocxTXTNumeric
End Class

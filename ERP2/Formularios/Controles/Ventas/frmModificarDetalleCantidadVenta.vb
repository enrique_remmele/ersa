﻿Public Class frmModificarDetalleCantidadVenta

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        CargarDatos()

    End Sub

    Sub CargarDatos()

        CSistema.dtToGrid(dgvProducto, dt)

        'Formato
        For c As Integer = 0 To dgvProducto.ColumnCount - 1
            dgvProducto.Columns(c).Visible = False
        Next

        dgvProducto.Columns("Referencia").Visible = True
        dgvProducto.Columns("Referencia").DisplayIndex = 0
        dgvProducto.Columns("Producto").Visible = True
        dgvProducto.Columns("Pre. Uni.").Visible = True
        dgvProducto.Columns("Cantidad").Visible = True
        dgvProducto.Columns("Total").Visible = True

        dgvProducto.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvProducto.Columns("Pre. Uni.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvProducto.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvProducto.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvProducto.Columns("Pre. Uni.").DefaultCellStyle.Format = "N0"
        dgvProducto.Columns("Cantidad").DefaultCellStyle.Format = "N2"
        dgvProducto.Columns("Total").DefaultCellStyle.Format = "N0"

        For Each oRow As DataRow In dt.Rows

            txtPrecioUnitario.SetValue(oRow("PrecioUnitario"))
            txtCantidad.SetValue(oRow("Cantidad"))
            txtImporte.SetValue(oRow("Total"))

        Next

    End Sub

    Sub CalcularImporte(ByVal Decimales As Boolean, ByVal Redondear As Boolean, ByVal PorTotal As Boolean)

        Dim Cantidad As Decimal
        Dim PrecioUnitario As Decimal
        Dim Importe As Decimal

        Cantidad = txtCantidad.ObtenerValor

        'Validar
        If Cantidad = 0 Then
            Exit Sub
        End If

        If PorTotal = True Then
            PrecioUnitario = txtImporte.ObtenerValor / Cantidad
        Else
            Dim PorcentajeDescuento As Decimal = CSistema.dtSumColumn(dtDescuento, "Porcentaje")
            Dim Descuento As Decimal = CSistema.CalcularPorcentaje(txtPrecioUnitario.ObtenerValor, PorcentajeDescuento, True, False)
            Dim SubTotal As Decimal = 0
            SubTotal = txtPrecioUnitario.ObtenerValor - Descuento
            Importe = txtPrecioUnitario.ObtenerValor * Cantidad
        End If

        If Decimales = False Then
            PrecioUnitario = CInt(Math.Round(PrecioUnitario, 0))
            Importe = CInt(Math.Round(Importe, 0))
        Else
            If Redondear = True Then
                PrecioUnitario = CInt(Math.Round(PrecioUnitario, 0))
                Importe = CInt(Math.Round(Importe, 0))
            End If
        End If

        If PorTotal = True Then
            txtPrecioUnitario.txt.Text = PrecioUnitario.ToString
        Else
            txtImporte.txt.Text = Importe.ToString
        End If

    End Sub

    Sub Aplicar()

        'Validar la cantidad
        'Obtenemos la cantidad que existe en el deposito el producto seleccionado
        Dim IDProducto As Integer = dt.Rows(0)("IDProducto")
        Dim IDDeposito As Integer = dt.Rows(0)("IDDeposito")
        Dim ExistenciaActual As Decimal = CSistema.ExecuteScalar("Select dbo.FExistenciaProducto(" & IDProducto & ", " & IDDeposito & ")")
        Dim Cantidad As Decimal = txtCantidad.ObtenerValor

        'Obtenemos la cantidad de productos, excepto los de la linea actual
        For Each oRow As DataRow In dt.Rows
            If oRow("IDProducto").ToString = IDProducto Then
                ExistenciaActual = ExistenciaActual + CDec(oRow("Cantidad").ToString)
            End If
        Next

        If Cantidad > ExistenciaActual Then
            MessageBox.Show("No hay suficiente stock!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Procesado = False
            Exit Sub
        End If

        'Actualizar las cantidades
        For Each oRow As DataRow In dt.Rows

            'Cambiar la cantidad y el total
            oRow("Cantidad") = txtCantidad.ObtenerValor
            oRow("Total") = txtCantidad.ObtenerValor * oRow("PrecioUnitario")
            oRow("TotalImpuesto") = 0
            oRow("TotalDiscriminado") = 0
            CSistema.CalcularIVA(oRow("IDImpuesto").ToString, CDec(oRow("Total").ToString), True, False, oRow("TotalDiscriminado"), oRow("TotalImpuesto"))

            'Costos
            oRow("TotalCosto") = txtCantidad.ObtenerValor * oRow("CostoUnitario")
            oRow("TotalCostoImpuesto") = 0
            oRow("TotalCostoDiscriminado") = 0
            CSistema.CalcularIVA(oRow("IDImpuesto").ToString, CDec(oRow("TotalCosto").ToString), True, False, oRow("TotalCostoDiscriminado"), oRow("TotalCostoImpuesto"))

        Next

        'Actualizar descuentos
        For Each oRow As DataRow In dtDescuento.Rows

            'Cambiar la cantidad y el total
            oRow("Cantidad") = txtCantidad.ObtenerValor
            oRow("Total") = txtCantidad.ObtenerValor * oRow("Descuento")
            oRow("TotalDescuentoDiscriminado") = txtCantidad.ObtenerValor * oRow("DescuentoDiscriminado")

        Next

        Procesado = True
        Me.Close()

    End Sub

    Private Sub frmModificarDetalleVenta_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Private Sub frmModificarDetalleVenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Procesado = False
        Me.Close()
    End Sub

    Private Sub dgvProducto_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then

            e.SuppressKeyPress = True

        End If

    End Sub

    Private Sub txtCantidad_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.TeclaPrecionada
        If txtCantidad.txt.Focused = False Then
            Exit Sub
        End If

        CalcularImporte(False, True, False)

    End Sub

    Private Sub btnAplicar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAplicar.Click
        Aplicar()
    End Sub

    Private Sub btnCancelar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Procesado = False
        Me.Close()
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxDocumentosVentas
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtSaldo = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtNotaDebito = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNotaCredito = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtCobranza = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.rdbCobranza = New System.Windows.Forms.RadioButton()
        Me.rdbNotaCredito = New System.Windows.Forms.RadioButton()
        Me.OcxDocumentosVentasNotaCredito1 = New ERP.ocxDocumentosVentasNotaCredito()
        Me.OcxDocumentosVentasCobranzas1 = New ERP.ocxDocumentosVentasCobranzas()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.43052!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.56948!))
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(734, 400)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtSaldo)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtNotaDebito)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtNotaCredito)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtCobranza)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtTotal)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(564, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.TableLayoutPanel1.SetRowSpan(Me.GroupBox1, 2)
        Me.GroupBox1.Size = New System.Drawing.Size(167, 394)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Valores"
        '
        'txtSaldo
        '
        Me.txtSaldo.BackColor = System.Drawing.Color.White
        Me.txtSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldo.ForeColor = System.Drawing.Color.Maroon
        Me.txtSaldo.Location = New System.Drawing.Point(70, 172)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.ReadOnly = True
        Me.txtSaldo.Size = New System.Drawing.Size(91, 20)
        Me.txtSaldo.TabIndex = 10
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 176)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(37, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Saldo:"
        '
        'txtNotaDebito
        '
        Me.txtNotaDebito.BackColor = System.Drawing.Color.MintCream
        Me.txtNotaDebito.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotaDebito.Location = New System.Drawing.Point(70, 60)
        Me.txtNotaDebito.Name = "txtNotaDebito"
        Me.txtNotaDebito.ReadOnly = True
        Me.txtNotaDebito.Size = New System.Drawing.Size(91, 20)
        Me.txtNotaDebito.TabIndex = 3
        Me.txtNotaDebito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "N. Debitos:"
        '
        'txtNotaCredito
        '
        Me.txtNotaCredito.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtNotaCredito.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotaCredito.ForeColor = System.Drawing.Color.Maroon
        Me.txtNotaCredito.Location = New System.Drawing.Point(70, 123)
        Me.txtNotaCredito.Name = "txtNotaCredito"
        Me.txtNotaCredito.ReadOnly = True
        Me.txtNotaCredito.Size = New System.Drawing.Size(91, 20)
        Me.txtNotaCredito.TabIndex = 7
        Me.txtNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 127)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "N. Creditos:"
        '
        'txtCobranza
        '
        Me.txtCobranza.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCobranza.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCobranza.ForeColor = System.Drawing.Color.Maroon
        Me.txtCobranza.Location = New System.Drawing.Point(70, 97)
        Me.txtCobranza.Name = "txtCobranza"
        Me.txtCobranza.ReadOnly = True
        Me.txtCobranza.Size = New System.Drawing.Size(91, 20)
        Me.txtCobranza.TabIndex = 5
        Me.txtCobranza.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 101)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Cobranzas:"
        '
        'txtTotal
        '
        Me.txtTotal.BackColor = System.Drawing.Color.MintCream
        Me.txtTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.Location = New System.Drawing.Point(70, 34)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.ReadOnly = True
        Me.txtTotal.Size = New System.Drawing.Size(91, 20)
        Me.txtTotal.TabIndex = 1
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Total:"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(24, 144)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(137, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "_______________________________"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.OcxDocumentosVentasNotaCredito1)
        Me.Panel1.Controls.Add(Me.OcxDocumentosVentasCobranzas1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(555, 356)
        Me.Panel1.TabIndex = 2
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbCobranza)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbNotaCredito)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 365)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(555, 32)
        Me.FlowLayoutPanel1.TabIndex = 3
        '
        'rdbCobranza
        '
        Me.rdbCobranza.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdbCobranza.AutoSize = True
        Me.rdbCobranza.Cursor = System.Windows.Forms.Cursors.Hand
        Me.rdbCobranza.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.rdbCobranza.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.rdbCobranza.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.rdbCobranza.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.rdbCobranza.Location = New System.Drawing.Point(3, 3)
        Me.rdbCobranza.Name = "rdbCobranza"
        Me.rdbCobranza.Size = New System.Drawing.Size(69, 25)
        Me.rdbCobranza.TabIndex = 0
        Me.rdbCobranza.TabStop = True
        Me.rdbCobranza.Text = "Cobranzas"
        Me.rdbCobranza.UseVisualStyleBackColor = True
        '
        'rdbNotaCredito
        '
        Me.rdbNotaCredito.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdbNotaCredito.AutoSize = True
        Me.rdbNotaCredito.Cursor = System.Windows.Forms.Cursors.Hand
        Me.rdbNotaCredito.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.rdbNotaCredito.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.rdbNotaCredito.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.rdbNotaCredito.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.rdbNotaCredito.Location = New System.Drawing.Point(78, 3)
        Me.rdbNotaCredito.Name = "rdbNotaCredito"
        Me.rdbNotaCredito.Size = New System.Drawing.Size(98, 25)
        Me.rdbNotaCredito.TabIndex = 1
        Me.rdbNotaCredito.TabStop = True
        Me.rdbNotaCredito.Text = "Notas de Credito"
        Me.rdbNotaCredito.UseVisualStyleBackColor = True
        '
        'OcxDocumentosVentasNotaCredito1
        '
        Me.OcxDocumentosVentasNotaCredito1.Cantidad = 0
        Me.OcxDocumentosVentasNotaCredito1.IDTransaccion = Nothing
        Me.OcxDocumentosVentasNotaCredito1.Location = New System.Drawing.Point(125, 101)
        Me.OcxDocumentosVentasNotaCredito1.Name = "OcxDocumentosVentasNotaCredito1"
        Me.OcxDocumentosVentasNotaCredito1.Size = New System.Drawing.Size(380, 150)
        Me.OcxDocumentosVentasNotaCredito1.TabIndex = 0
        Me.OcxDocumentosVentasNotaCredito1.Visible = False
        '
        'OcxDocumentosVentasCobranzas1
        '
        Me.OcxDocumentosVentasCobranzas1.Cantidad = 0
        Me.OcxDocumentosVentasCobranzas1.IDTransaccion = Nothing
        Me.OcxDocumentosVentasCobranzas1.Location = New System.Drawing.Point(16, 101)
        Me.OcxDocumentosVentasCobranzas1.Name = "OcxDocumentosVentasCobranzas1"
        Me.OcxDocumentosVentasCobranzas1.Size = New System.Drawing.Size(305, 133)
        Me.OcxDocumentosVentasCobranzas1.TabIndex = 0
        Me.OcxDocumentosVentasCobranzas1.Visible = False
        '
        'ocxDocumentosVentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxDocumentosVentas"
        Me.Size = New System.Drawing.Size(734, 400)
        Me.TableLayoutPanel1.ResumeLayout(false)
        Me.GroupBox1.ResumeLayout(false)
        Me.GroupBox1.PerformLayout
        Me.Panel1.ResumeLayout(false)
        Me.FlowLayoutPanel1.ResumeLayout(false)
        Me.FlowLayoutPanel1.PerformLayout
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents OcxDocumentosVentasCobranzas1 As ERP.ocxDocumentosVentasCobranzas
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtSaldo As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtNotaDebito As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNotaCredito As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCobranza As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents OcxDocumentosVentasNotaCredito1 As ERP.ocxDocumentosVentasNotaCredito
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents rdbCobranza As System.Windows.Forms.RadioButton
    Friend WithEvents rdbNotaCredito As System.Windows.Forms.RadioButton

End Class

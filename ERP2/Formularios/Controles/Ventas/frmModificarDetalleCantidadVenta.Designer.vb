﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificarDetalleCantidadVenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxCantidad = New System.Windows.Forms.GroupBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.dgvProducto = New System.Windows.Forms.DataGridView()
        Me.btnAplicar = New System.Windows.Forms.Button()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.lblPrecioUnitario = New System.Windows.Forms.Label()
        Me.txtPrecioUnitario = New ERP.ocxTXTNumeric()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.gbxCantidad.SuspendLayout()
        CType(Me.dgvProducto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxCantidad
        '
        Me.gbxCantidad.Controls.Add(Me.btnCancelar)
        Me.gbxCantidad.Controls.Add(Me.dgvProducto)
        Me.gbxCantidad.Controls.Add(Me.btnAplicar)
        Me.gbxCantidad.Controls.Add(Me.lblImporte)
        Me.gbxCantidad.Controls.Add(Me.txtImporte)
        Me.gbxCantidad.Controls.Add(Me.lblPrecioUnitario)
        Me.gbxCantidad.Controls.Add(Me.txtPrecioUnitario)
        Me.gbxCantidad.Controls.Add(Me.lblCantidad)
        Me.gbxCantidad.Controls.Add(Me.txtCantidad)
        Me.gbxCantidad.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxCantidad.Location = New System.Drawing.Point(0, 0)
        Me.gbxCantidad.Name = "gbxCantidad"
        Me.gbxCantidad.Size = New System.Drawing.Size(384, 133)
        Me.gbxCantidad.TabIndex = 2
        Me.gbxCantidad.TabStop = False
        Me.gbxCantidad.Text = "CANTIDADES"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(295, 94)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 11
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'dgvProducto
        '
        Me.dgvProducto.AllowUserToAddRows = False
        Me.dgvProducto.AllowUserToDeleteRows = False
        Me.dgvProducto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProducto.Location = New System.Drawing.Point(6, 19)
        Me.dgvProducto.Name = "dgvProducto"
        Me.dgvProducto.ReadOnly = True
        Me.dgvProducto.Size = New System.Drawing.Size(366, 49)
        Me.dgvProducto.TabIndex = 0
        '
        'btnAplicar
        '
        Me.btnAplicar.Location = New System.Drawing.Point(214, 94)
        Me.btnAplicar.Name = "btnAplicar"
        Me.btnAplicar.Size = New System.Drawing.Size(75, 23)
        Me.btnAplicar.TabIndex = 10
        Me.btnAplicar.Text = "Aplicar"
        Me.btnAplicar.UseVisualStyleBackColor = True
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(122, 79)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(45, 13)
        Me.lblImporte.TabIndex = 8
        Me.lblImporte.Text = "Importe:"
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = True
        Me.txtImporte.Indicaciones = Nothing
        Me.txtImporte.Location = New System.Drawing.Point(122, 95)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(86, 21)
        Me.txtImporte.SoloLectura = True
        Me.txtImporte.TabIndex = 9
        Me.txtImporte.TabStop = False
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'lblPrecioUnitario
        '
        Me.lblPrecioUnitario.AutoSize = True
        Me.lblPrecioUnitario.Location = New System.Drawing.Point(54, 79)
        Me.lblPrecioUnitario.Name = "lblPrecioUnitario"
        Me.lblPrecioUnitario.Size = New System.Drawing.Size(45, 13)
        Me.lblPrecioUnitario.TabIndex = 0
        Me.lblPrecioUnitario.Text = "Pr. Uni.:"
        '
        'txtPrecioUnitario
        '
        Me.txtPrecioUnitario.Color = System.Drawing.Color.Empty
        Me.txtPrecioUnitario.Decimales = True
        Me.txtPrecioUnitario.Indicaciones = Nothing
        Me.txtPrecioUnitario.Location = New System.Drawing.Point(54, 95)
        Me.txtPrecioUnitario.Name = "txtPrecioUnitario"
        Me.txtPrecioUnitario.Size = New System.Drawing.Size(68, 21)
        Me.txtPrecioUnitario.SoloLectura = True
        Me.txtPrecioUnitario.TabIndex = 1
        Me.txtPrecioUnitario.TabStop = False
        Me.txtPrecioUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrecioUnitario.Texto = "0"
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(6, 79)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(35, 13)
        Me.lblCantidad.TabIndex = 2
        Me.lblCantidad.Text = "Cant.:"
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = Nothing
        Me.txtCantidad.Location = New System.Drawing.Point(6, 95)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(48, 21)
        Me.txtCantidad.SoloLectura = False
        Me.txtCantidad.TabIndex = 3
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'frmModificarDetalleCantidadVenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 133)
        Me.Controls.Add(Me.gbxCantidad)
        Me.Name = "frmModificarDetalleCantidadVenta"
        Me.Text = "frmModificarDetalleVenta"
        Me.gbxCantidad.ResumeLayout(False)
        Me.gbxCantidad.PerformLayout()
        CType(Me.dgvProducto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxCantidad As System.Windows.Forms.GroupBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents dgvProducto As System.Windows.Forms.DataGridView
    Friend WithEvents btnAplicar As System.Windows.Forms.Button
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents txtImporte As ERP.ocxTXTNumeric
    Friend WithEvents lblPrecioUnitario As System.Windows.Forms.Label
    Friend WithEvents txtPrecioUnitario As ERP.ocxTXTNumeric
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As ERP.ocxTXTNumeric
End Class

﻿Imports System.IO

Public Class ocxClientesMapa

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private LatitudValue As String
    Public Property Latitud() As String
        Get
            Return LatitudValue
        End Get
        Set(ByVal value As String)
            LatitudValue = value
        End Set
    End Property

    Private LongitudValue As String
    Public Property Longitud() As String
        Get
            Return LongitudValue
        End Get
        Set(ByVal value As String)
            LongitudValue = value
        End Set
    End Property

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IniciadoValue As Boolean
    Public Property Iniciado() As Boolean
        Get
            Return IniciadoValue
        End Get
        Set(ByVal value As Boolean)
            IniciadoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Crear Archivo
        CrearArchivo()
        Iniciado = True

        'Variables
        'Panel2.BackColor = System.Drawing.ColorTranslator.FromHtml(Color)
        'lblZona.Text = ZonaVenta
        'Timer1.Enabled = False

    End Sub

    'Crear Archivo
    Sub CrearArchivo()

        'Si no hay internet, salir
        If CSistema.CheckForInternetConnection = False Then
            Iniciado = False
            Exit Sub
        End If

        Dim Archivo As String = VGCarpetaAplicacion & "htmLocalizarCliente.htm"

        'Si es que existe, eliminar
        Try

            If File.Exists(Archivo) Then
                File.Delete(Archivo)
            End If

        Catch ex As Exception

        End Try


        'Crear Archivo
        Try
            Dim oSW As New StreamWriter(Archivo)
            oSW.Write(My.Resources.htmLocalizarCliente)
            oSW.Flush()

        Catch ex As Exception

        End Try

        Try
            Threading.Thread.Sleep(1000)
            WebBrowser1.Navigate(Archivo)
        Catch ex As Exception
            CerrarMapa()
        End Try

    End Sub

    Sub Guardar()

        Try
            Dim lat As String = ""
            Dim lng As String = ""

            lat = WebBrowser1.Document.GetElementById("txtLatitudMarker").GetAttribute("value").ToString
            lng = WebBrowser1.Document.GetElementById("txtLongitudMarker").GetAttribute("value").ToString

            Dim sql As String = ""
            If rdbMatriz.Checked = True Then
                sql = "Update Cliente Set Latitud='" & lat & "', Longitud='" & lng & "' Where ID=" & ID
            Else
                sql = "Update ClienteSucursal Set Latitud='" & lat & "', Longitud='" & lng & "' Where IDCliente=" & ID & " And ID=" & cbxSucursales.SelectedValue & " "
                For Each oRow As DataRow In dt.Select("IDCliente=" & ID & " And ID=" & cbxSucursales.SelectedValue)
                    oRow("Latitud") = lat
                    oRow("Longitud") = lng
                Next
            End If

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                MessageBox.Show("El sistema no actualizo el registro! Intente nuevamente.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
        Catch ex As Exception
            CerrarMapa()
        End Try
        

    End Sub

    Sub CargarCliente()

        Try

            'Cargar las Sucursales
            dt = CSistema.ExecuteToDataTable("Select * From ClienteSucursal Where IDCliente=" & ID).Copy
            If dt.Rows.Count > 0 Then
                rdbSucursal.Enabled = True
                CSistema.SqlToComboBox(cbxSucursales, dt, "ID", "Sucursal")
            Else
                rdbSucursal.Checked = False
                rdbSucursal.Enabled = False
            End If

            rdbMatriz.Checked = True
            cbxSucursales.Enabled = False

            WebBrowser1.Document.GetElementById("txtLatitudMarker").InnerText = Latitud
            WebBrowser1.Document.GetElementById("txtLongitudMarker").InnerText = Longitud
            WebBrowser1.Document.GetElementById("btnCargarCliente").InvokeMember("click")

        Catch ex As Exception
            CerrarMapa()
        End Try
       

    End Sub

    Sub CargarSucursal()

        Dim Lat As String = ""
        Dim Lng As String = ""

        For Each oRow As DataRow In dt.Select("ID=" & cbxSucursales.SelectedValue & " And IDCliente=" & ID)
            Lat = oRow("Latitud").ToString
            Lng = oRow("Longitud").ToString
        Next

        WebBrowser1.Document.GetElementById("txtLatitudMarker").InnerText = Lat
        WebBrowser1.Document.GetElementById("txtLongitudMarker").InnerText = Lng
        WebBrowser1.Document.GetElementById("btnCargarCliente").InvokeMember("click")

    End Sub

    Sub CerrarMapa()

        TableLayoutPanel1.Enabled = False
        WebBrowser1.Dispose()

        Dim lbl As New Label
        lbl.Text = "SIN CONEXION"
        lbl.Name = "lbl"
        lbl.AutoSize = False
        lbl.Dock = DockStyle.Fill
        lbl.TextAlign = ContentAlignment.MiddleCenter
        lbl.ForeColor = Color.Red
        lbl.Font = New Font("Arial", 24, FontStyle.Bold)

        For Each ctr As Control In TableLayoutPanel1.Controls

            If ctr.Name = "lbl" Then
                ctr.Dispose()
            End If

        Next

        TableLayoutPanel1.Controls.Add(lbl)


    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        Try
            WebBrowser1.Document.GetElementById("btnCargarCliente").InvokeMember("click")
            Timer1.Enabled = False
        Catch ex As Exception
            CerrarMapa()
        End Try
        

    End Sub


    Private Sub WebBrowser1_DocumentCompleted(ByVal sender As Object, ByVal e As System.Windows.Forms.WebBrowserDocumentCompletedEventArgs) Handles WebBrowser1.DocumentCompleted

        Try
            WebBrowser1.Document.GetElementById("txtLatitudMarker").InnerText = Latitud
            WebBrowser1.Document.GetElementById("txtLongitudMarker").InnerText = Longitud

            Timer1.Interval = 1000
            Timer1.Enabled = True
        Catch ex As Exception
            CerrarMapa()
        End Try
        
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Inicializar()
    End Sub

    Private Sub rdbSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbSucursal.CheckedChanged
        If rdbSucursal.Checked = True Then
            cbxSucursales.Enabled = True
        Else
            cbxSucursales.Enabled = False
        End If
    End Sub

    Private Sub cbxSucursales_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursales.SelectedIndexChanged
        CargarSucursal()
    End Sub

    Private Sub rdbMatriz_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbMatriz.CheckedChanged
        If rdbMatriz.Checked = True Then
            CargarCliente()
        Else
            CargarSucursal()
        End If
    End Sub

End Class

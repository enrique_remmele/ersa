﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxUsuarioPermitidosPorOperacion
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvUsuario = New System.Windows.Forms.DataGridView()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnQuitar = New System.Windows.Forms.Button()
        Me.dgvUsuarioPermitido = New System.Windows.Forms.DataGridView()
        Me.btnHabilitar = New System.Windows.Forms.Button()
        Me.btnDenegar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        CType(Me.dgvUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvUsuarioPermitido, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvUsuario
        '
        Me.dgvUsuario.AllowUserToAddRows = False
        Me.dgvUsuario.AllowUserToDeleteRows = False
        Me.dgvUsuario.AllowUserToOrderColumns = True
        Me.dgvUsuario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvUsuario.Location = New System.Drawing.Point(6, 75)
        Me.dgvUsuario.Name = "dgvUsuario"
        Me.dgvUsuario.ReadOnly = True
        Me.dgvUsuario.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvUsuario.Size = New System.Drawing.Size(156, 221)
        Me.dgvUsuario.StandardTab = True
        Me.dgvUsuario.TabIndex = 3
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = ""
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = "Usuario"
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = "Usuario"
        Me.cbxUsuario.DataSource = "VUsuario"
        Me.cbxUsuario.DataValueMember = "ID"
        Me.cbxUsuario.FormABM = "frmVendedor"
        Me.cbxUsuario.Indicaciones = "Seleccion de Vendedor"
        Me.cbxUsuario.Location = New System.Drawing.Point(6, 19)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(156, 21)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 0
        Me.cbxUsuario.Texto = "ALDO GONZALES"
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(6, 46)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(75, 23)
        Me.btnAgregar.TabIndex = 1
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'btnQuitar
        '
        Me.btnQuitar.Location = New System.Drawing.Point(87, 46)
        Me.btnQuitar.Name = "btnQuitar"
        Me.btnQuitar.Size = New System.Drawing.Size(75, 23)
        Me.btnQuitar.TabIndex = 2
        Me.btnQuitar.Text = "Quitar"
        Me.btnQuitar.UseVisualStyleBackColor = True
        '
        'dgvUsuarioPermitido
        '
        Me.dgvUsuarioPermitido.AllowUserToAddRows = False
        Me.dgvUsuarioPermitido.AllowUserToDeleteRows = False
        Me.dgvUsuarioPermitido.AllowUserToOrderColumns = True
        Me.dgvUsuarioPermitido.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvUsuarioPermitido.Location = New System.Drawing.Point(6, 75)
        Me.dgvUsuarioPermitido.Name = "dgvUsuarioPermitido"
        Me.dgvUsuarioPermitido.ReadOnly = True
        Me.dgvUsuarioPermitido.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvUsuarioPermitido.Size = New System.Drawing.Size(282, 221)
        Me.dgvUsuarioPermitido.StandardTab = True
        Me.dgvUsuarioPermitido.TabIndex = 2
        '
        'btnHabilitar
        '
        Me.btnHabilitar.Location = New System.Drawing.Point(6, 46)
        Me.btnHabilitar.Name = "btnHabilitar"
        Me.btnHabilitar.Size = New System.Drawing.Size(75, 23)
        Me.btnHabilitar.TabIndex = 0
        Me.btnHabilitar.Text = "Habilitar"
        Me.btnHabilitar.UseVisualStyleBackColor = True
        '
        'btnDenegar
        '
        Me.btnDenegar.Location = New System.Drawing.Point(87, 46)
        Me.btnDenegar.Name = "btnDenegar"
        Me.btnDenegar.Size = New System.Drawing.Size(75, 23)
        Me.btnDenegar.TabIndex = 1
        Me.btnDenegar.Text = "Denegar"
        Me.btnDenegar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbxUsuario)
        Me.GroupBox1.Controls.Add(Me.dgvUsuario)
        Me.GroupBox1.Controls.Add(Me.btnAgregar)
        Me.GroupBox1.Controls.Add(Me.btnQuitar)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(170, 306)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Usuarios Permitidos"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnHabilitar)
        Me.GroupBox2.Controls.Add(Me.dgvUsuarioPermitido)
        Me.GroupBox2.Controls.Add(Me.btnDenegar)
        Me.GroupBox2.Location = New System.Drawing.Point(184, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(296, 306)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Operaciones permitidas por usuario:"
        '
        'ocxUsuarioPermitidosPorOperacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "ocxUsuarioPermitidosPorOperacion"
        Me.Size = New System.Drawing.Size(486, 326)
        CType(Me.dgvUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvUsuarioPermitido, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvUsuario As System.Windows.Forms.DataGridView
    Friend WithEvents cbxUsuario As ERP.ocxCBX
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnQuitar As System.Windows.Forms.Button
    Friend WithEvents dgvUsuarioPermitido As System.Windows.Forms.DataGridView
    Friend WithEvents btnHabilitar As System.Windows.Forms.Button
    Friend WithEvents btnDenegar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox

End Class

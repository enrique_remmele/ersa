﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxOpcionesGenerales
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkBloquearDocumentos = New System.Windows.Forms.CheckBox()
        Me.nudDias = New System.Windows.Forms.NumericUpDown()
        Me.lblDiasAntiguedad = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPropietarioBD = New ERP.ocxTXTString()
        Me.txtFormatoFecha = New ERP.ocxTXTString()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.nudDias, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(400, 334)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 302)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(394, 29)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(316, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(235, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtPropietarioBD)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txtFormatoFecha)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.nudDias)
        Me.Panel1.Controls.Add(Me.chkBloquearDocumentos)
        Me.Panel1.Controls.Add(Me.lblDiasAntiguedad)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(394, 293)
        Me.Panel1.TabIndex = 0
        '
        'chkBloquearDocumentos
        '
        Me.chkBloquearDocumentos.AutoSize = True
        Me.chkBloquearDocumentos.Location = New System.Drawing.Point(13, 14)
        Me.chkBloquearDocumentos.Name = "chkBloquearDocumentos"
        Me.chkBloquearDocumentos.Size = New System.Drawing.Size(179, 17)
        Me.chkBloquearDocumentos.TabIndex = 0
        Me.chkBloquearDocumentos.Text = "Bloqueo general de documentos"
        Me.chkBloquearDocumentos.UseVisualStyleBackColor = True
        '
        'nudDias
        '
        Me.nudDias.Location = New System.Drawing.Point(191, 12)
        Me.nudDias.Name = "nudDias"
        Me.nudDias.Size = New System.Drawing.Size(40, 20)
        Me.nudDias.TabIndex = 1
        '
        'lblDiasAntiguedad
        '
        Me.lblDiasAntiguedad.AutoSize = True
        Me.lblDiasAntiguedad.Location = New System.Drawing.Point(231, 16)
        Me.lblDiasAntiguedad.Name = "lblDiasAntiguedad"
        Me.lblDiasAntiguedad.Size = New System.Drawing.Size(26, 13)
        Me.lblDiasAntiguedad.TabIndex = 2
        Me.lblDiasAntiguedad.Text = "dias"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(30, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(231, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "* Bloquea todo tipo de operacion y documentos"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 65)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Formato de Fecha:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 91)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(159, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Propietario de la Base de Datos:"
        '
        'txtPropietarioBD
        '
        Me.txtPropietarioBD.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPropietarioBD.Color = System.Drawing.Color.Empty
        Me.txtPropietarioBD.Indicaciones = Nothing
        Me.txtPropietarioBD.Location = New System.Drawing.Point(175, 87)
        Me.txtPropietarioBD.Multilinea = False
        Me.txtPropietarioBD.Name = "txtPropietarioBD"
        Me.txtPropietarioBD.Size = New System.Drawing.Size(82, 20)
        Me.txtPropietarioBD.SoloLectura = False
        Me.txtPropietarioBD.TabIndex = 7
        Me.txtPropietarioBD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPropietarioBD.Texto = ""
        '
        'txtFormatoFecha
        '
        Me.txtFormatoFecha.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtFormatoFecha.Color = System.Drawing.Color.Empty
        Me.txtFormatoFecha.Indicaciones = Nothing
        Me.txtFormatoFecha.Location = New System.Drawing.Point(111, 61)
        Me.txtFormatoFecha.Multilinea = False
        Me.txtFormatoFecha.Name = "txtFormatoFecha"
        Me.txtFormatoFecha.Size = New System.Drawing.Size(146, 20)
        Me.txtFormatoFecha.SoloLectura = False
        Me.txtFormatoFecha.TabIndex = 5
        Me.txtFormatoFecha.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFormatoFecha.Texto = ""
        '
        'ocxOpcionesGenerales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxOpcionesGenerales"
        Me.Size = New System.Drawing.Size(400, 334)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.nudDias, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkBloquearDocumentos As System.Windows.Forms.CheckBox
    Friend WithEvents nudDias As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblDiasAntiguedad As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtFormatoFecha As ERP.ocxTXTString
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtPropietarioBD As ERP.ocxTXTString
    Friend WithEvents Label3 As System.Windows.Forms.Label

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxConfiguracionMovimiento
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblDiasAntiguedad = New System.Windows.Forms.Label()
        Me.nudDiasAnulacion = New System.Windows.Forms.NumericUpDown()
        Me.chkBloquearAnulacion = New System.Windows.Forms.CheckBox()
        Me.chkHabilitarCostoSiEsCero = New System.Windows.Forms.CheckBox()
        Me.chkBloquearCampoCosto = New System.Windows.Forms.CheckBox()
        Me.chkBloquearFecha = New System.Windows.Forms.CheckBox()
        Me.chkImprimirDocumento = New System.Windows.Forms.CheckBox()
        Me.chkExigirAutorizacion = New System.Windows.Forms.CheckBox()
        Me.rdbPromedio = New System.Windows.Forms.RadioButton()
        Me.rdbUltimoCosto = New System.Windows.Forms.RadioButton()
        Me.lblSeleccionCosto = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        CType(Me.nudDiasAnulacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblDiasAntiguedad
        '
        Me.lblDiasAntiguedad.AutoSize = True
        Me.lblDiasAntiguedad.Location = New System.Drawing.Point(264, 201)
        Me.lblDiasAntiguedad.Name = "lblDiasAntiguedad"
        Me.lblDiasAntiguedad.Size = New System.Drawing.Size(97, 13)
        Me.lblDiasAntiguedad.TabIndex = 12
        Me.lblDiasAntiguedad.Text = "dias de antiguedad"
        '
        'nudDiasAnulacion
        '
        Me.nudDiasAnulacion.Location = New System.Drawing.Point(224, 197)
        Me.nudDiasAnulacion.Name = "nudDiasAnulacion"
        Me.nudDiasAnulacion.Size = New System.Drawing.Size(40, 20)
        Me.nudDiasAnulacion.TabIndex = 11
        '
        'chkBloquearAnulacion
        '
        Me.chkBloquearAnulacion.AutoSize = True
        Me.chkBloquearAnulacion.Location = New System.Drawing.Point(13, 199)
        Me.chkBloquearAnulacion.Name = "chkBloquearAnulacion"
        Me.chkBloquearAnulacion.Size = New System.Drawing.Size(211, 17)
        Me.chkBloquearAnulacion.TabIndex = 10
        Me.chkBloquearAnulacion.Text = "Bloquear anulacion de documentos de "
        Me.chkBloquearAnulacion.UseVisualStyleBackColor = True
        '
        'chkHabilitarCostoSiEsCero
        '
        Me.chkHabilitarCostoSiEsCero.AutoSize = True
        Me.chkHabilitarCostoSiEsCero.Enabled = False
        Me.chkHabilitarCostoSiEsCero.Location = New System.Drawing.Point(31, 104)
        Me.chkHabilitarCostoSiEsCero.Name = "chkHabilitarCostoSiEsCero"
        Me.chkHabilitarCostoSiEsCero.Size = New System.Drawing.Size(194, 17)
        Me.chkHabilitarCostoSiEsCero.TabIndex = 4
        Me.chkHabilitarCostoSiEsCero.Text = "Habilitar solo si es que el costo es 0"
        Me.chkHabilitarCostoSiEsCero.UseVisualStyleBackColor = True
        '
        'chkBloquearCampoCosto
        '
        Me.chkBloquearCampoCosto.AutoSize = True
        Me.chkBloquearCampoCosto.Location = New System.Drawing.Point(16, 81)
        Me.chkBloquearCampoCosto.Name = "chkBloquearCampoCosto"
        Me.chkBloquearCampoCosto.Size = New System.Drawing.Size(147, 17)
        Me.chkBloquearCampoCosto.TabIndex = 3
        Me.chkBloquearCampoCosto.Text = "Bloquear campo de costo"
        Me.chkBloquearCampoCosto.UseVisualStyleBackColor = True
        '
        'chkBloquearFecha
        '
        Me.chkBloquearFecha.AutoSize = True
        Me.chkBloquearFecha.Location = New System.Drawing.Point(16, 12)
        Me.chkBloquearFecha.Name = "chkBloquearFecha"
        Me.chkBloquearFecha.Size = New System.Drawing.Size(98, 17)
        Me.chkBloquearFecha.TabIndex = 0
        Me.chkBloquearFecha.Text = "Bloquear fecha"
        Me.chkBloquearFecha.UseVisualStyleBackColor = True
        '
        'chkImprimirDocumento
        '
        Me.chkImprimirDocumento.AutoSize = True
        Me.chkImprimirDocumento.Location = New System.Drawing.Point(16, 58)
        Me.chkImprimirDocumento.Name = "chkImprimirDocumento"
        Me.chkImprimirDocumento.Size = New System.Drawing.Size(201, 17)
        Me.chkImprimirDocumento.TabIndex = 2
        Me.chkImprimirDocumento.Text = "Imprimir documento automaticamente"
        Me.chkImprimirDocumento.UseVisualStyleBackColor = True
        '
        'chkExigirAutorizacion
        '
        Me.chkExigirAutorizacion.AutoSize = True
        Me.chkExigirAutorizacion.Location = New System.Drawing.Point(16, 35)
        Me.chkExigirAutorizacion.Name = "chkExigirAutorizacion"
        Me.chkExigirAutorizacion.Size = New System.Drawing.Size(162, 17)
        Me.chkExigirAutorizacion.TabIndex = 1
        Me.chkExigirAutorizacion.Text = "Exigir campo de Autorizacion"
        Me.chkExigirAutorizacion.UseVisualStyleBackColor = True
        '
        'rdbPromedio
        '
        Me.rdbPromedio.AutoSize = True
        Me.rdbPromedio.Location = New System.Drawing.Point(120, 154)
        Me.rdbPromedio.Name = "rdbPromedio"
        Me.rdbPromedio.Size = New System.Drawing.Size(69, 17)
        Me.rdbPromedio.TabIndex = 9
        Me.rdbPromedio.TabStop = True
        Me.rdbPromedio.Text = "Promedio"
        Me.rdbPromedio.UseVisualStyleBackColor = True
        '
        'rdbUltimoCosto
        '
        Me.rdbUltimoCosto.AutoSize = True
        Me.rdbUltimoCosto.Location = New System.Drawing.Point(31, 154)
        Me.rdbUltimoCosto.Name = "rdbUltimoCosto"
        Me.rdbUltimoCosto.Size = New System.Drawing.Size(83, 17)
        Me.rdbUltimoCosto.TabIndex = 8
        Me.rdbUltimoCosto.TabStop = True
        Me.rdbUltimoCosto.Text = "Ultimo costo"
        Me.rdbUltimoCosto.UseVisualStyleBackColor = True
        '
        'lblSeleccionCosto
        '
        Me.lblSeleccionCosto.AutoSize = True
        Me.lblSeleccionCosto.Location = New System.Drawing.Point(13, 138)
        Me.lblSeleccionCosto.Name = "lblSeleccionCosto"
        Me.lblSeleccionCosto.Size = New System.Drawing.Size(301, 13)
        Me.lblSeleccionCosto.TabIndex = 5
        Me.lblSeleccionCosto.Text = "- Seleccione el tipo de costo que se utilizara en la transaccion:"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(400, 300)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 264)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(394, 33)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(316, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(235, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkBloquearFecha)
        Me.Panel1.Controls.Add(Me.lblDiasAntiguedad)
        Me.Panel1.Controls.Add(Me.lblSeleccionCosto)
        Me.Panel1.Controls.Add(Me.nudDiasAnulacion)
        Me.Panel1.Controls.Add(Me.rdbUltimoCosto)
        Me.Panel1.Controls.Add(Me.chkBloquearAnulacion)
        Me.Panel1.Controls.Add(Me.rdbPromedio)
        Me.Panel1.Controls.Add(Me.chkHabilitarCostoSiEsCero)
        Me.Panel1.Controls.Add(Me.chkExigirAutorizacion)
        Me.Panel1.Controls.Add(Me.chkBloquearCampoCosto)
        Me.Panel1.Controls.Add(Me.chkImprimirDocumento)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(394, 255)
        Me.Panel1.TabIndex = 13
        '
        'ocxConfiguracionMovimiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxConfiguracionMovimiento"
        Me.Size = New System.Drawing.Size(400, 300)
        CType(Me.nudDiasAnulacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkHabilitarCostoSiEsCero As System.Windows.Forms.CheckBox
    Friend WithEvents chkBloquearCampoCosto As System.Windows.Forms.CheckBox
    Friend WithEvents chkBloquearFecha As System.Windows.Forms.CheckBox
    Friend WithEvents chkImprimirDocumento As System.Windows.Forms.CheckBox
    Friend WithEvents chkExigirAutorizacion As System.Windows.Forms.CheckBox
    Friend WithEvents rdbPromedio As System.Windows.Forms.RadioButton
    Friend WithEvents rdbUltimoCosto As System.Windows.Forms.RadioButton
    Friend WithEvents lblSeleccionCosto As System.Windows.Forms.Label
    Friend WithEvents lblDiasAntiguedad As System.Windows.Forms.Label
    Friend WithEvents nudDiasAnulacion As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkBloquearAnulacion As System.Windows.Forms.CheckBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel

End Class

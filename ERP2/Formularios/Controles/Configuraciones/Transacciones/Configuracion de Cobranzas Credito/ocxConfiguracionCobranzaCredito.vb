﻿Public Class ocxConfiguracionCobranzaCredito

    'CLASES
    Dim CSistema As New CSistema

    'FUNCIONES
    Public Sub ObtenerInformacion()

        Try
            Dim oRow As DataRow = CSistema.ExecuteToDataTable("Select Top(1) * From Configuraciones")(0)

            chkBloquearFecha.Checked = CSistema.RetornarValorBoolean(oRow("CobranzaCreditoBloquearFecha").ToString)
            chkImprimirDocumento.Checked = CSistema.RetornarValorBoolean(oRow("CobranzaCreditoImprimirDocumento").ToString)
            chkBloquearAnulacion.Checked = CSistema.RetornarValorBoolean(oRow("CobranzaCreditoBloquearAnulacion").ToString)
            Dim Dias As Integer = CSistema.RetornarValorInteger(oRow("CobranzaCreditoDiasBloqueo").ToString)
            nudDiasAnulacion.Value = Dias

        Catch ex As Exception

        End Try


    End Sub

    Public Sub Guardar()

        Dim CostoProducto As String = ""

        Dim sql As String

        sql = "Update Configuraciones Set CobranzaCreditoBloquearFecha = '" & chkBloquearFecha.Checked.ToString & "', CobranzaCreditoImprimirDocumento = '" & chkImprimirDocumento.Checked.ToString & "', CobranzaCreditoBloquearAnulacion = '" & chkBloquearAnulacion.Checked.ToString & "', CobranzaCreditoDiasBloqueo = '" & nudDiasAnulacion.Value & "'"

        If CSistema.ExecuteNonQuery(sql) > 0 Then
            MessageBox.Show("Registro procesado!", "Cobranza Credito", MessageBoxButtons.OK, MessageBoxIcon.Information)
            FGConfiguraciones()
            ObtenerInformacion()
        Else
            MessageBox.Show("Probablemente no se produjeron los cambios! Intente nuevamente", "Cobranza Credito", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxConfiguracionCobranzaCredito
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkBloquearFecha = New System.Windows.Forms.CheckBox()
        Me.lblDiasAntiguedad = New System.Windows.Forms.Label()
        Me.chkImprimirDocumento = New System.Windows.Forms.CheckBox()
        Me.nudDiasAnulacion = New System.Windows.Forms.NumericUpDown()
        Me.chkBloquearAnulacion = New System.Windows.Forms.CheckBox()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.nudDiasAnulacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(235, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(316, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 268)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(394, 29)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(400, 300)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkBloquearFecha)
        Me.Panel1.Controls.Add(Me.lblDiasAntiguedad)
        Me.Panel1.Controls.Add(Me.chkImprimirDocumento)
        Me.Panel1.Controls.Add(Me.nudDiasAnulacion)
        Me.Panel1.Controls.Add(Me.chkBloquearAnulacion)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(394, 259)
        Me.Panel1.TabIndex = 2
        '
        'chkBloquearFecha
        '
        Me.chkBloquearFecha.AutoSize = True
        Me.chkBloquearFecha.Location = New System.Drawing.Point(13, 12)
        Me.chkBloquearFecha.Name = "chkBloquearFecha"
        Me.chkBloquearFecha.Size = New System.Drawing.Size(98, 17)
        Me.chkBloquearFecha.TabIndex = 5
        Me.chkBloquearFecha.Text = "Bloquear fecha"
        Me.chkBloquearFecha.UseVisualStyleBackColor = True
        '
        'lblDiasAntiguedad
        '
        Me.lblDiasAntiguedad.AutoSize = True
        Me.lblDiasAntiguedad.Location = New System.Drawing.Point(264, 60)
        Me.lblDiasAntiguedad.Name = "lblDiasAntiguedad"
        Me.lblDiasAntiguedad.Size = New System.Drawing.Size(97, 13)
        Me.lblDiasAntiguedad.TabIndex = 9
        Me.lblDiasAntiguedad.Text = "dias de antiguedad"
        '
        'chkImprimirDocumento
        '
        Me.chkImprimirDocumento.AutoSize = True
        Me.chkImprimirDocumento.Location = New System.Drawing.Point(13, 35)
        Me.chkImprimirDocumento.Name = "chkImprimirDocumento"
        Me.chkImprimirDocumento.Size = New System.Drawing.Size(201, 17)
        Me.chkImprimirDocumento.TabIndex = 6
        Me.chkImprimirDocumento.Text = "Imprimir documento automaticamente"
        Me.chkImprimirDocumento.UseVisualStyleBackColor = True
        '
        'nudDiasAnulacion
        '
        Me.nudDiasAnulacion.Location = New System.Drawing.Point(224, 56)
        Me.nudDiasAnulacion.Name = "nudDiasAnulacion"
        Me.nudDiasAnulacion.Size = New System.Drawing.Size(40, 20)
        Me.nudDiasAnulacion.TabIndex = 8
        '
        'chkBloquearAnulacion
        '
        Me.chkBloquearAnulacion.AutoSize = True
        Me.chkBloquearAnulacion.Location = New System.Drawing.Point(13, 58)
        Me.chkBloquearAnulacion.Name = "chkBloquearAnulacion"
        Me.chkBloquearAnulacion.Size = New System.Drawing.Size(211, 17)
        Me.chkBloquearAnulacion.TabIndex = 7
        Me.chkBloquearAnulacion.Text = "Bloquear anulacion de documentos de "
        Me.chkBloquearAnulacion.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'ocxConfiguracionCobranzaCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxConfiguracionCobranzaCredito"
        Me.Size = New System.Drawing.Size(400, 300)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.nudDiasAnulacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkBloquearFecha As System.Windows.Forms.CheckBox
    Friend WithEvents lblDiasAntiguedad As System.Windows.Forms.Label
    Friend WithEvents chkImprimirDocumento As System.Windows.Forms.CheckBox
    Friend WithEvents nudDiasAnulacion As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkBloquearAnulacion As System.Windows.Forms.CheckBox

End Class

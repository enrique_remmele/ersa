﻿Public Class ocxConfiguracionGasto

    'CLASES
    Dim CSistema As New CSistema

    'FUNCIONES
    Public Sub ObtenerInformacion()

        Try
            Dim oRow As DataRow = MVariablesGlobales.vgConfiguraciones

            chkBloquearEliminacion.Checked = CSistema.RetornarValorBoolean(oRow("GastoBloquearEliminacion").ToString)
            nudDiasEliminacion.Value = CSistema.RetornarValorInteger(oRow("GastoDiasBloqueo").ToString)
            chkSeleccionDeposito.Checked = CSistema.RetornarValorBoolean(oRow("GastoHabilitarDeposito").ToString)
            chkSeleccionDistribucion.Checked = CSistema.RetornarValorBoolean(oRow("GastoHabilitarDistribucion").ToString)

        Catch ex As Exception

        End Try


    End Sub

    Public Sub Guardar()

        Dim sql As String
        sql = "Update Configuraciones Set "
        sql = sql & "GastoBloquearEliminacion = '" & chkBloquearEliminacion.Checked.ToString & "'" & vbCrLf
        sql = sql & ", GastoDiasBloqueo = '" & nudDiasEliminacion.Value & "'" & vbCrLf
        sql = sql & ", GastoHabilitarDeposito = '" & chkSeleccionDeposito.Checked & "'" & vbCrLf
        sql = sql & ", GastoHabilitarDistribucion = '" & chkSeleccionDistribucion.Checked & "'" & vbCrLf

        If CSistema.ExecuteNonQuery(sql) > 0 Then
            MessageBox.Show("Registro procesado!", "Gasto", MessageBoxButtons.OK, MessageBoxIcon.Information)
            FGConfiguraciones()
            ObtenerInformacion()
        Else
            MessageBox.Show("Probablemente no se produjeron los cambios! Intente nuevamente", "Gasto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        ObtenerInformacion()
    End Sub

    Private Sub ocxConfiguracionGasto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ObtenerInformacion()
    End Sub

   
End Class

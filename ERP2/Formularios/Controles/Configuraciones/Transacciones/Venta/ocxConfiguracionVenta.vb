﻿Public Class ocxConfiguracionVenta

    'CLASES
    Dim CSistema As New CSistema

    'FUNCIONES
    Public Sub ObtenerInformacion()

        Try
            Dim oRow As DataRow = CSistema.ExecuteToDataTable("Select Top(1) * From Configuraciones Where IDSucursal=" & vgIDSucursal)(0)

            chkBloquearFecha.Checked = CSistema.RetornarValorBoolean(oRow("VentaBloquearFecha").ToString)
            chkImprimirDocumento.Checked = CSistema.RetornarValorBoolean(oRow("VentaImprimirDocumento").ToString)
            chkBloquearAnulacion.Checked = CSistema.RetornarValorBoolean(oRow("VentaBloquearAnulacion").ToString)

            chkModificarCantidad.Checked = CSistema.RetornarValorBoolean(oRow("VentaModificarCantidad").ToString)
            chkModificarDescuento.Checked = CSistema.RetornarValorBoolean(oRow("VentaModificarDescuento").ToString)
            chkPedirCredencialAgregarDescuento.Checked = CSistema.RetornarValorBoolean(oRow("VentaPedirCredencialAgregarDescuento").ToString)

            chkControlarDescuentoTactico.Checked = CSistema.RetornarValorBoolean(oRow("VentaControlarDescuentoTactico").ToString)
            chkControlarPorImporte.Checked = CSistema.RetornarValorBoolean(oRow("VentaControlarImporte").ToString)
            chkControlarPorPorcentaje.Checked = CSistema.RetornarValorBoolean(oRow("VentaControlarPorcentaje").ToString)
            chkSuperarTacticoConCredencial.Checked = CSistema.RetornarValorBoolean(oRow("VentaSuperarTacticoConCredencial").ToString)

            Dim Dias As Integer = CSistema.RetornarValorInteger(oRow("VentaDiasBloqueo").ToString)
            nudDiasAnulacion.Value = Dias

            chkModificarPrecio.Checked = CSistema.RetornarValorBoolean(oRow("VentaModificarPrecio").ToString)
            chkControlMargen.Checked = CSistema.RetornarValorBoolean(oRow("VentaControlarBajoElMargen").ToString)
            chkControlBajoCosto.Checked = CSistema.RetornarValorBoolean(oRow("VentaControlarBajoElCosto").ToString)

        Catch ex As Exception

        End Try


    End Sub

    Public Sub Guardar()

        Dim CostoProducto As String = ""

        Dim sql As String

        sql = "Update Configuraciones Set "
        sql = sql & "VentaBloquearFecha = '" & chkBloquearFecha.Checked.ToString & "', "
        sql = sql & "VentaImprimirDocumento = '" & chkImprimirDocumento.Checked.ToString & "', "
        sql = sql & "VentaBloquearAnulacion = '" & chkBloquearAnulacion.Checked.ToString & "', "
        sql = sql & "VentaDiasBloqueo = '" & nudDiasAnulacion.Value & "', "

        sql = sql & "VentaModificarDescuento = '" & chkModificarDescuento.Checked & "', "
        sql = sql & "VentaModificarCantidad = '" & chkModificarCantidad.Checked & "', "
        sql = sql & "VentaPedirCredencialAgregarDescuento = '" & chkPedirCredencialAgregarDescuento.Checked & "', "
        sql = sql & "VentaControlarDescuentoTactico = '" & chkControlarDescuentoTactico.Checked & "', "
        sql = sql & "VentaControlarImporte = '" & chkControlarPorImporte.Checked & "', "
        sql = sql & "VentaControlarPorcentaje = '" & chkControlarPorPorcentaje.Checked & "', "
        sql = sql & "VentaSuperarTacticoConCredencial = '" & chkSuperarTacticoConCredencial.Checked & "', "

        sql = sql & "VentaModificarPrecio = '" & chkModificarPrecio.Checked & "', "
        sql = sql & "VentaControlarBajoElMargen = '" & chkControlMargen.Checked & "', "
        sql = sql & "VentaControlarBajoElCosto = '" & chkControlBajoCosto.Checked & "' "

        sql = sql & "Where IDSucursal = " & vgIDSucursal & " "

        If CSistema.ExecuteNonQuery(sql) > 0 Then
            MessageBox.Show("Registro procesado!", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information)
            FGConfiguraciones()
            ObtenerInformacion()
        Else
            MessageBox.Show("Probablemente no se produjeron los cambios! Intente nuevamente", "Movimiento", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        ObtenerInformacion()
    End Sub

    Private Sub chkModificarPrecio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkModificarPrecio.CheckedChanged

        chkControlBajoCosto.Enabled = chkModificarPrecio.Checked
        chkControlMargen.Enabled = chkModificarPrecio.Checked

        If chkModificarPrecio.Checked = False Then
            chkControlBajoCosto.Checked = False
            chkControlMargen.Checked = False
        Else
            chkControlBajoCosto.Checked = True
            chkControlMargen.Checked = False
        End If

    End Sub

    Private Sub chkControlarDescuentoTactico_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkControlarDescuentoTactico.CheckedChanged

        chkSuperarTacticoConCredencial.Enabled = chkControlarDescuentoTactico.Checked
        chkControlarPorImporte.Enabled = chkControlarDescuentoTactico.Checked
        chkControlarPorPorcentaje.Enabled = chkControlarDescuentoTactico.Checked

        If chkControlarDescuentoTactico.Checked = False Then
            chkSuperarTacticoConCredencial.Checked = False
            chkControlarPorImporte.Checked = False
            chkControlarPorPorcentaje.Checked = False
        Else
            chkSuperarTacticoConCredencial.Checked = True
            chkControlarPorImporte.Checked = True
            chkControlarPorPorcentaje.Checked = True
        End If

    End Sub
End Class

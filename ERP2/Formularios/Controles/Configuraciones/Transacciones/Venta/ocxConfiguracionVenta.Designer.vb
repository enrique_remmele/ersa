﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxConfiguracionVenta
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chkControlBajoCosto = New System.Windows.Forms.CheckBox()
        Me.chkControlMargen = New System.Windows.Forms.CheckBox()
        Me.chkModificarPrecio = New System.Windows.Forms.CheckBox()
        Me.chkSuperarTacticoConCredencial = New System.Windows.Forms.CheckBox()
        Me.chkPedirCredencialAgregarDescuento = New System.Windows.Forms.CheckBox()
        Me.chkModificarCantidad = New System.Windows.Forms.CheckBox()
        Me.lblDetalle = New System.Windows.Forms.Label()
        Me.chkModificarDescuento = New System.Windows.Forms.CheckBox()
        Me.lblDiasAntiguedad = New System.Windows.Forms.Label()
        Me.nudDiasAnulacion = New System.Windows.Forms.NumericUpDown()
        Me.chkBloquearAnulacion = New System.Windows.Forms.CheckBox()
        Me.chkBloquearFecha = New System.Windows.Forms.CheckBox()
        Me.chkImprimirDocumento = New System.Windows.Forms.CheckBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkControlarPorPorcentaje = New System.Windows.Forms.CheckBox()
        Me.chkControlarPorImporte = New System.Windows.Forms.CheckBox()
        Me.chkControlarDescuentoTactico = New System.Windows.Forms.CheckBox()
        CType(Me.nudDiasAnulacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkControlBajoCosto
        '
        Me.chkControlBajoCosto.AutoSize = True
        Me.chkControlBajoCosto.Enabled = False
        Me.chkControlBajoCosto.Location = New System.Drawing.Point(34, 190)
        Me.chkControlBajoCosto.Name = "chkControlBajoCosto"
        Me.chkControlBajoCosto.Size = New System.Drawing.Size(214, 17)
        Me.chkControlBajoCosto.TabIndex = 9
        Me.chkControlBajoCosto.Text = "Controlar que no se venda bajo el costo"
        Me.chkControlBajoCosto.UseVisualStyleBackColor = True
        '
        'chkControlMargen
        '
        Me.chkControlMargen.AutoSize = True
        Me.chkControlMargen.Enabled = False
        Me.chkControlMargen.Location = New System.Drawing.Point(34, 212)
        Me.chkControlMargen.Name = "chkControlMargen"
        Me.chkControlMargen.Size = New System.Drawing.Size(169, 17)
        Me.chkControlMargen.TabIndex = 10
        Me.chkControlMargen.Text = "Control de margen establecido"
        Me.chkControlMargen.UseVisualStyleBackColor = True
        '
        'chkModificarPrecio
        '
        Me.chkModificarPrecio.AutoSize = True
        Me.chkModificarPrecio.Location = New System.Drawing.Point(8, 167)
        Me.chkModificarPrecio.Name = "chkModificarPrecio"
        Me.chkModificarPrecio.Size = New System.Drawing.Size(137, 17)
        Me.chkModificarPrecio.TabIndex = 8
        Me.chkModificarPrecio.Text = "Permitir modificar precio"
        Me.chkModificarPrecio.UseVisualStyleBackColor = True
        '
        'chkSuperarTacticoConCredencial
        '
        Me.chkSuperarTacticoConCredencial.AutoSize = True
        Me.chkSuperarTacticoConCredencial.Location = New System.Drawing.Point(34, 305)
        Me.chkSuperarTacticoConCredencial.Name = "chkSuperarTacticoConCredencial"
        Me.chkSuperarTacticoConCredencial.Size = New System.Drawing.Size(195, 17)
        Me.chkSuperarTacticoConCredencial.TabIndex = 15
        Me.chkSuperarTacticoConCredencial.Text = "Se puede superar con Credenciales"
        Me.chkSuperarTacticoConCredencial.UseVisualStyleBackColor = True
        '
        'chkPedirCredencialAgregarDescuento
        '
        Me.chkPedirCredencialAgregarDescuento.AutoSize = True
        Me.chkPedirCredencialAgregarDescuento.Location = New System.Drawing.Point(8, 236)
        Me.chkPedirCredencialAgregarDescuento.Name = "chkPedirCredencialAgregarDescuento"
        Me.chkPedirCredencialAgregarDescuento.Size = New System.Drawing.Size(291, 17)
        Me.chkPedirCredencialAgregarDescuento.TabIndex = 11
        Me.chkPedirCredencialAgregarDescuento.Text = "Pedir credenciales para asignar descuentos a productos"
        Me.chkPedirCredencialAgregarDescuento.UseVisualStyleBackColor = True
        '
        'chkModificarCantidad
        '
        Me.chkModificarCantidad.AutoSize = True
        Me.chkModificarCantidad.Location = New System.Drawing.Point(8, 144)
        Me.chkModificarCantidad.Name = "chkModificarCantidad"
        Me.chkModificarCantidad.Size = New System.Drawing.Size(149, 17)
        Me.chkModificarCantidad.TabIndex = 7
        Me.chkModificarCantidad.Text = "Permitir modificar cantidad"
        Me.chkModificarCantidad.UseVisualStyleBackColor = True
        '
        'lblDetalle
        '
        Me.lblDetalle.AutoSize = True
        Me.lblDetalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetalle.Location = New System.Drawing.Point(8, 102)
        Me.lblDetalle.Name = "lblDetalle"
        Me.lblDetalle.Size = New System.Drawing.Size(51, 13)
        Me.lblDetalle.TabIndex = 5
        Me.lblDetalle.Text = "Detalle:"
        '
        'chkModificarDescuento
        '
        Me.chkModificarDescuento.AutoSize = True
        Me.chkModificarDescuento.Location = New System.Drawing.Point(8, 121)
        Me.chkModificarDescuento.Name = "chkModificarDescuento"
        Me.chkModificarDescuento.Size = New System.Drawing.Size(163, 17)
        Me.chkModificarDescuento.TabIndex = 6
        Me.chkModificarDescuento.Text = "Permitir modificar descuentos"
        Me.chkModificarDescuento.UseVisualStyleBackColor = True
        '
        'lblDiasAntiguedad
        '
        Me.lblDiasAntiguedad.AutoSize = True
        Me.lblDiasAntiguedad.Location = New System.Drawing.Point(262, 61)
        Me.lblDiasAntiguedad.Name = "lblDiasAntiguedad"
        Me.lblDiasAntiguedad.Size = New System.Drawing.Size(97, 13)
        Me.lblDiasAntiguedad.TabIndex = 4
        Me.lblDiasAntiguedad.Text = "dias de antiguedad"
        '
        'nudDiasAnulacion
        '
        Me.nudDiasAnulacion.Location = New System.Drawing.Point(222, 57)
        Me.nudDiasAnulacion.Name = "nudDiasAnulacion"
        Me.nudDiasAnulacion.Size = New System.Drawing.Size(40, 20)
        Me.nudDiasAnulacion.TabIndex = 3
        '
        'chkBloquearAnulacion
        '
        Me.chkBloquearAnulacion.AutoSize = True
        Me.chkBloquearAnulacion.Location = New System.Drawing.Point(11, 59)
        Me.chkBloquearAnulacion.Name = "chkBloquearAnulacion"
        Me.chkBloquearAnulacion.Size = New System.Drawing.Size(211, 17)
        Me.chkBloquearAnulacion.TabIndex = 2
        Me.chkBloquearAnulacion.Text = "Bloquear anulacion de documentos de "
        Me.chkBloquearAnulacion.UseVisualStyleBackColor = True
        '
        'chkBloquearFecha
        '
        Me.chkBloquearFecha.AutoSize = True
        Me.chkBloquearFecha.Location = New System.Drawing.Point(11, 13)
        Me.chkBloquearFecha.Name = "chkBloquearFecha"
        Me.chkBloquearFecha.Size = New System.Drawing.Size(98, 17)
        Me.chkBloquearFecha.TabIndex = 0
        Me.chkBloquearFecha.Text = "Bloquear fecha"
        Me.chkBloquearFecha.UseVisualStyleBackColor = True
        '
        'chkImprimirDocumento
        '
        Me.chkImprimirDocumento.AutoSize = True
        Me.chkImprimirDocumento.Location = New System.Drawing.Point(11, 36)
        Me.chkImprimirDocumento.Name = "chkImprimirDocumento"
        Me.chkImprimirDocumento.Size = New System.Drawing.Size(201, 17)
        Me.chkImprimirDocumento.TabIndex = 1
        Me.chkImprimirDocumento.Text = "Imprimir documento automaticamente"
        Me.chkImprimirDocumento.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 347)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(394, 29)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(316, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(235, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(400, 379)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkControlarDescuentoTactico)
        Me.Panel1.Controls.Add(Me.chkControlarPorImporte)
        Me.Panel1.Controls.Add(Me.chkControlarPorPorcentaje)
        Me.Panel1.Controls.Add(Me.chkBloquearFecha)
        Me.Panel1.Controls.Add(Me.chkControlBajoCosto)
        Me.Panel1.Controls.Add(Me.chkImprimirDocumento)
        Me.Panel1.Controls.Add(Me.chkControlMargen)
        Me.Panel1.Controls.Add(Me.chkBloquearAnulacion)
        Me.Panel1.Controls.Add(Me.chkModificarPrecio)
        Me.Panel1.Controls.Add(Me.nudDiasAnulacion)
        Me.Panel1.Controls.Add(Me.chkSuperarTacticoConCredencial)
        Me.Panel1.Controls.Add(Me.lblDiasAntiguedad)
        Me.Panel1.Controls.Add(Me.chkPedirCredencialAgregarDescuento)
        Me.Panel1.Controls.Add(Me.chkModificarDescuento)
        Me.Panel1.Controls.Add(Me.chkModificarCantidad)
        Me.Panel1.Controls.Add(Me.lblDetalle)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(394, 338)
        Me.Panel1.TabIndex = 0
        '
        'chkControlarPorPorcentaje
        '
        Me.chkControlarPorPorcentaje.AutoSize = True
        Me.chkControlarPorPorcentaje.Enabled = False
        Me.chkControlarPorPorcentaje.Location = New System.Drawing.Point(34, 282)
        Me.chkControlarPorPorcentaje.Name = "chkControlarPorPorcentaje"
        Me.chkControlarPorPorcentaje.Size = New System.Drawing.Size(77, 17)
        Me.chkControlarPorPorcentaje.TabIndex = 13
        Me.chkControlarPorPorcentaje.Text = "Porcentaje"
        Me.chkControlarPorPorcentaje.UseVisualStyleBackColor = True
        '
        'chkControlarPorImporte
        '
        Me.chkControlarPorImporte.AutoSize = True
        Me.chkControlarPorImporte.Enabled = False
        Me.chkControlarPorImporte.Location = New System.Drawing.Point(111, 282)
        Me.chkControlarPorImporte.Name = "chkControlarPorImporte"
        Me.chkControlarPorImporte.Size = New System.Drawing.Size(61, 17)
        Me.chkControlarPorImporte.TabIndex = 14
        Me.chkControlarPorImporte.Text = "Importe"
        Me.chkControlarPorImporte.UseVisualStyleBackColor = True
        '
        'chkControlarDescuentoTactico
        '
        Me.chkControlarDescuentoTactico.AutoSize = True
        Me.chkControlarDescuentoTactico.Location = New System.Drawing.Point(8, 259)
        Me.chkControlarDescuentoTactico.Name = "chkControlarDescuentoTactico"
        Me.chkControlarDescuentoTactico.Size = New System.Drawing.Size(175, 17)
        Me.chkControlarDescuentoTactico.TabIndex = 12
        Me.chkControlarDescuentoTactico.Text = "Controlar Descuentos Tacticos:"
        Me.chkControlarDescuentoTactico.UseVisualStyleBackColor = True
        '
        'ocxConfiguracionVenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxConfiguracionVenta"
        Me.Size = New System.Drawing.Size(400, 379)
        CType(Me.nudDiasAnulacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblDiasAntiguedad As System.Windows.Forms.Label
    Friend WithEvents nudDiasAnulacion As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkBloquearAnulacion As System.Windows.Forms.CheckBox
    Friend WithEvents chkBloquearFecha As System.Windows.Forms.CheckBox
    Friend WithEvents chkImprimirDocumento As System.Windows.Forms.CheckBox
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents chkSuperarTacticoConCredencial As System.Windows.Forms.CheckBox
    Friend WithEvents chkPedirCredencialAgregarDescuento As System.Windows.Forms.CheckBox
    Friend WithEvents chkModificarCantidad As System.Windows.Forms.CheckBox
    Friend WithEvents lblDetalle As System.Windows.Forms.Label
    Friend WithEvents chkModificarDescuento As System.Windows.Forms.CheckBox
    Friend WithEvents chkModificarPrecio As System.Windows.Forms.CheckBox
    Friend WithEvents chkControlBajoCosto As System.Windows.Forms.CheckBox
    Friend WithEvents chkControlMargen As System.Windows.Forms.CheckBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkControlarPorImporte As System.Windows.Forms.CheckBox
    Friend WithEvents chkControlarPorPorcentaje As System.Windows.Forms.CheckBox
    Friend WithEvents chkControlarDescuentoTactico As System.Windows.Forms.CheckBox

End Class

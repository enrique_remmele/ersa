﻿Public Class ocxConfiguracionesPedido

    'CLASES
    Dim CSistema As New CSistema

    'FUNCIONES
    Public Sub ObtenerInformacion()

        Try
            Dim oRow As DataRow = CSistema.ExecuteToDataTable("Select Top(1) * From Configuraciones Where IDSucursal=" & vgIDSucursal)(0)

            chkBloquearTactico.Checked = CSistema.RetornarValorBoolean(oRow("PedidoBloquearTactico").ToString)

            chkControlarDescuentoTactico.Checked = CSistema.RetornarValorBoolean(oRow("PedidoControlarTactico").ToString)
            chkControlarTacticoPorcentaje.Checked = CSistema.RetornarValorBoolean(oRow("PedidoControlarTacticoPorcentaje").ToString)
            chkControlarTacticoImporte.Checked = CSistema.RetornarValorBoolean(oRow("PedidoControlarTacticoImporte").ToString)

            nudCantidadPedido.Value = CSistema.RetornarValorInteger(oRow("PedidoCantidadProducto").ToString)

        Catch ex As Exception

        End Try


    End Sub

    Public Sub Guardar()

        Dim CostoProducto As String = ""

        Dim sql As String

        sql = "Update Configuraciones Set "
        sql = sql & "PedidoBloquearTactico = '" & chkBloquearTactico.Checked.ToString & "', "
        sql = sql & "PedidoControlarTactico = '" & chkControlarDescuentoTactico.Checked.ToString & "', "
        sql = sql & "PedidoControlarTacticoPorcentaje = '" & chkControlarTacticoPorcentaje.Checked.ToString & "', "
        sql = sql & "PedidoControlarTacticoImporte = '" & chkControlarTacticoImporte.Checked.ToString & "', "

        sql = sql & "PedidoCantidadProducto = '" & nudCantidadPedido.Value & "' "
        sql = sql & "Where IDSucursal = " & vgIDSucursal & " "

        If CSistema.ExecuteNonQuery(sql) > 0 Then
            MessageBox.Show("Registro procesado!", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information)
            FGConfiguraciones()
            ObtenerInformacion()
        Else
            MessageBox.Show("Probablemente no se produjeron los cambios! Intente nuevamente", "Movimiento", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        ObtenerInformacion()
    End Sub

    Private Sub chkControlarDescuentoTactico_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkControlarDescuentoTactico.CheckedChanged

        chkControlarTacticoImporte.Enabled = chkControlarDescuentoTactico.Checked
        chkControlarTacticoPorcentaje.Enabled = chkControlarDescuentoTactico.Checked

        If chkControlarDescuentoTactico.Checked = True Then
            chkControlarTacticoImporte.Checked = True
            chkControlarTacticoPorcentaje.Checked = True
        Else
            chkControlarTacticoImporte.Checked = False
            chkControlarTacticoPorcentaje.Checked = False
        End If
    End Sub

End Class

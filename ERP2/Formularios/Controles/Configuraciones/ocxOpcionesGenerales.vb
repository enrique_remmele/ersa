﻿Public Class ocxOpcionesGenerales

    'CLASES
    Dim CSistema As New CSistema

    'FUNCIONES
    Public Sub ObtenerInformacion()

        Try
            Dim oRow As DataRow = CSistema.ExecuteToDataTable("Select Top(1) * From Configuraciones Where IDSucursal=" & vgIDSucursal)(0)

            txtFormatoFecha.SetValue(CSistema.RetornarValorString(oRow("FormatoFecha").ToString))
            txtPropietarioBD.SetValue(CSistema.RetornarValorString(oRow("PropietarioBD").ToString))
            chkBloquearDocumentos.Checked = CSistema.RetornarValorBoolean(oRow("BloquearDocumentos").ToString)
            nudDias.Value = CSistema.RetornarValorInteger(oRow("DiasBloqueo").ToString)

        Catch ex As Exception

        End Try


    End Sub

    Public Sub Guardar()

        Dim CostoProducto As String = ""

        Dim sql As String

        sql = "Update Configuraciones Set "
        sql = sql & "FormatoFecha = '" & txtFormatoFecha.GetValue & "', "
        sql = sql & "PropietarioBD = '" & txtPropietarioBD.GetValue & "', "
        sql = sql & "BloquearDocumentos = '" & chkBloquearDocumentos.Checked.ToString & "', "
        sql = sql & "DiasBloqueo = '" & nudDias.Value & "' "

        sql = sql & "Where IDSucursal = " & vgIDSucursal & " "

        If CSistema.ExecuteNonQuery(sql) > 0 Then
            FGConfiguraciones()
            ObtenerInformacion()
        Else
            MessageBox.Show("Probablemente no se produjeron los cambios! Intente nuevamente", "Movimiento", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        ObtenerInformacion()
    End Sub

End Class

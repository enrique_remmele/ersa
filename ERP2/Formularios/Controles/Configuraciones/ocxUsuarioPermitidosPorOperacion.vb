﻿Public Class ocxUsuarioPermitidosPorOperacion

    'CLASES
    Dim CSistema As New CSistema

    'FUNCIONES
    Sub Inicializar()

        ListarUsuario()
        ListarOperaciones()

    End Sub

    Sub ListarUsuario()

        Dim Sql As String = "Select U.ID, U.Usuario From Usuario U Join UsuarioPermitido UP On U.ID=UP.IDUsuario Order By 2"
        CSistema.SqlToDataGrid(dgvUsuario, Sql)

        'Formato
        dgvUsuario.Columns("Usuario").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub ListarOperaciones()

        Dim Sql As String = "Select ID, Descripcion From Operacion Order By 2"
        CSistema.SqlToDataGrid(dgvUsuarioPermitido, Sql)

        'Formato
        dgvUsuarioPermitido.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub ListarUsuarioOperacion()

        If dgvUsuario.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select IDOperacion From UsuarioOperacion Where IDUsuario=" & dgvUsuario.SelectedRows(0).Cells("ID").Value)

        For Each oRow As DataGridViewRow In dgvUsuarioPermitido.Rows
            Dim IDOperacion As Integer = oRow.Cells("ID").Value
            If dt.Select("IDOperacion=" & IDOperacion).Count > 0 Then
                oRow.DefaultCellStyle.BackColor = Color.GreenYellow
            Else
                oRow.DefaultCellStyle.BackColor = Color.White
            End If
        Next

    End Sub

    Sub AgregarUsuario()

        'Validar
        For Each oRow As DataRow In dgvUsuario.DataSource.rows
            If oRow("ID").ToString = cbxUsuario.GetValue.ToString Then
                MessageBox.Show("El usuario ya existe en la lista!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        Next

        Dim SQL As String = "Insert Into UsuarioPermitido(IDUsuario) values(" & cbxUsuario.GetValue & ")"
        If CSistema.ExecuteNonQuery(SQL) = 0 Then
            Exit Sub
        End If

        ListarUsuario()

    End Sub

    Sub EliminarUsuario()

        'Validar
        If dgvUsuario.SelectedRows.Count = 0 Then
            MessageBox.Show("Seleccione un regitro!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim Existe As Boolean = False
        For Each oRow As DataRow In dgvUsuario.DataSource.rows
            If oRow("ID").ToString = dgvUsuario.SelectedRows(0).Cells("ID").Value Then
                Existe = True
                Exit For
            End If
        Next

        If Existe = False Then
            MessageBox.Show("El usuario no existe!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim SQL As String = "Delete From UsuarioPermitido Where IDUsuario=" & dgvUsuario.SelectedRows(0).Cells("ID").Value & ""
        If CSistema.ExecuteNonQuery(SQL) = 0 Then
            Exit Sub
        End If

        ListarUsuario()

    End Sub

    Sub Habilitar()

        'Validar
        If dgvUsuario.SelectedRows.Count = 0 Then
            MessageBox.Show("Seleccione un usuario!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If dgvUsuarioPermitido.SelectedRows.Count = 0 Then
            MessageBox.Show("Seleccione una operacion!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim IDUsuario As Integer = dgvUsuario.SelectedRows(0).Cells("ID").Value
        Dim IDOperacion As Integer = dgvUsuarioPermitido.SelectedRows(0).Cells("ID").Value

        Dim SQL As String = "Insert Into UsuarioOperacion(IDUsuario, IDOperacion) values(" & IDUsuario & ", " & IDOperacion & ")"
        If CSistema.ExecuteNonQuery(SQL) = 0 Then
            Exit Sub
        End If

        ListarUsuarioOperacion()

    End Sub

    Sub Denegar()

        'Validar
        If dgvUsuario.SelectedRows.Count = 0 Then
            MessageBox.Show("Seleccione un usuario!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If dgvUsuarioPermitido.SelectedRows.Count = 0 Then
            MessageBox.Show("Seleccione una operacion!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim IDUsuario As Integer = dgvUsuario.SelectedRows(0).Cells("ID").Value
        Dim IDOperacion As Integer = dgvUsuarioPermitido.SelectedRows(0).Cells("ID").Value

        Dim SQL As String = "Delete From UsuarioOperacion Where IDUsuario=" & IDUsuario & " And IDOperacion=" & IDOperacion & ""
        If CSistema.ExecuteNonQuery(SQL) = 0 Then
            Exit Sub
        End If

        ListarUsuarioOperacion()

    End Sub

    Private Sub btnAgregar_Click(sender As System.Object, e As System.EventArgs) Handles btnAgregar.Click
        AgregarUsuario()
    End Sub

    Private Sub btnQuitar_Click(sender As System.Object, e As System.EventArgs) Handles btnQuitar.Click
        EliminarUsuario()
    End Sub

    Private Sub btnHabilitar_Click(sender As System.Object, e As System.EventArgs) Handles btnHabilitar.Click
        Habilitar()
    End Sub

    Private Sub btnDenegar_Click(sender As System.Object, e As System.EventArgs) Handles btnDenegar.Click
        Denegar()
    End Sub

    Private Sub dgvUsuario_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles dgvUsuario.SelectionChanged
        ListarUsuarioOperacion()
    End Sub
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxClasificacionProducto
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtClasificacion7 = New ERP.ocxTXTString()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtClasificacion6 = New ERP.ocxTXTString()
        Me.txtClasificacion5 = New ERP.ocxTXTString()
        Me.txtClasificacion4 = New ERP.ocxTXTString()
        Me.txtClasificacion3 = New ERP.ocxTXTString()
        Me.txtClasificacion2 = New ERP.ocxTXTString()
        Me.txtClasificacion1 = New ERP.ocxTXTString()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblClasificacion1 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(330, 286)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtClasificacion7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtClasificacion6)
        Me.GroupBox1.Controls.Add(Me.txtClasificacion5)
        Me.GroupBox1.Controls.Add(Me.txtClasificacion4)
        Me.GroupBox1.Controls.Add(Me.txtClasificacion3)
        Me.GroupBox1.Controls.Add(Me.txtClasificacion2)
        Me.GroupBox1.Controls.Add(Me.txtClasificacion1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.lblClasificacion1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(324, 245)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(25, 215)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(84, 13)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "- Clasificacion 7:"
        '
        'txtClasificacion7
        '
        Me.txtClasificacion7.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtClasificacion7.Color = System.Drawing.Color.Empty
        Me.txtClasificacion7.Indicaciones = Nothing
        Me.txtClasificacion7.Location = New System.Drawing.Point(115, 211)
        Me.txtClasificacion7.Multilinea = False
        Me.txtClasificacion7.Name = "txtClasificacion7"
        Me.txtClasificacion7.Size = New System.Drawing.Size(197, 21)
        Me.txtClasificacion7.SoloLectura = False
        Me.txtClasificacion7.TabIndex = 13
        Me.txtClasificacion7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtClasificacion7.Texto = ""
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(25, 188)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(84, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "- Clasificacion 6:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(25, 161)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(84, 13)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "- Clasificacion 5:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(25, 134)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(84, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "- Clasificacion 4:"
        '
        'txtClasificacion6
        '
        Me.txtClasificacion6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtClasificacion6.Color = System.Drawing.Color.Empty
        Me.txtClasificacion6.Indicaciones = Nothing
        Me.txtClasificacion6.Location = New System.Drawing.Point(115, 184)
        Me.txtClasificacion6.Multilinea = False
        Me.txtClasificacion6.Name = "txtClasificacion6"
        Me.txtClasificacion6.Size = New System.Drawing.Size(197, 21)
        Me.txtClasificacion6.SoloLectura = False
        Me.txtClasificacion6.TabIndex = 9
        Me.txtClasificacion6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtClasificacion6.Texto = ""
        '
        'txtClasificacion5
        '
        Me.txtClasificacion5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtClasificacion5.Color = System.Drawing.Color.Empty
        Me.txtClasificacion5.Indicaciones = Nothing
        Me.txtClasificacion5.Location = New System.Drawing.Point(115, 157)
        Me.txtClasificacion5.Multilinea = False
        Me.txtClasificacion5.Name = "txtClasificacion5"
        Me.txtClasificacion5.Size = New System.Drawing.Size(197, 21)
        Me.txtClasificacion5.SoloLectura = False
        Me.txtClasificacion5.TabIndex = 8
        Me.txtClasificacion5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtClasificacion5.Texto = ""
        '
        'txtClasificacion4
        '
        Me.txtClasificacion4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtClasificacion4.Color = System.Drawing.Color.Empty
        Me.txtClasificacion4.Indicaciones = Nothing
        Me.txtClasificacion4.Location = New System.Drawing.Point(115, 130)
        Me.txtClasificacion4.Multilinea = False
        Me.txtClasificacion4.Name = "txtClasificacion4"
        Me.txtClasificacion4.Size = New System.Drawing.Size(197, 21)
        Me.txtClasificacion4.SoloLectura = False
        Me.txtClasificacion4.TabIndex = 7
        Me.txtClasificacion4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtClasificacion4.Texto = ""
        '
        'txtClasificacion3
        '
        Me.txtClasificacion3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtClasificacion3.Color = System.Drawing.Color.Empty
        Me.txtClasificacion3.Indicaciones = Nothing
        Me.txtClasificacion3.Location = New System.Drawing.Point(115, 103)
        Me.txtClasificacion3.Multilinea = False
        Me.txtClasificacion3.Name = "txtClasificacion3"
        Me.txtClasificacion3.Size = New System.Drawing.Size(197, 21)
        Me.txtClasificacion3.SoloLectura = False
        Me.txtClasificacion3.TabIndex = 6
        Me.txtClasificacion3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtClasificacion3.Texto = ""
        '
        'txtClasificacion2
        '
        Me.txtClasificacion2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtClasificacion2.Color = System.Drawing.Color.Empty
        Me.txtClasificacion2.Indicaciones = Nothing
        Me.txtClasificacion2.Location = New System.Drawing.Point(115, 76)
        Me.txtClasificacion2.Multilinea = False
        Me.txtClasificacion2.Name = "txtClasificacion2"
        Me.txtClasificacion2.Size = New System.Drawing.Size(197, 21)
        Me.txtClasificacion2.SoloLectura = False
        Me.txtClasificacion2.TabIndex = 5
        Me.txtClasificacion2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtClasificacion2.Texto = ""
        '
        'txtClasificacion1
        '
        Me.txtClasificacion1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtClasificacion1.Color = System.Drawing.Color.Empty
        Me.txtClasificacion1.Indicaciones = Nothing
        Me.txtClasificacion1.Location = New System.Drawing.Point(115, 49)
        Me.txtClasificacion1.Multilinea = False
        Me.txtClasificacion1.Name = "txtClasificacion1"
        Me.txtClasificacion1.Size = New System.Drawing.Size(197, 21)
        Me.txtClasificacion1.SoloLectura = False
        Me.txtClasificacion1.TabIndex = 4
        Me.txtClasificacion1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtClasificacion1.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(25, 107)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "- Clasificacion 3:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(25, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "- Clasificacion 2:"
        '
        'lblClasificacion1
        '
        Me.lblClasificacion1.AutoSize = True
        Me.lblClasificacion1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClasificacion1.Location = New System.Drawing.Point(25, 53)
        Me.lblClasificacion1.Name = "lblClasificacion1"
        Me.lblClasificacion1.Size = New System.Drawing.Size(84, 13)
        Me.lblClasificacion1.TabIndex = 1
        Me.lblClasificacion1.Text = "- Clasificacion 1:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(135, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Clasificacion de Productos:"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 254)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(324, 29)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(246, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(165, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 1
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'ColorDialog1
        '
        Me.ColorDialog1.AnyColor = True
        Me.ColorDialog1.FullOpen = True
        '
        'ocxClasificacionProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxClasificacionProducto"
        Me.Size = New System.Drawing.Size(330, 286)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblClasificacion1 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents ColorDialog1 As System.Windows.Forms.ColorDialog
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtClasificacion6 As ERP.ocxTXTString
    Friend WithEvents txtClasificacion5 As ERP.ocxTXTString
    Friend WithEvents txtClasificacion4 As ERP.ocxTXTString
    Friend WithEvents txtClasificacion3 As ERP.ocxTXTString
    Friend WithEvents txtClasificacion2 As ERP.ocxTXTString
    Friend WithEvents txtClasificacion1 As ERP.ocxTXTString
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtClasificacion7 As ERP.ocxTXTString

End Class

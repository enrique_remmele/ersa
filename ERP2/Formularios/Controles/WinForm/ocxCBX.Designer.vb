﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxCBX
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbx = New System.Windows.Forms.ComboBox()
        Me.txt = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'cbx
        '
        Me.cbx.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cbx.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbx.FormattingEnabled = True
        Me.cbx.Location = New System.Drawing.Point(0, 0)
        Me.cbx.Name = "cbx"
        Me.cbx.Size = New System.Drawing.Size(262, 21)
        Me.cbx.TabIndex = 0
        '
        'txt
        '
        Me.txt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txt.Location = New System.Drawing.Point(0, 0)
        Me.txt.Name = "txt"
        Me.txt.Size = New System.Drawing.Size(262, 20)
        Me.txt.TabIndex = 1
        Me.txt.Visible = False
        '
        'ocxCBX
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.cbx)
        Me.Controls.Add(Me.txt)
        Me.Name = "ocxCBX"
        Me.Size = New System.Drawing.Size(262, 23)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Public WithEvents cbx As System.Windows.Forms.ComboBox
    Public WithEvents txt As System.Windows.Forms.TextBox

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxTXTProducto
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grid = New System.Windows.Forms.DataGridView()
        Me.txt = New ERP.ocxTXTString()
        CType(Me.grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grid
        '
        Me.grid.AllowUserToAddRows = False
        Me.grid.AllowUserToDeleteRows = False
        Me.grid.AllowUserToResizeColumns = False
        Me.grid.AllowUserToResizeRows = False
        Me.grid.BackgroundColor = System.Drawing.Color.White
        Me.grid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.grid.ColumnHeadersVisible = False
        Me.grid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.grid.GridColor = System.Drawing.Color.White
        Me.grid.Location = New System.Drawing.Point(74, 3)
        Me.grid.MultiSelect = False
        Me.grid.Name = "grid"
        Me.grid.ReadOnly = True
        Me.grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.grid.RowHeadersVisible = False
        Me.grid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grid.ShowCellErrors = False
        Me.grid.ShowCellToolTips = False
        Me.grid.ShowEditingIcon = False
        Me.grid.ShowRowErrors = False
        Me.grid.Size = New System.Drawing.Size(86, 20)
        Me.grid.TabIndex = 1
        '
        'txt
        '
        Me.txt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt.Color = System.Drawing.Color.Empty
        Me.txt.Indicaciones = Nothing
        Me.txt.Location = New System.Drawing.Point(3, 3)
        Me.txt.Margin = New System.Windows.Forms.Padding(0)
        Me.txt.Multilinea = False
        Me.txt.Name = "txt"
        Me.txt.Size = New System.Drawing.Size(65, 22)
        Me.txt.SoloLectura = False
        Me.txt.TabIndex = 2
        Me.txt.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txt.Texto = ""
        '
        'ocxTXTProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.txt)
        Me.Controls.Add(Me.grid)
        Me.Name = "ocxTXTProducto"
        Me.Size = New System.Drawing.Size(173, 30)
        CType(Me.grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grid As System.Windows.Forms.DataGridView
    Friend WithEvents txt As ERP.ocxTXTString

End Class

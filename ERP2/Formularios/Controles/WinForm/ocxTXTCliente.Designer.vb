﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxTXTCliente
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtRUC = New ERP.ocxTXTString()
        Me.txtRazonSocial = New ERP.ocxTXTString()
        Me.txtReferencia = New ERP.ocxTXTString()
        Me.grid = New System.Windows.Forms.DataGridView()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 68.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.txtRUC, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtRazonSocial, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtReferencia, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(428, 24)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'txtRUC
        '
        Me.txtRUC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRUC.Color = System.Drawing.Color.Empty
        Me.txtRUC.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRUC.Indicaciones = Nothing
        Me.txtRUC.Location = New System.Drawing.Point(318, 2)
        Me.txtRUC.Margin = New System.Windows.Forms.Padding(1)
        Me.txtRUC.Multilinea = False
        Me.txtRUC.Name = "txtRUC"
        Me.txtRUC.Size = New System.Drawing.Size(108, 20)
        Me.txtRUC.SoloLectura = False
        Me.txtRUC.TabIndex = 2
        Me.txtRUC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRUC.Texto = ""
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazonSocial.Color = System.Drawing.Color.Empty
        Me.txtRazonSocial.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRazonSocial.Indicaciones = Nothing
        Me.txtRazonSocial.Location = New System.Drawing.Point(71, 2)
        Me.txtRazonSocial.Margin = New System.Windows.Forms.Padding(1)
        Me.txtRazonSocial.Multilinea = False
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.Size = New System.Drawing.Size(244, 20)
        Me.txtRazonSocial.SoloLectura = False
        Me.txtRazonSocial.TabIndex = 1
        Me.txtRazonSocial.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRazonSocial.Texto = ""
        '
        'txtReferencia
        '
        Me.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferencia.Color = System.Drawing.Color.Empty
        Me.txtReferencia.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtReferencia.Indicaciones = Nothing
        Me.txtReferencia.Location = New System.Drawing.Point(2, 2)
        Me.txtReferencia.Margin = New System.Windows.Forms.Padding(1)
        Me.txtReferencia.Multilinea = False
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Size = New System.Drawing.Size(66, 20)
        Me.txtReferencia.SoloLectura = False
        Me.txtReferencia.TabIndex = 0
        Me.txtReferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtReferencia.Texto = ""
        '
        'grid
        '
        Me.grid.AllowUserToAddRows = False
        Me.grid.AllowUserToDeleteRows = False
        Me.grid.AllowUserToResizeColumns = False
        Me.grid.AllowUserToResizeRows = False
        Me.grid.BackgroundColor = System.Drawing.Color.White
        Me.grid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.grid.ColumnHeadersVisible = False
        Me.grid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.grid.GridColor = System.Drawing.Color.White
        Me.grid.Location = New System.Drawing.Point(3, 29)
        Me.grid.MultiSelect = False
        Me.grid.Name = "grid"
        Me.grid.ReadOnly = True
        Me.grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.grid.RowHeadersVisible = False
        Me.grid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grid.ShowCellErrors = False
        Me.grid.ShowCellToolTips = False
        Me.grid.ShowEditingIcon = False
        Me.grid.ShowRowErrors = False
        Me.grid.Size = New System.Drawing.Size(425, 20)
        Me.grid.TabIndex = 1
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(203, 32)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(44, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtID.Texto = ""
        Me.txtID.Visible = False
        '
        'ocxTXTCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.grid)
        Me.Controls.Add(Me.txtID)
        Me.Name = "ocxTXTCliente"
        Me.Size = New System.Drawing.Size(428, 56)
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents grid As System.Windows.Forms.DataGridView
    Friend WithEvents txtRUC As ERP.ocxTXTString
    Friend WithEvents txtRazonSocial As ERP.ocxTXTString
    Friend WithEvents txtReferencia As ERP.ocxTXTString

End Class

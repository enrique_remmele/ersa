﻿Public Class ocxCBX

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Public Event Editar(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    Public Event TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    Shadows Event GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    Shadows Event LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)

    'PROPIEDADES
    Private LockedValue As Boolean
    Public Property Locked() As Boolean
        Get
            Return LockedValue
        End Get
        Set(ByVal value As Boolean)
            LockedValue = value
            If value = False Then
                txt.Visible = True
                txt.ReadOnly = True
                txt.Text = cbx.Text
                cbx.Visible = False
                Me.TabStop = False
            Else
                txt.Visible = False
                cbx.Visible = True
                Me.TabStop = True
            End If
        End Set
    End Property

    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value
            If value = True Then
                txt.Visible = True
                txt.ReadOnly = True
                txt.BackColor = Color.FromArgb(vgColorSoloLectura)
                txt.Text = cbx.Text
                cbx.Visible = False
                Me.TabStop = False
            Else
                txt.Visible = False
                cbx.Visible = True
                cbx.BackColor = Color.FromArgb(vgColorBackColor)
                Me.TabStop = True
            End If
        End Set
    End Property

    Private SeleccionObligatoriaValue As Boolean
    Public Property SeleccionObligatoria() As Boolean
        Get
            Return SeleccionObligatoriaValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionObligatoriaValue = value
            If value = True Then
                cbx.DropDownStyle = ComboBoxStyle.DropDownList
            Else
                cbx.DropDownStyle = ComboBoxStyle.DropDown
            End If

        End Set
    End Property

    Private TextoValue As String
    Public Property Texto() As String
        Get
            Return cbx.Text
        End Get
        Set(ByVal value As String)
            TextoValue = value
            cbx.Text = value
            txt.Text = value
        End Set
    End Property

    Private IndicacionesValue As String
    ''' <summary>
    ''' Propiedad en donde se puede definir lo que hace el control.
    ''' Esto se mostrara al usuario.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Indicaciones() As String
        Get
            Return IndicacionesValue
        End Get
        Set(ByVal value As String)
            IndicacionesValue = value
        End Set
    End Property

    Private DataSourceValue As String
    Public Property DataSource() As String
        Get
            Return DataSourceValue
        End Get
        Set(ByVal value As String)
            DataSourceValue = value
        End Set
    End Property

    Private DataDisplayMemberValue As String
    Public Property DataDisplayMember() As String
        Get
            Return DataDisplayMemberValue
        End Get
        Set(ByVal value As String)
            DataDisplayMemberValue = value
        End Set
    End Property

    Private DataValueMemberValue As String
    Public Property DataValueMember() As String
        Get
            Return DataValueMemberValue
        End Get
        Set(ByVal value As String)
            DataValueMemberValue = value
        End Set
    End Property

    Private DataFilterValue As String
    Public Property DataFilter() As String
        Get
            Return DataFilterValue
        End Get
        Set(ByVal value As String)
            DataFilterValue = value
            Filtrar()
        End Set
    End Property

    Private DataOrderByValue As String
    Public Property DataOrderBy() As String
        Get
            Return DataOrderByValue
        End Get
        Set(ByVal value As String)
            DataOrderByValue = value
        End Set
    End Property

    Private FormABMValue As String
    Public Property FormABM() As String
        Get
            Return FormABMValue
        End Get
        Set(ByVal value As String)
            FormABMValue = value
        End Set
    End Property

    Private CampoWhereValue As String
    Public Property CampoWhere() As String
        Get
            Return CampoWhereValue
        End Get
        Set(ByVal value As String)
            CampoWhereValue = value
        End Set
    End Property

    Private CargarUnaSolaVezValue As Boolean
    Public Property CargarUnaSolaVez() As Boolean
        Get
            Return CargarUnaSolaVezValue
        End Get
        Set(ByVal value As Boolean)
            CargarUnaSolaVezValue = value
        End Set
    End Property

    Public Sub ActualizarTexto()

        cbx.Text = ""

        If DataValueMember Is Nothing Then
            DataValueMember = cbx.ValueMember
        End If

        If DataDisplayMember Is Nothing Then
            DataDisplayMember = cbx.DisplayMember
        End If

        Try
            If DataValueMember IsNot Nothing And DataDisplayMember IsNot Nothing Then
                For Each oRow As DataRow In cbx.DataSource.rows
                    If oRow(DataValueMember) = cbx.SelectedValue Then
                        cbx.Text = oRow(DataDisplayMember)
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception

        End Try
        

        txt.Text = cbx.Text

    End Sub

    Public Sub SelectedValue(ByVal ID As String)

        If IsNumeric(ID) = False Then
            Exit Sub
        End If

        cbx.SelectedValue = ID
        ActualizarTexto()

    End Sub

    Public Function GetValue() As Integer

        'Si el texto esta vacio, no se selecciono nada!!!!!
        If cbx.Text = "" Then
            Return 0
        End If

        If DataValueMember IsNot Nothing And DataDisplayMember IsNot Nothing Then
            Try
                For Each oRow As DataRow In cbx.DataSource.rows
                    If oRow(DataValueMember) = cbx.SelectedValue Then
                        cbx.Text = oRow(DataDisplayMember)
                        Exit For
                    End If
                Next
            Catch ex As Exception

            End Try

        End If

        If Validar() = False Then
            Return 0
        End If

        GetValue = cbx.SelectedValue

    End Function

    Public Sub Conectar(Optional ByVal Condicion As String = "", Optional ByVal OrderBy As String = "")

        'Comente temporalmente... si hay errores descomentar y fijarse en frmImpresionSeccion - Cargar Secciones
        'cbx.DataSource = Nothing

        If CargarUnaSolaVez = True Then
            If Not cbx.DataSource Is Nothing Then
                Exit Sub
            End If
        End If

        Try
            'Validar
            If DataSource = "" Then
                Exit Sub
            End If


            Dim dttemp As DataTable

            'Si existen una condicion
            dttemp = CData.GetTable(DataSource, Condicion)

            If OrderBy = "" Then
                If DataOrderBy <> "" Then
                    OrderBy = DataOrderBy
                Else
                    Try
                        OrderBy = DataDisplayMember
                    Catch ex As Exception

                    End Try
                End If
            End If

            'Si se quiere ordenar
            CData.OrderDataTable(dttemp, OrderBy)

            If DataValueMember = "" Then
                DataValueMember = "ID"
            End If

            If DataDisplayMember = "" Then
                DataDisplayMember = "Descripcion"
            End If

            cbx.DisplayMember = DataDisplayMember
            cbx.ValueMember = DataValueMember
            cbx.DataSource = dttemp
        Catch ex As Exception

        End Try


    End Sub

    Public Sub Conectar(ByVal dt As DataTable, ByVal Condicion As String, Optional ByVal OrderBy As String = "")

        cbx.DataSource = Nothing

        If DataSource = "" Or DataDisplayMember = "" Or DataValueMember = "" Then
            Exit Sub
        End If

        cbx.DisplayMember = DataDisplayMember
        cbx.ValueMember = DataValueMember
        cbx.DataSource = dt

        If DataOrderBy <> "" Then
            Ordenar(DataOrderBy)
        End If

    End Sub

    Public Sub Filtrar()

        cbx.DataSource = Nothing

        If DataSource = "" Or DataDisplayMember = "" Or DataValueMember = "" Then
            Exit Sub
        End If

        If DataFilter = "" Then
            Exit Sub
        End If

        Dim dt As DataTable = CData.GetTable(DataSource)
        dt = CData.FiltrarDataTable(dt, DataFilter).Copy

        cbx.DisplayMember = DataDisplayMember
        cbx.ValueMember = DataValueMember
        cbx.DataSource = dt

        If DataOrderBy <> "" Then
            Ordenar(DataOrderBy)
        End If

    End Sub

    Public Sub Ordenar(ByVal Orden As String)

        Dim dt As DataTable = cbx.DataSource
        CData.OrderDataTable(dt, Orden)

        cbx.DataSource = dt

    End Sub

    Public Function Validar(ByVal MensajeError As String, ByRef ctrError As ErrorProvider, ByRef ctr As Control, ByRef tsslEstado As ToolStripStatusLabel) As Boolean

        Validar = True


        If cbx.DataSource Is Nothing Then
            ctrError.SetError(ctr, MensajeError)
            ctrError.SetIconAlignment(ctr, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = MensajeError
            Return False
        End If

        If DataValueMember IsNot Nothing And DataDisplayMember IsNot Nothing Then
            For Each oRow As DataRow In cbx.DataSource.rows
                If oRow(DataDisplayMember).ToString = cbx.Text Then
                    cbx.SelectedValue = oRow(DataValueMember)
                    Exit For
                End If
            Next
        End If

        If Me.cbx.SelectedValue = Nothing Then
            ctrError.SetError(ctr, MensajeError)
            ctrError.SetIconAlignment(ctr, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = MensajeError
            Return False
        End If

        If Me.cbx.Text.Trim = "" Then
            ctrError.SetError(ctr, MensajeError)
            ctrError.SetIconAlignment(ctr, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = MensajeError
            Return False
        End If

    End Function

    Public Function Validar() As Boolean

        Validar = True

        If Me.SeleccionObligatoria = True Then

            If Me.cbx.SelectedValue = Nothing Then
                Return False
            End If

            If Me.cbx.Text.Trim = "" Then
                Return False
            End If

        End If

    End Function

    Function EstablecerCondicion(ByRef where As String, Optional ByVal Revertir As Boolean = False) As String

        If Me.Enabled = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                Return ""
            End If

            If cbx.SelectedValue = 0 Then
                Return ""
            End If

            If CampoWhere = "" Then
                Return ""
            End If

            Dim Asignacion As String = "="

            If Revertir = True Then
                Asignacion = "!="
            End If

            If where = "" Then
                where = " Where (" & CampoWhere & Asignacion & cbx.SelectedValue & ") "
            Else
                where = where & " And (" & CampoWhere & Asignacion & cbx.SelectedValue & ") "
            End If

        End If

    End Function

    Function EstablecerCondicion2(ByRef where As String) As String

        EstablecerCondicion2 = ""

        If Me.Enabled = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                Return ""
            End If

            If cbx.SelectedValue = 0 Then
                Return ""
            End If

            If CampoWhere = "" Then
                Return ""
            End If

            If where = "" Then
                where = CampoWhere & "=" & cbx.SelectedValue
            Else
                where = where & " And " & CampoWhere & "=" & cbx.SelectedValue
            End If

        End If

    End Function

    Sub EstablecerSubTitulo(ByRef SubTitulo As String)

        If Me.Enabled = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                Exit Sub
            End If

            If cbx.SelectedValue = 0 Then
                Exit Sub
            End If

            If SubTitulo = "" Then
                SubTitulo = cbx.Text
            Else
                SubTitulo = SubTitulo & " - " & cbx.Text
            End If

        End If

    End Sub

    Private Sub cbx_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbx.Click
        'RaiseEvent PropertyChanged(sender, e)
    End Sub

    Private Sub cbx_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbx.GotFocus

        'If cbx.Focused = True Then
        '    cbx.DroppedDown = True
        'End If

        If cbx.Focused = True And SeleccionObligatoria = True Then
            If cbx.SelectedIndex >= 0 Then
                cbx.Text = cbx.GetItemText(cbx.Items(cbx.SelectedIndex))
            End If
        End If

        cbx.BackColor = Color.FromArgb(vgColorFocus)

        RaiseEvent GotFocus(sender, e)

    End Sub

    Private Sub cbx_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbx.KeyUp

        If e.KeyCode = Keys.F2 Then
            RaiseEvent Editar(sender, e)
        End If

        If e.KeyValue > 64 And e.KeyValue < 106 Then
            If cbx.Focused = True And cbx.DroppedDown = False Then
                'cbx.DroppedDown = True
            End If
        End If

        If cbx.Text.Length = 0 And cbx.DroppedDown = True Then
            'cbx.DroppedDown = False
        End If

        If e.KeyCode = Keys.Up Or e.KeyCode = Keys.Down Then
            If cbx.DroppedDown = False Then
                cbx.DroppedDown = True
            End If
        End If

        If e.KeyCode = vgKeyActualizarTabla Then
            CData.ResetTable(Me.DataSource)
            Conectar()
        End If

        If e.KeyCode = vgKeyVerInformacionDetallada Then

            If IsNumeric(cbx.SelectedValue) = False Then
                Exit Sub
            End If

            If Me.DataSource = "" Or Me.DataValueMember = "" Then
                Exit Sub
            End If

            Dim frm As New frmPropiedad
            frm.TituloVentana = "Propiedades de " & Me.DataSource
            frm.Titulo = cbx.Text
            frm.Consulta = "Select * From " & Me.DataSource & " Where " & Me.DataValueMember & "=" & cbx.SelectedValue & " "
            frm.ShowDialog()

        End If

        If e.KeyCode = vgKeyEditar Then

            If Me.FormABM Is Nothing Then
                Exit Sub
            End If

            If Me.FormABM = "" Then
                Exit Sub
            End If


            Try

                Dim form As Form
                Dim tipoObjeto As Type = Type.GetType("ERP." & Me.FormABM)
                Dim objeto As Object = Activator.CreateInstance(tipoObjeto)
                form = DirectCast(objeto, Form)

                FGMostrarFormulario(Me.ParentForm, form, "Actualizacion", FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True)

                CData.ResetTable(Me.DataSource)
                Conectar()


            Catch ex As Exception

            End Try

        End If

        RaiseEvent TeclaPrecionada(sender, e)

    End Sub

    Private Sub cbx_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbx.Leave
        If cbx.Focused = False Then
            cbx.DroppedDown = False
        Else
            If cbx.SelectedIndex >= 0 And SeleccionObligatoria = True Then
                cbx.Text = cbx.GetItemText(cbx.Items(cbx.SelectedIndex))
            End If
        End If

        cbx.BackColor = Color.FromArgb(vgColorBackColor)

        RaiseEvent LostFocus(sender, e)

    End Sub

    Private Sub cbx_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbx.LostFocus
        If cbx.Focused = False Then
            cbx.DroppedDown = False
        End If

        RaiseEvent LostFocus(sender, e)

    End Sub

    Private Sub cbx_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbx.SelectedIndexChanged
        txt.Text = cbx.Text
        'RaiseEvent PropertyChanged(sender, e)
    End Sub

    Private Sub ocxCBX_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
        'RaiseEvent GotFocus(sender, e) <- Produce error
    End Sub

    Private Sub ocxCBX_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

    End Sub

    Private Sub cbx_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbx.TextChanged

        If cbx.DroppedDown = False Then
            txt.Text = cbx.Text
            RaiseEvent PropertyChanged(sender, e)
        End If
        
    End Sub

    Private Sub cbx_DropDownStyleChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbx.DropDownStyleChanged
        If cbx.DroppedDown = True Then
            If cbx.Text.Length = 1 Then

            End If
        Else

        End If

    End Sub

    Private Sub cbx_LocationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbx.LocationChanged

    End Sub

    Private Sub ocxCBX_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave
        'RaiseEvent PropertyChanged(sender, e)
    End Sub

    Private Sub ocxCBX_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If DataFilter <> "" Then
            Conectar(DataFilter)
        Else
            Conectar()
        End If

    End Sub

End Class

﻿Public Class ocxTXTNumeric

    Private DecimalesyValue As Boolean
    Public Property Decimales() As Boolean
        Get
            Return DecimalesyValue
        End Get
        Set(ByVal value As Boolean)
            DecimalesyValue = value
        End Set
    End Property

    'EVENTOS
    Public Event TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

    'PROPIEDADES
    Private TextAlignValue As System.Windows.Forms.HorizontalAlignment
    ''' <summary>
    ''' Indica como estara alineado el texto dentro del control
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property TextAlign() As System.Windows.Forms.HorizontalAlignment
        Get
            Return TextAlignValue
        End Get
        Set(ByVal value As System.Windows.Forms.HorizontalAlignment)
            TextAlignValue = value
        End Set
    End Property

    Private TextoValue As String
    Public Property Texto() As String
        Get
            Return TextoValue
        End Get
        Set(ByVal value As String)
            TextoValue = value
            txt.Text = value
        End Set
    End Property

    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value
            txt.ReadOnly = value
            txt.TabStop = Not value
            Me.TabStop = Not value

            If value = True Then
                txt.BackColor = Color.FromArgb(vgColorSoloLectura)
            Else
                txt.BackColor = Color.FromArgb(vgColorBackColor)
            End If
        End Set
    End Property

    Private IndicacionesValue As String
    ''' <summary>
    ''' Propiedad en donde se puede definir lo que hace el control.
    ''' Esto se mostrara al usuario.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Indicaciones() As String
        Get
            Return IndicacionesValue
        End Get
        Set(ByVal value As String)
            IndicacionesValue = value
        End Set
    End Property

    Private ColorValue As System.Drawing.Color
    Public Property Color() As System.Drawing.Color
        Get
            Return ColorValue
        End Get
        Set(ByVal value As System.Drawing.Color)
            ColorValue = value
            txt.BackColor = Color
        End Set
    End Property

    Public Function ObtenerValor() As Decimal

        ObtenerValor = "0"

        Try
            Dim tmp As String
            tmp = txt.Text.Trim

            If IsNumeric(tmp) = False Then
                Return "0"
            End If

            tmp = tmp.Replace(".", "")
            'tmp = tmp.Replace(",", ".")

            Return CDec(tmp)

        Catch ex As Exception
            Return 0
        End Try

    End Function

    Public Sub SetValue(ByVal importe As Decimal)

        Try
            txt.Text = importe

        Catch ex As Exception

        End Try

    End Sub

    Public Sub SetValue(ByVal importe As String)

        Try
            If IsNumeric(importe) = False Then
                txt.Text = "0"
                Exit Sub
            End If

            txt.Text = importe

        Catch ex As Exception

        End Try

    End Sub

    Private Sub EstablecerFormato()

        Try

            Dim valor As String = txt.Text

            If IsNumeric(valor) = False Then
                valor = "0"
                Exit Sub
            End If

            If CDec(valor) = 0 Then
                valor = "0"
                Exit Sub
            End If

            If Decimales = True Then
                valor = CStr(Format(CDec(valor), "###,###,###.##"))
            Else
                valor = CStr(Format(CDec(valor), "###,###,###"))
            End If

            If valor.Substring(0, 1) = "," Then
                valor = "0" & valor
            End If

            txt.Text = valor


        Catch ex As Exception

        End Try


    End Sub

    Private Sub txt_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt.GotFocus
        Try
            txt.Text = txt.Text.Replace(".", "")
            If Decimales = False Then
                If txt.Text.IndexOf(",") > 0 Then
                    txt.Text = txt.Text.Substring(0, txt.Text.IndexOf(","))
                End If
            End If
        Catch ex As Exception

        End Try

        If txt.ReadOnly = False Then
            txt.BackColor = Color.FromArgb(vgColorFocus)
        End If

    End Sub

    Private Sub txt_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt.LostFocus
        'txt.Text = Format(txt.Text, "Currency")
        EstablecerFormato()
       
    End Sub

    Private Sub txt_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt.KeyPress
        Try
            If Char.IsDigit(e.KeyChar) Then
                e.Handled = False
            ElseIf Char.IsControl(e.KeyChar) Then
                e.Handled = False
            Else
                If e.KeyChar = "," And Decimales = True Then
                    e.Handled = False
                ElseIf e.KeyChar = "-" Then
                    e.Handled = False
                Else
                    e.Handled = True
                End If
            End If

            Texto = txt.Text

        Catch ex As Exception

        End Try

    End Sub

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        TextAlign = HorizontalAlignment.Right
        Texto = "0"
        Decimales = True

    End Sub

    Private Sub txt_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt.KeyUp

        If SoloLectura = True Then
            Exit Sub
        End If

        RaiseEvent TeclaPrecionada(sender, e)
    End Sub

    Private Sub txt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt.TextChanged
        If txt.Focused = False Then
            EstablecerFormato()
        End If
    End Sub


    Private Sub txt_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt.Leave

        If txt.ReadOnly = False Then
            txt.BackColor = Drawing.Color.White
        End If

    End Sub

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxTXTDate
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txt = New System.Windows.Forms.MaskedTextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'txt
        '
        Me.txt.AsciiOnly = True
        Me.txt.BackColor = System.Drawing.Color.White
        Me.txt.BeepOnError = True
        Me.txt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txt.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite
        Me.txt.Location = New System.Drawing.Point(0, 0)
        Me.txt.Mask = "00/00/0000"
        Me.txt.Name = "txt"
        Me.txt.Size = New System.Drawing.Size(271, 20)
        Me.txt.SkipLiterals = False
        Me.txt.TabIndex = 0
        Me.txt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txt.ValidatingType = GetType(Date)
        '
        'ToolTip1
        '
        '
        'ocxTXTDate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.txt)
        Me.Name = "ocxTXTDate"
        Me.Size = New System.Drawing.Size(271, 21)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents txt As System.Windows.Forms.MaskedTextBox

End Class

﻿Public Class ocxTXTProducto

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'ENUMERACIONES
    Enum ENUMTipoDescuento
        TPR = 0
        TACTICO = 1
        EXPRESS = 2
        ACUERDO = 3
    End Enum

    'PROPIEDADES
    Private RegistroValue As DataRow
    Public Property Registro() As DataRow
        Get
            Return RegistroValue
        End Get
        Set(ByVal value As DataRow)
            RegistroValue = value
        End Set
    End Property

    Private SeleccionadoValue As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionadoValue = value
        End Set
    End Property

    Private AlturaMaximaValue As Integer
    Public Property AlturaMaxima() As Integer
        Get
            Return AlturaMaximaValue
        End Get
        Set(ByVal value As Integer)
            AlturaMaximaValue = value
        End Set
    End Property

    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDDepositoValue As Integer
    Public Property IDDeposito() As Integer
        Get
            Return IDDepositoValue
        End Get
        Set(ByVal value As Integer)
            IDDepositoValue = value
        End Set
    End Property

    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value
            txt.SoloLectura = value
            Me.Enabled = Not value
            Me.TabStop = Not value
        End Set

    End Property

    Private VentaValue As Boolean
    Public Property Venta() As Boolean
        Get
            Return VentaValue
        End Get
        Set(ByVal value As Boolean)
            VentaValue = value
        End Set
    End Property

    Private ConsultaValue As String
    Public Property Consulta() As String
        Get
            Return ConsultaValue
        End Get
        Set(ByVal value As String)
            ConsultaValue = value
        End Set
    End Property

    Private ColumnasNumericasValue As String()
    Public Property ColumnasNumericas() As String()
        Get
            Return ColumnasNumericasValue
        End Get
        Set(ByVal value As String())
            ColumnasNumericasValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    Private PreciosValue As DataTable
    Public Property Precios() As DataTable
        Get
            Return PreciosValue
        End Get
        Set(ByVal value As DataTable)
            PreciosValue = value
        End Set
    End Property

    Private TotalPorcentajeDescuentoValue As Decimal
    Public Property TotalPorcentajeDescuento() As Decimal
        Get
            Return TotalPorcentajeDescuentoValue
        End Get
        Set(ByVal value As Decimal)
            TotalPorcentajeDescuentoValue = value
        End Set
    End Property

    Private TieneDescuentoValue As Boolean
    Public Property TieneDescuento() As Boolean
        Get
            Return TieneDescuentoValue
        End Get
        Set(ByVal value As Boolean)
            TieneDescuentoValue = value
        End Set
    End Property

    Private ControlarExistenciaValue As Boolean
    Public Property ControlarExistencia() As Boolean
        Get
            Return ControlarExistenciaValue
        End Get
        Set(ByVal value As Boolean)
            ControlarExistenciaValue = value
        End Set
    End Property

    'EVENTOS
    Public Event ItemSeleccionado(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ItemMalSeleccionado(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ItemInicializado(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Private dt As New DataTable
    Private dtTemp As New DataTable
    Private BindingSource1 As New BindingSource
    Private vTabla As String = "VProducto"
    Private vTablaSp As String = "SpViewProducto"

    Sub Inicializar()

        'Controles
        txt.txt.Clear()
        grid.DataSource = Nothing
        grid.Visible = False

        'Funciones
        Conectar()
        EstablecerPosicion()

    End Sub

    Public Sub Conectar()

        'Dim Consulta As String = "Select ID, Descripcion, CodigoBarra, Ref, UnidadPorCaja, IDImpuesto, Impuesto, Costo From VProducto Where ControlarExistencia='True' And Estado='True' Order By Descripcion"
        'dt = CSistema.ExecuteToDataTable(Consulta)
        dt = CData.GetTable(vTabla, " ControlarExistencia='True' And Estado='True' ")
        BindingSource1.DataSource = dt

    End Sub

    Public Sub Conectar(ByVal vdt As DataTable)

        If dt Is Nothing Then
            Exit Sub
        End If

        Try
            dt = vdt.Copy
            BindingSource1.DataSource = dt
        Catch ex As Exception

        End Try

    End Sub

    Public Sub ConectarVenta()

        'Dim Consulta As String = "Select ID, Descripcion, CodigoBarra, Ref, UnidadPorCaja, IDImpuesto, Impuesto, [Ref Imp], 'Precio'=0, Exento, Costo From VProducto Where ControlarExistencia='True' And Estado='True' Order By Descripcion"
        'dt = CSistema.ExecuteToDataTable(Consulta)
        dt = CData.GetTable("VProducto", " Estado='True'  ")

    End Sub

    Private Sub Listar()

        'Validar
        If txt.txt.Focused = False Then
            Exit Sub
        End If

        If txt.txt.Text.Trim.Length <= 0 Then
            Exit Sub
        End If

        Seleccionado = False

        Dim Where As String = ""
        Dim Valor As String = txt.txt.Text.Trim

        Try

            If IsNumeric(Valor) = True And Valor.StartsWith("0") = False Then

                'Por ID
                If Valor.Length <= 5 Then
                    Where = " ID=" & Valor & " Or Ref Like '" & Valor & "' "
                End If

                'Por Referencia
                If Valor.Length >= 6 And Valor.Length <= 8 Then
                    Where = " Ref Like '" & Valor & "' "
                End If

                'Por Codigo de Barra
                If Valor.Length >= 9 Then
                    Where = " CodigoBarra='" & Valor & "' "
                End If

            Else

                If Valor.Length <= 1 Then
                    Where = " [Ref] = '" & Valor & "' "
                Else
                    'Por Descripcion y/o referencia
                    Where = " [Descripcion] Like '%" & Valor & "%' Or [Ref] Like '" & Valor & "%' "
                End If

            End If

            BindingSource1.DataSource = dt
            BindingSource1.Filter = Where
            grid.DataSource = BindingSource1.DataSource

            grid.Visible = True
            If AlturaMaxima > Math.Round(grid.RowCount * 20, 0) Then
                grid.Height = Math.Round(grid.RowCount * 20, 0)
            Else
                grid.Height = AlturaMaxima
            End If

            grid.BringToFront()
            Me.Height = grid.Height + txt.Height

            If grid.Columns.Count > 0 Then
                For i As Integer = 0 To grid.ColumnCount - 1
                    grid.Columns(i).Visible = False
                Next

                grid.Columns("Ref").DisplayIndex = 0
                grid.Columns("Descripcion").DisplayIndex = 1

                grid.Columns("Ref").Visible = True
                grid.Columns("Descripcion").Visible = True

                grid.Columns("Ref").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                grid.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

            End If


            

        Catch ex As Exception

        End Try
        


    End Sub

    Private Sub OcultarLista()

        grid.Visible = False
        Me.Height = txt.Height

    End Sub

    Public Sub LimpiarSeleccion()

        Seleccionado = False
        OcultarLista()
        txt.txt.Clear()
        Registro = Nothing

    End Sub

    Private Sub Seleccionar(Optional ByVal GridFocus As Boolean = False)

        If SoloLectura = True Then
            Exit Sub
        End If

        'Validar
        If grid.RowCount = 0 Then
            LimpiarSeleccion()
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
            Exit Sub
        End If

        'Si los controles estan vacios
        If txt.txt.Text = "" Then
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
            Exit Sub
        End If

        'If GridFocus = True Then
        '    Exit Sub
        'End If

        Try

            Dim oRow As DataRow = dt.Select(" ID=" & grid.SelectedRows(0).Cells("ID").Value)(0)

            If Venta = True Then

                If IDCliente = 0 Then
                    MessageBox.Show("Seleccione correctamente un cliente.", "Productos", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    OcultarLista()
                    Seleccionado = False
                    Exit Sub
                End If

                'Aprovechamos y sacamos la existencia tambien
                Precios = CSistema.ExecuteToDataTable("EXEC SpPrecioProducto2 @IDProducto = " & oRow("ID").ToString & ", @IDCliente = " & IDCliente & ", @IDSucursal = " & IDSucursal & ", @IDDeposito=" & IDDeposito)

                Dim Precio As Decimal

                For Each r As DataRow In Precios.Select(" IDTipo=-1")
                    Precio = r("Importe")
                Next

                oRow("Precio") = Precio

                ''Obtener Precio
                'Dim Precio As Decimal
                'Precio = CType(CSistema.ExecuteScalar("Select " & vgOwnerDBFunction & ".FPrecioProducto(" & oRow("ID").ToString & ", " & IDCliente & ", " & IDSucursal & ")"), Decimal)

                'oRow("Precio") = Precio

                ''Obtener los descuentos
                'dtDescuento = CSistema.ExecuteToDataTable("EXEC SpPrecioProducto2 @IDProducto = " & oRow("ID").ToString & ", @IDCliente = " & IDCliente & ", @IDSucursal = " & IDSucursal & "")
                'TieneDescuento = False

                'If dtDescuento IsNot Nothing Then
                '    If dtDescuento.Rows.Count > 0 Then
                '        TotalPorcentajeDescuento = CSistema.dtSumColumn(dtDescuento, "Porcentaje")
                '        TieneDescuento = True
                '    End If
                'End If

            End If

            Registro = oRow

            Try
                Registro("Existencia") = Precios.Rows(0)("Existencia")
            Catch ex As Exception

            End Try


            txt.txt.Text = oRow("Descripcion").ToString



            OcultarLista()
            Seleccionado = True
            RaiseEvent ItemSeleccionado(New Object, New EventArgs)

        Catch ex As Exception
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
        End Try


    End Sub

    Private Sub MostrarPropiedades()

        Dim ID As Integer
        Dim Descripcion As String = ""

        If Seleccionado = False Then

            If grid.RowCount = 0 Then
                Exit Sub
            End If

            If grid.SelectedRows.Count = 0 Then
                Exit Sub
            End If

            ID = grid.SelectedRows(0).Cells(0).Value
            Descripcion = grid.SelectedRows(0).Cells(1).Value
        Else
            ID = Registro("ID").ToString
            Descripcion = Registro("Descripcion").ToString
        End If



        Dim frm As New frmPropiedad
        frm.TituloVentana = "Propiedades del Producto"
        frm.Titulo = Descripcion
        frm.Consulta = "Select ID, Descripcion, CodigoBarra, Ref, 'Existencia'=dbo.FExistenciaProducto(ID, " & IDDeposito & "), 'Disponible'=dbo.FExistenciaProductoReal(ID, " & IDDeposito & "), 'Reservado'=dbo.FExistenciaProductoReservado(ID, " & IDDeposito & "), TipoProducto, Linea, Marca, Presentacion, Categoria, Proveedor, Division, Procedencia, UnidadMedida,Peso,UnidadPorCaja,Impuesto, CostoPromedio From VProducto Where ID=" & ID
        frm.ShowDialog()

    End Sub

    Private Sub MostrarConsulta()

        Dim frm As New frmProductoBuscar
        frm.Consulta = Consulta
        frm.ColumnasNumericas = ColumnasNumericas
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.WindowState = FormWindowState.Maximized
        frm.ControlarExistencia = ControlarExistencia
        frm.ShowDialog()


        If frm.ID = 0 Then
            Exit Sub
        End If

        'Si no existe en el DATATABLE, agregar
        If CData.GetTable(vTabla).Select("ID=" & frm.ID).Count = 0 Then

            'Actualizamos el Registro
            CData.Insertar(frm.ID, vTabla)

            dt = CData.GetTable(vTabla)
            BindingSource1.DataSource = dt

        End If

        txt.txt.Text = frm.ID
        Listar()

        Seleccionar()

    End Sub

    Private Sub ManejoTecla(ByVal txt As TextBox, ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Down Then
            If grid.RowCount > 0 Then
                grid.Focus()
                If grid.SelectedRows.Count = 0 Then
                    grid.Rows(0).Selected = True
                End If
            End If

            Exit Sub

        End If

        If e.KeyCode = Keys.Enter Then
            Seleccionar(False)
            Exit Sub
        End If

        If e.KeyCode = vgKeyVerInformacionDetallada Then
            MostrarPropiedades()
            Exit Sub
        End If

        If e.KeyCode = vgKeyConsultar Then
            MostrarConsulta()
            Exit Sub
        End If

        If e.KeyCode = Keys.ShiftKey Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Tab Then
            If Seleccionado = False Then
                Seleccionar()
            End If
            Exit Sub
        End If

        If e.KeyCode = vgKeyActualizarTabla Then

            CData.ResetTable(vTabla, "Exec " & vTablaSp & " @IDUsuario = " & vgIDUsuario & ", @IDSucursal = " & vgIDSucursal & ", @IDDeposito = " & vgIDDeposito & ", @IDTerminal = " & vgIDTerminal & " ")
            dt = CData.GetTable(vTabla)
            BindingSource1.DataSource = dt

            Exit Sub

        End If

        If e.KeyCode = Keys.Escape Then
            LimpiarSeleccion()
            OcultarLista()
            Exit Sub
        End If

        Listar()

    End Sub

    Public Sub SetValue(ByVal ID As Integer)

        Try

            If ID = -1 Then
                Registro = Nothing
                txt.txt.Text = ""

                OcultarLista()
                Seleccionado = False
                Exit Sub

            End If

            If dt.Select(" ID=" & ID).Count = 0 Then
                Registro = Nothing
                txt.txt.Text = ""

                OcultarLista()
                Seleccionado = False
                RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)

            Else

                Dim oRow As DataRow = dt.Select(" ID=" & ID)(0)

                Registro = oRow
                txt.txt.Text = oRow("ID").ToString

                OcultarLista()
                Seleccionado = True
                RaiseEvent ItemSeleccionado(New Object, New EventArgs)

            End If

        Catch ex As Exception
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
        End Try

    End Sub

    Public Sub EstablecerPosicion()

        'Tañanos
        txt.Width = Me.Width - 3
        grid.Width = txt.Width

        If grid.Visible = False Then
            Me.Height = txt.Height
        End If

        'Localizacion
        txt.Location = New Point(0, 0)
        grid.Left = txt.Left
        grid.Top = txt.Height

    End Sub

    Public Overloads Function Focus() As Boolean

        Focus = False

        If txt.txt.Focused = True Then
            Return True
        End If

        If Me.Focused = True Then
            Return True
        End If

    End Function

    Public Function ObtenerPorcentajeDescuento(ByVal IDTipoDescuento As Integer) As Decimal

        ObtenerPorcentajeDescuento = 0

        'Variables
        Dim Porcentaje As Decimal = 0

        If Precios Is Nothing Then
            Return 0
        End If

        If Precios.Rows.Count = 0 Then
            Return 0
        End If

        For Each oRow As DataRow In Precios.Select(" IDTipo= " & IDTipoDescuento)
            Porcentaje = oRow("Porcentaje")
        Next

        Return Porcentaje

    End Function

    Private Sub ocxTXTProducto_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave
        OcultarLista()
    End Sub

    Private Sub ocxTXTProducto_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LostFocus
        OcultarLista()
    End Sub

    Private Sub ocxTXTProducto_PreviewKeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.PreviewKeyDownEventArgs) Handles Me.PreviewKeyDown

    End Sub

    Private Sub ocxTXTProducto_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        EstablecerPosicion()
    End Sub

    Private Sub txt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt.TeclaPrecionada

    End Sub

    Private Sub txt_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt.KeyUp
        e.Handled = True
        ManejoTecla(txt.txt, e)
    End Sub

    Private Sub grid_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grid.KeyDown
        If e.KeyCode = Keys.Enter Then
            Seleccionar(True)
        End If
    End Sub

    Private Sub grid_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grid.KeyUp
        If e.KeyCode = Keys.F3 Then
            MostrarPropiedades()
        End If
    End Sub

    Private Sub ocxTXTProducto_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Enter
        txt.txt.Focus()
    End Sub

End Class

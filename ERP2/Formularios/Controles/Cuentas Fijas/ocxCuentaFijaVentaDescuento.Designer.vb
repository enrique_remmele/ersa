﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxCuentaFijaVentaDescuento
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.dgvLista = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbxTipoDescuento = New ERP.ocxCBX()
        Me.lblDescuento = New System.Windows.Forms.Label()
        Me.txtCuenta = New ERP.ocxTXTCuentaContable()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.lblDebeHaber = New System.Windows.Forms.Label()
        Me.cbxDebeHaber = New ERP.ocxCBX()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.lblOrden = New System.Windows.Forms.Label()
        Me.txtOrden = New ERP.ocxTXTNumeric()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxFiltroSucursal = New ERP.ocxCBX()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.dgvLista, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel3, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 184.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(838, 527)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLista.Location = New System.Drawing.Point(3, 261)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.Size = New System.Drawing.Size(832, 263)
        Me.dgvLista.TabIndex = 3
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtProveedor)
        Me.Panel1.Controls.Add(Me.lblProveedor)
        Me.Panel1.Controls.Add(Me.cbxTipoDescuento)
        Me.Panel1.Controls.Add(Me.lblDescuento)
        Me.Panel1.Controls.Add(Me.txtCuenta)
        Me.Panel1.Controls.Add(Me.lblSucursal)
        Me.Panel1.Controls.Add(Me.cbxSucursal)
        Me.Panel1.Controls.Add(Me.lblID)
        Me.Panel1.Controls.Add(Me.lblCuenta)
        Me.Panel1.Controls.Add(Me.txtID)
        Me.Panel1.Controls.Add(Me.lblMoneda)
        Me.Panel1.Controls.Add(Me.cbxMoneda)
        Me.Panel1.Controls.Add(Me.lblDebeHaber)
        Me.Panel1.Controls.Add(Me.cbxDebeHaber)
        Me.Panel1.Controls.Add(Me.txtDescripcion)
        Me.Panel1.Controls.Add(Me.lblOrden)
        Me.Panel1.Controls.Add(Me.txtOrden)
        Me.Panel1.Controls.Add(Me.lblDescripcion)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(832, 178)
        Me.Panel1.TabIndex = 0
        '
        'cbxTipoDescuento
        '
        Me.cbxTipoDescuento.CampoWhere = Nothing
        Me.cbxTipoDescuento.CargarUnaSolaVez = False
        Me.cbxTipoDescuento.DataDisplayMember = "Descripcion"
        Me.cbxTipoDescuento.DataFilter = Nothing
        Me.cbxTipoDescuento.DataOrderBy = "Descripcion"
        Me.cbxTipoDescuento.DataSource = "VTipoDescuento"
        Me.cbxTipoDescuento.DataValueMember = "ID"
        Me.cbxTipoDescuento.FormABM = Nothing
        Me.cbxTipoDescuento.Indicaciones = Nothing
        Me.cbxTipoDescuento.Location = New System.Drawing.Point(231, 68)
        Me.cbxTipoDescuento.Name = "cbxTipoDescuento"
        Me.cbxTipoDescuento.SeleccionObligatoria = False
        Me.cbxTipoDescuento.Size = New System.Drawing.Size(271, 21)
        Me.cbxTipoDescuento.SoloLectura = False
        Me.cbxTipoDescuento.TabIndex = 11
        Me.cbxTipoDescuento.Texto = "ACUERDO"
        '
        'lblDescuento
        '
        Me.lblDescuento.AutoSize = True
        Me.lblDescuento.Location = New System.Drawing.Point(163, 72)
        Me.lblDescuento.Name = "lblDescuento"
        Me.lblDescuento.Size = New System.Drawing.Size(62, 13)
        Me.lblDescuento.TabIndex = 10
        Me.lblDescuento.Text = "Descuento:"
        '
        'txtCuenta
        '
        Me.txtCuenta.AlturaMaxima = 100
        Me.txtCuenta.Consulta = Nothing
        Me.txtCuenta.ListarTodas = False
        Me.txtCuenta.Location = New System.Drawing.Point(70, 35)
        Me.txtCuenta.Name = "txtCuenta"
        Me.txtCuenta.Registro = Nothing
        Me.txtCuenta.Resolucion173 = False
        Me.txtCuenta.Seleccionado = False
        Me.txtCuenta.Size = New System.Drawing.Size(432, 27)
        Me.txtCuenta.SoloLectura = False
        Me.txtCuenta.TabIndex = 7
        Me.txtCuenta.Texto = Nothing
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(159, 12)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 2
        Me.lblSucursal.Text = "Sucursal:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = "Descripcion"
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(216, 8)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(143, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 3
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(3, 12)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(3, 40)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 6
        Me.lblCuenta.Text = "Cuenta:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(70, 7)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(73, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 1
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(365, 12)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 4
        Me.lblMoneda.Text = "Moneda:"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Referencia"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(425, 8)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(77, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 5
        Me.cbxMoneda.Texto = "GS"
        '
        'lblDebeHaber
        '
        Me.lblDebeHaber.AutoSize = True
        Me.lblDebeHaber.Location = New System.Drawing.Point(3, 72)
        Me.lblDebeHaber.Name = "lblDebeHaber"
        Me.lblDebeHaber.Size = New System.Drawing.Size(55, 13)
        Me.lblDebeHaber.TabIndex = 8
        Me.lblDebeHaber.Text = "Deb/Hab."
        '
        'cbxDebeHaber
        '
        Me.cbxDebeHaber.CampoWhere = Nothing
        Me.cbxDebeHaber.CargarUnaSolaVez = False
        Me.cbxDebeHaber.DataDisplayMember = Nothing
        Me.cbxDebeHaber.DataFilter = Nothing
        Me.cbxDebeHaber.DataOrderBy = Nothing
        Me.cbxDebeHaber.DataSource = Nothing
        Me.cbxDebeHaber.DataValueMember = Nothing
        Me.cbxDebeHaber.FormABM = Nothing
        Me.cbxDebeHaber.Indicaciones = Nothing
        Me.cbxDebeHaber.Location = New System.Drawing.Point(70, 68)
        Me.cbxDebeHaber.Name = "cbxDebeHaber"
        Me.cbxDebeHaber.SeleccionObligatoria = False
        Me.cbxDebeHaber.Size = New System.Drawing.Size(87, 21)
        Me.cbxDebeHaber.SoloLectura = False
        Me.cbxDebeHaber.TabIndex = 9
        Me.cbxDebeHaber.Texto = ""
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(71, 126)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(433, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 15
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Location = New System.Drawing.Point(2, 158)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(39, 13)
        Me.lblOrden.TabIndex = 16
        Me.lblOrden.Text = "Orden:"
        '
        'txtOrden
        '
        Me.txtOrden.Color = System.Drawing.Color.Empty
        Me.txtOrden.Decimales = True
        Me.txtOrden.Indicaciones = Nothing
        Me.txtOrden.Location = New System.Drawing.Point(71, 153)
        Me.txtOrden.Name = "txtOrden"
        Me.txtOrden.Size = New System.Drawing.Size(76, 22)
        Me.txtOrden.SoloLectura = False
        Me.txtOrden.TabIndex = 17
        Me.txtOrden.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOrden.Texto = "0"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(4, 130)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 14
        Me.lblDescripcion.Text = "Descripcion:"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnNuevo)
        Me.Panel2.Controls.Add(Me.btnEditar)
        Me.Panel2.Controls.Add(Me.btnCancelar)
        Me.Panel2.Controls.Add(Me.btnGuardar)
        Me.Panel2.Controls.Add(Me.btnEliminar)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 187)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(832, 30)
        Me.Panel2.TabIndex = 1
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(0, 3)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 0
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(81, 3)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 1
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(243, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(162, 3)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 2
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(426, 3)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 4
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.cbxFiltroSucursal)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 223)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(832, 32)
        Me.Panel3.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Filtrar por Sucursal:"
        '
        'cbxFiltroSucursal
        '
        Me.cbxFiltroSucursal.CampoWhere = Nothing
        Me.cbxFiltroSucursal.CargarUnaSolaVez = False
        Me.cbxFiltroSucursal.DataDisplayMember = "Descripcion"
        Me.cbxFiltroSucursal.DataFilter = Nothing
        Me.cbxFiltroSucursal.DataOrderBy = "Descripcion"
        Me.cbxFiltroSucursal.DataSource = "VSucursal"
        Me.cbxFiltroSucursal.DataValueMember = "ID"
        Me.cbxFiltroSucursal.FormABM = Nothing
        Me.cbxFiltroSucursal.Indicaciones = Nothing
        Me.cbxFiltroSucursal.Location = New System.Drawing.Point(110, 4)
        Me.cbxFiltroSucursal.Name = "cbxFiltroSucursal"
        Me.cbxFiltroSucursal.SeleccionObligatoria = False
        Me.cbxFiltroSucursal.Size = New System.Drawing.Size(143, 21)
        Me.cbxFiltroSucursal.SoloLectura = False
        Me.cbxFiltroSucursal.TabIndex = 1
        Me.cbxFiltroSucursal.Texto = "ASUNCION"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(43, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 527)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(838, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(3, 101)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(59, 13)
        Me.lblProveedor.TabIndex = 12
        Me.lblProveedor.Text = "Proveedor:"
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 80
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(70, 95)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(434, 27)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 13
        '
        'ocxCuentaFijaVentaDescuento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "ocxCuentaFijaVentaDescuento"
        Me.Size = New System.Drawing.Size(838, 549)
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cbxTipoDescuento As ERP.ocxCBX
    Friend WithEvents lblDescuento As System.Windows.Forms.Label
    Friend WithEvents txtCuenta As ERP.ocxTXTCuentaContable
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblDebeHaber As System.Windows.Forms.Label
    Friend WithEvents cbxDebeHaber As ERP.ocxCBX
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents txtOrden As ERP.ocxTXTNumeric
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents dgvLista As System.Windows.Forms.DataGridView
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxFiltroSucursal As ERP.ocxCBX
    Friend WithEvents txtProveedor As ERP.ocxTXTProveedor
    Friend WithEvents lblProveedor As System.Windows.Forms.Label

End Class

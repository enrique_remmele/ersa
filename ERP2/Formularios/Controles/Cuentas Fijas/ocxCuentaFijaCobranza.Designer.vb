﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxCuentaFijaCobranza
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.OcxCuentaFijaCobranzaVenta1 = New ERP.ocxCuentaFijaCobranzaVenta()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.OcxCuentaFijaCobranzaEfectivo1 = New ERP.ocxCuentaFijaCobranzaEfectivo()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.OcxCuentaFijaCobranzaCheque1 = New ERP.ocxCuentaFijaCobranzaCheque()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.OcxCuentaFijaCobranzaDocumento1 = New ERP.ocxCuentaFijaCobranzaDocumento()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.OcxCuentaFijaCobranzaTarjeta1 = New ERP.ocxCuentaFijaCobranzaTarjeta()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(805, 401)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.OcxCuentaFijaCobranzaVenta1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(797, 375)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Ventas"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'OcxCuentaFijaCobranzaVenta1
        '
        Me.OcxCuentaFijaCobranzaVenta1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxCuentaFijaCobranzaVenta1.Location = New System.Drawing.Point(3, 3)
        Me.OcxCuentaFijaCobranzaVenta1.Name = "OcxCuentaFijaCobranzaVenta1"
        Me.OcxCuentaFijaCobranzaVenta1.Size = New System.Drawing.Size(791, 369)
        Me.OcxCuentaFijaCobranzaVenta1.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.OcxCuentaFijaCobranzaEfectivo1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(797, 375)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Efectivo"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'OcxCuentaFijaCobranzaEfectivo1
        '
        Me.OcxCuentaFijaCobranzaEfectivo1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxCuentaFijaCobranzaEfectivo1.Location = New System.Drawing.Point(3, 3)
        Me.OcxCuentaFijaCobranzaEfectivo1.Name = "OcxCuentaFijaCobranzaEfectivo1"
        Me.OcxCuentaFijaCobranzaEfectivo1.Size = New System.Drawing.Size(791, 369)
        Me.OcxCuentaFijaCobranzaEfectivo1.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.OcxCuentaFijaCobranzaCheque1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(797, 375)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Cheques"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'OcxCuentaFijaCobranzaCheque1
        '
        Me.OcxCuentaFijaCobranzaCheque1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxCuentaFijaCobranzaCheque1.Location = New System.Drawing.Point(0, 0)
        Me.OcxCuentaFijaCobranzaCheque1.Name = "OcxCuentaFijaCobranzaCheque1"
        Me.OcxCuentaFijaCobranzaCheque1.Size = New System.Drawing.Size(192, 74)
        Me.OcxCuentaFijaCobranzaCheque1.TabIndex = 1
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.OcxCuentaFijaCobranzaDocumento1)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(797, 375)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Documentos"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'OcxCuentaFijaCobranzaDocumento1
        '
        Me.OcxCuentaFijaCobranzaDocumento1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxCuentaFijaCobranzaDocumento1.Location = New System.Drawing.Point(0, 0)
        Me.OcxCuentaFijaCobranzaDocumento1.Name = "OcxCuentaFijaCobranzaDocumento1"
        Me.OcxCuentaFijaCobranzaDocumento1.Size = New System.Drawing.Size(192, 74)
        Me.OcxCuentaFijaCobranzaDocumento1.TabIndex = 0
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.OcxCuentaFijaCobranzaTarjeta1)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(797, 375)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Tarjeta"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'OcxCuentaFijaCobranzaTarjeta1
        '
        Me.OcxCuentaFijaCobranzaTarjeta1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxCuentaFijaCobranzaTarjeta1.Location = New System.Drawing.Point(3, 3)
        Me.OcxCuentaFijaCobranzaTarjeta1.Name = "OcxCuentaFijaCobranzaTarjeta1"
        Me.OcxCuentaFijaCobranzaTarjeta1.Size = New System.Drawing.Size(791, 369)
        Me.OcxCuentaFijaCobranzaTarjeta1.TabIndex = 0
        '
        'ocxCuentaFijaCobranza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "ocxCuentaFijaCobranza"
        Me.Size = New System.Drawing.Size(805, 401)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents OcxCuentaFijaCobranzaVenta1 As ERP.ocxCuentaFijaCobranzaVenta
    Friend WithEvents OcxCuentaFijaCobranzaCheque1 As ERP.ocxCuentaFijaCobranzaCheque
    Friend WithEvents OcxCuentaFijaCobranzaEfectivo1 As ERP.ocxCuentaFijaCobranzaEfectivo
    Friend WithEvents OcxCuentaFijaCobranzaDocumento1 As ERP.ocxCuentaFijaCobranzaDocumento
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents OcxCuentaFijaCobranzaTarjeta1 As ERP.ocxCuentaFijaCobranzaTarjeta

End Class

﻿Public Class ocxCuentaFijaGasto

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'EVENTOS

    'PROPIEDADES

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control
    Dim IDOperacion As Integer

    'FUNCIONES
    Sub Inicializar()

        'Controles
        txtCuenta.Conectar()
        IDOperacion = CSistema.ObtenerIDOperacion(frmGastos.Name.ToString, "GASTO", "GAS")

        'Funciones
        CargarInformacion()
        InicializarControles()
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

    End Sub

    Sub CargarInformacion()

        'Cargar controles
        ReDim vControles(-1)

        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, txtCuenta)
        CSistema.CargaControl(vControles, cbxDebeHaber)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, cbxTipo)
        CSistema.CargaControl(vControles, cbxDetalleTipo)
        CSistema.CargaControl(vControles, txtOrden)
        CSistema.CargaControl(vControles, txtDescripcion)
        CSistema.CargaControl(vControles, chkBuscarEnProveedor)

        'DebeHaber
        cbxDebeHaber.cbx.Items.Add("DEBE")
        cbxDebeHaber.cbx.Items.Add("HABER")
        cbxDebeHaber.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Referencia From Moneda")
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Tipo de Cuenta
        CSistema.SqlToComboBox(cbxTipo.cbx, CData.GetTable("VTipoCuentaFijaAgrupado").Copy, "Tipo", "Tipo")

        Listar()

    End Sub

    Sub ObtenerInformacion()

        'Validar
     
        'Si es que se selecciono el registro.
        If lvLista.SelectedItems.Count = 0 Then
            ctrError.SetError(lvLista, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        Dim ID As Integer
        ID = lvLista.SelectedItems(0).SubItems(1).Text

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VCFGasto Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            cbxSucursal.SelectedValue(oRow("IDSucursal").ToString)
            cbxMoneda.SelectedValue(oRow("IDMoneda").ToString)

            txtCuenta.SetValue(oRow("CuentaContable").ToString)
            cbxDebeHaber.cbx.Text = oRow("Debe/Haber").ToString
            cbxTipoComprobante.cbx.Text = oRow("CodigoComprobante").ToString

            cbxTipo.cbx.Text = oRow("TipoCuentaFija").ToString
            cbxDetalleTipo.cbx.Text = oRow("CuentaFija").ToString
            txtOrden.txt.Text = oRow("Orden").ToString
            txtDescripcion.txt.Text = oRow("Descripcion").ToString
            chkBuscarEnProveedor.Valor = oRow("BuscarEnProveedor").ToString


            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        CSistema.SqlToLv(lvLista, "Select 'Codigo'=CuentaContable, ID, Orden, Denominacion, [Debe/Haber], Descripcion, 'T. Comp.'=TipoComprobante From VCFGasto Order By Orden")

        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If lvLista.Columns.Count > 0 Then

            'Esto hacemos para que: 
            '1- Que el ID sea visible en la primera columna
            '2- Y para que cuando el usuario escriba en el lv, el control filtre por su descripcion.
            lvLista.Columns(0).DisplayIndex = 1

            'Ahora seleccionamos automaticamente el registro especificado
            If lvLista.Items.Count > 0 Then
                If ID = 0 Then

                    lvLista.Items(0).Selected = True
                Else
                    For i As Integer = 0 To lvLista.Items.Count - 1
                        If lvLista.Items(i).SubItems(1).Text = ID Then
                            lvLista.Items(i).Selected = True
                            Exit For
                        End If
                    Next

                End If
            End If


        End If


        lvLista.Refresh()

    End Sub

    Sub InicializarControles()

        txtID.Text = CInt(CSistema.ExecuteScalar("Select ISNULL((Max(ID)+1), 1) From CuentaFijaGasto"))
        cbxTipo.cbx.Text = ""
        cbxDebeHaber.cbx.Text = ""
        txtCuenta.LimpiarSeleccion()
        txtOrden.txt.Clear()
      
    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro y sus dependientes. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        Dim param(-1) As SqlClient.SqlParameter

        Dim Debe As Boolean
        Dim Haber As Boolean

        If cbxDebeHaber.cbx.Text = "DEBE" Then
            Debe = True
            Haber = False
        End If

        If cbxDebeHaber.cbx.Text = "HABER" Then
            Debe = False
            Haber = True
        End If

        'Cuenta Fija
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ID", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaContable", txtCuenta.txtCodigo.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Debe", Debe, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Haber", Haber, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Orden", txtOrden.ObtenerValor, ParameterDirection.Input)

        'Cuenta Fija Venta
        CSistema.SetSQLParameter(param, "@BuscarEnProveedor", chkBuscarEnProveedor.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoCuentaFija", cbxDetalleTipo.cbx, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpCFGasto", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            Listar()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If


    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
        txtCuenta.txtCodigo.Focus()

    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        cbxTipo.cbx.Focus()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        'InicializarControles()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub lvLista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvLista.SelectedIndexChanged
        ObtenerInformacion()
    End Sub

    Private Sub txtCuenta_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCuenta.ItemSeleccionado
        cbxDebeHaber.cbx.Focus()
    End Sub
    
    Private Sub ocxCuentaFijaGasto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub cbxTipo_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipo.PropertyChanged

        cbxDetalleTipo.DataSource = Nothing

        If cbxTipo.cbx.SelectedValue Is Nothing Then
            Exit Sub
        End If

        Dim dt As DataTable = CData.GetTable("VTipoCuentaFija")
        dt = CData.FiltrarDataTable(dt, " Tipo='" & cbxTipo.cbx.SelectedValue & "' ")
        CSistema.SqlToComboBox(cbxDetalleTipo.cbx, dt, "ID", "Descripcion")

    End Sub

End Class

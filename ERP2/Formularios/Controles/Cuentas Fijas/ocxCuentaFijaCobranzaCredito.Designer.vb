﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxCuentaFijaCobranzaCredito
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.OcxCuentaFijaCobranzaCreditoVenta1 = New ERP.ocxCuentaFijaCobranzaCreditoVenta()
        Me.OcxCuentaFijaFormaPagoEfectivo1 = New ERP.ocxCuentaFijaFormaPagoEfectivo()
        Me.OcxCuentaFijaFormaPagoCheque1 = New ERP.ocxCuentaFijaFormaPagoCheque()
        Me.OcxCuentaFijaFormaPagoDocumento1 = New ERP.ocxCuentaFijaFormaPagoDocumento()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(691, 375)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.OcxCuentaFijaCobranzaCreditoVenta1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(683, 349)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Ventas"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.OcxCuentaFijaFormaPagoEfectivo1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(683, 349)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Efectivo"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.OcxCuentaFijaFormaPagoCheque1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(683, 349)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Cheques"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.OcxCuentaFijaFormaPagoDocumento1)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(683, 349)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Documentos"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'OcxCuentaFijaCobranzaCreditoVenta1
        '
        Me.OcxCuentaFijaCobranzaCreditoVenta1.Dock = System.Windows.Forms.DockStyle.Top
        Me.OcxCuentaFijaCobranzaCreditoVenta1.Location = New System.Drawing.Point(3, 3)
        Me.OcxCuentaFijaCobranzaCreditoVenta1.Name = "OcxCuentaFijaCobranzaCreditoVenta1"
        Me.OcxCuentaFijaCobranzaCreditoVenta1.Size = New System.Drawing.Size(677, 320)
        Me.OcxCuentaFijaCobranzaCreditoVenta1.TabIndex = 0
        '
        'OcxCuentaFijaFormaPagoEfectivo1
        '
        Me.OcxCuentaFijaFormaPagoEfectivo1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxCuentaFijaFormaPagoEfectivo1.Location = New System.Drawing.Point(3, 3)
        Me.OcxCuentaFijaFormaPagoEfectivo1.Name = "OcxCuentaFijaFormaPagoEfectivo1"
        Me.OcxCuentaFijaFormaPagoEfectivo1.Size = New System.Drawing.Size(677, 343)
        Me.OcxCuentaFijaFormaPagoEfectivo1.TabIndex = 0
        '
        'OcxCuentaFijaFormaPagoCheque1
        '
        Me.OcxCuentaFijaFormaPagoCheque1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxCuentaFijaFormaPagoCheque1.Location = New System.Drawing.Point(0, 0)
        Me.OcxCuentaFijaFormaPagoCheque1.Name = "OcxCuentaFijaFormaPagoCheque1"
        Me.OcxCuentaFijaFormaPagoCheque1.Size = New System.Drawing.Size(683, 349)
        Me.OcxCuentaFijaFormaPagoCheque1.TabIndex = 0
        '
        'OcxCuentaFijaFormaPagoDocumento1
        '
        Me.OcxCuentaFijaFormaPagoDocumento1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxCuentaFijaFormaPagoDocumento1.Location = New System.Drawing.Point(0, 0)
        Me.OcxCuentaFijaFormaPagoDocumento1.Name = "OcxCuentaFijaFormaPagoDocumento1"
        Me.OcxCuentaFijaFormaPagoDocumento1.Size = New System.Drawing.Size(683, 349)
        Me.OcxCuentaFijaFormaPagoDocumento1.TabIndex = 0
        '
        'ocxCuentaFijaCobranzaCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "ocxCuentaFijaCobranzaCredito"
        Me.Size = New System.Drawing.Size(691, 375)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents OcxCuentaFijaCobranzaCreditoVenta1 As ERP.ocxCuentaFijaCobranzaCreditoVenta
    Friend WithEvents OcxCuentaFijaFormaPagoEfectivo1 As ERP.ocxCuentaFijaFormaPagoEfectivo
    Friend WithEvents OcxCuentaFijaFormaPagoCheque1 As ERP.ocxCuentaFijaFormaPagoCheque
    Friend WithEvents OcxCuentaFijaFormaPagoDocumento1 As ERP.ocxCuentaFijaFormaPagoDocumento

End Class

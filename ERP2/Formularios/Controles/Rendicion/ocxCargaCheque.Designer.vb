﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxCargaCheque
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.lklEliminarCheque = New System.Windows.Forms.LinkLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lklVerDetalle = New System.Windows.Forms.LinkLabel()
        Me.btnCargar = New System.Windows.Forms.Button()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotal = New ERP.ocxTXTNumeric()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtTotalDiferidos = New ERP.ocxTXTNumeric()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtTotalOtrosBancos = New ERP.ocxTXTNumeric()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtTotalBancoLocal = New ERP.ocxTXTNumeric()
        Me.lblTotalBancoLocal = New System.Windows.Forms.Label()
        Me.ListView3 = New System.Windows.Forms.ListView()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lklEliminarCheque, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lvLista, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 1, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(665, 446)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'lklEliminarCheque
        '
        Me.lklEliminarCheque.AutoSize = True
        Me.lklEliminarCheque.Location = New System.Drawing.Point(3, 412)
        Me.lklEliminarCheque.Name = "lklEliminarCheque"
        Me.lklEliminarCheque.Size = New System.Drawing.Size(83, 13)
        Me.lklEliminarCheque.TabIndex = 2
        Me.lklEliminarCheque.TabStop = True
        Me.lklEliminarCheque.Text = "Eliminar Cheque"
        '
        'Panel1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.Panel1, 2)
        Me.Panel1.Controls.Add(Me.lklVerDetalle)
        Me.Panel1.Controls.Add(Me.btnCargar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(659, 22)
        Me.Panel1.TabIndex = 0
        '
        'lklVerDetalle
        '
        Me.lklVerDetalle.AutoSize = True
        Me.lklVerDetalle.Location = New System.Drawing.Point(127, 5)
        Me.lklVerDetalle.Name = "lklVerDetalle"
        Me.lklVerDetalle.Size = New System.Drawing.Size(98, 13)
        Me.lklVerDetalle.TabIndex = 3
        Me.lklVerDetalle.TabStop = True
        Me.lklVerDetalle.Text = "Ver detalladamente"
        '
        'btnCargar
        '
        Me.btnCargar.Location = New System.Drawing.Point(3, 0)
        Me.btnCargar.Name = "btnCargar"
        Me.btnCargar.Size = New System.Drawing.Size(118, 22)
        Me.btnCargar.TabIndex = 0
        Me.btnCargar.Text = "Agregar Cheque"
        Me.btnCargar.UseVisualStyleBackColor = True
        '
        'lvLista
        '
        Me.lvLista.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvLista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvLista.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader8})
        Me.TableLayoutPanel1.SetColumnSpan(Me.lvLista, 2)
        Me.lvLista.FullRowSelect = True
        Me.lvLista.GridLines = True
        Me.lvLista.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvLista.HideSelection = False
        Me.lvLista.Location = New System.Drawing.Point(3, 31)
        Me.lvLista.MultiSelect = False
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(659, 378)
        Me.lvLista.TabIndex = 0
        Me.lvLista.UseCompatibleStateImageBehavior = False
        Me.lvLista.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Banco"
        Me.ColumnHeader1.Width = 122
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Nro. Cheque"
        Me.ColumnHeader2.Width = 108
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Fecha"
        Me.ColumnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Comprobante Venta"
        Me.ColumnHeader4.Width = 110
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Diferido"
        Me.ColumnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader5.Width = 50
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Venc."
        Me.ColumnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader6.Width = 61
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Local"
        Me.ColumnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader7.Width = 41
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Importe"
        Me.ColumnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader8.Width = 102
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotal)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label7)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalDiferidos)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label6)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalOtrosBancos)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label8)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalBancoLocal)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblTotalBancoLocal)
        Me.FlowLayoutPanel1.Controls.Add(Me.ListView3)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(93, 415)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(569, 28)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'txtTotal
        '
        Me.txtTotal.Color = System.Drawing.Color.Empty
        Me.txtTotal.Decimales = True
        Me.txtTotal.Indicaciones = Nothing
        Me.txtTotal.Location = New System.Drawing.Point(497, 3)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(69, 22)
        Me.txtTotal.SoloLectura = True
        Me.txtTotal.TabIndex = 7
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Texto = "0"
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(449, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(42, 22)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Total:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTotalDiferidos
        '
        Me.txtTotalDiferidos.Color = System.Drawing.Color.Empty
        Me.txtTotalDiferidos.Decimales = True
        Me.txtTotalDiferidos.Indicaciones = Nothing
        Me.txtTotalDiferidos.Location = New System.Drawing.Point(374, 3)
        Me.txtTotalDiferidos.Name = "txtTotalDiferidos"
        Me.txtTotalDiferidos.Size = New System.Drawing.Size(69, 22)
        Me.txtTotalDiferidos.SoloLectura = True
        Me.txtTotalDiferidos.TabIndex = 5
        Me.txtTotalDiferidos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDiferidos.Texto = "0"
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(317, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 22)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Diferidos:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTotalOtrosBancos
        '
        Me.txtTotalOtrosBancos.Color = System.Drawing.Color.Empty
        Me.txtTotalOtrosBancos.Decimales = True
        Me.txtTotalOtrosBancos.Indicaciones = Nothing
        Me.txtTotalOtrosBancos.Location = New System.Drawing.Point(242, 3)
        Me.txtTotalOtrosBancos.Name = "txtTotalOtrosBancos"
        Me.txtTotalOtrosBancos.Size = New System.Drawing.Size(69, 22)
        Me.txtTotalOtrosBancos.SoloLectura = True
        Me.txtTotalOtrosBancos.TabIndex = 3
        Me.txtTotalOtrosBancos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalOtrosBancos.Texto = "0"
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(196, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(40, 22)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Otros:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTotalBancoLocal
        '
        Me.txtTotalBancoLocal.Color = System.Drawing.Color.Empty
        Me.txtTotalBancoLocal.Decimales = True
        Me.txtTotalBancoLocal.Indicaciones = Nothing
        Me.txtTotalBancoLocal.Location = New System.Drawing.Point(121, 3)
        Me.txtTotalBancoLocal.Name = "txtTotalBancoLocal"
        Me.txtTotalBancoLocal.Size = New System.Drawing.Size(69, 22)
        Me.txtTotalBancoLocal.SoloLectura = True
        Me.txtTotalBancoLocal.TabIndex = 1
        Me.txtTotalBancoLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalBancoLocal.Texto = "0"
        '
        'lblTotalBancoLocal
        '
        Me.lblTotalBancoLocal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalBancoLocal.Location = New System.Drawing.Point(22, 0)
        Me.lblTotalBancoLocal.Name = "lblTotalBancoLocal"
        Me.lblTotalBancoLocal.Size = New System.Drawing.Size(93, 22)
        Me.lblTotalBancoLocal.TabIndex = 0
        Me.lblTotalBancoLocal.Text = "Banco Local:"
        Me.lblTotalBancoLocal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ListView3
        '
        Me.ListView3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListView3.Location = New System.Drawing.Point(-124, 31)
        Me.ListView3.Name = "ListView3"
        Me.ListView3.Size = New System.Drawing.Size(690, 0)
        Me.ListView3.TabIndex = 13
        Me.ListView3.UseCompatibleStateImageBehavior = False
        '
        'ocxCargaCheque
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxCargaCheque"
        Me.Size = New System.Drawing.Size(665, 446)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lvLista As System.Windows.Forms.ListView
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotal As ERP.ocxTXTNumeric
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnCargar As System.Windows.Forms.Button
    Friend WithEvents ListView3 As System.Windows.Forms.ListView
    Friend WithEvents txtTotalDiferidos As ERP.ocxTXTNumeric
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtTotalOtrosBancos As ERP.ocxTXTNumeric
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtTotalBancoLocal As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalBancoLocal As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lklEliminarCheque As System.Windows.Forms.LinkLabel
    Friend WithEvents lklVerDetalle As System.Windows.Forms.LinkLabel

End Class

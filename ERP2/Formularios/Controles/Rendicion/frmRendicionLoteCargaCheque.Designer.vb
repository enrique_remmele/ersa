﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRendicionLoteCargaCheque
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCancelar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.pnlBancoLocal = New System.Windows.Forms.Panel()
        Me.rdbSi = New System.Windows.Forms.RadioButton()
        Me.rdbNo = New System.Windows.Forms.RadioButton()
        Me.lblBancoLocal = New System.Windows.Forms.Label()
        Me.txtFechaDiferido = New ERP.ocxTXTDate()
        Me.chkDiferido = New System.Windows.Forms.CheckBox()
        Me.lblNroCheque = New System.Windows.Forms.Label()
        Me.txtNroCheque = New ERP.ocxTXTString()
        Me.cbxBanco = New ERP.ocxCBX()
        Me.lblBanco = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblSaldo = New System.Windows.Forms.Label()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.txtCantidadCobrado = New ERP.ocxTXTNumeric()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.txtTotalCobrado = New ERP.ocxTXTNumeric()
        Me.lblTotalVentas = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.pnlBancoLocal.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.dgw, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel3, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 58.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 92.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(629, 532)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colSel, Me.colComprobante, Me.colCliente, Me.colTotal, Me.colImporte, Me.colCancelar})
        Me.dgw.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgw.Location = New System.Drawing.Point(3, 61)
        Me.dgw.Name = "dgw"
        Me.dgw.RowHeadersVisible = False
        Me.dgw.Size = New System.Drawing.Size(623, 376)
        Me.dgw.TabIndex = 1
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colSel
        '
        Me.colSel.HeaderText = "Sel"
        Me.colSel.Name = "colSel"
        Me.colSel.ReadOnly = True
        Me.colSel.Width = 30
        '
        'colComprobante
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colComprobante.DefaultCellStyle = DataGridViewCellStyle2
        Me.colComprobante.HeaderText = "Comprobante"
        Me.colComprobante.Name = "colComprobante"
        Me.colComprobante.ReadOnly = True
        Me.colComprobante.ToolTipText = "Comprobante de Venta"
        Me.colComprobante.Width = 95
        '
        'colCliente
        '
        Me.colCliente.HeaderText = "Cliente"
        Me.colCliente.Name = "colCliente"
        Me.colCliente.ReadOnly = True
        Me.colCliente.Width = 270
        '
        'colTotal
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = "0"
        Me.colTotal.DefaultCellStyle = DataGridViewCellStyle3
        Me.colTotal.HeaderText = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.ReadOnly = True
        Me.colTotal.Width = 78
        '
        'colImporte
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = "0"
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle4
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        Me.colImporte.ToolTipText = "Importe Cobrado"
        Me.colImporte.Width = 80
        '
        'colCancelar
        '
        Me.colCancelar.HeaderText = "Cancel"
        Me.colCancelar.Name = "colCancelar"
        Me.colCancelar.ReadOnly = True
        Me.colCancelar.ToolTipText = "Cancelar manualmente el comprobante"
        Me.colCancelar.Width = 35
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblFecha)
        Me.Panel1.Controls.Add(Me.txtFecha)
        Me.Panel1.Controls.Add(Me.txtImporte)
        Me.Panel1.Controls.Add(Me.lblImporte)
        Me.Panel1.Controls.Add(Me.pnlBancoLocal)
        Me.Panel1.Controls.Add(Me.lblBancoLocal)
        Me.Panel1.Controls.Add(Me.txtFechaDiferido)
        Me.Panel1.Controls.Add(Me.chkDiferido)
        Me.Panel1.Controls.Add(Me.lblNroCheque)
        Me.Panel1.Controls.Add(Me.txtNroCheque)
        Me.Panel1.Controls.Add(Me.cbxBanco)
        Me.Panel1.Controls.Add(Me.lblBanco)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(623, 52)
        Me.Panel1.TabIndex = 0
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(209, 7)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 4
        Me.lblFecha.Text = "Fecha:"
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(CType(0, Long))
        Me.txtFecha.Location = New System.Drawing.Point(203, 23)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(99, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 5
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = True
        Me.txtImporte.Indicaciones = Nothing
        Me.txtImporte.Location = New System.Drawing.Point(489, 22)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(89, 22)
        Me.txtImporte.SoloLectura = False
        Me.txtImporte.TabIndex = 10
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(490, 7)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(45, 13)
        Me.lblImporte.TabIndex = 11
        Me.lblImporte.Text = "Importe:"
        '
        'pnlBancoLocal
        '
        Me.pnlBancoLocal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlBancoLocal.Controls.Add(Me.rdbSi)
        Me.pnlBancoLocal.Controls.Add(Me.rdbNo)
        Me.pnlBancoLocal.Location = New System.Drawing.Point(401, 23)
        Me.pnlBancoLocal.Name = "pnlBancoLocal"
        Me.pnlBancoLocal.Size = New System.Drawing.Size(88, 20)
        Me.pnlBancoLocal.TabIndex = 9
        '
        'rdbSi
        '
        Me.rdbSi.AutoSize = True
        Me.rdbSi.Location = New System.Drawing.Point(3, 0)
        Me.rdbSi.Name = "rdbSi"
        Me.rdbSi.Size = New System.Drawing.Size(34, 17)
        Me.rdbSi.TabIndex = 0
        Me.rdbSi.TabStop = True
        Me.rdbSi.Text = "Si"
        Me.rdbSi.UseVisualStyleBackColor = True
        '
        'rdbNo
        '
        Me.rdbNo.AutoSize = True
        Me.rdbNo.Location = New System.Drawing.Point(43, 0)
        Me.rdbNo.Name = "rdbNo"
        Me.rdbNo.Size = New System.Drawing.Size(39, 17)
        Me.rdbNo.TabIndex = 1
        Me.rdbNo.TabStop = True
        Me.rdbNo.Text = "No"
        Me.rdbNo.UseVisualStyleBackColor = True
        '
        'lblBancoLocal
        '
        Me.lblBancoLocal.AutoSize = True
        Me.lblBancoLocal.Location = New System.Drawing.Point(400, 7)
        Me.lblBancoLocal.Name = "lblBancoLocal"
        Me.lblBancoLocal.Size = New System.Drawing.Size(70, 13)
        Me.lblBancoLocal.TabIndex = 8
        Me.lblBancoLocal.Text = "Banco Local:"
        '
        'txtFechaDiferido
        '
        Me.txtFechaDiferido.Color = System.Drawing.Color.Empty
        Me.txtFechaDiferido.Enabled = False
        Me.txtFechaDiferido.Fecha = New Date(CType(0, Long))
        Me.txtFechaDiferido.Location = New System.Drawing.Point(302, 23)
        Me.txtFechaDiferido.Name = "txtFechaDiferido"
        Me.txtFechaDiferido.PermitirNulo = False
        Me.txtFechaDiferido.Size = New System.Drawing.Size(99, 20)
        Me.txtFechaDiferido.SoloLectura = False
        Me.txtFechaDiferido.TabIndex = 7
        '
        'chkDiferido
        '
        Me.chkDiferido.AutoSize = True
        Me.chkDiferido.Location = New System.Drawing.Point(303, 5)
        Me.chkDiferido.Name = "chkDiferido"
        Me.chkDiferido.Size = New System.Drawing.Size(65, 17)
        Me.chkDiferido.TabIndex = 6
        Me.chkDiferido.Text = "Diferido:"
        Me.chkDiferido.UseVisualStyleBackColor = True
        '
        'lblNroCheque
        '
        Me.lblNroCheque.AutoSize = True
        Me.lblNroCheque.Location = New System.Drawing.Point(113, 7)
        Me.lblNroCheque.Name = "lblNroCheque"
        Me.lblNroCheque.Size = New System.Drawing.Size(70, 13)
        Me.lblNroCheque.TabIndex = 2
        Me.lblNroCheque.Text = "Nro. Cheque:"
        '
        'txtNroCheque
        '
        Me.txtNroCheque.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroCheque.Color = System.Drawing.Color.Empty
        Me.txtNroCheque.Indicaciones = Nothing
        Me.txtNroCheque.Location = New System.Drawing.Point(111, 23)
        Me.txtNroCheque.Multilinea = False
        Me.txtNroCheque.Name = "txtNroCheque"
        Me.txtNroCheque.Size = New System.Drawing.Size(92, 21)
        Me.txtNroCheque.SoloLectura = False
        Me.txtNroCheque.TabIndex = 3
        Me.txtNroCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroCheque.Texto = ""
        '
        'cbxBanco
        '
        Me.cbxBanco.CampoWhere = Nothing
        Me.cbxBanco.DataDisplayMember = Nothing
        Me.cbxBanco.DataFilter = Nothing
        Me.cbxBanco.DataOrderBy = Nothing
        Me.cbxBanco.DataSource = Nothing
        Me.cbxBanco.DataValueMember = Nothing
        Me.cbxBanco.FormABM = Nothing
        Me.cbxBanco.Indicaciones = Nothing
        Me.cbxBanco.Location = New System.Drawing.Point(9, 23)
        Me.cbxBanco.Name = "cbxBanco"
        Me.cbxBanco.SeleccionObligatoria = False
        Me.cbxBanco.Size = New System.Drawing.Size(102, 21)
        Me.cbxBanco.SoloLectura = False
        Me.cbxBanco.TabIndex = 1
        Me.cbxBanco.Texto = ""
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(9, 7)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(41, 13)
        Me.lblBanco.TabIndex = 0
        Me.lblBanco.Text = "Banco:"
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.lblSaldo)
        Me.Panel3.Controls.Add(Me.txtSaldo)
        Me.Panel3.Controls.Add(Me.StatusStrip1)
        Me.Panel3.Controls.Add(Me.txtCantidadCobrado)
        Me.Panel3.Controls.Add(Me.btnCancelar)
        Me.Panel3.Controls.Add(Me.btnAceptar)
        Me.Panel3.Controls.Add(Me.txtTotalCobrado)
        Me.Panel3.Controls.Add(Me.lblTotalVentas)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 443)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(623, 86)
        Me.Panel3.TabIndex = 2
        '
        'lblSaldo
        '
        Me.lblSaldo.AutoSize = True
        Me.lblSaldo.Location = New System.Drawing.Point(400, 8)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(77, 13)
        Me.lblSaldo.TabIndex = 3
        Me.lblSaldo.Text = "Saldo Cheque:"
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(476, 3)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(115, 22)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 4
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 62)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(621, 22)
        Me.StatusStrip1.TabIndex = 7
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'txtCantidadCobrado
        '
        Me.txtCantidadCobrado.Color = System.Drawing.Color.Empty
        Me.txtCantidadCobrado.Decimales = True
        Me.txtCantidadCobrado.Indicaciones = Nothing
        Me.txtCantidadCobrado.Location = New System.Drawing.Point(346, 3)
        Me.txtCantidadCobrado.Name = "txtCantidadCobrado"
        Me.txtCantidadCobrado.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadCobrado.SoloLectura = False
        Me.txtCantidadCobrado.TabIndex = 2
        Me.txtCantidadCobrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadCobrado.Texto = "0"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(516, 31)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 6
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(425, 31)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(85, 23)
        Me.btnAceptar.TabIndex = 5
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'txtTotalCobrado
        '
        Me.txtTotalCobrado.Color = System.Drawing.Color.Empty
        Me.txtTotalCobrado.Decimales = True
        Me.txtTotalCobrado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalCobrado.Indicaciones = Nothing
        Me.txtTotalCobrado.Location = New System.Drawing.Point(225, 3)
        Me.txtTotalCobrado.Name = "txtTotalCobrado"
        Me.txtTotalCobrado.Size = New System.Drawing.Size(115, 22)
        Me.txtTotalCobrado.SoloLectura = True
        Me.txtTotalCobrado.TabIndex = 1
        Me.txtTotalCobrado.TabStop = False
        Me.txtTotalCobrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCobrado.Texto = "0"
        '
        'lblTotalVentas
        '
        Me.lblTotalVentas.AutoSize = True
        Me.lblTotalVentas.Location = New System.Drawing.Point(149, 8)
        Me.lblTotalVentas.Name = "lblTotalVentas"
        Me.lblTotalVentas.Size = New System.Drawing.Size(70, 13)
        Me.lblTotalVentas.TabIndex = 0
        Me.lblTotalVentas.Text = "Total Ventas:"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmRendicionLoteCargaCheque
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(629, 532)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmRendicionLoteCargaCheque"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmRendicionLoteCargaCheque"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.pnlBancoLocal.ResumeLayout(False)
        Me.pnlBancoLocal.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents pnlBancoLocal As System.Windows.Forms.Panel
    Friend WithEvents rdbSi As System.Windows.Forms.RadioButton
    Friend WithEvents rdbNo As System.Windows.Forms.RadioButton
    Friend WithEvents lblBancoLocal As System.Windows.Forms.Label
    Friend WithEvents txtFechaDiferido As ERP.ocxTXTDate
    Friend WithEvents chkDiferido As System.Windows.Forms.CheckBox
    Friend WithEvents lblNroCheque As System.Windows.Forms.Label
    Friend WithEvents txtNroCheque As ERP.ocxTXTString
    Friend WithEvents cbxBanco As ERP.ocxCBX
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents txtImporte As ERP.ocxTXTNumeric
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidadCobrado As ERP.ocxTXTNumeric
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents txtTotalCobrado As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalVentas As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents txtSaldo As ERP.ocxTXTNumeric
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents colIDTransaccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSel As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCancelar As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class

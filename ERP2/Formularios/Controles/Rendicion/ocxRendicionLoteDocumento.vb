﻿Public Class ocxRendicionLoteDocumento

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private dtDetalleDocumentoValue As DataTable
    Public Property dtDetalleDocumento() As DataTable
        Get
            Return dtDetalleDocumentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleDocumentoValue = value
        End Set
    End Property

    Private dtDetalleLoteValue As DataTable
    Public Property dtDetalleLote() As DataTable
        Get
            Return dtDetalleLoteValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleLoteValue = value
        End Set
    End Property

    Private TotalValue As Decimal
    Public Property Total() As Decimal
        Get
            Return TotalValue
        End Get
        Set(ByVal value As Decimal)
            TotalValue = value
        End Set
    End Property

    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value
            btnAgregar.Enabled = Not value
            btnEliminar.Enabled = Not value
            Me.TabStop = Not value
        End Set

    End Property

    'EVENTOS
    Public Event ErrorDetectado(ByVal sender As Object, ByVal e As EventArgs, ByVal mensaje As String)
    Public Event UpdateTotal(ByVal sender As Object, ByVal e As EventArgs)

    'FUNCIONES
    Public Sub Inicializar()

        'Estructura del detalle
        dtDetalleDocumento = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleRendicionLoteDocumento  Order By NroComprobante").Copy
        dtDetalleDocumento.Columns.Add("Sel")

        'Inicializar valores extras
        For Each oRow As DataRow In dtDetalleDocumento.Rows
            oRow("Sel") = False
        Next

    End Sub

    Private Sub CargarDocumento()

        Try
            Dim frm As New frmRendicionLoteCargaDocumento

            'Comprobantes de venta del Lote
            frm.dtComprobantes = dtDetalleLote.Copy
            frm.dtDocumentos = dtDetalleDocumento.Copy

            frm.Inicializar()
            frm.ShowDialog(Me)

            If frm.RegistroCompletado = True Then

                dtDetalleDocumento.Rows.Clear()

                For Each oRow As DataRow In frm.dtDocumentos.Rows
                    dtDetalleDocumento.Rows.Add(oRow.ItemArray)
                Next

                ListarDocumentos()

            End If

        Catch ex As Exception
            CSistema.CargarError(ex, Me.Name, "CargarDocuemento")
        End Try


    End Sub

    Private Sub ListarDocumentos()

        lvLista.Items.Clear()

        For Each oRow As DataRow In dtDetalleDocumento.Rows
            Dim item As ListViewItem = lvLista.Items.Add(oRow("Tipo").ToString)
            item.SubItems.Add(oRow("Nro").ToString)
            item.SubItems.Add(oRow("Venta").ToString)
            item.SubItems.Add(oRow("Importe").ToString)
            item.SubItems.Add(oRow("Aplica").ToString)
        Next

        CSistema.FormatoMoneda(lvLista, 3)

        CalcularTotales()

    End Sub

    Public Sub CargarComprobantesLote()


        dtDetalleLote.Columns.Add("Sel")
        dtDetalleLote.Columns.Add("Importe")
        dtDetalleLote.Columns.Add("Cancelar")
        dtDetalleLote.Columns.Add("Aplicar")
        dtDetalleLote.Columns.Add("Aplica")
        For Each oRow As DataRow In dtDetalleLote.Rows
            oRow("Sel") = False
            oRow("Cancelar") = False
            oRow("Importe") = oRow("Saldo")
            oRow("Aplicar") = True
            oRow("Aplica") = "SI"
        Next

    End Sub

    Private Sub CalcularTotales()

        Total = CSistema.dtSumColumn(dtDetalleDocumento, "Importe")
        txtTotal.SetValue(Total)

        RaiseEvent UpdateTotal(New Object, New EventArgs)

    End Sub

    Sub Limpiar()

        IDTransaccion = 0
        Total = 0
        dtDetalleDocumento.Rows.Clear()
        ListarDocumentos()

    End Sub

    Sub EliminarRegistros()

        If lvLista.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        For Each item As ListViewItem In lvLista.SelectedItems

            Dim Tipo As String = item.SubItems(0).Text
            Dim Nro As String = item.SubItems(1).Text
            Dim Venta As String = item.SubItems(2).Text

            Dim Where As String = " Tipo = '" & Tipo & "' And Nro = '" & Nro & "' And Venta = '" & Venta & "' "
            For Each oRow As DataRow In dtDetalleDocumento.Select(Where)
                dtDetalleDocumento.Rows.Remove(oRow)
            Next

        Next

        ListarDocumentos()

    End Sub

    Public Sub Guardar(ByVal vIDTransaccion As Integer)

        IDTransaccion = vIDTransaccion

        Dim i As Integer = 1

        For Each oRow As DataRow In dtDetalleDocumento.Rows

            Dim param(-1) As SqlClient.SqlParameter

            'Entrada
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", i, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TipoComprobante", oRow("TipoComprobante"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroComprobante", oRow("NroComprobante"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccionVenta", oRow("IDTransaccionVenta"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Importe", oRow("Importe"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Aplicar", oRow("Aplicar"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Observacion", oRow("Observacion").ToString, ParameterDirection.Input)

            'Operacion
            CSistema.SetSQLParameter(param, "@Operacion", ERP.CSistema.NUMOperacionesRegistro.INS.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""
            If CSistema.ExecuteStoreProcedure(param, "SpDetalleRendicionLoteDocumento", False, False, MensajeRetorno) = False Then

            End If

            i = i + 1

        Next

    End Sub

    Public Sub CargarDatos(ByVal vIDTransaccion As Integer)

        IDTransaccion = vIDTransaccion
        dtDetalleDocumento = CSistema.ExecuteToDataTable("Select * From VDetalleRendicionLoteDocumento Where IDTransaccion=" & IDTransaccion).Copy
        ListarDocumentos()

    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        CargarDocumento()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        EliminarRegistros()
    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvLista.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarRegistros()
        End If
    End Sub

End Class

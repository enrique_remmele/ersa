﻿Public Class frmRendicionLoteCargaCheque

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private ComprobanteValue As String
    Public Property Comprobante() As String
        Get
            Return ComprobanteValue
        End Get
        Set(ByVal value As String)
            ComprobanteValue = value
        End Set
    End Property

    Private dtComprobantesValue As DataTable
    Public Property dtComprobantes() As DataTable
        Get
            Return dtComprobantesValue
        End Get
        Set(ByVal value As DataTable)
            dtComprobantesValue = value
        End Set
    End Property

    Private dtChequesValue As DataTable
    Public Property dtCheques() As DataTable
        Get
            Return dtChequesValue
        End Get
        Set(ByVal value As DataTable)
            dtChequesValue = value
        End Set
    End Property

    Private RegistroCompletadoValue As Boolean
    Public Property RegistroCompletado() As Boolean
        Get
            Return RegistroCompletadoValue
        End Get
        Set(ByVal value As Boolean)
            RegistroCompletadoValue = value
        End Set
    End Property

    'VARIABLES
    Dim BancoLocal As String

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Form
        Me.Text = "Cheques para el Lote: " & Comprobante

        'Funciones
        CargarInformacion()
        ListarComprobantes()

        RegistroCompletado = False

        'Foco
        cbxBanco.cbx.Focus()

    End Sub

    Sub CargarInformacion()

        'Bancos
        CSistema.SqlToComboBox(cbxBanco, "Select ID, Descripcion From Banco Order By 2")

        cbxBanco.cbx.Text = CArchivoInicio.IniGet(Me.Name.ToString, "CARGA CHEQUE RENDICION", "BANCO", "")
        BancoLocal = CArchivoInicio.IniGet(Me.Name.ToString, "CARGA CHEQUE RENDICION", "BANCO LOCAL", "")

        If BancoLocal <> "" Then

            If BancoLocal.Trim = cbxBanco.cbx.Text.Trim Then
                rdbSi.Checked = True
            Else
                rdbNo.Checked = True
            End If
        Else
            rdbNo.Checked = True
        End If

    End Sub

    Sub GuardarInformacion()

        'BANCO
        CArchivoInicio.IniWrite(Me.Name.ToString, "CARGA CHEQUE RENDICION", "BANCO", cbxBanco.cbx.Text)

        'BANCO LOCAL
        If rdbSi.Checked = True Then
            BancoLocal = cbxBanco.cbx.Text
            CArchivoInicio.IniWrite(Me.Name.ToString, "CARGA CHEQUE RENDICION", "BANCO LOCAL", BancoLocal)
        End If

    End Sub

    Sub ListarComprobantes()

        dgw.Rows.Clear()
        For Each oRow As DataRow In dtComprobantes.Rows

            Dim Registro(6) As String
            Registro(0) = oRow("IDTransaccionVenta").ToString
            Registro(1) = oRow("Sel").ToString
            Registro(2) = oRow("Comprobante").ToString
            Registro(3) = oRow("Cliente").ToString
            Registro(4) = CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(5) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(6) = oRow("Cancelar").ToString

            dgw.Rows.Add(Registro)

        Next

    End Sub

    Public Sub CargarComprobantes()


    End Sub

    Private Sub CalcularTotales()

        'variables
        Dim TotalVentas = 0
        Dim TotalCheque = 0
        Dim Saldo As Decimal = 0
        Dim CantidadComprobante As Integer = 0
        Dim Seleccionado As Boolean = False

        'Poner seleccionado
        For index As integer =0 to dgw.RowCount - 1

            Seleccionado = dgw.Rows(index).Cells(1).Value

            Dim IDTransaccion As Integer = dgw.Rows(index).Cells(0).Value

            For Each orow As DataRow In dtComprobantes.Select(" IDTransaccionVenta = " & IDTransaccion)
                orow("Sel") = Seleccionado

                'Importe
                orow("Importe") = dgw.Rows(index).Cells(5).Value
                orow("Total") = dgw.Rows(index).Cells(4).Value
            Next

        Next

        'Sumar Ventas
        For Each orow As DataRow In dtComprobantes.Select(" Sel = 'True' ")
            TotalVentas = TotalVentas + orow("Importe")
            CantidadComprobante = CantidadComprobante + 1
        Next

        TotalCheque = txtImporte.ObtenerValor

        'Calucluar Saldo
        Saldo = TotalCheque - TotalVentas

        'Valores
        txtTotalCobrado.txt.Text = TotalVentas
        txtCantidadCobrado.txt.Text = CantidadComprobante
        txtSaldo.txt.Text = Saldo

    End Sub

    Private Sub Procesar()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Validar
        If cbxBanco.cbx.Text.Trim = "" Then
            CSistema.MostrarError("Seleccione correctamente el banco!", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        If IsNumeric(cbxBanco.cbx.SelectedValue) = False Then
            CSistema.MostrarError("Seleccione correctamente el banco!", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'Nro de Cheque
        If txtNroCheque.txt.Text.Trim = "" Then
            CSistema.MostrarError("El Nro del cheque no es correcto!", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'Importe
        If txtImporte.ObtenerValor = 0 Then
            CSistema.MostrarError("El importe del cheque no es correcto!", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        Dim i As Integer = 0

        For Each oRow As DataRow In dtComprobantes.Select(" Sel = 'True' ")

            If oRow("Importe") > oRow("Total") Then
                Dim mensaje As String = "El Importe debe ser menor al Total!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            Else
                tsslEstado.Text = ""
            End If


            Dim NewRow As DataRow = dtCheques.NewRow

            NewRow("IDTransaccion") = IDTransaccion
            NewRow("ID") = i
            NewRow("Banco") = cbxBanco.cbx.Text
            NewRow("NroCheque") = txtNroCheque.txt.Text
            NewRow("Fecha") = txtFecha.GetValue
            NewRow("Fec.") = FormatDateTime(txtFecha.GetValue, DateFormat.ShortDate)

            NewRow("Diferido") = chkDiferido.Checked
            If chkDiferido.Checked = True Then
                NewRow("Dif") = "SI"
                NewRow("FechaVencimiento") = txtFechaDiferido.GetValue
                NewRow("Fec Venc.") = FormatDateTime(txtFechaDiferido.GetValue, DateFormat.ShortDate)
            Else
                NewRow("Dif") = "NO"
                NewRow("FechaVencimiento") = Date.Now
                NewRow("Fec Venc.") = "---"
            End If

            NewRow("BancoLocal") = rdbSi.Checked
            If rdbSi.Checked = True Then
                NewRow("Local") = "SI"
            Else
                NewRow("Local") = "NO"
            End If

            NewRow("IDTransaccionVenta") = oRow("IDTransaccionVenta")
            NewRow("Comprobante") = oRow("Comprobante")
            NewRow("Tipo") = ""
            NewRow("Cliente") = oRow("Cliente")
            NewRow("Saldo") = oRow("Saldo")
            NewRow("Credito") = oRow("Credito")
            NewRow("Importe") = oRow("Importe")

            dtCheques.Rows.Add(NewRow)

            i = i + 1

        Next

        RegistroCompletado = True
        Me.Close()

    End Sub

    Private Sub cbxBanco_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxBanco.Leave
        GuardarInformacion()
    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick
        ctrError.Clear()
        tsslEstado.Text = ""

        Dim Importe As Decimal = txtImporte.ObtenerValor

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        If e.ColumnIndex = dgw.Columns.Item("colSel").Index Then

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(5).ReadOnly = False
                        oRow.Cells(6).ReadOnly = False
                        oRow.Cells("colImporte").Value = Importe
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(5)

                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(5).ReadOnly = True
                        oRow.Cells(6).ReadOnly = True
                        oRow.Cells(5).Value = oRow.Cells(4).Value
                        oRow.Cells("colImporte").Value = oRow.Cells("colTotal").Value
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

        End If
    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit
        If e.ColumnIndex = 5 Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells(4).Value

            Else
                dgw.CurrentCell.Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells(5).Value)
            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Space Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim Importe As Decimal = txtImporte.ObtenerValor
            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(5).ReadOnly = False
                        oRow.Cells(6).ReadOnly = False
                        oRow.Cells("colImporte").Value = Importe
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(5)

                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(5).ReadOnly = True
                        oRow.Cells(6).ReadOnly = True
                        oRow.Cells(5).Value = oRow.Cells(4).Value
                        oRow.Cells(1).Value = False
                        oRow.Cells("colImporte").Value = oRow.Cells("colTotal").Value
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colImporte")
            End If

            CalcularTotales()

            dgw.Update()

        

            ' Your code here
            e.SuppressKeyPress = True

        End If

    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Index = e.RowIndex Then
                If oRow.Cells(1).Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                End If

                oRow.DefaultCellStyle.Font = f1

                oRow.Cells(5).Selected = True

            Else

                oRow.DefaultCellStyle.Font = f2

                If oRow.Cells(1).Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.White
                Else
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                End If
            End If
        Next
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells(5)
        End If

    End Sub

    Private Sub chkDiferido_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDiferido.CheckedChanged
        If chkDiferido.Checked = True Then
            txtFechaDiferido.Enabled = True
        Else
            txtFechaDiferido.Enabled = False
        End If
    End Sub

    Private Sub cbxBanco_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxBanco.PropertyChanged

        If BancoLocal <> "" Then

            If BancoLocal.Trim = cbxBanco.cbx.Text.Trim Then
                rdbSi.Checked = True
            Else
                rdbNo.Checked = True
            End If
        Else
            rdbNo.Checked = True
        End If

    End Sub

    Private Sub frmRendicionLoteCargaCheque_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub txtImporte_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtImporte.Leave
        CalcularTotales()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Procesar()
    End Sub

    Private Sub frmRendicionLoteCargaCheque_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class
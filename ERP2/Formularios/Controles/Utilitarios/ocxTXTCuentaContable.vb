﻿Public Class ocxTXTCuentaContable

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private RegistroValue As DataRow
    Public Property Registro() As DataRow
        Get
            Return RegistroValue
        End Get
        Set(ByVal value As DataRow)
            RegistroValue = value
        End Set
    End Property

    Private SeleccionadoValue As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionadoValue = value
        End Set
    End Property

    Private AlturaMaximaValue As Integer
    Public Property AlturaMaxima() As Integer
        Get
            Return AlturaMaximaValue
        End Get
        Set(ByVal value As Integer)
            AlturaMaximaValue = value
        End Set
    End Property

    Private ListarTodasValue As Boolean
    Public Property ListarTodas() As Boolean
        Get
            Return ListarTodasValue
        End Get
        Set(ByVal value As Boolean)
            ListarTodasValue = value
        End Set
    End Property

    Private Resolucion173Value As Boolean
    Public Property Resolucion173() As Boolean
        Get
            Return Resolucion173Value
        End Get
        Set(ByVal value As Boolean)
            Resolucion173Value = value
        End Set
    End Property

    Private TextoValue As String
    Public Property Texto() As String
        Get
            Return TextoValue
        End Get
        Set(ByVal value As String)
            TextoValue = value
            txtDescripcion.Text = value
        End Set
    End Property

    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value
            txtDescripcion.SoloLectura = value
            txtCodigo.SoloLectura = value
        End Set

    End Property

    Private ConsultaValue As String
    Public Property Consulta() As String
        Get
            Return ConsultaValue
        End Get
        Set(ByVal value As String)
            ConsultaValue = value
        End Set
    End Property

    'EVENTOS
    Public Event ItemSeleccionado(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ItemMalSeleccionado(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ItemInicializado(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Private dt As New DataTable
    Private dtTemp As New DataTable
    Private BindingSource1 As New BindingSource
    Private vTabla As String = "VCuentaContable"
    Private vTablaSp As String = "SpViewCuentaContable"

    Sub Inicializar()


        'Controles
        txtDescripcion.ResetText()
        txtCodigo.ResetText()

        grid.DataSource = Nothing
        grid.Visible = False

        'Funciones
        Conectar()

    End Sub

    Public Sub Conectar()

        Dim Where As String = " Activo='True' "

        If ListarTodas = False Then
            Where = Where & " And Imputable='True' "
        End If

        If Resolucion173 = True Then
            Where = Where & " And Resolucion173='True' "
        Else
            Where = Where & " And PlanCuentaTitular='True' "
        End If

        dt = CData.GetTable(vTabla).Copy
        dt = CData.FiltrarDataTable(dt, Where).Copy

        BindingSource1.DataSource = dt

    End Sub

    Public Sub Conectar(ByVal sql As String)

        dt = CSistema.ExecuteToDataTable(sql)
        BindingSource1.DataSource = dt

    End Sub

    Public Sub Conectar(ByVal dtTemp As DataTable)

        dt = dtTemp.Copy
        BindingSource1.DataSource = dt

    End Sub

    Private Sub Listar(ByVal ctr As TextBox, ByVal condicion As String)

        'Validar
        If ctr.Focused = False Then
            Exit Sub
        End If

        If ctr.Text = "" Then
            OcultarLista()
            Exit Sub
        End If

        Me.BringToFront()

        Seleccionado = False

        ConfigurarTamaño()

        Dim Where As String = ""
        Dim Valor As String = ctr.Text.Trim

        Try

            Select Case UCase(condicion)

                Case "CODIGO"

                    Where = " CODIGO = '" & ctr.Text.Trim & "' "

                Case "DESCRIPCION"

                    Where = " DESCRIPCION Like '%" & ctr.Text.Trim & "%' "

                Case Else

            End Select


            BindingSource1.Filter = Where
            grid.DataSource = BindingSource1.DataSource

            grid.Visible = True
            If AlturaMaxima > Math.Round(grid.RowCount * 20, 0) Then
                grid.Height = Math.Round(grid.RowCount * 20, 0)
            Else
                grid.Height = AlturaMaxima
            End If

            grid.BringToFront()
            Me.Height = grid.Height + TableLayoutPanel1.Height

            'Ocultar todas las columnas
            For i As Integer = 0 To grid.ColumnCount - 1
                grid.Columns(i).Visible = False
            Next

            If grid.Columns.Count > 0 Then
                grid.Columns(6).Visible = True
                grid.Columns(7).Visible = True
                grid.Columns(6).Width = txtCodigo.Width + 6
                grid.Columns(7).Width = txtDescripcion.Width + 6
            End If

            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.None

        Catch ex As Exception

        End Try



    End Sub

    Private Sub OcultarLista()

        grid.Visible = False
        Me.Height = TableLayoutPanel1.Height

    End Sub

    Public Sub LimpiarSeleccion()

        Seleccionado = False
        OcultarLista()
        txtCodigo.txt.Clear()
        txtDescripcion.txt.Clear()
        RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
        Registro = Nothing

    End Sub

    Public Sub Clear()

        txtCodigo.txt.Clear()
        txtDescripcion.txt.Clear()

        LimpiarSeleccion()

    End Sub

    Private Sub Seleccionar(Optional ByVal GridFocus As Boolean = False)

        If SoloLectura = True Then
            Exit Sub
        End If

        'Validar
        'If grid.RowCount = 0 Then
        '    LimpiarSeleccion()
        '    RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
        '    Exit Sub
        'End If

        'Si ya esta seleccionado
        If Seleccionado = True Then

            If Me.ActiveControl Is Nothing Then
                Me.ActiveControl = txtCodigo
                Exit Sub
            End If

            If Me.ActiveControl.Name = "txtDescripcion" Then
                RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
                Exit Sub
            Else
                txtDescripcion.txt.Focus()
            End If

            Exit Sub

        End If

        'Si los controles estan vacios
        If txtDescripcion.txt.Text = "" And txtCodigo.txt.Text = "" Then
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
            Exit Sub
        End If

        If GridFocus = True Then
            Exit Sub
        End If

        SeleccionarRegistro()

    End Sub

    Private Sub SeleccionarGrid()

        'Validar
        If grid.RowCount = 0 Then
            LimpiarSeleccion()
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
            Exit Sub
        End If

        'Si ya esta seleccionado
        If Seleccionado = True Then
            Exit Sub
        End If

        SeleccionarRegistro()

    End Sub

    Sub SeleccionarRegistro(Optional ByVal IDCuenta As String = "")

        Try

            Dim ID As Integer

            If IDCuenta = "" Then
                ID = grid.SelectedRows(0).Cells("Codigo").Value
            Else
                ID = IDCuenta
            End If

            Dim oRow As DataRow = dt.Select(" Codigo='" & ID & "' ")(0)

            Registro = oRow
            txtCodigo.txt.Text = oRow("Codigo").ToString
            txtDescripcion.txt.Text = oRow("Descripcion").ToString

            OcultarLista()
            Seleccionado = True
            RaiseEvent ItemSeleccionado(New Object, New EventArgs)

        Catch ex As Exception
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
        End Try

    End Sub

    Private Sub ConfigurarTamaño()

        'Tañanos
        'txtCodigo.Width = TableLayoutPanel1.Width
        grid.Width = TableLayoutPanel1.Width - 3

        If grid.Visible = False Then
            Me.Height = TableLayoutPanel1.Height
        End If

        'Localizacion
        grid.SendToBack()
        grid.Top = TableLayoutPanel1.Height - 4
        grid.Left = TableLayoutPanel1.Left + 3

    End Sub

    Public Sub SetValue(ByVal ID As String)

        Try

            Registro = Nothing
            txtCodigo.txt.Text = ""
            txtDescripcion.txt.Text = ""

            If dt.Select(" Codigo='" & ID & "'").Count = 0 Then

                OcultarLista()
                Seleccionado = False
                RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)

            Else

                Dim oRow As DataRow = dt.Select(" Codigo='" & ID & "'")(0)

                Registro = oRow
                txtCodigo.txt.Text = oRow("Codigo").ToString
                txtDescripcion.txt.Text = oRow("Descripcion").ToString

                OcultarLista()
                Seleccionado = True
                RaiseEvent ItemSeleccionado(New Object, New EventArgs)

            End If

        Catch ex As Exception
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
        End Try


    End Sub

    Public Sub SetMaxValue()

        Try
            Dim Codigo As Integer
            Dim Filtro As String = " Categoria = 1 "
            If ListarTodas = False Then
                Filtro = Filtro & " And Imputable='True' "
            End If

            For Each oRow As DataRow In dt.Select(Filtro)
                If IsNumeric(oRow("Codigo").ToString) Then
                    If CInt(oRow("Codigo")) > Codigo Then
                        Codigo = oRow("Codigo")
                    End If
                End If
            Next

            SetValue(Codigo.ToString)

        Catch ex As Exception
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
        End Try


    End Sub

    Private Sub MostrarPropiedades()

        Dim ID As Integer
        Dim Cuenta As String = ""

        If Seleccionado = False Then

            If grid.RowCount = 0 Then
                Exit Sub
            End If

            If grid.SelectedRows.Count = 0 Then
                Exit Sub
            End If

            ID = grid.SelectedRows(0).Cells(0).Value
            Cuenta = grid.SelectedRows(0).Cells(8).Value
        Else
            ID = Registro("ID").ToString
            Cuenta = Registro("Cuenta").ToString
        End If



        Dim frm As New frmPropiedad
        frm.TituloVentana = "Propiedades de Cuenta Contable"
        frm.Titulo = Cuenta
        frm.dt = CData.FiltrarDataTable(dt, "ID = " & ID).Copy
        frm.ShowDialog()

    End Sub

    Private Sub MostrarConsulta()

        Dim frm As New frmCuentaBuscar
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.WindowState = FormWindowState.Maximized
        frm.ShowDialog()

        If frm.ID = 0 Then
            Exit Sub
        End If

        'Si no existe en el DATATABLE, agregar
        If CData.GetTable(vTabla).Select("ID=" & frm.ID).Count = 0 Then

            'Actualizamos el Registro
            CData.Insertar(frm.ID, vTabla)

            dt = CData.GetTable(vTabla)
            BindingSource1.DataSource = dt

        End If

        SetValue(frm.ID)

    End Sub

    Public Function IsFocus() As Boolean

        IsFocus = False

        If txtCodigo.txt.Focused = True Then
            Return True
        End If

        If txtDescripcion.txt.Focused = True Then
            Return True
        End If

    End Function

    Public Sub SetFocus()
        txtCodigo.txt.Focus()
        txtCodigo.txt.SelectAll()
        If txtCodigo.txt.Focused = False Then
            txtCodigo.txt.Focus()
        End If

    End Sub


    Private Sub ManejoTecla(ByVal text As TextBox, ByVal e As System.Windows.Forms.KeyEventArgs, ByVal Condicion As String)

        'Si es solo lectura, salir
        If SoloLectura = True Then
            GoTo pasar
        End If

        If e.KeyCode = Keys.Down Then
            If grid.RowCount > 0 Then
                grid.Focus()
                If grid.SelectedRows.Count = 0 Then
                    grid.Rows(0).Selected = True
                End If
            End If

            Exit Sub

        End If

        If e.KeyCode = Keys.Enter Then
            If text.Text.Trim = "" Then
                If txtDescripcion.txt.Focused = True Then
                    Me.ParentForm.SelectNextControl(Me, True, True, True, True)
                Else
                    Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
                End If
            Else
                Seleccionar()
            End If
            Exit Sub
        End If


        If e.KeyCode = Keys.ShiftKey Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Tab Then
            If Seleccionado = False Then
                Seleccionar()
            End If
            Exit Sub
        End If

        If e.KeyCode = vgKeyConsultar Then
            If SoloLectura = False Then
                MostrarConsulta()
            End If
            Exit Sub
        End If

        If text.Text = "" Then
            LimpiarSeleccion()
            Exit Sub
        End If


        Listar(text, Condicion)

pasar:

        If e.KeyCode = vgKeyVerInformacionDetallada Then
            MostrarPropiedades()
            Exit Sub
        End If


        If e.KeyCode = vgKeyActualizarTabla Then

            CData.ResetTable(vTabla, "Exec " & vTablaSp & " @IDUsuario = " & vgIDUsuario & ", @IDSucursal = " & vgIDSucursal & ", @IDDeposito = " & vgIDDeposito & ", @IDTerminal = " & vgIDTerminal & " ")
            dt = CData.GetTable(vTabla)
            BindingSource1.DataSource = dt

            Exit Sub

        End If

    End Sub

    Private Sub ocxTXTCuenta_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave
        OcultarLista()
    End Sub

    Private Sub ocxTXTCuenta_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LostFocus
        OcultarLista()
    End Sub

    Private Sub grid_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grid.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Then
            SeleccionarGrid()
        End If
    End Sub

    Private Sub ocxTXTCuenta_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        ConfigurarTamaño()
    End Sub

    Private Sub grid_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grid.KeyUp

        If e.KeyCode = vgKeyVerInformacionDetallada Then
            MostrarPropiedades()
        End If

        If e.KeyCode = vgKeyConsultar Then
            MostrarConsulta()
        End If

    End Sub

    Private Sub ocxTXTCuenta_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Seleccionar(False)
    End Sub

    Private Sub txtCodigo_TeclaPrecionada(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtCodigo.TeclaPrecionada
        e.Handled = True
        ManejoTecla(txtCodigo.txt, e, "CODIGO")
    End Sub

    Private Sub txt_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtDescripcion.TeclaPrecionada
        e.Handled = True
        ManejoTecla(txtDescripcion.txt, e, "DESCRIPCION")
    End Sub
End Class

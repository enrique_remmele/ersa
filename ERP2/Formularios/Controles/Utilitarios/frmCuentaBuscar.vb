﻿Public Class frmCuentaBuscar

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'PROPIEDADES
    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private ConsultaValue As String
    Public Property Consulta() As String
        Get
            Return ConsultaValue
        End Get
        Set(ByVal value As String)
            ConsultaValue = value
        End Set
    End Property

    'VARIABLES

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'FUNCIONES
    'Listar Clientes
    Sub ListarCuentas()

        Dim Encontrado As Boolean = False

        For i As Integer = 0 To dgv.Rows.Count - 1

            Dim Valor As String = ""
            Dim Busqueda As String = txtDescripcion.Text.ToUpper
            Dim Cantidad As Integer = Busqueda.Length

            If rdbDenominacion.Checked = True Then
                Valor = dgv.Rows(i).Cells(1).Value.ToString.ToUpper
            End If

            If rdbCodigo.Checked = True Then
                Valor = dgv.Rows(i).Cells(0).Value.ToString.ToUpper
            End If

            If Valor.Length < Cantidad Then
                GoTo Siguiente
            End If

            If Valor.Substring(0, Cantidad) = Busqueda Then
                dgv.CurrentCell = dgv.Rows(i).Cells(0)
                Encontrado = True
                Exit For
            End If
Siguiente:

        Next

        If Encontrado = True Then
            dgv.Focus()
        Else
            txtDescripcion.SelectAll()
        End If



    End Sub

    'Guardar Informacion
    Sub GuardarInformacion()

        Dim Seleccion As Integer = 1

        If rdbDenominacion.Checked = True Then
            Seleccion = 1
        End If

        If rdbCodigo.Checked = True Then
            Seleccion = 2
        End If

        If rdbDenominacion.Checked = True Then
            Seleccion = 5
        End If

        'Seleccion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SELECCION", Seleccion)

    End Sub

    'Cargar Informacion
    Sub CargarInformacion()

        'Seleccion
        Dim Seleccion As Integer = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SELECCION", "1")

        Select Case Seleccion

            Case 1
                rdbDenominacion.Checked = True
            Case 2
                rdbCodigo.Checked = True
            Case Else
                rdbDenominacion.Checked = True
        End Select

        'Foco
        txtDescripcion.Focus()
        txtDescripcion.SelectAll()

        'Formato
        dgv.BackgroundColor = Color.White
        dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        dgv.RowHeadersVisible = False
        dgv.StandardTab = True

        Dim SQL As String = "Select Codigo, 'Denominacion'=Descripcion, Alias, Suc, Categoria, Tipo From VCuentaContable Where PlanCuentaTitular = 'True' Order By Denominacion"
        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        dgv.DataSource = dt

        If dgv.ColumnCount = 0 Then
            Exit Sub
        End If

        'Formato
        For c As Integer = 0 To dgv.ColumnCount - 1
            dgv.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Next

        dgv.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        'Ponemos los Totalizadores en otro color
        For i As Integer = 0 To dgv.Rows.Count - 1
            If dgv.Rows(i).Cells(5).Value.ToString.ToUpper = "TOTALIZADOR" Then
                dgv.Rows(i).DefaultCellStyle.BackColor = Color.LightGray
            End If
        Next

    End Sub

    'Seleccion
    Sub Seleccionar()

        If dgv.SelectedRows.Count > 0 Then

            'Validar
            'Si es un totalizador
            If dgv.SelectedRows(0).Cells(5).Value.ToString.ToUpper = "TOTALIZADOR" Then
                MessageBox.Show("No se puede seleccionar cuentas totalizadoras!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

            ID = dgv.SelectedRows(0).Cells(0).Value
            Me.Visible = False
            RaiseEvent PropertyChanged(New Object, New EventArgs)
        End If

    End Sub

    Private Sub txtDescripcion_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDescripcion.KeyUp
        If e.KeyCode = Keys.Enter Then
            ListarCuentas()
        End If

        If e.KeyCode = Keys.Down Then
            dgv.Focus()
        End If

    End Sub

    Private Sub frmClienteBuscar_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmClienteBuscar_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F2 Then

            If rdbDenominacion.Checked = True Then
                rdbCodigo.Checked = True
                GoTo Siguiente
            End If

            If rdbCodigo.Checked = True Then
                rdbDenominacion.Checked = True
                GoTo Siguiente
            End If

Siguiente:
            ListarCuentas()
            txtDescripcion.SelectAll()
            txtDescripcion.Focus()

        End If

        If e.KeyCode = Keys.Down Then
            If dgv.Rows.Count > 0 Then
                If dgv.SelectedRows.Count = 0 Then
                    dgv.Rows(0).Selected = True
                End If

                dgv.Focus()

            Else
                txtDescripcion.SelectAll()
            End If
        End If

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Private Sub frmClienteBuscar_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CargarInformacion()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub rdbReferencia_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbCodigo.CheckedChanged

    End Sub

    Private Sub txtDescripcion_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtDescripcion.TextChanged

    End Sub

    Private Sub dgv_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyUp
        If e.KeyCode = Keys.Enter Then
            Seleccionar()
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub dgv_DoubleClick(sender As System.Object, e As System.EventArgs) Handles dgv.DoubleClick
        Seleccionar()
    End Sub

    Private Sub dgv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyDown
        If e.KeyCode = Keys.Enter Then

            ' Your code here
            e.SuppressKeyPress = True

        End If

    End Sub

End Class
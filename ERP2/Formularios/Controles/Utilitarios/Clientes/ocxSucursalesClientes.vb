﻿Public Class ocxSucursalesClientes

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'PROPIEDADES
    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property

    Private IDPaisValue As Integer
    Public Property IDPais() As Integer
        Get
            Return IDPaisValue
        End Get
        Set(ByVal value As Integer)
            IDPaisValue = value
        End Set
    End Property

    Private IniciadoValue As Boolean
    Public Property Iniciado() As Boolean
        Get
            Return IniciadoValue
        End Get
        Set(ByVal value As Boolean)
            IniciadoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Variables
        vNuevo = False
        Iniciado = True

        'Funciones
        CargarInformacion()
        CSistema.InicializaControles(Me)

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Focus
        dgv.Focus()

    End Sub

    Sub CargarInformacion()

        'Vendedor
        CSistema.SqlToComboBox(cbxVendedor.cbx, "Select ID, Nombres From Vendedor Order By 2")

        'Listar Barrios
        CSistema.SqlToComboBox(cbxBarrio.cbx, "Select ID, Descripcion From Barrio Order By Orden Desc, Descripcion Asc")

        'Zona de Ventas
        CSistema.SqlToComboBox(cbxZonaVenta.cbx, "Select ID, Descripcion From ZonaVenta Order By Descripcion")

        'Sucursal
        CSistema.SqlToComboBox(cbxClienteSucursal.cbx, "Select ID, Descripcion From Sucursal Order By Descripcion")

        'Lista de Precios
        CSistema.SqlToComboBox(cbxListaPrecio.cbx, CData.GetTable("VListaPrecio", " IDSucursal=" & vgIDSucursal), "ID", "Descripcion")

        'Listar Estado de Clientes
        CSistema.SqlToComboBox(cbxEstado.cbx, "Select ID, Descripcion From EstadoCliente Order By Orden Desc, Descripcion Asc")

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtSucursal)
        CSistema.CargaControl(vControles, txtDireccion)
        CSistema.CargaControl(vControles, txtTelefono)
        CSistema.CargaControl(vControles, txtContacto)
        CSistema.CargaControl(vControles, cbxVendedor)
        CSistema.CargaControl(vControles, cbxDepartamento)
        CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxBarrio)
        CSistema.CargaControl(vControles, cbxZonaVenta)
        CSistema.CargaControl(vControles, cbxClienteSucursal)
        CSistema.CargaControl(vControles, cbxCobrador)
        CSistema.CargaControl(vControles, cbxListaPrecio)


        'Cargamos los paises en el lv
        Listar()

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If dgv.SelectedRows.Count = 0 Then
            ctrError.SetError(dgv, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = dgv.SelectedRows(0).Cells(1).Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VClienteSucursal Where ID=" & ID & " And IDCliente=" & IDCliente)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            txtSucursal.txt.Text = oRow("Sucursal").ToString
            txtDireccion.txt.Text = oRow("Direccion").ToString
            txtContacto.txt.Text = oRow("Contacto").ToString
            txtTelefono.txt.Text = oRow("Telefono").ToString

            cbxDepartamento.cbx.Text = oRow("Departamento").ToString
            cbxCiudad.cbx.Text = oRow("Ciudad").ToString
            cbxBarrio.cbx.Text = oRow("Barrio").ToString
            cbxZonaVenta.cbx.Text = oRow("ZonaVenta").ToString
            cbxVendedor.cbx.Text = oRow("Vendedor").ToString
            cbxClienteSucursal.cbx.Text = oRow("ClienteSucursal").ToString
            cbxListaPrecio.cbx.Text = oRow("ListaPrecio").ToString
            rdbContado.Checked = CBool(oRow("Contado").ToString)
            rdbCredito.Checked = CBool(oRow("Credito").ToString)
            cbxCobrador.cbx.Text = oRow("Cobrador").ToString
            cbxEstado.cbx.Text = oRow("Estados").ToString


            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        CSistema.SqlToDataGrid(dgv, "Select Sucursal, ID, Direccion, Ciudad, ZonaVenta From VClienteSucursal Where IDCliente=" & IDCliente & " Order By ID")

        'Ajustar la ultima columna
        dgv.Columns(dgv.Columns.Count - 1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub InicializarControles()

        'TextBox
        txtSucursal.txt.Clear()
        txtContacto.txt.Clear()
        txtDireccion.txt.Clear()
        txtTelefono.txt.Clear()

        'ComoBox
        'Listar departamentos de pais
        CSistema.SqlToComboBox(cbxDepartamento.cbx, "Select ID, Descripcion From Departamento Where IDPais=" & IDPais & " Order By Orden Desc, Descripcion Asc")
        cbxDepartamento.txt.Clear()

        cbxCiudad.txt.Clear()
        cbxBarrio.txt.Clear()
        cbxZonaVenta.txt.Clear()
        cbxVendedor.txt.Clear()
        cbxClienteSucursal.txt.Clear()

        'ToolStripStatusLabel
        tsslEstado.Text = ""

        'Funciones
        If Me.vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From ClienteSucursal Where IDCliente=" & IDCliente), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From ClienteSucursal Where IDCliente=" & IDCliente), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        'txtSucursal.txt.Focus()
        'txtSucursal.txt.SelectAll()

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        'Validar
        'Poner una descripcion
        If txtSucursal.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese una descipcion valida para sucursal!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgv.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgv, mensaje)
                ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@IDCliente", IDCliente, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Sucursal", txtSucursal.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Direccion", txtDireccion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Contacto", txtContacto.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Telefono", txtTelefono.txt.Text, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@IDPais", IDPais, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepartamento", cbxDepartamento.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCiudad", cbxCiudad.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDBarrio", cbxBarrio.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDZonaVenta", cbxZonaVenta.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDVendedor", cbxVendedor.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxClienteSucursal.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDListaPrecio", cbxListaPrecio.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Contado", rdbContado.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Credito", rdbCredito.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCobrador", cbxCobrador.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDEstado", cbxEstado.cbx, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""
        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpClienteSucursal", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            Listar()
            ctrError.Clear()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        txtSucursal.Focus()
        txtSucursal.txt.SelectAll()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub frmLineas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Inicializar()
    End Sub

    Private Sub cbxDepartamento_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxDepartamento.PropertyChanged
        cbxCiudad.cbx.Text = ""

        'Listar Ciudades
        If IsNumeric(cbxDepartamento.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxDepartamento.cbx.Text = "" Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select ID, Descripcion From Ciudad Where IDDepartamento=" & cbxDepartamento.cbx.SelectedValue & " Order By Orden Desc, Descripcion Asc")

        cbxCiudad.cbx.Text = ""

    End Sub

    Private Sub dgv_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub cbxVendedor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxVendedor.Load

    End Sub
    Private Sub cbxListaPrecio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxListaPrecio.Load

    End Sub

    Private Sub cbxClienteSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxClienteSucursal.PropertyChanged
        If cbxClienteSucursal.GetValue = 0 Then

            'Vendedor
            CSistema.SqlToComboBox(cbxVendedor.cbx, CData.GetTable("VVendedor", " IDSucursal=" & vgIDSucursal), "ID", "Nombres")

            'Lista de Precios
            CSistema.SqlToComboBox(cbxListaPrecio.cbx, CData.GetTable("VListaPrecio", " IDSucursal=" & vgIDSucursal), "ID", "Descripcion")

            Exit Sub

        End If

        'Vendedor
        CSistema.SqlToComboBox(cbxVendedor.cbx, CData.GetTable("VVendedor", " IDSucursal=" & cbxClienteSucursal.GetValue), "ID", "Nombres")

        'Lista de Precios
        CSistema.SqlToComboBox(cbxListaPrecio.cbx, CData.GetTable("VListaPrecio", " IDSucursal=" & cbxClienteSucursal.GetValue), "ID", "Descripcion")

        cbxVendedor.cbx.Text = ""
        cbxListaPrecio.cbx.Text = ""
    End Sub
End Class

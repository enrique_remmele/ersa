﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxSucursalesClientes
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lblVendedor = New System.Windows.Forms.Label()
        Me.lblBarrio = New System.Windows.Forms.Label()
        Me.lblCiudad = New System.Windows.Forms.Label()
        Me.lblDepartamento = New System.Windows.Forms.Label()
        Me.lblTelefono = New System.Windows.Forms.Label()
        Me.lblContacto = New System.Windows.Forms.Label()
        Me.lblDireccion = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblZonaVenta = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblSucursalCliente = New System.Windows.Forms.Label()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.lblListaPrecio = New System.Windows.Forms.Label()
        Me.rdbCredito = New System.Windows.Forms.RadioButton()
        Me.rdbContado = New System.Windows.Forms.RadioButton()
        Me.lblCondicionVenta = New System.Windows.Forms.Label()
        Me.lblCobrador = New System.Windows.Forms.Label()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.cbxCobrador = New ERP.ocxCBX()
        Me.cbxListaPrecio = New ERP.ocxCBX()
        Me.cbxClienteSucursal = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTString()
        Me.cbxZonaVenta = New ERP.ocxCBX()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.cbxBarrio = New ERP.ocxCBX()
        Me.cbxDepartamento = New ERP.ocxCBX()
        Me.txtTelefono = New ERP.ocxTXTString()
        Me.txtContacto = New ERP.ocxTXTString()
        Me.txtDireccion = New ERP.ocxTXTString()
        Me.txtSucursal = New ERP.ocxTXTString()
        Me.cbxEstado = New ERP.ocxCBX()
        Me.lblEstado = New System.Windows.Forms.Label()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(306, 119)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 4
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(387, 119)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 5
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(225, 119)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 3
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(144, 119)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 2
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(63, 119)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 1
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lblVendedor
        '
        Me.lblVendedor.AutoSize = True
        Me.lblVendedor.Location = New System.Drawing.Point(375, 242)
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Size = New System.Drawing.Size(56, 13)
        Me.lblVendedor.TabIndex = 14
        Me.lblVendedor.Text = "Vendedor:"
        '
        'lblBarrio
        '
        Me.lblBarrio.AutoSize = True
        Me.lblBarrio.Location = New System.Drawing.Point(394, 200)
        Me.lblBarrio.Name = "lblBarrio"
        Me.lblBarrio.Size = New System.Drawing.Size(37, 13)
        Me.lblBarrio.TabIndex = 20
        Me.lblBarrio.Text = "Barrio:"
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(388, 179)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(43, 13)
        Me.lblCiudad.TabIndex = 18
        Me.lblCiudad.Text = "Ciudad:"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(354, 158)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(77, 13)
        Me.lblDepartamento.TabIndex = 16
        Me.lblDepartamento.Text = "Departamento:"
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Location = New System.Drawing.Point(3, 242)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(52, 13)
        Me.lblTelefono.TabIndex = 12
        Me.lblTelefono.Text = "Telefono:"
        '
        'lblContacto
        '
        Me.lblContacto.AutoSize = True
        Me.lblContacto.Location = New System.Drawing.Point(3, 221)
        Me.lblContacto.Name = "lblContacto"
        Me.lblContacto.Size = New System.Drawing.Size(53, 13)
        Me.lblContacto.TabIndex = 10
        Me.lblContacto.Text = "Contacto:"
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.Location = New System.Drawing.Point(3, 200)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(55, 13)
        Me.lblDireccion.TabIndex = 8
        Me.lblDireccion.Text = "Direccion:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(3, 179)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 6
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblZonaVenta
        '
        Me.lblZonaVenta.AutoSize = True
        Me.lblZonaVenta.Location = New System.Drawing.Point(365, 221)
        Me.lblZonaVenta.Name = "lblZonaVenta"
        Me.lblZonaVenta.Size = New System.Drawing.Size(66, 13)
        Me.lblZonaVenta.TabIndex = 22
        Me.lblZonaVenta.Text = "Zona Venta:"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 331)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(685, 22)
        Me.StatusStrip1.TabIndex = 24
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(3, 157)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 25
        Me.lblID.Text = "ID:"
        '
        'lblSucursalCliente
        '
        Me.lblSucursalCliente.AutoSize = True
        Me.lblSucursalCliente.Location = New System.Drawing.Point(342, 284)
        Me.lblSucursalCliente.Name = "lblSucursalCliente"
        Me.lblSucursalCliente.Size = New System.Drawing.Size(86, 13)
        Me.lblSucursalCliente.TabIndex = 27
        Me.lblSucursalCliente.Text = "Sucursal Cliente:"
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(63, 7)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.Size = New System.Drawing.Size(612, 106)
        Me.dgv.TabIndex = 0
        '
        'lblListaPrecio
        '
        Me.lblListaPrecio.AutoSize = True
        Me.lblListaPrecio.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblListaPrecio.Location = New System.Drawing.Point(345, 307)
        Me.lblListaPrecio.Name = "lblListaPrecio"
        Me.lblListaPrecio.Size = New System.Drawing.Size(85, 13)
        Me.lblListaPrecio.TabIndex = 43
        Me.lblListaPrecio.Text = "Lista de Precios:"
        '
        'rdbCredito
        '
        Me.rdbCredito.AutoSize = True
        Me.rdbCredito.BackColor = System.Drawing.Color.Transparent
        Me.rdbCredito.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdbCredito.Location = New System.Drawing.Point(148, 291)
        Me.rdbCredito.Name = "rdbCredito"
        Me.rdbCredito.Size = New System.Drawing.Size(58, 17)
        Me.rdbCredito.TabIndex = 47
        Me.rdbCredito.TabStop = True
        Me.rdbCredito.Text = "Credito"
        Me.rdbCredito.UseVisualStyleBackColor = False
        '
        'rdbContado
        '
        Me.rdbContado.AutoSize = True
        Me.rdbContado.BackColor = System.Drawing.Color.Transparent
        Me.rdbContado.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdbContado.Location = New System.Drawing.Point(77, 291)
        Me.rdbContado.Name = "rdbContado"
        Me.rdbContado.Size = New System.Drawing.Size(65, 17)
        Me.rdbContado.TabIndex = 46
        Me.rdbContado.TabStop = True
        Me.rdbContado.Text = "Contado"
        Me.rdbContado.UseVisualStyleBackColor = False
        '
        'lblCondicionVenta
        '
        Me.lblCondicionVenta.AutoSize = True
        Me.lblCondicionVenta.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblCondicionVenta.Location = New System.Drawing.Point(1, 293)
        Me.lblCondicionVenta.Name = "lblCondicionVenta"
        Me.lblCondicionVenta.Size = New System.Drawing.Size(69, 13)
        Me.lblCondicionVenta.TabIndex = 45
        Me.lblCondicionVenta.Text = "Cond. Venta:"
        '
        'lblCobrador
        '
        Me.lblCobrador.AutoSize = True
        Me.lblCobrador.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblCobrador.Location = New System.Drawing.Point(373, 263)
        Me.lblCobrador.Name = "lblCobrador"
        Me.lblCobrador.Size = New System.Drawing.Size(53, 13)
        Me.lblCobrador.TabIndex = 48
        Me.lblCobrador.Text = "Cobrador:"
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(434, 175)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(168, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 50
        Me.cbxCiudad.Texto = ""
        '
        'cbxCobrador
        '
        Me.cbxCobrador.CampoWhere = Nothing
        Me.cbxCobrador.CargarUnaSolaVez = False
        Me.cbxCobrador.DataDisplayMember = "Nombres"
        Me.cbxCobrador.DataFilter = Nothing
        Me.cbxCobrador.DataOrderBy = Nothing
        Me.cbxCobrador.DataSource = ""
        Me.cbxCobrador.DataValueMember = "ID"
        Me.cbxCobrador.FormABM = "frmCobrador"
        Me.cbxCobrador.Indicaciones = Nothing
        Me.cbxCobrador.Location = New System.Drawing.Point(434, 259)
        Me.cbxCobrador.Name = "cbxCobrador"
        Me.cbxCobrador.SeleccionObligatoria = False
        Me.cbxCobrador.Size = New System.Drawing.Size(168, 21)
        Me.cbxCobrador.SoloLectura = False
        Me.cbxCobrador.TabIndex = 49
        Me.cbxCobrador.Texto = ""
        '
        'cbxListaPrecio
        '
        Me.cbxListaPrecio.CampoWhere = Nothing
        Me.cbxListaPrecio.CargarUnaSolaVez = False
        Me.cbxListaPrecio.DataDisplayMember = "Descripcion"
        Me.cbxListaPrecio.DataFilter = Nothing
        Me.cbxListaPrecio.DataOrderBy = Nothing
        Me.cbxListaPrecio.DataSource = ""
        Me.cbxListaPrecio.DataValueMember = "ID"
        Me.cbxListaPrecio.FormABM = "frmListaPrecio"
        Me.cbxListaPrecio.Indicaciones = Nothing
        Me.cbxListaPrecio.Location = New System.Drawing.Point(434, 301)
        Me.cbxListaPrecio.Name = "cbxListaPrecio"
        Me.cbxListaPrecio.SeleccionObligatoria = False
        Me.cbxListaPrecio.Size = New System.Drawing.Size(168, 21)
        Me.cbxListaPrecio.SoloLectura = False
        Me.cbxListaPrecio.TabIndex = 44
        Me.cbxListaPrecio.Texto = "BARES Y KIOSCOS"
        '
        'cbxClienteSucursal
        '
        Me.cbxClienteSucursal.CampoWhere = Nothing
        Me.cbxClienteSucursal.CargarUnaSolaVez = False
        Me.cbxClienteSucursal.DataDisplayMember = Nothing
        Me.cbxClienteSucursal.DataFilter = Nothing
        Me.cbxClienteSucursal.DataOrderBy = Nothing
        Me.cbxClienteSucursal.DataSource = Nothing
        Me.cbxClienteSucursal.DataValueMember = Nothing
        Me.cbxClienteSucursal.FormABM = Nothing
        Me.cbxClienteSucursal.Indicaciones = Nothing
        Me.cbxClienteSucursal.Location = New System.Drawing.Point(434, 280)
        Me.cbxClienteSucursal.Name = "cbxClienteSucursal"
        Me.cbxClienteSucursal.SeleccionObligatoria = False
        Me.cbxClienteSucursal.Size = New System.Drawing.Size(168, 21)
        Me.cbxClienteSucursal.SoloLectura = False
        Me.cbxClienteSucursal.TabIndex = 28
        Me.cbxClienteSucursal.Texto = ""
        '
        'txtID
        '
        Me.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(67, 154)
        Me.txtID.Multilinea = False
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(58, 21)
        Me.txtID.SoloLectura = True
        Me.txtID.TabIndex = 26
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtID.Texto = ""
        '
        'cbxZonaVenta
        '
        Me.cbxZonaVenta.CampoWhere = Nothing
        Me.cbxZonaVenta.CargarUnaSolaVez = False
        Me.cbxZonaVenta.DataDisplayMember = Nothing
        Me.cbxZonaVenta.DataFilter = Nothing
        Me.cbxZonaVenta.DataOrderBy = Nothing
        Me.cbxZonaVenta.DataSource = Nothing
        Me.cbxZonaVenta.DataValueMember = Nothing
        Me.cbxZonaVenta.FormABM = Nothing
        Me.cbxZonaVenta.Indicaciones = Nothing
        Me.cbxZonaVenta.Location = New System.Drawing.Point(434, 217)
        Me.cbxZonaVenta.Name = "cbxZonaVenta"
        Me.cbxZonaVenta.SeleccionObligatoria = False
        Me.cbxZonaVenta.Size = New System.Drawing.Size(168, 21)
        Me.cbxZonaVenta.SoloLectura = False
        Me.cbxZonaVenta.TabIndex = 23
        Me.cbxZonaVenta.Texto = ""
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = Nothing
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = Nothing
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = Nothing
        Me.cbxVendedor.DataSource = Nothing
        Me.cbxVendedor.DataValueMember = Nothing
        Me.cbxVendedor.FormABM = Nothing
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(434, 238)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(168, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 15
        Me.cbxVendedor.Texto = ""
        '
        'cbxBarrio
        '
        Me.cbxBarrio.CampoWhere = Nothing
        Me.cbxBarrio.CargarUnaSolaVez = False
        Me.cbxBarrio.DataDisplayMember = Nothing
        Me.cbxBarrio.DataFilter = Nothing
        Me.cbxBarrio.DataOrderBy = Nothing
        Me.cbxBarrio.DataSource = Nothing
        Me.cbxBarrio.DataValueMember = Nothing
        Me.cbxBarrio.FormABM = Nothing
        Me.cbxBarrio.Indicaciones = Nothing
        Me.cbxBarrio.Location = New System.Drawing.Point(434, 196)
        Me.cbxBarrio.Name = "cbxBarrio"
        Me.cbxBarrio.SeleccionObligatoria = False
        Me.cbxBarrio.Size = New System.Drawing.Size(168, 21)
        Me.cbxBarrio.SoloLectura = False
        Me.cbxBarrio.TabIndex = 21
        Me.cbxBarrio.Texto = ""
        '
        'cbxDepartamento
        '
        Me.cbxDepartamento.CampoWhere = Nothing
        Me.cbxDepartamento.CargarUnaSolaVez = False
        Me.cbxDepartamento.DataDisplayMember = Nothing
        Me.cbxDepartamento.DataFilter = Nothing
        Me.cbxDepartamento.DataOrderBy = Nothing
        Me.cbxDepartamento.DataSource = Nothing
        Me.cbxDepartamento.DataValueMember = Nothing
        Me.cbxDepartamento.FormABM = Nothing
        Me.cbxDepartamento.Indicaciones = Nothing
        Me.cbxDepartamento.Location = New System.Drawing.Point(434, 154)
        Me.cbxDepartamento.Name = "cbxDepartamento"
        Me.cbxDepartamento.SeleccionObligatoria = False
        Me.cbxDepartamento.Size = New System.Drawing.Size(168, 21)
        Me.cbxDepartamento.SoloLectura = False
        Me.cbxDepartamento.TabIndex = 17
        Me.cbxDepartamento.Texto = ""
        '
        'txtTelefono
        '
        Me.txtTelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefono.Color = System.Drawing.Color.Empty
        Me.txtTelefono.Indicaciones = Nothing
        Me.txtTelefono.Location = New System.Drawing.Point(67, 238)
        Me.txtTelefono.Multilinea = False
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(238, 21)
        Me.txtTelefono.SoloLectura = False
        Me.txtTelefono.TabIndex = 13
        Me.txtTelefono.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTelefono.Texto = ""
        '
        'txtContacto
        '
        Me.txtContacto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtContacto.Color = System.Drawing.Color.Empty
        Me.txtContacto.Indicaciones = Nothing
        Me.txtContacto.Location = New System.Drawing.Point(67, 217)
        Me.txtContacto.Multilinea = False
        Me.txtContacto.Name = "txtContacto"
        Me.txtContacto.Size = New System.Drawing.Size(238, 21)
        Me.txtContacto.SoloLectura = False
        Me.txtContacto.TabIndex = 11
        Me.txtContacto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtContacto.Texto = ""
        '
        'txtDireccion
        '
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Color = System.Drawing.Color.Empty
        Me.txtDireccion.Indicaciones = Nothing
        Me.txtDireccion.Location = New System.Drawing.Point(67, 196)
        Me.txtDireccion.Multilinea = False
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(238, 21)
        Me.txtDireccion.SoloLectura = False
        Me.txtDireccion.TabIndex = 9
        Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccion.Texto = ""
        '
        'txtSucursal
        '
        Me.txtSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSucursal.Color = System.Drawing.Color.Empty
        Me.txtSucursal.Indicaciones = Nothing
        Me.txtSucursal.Location = New System.Drawing.Point(67, 175)
        Me.txtSucursal.Multilinea = False
        Me.txtSucursal.Name = "txtSucursal"
        Me.txtSucursal.Size = New System.Drawing.Size(238, 21)
        Me.txtSucursal.SoloLectura = False
        Me.txtSucursal.TabIndex = 7
        Me.txtSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSucursal.Texto = ""
        '
        'cbxEstado
        '
        Me.cbxEstado.CampoWhere = Nothing
        Me.cbxEstado.CargarUnaSolaVez = False
        Me.cbxEstado.DataDisplayMember = Nothing
        Me.cbxEstado.DataFilter = Nothing
        Me.cbxEstado.DataOrderBy = Nothing
        Me.cbxEstado.DataSource = Nothing
        Me.cbxEstado.DataValueMember = Nothing
        Me.cbxEstado.FormABM = Nothing
        Me.cbxEstado.Indicaciones = Nothing
        Me.cbxEstado.Location = New System.Drawing.Point(67, 266)
        Me.cbxEstado.Name = "cbxEstado"
        Me.cbxEstado.SeleccionObligatoria = True
        Me.cbxEstado.Size = New System.Drawing.Size(238, 21)
        Me.cbxEstado.SoloLectura = False
        Me.cbxEstado.TabIndex = 52
        Me.cbxEstado.Texto = ""
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblEstado.Location = New System.Drawing.Point(15, 270)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 51
        Me.lblEstado.Text = "Estado:"
        '
        'ocxSucursalesClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.cbxEstado)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.cbxCiudad)
        Me.Controls.Add(Me.cbxCobrador)
        Me.Controls.Add(Me.lblCobrador)
        Me.Controls.Add(Me.rdbCredito)
        Me.Controls.Add(Me.rdbContado)
        Me.Controls.Add(Me.lblCondicionVenta)
        Me.Controls.Add(Me.cbxListaPrecio)
        Me.Controls.Add(Me.lblListaPrecio)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.cbxClienteSucursal)
        Me.Controls.Add(Me.lblSucursalCliente)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.cbxZonaVenta)
        Me.Controls.Add(Me.lblZonaVenta)
        Me.Controls.Add(Me.cbxVendedor)
        Me.Controls.Add(Me.cbxBarrio)
        Me.Controls.Add(Me.cbxDepartamento)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.lblVendedor)
        Me.Controls.Add(Me.lblBarrio)
        Me.Controls.Add(Me.lblCiudad)
        Me.Controls.Add(Me.lblDepartamento)
        Me.Controls.Add(Me.lblTelefono)
        Me.Controls.Add(Me.lblContacto)
        Me.Controls.Add(Me.lblDireccion)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.txtTelefono)
        Me.Controls.Add(Me.txtContacto)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.txtSucursal)
        Me.Name = "ocxSucursalesClientes"
        Me.Size = New System.Drawing.Size(685, 353)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lblVendedor As System.Windows.Forms.Label
    Friend WithEvents lblBarrio As System.Windows.Forms.Label
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents lblTelefono As System.Windows.Forms.Label
    Friend WithEvents lblContacto As System.Windows.Forms.Label
    Friend WithEvents lblDireccion As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents txtTelefono As ERP.ocxTXTString
    Friend WithEvents txtContacto As ERP.ocxTXTString
    Friend WithEvents txtDireccion As ERP.ocxTXTString
    Friend WithEvents txtSucursal As ERP.ocxTXTString
    Friend WithEvents cbxDepartamento As ERP.ocxCBX
    Friend WithEvents cbxBarrio As ERP.ocxCBX
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents cbxZonaVenta As ERP.ocxCBX
    Friend WithEvents lblZonaVenta As System.Windows.Forms.Label
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTString
    Friend WithEvents cbxClienteSucursal As ERP.ocxCBX
    Friend WithEvents lblSucursalCliente As System.Windows.Forms.Label
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents cbxListaPrecio As ERP.ocxCBX
    Friend WithEvents lblListaPrecio As System.Windows.Forms.Label
    Friend WithEvents rdbCredito As System.Windows.Forms.RadioButton
    Friend WithEvents rdbContado As System.Windows.Forms.RadioButton
    Friend WithEvents lblCondicionVenta As System.Windows.Forms.Label
    Friend WithEvents cbxCobrador As ERP.ocxCBX
    Friend WithEvents lblCobrador As System.Windows.Forms.Label
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents cbxEstado As ERP.ocxCBX
    Friend WithEvents lblEstado As System.Windows.Forms.Label

End Class

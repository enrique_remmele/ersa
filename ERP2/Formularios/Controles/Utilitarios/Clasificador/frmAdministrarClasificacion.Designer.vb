﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdministrarClasificacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblPadre = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblNivel = New System.Windows.Forms.Label()
        Me.lblCodigo = New System.Windows.Forms.Label()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lklImagen = New System.Windows.Forms.LinkLabel()
        Me.pbxImagen = New System.Windows.Forms.PictureBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.lblAlias = New System.Windows.Forms.Label()
        Me.lblNum = New System.Windows.Forms.Label()
        Me.txtAlias = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTString()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.txtCodigo = New ERP.ocxTXTString()
        Me.txtNivel = New ERP.ocxTXTString()
        Me.cbxPadre = New ERP.ocxCBX()
        Me.txtNumero = New ERP.ocxTXTNumeric()
        Me.cbxGrupo = New ERP.ocxCBX()
        Me.lblGrupo = New System.Windows.Forms.Label()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxImagen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblPadre
        '
        Me.lblPadre.AutoSize = True
        Me.lblPadre.Location = New System.Drawing.Point(12, 62)
        Me.lblPadre.Name = "lblPadre"
        Me.lblPadre.Size = New System.Drawing.Size(38, 13)
        Me.lblPadre.TabIndex = 8
        Me.lblPadre.Text = "Padre:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(12, 9)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'lblNivel
        '
        Me.lblNivel.AutoSize = True
        Me.lblNivel.Location = New System.Drawing.Point(12, 35)
        Me.lblNivel.Name = "lblNivel"
        Me.lblNivel.Size = New System.Drawing.Size(34, 13)
        Me.lblNivel.TabIndex = 2
        Me.lblNivel.Text = "Nivel:"
        '
        'lblCodigo
        '
        Me.lblCodigo.AutoSize = True
        Me.lblCodigo.Location = New System.Drawing.Point(201, 35)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(43, 13)
        Me.lblCodigo.TabIndex = 6
        Me.lblCodigo.Text = "Codigo:"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(12, 88)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 10
        Me.lblDescripcion.Text = "Descripción:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 278)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(326, 22)
        Me.StatusStrip1.TabIndex = 20
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(43, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(160, 244)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 18
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(241, 244)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 19
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(79, 244)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 17
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'lklImagen
        '
        Me.lklImagen.AutoSize = True
        Me.lklImagen.Location = New System.Drawing.Point(12, 175)
        Me.lklImagen.Name = "lklImagen"
        Me.lklImagen.Size = New System.Drawing.Size(45, 13)
        Me.lklImagen.TabIndex = 16
        Me.lklImagen.TabStop = True
        Me.lklImagen.Text = "Imagen:"
        '
        'pbxImagen
        '
        Me.pbxImagen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pbxImagen.Location = New System.Drawing.Point(79, 175)
        Me.pbxImagen.Name = "pbxImagen"
        Me.pbxImagen.Size = New System.Drawing.Size(50, 50)
        Me.pbxImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbxImagen.TabIndex = 45
        Me.pbxImagen.TabStop = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'lblAlias
        '
        Me.lblAlias.AutoSize = True
        Me.lblAlias.Location = New System.Drawing.Point(12, 116)
        Me.lblAlias.Name = "lblAlias"
        Me.lblAlias.Size = New System.Drawing.Size(32, 13)
        Me.lblAlias.TabIndex = 12
        Me.lblAlias.Text = "Alias:"
        '
        'lblNum
        '
        Me.lblNum.AutoSize = True
        Me.lblNum.Location = New System.Drawing.Point(113, 35)
        Me.lblNum.Name = "lblNum"
        Me.lblNum.Size = New System.Drawing.Size(32, 13)
        Me.lblNum.TabIndex = 4
        Me.lblNum.Text = "Num:"
        '
        'txtAlias
        '
        Me.txtAlias.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAlias.Color = System.Drawing.Color.Empty
        Me.txtAlias.Indicaciones = Nothing
        Me.txtAlias.Location = New System.Drawing.Point(79, 112)
        Me.txtAlias.Multilinea = False
        Me.txtAlias.Name = "txtAlias"
        Me.txtAlias.Size = New System.Drawing.Size(237, 21)
        Me.txtAlias.SoloLectura = False
        Me.txtAlias.TabIndex = 13
        Me.txtAlias.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtAlias.Texto = ""
        '
        'txtID
        '
        Me.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(79, 4)
        Me.txtID.Multilinea = False
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(55, 21)
        Me.txtID.SoloLectura = True
        Me.txtID.TabIndex = 1
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtID.Texto = ""
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(79, 85)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(237, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 11
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'txtCodigo
        '
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Color = System.Drawing.Color.Empty
        Me.txtCodigo.Indicaciones = Nothing
        Me.txtCodigo.Location = New System.Drawing.Point(244, 31)
        Me.txtCodigo.Multilinea = False
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(70, 21)
        Me.txtCodigo.SoloLectura = True
        Me.txtCodigo.TabIndex = 7
        Me.txtCodigo.TabStop = False
        Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCodigo.Texto = ""
        '
        'txtNivel
        '
        Me.txtNivel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNivel.Color = System.Drawing.Color.Empty
        Me.txtNivel.Indicaciones = Nothing
        Me.txtNivel.Location = New System.Drawing.Point(79, 31)
        Me.txtNivel.Multilinea = False
        Me.txtNivel.Name = "txtNivel"
        Me.txtNivel.Size = New System.Drawing.Size(34, 21)
        Me.txtNivel.SoloLectura = True
        Me.txtNivel.TabIndex = 3
        Me.txtNivel.TabStop = False
        Me.txtNivel.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNivel.Texto = ""
        '
        'cbxPadre
        '
        Me.cbxPadre.DataDisplayMember = Nothing
        Me.cbxPadre.DataFilter = Nothing
        Me.cbxPadre.DataSource = Nothing
        Me.cbxPadre.DataValueMember = Nothing
        Me.cbxPadre.FormABM = Nothing
        Me.cbxPadre.Indicaciones = Nothing
        Me.cbxPadre.Location = New System.Drawing.Point(79, 58)
        Me.cbxPadre.Name = "cbxPadre"
        Me.cbxPadre.SeleccionObligatoria = False
        Me.cbxPadre.Size = New System.Drawing.Size(237, 21)
        Me.cbxPadre.SoloLectura = False
        Me.cbxPadre.TabIndex = 9
        Me.cbxPadre.Texto = ""
        '
        'txtNumero
        '
        Me.txtNumero.Color = System.Drawing.Color.Empty
        Me.txtNumero.Decimales = True
        Me.txtNumero.Indicaciones = Nothing
        Me.txtNumero.Location = New System.Drawing.Point(143, 31)
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.Size = New System.Drawing.Size(53, 22)
        Me.txtNumero.SoloLectura = False
        Me.txtNumero.TabIndex = 5
        Me.txtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNumero.Texto = "0"
        '
        'cbxGrupo
        '
        Me.cbxGrupo.DataDisplayMember = "Descripcion"
        Me.cbxGrupo.DataFilter = "Estado = 'True'"
        Me.cbxGrupo.DataSource = "VClasificacionProductoAgrupador"
        Me.cbxGrupo.DataValueMember = "ID"
        Me.cbxGrupo.FormABM = "frmClasificacionProductoAgrupador"
        Me.cbxGrupo.Indicaciones = Nothing
        Me.cbxGrupo.Location = New System.Drawing.Point(79, 139)
        Me.cbxGrupo.Name = "cbxGrupo"
        Me.cbxGrupo.SeleccionObligatoria = False
        Me.cbxGrupo.Size = New System.Drawing.Size(237, 21)
        Me.cbxGrupo.SoloLectura = False
        Me.cbxGrupo.TabIndex = 15
        Me.cbxGrupo.Texto = ""
        '
        'lblGrupo
        '
        Me.lblGrupo.AutoSize = True
        Me.lblGrupo.Location = New System.Drawing.Point(12, 143)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.Size = New System.Drawing.Size(39, 13)
        Me.lblGrupo.TabIndex = 14
        Me.lblGrupo.Text = "Grupo:"
        '
        'frmAdministrarClasificacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(326, 300)
        Me.Controls.Add(Me.cbxGrupo)
        Me.Controls.Add(Me.lblGrupo)
        Me.Controls.Add(Me.txtNumero)
        Me.Controls.Add(Me.lblNum)
        Me.Controls.Add(Me.txtAlias)
        Me.Controls.Add(Me.lblAlias)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.pbxImagen)
        Me.Controls.Add(Me.lklImagen)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtCodigo)
        Me.Controls.Add(Me.lblCodigo)
        Me.Controls.Add(Me.txtNivel)
        Me.Controls.Add(Me.lblNivel)
        Me.Controls.Add(Me.cbxPadre)
        Me.Controls.Add(Me.lblPadre)
        Me.Controls.Add(Me.lblID)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.Name = "frmAdministrarClasificacion"
        Me.Text = "frmAgregarClasificacion"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxImagen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxPadre As ERP.ocxCBX
    Friend WithEvents lblPadre As System.Windows.Forms.Label
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents txtNivel As ERP.ocxTXTString
    Friend WithEvents lblNivel As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As ERP.ocxTXTString
    Friend WithEvents lblCodigo As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents pbxImagen As System.Windows.Forms.PictureBox
    Friend WithEvents lklImagen As System.Windows.Forms.LinkLabel
    Friend WithEvents txtID As ERP.ocxTXTString
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtAlias As ERP.ocxTXTString
    Friend WithEvents lblAlias As System.Windows.Forms.Label
    Friend WithEvents lblNum As System.Windows.Forms.Label
    Friend WithEvents txtNumero As ERP.ocxTXTNumeric
    Friend WithEvents cbxGrupo As ERP.ocxCBX
    Friend WithEvents lblGrupo As System.Windows.Forms.Label
End Class

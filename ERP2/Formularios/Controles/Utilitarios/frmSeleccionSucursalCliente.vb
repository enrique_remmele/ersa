﻿Public Class frmSeleccionSucursalCliente

    'CLASES
    Dim CSistema As New CSistema

    'PORPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private SucursalValue As DataRow
    Public Property Sucursal() As DataRow
        Get
            Return SucursalValue
        End Get
        Set(ByVal value As DataRow)
            SucursalValue = value
        End Set
    End Property

    Private ClienteValue As DataRow
    Public Property Cliente() As DataRow
        Get
            Return ClienteValue
        End Get
        Set(ByVal value As DataRow)
            ClienteValue = value
        End Set
    End Property

    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property

    Private MatrizSeleccionadaValue As Boolean
    Public Property MatrizSeleccionada() As Boolean
        Get
            Return MatrizSeleccionadaValue
        End Get
        Set(ByVal value As Boolean)
            MatrizSeleccionadaValue = value
        End Set
    End Property

    Private SeleccionadoValue As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionadoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        txtCliente.txt.Text = Cliente("RazonSocial").ToString
        Me.Text = "Seleccion de Sucursal del Cliente"
        MatrizSeleccionada = False
        Seleccionado = False

        'Cargar el DataTable
        Dim SQL As String

        If vgUsuarioEsVendedor = False Then
            SQL = "Select 'ID'=0, Sucursal, Vendedor, Direccion, Telefono, Ciudad, Barrio, ZonaVenta, ListaPrecio, Condicion From VCliente Where ID=" & IDCliente & " Union All Select ID, Sucursal, Vendedor, Direccion, Telefono, Ciudad, Barrio, ZonaVenta, ListaPrecio, Condicion From VClienteSucursal Where IDCliente=" & IDCliente
        Else
            SQL = "Select 'ID'=0, Sucursal, Vendedor, Direccion, Telefono, Ciudad, Barrio, ZonaVenta, ListaPrecio, Condicion From VCliente Where ID=" & IDCliente & " Union All Select ID, Sucursal, Vendedor, Direccion, Telefono, Ciudad, Barrio, ZonaVenta, ListaPrecio, Condicion From VClienteSucursal Where IDCliente=" & IDCliente & " And IDVendedor=" & vgUsuarioIDVendedor
        End If

        dt = CSistema.ExecuteToDataTable(SQL).Copy
        CSistema.dtToGrid(dgv, dt)

        'Foco
        dgv.Focus()

    End Sub

    Sub Seleccionar()

        If dgv.SelectedRows.Count = 0 Then
            CSistema.MostrarError("Seleccione un registro para continuar!", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.MiddleRight)
        End If

        Seleccionado = True

        Dim ID As Integer = dgv.SelectedRows(0).Cells(0).Value

        If ID = 0 Then
            MatrizSeleccionada = True
            Me.Close()
            Exit Sub
        End If

        For Each oRow As DataRow In dt.Select(" ID = " & ID)
            Sucursal = oRow
            Me.Close()
            Exit Sub
        Next

    End Sub

    Private Sub frmSeleccionSucursalCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Seleccionado = False
        Me.Close()
    End Sub

    Private Sub dgv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyDown
        If e.KeyCode = Keys.Enter Then

            ' Your code here
            e.SuppressKeyPress = True

        End If

    End Sub

    Private Sub dgv_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyUp
        If e.KeyCode = Keys.Enter Then
            Seleccionar()
        End If
    End Sub


End Class
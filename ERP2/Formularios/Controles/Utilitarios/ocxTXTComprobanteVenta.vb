﻿Public Class ocxTXTComprobanteVenta

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private RegistroValue As DataRow
    Public Property Registro() As DataRow
        Get
            Return RegistroValue
        End Get
        Set(ByVal value As DataRow)
            RegistroValue = value
        End Set
    End Property

    Private SeleccionadoValue As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionadoValue = value
        End Set
    End Property

    Private AlturaMaximaValue As Integer
    Public Property AlturaMaxima() As Integer
        Get
            Return AlturaMaximaValue
        End Get
        Set(ByVal value As Integer)
            AlturaMaximaValue = value
        End Set
    End Property

    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property

    Private TextoValue As String
    Public Property Texto() As String
        Get
            Return TextoValue
        End Get
        Set(ByVal value As String)
            TextoValue = value
            txt.Text = value
        End Set
    End Property

    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value
            txt.ReadOnly = value
        End Set

    End Property

    'EVENTOS
    Public Event ItemSeleccionado(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ItemMalSeleccionado(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ItemInicializado(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Private dt As New DataTable
    Private dtTemp As New DataTable
    Private BindingSource1 As New BindingSource

    Sub Inicializar()

        'Controles
        txt.Clear()
        grid.DataSource = Nothing
        grid.Visible = False
        IDCliente = 0

        'Funciones
        Conectar()

    End Sub

    Public Sub Conectar()

        If IDCliente = 0 Then
            Exit Sub
        End If

        Dim Where As String = " Where IDCliente=" & IDCliente & " And Credito = 'True' And Cancelado = 'False' And Anulado = 'False' "
        Dim OrderBy As String = " Order By FechaEmision "
        Dim Consulta As String = "Select Comprobante, Saldo, [Fec. Venc.], PE, Sucursal, TipoComprobante, Fec, FechaEmision, Moneda, Cotizacion, Total, Cancelado  From VVenta "
        dt = CSistema.ExecuteToDataTable(Consulta & " " & Where & " " & OrderBy)
        BindingSource1.DataSource = dt

    End Sub

    Private Sub Listar()

        'Validar
        If txt.Focused = False Then
            Exit Sub
        End If

        Seleccionado = False

        Dim Where As String = ""
        Dim Valor As String = txt.Text.Trim

        Try

            Where = " Comprobante Like '%" & Valor & "%' "

            BindingSource1.Filter = Where
            grid.DataSource = BindingSource1.DataSource

            grid.Visible = True
            If AlturaMaxima > Math.Round(grid.RowCount * 20, 0) Then
                grid.Height = Math.Round(grid.RowCount * 20, 0)
            Else
                grid.Height = AlturaMaxima
            End If

            grid.BringToFront()

            Me.Height = grid.Height + txt.Height

            If grid.Columns.Count > 0 Then
                grid.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                grid.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                grid.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

                For i As Integer = 3 To grid.ColumnCount - 1
                    grid.Columns(i).Visible = False
                Next

            End If

        Catch ex As Exception

        End Try



    End Sub

    Private Sub OcultarLista()

        grid.Visible = False
        Me.Height = txt.Height

    End Sub

    Private Sub LimpiarSeleccion()

        Seleccionado = False
        Registro = Nothing

    End Sub

    Private Sub Seleccionar()

        'Validar
        If grid.RowCount = 0 Then
            LimpiarSeleccion()
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
            Exit Sub
        End If

        Try

            Dim oRow As DataRow = dt.Select(" Comprobante=" & grid.SelectedRows(0).Cells(0).Value)(0)

            Registro = oRow
            txt.Text = oRow("Comprobante").ToString
            OcultarLista()
            Seleccionado = True
            RaiseEvent ItemSeleccionado(New Object, New EventArgs)

        Catch ex As Exception
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
        End Try


    End Sub

    Private Sub ocxTXTComprobanteVenta_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LostFocus
        OcultarLista()
    End Sub

    Private Sub ocxTXTComprobanteVenta_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged

        'Tañanos
        txt.Width = Me.Width
        grid.Width = txt.Width

        If grid.Visible = False Then
            Me.Height = txt.Height
        End If

        'Localizacion
        grid.Left = txt.Left
        grid.Top = txt.Height

    End Sub

    Private Sub txt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt.TextChanged
        Listar()
    End Sub

    Private Sub txt_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt.KeyUp
        If e.KeyCode = Keys.Down Then
            If grid.RowCount > 0 Then
                grid.Focus()
                If grid.SelectedRows.Count = 0 Then
                    grid.Rows(0).Selected = True
                End If
            End If
        End If

        If e.KeyCode = Keys.Enter Then
            Seleccionar()
        End If

        If e.KeyCode = Keys.Escape Then
            OcultarLista()
        End If

    End Sub

    Private Sub grid_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grid.KeyDown
        If e.KeyCode = Keys.Enter Then
            Seleccionar()
        End If
    End Sub

End Class

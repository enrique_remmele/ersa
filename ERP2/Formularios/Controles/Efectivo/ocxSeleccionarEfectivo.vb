﻿Public Class ocxSeleccionarEfectivo

    'CLASES
    Dim CSistema As New CSistema

    'EVENTOS
    Public Event TotalCambiado(ByVal sender As Object, ByVal e As EventArgs)

    'PROPIEDADES

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private TotalPagoValue As Decimal
    Public Property TotalPago() As Decimal
        Get
            Return TotalPagoValue
        End Get
        Set(ByVal value As Decimal)
            TotalPagoValue = value
        End Set
    End Property

    Private TotalChequeValue As Decimal
    Public Property Totalcheque() As Decimal
        Get
            Return TotalChequeValue
        End Get
        Set(ByVal value As Decimal)
            TotalChequeValue = value
        End Set
    End Property

    Private ConsultaValue As Boolean
    Public Property Consulta() As Boolean
        Get
            Return ConsultaValue
        End Get
        Set(ByVal value As Boolean)
            ConsultaValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Funciones
        Listar()
        'Cargar()

        'Foco
        'dgw.Focus()

    End Sub

    Sub Listar()

        'Limpiar la grilla
        dgw.Rows.Clear()
        Dim TotalDeuda As Decimal = 0

        For Each oRow As DataRow In dt.Rows

            Dim oRow1(8) As String

            oRow1(0) = oRow("IDTransaccion").ToString
            oRow1(1) = CBool(oRow("Sel").ToString)
            oRow1(2) = oRow("Comprobante").ToString
            oRow1(3) = oRow("CodigoComprobante").ToString
            oRow1(4) = oRow("Fec").ToString
            oRow1(5) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            oRow1(6) = CSistema.FormatoMoneda(oRow("ImporteHabilitado").ToString)

            If Consulta = True Then
                oRow1(7) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Else
                oRow1(7) = CSistema.FormatoMoneda(oRow("ImporteHabilitado").ToString)
            End If

            oRow1(8) = CBool(oRow("Cancelar").ToString)


            'Sumar el total de la deuda
            If IsNumeric(oRow("Importe").ToString) = True Then
                TotalDeuda = TotalDeuda + CDec(oRow("Importe").ToString)
            End If

            dgw.Rows.Add(oRow1)



        Next
        CalcularTotales()


    End Sub

    Sub Seleccionar()

        For Each oRow As DataGridViewRow In dgw.Rows
            Dim IDTransaccion As Integer = oRow.Cells("colIDTransaccion").Value
            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Sel") = oRow.Cells("colSeleccionar").Value
                oRow1("Importe") = oRow.Cells("colImporte").Value
                oRow1("Cancelar") = oRow.Cells("colCancelar").Value
            Next
        Next

    End Sub

    Sub Cargar()

        For Each oRow As DataRow In dt.Rows

            Dim oRow1(8) As String

            oRow1(0) = "0"
            oRow1(1) = CBool(oRow("Sel").ToString)
            oRow1(2) = oRow("CodigoComprobante").ToString
            oRow1(3) = oRow("TipoComprobante").ToString
            oRow1(4) = oRow("Fec").ToString
            oRow1(5) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            oRow1(6) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            oRow1(7) = CSistema.FormatoMoneda(oRow("ImporteHabilitado").ToString)
            oRow1(8) = CBool(oRow("Cancelar").ToString)

            dgw.Rows.Add(oRow1)

        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim TotalComprobantes As Decimal = 0
        Dim TotalSeleccionado As Decimal = 0
        Dim CantidadComprobantes As Integer = 0
        Dim CantidadSeleccionado As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows

            If oRow.Cells("colSeleccionar").Value = True Then
                TotalSeleccionado = TotalSeleccionado + oRow.Cells("colImporte").Value
                CantidadSeleccionado = CantidadSeleccionado + 1
            End If

            TotalComprobantes = TotalComprobantes + oRow.Cells("colImporte").Value
            CantidadComprobantes = CantidadComprobantes + 1
        Next

        txtTotalComprobantes.SetValue(TotalComprobantes)
        txtCantidadComprobantes.SetValue(CantidadComprobantes)

        txtTotalSelecionado.SetValue(TotalSeleccionado)
        txtCantidadSeleccionado.SetValue(CantidadSeleccionado)

        Seleccionar()
        RaiseEvent TotalCambiado(New Object, New EventArgs)

    End Sub

    Sub PintarCelda()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(1).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next



        If dgw.CurrentRow Is Nothing Then
            Exit Sub
        End If

        dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub

    Public Function TotalSeleccionado() As Decimal

        TotalSeleccionado = 0
        TotalSeleccionado = CSistema.dtSumColumn(dt, "APagar", "Sel = 'True' ")


    End Function

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick
        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


        For Each oRow As DataGridViewRow In dgw.Rows

            If oRow.Index = RowIndex Then

                If oRow.Cells("colSeleccionar").Value = False Then


                    ''Calcular 
                    Dim TotalSeleccionado As Decimal = CSistema.gridSumColumn(dgw, "colImporte", "colSeleccionar", "True")
                    Dim TotalActual As Decimal = oRow.Cells("colSaldo").Value
                    Dim Resultante As Decimal = 0

                    If (TotalSeleccionado + TotalActual) >= TotalPago And TotalPago <> 0 Then
                        Resultante = TotalSeleccionado - TotalPago + Totalcheque
                    Else
                        Resultante = TotalActual
                    End If

                    If Resultante < 0 Then
                        Resultante = Resultante * -1
                    End If
                    oRow.Cells("colImporte").Value = Resultante

                    oRow.Cells("colSeleccionar").Value = True
                    oRow.Cells("colSeleccionar").ReadOnly = False
                    oRow.Cells("colImporte").ReadOnly = False
                    oRow.Cells("colCancelar").ReadOnly = False
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                    'dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("colImporte")
                Else
                    oRow.Cells("colSeleccionar").ReadOnly = True
                    oRow.Cells("colImporte").ReadOnly = True
                    oRow.Cells("colSeleccionar").Value = False
                    oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

                    oRow.Cells("colImporte").Value = oRow.Cells("colSaldo").Value

                    'Calcular 
                    dgw.Update()

                End If

                Exit For

            End If

        Next

        'If RowIndex < dgw.Rows.Count - 1 Then
        '    dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colImporte")
        'End If

        CalcularTotales()

        dgw.Update()

    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Columns(e.ColumnIndex).Name = "colImporte" Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value

            Else
                dgw.CurrentCell.Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells("colImporte").Value)
            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Enter Then

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSeleccionar").Value = False Then

                        ''Calcular 
                        Dim TotalSeleccionado As Decimal = CSistema.gridSumColumn(dgw, "colImporte", "colSeleccionar", "True")
                        Dim TotalActual As Decimal = oRow.Cells("colSaldo").Value
                        Dim Resultante As Decimal = 0

                        If (TotalSeleccionado + TotalActual) >= TotalPago Then
                            Resultante = TotalSeleccionado - TotalPago + Totalcheque
                        Else
                            Resultante = TotalActual
                        End If

                        If Resultante < 0 Then
                            Resultante = Resultante * -1
                        End If
                        oRow.Cells("colImporte").Value = Resultante

                        oRow.Cells("colSeleccionar").Value = True
                        oRow.Cells("colSeleccionar").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = False
                        oRow.Cells("colCancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        'dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("colImporte")
                    Else
                        oRow.Cells("colSeleccionar").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colSeleccionar").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

                        oRow.Cells("colImporte").Value = oRow.Cells("colSaldo").Value

                        'Calcular 
                        dgw.Update()

                    End If

                    Exit For

                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colImporte")
            End If

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If
    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        Try
            For Each oRow As DataGridViewRow In dgw.Rows
                If oRow.Index = e.RowIndex Then
                    If oRow.Cells("colSeleccionar").Value = False Then
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    oRow.DefaultCellStyle.Font = f1


                    oRow.Cells("colImporte").Selected = True

                Else

                    oRow.DefaultCellStyle.Font = f2

                    If oRow.Cells("colSeleccionar").Value = False Then
                        oRow.DefaultCellStyle.BackColor = Color.White
                    Else
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                    End If
                End If
            Next
        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells("colImporte")
        End If

    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = True
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = False
        Next

        PintarCelda()
        CalcularTotales()
    End Sub

End Class

﻿Public Class ocxComisionProducto

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'EVENTOS
    Public Event ErrorProducido(ByVal mensaje As String)
    Public Event RegistroProcesado(ByVal mensaje As String)

    'PROPIEDADES
    Private IDProductoValue As Integer
    Public Property IDProducto() As Integer
        Get
            Return IDProductoValue
        End Get
        Set(ByVal value As Integer)
            IDProductoValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtListaVendedor As DataTable
    Dim dtComisionProducto As DataTable

    'FUNCIONES
    Sub Iniciarlizar()

        'Funciones
        CargarInformacion()
        ListarListaVendedor()

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, New Button, btnModificar, btnCancelar, btnGuardar, New Button, Nothing)

    End Sub

    Sub CargarInformacion()

        'Listas de Precio
        dtListaVendedor = New DataTable

        dtListaVendedor.Columns.Add("ID")
        dtListaVendedor.Columns.Add("Nombres")
        dtListaVendedor.Columns.Add("%Base")
        dtListaVendedor.Columns.Add("%EsteProducto")
        dtListaVendedor.Columns.Add("%Anterior")
        dtListaVendedor.Columns.Add("Ult.Cambio")
        dtListaVendedor.Columns.Add("Usuario")

        For Each oRow As DataRow In CData.GetTable("VVendedor", "  ").Rows
            Dim NewRow As DataRow = dtListaVendedor.NewRow
            NewRow("ID") = oRow("ID")
            NewRow("Nombres") = oRow("Nombres")
            NewRow("%Base") = 0
            NewRow("%EsteProducto") = 0
            NewRow("%Anterior") = 0
            NewRow("Ult.Cambio") = "---"
            NewRow("Usuario") = "---"
            dtListaVendedor.Rows.Add(NewRow)

        Next

        CSistema.SqlToComboBox(cbxListaPrecio.cbx, dtListaVendedor, "ID", "Nombres")

    End Sub

    Sub ListarListaVendedor()

        dgw.DataSource = Nothing
        dgw.DataSource = dtListaVendedor

        'Tamaño de Columanas
        For i As Integer = 0 To dgw.ColumnCount - 1
            dgw.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Next

        dgw.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgw.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        ''Formato
        'dgw.Columns(5).DefaultCellStyle.Format = "N0"

        ''Alineacion
        'dgw.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight


    End Sub

    Sub ListarComisionProductos()

        If IDProducto = 0 Then
            Exit Sub
        End If

        'Ocultamos el dgw
        dgw.Visible = False

        'Obtenemos la lista de vendedores
        dtComisionProducto = CSistema.ExecuteToDataTable("Select * From VComisionProducto Where IDProducto = " & IDProducto).Copy

        'Recorrer el dgw y actualizar los datos
        For Each oRow As DataGridViewRow In dgw.Rows
            Dim IDListaVendedor As Integer = oRow.Cells(0).Value
            Dim Existe As Boolean = False
            Dim oRow1 As DataRow = Nothing

            If dtComisionProducto.Select("IDListaVendedor=" & IDListaVendedor & " And IDProducto= " & IDProducto).Count > 0 Then
                oRow1 = dtComisionProducto.Select("IDListaVendedor=" & IDListaVendedor & " And IDProducto=" & IDProducto)(0)
                Existe = True
            End If

            'Vemos si existe el producto
            If Existe = True Then

                oRow.Cells(2).Value = oRow1("PorcentajeBase").ToString
                oRow.Cells(3).Value = oRow1("PorcentajeEsteProducto").ToString
                oRow.Cells(4).Value = oRow1("PorcentajeAnterior").ToString
                oRow.Cells(5).Value = oRow1("Ult.Cambio").ToString
                oRow.Cells(6).Value = oRow1("Usuario").ToString
                ' oRow.DefaultCellStyle.WrapMode = DataGridViewTriState.True
            Else
                oRow.Cells(2).Value = "0"
                oRow.Cells(3).Value = "0"
                oRow.Cells(4).Value = "0"
                oRow.Cells(5).Value = "---"
                oRow.Cells(6).Value = "---"
                ' oRow.DefaultCellStyle.WrapMode = DataGridViewTriState.True
            End If

        Next

        dgw.Visible = True

    End Sub

    Sub SeleccionarLista()

        'cbxListaPrecio.cbx.Text = ""
        txtPorcentajeBase.SetValue(0)
        txtPorcentajeEsteProducto.SetValue(0)


        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        BloquearControles(True)

        For Each oRow As DataGridViewRow In dgw.SelectedRows
            cbxListaPrecio.cbx.Text = oRow.Cells(1).Value
            txtPorcentajeBase.txt.Text = oRow.Cells(2).Value
            txtPorcentajeEsteProducto.txt.Text = oRow.Cells(3).Value
        Next

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, New Button, btnModificar, btnCancelar, btnGuardar, New Button, Nothing)

    End Sub

    Sub Modificar()

        If IDProducto = 0 Then
            Exit Sub
        End If

        BloquearControles(False)

        cbxListaPrecio.cbx.Focus()

       CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, New Button, btnModificar, btnCancelar, btnGuardar, New Button, Nothing)

    End Sub

    Sub Guardar()

        If IDProducto = 0 Then
            Exit Sub
        End If

        ctrError.Clear()

        BloquearControles(True)

        'Validar
        'Seleccion de Lista de Precio
        If IsNumeric(cbxListaPrecio.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente una lista de vendedor!"
            ctrError.SetError(cbxListaPrecio, mensaje)
            ctrError.SetIconAlignment(cbxListaPrecio, ErrorIconAlignment.MiddleRight)
            RaiseEvent ErrorProducido(mensaje)
            Exit Sub
        End If

        Dim Base As Boolean
        If txtPorcentajeBase.ObtenerValor = 0 Then
            Base = False
        Else
            Base = True
        End If

        'Procesar
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@IDProducto", IDProducto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDVendedor", cbxListaPrecio.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Base", Base, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PorcentajeBase", CSistema.FormatoNumeroBaseDatos(txtPorcentajeBase.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PorcentajeEsteProducto", CSistema.FormatoNumeroBaseDatos(txtPorcentajeEsteProducto.txt.Text, True), ParameterDirection.Input)

        'CSistema.SetSQLParameter(param, "@Operacion", Operacion, ParameterDirection.Input)


        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpComisionProducto", False, False, MensajeRetorno) = True Then
            RaiseEvent RegistroProcesado(MensajeRetorno)
            Actualizar()

        Else
            RaiseEvent ErrorProducido("Atencion: " & MensajeRetorno)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.GUARDAR, New Button, btnModificar, btnCancelar, btnGuardar, New Button, Nothing)

    End Sub

    Sub Cancelar()

        BloquearControles(True)

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, New Button, btnModificar, btnCancelar, btnGuardar, New Button, Nothing)

    End Sub

    Sub BloquearControles(ByVal value As Boolean)

        cbxListaPrecio.SoloLectura = value
        txtPorcentajeBase.SoloLectura = value
        txtPorcentajeEsteProducto.SoloLectura = value


    End Sub

    Sub Actualizar()

        ctrError.Clear()

        'Recargamos el dtLsitaprecio
        CData.ResetTable("VVendedor")
        dtComisionProducto = CData.GetTable("VVendedor", )
        CData.OrderDataTable(dtComisionProducto, "Nombres")

        'limpiamos el datagrid
        dgw.Columns.Clear()

        'volvemos a cargar
        ListarListaVendedor()

        'volvems a cargar la lista de vendedores
        ListarComisionProductos()



    End Sub

    Private Sub ocxListaPrecioProducto_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Iniciarlizar()
    End Sub

    Private Sub btnActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        Actualizar()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Excepciones()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)
        If MessageBox.Show("Desea copiar el Precio Unitario a todas las listas de precio?", "Precio Unitario", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = DialogResult.No Then
            Exit Sub
        End If
        Guardar()
    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)
        If MessageBox.Show("Desea copiar el Porcentaje de TPR a todas las listas de precio?", "TPR", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = DialogResult.No Then
            Exit Sub
        End If
        Guardar()
    End Sub

    Private Sub dgw_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgw.SelectionChanged
        SeleccionarLista()
    End Sub

End Class



﻿Public Class ocxListaPrecioProducto

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'EVENTOS
    Public Event ErrorProducido(ByVal mensaje As String)
    Public Event RegistroProcesado(ByVal mensaje As String)

    'PROPIEDADES
    Private IDProductoValue As Integer
    Public Property IDProducto() As Integer
        Get
            Return IDProductoValue
        End Get
        Set(ByVal value As Integer)
            IDProductoValue = value
        End Set
    End Property

    Private ReferenciaValue As String
    Public Property Referencia() As String
        Get
            Return ReferenciaValue
        End Get
        Set(ByVal value As String)
            ReferenciaValue = value
        End Set
    End Property


    'VARIABLES
    Dim dtLitaPrecio As DataTable
    Dim dtProductoLitaPrecio As DataTable

    'FUNCIONES
    Sub Iniciarlizar()

        'Controles
        txtDesde.PermitirNulo = True
        txtHasta.PermitirNulo = True

        'Funciones
        CargarInformacion()
        ListarListaPrecio()

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, New Button, btnModificar, btnCancelar, btnGuardar, New Button, Nothing)

    End Sub

    Sub CargarInformacion()

        'Listas de Precio
        dtLitaPrecio = CData.GetTable("VListaPrecio", "Estado = 'True' And IDSucursal=" & vgIDSucursal & " ")
        CSistema.SqlToComboBox(cbxListaPrecio.cbx, dtLitaPrecio, "ID", "Descripcion")

    End Sub

    Sub ListarListaPrecio()

        lvListaPrecio.Items.Clear()

        For Each oRow As DataRow In dtLitaPrecio.Rows
            Dim item As New ListViewItem(oRow("ID").ToString)
            item.SubItems.Add(oRow("Descripcion").ToString)
            item.SubItems.Add("GS")
            item.SubItems.Add("0")
            item.SubItems.Add("")
            item.SubItems.Add("---")
            item.SubItems.Add("---")

            lvListaPrecio.Items.Add(item)
        Next

    End Sub

    Sub ListarPreciosProductos()

        If IDProducto = 0 Then
            Exit Sub
        End If

        'Ocultamos el lv
        lvListaPrecio.Visible = False

        'Obtenemos la lista de precio
        dtProductoLitaPrecio = CSistema.ExecuteToDataTable("Select * From VProductoListaPrecio Where IDProducto = " & IDProducto & " And IDSucursal=" & vgIDSucursal).Copy

        'Recorremos el listview y actualizamos los datos
        For Each item As ListViewItem In lvListaPrecio.Items
            Dim IDListaPrecio As Integer = item.Text
            Dim Moneda As String = item.SubItems(2).Text
            Dim Existe As Boolean = False
            Dim oRow As DataRow = Nothing

            'Si ya tiene moneda... Buscar por moneda
            If Moneda <> "" Then

                If dtProductoLitaPrecio.Select(" IDListaPrecio = " & IDListaPrecio & " And IDProducto = " & IDProducto & " And Moneda = '" & Moneda & "' And IDSucursal=" & vgIDSucursal).Count > 0 Then
                    oRow = dtProductoLitaPrecio.Select(" IDListaPrecio = " & IDListaPrecio & " And IDProducto = " & IDProducto & " And Moneda = '" & Moneda & "' And IDSucursal=" & vgIDSucursal)(0)
                    Existe = True
                End If

            Else

                If dtProductoLitaPrecio.Select(" IDListaPrecio = " & IDListaPrecio & " And IDProducto = " & IDProducto & " And IDSucursal=" & vgIDSucursal).Count > 0 Then
                    oRow = dtProductoLitaPrecio.Select(" IDListaPrecio = " & IDListaPrecio & " And IDProducto = " & IDProducto & " And IDSucursal=" & vgIDSucursal)(0)
                    Existe = True
                End If

            End If

            'Vemos si esta en el producto
            If Existe = True Then

                item.SubItems(2).Text = oRow("Moneda").ToString
                item.SubItems(3).Text = oRow("Precio").ToString

                item.SubItems(4).Text = oRow("TPRPorcentaje").ToString & "%"
                item.SubItems(5).Text = oRow("TPRDesde").ToString
                item.SubItems(6).Text = oRow("TPRHasta").ToString

                'Pintar
                item.BackColor = Color.FromArgb(vgColorSoloLectura)

            Else

                item.SubItems(2).Text = ""
                item.SubItems(3).Text = "0"

                item.SubItems(4).Text = ""
                item.SubItems(5).Text = "---"
                item.SubItems(6).Text = "---"

                item.BackColor = Color.White

            End If

        Next

        'Formato
        CSistema.FormatoNumero(lvListaPrecio, 3)

        lvListaPrecio.Visible = True

    End Sub

    Sub ListarPrecioProductoReferencia()

        If Referencia.Length = 0 Then
            Exit Sub
        End If

        'Ocultamos el lv
        lvListaPrecio.Visible = False

        'Obtenemos la lista de precio
        dtProductoLitaPrecio = CSistema.ExecuteToDataTable("Select * From VProductoListaPrecio Where Referencia = '" & Referencia & "' And IDSucursal=" & vgIDSucursal).Copy

        'Recorremos el listview y actualizamos los datos
        For Each item As ListViewItem In lvListaPrecio.Items
            Dim IDListaPrecio As Integer = item.Text
            Dim Moneda As String = item.SubItems(2).Text
            Dim Existe As Boolean = False
            Dim oRow As DataRow = Nothing

            'Si ya tiene moneda... Buscar por moneda
            If Moneda <> "" Then

                If dtProductoLitaPrecio.Select(" IDListaPrecio = " & IDListaPrecio & " And Referencia = '" & Referencia & "' And Moneda = '" & Moneda & "' And IDSucursal=" & vgIDSucursal).Count > 0 Then
                    oRow = dtProductoLitaPrecio.Select(" IDListaPrecio = " & IDListaPrecio & " And Referencia = '" & Referencia & "' And Moneda = '" & Moneda & "' And IDSucursal=" & vgIDSucursal)(0)
                    Existe = True
                End If

            Else

                If dtProductoLitaPrecio.Select(" IDListaPrecio = " & IDListaPrecio & " And Referencia = '" & IDProducto & "' And IDSucursal=" & vgIDSucursal).Count > 0 Then
                    oRow = dtProductoLitaPrecio.Select(" IDListaPrecio = " & IDListaPrecio & " And Referencia = '" & Referencia & "' And IDSucursal=" & vgIDSucursal)(0)
                    Existe = True
                End If

            End If

            'Vemos si esta en el producto
            If Existe = True Then

                item.SubItems(2).Text = oRow("Moneda").ToString
                item.SubItems(3).Text = oRow("Precio").ToString

                item.SubItems(4).Text = oRow("TPRPorcentaje").ToString & "%"
                item.SubItems(5).Text = oRow("TPRDesde").ToString
                item.SubItems(6).Text = oRow("TPRHasta").ToString

                'Pintar
                item.BackColor = Color.FromArgb(vgColorSoloLectura)

            Else

                item.SubItems(2).Text = ""
                item.SubItems(3).Text = "0"

                item.SubItems(4).Text = ""
                item.SubItems(5).Text = "---"
                item.SubItems(6).Text = "---"

                item.BackColor = Color.White

            End If

        Next

        'Formato
        CSistema.FormatoNumero(lvListaPrecio, 3)

        lvListaPrecio.Visible = True

    End Sub
    Sub SeleccionarLista()

        cbxListaPrecio.cbx.Text = ""
        cbxMoneda.cbx.Text = ""
        txtPrecioUnitario.SetValue(0)
        txtTPR.SetValue(0)
        txtDesde.txt.Clear()
        txtHasta.txt.Clear()

        If lvListaPrecio.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        BloquearControles(True)

        For Each item As ListViewItem In lvListaPrecio.SelectedItems

            cbxListaPrecio.cbx.Text = item.SubItems(1).Text
            cbxMoneda.cbx.Text = item.SubItems(2).Text
            txtPrecioUnitario.SetValue(item.SubItems(3).Text)
            txtTPR.SetValue(item.SubItems(4).Text.Replace("%", ""))
            txtDesde.SetValue(item.SubItems(5).Text)
            txtHasta.SetValue(item.SubItems(6).Text)

        Next

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, New Button, btnModificar, btnCancelar, btnGuardar, New Button, Nothing)

    End Sub

    Sub Modificar()

        If IDProducto = 0 Then
            Exit Sub
        End If

        BloquearControles(False)

        cbxListaPrecio.cbx.Focus()

        If cbxMoneda.cbx.Text = "" Then
            cbxMoneda.cbx.Text = CData.GetTable("VMoneda", " ID = 1 ")(0)("Referencia").ToString
        End If

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, New Button, btnModificar, btnCancelar, btnGuardar, New Button, Nothing)

    End Sub

    Sub Guardar(ByVal Operacion As String)

        If IDProducto = 0 Then
            Exit Sub
        End If

        ctrError.Clear()

        BloquearControles(True)

        'Validar
        'Seleccion de Lista de Precio
        If IsNumeric(cbxListaPrecio.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente una lista de precio!"
            ctrError.SetError(cbxListaPrecio, mensaje)
            ctrError.SetIconAlignment(cbxListaPrecio, ErrorIconAlignment.MiddleRight)
            RaiseEvent ErrorProducido(mensaje)
            Exit Sub
        End If

        'Seleccion de Moneda
        If IsNumeric(cbxMoneda.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente una moneda!"
            ctrError.SetError(cbxMoneda, mensaje)
            ctrError.SetIconAlignment(cbxMoneda, ErrorIconAlignment.MiddleRight)
            RaiseEvent ErrorProducido(mensaje)
            Exit Sub
        End If


        'Procesar
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@IDProducto", IDProducto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDListaPrecio", cbxListaPrecio.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Precio", txtPrecioUnitario.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TPRPorcentaje", CSistema.FormatoMonedaBaseDatos(txtTPR.ObtenerValor, True), ParameterDirection.Input)

        If IsDate(txtDesde.txt.Text) = True Then
            CSistema.SetSQLParameter(param, "@TPRDesde", txtDesde.GetValueString, ParameterDirection.Input)
        End If

        If IsDate(txtHasta.txt.Text) = True Then
            CSistema.SetSQLParameter(param, "@TPRHasta", txtHasta.GetValueString, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Operacion", Operacion, ParameterDirection.Input)


        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpProductoListaPrecio", False, False, MensajeRetorno) = True Then
            RaiseEvent RegistroProcesado(MensajeRetorno)
            Actualizar()

        Else
            RaiseEvent ErrorProducido("Atencion: " & MensajeRetorno)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.GUARDAR, New Button, btnModificar, btnCancelar, btnGuardar, New Button, Nothing)

    End Sub

    Sub Cancelar()

        BloquearControles(True)

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, New Button, btnModificar, btnCancelar, btnGuardar, New Button, Nothing)

    End Sub

    Sub BloquearControles(ByVal value As Boolean)

        cbxListaPrecio.SoloLectura = value
        cbxMoneda.SoloLectura = value
        txtPrecioUnitario.SoloLectura = value

    End Sub

    Sub Actualizar()

        ctrError.Clear()

        'Recargamos el dtLsitaprecio
        CData.ResetTable("VListaPrecio")
        dtLitaPrecio = CData.GetTable("VListaPrecio", " EStado = 'True' And IDSucursal = " & vgIDSucursal)
        CData.OrderDataTable(dtLitaPrecio, "Descripcion")

        'Limpiamos el listview
        lvListaPrecio.Items.Clear()

        'Volvemos a Cargar
        ListarListaPrecio()

        'Volvemos a establecer la Lista de Precio
        ListarPreciosProductos()

    End Sub

    Sub Excepciones()

        If lvListaPrecio.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim ID As Integer
        ID = lvListaPrecio.SelectedItems(0).Text
        If dtProductoLitaPrecio.Select(" IDProducto=" & IDProducto & " And IDListaPrecio=" & ID & " And IDSucursal=" & vgIDSucursal & " ").Count = 0 Then
            MessageBox.Show("Establezca primeramente el precio de lista", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Dim frm As New frmExcepciones
        frm.IDProducto = IDProducto
        frm.IDListaPrecio = lvListaPrecio.SelectedItems(0).Text

        FGMostrarFormulario(Me.ParentForm, frm, "Excepciones de Precio", FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Private Sub ocxListaPrecioProducto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Iniciarlizar()
    End Sub

    Private Sub btnActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        Actualizar()
    End Sub

    Private Sub lvListaPrecio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvListaPrecio.SelectedIndexChanged
        SeleccionarLista()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        'Verificamos si existe
        Dim ID As Integer
        If lvListaPrecio.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        ID = lvListaPrecio.SelectedItems(0).Text

        Dim Operacion As ERP.CSistema.NUMOperacionesABM
        If dtProductoLitaPrecio.Select(" IDProducto=" & IDProducto & " And IDListaPrecio=" & ID & " And IDSucursal=" & vgIDSucursal & " ").Count = 0 Then
            Operacion = ERP.CSistema.NUMOperacionesABM.INS
        Else
            Operacion = ERP.CSistema.NUMOperacionesABM.UPD
        End If

        Guardar(Operacion.ToString)

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Excepciones()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        If MessageBox.Show("Desea copiar el Precio Unitario a todas las listas de precio?", "Precio Unitario", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = DialogResult.No Then
            Exit Sub
        End If

        Guardar("PRECIO")

    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        If MessageBox.Show("Desea copiar el Porcentaje de TPR a todas las listas de precio?", "TPR", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = DialogResult.No Then
            Exit Sub
        End If

        Guardar("TPR")
    End Sub

    Private Sub txtDesde_Foco() Handles txtDesde.Foco
        Dim Valor As String = txtTPR.ObtenerValor
        If Valor = 0 Then
            txtDesde.txt.Clear()
        End If
    End Sub

    Private Sub txtDesde_FocoPerdido() Handles txtDesde.FocoPerdido
        Dim Valor As String = txtTPR.ObtenerValor
        If Valor = 0 Then
            txtDesde.txt.Clear()
        End If
    End Sub

    Private Sub txtHasta_Foco() Handles txtHasta.Foco
        If txtPrecioUnitario.ObtenerValor = 0 Then
            txtHasta.txt.Clear()
        End If
    End Sub

    Private Sub txtHasta_FocoPerdido() Handles txtHasta.FocoPerdido
        If txtPrecioUnitario.ObtenerValor = 0 Then
            txtHasta.txt.Clear()
        End If
    End Sub

End Class

﻿Public Class ocxProductoClasificacion2

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'EVENTOS
    Public Event CheckedItem()

    'PROPIEDADES
    Private NivelValue As Integer
    Public Property Nivel() As Integer
        Get
            Return NivelValue
        End Get
        Set(ByVal value As Integer)
            NivelValue = value
        End Set
    End Property

    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private CheckBoxValue As Boolean
    Public Property CheckBox() As Boolean
        Get
            Return CheckBoxValue
        End Get
        Set(ByVal value As Boolean)
            CheckBoxValue = value
            'tv.CheckBoxes = value
        End Set
    End Property

    Private CheckBox2Value As Boolean
    Public Property CheckBox2() As Boolean
        Get
            Return CheckBox2Value
        End Get
        Set(ByVal value As Boolean)
            CheckBox2Value = value
            tv.CheckBoxes = value
        End Set
    End Property

    Private TablaRelacionValue As String
    Public Property TablaRelacion() As String
        Get
            Return TablaRelacionValue
        End Get
        Set(ByVal value As String)
            TablaRelacionValue = value
        End Set
    End Property

    Private TablaRelacionGuardarValue As String
    Public Property TablaRelacionGuardar() As String
        Get
            Return TablaRelacionGuardarValue
        End Get
        Set(ByVal value As String)
            TablaRelacionGuardarValue = value
        End Set
    End Property

    Private TablaRelacionVistaValue As String
    Public Property TablaRelacionVista() As String
        Get
            Return TablaRelacionVistaValue
        End Get
        Set(ByVal value As String)
            TablaRelacionVistaValue = value
        End Set
    End Property

    Private CampoDescripcionValue As String
    Public Property CampoDescripcion() As String
        Get
            Return CampoDescripcionValue
        End Get
        Set(ByVal value As String)
            CampoDescripcionValue = value
        End Set
    End Property

    Private CampoIDValue As String
    Public Property CampoID() As String
        Get
            Return CampoIDValue
        End Get
        Set(ByVal value As String)
            CampoIDValue = value
        End Set
    End Property

    Private CampoIDPadreValue As String
    Public Property CampoIDPadre() As String
        Get
            Return CampoIDPadreValue
        End Get
        Set(ByVal value As String)
            CampoIDPadreValue = value
        End Set
    End Property

    Private SeleccionValue As Boolean
    Public Property Seleccion() As Boolean
        Get
            Return SeleccionValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionValue = value
        End Set
    End Property

    Private dtPathValue As DataTable
    Public Property dtPath() As DataTable
        Get
            Return dtPathValue
        End Get
        Set(ByVal value As DataTable)
            dtPathValue = value
        End Set
    End Property

    Private BloquearValue As Boolean
    Public Property Bloquear() As Boolean
        Get
            Return BloquearValue
        End Get
        Set(ByVal value As Boolean)
            BloquearValue = value
        End Set
    End Property

    'VARIABLES

    'FUNCIONES
    Sub Inicialiar()

        'Estructurar datatable
        dtPath = New DataTable("Path")
        dtPath.Columns.Add("IDTipoProducto")
        dtPath.Columns.Add("IDLinea")
        dtPath.Columns.Add("IDSubLinea")
        dtPath.Columns.Add("IDSubLinea2")
        dtPath.Columns.Add("IDMarca")
        dtPath.Columns.Add("IDPresentacion")

        Bloquear = False

        If Seleccion = True Then
            tv.ContextMenuStrip = ContextMenuStrip2
        End If

        Listar()

    End Sub

    Sub Listar()

        tv.Nodes.Clear()

        If Nivel < 0 Then
            Exit Sub
        End If

        Dim dt As DataTable = CData.GetTable("VTipoProducto")
        For Each oRow As DataRow In dt.Rows

            Dim nodo As New TreeNode(oRow("Descripcion").ToString)
            nodo.Name = oRow("ID").ToString

            tv.Nodes.Add(nodo)

            ListarLinea(nodo)

        Next

    End Sub

    Sub ListarLinea(ByVal nodoPadre As TreeNode)

        If Nivel < 1 Then
            Exit Sub
        End If

        Dim dt As DataTable = CData.GetTable("VTipoProductoLinea", " IDTipoProducto = " & nodoPadre.Name)
        For Each oRow As DataRow In dt.Rows

            Dim nodo As New TreeNode(oRow("Linea").ToString)
            nodo.Name = oRow("IDLinea").ToString

            nodoPadre.Nodes.Add(nodo)

            ListarSubLinea(nodo)

        Next

    End Sub

    Sub ListarSubLinea(ByVal nodoPadre As TreeNode)

        If Nivel < 2 Then
            Exit Sub
        End If

        Dim dt As DataTable = CData.GetTable("VLineaSubLinea", " IDLinea = " & nodoPadre.Name & " And IDTipoProducto = " & nodoPadre.Parent.Name)
        For Each oRow As DataRow In dt.Rows

            Dim nodo As New TreeNode(oRow("SubLinea").ToString)
            nodo.Name = oRow("IDSubLinea").ToString

            nodoPadre.Nodes.Add(nodo)

            ListarSubLinea2(nodo)

        Next

    End Sub

    Sub ListarSubLinea2(ByVal nodoPadre As TreeNode)

        If Nivel < 3 Then
            Exit Sub
        End If

        Dim dt As DataTable = CData.GetTable("VSubLineaSubLinea2", " IDSubLinea = " & nodoPadre.Name & " And IDLinea = " & nodoPadre.Parent.Name & " And IDTipoProducto = " & nodoPadre.Parent.Parent.Name)
        For Each oRow As DataRow In dt.Rows

            Dim nodo As New TreeNode(oRow("SubLinea2").ToString)
            nodo.Name = oRow("IDSubLinea2").ToString

            nodoPadre.Nodes.Add(nodo)

            ListarMarca(nodo)

        Next



    End Sub

    Sub ListarMarca(ByVal nodoPadre As TreeNode)

        If Nivel < 4 Then
            Exit Sub
        End If

        Dim dt As DataTable = CData.GetTable("VSubLinea2Marca", " IDSubLinea2 = " & nodoPadre.Name & " And IDSubLinea = " & nodoPadre.Parent.Name & " And IDLinea = " & nodoPadre.Parent.Parent.Name & " And IDTipoProducto = " & nodoPadre.Parent.Parent.Parent.Name)
        For Each oRow As DataRow In dt.Rows

            Dim nodo As New TreeNode(oRow("Marca").ToString)
            nodo.Name = oRow("IDMarca").ToString

            nodoPadre.Nodes.Add(nodo)

            ListarPresentacion(nodo)

        Next

    End Sub

    Sub ListarPresentacion(ByVal nodoPadre As TreeNode)

        If Nivel < 5 Then
            Exit Sub
        End If

        Debug.Print(" IDMarca = " & nodoPadre.Name & " And IDSubLinea2 = " & nodoPadre.Parent.Name & " And IDSubLinea = " & nodoPadre.Parent.Parent.Name & " And IDLinea = " & nodoPadre.Parent.Parent.Parent.Name & " And IDTipoProducto = " & nodoPadre.Parent.Parent.Parent.Parent.Name)

        Dim dt As DataTable = CData.GetTable("VMarcaPresentacion", " IDMarca = " & nodoPadre.Name & " And IDSubLinea2 = " & nodoPadre.Parent.Name & " And IDSubLinea = " & nodoPadre.Parent.Parent.Name & " And IDLinea = " & nodoPadre.Parent.Parent.Parent.Name & " And IDTipoProducto = " & nodoPadre.Parent.Parent.Parent.Parent.Name)
        For Each oRow As DataRow In dt.Rows

            Dim nodo As New TreeNode(oRow("Presentacion").ToString)
            nodo.Name = oRow("IDPresentacion").ToString

            nodoPadre.Nodes.Add(nodo)

        Next

    End Sub

    Public Sub Registrar()

        'Iniciamos el dt
        dtPath.Rows.Clear()

        'Cargamos el dt
        CargarRegistros()

        'Guardamos en la base de datos
        Guardar()

    End Sub

    Private Sub CargarRegistros(Optional ByVal nodo1 As TreeNodeCollection = Nothing)

        If nodo1 Is Nothing Then
            nodo1 = tv.Nodes
        End If

        For Each nodo2 As TreeNode In nodo1

            If nodo2.Nodes.Count > 0 Then
                CargarRegistros(nodo2.Nodes)
            End If

            If nodo2.Checked = True And nodo2.Level = Nivel Then
                InsertarRegistro(nodo2)
            End If
        Next

    End Sub

    Private Sub Guardar()

        Dim CampoRelacion As String = ""

        Select Case Nivel + 1
            Case 0
                CampoRelacion = "IDTipoProducto"
            Case 1
                CampoRelacion = "IDLinea"
            Case 2
                CampoRelacion = "IDSubLinea"
            Case 3
                CampoRelacion = "IDSubLinea2"
            Case 4
                CampoRelacion = "IDMarca"
            Case 5
                CampoRelacion = "IDPresentacion"
        End Select


        'Eliminar todas las relaciones primeramente
        Dim Procesados As Integer ' = CSistema.ExecuteNonQuery("Delete From " & TablaRelacionGuardar & " Where " & CampoRelacion & " = " & ID)

        'Grabar
        Dim sql As String = ""
        For Each orow As DataRow In dtPath.Rows

            Select Case Nivel

                Case 0
                    'Verificar primero si existe
                    If CBool(CSistema.ExecuteScalar("Select Case When (Select " & CampoRelacion & " From " & TablaRelacionGuardar & " Where IDLinea= " & ID & " And IDTipoProducto=" & orow("IDTipoProducto") & ") Is Null Then 'False' Else 'True' End")) = False Then
                        sql = sql & "Insert Into " & TablaRelacion & "(IDTipoProducto, IDLinea) Values(" & orow("IDTipoProducto") & ", " & ID & " )" & vbCrLf
                        'sql = sql & "Update " & TablaRelacionGuardar & " Set IDTipoProducto=" & orow("IDTipoProducto") & " Where IDLinea= " & ID & " And IDTipoProducto=IDTipoProducto" & vbCrLf
                    End If
                Case 1
                    If CBool(CSistema.ExecuteScalar("Select Case When (Select " & CampoRelacion & " From " & TablaRelacionGuardar & " Where IDSubLinea=" & ID & " And IDTipoProducto=" & orow("IDTipoProducto") & " And IDLinea=" & orow("IDLinea") & ") Is Null Then 'False' Else 'True' End")) = False Then
                        sql = sql & "Insert Into " & TablaRelacion & "(IDTipoProducto, IDLinea, IDSubLinea) Values(" & orow("IDTipoProducto") & ", " & orow("IDLinea") & " , " & ID & " )" & vbCrLf
                    End If
                Case 2
                    If CBool(CSistema.ExecuteScalar("Select Case When (Select " & CampoRelacion & " From " & TablaRelacionGuardar & " Where IDSubLinea2= " & ID & " And IDTipoProducto=" & orow("IDTipoProducto") & " And IDLinea=" & orow("IDLinea") & " And IDSubLinea=" & orow("IDSubLinea") & ") Is Null Then 'False' Else 'True' End")) = False Then
                        sql = sql & "Insert Into " & TablaRelacion & "(IDTipoProducto, IDLinea, IDSubLinea, IDSubLinea2) Values(" & orow("IDTipoProducto") & ", " & orow("IDLinea") & ", " & orow("IDSubLinea") & ", " & ID & " )" & vbCrLf
                    End If
                Case 3
                    If CBool(CSistema.ExecuteScalar("Select Case When (Select " & CampoRelacion & " From " & TablaRelacionGuardar & " Where IDMarca= " & ID & " And IDTipoProducto=" & orow("IDTipoProducto") & " And IDLinea=" & orow("IDLinea") & " And IDSubLinea=" & orow("IDSubLinea") & " And IDSubLinea2=" & orow("IDSubLinea2") & ") Is Null Then 'False' Else 'True' End")) = False Then
                        sql = sql & "Insert Into " & TablaRelacion & "(IDTipoProducto, IDLinea, IDSubLinea, IDSubLinea2, IDMarca) Values(" & orow("IDTipoProducto") & ", " & orow("IDLinea") & ", " & orow("IDSubLinea") & ", " & orow("IDSubLinea2") & ", " & ID & ")" & vbCrLf
                    End If
                Case 4
                    If CBool(CSistema.ExecuteScalar("Select Case When (Select " & CampoRelacion & " From " & TablaRelacionGuardar & " Where IDPresentacion= " & ID & " And IDTipoProducto=" & orow("IDTipoProducto") & " And IDLinea=" & orow("IDLinea") & " And IDSubLinea=" & orow("IDSubLinea") & " And IDSubLinea2=" & orow("IDSubLinea2") & " And IDMarca=" & orow("IDMarca") & ") Is Null Then 'False' Else 'True' End")) = False Then
                        sql = sql & "Insert Into " & TablaRelacion & "(IDTipoProducto, IDLinea, IDSubLinea, IDSubLinea2, IDMarca, IDPresentacion) Values(" & orow("IDTipoProducto") & ", " & orow("IDLinea") & ", " & orow("IDSubLinea") & ", " & orow("IDSubLinea2") & ", " & orow("IDMarca") & ", " & ID & " )" & vbCrLf
                    End If
            End Select

        Next

        If sql <> "" Then
            Procesados = CSistema.ExecuteNonQuery(sql)
        End If

    End Sub

    Private Sub InsertarRegistro(ByVal Nodo As TreeNode)

        Debug.Print(Nodo.FullPath)
        Dim Matris() As String = Nodo.FullPath.Split(tv.PathSeparator)
        Dim NodoActual As TreeNode = Nothing
        Dim oRow As DataRow = dtPath.NewRow

        For i As Integer = 0 To Matris.Count - 1

            Select Case i

                Case 0
                    NodoActual = GetNode(tv.Nodes, Matris(i))
                    oRow("IDTipoProducto") = NodoActual.Name
                Case 1
                    NodoActual = GetNode(NodoActual.Nodes, Matris(i))
                    oRow("IDLinea") = NodoActual.Name
                Case 2
                    NodoActual = GetNode(NodoActual.Nodes, Matris(i))
                    oRow("IDSubLinea") = NodoActual.Name
                Case 3
                    NodoActual = GetNode(NodoActual.Nodes, Matris(i))
                    oRow("IDSubLinea2") = NodoActual.Name
                Case 4
                    NodoActual = GetNode(NodoActual.Nodes, Matris(i))
                    oRow("IDMarca") = NodoActual.Name
                Case 5
                    NodoActual = GetNode(NodoActual.Nodes, Matris(i))
                    oRow("IDPresentacion") = NodoActual.Name
            End Select
        Next

        dtPath.Rows.Add(oRow)

    End Sub

    Sub SeleccionarRelacionados()

        Bloquear = True

        DeseleccionarTodo()

        Dim dt As DataTable = CData.GetTable(TablaRelacion, CampoID & " = " & ID)

        For Each orow As DataRow In dt.Rows

            For i As Integer = 0 To dt.Columns.Count - 1
                Select Case i
                    Case 0
                        If dt.Columns(i).ColumnName = "IDTipoProducto" Then
                            SeleccionarRegistro(orow("IDTipoProducto"))
                        End If
                    Case 1
                        If dt.Columns(i).ColumnName = "IDLinea" Then
                            SeleccionarRegistro(orow("IDTipoProducto"), orow("IDLinea"))
                        End If
                    Case 2
                        If dt.Columns(i).ColumnName = "IDSubLinea" Then
                            SeleccionarRegistro(orow("IDTipoProducto"), orow("IDLinea"), orow("IDSubLinea"))
                        End If
                    Case 3
                        If dt.Columns(i).ColumnName = "IDSubLinea2" Then
                            SeleccionarRegistro(orow("IDTipoProducto"), orow("IDLinea"), orow("IDSubLinea"), orow("IDSubLinea2"))
                        End If
                    Case 4
                        If dt.Columns(i).ColumnName = "IDMarca" Then
                            SeleccionarRegistro(orow("IDTipoProducto"), orow("IDLinea"), orow("IDSubLinea"), orow("IDSubLinea2"), orow("IDMarca"))
                        End If
                    Case 5
                        If dt.Columns(i).ColumnName = "IDPresentacion" Then
                            SeleccionarRegistro(orow("IDTipoProducto"), orow("IDLinea"), orow("IDSubLinea"), orow("IDSubLinea2"), orow("IDMarca"), orow("IDPresentacion"))
                        End If
                End Select
            Next
        Next

        Bloquear = False

    End Sub

    Sub QuitarSeleccionHaciaAbajo(ByVal nodo As TreeNode)

        If tv.CheckBoxes = False Then
            Exit Sub
        End If

        If nodo.Checked = True Then
            Exit Sub
        End If

        'RaiseEvent CheckedItem()

        Bloquear = True

        nodo.ForeColor = Color.DarkGray

        For Each nodo1 As TreeNode In nodo.Nodes
            nodo1.Checked = False
            If nodo1.Nodes.Count > 0 Then
                QuitarSeleccionHaciaAbajo(nodo1)
            End If
        Next

        Bloquear = False

    End Sub

    Sub Actualizar()

        Listar()
        SeleccionarRelacionados()

    End Sub

    Private Sub SeleccionarRegistro(ByVal IDTipoProducto As Integer, Optional ByVal IDLinea As Integer = 0, Optional ByVal IDSubLinea As Integer = 0, Optional ByVal IDSubLinea2 As Integer = 0, Optional ByVal IDMarca As Integer = 0, Optional ByVal IDPresentacion As Integer = 0)

        Try
            Bloquear = True

            Dim nTipoProducto As TreeNode = Nothing
            Dim nLinea As TreeNode = Nothing
            Dim nSubLinea As TreeNode = Nothing
            Dim nSubLinea2 As TreeNode = Nothing
            Dim nMarca As TreeNode = Nothing
            Dim nPresentacion As TreeNode = Nothing

            For Each nTipoProducto In tv.Nodes
                If nTipoProducto.Name = IDTipoProducto Then
                    If nTipoProducto.Checked = False Then
                        nTipoProducto.Checked = True
                        nTipoProducto.ForeColor = Color.Black
                        nTipoProducto.EnsureVisible()
                    End If
                    Exit For
                End If
            Next

            If IDLinea <> 0 Then
                For Each nLinea In nTipoProducto.Nodes
                    If nLinea.Name = IDLinea Then
                        If nLinea.Checked = False Then
                            nLinea.Checked = True
                            nLinea.ForeColor = Color.Black
                            nLinea.EnsureVisible()
                        End If

                        Exit For

                    End If
                Next
            End If

            If IDSubLinea <> 0 Then
                For Each nSubLinea In nLinea.Nodes
                    If nSubLinea.Name = IDSubLinea Then
                        If nSubLinea.Checked = False Then
                            nSubLinea.Checked = True
                            nSubLinea.ForeColor = Color.Black
                            nSubLinea.EnsureVisible()
                        End If

                        Exit For

                    End If
                Next
            End If

            If IDSubLinea2 <> 0 Then
                For Each nSubLinea2 In nSubLinea.Nodes
                    If nSubLinea2.Name = IDSubLinea2 Then
                        If nSubLinea2.Checked = False Then
                            nSubLinea2.Checked = True
                            nSubLinea2.ForeColor = Color.Black
                            nSubLinea2.EnsureVisible()
                        End If

                        Exit For

                    End If
                Next
            End If

            If IDMarca <> 0 Then
                For Each nMarca In nSubLinea2.Nodes
                    If nMarca.Name = IDMarca Then
                        If nMarca.Checked = False Then
                            nMarca.Checked = True
                            nMarca.ForeColor = Color.Black
                            nMarca.EnsureVisible()
                        End If

                        Exit For

                    End If
                Next
            End If

            If IDPresentacion <> 0 Then
                For Each nPresentacion In nMarca.Nodes
                    If nPresentacion.Name = IDPresentacion Then
                        If nPresentacion.Checked = False Then
                            nPresentacion.Checked = True
                            nPresentacion.ForeColor = Color.Black
                            nPresentacion.EnsureVisible()
                        End If

                        Exit For

                    End If
                Next
            End If

        Catch ex As Exception

        End Try

        Bloquear = False


    End Sub

    Private Sub SeleccionarNodo(ByVal ID As Integer, Optional ByVal nodos As TreeNodeCollection = Nothing)

        If nodos Is Nothing Then
            nodos = tv.Nodes
        End If

        For Each nodo As TreeNode In nodos

            If nodo.Name = ID And nodo.Level = Nivel Then
                nodo.Checked = True
                nodo.ForeColor = Color.Black
                nodo.EnsureVisible()
                Exit For
            Else
                If nodo.Nodes.Count > 0 Then
                    SeleccionarNodo(ID, nodo.Nodes)
                End If
            End If
        Next

    End Sub

    Public Sub DeseleccionarTodo(Optional ByVal nodos As TreeNodeCollection = Nothing)



        If nodos Is Nothing Then
            nodos = tv.Nodes
        End If

        For Each nodo As TreeNode In nodos

            Bloquear = True

            nodo.Checked = False
            nodo.ForeColor = Color.DarkGray
            nodo.Collapse()

            If nodo.Nodes.Count > 0 Then
                DeseleccionarTodo(nodo.Nodes)
            End If

            Bloquear = False

        Next



    End Sub

    Private Sub SeleccionarHaciaArriba(ByVal Nodo1 As TreeNode, ByVal Seleccionar As Boolean)

        If Bloquear = True Then
            Exit Sub
        End If

        'Si el nivel es 0, simpelemte establecemos la seleccion y salimos del sistema
        If Nodo1.Level = 0 Then
            If Nodo1.Checked <> Seleccionar Then
                Nodo1.Checked = Seleccionar
            End If

            If Seleccionar = True Then
                Nodo1.ForeColor = Color.Black
            Else
                Nodo1.ForeColor = Color.DarkGray
            End If

            Exit Sub
        End If


        'Si seleccion es True
        If Seleccionar = True Then

            Nodo1.ForeColor = Color.Black

            'Establecemos True al padre
            Nodo1.Parent.Checked = Seleccionar

            'Vamos a un nivel mas arriba
            'SeleccionarHaciaArriba(Nodo1.Parent, Seleccionar)

        Else

            Nodo1.ForeColor = Color.DarkGray

            'Si es false, verificar si hay nodos del nivel marcados
            For Each nodo As TreeNode In Nodo1.Parent.Nodes

                'Si se trata del mismo nodo que genero, vamos a siguiente.
                If nodo.Name = Nodo1.Name And nodo.Level = Nodo1.Level Then
                    GoTo siguiente
                End If

                'Si hay un nodo marcado True no hacer nada
                If nodo.Checked = True Then
                    Exit Sub
                End If
siguiente:

            Next

            'Vamos a un nivel mas arriba
            SeleccionarHaciaArriba(Nodo1.Parent, Seleccionar)

        End If


    End Sub

    Private Sub SeleccionarHaciaAbajo(ByVal valor As Boolean, Optional ByVal Nodo1 As TreeNode = Nothing)

        If Bloquear = True Then
            Exit Sub
        End If

        If Nodo1 Is Nothing Then
            Nodo1 = tv.SelectedNode
        End If

        Debug.Print(Nodo1.Text)

        'Nodo1.Checked = True

        For Each nodo2 As TreeNode In Nodo1.Nodes
            nodo2.Checked = valor

            If nodo2.Nodes.Count > 0 Then
                SeleccionarHaciaAbajo(valor, nodo2)
            End If
        Next

        Bloquear = False

    End Sub

    Private Function GetID(ByVal nodos As TreeNodeCollection, ByVal Descripcion As String) As Integer

        GetID = Nothing
        For Each nodo2 As TreeNode In nodos
            If nodo2.Text = Descripcion Then
                GetID = nodo2.Name
                Exit For
            End If
        Next

    End Function

    Private Function GetNode(ByVal nodos As TreeNodeCollection, ByVal Descripcion As String) As TreeNode
        GetNode = Nothing
        For Each nodo2 As TreeNode In nodos
            If nodo2.Text = Descripcion Then
                GetNode = nodo2
                Exit For
            End If
        Next

    End Function

    Sub Eliminar()

        'Verificar si el nodo seleccionado esta checkeado
        If tv.SelectedNode Is Nothing Then
            Exit Sub
        End If

        If tv.SelectedNode.Checked = False Then
            Exit Sub
        End If

        If tv.SelectedNode.Level <> Nivel Then
            Exit Sub
        End If

        'Variables
        Dim IDTipoProducto As Integer
        Dim IDLinea As Integer
        Dim IDSubLinea As Integer
        Dim IDSubLinea2 As Integer
        Dim IDMarca As Integer
        Dim IDPresentacion As Integer

        'Consulta de Eliminacion
        If MessageBox.Show("Este proceso eliminara toda las relaciones que estan configuradas hacia abajo! Debera volver a configurar los grupos hacia abajo, de lo contrario posiblemente no apareceran en los informes ni en la ficha de productos correctamente. Se sugiere que se realice solo si se esta seguro/a. Desea continuar?", "ATENCION", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = DialogResult.No Then
            Exit Sub
        End If

        Dim Nodo As TreeNode = tv.SelectedNode
        Dim CampoRelacion As String = ""

        Select Case Nivel + 1
            Case 0
                CampoRelacion = "IDTipoProducto"
            Case 1
                CampoRelacion = "IDLinea"
            Case 2
                CampoRelacion = "IDSubLinea"
            Case 3
                CampoRelacion = "IDSubLinea2"
            Case 4
                CampoRelacion = "IDMarca"
            Case 5
                CampoRelacion = "IDPresentacion"
        End Select

        If Nivel = 0 Then
            IDTipoProducto = GetParent(Nodo).Name
            IDLinea = ID
        End If

        If Nivel = 1 Then
            IDLinea = GetParent(Nodo).Name
            IDTipoProducto = GetParent(Nodo).Name
            IDSubLinea = ID
        End If

        If Nivel = 2 Then
            IDSubLinea = GetParent(Nodo).Name
            IDLinea = GetParent(Nodo).Name
            IDTipoProducto = GetParent(Nodo).Name
            IDSubLinea2 = ID
        End If

        If Nivel = 3 Then
            IDSubLinea2 = GetParent(Nodo).Name
            IDSubLinea = GetParent(Nodo).Name
            IDLinea = GetParent(Nodo).Name
            IDTipoProducto = GetParent(Nodo).Name
            IDMarca = ID
        End If

        If Nivel = 4 Then
            IDMarca = GetParent(Nodo).Name
            IDSubLinea2 = GetParent(Nodo).Name
            IDSubLinea = GetParent(Nodo).Name
            IDLinea = GetParent(Nodo).Name
            IDTipoProducto = GetParent(Nodo).Name
            IDPresentacion = ID
        End If

        If Nivel = 5 Then
            IDPresentacion = GetParent(Nodo).Name
            IDMarca = GetParent(Nodo).Name
            IDSubLinea2 = GetParent(Nodo).Name
            IDSubLinea = GetParent(Nodo).Name
            IDLinea = GetParent(Nodo).Name
            IDTipoProducto = GetParent(Nodo).Name
        End If

        Dim SQL As String = ""

        Select Case Nivel

            Case 0
                SQL = SQL & "Delete From " & TablaRelacionGuardar & "  Where IDLinea= " & IDLinea & " And IDTipoProducto=" & IDTipoProducto
            Case 1
                SQL = SQL & "Delete From " & TablaRelacionGuardar & " Where IDSubLinea= " & IDSubLinea & " And IDLinea= " & IDLinea & " And IDTipoProducto=" & IDTipoProducto
            Case 2
                SQL = SQL & "Delete From " & TablaRelacionGuardar & " Where IDSubLinea2= " & IDSubLinea2 & " And IDSubLinea= " & IDSubLinea & " And IDLinea= " & IDLinea & " And IDTipoProducto=" & IDTipoProducto
            Case 3
                SQL = SQL & "Delete From " & TablaRelacionGuardar & " Where IDMarca= " & IDMarca & " And IDSubLinea2= " & IDSubLinea2 & " And IDSubLinea= " & IDSubLinea & " And IDLinea= " & IDLinea & " And IDTipoProducto=" & IDTipoProducto
            Case 4
                SQL = SQL & "Delete From " & TablaRelacionGuardar & " Where IDPresentacion= " & IDPresentacion & " And IDMarca= " & IDMarca & " And IDSubLinea2= " & IDSubLinea2 & " And IDSubLinea= " & IDSubLinea & " And IDLinea= " & IDLinea & " And IDTipoProducto=" & IDTipoProducto
            Case 5
                SQL = SQL & "Delete From " & TablaRelacionGuardar & " Where IDPresentacion= " & IDPresentacion & " And IDMarca= " & IDMarca & " And IDSubLinea2= " & IDSubLinea2 & " And IDSubLinea= " & IDSubLinea & " And IDLinea= " & IDLinea & " And IDTipoProducto=" & IDTipoProducto
        End Select

        CSistema.ExecuteNonQuery(SQL)

        'Actualizar todo
        If Nivel < 1 Then
            CData.EliminarFiltro(CampoRelacion & " = " & ID, "VTipoProductoLinea")
            CData.InsertarFiltro(CampoRelacion & " = " & ID, "VTipoProductoLinea")
        End If

        If Nivel < 2 Then
            CData.EliminarFiltro(CampoRelacion & " = " & ID, "VLineaSubLinea")
            CData.InsertarFiltro(CampoRelacion & " = " & ID, "VLineaSubLinea")
        End If

        If Nivel < 3 Then
            CData.EliminarFiltro(CampoRelacion & " = " & ID, "VSubLineaSubLinea2")
            CData.InsertarFiltro(CampoRelacion & " = " & ID, "VSubLineaSubLinea2")
        End If

        If Nivel < 4 Then
            CData.EliminarFiltro(CampoRelacion & " = " & ID, "VSubLinea2Marca")
            CData.InsertarFiltro(CampoRelacion & " = " & ID, "VSubLinea2Marca")
        End If

        If Nivel < 5 Then
            CData.EliminarFiltro(CampoRelacion & " = " & ID, "VMarcaPresentacion")
            CData.InsertarFiltro(CampoRelacion & " = " & ID, "VMarcaPresentacion")
        End If

        Actualizar()

    End Sub

    Sub Modificar()

        'Verificar si el nodo seleccionado esta checkeado
        If tv.SelectedNode Is Nothing Then
            Exit Sub
        End If

        If tv.SelectedNode.Checked = False Then
            Exit Sub
        End If

        If tv.SelectedNode.Level <> Nivel Then
            Exit Sub
        End If

        Dim frm As New frmModificarClasificacion
        frm.Nivel = Nivel
        Dim Nodo As TreeNode = tv.SelectedNode

        If Nivel = 0 Then
            frm.IDTipoProducto = GetParent(Nodo).Name
            frm.IDLinea = ID
        End If

        If Nivel = 1 Then
            frm.IDLinea = GetParent(Nodo).Name
            frm.IDTipoProducto = GetParent(Nodo).Name
            frm.IDSubLinea = ID
        End If

        If Nivel = 2 Then
            frm.IDSubLinea = GetParent(Nodo).Name
            frm.IDLinea = GetParent(Nodo).Name
            frm.IDTipoProducto = GetParent(Nodo).Name
            frm.IDSubLinea2 = ID
        End If

        If Nivel = 3 Then
            frm.IDSubLinea2 = GetParent(Nodo).Name
            frm.IDSubLinea = GetParent(Nodo).Name
            frm.IDLinea = GetParent(Nodo).Name
            frm.IDTipoProducto = GetParent(Nodo).Name
            frm.IDMarca = ID
        End If

        If Nivel = 4 Then
            frm.IDMarca = GetParent(Nodo).Name
            frm.IDSubLinea2 = GetParent(Nodo).Name
            frm.IDSubLinea = GetParent(Nodo).Name
            frm.IDLinea = GetParent(Nodo).Name
            frm.IDTipoProducto = GetParent(Nodo).Name
            frm.IDPresentacion = ID
        End If

        If Nivel = 5 Then
            frm.IDPresentacion = GetParent(Nodo).Name
            frm.IDMarca = GetParent(Nodo).Name
            frm.IDSubLinea2 = GetParent(Nodo).Name
            frm.IDSubLinea = GetParent(Nodo).Name
            frm.IDLinea = GetParent(Nodo).Name
            frm.IDTipoProducto = GetParent(Nodo).Name
        End If

        frm.ID = ID
        frm.TablaRelacion = TablaRelacion
        frm.TablaRelacionGuardar = TablaRelacionGuardar

        FGMostrarFormulario(Me.ParentForm, frm, "Modificar clasificacion de Producto", FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        Actualizar()

    End Sub

    Sub Agregar()

        'Verificar si el nodo seleccionado esta checkeado
        If tv.SelectedNode Is Nothing Then
            Exit Sub
        End If

        If tv.SelectedNode.Level <> Nivel Then
            Exit Sub
        End If

        Dim frm As New frmAgregarClasificacion
        frm.Nivel = Nivel
        Dim Nodo As TreeNode = tv.SelectedNode

        If Nivel = 0 Then
            frm.IDTipoProducto = GetParent(Nodo).Name
            frm.IDLinea = ID
        End If

        If Nivel = 1 Then
            frm.IDLinea = GetParent(Nodo).Name
            frm.IDTipoProducto = GetParent(Nodo).Name
            frm.IDSubLinea = ID
        End If

        If Nivel = 2 Then
            frm.IDSubLinea = GetParent(Nodo).Name
            frm.IDLinea = GetParent(Nodo).Name
            frm.IDTipoProducto = GetParent(Nodo).Name
            frm.IDSubLinea2 = ID
        End If

        If Nivel = 3 Then
            frm.IDSubLinea2 = GetParent(Nodo).Name
            frm.IDSubLinea = GetParent(Nodo).Name
            frm.IDLinea = GetParent(Nodo).Name
            frm.IDTipoProducto = GetParent(Nodo).Name
            frm.IDMarca = ID
        End If

        If Nivel = 4 Then
            frm.IDMarca = GetParent(Nodo).Name
            frm.IDSubLinea2 = GetParent(Nodo).Name
            frm.IDSubLinea = GetParent(Nodo).Name
            frm.IDLinea = GetParent(Nodo).Name
            frm.IDTipoProducto = GetParent(Nodo).Name
            frm.IDPresentacion = ID
        End If

        If Nivel = 5 Then
            frm.IDPresentacion = GetParent(Nodo).Name
            frm.IDMarca = GetParent(Nodo).Name
            frm.IDSubLinea2 = GetParent(Nodo).Name
            frm.IDSubLinea = GetParent(Nodo).Name
            frm.IDLinea = GetParent(Nodo).Name
            frm.IDTipoProducto = GetParent(Nodo).Name
        End If

        frm.ID = ID
        frm.TablaRelacion = TablaRelacion
        frm.TablaRelacionGuardar = TablaRelacionGuardar

        FGMostrarFormulario(Me.ParentForm, frm, "Modificar clasificacion de Producto", FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        Actualizar()

    End Sub

    Function GetParent(ByRef nodo As TreeNode) As TreeNode

        GetParent = nodo

        If nodo.Level > 0 Then
            nodo = nodo.Parent
        End If

    End Function

    Public Sub MarcarNodo(ByVal ID As String, ByVal Nivel As Integer, ByVal Marcar As Boolean)

        If IsNumeric(ID) = False Then
            Exit Sub
        End If

        'Variables
        MarcarNodo(ID, tv.Nodes, Marcar, Nivel)


    End Sub

    Private Sub MarcarNodo(ByVal ID As Integer, ByVal nodo As TreeNodeCollection, ByVal marcar As Boolean, ByVal Nivel As Integer)

        For Each nodo1 As TreeNode In nodo

            If nodo1.Level = Nivel Then
                If ID = nodo1.Name Then

                    If Nivel > 0 Then
                        If nodo1.Parent.Checked = False Then
                            Exit For
                        End If
                    End If

                    If nodo1.Checked <> marcar Then
                        nodo1.Checked = marcar
                        nodo1.EnsureVisible()
                    End If
                    'Exit For
                End If
            Else
                MarcarNodo(ID, nodo1.Nodes, marcar, Nivel)
                'Exit For
            End If
        Next

    End Sub

    Private Sub ocxProductoClasificacion2_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub tv_AfterCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tv.AfterCheck
        'SeleccionarHaciaArriba(e.Node, e.Node.Checked)
        Try
            SeleccionarHaciaAbajo(e.Node.Checked, e.Node)
            RaiseEvent CheckedItem()

        Catch ex As Exception

        End Try
        'QuitarSeleccionHaciaAbajo(e.Node)
    End Sub

    Private Sub tv_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tv.AfterSelect

    End Sub

    Private Sub ModificarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiModificar.Click
        Modificar()
    End Sub

    Private Sub EliminarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiEliminar.Click
        Eliminar()
    End Sub

    Private Sub tv_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tv.NodeMouseClick

        If e.Button = Windows.Forms.MouseButtons.Right Then

            ' Referenciamos el control
            Dim tv As Windows.Forms.TreeView = DirectCast(sender, Windows.Forms.TreeView)

            ' Seleccionamos el nodo
            tv.SelectedNode = e.Node
        Else
            RaiseEvent CheckedItem()
        End If

    End Sub

    Private Sub ActualizarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiActualizar.Click
        Actualizar()
    End Sub

    Private Sub AgregarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiAgregar.Click
        Agregar()
    End Sub

    Private Sub ContextMenuStrip1_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ContextMenuStrip1.Opening

        If tv.SelectedNode Is Nothing Then
            tsmiActualizar.Enabled = True
            tsmiAgregar.Enabled = False
            tsmiEliminar.Enabled = False
            tsmiModificar.Enabled = False
            Exit Sub
        End If

        If tv.SelectedNode.Level <> Nivel Then
            tsmiActualizar.Enabled = True
            tsmiAgregar.Enabled = False
            tsmiEliminar.Enabled = False
            tsmiModificar.Enabled = False
            Exit Sub
        End If

        If tv.SelectedNode.Checked = True Then
            tsmiActualizar.Enabled = True
            tsmiAgregar.Enabled = False
            tsmiEliminar.Enabled = True
            tsmiModificar.Enabled = True
        Else
            tsmiActualizar.Enabled = True
            tsmiAgregar.Enabled = True
            tsmiEliminar.Enabled = False
            tsmiModificar.Enabled = False
        End If

    End Sub

    Private Sub ToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem2.Click
        SeleccionarHaciaAbajo(True)
        RaiseEvent CheckedItem()
    End Sub

End Class

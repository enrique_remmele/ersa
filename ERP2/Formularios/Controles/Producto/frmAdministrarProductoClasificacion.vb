﻿Public Class frmAdministrarProductoClasificacion

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private PadreValue As String
    Public Property Padre() As String
        Get
            Return PadreValue
        End Get
        Set(ByVal value As String)
            PadreValue = value
        End Set
    End Property

    Private CodigoPadreValue As String
    Public Property CodigoPadre() As String
        Get
            Return CodigoPadreValue
        End Get
        Set(ByVal value As String)
            CodigoPadreValue = value
        End Set
    End Property

    Private IDPadreValue As Integer
    Public Property IDPadre() As Integer
        Get
            Return IDPadreValue
        End Get
        Set(ByVal value As Integer)
            IDPadreValue = value
        End Set
    End Property

    Private PlanCuentaValue As String
    Public Property PlanCuenta() As String
        Get
            Return PlanCuentaValue
        End Get
        Set(ByVal value As String)
            PlanCuentaValue = value
        End Set
    End Property

    Private IDPlanCuentaValue As Integer
    Public Property IDPlanCuenta() As Integer
        Get
            Return IDPlanCuentaValue
        End Get
        Set(ByVal value As Integer)
            IDPlanCuentaValue = value
        End Set
    End Property

    Private NuevoValue As Boolean
    Public Property Nuevo() As Boolean
        Get
            Return NuevoValue
        End Get
        Set(ByVal value As Boolean)
            NuevoValue = value
        End Set
    End Property

    Private CategoriaValue As Integer
    Public Property Categoria() As Integer
        Get
            Return CategoriaValue
        End Get
        Set(ByVal value As Integer)
            CategoriaValue = value
        End Set
    End Property

    Private TipoValue As String
    Public Property Tipo() As String
        Get
            Return TipoValue
        End Get
        Set(ByVal value As String)
            TipoValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    'EVENTOS

    'VARIABLES
    Dim oRow As DataRow

    'FUNCIONES
    Sub Inicializar()

        'Form
        If Nuevo = True Then
            Me.Text = "Agregar una Cuenta"
        Else
            Me.Text = "Modificar la cuenta"
        End If

        'RadioButton

        'Funciones
        CargarInformacion()


    End Sub

    Sub CargarInformacion()

        'Detalle
        txtNivel.txt.Text = Categoria

        If Nuevo = True Then

            ID = CInt(CSistema.ExecuteScalar("Select ISNULL(MAX(ID)+1, 1) From CuentaContable"))
            txtID.Text = ID
            txtPadre.Text = Padre
            txtCodigo.txt.Text = CodigoPadre
            txtCodigo.txt.Focus()
            txtCodigo.txt.SelectionStart = txtCodigo.txt.Text.Length

        Else

            Try
                oRow = CSistema.ExecuteToDataTable("Select * From VCuentaContable Where ID=" & ID & " ").Rows(0)
                txtID.Text = ID.ToString
                txtPadre.Text = oRow("Padre").ToString

                txtCodigo.txt.Text = oRow("Codigo").ToString
                txtAlias.txt.Text = oRow("Alias").ToString
                txtDescripcion.txt.Text = oRow("Descripcion").ToString

                txtNivel.txt.Text = oRow("Categoria").ToString

                IDPadre = oRow("IDPadre").ToString
                IDPlanCuenta = oRow("IDPlanCuenta").ToString

            Catch ex As Exception
                Dim mensaje As String = ex.Message
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End Try


        End If


    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Codigo
        If txtCodigo.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Introduzca un codigo valido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Descripcion
        If txtDescripcion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Introduzca una descripcion valida!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro y sus dependientes. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@ID", txtID.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDPlanCuenta", IDPlanCuenta, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Codigo", txtCodigo.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Alias", txtAlias.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Categoria", txtNivel.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDPadre", IDPadre, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpCuentaContable", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            Procesado = False
            Exit Sub
        End If

        Procesado = True

        Me.Close()

    End Sub

    Private Sub frmAdministrarCuentaContable_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Dim Opcion As CSistema.NUMOperacionesABM
        If Nuevo = True Then
            Opcion = ERP.CSistema.NUMOperacionesABM.INS
        Else
            Opcion = ERP.CSistema.NUMOperacionesABM.UPD
        End If

        Guardar(Opcion)

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Procesado = False
        Me.Close()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Guardar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub


End Class
﻿Public Class frmExcepciones

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'EVENTOS

    'PROPIEDADES
    Private IDProductoValue As String
    Public Property IDProducto() As String
        Get
            Return IDProductoValue
        End Get
        Set(ByVal value As String)
            IDProductoValue = value
        End Set
    End Property

    Private IDListaPrecioValue As String
    Public Property IDListaPrecio() As String
        Get
            Return IDListaPrecioValue
        End Get
        Set(ByVal value As String)
            IDListaPrecioValue = value
        End Set
    End Property

    'Variables
    Dim dt As DataTable
    Dim oRowProducto As DataRow
    Dim oRowListaPrecio As DataRow
    Dim ctr() As Control
    Dim vNuevo As Boolean


    Sub Inicializar()

        'Controles
        Dim dtCliente As DataTable = CData.GetTable("VCliente", "IDListaPrecio=" & IDListaPrecio).Copy

        txtCliente.Conectar(dtCliente)
        txtDesde.PermitirNulo = True
        txtHasta.PermitirNulo = True

        'Funcioines
        CargarInformacion()

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, ctr)

        Listar()

    End Sub

    Sub CargarInformacion()

        'Contoles
        ReDim ctr(-1)
        CSistema.CargaControl(ctr, txtCliente)
        CSistema.CargaControl(ctr, txtDescuento)
        CSistema.CargaControl(ctr, cbxTipoDescuento)
        CSistema.CargaControl(ctr, txtDesde)
        CSistema.CargaControl(ctr, txtHasta)

        CSistema.SqlToComboBox(cbxTipoDescuento.cbx, CData.GetTable("VTipoDescuento"), "ID", "Codigo")

        'Registro de Producto
        oRowProducto = CData.GetTable("VProducto", " ID = " & IDProducto)(0)
        oRowListaPrecio = CData.GetTable("VListaPrecio", " ID = " & IDListaPrecio)(0)

        If oRowProducto Is Nothing Then
            Exit Sub
        End If

        If oRowListaPrecio Is Nothing Then
            Exit Sub
        End If

        txtIDProducto.SetValue(IDProducto)
        txtProducto.txt.Text = oRowProducto("Descripcion").ToString
        txtReferencia.txt.Text = oRowProducto("Ref").ToString

        txtIDListaPrecio.SetValue(IDListaPrecio)
        txtListaPrecio.txt.Text = oRowListaPrecio("Descripcion").ToString


    End Sub

    Sub Seleccionar()

        txtCliente.Clear()
        txtDescuento.SetValue(0)
        cbxTipoDescuento.SelectedValue(0)
        txtDesde.txt.Clear()
        txtHasta.txt.Clear()

        If lvExcepciones.SelectedItems.Count = 0 Then
            Exit Sub
        End If


        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, ctr)

        For Each item As ListViewItem In lvExcepciones.SelectedItems

            txtCliente.SetValue(item.Text)
            cbxTipoDescuento.cbx.Text = item.SubItems(2).Text
            txtDescuento.SetValue(item.SubItems(3).Text.Replace("%", "").Trim)
            txtDesde.SetValue(item.SubItems(4).Text)
            txtHasta.SetValue(item.SubItems(5).Text)

        Next



    End Sub

    Sub Listar()

        lvExcepciones.Items.Clear()

        dt = CSistema.ExecuteToDataTable("Select IDCliente, Cliente, Moneda, TipoDescuentoCodigo, Descuento, Porcentaje, FechaDesde, FechaHasta From VProductoListaPrecioExcepciones Where IDListaPrecio=" & IDListaPrecio & "  And IDProducto=" & IDProducto & " ").Copy

        For Each oRow As DataRow In dt.Rows
            Dim item As New ListViewItem(oRow("IDCliente").ToString)
            item.SubItems.Add(oRow("Cliente").ToString)
            item.SubItems.Add(oRow("TipoDescuentoCodigo").ToString)
            item.SubItems.Add(oRow("Porcentaje").ToString & "%")
            item.SubItems.Add(oRow("FechaDesde").ToString)
            item.SubItems.Add(oRow("FechaHasta").ToString)

            lvExcepciones.Items.Add(item)

        Next

    End Sub

    Sub Guardar(ByVal operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Seleccione de cliente
        If txtCliente.Seleccionado = False Then
            CSistema.MostrarError("Seleccione correctamente el cliente!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
            Exit Sub
        End If

        'Seleccione de Tipo de Descuento
        If cbxTipoDescuento.GetValue = 0 Then
            CSistema.MostrarError("Seleccione correctamente el tipo de descuento!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
            Exit Sub
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDProducto", IDProducto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDListaPrecio", IDListaPrecio, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.Registro("ID"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoDescuento", cbxTipoDescuento.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Porcentaje", CSistema.FormatoNumeroBaseDatos(txtDescuento.txt.Text, True), ParameterDirection.Input)

        If IsDate(txtDesde.txt.Text) = True Then
            CSistema.SetSQLParameter(param, "@Desde", CSistema.FormatoFechaDesdeHastaBaseDatos(txtDesde.GetValue, True, False), ParameterDirection.Input)
        End If

        If IsDate(txtHasta.txt.Text) = True Then
            CSistema.SetSQLParameter(param, "@Hasta", CSistema.FormatoFechaDesdeHastaBaseDatos(txtHasta.GetValue, True, False), ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Operacion", operacion.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpProductoListaPrecioExcepciones", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, ctr)
            ctrError.Clear()
            Listar()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub Nuevo()

        vNuevo = True

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, ctr)
        txtCliente.Focus()
        txtCliente.txtID.Focus()
        txtCliente.txtID.txt.SelectAll()
    End Sub

    Sub Editar()

        vNuevo = False
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, ctr)
        txtCliente.Focus()
        txtCliente.txtID.Focus()
        txtCliente.txtID.txt.SelectAll()

    End Sub

    Sub Cancelar()


        ctrError.Clear()
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, ctr)

    End Sub

    Sub Eliminar()

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, ctr)

        Guardar(ERP.CSistema.NUMOperacionesABM.DEL)

    End Sub

    Sub ComportamientoDescuento()

        txtDesde.Enabled = False
        txtHasta.Enabled = False
        txtDesde.txt.Clear()
        txtHasta.txt.Clear()

        If cbxTipoDescuento.GetValue = 0 Then
            Exit Sub
        End If

        Dim dt As DataTable = CData.GetTable("VTipoDescuento", " ID = " & cbxTipoDescuento.GetValue).Copy
        If dt Is Nothing Then
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        txtDesde.Enabled = CBool(oRow("RangoFecha"))
        txtHasta.Enabled = CBool(oRow("RangoFecha"))

    End Sub

    Private Sub lvExcepciones_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvExcepciones.SelectedIndexChanged
        Seleccionar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Editar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Guardar(ERP.CSistema.NUMOperacionesABM.INS)

        Else
            Guardar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

    Private Sub frmExcepciones_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmExcepciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado
        cbxTipoDescuento.cbx.Focus()
    End Sub

    Private Sub cbxTipoDescuento_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoDescuento.PropertyChanged
        ComportamientoDescuento()
    End Sub

    Private Sub txtDescuento_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDescuento.Leave

        Dim dt As DataTable = CData.GetTable("VTipoDescuento", " ID = " & cbxTipoDescuento.GetValue).Copy
        If dt Is Nothing Then
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        If CDec(txtDescuento.txt.Text) > CDec(oRow("Limite")) Then
            CSistema.MostrarError("El limite maximo para este tipo de descuento descuento es de " & oRow("Limite"), ctrError, txtDescuento, tsslEstado, ErrorIconAlignment.TopRight)
            txtDescuento.txt.SelectAll()
            txtDescuento.txt.Focus()
            txtDescuento.SetValue(oRow("Limite"))
        End If

    End Sub

    Private Sub txtDescuento_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDescuento.TeclaPrecionada

    End Sub
End Class
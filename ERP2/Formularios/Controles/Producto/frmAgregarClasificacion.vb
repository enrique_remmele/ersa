﻿Public Class frmAgregarClasificacion

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'EVENTOS

    'PROPIEDADES
    Private NivelValue As Integer
    Public Property Nivel() As Integer
        Get
            Return NivelValue
        End Get
        Set(ByVal value As Integer)
            NivelValue = value
        End Set
    End Property

    Private IDTipoProductoValue As Integer
    Public Property IDTipoProducto() As Integer
        Get
            Return IDTipoProductoValue
        End Get
        Set(ByVal value As Integer)
            IDTipoProductoValue = value
        End Set
    End Property

    Private IDLineaValue As Integer
    Public Property IDLinea() As Integer
        Get
            Return IDLineaValue
        End Get
        Set(ByVal value As Integer)
            IDLineaValue = value
        End Set
    End Property

    Private IDSubLineaValue As Integer
    Public Property IDSubLinea() As Integer
        Get
            Return IDSubLineaValue
        End Get
        Set(ByVal value As Integer)
            IDSubLineaValue = value
        End Set
    End Property

    Private IDSubLinea2Value As Integer
    Public Property IDSubLinea2() As Integer
        Get
            Return IDSubLinea2Value
        End Get
        Set(ByVal value As Integer)
            IDSubLinea2Value = value
        End Set
    End Property

    Private IDMarcaValue As Integer
    Public Property IDMarca() As Integer
        Get
            Return IDMarcaValue
        End Get
        Set(ByVal value As Integer)
            IDMarcaValue = value
        End Set
    End Property

    Private IDPresentacionValue As Integer
    Public Property IDPresentacion() As Integer
        Get
            Return IDPresentacionValue
        End Get
        Set(ByVal value As Integer)
            IDPresentacionValue = value
        End Set
    End Property

    Private TablaRelacionValue As String
    Public Property TablaRelacion() As String
        Get
            Return TablaRelacionValue
        End Get
        Set(ByVal value As String)
            TablaRelacionValue = value
        End Set
    End Property

    Private TablaRelacionGuardarValue As String
    Public Property TablaRelacionGuardar() As String
        Get
            Return TablaRelacionGuardarValue
        End Get
        Set(ByVal value As String)
            TablaRelacionGuardarValue = value
        End Set
    End Property

    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    'VARIABLES

    'FUNCIONES

    Sub Inicializar()

        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Tipos de Producto
        CSistema.SqlToComboBox(cbxTipoProducto.cbx, CData.GetTable("VTipoProducto").Copy, "ID", "Descripcion")

        If Nivel >= 0 Then
            cbxTipoProducto.Enabled = True
            cbxTipoProducto.SelectedValue(IDTipoProducto)
        End If

        If Nivel >= 1 Then
            cbxLinea.Enabled = True
            cbxLinea.SelectedValue(IDLinea)
        End If

        If Nivel >= 2 Then
            cbxSubLinea.Enabled = True
            cbxSubLinea.SelectedValue(IDSubLinea)
        End If

        If Nivel >= 3 Then
            cbxSubLinea2.Enabled = True
            cbxSubLinea2.SelectedValue(IDSubLinea2)
        End If

        If Nivel >= 4 Then
            cbxMarca.Enabled = True
            cbxMarca.SelectedValue(IDMarca)
        End If

        If Nivel >= 5 Then
            cbxPresentacion.Enabled = True
            cbxPresentacion.SelectedValue(IDPresentacion)
        End If

    End Sub

    Sub Guardar()

        Dim CampoRelacion As String = ""

        Select Case Nivel + 1
            Case 0
                CampoRelacion = "IDTipoProducto"
            Case 1
                CampoRelacion = "IDLinea"
            Case 2
                CampoRelacion = "IDSubLinea"
            Case 3
                CampoRelacion = "IDSubLinea2"
            Case 4
                CampoRelacion = "IDMarca"
            Case 5
                CampoRelacion = "IDPresentacion"
        End Select


        'Eliminar todas las relaciones primeramente
        Dim Procesados As Integer

        'Grabar
        Dim sql As String = ""

        Select Case Nivel

            Case 0
                'Verificar primero si existe
                If CBool(CSistema.ExecuteScalar("Select Case When (Select " & CampoRelacion & " From " & TablaRelacionGuardar & " Where IDLinea= " & ID & " And IDTipoProducto=" & IDTipoProducto & ") Is Null Then 'False' Else 'True' End")) = False Then
                    sql = sql & "Insert Into " & TablaRelacion & "(IDTipoProducto, IDLinea) Values(" & IDTipoProducto & ", " & ID & " )" & vbCrLf
                End If
            Case 1
                If CBool(CSistema.ExecuteScalar("Select Case When (Select " & CampoRelacion & " From " & TablaRelacionGuardar & " Where IDSubLinea=" & ID & " And IDTipoProducto=" & IDTipoProducto & " And IDLinea=" & IDLinea & ") Is Null Then 'False' Else 'True' End")) = False Then
                    sql = sql & "Insert Into " & TablaRelacion & "(IDTipoProducto, IDLinea, IDSubLinea) Values(" & IDTipoProducto & ", " & IDLinea & " , " & ID & " )" & vbCrLf
                End If
            Case 2
                If CBool(CSistema.ExecuteScalar("Select Case When (Select " & CampoRelacion & " From " & TablaRelacionGuardar & " Where IDSubLinea2= " & ID & " And IDTipoProducto=" & IDTipoProducto & " And IDLinea=" & IDLinea & " And IDSubLinea=" & IDSubLinea & ") Is Null Then 'False' Else 'True' End")) = False Then
                    sql = sql & "Insert Into " & TablaRelacion & "(IDTipoProducto, IDLinea, IDSubLinea, IDSubLinea2) Values(" & IDTipoProducto & ", " & IDLinea & ", " & IDSubLinea & ", " & ID & " )" & vbCrLf
                End If
            Case 3
                If CBool(CSistema.ExecuteScalar("Select Case When (Select " & CampoRelacion & " From " & TablaRelacionGuardar & " Where IDMarca= " & ID & " And IDTipoProducto=" & IDTipoProducto & " And IDLinea=" & IDLinea & " And IDSubLinea=" & IDSubLinea & " And IDSubLinea2=" & IDSubLinea2 & ") Is Null Then 'False' Else 'True' End")) = False Then
                    sql = sql & "Insert Into " & TablaRelacion & "(IDTipoProducto, IDLinea, IDSubLinea, IDSubLinea2, IDMarca) Values(" & IDTipoProducto & ", " & IDLinea & ", " & IDSubLinea & ", " & IDSubLinea2 & ", " & ID & ")" & vbCrLf
                End If
            Case 4
                If CBool(CSistema.ExecuteScalar("Select Case When (Select " & CampoRelacion & " From " & TablaRelacionGuardar & " Where IDPresentacion= " & ID & " And IDTipoProducto=" & IDTipoProducto & " And IDLinea=" & IDLinea & " And IDSubLinea=" & IDSubLinea & " And IDSubLinea2=" & IDSubLinea2 & " And IDMarca=" & IDMarca & ") Is Null Then 'False' Else 'True' End")) = False Then
                    sql = sql & "Insert Into " & TablaRelacion & "(IDTipoProducto, IDLinea, IDSubLinea, IDSubLinea2, IDMarca, IDPresentacion) Values(" & IDTipoProducto & ", " & IDLinea & ", " & IDSubLinea & ", " & IDSubLinea2 & ", " & IDMarca & ", " & ID & " )" & vbCrLf
                End If
        End Select

        If sql <> "" Then
            Procesados = CSistema.ExecuteNonQuery(sql)

            If Nivel < 1 Then
                CData.EliminarFiltro(CampoRelacion & " = " & ID, "VTipoProductoLinea")
                CData.InsertarFiltro(CampoRelacion & " = " & ID, "VTipoProductoLinea")
            End If

            If Nivel < 2 Then
                CData.EliminarFiltro(CampoRelacion & " = " & ID, "VLineaSubLinea")
                CData.InsertarFiltro(CampoRelacion & " = " & ID, "VLineaSubLinea")
            End If

            If Nivel < 3 Then
                CData.EliminarFiltro(CampoRelacion & " = " & ID, "VSubLineaSubLinea2")
                CData.InsertarFiltro(CampoRelacion & " = " & ID, "VSubLineaSubLinea2")
            End If

            If Nivel < 4 Then
                CData.EliminarFiltro(CampoRelacion & " = " & ID, "VSubLinea2Marca")
                CData.InsertarFiltro(CampoRelacion & " = " & ID, "VSubLinea2Marca")
            End If

            If Nivel < 5 Then
                CData.EliminarFiltro(CampoRelacion & " = " & ID, "VMarcaPresentacion")
                CData.InsertarFiltro(CampoRelacion & " = " & ID, "VMarcaPresentacion")
            End If

        End If

        Me.Close()


    End Sub

    Sub EstablecerPath()

        Dim Resultado As String = ""

        If Nivel >= 0 Then
            Resultado = Resultado & " \ " & cbxTipoProducto.cbx.Text
        Else
            Resultado = Resultado & "..."
        End If

        If Nivel >= 1 Then
            Resultado = Resultado & " \ " & cbxLinea.cbx.Text
        Else
            Resultado = Resultado & " \ ..."
        End If

        If Nivel >= 2 Then
            Resultado = Resultado & " \ " & cbxSubLinea.cbx.Text
        Else
            Resultado = Resultado & " \ ..."
        End If

        If Nivel >= 3 Then
            Resultado = Resultado & " \ " & cbxSubLinea2.cbx.Text
        Else
            Resultado = Resultado & " \ ..."
        End If

        If Nivel >= 4 Then
            Resultado = Resultado & " \ " & cbxMarca.cbx.Text
        Else
            Resultado = Resultado & " \ ..."
        End If

        If Nivel >= 5 Then
            Resultado = Resultado & " \ " & cbxPresentacion.cbx.Text
        Else
            Resultado = Resultado & " \ ..."
        End If

        txtPath.Text = Resultado

    End Sub

    Private Sub cbxTipoProducto_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoProducto.PropertyChanged

        'Listar Lineas
        If IsNumeric(cbxTipoProducto.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxLinea.cbx, CData.GetTable("VTipoProductoLinea", " IDTipoProducto=" & cbxTipoProducto.GetValue), "IDLinea", "Linea")
        EstablecerPath()
    End Sub

    Private Sub cbxLinea_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxLinea.PropertyChanged

        'Listar Lineas
        If IsNumeric(cbxTipoProducto.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxLinea.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxSubLinea.cbx, CData.GetTable("VLineaSubLinea", " IDTipoProducto=" & cbxTipoProducto.GetValue & " And IDLinea = " & cbxLinea.GetValue), "IDSubLinea", "SubLinea")
        EstablecerPath()

    End Sub

    Private Sub cbxSubLinea_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSubLinea.PropertyChanged


        'Listar Lineas
        If IsNumeric(cbxTipoProducto.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxLinea.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxSubLinea.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxSubLinea2.cbx, CData.GetTable("VSubLineaSubLinea2", " IDTipoProducto=" & cbxTipoProducto.GetValue & " And IDLinea = " & cbxLinea.GetValue & " And IDSubLinea = " & cbxSubLinea.GetValue), "IDSubLinea2", "SubLinea2")
        EstablecerPath()
    End Sub

    Private Sub cbxSubLinea2_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSubLinea2.PropertyChanged

        'Listar Lineas
        If IsNumeric(cbxTipoProducto.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxLinea.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxSubLinea.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxSubLinea2.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxMarca.cbx, CData.GetTable("VSubLinea2Marca", " IDTipoProducto=" & cbxTipoProducto.GetValue & " And IDLinea = " & cbxLinea.GetValue & " And IDSubLinea = " & cbxSubLinea.GetValue & " And IDSubLinea2 = " & cbxSubLinea2.GetValue), "IDMarca", "Marca")
        EstablecerPath()

    End Sub

    Private Sub cbxMarca_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxMarca.PropertyChanged



        'Listar Lineas
        If IsNumeric(cbxTipoProducto.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxLinea.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxSubLinea.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxSubLinea2.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If IsNumeric(cbxMarca.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        CSistema.SqlToComboBox(cbxPresentacion.cbx, CData.GetTable("VMarcaPresentacion", " IDTipoProducto=" & cbxTipoProducto.GetValue & " And IDLinea = " & cbxLinea.GetValue & " And IDSubLinea = " & cbxSubLinea.GetValue & " And IDSubLinea2 = " & cbxSubLinea2.GetValue & " And IDMarca = " & cbxMarca.GetValue), "IDPresentacion", "Presentacion")
        EstablecerPath()

    End Sub

    Private Sub frmModificarClasificacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

End Class
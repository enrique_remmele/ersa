﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFacturarPedidos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFacturarPedidos))
        Me.gbxGrupo = New System.Windows.Forms.GroupBox()
        Me.lvGrupo = New System.Windows.Forms.ListView()
        Me.ImageList101 = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolStrip2 = New System.Windows.Forms.ToolStrip()
        Me.lblListadoPedidos = New System.Windows.Forms.ToolStripLabel()
        Me.cbxGrupos = New System.Windows.Forms.ToolStripComboBox()
        Me.btnActualizarGrupo = New System.Windows.Forms.ToolStripButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.lvPedidos = New System.Windows.Forms.ListView()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.FacturarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.EliminarDeLaListaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AnularRegistroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtCantidadPedidos = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtFechaDesde = New ERP.ocxTXTDate()
        Me.txtFechaHasta = New ERP.ocxTXTDate()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel2 = New System.Windows.Forms.ToolStripLabel()
        Me.cbxOrden = New System.Windows.Forms.ToolStripComboBox()
        Me.btnActualizarPedidos = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnProcesar = New System.Windows.Forms.ToolStripButton()
        Me.btnSeleccionarTodos = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.BtnFactAsistida = New System.Windows.Forms.Button()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lbFacturacionAsistida = New System.Windows.Forms.Label()
        Me.TmFacturacionAsistida = New System.Windows.Forms.Timer(Me.components)
        Me.gbxGrupo.SuspendLayout()
        Me.ToolStrip2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxGrupo
        '
        Me.gbxGrupo.Controls.Add(Me.lvGrupo)
        Me.gbxGrupo.Controls.Add(Me.ToolStrip2)
        Me.gbxGrupo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxGrupo.Location = New System.Drawing.Point(3, 3)
        Me.gbxGrupo.Name = "gbxGrupo"
        Me.gbxGrupo.Size = New System.Drawing.Size(231, 459)
        Me.gbxGrupo.TabIndex = 0
        Me.gbxGrupo.TabStop = False
        Me.gbxGrupo.Text = "Grupos"
        '
        'lvGrupo
        '
        Me.lvGrupo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvGrupo.HideSelection = False
        Me.lvGrupo.Location = New System.Drawing.Point(3, 41)
        Me.lvGrupo.Name = "lvGrupo"
        Me.lvGrupo.Size = New System.Drawing.Size(225, 415)
        Me.lvGrupo.SmallImageList = Me.ImageList101
        Me.lvGrupo.TabIndex = 1
        Me.lvGrupo.UseCompatibleStateImageBehavior = False
        '
        'ImageList101
        '
        Me.ImageList101.ImageStream = CType(resources.GetObject("ImageList101.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList101.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList101.Images.SetKeyName(0, "ROJO.JPG")
        Me.ImageList101.Images.SetKeyName(1, "AMARILLO.JPG")
        Me.ImageList101.Images.SetKeyName(2, "VERDE.JPG")
        '
        'ToolStrip2
        '
        Me.ToolStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblListadoPedidos, Me.cbxGrupos, Me.btnActualizarGrupo})
        Me.ToolStrip2.Location = New System.Drawing.Point(3, 16)
        Me.ToolStrip2.Name = "ToolStrip2"
        Me.ToolStrip2.Size = New System.Drawing.Size(225, 25)
        Me.ToolStrip2.TabIndex = 0
        Me.ToolStrip2.TabStop = True
        Me.ToolStrip2.Text = "ToolStrip2"
        '
        'lblListadoPedidos
        '
        Me.lblListadoPedidos.Name = "lblListadoPedidos"
        Me.lblListadoPedidos.Size = New System.Drawing.Size(50, 22)
        Me.lblListadoPedidos.Text = "Lista de:"
        '
        'cbxGrupos
        '
        Me.cbxGrupos.Items.AddRange(New Object() {"TODOS", "ZONA", "VENDEDOR"})
        Me.cbxGrupos.Name = "cbxGrupos"
        Me.cbxGrupos.Size = New System.Drawing.Size(121, 25)
        '
        'btnActualizarGrupo
        '
        Me.btnActualizarGrupo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnActualizarGrupo.Image = CType(resources.GetObject("btnActualizarGrupo.Image"), System.Drawing.Image)
        Me.btnActualizarGrupo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnActualizarGrupo.Name = "btnActualizarGrupo"
        Me.btnActualizarGrupo.Size = New System.Drawing.Size(23, 22)
        Me.btnActualizarGrupo.Text = "ToolStripButton1"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Location = New System.Drawing.Point(240, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(982, 459)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Lista de Facturas"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lvPedidos, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 16)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(976, 440)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'lvPedidos
        '
        Me.lvPedidos.CheckBoxes = True
        Me.lvPedidos.ContextMenuStrip = Me.ContextMenuStrip1
        Me.lvPedidos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvPedidos.HideSelection = False
        Me.lvPedidos.Location = New System.Drawing.Point(3, 35)
        Me.lvPedidos.Name = "lvPedidos"
        Me.lvPedidos.Size = New System.Drawing.Size(970, 367)
        Me.lvPedidos.SmallImageList = Me.ImageList101
        Me.lvPedidos.TabIndex = 1
        Me.lvPedidos.UseCompatibleStateImageBehavior = False
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FacturarToolStripMenuItem, Me.ToolStripSeparator3, Me.EliminarDeLaListaToolStripMenuItem, Me.AnularRegistroToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(173, 76)
        '
        'FacturarToolStripMenuItem
        '
        Me.FacturarToolStripMenuItem.Name = "FacturarToolStripMenuItem"
        Me.FacturarToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.FacturarToolStripMenuItem.Text = "Facturar"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(169, 6)
        '
        'EliminarDeLaListaToolStripMenuItem
        '
        Me.EliminarDeLaListaToolStripMenuItem.Image = CType(resources.GetObject("EliminarDeLaListaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.EliminarDeLaListaToolStripMenuItem.Name = "EliminarDeLaListaToolStripMenuItem"
        Me.EliminarDeLaListaToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.EliminarDeLaListaToolStripMenuItem.Text = "Eliminar de la Lista"
        '
        'AnularRegistroToolStripMenuItem
        '
        Me.AnularRegistroToolStripMenuItem.Name = "AnularRegistroToolStripMenuItem"
        Me.AnularRegistroToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.AnularRegistroToolStripMenuItem.Text = "Anular Registro"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel2, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1, 1, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 408)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(970, 29)
        Me.TableLayoutPanel2.TabIndex = 4
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(485, 29)
        Me.FlowLayoutPanel2.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtCantidadPedidos)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(485, 0)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(485, 29)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'txtCantidadPedidos
        '
        Me.txtCantidadPedidos.Color = System.Drawing.Color.Empty
        Me.txtCantidadPedidos.Decimales = True
        Me.txtCantidadPedidos.Indicaciones = Nothing
        Me.txtCantidadPedidos.Location = New System.Drawing.Point(415, 4)
        Me.txtCantidadPedidos.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCantidadPedidos.Name = "txtCantidadPedidos"
        Me.txtCantidadPedidos.Size = New System.Drawing.Size(66, 22)
        Me.txtCantidadPedidos.SoloLectura = True
        Me.txtCantidadPedidos.TabIndex = 1
        Me.txtCantidadPedidos.TabStop = False
        Me.txtCantidadPedidos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadPedidos.Texto = "0"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(345, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Cantidad:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.Label2)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtFechaDesde)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtFechaHasta)
        Me.FlowLayoutPanel4.Controls.Add(Me.ToolStrip1)
        Me.FlowLayoutPanel4.Controls.Add(Me.BtnFactAsistida)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(970, 26)
        Me.FlowLayoutPanel4.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 23)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Fecha:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFechaDesde
        '
        Me.txtFechaDesde.AñoFecha = 0
        Me.txtFechaDesde.Color = System.Drawing.Color.Empty
        Me.txtFechaDesde.Fecha = New Date(CType(0, Long))
        Me.txtFechaDesde.Location = New System.Drawing.Point(54, 4)
        Me.txtFechaDesde.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFechaDesde.MesFecha = 0
        Me.txtFechaDesde.Name = "txtFechaDesde"
        Me.txtFechaDesde.PermitirNulo = False
        Me.txtFechaDesde.Size = New System.Drawing.Size(65, 20)
        Me.txtFechaDesde.SoloLectura = False
        Me.txtFechaDesde.TabIndex = 1
        '
        'txtFechaHasta
        '
        Me.txtFechaHasta.AñoFecha = 0
        Me.txtFechaHasta.Color = System.Drawing.Color.Empty
        Me.txtFechaHasta.Fecha = New Date(CType(0, Long))
        Me.txtFechaHasta.Location = New System.Drawing.Point(127, 4)
        Me.txtFechaHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFechaHasta.MesFecha = 0
        Me.txtFechaHasta.Name = "txtFechaHasta"
        Me.txtFechaHasta.PermitirNulo = False
        Me.txtFechaHasta.Size = New System.Drawing.Size(65, 20)
        Me.txtFechaHasta.SoloLectura = False
        Me.txtFechaHasta.TabIndex = 2
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel2, Me.cbxOrden, Me.btnActualizarPedidos, Me.ToolStripSeparator1, Me.ToolStripSeparator2, Me.btnProcesar, Me.btnSeleccionarTodos, Me.ToolStripButton1})
        Me.ToolStrip1.Location = New System.Drawing.Point(196, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(554, 25)
        Me.ToolStrip1.TabIndex = 3
        Me.ToolStrip1.TabStop = True
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripLabel2
        '
        Me.ToolStripLabel2.Name = "ToolStripLabel2"
        Me.ToolStripLabel2.Size = New System.Drawing.Size(84, 22)
        Me.ToolStripLabel2.Text = "Ordenado por:"
        '
        'cbxOrden
        '
        Me.cbxOrden.Items.AddRange(New Object() {"CLIENTE", "ZONA", "VENDEDOR", "NUMERO", "FECHA DE CARGA"})
        Me.cbxOrden.Name = "cbxOrden"
        Me.cbxOrden.Size = New System.Drawing.Size(121, 25)
        '
        'btnActualizarPedidos
        '
        Me.btnActualizarPedidos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnActualizarPedidos.Image = CType(resources.GetObject("btnActualizarPedidos.Image"), System.Drawing.Image)
        Me.btnActualizarPedidos.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnActualizarPedidos.Name = "btnActualizarPedidos"
        Me.btnActualizarPedidos.Size = New System.Drawing.Size(23, 22)
        Me.btnActualizarPedidos.Text = "ToolStripButton1"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'btnProcesar
        '
        Me.btnProcesar.Image = CType(resources.GetObject("btnProcesar.Image"), System.Drawing.Image)
        Me.btnProcesar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnProcesar.Name = "btnProcesar"
        Me.btnProcesar.Size = New System.Drawing.Size(72, 22)
        Me.btnProcesar.Text = "Procesar"
        '
        'btnSeleccionarTodos
        '
        Me.btnSeleccionarTodos.Image = CType(resources.GetObject("btnSeleccionarTodos.Image"), System.Drawing.Image)
        Me.btnSeleccionarTodos.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSeleccionarTodos.Name = "btnSeleccionarTodos"
        Me.btnSeleccionarTodos.Size = New System.Drawing.Size(115, 22)
        Me.btnSeleccionarTodos.Text = "Seleccionar todo"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(113, 22)
        Me.ToolStripButton1.Text = "Quitar Seleccion"
        '
        'BtnFactAsistida
        '
        Me.BtnFactAsistida.AutoSize = True
        Me.BtnFactAsistida.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.BtnFactAsistida.Dock = System.Windows.Forms.DockStyle.Left
        Me.BtnFactAsistida.Location = New System.Drawing.Point(753, 3)
        Me.BtnFactAsistida.Name = "BtnFactAsistida"
        Me.BtnFactAsistida.Size = New System.Drawing.Size(148, 23)
        Me.BtnFactAsistida.TabIndex = 4
        Me.BtnFactAsistida.Text = "&Facturacion Asistida"
        Me.BtnFactAsistida.UseVisualStyleBackColor = False
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 237.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.FlowLayoutPanel3, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.gbxGrupo, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.GroupBox2, 1, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(1225, 498)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'FlowLayoutPanel3
        '
        Me.TableLayoutPanel3.SetColumnSpan(Me.FlowLayoutPanel3, 2)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnSalir)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblFecha)
        Me.FlowLayoutPanel3.Controls.Add(Me.lbFacturacionAsistida)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(0, 465)
        Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(1225, 33)
        Me.FlowLayoutPanel3.TabIndex = 2
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(1147, 3)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'lblFecha
        '
        Me.lblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(349, 0)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(792, 23)
        Me.lblFecha.TabIndex = 0
        Me.lblFecha.Text = "Label2"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbFacturacionAsistida
        '
        Me.lbFacturacionAsistida.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbFacturacionAsistida.Location = New System.Drawing.Point(55, 0)
        Me.lbFacturacionAsistida.Name = "lbFacturacionAsistida"
        Me.lbFacturacionAsistida.Size = New System.Drawing.Size(288, 23)
        Me.lbFacturacionAsistida.TabIndex = 2
        Me.lbFacturacionAsistida.Text = "Temporizados apagado"
        Me.lbFacturacionAsistida.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TmFacturacionAsistida
        '
        Me.TmFacturacionAsistida.Interval = 600000
        '
        'frmFacturarPedidos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1225, 498)
        Me.Controls.Add(Me.TableLayoutPanel3)
        Me.Name = "frmFacturarPedidos"
        Me.Tag = "Facturacion Asistida"
        Me.Text = "frmFacturarPedidos"
        Me.gbxGrupo.ResumeLayout(False)
        Me.gbxGrupo.PerformLayout()
        Me.ToolStrip2.ResumeLayout(False)
        Me.ToolStrip2.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel4.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxGrupo As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStrip2 As System.Windows.Forms.ToolStrip
    Friend WithEvents lblListadoPedidos As System.Windows.Forms.ToolStripLabel
    Friend WithEvents cbxGrupos As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents btnActualizarGrupo As System.Windows.Forms.ToolStripButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lvGrupo As System.Windows.Forms.ListView
    Friend WithEvents lvPedidos As System.Windows.Forms.ListView
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripLabel2 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents cbxOrden As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents btnActualizarPedidos As System.Windows.Forms.ToolStripButton
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtCantidadPedidos As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnSeleccionarTodos As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnProcesar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ImageList101 As System.Windows.Forms.ImageList
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents FacturarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtFechaDesde As ERP.ocxTXTDate
    Friend WithEvents txtFechaHasta As ERP.ocxTXTDate
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents EliminarDeLaListaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AnularRegistroToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BtnFactAsistida As Button
    Public WithEvents TmFacturacionAsistida As Timer
    Public WithEvents lbFacturacionAsistida As Label
End Class

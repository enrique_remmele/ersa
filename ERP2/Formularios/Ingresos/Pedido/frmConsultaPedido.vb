﻿Public Class frmConsultaPedido

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'Form

        'TextBox
        txtCantidadDetalle.txt.ResetText()
        txtCantidadOperacion.txt.ResetText()
        txtCliente.ResetText()
        txtCliente.Enabled = False
        txtTotalDetalle.txt.ResetText()
        txtTotalOperacion.txt.ResetText()


        'CheckBox
        chkComprobante.Checked = False
        chkCondicion.Checked = False
        chkVendedor.Checked = False
        chkSucursal.Checked = False

        'ComboBox
        'cbxComprobante.Enabled = False
        cbxCondicion.Enabled = False
        cbxVendedor.Enabled = False
        cbxSucursal.Enabled = False

        'RadioButton

        'ListView
        lvDetalle.Items.Clear()
        lvOperacion.Items.Clear()

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        CargarInformacion()

        'Foco

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'Condicion
        cbxCondicion.Items.Add("CONT")
        cbxCondicion.Items.Add("CRED")
        cbxCondicion.DropDownStyle = ComboBoxStyle.DropDownList

        'Tipo de Comprobante
        ' CSistema.SqlToComboBox(cbxComprobante, "Select ID, Codigo From TipoComprobante Where IDOperacion=1")

        'Vendedor
        CSistema.SqlToComboBox(cbxVendedor, "Select ID, Nombres From Vendedor Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

        'Condicion
        chkCondicion.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CONDICION ACTIVO", "False")
        cbxCondicion.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "CONDICION", "")

        'Comprobante
        chkComprobante.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "COMPROBANTE ACTIVO", "False")
        'cbxComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "COMPROBANTE", "")
        txtCliente.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "NROCOMPROBANTE", "")

        'Vendedor
        chkVendedor.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "USUARIO ACTIVO", "False")
        cbxVendedor.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "USUARIO", "")


    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCRUSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Condicion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CONDICION ACTIVO", chkCondicion.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "CONDICION", cbxCondicion.Text)

        'Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "COMPROBANTE ACTIVO", chkComprobante.Checked.ToString)
        'CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "COMPROBANTE", cbxComprobante.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "NROCOMPROBANTE", txtCliente.Text)

        'Usuario
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "USUARIO ACTIVO", chkVendedor.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "USUARIO", cbxVendedor.Text)

    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Establecer Condicion
    Function EstablecerCondicion(ByVal txt As TextBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If Where = "" Then
                Where = " Where (" & campo & " Like '%" & txt.Text & "%') "
            Else
                Where = Where & " And (" & campo & " Like '%" & txt.Text & "%') "
            End If

        End If


    End Function

    'Listar Operaciones
    Sub ListarOperaciones(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "")

        'limpiar txt del detalle
        txtCantidadDetalle.txt.Clear()
        txtTotalDetalle.txt.Clear()


        ctrError.Clear()

        Consulta = "Select IDSucursal,'Suc'=Sucursal, 'Fec. Comp.'=Fec, Cliente, Total, IDVendedor, Vendedor, Deposito, Observacion, IDTransaccion From VPedido "

        Where = Condicion

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la Sucursal!") = False Then
            Exit Sub
        End If

        'Condicion
        If chkCondicion.Checked = True Then
            If Where = "" Then
                Where = " Where (Condicion='" & cbxCondicion.Text & "') "
            Else
                Where = Where & " And (Condicion='" & cbxCondicion.Text & "') "
            End If
        End If

        If EstablecerCondicion(txtCliente, chkComprobante, "Cliente", "Cliente no valido!") = False Then
            Exit Sub
        End If

        'Vendedor
        If EstablecerCondicion(cbxVendedor, chkVendedor, "IDVendedor", "Seleccione correctamente el usuario!") = False Then
            Exit Sub
        End If

        lvOperacion.Items.Clear()
        lvDetalle.Items.Clear()

        'Solo por numero
        If Numero > 0 Then
            Where = " Where Numero = " & Numero & ""
        End If

        CSistema.SqlToLv(lvOperacion, Consulta & " " & Where & " Order By Cliente")

        'Formato
        lvOperacion.Columns(9).Width = 0

        'Totales
        CSistema.FormatoMoneda(lvOperacion, 4)
        'CSistema.FormatoMoneda(lvOperacion, 8)
        CSistema.TotalesLv(lvOperacion, txtTotalOperacion.txt, 4)
        txtCantidadOperacion.txt.Text = lvOperacion.Items.Count


    End Sub

    'Listar Detalle
    Sub ListarDetalle()

        ctrError.Clear()

        'Validar
        If lvOperacion.SelectedItems.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(lvOperacion.SelectedItems(0).SubItems(9).Text) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Limpiar ListView
        lvDetalle.Items.Clear()

        'Obtener el IDTransaccion
        IDTransaccion = CInt(lvOperacion.SelectedItems(0).SubItems(9).Text)
        Dim Consulta As String = "Select 'ID'=IDProducto, Producto, Cantidad, 'Pre. Uni.'=PrecioUnitario, Total From VDetallePedido Where IDTransaccion=" & IDTransaccion & " Order By ID"
        CSistema.SqlToLv(lvDetalle, Consulta)
        CSistema.FormatoMoneda(lvDetalle, 2)
        CSistema.FormatoMoneda(lvDetalle, 3)
        CSistema.FormatoMoneda(lvDetalle, 4)

        txtCantidadDetalle.txt.Text = lvDetalle.Items.Count
        CSistema.TotalesLv(lvDetalle, txtTotalDetalle.txt, 4)


    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        If lvOperacion.SelectedItems.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(lvOperacion.SelectedItems(0).SubItems(9).Text) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = CInt(lvOperacion.SelectedItems(0).SubItems(9).Text)

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

  Private Sub btnRegistrosDelDia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosDelDia.Click
        Where = " Where (DateDiff(dd, Fecha, GetDate())=0) "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub frmConsultaCompra_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaCompra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub lvOperacion_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvOperacion.DoubleClick
        SeleccionarRegistro()
    End Sub

    Private Sub lvOperacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvOperacion.SelectedIndexChanged
        ListarDetalle()
    End Sub

    Private Sub chkComprobante_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkComprobante.CheckedChanged
        If chkComprobante.Checked = True Then
            'cbxComprobante.Enabled = True
            txtCliente.Enabled = True
        Else
            'cbxComprobante.Enabled = False
            txtCliente.Enabled = False
        End If

    End Sub

    Private Sub chkUsuario_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkVendedor.CheckedChanged
        HabilitarControles(chkVendedor, cbxVendedor)
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub

    Private Sub btnRegistrosDelMes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosDelMes.Click
        Where = " Where (DateDiff(MM, Fecha, GetDate())=0) "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub btnRegistrosGenerales_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosGenerales.Click
        ListarOperaciones()
    End Sub

    Private Sub dtbRegistrosPorFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtbRegistrosPorFecha.Click
        Where = " Where (Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "') "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub lvOperacion_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvOperacion.KeyUp
        If e.KeyCode = Keys.Enter Then
            SeleccionarRegistro()
        End If

    End Sub

    Private Sub frmConsultaMovimiento_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub chkCondicion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCondicion.CheckedChanged
        HabilitarControles(chkCondicion, cbxCondicion)
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarOperaciones(txtOperacion.ObtenerValor)
            txtOperacion.txt.Focus()
            txtOperacion.txt.SelectAll()
        End If
    End Sub

    Private Sub dtpDesde_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpDesde.ValueChanged
        dtpHasta.Value = dtpDesde.Text
    End Sub

    '10-06-2021 - SC - Actualiza datos
    Sub frmConsultaPedido_Activate()
        Me.Refresh()
    End Sub
End Class


﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPedidoNUEVO
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblProducto = New System.Windows.Forms.Label()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.tsmiInformacion = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsmiEliminar = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiDescuento = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lblObservacionProducto = New System.Windows.Forms.Label()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.cbxTipoEntrega = New ERP.ocxCBX()
        Me.lblTipoEntrega = New System.Windows.Forms.Label()
        Me.txtHoraHasta = New System.Windows.Forms.DateTimePicker()
        Me.txtHoraDesde = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.chkRestriccionHoraEntrega = New ERP.ocxCHK()
        Me.lblVerInventario = New System.Windows.Forms.LinkLabel()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.cbxCondicion = New System.Windows.Forms.ComboBox()
        Me.OcxCotizacion1 = New ERP.ocxCotizacion()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtVenta = New ERP.ocxTXTString()
        Me.txtEstado = New ERP.ocxTXTString()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.txtTelefono = New ERP.ocxTXTString()
        Me.txtDetalle = New ERP.ocxTXTString()
        Me.lblDetalle = New System.Windows.Forms.Label()
        Me.txtCreditoSaldo = New ERP.ocxTXTNumeric()
        Me.cbxListaPrecio = New ERP.ocxCBX()
        Me.lblListaPrecio = New System.Windows.Forms.Label()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.lblDeposito = New System.Windows.Forms.Label()
        Me.txtFechaFacturar = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.txtDireccion = New ERP.ocxTXTString()
        Me.lblDireccion = New System.Windows.Forms.Label()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.lblTelefono = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.lblCreditoSaldo = New System.Windows.Forms.Label()
        Me.lblVenta = New System.Windows.Forms.Label()
        Me.lblVendedor = New System.Windows.Forms.Label()
        Me.cbxFormaPagoFactura = New ERP.ocxCBX()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblPlazo = New System.Windows.Forms.Label()
        Me.txtPlazo = New ERP.ocxTXTNumeric()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmiAgregar = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblDescuentoProducto = New System.Windows.Forms.Label()
        Me.gbxDetalle = New System.Windows.Forms.GroupBox()
        Me.txtDescuento = New ERP.ocxTXTNumeric()
        Me.txtObservacionProducto = New ERP.ocxTXTString()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.txtExistencia = New ERP.ocxTXTNumeric()
        Me.lblExistencia = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.OcxTXTNumeric1 = New ERP.ocxTXTNumeric()
        Me.txtPrecioUnitario = New ERP.ocxTXTNumeric()
        Me.lblPrecioUnitario = New System.Windows.Forms.Label()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.colID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColReferencia = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colDescripcion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colIVA = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colCantidad = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colPrecioUnitario = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colDescuentoUnitario = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colDescuentoPocentaje = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colExento = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colGravado = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ContextMenuStrip2 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AgregarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MidiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnClonar = New System.Windows.Forms.Button()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.flpReprocesado = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnPedidoOriginal = New System.Windows.Forms.Button()
        Me.lblFechaReproceso = New System.Windows.Forms.Label()
        Me.flpModificadoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblModificadoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioModificado = New System.Windows.Forms.Label()
        Me.lblFechaModificado = New System.Windows.Forms.Label()
        Me.btnPedidoNotaCredito = New System.Windows.Forms.Button()
        Me.chkEntregaCliente = New ERP.ocxCHK()
        Me.OcxImpuesto1 = New ERP.ocxImpuesto()
        Me.txtTotal = New ERP.ocxTXTNumeric()
        Me.txtTotalDescuento = New ERP.ocxTXTNumeric()
        Me.txtPorcentajeDescuento = New ERP.ocxTXTNumeric()
        Me.txtFecha = New ERP.ocxTXTDate()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.gbxCabecera.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.gbxDetalle.SuspendLayout()
        Me.ContextMenuStrip2.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.flpReprocesado.SuspendLayout()
        Me.flpModificadoPor.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblProducto
        '
        Me.lblProducto.AutoSize = True
        Me.lblProducto.Location = New System.Drawing.Point(6, 10)
        Me.lblProducto.Name = "lblProducto"
        Me.lblProducto.Size = New System.Drawing.Size(53, 13)
        Me.lblProducto.TabIndex = 0
        Me.lblProducto.Text = "Producto:"
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(222, 505)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 11
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 532)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(846, 22)
        Me.StatusStrip1.TabIndex = 16
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(575, 505)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 14
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(636, 10)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(35, 13)
        Me.lblCantidad.TabIndex = 12
        Me.lblCantidad.Text = "Cant.:"
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(494, 505)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 13
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(413, 505)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 12
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'tsmiInformacion
        '
        Me.tsmiInformacion.Name = "tsmiInformacion"
        Me.tsmiInformacion.Size = New System.Drawing.Size(148, 22)
        Me.tsmiInformacion.Text = "Informacion"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(145, 6)
        '
        'tsmiEliminar
        '
        Me.tsmiEliminar.Name = "tsmiEliminar"
        Me.tsmiEliminar.ShortcutKeys = System.Windows.Forms.Keys.Delete
        Me.tsmiEliminar.Size = New System.Drawing.Size(148, 22)
        Me.tsmiEliminar.Text = "Eliminar"
        '
        'tsmiDescuento
        '
        Me.tsmiDescuento.Name = "tsmiDescuento"
        Me.tsmiDescuento.Size = New System.Drawing.Size(148, 22)
        Me.tsmiDescuento.Text = "Descuentos"
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(90, 505)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(126, 23)
        Me.btnBusquedaAvanzada.TabIndex = 10
        Me.btnBusquedaAvanzada.Text = "&Busqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(750, 505)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 15
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'lblObservacionProducto
        '
        Me.lblObservacionProducto.AutoSize = True
        Me.lblObservacionProducto.Location = New System.Drawing.Point(384, 10)
        Me.lblObservacionProducto.Name = "lblObservacionProducto"
        Me.lblObservacionProducto.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacionProducto.TabIndex = 2
        Me.lblObservacionProducto.Text = "Observacion:"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(779, 10)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(45, 13)
        Me.lblImporte.TabIndex = 16
        Me.lblImporte.Text = "Importe:"
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.cbxTipoEntrega)
        Me.gbxCabecera.Controls.Add(Me.lblTipoEntrega)
        Me.gbxCabecera.Controls.Add(Me.txtHoraHasta)
        Me.gbxCabecera.Controls.Add(Me.txtHoraDesde)
        Me.gbxCabecera.Controls.Add(Me.Label9)
        Me.gbxCabecera.Controls.Add(Me.Label8)
        Me.gbxCabecera.Controls.Add(Me.Label7)
        Me.gbxCabecera.Controls.Add(Me.chkRestriccionHoraEntrega)
        Me.gbxCabecera.Controls.Add(Me.lblVerInventario)
        Me.gbxCabecera.Controls.Add(Me.txtCliente)
        Me.gbxCabecera.Controls.Add(Me.cbxCondicion)
        Me.gbxCabecera.Controls.Add(Me.OcxCotizacion1)
        Me.gbxCabecera.Controls.Add(Me.Label1)
        Me.gbxCabecera.Controls.Add(Me.txtVenta)
        Me.gbxCabecera.Controls.Add(Me.txtEstado)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtTelefono)
        Me.gbxCabecera.Controls.Add(Me.txtDetalle)
        Me.gbxCabecera.Controls.Add(Me.lblDetalle)
        Me.gbxCabecera.Controls.Add(Me.txtCreditoSaldo)
        Me.gbxCabecera.Controls.Add(Me.cbxListaPrecio)
        Me.gbxCabecera.Controls.Add(Me.lblListaPrecio)
        Me.gbxCabecera.Controls.Add(Me.cbxVendedor)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.cbxDeposito)
        Me.gbxCabecera.Controls.Add(Me.lblDeposito)
        Me.gbxCabecera.Controls.Add(Me.txtFechaFacturar)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.txtDireccion)
        Me.gbxCabecera.Controls.Add(Me.lblDireccion)
        Me.gbxCabecera.Controls.Add(Me.lblCliente)
        Me.gbxCabecera.Controls.Add(Me.lblTelefono)
        Me.gbxCabecera.Controls.Add(Me.lblEstado)
        Me.gbxCabecera.Controls.Add(Me.lblCreditoSaldo)
        Me.gbxCabecera.Controls.Add(Me.lblVenta)
        Me.gbxCabecera.Controls.Add(Me.lblVendedor)
        Me.gbxCabecera.Controls.Add(Me.cbxFormaPagoFactura)
        Me.gbxCabecera.Controls.Add(Me.Label5)
        Me.gbxCabecera.Controls.Add(Me.lblPlazo)
        Me.gbxCabecera.Controls.Add(Me.txtPlazo)
        Me.gbxCabecera.Location = New System.Drawing.Point(4, 0)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(827, 194)
        Me.gbxCabecera.TabIndex = 0
        Me.gbxCabecera.TabStop = False
        '
        'cbxTipoEntrega
        '
        Me.cbxTipoEntrega.CampoWhere = Nothing
        Me.cbxTipoEntrega.CargarUnaSolaVez = False
        Me.cbxTipoEntrega.DataDisplayMember = ""
        Me.cbxTipoEntrega.DataFilter = Nothing
        Me.cbxTipoEntrega.DataOrderBy = ""
        Me.cbxTipoEntrega.DataSource = ""
        Me.cbxTipoEntrega.DataValueMember = ""
        Me.cbxTipoEntrega.dtSeleccionado = Nothing
        Me.cbxTipoEntrega.FormABM = Nothing
        Me.cbxTipoEntrega.Indicaciones = Nothing
        Me.cbxTipoEntrega.Location = New System.Drawing.Point(560, 131)
        Me.cbxTipoEntrega.Name = "cbxTipoEntrega"
        Me.cbxTipoEntrega.SeleccionMultiple = False
        Me.cbxTipoEntrega.SeleccionObligatoria = True
        Me.cbxTipoEntrega.Size = New System.Drawing.Size(256, 21)
        Me.cbxTipoEntrega.SoloLectura = False
        Me.cbxTipoEntrega.TabIndex = 44
        Me.cbxTipoEntrega.Texto = ""
        '
        'lblTipoEntrega
        '
        Me.lblTipoEntrega.AutoSize = True
        Me.lblTipoEntrega.Location = New System.Drawing.Point(487, 134)
        Me.lblTipoEntrega.Name = "lblTipoEntrega"
        Me.lblTipoEntrega.Size = New System.Drawing.Size(71, 13)
        Me.lblTipoEntrega.TabIndex = 43
        Me.lblTipoEntrega.Text = "Tipo Entrega:"
        '
        'txtHoraHasta
        '
        Me.txtHoraHasta.CausesValidation = False
        Me.txtHoraHasta.Enabled = False
        Me.txtHoraHasta.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.txtHoraHasta.Location = New System.Drawing.Point(349, 131)
        Me.txtHoraHasta.Name = "txtHoraHasta"
        Me.txtHoraHasta.Size = New System.Drawing.Size(108, 20)
        Me.txtHoraHasta.TabIndex = 42
        Me.txtHoraHasta.Value = New Date(2019, 1, 25, 0, 0, 0, 0)
        '
        'txtHoraDesde
        '
        Me.txtHoraDesde.CausesValidation = False
        Me.txtHoraDesde.Enabled = False
        Me.txtHoraDesde.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.txtHoraDesde.Location = New System.Drawing.Point(134, 131)
        Me.txtHoraDesde.Name = "txtHoraDesde"
        Me.txtHoraDesde.Size = New System.Drawing.Size(108, 20)
        Me.txtHoraDesde.TabIndex = 41
        Me.txtHoraDesde.Value = New Date(2019, 1, 25, 0, 0, 0, 0)
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(1, 134)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(52, 13)
        Me.Label9.TabIndex = 40
        Me.Label9.Text = "Hora Fija:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(308, 134)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(38, 13)
        Me.Label8.TabIndex = 39
        Me.Label8.Text = "Hasta:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(92, 134)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 13)
        Me.Label7.TabIndex = 38
        Me.Label7.Text = "Desde:"
        '
        'chkRestriccionHoraEntrega
        '
        Me.chkRestriccionHoraEntrega.BackColor = System.Drawing.Color.Transparent
        Me.chkRestriccionHoraEntrega.Color = System.Drawing.Color.Empty
        Me.chkRestriccionHoraEntrega.Location = New System.Drawing.Point(63, 135)
        Me.chkRestriccionHoraEntrega.Name = "chkRestriccionHoraEntrega"
        Me.chkRestriccionHoraEntrega.Size = New System.Drawing.Size(13, 13)
        Me.chkRestriccionHoraEntrega.SoloLectura = False
        Me.chkRestriccionHoraEntrega.TabIndex = 37
        Me.chkRestriccionHoraEntrega.Texto = Nothing
        Me.chkRestriccionHoraEntrega.Valor = False
        '
        'lblVerInventario
        '
        Me.lblVerInventario.AutoSize = True
        Me.lblVerInventario.Location = New System.Drawing.Point(636, 163)
        Me.lblVerInventario.Name = "lblVerInventario"
        Me.lblVerInventario.Size = New System.Drawing.Size(73, 13)
        Me.lblVerInventario.TabIndex = 36
        Me.lblVerInventario.TabStop = True
        Me.lblVerInventario.Text = "Ver Inventario"
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 117
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.ControlCorto = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(352, 17)
        Me.txtCliente.MostrarSucursal = True
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(464, 24)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 6
        '
        'cbxCondicion
        '
        Me.cbxCondicion.Enabled = False
        Me.cbxCondicion.FormattingEnabled = True
        Me.cbxCondicion.Location = New System.Drawing.Point(63, 75)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.Size = New System.Drawing.Size(86, 21)
        Me.cbxCondicion.TabIndex = 16
        '
        'OcxCotizacion1
        '
        Me.OcxCotizacion1.dt = Nothing
        Me.OcxCotizacion1.FiltroFecha = Nothing
        Me.OcxCotizacion1.Location = New System.Drawing.Point(176, 42)
        Me.OcxCotizacion1.Name = "OcxCotizacion1"
        Me.OcxCotizacion1.Registro = Nothing
        Me.OcxCotizacion1.Saltar = False
        Me.OcxCotizacion1.Seleccionado = True
        Me.OcxCotizacion1.Size = New System.Drawing.Size(119, 28)
        Me.OcxCotizacion1.SoloLectura = False
        Me.OcxCotizacion1.TabIndex = 10
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 79)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Condicion:"
        '
        'txtVenta
        '
        Me.txtVenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVenta.Color = System.Drawing.Color.Beige
        Me.txtVenta.Indicaciones = Nothing
        Me.txtVenta.Location = New System.Drawing.Point(678, 103)
        Me.txtVenta.Multilinea = False
        Me.txtVenta.Name = "txtVenta"
        Me.txtVenta.Size = New System.Drawing.Size(138, 21)
        Me.txtVenta.SoloLectura = True
        Me.txtVenta.TabIndex = 33
        Me.txtVenta.TabStop = False
        Me.txtVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtVenta.Texto = ""
        '
        'txtEstado
        '
        Me.txtEstado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstado.Color = System.Drawing.Color.Beige
        Me.txtEstado.Indicaciones = Nothing
        Me.txtEstado.Location = New System.Drawing.Point(533, 103)
        Me.txtEstado.Multilinea = False
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.Size = New System.Drawing.Size(84, 21)
        Me.txtEstado.SoloLectura = True
        Me.txtEstado.TabIndex = 31
        Me.txtEstado.TabStop = False
        Me.txtEstado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEstado.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(220, 19)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(69, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.Texto = ""
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(191, 23)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(29, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Suc:"
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(63, 19)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(63, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(126, 19)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(59, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(4, 23)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'txtTelefono
        '
        Me.txtTelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefono.Color = System.Drawing.Color.Beige
        Me.txtTelefono.Indicaciones = Nothing
        Me.txtTelefono.Location = New System.Drawing.Point(712, 47)
        Me.txtTelefono.Multilinea = False
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(104, 21)
        Me.txtTelefono.SoloLectura = True
        Me.txtTelefono.TabIndex = 14
        Me.txtTelefono.TabStop = False
        Me.txtTelefono.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTelefono.Texto = ""
        '
        'txtDetalle
        '
        Me.txtDetalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDetalle.Color = System.Drawing.Color.Empty
        Me.txtDetalle.Indicaciones = Nothing
        Me.txtDetalle.Location = New System.Drawing.Point(63, 159)
        Me.txtDetalle.Multilinea = False
        Me.txtDetalle.Name = "txtDetalle"
        Me.txtDetalle.Size = New System.Drawing.Size(554, 21)
        Me.txtDetalle.SoloLectura = False
        Me.txtDetalle.TabIndex = 35
        Me.txtDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDetalle.Texto = ""
        '
        'lblDetalle
        '
        Me.lblDetalle.AutoSize = True
        Me.lblDetalle.Location = New System.Drawing.Point(4, 163)
        Me.lblDetalle.Name = "lblDetalle"
        Me.lblDetalle.Size = New System.Drawing.Size(43, 13)
        Me.lblDetalle.TabIndex = 34
        Me.lblDetalle.Text = "Detalle:"
        '
        'txtCreditoSaldo
        '
        Me.txtCreditoSaldo.Color = System.Drawing.Color.Beige
        Me.txtCreditoSaldo.Decimales = False
        Me.txtCreditoSaldo.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCreditoSaldo.Location = New System.Drawing.Point(214, 75)
        Me.txtCreditoSaldo.Name = "txtCreditoSaldo"
        Me.txtCreditoSaldo.Size = New System.Drawing.Size(75, 21)
        Me.txtCreditoSaldo.SoloLectura = True
        Me.txtCreditoSaldo.TabIndex = 18
        Me.txtCreditoSaldo.TabStop = False
        Me.txtCreditoSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCreditoSaldo.Texto = "0"
        '
        'cbxListaPrecio
        '
        Me.cbxListaPrecio.CampoWhere = Nothing
        Me.cbxListaPrecio.CargarUnaSolaVez = False
        Me.cbxListaPrecio.DataDisplayMember = "Lista"
        Me.cbxListaPrecio.DataFilter = Nothing
        Me.cbxListaPrecio.DataOrderBy = "Lista"
        Me.cbxListaPrecio.DataSource = "VListaPrecio"
        Me.cbxListaPrecio.DataValueMember = "ID"
        Me.cbxListaPrecio.dtSeleccionado = Nothing
        Me.cbxListaPrecio.FormABM = Nothing
        Me.cbxListaPrecio.Indicaciones = Nothing
        Me.cbxListaPrecio.Location = New System.Drawing.Point(676, 75)
        Me.cbxListaPrecio.Name = "cbxListaPrecio"
        Me.cbxListaPrecio.SeleccionMultiple = False
        Me.cbxListaPrecio.SeleccionObligatoria = True
        Me.cbxListaPrecio.Size = New System.Drawing.Size(140, 21)
        Me.cbxListaPrecio.SoloLectura = False
        Me.cbxListaPrecio.TabIndex = 25
        Me.cbxListaPrecio.Texto = ""
        '
        'lblListaPrecio
        '
        Me.lblListaPrecio.AutoSize = True
        Me.lblListaPrecio.Location = New System.Drawing.Point(621, 79)
        Me.lblListaPrecio.Name = "lblListaPrecio"
        Me.lblListaPrecio.Size = New System.Drawing.Size(52, 13)
        Me.lblListaPrecio.TabIndex = 24
        Me.lblListaPrecio.Text = "L. Precio:"
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = Nothing
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "VVendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.FormABM = Nothing
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(352, 103)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(138, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 29
        Me.cbxVendedor.Texto = "ALDO GONZALES"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(131, 51)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 9
        Me.lblMoneda.Text = "Moneda:"
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = Nothing
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = ""
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = Nothing
        Me.cbxDeposito.DataSource = ""
        Me.cbxDeposito.DataValueMember = ""
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(63, 103)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = True
        Me.cbxDeposito.Size = New System.Drawing.Size(226, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 27
        Me.cbxDeposito.Texto = ""
        '
        'lblDeposito
        '
        Me.lblDeposito.AutoSize = True
        Me.lblDeposito.Location = New System.Drawing.Point(4, 107)
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(52, 13)
        Me.lblDeposito.TabIndex = 26
        Me.lblDeposito.Text = "Deposito:"
        '
        'txtFechaFacturar
        '
        Me.txtFechaFacturar.AñoFecha = 0
        Me.txtFechaFacturar.Color = System.Drawing.Color.Empty
        Me.txtFechaFacturar.Fecha = New Date(2013, 6, 20, 15, 10, 56, 131)
        Me.txtFechaFacturar.Location = New System.Drawing.Point(63, 47)
        Me.txtFechaFacturar.MesFecha = 0
        Me.txtFechaFacturar.Name = "txtFechaFacturar"
        Me.txtFechaFacturar.PermitirNulo = False
        Me.txtFechaFacturar.Size = New System.Drawing.Size(67, 20)
        Me.txtFechaFacturar.SoloLectura = False
        Me.txtFechaFacturar.TabIndex = 8
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(4, 51)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(49, 13)
        Me.lblFecha.TabIndex = 7
        Me.lblFecha.Text = "Facturar:"
        '
        'txtDireccion
        '
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Color = System.Drawing.Color.Beige
        Me.txtDireccion.Indicaciones = Nothing
        Me.txtDireccion.Location = New System.Drawing.Point(352, 47)
        Me.txtDireccion.Multilinea = False
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(325, 21)
        Me.txtDireccion.SoloLectura = True
        Me.txtDireccion.TabIndex = 12
        Me.txtDireccion.TabStop = False
        Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccion.Texto = ""
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.Location = New System.Drawing.Point(293, 51)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(55, 13)
        Me.lblDireccion.TabIndex = 11
        Me.lblDireccion.Text = "Direccion:"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(306, 23)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(42, 13)
        Me.lblCliente.TabIndex = 5
        Me.lblCliente.Text = "Cliente:"
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Location = New System.Drawing.Point(682, 51)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(25, 13)
        Me.lblTelefono.TabIndex = 13
        Me.lblTelefono.Text = "Tel:"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(492, 107)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 30
        Me.lblEstado.Text = "Estado:"
        '
        'lblCreditoSaldo
        '
        Me.lblCreditoSaldo.AutoSize = True
        Me.lblCreditoSaldo.Location = New System.Drawing.Point(165, 79)
        Me.lblCreditoSaldo.Name = "lblCreditoSaldo"
        Me.lblCreditoSaldo.Size = New System.Drawing.Size(43, 13)
        Me.lblCreditoSaldo.TabIndex = 17
        Me.lblCreditoSaldo.Text = "Credito:"
        '
        'lblVenta
        '
        Me.lblVenta.AutoSize = True
        Me.lblVenta.Location = New System.Drawing.Point(638, 107)
        Me.lblVenta.Name = "lblVenta"
        Me.lblVenta.Size = New System.Drawing.Size(38, 13)
        Me.lblVenta.TabIndex = 32
        Me.lblVenta.Text = "Venta:"
        '
        'lblVendedor
        '
        Me.lblVendedor.AutoSize = True
        Me.lblVendedor.Location = New System.Drawing.Point(292, 107)
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Size = New System.Drawing.Size(56, 13)
        Me.lblVendedor.TabIndex = 28
        Me.lblVendedor.Text = "Vendedor:"
        '
        'cbxFormaPagoFactura
        '
        Me.cbxFormaPagoFactura.CampoWhere = Nothing
        Me.cbxFormaPagoFactura.CargarUnaSolaVez = True
        Me.cbxFormaPagoFactura.DataDisplayMember = "Referencia"
        Me.cbxFormaPagoFactura.DataFilter = Nothing
        Me.cbxFormaPagoFactura.DataOrderBy = "ID"
        Me.cbxFormaPagoFactura.DataSource = "VFormaPagoFactura"
        Me.cbxFormaPagoFactura.DataValueMember = "ID"
        Me.cbxFormaPagoFactura.dtSeleccionado = Nothing
        Me.cbxFormaPagoFactura.FormABM = Nothing
        Me.cbxFormaPagoFactura.Indicaciones = Nothing
        Me.cbxFormaPagoFactura.Location = New System.Drawing.Point(463, 75)
        Me.cbxFormaPagoFactura.Name = "cbxFormaPagoFactura"
        Me.cbxFormaPagoFactura.SeleccionMultiple = False
        Me.cbxFormaPagoFactura.SeleccionObligatoria = True
        Me.cbxFormaPagoFactura.Size = New System.Drawing.Size(157, 21)
        Me.cbxFormaPagoFactura.SoloLectura = False
        Me.cbxFormaPagoFactura.TabIndex = 23
        Me.cbxFormaPagoFactura.Texto = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(401, 79)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Form.Pago:"
        '
        'lblPlazo
        '
        Me.lblPlazo.AutoSize = True
        Me.lblPlazo.Location = New System.Drawing.Point(312, 79)
        Me.lblPlazo.Name = "lblPlazo"
        Me.lblPlazo.Size = New System.Drawing.Size(36, 13)
        Me.lblPlazo.TabIndex = 20
        Me.lblPlazo.Text = "Plazo:"
        '
        'txtPlazo
        '
        Me.txtPlazo.Color = System.Drawing.Color.Empty
        Me.txtPlazo.Decimales = False
        Me.txtPlazo.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtPlazo.Location = New System.Drawing.Point(352, 75)
        Me.txtPlazo.Name = "txtPlazo"
        Me.txtPlazo.Size = New System.Drawing.Size(47, 21)
        Me.txtPlazo.SoloLectura = True
        Me.txtPlazo.TabIndex = 21
        Me.txtPlazo.TabStop = False
        Me.txtPlazo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPlazo.Texto = "0"
        '
        'lblAnulado
        '
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Red
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(141, 25)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        Me.lblAnulado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(150, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 0
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(202, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 1
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(8, 400)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(441, 20)
        Me.flpRegistradoPor.TabIndex = 2
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmiAgregar, Me.tsmiEliminar, Me.tsmiInformacion, Me.ToolStripSeparator1, Me.tsmiDescuento})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(149, 98)
        '
        'tsmiAgregar
        '
        Me.tsmiAgregar.Name = "tsmiAgregar"
        Me.tsmiAgregar.ShortcutKeyDisplayString = "+"
        Me.tsmiAgregar.Size = New System.Drawing.Size(148, 22)
        Me.tsmiAgregar.Text = "Agregar"
        '
        'lblDescuentoProducto
        '
        Me.lblDescuentoProducto.AutoSize = True
        Me.lblDescuentoProducto.Location = New System.Drawing.Point(560, 11)
        Me.lblDescuentoProducto.Name = "lblDescuentoProducto"
        Me.lblDescuentoProducto.Size = New System.Drawing.Size(35, 13)
        Me.lblDescuentoProducto.TabIndex = 8
        Me.lblDescuentoProducto.Text = "Desc:"
        '
        'gbxDetalle
        '
        Me.gbxDetalle.Controls.Add(Me.txtDescuento)
        Me.gbxDetalle.Controls.Add(Me.txtObservacionProducto)
        Me.gbxDetalle.Controls.Add(Me.txtProducto)
        Me.gbxDetalle.Controls.Add(Me.txtExistencia)
        Me.gbxDetalle.Controls.Add(Me.lblExistencia)
        Me.gbxDetalle.Controls.Add(Me.Label3)
        Me.gbxDetalle.Controls.Add(Me.OcxTXTNumeric1)
        Me.gbxDetalle.Controls.Add(Me.lblDescuentoProducto)
        Me.gbxDetalle.Controls.Add(Me.txtPrecioUnitario)
        Me.gbxDetalle.Controls.Add(Me.lblPrecioUnitario)
        Me.gbxDetalle.Controls.Add(Me.lvLista)
        Me.gbxDetalle.Controls.Add(Me.lblObservacionProducto)
        Me.gbxDetalle.Controls.Add(Me.txtImporte)
        Me.gbxDetalle.Controls.Add(Me.lblImporte)
        Me.gbxDetalle.Controls.Add(Me.txtCantidad)
        Me.gbxDetalle.Controls.Add(Me.lblCantidad)
        Me.gbxDetalle.Controls.Add(Me.lblProducto)
        Me.gbxDetalle.Location = New System.Drawing.Point(4, 200)
        Me.gbxDetalle.Name = "gbxDetalle"
        Me.gbxDetalle.Size = New System.Drawing.Size(829, 201)
        Me.gbxDetalle.TabIndex = 1
        Me.gbxDetalle.TabStop = False
        '
        'txtDescuento
        '
        Me.txtDescuento.Color = System.Drawing.Color.Empty
        Me.txtDescuento.Decimales = True
        Me.txtDescuento.Indicaciones = Nothing
        Me.txtDescuento.Location = New System.Drawing.Point(560, 26)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(67, 21)
        Me.txtDescuento.SoloLectura = True
        Me.txtDescuento.TabIndex = 9
        Me.txtDescuento.TabStop = False
        Me.txtDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDescuento.Texto = "0"
        '
        'txtObservacionProducto
        '
        Me.txtObservacionProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacionProducto.Color = System.Drawing.Color.Empty
        Me.txtObservacionProducto.Indicaciones = Nothing
        Me.txtObservacionProducto.Location = New System.Drawing.Point(379, 26)
        Me.txtObservacionProducto.Multilinea = False
        Me.txtObservacionProducto.Name = "txtObservacionProducto"
        Me.txtObservacionProducto.Size = New System.Drawing.Size(143, 21)
        Me.txtObservacionProducto.SoloLectura = False
        Me.txtObservacionProducto.TabIndex = 3
        Me.txtObservacionProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacionProducto.Texto = ""
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 260
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = True
        Me.txtProducto.ControlarReservas = True
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(9, 26)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(371, 20)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 1
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'txtExistencia
        '
        Me.txtExistencia.Color = System.Drawing.Color.Empty
        Me.txtExistencia.Decimales = True
        Me.txtExistencia.Indicaciones = Nothing
        Me.txtExistencia.Location = New System.Drawing.Point(522, 26)
        Me.txtExistencia.Name = "txtExistencia"
        Me.txtExistencia.Size = New System.Drawing.Size(38, 21)
        Me.txtExistencia.SoloLectura = True
        Me.txtExistencia.TabIndex = 5
        Me.txtExistencia.TabStop = False
        Me.txtExistencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtExistencia.Texto = "0"
        '
        'lblExistencia
        '
        Me.lblExistencia.AutoSize = True
        Me.lblExistencia.Location = New System.Drawing.Point(522, 11)
        Me.lblExistencia.Name = "lblExistencia"
        Me.lblExistencia.Size = New System.Drawing.Size(32, 13)
        Me.lblExistencia.TabIndex = 4
        Me.lblExistencia.Text = "Exist:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(-74, -298)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 13)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Desc:"
        '
        'OcxTXTNumeric1
        '
        Me.OcxTXTNumeric1.Color = System.Drawing.Color.Empty
        Me.OcxTXTNumeric1.Decimales = False
        Me.OcxTXTNumeric1.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.OcxTXTNumeric1.Location = New System.Drawing.Point(-72, -308)
        Me.OcxTXTNumeric1.Name = "OcxTXTNumeric1"
        Me.OcxTXTNumeric1.Size = New System.Drawing.Size(37, 21)
        Me.OcxTXTNumeric1.SoloLectura = True
        Me.OcxTXTNumeric1.TabIndex = 15
        Me.OcxTXTNumeric1.TabStop = False
        Me.OcxTXTNumeric1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.OcxTXTNumeric1.Texto = "0"
        '
        'txtPrecioUnitario
        '
        Me.txtPrecioUnitario.Color = System.Drawing.Color.Empty
        Me.txtPrecioUnitario.Decimales = False
        Me.txtPrecioUnitario.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtPrecioUnitario.Location = New System.Drawing.Point(672, 26)
        Me.txtPrecioUnitario.Name = "txtPrecioUnitario"
        Me.txtPrecioUnitario.Size = New System.Drawing.Size(67, 21)
        Me.txtPrecioUnitario.SoloLectura = False
        Me.txtPrecioUnitario.TabIndex = 15
        Me.txtPrecioUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrecioUnitario.Texto = "0"
        '
        'lblPrecioUnitario
        '
        Me.lblPrecioUnitario.AutoSize = True
        Me.lblPrecioUnitario.Location = New System.Drawing.Point(677, 10)
        Me.lblPrecioUnitario.Name = "lblPrecioUnitario"
        Me.lblPrecioUnitario.Size = New System.Drawing.Size(62, 13)
        Me.lblPrecioUnitario.TabIndex = 14
        Me.lblPrecioUnitario.Text = "Precio Uni.:"
        '
        'lvLista
        '
        Me.lvLista.BackColor = System.Drawing.Color.White
        Me.lvLista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvLista.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colID, Me.ColReferencia, Me.colDescripcion, Me.colIVA, Me.colCantidad, Me.colPrecioUnitario, Me.colDescuentoUnitario, Me.colDescuentoPocentaje, Me.colExento, Me.colGravado})
        Me.lvLista.ContextMenuStrip = Me.ContextMenuStrip1
        Me.lvLista.FullRowSelect = True
        Me.lvLista.GridLines = True
        Me.lvLista.HideSelection = False
        Me.lvLista.Location = New System.Drawing.Point(8, 52)
        Me.lvLista.MultiSelect = False
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(817, 143)
        Me.lvLista.TabIndex = 18
        Me.lvLista.UseCompatibleStateImageBehavior = False
        Me.lvLista.View = System.Windows.Forms.View.Details
        '
        'colID
        '
        Me.colID.Text = "ID"
        Me.colID.Width = 0
        '
        'ColReferencia
        '
        Me.ColReferencia.Text = "Referencia"
        Me.ColReferencia.Width = 77
        '
        'colDescripcion
        '
        Me.colDescripcion.Text = "Descripcion"
        Me.colDescripcion.Width = 289
        '
        'colIVA
        '
        Me.colIVA.Text = "IVA"
        Me.colIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.colIVA.Width = 50
        '
        'colCantidad
        '
        Me.colCantidad.Text = "Cantidad"
        Me.colCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colCantidad.Width = 55
        '
        'colPrecioUnitario
        '
        Me.colPrecioUnitario.Text = "Pre. Uni."
        Me.colPrecioUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colPrecioUnitario.Width = 73
        '
        'colDescuentoUnitario
        '
        Me.colDescuentoUnitario.Text = "Desc. Uni."
        Me.colDescuentoUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colDescuentoUnitario.Width = 68
        '
        'colDescuentoPocentaje
        '
        Me.colDescuentoPocentaje.Text = "Desc. %"
        Me.colDescuentoPocentaje.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'colExento
        '
        Me.colExento.Text = "Exento"
        Me.colExento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colExento.Width = 65
        '
        'colGravado
        '
        Me.colGravado.Text = "Gravado"
        Me.colGravado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colGravado.Width = 75
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = False
        Me.txtImporte.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporte.Location = New System.Drawing.Point(740, 26)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(81, 21)
        Me.txtImporte.SoloLectura = True
        Me.txtImporte.TabIndex = 17
        Me.txtImporte.TabStop = False
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCantidad.Location = New System.Drawing.Point(628, 26)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(43, 21)
        Me.txtCantidad.SoloLectura = False
        Me.txtCantidad.TabIndex = 13
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(641, 413)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Total:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(613, 439)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Descuento:"
        '
        'ContextMenuStrip2
        '
        Me.ContextMenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AgregarToolStripMenuItem, Me.MidiToolStripMenuItem, Me.EliminarToolStripMenuItem1})
        Me.ContextMenuStrip2.Name = "ContextMenuStrip2"
        Me.ContextMenuStrip2.Size = New System.Drawing.Size(167, 70)
        '
        'AgregarToolStripMenuItem
        '
        Me.AgregarToolStripMenuItem.Name = "AgregarToolStripMenuItem"
        Me.AgregarToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.AgregarToolStripMenuItem.Text = "+ Agregar"
        '
        'MidiToolStripMenuItem
        '
        Me.MidiToolStripMenuItem.Name = "MidiToolStripMenuItem"
        Me.MidiToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.MidiToolStripMenuItem.Text = "Ctrl+M Modificar"
        '
        'EliminarToolStripMenuItem1
        '
        Me.EliminarToolStripMenuItem1.Name = "EliminarToolStripMenuItem1"
        Me.EliminarToolStripMenuItem1.Size = New System.Drawing.Size(166, 22)
        Me.EliminarToolStripMenuItem1.Text = "DEL Eliminar"
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(10, 505)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 9
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnClonar
        '
        Me.btnClonar.Location = New System.Drawing.Point(10, 505)
        Me.btnClonar.Name = "btnClonar"
        Me.btnClonar.Size = New System.Drawing.Size(75, 23)
        Me.btnClonar.TabIndex = 9
        Me.btnClonar.Text = "Reprocesar"
        Me.btnClonar.UseVisualStyleBackColor = True
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(7, 477)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(442, 25)
        Me.flpAnuladoPor.TabIndex = 3
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(312, 505)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 17
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'flpReprocesado
        '
        Me.flpReprocesado.Controls.Add(Me.Label6)
        Me.flpReprocesado.Controls.Add(Me.btnPedidoOriginal)
        Me.flpReprocesado.Controls.Add(Me.lblFechaReproceso)
        Me.flpReprocesado.Location = New System.Drawing.Point(7, 447)
        Me.flpReprocesado.Name = "flpReprocesado"
        Me.flpReprocesado.Size = New System.Drawing.Size(442, 28)
        Me.flpReprocesado.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.LemonChiffon
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.2!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.Color.Red
        Me.Label6.Location = New System.Drawing.Point(3, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(143, 25)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "REPROCESADO"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnPedidoOriginal
        '
        Me.btnPedidoOriginal.Location = New System.Drawing.Point(152, 3)
        Me.btnPedidoOriginal.Name = "btnPedidoOriginal"
        Me.btnPedidoOriginal.Size = New System.Drawing.Size(136, 23)
        Me.btnPedidoOriginal.TabIndex = 19
        Me.btnPedidoOriginal.Text = "Cargar Pedido Original"
        Me.btnPedidoOriginal.UseVisualStyleBackColor = True
        '
        'lblFechaReproceso
        '
        Me.lblFechaReproceso.AutoSize = True
        Me.lblFechaReproceso.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaReproceso.Location = New System.Drawing.Point(294, 0)
        Me.lblFechaReproceso.Name = "lblFechaReproceso"
        Me.lblFechaReproceso.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaReproceso.TabIndex = 20
        Me.lblFechaReproceso.Text = "Fecha"
        '
        'flpModificadoPor
        '
        Me.flpModificadoPor.Controls.Add(Me.lblModificadoPor)
        Me.flpModificadoPor.Controls.Add(Me.lblUsuarioModificado)
        Me.flpModificadoPor.Controls.Add(Me.lblFechaModificado)
        Me.flpModificadoPor.Location = New System.Drawing.Point(7, 424)
        Me.flpModificadoPor.Name = "flpModificadoPor"
        Me.flpModificadoPor.Size = New System.Drawing.Size(441, 20)
        Me.flpModificadoPor.TabIndex = 19
        '
        'lblModificadoPor
        '
        Me.lblModificadoPor.AutoSize = True
        Me.lblModificadoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblModificadoPor.Name = "lblModificadoPor"
        Me.lblModificadoPor.Size = New System.Drawing.Size(80, 13)
        Me.lblModificadoPor.TabIndex = 0
        Me.lblModificadoPor.Text = "Modificado por:"
        '
        'lblUsuarioModificado
        '
        Me.lblUsuarioModificado.AutoSize = True
        Me.lblUsuarioModificado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioModificado.Location = New System.Drawing.Point(89, 0)
        Me.lblUsuarioModificado.Name = "lblUsuarioModificado"
        Me.lblUsuarioModificado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioModificado.TabIndex = 1
        Me.lblUsuarioModificado.Text = "Usuario:"
        '
        'lblFechaModificado
        '
        Me.lblFechaModificado.AutoSize = True
        Me.lblFechaModificado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaModificado.Location = New System.Drawing.Point(141, 0)
        Me.lblFechaModificado.Name = "lblFechaModificado"
        Me.lblFechaModificado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaModificado.TabIndex = 2
        Me.lblFechaModificado.Text = "Fecha"
        '
        'btnPedidoNotaCredito
        '
        Me.btnPedidoNotaCredito.Location = New System.Drawing.Point(494, 476)
        Me.btnPedidoNotaCredito.Name = "btnPedidoNotaCredito"
        Me.btnPedidoNotaCredito.Size = New System.Drawing.Size(156, 23)
        Me.btnPedidoNotaCredito.TabIndex = 20
        Me.btnPedidoNotaCredito.Text = "Pedir Nota de Credito"
        Me.btnPedidoNotaCredito.UseVisualStyleBackColor = True
        '
        'chkEntregaCliente
        '
        Me.chkEntregaCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkEntregaCliente.Color = System.Drawing.Color.Empty
        Me.chkEntregaCliente.Enabled = False
        Me.chkEntregaCliente.Location = New System.Drawing.Point(458, 441)
        Me.chkEntregaCliente.Name = "chkEntregaCliente"
        Me.chkEntregaCliente.Size = New System.Drawing.Size(136, 18)
        Me.chkEntregaCliente.SoloLectura = False
        Me.chkEntregaCliente.TabIndex = 18
        Me.chkEntregaCliente.Texto = "Entrega directa"
        Me.chkEntregaCliente.Valor = False
        '
        'OcxImpuesto1
        '
        Me.OcxImpuesto1.Decimales = False
        Me.OcxImpuesto1.dtImpuesto = Nothing
        Me.OcxImpuesto1.IDMoneda = 0
        Me.OcxImpuesto1.Location = New System.Drawing.Point(455, 399)
        Me.OcxImpuesto1.Name = "OcxImpuesto1"
        Me.OcxImpuesto1.Size = New System.Drawing.Size(137, 39)
        Me.OcxImpuesto1.TabIndex = 3
        Me.OcxImpuesto1.TotalRetencionIVA = New Decimal(New Integer() {0, 0, 0, 0})
        Me.OcxImpuesto1.Visible = False
        '
        'txtTotal
        '
        Me.txtTotal.Color = System.Drawing.Color.Empty
        Me.txtTotal.Decimales = False
        Me.txtTotal.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotal.Location = New System.Drawing.Point(681, 409)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(146, 21)
        Me.txtTotal.SoloLectura = True
        Me.txtTotal.TabIndex = 5
        Me.txtTotal.TabStop = False
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Texto = "0"
        '
        'txtTotalDescuento
        '
        Me.txtTotalDescuento.Color = System.Drawing.Color.Empty
        Me.txtTotalDescuento.Decimales = False
        Me.txtTotalDescuento.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotalDescuento.Location = New System.Drawing.Point(723, 435)
        Me.txtTotalDescuento.Name = "txtTotalDescuento"
        Me.txtTotalDescuento.Size = New System.Drawing.Size(105, 21)
        Me.txtTotalDescuento.SoloLectura = True
        Me.txtTotalDescuento.TabIndex = 8
        Me.txtTotalDescuento.TabStop = False
        Me.txtTotalDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDescuento.Texto = "0"
        '
        'txtPorcentajeDescuento
        '
        Me.txtPorcentajeDescuento.Color = System.Drawing.Color.Empty
        Me.txtPorcentajeDescuento.Decimales = False
        Me.txtPorcentajeDescuento.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtPorcentajeDescuento.Location = New System.Drawing.Point(681, 435)
        Me.txtPorcentajeDescuento.Name = "txtPorcentajeDescuento"
        Me.txtPorcentajeDescuento.Size = New System.Drawing.Size(42, 21)
        Me.txtPorcentajeDescuento.SoloLectura = True
        Me.txtPorcentajeDescuento.TabIndex = 7
        Me.txtPorcentajeDescuento.TabStop = False
        Me.txtPorcentajeDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajeDescuento.Texto = "0"
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 6, 20, 15, 10, 48, 156)
        Me.txtFecha.Location = New System.Drawing.Point(69, 45)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(67, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 7
        '
        'frmPedidoNUEVO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(846, 554)
        Me.Controls.Add(Me.btnPedidoNotaCredito)
        Me.Controls.Add(Me.flpModificadoPor)
        Me.Controls.Add(Me.chkEntregaCliente)
        Me.Controls.Add(Me.flpReprocesado)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Controls.Add(Me.gbxDetalle)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.btnClonar)
        Me.Controls.Add(Me.flpAnuladoPor)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.OcxImpuesto1)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtTotalDescuento)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.txtPorcentajeDescuento)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnGuardar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmPedidoNUEVO"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "PEDIDOS"
        Me.Text = "Pedidos"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.gbxDetalle.ResumeLayout(False)
        Me.gbxDetalle.PerformLayout()
        Me.ContextMenuStrip2.ResumeLayout(False)
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.flpReprocesado.ResumeLayout(False)
        Me.flpReprocesado.PerformLayout()
        Me.flpModificadoPor.ResumeLayout(False)
        Me.flpModificadoPor.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblProducto As System.Windows.Forms.Label
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents txtTelefono As ERP.ocxTXTString
    Friend WithEvents lblTelefono As System.Windows.Forms.Label
    Friend WithEvents txtDetalle As ERP.ocxTXTString
    Friend WithEvents lblDetalle As System.Windows.Forms.Label
    Friend WithEvents txtCreditoSaldo As ERP.ocxTXTNumeric
    Friend WithEvents lblCreditoSaldo As System.Windows.Forms.Label
    Friend WithEvents cbxListaPrecio As ERP.ocxCBX
    Friend WithEvents lblListaPrecio As System.Windows.Forms.Label
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents lblVendedor As System.Windows.Forms.Label
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents lblDeposito As System.Windows.Forms.Label
    Friend WithEvents txtFechaFacturar As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As ERP.ocxTXTString
    Friend WithEvents lblDireccion As System.Windows.Forms.Label
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents gbxDetalle As System.Windows.Forms.GroupBox
    Friend WithEvents lblDescuentoProducto As System.Windows.Forms.Label
    Friend WithEvents txtPrecioUnitario As ERP.ocxTXTNumeric
    Friend WithEvents lblPrecioUnitario As System.Windows.Forms.Label
    Friend WithEvents txtObservacionProducto As ERP.ocxTXTString
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmiEliminar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiInformacion As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmiDescuento As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblObservacionProducto As System.Windows.Forms.Label
    Friend WithEvents txtImporte As ERP.ocxTXTNumeric
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As ERP.ocxTXTNumeric
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtTotalDescuento As ERP.ocxTXTNumeric
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtPorcentajeDescuento As ERP.ocxTXTNumeric
    Friend WithEvents txtTotal As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tsmiAgregar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStrip2 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents AgregarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MidiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OcxImpuesto1 As ERP.ocxImpuesto
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents txtVenta As ERP.ocxTXTString
    Friend WithEvents txtEstado As ERP.ocxTXTString
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lblVenta As System.Windows.Forms.Label
    Friend WithEvents btnClonar As System.Windows.Forms.Button
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents OcxTXTNumeric1 As ERP.ocxTXTNumeric
    Friend WithEvents txtExistencia As ERP.ocxTXTNumeric
    Friend WithEvents lblExistencia As System.Windows.Forms.Label
    Friend WithEvents txtDescuento As ERP.ocxTXTNumeric
    Friend WithEvents cbxCondicion As System.Windows.Forms.ComboBox
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cbxFormaPagoFactura As ERP.ocxCBX
    Friend WithEvents lblPlazo As System.Windows.Forms.Label
    Friend WithEvents txtPlazo As ERP.ocxTXTNumeric
    Friend WithEvents OcxCotizacion1 As ERP.ocxCotizacion
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents lblVerInventario As System.Windows.Forms.LinkLabel
    Friend WithEvents chkEntregaCliente As ERP.ocxCHK
    Friend WithEvents flpReprocesado As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnPedidoOriginal As System.Windows.Forms.Button
    Friend WithEvents lblFechaReproceso As System.Windows.Forms.Label
    Friend WithEvents flpModificadoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblModificadoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioModificado As System.Windows.Forms.Label
    Friend WithEvents lblFechaModificado As System.Windows.Forms.Label
    Friend WithEvents btnPedidoNotaCredito As Button
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents chkRestriccionHoraEntrega As ocxCHK
    Friend WithEvents txtHoraHasta As DateTimePicker
    Friend WithEvents txtHoraDesde As DateTimePicker
    Friend WithEvents lvLista As ListView
    Friend WithEvents colID As ColumnHeader
    Friend WithEvents ColReferencia As ColumnHeader
    Friend WithEvents colDescripcion As ColumnHeader
    Friend WithEvents colIVA As ColumnHeader
    Friend WithEvents colCantidad As ColumnHeader
    Friend WithEvents colPrecioUnitario As ColumnHeader
    Friend WithEvents colDescuentoUnitario As ColumnHeader
    Friend WithEvents colDescuentoPocentaje As ColumnHeader
    Friend WithEvents colExento As ColumnHeader
    Friend WithEvents colGravado As ColumnHeader
    Friend WithEvents cbxTipoEntrega As ocxCBX
    Friend WithEvents lblTipoEntrega As Label
End Class
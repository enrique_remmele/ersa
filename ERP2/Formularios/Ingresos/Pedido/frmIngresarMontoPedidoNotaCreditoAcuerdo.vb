﻿Public Class frmIngresarMontoPedidoNotaCreditoAcuerdo
    Public Property ImportePedido As Decimal
    Public Property Importe As Decimal
    Public Property Porcentaje As Decimal
    Public Property Procesado As Boolean = False
    Public Property Decimales As Boolean = False
    Dim vDecimalOperacion As Boolean


    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click, btnCancelar.Click
        Importe = txtImporte.ObtenerValor
        Porcentaje = txtPorcentaje.ObtenerValor
        If Importe > ImportePedido Then
            MessageBox.Show("El importe debe ser menor o igual que " & ImportePedido, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If Porcentaje > 100 Then
            MessageBox.Show("El Porcentaje no puede ser mayor al 100% ", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        Procesado = True
            Me.Close()
        End Sub

    Private Sub frmIngresarMontoPedidoNotaCredito_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtImporteFC.Text = CInt(ImportePedido)
        txtImporte.Decimales = Decimales
        txtImporte.Focus()
    End Sub

    Private Sub txtImporte_KeyUp(sender As Object, e As KeyEventArgs) Handles txtImporte.KeyUp
            If e.KeyCode = Keys.Enter Then
                Importe = txtImporte.ObtenerValor
                If Importe > ImportePedido Then
                    MessageBox.Show("El importe debe ser menor o igual que " & ImportePedido, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
                Me.Close()
            End If
        End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmIngresarMontoPedidoNotaCreditoAcuerdo_Activate()
        Me.Refresh()
    End Sub

    Private Sub txtImporte_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtImporte.TeclaPrecionada

        If txtImporte.txt.Text = "" Then
            MessageBox.Show("Ingresar Importe ", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        txtPorcentaje.txt.Text = 0
        txtPorcentaje.txt.Text = (txtImporte.txt.Text * 100) / txtImporteFC.Text


    End Sub

    Private Sub txtPorcentaje_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtPorcentaje.TeclaPrecionada

        If txtPorcentaje.txt.Text = "" Then
            MessageBox.Show("Ingresar Porcentaje ", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        txtImporte.txt.Text = 0
        txtImporte.txt.Text = (txtImporteFC.Text * txtPorcentaje.txt.Text) / 100

    End Sub

End Class

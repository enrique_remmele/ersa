﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAprobarExcesoLineaCredito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAprobarExcesoLineaCredito))
        Me.lvPedidos = New System.Windows.Forms.ListView()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtFechaDesde = New ERP.ocxTXTDate()
        Me.txtFechaHasta = New ERP.ocxTXTDate()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel2 = New System.Windows.Forms.ToolStripLabel()
        Me.cbxOrden = New System.Windows.Forms.ToolStripComboBox()
        Me.btnActualizarPedidos = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnAprobar = New System.Windows.Forms.ToolStripButton()
        Me.btnSeleccionarTodos = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.btnRechazar = New System.Windows.Forms.ToolStripButton()
        Me.btnVerInventario = New System.Windows.Forms.Button()
        Me.btnVerPedido = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtCantidadPedidos = New ERP.ocxTXTNumeric()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lvGrupo = New System.Windows.Forms.ListView()
        Me.gbxGrupo = New System.Windows.Forms.GroupBox()
        Me.ToolStrip2 = New System.Windows.Forms.ToolStrip()
        Me.lblListadoPedidos = New System.Windows.Forms.ToolStripLabel()
        Me.cbxGrupos = New System.Windows.Forms.ToolStripComboBox()
        Me.btnActualizarGrupo = New System.Windows.Forms.ToolStripButton()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.gbxGrupo.SuspendLayout()
        Me.ToolStrip2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lvPedidos
        '
        Me.lvPedidos.CheckBoxes = True
        Me.lvPedidos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvPedidos.Location = New System.Drawing.Point(3, 35)
        Me.lvPedidos.Name = "lvPedidos"
        Me.lvPedidos.Size = New System.Drawing.Size(1009, 442)
        Me.lvPedidos.SmallImageList = Me.ImageList1
        Me.lvPedidos.TabIndex = 1
        Me.lvPedidos.UseCompatibleStateImageBehavior = False
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "ROJO.JPG")
        Me.ImageList1.Images.SetKeyName(1, "AMARILLO.JPG")
        Me.ImageList1.Images.SetKeyName(2, "VERDE.JPG")
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 23)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Fecha:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.Label2)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtFechaDesde)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtFechaHasta)
        Me.FlowLayoutPanel4.Controls.Add(Me.ToolStrip1)
        Me.FlowLayoutPanel4.Controls.Add(Me.btnVerInventario)
        Me.FlowLayoutPanel4.Controls.Add(Me.btnVerPedido)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(1009, 26)
        Me.FlowLayoutPanel4.TabIndex = 0
        '
        'txtFechaDesde
        '
        Me.txtFechaDesde.AñoFecha = 0
        Me.txtFechaDesde.Color = System.Drawing.Color.Empty
        Me.txtFechaDesde.Fecha = New Date(CType(0, Long))
        Me.txtFechaDesde.Location = New System.Drawing.Point(53, 3)
        Me.txtFechaDesde.MesFecha = 0
        Me.txtFechaDesde.Name = "txtFechaDesde"
        Me.txtFechaDesde.PermitirNulo = False
        Me.txtFechaDesde.Size = New System.Drawing.Size(65, 20)
        Me.txtFechaDesde.SoloLectura = False
        Me.txtFechaDesde.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.txtFechaDesde, "Presione para listar Pedidos")
        '
        'txtFechaHasta
        '
        Me.txtFechaHasta.AñoFecha = 0
        Me.txtFechaHasta.Color = System.Drawing.Color.Empty
        Me.txtFechaHasta.Fecha = New Date(CType(0, Long))
        Me.txtFechaHasta.Location = New System.Drawing.Point(124, 3)
        Me.txtFechaHasta.MesFecha = 0
        Me.txtFechaHasta.Name = "txtFechaHasta"
        Me.txtFechaHasta.PermitirNulo = False
        Me.txtFechaHasta.Size = New System.Drawing.Size(65, 20)
        Me.txtFechaHasta.SoloLectura = False
        Me.txtFechaHasta.TabIndex = 2
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel2, Me.cbxOrden, Me.btnActualizarPedidos, Me.ToolStripSeparator1, Me.ToolStripSeparator2, Me.btnAprobar, Me.btnSeleccionarTodos, Me.ToolStripButton1, Me.btnRechazar})
        Me.ToolStrip1.Location = New System.Drawing.Point(192, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(626, 25)
        Me.ToolStrip1.TabIndex = 3
        Me.ToolStrip1.TabStop = True
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripLabel2
        '
        Me.ToolStripLabel2.Name = "ToolStripLabel2"
        Me.ToolStripLabel2.Size = New System.Drawing.Size(84, 22)
        Me.ToolStripLabel2.Text = "Ordenado por:"
        '
        'cbxOrden
        '
        Me.cbxOrden.Name = "cbxOrden"
        Me.cbxOrden.Size = New System.Drawing.Size(90, 25)
        '
        'btnActualizarPedidos
        '
        Me.btnActualizarPedidos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnActualizarPedidos.Image = CType(resources.GetObject("btnActualizarPedidos.Image"), System.Drawing.Image)
        Me.btnActualizarPedidos.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnActualizarPedidos.Name = "btnActualizarPedidos"
        Me.btnActualizarPedidos.Size = New System.Drawing.Size(23, 22)
        Me.btnActualizarPedidos.Text = "ToolStripButton1"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'btnAprobar
        '
        Me.btnAprobar.Image = CType(resources.GetObject("btnAprobar.Image"), System.Drawing.Image)
        Me.btnAprobar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnAprobar.Name = "btnAprobar"
        Me.btnAprobar.Size = New System.Drawing.Size(70, 22)
        Me.btnAprobar.Text = "Aprobar"
        '
        'btnSeleccionarTodos
        '
        Me.btnSeleccionarTodos.Image = CType(resources.GetObject("btnSeleccionarTodos.Image"), System.Drawing.Image)
        Me.btnSeleccionarTodos.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSeleccionarTodos.Name = "btnSeleccionarTodos"
        Me.btnSeleccionarTodos.Size = New System.Drawing.Size(115, 22)
        Me.btnSeleccionarTodos.Text = "Seleccionar todo"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(113, 22)
        Me.ToolStripButton1.Text = "Quitar Seleccion"
        '
        'btnRechazar
        '
        Me.btnRechazar.Image = Global.ERP.My.Resources.Resources.icons8_eliminar_16
        Me.btnRechazar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnRechazar.Name = "btnRechazar"
        Me.btnRechazar.Size = New System.Drawing.Size(74, 22)
        Me.btnRechazar.Text = "Rechazar"
        '
        'btnVerInventario
        '
        Me.btnVerInventario.Location = New System.Drawing.Point(821, 3)
        Me.btnVerInventario.Name = "btnVerInventario"
        Me.btnVerInventario.Size = New System.Drawing.Size(85, 23)
        Me.btnVerInventario.TabIndex = 6
        Me.btnVerInventario.Text = "Ver Inventario"
        Me.btnVerInventario.UseVisualStyleBackColor = True
        '
        'btnVerPedido
        '
        Me.btnVerPedido.Location = New System.Drawing.Point(3, 32)
        Me.btnVerPedido.Name = "btnVerPedido"
        Me.btnVerPedido.Size = New System.Drawing.Size(102, 23)
        Me.btnVerPedido.TabIndex = 7
        Me.btnVerPedido.Text = "Ver Pedido"
        Me.btnVerPedido.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(504, 29)
        Me.FlowLayoutPanel2.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(367, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Cantidad:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtCantidadPedidos)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(504, 0)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(505, 29)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'txtCantidadPedidos
        '
        Me.txtCantidadPedidos.Color = System.Drawing.Color.Empty
        Me.txtCantidadPedidos.Decimales = True
        Me.txtCantidadPedidos.Indicaciones = Nothing
        Me.txtCantidadPedidos.Location = New System.Drawing.Point(436, 3)
        Me.txtCantidadPedidos.Name = "txtCantidadPedidos"
        Me.txtCantidadPedidos.Size = New System.Drawing.Size(66, 22)
        Me.txtCantidadPedidos.SoloLectura = True
        Me.txtCantidadPedidos.TabIndex = 1
        Me.txtCantidadPedidos.TabStop = False
        Me.txtCantidadPedidos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadPedidos.Texto = "0"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel2, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1, 1, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 483)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1009, 29)
        Me.TableLayoutPanel2.TabIndex = 4
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(1112, 3)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'lvGrupo
        '
        Me.lvGrupo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvGrupo.Location = New System.Drawing.Point(3, 43)
        Me.lvGrupo.Name = "lvGrupo"
        Me.lvGrupo.Size = New System.Drawing.Size(151, 488)
        Me.lvGrupo.SmallImageList = Me.ImageList1
        Me.lvGrupo.TabIndex = 1
        Me.lvGrupo.UseCompatibleStateImageBehavior = False
        '
        'gbxGrupo
        '
        Me.gbxGrupo.Controls.Add(Me.lvGrupo)
        Me.gbxGrupo.Controls.Add(Me.ToolStrip2)
        Me.gbxGrupo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxGrupo.Location = New System.Drawing.Point(3, 3)
        Me.gbxGrupo.Name = "gbxGrupo"
        Me.gbxGrupo.Size = New System.Drawing.Size(157, 534)
        Me.gbxGrupo.TabIndex = 0
        Me.gbxGrupo.TabStop = False
        Me.gbxGrupo.Text = "Grupos"
        '
        'ToolStrip2
        '
        Me.ToolStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblListadoPedidos, Me.cbxGrupos, Me.btnActualizarGrupo})
        Me.ToolStrip2.Location = New System.Drawing.Point(3, 16)
        Me.ToolStrip2.Name = "ToolStrip2"
        Me.ToolStrip2.Size = New System.Drawing.Size(151, 27)
        Me.ToolStrip2.TabIndex = 0
        Me.ToolStrip2.TabStop = True
        Me.ToolStrip2.Text = "ToolStrip2"
        '
        'lblListadoPedidos
        '
        Me.lblListadoPedidos.Name = "lblListadoPedidos"
        Me.lblListadoPedidos.Size = New System.Drawing.Size(50, 24)
        Me.lblListadoPedidos.Text = "Lista de:"
        '
        'cbxGrupos
        '
        Me.cbxGrupos.Items.AddRange(New Object() {"TODOS"})
        Me.cbxGrupos.Name = "cbxGrupos"
        Me.cbxGrupos.Size = New System.Drawing.Size(121, 23)
        '
        'btnActualizarGrupo
        '
        Me.btnActualizarGrupo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnActualizarGrupo.Image = CType(resources.GetObject("btnActualizarGrupo.Image"), System.Drawing.Image)
        Me.btnActualizarGrupo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnActualizarGrupo.Name = "btnActualizarGrupo"
        Me.btnActualizarGrupo.Size = New System.Drawing.Size(23, 20)
        Me.btnActualizarGrupo.Text = "ToolStripButton1"
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 163.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.FlowLayoutPanel3, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.gbxGrupo, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.GroupBox2, 1, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(1190, 573)
        Me.TableLayoutPanel3.TabIndex = 2
        '
        'FlowLayoutPanel3
        '
        Me.TableLayoutPanel3.SetColumnSpan(Me.FlowLayoutPanel3, 2)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnSalir)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblFecha)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(0, 540)
        Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(1190, 33)
        Me.FlowLayoutPanel3.TabIndex = 2
        '
        'lblFecha
        '
        Me.lblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(314, 0)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(792, 23)
        Me.lblFecha.TabIndex = 0
        Me.lblFecha.Text = "Label2"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Location = New System.Drawing.Point(166, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1021, 534)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Lista de Facturas"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lvPedidos, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 16)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1015, 515)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'frmAprobarExcesoLineaCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1190, 573)
        Me.Controls.Add(Me.TableLayoutPanel3)
        Me.Name = "frmAprobarExcesoLineaCredito"
        Me.Text = "frmAprobarExcesoLineaCredito"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel4.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.gbxGrupo.ResumeLayout(False)
        Me.gbxGrupo.PerformLayout()
        Me.ToolStrip2.ResumeLayout(False)
        Me.ToolStrip2.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtCantidadPedidos As ERP.ocxTXTNumeric
    Friend WithEvents lvPedidos As System.Windows.Forms.ListView
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtFechaDesde As ERP.ocxTXTDate
    Friend WithEvents txtFechaHasta As ERP.ocxTXTDate
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripLabel2 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents cbxOrden As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents btnActualizarPedidos As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnAprobar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnSeleccionarTodos As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents lvGrupo As System.Windows.Forms.ListView
    Friend WithEvents gbxGrupo As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStrip2 As System.Windows.Forms.ToolStrip
    Friend WithEvents lblListadoPedidos As System.Windows.Forms.ToolStripLabel
    Friend WithEvents cbxGrupos As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents btnActualizarGrupo As System.Windows.Forms.ToolStripButton
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnVerInventario As System.Windows.Forms.Button
    Friend WithEvents btnVerPedido As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnRechazar As System.Windows.Forms.ToolStripButton
End Class

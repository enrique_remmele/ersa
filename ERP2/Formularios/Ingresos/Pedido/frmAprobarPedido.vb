﻿Imports System.Threading
Public Class frmAprobarPedido
    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim tProcesar As Thread
    Dim vProcesar As Boolean = False


    Enum ENUMOperacionPedido
        APROBAR = 1
        RECHAZAR = 2
        RECUPERAR = 3
        ANULAR = 4
    End Enum

    'FUNCIONES
    Sub Inicializar()

        Me.KeyPreview = True

        'Varios
        CheckForIllegalCrossThreadCalls = False

        'cbxGrupos.SelectedIndex = 0
        cbxOrden.SelectedIndex = 0

        'Funciones
        ListarGrupos()

        'Fecha facturacion
        If IsDate(VGFechaFacturacion) = False Then
            VGFechaFacturacion = VGFechaHoraSistema
        End If

        Dim Fecha As Date = VGFechaFacturacion

        txtFechaDesde.SetValue(Fecha)
        txtFechaHasta.SetValue(Fecha)

        lblFecha.Text = " * Fecha de Facturacion: " & Fecha.ToShortDateString

        Dim f As Font = New Font(lblFecha.Font.FontFamily, 12, FontStyle.Bold, lblFecha.Font.Unit)
        lblFecha.Font = f
        lblFecha.ForeColor = Color.Maroon


    End Sub

    Sub CargarInformacion()

    End Sub

    Sub GuardarInformacion()

    End Sub
    Sub ListarGrupos()

        lvGrupo.Items.Clear()
        lvPedidos.Items.Clear()

        Dim SQL As String = ""
        'Dim INDEX As Integer = cbxGrupos.SelectedIndex

        If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        End If



        Dim dtGrupo As New DataTable("Grupos")

        dtGrupo.Columns.Add("Grupo", Type.GetType("System.String"))

        Dim newRow As DataRow = dtGrupo.NewRow()
        newRow(0) = "PENDIENTES"
        dtGrupo.Rows.Add(newRow)
        Dim newRowb As DataRow = dtGrupo.NewRow()
        newRowb(0) = "APROBADOS"
        dtGrupo.Rows.Add(newRowb)
        Dim newRowc As DataRow = dtGrupo.NewRow()
        newRowc(0) = "RECHAZADOS"
        dtGrupo.Rows.Add(newRowc)


        'If INDEX = 0 Then
        '    SQL = "Select 'Tipo'='Listar Todos'"
        'End If

        'If INDEX = 1 Then
        '    'SQL = "Select 'ID'=IDZonaVenta, 'Descripcion'=ZonaVenta, 'Cantidad'=COUNT(*) From VPedidosAFacturar Where IDSucursal=" & vgIDSucursal & " And Aprobacion='True' And DATEDIFF(d, FechaFacturar, '" & vgConfiguraciones("VentaFechaFacturacion") & "') = 0 Group By IDZonaVenta, ZonaVenta Order By 1"
        '    SQL = "Select 'ID'=IDZonaVenta, 'Descripcion'=ZonaVenta, 'Cantidad'=COUNT(*) From VPedidosAFacturar Where Aprobacion='True' And FechaFacturar between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'  Group By IDZonaVenta, ZonaVenta Order By 1"
        'End If

        'If INDEX = 2 Then
        '    'SQL = "Select 'ID'=IDVendedor, 'Descripcion'=Vendedor, 'Cantidad'=COUNT(*) From VPedidosAFacturar Where IDSucursal=" & vgIDSucursal & " And Aprobacion='True' And DATEDIFF(d, FechaFacturar, '" & vgConfiguraciones("VentaFechaFacturacion") & "') = 0 Group By IDVendedor, Vendedor Order By 1"
        '    SQL = "Select 'ID'=IDVendedor, 'Descripcion'=Vendedor, 'Cantidad'=COUNT(*) From VPedidosAFacturar Where Aprobacion='True' And FechaFacturar between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "' Group By IDVendedor, Vendedor Order By 1"
        'End If

        CSistema.dtToLv(lvGrupo, dtGrupo)


        For i = 0 To lvGrupo.Columns.Count - 1
            lvGrupo.Columns(i).AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize)
        Next

    End Sub

    Sub SeleccionarGrupo()

        ListarPedidos()

    End Sub

    Sub ListarPedidos()

        If lvGrupo.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim ID As String = lvGrupo.SelectedItems(0).Text
        Dim OrderBy As String = ""
        Dim Campo As String = ""


        If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        End If

        'Select Case cbxGrupos.SelectedIndex

        '    Case 0
        '        SQL = "Select 'Pedido'=Numero,Sucursal, 'Tipo'=FormaPagoFactura, 'Fec'=Facturar, Cliente, 'Entrega'=Direccion, 'Zona'=ZonaVenta, Vendedor, Total, Observacion, Estado,Aprobado,IDTransaccion From VPedido Where Facturado='False' And Anulado = 'False' and Aprobacion  = 'True' And FechaFacturar  Between '" & txtFechaDesde.GetValueString & "' And '" & txtFechaHasta.GetValueString & "'"
        '    Case 1
        '        If IsNumeric(ID) = False Then
        '            Exit Sub
        '        End If
        '        Campo = "IDZonaVenta"
        '        SQL = "Select 'Pedido'=Numero, Sucursal, 'Tipo'=FormaPagoFactura, 'Fec'=Facturar, Cliente, 'Entrega'=Direccion, 'Zona'=ZonaVenta, Vendedor, Total, Observacion, Estado,Aprobado,IDTransaccion From VPedido Where Facturado='False' And Anulado = 'False' And Aprobacion='True' And FechaFacturar Between '" & txtFechaDesde.GetValueString & "' And '" & txtFechaHasta.GetValueString & "' And " & Campo & "=" & ID & " "
        '    Case 2
        '        If IsNumeric(ID) = False Then
        '            Exit Sub
        '        End If
        '        Campo = "IDVendedor"
        '        SQL = "Select 'Pedido'=Numero, Sucursal, 'Tipo'=FormaPagoFactura, 'Fec'=Facturar, Cliente, 'Entrega'=Direccion, 'Zona'=ZonaVenta, Vendedor, Total, Observacion, Estado,Aprobado,IDTransaccion From VPedido Where Facturado='False' And Anulado = 'False' And Aprobacion='True' And FechaFacturar Between '" & txtFechaDesde.GetValueString & "' And '" & txtFechaHasta.GetValueString & "' And " & Campo & "=" & ID & " "
        'End Select

        Select Case lvGrupo.SelectedItems.Item(0).Text
            Case "PENDIENTES"
                SQL = "Select 'Pedido'=Numero,Sucursal, 'Tipo'=FormaPagoFactura, 'Fec'=Facturar, Cliente, 'Entrega'=Direccion, 'Zona'=ZonaVenta, Vendedor, Total, Observacion, Estado,Aprobado,IDTransaccion From VPedido Where Facturado='False' And Anulado = 'False' and Aprobacion  = 'True' and AprobadoCancelarAutomatico='PENDIENTE' And FechaFacturar  Between '" & txtFechaDesde.GetValueString & "' And '" & txtFechaHasta.GetValueString & "'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = True
                btnAprobar.Text = "Aprobar"
                btnRechazar.Text = "Rechazar"
            Case "APROBADOS"
                SQL = "Select 'Pedido'=Numero,Sucursal, 'Tipo'=FormaPagoFactura, 'Fec'=Facturar, Cliente, 'Entrega'=Direccion, 'Zona'=ZonaVenta, Vendedor, Total, Observacion, Estado,Aprobado,IDTransaccion From VPedido Where Facturado='False' And Anulado = 'False' and Aprobacion  = 'True' and AprobadoCancelarAutomatico='APROBADO' And FechaFacturar  Between '" & txtFechaDesde.GetValueString & "' And '" & txtFechaHasta.GetValueString & "'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = False
                btnAprobar.Text = "Recuperar"
            Case "RECHAZADOS"
                SQL = "Select 'Pedido'=Numero,Sucursal, 'Tipo'=FormaPagoFactura, 'Fec'=Facturar, Cliente, 'Entrega'=Direccion, 'Zona'=ZonaVenta, Vendedor, Total, Observacion, Estado,Aprobado,IDTransaccion From VPedido Where Facturado='False' And Anulado = 'False' and Aprobacion  = 'True' and AprobadoCancelarAutomatico='RECHAZADO' And FechaFacturar  Between '" & txtFechaDesde.GetValueString & "' And '" & txtFechaHasta.GetValueString & "'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = False
                btnAprobar.Text = "Recuperar"
        End Select


        Select Case cbxOrden.SelectedIndex
            Case 0
                OrderBy = " Order By Sucursal,Cliente"
            Case 1
                OrderBy = " Order By Sucursal,ZonaVenta"
            Case 2
                OrderBy = " Order By Sucursal,Vendedor"
            Case 3
                OrderBy = " Order By Sucursal,FormaPagoFactura"
        End Select

        Listar(SQL & OrderBy)

    End Sub

    Sub Listar(ByVal SQL As String)

        CSistema.SqlToLv(lvPedidos, SQL)

        'Seleccionar todos
        lvPedidos.Columns(8).TextAlign = HorizontalAlignment.Right

        For Each item As ListViewItem In lvPedidos.Items
            'item.Checked = True
            item.Checked = CBool(item.SubItems(11).Text)
            item.SubItems(8).Text = CSistema.FormatoMoneda(item.SubItems(8).Text)
            If item.Checked = True Then
                item.SubItems(10).Text = "Aprobado"
            End If
        Next

        lvPedidos.Columns(9).Width = 0


        txtCantidadPedidos.txt.Text = lvPedidos.Items.Count

    End Sub

    Sub Procesar()
        'verifica si hay alguno checkeado
        If lvPedidos.CheckedItems.Count = 0 Then
            'Exit Sub
        End If

        If vProcesar = True Then
            Exit Sub
        End If

        vProcesar = True
        tProcesar = New Thread(AddressOf FacturarPedido)
        tProcesar.Start()

    End Sub

    Sub Detener()
        Try
            If vProcesar = False Then
                Exit Sub
            End If

            tProcesar.Abort()
            vProcesar = False
        Catch ex As Exception

        End Try

        Thread.Sleep(1000)

    End Sub

    Sub AprobarRechazarPedido(ByVal Operacion As ENUMOperacionPedido)

        lvGrupo.Enabled = False
        lvPedidos.Enabled = False

        'Si no esta seleccionado ningun grupo, seleccionamos el primero
        If lvGrupo.Items.Count = 0 Then
            GoTo terminar
        End If

        If lvGrupo.SelectedItems.Count = 0 Then
            'GoTo terminar
        End If

        For Each item As ListViewItem In lvPedidos.Items
            'For Each item As ListViewItem In lvPedidos.CheckedItems
            If item.Checked = False Then
                GoTo siguiente
            End If

            If item.SubItems(10).Text.ToUpper <> "PENDIENTE" Then
                'GoTo siguiente
            End If

            item.ImageIndex = 1

            'Dim ID As Integer = item.Text
            Dim IDTransaccion As Integer = item.SubItems(12).Text
            'Dim Comprobante As String = item.SubItems(2).Text
            Dim frm As New frmVentaPedido

            'Facturar(frm, ID, Comprobante)

            'Actualizar el item
            Select Case Operacion
                Case ENUMOperacionPedido.APROBAR
                    Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionCancelarAutomatico @IDTransaccionPedido = " & IDTransaccion & ", @valor='APROBAR', @IDUsuario = " & vgIDUsuario & "").Copy
                Case ENUMOperacionPedido.RECHAZAR
                    Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionCancelarAutomatico @IDTransaccionPedido = " & IDTransaccion & ", @valor='RECHAZAR', @IDUsuario = " & vgIDUsuario & "").Copy
                Case ENUMOperacionPedido.RECUPERAR
                    Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionCancelarAutomatico @IDTransaccionPedido = " & IDTransaccion & ", @valor='RECUPERAR', @IDUsuario = " & vgIDUsuario & "").Copy
            End Select

            'If item.Checked = True Then
            '    item.SubItems(12).Text = "True"
            'End If

            'If item.Checked = False Then
            '    item.SubItems(12).Text = "False"
            'End If

siguiente:

        Next

terminar:
        ListarPedidos()
        lvGrupo.Enabled = True
        lvPedidos.Enabled = True
        vProcesar = False

    End Sub


    Sub FacturarPedido()

        lvGrupo.Enabled = False
        lvPedidos.Enabled = False

        'Si no esta seleccionado ningun grupo, seleccionamos el primero
        If lvGrupo.Items.Count = 0 Then
            GoTo terminar
        End If

        If lvGrupo.SelectedItems.Count = 0 Then
            'GoTo terminar
        End If

        For Each item As ListViewItem In lvPedidos.Items
            'For Each item As ListViewItem In lvPedidos.CheckedItems

            If vProcesar = False Then
                GoTo terminar
            End If

            If item.Checked = False Then
                'GoTo siguiente
            End If

            If item.SubItems(10).Text.ToUpper <> "PENDIENTE" Then
                'GoTo siguiente
            End If

            item.ImageIndex = 1

            Dim ID As Integer = item.Text
            Dim IDTransaccion As Integer = item.SubItems(12).Text
            Dim Comprobante As String = item.SubItems(2).Text
            Dim frm As New frmVentaPedido

            'Facturar(frm, ID, Comprobante)

            'Actualizar el item
            Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAprobarPedido @IDTransaccionPedido = " & IDTransaccion & ", @valor=" & item.Checked & ",@IDUsuario = " & vgIDUsuario & "").Copy

            'Dim oRow As DataRow = dttemp.Rows(0)

            'If oRow("Procesado") = False Then
            'item.SubItems(1).Text = oRow("Comprobante")
            'item.SubItems(2).Text = oRow("Mensaje")
            'lvPedidos.Columns(1).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            'lvPedidos.Columns(2).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            'item.SubItems(10).Text = "Error"
            'item.ImageIndex = 0
            'Else
            'item.SubItems(1).Text = oRow("Comprobante")
            'item.SubItems(2).Text = oRow("Mensaje")
            'lvPedidos.Columns(1).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            'lvPedidos.Columns(2).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)

            If item.Checked = True Then
                item.SubItems(10).Text = "Aprobado"
                item.SubItems(11).Text = "True"
            End If

            If item.Checked = False Then
                item.SubItems(10).Text = "Pendiente"
                item.SubItems(11).Text = "False"
            End If

            'item.ImageIndex = 2
            'End If

            'Mostrar mensaje si es que se quiere continuar
            'If item.Index < lvPedidos.Items.Count - 1 Then
            'Dim frmDialog As New frmDialog
            'frmDialog.Mensaje = "Desea continuar facturando?"
            'frmDialog.Titulo = "Pedido"

            'If frmDialog.ShowDialog(Me) = Windows.Forms.DialogResult.No Then
            'GoTo terminar
            'End If
            'End If

siguiente:

        Next

terminar:

        lvGrupo.Enabled = True
        lvPedidos.Enabled = True
        vProcesar = False

    End Sub

    Sub FacturarPedido2()

        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        For Each item As ListViewItem In lvPedidos.SelectedItems

            If item.SubItems(10).Text.ToUpper <> "PENDIENTE" Then
                Exit For
            End If

            item.ImageIndex = 1

            Dim ID As Integer = item.Text
            Dim IDTransaccion As Integer = item.SubItems(12).Text
            Dim Comprobante As String = item.SubItems(1).Text
            Dim frm As New frmVentaPedido

            'Facturar(frm, ID, Comprobante)

            'Actualizar el item
            Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAprobarPedido @IDTransaccionPedido = " & IDTransaccion & ", @valor=" & item.Checked & ",@IDUsuario = " & vgIDUsuario & "").Copy
            'Dim oRow As DataRow = dttemp.Rows(0)

            If item.Checked = True Then
                item.SubItems(10).Text = "Aprobado"
            End If

            If item.Checked = False Then
                item.SubItems(10).Text = "Pendiente"
            End If

        Next
        ListarPedidos()
    End Sub

    Sub Facturar(ByVal frm As frmVentaPedido, ByVal ID As Integer, ByVal Comprobante As String)

        frm.IDPedido = ID
        frm.Comprobante = Comprobante
        frm.Inicializar()
        frm.Nuevo()
        frm.EsPedido = True
        frm.CargarPedido(vgIDSucursal)
        FGMostrarFormulario(Me, frm, "Venta de Pedido Nro: " & ID, Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Sub SeleccionarTodo()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = True
        Next
    End Sub

    Sub QuitarSeleccion()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = False
        Next
    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        Select Case e.KeyCode
            Case Keys.Enter
                CSistema.SelectNextControl(Me, e.KeyCode)
        End Select

    End Sub

    Sub EliminarPedido()

        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        lvPedidos.SelectedItems(0).Remove()

    End Sub

    Sub Anular()

        If lvPedidos.SelectedItems.Count = 0 Then
            MessageBox.Show("Seleccione un registro para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Dim IDOperacion As Integer = CSistema.ObtenerIDOperacion(frmPedido.Name, "PEDIDO", "PED")
        Dim Numero As Integer = lvPedidos.SelectedItems(0).SubItems(0).Text
        Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select Top(1) IDTransaccion From Pedido Where Numero=" & Numero & " And IDSucursal=" & vgIDSucursal)

        'Verificar que el documento no esta ya anulado
        Dim Anulado As Boolean
        Anulado = CType(CSistema.ExecuteScalar("Select IsNull((Select Anulado From Pedido Where IDTransaccion=" & IDTransaccion & "), 'False')"), Boolean)
        If Anulado = True Then
            MessageBox.Show("El documento ya esta anulado!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        'Consultar
        If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        'Anulamos
        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", ERP.CSistema.NUMOperacionesRegistro.ANULAR.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Anular Registro
        If CSistema.ExecuteStoreProcedure(param, "SpPedido", False, False, MensajeRetorno) = False Then
            MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        ListarPedidos()

    End Sub

    Private Sub frmAprobarPedido_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Detener()
        GuardarInformacion()
    End Sub

    Private Sub frmAprobarPedido_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        ManejarTecla(e)
    End Sub

    Private Sub frmAprobarPedido_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnActualizarGrupo_Click(sender As System.Object, e As System.EventArgs)
        ListarGrupos()
    End Sub

    Private Sub cbxGrupos_SelectedIndexChanged(sender As System.Object, e As System.EventArgs)
        ListarGrupos()
    End Sub

    Private Sub lvGrupo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lvGrupo.SelectedIndexChanged
        SeleccionarGrupo()
    End Sub

    Private Sub btnProcesar_Click(sender As System.Object, e As System.EventArgs) Handles btnAprobar.Click
        '        Procesar()
        Select Case lvGrupo.SelectedItems.Item(0).Text
            Case "RECHAZADOS"
                AprobarRechazarPedido(ENUMOperacionPedido.RECUPERAR)
            Case "APROBADOS"
                AprobarRechazarPedido(ENUMOperacionPedido.RECUPERAR)
            Case Else
                AprobarRechazarPedido(ENUMOperacionPedido.APROBAR)
        End Select
    End Sub

    Private Sub btnSeleccionarTodos_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionarTodos.Click
        SeleccionarTodo()
    End Sub

    Private Sub ToolStripButton1_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton1.Click
        QuitarSeleccion()
    End Sub

    Private Sub FacturarToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles FacturarToolStripMenuItem.Click
        FacturarPedido2()
    End Sub

    Private Sub btnActualizarPedidos_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarPedidos.Click
        ListarPedidos()
    End Sub

    Private Sub EliminarDeLaListaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles EliminarDeLaListaToolStripMenuItem.Click
        EliminarPedido()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub AnularRegistroToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AnularRegistroToolStripMenuItem.Click
        Anular()
    End Sub

    Private Sub btnRechazar_Click(sender As Object, e As EventArgs) Handles btnRechazar.Click
        AprobarRechazarPedido(ENUMOperacionPedido.RECHAZAR)
    End Sub

    '10-06-2021 - SC - Actualiza datos
    Sub frmAprobarPedido_Activate()
        Me.Refresh()
    End Sub

End Class
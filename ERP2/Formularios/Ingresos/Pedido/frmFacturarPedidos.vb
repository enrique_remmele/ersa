﻿Imports System.Threading
Imports System.Web.Services.Description
Imports DevExpress.CodeParser

Public Class frmFacturarPedidos

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim tProcesar As Thread
    Dim vProcesar As Boolean = False
    Dim vFacturaAsistida As Boolean = False
    Public FPMensajeError As String = ""
    Public FrmMensajeError As String = ""

    'FUNCIONES
    Sub Inicializar()

        'vFacturaAsistida = False
        Me.KeyPreview = True

        'Varios
        CheckForIllegalCrossThreadCalls = False

        cbxGrupos.SelectedIndex = 0
        cbxOrden.SelectedIndex = 4

        'Funciones
        ListarGrupos()

        'Fecha facturacion
        If IsDate(VGFechaFacturacion) = False Then
            VGFechaFacturacion = VGFechaHoraSistema
        End If

        Dim Fecha As Date = VGFechaFacturacion

        txtFechaDesde.SetValue(Fecha)
        txtFechaHasta.SetValue(Fecha)

        lblFecha.Text = " * Fecha de Facturacion: " & Fecha.ToShortDateString

        Dim f As Font = New Font(lblFecha.Font.FontFamily, 12, FontStyle.Bold, lblFecha.Font.Unit)
        lblFecha.Font = f
        lblFecha.ForeColor = Color.Maroon


    End Sub

    Sub CargarInformacion()

    End Sub

    Sub GuardarInformacion()

    End Sub

    Sub ListarGrupos()


        vgConfiguraciones = CSistema.ExecuteToDataTable("Select TOP 1 * From Configuraciones Where IDSucursal=" & vgIDSucursal, "", 15)(0)
        VGFechaFacturacion = CSistema.ExecuteScalar("Select TOP 1 Fecha From FechaFacturacion Where IDSucursal=" & vgIDSucursal)
        If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        End If
        lblFecha.Text = " * Fecha de Facturacion: " & VGFechaFacturacion.ToShortDateString
        lvGrupo.Items.Clear()
        lvPedidos.Items.Clear()

        Dim SQL As String = ""
        Dim INDEX As Integer = cbxGrupos.SelectedIndex

        vgConfiguraciones = CSistema.ExecuteToDataTable("Select TOP 1 * From Configuraciones Where IDSucursal=" & vgIDSucursal, "", 15)(0)
        VGFechaFacturacion = CSistema.ExecuteScalar("Select TOP 1 Fecha From FechaFacturacion Where IDSucursal=" & vgIDSucursal)

        If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        End If


        If INDEX = 0 Then
            SQL = ""
        End If

        If INDEX = 1 Then
            SQL = "Select 'ID'=IDZonaVenta, 'Descripcion'=ZonaVenta, 'Cantidad'=COUNT(*) From VPedidosAFacturar Where IDSucursal=" & vgIDSucursal & " And FechaFacturar between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "' Group By IDZonaVenta, ZonaVenta Order By 1"
        End If

        If INDEX = 2 Then
            SQL = "Select 'ID'=IDVendedor, 'Descripcion'=Vendedor, 'Cantidad'=COUNT(*) From VPedidosAFacturar Where IDSucursal=" & vgIDSucursal & " And FechaFacturar between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "' Group By IDVendedor, Vendedor Order By 1"
        End If
        If SQL = "" Then
            lvGrupo.View = View.Details
            lvGrupo.Columns.Add("Listado", "Listado")
            lvGrupo.Items.Add("Todo")
            lvGrupo.Items.Add("Aprobados-Anulados")
            lvGrupo.Columns("Listado").AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
        Else
            CSistema.SqlToLv(lvGrupo, SQL)
        End If


    End Sub

    Sub SeleccionarGrupo()

        ListarPedidos()

    End Sub

    Sub ListarPedidos()


        vgConfiguraciones = CSistema.ExecuteToDataTable("Select TOP 1 * From Configuraciones Where IDSucursal=" & vgIDSucursal, "", 15)(0)



        VGFechaFacturacion = CSistema.ExecuteScalar("Select TOP 1 Fecha From FechaFacturacion Where IDSucursal=" & vgIDSucursal)


        If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        End If
        lblFecha.Text = " * Fecha de Facturacion: " & VGFechaFacturacion.ToShortDateString

        If lvGrupo.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim ID As String = lvGrupo.SelectedItems(0).Text
        Dim OrderBy As String = ""
        Dim Campo As String = ""


        If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        End If

        Select Case cbxGrupos.SelectedIndex

            Case 0
                If ID = "Todo" Then
                    'SQL = "Select 'Pedido'=Numero, Comprobante, 'Venta'='', 'Fec'=Facturar, Cliente, 'Entrega'=Direccion, 'Zona'=ZonaVenta, Vendedor, Total, Observacion, Estado,IDTransaccion, 'Fecha Carga'=Fecha, 'Condicion'= (Case when Credito=1 Then 'Credito' else 'Contado' end),'FormaPago'=FormaPagoFactura  From VPedido Where AnuladoAprobado = 'False' and Facturado='False' And Anulado = 'False' and Aprobado = 'True' And IDSucursal=" & vgIDSucursal & "  And FechaFacturar  Between '" & txtFechaDesde.GetValueString & "' And '" & txtFechaHasta.GetValueString & "'"
                    SQL = "Select 'Pedido'=Numero, Comprobante, 'Venta'='', 'Fec'=Facturar, Cliente, 'Entrega'=Direccion, 'Zona'=ZonaVenta, Vendedor, Total, Observacion, Estado,IDTransaccion, 'Fecha Carga'=Fecha From VPedido Where AnuladoAprobado = 'False' and Facturado='False' And Anulado = 'False' and Aprobado = 'True' And IDSucursal=" & vgIDSucursal & "  And FechaFacturar  Between '" & txtFechaDesde.GetValueString & "' And '" & txtFechaHasta.GetValueString & "'"
                End If
                If ID = "Aprobados-Anulados" Then
                    SQL = "Select 'Pedido'=Numero, Comprobante, 'Venta'='', 'Fec'=Facturar, Cliente, 'Entrega'=Direccion, 'Zona'=ZonaVenta, Vendedor, Total, Observacion, Estado,IDTransaccion, 'Fecha Carga'=Fecha From VPedido Where AnuladoAprobado = 'True' and Facturado='False' And Anulado = 'False' and Aprobado = 'True' And IDSucursal=" & vgIDSucursal & "  And FechaFacturar  Between '" & txtFechaDesde.GetValueString & "' And '" & txtFechaHasta.GetValueString & "'"
                End If
            Case 1
                If IsNumeric(ID) = False Then
                    Exit Sub
                End If
                Campo = "IDZonaVenta"
                SQL = "Select 'Pedido'=Numero, Comprobante, 'Venta'='', 'Fec'=Facturar, Cliente, 'Entrega'=Direccion, 'Zona'=ZonaVenta, Vendedor, Total, Observacion, Estado,IDTransaccion,'Fecha Carga'=Fecha From VPedido Where Facturado='False' And Anulado = 'False' And Aprobado = 'True' and IDSucursal=" & vgIDSucursal & "  And FechaFacturar  Between '" & txtFechaDesde.GetValueString & "' And '" & txtFechaHasta.GetValueString & "' And " & Campo & "=" & ID & " "
            Case 2
                If IsNumeric(ID) = False Then
                    Exit Sub
                End If
                Campo = "IDVendedor"
                SQL = "Select 'Pedido'=Numero, Comprobante, 'Venta'='', 'Fec'=Facturar, Cliente, 'Entrega'=Direccion, 'Zona'=ZonaVenta, Vendedor, Total, Observacion, Estado,IDTransaccion,'Fecha Carga'=Fecha From VPedido Where Facturado='False' And Anulado = 'False'And Aprobado = 'True' and IDSucursal=" & vgIDSucursal & "  And FechaFacturar   Between '" & txtFechaDesde.GetValueString & "' And '" & txtFechaHasta.GetValueString & "' And " & Campo & "=" & ID & " "
        End Select

        Select Case cbxOrden.SelectedIndex
            Case 0
                OrderBy = " Order By Cliente"
            Case 1
                OrderBy = " Order By ZonaVenta"
            Case 2
                OrderBy = " Order By Vendedor"
            Case 3
                OrderBy = " Order By Numero"
            Case 4
                OrderBy = " Order By Fecha asc"
        End Select

        Listar(SQL & OrderBy)

    End Sub

    Sub Listar(ByVal SQL As String)

        CSistema.SqlToLv(lvPedidos, SQL)

        'Seleccionar todos
        lvPedidos.Columns(8).TextAlign = HorizontalAlignment.Right

        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = True
            item.SubItems(8).Text = CSistema.FormatoMoneda(item.SubItems(8).Text)
        Next

        lvPedidos.Columns(11).Width = 0

        txtCantidadPedidos.txt.Text = lvPedidos.Items.Count
        lblFecha.Text = " * Fecha de Facturacion: " & VGFechaFacturacion.ToShortDateString


    End Sub

    Sub Procesar()

        If lvPedidos.CheckedItems.Count = 0 Then
            Exit Sub
        End If

        If vProcesar = True Then
            Exit Sub
        End If

        vProcesar = True
        tProcesar = New Thread(AddressOf FacturarPedido)
        tProcesar.Start()


    End Sub

    Sub Detener()
        Try
            If vProcesar = False Then
                Exit Sub
            End If

            tProcesar.Abort()
            vProcesar = False
        Catch ex As Exception

        End Try

        Thread.Sleep(1000)

    End Sub

    Sub FacturarPedido()


        lvGrupo.Enabled = False
        lvPedidos.Enabled = False
        FPMensajeError = ""
        FrmMensajeError = ""
        'Si no esta seleccionado ningun grupo, seleccionamos el primero
        If lvGrupo.Items.Count = 0 Then
            GoTo terminar
        End If

        If lvGrupo.SelectedItems.Count = 0 Then
            GoTo terminar
        End If

        For Each item As ListViewItem In lvPedidos.CheckedItems
            FrmMensajeError = ""
            FPMensajeError = ""


            If vProcesar = False Then
                GoTo terminar
            End If

            If item.Checked = False Then
                GoTo siguiente
            End If

            If item.SubItems(10).Text.ToUpper <> "PENDIENTE" Then
                GoTo siguiente
            End If

            item.ImageIndex = 1

            Dim ID As Integer = item.Text
            Dim IDTransaccion As Integer = item.SubItems(11).Text
            Dim Comprobante As String = item.SubItems(2).Text
            Dim frm As New frmVentaPedido

            'Dim vAnulado As Boolean = CSistema.RetornarValorBoolean(CData.GetRow(" IDTransaccion = " & IDTransaccion, "vPedido")("Anulado").ToString)
            Dim vAnulado As Boolean = CType(CSistema.ExecuteScalar("Select IsNull((Select Anulado From vPedido Where IDTransaccion=" & IDTransaccion & "), 'False')"), Boolean)
            If vAnulado = True Then
                FPMensajeError = "El pedido que desea facturar fue anulado"
                If vFacturaAsistida = False Then
                    MessageBox.Show("El pedido que desea facturar fue anulado!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                End If

                GoTo siguiente
            End If

            'If CBool(CSistema.ExecuteScalar("Select count(*) from DetalleNotaCreditoDiferenciaPrecio where IDTransaccionPedidoVenta = " & IDTransaccion & " and IDTransaccionPedidoNotaCredito = 0 ")) Then
            '    MessageBox.Show("Existe un pedido de nota de credito pendiente de liberar en el panel de Cast. Liberelo antes de continuar.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            '    GoTo siguiente
            'End If

            If CBool(CSistema.ExecuteScalar("Exec SpPedidoVerificarPedidoNCPendiente @IDTransaccion = " & IDTransaccion)) Then
                FPMensajeError = "Existe un pedido de nota de credito pendiente de liberar en el panel de Cast"
                If vFacturaAsistida = False Then
                    MessageBox.Show("Existe un pedido de nota de credito pendiente de liberar en el panel de Cast. Liberelo antes de continuar.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    GoTo siguiente
                End If

            End If

            Facturar(frm, ID, Comprobante, IDTransaccion)
            'FPMensajeError = frm.mensajeError
            If frm.mensajeError IsNot Nothing Then
                FrmMensajeError = frm.mensajeError
            End If



            'Actualizar el item

            Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpVentaEstado @IDTransaccionVenta = " & frm.IDTransaccion & ", @IDPedido=" & ID & ", @IDSucursal=" & vgIDSucursal & " ").Copy

            Dim oRow As DataRow = dttemp.Rows(0)

            If oRow("Procesado") = False Then
                item.SubItems(1).Text = oRow("Comprobante")
                item.SubItems(2).Text = oRow("Mensaje") + "  " + FPMensajeError + " " + FrmMensajeError
                lvPedidos.Columns(1).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                lvPedidos.Columns(2).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                item.SubItems(10).Text = "Error"
                item.ImageIndex = 0
            Else
                item.SubItems(1).Text = oRow("Comprobante")
                item.SubItems(2).Text = oRow("Mensaje")
                lvPedidos.Columns(1).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                lvPedidos.Columns(2).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                item.SubItems(10).Text = "Procesado"
                item.ImageIndex = 2
            End If

            If vFacturaAsistida = False Then
                'Mostrar mensaje si es que se quiere continuar
                If item.Index < lvPedidos.Items.Count - 1 Then
                    Dim frmDialog As New frmDialog
                    frmDialog.Mensaje = "Desea continuar facturando?"
                    frmDialog.Titulo = "Pedido"

                    If frmDialog.ShowDialog(Me) = Windows.Forms.DialogResult.No Then
                        GoTo terminar
                    End If
                End If
            End If

            If vFacturaAsistida = True Then
                BtnFactAsistida.BackColor = Color.DarkOrange
                BtnFactAsistida.ForeColor = Color.White
                Thread.Sleep(2000)
                BtnFactAsistida.BackColor = Color.DarkGreen
                BtnFactAsistida.ForeColor = Color.White
                If vFacturaAsistida = False Then
                    BtnFactAsistida.BackColor = Color.Gray
                    BtnFactAsistida.ForeColor = Color.White
                    GoTo terminar
                End If
            End If


siguiente:

        Next
        TmFacturacionAsistida.Enabled = True
terminar:

        lvGrupo.Enabled = True
        lvPedidos.Enabled = True
        vProcesar = False

    End Sub

    Sub FacturarPedido2()

        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If
        TmFacturacionAsistida.Enabled = False
        lbFacturacionAsistida.Text = "Temporizador INACTIVO"
        For Each item As ListViewItem In lvPedidos.SelectedItems

            If item.SubItems(10).Text.ToUpper <> "PENDIENTE" Then
                Exit For
            End If

            item.ImageIndex = 1

            Dim ID As Integer = item.Text
            Dim IDTransaccion As Integer = item.SubItems(11).Text
            Dim Comprobante As String = item.SubItems(1).Text
            Dim vAutorizado As Boolean = False
            vAutorizado = CType(CSistema.ExecuteScalar("Select IsNull((Select AutorizacionLineaCredito From Pedido Where IDTransaccion=" & IDTransaccion & "), 'False')"), Boolean)

            If CBool(CSistema.ExecuteScalar("Select count(*) from DetalleNotaCreditoDiferenciaPrecio where IDTransaccionPedidoVenta = " & IDTransaccion & " and IDTransaccionPedidoNotaCredito = 0 ")) Then
                MessageBox.Show("Existe un pedido de nota de credito pendiente de liberar en el panel de Cast. Liberelo antes de continuar.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                GoTo siguiente
            End If

            Dim frm As New frmVentaPedido
            frm.ExcesoLineaCreditoAutorizado = vAutorizado
            'Dim vAnulado As Boolean = CSistema.RetornarValorBoolean(CData.GetRow(" IDTransaccion = " & IDTransaccion, "vPedido")("Anulado").ToString)
            Dim vAnulado As Boolean = CType(CSistema.ExecuteScalar("Select IsNull((Select Anulado From vPedido Where IDTransaccion=" & IDTransaccion & "), 'False')"), Boolean)
            If vAnulado = False Then
                Facturar(frm, ID, Comprobante, IDTransaccion)

                'Actualizar el item
                Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpVentaEstado @IDTransaccionVenta = " & frm.NuevoIDTransaccion & ", @IDPedido=" & ID & ", @IDSucursal=" & vgIDSucursal & " ").Copy
                Dim oRow As DataRow = dttemp.Rows(0)

                If oRow("Procesado") = False Then
                    item.SubItems(1).Text = oRow("Comprobante")
                    item.SubItems(2).Text = oRow("Mensaje")
                    lvPedidos.Columns(1).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                    lvPedidos.Columns(2).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                    item.SubItems(10).Text = "Error"
                    item.ImageIndex = 0
                Else
                    item.SubItems(1).Text = oRow("Comprobante")
                    item.SubItems(2).Text = oRow("Mensaje")
                    lvPedidos.Columns(1).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                    lvPedidos.Columns(2).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                    item.SubItems(10).Text = "Procesado"
                    item.ImageIndex = 2
                End If
            Else
                MessageBox.Show("El pedido que desea facturar fue anulado!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
siguiente:

        Next
        TmFacturacionAsistida.Enabled = True
        lbFacturacionAsistida.Text = "Temporizador ACTIVO"
    End Sub

    Sub Facturar(ByVal frm As frmVentaPedido, ByVal ID As Integer, ByVal Comprobante As String, IDTransaccionPedido As Integer)
        Dim vAutorizado As Boolean = False

        ' falta condicion de compra credito?
        vAutorizado = CType(CSistema.ExecuteScalar("Select IsNull((Select AutorizacionLineaCredito From Pedido Where IDTransaccion=" & IDTransaccionPedido & "), 'False')"), Boolean)
        'vAutorizado = CType(CSistema.ExecuteScalar("Select IsNull((VPedidoExcesoLineaCreditoAlt From Pedido Where IDTransaccion=" & IDTransaccionPedido & "), 'False')"), Boolean)

        If vAutorizado = False Then
            'If CSistema.ExecuteScalar("Select count(*) from VPedidoExcesoLineaCreditoAlt where IDTransaccion = " & IDTransaccionPedido) > 0 Then
            'Dim Mensaje As String = CSistema.ExecuteScalar("Select AprobarPor from VPedidoExcesoLineaCreditoAlt where IDTransaccion = " & IDTransaccionPedido)
            Dim Mensaje As String = CSistema.ExecuteScalar("Select AprobarPor from VPedidoExcesoLineaCredito where IDTransaccion = " & IDTransaccionPedido)
            'Dim Mensaje As String
            'Mensaje = "El cliente tiene facturas pendientes"
            If Mensaje <> "" Then
                Select Case Mensaje
                    Case "CHEQUE RECHAZADO"
                        Mensaje = "El cliente tiene un CHEQUE RECHAZADO."
                    Case "DOBLE CONTADO"
                        Mensaje = "El cliente tiene una FACTURA CONTADO PENDIENTE DE PAGO DE DIAS ANTERIORES."
                    Case "EXCESO LINEA DE CREDITO"
                        Mensaje = "Se sobrepaso la LINEA DE CREDITO de este cliente."
                    Case "FACTURA ATRASADA"
                        Mensaje = "El cliente posee FACTURAS ATRASADAS."
                End Select
                FPMensajeError = Mensaje

                If vFacturaAsistida = False Then
                    MessageBox.Show(Mensaje, "Favor comunicarse con Cobranzas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If

                frm.NuevoIDTransaccion = 0
                Exit Sub
            End If

        End If

        frm.IDPedido = ID
        frm.Comprobante = Comprobante
        frm.IDTransaccionPedido = IDTransaccionPedido
        frm.ExcesoLineaCreditoAutorizado = vAutorizado
        frm.pFacturacionAsistida = vFacturaAsistida


        frm.Inicializar()

        frm.Nuevo()

        frm.EsPedido = True

        frm.CargarPedido(vgIDSucursal)

        FGMostrarFormulario(Me, frm, "Venta de Pedido Nro: " & ID, Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)


    End Sub

    Sub SeleccionarTodo()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = True
        Next
    End Sub

    Sub QuitarSeleccion()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = False
        Next
    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        Select Case e.KeyCode
            Case Keys.Enter
                CSistema.SelectNextControl(Me, e.KeyCode)
        End Select

    End Sub

    Sub EliminarPedido()

        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        lvPedidos.SelectedItems(0).Remove()

    End Sub

    Sub Anular()

        If lvPedidos.SelectedItems.Count = 0 Then
            MessageBox.Show("Seleccione un registro para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Dim IDOperacion As Integer = CSistema.ObtenerIDOperacion(frmPedido.Name, "PEDIDO", "PED")
        Dim Numero As Integer = lvPedidos.SelectedItems(0).SubItems(0).Text
        Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select Top(1) IDTransaccion From Pedido Where Numero=" & Numero & " And IDSucursal=" & vgIDSucursal)

        'Verificar que el documento no esta ya anulado
        Dim Anulado As Boolean
        Anulado = CType(CSistema.ExecuteScalar("Select IsNull((Select Anulado From Pedido Where IDTransaccion=" & IDTransaccion & "), 'False')"), Boolean)
        If Anulado = True Then
            MessageBox.Show("El documento ya esta anulado!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        'Consultar
        If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        'Anulamos
        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", ERP.CSistema.NUMOperacionesRegistro.ANULAR.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Anular Registro
        If CSistema.ExecuteStoreProcedure(param, "SpPedido", False, False, MensajeRetorno) = False Then
            MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        ListarPedidos()

    End Sub

    Private Sub frmFacturarPedidos_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Detener()
        GuardarInformacion()
        'FA 20/06/2023
        LiberarMemoria()
    End Sub

    Private Sub frmFacturarPedidos_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        ManejarTecla(e)

    End Sub

    Private Sub frmFacturarPedidos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnActualizarGrupo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizarGrupo.Click
        ListarGrupos()
    End Sub

    Private Sub cbxGrupos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxGrupos.SelectedIndexChanged
        ListarGrupos()
    End Sub

    Private Sub lvGrupo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvGrupo.SelectedIndexChanged
        SeleccionarGrupo()
    End Sub

    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

    Private Sub btnDetener_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnSeleccionarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionarTodos.Click
        SeleccionarTodo()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        QuitarSeleccion()
    End Sub

    Private Sub FacturarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FacturarToolStripMenuItem.Click
        FacturarPedido2()
    End Sub

    Private Sub btnActualizarPedidos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizarPedidos.Click
        ListarPedidos()
    End Sub

    Private Sub EliminarDeLaListaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarDeLaListaToolStripMenuItem.Click
        EliminarPedido()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub AnularRegistroToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AnularRegistroToolStripMenuItem.Click
        Anular()
    End Sub

    Private Sub lvPedidos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvPedidos.SelectedIndexChanged

    End Sub

    '10-06-2021 - SC - Actualiza datos
    Sub frmFacturarPedidos_Activate()
        If TmFacturacionAsistida.Enabled = True Then
            lbFacturacionAsistida.Text = "Temporizador ACTIVO"
        Else
            lbFacturacionAsistida.Text = "Temporizador INACTIVO"
        End If
        Me.Refresh()
    End Sub

    Private Sub BtnFactAsistida_Click(sender As Object, e As EventArgs) Handles BtnFactAsistida.Click
        If vFacturaAsistida = False Then
            vFacturaAsistida = True
            BtnFactAsistida.BackColor = Color.DarkGreen
            BtnFactAsistida.ForeColor = Color.White
            TmFacturacionAsistida.Enabled = True

        Else
            vFacturaAsistida = False
            BtnFactAsistida.BackColor = Color.Gray
            BtnFactAsistida.ForeColor = Color.White
            TmFacturacionAsistida.Enabled = False
        End If

    End Sub

    Private Sub procesarTimerClick(sender As Object, e As EventArgs) Handles TmFacturacionAsistida.Tick
        If vFacturaAsistida = True Then

            'MessageBox.Show("temporizador", "Atencion", MessageBoxButtons.OK)
            TmFacturacionAsistida.Enabled = False
            lbFacturacionAsistida.Text = "Temporizador INACTIVO"
            ListarPedidos()
            SeleccionarTodo()
            Procesar()
            TmFacturacionAsistida.Enabled = True
            lbFacturacionAsistida.Text = "Temporizador ACTIVO"
        End If

    End Sub
End Class
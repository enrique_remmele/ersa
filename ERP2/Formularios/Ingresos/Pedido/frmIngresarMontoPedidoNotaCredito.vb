﻿Public Class frmIngresarMontoPedidoNotaCredito
    Public Property ImportePedido As Decimal
    Public Property Importe As Decimal
    Public Property Procesado As Boolean = False
    Public Property Decimales As Boolean = False


    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click, btnCancelar.Click
        Importe = txtImporte.ObtenerValor
        'If Importe >= ImportePedido Then
        '    MessageBox.Show("El importe debe ser menor que " & ImportePedido, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    Exit Sub
        'End If
        If Importe > ImportePedido Then
            MessageBox.Show("El importe debe ser menor o igual que " & ImportePedido, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        Procesado = True
        Me.Close()
    End Sub

    Private Sub frmIngresarMontoPedidoNotaCredito_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtImporte.Decimales = Decimales
        txtImporte.Focus()
    End Sub

    Private Sub txtImporte_KeyUp(sender As Object, e As KeyEventArgs) Handles txtImporte.KeyUp
        If e.KeyCode = Keys.Enter Then
            Importe = txtImporte.ObtenerValor
            If Importe > ImportePedido Then
                MessageBox.Show("El importe debe ser menor o igual que " & ImportePedido, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
            Me.Close()
        End If
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmIngresarMontoPedidoNotaCredito_Activate()
        Me.Refresh()
    End Sub
End Class
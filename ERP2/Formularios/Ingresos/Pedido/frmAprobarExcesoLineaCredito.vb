﻿Imports System.Threading
Imports ERP.Reporte
Public Class frmAprobarExcesoLineaCredito
    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim PorcentajeAsignado As Decimal = 0
    Dim ImporteAsignado As Decimal = 0

    'PROPIEDADES

    'VARIABLES
    Dim tProcesar As Thread
    Dim vProcesar As Boolean = False

    Enum ENUMOperacionPedido
        APROBAR = 1
        RECHAZAR = 2
        RECUPERAR = 3
        ANULAR = 4
    End Enum


    'FUNCIONES
    Sub Inicializar()

        Me.KeyPreview = True

        'Varios
        CheckForIllegalCrossThreadCalls = False
        CargarInformacion()
        cbxGrupos.SelectedIndex = 0
        cbxOrden.SelectedIndex = 0

        'Funciones
        ListarGrupos()

        'Fecha facturacion
        If IsDate(VGFechaFacturacion) = False Then
            VGFechaFacturacion = VGFechaHoraSistema
        End If

        Dim Fecha As Date = VGFechaFacturacion

        txtFechaDesde.Hoy()
        txtFechaHasta.UnaSemana()

        lblFecha.Text = " * Fecha de Facturacion: " & Fecha.ToShortDateString

        Dim f As Font = New Font(lblFecha.Font.FontFamily, 12, FontStyle.Bold, lblFecha.Font.Unit)
        lblFecha.Font = f
        lblFecha.ForeColor = Color.Maroon
        CargarPorcentaje()
        ListarPedidos()
    End Sub

    Sub CargarInformacion()

        Dim dtOrden As DataTable = CSistema.ExecuteToDataTable("Select Top(0) * From VPedidoExcesoLineaCredito")

        For index = 1 To dtOrden.Columns.Count - 1
            cbxOrden.Items.Add(dtOrden.Columns(index).ColumnName)
        Next


    End Sub

    Sub GuardarInformacion()

    End Sub
    Sub ListarGrupos()

        lvGrupo.Items.Clear()
        lvPedidos.Items.Clear()

        Dim SQL As String = ""
        Dim INDEX As Integer = cbxGrupos.SelectedIndex

        If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        End If

        Dim dtGrupo As New DataTable("Grupos")

        dtGrupo.Columns.Add("Grupo", Type.GetType("System.String"))

        Dim newRow As DataRow = dtGrupo.NewRow()
        newRow(0) = "EXCESO DE LINEA"
        dtGrupo.Rows.Add(newRow)
        Dim newRowb As DataRow = dtGrupo.NewRow()
        newRowb(0) = "FACTURAS VENCIDAS"
        dtGrupo.Rows.Add(newRowb)
        Dim newRowc As DataRow = dtGrupo.NewRow()
        newRowc(0) = "CHEQUES RECHAZADOS"
        dtGrupo.Rows.Add(newRowc)
        Dim newRowd As DataRow = dtGrupo.NewRow()
        newRowd(0) = "DOBLE CONTADO"
        dtGrupo.Rows.Add(newRowd)
        Dim newRowx As DataRow = dtGrupo.NewRow()
        newRowx(0) = "APROBADOS"
        dtGrupo.Rows.Add(newRowx)
        Dim newRowz As DataRow = dtGrupo.NewRow()
        newRowz(0) = "RECHAZADOS"
        dtGrupo.Rows.Add(newRowz)

        CSistema.dtToLv(lvGrupo, dtGrupo)


        For i = 0 To lvGrupo.Columns.Count - 1
            lvGrupo.Columns(i).AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize)
        Next


     
    End Sub

    Sub SeleccionarGrupo()

        ListarPedidos()

    End Sub

    Sub ListarPedidos()

        If lvGrupo.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim SQL As String = ""
        'Dim ID As String = lvGrupo.SelectedItems(0).Text
        Dim OrderBy As String = ""
        Dim Campo As String = ""


        If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        End If
         Select lvGrupo.SelectedItems.Item(0).Text
            Case "EXCESO DE LINEA"
                SQL = "Select * From VPedidoExcesoLineaCredito Where Aprobado = 'PENDIENTE' and AprobarPor = 'EXCESO LINEA DE CREDITO' and cast(FechaFacturar as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = True
                btnAprobar.Text = "Aprobar"
                btnRechazar.Text = "Rechazar"
            Case "FACTURAS VENCIDAS"
                SQL = "Select * From VPedidoExcesoLineaCredito Where Aprobado = 'PENDIENTE' and AprobarPor = 'FACTURA ATRASADA' and cast(FechaFacturar as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = True
                btnAprobar.Text = "Aprobar"
                btnRechazar.Text = "Rechazar"
            Case "CHEQUES RECHAZADOS"
                SQL = "Select * From VPedidoExcesoLineaCredito Where Aprobado = 'PENDIENTE' and AprobarPor = 'CHEQUE RECHAZADO' and cast(FechaFacturar as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = True
                btnAprobar.Text = "Aprobar"
                btnRechazar.Text = "Rechazar"
            Case "DOBLE CONTADO"
                SQL = "Select * From VPedidoExcesoLineaCredito Where Aprobado = 'PENDIENTE' and AprobarPor = 'DOBLE CONTADO' and cast(FechaFacturar as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = True
                btnAprobar.Text = "Aprobar"
                btnRechazar.Text = "Rechazar"
            Case "APROBADOS"
                SQL = "Select * From VPedidoExcesoLineaCredito Where Aprobado = 'APROBADO' and cast(FechaFacturar as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = False
                btnAprobar.Text = "Recuperar"
            Case "RECHAZADOS"
                SQL = "Select * From VPedidoExcesoLineaCredito Where Aprobado = 'RECHAZADO' and cast(FechaFacturar as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = False
                btnAprobar.Text = "Recuperar"
        End Select

        Listar(SQL & " Order By [" & cbxOrden.Text & "]")

    End Sub

    Sub Listar(ByVal SQL As String)

        CSistema.SqlToLv(lvPedidos, SQL)

        If lvPedidos.Items.Count = 0 Then
            Exit Sub
        End If



        'Seleccionar todos
        lvPedidos.Columns(8).TextAlign = HorizontalAlignment.Right
        lvPedidos.Columns(9).TextAlign = HorizontalAlignment.Right
        lvPedidos.Columns(10).TextAlign = HorizontalAlignment.Right
        lvPedidos.Columns(6).TextAlign = HorizontalAlignment.Right


        For Each item As ListViewItem In lvPedidos.Items
            'item.Checked = True
            item.SubItems(6).Text = CSistema.FormatoMoneda(item.SubItems(6).Text)
            item.SubItems(8).Text = CSistema.FormatoMoneda(item.SubItems(8).Text)
            item.SubItems(9).Text = CSistema.FormatoMoneda(item.SubItems(9).Text)
            item.SubItems(10).Text = CSistema.FormatoMoneda(item.SubItems(10).Text, True)


            'If item.Checked = True Then
            '    item.SubItems(11).Text = "Aprobado"
            'End If
        Next

        lvPedidos.Columns(12).Width = 0
        lvPedidos.Columns(14).Width = 0

        txtCantidadPedidos.txt.Text = lvPedidos.Items.Count

    End Sub

    'Sub AprobarRechazarPedido(ByVal Operacion As ENUMOperacionPedido)
    '    'verifica si hay alguno checkeado
    '    If lvPedidos.CheckedItems.Count = 0 Then
    '        'Exit Sub
    '    End If

    '    If vProcesar = True Then
    '        Exit Sub
    '    End If

    '    vProcesar = True
    '    tProcesar = New Thread(AddressOf FacturarPedido)
    '    tProcesar.Start()

    'End Sub

    Sub Detener()
        Try
            If vProcesar = False Then
                Exit Sub
            End If

            tProcesar.Abort()
            vProcesar = False
        Catch ex As Exception

        End Try

        Thread.Sleep(1000)

    End Sub

    Sub AprobarRechazarPedido(ByVal Operacion As ENUMOperacionPedido)

        lvGrupo.Enabled = False
        lvPedidos.Enabled = False

        'Si no esta seleccionado ningun grupo, seleccionamos el primero
        If lvGrupo.Items.Count = 0 Then
            GoTo terminar
        End If

        If lvGrupo.SelectedItems.Count = 0 Then
            'GoTo terminar
        End If

        For Each item As ListViewItem In lvPedidos.Items
            'For Each item As ListViewItem In lvPedidos.CheckedItems
            If item.Checked = False Then
                GoTo siguiente
            End If

            If item.SubItems(10).Text.ToUpper <> "PENDIENTE" Then
                'GoTo siguiente
            End If

            item.ImageIndex = 1

            'Dim ID As Integer = item.Text
            Dim IDTransaccion As Integer = item.SubItems(13).Text
            'Dim Comprobante As String = item.SubItems(2).Text
            Dim frm As New frmVentaPedido

            'Facturar(frm, ID, Comprobante)

            'Actualizar el item
            If ControlarPorcentaje(item.SubItems(11).Text, item.SubItems(10).Text) = True Then
                Select Case Operacion
                    Case ENUMOperacionPedido.APROBAR
                        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionLineaCredito @IDTransaccionPedido = " & IDTransaccion & ", @valor='APROBAR', @IDUsuario = " & vgIDUsuario & "").Copy
                    Case ENUMOperacionPedido.RECHAZAR
                        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionLineaCredito @IDTransaccionPedido = " & IDTransaccion & ", @valor='RECHAZAR', @IDUsuario = " & vgIDUsuario & "").Copy
                    Case ENUMOperacionPedido.RECUPERAR
                        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionLineaCredito @IDTransaccionPedido = " & IDTransaccion & ", @valor='RECUPERAR', @IDUsuario = " & vgIDUsuario & "").Copy
                End Select

            End If

            'If item.Checked = True Then
            '    item.SubItems(12).Text = "True"
            'End If

            'If item.Checked = False Then
            '    item.SubItems(12).Text = "False"
            'End If

siguiente:

        Next

terminar:
        ListarPedidos()
        lvGrupo.Enabled = True
        lvPedidos.Enabled = True
        vProcesar = False

    End Sub

    'Sub FacturarPedido2()

    '    If lvPedidos.SelectedItems.Count = 0 Then
    '        Exit Sub
    '    End If

    '    For Each item As ListViewItem In lvPedidos.SelectedItems

    '        If item.SubItems(10).Text.ToUpper <> "PENDIENTE" Then
    '            Exit For
    '        End If

    '        item.ImageIndex = 1

    '        'Dim ID As Integer = item.Text
    '        Dim IDTransaccion As Integer = item.SubItems(12).Text
    '        'Dim Comprobante As String = item.SubItems(2).Text
    '        Dim frm As New frmVentaPedido

    '        'Facturar(frm, ID, Comprobante)

    '        'Actualizar el item
    '        If ControlarPorcentaje(item.SubItems(10).Text, item.SubItems(9).Text) = True Then
    '            Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionLineaCredito @IDTransaccionPedido = " & IDTransaccion & ", @valor=" & item.Checked & ",@IDUsuario = " & vgIDUsuario & "").Copy
    '        End If
    '        'Dim oRow As DataRow = dttemp.Rows(0)
    '        If item.Checked = True Then
    '            item.SubItems(11).Text = "Aprobado"
    '        End If

    '        If item.Checked = False Then
    '            item.SubItems(11).Text = "Pendiente"
    '        End If


    '    Next

    '    ListarPedidos()

    'End Sub

    Sub ListarInventario()
        Dim CReporte As New CReporteCuentasACobrar
        Dim frm As New frmReporte
        Dim Titulo As String = "Inventario de Clientes "
        Dim TipoInforme As String = "Cliente: "
        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""


        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        For Each item As ListViewItem In lvPedidos.SelectedItems
            item.ImageIndex = 1

            Dim IDCliente As Integer = item.SubItems(14).Text 'IDCliente
            Where = " Where anulado = 0 and IDCliente = " & IDCliente
            OrderBy = "Order by Cliente, IDSucursal, FechaVencimiento "
            Titulo = "INVENTARIO DE DOCUMENTOS PENDIENTES A COBRAR"
            CReporte.InventarioDocumentoPendiente(frm, Titulo, TipoInforme, Where, WhereDetalle, OrderBy, Top, 0, vgUsuarioIdentificador)

        Next
    End Sub

    Sub VerPedido()
        Dim frm As New frmPedido

        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        For Each item As ListViewItem In lvPedidos.SelectedItems
            item.ImageIndex = 1
            frm.Show()
            frm.EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
            frm.CargarOperacion(item.SubItems(13).Text) ' IDTransaccion
        Next
    End Sub

    Sub CargarPorcentaje()
        PorcentajeAsignado = CType(CSistema.ExecuteScalar("Select IsNull(porcentaje,0) from porcentajeExcesoLineacredito where IDUsuario=" & vgIDUsuario), Decimal)
        ImporteAsignado = CType(CSistema.ExecuteScalar("Select IsNull(Importe,0) from porcentajeExcesoLineacredito where IDUsuario=" & vgIDUsuario), Decimal)
    End Sub

    Function ControlarPorcentaje(ByVal Porcentaje As Decimal, ByVal Importe As Decimal) As Boolean
        ControlarPorcentaje = True
        If PorcentajeAsignado = 0 And ImporteAsignado = 0 Then
            Return False
        End If

        If (Porcentaje > PorcentajeAsignado) And (Importe > ImporteAsignado) Then
            Return False
        End If
    End Function

    Sub Facturar(ByVal frm As frmVentaPedido, ByVal ID As Integer, ByVal Comprobante As String)

        frm.IDPedido = ID
        frm.Comprobante = Comprobante
        frm.Inicializar()
        frm.Nuevo()
        frm.EsPedido = True
        frm.CargarPedido(vgIDSucursal)
        FGMostrarFormulario(Me, frm, "Venta de Pedido Nro: " & ID, Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Sub SeleccionarTodo()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = True
        Next
    End Sub

    Sub QuitarSeleccion()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = False
        Next
    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        Select Case e.KeyCode
            Case Keys.Enter
                CSistema.SelectNextControl(Me, e.KeyCode)
        End Select

    End Sub

    Sub EliminarPedido()

        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        lvPedidos.SelectedItems(0).Remove()

    End Sub

    Sub Anular()

        If lvPedidos.SelectedItems.Count = 0 Then
            MessageBox.Show("Seleccione un registro para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Dim IDOperacion As Integer = CSistema.ObtenerIDOperacion(frmPedido.Name, "PEDIDO", "PED")
        Dim Numero As Integer = lvPedidos.SelectedItems(0).SubItems(0).Text
        Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select Top(1) IDTransaccion From Pedido Where Numero=" & Numero & " And IDSucursal=" & vgIDSucursal)

        'Verificar que el documento no esta ya anulado
        Dim Anulado As Boolean
        Anulado = CType(CSistema.ExecuteScalar("Select IsNull((Select Anulado From Pedido Where IDTransaccion=" & IDTransaccion & "), 'False')"), Boolean)
        If Anulado = True Then
            MessageBox.Show("El documento ya esta anulado!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        'Consultar
        If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        'Anulamos
        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", ERP.CSistema.NUMOperacionesRegistro.ANULAR.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Anular Registro
        If CSistema.ExecuteStoreProcedure(param, "SpPedido", False, False, MensajeRetorno) = False Then
            MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        ListarPedidos()

    End Sub

    Private Sub frmAprobarPedido_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Detener()
        GuardarInformacion()
    End Sub

    Private Sub frmAprobarPedido_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        ManejarTecla(e)
    End Sub

    Private Sub frmAprobarPedido_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnActualizarGrupo_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarGrupo.Click
        ListarGrupos()
    End Sub

    Private Sub cbxGrupos_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxGrupos.SelectedIndexChanged
        ListarGrupos()
    End Sub

    Private Sub lvGrupo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lvGrupo.SelectedIndexChanged
        SeleccionarGrupo()
    End Sub

    Private Sub btnProcesar_Click(sender As System.Object, e As System.EventArgs) Handles btnAprobar.Click
        'Procesar()
        Select Case lvGrupo.SelectedItems.Item(0).Text
            Case "RECHAZADOS"
                AprobarRechazarPedido(ENUMOperacionPedido.RECUPERAR)
            Case "APROBADOS"
                AprobarRechazarPedido(ENUMOperacionPedido.RECUPERAR)
            Case Else
                AprobarRechazarPedido(ENUMOperacionPedido.APROBAR)
        End Select

    End Sub

    Private Sub btnSeleccionarTodos_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionarTodos.Click
        SeleccionarTodo()
    End Sub

    Private Sub ToolStripButton1_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton1.Click
        QuitarSeleccion()
    End Sub

    Private Sub btnActualizarPedidos_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarPedidos.Click
        ListarPedidos()
    End Sub

    Private Sub EliminarDeLaListaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        EliminarPedido()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub AnularRegistroToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        Anular()
    End Sub

    Private Sub btnVerInventario_Click(sender As System.Object, e As System.EventArgs) Handles btnVerInventario.Click
        ListarInventario()
    End Sub

    Private Sub btnVerPedido_Click(sender As System.Object, e As System.EventArgs) Handles btnVerPedido.Click
        VerPedido()
    End Sub

    Private Sub ToolStripButton2_Click(sender As System.Object, e As System.EventArgs) Handles btnRechazar.Click
        'Select Case lvGrupo.SelectedItems.Item(0).Text
        '    Case "PENDIENTE"
        '        AprobarRechazarPedido(ENUMOperacionPedido.RECHAZAR)
        '    Case "RECHAZADO"
        '        AprobarRechazarPedido(ENUMOperacionPedido.ANULAR)
        'End Select

        AprobarRechazarPedido(ENUMOperacionPedido.RECHAZAR)

    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmAprobarExcesoLineaCredito_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmImportarPedidos

    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim dtCabecera As DataTable
    Dim dtDetalle As DataTable

    Sub CargarPath()

        Dim obj As New FolderBrowserDialog

        If obj.ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then
            Exit Sub
        End If

        txtPathClientes.SetValue(obj.SelectedPath)

    End Sub

    Sub ListarCabecera()

        Try
            dtCabecera = CSistema.ExecuteToDataTableDBF("Select * From TRFPEDIDO", txtPathClientes.GetValue)
            dgvCabecera.DataSource = dtCabecera

            For c As Integer = 0 To dgvCabecera.Columns.Count - 1
                dgvCabecera.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            Next

        Catch ex As Exception

        End Try


    End Sub

    Sub ListarDetalle()

        Try
            Dim ID As Integer = dgvCabecera.SelectedRows(0).Cells(0).Value

            dtDetalle = CSistema.ExecuteToDataTableDBF("Select * From TRFPEDETAL", txtPathClientes.GetValue)
            dgvDetalle.DataSource = dtDetalle

            For c As Integer = 0 To dgvDetalle.Columns.Count - 1
                dgvDetalle.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            Next

        Catch ex As Exception

        End Try


    End Sub

    Private Sub frmImportarPedidos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnExaminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExaminar.Click
        CargarPath()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ListarCabecera()
    End Sub

    Private Sub dgvCabecera_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCabecera.CellContentClick

    End Sub

    Private Sub dgvCabecera_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvCabecera.SelectionChanged
        ListarDetalle()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmImportarPedidos_Activate()
        Me.Refresh()
    End Sub
End Class
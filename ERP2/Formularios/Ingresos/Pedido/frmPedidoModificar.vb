﻿Public Class frmPedidoModificar
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Dim CData As New CData
    Dim CDescuento As New CDescuento
    Dim vDecimalOperacion As Boolean
    Dim vIDTipoProducto As Integer = 0
    Dim vTipoProducto As String = ""

    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private NuevoIDTransaccionValue As Integer
    Public Property NuevoIDTransaccion() As Integer
        Get
            Return NuevoIDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            NuevoIDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private EsMovilValue As Boolean
    Public Property EsMovil() As Boolean
        Get
            Return EsMovilValue
        End Get
        Set(ByVal value As Boolean)
            EsMovilValue = value
        End Set
    End Property

    Private orowConfiguracionesValue As DataRow
    Public Property orowConfiguraciones() As DataRow
        Get
            Return orowConfiguracionesValue
        End Get
        Set(ByVal value As DataRow)
            orowConfiguracionesValue = value
        End Set
    End Property
    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property
    Private ClienteValue As String
    Public Property Cliente() As String
        Get
            Return ClienteValue
        End Get
        Set(ByVal value As String)
            ClienteValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtPedido As New DataTable
    Dim dtDetalle As New DataTable
    Dim dtDescuento As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim dtPreciosProducto As DataTable
    Dim vPermitirModificacion As Boolean

    Sub Inicializar()

        Me.AcceptButton = New Button
        'txtCliente.Conectar(CData.CreateTable("VCliente", "Select Top(0) * From VCliente", 10))
        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Condicion, Vendedor, 'Saldo Credito'=SaldoCredito, PlazoCredito  From VCliente Where Estado='ACTIVO' "
        txtCliente.frm = Me
        If vgUsuarioEsVendedor Then
            txtCliente.Consulta = txtCliente.Consulta & " AND IDVendedor=" & vgUsuarioIDVendedor
        End If

        'Otros
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False
        OcxImpuesto1.Inicializar()
        OcxImpuesto1.dg.ReadOnly = True

        'Propiedades
        IDTransaccion = 0
        vNuevo = False

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "PEDIDO", "PED", CadenaConexion)
        CargarInformacion()

        'Clases

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        'Foco
        txtCliente.Focus()

        Dim f As New Font(lblAnulado.Font.FontFamily, 12, FontStyle.Bold, lblAnulado.Font.Unit)
        lblAnulado.Font = f
        lblAnulado.ForeColor = Color.Red

        'txtDescuentoProducto.BackColor = Color.FromArgb(MVariablesGlobales.vgColorSoloLectura)
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        If IDCliente > 0 Then
            Nuevo()
            txtCliente.SetValue(IDCliente)
            txtFechaFacturar.Focus()
        End If
        OcxCotizacion1.txtCotizacion.Enabled = False
    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'Cabecera
        CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, txtFechaFacturar)
        CSistema.CargaControl(vControles, cbxCondicion)
        CSistema.CargaControl(vControles, cbxFormaPagoFactura)
        CSistema.CargaControl(vControles, cbxListaPrecio)
        CSistema.CargaControl(vControles, cbxDeposito)
        CSistema.CargaControl(vControles, cbxVendedor)
        CSistema.CargaControl(vControles, txtDetalle)
        CSistema.CargaControl(vControles, OcxCotizacion1)
        CSistema.CargaControl(vControles, txtDetalle)

        OcxCotizacion1.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFechaFacturar.txt.Text, True, False)
        OcxCotizacion1.Inicializar()

        dtDetalle = CData.GetStructure("VDetallePedido", "Select Top(0) * From VDetallePedido")
        dtDescuento = CData.GetStructure("VDescuentopedido", "Select Top(0) * From VDescuentoPedido")

        dtPreciosProducto = New DataTable
        dtPreciosProducto.Columns.Add("Precio")
        dtPreciosProducto.Columns.Add("Cantidad")
        dtPreciosProducto.Columns.Add("PorcentajeDescuento")
        dtPreciosProducto.Columns.Add("DescuentoUnitario")
        dtPreciosProducto.Columns.Add("TotalDescuento")
        dtPreciosProducto.Columns.Add("Total")
        dtPreciosProducto.Columns.Add("TotalSinDescuento")
        dtPreciosProducto.Columns.Add("DescuentoUnitarioDiscriminado")
        dtPreciosProducto.Columns.Add("TotalDescuentoDiscriminado")

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()
        'CARGAR CONDICION
        cbxCondicion.Items.Add("Contado")
        cbxCondicion.Items.Add("Credito")
        cbxCondicion.DropDownStyle = ComboBoxStyle.DropDownList

        'Forma de pago Factura

        'Ciudades
        CSistema.SqlToComboBox(cbxCiudad.cbx, CData.GetTable("VSucursal", " ID = " & vgIDSucursal), "IDCiudad", "CodigoCiudad")

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal", " ID = " & vgIDSucursal), "ID", "Descripcion")
        cbxSucursal.SelectedValue(vgIDSucursal)

        'Deposito
        CSistema.SqlToComboBox(cbxDeposito.cbx, "Select ID, Deposito From VDeposito Where IDSucursal=" & cbxSucursal.cbx.SelectedValue)
        cbxDeposito.cbx.SelectedValue = vgIDDeposito

        'Vendedor
        cbxVendedor.Conectar()

        'Listas de Precio
        cbxListaPrecio.Conectar(" Estado='True' ")

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Moneda
        OcxCotizacion1.cbxMoneda.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MONEDA", "")
    End Sub

    Sub GuardarInformacion()
        'Moneda
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", OcxCotizacion1.cbxMoneda.cbx.Text)
    End Sub

    Sub Nuevo()
        vTipoProducto = ""
        vIDTipoProducto = 0
        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Limpiar cabecera
        txtCliente.SetValue(Nothing)

        'Limpiar detalle
        dtDetalle.Rows.Clear()
        dtDescuento.Rows.Clear()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)
        CDescuento.dtDescuento = dtDescuento
        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True
        txtEstado.txt.Clear()
        txtVenta.txt.Text = "---"
        txtTotal.txt.Text = ""
        txtPorcentajeDescuento.txt.Text = ""
        Txtdescuento.txt.Text = ""
        txtDetalle.txt.Text = ""
        btnModificar.Visible = False


        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        'Obtener registro nuevo
        If vgUsuarioEsVendedor = False Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From Pedido Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And Procesado='True' ),1)"), Integer)
        Else
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From VPedidoVendedor Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And Procesado='True' And IDVendedor=" & vgUsuarioIDVendedor & " ),1)"), Integer)
        End If


        'Bloquear Nro de Operacion
        txtID.SoloLectura = True

        'Poner el foco en el proveedor
        Me.ActiveControl = txtCliente
        txtCliente.Focus()
        txtCliente.SetFocus()

        OcxImpuesto1.IDMoneda = OcxCotizacion1.cbxMoneda.GetValue
        OcxCotizacion1.Cargar()
        If CSistema.ExecuteScalar("select count(*) from Cotizacion where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False) & "' and IDMoneda = " & OcxCotizacion1.Registro("ID")) = 0 And OcxCotizacion1.Registro("ID") > 1 Then
            MessageBox.Show("No se ha fijado cotizacion para la moneda seleccionada.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
            Exit Sub
        End If
    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)
        vNuevo = False

        'Bloquear Nro de Operacion
        txtID.SoloLectura = False

        Dim e As New KeyEventArgs(Keys.End)
        txtID_TeclaPrecionada(New Object, e)

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Seleccion de Cliente
        If txtCliente.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If txtCliente.Registro Is Nothing Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        If txtDetalle.Texto = "" Then
            Dim mensaje As String = "Debe introducir un detalle!"
            CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        If IsNumeric(txtCliente.Registro("ID").ToString) = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Control de Fechas.
        'If DateDiff(DateInterval.Day, VGFechaHoraSistema, txtFechaFacturar.GetValue) < 0 Then
        '    Dim mensaje As String = "La fecha no puede ser menor a hoy!"
        '    CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Function
        'End If


        'Seleccion de Moneda
        If (IsNumeric(cbxVendedor.cbx.SelectedValue) = False) Or (cbxVendedor.cbx.Text = "") Then
            Dim mensaje As String = "Seleccione correctamente un vendedor!"
            CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Seleccion de Deposito
        If IsNumeric(cbxDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Existencia en Detalle
        If dtDetalle.Rows.Count = 0 Then
            Dim mensaje As String = "El documento debe tener por lo menos 1 detalle!"
            CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Total
        If CDec(OcxImpuesto1.txtTotal.ObtenerValor) <= 0 Then
            Dim mensaje As String = "El importe del documento no es valido!"
            CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        ValidarDocumento = True

    End Function

    Sub Guardar()

        If ValidarDocumento(ERP.CSistema.NUMOperacionesRegistro.INS) = False Then
            Exit Sub
        End If

        Dim SQL As String = ""

        'Nueva Transaccion
        NuevoIDTransaccion = CSistema.NuevaTransaccion(IDOperacion, 0, 0, cbxSucursal.GetValue, cbxDeposito.GetValue, 0, CadenaConexion)

        SQL = "Update pedido set FechaFacturar = '" & CSistema.FormatoFechaBaseDatos(txtFechaFacturar.txt.Text, True, False) & "' where IDTRansaccion = " & IDTransaccion

        'Guardamos
        Dim RowCount As Integer = CSistema.ExecuteNonQuery(SQL, CadenaConexion)

        If RowCount = 0 Then
            CSistema.MostrarError("Error de sistema!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If
    End Sub


    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, New Button, btnGuardar, btnCancelar, New Button, New Button, New Button, New Button, vControles, btnModificar)

    End Sub
    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)
        Try
            vNuevo = False

            ctrError.Clear()
            tsslEstado.Text = ""
            txtID.Focus()

            'Bloquear Nro de Operacion
            txtID.SoloLectura = False

            'Obtenemos el IDTransaccion
            If vIDTransaccion = 0 Then
                If vgUsuarioEsVendedor = False Then
                    IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select TOP 1 IDTransaccion From Pedido Where Numero = " & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And Procesado='True'), 0 )", "", 10)
                Else
                    IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select TOP 1 IDTransaccion From VPedidoVendedor Where Numero = " & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And IDVendedor=" & vgUsuarioIDVendedor & " And Procesado='True'), 0 )", "", 10)
                End If

            Else
                IDTransaccion = vIDTransaccion
            End If

            If IDTransaccion = 0 Then
                Dim mensaje As String = "El sistema no encuentra el registro!"
                CSistema.MostrarError(mensaje, ctrError, txtID, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Sub
            End If

            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

            dtDetalle.Clear()

            If vgUsuarioEsVendedor = False Then
                dtPedido = CSistema.ExecuteToDataTable("Select TOP 1 * From VPedido Where IDTransaccion=" & IDTransaccion, "", 10).Copy
            Else
                dtPedido = CSistema.ExecuteToDataTable("Select TOP 1 * From VPedidoVendedor Where IDTransaccion=" & IDTransaccion, "", 10).Copy
            End If

            dtDetalle = CSistema.ExecuteToDataTable("Select * From VDetallePedido Where IDTransaccion=" & IDTransaccion & " Order By ID", "", 10).Copy
            dtDetalle.Columns.Add("Nuevo")

            dtDescuento = CSistema.ExecuteToDataTable("Select * From VDescuentoPedido Where IDTransaccion=" & IDTransaccion & " Order By ID", "", 10).Copy
            dtDescuento.Columns.Add("Nuevo")
            CDescuento.dtDescuento = dtDescuento

            For Each dRow As DataRow In dtDetalle.Rows
                dRow("Nuevo") = False
            Next

            For Each dRow As DataRow In dtDescuento.Rows
                dRow("Nuevo") = False
            Next

            'Cargamos la cabecera
            If dtPedido Is Nothing Then
                Dim mensaje As String = "Error en la consulta! Problemas tecnico."
                ctrError.SetError(txtID, mensaje)
                ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If dtPedido.Rows.Count = 0 Then
                Exit Sub
            End If

            Dim oRow As DataRow = dtPedido.Rows(0)

            'Punto de Expedicion
            txtID.SetValue(oRow("Numero"))
            cbxCiudad.txt.Text = oRow("Ciudad").ToString
            cbxSucursal.txt.Text = oRow("Sucursal").ToString
            txtCliente.SetValue(oRow("IDCliente").ToString)
            txtCliente.SetValue(oRow("Ref.Cliente").ToString, oRow("Cliente").ToString, oRow("RUC").ToString)
            txtDireccion.txt.Text = oRow("Direccion").ToString
            txtTelefono.txt.Text = oRow("Telefono").ToString
            txtFechaFacturar.SetValueFromString(CDate(oRow("FechaFacturar").ToString))
            cbxCondicion.SelectedIndex = IIf(oRow("Credito").ToString = False, 0, 1)


            cbxDeposito.cbx.Text = oRow("Deposito").ToString
            cbxVendedor.txt.Text = oRow("Vendedor").ToString
            'cbxVendedor.SoloLectura = True
            txtDetalle.txt.Text = oRow("Observacion").ToString
            cbxListaPrecio.txt.Text = oRow("Lista de Precio").ToString
            OcxCotizacion1.SetValue(oRow("Moneda").ToString, oRow("Cotizacion").ToString)
            OcxCotizacion1.cbxMoneda.cbx.SelectedValue = oRow("IDMoneda")
            txtEstado.txt.Text = oRow("Estado").ToString
            txtVenta.txt.Text = oRow("Venta").ToString

            cbxFormaPagoFactura.txt.Text = oRow("FormaPagoFactura").ToString

            flpRegistradoPor.Visible = True
            lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
            lblUsuarioRegistro.Text = oRow("usuario").ToString

            If CBool(oRow("Anulado").ToString) = True Then
                flpAnuladoPor.Visible = True
                lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
                lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
                btnModificar.Visible = True
            Else
                flpAnuladoPor.Visible = False
                btnModificar.Visible = False
            End If

            CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error al cargar operación", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Sub ObtenerInformacionClientes()

        Dim oRow As DataRow = txtCliente.Registro
        Dim oRowSucursal As DataRow = txtCliente.Sucursal

        'Limpiar
        txtDireccion.txt.Clear()
        txtTelefono.txt.Clear()

        'Datos Comunes
        txtCreditoSaldo.txt.Text = oRow("SaldoCredito").ToString
        'txtProducto.IDCliente = oRow("ID").ToString
        cbxCondicion.Text = oRow("Condicion").ToString
        txtPlazo.txt.Text = oRow("PlazoCredito").ToString

        If txtCliente.SucursalSeleccionada = False Then
            cbxListaPrecio.cbx.Text = cbxSucursal.cbx.Text & "-" & oRow("ListaPrecio").ToString
            'cbxListaPrecio.cbx.Text = oRow("ListaPrecio").ToString
            txtDireccion.txt.Text = oRow("Direccion").ToString
            txtTelefono.txt.Text = oRow("Telefono").ToString
            cbxVendedor.cbx.Text = oRow("Vendedor").ToString
            cbxCondicion.Text = oRow("CONDICION").ToString
            'txtProducto.IDClienteSucursal = 0
        Else
            cbxListaPrecio.cbx.Text = cbxSucursal.cbx.Text & "-" & oRowSucursal("ListaPrecio").ToString
            'cbxListaPrecio.cbx.Text = oRowSucursal("ListaPrecio").ToString
            txtDireccion.txt.Text = oRowSucursal("Direccion").ToString
            txtTelefono.txt.Text = oRowSucursal("Telefono").ToString
            cbxVendedor.cbx.Text = oRowSucursal("Vendedor").ToString
            cbxCondicion.Text = oRowSucursal("CONDICION").ToString
            'txtProducto.IDClienteSucursal = oRowSucursal("ID").ToString
        End If
        'Gestion Judicial
        Dim vJudicial As Boolean = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & oRow("ID").ToString, "VCliente")("Judicial").ToString)
        If vJudicial = True Then
            MessageBox.Show("El cliente se encuentra en Estado Judicial!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Inicializar()
        End If

        If cbxCondicion.Text = "Credito" Then
            cbxCondicion.Enabled = True
        Else
            cbxCondicion.Enabled = False
        End If

        'Configuraciones
        'cbxMoneda.SelectedValue(oRow("IDMoneda"))
        'OcxCotizacion1.Registro("ID") = oRow("IDMoneda")

        If txtCliente.SucursalSeleccionada = False Then
            txtDireccion.txt.Text = oRow("Direccion").ToString
            txtTelefono.txt.Text = oRow("Telefono").ToString
            cbxVendedor.cbx.Text = oRow("Vendedor").ToString

            If oRow("Vendedor").ToString = "" Then
                'cbxVendedor.SoloLectura = False
            Else
                'cbxVendedor.SoloLectura = True
            End If

        Else
            txtDireccion.txt.Text = oRowSucursal("Direccion").ToString
            txtTelefono.txt.Text = oRowSucursal("Telefono").ToString
            cbxVendedor.cbx.Text = oRowSucursal("Vendedor").ToString

            If oRowSucursal("Vendedor").ToString = "" Then
                'cbxVendedor.SoloLectura = False
            Else
                'cbxVendedor.SoloLectura = True
            End If

        End If
    End Sub

    Sub CambiarSucursal()
        cbxDeposito.cbx.DataSource = Nothing
        cbxListaPrecio.cbx.DataSource = Nothing

        If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxSucursal.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Depositos
        CSistema.SqlToComboBox(cbxDeposito.cbx, CData.GetTable("VDeposito", "IDSucursal=" & cbxSucursal.GetValue), "ID", "Deposito")

        'Listas de Precios
        CSistema.SqlToComboBox(cbxListaPrecio.cbx, CData.GetTable("VListaPrecio", "IDSucursal=" & cbxSucursal.GetValue), "ID", "Lista")
    End Sub

    'Por porcentaje o por importe
    Sub ManejarTecla(ByRef e As System.Windows.Forms.KeyEventArgs)

        If vNuevo Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.ObtenerValor
            ID = CInt(ID) + 1
            txtID.SetValue(ID)
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.ObtenerValor

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.SetValue(ID)
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer

            If vgUsuarioEsVendedor = False Then
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VPedido Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)
            Else
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VPedidoVendedor Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And IDVendedor=" & vgUsuarioIDVendedor), Integer)
            End If


            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = vgKeyConsultar Then

            'Si no esta en el cliente
            If txtCliente.IsFocus = True Then
                Exit Sub
            End If

            'Si esta en el producto
            'If txtProducto.Focus = True Then
            Dim IDCliente As String
            If txtCliente.Registro Is Nothing Then
                Exit Sub
            End If

            IDCliente = txtCliente.Registro("ID").ToString

            Exit Sub
            'End If

            'Buscar()
        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer

            If vgUsuarioEsVendedor = False Then
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VPedido Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)
            Else
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VPedidoVendedor Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And IDVendedor=" & vgUsuarioIDVendedor), Integer)
            End If

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Public Sub EstablecerVariablesGlobales(Optional _vgIDUsuario As Integer = 0, Optional _vgIDSucursal As Integer = 0, Optional _vgIDDeposito As Integer = 0, Optional _vgIDTerminal As Integer = 0, Optional _vgIDPerfil As Integer = 0)

        If _vgIDUsuario <> 0 Then
            vgIDUsuario = _vgIDUsuario
        End If

        If _vgIDSucursal <> 0 Then
            vgIDSucursal = _vgIDSucursal
        End If

        If _vgIDDeposito <> 0 Then
            vgIDDeposito = _vgIDDeposito
        End If

        If _vgIDTerminal <> 0 Then
            vgIDTerminal = _vgIDTerminal
        End If

        If _vgIDPerfil <> 0 Then
            vgIDPerfil = _vgIDPerfil
        End If

    End Sub
    Sub Modificar()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.MODIFICAR)

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        txtFechaFacturar.Enabled = True
        txtFechaFacturar.txt.Text = Today


    End Sub

    Sub MostrarDecimales(ByVal IDMoneda As Integer)

        'If dgw.Columns.Count = 0 Then
        '    Exit Sub
        'End If

        If IDMoneda = 1 Then
            'txtPrecioUnitario.Decimales = False
            txtTotal.Decimales = False
            Txtdescuento.Decimales = False
            'dgw.Columns("Amortizacion").DefaultCellStyle.Format = "N0"
            'dgw.Columns("Interes").DefaultCellStyle.Format = "N0"
            'dgw.Columns("Impuesto").DefaultCellStyle.Format = "N0"
            'dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N0"
            'dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N0"
        Else
            txtTotal.Decimales = True
            Txtdescuento.Decimales = True
            'dgw.Columns("Amortizacion").DefaultCellStyle.Format = "N2"
            'dgw.Columns("Interes").DefaultCellStyle.Format = "N2"
            'dgw.Columns("Impuesto").DefaultCellStyle.Format = "N2"
            'dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N2"
            'dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N2"
        End If

    End Sub

    Private Sub frmCompra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub frmCompra_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmCompra_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Enter Then

            Debug.Print(Me.ActiveControl.Name)

        End If

        CSistema.SelectNextControl(Me, e.KeyCode)


    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        vNuevo = True
        Guardar()
        txtID.txt.Focus()
        txtID.txt.SelectAll()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada

        ManejarTecla(e)

    End Sub

    Private Sub txtCliente_ItemMalSeleccionado(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCliente.ItemMalSeleccionado

        'Limpiar los otros controles
        txtDireccion.txt.Clear()
        txtTelefono.txt.Clear()
        cbxListaPrecio.txt.Clear()

        'Datos Comunes
        cbxListaPrecio.cbx.Text = ""
        txtCreditoSaldo.txt.Text = "0"
        'txtProducto.IDCliente = Nothing

    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado
        ObtenerInformacionClientes()
        If vNuevo = True Then
            If txtCliente.ClienteVario = True Then
                txtDireccion.SoloLectura = False
                txtTelefono.SoloLectura = False
            Else
                txtDireccion.SoloLectura = True
                txtTelefono.SoloLectura = True
            End If
            txtFechaFacturar.txt.Focus()
        End If
    End Sub
    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

    End Sub

    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        CambiarSucursal()
    End Sub

    Private Sub frmPedido_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Cliente = ""
        IDCliente = 0
    End Sub

    Private Sub btnModificar_Click(sender As System.Object, e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    '10-06-2021 - SC - Actualiza datos
    Sub frmPedidoModificar_Activate()
        Me.Refresh()
    End Sub
End Class
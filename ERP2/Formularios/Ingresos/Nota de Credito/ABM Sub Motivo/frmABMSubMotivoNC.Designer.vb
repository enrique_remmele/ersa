﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmABMSubMotivoNC
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.cbxMotivoNotaCredito = New ERP.ocxCBX()
        Me.lblMotivo = New System.Windows.Forms.Label()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.chkDevolucion = New ERP.ocxCHK()
        Me.chkAnulacion = New ERP.ocxCHK()
        Me.chkFinanciero = New ERP.ocxCHK()
        Me.chkDiferenciaPrecio = New ERP.ocxCHK()
        Me.chkAcuerdoComercial = New ERP.ocxCHK()
        Me.chkDiferenciaPeso = New ERP.ocxCHK()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(90, 61)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(156, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 1
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(90, 4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 18
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 420)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(576, 22)
        Me.StatusStrip1.TabIndex = 33
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(490, 391)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 32
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(333, 142)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(414, 142)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 6
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(252, 142)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 2
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(171, 142)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 5
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(90, 142)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 4
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(12, 65)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 19
        Me.lblDescripcion.Text = "Descripción:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(12, 9)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 17
        Me.lblID.Text = "ID:"
        '
        'cbxMotivoNotaCredito
        '
        Me.cbxMotivoNotaCredito.CampoWhere = Nothing
        Me.cbxMotivoNotaCredito.CargarUnaSolaVez = False
        Me.cbxMotivoNotaCredito.DataDisplayMember = Nothing
        Me.cbxMotivoNotaCredito.DataFilter = Nothing
        Me.cbxMotivoNotaCredito.DataOrderBy = Nothing
        Me.cbxMotivoNotaCredito.DataSource = Nothing
        Me.cbxMotivoNotaCredito.DataValueMember = Nothing
        Me.cbxMotivoNotaCredito.dtSeleccionado = Nothing
        Me.cbxMotivoNotaCredito.FormABM = Nothing
        Me.cbxMotivoNotaCredito.Indicaciones = Nothing
        Me.cbxMotivoNotaCredito.Location = New System.Drawing.Point(89, 32)
        Me.cbxMotivoNotaCredito.Name = "cbxMotivoNotaCredito"
        Me.cbxMotivoNotaCredito.SeleccionMultiple = False
        Me.cbxMotivoNotaCredito.SeleccionObligatoria = False
        Me.cbxMotivoNotaCredito.Size = New System.Drawing.Size(157, 23)
        Me.cbxMotivoNotaCredito.SoloLectura = False
        Me.cbxMotivoNotaCredito.TabIndex = 0
        Me.cbxMotivoNotaCredito.Texto = ""
        '
        'lblMotivo
        '
        Me.lblMotivo.AutoSize = True
        Me.lblMotivo.Location = New System.Drawing.Point(12, 37)
        Me.lblMotivo.Name = "lblMotivo"
        Me.lblMotivo.Size = New System.Drawing.Size(42, 13)
        Me.lblMotivo.TabIndex = 35
        Me.lblMotivo.Text = "Motivo:"
        '
        'dgv
        '
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(90, 171)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.Size = New System.Drawing.Size(475, 214)
        Me.dgv.TabIndex = 36
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 94)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 37
        Me.Label1.Text = "Estado:"
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(142, 92)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 93
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(90, 92)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 92
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'chkDevolucion
        '
        Me.chkDevolucion.BackColor = System.Drawing.Color.Transparent
        Me.chkDevolucion.Color = System.Drawing.Color.Empty
        Me.chkDevolucion.Enabled = False
        Me.chkDevolucion.Location = New System.Drawing.Point(310, 20)
        Me.chkDevolucion.Name = "chkDevolucion"
        Me.chkDevolucion.Size = New System.Drawing.Size(72, 21)
        Me.chkDevolucion.SoloLectura = False
        Me.chkDevolucion.TabIndex = 94
        Me.chkDevolucion.Texto = "Devolución"
        Me.chkDevolucion.Valor = False
        '
        'chkAnulacion
        '
        Me.chkAnulacion.BackColor = System.Drawing.Color.Transparent
        Me.chkAnulacion.Color = System.Drawing.Color.Empty
        Me.chkAnulacion.Enabled = False
        Me.chkAnulacion.Location = New System.Drawing.Point(433, 20)
        Me.chkAnulacion.Name = "chkAnulacion"
        Me.chkAnulacion.Size = New System.Drawing.Size(72, 21)
        Me.chkAnulacion.SoloLectura = False
        Me.chkAnulacion.TabIndex = 95
        Me.chkAnulacion.Texto = "Anulación"
        Me.chkAnulacion.Valor = False
        '
        'chkFinanciero
        '
        Me.chkFinanciero.BackColor = System.Drawing.Color.Transparent
        Me.chkFinanciero.Color = System.Drawing.Color.Empty
        Me.chkFinanciero.Location = New System.Drawing.Point(310, 39)
        Me.chkFinanciero.Name = "chkFinanciero"
        Me.chkFinanciero.Size = New System.Drawing.Size(72, 21)
        Me.chkFinanciero.SoloLectura = False
        Me.chkFinanciero.TabIndex = 96
        Me.chkFinanciero.Texto = "Financiero"
        Me.chkFinanciero.Valor = False
        '
        'chkDiferenciaPrecio
        '
        Me.chkDiferenciaPrecio.BackColor = System.Drawing.Color.Transparent
        Me.chkDiferenciaPrecio.Color = System.Drawing.Color.Empty
        Me.chkDiferenciaPrecio.Location = New System.Drawing.Point(310, 58)
        Me.chkDiferenciaPrecio.Name = "chkDiferenciaPrecio"
        Me.chkDiferenciaPrecio.Size = New System.Drawing.Size(121, 21)
        Me.chkDiferenciaPrecio.SoloLectura = False
        Me.chkDiferenciaPrecio.TabIndex = 97
        Me.chkDiferenciaPrecio.Texto = "Diferencia de Precio"
        Me.chkDiferenciaPrecio.Valor = False
        '
        'chkAcuerdoComercial
        '
        Me.chkAcuerdoComercial.BackColor = System.Drawing.Color.Transparent
        Me.chkAcuerdoComercial.Color = System.Drawing.Color.Empty
        Me.chkAcuerdoComercial.Location = New System.Drawing.Point(433, 39)
        Me.chkAcuerdoComercial.Name = "chkAcuerdoComercial"
        Me.chkAcuerdoComercial.Size = New System.Drawing.Size(72, 21)
        Me.chkAcuerdoComercial.SoloLectura = False
        Me.chkAcuerdoComercial.TabIndex = 98
        Me.chkAcuerdoComercial.Texto = "Acuerdo"
        Me.chkAcuerdoComercial.Valor = False
        '
        'chkDiferenciaPeso
        '
        Me.chkDiferenciaPeso.BackColor = System.Drawing.Color.Transparent
        Me.chkDiferenciaPeso.Color = System.Drawing.Color.Empty
        Me.chkDiferenciaPeso.Location = New System.Drawing.Point(433, 58)
        Me.chkDiferenciaPeso.Name = "chkDiferenciaPeso"
        Me.chkDiferenciaPeso.Size = New System.Drawing.Size(121, 21)
        Me.chkDiferenciaPeso.SoloLectura = False
        Me.chkDiferenciaPeso.TabIndex = 99
        Me.chkDiferenciaPeso.Texto = "Diferencia de PESO"
        Me.chkDiferenciaPeso.Valor = False
        '
        'frmABMSubMotivoNC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(576, 442)
        Me.Controls.Add(Me.chkDiferenciaPeso)
        Me.Controls.Add(Me.chkAcuerdoComercial)
        Me.Controls.Add(Me.chkDiferenciaPrecio)
        Me.Controls.Add(Me.chkFinanciero)
        Me.Controls.Add(Me.chkAnulacion)
        Me.Controls.Add(Me.chkDevolucion)
        Me.Controls.Add(Me.rdbDesactivado)
        Me.Controls.Add(Me.rdbActivo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.lblMotivo)
        Me.Controls.Add(Me.cbxMotivoNotaCredito)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.lblID)
        Me.Name = "frmABMSubMotivoNC"
        Me.Text = "frmABMSubMotivoNC"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents lblMotivo As System.Windows.Forms.Label
    Friend WithEvents cbxMotivoNotaCredito As ERP.ocxCBX
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents rdbDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbActivo As System.Windows.Forms.RadioButton
    Friend WithEvents chkAcuerdoComercial As ERP.ocxCHK
    Friend WithEvents chkDiferenciaPrecio As ERP.ocxCHK
    Friend WithEvents chkFinanciero As ERP.ocxCHK
    Friend WithEvents chkAnulacion As ERP.ocxCHK
    Friend WithEvents chkDevolucion As ERP.ocxCHK
    Friend WithEvents chkDiferenciaPeso As ocxCHK
End Class

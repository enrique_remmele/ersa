﻿Public Class frmABMSubMotivoNC
    'CLASES
    Dim CSistema As New CSistema


    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control
    Dim Editando As Boolean = False
    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        'Funciones
        CargarInformacion()

        'Botones
        EstablecerBotones(CSistema.NUMHabilitacionBotonesABM.INICIO)

        'Focus
        dgv.Focus()

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesABM)

        CSistema.ControlBotonesABM(Operacion, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtDescripcion)
        CSistema.CargaControl(vControles, cbxMotivoNotaCredito)
        CSistema.CargaControl(vControles, rdbActivo)
        CSistema.CargaControl(vControles, rdbDesactivado)
        CSistema.CargaControl(vControles, chkAnulacion)
        CSistema.CargaControl(vControles, chkDevolucion)
        CSistema.CargaControl(vControles, chkFinanciero)
        CSistema.CargaControl(vControles, chkDiferenciaPrecio)
        CSistema.CargaControl(vControles, chkAcuerdoComercial)

        'CARGAR UNIDAD
        cbxMotivoNotaCredito.cbx.Items.Add("DESCUENTO").ToString()
        cbxMotivoNotaCredito.cbx.Items.Add("DEVOLUCION").ToString()

        Listar()

    End Sub

    Sub ObtenerInformacion()
        Editando = False
        'Validar
        'Si es que se selecciono el registro.
        If dgv.SelectedRows.Count = 0 Then
            ctrError.SetError(dgv, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = dgv.SelectedRows(0).Cells("ID").Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From SubMotivoNotaCredito Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            cbxMotivoNotaCredito.cbx.Text = oRow("Motivo").ToString
            txtDescripcion.txt.Text = oRow("Descripcion").ToString
            chkAnulacion.Valor = oRow("Anulacion")
            chkDevolucion.Valor = oRow("Devolucion")
            chkFinanciero.Valor = oRow("Financiero")
            chkDiferenciaPrecio.Valor = oRow("DiferenciaPrecio")
            chkAcuerdoComercial.Valor = oRow("AcuerdoComercial")
            rdbActivo.Checked = oRow("Estado")
            rdbDesactivado.Checked = Not oRow("Estado")

            'Configuramos los controles ABM como EDITAR
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar()
        Editando = False
        dgv.Refresh()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select ID, Motivo, Descripcion, Tipo, Estado From vSubMotivoNotaCredito")

        CSistema.dtToGrid(dgv, dt)
        dgv.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill


    End Sub

    Sub InicializarControles()

        'TextBox
        txtDescripcion.txt.Clear()
        cbxMotivoNotaCredito.Texto = ""

        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From SubMotivoNotaCredito"), Integer)
        Else
            Listar()
        End If

        'Error
        ctrError.Clear()

        'Foco
        cbxMotivoNotaCredito.cbx.Focus()

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Escritura de la Descripcion
        If txtDescripcion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Debe ingresar una descripcion valida!"
            ctrError.SetError(txtDescripcion, mensaje)
            ctrError.SetIconAlignment(txtDescripcion, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgv.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgv, mensaje)
                ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Motivo", cbxMotivoNotaCredito.cbx.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Devolucion", chkDevolucion.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Anulacion", chkAnulacion.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DiferenciaPrecio", chkDiferenciaPrecio.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DiferenciaPeso", chkDiferenciaPeso.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Financiero", chkFinanciero.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Acuerdo", chkAcuerdoComercial.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpSubMotivoNotaCredito", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO)
            ctrError.Clear()
            Listar()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno

            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub Nuevo()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO)
        vNuevo = True
        InicializarControles()
        Editando = True
    End Sub

    Sub Editar()
        'Establecemos los botones a Editando
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO)
        Editando = True

        vNuevo = False

        'Foco
        txtDescripcion.Focus()
        txtDescripcion.txt.SelectAll()
    End Sub

    Sub Cancelar()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Sub Eliminar()
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Editar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

    Private Sub frmBanco_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Enter Then
            CSistema.SelectNextControl(Me, e.KeyCode)
        End If
    End Sub

    Private Sub frmBanco_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub dgv_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick
        ObtenerInformacion()
    End Sub

    Private Sub dgv_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles dgv.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub cbxMotivoNotaCredito_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxMotivoNotaCredito.PropertyChanged
        If cbxMotivoNotaCredito.cbx.SelectedIndex = 0 Then
            chkAcuerdoComercial.Enabled = True
            chkDiferenciaPrecio.Enabled = True
            chkFinanciero.Enabled = True
            chkDiferenciaPeso.Enabled = True
            chkDevolucion.Enabled = False
            chkAnulacion.Enabled = False
            If Editando = True Then
                chkDevolucion.chk.Checked = False
                chkAnulacion.chk.Checked = False
            End If
        Else
            chkAcuerdoComercial.Enabled = False
            chkDiferenciaPrecio.Enabled = False
            chkFinanciero.Enabled = False
            chkDiferenciaPeso.Enabled = False
            chkDevolucion.Enabled = True
            chkAnulacion.Enabled = True
            If Editando = True Then
                chkAcuerdoComercial.chk.Checked = False
                chkDiferenciaPrecio.chk.Checked = False
                chkFinanciero.chk.Checked = False
                chkDiferenciaPeso.chk.Checked = False
            End If
            End If
    End Sub

    Private Sub chkDevolucion_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkDevolucion.PropertyChanged
        If Editando = False Then
            Exit Sub
        End If
        If value = True Then
            chkAcuerdoComercial.chk.Checked = False
            chkDiferenciaPrecio.chk.Checked = False
            chkFinanciero.chk.Checked = False
            chkAnulacion.chk.Checked = False
            'chkDevolucion.chk.Checked = False
            chkDiferenciaPeso.chk.Checked = False
        End If
    End Sub

    Private Sub chkAnulacion_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkAnulacion.PropertyChanged

        If Editando = False Then
            Exit Sub
        End If
        If value = True Then
            chkAcuerdoComercial.chk.Checked = False
            chkDiferenciaPrecio.chk.Checked = False
            chkFinanciero.chk.Checked = False
            'chkAnulacion.chk.Checked = False
            chkDevolucion.chk.Checked = False
            chkDiferenciaPeso.chk.Checked = False
        End If
    End Sub

    Private Sub chkFinanciero_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkFinanciero.PropertyChanged
        If Editando = False Then
            Exit Sub
        End If
        If value = True Then
            chkAcuerdoComercial.chk.Checked = False
            chkDiferenciaPrecio.chk.Checked = False
            'chkFinanciero.chk.Checked = False
            chkAnulacion.chk.Checked = False
            chkDevolucion.chk.Checked = False
            chkDiferenciaPeso.chk.Checked = False
        End If
    End Sub

    Private Sub chkAcuerdoComercial_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkAcuerdoComercial.PropertyChanged
        If Editando = False Then
            Exit Sub
        End If
        If value = True Then
            'chkAcuerdoComercial.chk.Checked = False
            chkDiferenciaPrecio.chk.Checked = False
            chkFinanciero.chk.Checked = False
            chkAnulacion.chk.Checked = False
            chkDevolucion.chk.Checked = False
            chkDiferenciaPeso.chk.Checked = False
        End If
    End Sub

    Private Sub chkDiferenciaPrecio_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkDiferenciaPrecio.PropertyChanged
        If Editando = False Then
            Exit Sub
        End If
        If value = True Then
            chkAcuerdoComercial.chk.Checked = False
            chkFinanciero.chk.Checked = False
            chkAnulacion.chk.Checked = False
            chkDevolucion.chk.Checked = False
            chkDiferenciaPeso.chk.Checked = False
        End If
    End Sub

    Private Sub chkDiferenciaPeso_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkDiferenciaPeso.PropertyChanged
        If Editando = False Then
            Exit Sub
        End If
        If value = True Then
            chkAcuerdoComercial.chk.Checked = False
            chkFinanciero.chk.Checked = False
            chkAnulacion.chk.Checked = False
            chkDevolucion.chk.Checked = False
            chkDiferenciaPrecio.chk.Checked = False
        End If
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmABMSubMotivoNC_Activate()
        Me.Refresh()
    End Sub
End Class
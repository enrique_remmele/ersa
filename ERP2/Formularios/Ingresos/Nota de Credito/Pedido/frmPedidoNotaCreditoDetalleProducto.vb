﻿Public Class frmPedidoNotaCreditoDetalleProducto

    Dim CSistema As New CSistema

    Public Property IDSubMotivo As Integer
    Public Property IDTransaccion As Integer

    Sub Inicializar()
        CargarOperacion()
    End Sub

    Sub CargarOperacion()
        Dim Tipo As String = CSistema.ExecuteScalar("select Tipo from vsubmotivoNotaCredito where id=" & IDSubMotivo)
        Select Case Tipo
            Case "Acuerdo"
                ListarProductoAcuerdo()
            Case "Diferencia de Precio"
                ListarProductoDiferenciaPrecio()
            Case "Diferencia de PESO"
                ListarProductoDiferenciaPeso()
        End Select
    End Sub

    Sub ListarProductoDiferenciaPrecio()

        Dim dtProductoDiferenciaPrecio As New DataTable()
        dtProductoDiferenciaPrecio = CSistema.ExecuteToDataTable("Select Referencia, Descripcion, 'Precio Anterior'=PrecioOriginal,'Desc. Unitario'=ImporteDescuentoUnitario, 'Precio Final'=NuevoPrecio, Cantidad, 'Total Desc.'=ImporteDescuento from vDetalleNotaCreditoDiferenciaPrecio where IDTransaccionPedidoNotaCredito= " & IDTransaccion)
        dgv.DataSource = Nothing


        CSistema.dtToGrid(dgv, dtProductoDiferenciaPrecio)

        CSistema.DataGridColumnasNumericas(dgv, {"Desc. Unitario", "Precio Anterior", "Precio Final", "Cantidad", "Desc. Unitario", "Total Desc."}, False)
        dgv.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        dgv.Columns("Referencia").DisplayIndex = 0
        dgv.Columns("Descripcion").DisplayIndex = 1
        dgv.Columns("Precio Anterior").DisplayIndex = 2
        dgv.Columns("Desc. Unitario").DisplayIndex = 3
        dgv.Columns("Precio Final").DisplayIndex = 4
        dgv.Columns("Cantidad").DisplayIndex = 5
        dgv.Columns("Total Desc.").DisplayIndex = 6

    End Sub

    Sub ListarProductoDiferenciaPeso()

        Dim dtProductoDiferenciaPeso As New DataTable()
        dtProductoDiferenciaPeso = CSistema.ExecuteToDataTable("Select Referencia, Descripcion, 'DescuentoUnitario'=ImporteDescuentoUnitario, 'TotalDescuentoProducto'=ImporteDescuento from vDetalleNotaCreditoDiferenciaPeso where IDTransaccionPedidoNotaCredito= " & IDTransaccion)
        dgv.DataSource = Nothing


        CSistema.dtToGrid(dgv, dtProductoDiferenciaPeso)

        CSistema.DataGridColumnasNumericas(dgv, {"DescuentoUnitario", "TotalDescuentoProducto"}, False)
        dgv.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        dgv.Columns("Referencia").DisplayIndex = 0
        dgv.Columns("Descripcion").DisplayIndex = 1
        dgv.Columns("DescuentoUnitario").DisplayIndex = 2
        dgv.Columns("TotalDescuentoProducto").DisplayIndex = 3

    End Sub

    Sub ListarProductoAcuerdo()

        Dim dtProductoAcuerdo As New DataTable()
        dtProductoAcuerdo = CSistema.ExecuteToDataTable("Select Referencia, Descripcion, DescuentoUnitario, Cantidad, TotalDescuentoProducto from vDetalleNotaCreditoAcuerdo where IDTransaccionPedidoNotaCredito= " & IDTransaccion)
        dgv.DataSource = Nothing

        CSistema.dtToGrid(dgv, dtProductoAcuerdo)

        CSistema.DataGridColumnasNumericas(dgv, {"DescuentoUnitario", "Cantidad", "TotalDescuentoProducto"}, False)
        dgv.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        dgv.Columns("Referencia").DisplayIndex = 0
        dgv.Columns("Descripcion").DisplayIndex = 1
        dgv.Columns("DescuentoUnitario").DisplayIndex = 2
        dgv.Columns("Cantidad").DisplayIndex = 3
        dgv.Columns("TotalDescuentoProducto").DisplayIndex = 4

    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    '10-06-2021 - SC - Actualiza datos
    Sub frmPedidoNotaCreditoDetalleProducto_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmEstablecerDevolucion

    Dim CSistema As New CSistema
    Dim CData As New CData

    Public Property IDTransaccion As Integer
    Public Property IDDeposito As Integer
    Public Property IDSucursal As Integer
    Public Property dtDevolucion As DataTable
    Public Property Total As Decimal

    Dim dtFactura As DataTable
    Dim dtMotivoDevolucion As DataTable
    Dim dt As DataTable

    Sub Inicializar()

        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        dtMotivoDevolucion = CSistema.ExecuteToDataTable("Select ID, Descripcion from MotivoDevolucionNC where Activo = 1")

        Dim SQL As String = ""
        dtFactura = CSistema.ExecuteToDataTable("Select * from vVenta where idtransaccion = " & IDTransaccion)
        Label1.Text = "Factura Nro: " & dtFactura(0)("Comprobante").ToString & " -  Fecha de Emision: " & CDate(dtFactura(0)("FechaEmision"))
        'dtDevolucion = CSistema.ExecuteToDataTable("Select 'Sel' = Cast(0 as bit), Referencia, Producto, Cantidad, 'Precio'=PrecioUnitario - DescuentoUnitario, 'Total'=0, idtransaccion, idproducto , 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0 From VDetallePedidoNotaCredito where idtransaccion = " & IDTransaccion)
        'Select *, 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0 From VDetallePedidoNotaCredito Where IDTransaccion=" & IDTransaccion & " Order By ID
        SQL = "Select 'Sel' = Cast(0 as bit), 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0, *, 'Cantidad a Devolver'=0 From VDetalleVentaParaNotaCredito where idtransaccion = " & IDTransaccion
        dt = CSistema.ExecuteToDataTable(SQL)
        CSistema.SqlToDataGrid(dgv, SQL)

        Dim comboboxColumn As New DataGridViewComboBoxColumn
        comboboxColumn.Name = "Motivo Devolucion"
        comboboxColumn.DataSource = dtMotivoDevolucion
        comboboxColumn.DisplayMember = "Descripcion"
        comboboxColumn.ValueMember = "ID"
        dgv.Columns.Add(comboboxColumn)

        CSistema.DataGridColumnasVisibles(dgv, {"Sel", "Ref", "Descripcion", "Cantidad", "SaldoCantidad", "Cantidad a Devolver", "PrecioUnitarioNeto", "Motivo Devolucion"})
        CSistema.DataGridColumnasNumericas(dgv, {"Cantidad", "SaldoCantidad", "Cantidad a Devolver"}, True)
        CSistema.DataGridColumnasNumericas(dgv, {"PrecioUnitarioNeto"}, True)
        dgv.Columns("Motivo Devolucion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Update()

        dgv.CurrentCell = dgv.Rows(0).Cells("Sel")

    End Sub

    'Sub BloquearHabilitarEdicion()
    '    Select Case dgv.CurrentCell.OwningColumn.Name
    '        Case "Cantidad a Devolver"
    '            dgv.ReadOnly = False
    '        Case "PrecioUnitarioNeto"
    '            dgv.ReadOnly = False
    '        Case "Motivo Devolucion"
    '            dgv.ReadOnly = False
    '        Case "Sel"
    '            dgv.ReadOnly = False
    '        Case Else
    '            dgv.ReadOnly = True
    '    End Select
    'End Sub

    Sub Guardar()

        dtDevolucion.Clear()

        For i = 0 To dgv.Rows.Count - 1
            If CBool(dgv.Rows(i).Cells("Sel").Value) = True Then


                Dim dRow As DataRow = dtDevolucion.NewRow

                dRow("IDProducto") = dgv.Rows(i).Cells("ID").Value
                dRow("ID") = dtDevolucion.Rows.Count
                dRow("IDTransaccionVenta") = IDTransaccion

                'If dgv.Rows(i).Cells("Observacion").ToString.Trim.Length = 0 Then
                '    dRow("Descripcion") = dgv.Rows(i).Cells("Descripcion").ToString
                'Else
                '    dRow("Descripcion") = dgv.Rows(i).Cells("Descripcion").ToString & " - " & dgv.Rows(i).Cells("Observacion").ToString
                'End If
                dRow("Descripcion") = dgv.Rows(i).Cells("Descripcion").Value
                dRow("Referencia") = dgv.Rows(i).Cells("Ref").Value
                dRow("CodigoBarra") = dgv.Rows(i).Cells("CodigoBarra").Value
                dRow("Observacion") = ""

                'Deposito
                dRow("IDDeposito") = IDDeposito
                dRow("Deposito") = CData.GetRow("ID=" & IDDeposito, "VDeposito")("Deposito")

                'Cantidad y Precios
                Dim CantidadProducto As Decimal
                dRow("Cantidad") = dgv.Rows(i).Cells("Cantidad a Devolver").Value
                CantidadProducto = dgv.Rows(i).Cells("Cantidad a Devolver").Value
                Dim PrecioUnitarioNeto As Decimal
                PrecioUnitarioNeto = dgv.Rows(i).Cells("PrecioUnitarioNeto").Value
                ' PrecioUnitario = VentaRow("PrecioUnitario")

                dRow("PrecioUnitario") = PrecioUnitarioNeto
                dRow("Pre. Uni.") = PrecioUnitarioNeto
                dRow("Total") = PrecioUnitarioNeto * CantidadProducto

                'Impuestos
                dRow("IDImpuesto") = dgv.Rows(i).Cells("IDImpuesto").Value
                dRow("Impuesto") = dgv.Rows(i).Cells("Impuesto").Value
                dRow("Ref Imp") = dgv.Rows(i).Cells("Ref Imp").Value
                dRow("Exento") = dgv.Rows(i).Cells("Exento").Value
                dRow("TotalImpuesto") = 0
                dRow("TotalDiscriminado") = 0
                CSistema.CalcularIVA(dRow("IDImpuesto").ToString, CDec(dRow("Total").ToString), True, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))

                'Costo
                'dRow("CostoUnitario") = VentaRow("Costo").ToString
                'dRow("TotalCosto") = CantidadProducto * VentaRow("Costo").ToString
                'dRow("TotalCostoImpuesto") = 0
                'dRow("TotalCostoDiscriminado") = dRow("TotalCosto")
                'CSistema.CalcularIVA(VentaRow("IDImpuesto").ToString, CDec(dRow("TotalCosto").ToString), vDecimalOperacion, True, 0, dRow("TotalCostoImpuesto"))

                dRow("TotalCosto") = 0
                dRow("TotalCostoImpuesto") = 0
                dRow("TotalCostoDiscriminado") = 0
                'CSistema.CalcularIVA(dRow("IDImpuesto").ToString, CDec(dRow("TotalCosto").ToString), True, False, 0, dRow("TotalCostoImpuesto"))

                'Descuentos
                dRow("PorcentajeDescuento") = 0 'dgv.Rows(i).Cells("PorcentajeDescuento").Value
                dRow("DescuentoUnitario") = 0 'dgv.Rows(i).Cells("DescuentoUnitario").Value
                dRow("DescuentoUnitarioDiscriminado") = 0 'dgv.Rows(i).Cells("DescuentoUnitarioDiscriminado").Value
                CSistema.CalcularIVA(dRow("IDImpuesto"), dgv.Rows(i).Cells("DescuentoUnitario").Value, True, True, dRow("DescuentoUnitarioDiscriminado"))

                dRow("TotalDescuento") = 0 'dgv.Rows(i).Cells("DescuentoUnitario").Value * CantidadProducto
                dRow("TotalDescuentoDiscriminado") = 0 'dRow("DescuentoUnitarioDiscriminado") * CantidadProducto

                'Totales
                dRow("TotalBruto") = dRow("Total")
                dRow("TotalSinDescuento") = dRow("Total") '- dRow("TotalDescuento")
                dRow("TotalSinImpuesto") = dRow("Total") - dRow("TotalImpuesto")
                dRow("TotalNeto") = 0
                dRow("TotalNetoConDescuentoNeto") = dRow("TotalDiscriminado") '+ dRow("TotalDescuentoDiscriminado")

                'Venta
                dRow("IDTransaccionVenta") = IDTransaccion
                dRow("Comprobante") = dtFactura(0)("Comprobante").ToString

                Try
                    dRow("Saldo") = dtFactura(0)("Saldo")
                    dRow("Cobrado") = dtFactura(0)("Cobrado")
                    dRow("Descontado") = dtFactura(0)("Descontado")

                Catch ex As Exception

                End Try

                'Cantidad Caja
                dRow("Caja") = 0
                dRow("CantidadCaja") = 0

                dRow("IDMotivoDevolucion") = dgv.Rows(i).Cells("Motivo Devolucion").Value
                Dim Motivo As String = ""
                Motivo = dtMotivoDevolucion.Select("ID =" & dgv.Rows(i).Cells("Motivo Devolucion").Value)(0)(1)
                dRow("MotivoDevolucion") = Motivo
                dtDevolucion.Rows.Add(dRow)

            End If
        Next

    End Sub

    Sub CalcularTotal()

        Total = 0
        For i = 0 To dgv.Rows.Count - 1
            If CBool(dgv.Rows(i).Cells("Sel").Value) = True Then
                Total = Total + (dgv.Rows(i).Cells("PrecioUnitarioNeto").Value * dgv.Rows(i).Cells("Cantidad a Devolver").Value)
            End If

        Next

        txtTotal.txt.Text = Total

    End Sub

    Function Validar(ByVal Columna As String) As Boolean

        Select Case Columna
            Case "Cantidad a Devolver"
                If dgv.SelectedRows(0).Cells(Columna).Value > dgv.SelectedRows(0).Cells("SaldoCantidad").Value Then
                    MessageBox.Show("El saldo de este producto es de " & dgv.SelectedRows(0).Cells("SaldoCantidad").Value & " unidades en esta factura, no es posible devolver mas que esta cantidad.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgv.SelectedRows(0).Cells(Columna).Value = dgv.SelectedRows(0).Cells("SaldoCantidad").Value
                    Return False
                End If
            Case "Motivo Devolucion"
                If dgv.SelectedRows(0).Cells(Columna).Value Is Nothing Then
                    MessageBox.Show("Debe seleccionar un motivo de devoluciona por cada producto", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return False
                End If
            Case "PrecioUnitarioNeto"
                If dgv.SelectedRows(0).Cells(Columna).Value > dt.Select("id = " & dgv.SelectedRows(0).Cells("ID").Value)(0)("PrecioUnitarioNeto") Then
                    MessageBox.Show("El monto solo puede ser igual o menor al precio neto", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgv.SelectedRows(0).Cells(Columna).Value = dt.Select("id = " & dgv.SelectedRows(0).Cells("ID").Value)(0)("PrecioUnitarioNeto")
                    Return False
                End If
        End Select

        Return True

    End Function


    Private Sub frmEstablecerDevolucion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        For i = 0 To dgv.Rows.Count - 1
            If CBool(dgv.Rows(i).Cells("Sel").Value) = True Then
                If dgv.Rows(i).Cells("Motivo Devolucion").Value Is Nothing Then
                    MessageBox.Show("Debe seleccionar un motivo de devolucion por cada producto seleccionado!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
            End If
        Next


        Guardar()

        Me.Close()
    End Sub

    Private Sub dgv_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgv.CellEndEdit

        If Validar(dgv.SelectedCells(dgv.CurrentCell.ColumnIndex).OwningColumn.Name) Then
            CalcularTotal()
        End If

    End Sub

    Private Sub dgv_KeyDown(sender As Object, e As KeyEventArgs) Handles dgv.KeyDown, dgv.KeyUp

        If e.KeyCode = Keys.Enter Then

            If dgv.SelectedCells.Count = 0 Then
                Exit Sub
            End If


            Dim RowIndex As Integer = dgv.SelectedCells.Item(0).RowIndex
            Dim ColIndex As Integer = dgv.SelectedCells.Item(0).ColumnIndex

            For Each oRow As DataGridViewRow In dgv.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("Sel").Value = False Then
                        oRow.Cells("Sel").Value = True
                    Else
                        Exit Sub
                    End If


                    For Each oCol As DataGridViewColumn In dgv.Columns

                        If dgv.SelectedCells.Item(0).OwningColumn.Name = "Sel" Then
                            dgv.Rows(oRow.Index).Cells("Cantidad a Devolver").Selected = True
                        End If

                        If dgv.SelectedCells.Item(0).OwningColumn.Name = "Cantidad a Devolver" Then
                            dgv.Rows(oRow.Index).Cells("Motivo Devolucion").Selected = True
                        End If
                        Exit For
                    Next

                End If

            Next

            CalcularTotal()

            ' dgv.Update()

            ' Your code here
            e.SuppressKeyPress = True

            Exit Sub

        End If
        CalcularTotal()

    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmEstablecerDevolucion_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmAgregarFacturaAcuerdo
    Dim CSistema As New CSistema

    Public Property IDTransaccion As Integer
    Public Property IDCliente As Integer
    Public Property IDProducto As Integer
    Public Property Monto As Decimal


    Public Property IDMoneda As Integer
    Public Property Decimales As Boolean
    Public Property Aplicar As Boolean = False
    Public Property Pedido As Boolean = False
    Public Property UnaSolaFactura As Boolean
    Public Property ImportePedido As Decimal

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    'Dim dt As DataTable
    Dim Plazo As Integer
    Dim dtDetalle As New DataTable


    'FUNCIONES
    Sub Inicializar()
        'Plazo = CSistema.ExecuteScalar("Select top(1) PlazoDevolucionDesdeEmisionFactura from Configuraciones where idsucursal = " & IDSucursal)
        CargarInformacion()
    End Sub

    Sub CargarInformacion()
        txtProducto.Conectar(True, False)
    End Sub

    Private Sub txtProducto_ItemSeleccionado(sender As Object, e As EventArgs) Handles txtProducto.ItemSeleccionado
        txtMonto.Focus()
    End Sub

    Private Sub frmAgregarFacturaAcuerdo_Load(sender As Object, e As EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Public Function txtMonto_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtMonto.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            If txtProducto.Seleccionado = False Then
                txtProducto.Focus()
                MessageBox.Show("Favor ingrese un producto", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Function
            End If
            'If txtMonto.txt.Text = "0" Then
            '    txtMonto.Focus()
            '    MessageBox.Show("Favor ingrese una monto", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    Exit Sub
            'End If

            If Not IsNumeric(txtMonto.txt.Text) Then
                txtMonto.SetValue(0)
                txtMonto.txt.Text = "0"
            End If

        End If


        CargarFacturaACuerdo()
        Me.Close()

    End Function

    Function CargarFacturaACuerdo()

        Dim frm As New SeleccionarFacturaAcuerdo
        'frm.IDCliente = txtCliente.Registro("ID")
        SeleccionarFacturaAcuerdo.StartPosition = FormStartPosition.CenterScreen
        'SeleccionarFacturaSegunFiltro.IDDeposito = cbxDeposito.cbx.SelectedValue
        SeleccionarFacturaAcuerdo.IDProducto = txtProducto.Registro("ID")
        SeleccionarFacturaAcuerdo.IDCliente = IDCliente
        'SeleccionarFacturaAcuerdo.IDSucursal = txtSucursal.Registro("ID")
        'SeleccionarFacturaSegunFiltro.CantidadADevolver = CDec(txtCantidad.ObtenerValor)
        'frm.IDProducto = txtProducto.Registro("ID")
        frm.Monto = txtMonto.ObtenerValor
        'frm.dt = dtDetalle.Clone
        'SeleccionarFacturaSegunFiltro.IdSucursalCliente = IdSucursalCliente
        'SeleccionarFacturaSegunFiltro.IDFamilia = txtProducto.Registro("IDSubLinea")
        'SeleccionarFacturaSegunFiltro.IDPresentacion = txtProducto.Registro("IDPresentacion")
        'SeleccionarFacturaSegunFiltro.TSucursales = chkTSucursales.Checked
        SeleccionarFacturaAcuerdo.ShowDialog()

        Return dt

        'frm.Pedido = True
        'frm.UnaSolaFactura = UnaSolaFactura

        Dim CantidadPedidoNCPendientes As Integer = 0
        'Dim dt As DataTable = frm.dt.Copy()
        'dt.Clear()
        For Each dRow As DataRow In dt.Select("Sel = 'True'")
            CantidadPedidoNCPendientes = CSistema.ExecuteScalar("Select CantidadPedidoNCPendientes from dbo.FNotaCreditoPorVenta(" & dRow("IDTransaccion") & ")")

            If CantidadPedidoNCPendientes > 0 Then
                MessageBox.Show("El documento ya tiene Pedidos de NC pendientes de Procesar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                'CargarComprobante()
                Exit Function
            End If
            dt.ImportRow(dRow)
        Next





        If dtDetalle.Rows.Count = 0 Then
            dtDetalle = dt
        Else
            For Each oRow As DataRow In dt.Rows


                Dim newRow As DataRow = dtDetalle.NewRow()

                If dtDetalle.Select("IDTransaccionVenta =" & oRow("IDTransaccionVenta") & " and IDProducto = " & oRow("IDProducto")).Count() > 0 Then
                    Dim Cantidad As Integer = dtDetalle.Select("IDTransaccionVenta =" & oRow("IDTransaccionVenta") & " and IDProducto = " & oRow("IDProducto"))(0)("Cantidad")
                    dtDetalle.Select("IDTransaccionVenta =" & oRow("IDTransaccionVenta") & " and IDProducto = " & oRow("IDProducto"))(0)("Cantidad") = Cantidad + oRow("Cantidad")
                Else

                    For i = 0 To dtDetalle.Columns.Count - 1
                        newRow(i) = oRow(i)
                    Next
                    dtDetalle.Rows.Add(newRow)

                End If

            Next

            Dim j As Integer = 0
            For Each dRow As DataRow In dtDetalle.Rows
                dRow("ID") = j
                j = j + 1
            Next
        End If


        'ListarDetalle()




    End Function


End Class
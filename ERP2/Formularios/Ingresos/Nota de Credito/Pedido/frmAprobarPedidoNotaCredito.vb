﻿Imports System.Threading
Imports ERP.Reporte

Public Class frmAprobarPedidoNotaCredito
    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim PorcentajeAsignado As Decimal = 0
    Dim ImporteAsignado As Decimal = 0

    'PROPIEDADES

    'VARIABLES
    Dim tProcesar As Thread
    Dim vProcesar As Boolean = False

    Enum ENUMOperacionPedido
        APROBAR = 1
        RECHAZAR = 2
        RECUPERAR = 3
        ANULAR = 4
    End Enum

    'FUNCIONES
    Sub Inicializar()

        Me.KeyPreview = True

        'Varios
        CheckForIllegalCrossThreadCalls = False

        cbxGrupos.SelectedIndex = 0
        cbxOrden.SelectedIndex = 0

        'Funciones
        ListarGrupos()

        'Fecha facturacion
        If IsDate(VGFechaFacturacion) = False Then
            VGFechaFacturacion = VGFechaHoraSistema
        End If

        Dim Fecha As Date = VGFechaFacturacion

        txtFechaDesde.Hoy()
        txtFechaHasta.UnaSemana()


        ListarPedidos()
    End Sub

    Sub ListarGrupos()

        lvGrupo.Items.Clear()
        lvPedidos.Items.Clear()

        Dim SQL As String = ""
        Dim INDEX As Integer = cbxGrupos.SelectedIndex

        If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        End If

        'If INDEX = 0 Then
        '    SQL = "Select 'Tipo'='Listar Todos'"
        'End If


        'CSistema.SqlToLv(lvGrupo, SQL)
        ' Dim objListItem As New ListViewItem
        Dim dtGrupo As New DataTable("Grupos")

        dtGrupo.Columns.Add("Grupo", Type.GetType("System.String"))

        Dim newRow As DataRow = dtGrupo.NewRow()
        newRow(0) = "Listar Todos"
        dtGrupo.Rows.Add(newRow)
        Dim newRowb As DataRow = dtGrupo.NewRow()
        newRowb(0) = "PENDIENTE"
        dtGrupo.Rows.Add(newRowb)
        Dim newRowc As DataRow = dtGrupo.NewRow()
        newRowc(0) = "APROBADO"
        dtGrupo.Rows.Add(newRowc)
        Dim newRowd As DataRow = dtGrupo.NewRow()
        newRowd(0) = "RECHAZADO"
        dtGrupo.Rows.Add(newRowd)
        Dim newRowe As DataRow = dtGrupo.NewRow()
        newRowe(0) = "IMPRESO"
        dtGrupo.Rows.Add(newRowe)
        Dim newRowf As DataRow = dtGrupo.NewRow()
        newRowf(0) = "ANULADO"
        dtGrupo.Rows.Add(newRowf)


        CSistema.dtToLv(lvGrupo, dtGrupo)


        For i = 0 To lvGrupo.Columns.Count - 1
            lvGrupo.Columns(i).AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize)
        Next


    End Sub

    Sub SeleccionarGrupo()
        ListarPedidos()
    End Sub

    Sub ListarPedidos()

        If lvGrupo.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim ID As String = lvGrupo.SelectedItems(0).Text
        Dim OrderBy As String = ""
        Dim Campo As String = ""


        'If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
        '    vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        'End If
        Select Case lvGrupo.SelectedItems.Item(0).Text
            Case "Listar Todos"
                SQL = "Select IDTransaccion, Suc, 'N°'=NroOperacion, 'Fecha'=Convert(Date,FechaTransaccion), 'FechaProcesar'=Fecha,'Motivo'=MotivoNotaCredito, 'Sub Motivo'=SubMotivoNotaCredito,  'Cliente'=Concat(Cliente, '(',Referencia,')'), 'Descuento Previo'=TotalDescuentoPrevio ,'Total Descuento' = TotalDescuentoComercial, 'Total Venta'=TotalVenta, Porcentaje, 'Solicitante'=NombreUsuario, 'Estado'=EstadoNC, 'Aprobado por'= NombreUsuarioAprobado, FechaAprobado From VPedidoNotaCreditoAutorizador Where cast(Fecha as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = False
                btnRechazar.Enabled = False
                btnAprobar.Text = "Aprobar"
                btnRechazar.Text = "Rechazar"
            Case "APROBADO"
                SQL = "Select IDTransaccion, Suc, 'N°'=NroOperacion, 'Fecha'=Convert(Date,FechaTransaccion), 'FechaProcesar'=Fecha,'Motivo'=MotivoNotaCredito, 'Sub Motivo'=SubMotivoNotaCredito,  'Cliente'=Concat(Cliente, '(',Referencia,')'), 'Descuento Previo'=TotalDescuentoPrevio ,'Total Descuento' = TotalDescuentoComercial, 'Total Venta'=TotalVenta, Porcentaje, 'Solicitante'=NombreUsuario, 'Estado'=EstadoNC, 'Aprobado por'= NombreUsuarioAprobado, FechaAprobado From VPedidoNotaCreditoAutorizador Where Anulado = 0 AND EstadoNC = 'APROBADO' AND ProcesadoNC='PENDIENTE' AND cast(Fecha as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = False
                btnAprobar.Text = "Recuperar"
            Case "RECHAZADO"
                SQL = "Select IDTransaccion, Suc, 'N°'=NroOperacion, 'Fecha'=Convert(Date,FechaTransaccion), 'FechaProcesar'=Fecha,'Motivo'=MotivoNotaCredito, 'Sub Motivo'=SubMotivoNotaCredito,  'Cliente'=Concat(Cliente, '(',Referencia,')'), 'Descuento Previo'=TotalDescuentoPrevio ,'Total Descuento' = TotalDescuentoComercial, 'Total Venta'=TotalVenta, Porcentaje, 'Solicitante'=NombreUsuario, 'Estado'=EstadoNC, 'Aprobado por'= NombreUsuarioAprobado, FechaAprobado From VPedidoNotaCreditoAutorizador Where Anulado = 0 AND EstadoNC = 'RECHAZADO' AND cast(Fecha as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = True
                btnAprobar.Text = "Recuperar"
                btnRechazar.Text = "Anular"
            Case "PENDIENTE"
                SQL = "Select IDTransaccion, Suc, 'N°'=NroOperacion, 'Fecha'=Convert(Date,FechaTransaccion), 'FechaProcesar'=Fecha,'Motivo'=MotivoNotaCredito, 'Sub Motivo'=SubMotivoNotaCredito,  'Cliente'=Concat(Cliente, '(',Referencia,')'), 'Descuento Previo'=TotalDescuentoPrevio ,'Total Descuento' = TotalDescuentoComercial, 'Total Venta'=TotalVenta, Porcentaje, 'Solicitante'=NombreUsuario, 'Estado'=EstadoNC, 'Aprobado por'= NombreUsuarioAprobado, FechaAprobado From VPedidoNotaCreditoAutorizador Where Anulado = 0 AND EstadoNC = 'PENDIENTE' AND cast(Fecha as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = True
                btnAprobar.Text = "Aprobar"
                btnRechazar.Text = "Rechazar"
            Case "IMPRESO"
                SQL = "Select IDTransaccion, Suc, 'N°'=NroOperacion, 'Fecha'=Convert(Date,FechaTransaccion), 'FechaProcesar'=Fecha,'Motivo'=MotivoNotaCredito, 'Sub Motivo'=SubMotivoNotaCredito,  'Cliente'=Concat(Cliente, '(',Referencia,')'), 'Descuento Previo'=TotalDescuentoPrevio ,'Total Descuento' = TotalDescuentoComercial, 'Total Venta'=TotalVenta, Porcentaje, 'Solicitante'=NombreUsuario, 'Estado'=EstadoNC, 'Aprobado por'= NombreUsuarioAprobado, FechaAprobado From VPedidoNotaCreditoAutorizador Where Anulado = 0 AND ProcesadoNC='PROCESADO' AND cast(Fecha as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = False
                btnRechazar.Enabled = False
                btnAprobar.Text = "Aprobar"
                btnRechazar.Text = "Rechazar"
            Case "ANULADO"
                SQL = "Select IDTransaccion, Suc, 'N°'=NroOperacion, 'Fecha'=Convert(Date,FechaTransaccion), 'FechaProcesar'=Fecha,'Motivo'=MotivoNotaCredito, 'Sub Motivo'=SubMotivoNotaCredito,  'Cliente'=Concat(Cliente, '(',Referencia,')'), 'Descuento Previo'=TotalDescuentoPrevio ,'Total Descuento' = TotalDescuentoComercial, 'Total Venta'=TotalVenta, Porcentaje, 'Solicitante'=NombreUsuario, 'Estado'=EstadoNC, 'Aprobado por'= NombreUsuarioAprobado, FechaAprobado From VPedidoNotaCreditoAutorizador Where Anulado = 1 AND cast(Fecha as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = False
                btnRechazar.Enabled = False
                btnAprobar.Text = "Aprobar"
                btnRechazar.Text = "Rechazar"
        End Select

        OrderBy = " Order By [" & cbxOrden.Text & "]"

        Listar(SQL & OrderBy)

    End Sub

    Sub Listar(ByVal SQL As String)

        CSistema.SqlToLv(lvPedidos, SQL)

        'Seleccionar todos
        lvPedidos.Columns(10).TextAlign = HorizontalAlignment.Right
        lvPedidos.Columns(9).TextAlign = HorizontalAlignment.Right
        lvPedidos.Columns(8).TextAlign = HorizontalAlignment.Right

        For Each item As ListViewItem In lvPedidos.Items
            item.SubItems(8).Text = CSistema.FormatoMoneda(item.SubItems(8).Text)
            item.SubItems(9).Text = CSistema.FormatoMoneda(item.SubItems(9).Text)
            item.SubItems(10).Text = CSistema.FormatoMoneda(item.SubItems(10).Text)
            item.SubItems(3).Text = CDate(item.SubItems(3).Text).ToShortDateString
            item.SubItems(4).Text = CDate(item.SubItems(4).Text).ToShortDateString
            Try
                If item.SubItems(13).Text <> "PENDIENTE" Then
                    item.SubItems(15).Text = CDate(item.SubItems(15).Text).ToShortDateString
                End If
            Catch ex As Exception

            End Try

            If item.SubItems(8).Text <> "0" Then
                item.BackColor = Color.Red
            End If


            'If item.SubItems(10).Text = "APROBADO" Then
            '    item.Checked = True
            'End If
        Next


        txtCantidadPedidos.txt.Text = lvPedidos.Items.Count

    End Sub

    Sub AprobarRechazarPedido(ByVal Operacion As ENUMOperacionPedido)


        Dim Observacion As String = ""

        Dim IDSubMotivoNC As Integer = 0
        'Esta mal, verificar para pasar IDSubMotivo
        For Each item As ListViewItem In lvPedidos.Items
            If item.Checked Then

                IDSubMotivoNC = CSistema.ExecuteScalar("Select Isnull((Select IDSubMotivoNotaCredito from PedidoNotaCredito where idtransaccion = " & item.SubItems(0).Text & "),-1)")
                If IDSubMotivoNC = -1 Then
                    Dim param(-1) As SqlClient.SqlParameter

                    CSistema.SetSQLParameter(param, "@IDTransaccion", item.SubItems(0).Text, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@ObservacionAutorizador", Observacion, ParameterDirection.Input)

                    'CSistema.SetSQLParameter(param, "@Estado", item.Checked, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)
                    CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

                    'Informacion de Salida
                    CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
                    CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

                    Dim MensajeRetorno As String = ""

                    '@Procesado (de la sp) es Verdadero
                    If CSistema.ExecuteStoreProcedure(param, "SpAprobarDevolucionSinNotaCredito", False, False, MensajeRetorno) = True Then
                        MessageBox.Show("Se proceso correctamente la Operacion N° '" & item.SubItems(2).Text & "' donde el Sub Motivo es '" & item.SubItems(6).Text & "'. ", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else '@Procesado (de la sp) es Falso
                        MessageBox.Show("No se ha procesado correctamente la Operacion N° '" & item.SubItems(2).Text & "' - " & MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    End If

                Else
                    If CSistema.ExecuteScalar("Select dbo.FExistenciaUsuarioAutorizarNotaCredito(" & vgIDUsuario & " , " & IDSubMotivoNC & ")") = True Then

                        If item.SubItems(1).Text = "DEVOLUCION" And Operacion = ENUMOperacionPedido.APROBAR Then

                        Else
                            Observacion = InputBox("Ingrese una Observacion", "Atencion", "")
                        End If

                        Dim param(-1) As SqlClient.SqlParameter

                        CSistema.SetSQLParameter(param, "@IDTransaccion", item.SubItems(0).Text, ParameterDirection.Input)
                        CSistema.SetSQLParameter(param, "@ObservacionAutorizador", Observacion, ParameterDirection.Input)

                        'CSistema.SetSQLParameter(param, "@Estado", item.Checked, ParameterDirection.Input)
                        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
                        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
                        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)
                        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

                        'Informacion de Salida
                        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
                        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

                        Dim MensajeRetorno As String = ""

                        '@Procesado (de la sp) es Verdadero
                        If CSistema.ExecuteStoreProcedure(param, "SpAprobarPedidoNotaCredito", False, False, MensajeRetorno) = True Then
                            MessageBox.Show("Se proceso correctamente la Operacion N° '" & item.SubItems(2).Text & "' donde el Sub Motivo es '" & item.SubItems(6).Text & "'. ", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Else '@Procesado (de la sp) es Falso
                            MessageBox.Show("No se ha procesado correctamente la Operacion N° '" & item.SubItems(2).Text & "' - " & MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        End If
                    Else 'Si no tiene permisos suficientes
                        MsgBox("Su usuario no cuenta con los permisos suficientes para realizar cambios en la operación N° '" & item.SubItems(2).Text & "' donde el Sub Motivo es '" & item.SubItems(6).Text & "'. ", MsgBoxStyle.Exclamation, "Atención!")
                    End If
                End If
            End If

        Next

        MsgBox("Registros procesados", MsgBoxStyle.Information, " ")

    End Sub

    Sub Detener()
        Try
            If vProcesar = False Then
                Exit Sub
            End If

            tProcesar.Abort()
            vProcesar = False
        Catch ex As Exception

        End Try

        Thread.Sleep(1000)

    End Sub

    Sub VerPedido()





        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        For Each item As ListViewItem In lvPedidos.SelectedItems
            item.ImageIndex = 1

            If item.SubItems(6).Text = "DEVOLUCION SIN NOTA DE CREDITO" Then
                Dim frm As New frmDevolucionSinNotaCredido
                frm.IDTransaccion = item.SubItems(0).Text
                frm.Visualizar = True
                frm.Show()
                frm.EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
                frm.CargarOperacion(item.SubItems(0).Text) ' IDTransaccion
            Else
                Dim frm As New frmPedidoNotaCredito
                frm.IDTransaccion = item.SubItems(0).Text
                frm.Visualizar = True
                frm.Show()
                frm.EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
                frm.CargarOperacion(item.SubItems(0).Text) ' IDTransaccion
            End If


        Next
    End Sub

    Function ControlarPorcentaje(ByVal Porcentaje As Decimal, ByVal Importe As Decimal) As Boolean
        ControlarPorcentaje = True
        If (Porcentaje > PorcentajeAsignado) And (Importe > ImporteAsignado) Then
            Return False
        End If
    End Function

    Sub SeleccionarTodo()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = True
        Next
    End Sub

    Sub QuitarSeleccion()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = False
        Next
    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        Select Case e.KeyCode
            Case Keys.Enter
                CSistema.SelectNextControl(Me, e.KeyCode)
        End Select

    End Sub

    Sub EliminarPedido()

        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        lvPedidos.SelectedItems(0).Remove()

    End Sub

    Sub Exportar()
        CSistema.LvToExcel(lvPedidos)
    End Sub

    Private Sub frmAprobarPedido_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Detener()
    End Sub

    Private Sub frmAprobarPedido_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        ManejarTecla(e)
    End Sub

    Private Sub frmAprobarPedido_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnActualizarGrupo_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarGrupo.Click
        ListarGrupos()
    End Sub

    Private Sub cbxGrupos_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxGrupos.SelectedIndexChanged
        ListarGrupos()
    End Sub

    Private Sub lvGrupo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lvGrupo.SelectedIndexChanged
        SeleccionarGrupo()
    End Sub

    Private Sub btnProcesar_Click(sender As System.Object, e As System.EventArgs) Handles btnAprobar.Click
        Select Case lvGrupo.SelectedItems.Item(0).Text
            Case "PENDIENTE"
                AprobarRechazarPedido(ENUMOperacionPedido.APROBAR)
            Case "RECHAZADO"
                AprobarRechazarPedido(ENUMOperacionPedido.RECUPERAR)
            Case "APROBADO"
                AprobarRechazarPedido(ENUMOperacionPedido.RECUPERAR)

        End Select

        ListarPedidos()
    End Sub

    Private Sub btnSeleccionarTodos_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionarTodos.Click
        SeleccionarTodo()
    End Sub

    Private Sub ToolStripButton1_Click(sender As System.Object, e As System.EventArgs)
        QuitarSeleccion()
    End Sub

    Private Sub btnActualizarPedidos_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarPedidos.Click
        ListarPedidos()
    End Sub

    Private Sub btnVerPedido_Click(sender As System.Object, e As System.EventArgs) Handles btnVerPedido.Click
        VerPedido()
    End Sub

    'Private Sub ProcesarPedidoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
    '    AprobarPedido2()
    '    ListarPedidos()
    'End Sub

    Private Sub btnRechazar_Click(sender As System.Object, e As System.EventArgs) Handles btnRechazar.Click
        Select Case lvGrupo.SelectedItems.Item(0).Text
            Case "PENDIENTE"
                AprobarRechazarPedido(ENUMOperacionPedido.RECHAZAR)
            Case "RECHAZADO"
                AprobarRechazarPedido(ENUMOperacionPedido.ANULAR)
        End Select

        ListarPedidos()

    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Exportar()
    End Sub

    '10-06-2021 - SC - Actualiza datos
    Sub frmAprobarPedidoNotaCredito_Activate()
        Me.Refresh()
    End Sub
End Class
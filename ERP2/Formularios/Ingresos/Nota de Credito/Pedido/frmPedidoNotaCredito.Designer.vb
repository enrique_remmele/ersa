﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPedidoNotaCredito
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPedidoNotaCredito))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.VerDetalleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportarAExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.gbxComprobante = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNroNotaCredito = New ERP.ocxTXTString()
        Me.chkProcesadoNC = New ERP.ocxCHK()
        Me.cbxSubMotivo = New ERP.ocxCBX()
        Me.lblSubMotivo = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblDejarpendiente = New System.Windows.Forms.Label()
        Me.lblNroSolicitudCliente = New System.Windows.Forms.Label()
        Me.lblAcuerdo = New System.Windows.Forms.Label()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.pnlDescuento = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCondicion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colVencimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCobrado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDescontado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCancelar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtTotalDescuento = New ERP.ocxTXTNumeric()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtCantidadCobrado = New ERP.ocxTXTNumeric()
        Me.txtDescontados = New ERP.ocxTXTNumeric()
        Me.lblTotalCobrado = New System.Windows.Forms.Label()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.lblDeudaTotal = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lklAgregarComprobante = New System.Windows.Forms.LinkLabel()
        Me.lklAgregarComprobanteDescuento = New System.Windows.Forms.LinkLabel()
        Me.lklEliminarComprobanteDescuento = New System.Windows.Forms.LinkLabel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lklAgregarDetalleDescuento = New System.Windows.Forms.LinkLabel()
        Me.lklEliminarDetalleDescuento = New System.Windows.Forms.LinkLabel()
        Me.lvListaDescuento = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.pnlDevolucion = New System.Windows.Forms.Panel()
        Me.txtProductoDevolucion = New ERP.ocxTXTProducto()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.colID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colReferencia = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colDescripcion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colComprobante = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colMotio = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colCantidad = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colPrecioUnitario = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColTotal = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnCargarFacturaDevolucion = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtPrecioUnitario = New ERP.ocxTXTNumeric()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.chkTSucursales = New System.Windows.Forms.CheckBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnAnula = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.txtNumeroSolicitudCliente = New ERP.ocxTXTString()
        Me.txtFechaAutorizacion = New ERP.ocxTXTDate()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtUsuarioAutorizador = New ERP.ocxTXTString()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtEstadoAutorizacion = New ERP.ocxTXTString()
        Me.txtObservacionAutorizacion = New ERP.ocxTXTString()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtTelefono = New ERP.ocxTXTString()
        Me.lblTelefono = New System.Windows.Forms.Label()
        Me.lblDireccion = New System.Windows.Forms.Label()
        Me.chkAplicar = New ERP.ocxCHK()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.rdbDescuentos = New System.Windows.Forms.RadioButton()
        Me.rdbDevolucion = New System.Windows.Forms.RadioButton()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblDeposito = New System.Windows.Forms.Label()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.OcxCotizacion1 = New ERP.ocxCotizacion()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDireccion = New ERP.ocxTXTString()
        Me.cbxComprobanteImpresion = New ERP.ocxCBX()
        Me.cbxAcuerdo = New ERP.ocxCBX()
        Me.txtAcuerdo = New ERP.ocxTXTString()
        Me.TxtComprobanteImpresion = New ERP.ocxTXTString()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.OcxImpuesto1 = New ERP.ocxImpuesto()
        Me.btnProductosRelacionados = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.gbxComprobante.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.pnlDescuento.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.pnlDevolucion.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.gbxCabecera.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VerDetalleToolStripMenuItem, Me.ExportarAExcelToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(158, 48)
        '
        'VerDetalleToolStripMenuItem
        '
        Me.VerDetalleToolStripMenuItem.Name = "VerDetalleToolStripMenuItem"
        Me.VerDetalleToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.VerDetalleToolStripMenuItem.Text = "Ver Detalle"
        '
        'ExportarAExcelToolStripMenuItem
        '
        Me.ExportarAExcelToolStripMenuItem.Name = "ExportarAExcelToolStripMenuItem"
        Me.ExportarAExcelToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.ExportarAExcelToolStripMenuItem.Text = "Exportar a Excel"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'gbxComprobante
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.gbxComprobante, 2)
        Me.gbxComprobante.Controls.Add(Me.Label1)
        Me.gbxComprobante.Controls.Add(Me.txtNroNotaCredito)
        Me.gbxComprobante.Controls.Add(Me.chkProcesadoNC)
        Me.gbxComprobante.Controls.Add(Me.cbxSubMotivo)
        Me.gbxComprobante.Controls.Add(Me.lblSubMotivo)
        Me.gbxComprobante.Controls.Add(Me.cbxSucursal)
        Me.gbxComprobante.Controls.Add(Me.txtID)
        Me.gbxComprobante.Controls.Add(Me.lblOperacion)
        Me.gbxComprobante.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxComprobante.Location = New System.Drawing.Point(0, 0)
        Me.gbxComprobante.Margin = New System.Windows.Forms.Padding(0)
        Me.gbxComprobante.Name = "gbxComprobante"
        Me.gbxComprobante.Size = New System.Drawing.Size(794, 42)
        Me.gbxComprobante.TabIndex = 0
        Me.gbxComprobante.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(585, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 13)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "Nota de Credito:"
        '
        'txtNroNotaCredito
        '
        Me.txtNroNotaCredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtNroNotaCredito.Indicaciones = Nothing
        Me.txtNroNotaCredito.Location = New System.Drawing.Point(675, 9)
        Me.txtNroNotaCredito.Multilinea = False
        Me.txtNroNotaCredito.Name = "txtNroNotaCredito"
        Me.txtNroNotaCredito.Size = New System.Drawing.Size(105, 24)
        Me.txtNroNotaCredito.SoloLectura = True
        Me.txtNroNotaCredito.TabIndex = 39
        Me.txtNroNotaCredito.TabStop = False
        Me.txtNroNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroNotaCredito.Texto = ""
        '
        'chkProcesadoNC
        '
        Me.chkProcesadoNC.BackColor = System.Drawing.Color.Transparent
        Me.chkProcesadoNC.Color = System.Drawing.Color.Empty
        Me.chkProcesadoNC.Location = New System.Drawing.Point(496, 12)
        Me.chkProcesadoNC.Name = "chkProcesadoNC"
        Me.chkProcesadoNC.Size = New System.Drawing.Size(75, 19)
        Me.chkProcesadoNC.SoloLectura = True
        Me.chkProcesadoNC.TabIndex = 38
        Me.chkProcesadoNC.Texto = "Procesado"
        Me.chkProcesadoNC.Valor = False
        '
        'cbxSubMotivo
        '
        Me.cbxSubMotivo.CampoWhere = Nothing
        Me.cbxSubMotivo.CargarUnaSolaVez = False
        Me.cbxSubMotivo.DataDisplayMember = Nothing
        Me.cbxSubMotivo.DataFilter = Nothing
        Me.cbxSubMotivo.DataOrderBy = Nothing
        Me.cbxSubMotivo.DataSource = Nothing
        Me.cbxSubMotivo.DataValueMember = Nothing
        Me.cbxSubMotivo.dtSeleccionado = Nothing
        Me.cbxSubMotivo.FormABM = Nothing
        Me.cbxSubMotivo.Indicaciones = Nothing
        Me.cbxSubMotivo.Location = New System.Drawing.Point(267, 11)
        Me.cbxSubMotivo.Name = "cbxSubMotivo"
        Me.cbxSubMotivo.SeleccionMultiple = False
        Me.cbxSubMotivo.SeleccionObligatoria = True
        Me.cbxSubMotivo.Size = New System.Drawing.Size(195, 21)
        Me.cbxSubMotivo.SoloLectura = False
        Me.cbxSubMotivo.TabIndex = 9
        Me.cbxSubMotivo.Texto = ""
        '
        'lblSubMotivo
        '
        Me.lblSubMotivo.AutoSize = True
        Me.lblSubMotivo.Location = New System.Drawing.Point(201, 15)
        Me.lblSubMotivo.Name = "lblSubMotivo"
        Me.lblSubMotivo.Size = New System.Drawing.Size(64, 13)
        Me.lblSubMotivo.TabIndex = 8
        Me.lblSubMotivo.Text = "Sub Motivo:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(63, 11)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(69, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 7
        Me.cbxSucursal.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(132, 11)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(59, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 6
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(5, 15)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 5
        Me.lblOperacion.Text = "Operacion:"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'lblDejarpendiente
        '
        Me.lblDejarpendiente.AutoSize = True
        Me.lblDejarpendiente.Location = New System.Drawing.Point(614, 76)
        Me.lblDejarpendiente.Name = "lblDejarpendiente"
        Me.lblDejarpendiente.Size = New System.Drawing.Size(149, 13)
        Me.lblDejarpendiente.TabIndex = 12
        Me.lblDejarpendiente.Text = "Dejar pendiente de Aplicacion"
        Me.ToolTip1.SetToolTip(Me.lblDejarpendiente, resources.GetString("lblDejarpendiente.ToolTip"))
        '
        'lblNroSolicitudCliente
        '
        Me.lblNroSolicitudCliente.AutoSize = True
        Me.lblNroSolicitudCliente.Location = New System.Drawing.Point(585, 101)
        Me.lblNroSolicitudCliente.Name = "lblNroSolicitudCliente"
        Me.lblNroSolicitudCliente.Size = New System.Drawing.Size(108, 13)
        Me.lblNroSolicitudCliente.TabIndex = 52
        Me.lblNroSolicitudCliente.Text = "Nro. Solicitud Cliente:"
        Me.ToolTip1.SetToolTip(Me.lblNroSolicitudCliente, resources.GetString("lblNroSolicitudCliente.ToolTip"))
        '
        'lblAcuerdo
        '
        Me.lblAcuerdo.AutoSize = True
        Me.lblAcuerdo.Location = New System.Drawing.Point(585, 101)
        Me.lblAcuerdo.Name = "lblAcuerdo"
        Me.lblAcuerdo.Size = New System.Drawing.Size(108, 13)
        Me.lblAcuerdo.TabIndex = 56
        Me.lblAcuerdo.Text = "Nro. Acuerdo Cliente:"
        Me.ToolTip1.SetToolTip(Me.lblAcuerdo, resources.GetString("lblAcuerdo.ToolTip"))
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 436.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.GroupBox1, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel1, 0, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.gbxComprobante, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.gbxCabecera, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.OcxImpuesto1, 1, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.btnProductosRelacionados, 0, 3)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 6
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 176.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 96.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(794, 658)
        Me.TableLayoutPanel2.TabIndex = 3
        '
        'GroupBox1
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.GroupBox1, 2)
        Me.GroupBox1.Controls.Add(Me.pnlDescuento)
        Me.GroupBox1.Controls.Add(Me.pnlDevolucion)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(0, 218)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(794, 252)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'pnlDescuento
        '
        Me.pnlDescuento.Controls.Add(Me.TableLayoutPanel3)
        Me.pnlDescuento.Location = New System.Drawing.Point(420, 10)
        Me.pnlDescuento.Name = "pnlDescuento"
        Me.pnlDescuento.Size = New System.Drawing.Size(366, 233)
        Me.pnlDescuento.TabIndex = 3
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.46056!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.53944!))
        Me.TableLayoutPanel3.Controls.Add(Me.dgw, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel2, 1, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.FlowLayoutPanel5, 1, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.FlowLayoutPanel4, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.FlowLayoutPanel2, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.lvListaDescuento, 0, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 4
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(366, 233)
        Me.TableLayoutPanel3.TabIndex = 21
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colSel, Me.DataGridViewTextBoxColumn1, Me.colTipo, Me.colCondicion, Me.colVencimiento, Me.DataGridViewTextBoxColumn2, Me.colCobrado, Me.colDescontado, Me.colSaldo, Me.colImporte, Me.colCancelar})
        Me.TableLayoutPanel3.SetColumnSpan(Me.dgw, 2)
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgw.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgw.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgw.Enabled = False
        Me.dgw.Location = New System.Drawing.Point(3, 115)
        Me.dgw.Name = "dgw"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.RowHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(360, 83)
        Me.dgw.TabIndex = 4
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colSel
        '
        Me.colSel.HeaderText = "Sel"
        Me.colSel.Name = "colSel"
        Me.colSel.ReadOnly = True
        Me.colSel.Visible = False
        Me.colSel.Width = 30
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn1.HeaderText = "Comprobante"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.ToolTipText = "Comprobante de Venta"
        '
        'colTipo
        '
        Me.colTipo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colTipo.DefaultCellStyle = DataGridViewCellStyle4
        Me.colTipo.HeaderText = "Tipo"
        Me.colTipo.Name = "colTipo"
        Me.colTipo.ReadOnly = True
        Me.colTipo.ToolTipText = "Tipo de Comprobante"
        '
        'colCondicion
        '
        Me.colCondicion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colCondicion.DefaultCellStyle = DataGridViewCellStyle5
        Me.colCondicion.HeaderText = "Cond."
        Me.colCondicion.Name = "colCondicion"
        Me.colCondicion.ReadOnly = True
        '
        'colVencimiento
        '
        Me.colVencimiento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colVencimiento.DefaultCellStyle = DataGridViewCellStyle6
        Me.colVencimiento.HeaderText = "Venc."
        Me.colVencimiento.Name = "colVencimiento"
        Me.colVencimiento.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = "0"
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewTextBoxColumn2.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'colCobrado
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = "0"
        Me.colCobrado.DefaultCellStyle = DataGridViewCellStyle8
        Me.colCobrado.HeaderText = "Cobrado"
        Me.colCobrado.Name = "colCobrado"
        Me.colCobrado.ReadOnly = True
        Me.colCobrado.Visible = False
        Me.colCobrado.Width = 90
        '
        'colDescontado
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        DataGridViewCellStyle9.NullValue = "0"
        Me.colDescontado.DefaultCellStyle = DataGridViewCellStyle9
        Me.colDescontado.HeaderText = "Descontado"
        Me.colDescontado.Name = "colDescontado"
        Me.colDescontado.ReadOnly = True
        Me.colDescontado.Visible = False
        Me.colDescontado.Width = 90
        '
        'colSaldo
        '
        Me.colSaldo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        DataGridViewCellStyle10.NullValue = "0"
        Me.colSaldo.DefaultCellStyle = DataGridViewCellStyle10
        Me.colSaldo.HeaderText = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.ReadOnly = True
        Me.colSaldo.Visible = False
        '
        'colImporte
        '
        Me.colImporte.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = "0"
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle11
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        Me.colImporte.ToolTipText = "Importe Cobrado"
        '
        'colCancelar
        '
        Me.colCancelar.HeaderText = "Cancel"
        Me.colCancelar.Name = "colCancelar"
        Me.colCancelar.ReadOnly = True
        Me.colCancelar.ToolTipText = "Cancelar manualmente el comprobante"
        Me.colCancelar.Visible = False
        Me.colCancelar.Width = 35
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txtTotalDescuento)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(122, 89)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(244, 23)
        Me.Panel2.TabIndex = 6
        '
        'txtTotalDescuento
        '
        Me.txtTotalDescuento.Color = System.Drawing.Color.Empty
        Me.txtTotalDescuento.Decimales = True
        Me.txtTotalDescuento.Indicaciones = Nothing
        Me.txtTotalDescuento.Location = New System.Drawing.Point(354, 3)
        Me.txtTotalDescuento.Name = "txtTotalDescuento"
        Me.txtTotalDescuento.Size = New System.Drawing.Size(115, 22)
        Me.txtTotalDescuento.SoloLectura = True
        Me.txtTotalDescuento.TabIndex = 0
        Me.txtTotalDescuento.TabStop = False
        Me.txtTotalDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDescuento.Texto = "0"
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.Controls.Add(Me.txtCantidadCobrado)
        Me.FlowLayoutPanel5.Controls.Add(Me.txtDescontados)
        Me.FlowLayoutPanel5.Controls.Add(Me.lblTotalCobrado)
        Me.FlowLayoutPanel5.Controls.Add(Me.txtSaldo)
        Me.FlowLayoutPanel5.Controls.Add(Me.lblDeudaTotal)
        Me.FlowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel5.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(122, 201)
        Me.FlowLayoutPanel5.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(244, 32)
        Me.FlowLayoutPanel5.TabIndex = 5
        '
        'txtCantidadCobrado
        '
        Me.txtCantidadCobrado.Color = System.Drawing.Color.Empty
        Me.txtCantidadCobrado.Decimales = True
        Me.txtCantidadCobrado.Indicaciones = Nothing
        Me.txtCantidadCobrado.Location = New System.Drawing.Point(196, 3)
        Me.txtCantidadCobrado.Name = "txtCantidadCobrado"
        Me.txtCantidadCobrado.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadCobrado.SoloLectura = True
        Me.txtCantidadCobrado.TabIndex = 4
        Me.txtCantidadCobrado.TabStop = False
        Me.txtCantidadCobrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadCobrado.Texto = "0"
        '
        'txtDescontados
        '
        Me.txtDescontados.Color = System.Drawing.Color.Empty
        Me.txtDescontados.Decimales = True
        Me.txtDescontados.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescontados.Indicaciones = Nothing
        Me.txtDescontados.Location = New System.Drawing.Point(74, 3)
        Me.txtDescontados.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtDescontados.Name = "txtDescontados"
        Me.txtDescontados.Size = New System.Drawing.Size(115, 22)
        Me.txtDescontados.SoloLectura = True
        Me.txtDescontados.TabIndex = 3
        Me.txtDescontados.TabStop = False
        Me.txtDescontados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDescontados.Texto = "0"
        '
        'lblTotalCobrado
        '
        Me.lblTotalCobrado.AutoSize = True
        Me.lblTotalCobrado.Location = New System.Drawing.Point(5, 6)
        Me.lblTotalCobrado.Margin = New System.Windows.Forms.Padding(3, 6, 3, 0)
        Me.lblTotalCobrado.Name = "lblTotalCobrado"
        Me.lblTotalCobrado.Size = New System.Drawing.Size(62, 13)
        Me.lblTotalCobrado.TabIndex = 2
        Me.lblTotalCobrado.Text = "Descuento:"
        Me.lblTotalCobrado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(132, 31)
        Me.txtSaldo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(108, 22)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 1
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'lblDeudaTotal
        '
        Me.lblDeudaTotal.AutoSize = True
        Me.lblDeudaTotal.Location = New System.Drawing.Point(88, 34)
        Me.lblDeudaTotal.Margin = New System.Windows.Forms.Padding(3, 6, 3, 0)
        Me.lblDeudaTotal.Name = "lblDeudaTotal"
        Me.lblDeudaTotal.Size = New System.Drawing.Size(37, 13)
        Me.lblDeudaTotal.TabIndex = 0
        Me.lblDeudaTotal.Text = "Saldo:"
        Me.lblDeudaTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.lklAgregarComprobante)
        Me.FlowLayoutPanel4.Controls.Add(Me.lklAgregarComprobanteDescuento)
        Me.FlowLayoutPanel4.Controls.Add(Me.lklEliminarComprobanteDescuento)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(0, 201)
        Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(122, 32)
        Me.FlowLayoutPanel4.TabIndex = 2
        '
        'lklAgregarComprobante
        '
        Me.lklAgregarComprobante.AutoSize = True
        Me.lklAgregarComprobante.Location = New System.Drawing.Point(3, 0)
        Me.lklAgregarComprobante.Name = "lklAgregarComprobante"
        Me.lklAgregarComprobante.Size = New System.Drawing.Size(83, 13)
        Me.lklAgregarComprobante.TabIndex = 2
        Me.lklAgregarComprobante.TabStop = True
        Me.lklAgregarComprobante.Text = "Agregar Factura"
        Me.lklAgregarComprobante.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'lklAgregarComprobanteDescuento
        '
        Me.lklAgregarComprobanteDescuento.AutoSize = True
        Me.lklAgregarComprobanteDescuento.Location = New System.Drawing.Point(3, 13)
        Me.lklAgregarComprobanteDescuento.Name = "lklAgregarComprobanteDescuento"
        Me.lklAgregarComprobanteDescuento.Size = New System.Drawing.Size(44, 13)
        Me.lklAgregarComprobanteDescuento.TabIndex = 0
        Me.lklAgregarComprobanteDescuento.TabStop = True
        Me.lklAgregarComprobanteDescuento.Text = "Agregar"
        Me.lklAgregarComprobanteDescuento.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'lklEliminarComprobanteDescuento
        '
        Me.lklEliminarComprobanteDescuento.AutoSize = True
        Me.lklEliminarComprobanteDescuento.Location = New System.Drawing.Point(53, 13)
        Me.lklEliminarComprobanteDescuento.Name = "lklEliminarComprobanteDescuento"
        Me.lklEliminarComprobanteDescuento.Size = New System.Drawing.Size(43, 13)
        Me.lklEliminarComprobanteDescuento.TabIndex = 1
        Me.lklEliminarComprobanteDescuento.TabStop = True
        Me.lklEliminarComprobanteDescuento.Text = "Eliminar"
        Me.lklEliminarComprobanteDescuento.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.lklAgregarDetalleDescuento)
        Me.FlowLayoutPanel2.Controls.Add(Me.lklEliminarDetalleDescuento)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 89)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(122, 23)
        Me.FlowLayoutPanel2.TabIndex = 0
        '
        'lklAgregarDetalleDescuento
        '
        Me.lklAgregarDetalleDescuento.AutoSize = True
        Me.lklAgregarDetalleDescuento.Location = New System.Drawing.Point(3, 0)
        Me.lklAgregarDetalleDescuento.Name = "lklAgregarDetalleDescuento"
        Me.lklAgregarDetalleDescuento.Size = New System.Drawing.Size(44, 13)
        Me.lklAgregarDetalleDescuento.TabIndex = 0
        Me.lklAgregarDetalleDescuento.TabStop = True
        Me.lklAgregarDetalleDescuento.Text = "Agregar"
        '
        'lklEliminarDetalleDescuento
        '
        Me.lklEliminarDetalleDescuento.AutoSize = True
        Me.lklEliminarDetalleDescuento.Location = New System.Drawing.Point(53, 0)
        Me.lklEliminarDetalleDescuento.Name = "lklEliminarDetalleDescuento"
        Me.lklEliminarDetalleDescuento.Size = New System.Drawing.Size(43, 13)
        Me.lklEliminarDetalleDescuento.TabIndex = 1
        Me.lklEliminarDetalleDescuento.TabStop = True
        Me.lklEliminarDetalleDescuento.Text = "Eliminar"
        '
        'lvListaDescuento
        '
        Me.lvListaDescuento.BackColor = System.Drawing.Color.White
        Me.lvListaDescuento.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader8})
        Me.TableLayoutPanel3.SetColumnSpan(Me.lvListaDescuento, 2)
        Me.lvListaDescuento.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvListaDescuento.Enabled = False
        Me.lvListaDescuento.FullRowSelect = True
        Me.lvListaDescuento.GridLines = True
        Me.lvListaDescuento.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvListaDescuento.HideSelection = False
        Me.lvListaDescuento.Location = New System.Drawing.Point(3, 3)
        Me.lvListaDescuento.MultiSelect = False
        Me.lvListaDescuento.Name = "lvListaDescuento"
        Me.lvListaDescuento.Size = New System.Drawing.Size(360, 83)
        Me.lvListaDescuento.TabIndex = 1
        Me.lvListaDescuento.UseCompatibleStateImageBehavior = False
        Me.lvListaDescuento.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "ID"
        Me.ColumnHeader1.Width = 40
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Descripcion"
        Me.ColumnHeader2.Width = 603
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Total"
        Me.ColumnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader8.Width = 115
        '
        'pnlDevolucion
        '
        Me.pnlDevolucion.Controls.Add(Me.txtProductoDevolucion)
        Me.pnlDevolucion.Controls.Add(Me.lvLista)
        Me.pnlDevolucion.Controls.Add(Me.Label8)
        Me.pnlDevolucion.Controls.Add(Me.btnCargarFacturaDevolucion)
        Me.pnlDevolucion.Controls.Add(Me.TableLayoutPanel1)
        Me.pnlDevolucion.Controls.Add(Me.chkTSucursales)
        Me.pnlDevolucion.Location = New System.Drawing.Point(9, 13)
        Me.pnlDevolucion.Name = "pnlDevolucion"
        Me.pnlDevolucion.Size = New System.Drawing.Size(777, 236)
        Me.pnlDevolucion.TabIndex = 2
        '
        'txtProductoDevolucion
        '
        Me.txtProductoDevolucion.AlturaMaxima = 260
        Me.txtProductoDevolucion.ColumnasNumericas = Nothing
        Me.txtProductoDevolucion.Compra = False
        Me.txtProductoDevolucion.Consulta = Nothing
        Me.txtProductoDevolucion.ControlarExistencia = False
        Me.txtProductoDevolucion.ControlarReservas = False
        Me.txtProductoDevolucion.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProductoDevolucion.dtDescuento = Nothing
        Me.txtProductoDevolucion.dtProductosSeleccionados = Nothing
        Me.txtProductoDevolucion.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProductoDevolucion.IDCliente = 0
        Me.txtProductoDevolucion.IDClienteSucursal = 0
        Me.txtProductoDevolucion.IDDeposito = 0
        Me.txtProductoDevolucion.IDListaPrecio = 0
        Me.txtProductoDevolucion.IDMoneda = 0
        Me.txtProductoDevolucion.IDSucursal = 0
        Me.txtProductoDevolucion.Location = New System.Drawing.Point(133, 15)
        Me.txtProductoDevolucion.Margin = New System.Windows.Forms.Padding(0)
        Me.txtProductoDevolucion.Name = "txtProductoDevolucion"
        Me.txtProductoDevolucion.Pedido = False
        Me.txtProductoDevolucion.Precios = Nothing
        Me.txtProductoDevolucion.Registro = Nothing
        Me.txtProductoDevolucion.Seleccionado = False
        Me.txtProductoDevolucion.SeleccionMultiple = False
        Me.txtProductoDevolucion.Size = New System.Drawing.Size(445, 21)
        Me.txtProductoDevolucion.SoloLectura = False
        Me.txtProductoDevolucion.TabIndex = 23
        Me.txtProductoDevolucion.TieneDescuento = False
        Me.txtProductoDevolucion.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProductoDevolucion.Venta = False
        '
        'lvLista
        '
        Me.lvLista.BackColor = System.Drawing.Color.White
        Me.lvLista.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colID, Me.colReferencia, Me.colDescripcion, Me.colComprobante, Me.colMotio, Me.colCantidad, Me.colPrecioUnitario, Me.ColTotal})
        Me.lvLista.ContextMenuStrip = Me.ContextMenuStrip1
        Me.lvLista.FullRowSelect = True
        Me.lvLista.GridLines = True
        Me.lvLista.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvLista.HideSelection = False
        Me.lvLista.Location = New System.Drawing.Point(2, 39)
        Me.lvLista.MultiSelect = False
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(771, 190)
        Me.lvLista.TabIndex = 14
        Me.lvLista.UseCompatibleStateImageBehavior = False
        Me.lvLista.View = System.Windows.Forms.View.Details
        '
        'colID
        '
        Me.colID.Text = "ID"
        Me.colID.Width = 0
        '
        'colReferencia
        '
        Me.colReferencia.Text = "Ref."
        Me.colReferencia.Width = 50
        '
        'colDescripcion
        '
        Me.colDescripcion.Text = "Descripcion"
        Me.colDescripcion.Width = 250
        '
        'colComprobante
        '
        Me.colComprobante.Text = "Comprobante"
        Me.colComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.colComprobante.Width = 90
        '
        'colMotio
        '
        Me.colMotio.Text = "MotivoDevolucion"
        Me.colMotio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.colMotio.Width = 150
        '
        'colCantidad
        '
        Me.colCantidad.Text = "Cantidad"
        Me.colCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colCantidad.Width = 55
        '
        'colPrecioUnitario
        '
        Me.colPrecioUnitario.Text = "Pre. Uni."
        Me.colPrecioUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colPrecioUnitario.Width = 73
        '
        'ColTotal
        '
        Me.ColTotal.Text = "Total"
        Me.ColTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColTotal.Width = 70
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(100, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(53, 13)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Producto:"
        '
        'btnCargarFacturaDevolucion
        '
        Me.btnCargarFacturaDevolucion.Location = New System.Drawing.Point(3, 13)
        Me.btnCargarFacturaDevolucion.Name = "btnCargarFacturaDevolucion"
        Me.btnCargarFacturaDevolucion.Size = New System.Drawing.Size(91, 21)
        Me.btnCargarFacturaDevolucion.TabIndex = 0
        Me.btnCargarFacturaDevolucion.Text = "Sel. FACT"
        Me.btnCargarFacturaDevolucion.UseVisualStyleBackColor = True
        Me.btnCargarFacturaDevolucion.Visible = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel4, 0, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(581, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.1046!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(187, 69)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 4
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 95.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 95.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 95.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.txtPrecioUnitario, 3, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.txtCantidad, 2, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.Label9, 3, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label10, 2, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel3, 1, 1)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel4.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 2
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 13.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(187, 69)
        Me.TableLayoutPanel4.TabIndex = 1
        '
        'txtPrecioUnitario
        '
        Me.txtPrecioUnitario.Color = System.Drawing.Color.Empty
        Me.txtPrecioUnitario.Decimales = False
        Me.txtPrecioUnitario.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtPrecioUnitario.Location = New System.Drawing.Point(95, 16)
        Me.txtPrecioUnitario.Name = "txtPrecioUnitario"
        Me.txtPrecioUnitario.Size = New System.Drawing.Size(89, 21)
        Me.txtPrecioUnitario.SoloLectura = False
        Me.txtPrecioUnitario.TabIndex = 3
        Me.txtPrecioUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrecioUnitario.Texto = "0"
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = False
        Me.txtCantidad.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCantidad.Location = New System.Drawing.Point(0, 16)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(89, 21)
        Me.txtCantidad.SoloLectura = False
        Me.txtCantidad.TabIndex = 2
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(95, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(62, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Precio Uni.:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(0, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(52, 13)
        Me.Label10.TabIndex = 20
        Me.Label10.Text = "Cantidad:"
        '
        'Panel3
        '
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(95, 13)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1, 56)
        Me.Panel3.TabIndex = 21
        '
        'chkTSucursales
        '
        Me.chkTSucursales.AutoSize = True
        Me.chkTSucursales.Location = New System.Drawing.Point(3, 16)
        Me.chkTSucursales.Name = "chkTSucursales"
        Me.chkTSucursales.Size = New System.Drawing.Size(127, 17)
        Me.chkTSucursales.TabIndex = 4
        Me.chkTSucursales.Text = "Todas las Sucursales"
        Me.chkTSucursales.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.Panel1, 2)
        Me.Panel1.Controls.Add(Me.btnAnular)
        Me.Panel1.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Panel1.Controls.Add(Me.btnImprimir)
        Me.Panel1.Controls.Add(Me.btnAsiento)
        Me.Panel1.Controls.Add(Me.btnAnula)
        Me.Panel1.Controls.Add(Me.btnNuevo)
        Me.Panel1.Controls.Add(Me.btnSalir)
        Me.Panel1.Controls.Add(Me.btnGuardar)
        Me.Panel1.Controls.Add(Me.btnCancelar)
        Me.Panel1.Controls.Add(Me.btnEliminar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 598)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(794, 60)
        Me.Panel1.TabIndex = 6
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(204, 2)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 8
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(3, 2)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(120, 23)
        Me.btnBusquedaAvanzada.TabIndex = 0
        Me.btnBusquedaAvanzada.Text = "&Busqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(126, 2)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 1
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        Me.btnImprimir.Visible = False
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(342, 2)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 3
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        Me.btnAsiento.Visible = False
        '
        'btnAnula
        '
        Me.btnAnula.Location = New System.Drawing.Point(-94, -558)
        Me.btnAnula.Name = "btnAnula"
        Me.btnAnula.Size = New System.Drawing.Size(75, 23)
        Me.btnAnula.TabIndex = 2
        Me.btnAnula.Text = "Anular"
        Me.btnAnula.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(423, 2)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 4
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(702, 2)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 7
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(504, 2)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 5
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(592, 2)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 6
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(204, 2)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        Me.btnEliminar.Visible = False
        '
        'gbxCabecera
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.gbxCabecera, 2)
        Me.gbxCabecera.Controls.Add(Me.lblNroSolicitudCliente)
        Me.gbxCabecera.Controls.Add(Me.txtNumeroSolicitudCliente)
        Me.gbxCabecera.Controls.Add(Me.txtFechaAutorizacion)
        Me.gbxCabecera.Controls.Add(Me.Label6)
        Me.gbxCabecera.Controls.Add(Me.Label5)
        Me.gbxCabecera.Controls.Add(Me.txtUsuarioAutorizador)
        Me.gbxCabecera.Controls.Add(Me.Label3)
        Me.gbxCabecera.Controls.Add(Me.txtEstadoAutorizacion)
        Me.gbxCabecera.Controls.Add(Me.txtObservacionAutorizacion)
        Me.gbxCabecera.Controls.Add(Me.Label2)
        Me.gbxCabecera.Controls.Add(Me.txtTelefono)
        Me.gbxCabecera.Controls.Add(Me.lblTelefono)
        Me.gbxCabecera.Controls.Add(Me.lblDireccion)
        Me.gbxCabecera.Controls.Add(Me.chkAplicar)
        Me.gbxCabecera.Controls.Add(Me.txtCliente)
        Me.gbxCabecera.Controls.Add(Me.lblDejarpendiente)
        Me.gbxCabecera.Controls.Add(Me.rdbDescuentos)
        Me.gbxCabecera.Controls.Add(Me.rdbDevolucion)
        Me.gbxCabecera.Controls.Add(Me.cbxDeposito)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblDeposito)
        Me.gbxCabecera.Controls.Add(Me.lblCliente)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.OcxCotizacion1)
        Me.gbxCabecera.Controls.Add(Me.Label4)
        Me.gbxCabecera.Controls.Add(Me.txtDireccion)
        Me.gbxCabecera.Controls.Add(Me.cbxComprobanteImpresion)
        Me.gbxCabecera.Controls.Add(Me.cbxAcuerdo)
        Me.gbxCabecera.Controls.Add(Me.lblAcuerdo)
        Me.gbxCabecera.Controls.Add(Me.txtAcuerdo)
        Me.gbxCabecera.Controls.Add(Me.TxtComprobanteImpresion)
        Me.gbxCabecera.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxCabecera.Location = New System.Drawing.Point(0, 42)
        Me.gbxCabecera.Margin = New System.Windows.Forms.Padding(0)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(794, 176)
        Me.gbxCabecera.TabIndex = 1
        Me.gbxCabecera.TabStop = False
        '
        'txtNumeroSolicitudCliente
        '
        Me.txtNumeroSolicitudCliente.BackColor = System.Drawing.SystemColors.Control
        Me.txtNumeroSolicitudCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumeroSolicitudCliente.Color = System.Drawing.Color.Empty
        Me.txtNumeroSolicitudCliente.Indicaciones = Nothing
        Me.txtNumeroSolicitudCliente.Location = New System.Drawing.Point(707, 98)
        Me.txtNumeroSolicitudCliente.Multilinea = False
        Me.txtNumeroSolicitudCliente.Name = "txtNumeroSolicitudCliente"
        Me.txtNumeroSolicitudCliente.Size = New System.Drawing.Size(74, 22)
        Me.txtNumeroSolicitudCliente.SoloLectura = False
        Me.txtNumeroSolicitudCliente.TabIndex = 51
        Me.txtNumeroSolicitudCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNumeroSolicitudCliente.Texto = ""
        '
        'txtFechaAutorizacion
        '
        Me.txtFechaAutorizacion.AñoFecha = 0
        Me.txtFechaAutorizacion.Color = System.Drawing.Color.Empty
        Me.txtFechaAutorizacion.Enabled = False
        Me.txtFechaAutorizacion.Fecha = New Date(2013, 5, 3, 16, 59, 18, 132)
        Me.txtFechaAutorizacion.Location = New System.Drawing.Point(568, 123)
        Me.txtFechaAutorizacion.MesFecha = 0
        Me.txtFechaAutorizacion.Name = "txtFechaAutorizacion"
        Me.txtFechaAutorizacion.PermitirNulo = False
        Me.txtFechaAutorizacion.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaAutorizacion.SoloLectura = True
        Me.txtFechaAutorizacion.TabIndex = 50
        Me.txtFechaAutorizacion.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(467, 125)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 13)
        Me.Label6.TabIndex = 49
        Me.Label6.Text = "Fecha Autorizacion:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(202, 125)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 13)
        Me.Label5.TabIndex = 47
        Me.Label5.Text = "Autorizador:"
        '
        'txtUsuarioAutorizador
        '
        Me.txtUsuarioAutorizador.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUsuarioAutorizador.Color = System.Drawing.Color.Empty
        Me.txtUsuarioAutorizador.Enabled = False
        Me.txtUsuarioAutorizador.Indicaciones = Nothing
        Me.txtUsuarioAutorizador.Location = New System.Drawing.Point(265, 122)
        Me.txtUsuarioAutorizador.Multilinea = False
        Me.txtUsuarioAutorizador.Name = "txtUsuarioAutorizador"
        Me.txtUsuarioAutorizador.Size = New System.Drawing.Size(200, 21)
        Me.txtUsuarioAutorizador.SoloLectura = True
        Me.txtUsuarioAutorizador.TabIndex = 46
        Me.txtUsuarioAutorizador.TabStop = False
        Me.txtUsuarioAutorizador.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUsuarioAutorizador.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(5, 125)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(104, 13)
        Me.Label3.TabIndex = 45
        Me.Label3.Text = "Estado Autorizacion:"
        '
        'txtEstadoAutorizacion
        '
        Me.txtEstadoAutorizacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstadoAutorizacion.Color = System.Drawing.Color.Empty
        Me.txtEstadoAutorizacion.Enabled = False
        Me.txtEstadoAutorizacion.Indicaciones = Nothing
        Me.txtEstadoAutorizacion.Location = New System.Drawing.Point(110, 122)
        Me.txtEstadoAutorizacion.Multilinea = False
        Me.txtEstadoAutorizacion.Name = "txtEstadoAutorizacion"
        Me.txtEstadoAutorizacion.Size = New System.Drawing.Size(91, 21)
        Me.txtEstadoAutorizacion.SoloLectura = True
        Me.txtEstadoAutorizacion.TabIndex = 44
        Me.txtEstadoAutorizacion.TabStop = False
        Me.txtEstadoAutorizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEstadoAutorizacion.Texto = ""
        '
        'txtObservacionAutorizacion
        '
        Me.txtObservacionAutorizacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacionAutorizacion.Color = System.Drawing.Color.Empty
        Me.txtObservacionAutorizacion.Indicaciones = Nothing
        Me.txtObservacionAutorizacion.Location = New System.Drawing.Point(132, 149)
        Me.txtObservacionAutorizacion.Multilinea = False
        Me.txtObservacionAutorizacion.Name = "txtObservacionAutorizacion"
        Me.txtObservacionAutorizacion.Size = New System.Drawing.Size(651, 21)
        Me.txtObservacionAutorizacion.SoloLectura = True
        Me.txtObservacionAutorizacion.TabIndex = 43
        Me.txtObservacionAutorizacion.TabStop = False
        Me.txtObservacionAutorizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacionAutorizacion.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(5, 152)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(126, 13)
        Me.Label2.TabIndex = 42
        Me.Label2.Text = "Observacion Autorizador:"
        '
        'txtTelefono
        '
        Me.txtTelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefono.Color = System.Drawing.Color.Beige
        Me.txtTelefono.Indicaciones = Nothing
        Me.txtTelefono.Location = New System.Drawing.Point(689, 44)
        Me.txtTelefono.Multilinea = False
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(94, 21)
        Me.txtTelefono.SoloLectura = True
        Me.txtTelefono.TabIndex = 41
        Me.txtTelefono.TabStop = False
        Me.txtTelefono.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTelefono.Texto = ""
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Location = New System.Drawing.Point(666, 48)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(25, 13)
        Me.lblTelefono.TabIndex = 40
        Me.lblTelefono.Text = "Tel:"
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.Location = New System.Drawing.Point(5, 48)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(55, 13)
        Me.lblDireccion.TabIndex = 38
        Me.lblDireccion.Text = "Direccion:"
        '
        'chkAplicar
        '
        Me.chkAplicar.BackColor = System.Drawing.Color.Transparent
        Me.chkAplicar.Color = System.Drawing.Color.Empty
        Me.chkAplicar.Location = New System.Drawing.Point(769, 76)
        Me.chkAplicar.Name = "chkAplicar"
        Me.chkAplicar.Size = New System.Drawing.Size(17, 12)
        Me.chkAplicar.SoloLectura = False
        Me.chkAplicar.TabIndex = 13
        Me.chkAplicar.Texto = ""
        Me.chkAplicar.Valor = False
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 61
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.ControlCorto = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(75, 14)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(588, 24)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 0
        '
        'rdbDescuentos
        '
        Me.rdbDescuentos.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdbDescuentos.Cursor = System.Windows.Forms.Cursors.Hand
        Me.rdbDescuentos.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.rdbDescuentos.FlatAppearance.CheckedBackColor = System.Drawing.Color.MintCream
        Me.rdbDescuentos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.rdbDescuentos.Location = New System.Drawing.Point(542, 71)
        Me.rdbDescuentos.Name = "rdbDescuentos"
        Me.rdbDescuentos.Size = New System.Drawing.Size(71, 22)
        Me.rdbDescuentos.TabIndex = 5
        Me.rdbDescuentos.Text = "Descuento"
        Me.rdbDescuentos.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.rdbDescuentos.UseVisualStyleBackColor = True
        '
        'rdbDevolucion
        '
        Me.rdbDevolucion.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdbDevolucion.Checked = True
        Me.rdbDevolucion.Cursor = System.Windows.Forms.Cursors.Hand
        Me.rdbDevolucion.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.rdbDevolucion.FlatAppearance.CheckedBackColor = System.Drawing.Color.MintCream
        Me.rdbDevolucion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.rdbDevolucion.Location = New System.Drawing.Point(468, 71)
        Me.rdbDevolucion.Name = "rdbDevolucion"
        Me.rdbDevolucion.Size = New System.Drawing.Size(73, 22)
        Me.rdbDevolucion.TabIndex = 4
        Me.rdbDevolucion.TabStop = True
        Me.rdbDevolucion.Text = "Devolucion"
        Me.rdbDevolucion.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.rdbDevolucion.UseVisualStyleBackColor = True
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = Nothing
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = ""
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = Nothing
        Me.cbxDeposito.DataSource = ""
        Me.cbxDeposito.DataValueMember = ""
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(76, 72)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = True
        Me.cbxDeposito.Size = New System.Drawing.Size(201, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 2
        Me.cbxDeposito.Texto = ""
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(283, 76)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 6
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblDeposito
        '
        Me.lblDeposito.AutoSize = True
        Me.lblDeposito.Location = New System.Drawing.Point(5, 76)
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(52, 13)
        Me.lblDeposito.TabIndex = 4
        Me.lblDeposito.Text = "Deposito:"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(5, 20)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(42, 13)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Text = "Cliente:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(76, 98)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(495, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 6
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 5, 3, 16, 59, 18, 132)
        Me.txtFecha.Location = New System.Drawing.Point(709, 14)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 1
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(669, 18)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Text = "Fecha:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(5, 101)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 14
        Me.lblObservacion.Text = "Observacion:"
        '
        'OcxCotizacion1
        '
        Me.OcxCotizacion1.dt = Nothing
        Me.OcxCotizacion1.FiltroFecha = Nothing
        Me.OcxCotizacion1.Location = New System.Drawing.Point(329, 69)
        Me.OcxCotizacion1.Name = "OcxCotizacion1"
        Me.OcxCotizacion1.Registro = Nothing
        Me.OcxCotizacion1.Saltar = False
        Me.OcxCotizacion1.Seleccionado = True
        Me.OcxCotizacion1.Size = New System.Drawing.Size(139, 28)
        Me.OcxCotizacion1.SoloLectura = False
        Me.OcxCotizacion1.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(437, 76)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Tipo:"
        '
        'txtDireccion
        '
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Color = System.Drawing.Color.Beige
        Me.txtDireccion.Indicaciones = Nothing
        Me.txtDireccion.Location = New System.Drawing.Point(76, 44)
        Me.txtDireccion.Multilinea = False
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(584, 21)
        Me.txtDireccion.SoloLectura = True
        Me.txtDireccion.TabIndex = 39
        Me.txtDireccion.TabStop = False
        Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccion.Texto = ""
        '
        'cbxComprobanteImpresion
        '
        Me.cbxComprobanteImpresion.CampoWhere = Nothing
        Me.cbxComprobanteImpresion.CargarUnaSolaVez = False
        Me.cbxComprobanteImpresion.DataDisplayMember = Nothing
        Me.cbxComprobanteImpresion.DataFilter = Nothing
        Me.cbxComprobanteImpresion.DataOrderBy = Nothing
        Me.cbxComprobanteImpresion.DataSource = Nothing
        Me.cbxComprobanteImpresion.DataValueMember = Nothing
        Me.cbxComprobanteImpresion.dtSeleccionado = Nothing
        Me.cbxComprobanteImpresion.FormABM = Nothing
        Me.cbxComprobanteImpresion.Indicaciones = Nothing
        Me.cbxComprobanteImpresion.Location = New System.Drawing.Point(648, 122)
        Me.cbxComprobanteImpresion.Name = "cbxComprobanteImpresion"
        Me.cbxComprobanteImpresion.SeleccionMultiple = False
        Me.cbxComprobanteImpresion.SeleccionObligatoria = True
        Me.cbxComprobanteImpresion.Size = New System.Drawing.Size(135, 21)
        Me.cbxComprobanteImpresion.SoloLectura = False
        Me.cbxComprobanteImpresion.TabIndex = 53
        Me.cbxComprobanteImpresion.Texto = ""
        '
        'cbxAcuerdo
        '
        Me.cbxAcuerdo.CampoWhere = Nothing
        Me.cbxAcuerdo.CargarUnaSolaVez = False
        Me.cbxAcuerdo.DataDisplayMember = Nothing
        Me.cbxAcuerdo.DataFilter = Nothing
        Me.cbxAcuerdo.DataOrderBy = Nothing
        Me.cbxAcuerdo.DataSource = Nothing
        Me.cbxAcuerdo.DataValueMember = Nothing
        Me.cbxAcuerdo.dtSeleccionado = Nothing
        Me.cbxAcuerdo.FormABM = Nothing
        Me.cbxAcuerdo.Indicaciones = Nothing
        Me.cbxAcuerdo.Location = New System.Drawing.Point(705, 98)
        Me.cbxAcuerdo.Name = "cbxAcuerdo"
        Me.cbxAcuerdo.SeleccionMultiple = False
        Me.cbxAcuerdo.SeleccionObligatoria = True
        Me.cbxAcuerdo.Size = New System.Drawing.Size(78, 21)
        Me.cbxAcuerdo.SoloLectura = False
        Me.cbxAcuerdo.TabIndex = 57
        Me.cbxAcuerdo.Texto = ""
        '
        'txtAcuerdo
        '
        Me.txtAcuerdo.BackColor = System.Drawing.SystemColors.Control
        Me.txtAcuerdo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAcuerdo.Color = System.Drawing.Color.Empty
        Me.txtAcuerdo.Enabled = False
        Me.txtAcuerdo.Indicaciones = Nothing
        Me.txtAcuerdo.Location = New System.Drawing.Point(706, 98)
        Me.txtAcuerdo.Multilinea = False
        Me.txtAcuerdo.Name = "txtAcuerdo"
        Me.txtAcuerdo.Size = New System.Drawing.Size(74, 22)
        Me.txtAcuerdo.SoloLectura = False
        Me.txtAcuerdo.TabIndex = 55
        Me.txtAcuerdo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtAcuerdo.Texto = ""
        '
        'TxtComprobanteImpresion
        '
        Me.TxtComprobanteImpresion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtComprobanteImpresion.Color = System.Drawing.Color.Empty
        Me.TxtComprobanteImpresion.Enabled = False
        Me.TxtComprobanteImpresion.Indicaciones = Nothing
        Me.TxtComprobanteImpresion.Location = New System.Drawing.Point(648, 122)
        Me.TxtComprobanteImpresion.Multilinea = False
        Me.TxtComprobanteImpresion.Name = "TxtComprobanteImpresion"
        Me.TxtComprobanteImpresion.Size = New System.Drawing.Size(135, 21)
        Me.TxtComprobanteImpresion.SoloLectura = True
        Me.TxtComprobanteImpresion.TabIndex = 54
        Me.TxtComprobanteImpresion.TabStop = False
        Me.TxtComprobanteImpresion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TxtComprobanteImpresion.Texto = ""
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.flpRegistradoPor)
        Me.FlowLayoutPanel1.Controls.Add(Me.flpAnuladoPor)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 502)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(358, 96)
        Me.FlowLayoutPanel1.TabIndex = 4
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(3, 3)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(277, 20)
        Me.flpRegistradoPor.TabIndex = 0
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(3, 29)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(315, 20)
        Me.flpAnuladoPor.TabIndex = 1
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'OcxImpuesto1
        '
        Me.OcxImpuesto1.Decimales = False
        Me.OcxImpuesto1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxImpuesto1.dtImpuesto = Nothing
        Me.OcxImpuesto1.IDMoneda = 0
        Me.OcxImpuesto1.Location = New System.Drawing.Point(361, 473)
        Me.OcxImpuesto1.Name = "OcxImpuesto1"
        Me.TableLayoutPanel2.SetRowSpan(Me.OcxImpuesto1, 2)
        Me.OcxImpuesto1.Size = New System.Drawing.Size(430, 122)
        Me.OcxImpuesto1.TabIndex = 5
        Me.OcxImpuesto1.TotalRetencionIVA = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'btnProductosRelacionados
        '
        Me.btnProductosRelacionados.Location = New System.Drawing.Point(3, 473)
        Me.btnProductosRelacionados.Name = "btnProductosRelacionados"
        Me.btnProductosRelacionados.Size = New System.Drawing.Size(168, 23)
        Me.btnProductosRelacionados.TabIndex = 7
        Me.btnProductosRelacionados.Text = "Productos Relacionados"
        Me.btnProductosRelacionados.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 658)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(794, 22)
        Me.StatusStrip1.TabIndex = 2
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'frmPedidoNotaCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(794, 680)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmPedidoNotaCredito"
        Me.Tag = "frmPedidoNotaCredito"
        Me.Text = "frmPedidoNotaCredito"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.gbxComprobante.ResumeLayout(False)
        Me.gbxComprobante.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.pnlDescuento.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel5.PerformLayout()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel4.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        Me.pnlDevolucion.ResumeLayout(False)
        Me.pnlDevolucion.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel4.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents VerDetalleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportarAExcelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents gbxComprobante As System.Windows.Forms.GroupBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lblSubMotivo As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnAnular As Button
    Friend WithEvents btnBusquedaAvanzada As Button
    Friend WithEvents btnImprimir As Button
    Friend WithEvents btnAsiento As Button
    Friend WithEvents btnAnula As Button
    Friend WithEvents btnNuevo As Button
    Friend WithEvents btnSalir As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnEliminar As Button
    Friend WithEvents gbxCabecera As GroupBox
    Friend WithEvents TxtComprobanteImpresion As ocxTXTString
    Friend WithEvents cbxComprobanteImpresion As ocxCBX
    Friend WithEvents lblNroSolicitudCliente As Label
    Friend WithEvents txtNumeroSolicitudCliente As ocxTXTString
    Friend WithEvents txtFechaAutorizacion As ocxTXTDate
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtUsuarioAutorizador As ocxTXTString
    Friend WithEvents Label3 As Label
    Friend WithEvents txtEstadoAutorizacion As ocxTXTString
    Friend WithEvents txtObservacionAutorizacion As ocxTXTString
    Friend WithEvents Label2 As Label
    Friend WithEvents txtTelefono As ocxTXTString
    Friend WithEvents lblTelefono As Label
    Friend WithEvents lblDireccion As Label
    Friend WithEvents chkAplicar As ocxCHK
    Friend WithEvents txtCliente As ocxTXTCliente
    Friend WithEvents lblDejarpendiente As Label
    Friend WithEvents rdbDescuentos As RadioButton
    Friend WithEvents rdbDevolucion As RadioButton
    Friend WithEvents cbxDeposito As ocxCBX
    Friend WithEvents lblMoneda As Label
    Friend WithEvents lblDeposito As Label
    Friend WithEvents lblCliente As Label
    Friend WithEvents txtObservacion As ocxTXTString
    Friend WithEvents txtFecha As ocxTXTDate
    Friend WithEvents lblFecha As Label
    Friend WithEvents lblObservacion As Label
    Friend WithEvents OcxCotizacion1 As ocxCotizacion
    Friend WithEvents Label4 As Label
    Friend WithEvents txtDireccion As ocxTXTString
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents flpRegistradoPor As FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As Label
    Friend WithEvents lblUsuarioRegistro As Label
    Friend WithEvents lblFechaRegistro As Label
    Friend WithEvents flpAnuladoPor As FlowLayoutPanel
    Friend WithEvents lblAnulado As Label
    Friend WithEvents lblUsuarioAnulado As Label
    Friend WithEvents lblFechaAnulado As Label
    Friend WithEvents OcxImpuesto1 As ocxImpuesto
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents pnlDescuento As Panel
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents lvListaDescuento As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader8 As ColumnHeader
    Friend WithEvents dgw As DataGridView
    Friend WithEvents colIDTransaccion As DataGridViewTextBoxColumn
    Friend WithEvents colSel As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents colTipo As DataGridViewTextBoxColumn
    Friend WithEvents colCondicion As DataGridViewTextBoxColumn
    Friend WithEvents colVencimiento As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents colCobrado As DataGridViewTextBoxColumn
    Friend WithEvents colDescontado As DataGridViewTextBoxColumn
    Friend WithEvents colSaldo As DataGridViewTextBoxColumn
    Friend WithEvents colImporte As DataGridViewTextBoxColumn
    Friend WithEvents colCancelar As DataGridViewCheckBoxColumn
    Friend WithEvents Panel2 As Panel
    Friend WithEvents txtTotalDescuento As ocxTXTNumeric
    Friend WithEvents FlowLayoutPanel5 As FlowLayoutPanel
    Friend WithEvents txtCantidadCobrado As ocxTXTNumeric
    Friend WithEvents txtDescontados As ocxTXTNumeric
    Friend WithEvents lblTotalCobrado As Label
    Friend WithEvents txtSaldo As ocxTXTNumeric
    Friend WithEvents lblDeudaTotal As Label
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents lklAgregarComprobanteDescuento As LinkLabel
    Friend WithEvents lklEliminarComprobanteDescuento As LinkLabel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents lklAgregarDetalleDescuento As LinkLabel
    Friend WithEvents lklEliminarDetalleDescuento As LinkLabel
    Friend WithEvents btnProductosRelacionados As Button
    Friend WithEvents lklAgregarComprobante As LinkLabel
    Friend WithEvents txtProducto As ocxTXTProducto
    Friend WithEvents lblAcuerdo As Label
    Friend WithEvents txtAcuerdo As ocxTXTString
    Friend WithEvents cbxAcuerdo As ocxCBX
    Friend WithEvents pnlDevolucion As Panel
    Friend WithEvents txtProductoDevolucion As ocxTXTProducto
    Friend WithEvents lvLista As ListView
    Friend WithEvents colID As ColumnHeader
    Friend WithEvents colReferencia As ColumnHeader
    Friend WithEvents colDescripcion As ColumnHeader
    Friend WithEvents colComprobante As ColumnHeader
    Friend WithEvents colMotio As ColumnHeader
    Friend WithEvents colCantidad As ColumnHeader
    Friend WithEvents colPrecioUnitario As ColumnHeader
    Friend WithEvents ColTotal As ColumnHeader
    Friend WithEvents chkTSucursales As CheckBox
    Friend WithEvents Label8 As Label
    Friend WithEvents btnCargarFacturaDevolucion As Button
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents txtPrecioUnitario As ocxTXTNumeric
    Friend WithEvents txtCantidad As ocxTXTNumeric
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents txtNroNotaCredito As ocxTXTString
    Friend WithEvents chkProcesadoNC As ocxCHK
    Friend WithEvents cbxSubMotivo As ocxCBX
    Friend WithEvents cbxSucursal As ocxCBX
End Class

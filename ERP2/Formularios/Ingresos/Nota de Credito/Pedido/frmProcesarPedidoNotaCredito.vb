﻿Imports System.Threading
Imports ERP.Reporte
Public Class frmProcesarPedidoNotaCredito
    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim PorcentajeAsignado As Decimal = 0
    Dim ImporteAsignado As Decimal = 0

    'PROPIEDADES

    'VARIABLES
    Dim tProcesar As Thread
    Dim vProcesar As Boolean = False


    'FUNCIONES
    Sub Inicializar()

        Me.KeyPreview = True

        'Varios
        CheckForIllegalCrossThreadCalls = False

        cbxGrupos.SelectedIndex = 0
        cbxOrden.SelectedIndex = 0

        'Funciones
        ListarGrupos()

        'Fecha facturacion
        If IsDate(VGFechaFacturacion) = False Then
            VGFechaFacturacion = VGFechaHoraSistema
        End If

        Dim Fecha As Date = VGFechaFacturacion

        txtFechaDesde.txt.Text = "01/01/2000"
        txtFechaHasta.Hoy()

        lblFecha.Text = " * Fecha de Facturacion: " & Fecha.ToShortDateString

        Dim f As Font = New Font(lblFecha.Font.FontFamily, 12, FontStyle.Bold, lblFecha.Font.Unit)
        lblFecha.Font = f
        lblFecha.ForeColor = Color.Maroon
        ListarPedidos()
    End Sub

    Sub ListarGrupos()

        lvGrupo.Items.Clear()

        vgConfiguraciones = CSistema.ExecuteToDataTable("Select TOP 1 * From Configuraciones Where IDSucursal=" & vgIDSucursal, "", 15)(0)
        VGFechaFacturacion = CSistema.ExecuteScalar("Select TOP 1 Fecha From FechaFacturacion Where IDSucursal=" & vgIDSucursal)
        If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        End If
        lblFecha.Text = " * Fecha de Facturacion: " & VGFechaFacturacion.ToShortDateString

        lvGrupo.View = View.Details
        lvGrupo.Columns.Add("Listado", "Listado")
        lvGrupo.Items.Add("Todo")
        lvGrupo.Items.Add("Aprobados-Anulados")
        lvGrupo.Columns("Listado").AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)


    End Sub

    Sub SeleccionarGrupo()
        ListarPedidos()
    End Sub

    Sub ListarPedidos()


        vgConfiguraciones = CSistema.ExecuteToDataTable("Select TOP 1 * From Configuraciones Where IDSucursal=" & vgIDSucursal, "", 15)(0)
        VGFechaFacturacion = CSistema.ExecuteScalar("Select TOP 1 Fecha From FechaFacturacion Where IDSucursal=" & vgIDSucursal)
        If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        End If
        lblFecha.Text = " * Fecha de Facturacion: " & VGFechaFacturacion.ToShortDateString


        Dim SQL As String = ""
        Dim OrderBy As String = ""
        Dim Campo As String = ""

        Dim ID As String = ""
        Try
            ID = lvGrupo.SelectedItems(0).Text
        Catch ex As Exception
            ID = lvGrupo.Items(0).Text
        End Try


        If ID = "Todo" Then
            SQL = "Select IDTransaccion, Sucursal, 'N° Operacion'=NroOperacion, 'Motivo'=MotivoNotaCredito, 'Sub Motivo'=SubMotivoNotaCredito,  Cliente, Referencia, Ruc, 'Procesado'=ProcesadoNC, 'Estado'=EstadoNC, UsuarioAprobado, ProcesadoNC From VPedidoNotaCredito Where AnuladoAprobado = 'False' and IDSucursal = " & vgIDSucursal & " and EstadoNC in ('APROBADO', 'PENDIENTE') And ProcesadoNC='PENDIENTE' And cast(Fecha as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
        End If
        If ID = "Aprobados-Anulados" Then
            SQL = "Select IDTransaccion, Sucursal, 'N° Operacion'=NroOperacion, 'Motivo'=MotivoNotaCredito, 'Sub Motivo'=SubMotivoNotaCredito,  Cliente, Referencia, Ruc, 'Procesado'=ProcesadoNC, 'Estado'=EstadoNC, UsuarioAprobado, ProcesadoNC From VPedidoNotaCredito Where AnuladoAprobado = 'True' and IDSucursal = " & vgIDSucursal & " and EstadoNC in ('APROBADO', 'PENDIENTE') And ProcesadoNC='PENDIENTE' And cast(Fecha as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
        End If


        OrderBy = " Order By Suc,Cliente"

        Listar(SQL & OrderBy)

    End Sub

    Sub Listar(ByVal SQL As String)

        CSistema.SqlToLv(lvPedidos, SQL)

        'Seleccionar todos
        lvPedidos.Columns(7).TextAlign = HorizontalAlignment.Right
        lvPedidos.Columns(6).TextAlign = HorizontalAlignment.Right

        For Each item As ListViewItem In lvPedidos.Items
            If item.SubItems(9).Text = "APROBADO" Then
                item.Checked = True
            End If
        Next

        txtCantidadPedidos.txt.Text = lvPedidos.Items.Count
        lblFecha.Text = " * Fecha de Facturacion: " & VGFechaFacturacion.ToShortDateString


    End Sub

    Sub Detener()
        Try
            If vProcesar = False Then
                Exit Sub
            End If

            tProcesar.Abort()
            vProcesar = False
        Catch ex As Exception

        End Try

        Thread.Sleep(1000)

    End Sub

    Sub VerPedido()
        Dim frm As New frmPedidoNotaCredito

        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        For Each item As ListViewItem In lvPedidos.SelectedItems
            item.ImageIndex = 1
            frm.Show()
            ' frm.TableLayoutPanel2.Enabled = False
            frm.EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
            frm.CargarOperacion(item.SubItems(0).Text) ' IDTransaccion
        Next
    End Sub

    Sub FacturarPedido()

        VGFechaFacturacion = CSistema.ExecuteScalar("Select TOP 1 Fecha From FechaFacturacion Where IDSucursal=" & vgIDSucursal)

        lvGrupo.Enabled = False
        lvPedidos.Enabled = False

        For Each item As ListViewItem In lvPedidos.Items
            If item.Checked Then

                If item.SubItems(8).Text.ToUpper <> "PENDIENTE" Then
                    GoTo siguiente
                End If

                If item.SubItems(9).Text.ToUpper = "PENDIENTE" Then
                    GoTo siguiente
                End If

                item.ImageIndex = 1

                Dim ID As Integer = item.SubItems(2).Text
                Dim IDTransaccion As Integer = item.SubItems(0).Text
                'Dim Comprobante As String = item.SubItems(2).Text
                Dim frm As New frmNotaCreditoPedido

                'Dim vAnulado As Boolean = CSistema.RetornarValorBoolean(CData.GetRow(" IDTransaccion = " & IDTransaccion, "vPedido")("Anulado").ToString)
                Dim vAnulado As Boolean = CType(CSistema.ExecuteScalar("Select IsNull((Select Anulado From VPedidoNotaCredito Where IDTransaccion=" & IDTransaccion & "), 'False')"), Boolean)
                Dim vDesdePedidoVenta As Boolean = CType(CSistema.ExecuteScalar("Select IsNull((Select DesdePedidoVenta From VPedidoNotaCredito Where IDTransaccion=" & IDTransaccion & "), 'False')"), Boolean)
                If vAnulado Then
                    MessageBox.Show("El pedido que desea procesar fue anulado!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    GoTo siguiente
                End If

                If vDesdePedidoVenta Then
                    Dim vIDTransaccionVenta As Integer = 0
                    vIDTransaccionVenta = CSistema.ExecuteScalar("Select IDTransaccionVenta from vPedidoNotaCreditoPedidoVenta where IDTransaccionPedidoNotaCredito = " & IDTransaccion)
                    If vIDTransaccionVenta = 0 Then
                        MessageBox.Show("El pedido de venta asociado no esta facturado. Debe Facturar el Pedido de Venta antes de imprimir la Nota de Credito!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                        GoTo siguiente
                    End If
                End If

                Facturar(frm, ID, IDTransaccion)
                If frm.Procesado = False Then
                    GoTo siguiente
                End If
                'Actualizar el item
                Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpPedidoNotaCreditoEstado @IDTransaccionNotaCredito = " & frm.IDTransaccion & ", @IDPedido=" & ID & ", @IDSucursal=" & vgIDSucursal & " ").Copy
                Dim oRow As DataRow = dttemp.Rows(0)

                If oRow("Procesado") = False Then
                    item.SubItems(1).Text = oRow("Comprobante")
                    item.SubItems(2).Text = oRow("Mensaje")
                    lvPedidos.Columns(1).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                    lvPedidos.Columns(2).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                    item.SubItems(10).Text = "Error"
                    item.ImageIndex = 0
                Else
                    item.SubItems(1).Text = oRow("Comprobante")
                    item.SubItems(2).Text = oRow("Mensaje")
                    lvPedidos.Columns(1).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                    lvPedidos.Columns(2).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                    item.SubItems(10).Text = "Procesado"
                    item.ImageIndex = 2
                End If

                'Mostrar mensaje si es que se quiere continuar
                If item.Index < lvPedidos.Items.Count - 1 Then
                    Dim frmDialog As New frmDialog
                    frmDialog.Mensaje = "Desea continuar procesando?"
                    frmDialog.Titulo = "Pedido"

                    If frmDialog.ShowDialog(Me) = Windows.Forms.DialogResult.No Then
                        GoTo terminar
                    End If

siguiente:
                End If
            End If
        Next

terminar:

        lvGrupo.Enabled = True
        lvPedidos.Enabled = True
        vProcesar = False

    End Sub

    Sub FacturarPedido2()

        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        For Each item As ListViewItem In lvPedidos.SelectedItems

            If item.SubItems(10).Text.ToUpper <> "PENDIENTE" Then
                Exit For
            End If

            item.ImageIndex = 1

            Dim ID As Integer = item.Text
            Dim IDTransaccion As Integer = item.SubItems(11).Text
            Dim Comprobante As String = item.SubItems(1).Text
            'Dim vAutorizado As Boolean = False
            'vAutorizado = CType(CSistema.ExecuteScalar("Select IsNull((Select AutorizacionLineaCredito From Pedido Where IDTransaccion=" & IDTransaccion & "), 'False')"), Boolean)
            Dim frm As New frmNotaCreditoPedido
            'frm.ExcesoLineaCreditoAutorizado = vAutorizado
            'Dim vAnulado As Boolean = CSistema.RetornarValorBoolean(CData.GetRow(" IDTransaccion = " & IDTransaccion, "vPedido")("Anulado").ToString)
            Dim vAnulado As Boolean = CType(CSistema.ExecuteScalar("Select IsNull((Select Anulado From vPedidoNotaCredito Where IDTransaccion=" & IDTransaccion & "), 'False')"), Boolean)
            If vAnulado = False Then
                Facturar(frm, ID, IDTransaccion)

                'Actualizar el item
                Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpPedidoNotaCreditoEstado @IDTransaccionNotaCredito = " & frm.NuevoIDTransaccion & ", @IDPedido=" & ID & ", @IDSucursal=" & vgIDSucursal & " ").Copy
                Dim oRow As DataRow = dttemp.Rows(0)

                If oRow("Procesado") = False Then
                    item.SubItems(1).Text = oRow("Comprobante")
                    item.SubItems(2).Text = oRow("Mensaje")
                    lvPedidos.Columns(1).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                    lvPedidos.Columns(2).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                    item.SubItems(10).Text = "Error"
                    item.ImageIndex = 0
                Else
                    item.SubItems(1).Text = oRow("Comprobante")
                    item.SubItems(2).Text = oRow("Mensaje")
                    lvPedidos.Columns(1).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                    lvPedidos.Columns(2).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                    item.SubItems(10).Text = "Procesado"
                    item.ImageIndex = 2
                End If
            Else
                MessageBox.Show("El pedido que desea facturar fue anulado!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If


        Next

    End Sub

    Sub Facturar(ByVal frm As frmNotaCreditoPedido, ByVal ID As Integer, IDTransaccionPedido As Integer)

        Dim IDSubMotivoNotaCredito As Integer = CSistema.ExecuteScalar("Select IDSubMotivoNotaCredito from PedidoNotaCredito where idtransaccion =" & IDTransaccionPedido)

        frm.IDPedido = ID
        'frm.Comprobante = Comprobante
        frm.IDTransaccionPedido = IDTransaccionPedido
        frm.vIDSubMotivoNotaCredito = IDSubMotivoNotaCredito
        frm.Inicializar()

        FGMostrarFormulario(Me, frm, "Nota de Credito de Pedido Nro: " & ID, Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Function ControlarPorcentaje(ByVal Porcentaje As Decimal, ByVal Importe As Decimal) As Boolean
        ControlarPorcentaje = True
        If (Porcentaje > PorcentajeAsignado) And (Importe > ImporteAsignado) Then
            Return False
        End If
    End Function

    Sub SeleccionarTodo()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = True
        Next
    End Sub

    Sub QuitarSeleccion()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = False
        Next
    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        Select Case e.KeyCode
            Case Keys.Enter
                CSistema.SelectNextControl(Me, e.KeyCode)
        End Select

    End Sub

    Sub EliminarPedido()

        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        lvPedidos.SelectedItems(0).Remove()

    End Sub

    Private Sub frmAprobarPedido_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Detener()
    End Sub

    Private Sub frmAprobarPedido_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        ManejarTecla(e)
    End Sub

    Private Sub frmAprobarPedido_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnActualizarGrupo_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarGrupo.Click
        ListarGrupos()
    End Sub

    Private Sub cbxGrupos_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxGrupos.SelectedIndexChanged
        ListarGrupos()
    End Sub

    Private Sub lvGrupo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lvGrupo.SelectedIndexChanged
        SeleccionarGrupo()
    End Sub

    Private Sub btnProcesar_Click(sender As System.Object, e As System.EventArgs) Handles btnProcesar.Click
        'Procesar()
        FacturarPedido()
        ListarPedidos()
    End Sub

    Private Sub btnSeleccionarTodos_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionarTodos.Click
        SeleccionarTodo()
    End Sub

    Private Sub ToolStripButton1_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton1.Click
        QuitarSeleccion()
    End Sub

    Private Sub btnActualizarPedidos_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarPedidos.Click
        ListarPedidos()
    End Sub

    Private Sub btnVerPedido_Click(sender As System.Object, e As System.EventArgs) Handles btnVerPedido.Click
        VerPedido()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        nmModuloActivo = "SistemaBase"
        Me.Close()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmProcesarPedidoNotaCredito_Activate()
        Me.Refresh()
    End Sub
End Class
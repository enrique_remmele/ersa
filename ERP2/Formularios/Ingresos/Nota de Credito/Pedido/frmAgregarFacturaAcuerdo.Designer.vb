﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAgregarFacturaAcuerdo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtMonto = New ERP.ocxTXTNumeric()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 260
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(25, 38)
        Me.txtProducto.Margin = New System.Windows.Forms.Padding(0)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(445, 21)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 29
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(26, 15)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(53, 13)
        Me.Label8.TabIndex = 26
        Me.Label8.Text = "Producto:"
        '
        'txtMonto
        '
        Me.txtMonto.Color = System.Drawing.Color.Empty
        Me.txtMonto.Decimales = False
        Me.txtMonto.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtMonto.Location = New System.Drawing.Point(502, 38)
        Me.txtMonto.Name = "txtMonto"
        Me.txtMonto.Size = New System.Drawing.Size(89, 21)
        Me.txtMonto.SoloLectura = False
        Me.txtMonto.TabIndex = 25
        Me.txtMonto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMonto.Texto = "0"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(499, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(40, 13)
        Me.Label9.TabIndex = 27
        Me.Label9.Text = "Monto:"
        '
        'frmAgregarFacturaAcuerdo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(629, 97)
        Me.Controls.Add(Me.txtProducto)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtMonto)
        Me.Controls.Add(Me.Label9)
        Me.Name = "frmAgregarFacturaAcuerdo"
        Me.Text = "frmAgregarFacturaAcuerdo"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtProducto As ocxTXTProducto
    Friend WithEvents txtCliente As ocxTXTCliente
    Friend WithEvents Label8 As Label
    Friend WithEvents txtMonto As ocxTXTNumeric
    Friend WithEvents Label9 As Label
End Class

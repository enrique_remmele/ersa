﻿Public Class frmEstablecerAcuerdo
    Dim CSistema As New CSistema

    Public Property IDTransaccion As Integer
    Public Property dt As New DataTable
    Dim SaldoComprobante As Decimal

    Sub Inicializar()
        CargarInformacion()
    End Sub
    Sub CargarInformacion()

        dt = CSistema.ExecuteToDataTable("Select Descripcion, Cantidad, PrecioUnitario, DescuentoUnitario, 'Precio Original'= PrecioUnitario - DescuentoUnitario, 'Total'= (PrecioUnitario - DescuentoUnitario) * Cantidad , 'Porcentaje'=0, 'ImporteDescuento'=0, IDProducto, IDCliente, IDListaPrecio, IDImpuesto, IDProductoDescuento, IDTransaccion from vDetalleVenta where idtransaccion = " & IDTransaccion)
        SaldoComprobante = CSistema.ExecuteScalar("Select Saldo from venta where idtransaccion = " & IDTransaccion)

        CSistema.dtToGrid(dgv, dt)
        CSistema.DataGridColumnasNumericas(dgv, {"PrecioUnitario", "DescuentoUnitario", "Precio Original", "Total", "Porcentaje"}, True)


        dgv.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("IDProducto").Visible = False
        dgv.Columns("IDCliente").Visible = False
        dgv.Columns("IDListaPrecio").Visible = False
        dgv.Columns("IDImpuesto").Visible = False
        dgv.Columns("IDProductoDescuento").Visible = False
        dgv.Columns("ImporteDescuento").Visible = False
        dgv.Columns("IDTransaccion").Visible = False
    End Sub


    Sub Guardar()

        If CDec(txtTotal.Texto) > SaldoComprobante Then
            MessageBox.Show("El importe total supera al saldo del comprobante", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            Exit Sub
        End If

        For Each row As DataGridViewRow In dgv.Rows
            For Each col As DataGridViewColumn In dgv.Columns
                If col.Name = "ImporteDescuento" Then
                    If dt.Rows(row.Index)("Porcentaje") <= 0 Then
                        MessageBox.Show("El importe no puede ser cero", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                        Exit Sub
                        'dt.Rows(row.Index)("Porcentaje") = dt.Rows(row.Index)("Precio Original")
                    End If
                    dt.Rows(row.Index)(col.Index) = dt.Rows(row.Index)("Total") / dt.Rows(row.Index)("Porcentaje")
                Else
                    dt.Rows(row.Index)(col.Index) = dgv.Rows(row.Index).Cells(col.Index).Value
                End If
            Next
        Next

        Me.Close()

    End Sub

    Sub BloquearHabilitarEdicion()
        Select Case dgv.CurrentCell.OwningColumn.Name
            Case "Porcentaje"
                dgv.ReadOnly = False
            Case Else
                dgv.ReadOnly = True
        End Select
    End Sub

    Sub CalcularTotal()
        Dim Total As Decimal = 0

        For Each Row As DataGridViewRow In dgv.Rows
            Total = Total + ((Row.Cells("Total").Value * Row.Cells("Porcentaje").Value) / 100)
        Next

        txtTotal.txt.Text = Total

    End Sub

    Private Sub dgv_SelectionChanged(sender As Object, e As EventArgs) Handles dgv.Click
        BloquearHabilitarEdicion()
    End Sub

    Private Sub frmEstablecerDiferenciaPrecio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub dgv_KeyDown(sender As Object, e As KeyEventArgs) Handles dgv.KeyDown
        BloquearHabilitarEdicion()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        dt.Reset()
        Me.Close()
    End Sub

    Private Sub dgv_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgv.CellEndEdit
        CalcularTotal()
    End Sub

    '10-06-2021 - SC - Actualiza datos
    Sub frmEstablecerDiferenciaPrecio_Activate()
        Me.Refresh()
    End Sub

End Class
﻿Public Class frmEstablecerDiferenciaPeso
    Dim CSistema As New CSistema

    Public Property IDTransaccion As Integer
    Public Property dt As New DataTable
    Dim SaldoComprobante As Decimal

    Sub Inicializar()
        CargarInformacion()
    End Sub
    Sub CargarInformacion()

        dt = CSistema.ExecuteToDataTable("Select Descripcion, Cantidad, 'Precio Unitario'=PrecioUnitario- DescuentoUnitario, 'Precio Total Original'= Total, 'ImporteDescuento'=0, IDProducto, IDCliente, IDListaPrecio, IDImpuesto, IDProductoDescuento, IDTransaccion from vDetalleVenta where idtransaccion = " & IDTransaccion)
        SaldoComprobante = CSistema.ExecuteScalar("Select Saldo from venta where idtransaccion = " & IDTransaccion)

        CSistema.dtToGrid(dgv, dt)
        CSistema.DataGridColumnasNumericas(dgv, {"Precio Total Original", "ImporteDescuento", "Precio Unitario"}, True)


        dgv.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("IDProducto").Visible = False
        dgv.Columns("IDCliente").Visible = False
        dgv.Columns("Cantidad").Visible = False
        dgv.Columns("IDListaPrecio").Visible = False
        dgv.Columns("IDImpuesto").Visible = False
        dgv.Columns("IDProductoDescuento").Visible = False
        dgv.Columns("IDTransaccion").Visible = False
    End Sub

    Sub Guardar()

        If CDec(txtTotal.Texto) > SaldoComprobante Then
            MessageBox.Show("El importe total supera al saldo del comprobante", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            Exit Sub
        End If

        For Each row As DataGridViewRow In dgv.Rows
            For Each col As DataGridViewColumn In dgv.Columns
                If col.Name = "ImporteDescuento" Then
                    dt.Rows(row.Index)(col.Index) = dgv.Rows(row.Index).Cells(col.Index).Value
                End If
            Next
        Next

        Me.Close()

    End Sub

    Sub BloquearHabilitarEdicion()
        Select Case dgv.CurrentCell.OwningColumn.Name
            Case "ImporteDescuento"
                dgv.ReadOnly = False
            Case Else
                dgv.ReadOnly = True
        End Select
    End Sub

    Sub CalcularTotal()
        Dim Total As Decimal = 0

        For Each Row As DataGridViewRow In dgv.Rows
            Total = Total + Row.Cells("ImporteDescuento").Value
        Next

        txtTotal.txt.Text = Total

    End Sub

    Private Sub dgv_SelectionChanged(sender As Object, e As EventArgs) Handles dgv.Click
        BloquearHabilitarEdicion()
    End Sub

    Private Sub frmEstablecerDiferenciaPrecio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub dgv_KeyDown(sender As Object, e As KeyEventArgs) Handles dgv.KeyDown
        BloquearHabilitarEdicion()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        dt.Reset()
        Me.Close()
    End Sub

    Private Sub dgv_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgv.CellEndEdit
        CalcularTotal()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmEstablecerDiferenciaPeso_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class SeleccionarFacturaAcuerdo

    'Clases
    Dim CSistema As New CSistema
    Dim CData As New CData

    Dim dtDetalle As New DataTable
    Dim vDecimalOperacion As Boolean
    Dim vNuevo As Boolean
    Dim dt As DataTable
    Dim Plazo As Integer

    'Propiedades
    Public Property IDDeposito As Integer
    Public Property IDCliente As Integer
    Public Property IDSucursal As Integer
    Public Property IDProducto As Integer
    Public Property IDFamilia As Integer
    Public Property IDPresentacion As Integer
    'Public Property CantidadADevolver As Integer
    Public Property Monto As Decimal
    Public Property IdSucursalCliente As Integer
    Public Property TSucursales As Boolean
    Public Property dtAcuerdoFactura As DataTable

    Public Property ImporteNC As Decimal




    Sub Inicializar()

        'Plazo = CSistema.ExecuteScalar("Select top(1) Plazo from VencimientodeProductos where IDFamilia = " & IDFamilia & "and IDPresentacion = " & IDPresentacion)
        Plazo = CSistema.ExecuteScalar("Select top(1) PlazoDevolucionDesdeEmisionFactura from Configuraciones where idsucursal = " & IDSucursal)
        'If Plazo = "" Then
        '    MessageBox.Show("Este Producto no tiene configurado su vencimiento (Familia/Presentacion)", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    Exit Sub
        'End If

        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        Dim SQL As String = ""
        'dtFactura = CSistema.ExecuteToDataTable("Select * from vVenta where idtransaccion = " & IDTransaccion)
        'dtFactura = CSistema.ExecuteToDataTable("Select * from vVenta where IDTransaccion in (Select Distinct IDTransaccion from DetalleVenta where IDCliente = " & IDCliente & " and IDProducto = " & IDProducto & " and dbo.FPrecioUnitarioNeto(IDProducto, IDTransaccion) = " & PrecioUnitario & ") ")
        'Label1.Text = "Factura Nro: " & dtFactura(0)("Comprobante").ToString & " -  Fecha de Emision: " & CDate(dtFactura(0)("FechaEmision"))
        'dtDevolucion = CSistema.ExecuteToDataTable("Select 'Sel' = Cast(0 as bit), Referencia, Producto, Cantidad, 'Precio'=PrecioUnitario - DescuentoUnitario, 'Total'=0, idtransaccion, idproducto , 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0 From VDetallePedidoNotaCredito where idtransaccion = " & IDTransaccion)
        'Select *, 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0 From VDetallePedidoNotaCredito Where IDTransaccion=" & IDTransaccion & " Order By ID
        'If Monto > 0 Then
        '    If TSucursales = True Then
        '        SQL = "Select 'Sel' = Cast(0 as bit), 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0, *, 'Cantidad a Devolver'=0 From VDetalleVentaParaNotaCredito where Anulado = 0 and DATEDIFF(DD, FechaEmision, GetDate()) <= " & Plazo & " and IDCliente = " & IDCliente & " and IDProducto = " & IDProducto & " and PrecioUnitarioNeto = " & Monto & " and SaldoCantidad >= " & CantidadADevolver '& " and IDSucursalcliente = " & IdSucursalCliente
        '    Else
        '        SQL = "Select 'Sel' = Cast(0 as bit), 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0, *, 'Cantidad a Devolver'=0 From VDetalleVentaParaNotaCredito where Anulado = 0 and DATEDIFF(DD, FechaEmision, GetDate()) <= " & Plazo & " and IDCliente = " & IDCliente & " and IDProducto = " & IDProducto & " and PrecioUnitarioNeto = " & Monto & " and SaldoCantidad >= " & CantidadADevolver & " and IDSucursalcliente = " & IdSucursalCliente

        '    End If
        'End If
        'If Monto <= 0 Then
        '    If TSucursales = True Then
        SQL = "Select 'Sel' = Cast(0 as bit), 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0, *, 'Cantidad a Devolver'=0 From VDetalleVentaParaNotaCredito where Anulado = 0 and DATEDIFF(DD, FechaEmision, GetDate()) <= " & Plazo & " and IDCliente = " & IDCliente & " and IDProducto = " & IDProducto '& " and IDSucursalcliente = " & IdSucursalCliente
                '    Else
                '        SQL = "Select 'Sel' = Cast(0 as bit), 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0, *, 'Cantidad a Devolver'=0 From VDetalleVentaParaNotaCredito where Anulado = 0 and DATEDIFF(DD, FechaEmision, GetDate()) <= " & Plazo & " and IDCliente = " & IDCliente & " and IDProducto = " & IDProducto & " and IDSucursalcliente = " & IdSucursalCliente
                '    End If
                'End If

                dt = CSistema.ExecuteToDataTable(SQL)
        CSistema.SqlToDataGrid(dgv, SQL)

        Dim comboboxColumn As New DataGridViewComboBoxColumn
        'comboboxColumn.Name = "Motivo Devolucion"
        'comboboxColumn.DataSource = dtMotivoDevolucion
        comboboxColumn.DisplayMember = "Descripcion"
        comboboxColumn.ValueMember = "ID"
        dgv.Columns.Add(comboboxColumn)

        CSistema.DataGridColumnasVisibles(dgv, {"Sel", "Comprobante", "FechaEmision"})
        'CSistema.DataGridColumnasNumericas(dgv, {"Cantidad", "SaldoCantidad", "Cantidad a Devolver"}, True)
        'CSistema.DataGridColumnasNumericas(dgv, {"PrecioUnitarioNeto"}, True)
        'dgv.Columns("Motivo Devolucion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Update()

        If dgv.RowCount > 0 Then
            dgv.CurrentCell = dgv.Rows(0).Cells("Sel")
        End If


    End Sub


    Private Sub SeleccionarFacturaAcuerdo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        Dim SeleccionadoOK As Boolean = False

        For i = 0 To dgv.Rows.Count - 1

            If CBool(dgv.Rows(i).Cells("Sel").Value) = True Then
                'Comentario de prueba
                'If SeleccionadoOK Then
                '    MessageBox.Show("Debe seleccionar solo un comprobante", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                '    Exit Sub
                'End If
                'If dgv.Rows(i).Cells("Motivo Devolucion").Value Is Nothing Then
                '    MessageBox.Show("Debe seleccionar un motivo de devolucion!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '    Exit Sub
                'End If
                SeleccionadoOK = True
            End If
        Next


        CargarDetalle()


    End Sub

    Sub CargarDetalle() '(ByVal dtAcuerdoFactura As DataTable)

        Dim Producto As Decimal = 0
        Dim Impuesto As Integer = 0


        If dtAcuerdoFactura Is Nothing Then
            'MessageBox.Show("No se encontraron descuentos para este pedido", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If


        'If DescuentoFinanciero Then

        'End If

        For Each Row As DataRow In dtAcuerdoFactura.Select("[Porcentaje] <> [Precio Original]")
            Row("IDProducto").ToString()
            Impuesto = Row("ImporteDescuento")
            Producto = Row("IDProducto")
            'CargarDescuentosProducto(Impuesto, Producto)
        Next


    End Sub


    '10-06-2021 - SC - Actualiza datos
    Sub SeleccionarFacturaAcuerdo_Activate()
        Me.Refresh()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

End Class
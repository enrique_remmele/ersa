﻿Public Class SeleccionarFacturas

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private dtdetalleValue As DataTable
    Public Property dtdetalle() As DataTable
        Get
            Return dtdetalleValue
        End Get
        Set(ByVal value As DataTable)
            dtdetalleValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property
    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set

    End Property

    Public Property IDMoneda As Integer
    Public Property Decimales As Boolean
    Public Property Aplicar As Boolean = False
    Public Property Pedido As Boolean = False

    'FUNCIONES
    Sub Inicializar()

        'Formato DataGrid
        Dim CantidadDecimales As Integer = 0
        If Decimales Then
            CantidadDecimales = 2
        End If

        dgvComprobante.Columns("colTotal").DefaultCellStyle.Format = "N" & CantidadDecimales
        dgvComprobante.Columns("colCobrado").DefaultCellStyle.Format = "N" & CantidadDecimales
        dgvComprobante.Columns("colDescontado").DefaultCellStyle.Format = "N" & CantidadDecimales
        dgvComprobante.Columns("colSaldo").DefaultCellStyle.Format = "N" & CantidadDecimales
        dgvComprobante.Columns("colImporte").DefaultCellStyle.Format = "N" & CantidadDecimales

        ''txtTotal.Decimales = Decimales

        'Funciones
        CargarComprobante()

    End Sub

    Sub CargarComprobante()

        'Limpiar la grilla
        dgvComprobante.Rows.Clear()

        For Each oRow As DataRow In dt.Rows

            Dim Registro(11) As String
            Registro(0) = oRow("IDTransaccion").ToString
            'Registro(1) = oRow("Sel").ToString
            Registro(1) = False
            Registro(2) = oRow("Comprobante").ToString
            Registro(3) = oRow("Cod.").ToString
            Registro(4) = oRow("Condicion").ToString
            Registro(5) = oRow("Fec. Venc.").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
            Registro(7) = CSistema.FormatoMoneda(Decimales)
            Registro(8) = CSistema.FormatoMoneda(Decimales)
            Registro(9) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)

            If CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales) > 0 Then
                Registro(10) = CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales)
            Else
                Registro(10) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)
            End If

            Registro(11) = oRow("Cancelar").ToString

            'Sumar el total de la deuda
            dgvComprobante.Rows.Add(Registro)

        Next

        'Formato
        dgvComprobante.Columns("Sel").ReadOnly = False

        If dgvComprobante.Rows.Count > 0 Then
            Me.ActiveControl = dgvDetalle
        End If

        'Totales
        txtCantidadComprobante.SetValue(dgvComprobante.RowCount)

        dgvComprobante.Columns("DataGridViewTextBoxColumn1").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill


    End Sub

    Sub CargarProductos()

        'Dim SQL As String = "Select Descripcion, Cantidad, PrecioUnitario, DescuentoUnitario, 'Precio Original'= PrecioUnitario - DescuentoUnitario, 'Total'= (PrecioUnitario - DescuentoUnitario) * Cantidad , 'Porcentaje'=0, 'ImporteDescuento'=0, IDProducto, IDCliente, IDListaPrecio, IDImpuesto, IDProductoDescuento, IDTransaccion from vDetalleVenta"
        'Dim SQL As String = "Select dv.Descripcion, dv.Cantidad, dv.PrecioUnitario, dv.DescuentoUnitario, 'Precio Original'= dv.PrecioUnitario - dv.DescuentoUnitario, 'Total'= (dv.PrecioUnitario - dv.DescuentoUnitario) * dv.Cantidad , 'Porcentaje'=CONVERT(decimal(12,4),(dv.Total / v.Total) * (100)), 'A asignar'=0, 'ImporteDescuento'=0, dv.IDProducto, dv.IDCliente, dv.IDListaPrecio, dv.IDImpuesto, dv.IDProductoDescuento, dv.IDTransaccion from vDetalleVenta dv join venta v on v.idtransaccion = dv.IDTransaccion"
        Dim SQL As String = "Select dv.Descripcion, dv.Cantidad, dv.PrecioUnitario, dv.DescuentoUnitario, 'Precio Original'= dv.PrecioUnitario - dv.DescuentoUnitario, 'Total'= (dv.PrecioUnitario - dv.DescuentoUnitario) * dv.Cantidad , 'Porcentaje'=round(((dv.Total / v.Total) * (100)),0), 'A asignar'=0, 'ImporteDescuento'=0, dv.IDProducto, dv.IDCliente, dv.IDListaPrecio, dv.IDImpuesto, dv.IDProductoDescuento, dv.IDTransaccion from vDetalleVenta dv join venta v on v.idtransaccion = dv.IDTransaccion"
        'dt = CSistema.ExecuteToDataTable("Select Descripcion, Cantidad, PrecioUnitario, DescuentoUnitario, 'Precio Original'= PrecioUnitario - DescuentoUnitario, 'Total'= (PrecioUnitario - DescuentoUnitario) * Cantidad , 'Porcentaje'=0, 'ImporteDescuento'=0, IDProducto, IDCliente, IDListaPrecio, IDImpuesto, IDProductoDescuento, IDTransaccion from vDetalleVenta")
        'where idtransaccion = " & IDTransaccion
        '"Select IDTransaccion, ID, IDProducto, Producto, 'Cantidad'=CantidadAEntregar,'Fecha Entrega'= FecEntrega, 'Comprobante'=Concat(Cod,'-',NroComprobante), 'Peso'=(CantidadAEntregar*Peso), Saldo, 'PesoUnitario'=Peso from VDetalleOrdenDePedido  "
        Dim Where As String = ""
        Dim GroupBy As String = " "

        For Each oRow As DataGridViewRow In dgvComprobante.Rows

            If oRow.Cells("Sel").Value = True Then
                If Where = "" Then
                    Where = " Where dv.IDTransaccion=" & oRow.Cells("IDTransaccion").Value & " "
                Else
                    Where = Where & " Or (dv.IDTransaccion=" & oRow.Cells("IDTransaccion").Value & ") "
                End If
            Else
                txtCantidadDetalle.txt.Text = ""
                txtImporteTotal.txt.Text = 0
                txtImporteaasignar.txt.Text = 0
                txtporcentaje.txt.Text = ""
            End If
        Next

        If Where = "" Then
            dtdetalle.Rows.Clear()
            Exit Sub
        Else
            Where = Where
            dtdetalle = CSistema.ExecuteToDataTable(SQL & Where & GroupBy)
        End If

        CSistema.dtToGrid(dgvDetalle, dtdetalle)
        'CSistema.DataGridColumnasNumericas(dgvDetalle, {"PrecioUnitario", "DescuentoUnitario", "Precio Original", "Total", "Porcentaje"}, True)
        CSistema.DataGridColumnasNumericas(dgvDetalle, {"PrecioUnitario", "DescuentoUnitario", "Precio Original", "Total", "Porcentaje", "A Asignar"}, True)

        'Formato
        dgvDetalle.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvDetalle.Columns("IDProducto").Visible = False
        dgvDetalle.Columns("IDCliente").Visible = False
        dgvDetalle.Columns("IDListaPrecio").Visible = False
        dgvDetalle.Columns("IDImpuesto").Visible = False
        dgvDetalle.Columns("IDProductoDescuento").Visible = False
        dgvDetalle.Columns("ImporteDescuento").Visible = False
        dgvDetalle.Columns("IDTransaccion").Visible = False

        If dgvDetalle.Rows.Count > 0 Then
            Me.ActiveControl = dgvComprobante
        End If

        'Totales
        txtCantidadDetalle.SetValue(dgvDetalle.RowCount)
        txtImporteTotal.txt.Text = CSistema.FormatoMoneda(CSistema.dtSumColumn(dtdetalle, "Total"), True)
        txtporcentaje.txt.Text = CSistema.FormatoMoneda(CSistema.dtSumColumn(dtdetalle, "Porcentaje"), True)

    End Sub

    Sub Aceptar()

        Procesado = True
        Me.Close()

    End Sub

    Sub Cancelar()

        Procesado = False
        Me.Close()

    End Sub

    Sub EliminarProducto()
        'If IDTransaccion > 0 Then
        '    Exit Sub
        'End If

        'Validar
        If dgvDetalle.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgvDetalle, mensaje)
            ctrError.SetIconAlignment(dgvDetalle, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'dt.Rows(dgvDetalle.SelectedRows(0).Index).Delete()
        dtdetalle.Rows.RemoveAt(dgvDetalle.SelectedRows(0).Index)

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtdetalle.Rows
            'oRow("ID") = Index
            'Index = Index + 1
        Next

        'RecargarComprobante()

        txtCantidadDetalle.SetValue(dgvDetalle.RowCount)
        txtImporteTotal.txt.Text = CSistema.FormatoMoneda(CSistema.dtSumColumn(dtdetalle, "Total"), True)
        txtporcentaje.txt.Text = CSistema.FormatoMoneda(CSistema.dtSumColumn(dtdetalle, "Porcentaje"), True)

    End Sub

    Private Sub frmRemisionCargarFactura_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        'If Me.ActiveControl.Name = txtNroComprobante.Name Then
        '    Exit Sub
        'End If

        If Me.ActiveControl.Name = dgvComprobante.Name Then
            Exit Sub
        End If

        If Me.ActiveControl.Name = dgvDetalle.Name Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmRemisionCargarFactura_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    'Private Sub btnCargarComprobante_Click(sender As System.Object, e As System.EventArgs) Handles btnCargarComprobante.Click
    '    CargarComprobante(" cast(Fecha as date) Between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' ")
    'End Sub

    'Private Sub lklComprobantesMes_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklComprobantesMes.LinkClicked
    '        CargarComprobante(" Month(Fecha)=" & Date.Now.Month & " And Year(Fecha)=" & Date.Now.Year & " ")
    '    End Sub

    'Private Sub lklSeleccionarTodo_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklSeleccionarTodo.LinkClicked

    'If lklSeleccionarTodo.Text = "Seleccionar todo" Then
    '            For Each oRow As DataGridViewRow In dgvComprobante.Rows
    '                oRow.Cells("Sel").Value = True
    '            Next
    '            lklSeleccionarTodo.Text = "Quitar todo"
    '            CargarProductos()
    '            Exit Sub
    '        End If

    '        If lklSeleccionarTodo.Text = "Quitar todo" Then
    '            For Each oRow As DataGridViewRow In dgvComprobante.Rows
    '                oRow.Cells("Sel").Value = False
    '            Next
    '            lklSeleccionarTodo.Text = "Seleccionar todo"
    '            CargarProductos()
    '            Exit Sub
    '        End If

    'End Sub

    Private Sub lklQuitarSeleccionados_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklQuitarSeleccionados.LinkClicked
        EliminarProducto()
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Aceptar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub dgvComprobante_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvComprobante.CellClick

        Dim CantidadPedidoNCPendientes As Integer = 0
        CantidadPedidoNCPendientes = CSistema.ExecuteScalar("Select CantidadPedidoNCPendientes from dbo.FNotaCreditoPorVenta(" & dgvComprobante.SelectedRows(0).Cells("IDTransaccion").Value & ")")

        If CantidadPedidoNCPendientes > 0 And dgvComprobante.SelectedRows(0).Cells("Sel").Value = True Then
            MessageBox.Show("El documento ya tiene Pedidos de NC pendientes de Procesar. Elegir otro Comprobante", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            dgvComprobante.SelectedRows(0).Cells("Sel").Value = False
            dgvDetalle.Columns.Clear()
            Exit Sub
        End If

        'dgvComprobante.SelectedRows(0).Cells("Sel").Value = CStr(Not CBool(dgvComprobante.SelectedRows(0).Cells("Sel").Value))
        'MArcar

        If dgvComprobante.SelectedRows.Count > 0 Then

            For i = 0 To dgvComprobante.Rows.Count - 1
                dgvComprobante.Rows(i).Cells("Sel").Value = False
            Next
        End If
        dgvComprobante.CurrentRow.Cells("Sel").Value = True

        CargarProductos()
        Procesar()
    End Sub

    'Private Sub dgvComprobante_Click(sender As Object, e As System.EventArgs) Handles dgvComprobante.Click

    '    Dim CantidadPedidoNCPendientes As Integer = 0

    '    dgvComprobante.SelectedRows(0).Cells("Sel").Value = CStr(Not CBool(dgvComprobante.SelectedRows(0).Cells("Sel").Value))

    '    CantidadPedidoNCPendientes = CSistema.ExecuteScalar("Select CantidadPedidoNCPendientes from dbo.FNotaCreditoPorVenta(" & dgvComprobante.SelectedRows(0).Cells("IDTransaccion").Value & ")")

    '    If CantidadPedidoNCPendientes > 0 And dgvComprobante.SelectedRows(0).Cells("Sel").Value = True Then
    '        MessageBox.Show("El documento ya tiene Pedidos de NC pendientes de Procesar. Elegir otro Comprobante", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
    '        Exit Sub
    '    End If

    '    CargarProductos()
    '    Procesar()

    'End Sub

    Private Sub dgvComprobante_DoubleClick(sender As Object, e As System.EventArgs) Handles dgvComprobante.DoubleClick

        Dim CantidadPedidoNCPendientes As Integer = 0
        CantidadPedidoNCPendientes = CSistema.ExecuteScalar("Select CantidadPedidoNCPendientes from dbo.FNotaCreditoPorVenta(" & dgvComprobante.SelectedRows(0).Cells("IDTransaccion").Value & ")")

        If CantidadPedidoNCPendientes > 0 And dgvComprobante.SelectedRows(0).Cells("Sel").Value = True Then
            MessageBox.Show("El documento ya tiene Pedidos de NC pendientes de Procesar. Elegir otro Comprobante", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            dgvComprobante.SelectedRows(0).Cells("Sel").Value = False
            dgvDetalle.Columns.Clear()
            Exit Sub
        End If

        'MArcar
        If dgvComprobante.SelectedRows.Count > 0 Then

            For i = 0 To dgvComprobante.Rows.Count - 1
                dgvComprobante.Rows(i).Cells("Sel").Value = False
            Next
        End If
        dgvComprobante.CurrentRow.Cells("Sel").Value = True

        CargarProductos()
        Procesar()
    End Sub

    Private Sub dgvComprobante_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvComprobante.KeyDown

        'If e.KeyCode = Keys.Enter Then

        '    'MArcar
        '    If dgvComprobante.SelectedRows.Count > 0 Then

        '        dgvComprobante.SelectedRows(0).Cells("Sel").Value = CStr(Not CBool(dgvComprobante.SelectedRows(0).Cells("Sel").Value))

        '        If dgvComprobante.SelectedRows(0).Index <> dgvComprobante.Rows.Count - 1 Then
        '            dgvComprobante.CurrentCell = dgvComprobante.SelectedRows(0).Cells("Sel")
        '        End If

        '        CargarProductos()
        '    End If
        '    e.SuppressKeyPress = True
        'End If

        Dim CantidadPedidoNCPendientes As Integer = 0
        CantidadPedidoNCPendientes = CSistema.ExecuteScalar("Select CantidadPedidoNCPendientes from dbo.FNotaCreditoPorVenta(" & dgvComprobante.SelectedRows(0).Cells("IDTransaccion").Value & ")")

        If CantidadPedidoNCPendientes > 0 And dgvComprobante.SelectedRows(0).Cells("Sel").Value = True Then
            MessageBox.Show("El documento ya tiene Pedidos de NC pendientes de Procesar. Elegir otro Comprobante", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            dgvComprobante.SelectedRows(0).Cells("Sel").Value = False
            dgvDetalle.Columns.Clear()
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then

            'MArcar
            If dgvComprobante.SelectedRows.Count > 0 Then

                For i = 0 To dgvComprobante.Rows.Count - 1
                    dgvComprobante.Rows(i).Cells("Sel").Value = False
                Next
            End If
            dgvComprobante.CurrentRow.Cells("Sel").Value = True

            CargarProductos()
            Procesar()
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub dgvDetalle_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvDetalle.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If
    End Sub


    Private Sub dgvDetalle_CellEndEdit(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellEndEdit

        CalcularTotal()
        'RecargarComprobante()

    End Sub

    Sub CalcularTotal()
        Dim Total As Decimal = 0
        Dim Aasignar As Decimal = 0

        Aasignar = txtImporteaasignar.txt.Text

        'For Each Row As DataGridViewRow In dgvDetalle.Rows
        '    Total = Total + ((Row.Cells("Total").Value * Row.Cells("Porcentaje").Value) / 100)
        'Next
        For Each Row As DataGridViewRow In dgvDetalle.Rows
            'Total = Total + ((Row.Cells("Total").Value * Row.Cells("Porcentaje").Value) / 100)
            Row.Cells("A asignar").Value = Aasignar * (Row.Cells("Porcentaje").Value) / 100
        Next

        'txtImporteTotal.txt.Text = Total
        txtporcentaje.txt.Text = CSistema.FormatoMoneda(CSistema.dtSumColumn(dtdetalle, "Porcentaje"), True)
        If txtporcentaje.txt.Text > 100 Then
            tsslEstado.Text = "Porcentaje no puede ser mayor al 100%"
            ctrError.SetError(btnAceptar, "Atencion: ")
            ctrError.SetIconAlignment(btnAceptar, ErrorIconAlignment.TopRight)
            btnAceptar.Enabled = False
        Else
            tsslEstado.Text = ""
            btnAceptar.Enabled = True
        End If

    End Sub

    Sub Procesar()

        Procesado = True
        For Each oRow As DataGridViewRow In dgvComprobante.Rows
            If oRow.Cells("Sel").Value = True Then
                Dim DocumentoRow As DataRow = CData.GetRow("IDTransaccion = " & oRow.Cells("IDTransaccion").Value, dt)
                If Not DocumentoRow Is Nothing Then
                    DocumentoRow("Sel") = oRow.Cells("Sel").Value
                    DocumentoRow("Importe") = oRow.Cells("colImporte").Value.ToString.Replace(".", "")
                    DocumentoRow("Cancelar") = oRow.Cells("colCancel").Value.ToString.Replace(".", "")
                End If
            End If
        Next

        'Me.Close()

    End Sub

    'Sub RecargarComprobante()

    '    dgvComprobante.Rows.Clear()

    '    For Each oRow As DataRow In dt.Rows

    '        Dim Registro(11) As String

    '        Registro(0) = oRow("IDTransaccion").ToString
    '        Registro(1) = oRow("Sel").ToString
    '        Registro(2) = oRow("Comprobante").ToString
    '        Registro(3) = oRow("Cod.").ToString
    '        Registro(4) = oRow("Condicion").ToString
    '        Registro(5) = oRow("Fec. Venc.").ToString
    '        Registro(6) = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
    '        Registro(7) = CSistema.FormatoMoneda(Decimales)
    '        Registro(8) = CSistema.FormatoMoneda(Decimales)
    '        Registro(9) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)
    '        If CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales) > 0 Then
    '            Registro(10) = CSistema.dtSumColumn(dtdetalle, "Total") / 100
    '        Else
    '            Registro(10) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)
    '        End If

    '        dgvComprobante.Rows.Add(Registro)

    '    Next

    '    'Formato
    '    'dgvComprobante.Columns("Sel").ReadOnly = False

    '    If dgvComprobante.Rows.Count > 0 Then
    '        Me.ActiveControl = dgvDetalle
    '    End If

    '    'Totales
    '    txtCantidadComprobante.SetValue(dgvComprobante.RowCount)

    '    dgvComprobante.Columns("DataGridViewTextBoxColumn1").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    'End Sub

    'Sub ReCargarComprobante()

    '    Dim SQL As String = "Select 'Sel'='False', Comprobante,[Cod.], Cliente, Total, Cobrado, Descontado, Saldo, Condicion,Fec,[Fec. Venc.], 'Importe'=0.0, 'Cancelar'='False' From VVenta"
    '    Dim Where As String = ""
    '    Dim GroupBy As String = " "

    '    For Each oRow As DataGridViewRow In dgvComprobante.Rows

    '        If oRow.Cells("Sel").Value = True Then
    '            If Where = "" Then
    '                Where = " Where IDTransaccion=" & oRow.Cells("IDTransaccion").Value & " "
    '            Else
    '                Where = Where & " Or (IDTransaccion=" & oRow.Cells("IDTransaccion").Value & ") "
    '            End If
    '        End If
    '    Next

    '    If Where = "" Then
    '        dtdetalle.Rows.Clear()
    '        Exit Sub
    '    Else
    '        Where = Where
    '        dt = CSistema.ExecuteToDataTable(SQL & Where & GroupBy)
    '    End If

    '    CSistema.dtToGrid(dgvComprobante, dt)
    '    'CSistema.DataGridColumnasNumericas(dgvcomprobante, {"PrecioUnitario", "DescuentoUnitario", "Precio Original", "Total", "Porcentaje"}, True)

    '    'Formato
    '    'dgvDetalle.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
    '    'dgvDetalle.Columns("IDProducto").Visible = False
    '    'dgvDetalle.Columns("IDCliente").Visible = False
    '    'dgvDetalle.Columns("IDListaPrecio").Visible = False
    '    'dgvDetalle.Columns("IDImpuesto").Visible = False
    '    'dgvDetalle.Columns("IDProductoDescuento").Visible = False
    '    'dgvDetalle.Columns("ImporteDescuento").Visible = False
    '    'dgvDetalle.Columns("IDTransaccion").Visible = False

    '    If dgvComprobante.Rows.Count > 0 Then
    '        Me.ActiveControl = dgvDetalle
    '    End If

    '    'Totales
    '    txtCantidadDetalle.SetValue(dgvComprobante.RowCount)
    '    txtImporteTotal.txt.Text = CSistema.FormatoMoneda(CSistema.dtSumColumn(dtdetalle, "Total"), True)

    'End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub SeleccionarFacturas_Activate()
        Me.Refresh()
    End Sub


End Class
﻿Public Class frmAutorizarDevolucion
    Public Property IDTransaccion As Integer
    Public Property Operacion As String
    Public Property Procesado As Boolean
    Public Property NroOperacion As Integer
    Public Property SubMotivo As String
    Public Property IDSucursal As Integer

    Dim CSistema As New CSistema
    Dim dtDepositosDesignados As DataTable

    Sub Inicializar()

        CargaInformacion()

    End Sub

    Sub CargaInformacion()

        dtDepositosDesignados = CSistema.ExecuteToDataTable("Select Destino,IDDeposito,Deposito from vDestinoDevolucion where idsucursal = " & IDSucursal)


    End Sub

    Sub Guardar()

        Dim IDSubMotivoNC As Integer = 0

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ObservacionAutorizador", txtObservacion.text, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@DestinoDevolucion", cbxDestino.SelectedValue.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        '@Procesado (de la sp) es Verdadero
        If CSistema.ExecuteStoreProcedure(param, "SpAprobarPedidoNotaCredito", False, False, MensajeRetorno) = True Then
            MessageBox.Show("Se proceso correctamente la Operacion N° '" & NroOperacion & "' donde el Sub Motivo es '" & SubMotivo & "'. ", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else '@Procesado (de la sp) es Falso
            MessageBox.Show("No se ha procesado correctamente la Operacion N° '" & NroOperacion & "' - " & MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Guardar()
        Procesado = True
        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Procesado = False
        Me.Close()
    End Sub

    Private Sub cbxDestino_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbxDestino.SelectedValueChanged
        lblDeposito.Text = "Deposito: " & dtDepositosDesignados.Select(" Destino= " & cbxDestino.SelectedValue.ToString)(0)("Deposito").ToString()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmAutorizarDevolucion_Activate()
        Me.Refresh()
    End Sub
End Class
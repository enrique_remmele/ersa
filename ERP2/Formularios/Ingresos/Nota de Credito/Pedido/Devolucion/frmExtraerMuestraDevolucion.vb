﻿Public Class frmExtraerMuestraDevolucion

    Public Property Procesado As Boolean
    Public Property IDTransaccion As Integer
    Public Property dtProductosExtraer As New DataTable
    Public Property dtPedidoNotaCredito As New DataTable

    Dim CSistema As New CSistema
    Dim dtProductos As New DataTable


    Sub Inicializar()
        CargarInformacion()
    End Sub

    Sub CargarInformacion()
        dtPedidoNotaCredito = CSistema.ExecuteToDataTable("Select * from vPedidoNotaCredito where idtransaccion = " & IDTransaccion)
        dtProductos = CSistema.ExecuteToDataTable("Select idproducto,Producto, MotivoDevolucion,Cantidad,'Extraer'=0 from vDetallePedidoNotaCredito where idtransaccion = " & IDTransaccion)

        dtProductosExtraer = dtProductos.Clone()

        CSistema.dtToGrid(dgv, dtProductos)
        CSistema.DataGridColumnasVisibles(dgv, {"Producto", "Cantidad", "Extraer"})
        dgv.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill



    End Sub

    Sub Generar()
        For Each oRow As DataRow In dtProductos.Rows
            If IsNumeric(oRow("Extraer")) Then

                If oRow("Extraer") > 0 Then
                    Dim NewRow As DataRow = dtProductosExtraer.NewRow()
                    For i = 0 To dtProductos.Columns.Count - 1
                        If dtProductos.Columns(i).ColumnName = "Cantidad" Then
                            NewRow(i) = oRow("Extraer")
                        Else
                            NewRow(i) = oRow(i)
                        End If
                    Next
                    dtProductosExtraer.Rows.Add(NewRow)
                End If
            End If
        Next

        Dim frm As New frmMuestraParaPrueba
        frm.Show()
        frm.CargarMuestra(dtProductosExtraer, dtPedidoNotaCredito, "Panaderia")


    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Procesado = False
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Generar()
        Procesado = True
        Me.Close()
    End Sub

    Private Sub dgv_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv.CellContentClick

        If dgv.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        dgv.CurrentCell = dgv.Rows(e.RowIndex).Cells(4)


    End Sub

    Private Sub frmExtraerMuestraDevolucion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmExtraerMuestraDevolucion_Activate()
        Me.Refresh()
    End Sub
End Class
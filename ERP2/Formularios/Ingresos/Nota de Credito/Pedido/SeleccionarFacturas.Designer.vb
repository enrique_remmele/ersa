﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SeleccionarFacturas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lklQuitarSeleccionados = New System.Windows.Forms.LinkLabel()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblCantidadDetalle = New System.Windows.Forms.Label()
        Me.lblImporteTotal = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvComprobante = New System.Windows.Forms.DataGridView()
        Me.IDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColCondicion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColVencimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColCobrado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColDescontado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColCancel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblCantidadComprobante = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtCantidadDetalle = New ERP.ocxTXTString()
        Me.txtImporteTotal = New ERP.ocxTXTString()
        Me.txtporcentaje = New ERP.ocxTXTString()
        Me.txtImporteaasignar = New ERP.ocxTXTString()
        Me.txtCantidadComprobante = New ERP.ocxTXTString()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.dgvComprobante, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lklQuitarSeleccionados
        '
        Me.lklQuitarSeleccionados.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklQuitarSeleccionados.Location = New System.Drawing.Point(3, 0)
        Me.lklQuitarSeleccionados.Name = "lklQuitarSeleccionados"
        Me.lklQuitarSeleccionados.Size = New System.Drawing.Size(160, 25)
        Me.lklQuitarSeleccionados.TabIndex = 0
        Me.lklQuitarSeleccionados.TabStop = True
        Me.lklQuitarSeleccionados.Text = "Quitar seleccionados"
        Me.lklQuitarSeleccionados.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel5.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(568, 217)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(177, 28)
        Me.FlowLayoutPanel5.TabIndex = 3
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.53476!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.46524!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel5, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvDetalle, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvComprobante, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 63.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(748, 492)
        Me.TableLayoutPanel1.TabIndex = 41
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel4.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(568, 431)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(177, 58)
        Me.FlowLayoutPanel4.TabIndex = 5
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(99, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(15, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(78, 23)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.AllowUserToOrderColumns = True
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.TableLayoutPanel1.SetColumnSpan(Me.dgvDetalle, 2)
        Me.dgvDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDetalle.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvDetalle.Location = New System.Drawing.Point(3, 251)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.Size = New System.Drawing.Size(742, 174)
        Me.dgvDetalle.TabIndex = 4
        '
        'FlowLayoutPanel1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.FlowLayoutPanel1, 2)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(742, 28)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.lklQuitarSeleccionados)
        Me.FlowLayoutPanel2.Controls.Add(Me.lblCantidadDetalle)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtCantidadDetalle)
        Me.FlowLayoutPanel2.Controls.Add(Me.lblImporteTotal)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtImporteTotal)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtporcentaje)
        Me.FlowLayoutPanel2.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtImporteaasignar)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 431)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(559, 58)
        Me.FlowLayoutPanel2.TabIndex = 6
        '
        'lblCantidadDetalle
        '
        Me.lblCantidadDetalle.Location = New System.Drawing.Point(169, 0)
        Me.lblCantidadDetalle.Name = "lblCantidadDetalle"
        Me.lblCantidadDetalle.Size = New System.Drawing.Size(58, 28)
        Me.lblCantidadDetalle.TabIndex = 1
        Me.lblCantidadDetalle.Text = "Cantidad:"
        Me.lblCantidadDetalle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblImporteTotal
        '
        Me.lblImporteTotal.Location = New System.Drawing.Point(317, 0)
        Me.lblImporteTotal.Name = "lblImporteTotal"
        Me.lblImporteTotal.Size = New System.Drawing.Size(86, 28)
        Me.lblImporteTotal.TabIndex = 4
        Me.lblImporteTotal.Text = "Importe Total:"
        Me.lblImporteTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(3, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(400, 24)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Monto:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgvComprobante
        '
        Me.dgvComprobante.AllowUserToAddRows = False
        Me.dgvComprobante.AllowUserToDeleteRows = False
        Me.dgvComprobante.AllowUserToOrderColumns = True
        Me.dgvComprobante.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvComprobante.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDTransaccion, Me.Sel, Me.DataGridViewTextBoxColumn1, Me.ColTipo, Me.ColCondicion, Me.ColVencimiento, Me.ColTotal, Me.ColCobrado, Me.ColDescontado, Me.ColSaldo, Me.ColImporte, Me.ColCancel})
        Me.TableLayoutPanel1.SetColumnSpan(Me.dgvComprobante, 2)
        Me.dgvComprobante.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvComprobante.Location = New System.Drawing.Point(3, 37)
        Me.dgvComprobante.MultiSelect = False
        Me.dgvComprobante.Name = "dgvComprobante"
        Me.dgvComprobante.ReadOnly = True
        Me.dgvComprobante.RowHeadersVisible = False
        Me.dgvComprobante.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvComprobante.Size = New System.Drawing.Size(742, 174)
        Me.dgvComprobante.StandardTab = True
        Me.dgvComprobante.TabIndex = 1
        '
        'IDTransaccion
        '
        Me.IDTransaccion.HeaderText = "IDTransaccion"
        Me.IDTransaccion.Name = "IDTransaccion"
        Me.IDTransaccion.ReadOnly = True
        Me.IDTransaccion.Visible = False
        '
        'Sel
        '
        Me.Sel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Sel.HeaderText = "Sel"
        Me.Sel.Name = "Sel"
        Me.Sel.ReadOnly = True
        Me.Sel.Width = 28
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn1.HeaderText = "Comprobante"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 95
        '
        'ColTipo
        '
        Me.ColTipo.HeaderText = "Tipo"
        Me.ColTipo.Name = "ColTipo"
        Me.ColTipo.ReadOnly = True
        Me.ColTipo.Width = 50
        '
        'ColCondicion
        '
        Me.ColCondicion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.ColCondicion.HeaderText = "Cond."
        Me.ColCondicion.Name = "ColCondicion"
        Me.ColCondicion.ReadOnly = True
        Me.ColCondicion.Width = 60
        '
        'ColVencimiento
        '
        Me.ColVencimiento.HeaderText = "Venc."
        Me.ColVencimiento.Name = "ColVencimiento"
        Me.ColVencimiento.ReadOnly = True
        Me.ColVencimiento.Visible = False
        Me.ColVencimiento.Width = 75
        '
        'ColTotal
        '
        Me.ColTotal.HeaderText = "Total"
        Me.ColTotal.Name = "ColTotal"
        Me.ColTotal.ReadOnly = True
        Me.ColTotal.Width = 78
        '
        'ColCobrado
        '
        Me.ColCobrado.HeaderText = "Cobrado"
        Me.ColCobrado.Name = "ColCobrado"
        Me.ColCobrado.ReadOnly = True
        Me.ColCobrado.Width = 78
        '
        'ColDescontado
        '
        Me.ColDescontado.HeaderText = "Descontado"
        Me.ColDescontado.Name = "ColDescontado"
        Me.ColDescontado.ReadOnly = True
        Me.ColDescontado.Width = 78
        '
        'ColSaldo
        '
        Me.ColSaldo.HeaderText = "Saldo"
        Me.ColSaldo.Name = "ColSaldo"
        Me.ColSaldo.ReadOnly = True
        Me.ColSaldo.Width = 78
        '
        'ColImporte
        '
        Me.ColImporte.HeaderText = "Importe"
        Me.ColImporte.Name = "ColImporte"
        Me.ColImporte.ReadOnly = True
        Me.ColImporte.Width = 80
        '
        'ColCancel
        '
        Me.ColCancel.HeaderText = "Cancel"
        Me.ColCancel.Name = "ColCancel"
        Me.ColCancel.ReadOnly = True
        Me.ColCancel.Width = 35
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.lblCantidadComprobante)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtCantidadComprobante)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 217)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(559, 28)
        Me.FlowLayoutPanel3.TabIndex = 2
        '
        'lblCantidadComprobante
        '
        Me.lblCantidadComprobante.Location = New System.Drawing.Point(3, 0)
        Me.lblCantidadComprobante.Name = "lblCantidadComprobante"
        Me.lblCantidadComprobante.Size = New System.Drawing.Size(54, 28)
        Me.lblCantidadComprobante.TabIndex = 1
        Me.lblCantidadComprobante.Text = "Cantidad:"
        Me.lblCantidadComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 492)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(748, 22)
        Me.StatusStrip1.TabIndex = 42
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'txtCantidadDetalle
        '
        Me.txtCantidadDetalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCantidadDetalle.Color = System.Drawing.Color.Empty
        Me.txtCantidadDetalle.Indicaciones = Nothing
        Me.txtCantidadDetalle.Location = New System.Drawing.Point(233, 5)
        Me.txtCantidadDetalle.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtCantidadDetalle.Multilinea = False
        Me.txtCantidadDetalle.Name = "txtCantidadDetalle"
        Me.txtCantidadDetalle.Size = New System.Drawing.Size(78, 25)
        Me.txtCantidadDetalle.SoloLectura = True
        Me.txtCantidadDetalle.TabIndex = 2
        Me.txtCantidadDetalle.TabStop = False
        Me.txtCantidadDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCantidadDetalle.Texto = ""
        '
        'txtImporteTotal
        '
        Me.txtImporteTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImporteTotal.Color = System.Drawing.Color.Empty
        Me.txtImporteTotal.Indicaciones = Nothing
        Me.txtImporteTotal.Location = New System.Drawing.Point(409, 5)
        Me.txtImporteTotal.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtImporteTotal.Multilinea = False
        Me.txtImporteTotal.Name = "txtImporteTotal"
        Me.txtImporteTotal.Size = New System.Drawing.Size(93, 25)
        Me.txtImporteTotal.SoloLectura = True
        Me.txtImporteTotal.TabIndex = 3
        Me.txtImporteTotal.TabStop = False
        Me.txtImporteTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtImporteTotal.Texto = ""
        '
        'txtporcentaje
        '
        Me.txtporcentaje.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtporcentaje.Color = System.Drawing.Color.Empty
        Me.txtporcentaje.Indicaciones = Nothing
        Me.txtporcentaje.Location = New System.Drawing.Point(508, 5)
        Me.txtporcentaje.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtporcentaje.Multilinea = False
        Me.txtporcentaje.Name = "txtporcentaje"
        Me.txtporcentaje.Size = New System.Drawing.Size(42, 25)
        Me.txtporcentaje.SoloLectura = True
        Me.txtporcentaje.TabIndex = 17
        Me.txtporcentaje.TabStop = False
        Me.txtporcentaje.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtporcentaje.Texto = ""
        '
        'txtImporteaasignar
        '
        Me.txtImporteaasignar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImporteaasignar.Color = System.Drawing.Color.Empty
        Me.txtImporteaasignar.Indicaciones = Nothing
        Me.txtImporteaasignar.Location = New System.Drawing.Point(409, 36)
        Me.txtImporteaasignar.Multilinea = False
        Me.txtImporteaasignar.Name = "txtImporteaasignar"
        Me.txtImporteaasignar.Size = New System.Drawing.Size(93, 21)
        Me.txtImporteaasignar.SoloLectura = False
        Me.txtImporteaasignar.TabIndex = 15
        Me.txtImporteaasignar.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtImporteaasignar.Texto = ""
        '
        'txtCantidadComprobante
        '
        Me.txtCantidadComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCantidadComprobante.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobante.Indicaciones = Nothing
        Me.txtCantidadComprobante.Location = New System.Drawing.Point(63, 5)
        Me.txtCantidadComprobante.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtCantidadComprobante.Multilinea = False
        Me.txtCantidadComprobante.Name = "txtCantidadComprobante"
        Me.txtCantidadComprobante.Size = New System.Drawing.Size(78, 25)
        Me.txtCantidadComprobante.SoloLectura = True
        Me.txtCantidadComprobante.TabIndex = 2
        Me.txtCantidadComprobante.TabStop = False
        Me.txtCantidadComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCantidadComprobante.Texto = ""
        '
        'SeleccionarFacturas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(748, 514)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "SeleccionarFacturas"
        Me.Text = "SeleccionarFactura"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        CType(Me.dgvComprobante, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lklQuitarSeleccionados As LinkLabel
    Friend WithEvents FlowLayoutPanel5 As FlowLayoutPanel
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnAceptar As Button
    Friend WithEvents dgvDetalle As DataGridView
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents lblCantidadDetalle As Label
    Friend WithEvents txtCantidadDetalle As ocxTXTString
    Friend WithEvents dgvComprobante As DataGridView
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents lblCantidadComprobante As Label
    Friend WithEvents txtCantidadComprobante As ocxTXTString
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents lblImporteTotal As Label
    Friend WithEvents txtImporteTotal As ocxTXTString
    Friend WithEvents IDTransaccion As DataGridViewTextBoxColumn
    Friend WithEvents Sel As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents ColTipo As DataGridViewTextBoxColumn
    Friend WithEvents ColCondicion As DataGridViewTextBoxColumn
    Friend WithEvents ColVencimiento As DataGridViewTextBoxColumn
    Friend WithEvents ColTotal As DataGridViewTextBoxColumn
    Friend WithEvents ColCobrado As DataGridViewTextBoxColumn
    Friend WithEvents ColDescontado As DataGridViewTextBoxColumn
    Friend WithEvents ColSaldo As DataGridViewTextBoxColumn
    Friend WithEvents ColImporte As DataGridViewTextBoxColumn
    Friend WithEvents ColCancel As DataGridViewCheckBoxColumn
    Friend WithEvents txtImporteaasignar As ocxTXTString
    Friend WithEvents Label1 As Label
    Friend WithEvents txtporcentaje As ocxTXTString
End Class

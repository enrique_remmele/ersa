﻿Public Class frmEstablecerComprobanteDevolucion
    Dim CSistema As New CSistema
    Public Property IDCliente As Integer
    Public Property IDSucursal As Integer
    Public Property IdSucursalCliente As Integer 'SC-04/02/2022 para obtener adicional con filtro IDSucursal
    Public Property IDMoneda As Integer
    Public Property IDTransaccion As Integer
    Public Property Saldo As Decimal
    Public Property IDMotivo As Integer
    Public Property Motivo As String

    Dim Plazo As Integer
    Dim dt As DataTable
    Dim dtMotivo As DataTable

    Sub Inicializar()

        Plazo = CSistema.ExecuteScalar("Select top(1) PlazoDevolucionDesdeEmisionFactura from Configuraciones where idsucursal = " & IDSucursal)

        Listar("")

    End Sub


    Sub Listar(ByVal Comprobante As String)
        'Anterior
        'dt = CSistema.ExecuteToDataTable("Select * from vVenta Where DATEDIFF(DD, FechaEmision, GetDate()) <= " & Plazo & " and Anulado = 'False' And IDCliente =" & IDCliente & " and IDMoneda = " & IDMoneda & " And Comprobante Like '%" & Comprobante.Trim & "%'")

        'Actual: SC-04/02/2022 para obtener adicional con filtro IDSucursal
        'If IdSucursalCliente = 0 Then
        '    dt = CSistema.ExecuteToDataTable("Select * from vVenta Where DATEDIFF(DD, FechaEmision, GetDate()) <= " & Plazo & " and Anulado = 'False' And IDCliente =" & IDCliente & " and IDMoneda = " & IDMoneda & " And Comprobante Like '%" & Comprobante.Trim & "%'")

        'ElseIf IdSucursalCliente <> 0 Then
        '    dt = CSistema.ExecuteToDataTable("Select * from vVenta Where DATEDIFF(DD, FechaEmision, GetDate()) <= " & Plazo & " and Anulado = 'False' And IDCliente =" & IDCliente & " and IDMoneda = " & IDMoneda & " and IDSucursalCliente=" & IdSucursalCliente & " And Comprobante Like '%" & Comprobante.Trim & "%'")
        'End If

        'dt = CSistema.ExecuteToDataTable("Select * from vVenta Where DATEDIFF(DD, FechaEmision, GetDate()) <= " & Plazo & " and Anulado = 'False' And IDCliente =" & IDCliente & " and IDMoneda = " & IDMoneda & " and IDSucursalCliente=" & IdSucursalCliente & " And Comprobante Like '%" & Comprobante.Trim & "%'")
        dt = CSistema.ExecuteToDataTable("Select IDTransaccion, Sucursal, Comprobante, Referencia, Cliente, RUC, FechaEmision, Condicion, [Fec. Venc.], Total, Saldo from vVenta Where DATEDIFF(DD, FechaEmision, GetDate()) <= " & Plazo & " and Anulado = 'False' And IDCliente =" & IDCliente & " and IDMoneda = " & IDMoneda & " and IDSucursalCliente=" & IdSucursalCliente & " And Comprobante Like '%" & Comprobante.Trim & "%'")
        dtMotivo = CSistema.ExecuteToDataTable("select ID, Descripcion from vMotivoDevolucionNC where IDSubmotivoNC = 6")

        CSistema.dtToGrid(dgv, dt)

        Dim comboboxColumn As New DataGridViewComboBoxColumn
        comboboxColumn.Name = "Motivo"
        comboboxColumn.DataSource = dtmotivo
        comboboxColumn.DisplayMember = "Descripcion"
        comboboxColumn.ValueMember = "ID"
        dgv.Columns.Add(comboboxColumn)

        'CSistema.dtToGrid(dgv, dt)
        'CSistema.DataGridColumnasVisibles(dgv, {"IDTransaccion", "Sucursal", "Comprobante", "Referencia", "Cliente", "RUC", "FechaEmision", "Condicion", "[Fec. Venc.]", "Total", "Saldo", "IDUsuario"})
        CSistema.DataGridColumnasVisibles(dgv, {"IDTransaccion", "Sucursal", "Comprobante", "Referencia", "Cliente", "RUC", "FechaEmision", "Condicion", "[Fec. Venc.]", "Total", "Saldo", "Motivo"})
        CSistema.DataGridColumnasNumericas(dgv, {"Total"}, False)
        CSistema.DataGridColumnasNumericas(dgv, {"Saldo"}, False)
        dgv.Columns("Cliente").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("Motivo").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Update()

    End Sub

    Private Sub frmEstablecerComprobanteDevolucion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        If dgv.SelectedRows(0).Cells("Motivo").Value = 0 Then
            MessageBox.Show("Seleccione un motivo", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If dgv.SelectedRows.Count = 0 Then
            MessageBox.Show("Seleccione un comprobante", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        IDTransaccion = dgv.SelectedRows(0).Cells("IDTransaccion").Value
        Saldo = dgv.SelectedRows(0).Cells("Saldo").Value
        IDMotivo = dgv.SelectedRows(0).Cells("Motivo").Value
        Motivo = dtMotivo.Select("ID =" & dgv.SelectedRows(0).Cells("Motivo").Value)(0)(1)

        Me.Close()

    End Sub

    Private Sub txtComprobante_KeyUp(sender As Object, e As KeyEventArgs) Handles txtComprobante.KeyUp
        If e.KeyCode = Keys.Enter Then

            Listar(txtComprobante.Text.Trim)

        End If
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmEstablecerComprobanteDevolucion_Activate()
        Me.Refresh()
    End Sub
End Class
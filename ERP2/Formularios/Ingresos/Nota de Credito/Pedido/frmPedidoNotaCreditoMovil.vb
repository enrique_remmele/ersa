﻿Imports System.Threading
Public Class frmPedidoNotaCreditoMovil
    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim PorcentajeAsignado As Decimal = 0
    Dim ImporteAsignado As Decimal = 0

    'PROPIEDADES

    'VARIABLES
    Dim tProcesar As Thread
    Dim vProcesar As Boolean = False

    Enum ENUMOperacionPedido
        APROBAR = 1
        RECHAZAR = 2
        RECUPERAR = 3
        ELIMINAR = 4
    End Enum


    'FUNCIONES
    Sub Inicializar()

        Me.KeyPreview = True

        'Varios
        CheckForIllegalCrossThreadCalls = False


        cbxOrden.SelectedIndex = 0

        'Funciones

        'Fecha facturacion
        If IsDate(VGFechaFacturacion) = False Then
            VGFechaFacturacion = VGFechaHoraSistema
        End If

        txtFechaDesde.Hoy()
        txtFechaHasta.UnaSemana()



        ListarPedidos()
    End Sub

    Sub CargarInformacion()

    End Sub

    Sub GuardarInformacion()

    End Sub

    Sub SeleccionarGrupo()

        ListarPedidos()

    End Sub

    Sub ListarPedidos()

        Dim SQL As String = ""
        'Dim ID As String = lvGrupo.SelectedItems(0).Text
        Dim OrderBy As String = ""
        Dim Campo As String = ""


        If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        End If


        OrderBy = " Order By Cliente"

        SQL = "Select 'Pedido'=Numero,Sucursal, 'Tipo'=FormaPagoFactura, 'Fec'=Facturar, Cliente, 'Entrega'=Direccion, 'Zona'=ZonaVenta, Vendedor, Total, Observacion, Estado,Aprobado,IDTransaccion From VPedidoMovilDiferenciaPrecio Where Estado = 'PENDIENTE'"

        Listar(SQL & OrderBy)

    End Sub

    Sub Listar(ByVal SQL As String)

        CSistema.SqlToLv(lvPedidos, SQL)

        'lvPedidos.Columns(1).Width = 10
        'lvPedidos.Columns(3).Width = 0
        'lvPedidos.Columns(5).Width = 0
        'lvPedidos.Columns(7).Width = 0
        'lvPedidos.Columns(9).Width = 0

    End Sub

    Sub Detener()
        Try
            If vProcesar = False Then
                Exit Sub
            End If

            tProcesar.Abort()
            vProcesar = False
        Catch ex As Exception

        End Try

        Thread.Sleep(1000)

    End Sub

    Sub AprobarRechazarPedido(ByVal Operacion As ENUMOperacionPedido)

        lvPedidos.Enabled = False

        For Each item As ListViewItem In lvPedidos.Items
            'For Each item As ListViewItem In lvPedidos.CheckedItems
            If item.Checked = False Then
                GoTo siguiente
            End If

            item.ImageIndex = 1

            'Dim ID As Integer = item.Text
            Dim IDTransaccionPedido As Integer = item.SubItems(12).Text

            If CSistema.ExecuteScalar("select Top(1) IDTransaccionPedidoNotaCredito from DetalleNotaCreditoDiferenciaPrecio where IDTransaccionPedidoVenta =" & IDTransaccionPedido) > 0 Then
                ListarPedidos()
                Exit Sub
            End If

            Dim frm As New frmPedidoNotaCredito
            frm.IDTransaccion = 0
            frm.IDTransaccionPedidoVenta = IDTransaccionPedido
            frm.CargarDesdePedidoVenta = True
            frm.DiferenciaPrecioAutomatico = True
            frm.Visualizar = True
            frm.ImporteNC = 0
            FGMostrarFormulario(Me, frm, "Pedidos de Notas de Credito", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True)



siguiente:

        Next

terminar:
        ListarPedidos()
        lvPedidos.Enabled = True
        vProcesar = False

    End Sub

    Sub VerPedido()

        Dim frm As New frmPedido

        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        For Each item As ListViewItem In lvPedidos.SelectedItems
            item.ImageIndex = 1
            frm.ShowDialog()
            frm.EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
            frm.CargarOperacion(item.SubItems(12).Text) ' IDTransaccion
            frm.btnPedidoNotaCredito.Visible = False
        Next

    End Sub

    Sub SeleccionarTodo()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = True
        Next
    End Sub

    Sub QuitarSeleccion()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = False
        Next
    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        Select Case e.KeyCode
            Case Keys.Enter
                CSistema.SelectNextControl(Me, e.KeyCode)
        End Select

    End Sub

    Private Sub frmAprobarPedido_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Detener()
        GuardarInformacion()
    End Sub

    Private Sub frmAprobarPedido_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        ManejarTecla(e)
    End Sub

    Private Sub frmAprobarPedido_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnProcesar_Click(sender As System.Object, e As System.EventArgs) Handles btnAprobar.Click

        AprobarRechazarPedido(ENUMOperacionPedido.APROBAR)

    End Sub

    Private Sub btnSeleccionarTodos_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionarTodos.Click
        SeleccionarTodo()
    End Sub

    Private Sub ToolStripButton1_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton1.Click
        QuitarSeleccion()
    End Sub

    Private Sub btnActualizarPedidos_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarPedidos.Click
        ListarPedidos()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub btnV_Click(sender As System.Object, e As System.EventArgs) Handles btnCargarSolicitud.Click
        VerPedido()
    End Sub

    Private Sub TableLayoutPanel3_Paint(sender As Object, e As PaintEventArgs) Handles TableLayoutPanel3.Paint

    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmPedidoNotaCreditoMovil_Activate()
        Me.Refresh()
    End Sub
End Class
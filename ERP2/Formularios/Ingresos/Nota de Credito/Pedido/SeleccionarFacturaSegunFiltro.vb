﻿Public Class SeleccionarFacturaSegunFiltro

    Dim CSistema As New CSistema
    Dim CData As New CData

    Public Property IDDeposito As Integer
    Public Property IDCliente As Integer
    Public Property IDSucursal As Integer
    Public Property IDProducto As Integer
    Public Property IDFamilia As Integer
    Public Property IDPresentacion As Integer
    Public Property CantidadADevolver As Integer
    Public Property PrecioUnitario As Decimal
    Public Property dtDevolucion As DataTable
    Public Property IdSucursalCliente As Integer
    Public Property TSucursales As Boolean
    Public Property IDSubMotivoNC As Integer

    Dim dtMotivoDevolucion As DataTable
    Dim dt As DataTable
    Dim Plazo As Integer


    Sub Inicializar()

        Plazo = CSistema.ExecuteScalar("Select top(1) Plazo from VencimientodeProductos where IDFamilia = " & IDFamilia & "and IDPresentacion = " & IDPresentacion)
        'Plazo = CSistema.ExecuteScalar("Select top(1) PlazoDevolucionDesdeEmisionFactura from Configuraciones where idsucursal = " & IDSucursal)
        'If Plazo = "" Then
        '    MessageBox.Show("Este Producto no tiene configurado su vencimiento (Familia/Presentacion)", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    Exit Sub
        'End If

        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        dtMotivoDevolucion = CSistema.ExecuteToDataTable("Select ID, Descripcion from MotivoDevolucionNC where Activo = 1 and IDSubmotivoNC =" & IDSubMotivoNC)

        Dim SQL As String = ""
        'dtFactura = CSistema.ExecuteToDataTable("Select * from vVenta where idtransaccion = " & IDTransaccion)
        'dtFactura = CSistema.ExecuteToDataTable("Select * from vVenta where IDTransaccion in (Select Distinct IDTransaccion from DetalleVenta where IDCliente = " & IDCliente & " and IDProducto = " & IDProducto & " and dbo.FPrecioUnitarioNeto(IDProducto, IDTransaccion) = " & PrecioUnitario & ") ")
        'Label1.Text = "Factura Nro: " & dtFactura(0)("Comprobante").ToString & " -  Fecha de Emision: " & CDate(dtFactura(0)("FechaEmision"))
        'dtDevolucion = CSistema.ExecuteToDataTable("Select 'Sel' = Cast(0 as bit), Referencia, Producto, Cantidad, 'Precio'=PrecioUnitario - DescuentoUnitario, 'Total'=0, idtransaccion, idproducto , 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0 From VDetallePedidoNotaCredito where idtransaccion = " & IDTransaccion)
        'Select *, 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0 From VDetallePedidoNotaCredito Where IDTransaccion=" & IDTransaccion & " Order By ID
        If PrecioUnitario > 0 Then
            If TSucursales = True Then
                SQL = "Select 'Sel' = Cast(0 as bit), 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0, *, 'Cantidad a Devolver'=0 From VDetalleVentaParaNotaCredito where Anulado = 0 and DATEDIFF(DD, FechaEmision, GetDate()) <= " & Plazo & " and IDCliente = " & IDCliente & " and IDProducto = " & IDProducto & " and PrecioUnitarioNeto = " & PrecioUnitario & " and SaldoCantidad >= " & CantidadADevolver '& " and IDSucursalcliente = " & IdSucursalCliente
            Else
                SQL = "Select 'Sel' = Cast(0 as bit), 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0, *, 'Cantidad a Devolver'=0 From VDetalleVentaParaNotaCredito where Anulado = 0 and DATEDIFF(DD, FechaEmision, GetDate()) <= " & Plazo & " and IDCliente = " & IDCliente & " and IDProducto = " & IDProducto & " and PrecioUnitarioNeto = " & PrecioUnitario & " and SaldoCantidad >= " & CantidadADevolver & " and IDSucursalcliente = " & IdSucursalCliente

            End If
        End If
        If PrecioUnitario <= 0 Then
            If TSucursales = True Then
                SQL = "Select 'Sel' = Cast(0 as bit), 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0, *, 'Cantidad a Devolver'=0 From VDetalleVentaParaNotaCredito where Anulado = 0 and DATEDIFF(DD, FechaEmision, GetDate()) <= " & Plazo & " and IDCliente = " & IDCliente & " and IDProducto = " & IDProducto & " and SaldoCantidad >= " & CantidadADevolver '& " and IDSucursalcliente = " & IdSucursalCliente
            Else
                SQL = "Select 'Sel' = Cast(0 as bit), 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0, *, 'Cantidad a Devolver'=0 From VDetalleVentaParaNotaCredito where Anulado = 0 and DATEDIFF(DD, FechaEmision, GetDate()) <= " & Plazo & " and IDCliente = " & IDCliente & " and IDProducto = " & IDProducto & " and SaldoCantidad >= " & CantidadADevolver & " and IDSucursalcliente = " & IdSucursalCliente
            End If
        End If

        dt = CSistema.ExecuteToDataTable(SQL)
        CSistema.SqlToDataGrid(dgv, SQL)

        Dim comboboxColumn As New DataGridViewComboBoxColumn
        comboboxColumn.Name = "Motivo Devolucion"
        comboboxColumn.DataSource = dtMotivoDevolucion
        comboboxColumn.DisplayMember = "Descripcion"
        comboboxColumn.ValueMember = "ID"
        dgv.Columns.Add(comboboxColumn)

        CSistema.DataGridColumnasVisibles(dgv, {"Sel", "Comprobante", "FechaEmision", "Motivo Devolucion"})
        'CSistema.DataGridColumnasNumericas(dgv, {"Cantidad", "SaldoCantidad", "Cantidad a Devolver"}, True)
        'CSistema.DataGridColumnasNumericas(dgv, {"PrecioUnitarioNeto"}, True)
        dgv.Columns("Motivo Devolucion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Update()

        If dgv.RowCount > 0 Then
            dgv.CurrentCell = dgv.Rows(0).Cells("Sel")
        End If


    End Sub

    Sub Guardar()
        dtDevolucion.Clear()

        For i = 0 To dgv.Rows.Count - 1
            If CBool(dgv.Rows(i).Cells("Sel").Value) = True Then

                Dim dRow As DataRow = dtDevolucion.NewRow

                dRow("IDProducto") = dgv.Rows(i).Cells("ID").Value
                dRow("ID") = dtDevolucion.Rows.Count
                dRow("IDTransaccionVenta") = dgv.Rows(i).Cells("IDTransaccion").Value
                'Plazo para elegir el plazo mayor en NC
                dRow("Plazo") = dgv.Rows(i).Cells("Plazo").Value
                'If dgv.Rows(i).Cells("Observacion").ToString.Trim.Length = 0 Then
                '    dRow("Descripcion") = dgv.Rows(i).Cells("Descripcion").ToString
                'Else
                '    dRow("Descripcion") = dgv.Rows(i).Cells("Descripcion").ToString & " - " & dgv.Rows(i).Cells("Observacion").ToString
                'End If
                dRow("Descripcion") = dgv.Rows(i).Cells("Descripcion").Value
                dRow("Referencia") = dgv.Rows(i).Cells("Ref").Value
                dRow("CodigoBarra") = dgv.Rows(i).Cells("CodigoBarra").Value
                dRow("Observacion") = ""

                'Deposito
                dRow("IDDeposito") = IDDeposito
                dRow("Deposito") = CData.GetRow("ID=" & IDDeposito, "VDeposito")("Deposito")

                'Cantidad y Precios
                Dim CantidadProducto As Decimal
                dRow("Cantidad") = CantidadADevolver
                CantidadProducto = CantidadADevolver
                Dim PrecioUnitarioNeto As Decimal
                PrecioUnitarioNeto = dgv.Rows(i).Cells("PrecioUnitarioNeto").Value
                ' PrecioUnitario = VentaRow("PrecioUnitario")

                dRow("PrecioUnitario") = PrecioUnitarioNeto
                dRow("Pre. Uni.") = PrecioUnitarioNeto
                dRow("Total") = PrecioUnitarioNeto * CantidadProducto

                'Impuestos
                dRow("IDImpuesto") = dgv.Rows(i).Cells("IDImpuesto").Value
                dRow("Impuesto") = dgv.Rows(i).Cells("Impuesto").Value
                dRow("Ref Imp") = dgv.Rows(i).Cells("Ref Imp").Value
                dRow("Exento") = dgv.Rows(i).Cells("Exento").Value
                dRow("TotalImpuesto") = 0
                dRow("TotalDiscriminado") = 0
                CSistema.CalcularIVA(dRow("IDImpuesto").ToString, CDec(dRow("Total").ToString), True, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))

                'Costo
                'dRow("CostoUnitario") = VentaRow("Costo").ToString
                'dRow("TotalCosto") = CantidadProducto * VentaRow("Costo").ToString
                'dRow("TotalCostoImpuesto") = 0
                'dRow("TotalCostoDiscriminado") = dRow("TotalCosto")
                'CSistema.CalcularIVA(VentaRow("IDImpuesto").ToString, CDec(dRow("TotalCosto").ToString), vDecimalOperacion, True, 0, dRow("TotalCostoImpuesto"))

                dRow("TotalCosto") = 0
                dRow("TotalCostoImpuesto") = 0
                dRow("TotalCostoDiscriminado") = 0
                'CSistema.CalcularIVA(dRow("IDImpuesto").ToString, CDec(dRow("TotalCosto").ToString), True, False, 0, dRow("TotalCostoImpuesto"))

                'Descuentos
                dRow("PorcentajeDescuento") = 0 'dgv.Rows(i).Cells("PorcentajeDescuento").Value
                dRow("DescuentoUnitario") = 0 'dgv.Rows(i).Cells("DescuentoUnitario").Value
                dRow("DescuentoUnitarioDiscriminado") = 0 'dgv.Rows(i).Cells("DescuentoUnitarioDiscriminado").Value
                CSistema.CalcularIVA(dRow("IDImpuesto"), dgv.Rows(i).Cells("DescuentoUnitario").Value, True, True, dRow("DescuentoUnitarioDiscriminado"))

                dRow("TotalDescuento") = 0 'dgv.Rows(i).Cells("DescuentoUnitario").Value * CantidadProducto
                dRow("TotalDescuentoDiscriminado") = 0 'dRow("DescuentoUnitarioDiscriminado") * CantidadProducto

                'Totales
                dRow("TotalBruto") = dRow("Total")
                dRow("TotalSinDescuento") = dRow("Total") '- dRow("TotalDescuento")
                dRow("TotalSinImpuesto") = dRow("Total") - dRow("TotalImpuesto")
                dRow("TotalNeto") = 0
                dRow("TotalNetoConDescuentoNeto") = dRow("TotalDiscriminado") '+ dRow("TotalDescuentoDiscriminado")

                'Venta
                dRow("IDTransaccionVenta") = dgv.Rows(i).Cells("IDTransaccion").Value
                dRow("Comprobante") = dgv.Rows(i).Cells("Comprobante").Value

                Try
                    dRow("Cobrado") = dgv.Rows(i).Cells("Cobrado").Value
                    dRow("Descontado") = dgv.Rows(i).Cells("Descontado").Value
                    dRow("Saldo") = dgv.Rows(i).Cells("Saldo").Value
                Catch ex As Exception

                End Try

                'Cantidad Caja
                dRow("Caja") = 0
                dRow("CantidadCaja") = 0

                dRow("IDMotivoDevolucion") = dgv.Rows(i).Cells("Motivo Devolucion").Value
                'Dim MotivoCombo As New ComboBox
                'Dim dt As New DataTable
                Dim Motivo As String = ""
                'CSistema.SqlToComboBox(MotivoCombo, dtMotivoDevolucion)
                'MotivoCombo.SelectedValue = dgv.Rows(i).Cells("Motivo Devolucion").Value
                Motivo = dtMotivoDevolucion.Select("ID =" & dgv.Rows(i).Cells("Motivo Devolucion").Value)(0)(1)

                dRow("MotivoDevolucion") = Motivo
                dRow("IDSubLinea") = dgv.Rows(i).Cells("IDSubLinea").Value
                dRow("SubLinea") = dgv.Rows(i).Cells("SubLinea").Value
                dRow("IDPresentacion") = dgv.Rows(i).Cells("IDPresentacion").Value
                dRow("Presentacion") = dgv.Rows(i).Cells("Presentacion").Value

                dtDevolucion.Rows.Add(dRow)

            End If
        Next
    End Sub

    Private Sub SeleccionarFacturaSegunFiltro_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        Dim SeleccionadoOK As Boolean = False

        For i = 0 To dgv.Rows.Count - 1

            If CBool(dgv.Rows(i).Cells("Sel").Value) = True Then
                If SeleccionadoOK Then
                    MessageBox.Show("Debe seleccionar solo un comprobante", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
                If dgv.Rows(i).Cells("Motivo Devolucion").Value Is Nothing Then
                    MessageBox.Show("Debe seleccionar un motivo de devolucion!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
                SeleccionadoOK = True
            End If
        Next


        Guardar()

        Me.Close()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub SeleccionarFacturaSegunFiltro_Activate()
        Me.Refresh()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class
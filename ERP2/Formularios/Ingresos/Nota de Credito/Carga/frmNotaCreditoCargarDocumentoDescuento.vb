﻿Public Class frmNotaCreditoCargarDocumentoDescuento

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Public Property IDMoneda As Integer
    Public Property Decimales As Boolean
    Public Property Aplicar As Boolean = False
    Public Property Pedido As Boolean = False
    Public Property UnaSolaFactura As Boolean

    'FUNICONES
    Sub Inicializar()

        'Formato DataGrid
        Dim CantidadDecimales As Integer = 0
        If Decimales Then
            CantidadDecimales = 2
        End If

        dgw.Columns("colTotal").DefaultCellStyle.Format = "N" & CantidadDecimales
        dgw.Columns("colCobrado").DefaultCellStyle.Format = "N" & CantidadDecimales
        dgw.Columns("colDescontado").DefaultCellStyle.Format = "N" & CantidadDecimales
        dgw.Columns("colSaldo").DefaultCellStyle.Format = "N" & CantidadDecimales
        dgw.Columns("colImporte").DefaultCellStyle.Format = "N" & CantidadDecimales

        txtTotal.Decimales = Decimales

        'Funciones
        ListarComprobanteDescuento()

    End Sub

    Sub ListarComprobanteDescuento()

        'Limpiar la grilla
        dgw.Rows.Clear()

        For Each oRow As DataRow In dt.Rows

            Dim Registro(11) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Sel").ToString
            Registro(2) = oRow("Comprobante").ToString
            Registro(3) = oRow("Cod.").ToString
            Registro(4) = oRow("Condicion").ToString
            Registro(5) = oRow("Fec. Venc.").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
            Registro(7) = CSistema.FormatoMoneda(Decimales)
            Registro(8) = CSistema.FormatoMoneda(Decimales)
            Registro(9) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)

            If CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales) > 0 Then
                Registro(10) = CSistema.FormatoMoneda(oRow("Importe").ToString, Decimales)
            Else
                Registro(10) = CSistema.FormatoMoneda(oRow("Saldo").ToString, Decimales)
            End If

            Registro(11) = oRow("Cancelar").ToString

            'Sumar el total de la deuda
            dgw.Rows.Add(Registro)

        Next

        If Pedido Then
            dgw.Columns("colCobrado").Visible = False
            dgw.Columns("colDescontado").Visible = False
        End If

        If Aplicar = False Then
            dgw.Columns(11).ReadOnly = True
        End If

        dgw.Columns("DataGridViewTextBoxColumn1").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub Procesar()

        Procesado = True
        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Cells("colSel").Value = True Then
                Dim DocumentoRow As DataRow = CData.GetRow("IDTransaccion = " & oRow.Cells("colIDTransaccion").Value, dt)
                If Not DocumentoRow Is Nothing Then
                    DocumentoRow("Sel") = oRow.Cells("colSel").Value
                    DocumentoRow("Importe") = oRow.Cells("colImporte").Value.ToString.Replace(".", "")
                    DocumentoRow("Cancelar") = oRow.Cells("colCancelar").Value.ToString.Replace(".", "")
                End If
            End If
        Next

        Me.Close()

    End Sub

    Sub CalcularTotales()

        Dim TotalPagado As Decimal = 0
        Dim CantidadPagado As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Cells("colSel").Value = True Then
                TotalPagado = TotalPagado + oRow.Cells("colImporte").Value
                CantidadPagado = CantidadPagado + 1
            End If
        Next

        txtTotal.SetValue(TotalPagado)
        txtCantidad.SetValue(CantidadPagado)

    End Sub

    Private Sub frmNotaCreditoCargarDocumentoDescuento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Procesar()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    '    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

    '        If dgw.Columns(e.ColumnIndex).Name = "colImporte" Then

    '            If IsNumeric(dgw.CurrentCell.Value) = False Then

    '                Dim mensaje As String = "El importe debe ser valido!"
    '                ctrError.SetError(dgw, mensaje)
    '                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
    '                tsslEstado.Text = mensaje

    '                dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value

    '            Else
    '                'Controlar Saldo
    '                Dim Saldo As Decimal = dgw.CurrentRow.Cells("colSaldo").Value
    '                Dim Importe As Decimal = dgw.CurrentRow.Cells("colImporte").Value

    '                If Importe > Saldo Then
    '                    Dim mensaje As String = "El importe no puede ser mayor al saldo!"
    '                    CSistema.MostrarError(mensaje, ctrError, dgw, tsslEstado)
    '                    dgw.CurrentCell.Value = dgw.CurrentRow.Cells("colSaldo").Value
    '                    GoTo siguiente
    '                End If

    '                dgw.CurrentCell.Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells("colImporte").Value, Decimales)

    '            End If

    'siguiente:

    '            CalcularTotales()

    '        End If

    '    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown
        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSel").Value = False Then
                        oRow.Cells("colSel").Value = True
                        oRow.Cells("colSel").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = False
                        oRow.Cells("colCancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells("colImporte")
                    Else
                        oRow.Cells("colSel").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colCancelar").ReadOnly = True
                        oRow.Cells("colImporte").Value = oRow.Cells("colSaldo").Value
                        oRow.Cells("colSel").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

            Exit Sub

        End If

        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSel").Value = False Then
                        oRow.Cells("colSel").Value = True
                        oRow.Cells("colSel").ReadOnly = False
                        oRow.Cells("colImporte").ReadOnly = False
                        oRow.Cells("colCancelar").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                    Else
                        oRow.Cells("colSel").ReadOnly = True
                        oRow.Cells("colImporte").ReadOnly = True
                        oRow.Cells("colCancelar").ReadOnly = True
                        oRow.Cells("colImporte").Value = oRow.Cells("colSaldo").Value
                        oRow.Cells("colSel").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For


                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colImporte")
            End If

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

            Exit Sub

        End If


    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Index = e.RowIndex Then
                If oRow.Cells("colSel").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                End If

                oRow.DefaultCellStyle.Font = f1

                oRow.Cells("colImporte").Selected = True

            Else

                oRow.DefaultCellStyle.Font = f2

                If oRow.Cells("colSel").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.White
                Else
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                End If
            End If
        Next
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp

        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            ' dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells("colImporte")
        End If

    End Sub

    Private Sub dgw_SelectionChanged(sender As Object, e As EventArgs) Handles dgw.SelectionChanged

        If dgw.CurrentRow Is Nothing Then
            Exit Sub
        End If

        'If UnaSolaFactura Then
        'Dim indice As Integer = dgw.CurrentRow.Index
        '    dgw.Rows(indice).Cells("colSel").Value = True

        'For Each oRow As DataGridViewRow In dgw.Rows
        '        If oRow.Index <> indice Then
        '            oRow.Cells("colSel").Value = False
        '        End If
        '    Next
        'End If

    End Sub

    '10-06-2021 - SC - Actualiza datos
    Sub frmNotaCreditoCargarDocumentoDescuento_Activate()
        Me.Refresh()
    End Sub
End Class
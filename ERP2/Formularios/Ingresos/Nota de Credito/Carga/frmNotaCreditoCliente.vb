﻿Imports ERP.Reporte
Public Class frmNotaCreditoCliente

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Dim CAsiento As New CAsientoNotaCredito
    Dim CData As New CData
    Dim CReporte As New CReporteNotaCredito
    Dim vDecimalOperacion As Boolean
    Dim IDMotivoAnulacion As Integer = 0
    Dim Comprobante As String

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    'JGR 20140816
    Public Property tipoComprobante() As String


    'EVENTOS

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dtVentas As New DataTable
    Dim dtDetalleVenta As New DataTable
    Dim dtNotaCreditoVenta As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim dtTerminalPuntoExpedicion As DataTable
    Dim vHabilitar As Boolean
    Dim vConComprobantes As Boolean
    Dim vObservacionDevolucion As String
    Dim vObservacionDescuento As String
    Public dtPuntoExpedicion As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Controles
        txtProducto.Conectar()
        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito  From VCliente Where Estado='ACTIVO' "
        txtCliente.frm = Me
        txtCliente.MostrarSucursal = True

        OcxImpuesto1.Inicializar()
        OcxImpuesto1.dg.ReadOnly = True
        OcxCotizacion1.Inicializar()
        'Propiedades

        'IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "NOTA CREDITO", "NCR")
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "NOTA CREDITO", tipoComprobante)
        vNuevo = False
        vHabilitar = True
        vConComprobantes = True

        'SC: 03/06/2022 - 
        cbxComprobanteImpresion.Visible = False
        cbxComprobanteImpresion.Enabled = False
        TxtComprobanteImpresion.Visible = True
        TxtComprobanteImpresion.Enabled = False

        'Otros
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False
        gbxCabecera.BringToFront()
        pnlDevolucion.Dock = DockStyle.Fill
        pnlDescuento.Dock = DockStyle.Fill

        'Variables
        vObservacionDescuento = "DESCUENTO CONCEDIDO"
        vObservacionDevolucion = "DEVOLUCION DE MERCADERIAS"
        'Cotizacion
        OcxCotizacion1.Inicializar()

        'Funciones
        CargarInformacion()
        ObtenerInformacionPuntoVenta()

        'Clases
        CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        If IDTransaccion = 0 Then
            MoverRegistro(New KeyEventArgs(Keys.End))
        Else
            CargarOperacion(IDTransaccion)
        End If


        'ObtenerDeposito()

        'Foco
        txtNroComprobante.Focus()
        txtNroComprobante.txt.Focus()
        txtNroComprobante.txt.SelectAll()
        OcxCotizacion1.txtCotizacion.Enabled = False
        txtNroComprobante.Enabled = "True"
        'Esta opcion es configurable en el menu de configuracion de pedido
        chkImprimir2.Valor = CBool(vgConfiguraciones("NCImprimirSinRpt").ToString)
        btnNuevo.Enabled = False

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Combrobante
        CSistema.CargaControl(vControles, txtTimbrado)

        'Cabecera
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, cbxDeposito)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, rdbDescuentos)
        CSistema.CargaControl(vControles, rdbDevolucion)
        CSistema.CargaControl(vControles, chkAplicar)

        'Detalle
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, txtObservacionProducto)
        CSistema.CargaControl(vControles, txtCantidad)
        CSistema.CargaControl(vControles, txtCostoUnitario)
        CSistema.CargaControl(vControles, txtImporte)
        CSistema.CargaControl(vControles, lvLista)

        'Decuento
        CSistema.CargaControl(vControles, lklAgregarDetalleDescuento)
        CSistema.CargaControl(vControles, lklEliminarDetalleDescuento)
        CSistema.CargaControl(vControles, lklAgregarComprobanteDescuento)
        CSistema.CargaControl(vControles, lklEliminarComprobanteDescuento)
        CSistema.CargaControl(vControles, lvListaDescuento)
        CSistema.CargaControl(vControles, dgw)

        CSistema.CargaControl(vControles, OcxCotizacion1)

        'Detalles
        dtDetalle = CData.GetStructure("VDetalleNotaCredito", "Select Top(0) *, 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0  From VDetalleNotaCredito")
        dtDetalleVenta = CData.GetStructure("VDetalleVenta", "Select Top(0) *, 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0 From VDetalleVenta")
        dtDescuento = CData.GetStructure("VDescuento", "Select Top(0) * From VDescuento")

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CARGAR CONTROLES
        ' ''Ciudad
        ''CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Deposito
        CSistema.SqlToComboBox(cbxDeposito.cbx, "Select ID, Deposito  From VDeposito Order By IDSucursal,IDTipoDeposito")

        'JGr 20140816
        'Tipo de Comprobante
        Try
            CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select TOP 1 ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion & " And Codigo = 'NCR'")
            cbxTipoComprobante.cbx.SelectedIndex = 0

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error al cargar tipo de comprobante", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        'Puntos de Expediciones
        dtPuntoExpedicion = CData.GetTable("VPuntoExpedicion").Copy
        dtPuntoExpedicion = CData.FiltrarDataTable(dtPuntoExpedicion, " IDOperacion = " & IDOperacion & " And IDTipoComprobante = " & cbxTipoComprobante.GetValue & "  ")
        'JGR 20140910 Documento interno
        'If cbxTipoComprobante.txt.Text = "NCIC" Then
        '    txtIDTimbrado.SetValue(dtPuntoExpedicion(0)("ID"))
        '    Label7.Visible = False
        '    chkAplicar.Valor = False
        '    chkAplicar.Visible = False
        '    btnAsiento.Visible = False
        'Else
        txtIDTimbrado.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIMBRADO", 0))
        'End If

        ObtenerInformacionPuntoVenta()


        'Ciudades
        cbxCiudad.Conectar("", "Descripcion")

        'Sucursal
        cbxSucursal.Conectar()

        'Depositos
        'cbxDeposito.Conectar()
        'cbxDeposito.cbx.Text = vgDeposito


        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Tipo de Comprobante
        'cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Impuestos
        OcxImpuesto1.EstablecerSoloLectura()

        'Radio Button
        rdbDevolucion.Checked = True


        'checK


    End Sub

    'Sub GuardarInformacion()

    '    'Ciudad
    '    CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

    '    'Tipo de Comprobante
    '    CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

    '    'Sucursal
    '    CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

    '    'Deposito
    '    CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DEPOSITO", cbxDeposito.cbx.Text)

    'End Sub

    Sub EstablecerBotones(ByVal Operacion As CSistema.NUMHabilitacionBotonesRegistros)

        'Configurar botones
        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnula, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles, btnEliminar)

    End Sub

    Sub GuardarInformacion()
        'JGR 20140816
        'TIMBRADO
        If cbxTipoComprobante.txt.Text <> "NCIC" Then
            CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIMBRADO", txtIDTimbrado.GetValue)
        End If


    End Sub

    Sub Nuevo()
        IDTransaccion = 0
        txtCliente.Conectar()
        If CBool(vgConfiguraciones("BloquearNroComprobante").ToString) = True Then
            txtNroComprobante.Enabled = False
        Else
            txtNroComprobante.Enabled = True
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)
        OcxCotizacion1.Inicializar()
        ''Limpiar detalle
        dtDetalle.Rows.Clear()
        dtDescuento.Rows.Clear()
        dtVentas.Rows.Clear()
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, True, vDecimalOperacion)
        OcxImpuesto1.CargarValores(dtDetalle)

        dtNotaCreditoVenta.Rows.Clear()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True
        CAsiento.Inicializar()

        'CheckBox
        chkAplicar.Valor = False
        rdbDevolucion.Checked = True
        txtObservacion.SetValue(vObservacionDevolucion)

        'Cabecera
        'CONFIGURACIONES
        If IsDate(VGFechaFacturacion) = False Then
            VGFechaFacturacion = VGFechaHoraSistema
        End If

        If CBool(vgConfiguraciones("VentaBloquearFecha").ToString) = True Then
            txtFecha.Enabled = False
        Else
            txtFecha.Enabled = True
        End If

        txtFecha.SetValue(VGFechaFacturacion)

        cbxComprobante.SeleccionObligatoria = False
        cbxComprobante.cbx.Text = ""
        cbxTipoComprobante.cbx.Text = ""
        cbxComprobanteImpresion.cbx.Text = ""
        TxtComprobanteImpresion.Clear()
        txtCliente.Clear()
        txtDireccion.Clear()
        txtTelefono.Clear()

        txtCostoUnitario.SoloLectura = True
        txtImporte.SoloLectura = True
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False
        chkAplicar.Valor = False
        rdbDescuentos.Enabled = True
        rdbDevolucion.Enabled = True
        chkAplicar.Enabled = True

        OcxImpuesto1.IDMoneda = OcxCotizacion1.Registro("ID")

        'Limpiar Detalle
        lvLista.Items.Clear()
        cbxComprobante.cbx.Text = " "
        Dim s As String = txtNroComprobante.txt.Text

        'Obtener registro nuevo

        'Puntos de Expediciones
        CData.ResetTable("VPuntoExpedicion")
        dtPuntoExpedicion = CData.GetTable("VPuntoExpedicion", " IDOperacion = " & IDOperacion & " And Estado='True' ")
        ObtenerInformacionPuntoVenta()
        'Puntos de Expediciones
        'JGR 20140910
        'Try
        '    If cbxTipoComprobante.txt.Text = "NCIC" Then 'Documento interno suma un nro autonumerico
        '        If s = txtNroComprobante.txt.Text Then
        '            txtNroComprobante.txt.Text = (CInt(s) + 1).ToString
        '        End If
        '    End If
        'Catch
        'End Try


        'Poner el foco en el proveedor
        txtNroComprobante.txt.SelectAll()
        txtNroComprobante.txt.Focus()

        ObtenerDeposito()

        'limpiar Detalle Descuento
        lvListaDescuento.Items.Clear()
        dgw.Rows.Clear()
        txtTotalDescuento.txt.Text = ""
        txtSaldo.txt.Text = ""
        txtDescontados.txt.Text = ""
        txtCantidadCobrado.txt.Text = ""
        cbxComprobante.SoloLectura = False
        txtObservacion.SoloLectura = False
        dgw.ReadOnly = False
        txtObservacionProducto.SoloLectura = False
        txtProducto.txt.Text = ""
        btnVerPedidoNC.Visible = False
        btnRecalcularSaldoNC.Visible = False
        If CSistema.ExecuteScalar("select count(*) from Cotizacion where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False) & "' and IDMoneda = " & OcxCotizacion1.Registro("ID")) = 0 And OcxCotizacion1.Registro("ID") > 1 Then
            MessageBox.Show("No se ha fijado cotizacion para la moneda seleccionada.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
        End If
        OcxCotizacion1.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
        OcxCotizacion1.Recargar()
        OcxImpuesto1.SetIDMoneda(OcxCotizacion1.Registro("ID"))
        MostrarDecimales(OcxCotizacion1.Registro("ID"))
        vDecimalOperacion = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & OcxCotizacion1.Registro("ID"), "vMoneda")("Decimales").ToString)
        txtProducto.IDMoneda = OcxCotizacion1.Registro("ID")
        txtProducto.Cotizacion = OcxCotizacion1.Registro("Cotizacion")
        txtImporte.Decimales = vDecimalOperacion
        txtCostoUnitario.Decimales = vDecimalOperacion

        'SC: 31/05/2022 - Nuevo para Impresion
        'TxtComprobanteImpresion.txt.Text = CSistema.ExecuteScalar("Select IsNull((Select top(1) v.Comprobante From VVenta v Join VDetallePedidoNotaCredito dp On v.IDTransaccion = dp.IDTransaccionVenta Join VPedidoNotaCredito nt On dp.IDTransaccion = nt.IDTransaccion Where v.IDTransaccion = dp.IDTransaccionVenta And nt.TotalDiscriminado <= v.TotalDiscriminado And dp.IDTransaccion = " & IDTransaccion & "), '0' )")
        'If TxtComprobanteImpresion.Text = "0" Then
        '    TxtComprobanteImpresion = CSistema.ExecuteScalar("Select IsNUll((Select top(1) v.Comprobante From VVenta v Join VPedidoNotaCredito nt On v.idcliente = nt.IDCliente Where V.Anulado = 0  And V.Procesado = 1 And nt.TotalDiscriminado <= v.TotalDiscriminado And (datediff(day, v.FechaEmision, getdate())) <= 180 and nt.idtransaccion =" & IDTransaccion & "Order By v.fechaemision asc ), '0' )")
        'End If

        CargarComprobante()
    End Sub

    Sub ObtenerDeposito()

        If cbxSucursal.cbx.SelectedValue Is Nothing Then
            Exit Sub
        End If

        Dim dttemp As DataTable = CData.GetTable("VDeposito", "IDSucursal=" & cbxSucursal.cbx.SelectedValue).Copy

        CSistema.SqlToComboBox(cbxDeposito.cbx, dttemp)

        Dim DepositoRow As DataRow = CData.GetRow("IDSucursal=" & cbxSucursal.cbx.SelectedValue, "VDeposito")

        cbxDeposito.cbx.Text = DepositoRow("Deposito").ToString
        cbxDeposito.cbx.Text = vgDeposito


    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Seleccion de Punto de Expedicion
        If IsNumeric(txtIDTimbrado.GetValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el punto de venta!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Nro de Comprobante, mayor a 0
        If IsNumeric(txtNroComprobante.txt.Text) = False Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Asignamos entero al nro de comprobante
        txtNroComprobante.txt.Text = CInt(txtNroComprobante.txt.Text)

        If CInt(txtNroComprobante.txt.Text) = 0 Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Cliente
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If txtCliente.Seleccionado = False Then
                Dim mensaje As String = "Seleccione correctamente el cliente!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If txtCliente.Registro Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente el cliente!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If IsNumeric(txtCliente.Registro("ID").ToString) = False Then
                Dim mensaje As String = "Seleccione correctamente el cliente!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If
        End If

        'Seleccion de Deposito
        If IsNumeric(cbxDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Existencia en Detalle
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If dtDetalle.Rows.Count = 0 Then
                Dim mensaje As String = "El documento debe tener por lo menos 1 detalle!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If chkAplicar.Valor = True And vConComprobantes = True Then
                If rdbDescuentos.Checked Then
                    If dgw.Rows.Count = 0 Then
                        Dim mensaje As String = "El documento debe tener por lo menos 1 detalle!"
                        ctrError.SetError(btnGuardar, mensaje)
                        ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                        tsslEstado.Text = mensaje
                        Exit Function
                    End If
                End If
            End If

            If chkAplicar.Valor = False Then
                If rdbDescuentos.Checked Then
                    If txtSaldo.ObtenerValor <> 0 Then
                        Dim mensaje As String = "El saldo debe ser 0!"
                        ctrError.SetError(btnGuardar, mensaje)
                        ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                        tsslEstado.Text = mensaje
                        Exit Function
                    End If
                End If
            End If

            'Total
            If CDec(OcxImpuesto1.txtTotal.ObtenerValor) <= 0 Then
                Dim mensaje As String = "El importe del documento no es valido!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If (CDec(txtSaldo.ObtenerValor) <> 0) And (CDec(txtSaldo.ObtenerValor) <> CDec(txtTotalDescuento.ObtenerValor)) Then
                Dim mensaje As String = "La Nota de Credito no puede quedar con saldo."
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            'Exigir asiento solo si es por descuento
            If rdbDescuentos.Checked = True Then
                'JGR 20140910 Si no es documento interno
                If cbxTipoComprobante.txt.Text <> "NCIC" Then
                    'Validar el Asiento
                    If CAsiento.ObtenerSaldo <> 0 Then
                        CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                        Exit Function
                    End If

                    'Validar el Asiento
                    If CAsiento.ObtenerTotal = 0 Then
                        CSistema.MostrarError("El asiento no puede ser 0!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                        Exit Function
                    End If
                End If


            End If

            If cbxSucursal.cbx.SelectedValue <> 5 Then

                If cbxComprobanteImpresion.cbx.Text = "" Or cbxComprobanteImpresion.cbx.Text = "" Then
                    Dim mensaje As String = "Seleccionar factura para impresión"
                    ctrError.SetError(btnGuardar, mensaje)
                    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Function
                End If
            End If
        End If


            'Si va a anular
            If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            Dim frm As New frmAgregarMotivoAnulacionNotaCredito
            frm.Text = " Motivo Anulacion NC"
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.ShowDialog(Me)
            IDMotivoAnulacion = frm.IDMotivo

            If Not frm.Mantener Then
                If MessageBox.Show("Atencion! Esto anulara tambien el PEDIDO DE NOTA DE CREDITO. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.No Then
                    Exit Function
                End If
            End If
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        ValidarDocumento = True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        'Punto de Expedicion
        Dim PuntoExpedicionRow As DataRow = CData.GetTable("VPuntoExpedicion", " IDOperacion = " & IDOperacion).Select("ID=" & txtIDTimbrado.GetValue)(0)

        'Si fue seleccionado una sucursal del cliente
        If txtCliente.SucursalSeleccionada = True Then
            CSistema.SetSQLParameter(param, "@IDSucursalCliente", txtCliente.Sucursal("ID").ToString, ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@IDSucursalCliente", "0", ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDPuntoExpedicion", txtIDTimbrado.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", PuntoExpedicionRow("IDTipoComprobante").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtNroComprobante.GetValue, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@Comprobante", PuntoExpedicionRow("ReferenciaSucursal").ToString & "-" & PuntoExpedicionRow("ReferenciaPunto").ToString & "-" & txtNroComprobante.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", PuntoExpedicionRow("IDSucursal").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoOperacion", cbxDeposito.GetValue, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Direccion", txtDireccion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Telefono", txtTelefono.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Aplicar", chkAplicar.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descuento", rdbDescuentos.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Devolucion", rdbDevolucion.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ConComprobantes", vConComprobantes, ParameterDirection.Input)

        'Saldo
        If rdbDevolucion.Checked = True Then
            CSistema.SetSQLParameter(param, "@Saldo", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "Total"), vDecimalOperacion), ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Saldo", CSistema.FormatoMonedaBaseDatos(txtSaldo.ObtenerValor, vDecimalOperacion), ParameterDirection.Input)
        End If

        'Otros

        'Moneda
        CSistema.SetSQLParameter(param, "@IDMoneda", OcxCotizacion1.Registro("ID"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoNumeroBaseDatos(OcxCotizacion1.Registro("Cotizacion"), True), ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text.Trim, ParameterDirection.Input)

        'SC:31/05/2022 Nuevo
        'CSistema.SetSQLParameter(param, "@ComprobanteImpresion", TxtComprobanteImpresion.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ComprobanteImpresion", cbxComprobanteImpresion.Text.Trim, ParameterDirection.Input)

        'Totales
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "Total"), vDecimalOperacion), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalImpuesto"), vDecimalOperacion), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalDiscriminado"), vDecimalOperacion), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDescuento", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalDescuento"), vDecimalOperacion), ParameterDirection.Input)


        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        'Capturamos el index de la Operacion para un posible proceso posterior
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpNotaCredito", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From Venta Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpNotaCredito", False, False, MensajeRetorno, IDTransaccion)
            End If

            If MensajeRetorno = "No se puede anular con fecha distinta a hoy" Then

                If MessageBox.Show("Desea solicitar anulacion por autorizacion?", "Cargar solicitud de anulacion", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.Yes Then
                    Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionAnulacion @IDTransaccion = " & IDTransaccion & ",@Operacion='SOLICITAR',@IDUsuario = " & vgIDUsuario & ",@IDMotivo = " & IDMotivoAnulacion & ",@IDSucursal =" & vgIDSucursal & ",@IDDeposito =" & vgIDDeposito & ",@IDTerminal =" & vgIDTerminal).Copy
                    If Not dttemp Is Nothing Then
                        If dttemp.Rows.Count > 0 Then
                            MessageBox.Show(dttemp.Rows(0)("Mensaje").ToString, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    End If
                End If
            End If
            Exit Sub

        End If

        Dim Procesar As Boolean = True

        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
            CargarOperacion(IDTransaccion)
        End If

        'Guardar detalle
        If IDTransaccion > 0 Then

            'Guardar 
            Procesar = InsertarDetalle(IDTransaccion)

            'Cargamos el DetalleImpuesto
            CDetalleImpuesto.Decimales = vDecimalOperacion
            CDetalleImpuesto.Guardar(IDTransaccion)

            'Cargar los descuentos
            InsertDescuento(IDTransaccion)

        End If

        If Procesar = True Then

            Try

                ReDim param(-1)

                CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

                'Informacion de Salida
                CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
                CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

                'Aplicar
                If CSistema.ExecuteStoreProcedure(param, "SpNotaCreditoProcesar", False, False, MensajeRetorno) = False Then

                End If


            Catch ex As Exception

            End Try

        End If


        'Si es nuevo
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then
            'JGR 20140816
            If cbxTipoComprobante.txt.Text <> "NCIC" Then 'Si no es Documento interno
                'Cargamos el asiento
                'Guardar el Asiento solo si es por descuento
                If rdbDescuentos.Checked = True Then
                    If CAsiento.dtDetalleAsiento.Rows.Count > 0 Then
                        CAsiento.IDTransaccion = IDTransaccion
                        CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
                    End If
                End If
            End If


            'Imprimimos
            'Imprimir()

        End If


        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtNroComprobante.SoloLectura = False

    End Sub

    Function InsertDescuento(ByVal IDTransaccion As Integer) As String

        InsertDescuento = ""
        Dim i As Integer
        For Each oRow As DataRow In dtDescuento.Rows
            Dim param As New DataTable
            CSistema.SetSQLParameter(param, "IDTransaccion", IDTransaccion)
            CSistema.SetSQLParameter(param, "IDProducto", oRow("IDProducto"))
            CSistema.SetSQLParameter(param, "ID", oRow("ID"))
            CSistema.SetSQLParameter(param, "IDDescuento", i)
            CSistema.SetSQLParameter(param, "IDDeposito", oRow("IDDeposito"))
            CSistema.SetSQLParameter(param, "IDTipoDescuento", oRow("IDTipoDescuento"))
            CSistema.SetSQLParameter(param, "IDActividad", oRow("IDActividad").ToString)
            CSistema.SetSQLParameter(param, "Descuento", CSistema.FormatoMonedaBaseDatos(oRow("Descuento"), vDecimalOperacion))
            CSistema.SetSQLParameter(param, "Porcentaje", CSistema.FormatoNumeroBaseDatos(oRow("Porcentaje"), vDecimalOperacion))
            CSistema.SetSQLParameter(param, "DescuentoDiscriminado", CSistema.FormatoNumeroBaseDatos(oRow("DescuentoDiscriminado"), vDecimalOperacion))

            InsertDescuento = InsertDescuento & CSistema.InsertSQL(param, "Descuento") & vbCrLf

            i = i + 1

        Next

    End Function

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = "Nota de Credito: " & cbxTipoComprobante.cbx.Text & " " & ObtenerNroComprobante() & "  -  " & txtCliente.txtRazonSocial.Text

            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From vNotaCredito Where Comprobante='" & ObtenerNroComprobante() & "' And IDPuntoExpedicion=" & txtIDTimbrado.GetValue & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else

            'Validar
            If cbxCiudad.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la ciudad de operacion!"
                ctrError.SetError(cbxCiudad, mensaje)
                ctrError.SetIconAlignment(cbxCiudad, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(cbxTipoComprobante, mensaje)
                ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If txtCliente.Seleccionado = False Then
                Dim mensaje As String = "Seleccione correctamente el cliente!"
                ctrError.SetError(txtCliente, mensaje)
                ctrError.SetIconAlignment(txtCliente, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = "Venta: " & cbxTipoComprobante.cbx.Text & " " & ObtenerNroComprobante() & "  -  " & txtCliente.txtRazonSocial.Text

            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento
            frm.CAsiento.Bloquear = CAsiento.Bloquear

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
            CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            CAsiento.NroCaja = frm.CAsiento.NroCaja
            CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                VisualizarAsiento()
            End If

        End If

    End Sub

    Sub GenerarAsiento()


        'EstablecerCabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = cbxCiudad.cbx.SelectedValue
        oRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
        oRow("Fecha") = txtFecha.GetValue
        oRow("IDMoneda") = OcxCotizacion1.Registro("ID")
        oRow("Cotizacion") = OcxCotizacion1.Registro("Cotizacion")
        oRow("IDTipoComprobante") = cbxTipoComprobante.cbx.SelectedValue
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = ObtenerNroComprobante()
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & ObtenerNroComprobante()
        oRow("Detalle") = txtObservacion.txt.Text
        oRow("Bloquear") = CAsiento.Bloquear
        oRow("Total") = OcxImpuesto1.txtTotal.ObtenerValor
        oRow("GastosMultiples") = False

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)

        CAsiento.IDCliente = txtCliente.Registro("ID").ToString

        OcxImpuesto1.Generar()
        CAsiento.dtImpuesto = OcxImpuesto1.dtImpuesto.Copy
        CAsiento.dtDetalleProductos = dtDetalle
        CAsiento.dtDescuento = dtDescuento
        CAsiento.IDCliente = txtCliente.Registro("ID").ToString
        CAsiento.Generar()

    End Sub

    Sub CargarDescuento(ByVal Venta As DataRow)

        'Variables
        Dim vIDTransaccionVenta As Integer = Venta("IDTransaccion")
        Dim vIDProducto As Integer = Venta("ID")
        Dim vID As Integer = 0

        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Select * From VDescuento Where IDTransaccion=" & vIDTransaccionVenta & " And IDProducto=" & vIDProducto & " And ID=" & vID & " ").Copy

        If dttemp Is Nothing Then
            Exit Sub
        End If

        'Agregar
        For Each oRow As DataRow In dttemp.Rows
            Dim NewRow As DataRow = dtDescuento.NewRow
            NewRow.ItemArray = oRow.ItemArray

            If NewRow("Descuento") = 0 Then
                GoTo siguiente
            End If

            'Descuento 
            Dim Descuento As Decimal = NewRow("Descuento")
            Dim DescuentoDiscriminado As Decimal = 0
            Dim TotalDescuentoDiscriminado As Decimal = 0

            NewRow("Cantidad") = txtCantidad.ObtenerValor
            NewRow("Total") = txtCantidad.ObtenerValor * Descuento

            CSistema.CalcularIVA(Venta("IDImpuesto"), Descuento, False, True, DescuentoDiscriminado)
            NewRow("DescuentoDiscriminado") = DescuentoDiscriminado

            TotalDescuentoDiscriminado = NewRow("Cantidad") * DescuentoDiscriminado
            NewRow("TotalDescuentoDiscriminado") = TotalDescuentoDiscriminado

            dtDescuento.Rows.Add(NewRow.ItemArray)

siguiente:

        Next

    End Sub

    Sub EliminarDescuentos()

        For Each item As ListViewItem In lvLista.SelectedItems
            Dim IDProducto As Integer = item.Text
            Dim ID As Integer = item.Index

            For i As Integer = dtDescuento.Rows.Count - 1 To 0 Step -1
                Dim oRow As DataRow = dtDescuento.Rows(i)
                If oRow("IDProducto") = IDProducto And oRow("ID") = ID Then
                    dtDescuento.Rows.RemoveAt(i)
                End If
            Next

        Next

    End Sub

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMotivo", IDMotivoAnulacion, ParameterDirection.Input)


        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""
        Dim MensajeOriginal As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpNotaCredito", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            MensajeOriginal = MensajeRetorno
            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From NotaCredito Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpNotaCredito", False, False, MensajeRetorno, IDTransaccion)
            End If

            If MensajeOriginal = "No se puede anular con fecha distinta a hoy" Then

                If MessageBox.Show("Desea solicitar anulacion por autorizacion?", "Cargar solicitud de anulacion", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.Yes Then
                    Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionAnulacion  @IDTransaccion = " & IDTransaccion & ",@Operacion='SOLICITAR',@IDUsuario = " & vgIDUsuario & ",@IDMotivo = " & IDMotivoAnulacion & ",@IDSucursal =" & vgIDSucursal & ",@IDDeposito =" & vgIDDeposito & ",@IDTerminal =" & vgIDTerminal).Copy
                    If Not dttemp Is Nothing Then
                        If dttemp.Rows.Count > 0 Then
                            MessageBox.Show(dttemp.Rows(0)("Mensaje").ToString, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    End If
                End If
            End If

            Exit Sub

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

    End Sub

    Sub Eliminar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        'Consultar
        If MessageBox.Show("Desea eliminar el comprobante?", "Elininar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> DialogResult.Yes Then
            Exit Sub
        End If

        tsslEstado.Text = ""
        ctrError.Clear()

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpNotaCredito", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From NotaCredito Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpNotaCredito", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

        txtNroComprobante.Focus()
        MoverRegistro(New KeyEventArgs(Keys.End))

    End Sub

    Sub Buscar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

        'Otros
        Dim frm As New frmConsultaNotaCredito
        frm.IDOperacion = txtIDTimbrado.txt.Text
        FGMostrarFormulario(Me, frm, "Notas de Credito - Busqueda", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False, True)

        If frm.IDTransaccion > 0 Then
            txtIDTimbrado.SetValue(frm.IDTimbrado)
            ObtenerInformacionPuntoVenta()
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalle = True

        Seleccionardescuento()
        Dim i As Integer = 0

        'Nota de Credito por Devolucion
        If rdbDevolucion.Checked = True Then

            'Recorrer el detalle
            For Each oRow As DataRow In dtDetalle.Rows

                Dim sql As String = ""
                Dim sql2 As String = ""

                'Si es la aplicacion se queda pendiente
                If chkAplicar.Valor = True Then

                    'Si es pendiente, pero se guarda con comprobante
                    If vConComprobantes = True Then
                        sql = "Insert Into DetalleNotaCredito (IDTransaccion, IDProducto, ID, IDTransaccionVenta, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, CostoUnitario, DescuentoUnitario, PorcentajeDescuento, Total, TotalImpuesto, TotalCostoImpuesto, TotalCosto, TotalDescuento, TotalDiscriminado, TotalCostoDiscriminado, Caja, CantidadCaja, DescuentoUnitarioDiscriminado, TotalDescuentoDiscriminado, IDMotivoDevolucion) Values(" & IDTransaccion & "," & oRow("IDProducto").ToString & ", " & oRow("ID").ToString & "," & oRow("IDTransaccionVenta").ToString & "," & oRow("IDDeposito").ToString & ",'" & oRow("Observacion").ToString & "'," & oRow("IDImpuesto").ToString & "," & CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString, True) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PrecioUnitario").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("CostoUnitario").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("DescuentoUnitario").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PorcentajeDescuento").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCostoImpuesto").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCosto").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalDescuento").ToString, vDecimalOperacion) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString, vDecimalOperacion) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString, vDecimalOperacion) & " ,'" & oRow("Caja").ToString & "'," & CSistema.FormatoNumeroBaseDatos(oRow("CantidadCaja").ToString) & "," & CSistema.FormatoNumeroBaseDatos(oRow("DescuentoUnitarioDiscriminado").ToString, vDecimalOperacion) & "," & CSistema.FormatoNumeroBaseDatos(oRow("TotalDescuentoDiscriminado").ToString, vDecimalOperacion) & "," & oRow("IDMotivoDevolucion").ToString & " )"
                    Else
                        sql = "Insert Into DetalleNotaCredito (IDTransaccion, IDProducto, ID, IDTransaccionVenta, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, CostoUnitario, DescuentoUnitario, PorcentajeDescuento, Total, TotalImpuesto, TotalCostoImpuesto, TotalCosto, TotalDescuento, TotalDiscriminado, TotalCostoDiscriminado, Caja, CantidadCaja, DescuentoUnitarioDiscriminado, TotalDescuentoDiscriminado, IDMotivoDevolucion) Values(" & IDTransaccion & "," & oRow("IDProducto").ToString & ", " & oRow("ID").ToString & ",NULL," & oRow("IDDeposito").ToString & ",'" & oRow("Observacion").ToString & "'," & oRow("IDImpuesto").ToString & "," & CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString, True) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PrecioUnitario").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("CostoUnitario").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("DescuentoUnitario").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PorcentajeDescuento").ToString, True) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCostoImpuesto").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCosto").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalDescuento").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString, vDecimalOperacion) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString, vDecimalOperacion) & " ,'" & oRow("Caja").ToString & "'," & CSistema.FormatoNumeroBaseDatos(oRow("CantidadCaja").ToString) & "," & CSistema.FormatoNumeroBaseDatos(oRow("DescuentoUnitarioDiscriminado").ToString, vDecimalOperacion) & "," & CSistema.FormatoNumeroBaseDatos(oRow("TotalDescuentoDiscriminado").ToString, vDecimalOperacion) & "," & oRow("IDMotivoDevolucion").ToString & " )"
                    End If

                    'Si es pendiente, pero se guarda con comprobante
                    If vConComprobantes = True Then
                        sql2 = "Insert Into NotaCreditoVenta (IDTransaccionNotaCredito, IDTransaccionVentaGenerada, ID, Importe, Cobrado, Descontado, Saldo) Values (" & IDTransaccion & "," & oRow("IDTransaccionVenta").ToString & ", " & i & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Descontado").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString, vDecimalOperacion) & ")"
                    End If

                End If

                'Si la aplicacion es directa, insertar comunmente
                If chkAplicar.Valor = False Then
                    sql = "Insert Into DetalleNotaCredito (IDTransaccion, IDProducto, ID, IDTransaccionVenta, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, CostoUnitario, DescuentoUnitario, PorcentajeDescuento, Total, TotalImpuesto, TotalCostoImpuesto, TotalCosto, TotalDescuento, TotalDiscriminado, TotalCostoDiscriminado, Caja, CantidadCaja, IDMotivoDevolucion) Values(" & IDTransaccion & "," & oRow("IDProducto").ToString & ", " & oRow("ID").ToString & "," & oRow("IDTransaccionVenta").ToString & "," & oRow("IDDeposito").ToString & ",'" & oRow("Observacion").ToString & "'," & oRow("IDImpuesto").ToString & "," & CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString, True) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PrecioUnitario").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("CostoUnitario").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("DescuentoUnitario").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PorcentajeDescuento").ToString, True) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCostoImpuesto").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCosto").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalDescuento").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString, vDecimalOperacion) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString, vDecimalOperacion) & ",'" & oRow("Caja").ToString & "'," & CSistema.FormatoNumeroBaseDatos(oRow("CantidadCaja").ToString) & "," & oRow("IDMotivoDevolucion").ToString & " )"
                    sql2 = "Insert Into NotaCreditoVenta (IDTransaccionNotaCredito, IDTransaccionVentaGenerada, ID, Importe,Cobrado,Descontado,Saldo) Values (" & IDTransaccion & "," & oRow("IDTransaccionVenta").ToString & ", " & i & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Descontado").ToString, vDecimalOperacion) & "," & 0 & ")"
                End If

                'Ejecutar DetalleNotaCredito
                CSistema.ExecuteNonQuery(sql)

                'Guardar NotaCreditoVenta
                If sql2.Trim.Length > 0 Then
                    CSistema.ExecuteNonQuery(sql2)
                End If

                i = i + 1

            Next

        End If

        If rdbDescuentos.Checked = True Then

            If chkAplicar.Valor = False Then

                For Each oRow As DataRow In dtNotaCreditoVenta.Select(" Sel = 'True' ")
                    Dim sql As String
                    sql = "Insert Into NotaCreditoVenta (IDTransaccionNotaCredito, IDTransaccionVentaGenerada, ID, Importe, Cobrado, Descontado, Saldo) Values (" & IDTransaccion & "," & oRow("IDTransaccion").ToString & ", " & i & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Descontado").ToString, vDecimalOperacion) & "," & 0 & ") "
                    If CSistema.ExecuteNonQuery(sql) = 0 Then
                        Return False
                    End If

                    i = i + 1

                Next

                For Each oRow As DataRow In dtDetalle.Rows
                    Dim sql2 As String = ""
                    sql2 = "Insert Into DetalleNotaCredito (IDTransaccion, IDProducto, ID, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, CostoUnitario, DescuentoUnitario, PorcentajeDescuento, Total, TotalImpuesto, TotalCostoImpuesto, TotalCosto, TotalDescuento, TotalDiscriminado, TotalCostoDiscriminado, Caja, CantidadCaja, IDMotivoDevolucion) Values(" & IDTransaccion & "," & oRow("IDProducto").ToString & ", " & oRow("ID").ToString & "," & oRow("IDDeposito").ToString & ",'" & oRow("Observacion").ToString & "'," & oRow("IDImpuesto").ToString & "," & CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString, True) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PrecioUnitario").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("CostoUnitario").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("DescuentoUnitario").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PorcentajeDescuento").ToString, True) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCostoImpuesto").ToString, vDecimalOperacion) & "," & CSistema.FormatoNumeroBaseDatos(oRow("TotalCosto").ToString, vDecimalOperacion) & "," & CSistema.FormatoNumeroBaseDatos(oRow("TotalDescuento").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString, vDecimalOperacion) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString, vDecimalOperacion) & ",'" & oRow("Caja").ToString & "'," & CSistema.FormatoNumeroBaseDatos(oRow("CantidadCaja").ToString) & "," & oRow("IDMotivoDevolucion").ToString & " )"
                    If CSistema.ExecuteNonQuery(sql2) = 0 Then
                        Return False
                    End If
                Next

            Else
                For Each oRow As DataRow In dtNotaCreditoVenta.Select(" Sel = 'True' ")
                    Dim sql As String
                    sql = "Insert Into NotaCreditoVenta (IDTransaccionNotaCredito, IDTransaccionVentaGenerada, ID, Importe, Cobrado, Descontado, Saldo) Values (" & IDTransaccion & "," & oRow("IDTransaccion").ToString & ", " & i & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Descontado").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString, vDecimalOperacion) & ") "
                    If CSistema.ExecuteNonQuery(sql) = 0 Then
                        Return False
                    End If

                    i = i + 1

                Next

                For Each oRow As DataRow In dtDetalle.Rows
                    Dim sql2 As String = ""
                    sql2 = "Insert Into DetalleNotaCredito (IDTransaccion, IDProducto, ID, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, CostoUnitario, DescuentoUnitario, PorcentajeDescuento, Total, TotalImpuesto, TotalCostoImpuesto, TotalCosto, TotalDescuento, TotalDiscriminado, TotalCostoDiscriminado, Caja, CantidadCaja, IDMotivoDevolucion) Values(" & IDTransaccion & "," & oRow("IDProducto").ToString & ", " & oRow("ID").ToString & "," & oRow("IDDeposito").ToString & ",'" & oRow("Observacion").ToString & "'," & oRow("IDImpuesto").ToString & "," & CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString, True) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PrecioUnitario").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("CostoUnitario").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("DescuentoUnitario").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PorcentajeDescuento").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCostoImpuesto").ToString, vDecimalOperacion) & "," & CSistema.FormatoNumeroBaseDatos(oRow("TotalCosto").ToString, vDecimalOperacion) & "," & CSistema.FormatoNumeroBaseDatos(oRow("TotalDescuento").ToString, vDecimalOperacion) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString, vDecimalOperacion) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString, vDecimalOperacion) & ",'" & oRow("Caja").ToString & "'," & CSistema.FormatoNumeroBaseDatos(oRow("CantidadCaja").ToString) & "," & oRow("IDMotivoDevolucion").ToString & " )"
                    If CSistema.ExecuteNonQuery(sql2) = 0 Then
                        Return False
                    End If
                Next
            End If

        End If
        'End If
    End Function

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        cbxComprobante.SoloLectura = True
        txtObservacion.SoloLectura = True
        txtObservacionProducto.SoloLectura = True

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VNotaCredito Where Comprobante = '" & ObtenerNroComprobante() & "' And IDPuntoExpedicion=" & txtIDTimbrado.GetValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtNroComprobante, mensaje)
            ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            txtNroComprobante.txt.Focus()
            txtNroComprobante.txt.SelectAll()
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VNotaCredito Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select *, 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0 From VDetalleNotaCredito Where IDTransaccion=" & IDTransaccion & " Order By ID").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtNroComprobante, mensaje)
            ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)
        txtNroComprobante.SetValue(oRow("NroComprobante").ToString)
        Comprobante = oRow("Comprobante")
        'Dim dtCliente As DataTable
        'dtCliente = CSistema.ExecuteToDataTable("Select * from vCliente where ID = " & oRow("IDCliente").ToString).Copy
        'txtCliente.Conectar(dtCliente)
        'txtCliente.SetValue(oRow("IDCliente").ToString)
        txtCliente.SetValue(oRow("IDCliente").ToString)
        txtDireccion.txt.Text = oRow("Direccion").ToString
        txtTelefono.txt.Text = oRow("Telefono").ToString
        'Razon Social y RUC de clientes varios
        txtCliente.SetValue(oRow("Referencia").ToString, oRow("Cliente").ToString, oRow("RUC").ToString)
        'txtCliente.txtRazonSocial.txt.Text = oRow("Cliente").ToString
        'txtCliente.txtRUC.txt.Text = oRow("RUC").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        cbxDeposito.cbx.Text = oRow("Deposito").ToString
        'Moneda
        OcxCotizacion1.cbxMoneda.txt.Text = oRow("Moneda")
        OcxCotizacion1.cbxMoneda.cbx.SelectedValue = oRow("IDMoneda")
        OcxCotizacion1.SetValue(oRow("Moneda").ToString, oRow("Cotizacion").ToString)
        OcxCotizacion1.cbxMoneda.txt.Text = oRow("Moneda")
        OcxCotizacion1.cbxMoneda.cbx.SelectedValue = oRow("IDMoneda")

        txtObservacion.txt.Text = oRow("Observacion").ToString
        chkAplicar.Valor = oRow("Aplicar").ToString
        rdbDescuentos.Checked = oRow("Descuento").ToString
        rdbDevolucion.Checked = oRow("Devolucion").ToString
        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            'lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            lblUsuarioAnulado.Text = oRow("UsuarioAnulacion").ToString
            'Probar comentar el btnEliminar-10/08/2021-SCG
            btnEliminar.Visible = True
            btnAnular.Visible = False
            btnVerPedidoNC.Visible = False
            btnRecalcularSaldoNC.Visible = False

        Else
            flpAnuladoPor.Visible = False
            btnEliminar.Visible = False
            btnAnular.Visible = True
            btnVerPedidoNC.Visible = True
            btnRecalcularSaldoNC.Visible = True
        End If

        'Cargamos el detalle
        ListarDetalle()
        ListarDetalleDescuento()
        CDetalleImpuesto.Decimales = vDecimalOperacion
        OcxImpuesto1.Decimales = vDecimalOperacion
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, True, vDecimalOperacion)
        CalcularTotales()
        dgw.ReadOnly = True
        CargarComprobanteDescuentofacturados(IDTransaccion)

        calcularTotalDescuento()

        txtSaldo.txt.Text = ""

        'Inicializamos el Asiento
        CAsiento.Limpiar()

        'Foco
        txtNroComprobante.txt.Focus()
        txtNroComprobante.txt.SelectAll()
        btnNuevo.Enabled = False

        'SC: 03/06/2022 - 
        cbxComprobanteImpresion.Visible = False
        cbxComprobanteImpresion.Enabled = False
        TxtComprobanteImpresion.Visible = True
        TxtComprobanteImpresion.txt.Text = CSistema.ExecuteScalar("Select IsNull((select v.Comprobante From VNotaCredito nc Join VVenta v on nc.IDTransaccionVentaImpresion = v.IDTransaccion Where nc.IdTransaccion = " & IDTransaccion & "), 0 )")


    End Sub

    Sub Cancelar()
        txtNroComprobante.Enabled = "True"
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtNroComprobante.txt.ReadOnly = False
        txtNroComprobante.txt.Focus()
        cbxComprobante.SoloLectura = True
        txtObservacionProducto.SoloLectura = True
        'txtProducto.txt.sololectura = True

        Dim ID As Integer
        ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(NroComprobante), 1) From VNotaCredito Where IDPuntoExpedicion=" & txtIDTimbrado.GetValue & " "), Integer)

        txtNroComprobante.txt.Text = ID
        txtNroComprobante.txt.SelectAll()
        CargarOperacion()

    End Sub

    Sub CalcularImporte(ByVal Decimales As Boolean, ByVal Redondear As Boolean, ByVal PorTotal As Boolean)

        ctrError.Clear()
        tsslEstado.Text = ""

        Dim Cantidad As Decimal
        Dim PrecioUnitario As Decimal
        Dim Importe As Decimal

        Cantidad = CSistema.FormatoMoneda(txtCantidad.txt.Text, True)

        'Validar
        If Cantidad = 0 Then
            Dim mensaje As String = "No se puede calcular los montos si la cantidad es 0!"
            ctrError.SetError(lvLista, mensaje)
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If PorTotal = True Then
            PrecioUnitario = CSistema.FormatoMoneda(txtImporte.txt.Text, vDecimalOperacion) / Cantidad
        Else
            Importe = CSistema.FormatoMoneda(txtCostoUnitario.txt.Text, True) * Cantidad
        End If

        If Decimales = False Then
            PrecioUnitario = CInt(Math.Round(PrecioUnitario, 0))
            Importe = CLng(Math.Round(Importe, 0))
        Else
            'If Redondear = True Then
            PrecioUnitario = CDec(Math.Round(PrecioUnitario, 2))
            Importe = CDec(Math.Round(Importe, 2))
            'End If
        End If

        If PorTotal = True Then
            txtCostoUnitario.txt.Text = PrecioUnitario.ToString
        Else
            txtImporte.txt.Text = Importe.ToString
        End If

    End Sub

    Sub CalcularTotales()

        OcxImpuesto1.CargarValores(CDetalleImpuesto.dt)

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        lvLista.Items.Clear()

        'Variables
        Dim Total As Decimal = 0
        Dim Saldo As Decimal = 0
        'Cargamos registro por registro
        For Each oRow As DataRow In dtDetalle.Rows

            Dim item As ListViewItem = lvLista.Items.Add(oRow("IDProducto").ToString)

            Dim Descripcion As String
            If oRow("Observacion").ToString.Trim.Length > 0 Then
                Descripcion = IIf(oRow("Descripcion").ToString.Trim = oRow("Observacion").ToString.Trim, oRow("Producto").ToString.Trim + " - " + oRow("Observacion"), oRow("Descripcion").ToString.Trim & " - " & oRow("Observacion").ToString.Trim)
            Else
                Descripcion = oRow("Descripcion").ToString.Trim

            End If
            item.SubItems.Add(oRow("Referencia").ToString)
            item.SubItems.Add(oRow("Descripcion").ToString)
            item.SubItems.Add(oRow("Comprobante").ToString)
            item.SubItems.Add(oRow("Ref Imp").ToString)
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("Cantidad").ToString, True))
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("PrecioUnitario").ToString, vDecimalOperacion))
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("Total").ToString, vDecimalOperacion))


        Next

        Bloquear()

    End Sub

    Sub CargarProducto()

        If vConComprobantes = True Then
            CargarProductoConComprobante()
        Else
            CargarProductoSinComprobante()
        End If

    End Sub

    Sub CargarProductoConComprobante()

        'Validar
        'Seleccion de Producto
        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Deposito
        If IsNumeric(cbxDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If cbxDeposito.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cantidad
        If IsNumeric(txtCantidad.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtCantidad.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim CantidadProducto As Decimal = txtCantidad.ObtenerValor
        Dim CantidadCaja As Integer = CantidadProducto / txtProducto.Registro("UnidadPorCaja")

        'Obtenemos la cantidad que existe en el comprobante
        Dim CantidadComprobante As Decimal = txtProducto.Registro("SaldoCantidad").ToString

        'Obtenemos la cantidad de productos incluyendo los que estan en el detalle
        For Each oRow As DataRow In dtDetalle.Rows

            If cbxComprobante.GetValue = oRow("IDtransaccionVenta") And txtProducto.Registro("ID") = oRow("IDProducto") Then
                CSistema.MostrarError("No puede ingresar el mismo producto 2 veces", ctrError, txtProducto, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Sub
            End If

        Next

        If CantidadProducto > CantidadComprobante Then
            CSistema.MostrarError("La cantidad no puede ser mayor al del comprobante (" & CSistema.FormatoNumero(CantidadComprobante.ToString) & ")", ctrError, txtCantidad, tsslEstado, ErrorIconAlignment.MiddleRight)
            Exit Sub
        End If

        'Cargamos el registro en el detalle
        dtDetalle.Columns("IDTransaccionVenta").AllowDBNull = True

        Dim dRow As DataRow = dtDetalle.NewRow()
        Dim VentaRow As DataRow = dtDetalleVenta.Select("IDTransaccion= " & cbxComprobante.GetValue & " And ID = " & txtProducto.Registro("ID") & " ")(0)

        Try

            'Obtener Valores
            'Productos
            dRow("IDProducto") = txtProducto.Registro("ID").ToString
            dRow("ID") = dtDetalle.Rows.Count
            dRow("IDTransaccionVenta") = cbxComprobante.GetValue

            If txtObservacionProducto.txt.Text.Trim.Length = 0 Then
                dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString
            Else
                dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString & " - " & txtObservacionProducto.txt.Text
            End If
            dRow("Referencia") = txtProducto.Registro("Ref")
            dRow("CodigoBarra") = VentaRow("CodigoBarra").ToString
            dRow("Observacion") = txtObservacionProducto.txt.Text

            'Deposito
            dRow("IDDeposito") = cbxDeposito.cbx.SelectedValue
            dRow("Deposito") = cbxDeposito.cbx.Text

            'Cantidad y Precios
            dRow("Cantidad") = CantidadProducto

            Dim PrecioUnitario As Decimal
            PrecioUnitario = VentaRow("PrecioUnitario") - VentaRow("DescuentoUnitario")
            ' PrecioUnitario = VentaRow("PrecioUnitario")

            dRow("PrecioUnitario") = PrecioUnitario
            dRow("Pre. Uni.") = PrecioUnitario
            dRow("Total") = PrecioUnitario * CantidadProducto

            'Impuestos
            dRow("IDImpuesto") = VentaRow("IDImpuesto").ToString
            dRow("Impuesto") = VentaRow("Impuesto").ToString
            dRow("Ref Imp") = VentaRow("Ref Imp").ToString
            dRow("Exento") = VentaRow("Exento").ToString
            dRow("TotalImpuesto") = 0
            dRow("TotalDiscriminado") = 0
            CSistema.CalcularIVA(dRow("IDImpuesto").ToString, CDec(dRow("Total").ToString), vDecimalOperacion, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))

            'Costo
            'dRow("CostoUnitario") = VentaRow("Costo").ToString
            'dRow("TotalCosto") = CantidadProducto * VentaRow("Costo").ToString
            'dRow("TotalCostoImpuesto") = 0
            'dRow("TotalCostoDiscriminado") = dRow("TotalCosto")
            'CSistema.CalcularIVA(VentaRow("IDImpuesto").ToString, CDec(dRow("TotalCosto").ToString), vDecimalOperacion, True, 0, dRow("TotalCostoImpuesto"))

            'COsto igual a Venta
            'Costo

            Dim vCotizacionMonedaCosto As Decimal
            'CSistema.ExecuteScalar("Select isnull(cotizacion,0) from vCotizacion where ID = (Select max(ID) from Cotizacion where IDMoneda = " & txtProducto.Registro("IDMoneda").ToString & ")")

            If OcxCotizacion1.Registro("ID") = txtProducto.Registro("IDMoneda").ToString Then
                dRow("CostoUnitario") = txtProducto.Registro("Costo").ToString
            ElseIf (OcxCotizacion1.Registro("ID") = 1) And (txtProducto.Registro("IDMoneda").ToString <> 1) Then 'FACT GS - Costo otros
                vCotizacionMonedaCosto = CSistema.ExecuteScalar("Select isnull(cotizacion,0) from vCotizacion where ID = (Select max(ID) from Cotizacion where IDMoneda = " & txtProducto.Registro("IDMoneda").ToString & ")")
                dRow("CostoUnitario") = txtProducto.Registro("Costo").ToString * vCotizacionMonedaCosto
            ElseIf (OcxCotizacion1.Registro("ID") <> 1) And (txtProducto.Registro("IDMoneda").ToString <> 1) And (OcxCotizacion1.Registro("ID") <> txtProducto.Registro("IDMoneda").ToString) Then 'FACT US - Costo otros
                vCotizacionMonedaCosto = CSistema.ExecuteScalar("Select isnull(cotizacion,0) from vCotizacion where ID = (Select max(ID) from Cotizacion where IDMoneda = " & txtProducto.Registro("IDMoneda").ToString & ")")
                dRow("CostoUnitario") = (txtProducto.Registro("Costo").ToString * vCotizacionMonedaCosto) / OcxCotizacion1.Registro("Cotizacion")
            ElseIf (OcxCotizacion1.Registro("ID") <> 1) And (txtProducto.Registro("IDMoneda").ToString = 1) Then 'FACT US - Costo GS
                dRow("CostoUnitario") = txtProducto.Registro("Costo").ToString / OcxCotizacion1.Registro("Cotizacion")
            End If


            'dRow("TotalCosto") = CantidadProducto * txtProducto.Registro("Costo").ToString
            'dRow("TotalCostoImpuesto") = 0
            'dRow("TotalCostoDiscriminado") = dRow("TotalCosto")
            'CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("TotalCosto").ToString), True, False, 0, dRow("TotalCostoImpuesto"))

            dRow("TotalCosto") = CantidadProducto * dRow("CostoUnitario")
            dRow("TotalCostoImpuesto") = 0
            dRow("TotalCostoDiscriminado") = dRow("TotalCosto")
            CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("TotalCosto").ToString), True, False, 0, dRow("TotalCostoImpuesto"))

            'Descuentos
            dRow("PorcentajeDescuento") = VentaRow("PorcentajeDescuento")
            dRow("DescuentoUnitario") = VentaRow("DescuentoUnitario")
            dRow("DescuentoUnitarioDiscriminado") = VentaRow("DescuentoUnitarioDiscriminado")
            CSistema.CalcularIVA(VentaRow("IDImpuesto"), VentaRow("DescuentoUnitario"), vDecimalOperacion, True, dRow("DescuentoUnitarioDiscriminado"))

            dRow("TotalDescuento") = VentaRow("DescuentoUnitario") * CantidadProducto
            dRow("TotalDescuentoDiscriminado") = dRow("DescuentoUnitarioDiscriminado") * CantidadProducto

            'Totales
            dRow("TotalBruto") = dRow("Total")
            dRow("TotalSinDescuento") = dRow("Total") - dRow("TotalDescuento")
            dRow("TotalSinImpuesto") = dRow("Total") - dRow("TotalImpuesto")
            dRow("TotalNeto") = 0
            dRow("TotalNetoConDescuentoNeto") = dRow("TotalDiscriminado") + dRow("TotalDescuentoDiscriminado")

            'Venta
            dRow("IDTransaccionVenta") = cbxComprobante.GetValue
            dRow("Comprobante") = cbxComprobante.cbx.Text

            Try
                dRow("Cobrado") = dtVentas.Select(" IDTransaccion = " & cbxComprobante.GetValue)(0)("Cobrado")
                dRow("Descontado") = dtVentas.Select(" IDTransaccion = " & cbxComprobante.GetValue)(0)("Descontado")
                dRow("Saldo") = dtVentas.Select(" IDTransaccion = " & cbxComprobante.GetValue)(0)("Saldo")
            Catch ex As Exception

            End Try

            'Cantidad Caja
            dRow("Caja") = 0
            dRow("CantidadCaja") = CantidadCaja
            If rdbDevolucion.Checked = True Then
                Dim frm As New frmMotivoDevolucionNC
                frm.IDTransaccion = IDTransaccion
                frm.Text = " Motivo Devolucion NC "
                frm.WindowState = FormWindowState.Normal
                frm.StartPosition = FormStartPosition.CenterScreen
                frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
                frm.ShowDialog(Me)

                dRow("IDMotivoDevolucion") = frm.IDMotivo
            Else
                dRow("IDMotivoDevolucion") = 0
            End If
            'Agregamos al detalle
            dtDetalle.Rows.Add(dRow)

            'Cargar Descuento
            CargarDescuento(VentaRow)

            ListarDetalle()
            CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, True, vDecimalOperacion)
            CalcularTotales()

            'Inicializamos los valores
            txtObservacionProducto.txt.Clear()
            txtCantidad.txt.Text = "0"
            txtImporte.txt.Text = "0"
            txtCostoUnitario.txt.Text = "0"
            txtProducto.txt.Clear()

            'Retorno de Foco
            txtProducto.txt.SelectAll()
            txtProducto.txt.Focus()

            ctrError.Clear()
            tsslEstado.Text = ""

        Catch ex As Exception

        End Try


    End Sub

    Sub CargarProductoSinComprobante()

        'Validar
        'Seleccion de Producto
        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Deposito
        If IsNumeric(cbxDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If cbxDeposito.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cantidad
        If IsNumeric(txtCantidad.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtCantidad.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim CantidadProducto As Decimal = txtCantidad.ObtenerValor
        Dim CantidadCaja As Integer = CantidadProducto / txtProducto.Registro("UnidadPorCaja")




        If vHabilitar = True Then

            'Obtenemos la cantidad que existe en el comprobante
            Dim CantidadComprobante As Decimal = txtProducto.Registro("SaldoCantidad").ToString

            'Obtenemos la cantidad de productos incluyendo los que estan en el detalle
            For Each oRow As DataRow In dtDetalle.Rows

                If cbxComprobante.GetValue = oRow("IDtransaccionVenta") And txtProducto.Registro("ID") = oRow("IDProducto") Then
                    CSistema.MostrarError("No puede ingresar el mismo producto 2 veces", ctrError, txtProducto, tsslEstado, ErrorIconAlignment.MiddleRight)
                    Exit Sub
                End If

            Next

            If CantidadProducto > CantidadComprobante Then
                CSistema.MostrarError("La cantidad no puede ser mayor al del comprobante (" & CSistema.FormatoNumero(CantidadComprobante.ToString) & ")", ctrError, txtCantidad, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Sub
            End If


        End If


        'Cargamos el registro en el detalle
        dtDetalle.Columns("IDTransaccionVenta").AllowDBNull = True

        Dim dRow As DataRow = dtDetalle.NewRow()

        'Obtener Valores
        'Productos
        dRow("IDProducto") = txtProducto.Registro("ID").ToString
        dRow("ID") = dtDetalle.Rows.Count
        dRow("IDTransaccionVenta") = cbxComprobante.GetValue

        If txtObservacionProducto.txt.Text.Trim.Length = 0 Then
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString
        Else
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString & " - " & txtObservacionProducto.txt.Text
        End If

        dRow("CodigoBarra") = txtProducto.Registro("CodigoBarra").ToString
        dRow("Observacion") = txtObservacionProducto.txt.Text

        'Deposito
        dRow("IDDeposito") = cbxDeposito.cbx.SelectedValue
        dRow("Deposito") = cbxDeposito.cbx.Text

        'Cantidad y Precios
        dRow("Cantidad") = CantidadProducto

        Dim PrecioUnitario As Decimal

        If vHabilitar = True Then
            PrecioUnitario = CDec(txtProducto.Registro("PrecioUnitario").ToString)
        Else
            PrecioUnitario = txtCostoUnitario.txt.Text
        End If

        dRow("PrecioUnitario") = PrecioUnitario
        dRow("Pre. Uni.") = PrecioUnitario
        dRow("Total") = PrecioUnitario * CantidadProducto

        'Impuestos
        dRow("IDImpuesto") = txtProducto.Registro("IDImpuesto").ToString
        dRow("Impuesto") = txtProducto.Registro("Impuesto").ToString
        dRow("Ref Imp") = txtProducto.Registro("Ref Imp").ToString
        dRow("TotalImpuesto") = 0
        dRow("TotalDiscriminado") = 0
        CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("Total").ToString), vDecimalOperacion, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))

        'Costo
        dRow("CostoUnitario") = txtProducto.Registro("Costo").ToString
        dRow("TotalCosto") = CantidadProducto * txtProducto.Registro("Costo").ToString
        dRow("TotalCostoImpuesto") = 0
        dRow("TotalCostoDiscriminado") = 0
        CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("TotalCosto").ToString), vDecimalOperacion, True, dRow("TotalCostoDiscriminado"), dRow("TotalCostoImpuesto"))

        'Descuento
        dRow("TotalDescuento") = 0
        dRow("DescuentoUnitario") = 0
        dRow("PorcentajeDescuento") = 0
        dRow("TotalDescuentoDiscriminado") = 0

        'Totales
        dRow("TotalBruto") = 0
        dRow("TotalSinDescuento") = dRow("Total") - dRow("TotalDescuento")
        dRow("TotalSinImpuesto") = dRow("Total") - dRow("TotalImpuesto")
        dRow("TotalNeto") = 0
        dRow("TotalNetoConDescuentoNeto") = dRow("TotalDiscriminado") + dRow("TotalDescuentoDiscriminado")

        'Venta
        dRow("IDTransaccionVenta") = cbxComprobante.GetValue
        dRow("Comprobante") = cbxComprobante.cbx.Text

        Try
            dRow("Cobrado") = dtVentas.Select(" IDTransaccion = " & cbxComprobante.GetValue)(0)("Cobrado")
            dRow("Descontado") = dtVentas.Select(" IDTransaccion = " & cbxComprobante.GetValue)(0)("Descontado")
            dRow("Saldo") = dtVentas.Select(" IDTransaccion = " & cbxComprobante.GetValue)(0)("Saldo")
        Catch ex As Exception

        End Try

        'Cantidad Caja
        dRow("Caja") = 0
        dRow("CantidadCaja") = CantidadCaja

        If rdbDevolucion.Checked = True Then
            Dim frm As New frmMotivoDevolucionNC
            frm.IDTransaccion = IDTransaccion
            frm.Text = " Motivo Devolucion NC "
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.ShowDialog(Me)

            dRow("IDMotivoDevolucion") = frm.IDMotivo
        Else
            dRow("IDMotivoDevolucion") = 0
        End If

        'Agregamos al detalle
        dtDetalle.Rows.Add(dRow)

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, True, vDecimalOperacion)
        CalcularTotales()

        'Inicializamos los valores
        txtObservacionProducto.txt.Clear()
        txtCantidad.txt.Text = "0"
        txtImporte.txt.Text = "0"
        txtCostoUnitario.txt.Text = "0"
        txtProducto.txt.Clear()

        'Retorno de Foco
        txtProducto.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Sub CargarDescuentos()

        'Validar
        'Cliente
        If txtCliente.Seleccionado = False Then

            MsgBox("Selecione un cliente", vbExclamation)

            Exit Sub

        End If

        Dim frm As New frmNotaCreditoCargarDescuento
        frm.Decimales = vDecimalOperacion
        frm.IDSucursal = cbxSucursal.cbx.SelectedValue
        FGMostrarFormulario(Me, frm, "Cargar detalle", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)


        If frm.Procesado = False Then
            Exit Sub
        End If

        'Cargamos el registro en el detalle
        Dim dRow As DataRow = dtDetalle.NewRow()


        'Obtener Valores
        'Productos
        dRow("IDProducto") = frm.Registro("ID").ToString
        dRow("ID") = dtDetalle.Rows.Count
        dRow("Descripcion") = frm.Registro("Descripcion").ToString
        dRow("CodigoBarra") = frm.Registro("CodigoBarra").ToString
        dRow("Observacion") = frm.Observacion

        'Deposito
        dRow("IDDeposito") = cbxDeposito.cbx.SelectedValue
        dRow("Deposito") = cbxDeposito.cbx.Text

        'Cantidad y Precios
        dRow("Total") = frm.Importe

        'Impuestos
        dRow("IDImpuesto") = frm.Registro("IDImpuesto").ToString
        dRow("Impuesto") = frm.Registro("Impuesto").ToString
        dRow("Ref Imp") = frm.Registro("Ref Imp").ToString
        dRow("TotalImpuesto") = 0
        dRow("TotalDiscriminado") = 0
        CSistema.CalcularIVA(dRow("IDImpuesto"), CDec(dRow("Total").ToString), vDecimalOperacion, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))

        'Costo
        dRow("CostoUnitario") = 0
        dRow("TotalCosto") = 0
        dRow("TotalCostoImpuesto") = 0
        dRow("TotalCostoDiscriminado") = 0

        'Descuento
        dRow("TotalDescuento") = 0
        dRow("DescuentoUnitario") = 0
        dRow("PorcentajeDescuento") = 0
        dRow("TotalDescuentoDiscriminado") = 0

        'Totales
        dRow("TotalBruto") = 0
        dRow("TotalSinDescuento") = dRow("Total") - dRow("TotalDescuento")
        dRow("TotalSinImpuesto") = dRow("Total") - dRow("TotalImpuesto")
        dRow("TotalNeto") = 0
        dRow("TotalNetoConDescuentoNeto") = dRow("TotalDiscriminado") + dRow("TotalDescuentoDiscriminado")

        'Cantidad Caja
        dRow("Caja") = False
        dRow("CantidadCaja") = 1
        dRow("IDMotivoDevolucion") = 0

        'Agregamos al detalle
        dtDetalle.Rows.Add(dRow)

        ListarDetalleDescuento()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, True, vDecimalOperacion)
        CalcularTotales()

        'Inicializamos los valores
        txtObservacionProducto.txt.Clear()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Sub ListarDetalleDescuento()

        'Limpiamos todo el detalle
        lvListaDescuento.Items.Clear()

        'Variables
        Dim Total As Decimal = 0

        'Cargamos registro por registro
        For Each oRow As DataRow In dtDetalle.Rows

            Dim item As ListViewItem = lvListaDescuento.Items.Add(oRow("IDProducto").ToString)

            Dim Descripcion As String
            If oRow("Observacion").ToString.Trim.Length > 0 Then
                Descripcion = IIf(oRow("Descripcion").ToString.Trim = oRow("Observacion").ToString.Trim, oRow("Producto").ToString.Trim + " - " + oRow("Observacion"), oRow("Descripcion").ToString.Trim & " - " & oRow("Observacion").ToString.Trim)
            Else
                Descripcion = oRow("Descripcion").ToString.Trim
            End If

            item.SubItems.Add(Descripcion)
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("Total").ToString, vDecimalOperacion))
            Total = Total + CDec(oRow("Total").ToString)

        Next

        txtTotalDescuento.txt.Text = Total

        If vNuevo Then
            calcularTotalDescuento()
        End If

        Bloquear()


    End Sub

    Sub EliminarProductoDevolucion()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If lvLista.SelectedItems.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(lvLista, mensaje)
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Eliminamos los descuentos
        EliminarDescuentos()

        dtDetalle.Rows(lvLista.SelectedIndices(0)).Delete()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtDetalle.Rows
            If Not oRow.RowState = DataRowState.Deleted Then

                'Renumerar descuentos
                For Each dRow As DataRow In dtDescuento.Select(" IDProducto=" & oRow("IDProducto") & " And ID=" & oRow("ID") & " ")
                    dRow("ID") = Index
                Next

                oRow("ID") = Index
                Index = Index + 1
            End If
        Next

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, True, vDecimalOperacion)
        CalcularTotales()
        Bloquear()

    End Sub

    Sub EliminarProductoDescuento()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If lvListaDescuento.SelectedItems.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(lvListaDescuento, mensaje)
            ctrError.SetIconAlignment(lvListaDescuento, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        dtDetalle.Rows(lvListaDescuento.SelectedIndices(0)).Delete()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtDetalle.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        ListarDetalleDescuento()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, True, vDecimalOperacion)
        CalcularTotales()

        Bloquear()

    End Sub

    Sub EliminarComprobanteDescuento()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If dgw.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgw, mensaje)
            ctrError.SetIconAlignment(lvListaDescuento, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        For Each oRow As DataGridViewRow In dgw.SelectedRows
            Dim vIDTransaccion As Integer

            vIDTransaccion = oRow.Cells("colIDTransaccion").Value
            For Each dtRow As DataRow In dtNotaCreditoVenta.Select(" IDTransaccion = " & vIDTransaccion)
                dtRow("Sel") = False

            Next
        Next

        ListarComprobanteDescuento()


    End Sub

    Sub CargarComprobante()

        If txtCliente.Seleccionado = False Then
            Exit Sub
        End If

        CargarComprobanteDevolucion()
        ObtenerComprobanteDescuento()


    End Sub

    Sub CargarComprobanteDevolucion()

        ctrError.Clear()
        tsslEstado.Text = ""

        If vNuevo = False Then
            If txtCliente.Seleccionado = True Then
                Exit Sub
            End If
        End If

        If txtCliente.Seleccionado = False Then
            CSistema.MostrarError("Seleccione el cliente", ctrError, chkAplicar, tsslEstado)
            Exit Sub
        End If

        If chkAplicar.Valor = True Then
            dtVentas = CSistema.ExecuteToDataTable("Select IDTransaccion, Comprobante, Cobrado, Descontado, Saldo From vVenta Where DATEDIFF(DD, FechaEmision, GetDate()) <= 365 and Anulado = 'False' And Cancelado='False' And IDCliente =" & txtCliente.Registro("ID").ToString & " and IDMoneda = " & OcxCotizacion1.Registro("ID")).Copy
            dtDetalleVenta = CSistema.ExecuteToDataTable("Select * From VDetalleVentaParaNotaCredito V Where V.Dias <= 365 And Cancelado='False' And V.IDCliente =" & txtCliente.Registro("ID").ToString).Copy
        Else
            dtVentas = CSistema.ExecuteToDataTable("Select IDTransaccion, Comprobante, Cobrado, Descontado, Saldo From vVenta Where Cancelado = 'False' and Anulado = 'False' and IDCliente =" & txtCliente.Registro("ID").ToString & " and IDMoneda = " & OcxCotizacion1.Registro("ID")).Copy
            dtDetalleVenta = CSistema.ExecuteToDataTable("Select * From VDetalleVentaParaNotaCredito V Where V.Cancelado='False' And V.IDCliente =" & txtCliente.Registro("ID").ToString).Copy
        End If
        CSistema.SqlToComboBox(cbxComprobante.cbx, dtVentas, "IDTransaccion", "Comprobante")
        cbxComprobante.cbx.Text = ""

    End Sub

    Sub ObtenerComprobanteDescuento()

        If txtCliente.Registro Is Nothing Then
            Exit Sub
        End If

        dtNotaCreditoVenta = CSistema.ExecuteToDataTable("Select IDTransaccion, Comprobante,[Cod.], Cliente, Total, Cobrado, Descontado, Saldo, Condicion,Fec,[Fec. Venc.], 'Sel'='False', 'Importe'=0.0, 'Cancelar'='False' From VVenta where Anulado = 'False' and Cancelado = 'False' And Saldo>0 and IDCliente =" & txtCliente.Registro("ID").ToString & " and IDMOneda = " & OcxCotizacion1.Registro("ID")).Copy

    End Sub

    Sub CargarComprobanteDescuento()

        Dim frm As New frmNotaCreditoCargarDocumentoDescuento
        frm.Decimales = vDecimalOperacion
        frm.dt = dtNotaCreditoVenta
        FGMostrarFormulario(Me, frm, "Comprobantes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        dtNotaCreditoVenta = frm.dt
        ListarComprobanteDescuento()

    End Sub

    Sub ListarComprobanteDescuento()

        'Limpiar la grilla
        dgw.Rows.Clear()

        Dim dt As DataTable = dtNotaCreditoVenta.Copy
        For Each oRow As DataRow In dt.Select("Sel='True'")

            Dim Registro(11) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Sel").ToString
            Registro(2) = oRow("Comprobante").ToString
            Registro(3) = oRow("Cod.").ToString
            Registro(4) = oRow("Condicion").ToString
            Registro(5) = oRow("Fec. Venc.").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Total").ToString, vDecimalOperacion)
            Registro(7) = CSistema.FormatoMoneda(oRow("Cobrado").ToString, vDecimalOperacion)
            Registro(8) = CSistema.FormatoMoneda(oRow("Descontado").ToString, vDecimalOperacion)
            Registro(9) = CSistema.FormatoMoneda(oRow("Saldo").ToString, vDecimalOperacion)
            Registro(10) = CSistema.FormatoMoneda(oRow("Importe").ToString, vDecimalOperacion)
            Registro(11) = oRow("Cancelar").ToString

            'Sumar el total de la deuda
            dgw.Rows.Add(Registro)

        Next

        calcularTotalDescuento()

    End Sub

    Sub CargarComprobanteDescuentofacturados(ByVal IDTransaccion As Integer)

        'If txtCliente.Registro Is Nothing Then
        '    Exit Sub
        'End If


        'dtNotaCreditoVenta = CSistema.ExecuteToDataTable("Select IDTransaccion, Comprobante,[Cod.], Cliente, Total, NCV.Cobrado, NCV.Descontado, NCV.Saldo, NCV.Importe, Condicion, Fec, [Fec. Venc.], 'Sel'='True', 'Cancelar'='False' From VVenta V Join NotaCreditoVenta NCV On V.IDTransaccion=NCV.IDTransaccionVentaGenerada  where IDTransaccionNotaCredito = " & IDTransaccion & " And IDCliente =" & txtCliente.Registro("ID").ToString).Copy
        dtNotaCreditoVenta = CSistema.ExecuteToDataTable("Select IDTransaccion, Comprobante,[Cod.], Cliente, Total, NCV.Cobrado, NCV.Descontado, NCV.Saldo, NCV.Importe, Condicion, Fec, [Fec. Venc.], 'Sel'='True', 'Cancelar'='False' From VVenta V Join NotaCreditoVenta NCV On V.IDTransaccion=NCV.IDTransaccionVentaGenerada  where IDTransaccionNotaCredito = " & IDTransaccion).Copy

        'Limpiar la grilla
        dgw.Rows.Clear()
        'Dim TotalSaldo As Decimal = 0

        For Each oRow As DataRow In dtNotaCreditoVenta.Rows

            Dim Registro(11) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Sel").ToString
            Registro(2) = oRow("Comprobante").ToString
            Registro(3) = oRow("Cod.").ToString
            Registro(4) = oRow("Condicion").ToString
            Registro(5) = oRow("Fec. Venc.").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Total").ToString, vDecimalOperacion)
            Registro(7) = CSistema.FormatoMoneda(oRow("Cobrado").ToString, vDecimalOperacion)
            Registro(8) = CSistema.FormatoMoneda(oRow("Descontado").ToString, vDecimalOperacion)
            Registro(9) = CSistema.FormatoMoneda(oRow("Saldo").ToString, vDecimalOperacion)
            Registro(10) = CSistema.FormatoMoneda(oRow("Importe").ToString, vDecimalOperacion)
            Registro(11) = oRow("Cancelar").ToString

            'Sumar el total de la deuda

            'TotalSaldo = CDec(oRow("Saldo").ToString) - txtTotalDescuento.txt.Text
            dgw.Rows.Add(Registro)

        Next

        ' txtDescuento.txt.ReadOnly = True
        'dgw.ReadOnly = True




    End Sub

    Function ObtenerNroComprobante() As String

        'Dim dt As DataTable = CData.GetTable("VPuntoExpedicion", " IDOperacion = " & IDOperacion) '& " AND IDTipoComprobante" = cbxTipoComprobante.GetValue)
        Dim dt As DataTable = CData.FiltrarDataTable(dtPuntoExpedicion, " IDOperacion = " & IDOperacion & " And IDTipoComprobante = " & cbxTipoComprobante.GetValue) '& " And Estado='True' ")
        If dt Is Nothing AndAlso dt.Rows.Count = 0 Then
            Return "0"
        End If

        If IsNumeric(txtIDTimbrado.GetValue) = False Then
            Return "0"
        End If

        If dt.Select("ID=" & txtIDTimbrado.GetValue).Count = 0 Then
            Return "0"
        End If

        Dim PuntoExpedicionRow As DataRow = dt.Select("ID=" & txtIDTimbrado.GetValue)(0)
        ObtenerNroComprobante = PuntoExpedicionRow("ReferenciaSucursal").ToString & "-" & PuntoExpedicionRow("ReferenciaPunto").ToString & "-" & txtNroComprobante.GetValue



    End Function

    Sub ObtenerInformacionPuntoVenta()

        txtTalonario.txt.Text = "0"
        txtVencimientoTimbrado.txt.Text = ""
        txtRestoTimbrado.txt.Text = "0"
        cbxTipoComprobante.cbx.Text = ""
        txtReferenciaSucursal.txt.Text = "000"
        txtReferenciaTerminal.txt.Text = "000"
        txtNroComprobante.txt.Text = "000"
        cbxCiudad.cbx.Text = ""
        cbxSucursal.cbx.Text = ""
        txtProducto.IDSucursal = vgIDSucursal

        If IsNumeric(txtIDTimbrado.GetValue) = False Then
            Exit Sub
        End If

        If dtPuntoExpedicion Is Nothing Then
            Exit Sub
        End If

        For Each oRow As DataRow In dtPuntoExpedicion.Select(" ID=" & txtIDTimbrado.GetValue)
            txtTimbrado.txt.Text = oRow("Timbrado").ToString
            txtTalonario.txt.Text = oRow("TalonarioActual").ToString
            txtVencimientoTimbrado.SetValueFromString(oRow("Vencimiento").ToString)
            txtRestoTimbrado.txt.Text = oRow("Saldo").ToString
            cbxTipoComprobante.SelectedValue(oRow("IDTipoComprobante").ToString)
            txtReferenciaSucursal.txt.Text = oRow("ReferenciaSucursal").ToString
            txtReferenciaTerminal.txt.Text = oRow("ReferenciaPunto").ToString
            txtNroComprobante.txt.Text = oRow("ProximoNumero").ToString
            cbxCiudad.SelectedValue(oRow("IDCiudad").ToString)
            cbxSucursal.SelectedValue(oRow("IDSucursal").ToString)

            ' cbxDeposito.cbx.Text = 
            txtProducto.IDSucursal = oRow("IDSucursal").ToString
        Next

    End Sub

    Sub calcularTotalDescuento()

        Dim TotalSaldo As Decimal = 0
        Dim Total As Decimal = 0
        Dim TotalComprobantes As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows

            If oRow.Cells(1).Value = True Then
                Total = Total + oRow.Cells(10).Value
                TotalComprobantes = TotalComprobantes + 1
            End If

        Next
        TotalSaldo = txtTotalDescuento.ObtenerValor - Total
        txtDescontados.SetValue(Total)
        txtCantidadCobrado.SetValue(TotalComprobantes)
        txtSaldo.SetValue(TotalSaldo)


    End Sub

    Sub Aplica()

        If vNuevo = False Then
            Exit Sub
        End If

        If chkAplicar.Valor = True Then
            HabilitarConComprobante(True)
            vHabilitar = True
            ObtenerComprobanteDescuento()
            vConComprobantes = True


            'Comentada la Pregunta 28/01/2022 SC: ya que siempre debe ser con Comprobante
            'If MessageBox.Show("Desea aplicar con comprobantes?", "Aplicar", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
            '    HabilitarConComprobante(True)
            '    vHabilitar = True
            '    ObtenerComprobanteDescuento()
            '    vConComprobantes = True
            'Else
            '    HabilitarConComprobante(False)
            '    vHabilitar = False
            '    vConComprobantes = False
            '    If rdbDescuentos.Checked = True Then
            '        dgw.Rows.Clear()
            '        'chkAplicar.Valor = False
            '    End If
            'End If
        Else
            HabilitarConComprobante(True)
        End If


    End Sub

    Sub HabilitarConComprobante(ByRef Comprobante As Boolean)

        If Comprobante = True Then
            cbxComprobante.cbx.Text = ""
            cbxComprobante.SoloLectura = False
            CargarComprobanteDevolucion()
            txtCostoUnitario.SoloLectura = True
            txtImporte.SoloLectura = True

            'Descuento
            lklAgregarComprobanteDescuento.Enabled = True
            lklEliminarComprobanteDescuento.Enabled = True

        Else
            cbxComprobante.SeleccionObligatoria = False
            cbxComprobante.cbx.Text = ""
            cbxComprobante.SoloLectura = True
            txtProducto.Conectar()
            txtCostoUnitario.SoloLectura = False
            txtImporte.SoloLectura = False

            'Descuento
            lklAgregarComprobanteDescuento.Enabled = False
            lklEliminarComprobanteDescuento.Enabled = False

        End If

    End Sub

    Sub Seleccionardescuento()

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells(0).Value

            For Each oRow1 As DataRow In dtNotaCreditoVenta.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Sel") = oRow.Cells("colSel").Value
                oRow1("Importe") = CSistema.FormatoMoneda(oRow.Cells("colImporte").Value, vDecimalOperacion)
                oRow1("Cancelar") = oRow.Cells("colcancelar").Value
            Next

        Next



    End Sub

    Sub MoverRegistro(ByRef e As System.Windows.Forms.KeyEventArgs)

        If vNuevo = True Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtNroComprobante.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtNroComprobante.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(NroComprobante), 1) From VNotaCredito Where IDPuntoExpedicion=" & txtIDTimbrado.GetValue & " "), Integer)

            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(NroComprobante), 1) From VNotaCredito Where IDPuntoExpedicion=" & txtIDTimbrado.GetValue & " "), Integer)

            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        'If e.KeyCode = vgKeyNuevoRegistro And btnNuevo.Enabled = True Then
        '   Nuevo()
        'End If
        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub Bloquear()

        If dtDetalle.Rows.Count > 0 Then
            rdbDescuentos.Enabled = False
            rdbDevolucion.Enabled = False
            chkAplicar.Enabled = False
            cbxDeposito.SoloLectura = True
            txtCliente.SoloLectura = True
        Else
            rdbDescuentos.Enabled = True
            rdbDevolucion.Enabled = True
            chkAplicar.Enabled = True
            cbxDeposito.SoloLectura = False
            txtCliente.SoloLectura = False
        End If


        For i As Integer = 0 To dgw.Rows.Count - 1
            If dgw.Rows(i).Cells("colSel").Value = True Then
                rdbDescuentos.Enabled = False
                rdbDevolucion.Enabled = False
                chkAplicar.Enabled = False
                Exit For
            End If
        Next



    End Sub

    Sub Imprimir()

        Dim Total As Double = OcxImpuesto1.txtTotal.ObtenerValor
        Dim NumeroALetra As String = CSistema.NumeroALetra(OcxImpuesto1.txtTotal.ObtenerValor)

        Dim Where As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar IDTransaccion
        Where = " Where IDTransaccion =" & IDTransaccion

        'Dim dtComprobanteVenta As New DataTable
        Dim Comprobantes As String = ""
        'dtComprobanteVenta = CSistema.ExecuteToDataTable("Select distinct Comprobante from VNotaCreditoVentaAplicacion where IDTransaccion =" & IDTransaccion & " ").Copy
        'For Each oRow As DataRow In dtComprobanteVenta.Rows
        '    Dim param As New DataTable
        '    Comprobantes = Comprobantes & oRow("Comprobante").ToString & "    "
        'Next
        Comprobantes = CSistema.ExecuteScalar("Select ComprobanteImpresion from VNotaCredito where IDTransaccion =" & IDTransaccion)

        Dim Path As String = vgImpresionNotaCreditoPath
        CReporte.ImpresionNotaCredito(frm, Where, vgImpresionNotaCreditoPath, vgImpresionFacturaImpresora, NumeroALetra, CSistema.FormatoMoneda(Total, vDecimalOperacion), Comprobantes)

    End Sub

    Sub Imprimir2(Optional ByVal ImprimirDirecto As Boolean = False)
        Dim CImpresion As New CImpresion
        CImpresion.IDTransaccion = IDTransaccion
        CImpresion.IDFormularioImpresion = CSistema.RetornarValorString(CData.GetRow("Descripcion='NOTA DE CREDITO'", "FormularioImpresion")("IDFormularioImpresion").ToString)
        CImpresion.Impresora = vgImpresionFacturaImpresora
        CImpresion.Test = False
        CImpresion.Inicializar()
        CImpresion.Imprimir()
    End Sub

    Sub SeleccionarTimbrado()

        Dim frm As New frmSeleccionarTimbrado
        Try
            frm.ID = txtIDTimbrado.GetValue
            frm.IDOperacion = IDOperacion
            frm.IDSucursal = vgIDSucursal

        Catch ex As Exception

        End Try


        FGMostrarFormulario(Me, frm, "Timbrados", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Seleccionado = False Then
            Exit Sub
        End If

        txtIDTimbrado.SetValue(frm.ID)
        ObtenerInformacionPuntoVenta()

    End Sub
    Sub MostrarDecimales(ByVal IDMoneda As Integer)

        'If dgw.Columns.Count = 0 Then
        '    Exit Sub
        'End If

        If IDMoneda = 1 Then
            txtCostoUnitario.Decimales = False
            txtImporte.Decimales = False
            'Format(lvLista.Columns("Cantidad"), "##,##")
            'dgw.Columns("Interes").DefaultCellStyle.Format = "N0"
            'dgw.Columns("Impuesto").DefaultCellStyle.Format = "N0"
            'dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N0"
            'dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N0"
        Else
            txtCostoUnitario.Decimales = True
            txtImporte.Decimales = True
            'Format(lvLista.Columns("Cantidad"), "##,##.####")
            'dgw.Columns("Amortizacion").DefaultCellStyle.Format = "N2"
            'dgw.Columns("Interes").DefaultCellStyle.Format = "N2"
            'dgw.Columns("Impuesto").DefaultCellStyle.Format = "N2"
            'dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N2"
            'dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N2"
        End If
    End Sub

    Private Sub frmNotaCreditoCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
        LiberarMemoria()
    End Sub

    Private Sub frmNotaCreditoCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Enter Then

            If txtCantidad.txt.Focused = True Then
                Exit Sub
            End If

            If txtImporte.txt.Focused = True Then
                Exit Sub
            End If

            If dgw.Focused = True Then
                Exit Sub
            End If
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub frmNotaCreditoCliente_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub txtCantidad_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.TeclaPrecionada
        If txtCantidad.txt.Focused = False Then
            Exit Sub
        End If

        CalcularImporte(vDecimalOperacion, True, False)

        If e.KeyCode = Keys.Enter Then
            If vHabilitar = True Then
                CargarProducto()
            Else
                txtCostoUnitario.txt.Focus()
            End If

        End If
    End Sub

    Private Sub txtCostoUnitario_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCostoUnitario.TeclaPrecionada

        'If txtCostoUnitario.txt.Focused = False Then
        '    Exit Sub
        'End If

        CalcularImporte(False, True, False)

        If e.KeyCode = Keys.Enter Then
            If vHabilitar = True Then
                CargarProducto()
            Else
                txtImporte.txt.Focus()
            End If

        End If
    End Sub

    Private Sub txtProducto_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then

            If vConComprobantes = True Then
                txtCostoUnitario.txt.Text = txtProducto.Registro("PrecioUnitario") - txtProducto.Registro("DescuentoUnitario")
            Else
                txtCostoUnitario.txt.Text = txtProducto.Registro("Precio").ToString
            End If
            CalcularImporte(False, True, False)
            txtObservacionProducto.Focus()

        End If

    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado
        Dim oRow As DataRow = txtCliente.Registro
        Dim oRowSucursal As DataRow = txtCliente.Sucursal

        If txtCliente.SucursalSeleccionada = False Then
            txtDireccion.txt.Text = oRow("Direccion").ToString
            txtTelefono.txt.Text = oRow("Telefono").ToString
        Else
            txtDireccion.txt.Text = oRowSucursal("Direccion").ToString
            txtTelefono.txt.Text = oRowSucursal("Telefono").ToString
        End If

        If vNuevo = True Then
            CargarComprobante()
            txtFecha.txt.Focus()
        End If

    End Sub

    Private Sub cbxComprobante_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxComprobante.Leave

        If chkAplicar.Valor = True Then
            dtDetalleVenta = CSistema.ExecuteToDataTable("Select * From VDetalleVentaParaNotaCredito V Where IDTransaccion=" & cbxComprobante.GetValue & " ").Copy
        Else
            dtDetalleVenta = CSistema.ExecuteToDataTable("Select * From VDetalleVentaParaNotaCredito V Where V.Cancelado='False' And V.IDCliente =" & txtCliente.Registro("ID").ToString).Copy
        End If

        Dim dttemp As DataTable = CData.FiltrarDataTable(dtDetalleVenta, " IDTransaccion = " & cbxComprobante.GetValue)
        txtProducto.Conectar(dttemp)

    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvLista.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProductoDevolucion()
        End If

    End Sub

    Private Sub cbxPuntoExpedicion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ObtenerInformacionPuntoVenta()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If CSistema.ExecuteScalar("select count(*) from Cotizacion where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False) & "' and IDMoneda = " & OcxCotizacion1.Registro("ID")) = 0 And OcxCotizacion1.Registro("ID") > 1 Then
            MessageBox.Show("No se ha fijado cotizacion para la moneda seleccionada.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
        End If
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        MoverRegistro(e)
    End Sub

    Private Sub chkAplicar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkAplicar.PropertyChanged
        Aplica()
    End Sub

    Private Sub txtImporte_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtImporte.TeclaPrecionada
        If txtImporte.txt.Focused = False Then
            Exit Sub
        End If

        CalcularImporte(False, True, True)


        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If

    End Sub

    Private Sub rdbDescuentos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbDescuentos.CheckedChanged
        If rdbDescuentos.Checked = True Then
            ObtenerComprobanteDescuento()
            pnlDevolucion.Visible = False
            pnlDescuento.Visible = True

            If vNuevo = True Then
                If txtObservacion.GetValue = vObservacionDescuento Or txtObservacion.GetValue = vObservacionDevolucion Then
                    txtObservacion.SetValue(vObservacionDescuento)
                End If
            End If


        End If
    End Sub

    Private Sub lvListaDescuento_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvListaDescuento.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProductoDescuento()
        End If
    End Sub

    Private Sub rdbDevolucion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbDevolucion.CheckedChanged

        If rdbDevolucion.Checked = True Then
            pnlDevolucion.Visible = True
            pnlDescuento.Visible = False

            If vNuevo = True Then
                If txtObservacion.GetValue = vObservacionDescuento Or txtObservacion.GetValue = vObservacionDevolucion Then
                    txtObservacion.SetValue(vObservacionDevolucion)
                End If
            End If

        End If

    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnula.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub cbxCiudad_PropertyChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

        cbxSucursal.cbx.Text = ""
        cbxDeposito.cbx.Text = ""

        If cbxCiudad.Validar() = False Then
            Exit Sub
        End If

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal", " IDCiudad = " & cbxCiudad.GetValue), "ID", "Descripcion")
        cbxSucursal.SelectedValue(vgIDSucursal)

    End Sub

    Private Sub cbxSucursal_PropertyChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'cbxDeposito.cbx.Text = ""

        'If cbxSucursal.Validar() = False Then
        '    Exit Sub
        'End If

        ''Deposito
        'CSistema.SqlToComboBox(cbxDeposito.cbx, CData.GetTable("VDeposito", " IDSucursal = " & cbxSucursal.GetValue), "ID", "Deposito")
        'cbxDeposito.SelectedValue(vgIDDeposito)

    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Close()

    End Sub

    Private Sub lklAgregarDescuento_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklAgregarDetalleDescuento.LinkClicked
        CargarDescuentos()
    End Sub

    Private Sub LinkLabel3_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklAgregarComprobanteDescuento.LinkClicked
        CargarComprobanteDescuento()
    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarDetalleDescuento.LinkClicked
        EliminarProductoDescuento()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarComprobanteDescuento.LinkClicked
        EliminarComprobanteDescuento()
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarComprobanteDescuento()
        End If
    End Sub

    Private Sub VerDetalleToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VerDetalleToolStripMenuItem.Click

        If lvLista.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim Filtro As String
        Filtro = " ID=" & lvLista.SelectedItems(0).Index & " And IDProducto=" & lvLista.SelectedItems(0).Text & " "

        Dim dttemp As DataTable = dtDetalle.Copy
        dttemp = CData.FiltrarDataTable(dttemp, Filtro)
        CSistema.MostrarPropiedades(Me, "Detalle", "", "", dttemp)

    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        CSistema.dtToExcel(dtDetalle)
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        'If chkImprimir2.Valor = False Then
        '    Imprimir()
        'Else
        '    Imprimir2()
        'End If
        'Usar el formato directo sin checkear la opcion - JCAveiro 16022024
        Imprimir2()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Eliminar(ERP.CSistema.NUMOperacionesRegistro.DEL)
    End Sub

    Private Sub lklTimbrado_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklTimbrado.LinkClicked
        SeleccionarTimbrado()
    End Sub

    Private Sub btnAnular_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub OcxCotizacion1_CambioMoneda() Handles OcxCotizacion1.CambioMoneda
        OcxCotizacion1.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
        OcxCotizacion1.Recargar()
        OcxImpuesto1.SetIDMoneda(OcxCotizacion1.Registro("ID"))
        MostrarDecimales(OcxCotizacion1.Registro("ID"))
        vDecimalOperacion = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & OcxCotizacion1.Registro("ID"), "vMoneda")("Decimales").ToString)
        txtProducto.IDMoneda = OcxCotizacion1.Registro("ID")
        txtProducto.Cotizacion = OcxCotizacion1.Registro("Cotizacion")
        txtImporte.Decimales = vDecimalOperacion
        txtCostoUnitario.Decimales = vDecimalOperacion
        CargarComprobante()
    End Sub

    Private Sub txtFecha_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtFecha.TeclaPrecionada
        OcxCotizacion1.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
        OcxCotizacion1.Recargar()
        OcxImpuesto1.SetIDMoneda(OcxCotizacion1.Registro("ID"))
        MostrarDecimales(OcxCotizacion1.Registro("ID"))
        vDecimalOperacion = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & OcxCotizacion1.Registro("ID"), "vMoneda")("Decimales").ToString)
        txtProducto.IDMoneda = OcxCotizacion1.Registro("ID")
        txtProducto.Cotizacion = OcxCotizacion1.Registro("Cotizacion")
        txtImporte.Decimales = vDecimalOperacion
        txtCostoUnitario.Decimales = vDecimalOperacion
        CargarComprobante()
    End Sub

    Private Sub btnVerPedidoNC_Click(sender As Object, e As EventArgs) Handles btnVerPedidoNC.Click
        Dim vIDTransaccionPedido As Integer = CSistema.ExecuteScalar("Select IDTransaccionPedido from PedidoNCNotaCredito where IDTransaccionNotaCredito = " & IDTransaccion)
        Dim dtPedido As DataTable = CSistema.ExecuteToDataTable("Select * from PedidoNotaCredito where IDTransaccion = " & vIDTransaccionPedido)
        Dim vNumero As Integer = dtPedido.Rows(0)("Numero")
        Dim vIDSucursal As Integer = dtPedido.Rows(0)("IDSucursal")
        Dim frm As New frmPedidoNotaCredito

        frm.Show()
        frm.EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
        frm.cbxSucursal.cbx.SelectedValue = vIDSucursal
        frm.txtID.txt.Text = vNumero
        frm.CargarOperacion()
        If rdbDescuentos.Checked Then
            frm.ListarComprobanteDescuento()
            frm.ListarDetalleDescuento()
        Else
            frm.ListarDetalle()
        End If



    End Sub

    Private Sub btnRecalcularSaldoNC_Click(sender As Object, e As EventArgs) Handles btnRecalcularSaldoNC.Click
        Try
            CSistema.ExecuteNonQuery("exec spRecalcularNotaCredito " & IDTransaccion)
            MessageBox.Show("Recalculo Realizado", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show("Ha ocurrido un error, comuniquese con el administrador del sistema.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    Private Sub lklRecepcionDocumento_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lklRecepcionDocumento.LinkClicked
        Dim frm As New frmRecepcionDocumento
        frm.IDTransaccion = IDTransaccion
        frm.Comprobante = Comprobante
        frm.Show()
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNotaCreditoCargarDescuento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.lblDescuento = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.Cancelar = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.txtDescuento = New ERP.ocxTXTProducto()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(9, 52)
        Me.lblObservacion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(92, 17)
        Me.lblObservacion.TabIndex = 2
        Me.lblObservacion.Text = "Observacion:"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(9, 85)
        Me.lblImporte.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(59, 17)
        Me.lblImporte.TabIndex = 4
        Me.lblImporte.Text = "Importe:"
        '
        'lblDescuento
        '
        Me.lblDescuento.AutoSize = True
        Me.lblDescuento.Location = New System.Drawing.Point(9, 20)
        Me.lblDescuento.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDescuento.Name = "lblDescuento"
        Me.lblDescuento.Size = New System.Drawing.Size(80, 17)
        Me.lblDescuento.TabIndex = 0
        Me.lblDescuento.Text = "Descuento:"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(267, 129)
        Me.btnAceptar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(100, 28)
        Me.btnAceptar.TabIndex = 6
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'Cancelar
        '
        Me.Cancelar.Location = New System.Drawing.Point(375, 129)
        Me.Cancelar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Cancelar.Name = "Cancelar"
        Me.Cancelar.Size = New System.Drawing.Size(100, 28)
        Me.Cancelar.TabIndex = 7
        Me.Cancelar.Text = "Cancelar"
        Me.Cancelar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 177)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(484, 22)
        Me.StatusStrip1.TabIndex = 8
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'txtDescuento
        '
        Me.txtDescuento.AlturaMaxima = 125
        Me.txtDescuento.ColumnasNumericas = Nothing
        Me.txtDescuento.Consulta = Nothing
        Me.txtDescuento.ControlarExistencia = False
        Me.txtDescuento.ControlarReservas = False
        Me.txtDescuento.dtDescuento = Nothing
        Me.txtDescuento.IDCliente = 0
        Me.txtDescuento.IDClienteSucursal = 0
        Me.txtDescuento.IDDeposito = 0
        Me.txtDescuento.IDListaPrecio = 0
        Me.txtDescuento.IDSucursal = 0
        Me.txtDescuento.Location = New System.Drawing.Point(110, 12)
        Me.txtDescuento.Margin = New System.Windows.Forms.Padding(5)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Precios = Nothing
        Me.txtDescuento.Registro = Nothing
        Me.txtDescuento.Seleccionado = False
        Me.txtDescuento.Size = New System.Drawing.Size(364, 25)
        Me.txtDescuento.SoloLectura = False
        Me.txtDescuento.TabIndex = 1
        Me.txtDescuento.TieneDescuento = False
        Me.txtDescuento.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDescuento.Venta = False
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(110, 50)
        Me.txtObservacion.Margin = New System.Windows.Forms.Padding(5)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(360, 26)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 3
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = False
        Me.txtImporte.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporte.Location = New System.Drawing.Point(110, 86)
        Me.txtImporte.Margin = New System.Windows.Forms.Padding(5)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(123, 26)
        Me.txtImporte.SoloLectura = False
        Me.txtImporte.TabIndex = 5
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'frmNotaCreditoCargarDescuento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(484, 199)
        Me.Controls.Add(Me.txtDescuento)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.Cancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.txtObservacion)
        Me.Controls.Add(Me.lblObservacion)
        Me.Controls.Add(Me.lblImporte)
        Me.Controls.Add(Me.lblDescuento)
        Me.Controls.Add(Me.txtImporte)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "frmNotaCreditoCargarDescuento"
        Me.Text = "frmNotaCreditoCargarDescuento"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents txtDescuento As ERP.ocxTXTProducto
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents lblDescuento As System.Windows.Forms.Label
    Friend WithEvents txtImporte As ERP.ocxTXTNumeric
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents Cancelar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
End Class

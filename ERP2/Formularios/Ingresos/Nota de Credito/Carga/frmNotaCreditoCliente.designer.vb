﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNotaCreditoCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNotaCreditoCliente))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.colID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colReferencia = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colDescripcion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colComprobante = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colIVA = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colCantidad = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colPrecioUnitario = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColTotal = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.VerDetalleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportarAExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.lblCostoUnitario = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblObservacionProducto = New System.Windows.Forms.Label()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.cbxComprobante = New ERP.ocxCBX()
        Me.txtCostoUnitario = New ERP.ocxTXTNumeric()
        Me.lblProducto = New System.Windows.Forms.Label()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.txtObservacionProducto = New ERP.ocxTXTString()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.lblRestoTimbrado = New System.Windows.Forms.Label()
        Me.lblCiudad = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.gbxComprobante = New System.Windows.Forms.GroupBox()
        Me.cbxComprobanteImpresion = New ERP.ocxCBX()
        Me.TxtComprobanteImpresion = New ERP.ocxTXTString()
        Me.chkImprimir2 = New ERP.ocxCHK()
        Me.txtIDTimbrado = New ERP.ocxTXTString()
        Me.lklTimbrado = New System.Windows.Forms.LinkLabel()
        Me.txtTimbrado = New ERP.ocxTXTString()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtRestoTimbrado = New ERP.ocxTXTNumeric()
        Me.txtVencimientoTimbrado = New ERP.ocxTXTDate()
        Me.lblVenimientoTimbrado = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.txtNroComprobante = New ERP.ocxTXTString()
        Me.txtTalonario = New ERP.ocxTXTString()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.lblTalonario = New System.Windows.Forms.Label()
        Me.txtReferenciaSucursal = New ERP.ocxTXTString()
        Me.txtReferenciaTerminal = New ERP.ocxTXTString()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.btnAnula = New System.Windows.Forms.Button()
        Me.rdbDescuentos = New System.Windows.Forms.RadioButton()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.rdbDevolucion = New System.Windows.Forms.RadioButton()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.txtTelefono = New ERP.ocxTXTString()
        Me.lblTelefono = New System.Windows.Forms.Label()
        Me.txtDireccion = New ERP.ocxTXTString()
        Me.lblDireccion = New System.Windows.Forms.Label()
        Me.chkAplicar = New ERP.ocxCHK()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblDeposito = New System.Windows.Forms.Label()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.OcxCotizacion1 = New ERP.ocxCotizacion()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.OcxImpuesto1 = New ERP.ocxImpuesto()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.pnlDescuento = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.lvListaDescuento = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCondicion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colVencimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCobrado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDescontado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCancelar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtTotalDescuento = New ERP.ocxTXTNumeric()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtCantidadCobrado = New ERP.ocxTXTNumeric()
        Me.txtDescontados = New ERP.ocxTXTNumeric()
        Me.lblTotalCobrado = New System.Windows.Forms.Label()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.lblDeudaTotal = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lklAgregarComprobanteDescuento = New System.Windows.Forms.LinkLabel()
        Me.lklEliminarComprobanteDescuento = New System.Windows.Forms.LinkLabel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lklAgregarDetalleDescuento = New System.Windows.Forms.LinkLabel()
        Me.lklEliminarDetalleDescuento = New System.Windows.Forms.LinkLabel()
        Me.pnlDevolucion = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lklRecepcionDocumento = New System.Windows.Forms.LinkLabel()
        Me.btnRecalcularSaldoNC = New System.Windows.Forms.Button()
        Me.btnVerPedidoNC = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.gbxComprobante.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.gbxCabecera.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.pnlDescuento.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.pnlDevolucion.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 6
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 59.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.txtProducto, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lvLista, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.lblImporte, 5, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblCostoUnitario, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblObservacionProducto, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblCantidad, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.cbxComprobante, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtCostoUnitario, 4, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lblProducto, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtImporte, 5, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtObservacionProducto, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtCantidad, 3, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 13.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(408, 262)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 260
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(118, 16)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(57, 21)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 3
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'lvLista
        '
        Me.lvLista.BackColor = System.Drawing.Color.White
        Me.lvLista.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colID, Me.colReferencia, Me.colDescripcion, Me.colComprobante, Me.colIVA, Me.colCantidad, Me.colPrecioUnitario, Me.ColTotal})
        Me.TableLayoutPanel1.SetColumnSpan(Me.lvLista, 6)
        Me.lvLista.ContextMenuStrip = Me.ContextMenuStrip1
        Me.lvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvLista.FullRowSelect = True
        Me.lvLista.GridLines = True
        Me.lvLista.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvLista.HideSelection = False
        Me.lvLista.Location = New System.Drawing.Point(3, 43)
        Me.lvLista.MultiSelect = False
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(402, 216)
        Me.lvLista.TabIndex = 12
        Me.lvLista.UseCompatibleStateImageBehavior = False
        Me.lvLista.View = System.Windows.Forms.View.Details
        '
        'colID
        '
        Me.colID.Text = "ID"
        Me.colID.Width = 0
        '
        'colReferencia
        '
        Me.colReferencia.Text = "Referencia"
        Me.colReferencia.Width = 67
        '
        'colDescripcion
        '
        Me.colDescripcion.Text = "Descripcion"
        Me.colDescripcion.Width = 333
        '
        'colComprobante
        '
        Me.colComprobante.Text = "Comprobante"
        Me.colComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.colComprobante.Width = 90
        '
        'colIVA
        '
        Me.colIVA.Text = "IVA"
        Me.colIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.colIVA.Width = 55
        '
        'colCantidad
        '
        Me.colCantidad.Text = "Cantidad"
        Me.colCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colCantidad.Width = 55
        '
        'colPrecioUnitario
        '
        Me.colPrecioUnitario.Text = "Pre. Uni."
        Me.colPrecioUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colPrecioUnitario.Width = 73
        '
        'ColTotal
        '
        Me.ColTotal.Text = "Total"
        Me.ColTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColTotal.Width = 70
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VerDetalleToolStripMenuItem, Me.ExportarAExcelToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(158, 48)
        '
        'VerDetalleToolStripMenuItem
        '
        Me.VerDetalleToolStripMenuItem.Name = "VerDetalleToolStripMenuItem"
        Me.VerDetalleToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.VerDetalleToolStripMenuItem.Text = "Ver Detalle"
        '
        'ExportarAExcelToolStripMenuItem
        '
        Me.ExportarAExcelToolStripMenuItem.Name = "ExportarAExcelToolStripMenuItem"
        Me.ExportarAExcelToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.ExportarAExcelToolStripMenuItem.Text = "Exportar a Excel"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(335, 0)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(45, 13)
        Me.lblImporte.TabIndex = 10
        Me.lblImporte.Text = "Importe:"
        '
        'lblCostoUnitario
        '
        Me.lblCostoUnitario.AutoSize = True
        Me.lblCostoUnitario.Location = New System.Drawing.Point(282, 0)
        Me.lblCostoUnitario.Name = "lblCostoUnitario"
        Me.lblCostoUnitario.Size = New System.Drawing.Size(37, 13)
        Me.lblCostoUnitario.TabIndex = 8
        Me.lblCostoUnitario.Text = "Costo Uni.:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Compro."
        '
        'lblObservacionProducto
        '
        Me.lblObservacionProducto.AutoSize = True
        Me.lblObservacionProducto.Location = New System.Drawing.Point(181, 0)
        Me.lblObservacionProducto.Name = "lblObservacionProducto"
        Me.lblObservacionProducto.Size = New System.Drawing.Size(35, 13)
        Me.lblObservacionProducto.TabIndex = 4
        Me.lblObservacionProducto.Text = "Observacion:"
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(223, 0)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidad.TabIndex = 6
        Me.lblCantidad.Text = "Cantidad:"
        '
        'cbxComprobante
        '
        Me.cbxComprobante.CampoWhere = Nothing
        Me.cbxComprobante.CargarUnaSolaVez = False
        Me.cbxComprobante.DataDisplayMember = Nothing
        Me.cbxComprobante.DataFilter = Nothing
        Me.cbxComprobante.DataOrderBy = Nothing
        Me.cbxComprobante.DataSource = Nothing
        Me.cbxComprobante.DataValueMember = Nothing
        Me.cbxComprobante.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cbxComprobante.dtSeleccionado = Nothing
        Me.cbxComprobante.FormABM = Nothing
        Me.cbxComprobante.Indicaciones = Nothing
        Me.cbxComprobante.Location = New System.Drawing.Point(3, 16)
        Me.cbxComprobante.Name = "cbxComprobante"
        Me.cbxComprobante.SeleccionMultiple = False
        Me.cbxComprobante.SeleccionObligatoria = True
        Me.cbxComprobante.Size = New System.Drawing.Size(109, 21)
        Me.cbxComprobante.SoloLectura = False
        Me.cbxComprobante.TabIndex = 1
        Me.cbxComprobante.Texto = ""
        '
        'txtCostoUnitario
        '
        Me.txtCostoUnitario.Color = System.Drawing.Color.Empty
        Me.txtCostoUnitario.Decimales = False
        Me.txtCostoUnitario.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCostoUnitario.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCostoUnitario.Location = New System.Drawing.Point(282, 16)
        Me.txtCostoUnitario.Name = "txtCostoUnitario"
        Me.txtCostoUnitario.Size = New System.Drawing.Size(47, 21)
        Me.txtCostoUnitario.SoloLectura = False
        Me.txtCostoUnitario.TabIndex = 9
        Me.txtCostoUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCostoUnitario.Texto = "0"
        '
        'lblProducto
        '
        Me.lblProducto.AutoSize = True
        Me.lblProducto.Location = New System.Drawing.Point(118, 0)
        Me.lblProducto.Name = "lblProducto"
        Me.lblProducto.Size = New System.Drawing.Size(53, 13)
        Me.lblProducto.TabIndex = 2
        Me.lblProducto.Text = "Producto:"
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = False
        Me.txtImporte.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtImporte.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporte.Location = New System.Drawing.Point(335, 16)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(70, 21)
        Me.txtImporte.SoloLectura = False
        Me.txtImporte.TabIndex = 11
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'txtObservacionProducto
        '
        Me.txtObservacionProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacionProducto.Color = System.Drawing.Color.Empty
        Me.txtObservacionProducto.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtObservacionProducto.Indicaciones = Nothing
        Me.txtObservacionProducto.Location = New System.Drawing.Point(181, 16)
        Me.txtObservacionProducto.Multilinea = False
        Me.txtObservacionProducto.Name = "txtObservacionProducto"
        Me.txtObservacionProducto.Size = New System.Drawing.Size(36, 21)
        Me.txtObservacionProducto.SoloLectura = False
        Me.txtObservacionProducto.TabIndex = 5
        Me.txtObservacionProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacionProducto.Texto = ""
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCantidad.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCantidad.Location = New System.Drawing.Point(223, 16)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(53, 21)
        Me.txtCantidad.SoloLectura = False
        Me.txtCantidad.TabIndex = 7
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'lblRestoTimbrado
        '
        Me.lblRestoTimbrado.AutoSize = True
        Me.lblRestoTimbrado.Location = New System.Drawing.Point(251, 39)
        Me.lblRestoTimbrado.Name = "lblRestoTimbrado"
        Me.lblRestoTimbrado.Size = New System.Drawing.Size(138, 13)
        Me.lblRestoTimbrado.TabIndex = 15
        Me.lblRestoTimbrado.Text = "Nro. restantes en Talonario:"
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(275, 15)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(28, 13)
        Me.lblCiudad.TabIndex = 2
        Me.lblCiudad.Text = "Ciu.:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(357, 15)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(32, 13)
        Me.lblSucursal.TabIndex = 4
        Me.lblSucursal.Text = "Suc.:"
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(3, 29)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(315, 20)
        Me.flpAnuladoPor.TabIndex = 1
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'gbxComprobante
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.gbxComprobante, 2)
        Me.gbxComprobante.Controls.Add(Me.cbxComprobanteImpresion)
        Me.gbxComprobante.Controls.Add(Me.TxtComprobanteImpresion)
        Me.gbxComprobante.Controls.Add(Me.chkImprimir2)
        Me.gbxComprobante.Controls.Add(Me.txtIDTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lklTimbrado)
        Me.gbxComprobante.Controls.Add(Me.txtTimbrado)
        Me.gbxComprobante.Controls.Add(Me.cbxSucursal)
        Me.gbxComprobante.Controls.Add(Me.cbxCiudad)
        Me.gbxComprobante.Controls.Add(Me.txtRestoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lblRestoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lblCiudad)
        Me.gbxComprobante.Controls.Add(Me.lblSucursal)
        Me.gbxComprobante.Controls.Add(Me.txtVencimientoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lblVenimientoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lblComprobante)
        Me.gbxComprobante.Controls.Add(Me.txtNroComprobante)
        Me.gbxComprobante.Controls.Add(Me.txtTalonario)
        Me.gbxComprobante.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxComprobante.Controls.Add(Me.lblTalonario)
        Me.gbxComprobante.Controls.Add(Me.txtReferenciaSucursal)
        Me.gbxComprobante.Controls.Add(Me.txtReferenciaTerminal)
        Me.gbxComprobante.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxComprobante.Location = New System.Drawing.Point(0, 0)
        Me.gbxComprobante.Margin = New System.Windows.Forms.Padding(0)
        Me.gbxComprobante.Name = "gbxComprobante"
        Me.gbxComprobante.Size = New System.Drawing.Size(789, 62)
        Me.gbxComprobante.TabIndex = 0
        Me.gbxComprobante.TabStop = False
        '
        'cbxComprobanteImpresion
        '
        Me.cbxComprobanteImpresion.CampoWhere = Nothing
        Me.cbxComprobanteImpresion.CargarUnaSolaVez = False
        Me.cbxComprobanteImpresion.DataDisplayMember = Nothing
        Me.cbxComprobanteImpresion.DataFilter = Nothing
        Me.cbxComprobanteImpresion.DataOrderBy = Nothing
        Me.cbxComprobanteImpresion.DataSource = Nothing
        Me.cbxComprobanteImpresion.DataValueMember = Nothing
        Me.cbxComprobanteImpresion.dtSeleccionado = Nothing
        Me.cbxComprobanteImpresion.FormABM = Nothing
        Me.cbxComprobanteImpresion.Indicaciones = Nothing
        Me.cbxComprobanteImpresion.Location = New System.Drawing.Point(588, 35)
        Me.cbxComprobanteImpresion.Name = "cbxComprobanteImpresion"
        Me.cbxComprobanteImpresion.SeleccionMultiple = False
        Me.cbxComprobanteImpresion.SeleccionObligatoria = False
        Me.cbxComprobanteImpresion.Size = New System.Drawing.Size(195, 21)
        Me.cbxComprobanteImpresion.SoloLectura = False
        Me.cbxComprobanteImpresion.TabIndex = 54
        Me.cbxComprobanteImpresion.Texto = ""
        '
        'TxtComprobanteImpresion
        '
        Me.TxtComprobanteImpresion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtComprobanteImpresion.Color = System.Drawing.Color.Empty
        Me.TxtComprobanteImpresion.Enabled = False
        Me.TxtComprobanteImpresion.Indicaciones = Nothing
        Me.TxtComprobanteImpresion.Location = New System.Drawing.Point(588, 35)
        Me.TxtComprobanteImpresion.Multilinea = False
        Me.TxtComprobanteImpresion.Name = "TxtComprobanteImpresion"
        Me.TxtComprobanteImpresion.Size = New System.Drawing.Size(195, 21)
        Me.TxtComprobanteImpresion.SoloLectura = False
        Me.TxtComprobanteImpresion.TabIndex = 26
        Me.TxtComprobanteImpresion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TxtComprobanteImpresion.Texto = ""
        '
        'chkImprimir2
        '
        Me.chkImprimir2.BackColor = System.Drawing.Color.Transparent
        Me.chkImprimir2.Color = System.Drawing.Color.Empty
        Me.chkImprimir2.Location = New System.Drawing.Point(515, 35)
        Me.chkImprimir2.Name = "chkImprimir2"
        Me.chkImprimir2.Size = New System.Drawing.Size(71, 21)
        Me.chkImprimir2.SoloLectura = False
        Me.chkImprimir2.TabIndex = 25
        Me.chkImprimir2.Texto = "Imprimir 2"
        Me.chkImprimir2.Valor = False
        '
        'txtIDTimbrado
        '
        Me.txtIDTimbrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIDTimbrado.Color = System.Drawing.Color.Beige
        Me.txtIDTimbrado.Indicaciones = Nothing
        Me.txtIDTimbrado.Location = New System.Drawing.Point(69, 11)
        Me.txtIDTimbrado.Multilinea = False
        Me.txtIDTimbrado.Name = "txtIDTimbrado"
        Me.txtIDTimbrado.Size = New System.Drawing.Size(36, 21)
        Me.txtIDTimbrado.SoloLectura = True
        Me.txtIDTimbrado.TabIndex = 1
        Me.txtIDTimbrado.TabStop = False
        Me.txtIDTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtIDTimbrado.Texto = ""
        Me.txtIDTimbrado.Visible = False
        '
        'lklTimbrado
        '
        Me.lklTimbrado.AutoSize = True
        Me.lklTimbrado.Location = New System.Drawing.Point(6, 15)
        Me.lklTimbrado.Name = "lklTimbrado"
        Me.lklTimbrado.Size = New System.Drawing.Size(54, 13)
        Me.lklTimbrado.TabIndex = 0
        Me.lklTimbrado.TabStop = True
        Me.lklTimbrado.Text = "Timbrado:"
        '
        'txtTimbrado
        '
        Me.txtTimbrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTimbrado.Color = System.Drawing.Color.Beige
        Me.txtTimbrado.Indicaciones = Nothing
        Me.txtTimbrado.Location = New System.Drawing.Point(68, 11)
        Me.txtTimbrado.Multilinea = False
        Me.txtTimbrado.Name = "txtTimbrado"
        Me.txtTimbrado.Size = New System.Drawing.Size(178, 21)
        Me.txtTimbrado.SoloLectura = True
        Me.txtTimbrado.TabIndex = 2
        Me.txtTimbrado.TabStop = False
        Me.txtTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTimbrado.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Codigo"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(390, 11)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(117, 21)
        Me.cbxSucursal.SoloLectura = True
        Me.cbxSucursal.TabIndex = 5
        Me.cbxSucursal.TabStop = False
        Me.cbxSucursal.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = "Codigo"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = "VCiudad"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(302, 11)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(54, 21)
        Me.cbxCiudad.SoloLectura = True
        Me.cbxCiudad.TabIndex = 3
        Me.cbxCiudad.TabStop = False
        Me.cbxCiudad.Texto = ""
        '
        'txtRestoTimbrado
        '
        Me.txtRestoTimbrado.Color = System.Drawing.Color.Beige
        Me.txtRestoTimbrado.Decimales = True
        Me.txtRestoTimbrado.Indicaciones = Nothing
        Me.txtRestoTimbrado.Location = New System.Drawing.Point(389, 34)
        Me.txtRestoTimbrado.Name = "txtRestoTimbrado"
        Me.txtRestoTimbrado.Size = New System.Drawing.Size(117, 22)
        Me.txtRestoTimbrado.SoloLectura = True
        Me.txtRestoTimbrado.TabIndex = 16
        Me.txtRestoTimbrado.TabStop = False
        Me.txtRestoTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRestoTimbrado.Texto = "0"
        '
        'txtVencimientoTimbrado
        '
        Me.txtVencimientoTimbrado.AñoFecha = 0
        Me.txtVencimientoTimbrado.Color = System.Drawing.Color.Beige
        Me.txtVencimientoTimbrado.Fecha = New Date(2013, 5, 3, 16, 59, 17, 669)
        Me.txtVencimientoTimbrado.Location = New System.Drawing.Point(172, 35)
        Me.txtVencimientoTimbrado.MesFecha = 0
        Me.txtVencimientoTimbrado.Name = "txtVencimientoTimbrado"
        Me.txtVencimientoTimbrado.PermitirNulo = False
        Me.txtVencimientoTimbrado.Size = New System.Drawing.Size(74, 20)
        Me.txtVencimientoTimbrado.SoloLectura = True
        Me.txtVencimientoTimbrado.TabIndex = 14
        Me.txtVencimientoTimbrado.TabStop = False
        '
        'lblVenimientoTimbrado
        '
        Me.lblVenimientoTimbrado.AutoSize = True
        Me.lblVenimientoTimbrado.Location = New System.Drawing.Point(104, 39)
        Me.lblVenimientoTimbrado.Name = "lblVenimientoTimbrado"
        Me.lblVenimientoTimbrado.Size = New System.Drawing.Size(68, 13)
        Me.lblVenimientoTimbrado.TabIndex = 13
        Me.lblVenimientoTimbrado.Text = "Vencimiento:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(512, 15)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(55, 13)
        Me.lblComprobante.TabIndex = 6
        Me.lblComprobante.Text = "Comprob.:"
        '
        'txtNroComprobante
        '
        Me.txtNroComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroComprobante.Color = System.Drawing.Color.Empty
        Me.txtNroComprobante.Indicaciones = Nothing
        Me.txtNroComprobante.Location = New System.Drawing.Point(680, 11)
        Me.txtNroComprobante.Multilinea = False
        Me.txtNroComprobante.Name = "txtNroComprobante"
        Me.txtNroComprobante.Size = New System.Drawing.Size(103, 21)
        Me.txtNroComprobante.SoloLectura = False
        Me.txtNroComprobante.TabIndex = 10
        Me.txtNroComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNroComprobante.Texto = ""
        '
        'txtTalonario
        '
        Me.txtTalonario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTalonario.Color = System.Drawing.Color.Beige
        Me.txtTalonario.Indicaciones = Nothing
        Me.txtTalonario.Location = New System.Drawing.Point(68, 35)
        Me.txtTalonario.Multilinea = False
        Me.txtTalonario.Name = "txtTalonario"
        Me.txtTalonario.Size = New System.Drawing.Size(36, 21)
        Me.txtTalonario.SoloLectura = True
        Me.txtTalonario.TabIndex = 12
        Me.txtTalonario.TabStop = False
        Me.txtTalonario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTalonario.Texto = ""
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Codigo"
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(569, 11)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(55, 21)
        Me.cbxTipoComprobante.SoloLectura = True
        Me.cbxTipoComprobante.TabIndex = 7
        Me.cbxTipoComprobante.TabStop = False
        Me.cbxTipoComprobante.Texto = ""
        '
        'lblTalonario
        '
        Me.lblTalonario.AutoSize = True
        Me.lblTalonario.Location = New System.Drawing.Point(9, 39)
        Me.lblTalonario.Name = "lblTalonario"
        Me.lblTalonario.Size = New System.Drawing.Size(54, 13)
        Me.lblTalonario.TabIndex = 11
        Me.lblTalonario.Text = "Talonario:"
        '
        'txtReferenciaSucursal
        '
        Me.txtReferenciaSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferenciaSucursal.Color = System.Drawing.Color.Empty
        Me.txtReferenciaSucursal.Indicaciones = Nothing
        Me.txtReferenciaSucursal.Location = New System.Drawing.Point(624, 11)
        Me.txtReferenciaSucursal.Multilinea = False
        Me.txtReferenciaSucursal.Name = "txtReferenciaSucursal"
        Me.txtReferenciaSucursal.Size = New System.Drawing.Size(28, 21)
        Me.txtReferenciaSucursal.SoloLectura = True
        Me.txtReferenciaSucursal.TabIndex = 8
        Me.txtReferenciaSucursal.TabStop = False
        Me.txtReferenciaSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferenciaSucursal.Texto = ""
        '
        'txtReferenciaTerminal
        '
        Me.txtReferenciaTerminal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferenciaTerminal.Color = System.Drawing.Color.Empty
        Me.txtReferenciaTerminal.Indicaciones = Nothing
        Me.txtReferenciaTerminal.Location = New System.Drawing.Point(652, 11)
        Me.txtReferenciaTerminal.Multilinea = False
        Me.txtReferenciaTerminal.Name = "txtReferenciaTerminal"
        Me.txtReferenciaTerminal.Size = New System.Drawing.Size(28, 21)
        Me.txtReferenciaTerminal.SoloLectura = True
        Me.txtReferenciaTerminal.TabIndex = 9
        Me.txtReferenciaTerminal.TabStop = False
        Me.txtReferenciaTerminal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferenciaTerminal.Texto = ""
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(3, 3)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(277, 20)
        Me.flpRegistradoPor.TabIndex = 0
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'btnAnula
        '
        Me.btnAnula.Location = New System.Drawing.Point(-94, -558)
        Me.btnAnula.Name = "btnAnula"
        Me.btnAnula.Size = New System.Drawing.Size(75, 23)
        Me.btnAnula.TabIndex = 2
        Me.btnAnula.Text = "Anular"
        Me.btnAnula.UseVisualStyleBackColor = True
        '
        'rdbDescuentos
        '
        Me.rdbDescuentos.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdbDescuentos.Cursor = System.Windows.Forms.Cursors.Hand
        Me.rdbDescuentos.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.rdbDescuentos.FlatAppearance.CheckedBackColor = System.Drawing.Color.MintCream
        Me.rdbDescuentos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.rdbDescuentos.Location = New System.Drawing.Point(542, 69)
        Me.rdbDescuentos.Name = "rdbDescuentos"
        Me.rdbDescuentos.Size = New System.Drawing.Size(71, 22)
        Me.rdbDescuentos.TabIndex = 11
        Me.rdbDescuentos.Text = "Descuento"
        Me.rdbDescuentos.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.rdbDescuentos.UseVisualStyleBackColor = True
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(702, 2)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 7
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'rdbDevolucion
        '
        Me.rdbDevolucion.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdbDevolucion.Checked = True
        Me.rdbDevolucion.Cursor = System.Windows.Forms.Cursors.Hand
        Me.rdbDevolucion.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.rdbDevolucion.FlatAppearance.CheckedBackColor = System.Drawing.Color.MintCream
        Me.rdbDevolucion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.rdbDevolucion.Location = New System.Drawing.Point(468, 69)
        Me.rdbDevolucion.Name = "rdbDevolucion"
        Me.rdbDevolucion.Size = New System.Drawing.Size(73, 22)
        Me.rdbDevolucion.TabIndex = 10
        Me.rdbDevolucion.TabStop = True
        Me.rdbDevolucion.Text = "Devolucion"
        Me.rdbDevolucion.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.rdbDevolucion.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(588, 2)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 6
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'gbxCabecera
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.gbxCabecera, 2)
        Me.gbxCabecera.Controls.Add(Me.txtTelefono)
        Me.gbxCabecera.Controls.Add(Me.lblTelefono)
        Me.gbxCabecera.Controls.Add(Me.lblDireccion)
        Me.gbxCabecera.Controls.Add(Me.chkAplicar)
        Me.gbxCabecera.Controls.Add(Me.txtCliente)
        Me.gbxCabecera.Controls.Add(Me.Label7)
        Me.gbxCabecera.Controls.Add(Me.rdbDescuentos)
        Me.gbxCabecera.Controls.Add(Me.rdbDevolucion)
        Me.gbxCabecera.Controls.Add(Me.cbxDeposito)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblDeposito)
        Me.gbxCabecera.Controls.Add(Me.lblCliente)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.OcxCotizacion1)
        Me.gbxCabecera.Controls.Add(Me.Label4)
        Me.gbxCabecera.Controls.Add(Me.txtDireccion)
        Me.gbxCabecera.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxCabecera.Location = New System.Drawing.Point(0, 62)
        Me.gbxCabecera.Margin = New System.Windows.Forms.Padding(0)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(789, 125)
        Me.gbxCabecera.TabIndex = 1
        Me.gbxCabecera.TabStop = False
        '
        'txtTelefono
        '
        Me.txtTelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefono.Color = System.Drawing.Color.Beige
        Me.txtTelefono.Indicaciones = Nothing
        Me.txtTelefono.Location = New System.Drawing.Point(688, 44)
        Me.txtTelefono.Multilinea = False
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(94, 21)
        Me.txtTelefono.SoloLectura = True
        Me.txtTelefono.TabIndex = 49
        Me.txtTelefono.TabStop = False
        Me.txtTelefono.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTelefono.Texto = ""
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Location = New System.Drawing.Point(665, 48)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(25, 13)
        Me.lblTelefono.TabIndex = 48
        Me.lblTelefono.Text = "Tel:"
        '
        'txtDireccion
        '
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Color = System.Drawing.Color.Beige
        Me.txtDireccion.Indicaciones = Nothing
        Me.txtDireccion.Location = New System.Drawing.Point(75, 44)
        Me.txtDireccion.Multilinea = False
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(584, 21)
        Me.txtDireccion.SoloLectura = True
        Me.txtDireccion.TabIndex = 47
        Me.txtDireccion.TabStop = False
        Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccion.Texto = ""
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.Location = New System.Drawing.Point(4, 48)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(55, 13)
        Me.lblDireccion.TabIndex = 46
        Me.lblDireccion.Text = "Direccion:"
        '
        'chkAplicar
        '
        Me.chkAplicar.BackColor = System.Drawing.Color.Transparent
        Me.chkAplicar.Color = System.Drawing.Color.Empty
        Me.chkAplicar.Location = New System.Drawing.Point(769, 74)
        Me.chkAplicar.Name = "chkAplicar"
        Me.chkAplicar.Size = New System.Drawing.Size(17, 12)
        Me.chkAplicar.SoloLectura = False
        Me.chkAplicar.TabIndex = 13
        Me.chkAplicar.Texto = ""
        Me.chkAplicar.Valor = False
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 61
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.ControlCorto = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(75, 14)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(588, 24)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(614, 74)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(149, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Dejar pendiente de Aplicacion"
        Me.ToolTip1.SetToolTip(Me.Label7, resources.GetString("Label7.ToolTip"))
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = Nothing
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = ""
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = Nothing
        Me.cbxDeposito.DataSource = ""
        Me.cbxDeposito.DataValueMember = ""
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(76, 70)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = True
        Me.cbxDeposito.Size = New System.Drawing.Size(201, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 5
        Me.cbxDeposito.Texto = ""
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(283, 74)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 6
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblDeposito
        '
        Me.lblDeposito.AutoSize = True
        Me.lblDeposito.Location = New System.Drawing.Point(5, 74)
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(52, 13)
        Me.lblDeposito.TabIndex = 4
        Me.lblDeposito.Text = "Deposito:"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(5, 20)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(42, 13)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Text = "Cliente:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(76, 96)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(707, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 15
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 5, 3, 16, 59, 18, 132)
        Me.txtFecha.Location = New System.Drawing.Point(709, 14)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 3
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(669, 18)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Text = "Fecha:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(5, 99)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 14
        Me.lblObservacion.Text = "Observacion:"
        '
        'OcxCotizacion1
        '
        Me.OcxCotizacion1.dt = Nothing
        Me.OcxCotizacion1.FiltroFecha = Nothing
        Me.OcxCotizacion1.Location = New System.Drawing.Point(329, 67)
        Me.OcxCotizacion1.Name = "OcxCotizacion1"
        Me.OcxCotizacion1.Registro = Nothing
        Me.OcxCotizacion1.Saltar = False
        Me.OcxCotizacion1.Seleccionado = True
        Me.OcxCotizacion1.Size = New System.Drawing.Size(139, 28)
        Me.OcxCotizacion1.SoloLectura = False
        Me.OcxCotizacion1.TabIndex = 37
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(437, 74)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Tipo:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 623)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(789, 22)
        Me.StatusStrip1.TabIndex = 0
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(511, 2)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 5
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(434, 2)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 4
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(357, 2)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 3
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(126, 2)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 1
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(3, 2)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(120, 23)
        Me.btnBusquedaAvanzada.TabIndex = 0
        Me.btnBusquedaAvanzada.Text = "&Busqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 436.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.Panel1, 0, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.gbxComprobante, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.gbxCabecera, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.OcxImpuesto1, 1, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.GroupBox1, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel3, 0, 3)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 6
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 53.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(789, 623)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'Panel1
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.Panel1, 2)
        Me.Panel1.Controls.Add(Me.btnAnular)
        Me.Panel1.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Panel1.Controls.Add(Me.btnImprimir)
        Me.Panel1.Controls.Add(Me.btnAsiento)
        Me.Panel1.Controls.Add(Me.btnAnula)
        Me.Panel1.Controls.Add(Me.btnNuevo)
        Me.Panel1.Controls.Add(Me.btnSalir)
        Me.Panel1.Controls.Add(Me.btnGuardar)
        Me.Panel1.Controls.Add(Me.btnCancelar)
        Me.Panel1.Controls.Add(Me.btnEliminar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 595)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(789, 28)
        Me.Panel1.TabIndex = 6
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(280, 2)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 8
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(203, 2)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.flpRegistradoPor)
        Me.FlowLayoutPanel1.Controls.Add(Me.flpAnuladoPor)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 542)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(353, 53)
        Me.FlowLayoutPanel1.TabIndex = 4
        '
        'OcxImpuesto1
        '
        Me.OcxImpuesto1.Decimales = False
        Me.OcxImpuesto1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxImpuesto1.dtImpuesto = Nothing
        Me.OcxImpuesto1.IDMoneda = 0
        Me.OcxImpuesto1.Location = New System.Drawing.Point(356, 470)
        Me.OcxImpuesto1.Name = "OcxImpuesto1"
        Me.TableLayoutPanel2.SetRowSpan(Me.OcxImpuesto1, 2)
        Me.OcxImpuesto1.Size = New System.Drawing.Size(430, 122)
        Me.OcxImpuesto1.TabIndex = 5
        Me.OcxImpuesto1.TotalRetencionIVA = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'GroupBox1
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.GroupBox1, 2)
        Me.GroupBox1.Controls.Add(Me.pnlDescuento)
        Me.GroupBox1.Controls.Add(Me.pnlDevolucion)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(0, 187)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(789, 280)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'pnlDescuento
        '
        Me.pnlDescuento.Controls.Add(Me.TableLayoutPanel3)
        Me.pnlDescuento.Location = New System.Drawing.Point(416, 14)
        Me.pnlDescuento.Name = "pnlDescuento"
        Me.pnlDescuento.Size = New System.Drawing.Size(366, 262)
        Me.pnlDescuento.TabIndex = 3
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.46056!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.53944!))
        Me.TableLayoutPanel3.Controls.Add(Me.lvListaDescuento, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.dgw, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel2, 1, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.FlowLayoutPanel5, 1, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.FlowLayoutPanel4, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.FlowLayoutPanel2, 0, 1)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 4
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(366, 262)
        Me.TableLayoutPanel3.TabIndex = 21
        '
        'lvListaDescuento
        '
        Me.lvListaDescuento.BackColor = System.Drawing.Color.White
        Me.lvListaDescuento.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader8})
        Me.TableLayoutPanel3.SetColumnSpan(Me.lvListaDescuento, 2)
        Me.lvListaDescuento.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvListaDescuento.FullRowSelect = True
        Me.lvListaDescuento.GridLines = True
        Me.lvListaDescuento.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvListaDescuento.HideSelection = False
        Me.lvListaDescuento.Location = New System.Drawing.Point(3, 3)
        Me.lvListaDescuento.MultiSelect = False
        Me.lvListaDescuento.Name = "lvListaDescuento"
        Me.lvListaDescuento.Size = New System.Drawing.Size(360, 97)
        Me.lvListaDescuento.TabIndex = 1
        Me.lvListaDescuento.UseCompatibleStateImageBehavior = False
        Me.lvListaDescuento.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "ID"
        Me.ColumnHeader1.Width = 40
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Descripcion"
        Me.ColumnHeader2.Width = 603
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Total"
        Me.ColumnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader8.Width = 115
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colSel, Me.DataGridViewTextBoxColumn1, Me.colTipo, Me.colCondicion, Me.colVencimiento, Me.DataGridViewTextBoxColumn2, Me.colCobrado, Me.colDescontado, Me.colSaldo, Me.colImporte, Me.colCancelar})
        Me.TableLayoutPanel3.SetColumnSpan(Me.dgw, 2)
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgw.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgw.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgw.Location = New System.Drawing.Point(3, 131)
        Me.dgw.Name = "dgw"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.RowHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(360, 97)
        Me.dgw.TabIndex = 4
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colSel
        '
        Me.colSel.HeaderText = "Sel"
        Me.colSel.Name = "colSel"
        Me.colSel.ReadOnly = True
        Me.colSel.Visible = False
        Me.colSel.Width = 30
        '
        'DataGridViewTextBoxColumn1
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn1.HeaderText = "Comprobante"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.ToolTipText = "Comprobante de Venta"
        Me.DataGridViewTextBoxColumn1.Width = 120
        '
        'colTipo
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colTipo.DefaultCellStyle = DataGridViewCellStyle4
        Me.colTipo.HeaderText = "Tipo"
        Me.colTipo.Name = "colTipo"
        Me.colTipo.ReadOnly = True
        Me.colTipo.ToolTipText = "Tipo de Comprobante"
        Me.colTipo.Width = 50
        '
        'colCondicion
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colCondicion.DefaultCellStyle = DataGridViewCellStyle5
        Me.colCondicion.HeaderText = "Cond."
        Me.colCondicion.Name = "colCondicion"
        Me.colCondicion.ReadOnly = True
        Me.colCondicion.Width = 50
        '
        'colVencimiento
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colVencimiento.DefaultCellStyle = DataGridViewCellStyle6
        Me.colVencimiento.HeaderText = "Venc."
        Me.colVencimiento.Name = "colVencimiento"
        Me.colVencimiento.ReadOnly = True
        Me.colVencimiento.Width = 90
        '
        'DataGridViewTextBoxColumn2
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = "0"
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewTextBoxColumn2.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 90
        '
        'colCobrado
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = "0"
        Me.colCobrado.DefaultCellStyle = DataGridViewCellStyle8
        Me.colCobrado.HeaderText = "Cobrado"
        Me.colCobrado.Name = "colCobrado"
        Me.colCobrado.ReadOnly = True
        Me.colCobrado.Width = 90
        '
        'colDescontado
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        DataGridViewCellStyle9.NullValue = "0"
        Me.colDescontado.DefaultCellStyle = DataGridViewCellStyle9
        Me.colDescontado.HeaderText = "Descontado"
        Me.colDescontado.Name = "colDescontado"
        Me.colDescontado.ReadOnly = True
        Me.colDescontado.Width = 90
        '
        'colSaldo
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        DataGridViewCellStyle10.NullValue = "0"
        Me.colSaldo.DefaultCellStyle = DataGridViewCellStyle10
        Me.colSaldo.HeaderText = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.ReadOnly = True
        Me.colSaldo.Width = 90
        '
        'colImporte
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = "0"
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle11
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        Me.colImporte.ToolTipText = "Importe Cobrado"
        Me.colImporte.Width = 90
        '
        'colCancelar
        '
        Me.colCancelar.HeaderText = "Cancel"
        Me.colCancelar.Name = "colCancelar"
        Me.colCancelar.ReadOnly = True
        Me.colCancelar.ToolTipText = "Cancelar manualmente el comprobante"
        Me.colCancelar.Visible = False
        Me.colCancelar.Width = 35
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txtTotalDescuento)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(122, 103)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(244, 25)
        Me.Panel2.TabIndex = 6
        '
        'txtTotalDescuento
        '
        Me.txtTotalDescuento.Color = System.Drawing.Color.Empty
        Me.txtTotalDescuento.Decimales = True
        Me.txtTotalDescuento.Indicaciones = Nothing
        Me.txtTotalDescuento.Location = New System.Drawing.Point(354, 3)
        Me.txtTotalDescuento.Name = "txtTotalDescuento"
        Me.txtTotalDescuento.Size = New System.Drawing.Size(115, 22)
        Me.txtTotalDescuento.SoloLectura = True
        Me.txtTotalDescuento.TabIndex = 0
        Me.txtTotalDescuento.TabStop = False
        Me.txtTotalDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDescuento.Texto = "0"
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.Controls.Add(Me.txtCantidadCobrado)
        Me.FlowLayoutPanel5.Controls.Add(Me.txtDescontados)
        Me.FlowLayoutPanel5.Controls.Add(Me.lblTotalCobrado)
        Me.FlowLayoutPanel5.Controls.Add(Me.txtSaldo)
        Me.FlowLayoutPanel5.Controls.Add(Me.lblDeudaTotal)
        Me.FlowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel5.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(122, 231)
        Me.FlowLayoutPanel5.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(244, 31)
        Me.FlowLayoutPanel5.TabIndex = 5
        '
        'txtCantidadCobrado
        '
        Me.txtCantidadCobrado.Color = System.Drawing.Color.Empty
        Me.txtCantidadCobrado.Decimales = True
        Me.txtCantidadCobrado.Indicaciones = Nothing
        Me.txtCantidadCobrado.Location = New System.Drawing.Point(196, 3)
        Me.txtCantidadCobrado.Name = "txtCantidadCobrado"
        Me.txtCantidadCobrado.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadCobrado.SoloLectura = True
        Me.txtCantidadCobrado.TabIndex = 4
        Me.txtCantidadCobrado.TabStop = False
        Me.txtCantidadCobrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadCobrado.Texto = "0"
        '
        'txtDescontados
        '
        Me.txtDescontados.Color = System.Drawing.Color.Empty
        Me.txtDescontados.Decimales = True
        Me.txtDescontados.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescontados.Indicaciones = Nothing
        Me.txtDescontados.Location = New System.Drawing.Point(75, 3)
        Me.txtDescontados.Name = "txtDescontados"
        Me.txtDescontados.Size = New System.Drawing.Size(115, 22)
        Me.txtDescontados.SoloLectura = True
        Me.txtDescontados.TabIndex = 3
        Me.txtDescontados.TabStop = False
        Me.txtDescontados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDescontados.Texto = "0"
        '
        'lblTotalCobrado
        '
        Me.lblTotalCobrado.AutoSize = True
        Me.lblTotalCobrado.Location = New System.Drawing.Point(7, 6)
        Me.lblTotalCobrado.Margin = New System.Windows.Forms.Padding(3, 6, 3, 0)
        Me.lblTotalCobrado.Name = "lblTotalCobrado"
        Me.lblTotalCobrado.Size = New System.Drawing.Size(62, 13)
        Me.lblTotalCobrado.TabIndex = 2
        Me.lblTotalCobrado.Text = "Descuento:"
        Me.lblTotalCobrado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(133, 31)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(108, 22)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 1
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'lblDeudaTotal
        '
        Me.lblDeudaTotal.AutoSize = True
        Me.lblDeudaTotal.Location = New System.Drawing.Point(90, 34)
        Me.lblDeudaTotal.Margin = New System.Windows.Forms.Padding(3, 6, 3, 0)
        Me.lblDeudaTotal.Name = "lblDeudaTotal"
        Me.lblDeudaTotal.Size = New System.Drawing.Size(37, 13)
        Me.lblDeudaTotal.TabIndex = 0
        Me.lblDeudaTotal.Text = "Saldo:"
        Me.lblDeudaTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.lklAgregarComprobanteDescuento)
        Me.FlowLayoutPanel4.Controls.Add(Me.lklEliminarComprobanteDescuento)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(0, 231)
        Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(122, 31)
        Me.FlowLayoutPanel4.TabIndex = 2
        '
        'lklAgregarComprobanteDescuento
        '
        Me.lklAgregarComprobanteDescuento.AutoSize = True
        Me.lklAgregarComprobanteDescuento.Location = New System.Drawing.Point(3, 0)
        Me.lklAgregarComprobanteDescuento.Name = "lklAgregarComprobanteDescuento"
        Me.lklAgregarComprobanteDescuento.Size = New System.Drawing.Size(44, 13)
        Me.lklAgregarComprobanteDescuento.TabIndex = 0
        Me.lklAgregarComprobanteDescuento.TabStop = True
        Me.lklAgregarComprobanteDescuento.Text = "Agregar"
        Me.lklAgregarComprobanteDescuento.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'lklEliminarComprobanteDescuento
        '
        Me.lklEliminarComprobanteDescuento.AutoSize = True
        Me.lklEliminarComprobanteDescuento.Location = New System.Drawing.Point(53, 0)
        Me.lklEliminarComprobanteDescuento.Name = "lklEliminarComprobanteDescuento"
        Me.lklEliminarComprobanteDescuento.Size = New System.Drawing.Size(43, 13)
        Me.lklEliminarComprobanteDescuento.TabIndex = 1
        Me.lklEliminarComprobanteDescuento.TabStop = True
        Me.lklEliminarComprobanteDescuento.Text = "Eliminar"
        Me.lklEliminarComprobanteDescuento.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.lklAgregarDetalleDescuento)
        Me.FlowLayoutPanel2.Controls.Add(Me.lklEliminarDetalleDescuento)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 103)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(122, 25)
        Me.FlowLayoutPanel2.TabIndex = 0
        '
        'lklAgregarDetalleDescuento
        '
        Me.lklAgregarDetalleDescuento.AutoSize = True
        Me.lklAgregarDetalleDescuento.Location = New System.Drawing.Point(3, 0)
        Me.lklAgregarDetalleDescuento.Name = "lklAgregarDetalleDescuento"
        Me.lklAgregarDetalleDescuento.Size = New System.Drawing.Size(44, 13)
        Me.lklAgregarDetalleDescuento.TabIndex = 0
        Me.lklAgregarDetalleDescuento.TabStop = True
        Me.lklAgregarDetalleDescuento.Text = "Agregar"
        '
        'lklEliminarDetalleDescuento
        '
        Me.lklEliminarDetalleDescuento.AutoSize = True
        Me.lklEliminarDetalleDescuento.Location = New System.Drawing.Point(53, 0)
        Me.lklEliminarDetalleDescuento.Name = "lklEliminarDetalleDescuento"
        Me.lklEliminarDetalleDescuento.Size = New System.Drawing.Size(43, 13)
        Me.lklEliminarDetalleDescuento.TabIndex = 1
        Me.lklEliminarDetalleDescuento.TabStop = True
        Me.lklEliminarDetalleDescuento.Text = "Eliminar"
        '
        'pnlDevolucion
        '
        Me.pnlDevolucion.Controls.Add(Me.TableLayoutPanel1)
        Me.pnlDevolucion.Location = New System.Drawing.Point(5, 17)
        Me.pnlDevolucion.Name = "pnlDevolucion"
        Me.pnlDevolucion.Size = New System.Drawing.Size(408, 262)
        Me.pnlDevolucion.TabIndex = 2
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.lklRecepcionDocumento)
        Me.Panel3.Controls.Add(Me.btnRecalcularSaldoNC)
        Me.Panel3.Controls.Add(Me.btnVerPedidoNC)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(0, 467)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(353, 75)
        Me.Panel3.TabIndex = 7
        '
        'lklRecepcionDocumento
        '
        Me.lklRecepcionDocumento.AutoSize = True
        Me.lklRecepcionDocumento.Location = New System.Drawing.Point(190, 15)
        Me.lklRecepcionDocumento.Name = "lklRecepcionDocumento"
        Me.lklRecepcionDocumento.Size = New System.Drawing.Size(134, 13)
        Me.lklRecepcionDocumento.TabIndex = 10
        Me.lklRecepcionDocumento.TabStop = True
        Me.lklRecepcionDocumento.Text = "Recepcion del Documento"
        '
        'btnRecalcularSaldoNC
        '
        Me.btnRecalcularSaldoNC.Location = New System.Drawing.Point(3, 34)
        Me.btnRecalcularSaldoNC.Name = "btnRecalcularSaldoNC"
        Me.btnRecalcularSaldoNC.Size = New System.Drawing.Size(169, 23)
        Me.btnRecalcularSaldoNC.TabIndex = 9
        Me.btnRecalcularSaldoNC.Text = "Recalcular Saldo Nota Credito"
        Me.btnRecalcularSaldoNC.UseVisualStyleBackColor = True
        '
        'btnVerPedidoNC
        '
        Me.btnVerPedidoNC.Location = New System.Drawing.Point(4, 5)
        Me.btnVerPedidoNC.Name = "btnVerPedidoNC"
        Me.btnVerPedidoNC.Size = New System.Drawing.Size(169, 23)
        Me.btnVerPedidoNC.TabIndex = 8
        Me.btnVerPedidoNC.Text = "Ver Pedido de Nota de Credito"
        Me.btnVerPedidoNC.UseVisualStyleBackColor = True
        '
        'frmNotaCreditoCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(789, 645)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmNotaCreditoCliente"
        Me.Tag = "frmNotaCreditoCliente"
        Me.Text = "frmNotaCreditoCliente"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.gbxComprobante.ResumeLayout(False)
        Me.gbxComprobante.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.pnlDescuento.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel5.PerformLayout()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel4.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        Me.pnlDevolucion.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtRestoTimbrado As ERP.ocxTXTNumeric
    Friend WithEvents cbxComprobante As ERP.ocxCBX
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCostoUnitario As ERP.ocxTXTNumeric
    Friend WithEvents lblCostoUnitario As System.Windows.Forms.Label
    Friend WithEvents txtObservacionProducto As ERP.ocxTXTString
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents lvLista As System.Windows.Forms.ListView
    Friend WithEvents colID As System.Windows.Forms.ColumnHeader
    Friend WithEvents colDescripcion As System.Windows.Forms.ColumnHeader
    Friend WithEvents colComprobante As System.Windows.Forms.ColumnHeader
    Friend WithEvents colIVA As System.Windows.Forms.ColumnHeader
    Friend WithEvents colCantidad As System.Windows.Forms.ColumnHeader
    Friend WithEvents colPrecioUnitario As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColTotal As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblObservacionProducto As System.Windows.Forms.Label
    Friend WithEvents txtImporte As ERP.ocxTXTNumeric
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As ERP.ocxTXTNumeric
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents lblProducto As System.Windows.Forms.Label
    Friend WithEvents lblRestoTimbrado As System.Windows.Forms.Label
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents gbxComprobante As System.Windows.Forms.GroupBox
    Friend WithEvents txtVencimientoTimbrado As ERP.ocxTXTDate
    Friend WithEvents lblVenimientoTimbrado As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents txtNroComprobante As ERP.ocxTXTString
    Friend WithEvents txtTalonario As ERP.ocxTXTString
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents lblTalonario As System.Windows.Forms.Label
    Friend WithEvents txtReferenciaSucursal As ERP.ocxTXTString
    Friend WithEvents txtReferenciaTerminal As ERP.ocxTXTString
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents btnAnula As System.Windows.Forms.Button
    Friend WithEvents rdbDescuentos As System.Windows.Forms.RadioButton
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents rdbDevolucion As System.Windows.Forms.RadioButton
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents lblDeposito As System.Windows.Forms.Label
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents OcxImpuesto1 As ERP.ocxImpuesto
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents chkAplicar As ERP.ocxCHK
    Friend WithEvents pnlDevolucion As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents VerDetalleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportarAExcelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents colReferencia As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents txtTimbrado As ERP.ocxTXTString
    Friend WithEvents lklTimbrado As System.Windows.Forms.LinkLabel
    Friend WithEvents txtIDTimbrado As ERP.ocxTXTString
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents OcxCotizacion1 As ERP.ocxCotizacion
    Friend WithEvents chkImprimir2 As ERP.ocxCHK
    Friend WithEvents pnlDescuento As Panel
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents lvListaDescuento As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader8 As ColumnHeader
    Friend WithEvents dgw As DataGridView
    Friend WithEvents colIDTransaccion As DataGridViewTextBoxColumn
    Friend WithEvents colSel As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents colTipo As DataGridViewTextBoxColumn
    Friend WithEvents colCondicion As DataGridViewTextBoxColumn
    Friend WithEvents colVencimiento As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents colCobrado As DataGridViewTextBoxColumn
    Friend WithEvents colDescontado As DataGridViewTextBoxColumn
    Friend WithEvents colSaldo As DataGridViewTextBoxColumn
    Friend WithEvents colImporte As DataGridViewTextBoxColumn
    Friend WithEvents colCancelar As DataGridViewCheckBoxColumn
    Friend WithEvents Panel2 As Panel
    Friend WithEvents txtTotalDescuento As ocxTXTNumeric
    Friend WithEvents FlowLayoutPanel5 As FlowLayoutPanel
    Friend WithEvents txtCantidadCobrado As ocxTXTNumeric
    Friend WithEvents txtDescontados As ocxTXTNumeric
    Friend WithEvents lblTotalCobrado As Label
    Friend WithEvents txtSaldo As ocxTXTNumeric
    Friend WithEvents lblDeudaTotal As Label
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents lklAgregarComprobanteDescuento As LinkLabel
    Friend WithEvents lklEliminarComprobanteDescuento As LinkLabel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents lklAgregarDetalleDescuento As LinkLabel
    Friend WithEvents lklEliminarDetalleDescuento As LinkLabel
    Friend WithEvents txtTelefono As ocxTXTString
    Friend WithEvents lblTelefono As Label
    Friend WithEvents txtDireccion As ocxTXTString
    Friend WithEvents lblDireccion As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents btnRecalcularSaldoNC As Button
    Friend WithEvents btnVerPedidoNC As Button
    Friend WithEvents lklRecepcionDocumento As LinkLabel
    Friend WithEvents TxtComprobanteImpresion As ocxTXTString
    Friend WithEvents cbxComprobanteImpresion As ocxCBX
End Class

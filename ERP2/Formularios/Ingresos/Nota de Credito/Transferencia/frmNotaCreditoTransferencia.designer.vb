﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNotaCreditoTransferencia
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.bntAgregarNotacredito = New System.Windows.Forms.Button()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblNotaCredito = New System.Windows.Forms.Label()
        Me.txtTotalNotaCredito = New ERP.ocxTXTNumeric()
        Me.txtMonedaNotaCredito = New ERP.ocxTXTString()
        Me.txtFechaNotaCredito = New ERP.ocxTXTString()
        Me.txtObservacionNotaCredito = New ERP.ocxTXTString()
        Me.lblMonedaNotaCredito = New System.Windows.Forms.Label()
        Me.txtCotizacionNC = New ERP.ocxTXTNumeric()
        Me.lblFechaNotaCredito = New System.Windows.Forms.Label()
        Me.txtComprobanteNotaCredito = New ERP.ocxTXTString()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnTranferirFacturaPendiente = New System.Windows.Forms.Button()
        Me.btnTransferirFacturaNotaCredito = New System.Windows.Forms.Button()
        Me.lblFacturaPendiente = New System.Windows.Forms.Label()
        Me.lblFacturasNotaCredito = New System.Windows.Forms.Label()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.lblFacturaSaliente = New System.Windows.Forms.Label()
        Me.lblFacturaEntrante = New System.Windows.Forms.Label()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.txtFacturasNotaCredito = New ERP.ocxTXTNumeric()
        Me.TxtFacturaPendientes = New ERP.ocxTXTNumeric()
        Me.ColIDTransaccion1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColIDTransaccionNotaCreditoAplicacion1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ESV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColIDTransaccionNotaCreditoAplicacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ESFNC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        Me.gbxCabecera.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bntAgregarNotacredito
        '
        Me.bntAgregarNotacredito.Location = New System.Drawing.Point(183, 26)
        Me.bntAgregarNotacredito.Name = "bntAgregarNotacredito"
        Me.bntAgregarNotacredito.Size = New System.Drawing.Size(71, 21)
        Me.bntAgregarNotacredito.TabIndex = 3
        Me.bntAgregarNotacredito.Text = "Seleccionar"
        Me.bntAgregarNotacredito.UseVisualStyleBackColor = True
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(334, 82)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(34, 13)
        Me.lblTotal.TabIndex = 11
        Me.lblTotal.Text = "Total:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(2, 31)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Comprobante:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(2, 57)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Observacion:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblNotaCredito)
        Me.GroupBox1.Controls.Add(Me.txtTotalNotaCredito)
        Me.GroupBox1.Controls.Add(Me.txtMonedaNotaCredito)
        Me.GroupBox1.Controls.Add(Me.txtFechaNotaCredito)
        Me.GroupBox1.Controls.Add(Me.bntAgregarNotacredito)
        Me.GroupBox1.Controls.Add(Me.lblTotal)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtObservacionNotaCredito)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.lblMonedaNotaCredito)
        Me.GroupBox1.Controls.Add(Me.txtCotizacionNC)
        Me.GroupBox1.Controls.Add(Me.lblFechaNotaCredito)
        Me.GroupBox1.Controls.Add(Me.txtComprobanteNotaCredito)
        Me.GroupBox1.Location = New System.Drawing.Point(2, 133)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(464, 104)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'lblNotaCredito
        '
        Me.lblNotaCredito.AutoSize = True
        Me.lblNotaCredito.Location = New System.Drawing.Point(2, 10)
        Me.lblNotaCredito.Name = "lblNotaCredito"
        Me.lblNotaCredito.Size = New System.Drawing.Size(69, 13)
        Me.lblNotaCredito.TabIndex = 0
        Me.lblNotaCredito.Text = "Nota Credito "
        '
        'txtTotalNotaCredito
        '
        Me.txtTotalNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtTotalNotaCredito.Decimales = False
        Me.txtTotalNotaCredito.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotalNotaCredito.Location = New System.Drawing.Point(372, 77)
        Me.txtTotalNotaCredito.Name = "txtTotalNotaCredito"
        Me.txtTotalNotaCredito.Size = New System.Drawing.Size(84, 21)
        Me.txtTotalNotaCredito.SoloLectura = False
        Me.txtTotalNotaCredito.TabIndex = 12
        Me.txtTotalNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalNotaCredito.Texto = "0"
        '
        'txtMonedaNotaCredito
        '
        Me.txtMonedaNotaCredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMonedaNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtMonedaNotaCredito.Indicaciones = Nothing
        Me.txtMonedaNotaCredito.Location = New System.Drawing.Point(306, 25)
        Me.txtMonedaNotaCredito.Multilinea = False
        Me.txtMonedaNotaCredito.Name = "txtMonedaNotaCredito"
        Me.txtMonedaNotaCredito.Size = New System.Drawing.Size(60, 21)
        Me.txtMonedaNotaCredito.SoloLectura = False
        Me.txtMonedaNotaCredito.TabIndex = 5
        Me.txtMonedaNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMonedaNotaCredito.Texto = ""
        '
        'txtFechaNotaCredito
        '
        Me.txtFechaNotaCredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFechaNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtFechaNotaCredito.Indicaciones = Nothing
        Me.txtFechaNotaCredito.Location = New System.Drawing.Point(379, 52)
        Me.txtFechaNotaCredito.Multilinea = False
        Me.txtFechaNotaCredito.Name = "txtFechaNotaCredito"
        Me.txtFechaNotaCredito.Size = New System.Drawing.Size(77, 20)
        Me.txtFechaNotaCredito.SoloLectura = False
        Me.txtFechaNotaCredito.TabIndex = 10
        Me.txtFechaNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFechaNotaCredito.Texto = ""
        '
        'txtObservacionNotaCredito
        '
        Me.txtObservacionNotaCredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacionNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtObservacionNotaCredito.Indicaciones = Nothing
        Me.txtObservacionNotaCredito.Location = New System.Drawing.Point(79, 53)
        Me.txtObservacionNotaCredito.Multilinea = False
        Me.txtObservacionNotaCredito.Name = "txtObservacionNotaCredito"
        Me.txtObservacionNotaCredito.Size = New System.Drawing.Size(241, 21)
        Me.txtObservacionNotaCredito.SoloLectura = False
        Me.txtObservacionNotaCredito.TabIndex = 8
        Me.txtObservacionNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacionNotaCredito.Texto = ""
        '
        'lblMonedaNotaCredito
        '
        Me.lblMonedaNotaCredito.AutoSize = True
        Me.lblMonedaNotaCredito.Location = New System.Drawing.Point(260, 29)
        Me.lblMonedaNotaCredito.Name = "lblMonedaNotaCredito"
        Me.lblMonedaNotaCredito.Size = New System.Drawing.Size(49, 13)
        Me.lblMonedaNotaCredito.TabIndex = 4
        Me.lblMonedaNotaCredito.Text = "Moneda:"
        '
        'txtCotizacionNC
        '
        Me.txtCotizacionNC.Color = System.Drawing.Color.Empty
        Me.txtCotizacionNC.Decimales = False
        Me.txtCotizacionNC.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacionNC.Location = New System.Drawing.Point(379, 25)
        Me.txtCotizacionNC.Name = "txtCotizacionNC"
        Me.txtCotizacionNC.Size = New System.Drawing.Size(77, 21)
        Me.txtCotizacionNC.SoloLectura = False
        Me.txtCotizacionNC.TabIndex = 6
        Me.txtCotizacionNC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacionNC.Texto = "0"
        '
        'lblFechaNotaCredito
        '
        Me.lblFechaNotaCredito.AutoSize = True
        Me.lblFechaNotaCredito.Location = New System.Drawing.Point(343, 56)
        Me.lblFechaNotaCredito.Name = "lblFechaNotaCredito"
        Me.lblFechaNotaCredito.Size = New System.Drawing.Size(40, 13)
        Me.lblFechaNotaCredito.TabIndex = 9
        Me.lblFechaNotaCredito.Text = "Fecha:"
        '
        'txtComprobanteNotaCredito
        '
        Me.txtComprobanteNotaCredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobanteNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtComprobanteNotaCredito.Indicaciones = Nothing
        Me.txtComprobanteNotaCredito.Location = New System.Drawing.Point(77, 26)
        Me.txtComprobanteNotaCredito.Multilinea = False
        Me.txtComprobanteNotaCredito.Name = "txtComprobanteNotaCredito"
        Me.txtComprobanteNotaCredito.Size = New System.Drawing.Size(91, 21)
        Me.txtComprobanteNotaCredito.SoloLectura = False
        Me.txtComprobanteNotaCredito.TabIndex = 2
        Me.txtComprobanteNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobanteNotaCredito.Texto = ""
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(6, 257)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(174, 134)
        Me.ListBox1.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(58, 402)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Total:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(314, 402)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Total:"
        '
        'btnTranferirFacturaPendiente
        '
        Me.btnTranferirFacturaPendiente.Location = New System.Drawing.Point(204, 290)
        Me.btnTranferirFacturaPendiente.Name = "btnTranferirFacturaPendiente"
        Me.btnTranferirFacturaPendiente.Size = New System.Drawing.Size(63, 23)
        Me.btnTranferirFacturaPendiente.TabIndex = 4
        Me.btnTranferirFacturaPendiente.Text = ">>"
        Me.btnTranferirFacturaPendiente.UseVisualStyleBackColor = True
        '
        'btnTransferirFacturaNotaCredito
        '
        Me.btnTransferirFacturaNotaCredito.Location = New System.Drawing.Point(204, 313)
        Me.btnTransferirFacturaNotaCredito.Name = "btnTransferirFacturaNotaCredito"
        Me.btnTransferirFacturaNotaCredito.Size = New System.Drawing.Size(63, 23)
        Me.btnTransferirFacturaNotaCredito.TabIndex = 5
        Me.btnTransferirFacturaNotaCredito.Text = "<<"
        Me.btnTransferirFacturaNotaCredito.UseVisualStyleBackColor = True
        '
        'lblFacturaPendiente
        '
        Me.lblFacturaPendiente.AutoSize = True
        Me.lblFacturaPendiente.Location = New System.Drawing.Point(3, 241)
        Me.lblFacturaPendiente.Name = "lblFacturaPendiente"
        Me.lblFacturaPendiente.Size = New System.Drawing.Size(104, 13)
        Me.lblFacturaPendiente.TabIndex = 2
        Me.lblFacturaPendiente.Text = "Facturas Pendientes"
        '
        'lblFacturasNotaCredito
        '
        Me.lblFacturasNotaCredito.AutoSize = True
        Me.lblFacturasNotaCredito.Location = New System.Drawing.Point(271, 244)
        Me.lblFacturasNotaCredito.Name = "lblFacturasNotaCredito"
        Me.lblFacturasNotaCredito.Size = New System.Drawing.Size(105, 13)
        Me.lblFacturasNotaCredito.TabIndex = 6
        Me.lblFacturasNotaCredito.Text = "Factura Nota Credito"
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.txtCliente)
        Me.gbxCabecera.Controls.Add(Me.Label5)
        Me.gbxCabecera.Controls.Add(Me.cbxMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.txtCotizacion)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.Label6)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Location = New System.Drawing.Point(2, 6)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(464, 123)
        Me.gbxCabecera.TabIndex = 0
        Me.gbxCabecera.TabStop = False
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 117
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(76, 39)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(385, 23)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(5, 43)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(42, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Cliente:"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(78, 68)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(74, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 11
        Me.cbxMoneda.Texto = ""
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(5, 72)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 10
        Me.lblMoneda.Text = "Moneda:"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(166, 68)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(92, 21)
        Me.txtCotizacion.SoloLectura = False
        Me.txtCotizacion.TabIndex = 12
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "0"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(211, 12)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(60, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 3, 18, 14, 6, 37, 841)
        Me.txtFecha.Location = New System.Drawing.Point(383, 69)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 14
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(341, 72)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Fecha:"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(343, 10)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(58, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 6
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(402, 10)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(55, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 7
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(76, 12)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(51, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(131, 12)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(46, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(5, 100)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 15
        Me.lblObservacion.Text = "Observacion:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(180, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(32, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Suc.:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(304, 14)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(40, 13)
        Me.lblComprobante.TabIndex = 5
        Me.lblComprobante.Text = "Comp.:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(5, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(76, 96)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(381, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 16
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(1, 424)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(75, 23)
        Me.btnBusquedaAvanzada.TabIndex = 12
        Me.btnBusquedaAvanzada.Text = "&Busqueda"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(389, 424)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 17
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(314, 424)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 16
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(239, 424)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 15
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(164, 424)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 14
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 448)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(470, 22)
        Me.StatusStrip1.TabIndex = 23
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.ColIDTransaccionNotaCreditoAplicacion, Me.colComprobante, Me.colImporte, Me.ESFNC})
        Me.DataGridView1.Location = New System.Drawing.Point(271, 257)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(195, 134)
        Me.DataGridView1.TabIndex = 7
        '
        'lblFacturaSaliente
        '
        Me.lblFacturaSaliente.AutoSize = True
        Me.lblFacturaSaliente.Location = New System.Drawing.Point(4, 242)
        Me.lblFacturaSaliente.Name = "lblFacturaSaliente"
        Me.lblFacturaSaliente.Size = New System.Drawing.Size(94, 13)
        Me.lblFacturaSaliente.TabIndex = 24
        Me.lblFacturaSaliente.Text = "Facturas Salientes"
        '
        'lblFacturaEntrante
        '
        Me.lblFacturaEntrante.AutoSize = True
        Me.lblFacturaEntrante.Location = New System.Drawing.Point(271, 244)
        Me.lblFacturaEntrante.Name = "lblFacturaEntrante"
        Me.lblFacturaEntrante.Size = New System.Drawing.Size(86, 13)
        Me.lblFacturaEntrante.TabIndex = 25
        Me.lblFacturaEntrante.Text = "Factura Entrante"
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColIDTransaccion1, Me.ColIDTransaccionNotaCreditoAplicacion1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.ESV})
        Me.DataGridView2.Location = New System.Drawing.Point(0, 257)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.RowHeadersVisible = False
        Me.DataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView2.Size = New System.Drawing.Size(199, 134)
        Me.DataGridView2.TabIndex = 26
        '
        'txtFacturasNotaCredito
        '
        Me.txtFacturasNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtFacturasNotaCredito.Decimales = False
        Me.txtFacturasNotaCredito.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtFacturasNotaCredito.Location = New System.Drawing.Point(348, 398)
        Me.txtFacturasNotaCredito.Name = "txtFacturasNotaCredito"
        Me.txtFacturasNotaCredito.Size = New System.Drawing.Size(84, 21)
        Me.txtFacturasNotaCredito.SoloLectura = False
        Me.txtFacturasNotaCredito.TabIndex = 11
        Me.txtFacturasNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFacturasNotaCredito.Texto = "0"
        '
        'TxtFacturaPendientes
        '
        Me.TxtFacturaPendientes.Color = System.Drawing.Color.Empty
        Me.TxtFacturaPendientes.Decimales = False
        Me.TxtFacturaPendientes.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.TxtFacturaPendientes.Location = New System.Drawing.Point(94, 398)
        Me.TxtFacturaPendientes.Name = "TxtFacturaPendientes"
        Me.TxtFacturaPendientes.Size = New System.Drawing.Size(84, 21)
        Me.TxtFacturaPendientes.SoloLectura = False
        Me.TxtFacturaPendientes.TabIndex = 9
        Me.TxtFacturaPendientes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TxtFacturaPendientes.Texto = "0"
        '
        'ColIDTransaccion1
        '
        Me.ColIDTransaccion1.HeaderText = "IDTransaccion"
        Me.ColIDTransaccion1.Name = "ColIDTransaccion1"
        Me.ColIDTransaccion1.ReadOnly = True
        Me.ColIDTransaccion1.Visible = False
        '
        'ColIDTransaccionNotaCreditoAplicacion1
        '
        Me.ColIDTransaccionNotaCreditoAplicacion1.HeaderText = "IDTransaccionNotaCreditoAplicacion"
        Me.ColIDTransaccionNotaCreditoAplicacion1.Name = "ColIDTransaccionNotaCreditoAplicacion1"
        Me.ColIDTransaccionNotaCreditoAplicacion1.ReadOnly = True
        Me.ColIDTransaccionNotaCreditoAplicacion1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Comprobante"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 95
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N0"
        DataGridViewCellStyle1.NullValue = "0"
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridViewTextBoxColumn3.HeaderText = "Importe"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'ESV
        '
        Me.ESV.HeaderText = "ES"
        Me.ESV.Name = "ESV"
        Me.ESV.ReadOnly = True
        Me.ESV.Visible = False
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'ColIDTransaccionNotaCreditoAplicacion
        '
        Me.ColIDTransaccionNotaCreditoAplicacion.HeaderText = "IDTransaccionNotaCreditoAplicacion"
        Me.ColIDTransaccionNotaCreditoAplicacion.Name = "ColIDTransaccionNotaCreditoAplicacion"
        Me.ColIDTransaccionNotaCreditoAplicacion.Visible = False
        '
        'colComprobante
        '
        Me.colComprobante.HeaderText = "Comprobante"
        Me.colComprobante.Name = "colComprobante"
        Me.colComprobante.ReadOnly = True
        Me.colComprobante.Width = 95
        '
        'colImporte
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = "0"
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle2
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        '
        'ESFNC
        '
        Me.ESFNC.HeaderText = "ES"
        Me.ESFNC.Name = "ESFNC"
        Me.ESFNC.Visible = False
        Me.ESFNC.Width = 30
        '
        'frmNotaCreditoTransferencia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(470, 470)
        Me.Controls.Add(Me.DataGridView2)
        Me.Controls.Add(Me.lblFacturaEntrante)
        Me.Controls.Add(Me.lblFacturaSaliente)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Controls.Add(Me.lblFacturasNotaCredito)
        Me.Controls.Add(Me.lblFacturaPendiente)
        Me.Controls.Add(Me.btnTransferirFacturaNotaCredito)
        Me.Controls.Add(Me.btnTranferirFacturaPendiente)
        Me.Controls.Add(Me.txtFacturasNotaCredito)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TxtFacturaPendientes)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.GroupBox1)
        Me.KeyPreview = True
        Me.Name = "frmNotaCreditoTransferencia"
        Me.Text = "frmNotaCreditoTransferencia"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtMonedaNotaCredito As ERP.ocxTXTString
    Friend WithEvents txtFechaNotaCredito As ERP.ocxTXTString
    Friend WithEvents bntAgregarNotacredito As System.Windows.Forms.Button
    Friend WithEvents txtCotizacionNC As ERP.ocxTXTNumeric
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTotalNotaCredito As ERP.ocxTXTNumeric
    Friend WithEvents lblNotaCredito As System.Windows.Forms.Label
    Friend WithEvents txtObservacionNotaCredito As ERP.ocxTXTString
    Friend WithEvents lblMonedaNotaCredito As System.Windows.Forms.Label
    Friend WithEvents lblFechaNotaCredito As System.Windows.Forms.Label
    Friend WithEvents txtComprobanteNotaCredito As ERP.ocxTXTString
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents TxtFacturaPendientes As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtFacturasNotaCredito As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnTranferirFacturaPendiente As System.Windows.Forms.Button
    Friend WithEvents btnTransferirFacturaNotaCredito As System.Windows.Forms.Button
    Friend WithEvents lblFacturaPendiente As System.Windows.Forms.Label
    Friend WithEvents lblFacturasNotaCredito As System.Windows.Forms.Label
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents lblFacturaEntrante As System.Windows.Forms.Label
    Friend WithEvents lblFacturaSaliente As System.Windows.Forms.Label
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents ColIDTransaccion1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColIDTransaccionNotaCreditoAplicacion1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ESV As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIDTransaccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColIDTransaccionNotaCreditoAplicacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ESFNC As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

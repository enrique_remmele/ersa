﻿Public Class frmNotaCreditoTransferencia
    'CLASES
    Dim CSistema As New CSistema
    Dim CAsientoContableCobranzaCredito As New CAsientoContableCobranzaCredito
    Dim CArchivoInicio As New CArchivoInicio

    'VARIABLES
    Dim vControles() As Control
    Dim dtCargarNotaCredito As New DataTable
    Dim dtVentasPendientes As New DataTable
    Dim dtFacturaNotaCredito As New DataTable
    Dim vNuevo As Boolean

    'PROPIEDADES
    Private IDTransaccionNotaCreditoValue As Integer
    Public Property IDTransaccionNotaCredito() As Integer
        Get
            Return IDTransaccionNotaCreditoValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionNotaCreditoValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Controles
        txtCliente.Conectar()

        'Otros
        cbxCiudad.Enabled = True

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "TRANSFERENCIA NOTA DE CREDITO", "TNC")
        vNuevo = True

        'Funciones
        CargarInformacion()

        'Clases
        CAsientoContableCobranzaCredito.Inicializar()


        'Botones
        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO, btnNuevo, btnGuardar, btnCancelar, New Button, New Button, btnBusquedaAvanzada, New Button, vControles)
       

        Dim ID As Integer
        ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VNotaCreditoTransferencia Where IDsucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)

        txtID.txt.Text = ID
        txtID.txt.SelectAll()
        CargarOperacion()

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        ' CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, txtCotizacion)
        CSistema.CargaControl(vControles, bntAgregarNotacredito)
        CSistema.CargaControl(vControles, txtComprobanteNotaCredito)
        CSistema.CargaControl(vControles, txtFechaNotaCredito)
        CSistema.CargaControl(vControles, txtMonedaNotaCredito)
        CSistema.CargaControl(vControles, txtCotizacionNC)
        CSistema.CargaControl(vControles, txtObservacionNotaCredito)
        CSistema.CargaControl(vControles, txtTotalNotaCredito)
        CSistema.CargaControl(vControles, bntAgregarNotacredito)
        CSistema.CargaControl(vControles, txtComprobanteNotaCredito)
        CSistema.CargaControl(vControles, txtFechaNotaCredito)
        CSistema.CargaControl(vControles, txtMonedaNotaCredito)
        CSistema.CargaControl(vControles, txtCotizacionNC)
        CSistema.CargaControl(vControles, txtObservacionNotaCredito)
        CSistema.CargaControl(vControles, txtTotalNotaCredito)
        CSistema.CargaControl(vControles, TxtFacturaPendientes)
        CSistema.CargaControl(vControles, txtFacturasNotaCredito)

        'CARGAR ESTRUCTURA DEL DETALLE 


        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Referencia From Moneda")
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Moneda
        txtCotizacion.txt.Text = 1

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        lblFacturaPendiente.Visible = True
        lblFacturaSaliente.Visible = False
        lblFacturasNotaCredito.Visible = True
        lblFacturaEntrante.Visible = False
        

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)



    End Sub

    Sub Nuevo()

        'Configurar botones
        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO, btnNuevo, btnGuardar, btnCancelar, New Button, New Button, btnBusquedaAvanzada, New Button, vControles)

        'Limpiar detalle
        '
        ' ListarVentas()

        vNuevo = True

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        CAsientoContableCobranzaCredito.Limpiar()

        ' vNuevo = True

        'Cabecera
        txtFecha.txt.Text = ""
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()
        txtCliente.Clear()

       

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From TransferenciaNotaCredito),1)"), Integer)
        txtComprobante.txt.Text = CSistema.ObtenerProximoNroComprobante(cbxTipoComprobante.cbx.SelectedValue, "TransferenciaNotaCredito", "NroComprobante", cbxSucursal.cbx.SelectedValue)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Bloquear Nota Credito Aplicar
        txtComprobanteNotaCredito.txt.ReadOnly = True
        txtFechaNotaCredito.txt.ReadOnly = True
        txtMonedaNotaCredito.txt.ReadOnly = True
        txtCotizacionNC.txt.ReadOnly = True
        txtObservacionNotaCredito.txt.ReadOnly = True
        txtTotalNotaCredito.txt.ReadOnly = True
        TxtFacturaPendientes.txt.ReadOnly = True
        txtFacturasNotaCredito.txt.ReadOnly = True


      
        'Poner el foco en el proveedor
        cbxTipoComprobante.cbx.Focus()

        'Limpiar  Transferencia Nota de Credito
        txtComprobanteNotaCredito.txt.Text = ""
        txtFechaNotaCredito.txt.Text = ""
        txtMonedaNotaCredito.txt.Text = ""
        txtComprobanteNotaCredito.txt.Text = ""
        txtObservacionNotaCredito.txt.Text = ""
        txtTotalNotaCredito.txt.Text = ""
        TxtFacturaPendientes.txt.Text = ""
        txtFacturasNotaCredito.txt.Text = ""
        ListBox1.DataSource = Nothing
        DataGridView1.Rows.Clear()
        DataGridView2.Rows.Clear()
        lblFacturaPendiente.Visible = True
        lblFacturaSaliente.Visible = False
        lblFacturasNotaCredito.Visible = True
        lblFacturaEntrante.Visible = False
        btnTranferirFacturaPendiente.Enabled = True
        btnTransferirFacturaNotaCredito.Enabled = True

    End Sub

    Sub Cancelar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR, btnNuevo, btnGuardar, btnCancelar, New Button, New Button, btnBusquedaAvanzada, New Button, vControles)

        Dim ID As Integer
        ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VNotaCreditoTransferencia Where IDsucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)

        txtID.txt.Text = ID
        txtID.txt.SelectAll()
        CargarOperacion()
        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDTransaccionNotaCredito", IDTransaccionNotaCredito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(txtCotizacion.txt.Text), ParameterDirection.Input)


        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpNotaCreditoTransferencia", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From TransferenciaNotaCredito Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpNotaCreditoTransferencia", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        Dim Procesar As Boolean = True


        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR, btnNuevo, btnGuardar, btnCancelar, New Button, New Button, btnBusquedaAvanzada, New Button, vControles)
            'CargarOperacion(IDTransaccion)

            txtID.SoloLectura = False
        End If

        'Guardar detalle
        If IDTransaccion > 0 Then

            'Guardar Nota Credito Aplicada
            Procesar = InsertarDetalle(IDTransaccion)


        End If



        Try

            ReDim param(-1)

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            'Aplicar
            If CSistema.ExecuteStoreProcedure(param, "SpNotaCreditoTransferenciaProcesar", False, False, MensajeRetorno) = False Then

            End If

            ''Cargamos el Asiento
            ''Cargamos el asiento
            'CAsientoContableCobranzaCredito.CAsiento.IDTransaccion = IDTransaccion
            'GenerarAsiento()

            'CAsientoContableCobranzaCredito.CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        Catch ex As Exception

        End Try


        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR, btnNuevo, btnGuardar, btnCancelar, New Button, New Button, btnBusquedaAvanzada, New Button, vControles)
        ' CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False

    End Sub

    Sub Buscar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA, btnNuevo, btnGuardar, btnCancelar, New Button, New Button, btnBusquedaAvanzada, New Button, vControles)

        'Otros
        Dim frm As New frmConsultaTransferenciaNotaCredito
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog(Me)

        'If frm.IDTransaccion > 0 Then
        '    CargarOperacion(frm.IDTransaccion)
        'End If

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False


        'Nro de Comprobante, mayor a 0
        If IsNumeric(txtComprobante.txt.Text) = False Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Saldo de Nota Credito y Facturas de Nota Credito deben ser Iguales
        If txtFacturasNotaCredito.txt.Text <> txtTotalNotaCredito.txt.Text Then
            Dim mensaje As String = "El monto de la nota de credito no coinciden con el total de las facturas!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If


        'Asignamos entero al nro de comprobante
        txtComprobante.txt.Text = CInt(txtComprobante.txt.Text)

        If CInt(txtComprobante.txt.Text) = 0 Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Cliente
        If txtCliente.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If txtCliente.Registro Is Nothing Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If IsNumeric(txtCliente.Registro("ID").ToString) = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Moneda
        If IsNumeric(cbxMoneda.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el tipo de moneda!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If



        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        ValidarDocumento = True

    End Function

    Sub SeleccionarCliente()

        'Si no es nuevo, salir
        If vNuevo = False Then
            Exit Sub
        End If

        'Limpiar Nota de Credito
        txtFechaNotaCredito.txt.Text = ""
        txtComprobanteNotaCredito.txt.Text = ""
        txtMonedaNotaCredito.txt.Text = ""
        txtCotizacionNC.txt.Text = ""
        txtObservacionNotaCredito.txt.Text = ""
        txtTotalNotaCredito.txt.Text = ""

        'Limpiar Facturas Nota de Credito
        DataGridView1.Rows.Clear()

        If txtCliente.Seleccionado = False Then
            dtCargarNotaCredito.Rows.Clear()
            Exit Sub
        End If

        'Obtenemos las Notas de Credito y Facturas Pendientes
        '!!!IMPORTANTE - Solo se pueden transferir Notas de Credito que no se aplicaron desde su origen.
        dtCargarNotaCredito = CSistema.ExecuteToDataTable("Select IDTransaccion, Sucursal, Fec, [Cod.], Comprobante, Tipo, Total, Saldo, Aplicado, Observacion, Moneda, Cotizacion, 'Sel'='True' from VNotaCredito Where Aplicar='False' And Anulado='False' And IDCliente=" & txtCliente.Registro("ID").ToString).Copy
        dtVentasPendientes = CSistema.ExecuteToDataTable("Select 'ES'='S','IDTransaccionVenta'=IDTransaccion, Comprobante, 'Importe'=Saldo, 'Tipo'='VentaPendiente', 'IDTransaccionNotaCreditoAplicacion'=0, 'Saldo'=Importe From VVentasPendientes Where IDCliente=" & txtCliente.Registro("ID").ToString).Copy

        'Listamos las Facturas pendientes
        ListarVentas()

        'Foco
        cbxMoneda.cbx.Focus()

    End Sub

    Sub SeleccionarNotaCredito()

        'Validar
        If txtCliente.Seleccionado = False Then
            MessageBox.Show("Seleccione un cliente para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If


        If CargarNotaCredito() = False Then
            Exit Sub
        End If

        'Cargamos la transaccion seleccionada
        CargarNotaCreditoAplicar()

        'Listamos las facturas de la nota de credito
        ListarFacturaNotaCredito()

    End Sub

    Function CargarNotaCredito() As Boolean

        CargarNotaCredito = False

        Dim frm As New frmSeleccionarNotaCreditoAplicacion
        frm.Text = " Seleccionar Nota de Credito "
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtCargarNotaCredito
        frm.txtImportante.Visible = True
        FGMostrarFormulario(Me, frm, "Seleccione la Nota de Credito para la operacion", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Seleccionado = False Then
            Return False
        End If

        IDTransaccionNotaCredito = frm.IDTransaccion

        Return True

    End Function

    Sub CargarNotaCreditoAplicar()

        For Each oRow As DataRow In dtCargarNotaCredito.Rows()
            If oRow("Sel") = True Then
                If oRow("IDTransaccion").ToString = IDTransaccionNotaCredito Then
                    txtFechaNotaCredito.txt.Text = oRow("Fec").ToString
                    txtComprobanteNotaCredito.txt.Text = oRow("Comprobante").ToString
                    txtMonedaNotaCredito.txt.Text = oRow("Moneda").ToString
                    txtCotizacionNC.txt.Text = oRow("Cotizacion").ToString
                    txtObservacionNotaCredito.txt.Text = oRow("Observacion").ToString
                    txtTotalNotaCredito.SetValue(oRow("Aplicado").ToString)
                    Exit For
                End If
            End If
        Next


        'Cargar Facturasa de la nota de credito seleccionada
        dtFacturaNotaCredito = CSistema.ExecuteToDataTable("Select 'ES'='N',IDTransaccionVenta, Comprobante, Importe, 'Tipo'= 'FacturaNotaCredito',  IDTransaccionNotaCreditoAplicacion, 'Saldo'=Importe From VNotaCreditoVentaAplicacion Where Aplicado='True' And IDTransaccion=" & IDTransaccionNotaCredito).Copy


    End Sub

    Sub ListarVentas()

        'Cargar en la Grilla
        DataGridView2.Rows.Clear()

        For Each oRow As DataRow In dtVentasPendientes.Rows
            Dim rIndex As Integer = DataGridView2.Rows.Add()
            DataGridView2.Item(0, rIndex).Value = oRow("IDTransaccionVenta").ToString
            DataGridView2.Item(1, rIndex).Value = oRow("IDTransaccionNotaCreditoAplicacion").ToString
            DataGridView2.Item(2, rIndex).Value = oRow("Comprobante").ToString
            DataGridView2.Item(3, rIndex).Value = CSistema.FormatoMoneda(oRow("Importe").ToString)
            DataGridView2.Item(4, rIndex).Value = oRow("ES").ToString
        Next


        'ListBox1.DataSource = Nothing
        'ListBox1.DisplayMember = "Comprobante"
        'ListBox1.ValueMember = "IDTransaccionVenta"
        'ListBox1.DataSource = dtVentasPendientes

        TxtFacturaPendientes.txt.Text = CSistema.dtSumColumn(dtVentasPendientes, "Importe")

        'Formato
        DataGridView2.Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub ListarFacturaNotaCredito()

        'Cargar en la Grilla
        DataGridView1.Rows.Clear()

        For Each oRow As DataRow In dtFacturaNotaCredito.Rows
            Dim rIndex As Integer = DataGridView1.Rows.Add()
            DataGridView1.Item(0, rIndex).Value = oRow("IDTransaccionVenta").ToString
            DataGridView1.Item(1, rIndex).Value = oRow("IDTransaccionNotaCreditoAplicacion").ToString
            DataGridView1.Item(2, rIndex).Value = oRow("Comprobante").ToString
            DataGridView1.Item(3, rIndex).Value = CSistema.FormatoMoneda(oRow("Importe").ToString)
            DataGridView1.Item(4, rIndex).Value = oRow("ES").ToString


        Next

        txtFacturasNotaCredito.txt.Text = CSistema.dtSumColumn(dtFacturaNotaCredito, "Importe")

        'Formato
        DataGridView1.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub DarEntrada()

        ctrError.Clear()
        tsslEstado.Text = ""


        'Validar
        Dim FacturasNotaCredito As Decimal = txtFacturasNotaCredito.ObtenerValor
        Dim TotalNotaCredito As Decimal = txtTotalNotaCredito.ObtenerValor

        If FacturasNotaCredito >= TotalNotaCredito Then
            Dim mensaje As String = "El monto de la factura no puede ser mayor al monto total de la nota de credito!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If DataGridView2.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim NewRow As DataRow = dtFacturaNotaCredito.NewRow
        Dim vIDTransaccion As Integer = DataGridView2.SelectedRows(0).Cells("colIDTransaccion1").Value
        Dim vIDTransaccionNotaCreditoAplicacion As Integer = DataGridView2.SelectedRows(0).Cells("ColIDTransaccionNotaCreditoAplicacion1").Value

        'For Each oRow As DataRow In dtVentasPendientes.Select("IDTransaccionNotaCreditoAplicacion =" & vIDTransaccionNotaCreditoAplicacion)
        '    If oRow("Tipo") <> "FacturaNotaCredito" Then
        '        vIDTransaccionNotaCreditoAplicacion = 0
        '    End If
        'Next

        Dim total As Integer
        'Agregar a a la tabla Facturas de Nota de Creditos
        For Each oRow As DataRow In dtVentasPendientes.Select(" IDTransaccionVenta = " & vIDTransaccion & " And IDTransaccionNotaCreditoAplicacion =" & vIDTransaccionNotaCreditoAplicacion)
            total = txtTotalNotaCredito.ObtenerValor - txtFacturasNotaCredito.ObtenerValor

            'Verificar que una factura que se dio salida no se pueda ingresar de nuevo
            'If oRow("ES") = "N" Then
            'Dim mensaje As String = "Se quiere dar entrada a una factuara que se dio salida!"
            'ctrError.SetError(btnGuardar, mensaje)
            'ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            'tsslEstado.Text = mensaje
            'Exit Sub
            'End If
            Dim vImporteSaldoFactura As Integer = oRow("Importe")
            Dim vImporteInsertarNotaCredito As Integer
            If total = 0 Then
                oRow("Importe") = oRow("Importe")

            Else
                If total > oRow("Importe") Then
                    oRow("Importe") = oRow("Importe")
                Else
                    oRow("Importe") = total
                End If
            End If


            'Validar que no se de entrada a una factura que ya esta asociada a la Nota de Credito
            'IDTransaccionNotaCredito
            'DataGridView2.SelectedRows(0).Cells("colIDTransaccion1").Value
            If (CType(CSistema.ExecuteScalar("Select count(*) from VNotaCreditoVentaAplicacion where Aplicado = 'True' and IDTransaccion =" & IDTransaccionNotaCredito & " and IDTransaccionVenta =" & DataGridView2.SelectedRows(0).Cells("colIDTransaccion1").Value & " "), Integer) <> 0) And (oRow("Importe") = txtTotalNotaCredito.ObtenerValor) Then
                Dim mensaje As String = "No se puede dar entrada a la Factura inicial con el mismo monto!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'Se pone VentaPendiente para que inserte el porcentaje de la factura inicial asiganada a la nota de credito.
            If vImporteInsertarNotaCredito < vImporteSaldoFactura Then
                oRow("Tipo") = "VentaPendiente"
            End If

            NewRow.ItemArray = oRow.ItemArray
            vImporteInsertarNotaCredito = oRow("Importe")
            oRow("Importe") = vImporteSaldoFactura
            If vImporteInsertarNotaCredito < vImporteSaldoFactura Then
                oRow("Tipo") = "FacturaNotaCredito"
            End If
            dtFacturaNotaCredito.Rows.Add(NewRow)
            Exit For
        Next

        'Eliminar de la tabla Ventas Pendientes
        For Each oRow As DataRow In dtVentasPendientes.Select(" IDTransaccionVenta = " & vIDTransaccion)
            If oRow("ES") = "S" Then
                dtVentasPendientes.Rows.Remove(oRow)
            End If

        Next

        ListarVentas()
        ListarFacturaNotaCredito()

    End Sub

    Sub DarSalida()

        'Validar
        If DataGridView1.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim NewRow As DataRow = dtVentasPendientes.NewRow
        Dim vIDTransaccion As Integer = DataGridView1.SelectedRows(0).Cells("colIDTransaccion").Value

        If (CType(CSistema.ExecuteScalar("Select saldo from Venta where IDTransaccion =" & vIDTransaccion), Integer) = 0) Then
            If MessageBox.Show("Atencion! La Factura ya esta cancelada. Desea continuar?", "Factura Cancelada", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If




        'Eliminar de la tabla de Ventas pendientes si ya existe la misma factura
        For Each oRow As DataRow In dtVentasPendientes.Select(" IDTransaccionVenta = " & vIDTransaccion)
            dtVentasPendientes.Rows.Remove(oRow)
        Next

        'Agregar a a la tabla  Ventas Pendientes 
        For Each oRow As DataRow In dtFacturaNotaCredito.Select(" IDTransaccionVenta = " & vIDTransaccion)
            oRow("Importe") = oRow("Saldo")
            NewRow.ItemArray = oRow.ItemArray
            dtVentasPendientes.Rows.Add(NewRow)
            Exit For
        Next


        'Eliminar de la tabla Facturas de Nota de Creditos
        For Each oRow As DataRow In dtFacturaNotaCredito.Select(" IDTransaccionVenta = " & vIDTransaccion)
            dtFacturaNotaCredito.Rows.Remove(oRow)
        Next

        ListarVentas()
        ListarFacturaNotaCredito()

    End Sub

    Function InsertarDetalle(ByRef IDTransaccion As Integer) As Boolean
        InsertarDetalle = True
        Dim id As Integer = 0

        For Each oRow As DataRow In dtVentasPendientes.Rows

            'For i As Integer = 0 To dtVentasPendientes.Columns.Count - 1
            '    MsgBox(dtVentasPendientes.Columns(i).ColumnName & ":" & oRow(i).ToString)
            'Next

            Dim Tipo As String = oRow("Tipo")

            If Tipo = "FacturaNotaCredito" Then
                Dim sql As String
                sql = "Insert Into DetalleTransferenciaNotaCredito (IDTransaccionTransferenciaNotaCredito, ID, Entrante, Saliente, IDTransaccionVenta, Importe, IDTransaccionNotaCreditoAplicacion ) Values (" & IDTransaccion & "," & id & ",'False','True','" & oRow("IDTransaccionVenta").ToString & "', " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & oRow("IDTransaccionNotaCreditoAplicacion").ToString & " )"
                If CSistema.ExecuteNonQuery(sql) = 0 Then
                    Return False
                End If
                id = id + 1
            End If

        Next

        For Each oRow As DataRow In dtFacturaNotaCredito.Rows
            'For i As Integer = 0 To dtFacturaNotaCredito.Columns.Count - 1
            '    MsgBox(dtFacturaNotaCredito.Columns(i).ColumnName & ":" & oRow(i))
            'Next

            Dim Tipo As String = oRow("Tipo")

            If Tipo = "VentaPendiente" Then
                Dim sql As String
                sql = "Insert Into DetalleTransferenciaNotaCredito (IDTransaccionTransferenciaNotaCredito, ID, Entrante, Saliente, IDTransaccionVenta, Importe, IDTransaccionNotaCreditoAplicacion ) Values (" & IDTransaccion & "," & id & ",'True','False','" & oRow("IDTransaccionVenta").ToString & "'," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & oRow("IDTransaccionNotaCreditoAplicacion").ToString & ")"
                If CSistema.ExecuteNonQuery(sql) = 0 Then
                    Return False
                End If
                id = id + 1
            End If

        Next

    End Function

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)


        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From TransferenciaNotaCredito Where Numero=" & txtID.ObtenerValor & "And IDSucursal = " & cbxSucursal.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtComprobante, mensaje)
            ctrError.SetIconAlignment(txtComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA, btnNuevo, btnGuardar, btnCancelar, New Button, New Button, btnBusquedaAvanzada, New Button, vControles)



        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VNotaCreditoTransferencia Where IDTransaccion=" & IDTransaccion)


        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtComprobante, mensaje)
            ctrError.SetIconAlignment(txtComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)
        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        txtCliente.SetValue(oRow("IDCliente").ToString)
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtFecha.SetValueFromString(oRow("Fecha").ToString)
        txtCotizacion.txt.Text = CSistema.FormatoMoneda(oRow("Cotizacion"))
        txtObservacion.txt.Text = oRow("Observacion").ToString
       
        'Cargamos Nota de Credito y Facturas
        'Seleccionar desde TransferenciaNotaCredito IDTransaccionNOtaCredito
        dtCargarNotaCredito = CSistema.ExecuteToDataTable("Select TNC.IDTransaccion , N.IDTransaccion, N .Fec , N .Comprobante, N .Moneda, N.Cotizacion, N.Observacion, N.Total, N.Saldo, N.Aplicado, 'Sel'='True'  From TransferenciaNotaCredito  TNC Join VNotaCredito N On TNC.IDTransaccionNotaCredito =N.IDTransaccion Where TNC.IDTransaccion=" & IDTransaccion).Copy
        If dtCargarNotaCredito IsNot Nothing Then
            If dtCargarNotaCredito.Rows.Count > 0 Then
                IDTransaccionNotaCredito = dtCargarNotaCredito.Rows(0)("IDTransaccion")
            End If
        End If

        CargarNotaCreditoAplicar()

        dtVentasPendientes = CSistema.ExecuteToDataTable("Select 'ES'='S',DTNC.IDTransaccionTransferenciaNotaCredito,DTNC.IDtransaccionVenta,DTNC.IDTransaccionNotaCreditoAplicacion, V.Comprobante, DTNC.Importe from DetalleTransferenciaNotaCredito DTNC join Venta V On DTNC.IDTransaccionVenta=V.IDTransaccion Where DTNC.Entrante=0 And DTNC.IDTransaccionTransferenciaNotaCredito = " & IDTransaccion).Copy
        ListarVentas()

        dtFacturaNotaCredito = CSistema.ExecuteToDataTable("Select 'ES'='N',DTNC.IDTransaccionTransferenciaNotaCredito, DTNC.IDTransaccionNotaCreditoAplicacion, DTNC.IDtransaccionVenta,  V.Comprobante, DTNC.Importe  from DetalleTransferenciaNotaCredito DTNC join Venta V On DTNC.IDTransaccionVenta=V.IDTransaccion Where DTNC.Saliente=0 And DTNC.IDTransaccionTransferenciaNotaCredito =" & IDTransaccion).Copy
        ListarFacturaNotaCredito()

        lblFacturaPendiente.Visible = False
        lblFacturaSaliente.Visible = True
        lblFacturasNotaCredito.Visible = False
        lblFacturaEntrante.Visible = True
        DataGridView1.ReadOnly = True
        btnTranferirFacturaPendiente.Enabled = False
        btnTransferirFacturaNotaCredito.Enabled = False

    End Sub

    Private Sub frmNotaCreditoTransferencia_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmNotaCreditoTransferencia_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()

    End Sub

    Private Sub bntAgregarNotacredito_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bntAgregarNotacredito.Click
        SeleccionarNotaCredito()
    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado
        SeleccionarCliente()
    End Sub

    Private Sub btnTranferirFacturaPendiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTranferirFacturaPendiente.Click
        DarEntrada()
    End Sub

    Private Sub btnTransferirFacturaNotaCredito_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTransferirFacturaNotaCredito.Click
        DarSalida()
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged
        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VNotaCreditoTransferencia Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VNotaCreditoTransferencia Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub DataGridView1_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellEndEdit
        If DataGridView1.Columns(e.ColumnIndex).Name = "colImporte" Then

            If IsNumeric(DataGridView1.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(DataGridView1, mensaje)
                ctrError.SetIconAlignment(DataGridView1, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                DataGridView1.CurrentCell.Value = DataGridView1.CurrentRow.Cells("colImporte").Value

            Else
                DataGridView1.CurrentCell.Value = CSistema.FormatoMoneda(DataGridView1.CurrentRow.Cells("colImporte").Value)
            End If

            For Each oRow As DataGridViewRow In DataGridView1.Rows
                Dim IDTransaccion As Integer = oRow.Cells("colIDTransaccion").Value
                For Each oRow1 As DataRow In dtFacturaNotaCredito.Select("IDTransaccionVenta  =" & IDTransaccion)
                    oRow1("Importe") = oRow.Cells("colImporte").Value
                Next
            Next
            txtFacturasNotaCredito.txt.Text = CSistema.dtSumColumn(dtFacturaNotaCredito, "Importe")

        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmNotaCreditoTransferencia_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Enter Then

            If txtCliente.Focused = True Then
                Exit Sub
            End If

        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    '10-06-2021 - SC - Actualiza datos
    Sub frmNotaCreditoTransferencia_Activate()
        Me.Refresh()
    End Sub
End Class
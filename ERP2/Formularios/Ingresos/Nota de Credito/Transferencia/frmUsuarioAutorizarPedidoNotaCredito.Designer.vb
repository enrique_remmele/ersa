﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsuarioAutorizarPedidoNotaCredito
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxDato = New System.Windows.Forms.GroupBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.cbxSubMotivoNotaCredito = New ERP.ocxCBX()
        Me.lblSubMotivo = New System.Windows.Forms.Label()
        Me.cbxMotivoNotaCredito = New ERP.ocxCBX()
        Me.lblMotivo = New System.Windows.Forms.Label()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.dgvUsuario = New System.Windows.Forms.DataGridView()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblUsuario1 = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtMontoLimite = New ERP.ocxTXTNumeric()
        Me.txtPorcentajeLimite = New ERP.ocxTXTNumeric()
        Me.gbxDato.SuspendLayout()
        CType(Me.dgvUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxDato
        '
        Me.gbxDato.Controls.Add(Me.txtPorcentajeLimite)
        Me.gbxDato.Controls.Add(Me.txtMontoLimite)
        Me.gbxDato.Controls.Add(Me.Label3)
        Me.gbxDato.Controls.Add(Me.Label2)
        Me.gbxDato.Controls.Add(Me.Label1)
        Me.gbxDato.Controls.Add(Me.btnCancelar)
        Me.gbxDato.Controls.Add(Me.btnEliminar)
        Me.gbxDato.Controls.Add(Me.btnGuardar)
        Me.gbxDato.Controls.Add(Me.btnEditar)
        Me.gbxDato.Controls.Add(Me.btnNuevo)
        Me.gbxDato.Controls.Add(Me.cbxSubMotivoNotaCredito)
        Me.gbxDato.Controls.Add(Me.lblSubMotivo)
        Me.gbxDato.Controls.Add(Me.cbxMotivoNotaCredito)
        Me.gbxDato.Controls.Add(Me.lblMotivo)
        Me.gbxDato.Controls.Add(Me.cbxUsuario)
        Me.gbxDato.Controls.Add(Me.dgvUsuario)
        Me.gbxDato.Controls.Add(Me.lblID)
        Me.gbxDato.Controls.Add(Me.lblUsuario1)
        Me.gbxDato.Controls.Add(Me.lblEstado)
        Me.gbxDato.Controls.Add(Me.txtID)
        Me.gbxDato.Controls.Add(Me.rdbActivo)
        Me.gbxDato.Controls.Add(Me.rdbDesactivado)
        Me.gbxDato.Location = New System.Drawing.Point(3, 2)
        Me.gbxDato.Name = "gbxDato"
        Me.gbxDato.Size = New System.Drawing.Size(454, 504)
        Me.gbxDato.TabIndex = 0
        Me.gbxDato.TabStop = False
        Me.gbxDato.Text = "Datos"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(318, 186)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(64, 23)
        Me.btnCancelar.TabIndex = 58
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(382, 186)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(64, 23)
        Me.btnEliminar.TabIndex = 59
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(254, 186)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(64, 23)
        Me.btnGuardar.TabIndex = 55
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(190, 186)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(64, 23)
        Me.btnEditar.TabIndex = 57
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(125, 186)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(64, 23)
        Me.btnNuevo.TabIndex = 56
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'cbxSubMotivoNotaCredito
        '
        Me.cbxSubMotivoNotaCredito.CampoWhere = Nothing
        Me.cbxSubMotivoNotaCredito.CargarUnaSolaVez = False
        Me.cbxSubMotivoNotaCredito.DataDisplayMember = Nothing
        Me.cbxSubMotivoNotaCredito.DataFilter = Nothing
        Me.cbxSubMotivoNotaCredito.DataOrderBy = Nothing
        Me.cbxSubMotivoNotaCredito.DataSource = Nothing
        Me.cbxSubMotivoNotaCredito.DataValueMember = Nothing
        Me.cbxSubMotivoNotaCredito.dtSeleccionado = Nothing
        Me.cbxSubMotivoNotaCredito.FormABM = Nothing
        Me.cbxSubMotivoNotaCredito.Indicaciones = Nothing
        Me.cbxSubMotivoNotaCredito.Location = New System.Drawing.Point(87, 96)
        Me.cbxSubMotivoNotaCredito.Name = "cbxSubMotivoNotaCredito"
        Me.cbxSubMotivoNotaCredito.SeleccionMultiple = False
        Me.cbxSubMotivoNotaCredito.SeleccionObligatoria = False
        Me.cbxSubMotivoNotaCredito.Size = New System.Drawing.Size(262, 23)
        Me.cbxSubMotivoNotaCredito.SoloLectura = False
        Me.cbxSubMotivoNotaCredito.TabIndex = 54
        Me.cbxSubMotivoNotaCredito.Texto = ""
        '
        'lblSubMotivo
        '
        Me.lblSubMotivo.AutoSize = True
        Me.lblSubMotivo.Location = New System.Drawing.Point(9, 102)
        Me.lblSubMotivo.Name = "lblSubMotivo"
        Me.lblSubMotivo.Size = New System.Drawing.Size(64, 13)
        Me.lblSubMotivo.TabIndex = 53
        Me.lblSubMotivo.Text = "Sub Motivo:"
        '
        'cbxMotivoNotaCredito
        '
        Me.cbxMotivoNotaCredito.CampoWhere = Nothing
        Me.cbxMotivoNotaCredito.CargarUnaSolaVez = False
        Me.cbxMotivoNotaCredito.DataDisplayMember = Nothing
        Me.cbxMotivoNotaCredito.DataFilter = Nothing
        Me.cbxMotivoNotaCredito.DataOrderBy = Nothing
        Me.cbxMotivoNotaCredito.DataSource = Nothing
        Me.cbxMotivoNotaCredito.DataValueMember = Nothing
        Me.cbxMotivoNotaCredito.dtSeleccionado = Nothing
        Me.cbxMotivoNotaCredito.FormABM = Nothing
        Me.cbxMotivoNotaCredito.Indicaciones = Nothing
        Me.cbxMotivoNotaCredito.Location = New System.Drawing.Point(87, 67)
        Me.cbxMotivoNotaCredito.Name = "cbxMotivoNotaCredito"
        Me.cbxMotivoNotaCredito.SeleccionMultiple = False
        Me.cbxMotivoNotaCredito.SeleccionObligatoria = False
        Me.cbxMotivoNotaCredito.Size = New System.Drawing.Size(262, 23)
        Me.cbxMotivoNotaCredito.SoloLectura = False
        Me.cbxMotivoNotaCredito.TabIndex = 52
        Me.cbxMotivoNotaCredito.Texto = ""
        '
        'lblMotivo
        '
        Me.lblMotivo.AutoSize = True
        Me.lblMotivo.Location = New System.Drawing.Point(9, 73)
        Me.lblMotivo.Name = "lblMotivo"
        Me.lblMotivo.Size = New System.Drawing.Size(42, 13)
        Me.lblMotivo.TabIndex = 51
        Me.lblMotivo.Text = "Motivo:"
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = Nothing
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = Nothing
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = Nothing
        Me.cbxUsuario.DataSource = Nothing
        Me.cbxUsuario.DataValueMember = Nothing
        Me.cbxUsuario.dtSeleccionado = Nothing
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(87, 38)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionMultiple = False
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(262, 23)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 50
        Me.cbxUsuario.Texto = ""
        '
        'dgvUsuario
        '
        Me.dgvUsuario.AllowUserToAddRows = False
        Me.dgvUsuario.AllowUserToDeleteRows = False
        Me.dgvUsuario.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvUsuario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvUsuario.Location = New System.Drawing.Point(9, 214)
        Me.dgvUsuario.Name = "dgvUsuario"
        Me.dgvUsuario.ReadOnly = True
        Me.dgvUsuario.Size = New System.Drawing.Size(437, 284)
        Me.dgvUsuario.TabIndex = 49
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(9, 18)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 41
        Me.lblID.Text = "ID:"
        '
        'lblUsuario1
        '
        Me.lblUsuario1.AutoSize = True
        Me.lblUsuario1.Location = New System.Drawing.Point(9, 44)
        Me.lblUsuario1.Name = "lblUsuario1"
        Me.lblUsuario1.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuario1.TabIndex = 43
        Me.lblUsuario1.Text = "Usuario:"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(9, 160)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 45
        Me.lblEstado.Text = "Estado:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(87, 13)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 42
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(87, 158)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 46
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(148, 158)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 47
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 130)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 13)
        Me.Label1.TabIndex = 60
        Me.Label1.Text = "Limite:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(84, 130)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 61
        Me.Label2.Text = "Monto:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(234, 130)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 13)
        Me.Label3.TabIndex = 62
        Me.Label3.Text = "Porcentaje:"
        '
        'txtMontoLimite
        '
        Me.txtMontoLimite.Color = System.Drawing.Color.Empty
        Me.txtMontoLimite.Decimales = True
        Me.txtMontoLimite.Indicaciones = Nothing
        Me.txtMontoLimite.Location = New System.Drawing.Point(131, 126)
        Me.txtMontoLimite.Name = "txtMontoLimite"
        Me.txtMontoLimite.Size = New System.Drawing.Size(97, 21)
        Me.txtMontoLimite.SoloLectura = False
        Me.txtMontoLimite.TabIndex = 63
        Me.txtMontoLimite.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMontoLimite.Texto = "0"
        '
        'txtPorcentajeLimite
        '
        Me.txtPorcentajeLimite.Color = System.Drawing.Color.Empty
        Me.txtPorcentajeLimite.Decimales = True
        Me.txtPorcentajeLimite.Indicaciones = Nothing
        Me.txtPorcentajeLimite.Location = New System.Drawing.Point(301, 126)
        Me.txtPorcentajeLimite.Name = "txtPorcentajeLimite"
        Me.txtPorcentajeLimite.Size = New System.Drawing.Size(48, 21)
        Me.txtPorcentajeLimite.SoloLectura = False
        Me.txtPorcentajeLimite.TabIndex = 64
        Me.txtPorcentajeLimite.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajeLimite.Texto = "0"
        '
        'frmUsuarioAutorizarPedidoNotaCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(461, 508)
        Me.Controls.Add(Me.gbxDato)
        Me.Name = "frmUsuarioAutorizarPedidoNotaCredito"
        Me.Text = "UsuarioAutorizarPedidoNotaCredito"
        Me.gbxDato.ResumeLayout(False)
        Me.gbxDato.PerformLayout()
        CType(Me.dgvUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxDato As System.Windows.Forms.GroupBox
    Friend WithEvents dgvUsuario As System.Windows.Forms.DataGridView
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents lblUsuario1 As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents rdbActivo As System.Windows.Forms.RadioButton
    Friend WithEvents rdbDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents cbxMotivoNotaCredito As ERP.ocxCBX
    Friend WithEvents lblMotivo As System.Windows.Forms.Label
    Friend WithEvents cbxUsuario As ERP.ocxCBX
    Friend WithEvents cbxSubMotivoNotaCredito As ERP.ocxCBX
    Friend WithEvents lblSubMotivo As System.Windows.Forms.Label
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents txtPorcentajeLimite As ERP.ocxTXTNumeric
    Friend WithEvents txtMontoLimite As ERP.ocxTXTNumeric
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class

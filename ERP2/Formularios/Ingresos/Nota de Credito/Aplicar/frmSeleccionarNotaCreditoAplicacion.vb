﻿Public Class frmSeleccionarNotaCreditoAplicacion
    Dim CSistema As New CSistema
    Dim CData As New CData

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private SeleccionadoValue As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionadoValue = value
        End Set
    End Property

    Private DecimalesValue As Boolean
    Public Property Decimales() As Boolean
        Get
            Return DecimalesValue
        End Get
        Set(ByVal value As Boolean)
            DecimalesValue = value
        End Set
    End Property

    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        txtImportante.BackColor = Me.BackColor

        'Listar lvListar
        Listar(dt)

    End Sub

    Sub Listar(ByVal dt As DataTable)


        If dt Is Nothing Then
            Exit Sub
        End If

        'Deseleccionar todo
        For Each oRow As DataRow In dt.Rows
            oRow("Sel") = False
        Next

        CSistema.dtToGrid(lvListar, dt)

        'Formato
        lvListar.Columns("IDTransaccion").Visible = False
        lvListar.Columns("Sel").Visible = False
        lvListar.Columns("Moneda").Visible = False
        lvListar.Columns("Cotizacion").Visible = False

        'Ocultar
        If Decimales = True Then
            lvListar.Columns("Total").DefaultCellStyle.Format = "N2"
            lvListar.Columns("Saldo").DefaultCellStyle.Format = "N2"
            lvListar.Columns("Aplicado").DefaultCellStyle.Format = "N2"
        Else
            lvListar.Columns("Total").DefaultCellStyle.Format = "N0"
            lvListar.Columns("Saldo").DefaultCellStyle.Format = "N0"
            lvListar.Columns("Aplicado").DefaultCellStyle.Format = "N0"
        End If
        
        lvListar.Columns("Saldo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        lvListar.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        lvListar.Columns("Aplicado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        lvListar.Columns("Comprobante").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill


    End Sub

    Sub Seleccionar()

        If lvListar.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        IDTransaccion = lvListar.SelectedRows(0).Cells("IDTransaccion").Value
        Seleccionado = True

        For Each oRow As DataRow In dt.Rows
            If oRow("IDTransaccion") = IDTransaccion Then
                oRow("Sel") = True
                Exit For
            End If
        Next

        Me.Close()

    End Sub

    Private Sub frmSeleccionarNotaCreditoAplicacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub BtnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancelar.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        seleccionar()
    End Sub

    Private Sub LvListar_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            Seleccionar()
        End If
    End Sub

    Private Sub txtNroCheque_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroCheque.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then

            Dim dtF As DataTable = dt.Copy

            dtF = CData.FiltrarDataTable(dtF, "Comprobante Like '%" & txtNroCheque.GetValue & "%' ")
            Listar(dtF)

        End If
    End Sub

    Private Sub lvListar_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles lvListar.KeyPress
       
    End Sub

    Private Sub lvListar_KeyUp_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvListar.KeyUp
        If e.KeyCode = Keys.Enter Then
            Seleccionar()
        End If
    End Sub

    Private Sub frmSeleccionarNotaCreditoAplicacion_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Enter Then

            If txtNroCheque.txt.Focused = True Then
                Exit Sub
            End If

            CSistema.SelectNextControl(Me, e.KeyCode)
        End If
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmSeleccionarNotaCreditoAplicacion_Activate()
        Me.Refresh()
    End Sub
End Class
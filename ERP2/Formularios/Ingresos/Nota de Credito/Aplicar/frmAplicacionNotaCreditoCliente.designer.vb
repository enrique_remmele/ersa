﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAplicacionNotaCreditoCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.OcxCotizacion1 = New ERP.ocxCotizacion()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.gbxNotaCreditoAplicar = New System.Windows.Forms.GroupBox()
        Me.txtDebitoNotaCredito = New ERP.ocxTXTNumeric()
        Me.txtSaldoNotaCredito = New ERP.ocxTXTNumeric()
        Me.txtTotalNotaCredito = New ERP.ocxTXTNumeric()
        Me.txtMonedaNotaCredito = New ERP.ocxTXTString()
        Me.txtFechaNotaCredito = New ERP.ocxTXTString()
        Me.bntAgregarNotacredito = New System.Windows.Forms.Button()
        Me.lblDebito = New System.Windows.Forms.Label()
        Me.LblSaldo = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtObservacionNotaCredito = New ERP.ocxTXTString()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblMonedaNotaCredito = New System.Windows.Forms.Label()
        Me.txtCotizacionNC = New ERP.ocxTXTNumeric()
        Me.lblFechaNotaCredito = New System.Windows.Forms.Label()
        Me.txtComprobanteNotaCredito = New ERP.ocxTXTString()
        Me.gbxComprobantes = New System.Windows.Forms.GroupBox()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colIDtransaccio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCotizacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCobrado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDescontado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtTotalVenta = New ERP.ocxTXTNumeric()
        Me.lblTotalVenta = New System.Windows.Forms.Label()
        Me.lklEliminarFactura = New System.Windows.Forms.LinkLabel()
        Me.btnAgregarFactura = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblSaldoTotal = New System.Windows.Forms.Label()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.txtSaldoTotal = New ERP.ocxTXTNumeric()
        Me.gbxCabecera.SuspendLayout()
        Me.gbxNotaCreditoAplicar.SuspendLayout()
        Me.gbxComprobantes.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.OcxCotizacion1)
        Me.gbxCabecera.Controls.Add(Me.txtCliente)
        Me.gbxCabecera.Controls.Add(Me.lblCliente)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Location = New System.Drawing.Point(4, 3)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(684, 106)
        Me.gbxCabecera.TabIndex = 0
        Me.gbxCabecera.TabStop = False
        '
        'OcxCotizacion1
        '
        Me.OcxCotizacion1.dt = Nothing
        Me.OcxCotizacion1.FiltroFecha = Nothing
        Me.OcxCotizacion1.Location = New System.Drawing.Point(504, 37)
        Me.OcxCotizacion1.Name = "OcxCotizacion1"
        Me.OcxCotizacion1.Registro = Nothing
        Me.OcxCotizacion1.Saltar = False
        Me.OcxCotizacion1.Seleccionado = True
        Me.OcxCotizacion1.Size = New System.Drawing.Size(148, 28)
        Me.OcxCotizacion1.SoloLectura = False
        Me.OcxCotizacion1.TabIndex = 37
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 117
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.ControlCorto = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(76, 37)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(365, 23)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 11
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(5, 43)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(42, 13)
        Me.lblCliente.TabIndex = 10
        Me.lblCliente.Text = "Cliente:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(76, 70)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(599, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 16
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(449, 45)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 12
        Me.lblMoneda.Text = "Moneda:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(229, 12)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(98, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 3, 15, 8, 54, 12, 828)
        Me.txtFecha.Location = New System.Drawing.Point(599, 11)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 9
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(560, 14)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 8
        Me.lblFecha.Text = "Fecha:"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(367, 10)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(63, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 6
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(434, 10)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(126, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 7
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(76, 12)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(63, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(139, 12)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(59, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(5, 74)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 15
        Me.lblObservacion.Text = "Observacion:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(198, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(32, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Suc.:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(326, 14)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(40, 13)
        Me.lblComprobante.TabIndex = 5
        Me.lblComprobante.Text = "Comp.:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(5, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'gbxNotaCreditoAplicar
        '
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.txtDebitoNotaCredito)
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.txtSaldoNotaCredito)
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.txtTotalNotaCredito)
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.txtMonedaNotaCredito)
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.txtFechaNotaCredito)
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.bntAgregarNotacredito)
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.lblDebito)
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.LblSaldo)
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.lblTotal)
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.Label4)
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.txtObservacionNotaCredito)
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.Label3)
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.lblMonedaNotaCredito)
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.txtCotizacionNC)
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.lblFechaNotaCredito)
        Me.gbxNotaCreditoAplicar.Controls.Add(Me.txtComprobanteNotaCredito)
        Me.gbxNotaCreditoAplicar.Location = New System.Drawing.Point(4, 115)
        Me.gbxNotaCreditoAplicar.Name = "gbxNotaCreditoAplicar"
        Me.gbxNotaCreditoAplicar.Size = New System.Drawing.Size(684, 114)
        Me.gbxNotaCreditoAplicar.TabIndex = 1
        Me.gbxNotaCreditoAplicar.TabStop = False
        Me.gbxNotaCreditoAplicar.Text = "Nota Credito a Aplicar"
        '
        'txtDebitoNotaCredito
        '
        Me.txtDebitoNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtDebitoNotaCredito.Decimales = False
        Me.txtDebitoNotaCredito.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtDebitoNotaCredito.Location = New System.Drawing.Point(560, 84)
        Me.txtDebitoNotaCredito.Name = "txtDebitoNotaCredito"
        Me.txtDebitoNotaCredito.Size = New System.Drawing.Size(111, 21)
        Me.txtDebitoNotaCredito.SoloLectura = False
        Me.txtDebitoNotaCredito.TabIndex = 16
        Me.txtDebitoNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDebitoNotaCredito.Texto = "0"
        '
        'txtSaldoNotaCredito
        '
        Me.txtSaldoNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtSaldoNotaCredito.Decimales = False
        Me.txtSaldoNotaCredito.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtSaldoNotaCredito.Location = New System.Drawing.Point(256, 84)
        Me.txtSaldoNotaCredito.Name = "txtSaldoNotaCredito"
        Me.txtSaldoNotaCredito.Size = New System.Drawing.Size(104, 21)
        Me.txtSaldoNotaCredito.SoloLectura = True
        Me.txtSaldoNotaCredito.TabIndex = 14
        Me.txtSaldoNotaCredito.TabStop = False
        Me.txtSaldoNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoNotaCredito.Texto = "0"
        '
        'txtTotalNotaCredito
        '
        Me.txtTotalNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtTotalNotaCredito.Decimales = False
        Me.txtTotalNotaCredito.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotalNotaCredito.Location = New System.Drawing.Point(75, 84)
        Me.txtTotalNotaCredito.Name = "txtTotalNotaCredito"
        Me.txtTotalNotaCredito.Size = New System.Drawing.Size(102, 21)
        Me.txtTotalNotaCredito.SoloLectura = True
        Me.txtTotalNotaCredito.TabIndex = 12
        Me.txtTotalNotaCredito.TabStop = False
        Me.txtTotalNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalNotaCredito.Texto = "0"
        '
        'txtMonedaNotaCredito
        '
        Me.txtMonedaNotaCredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMonedaNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtMonedaNotaCredito.Indicaciones = Nothing
        Me.txtMonedaNotaCredito.Location = New System.Drawing.Point(520, 30)
        Me.txtMonedaNotaCredito.Multilinea = False
        Me.txtMonedaNotaCredito.Name = "txtMonedaNotaCredito"
        Me.txtMonedaNotaCredito.Size = New System.Drawing.Size(67, 21)
        Me.txtMonedaNotaCredito.SoloLectura = True
        Me.txtMonedaNotaCredito.TabIndex = 7
        Me.txtMonedaNotaCredito.TabStop = False
        Me.txtMonedaNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMonedaNotaCredito.Texto = ""
        '
        'txtFechaNotaCredito
        '
        Me.txtFechaNotaCredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFechaNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtFechaNotaCredito.Indicaciones = Nothing
        Me.txtFechaNotaCredito.Location = New System.Drawing.Point(394, 30)
        Me.txtFechaNotaCredito.Multilinea = False
        Me.txtFechaNotaCredito.Name = "txtFechaNotaCredito"
        Me.txtFechaNotaCredito.Size = New System.Drawing.Size(77, 21)
        Me.txtFechaNotaCredito.SoloLectura = True
        Me.txtFechaNotaCredito.TabIndex = 5
        Me.txtFechaNotaCredito.TabStop = False
        Me.txtFechaNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFechaNotaCredito.Texto = ""
        '
        'bntAgregarNotacredito
        '
        Me.bntAgregarNotacredito.Location = New System.Drawing.Point(235, 28)
        Me.bntAgregarNotacredito.Name = "bntAgregarNotacredito"
        Me.bntAgregarNotacredito.Size = New System.Drawing.Size(119, 22)
        Me.bntAgregarNotacredito.TabIndex = 3
        Me.bntAgregarNotacredito.Text = "Seleccionar"
        Me.bntAgregarNotacredito.UseVisualStyleBackColor = True
        '
        'lblDebito
        '
        Me.lblDebito.AutoSize = True
        Me.lblDebito.Location = New System.Drawing.Point(513, 88)
        Me.lblDebito.Name = "lblDebito"
        Me.lblDebito.Size = New System.Drawing.Size(41, 13)
        Me.lblDebito.TabIndex = 15
        Me.lblDebito.Text = "Debito:"
        '
        'LblSaldo
        '
        Me.LblSaldo.AutoSize = True
        Me.LblSaldo.Location = New System.Drawing.Point(213, 88)
        Me.LblSaldo.Name = "LblSaldo"
        Me.LblSaldo.Size = New System.Drawing.Size(37, 13)
        Me.LblSaldo.TabIndex = 13
        Me.LblSaldo.Text = "Saldo:"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(2, 88)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(34, 13)
        Me.lblTotal.TabIndex = 11
        Me.lblTotal.Text = "Total:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(2, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Comprobante:"
        '
        'txtObservacionNotaCredito
        '
        Me.txtObservacionNotaCredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacionNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtObservacionNotaCredito.Indicaciones = Nothing
        Me.txtObservacionNotaCredito.Location = New System.Drawing.Point(75, 57)
        Me.txtObservacionNotaCredito.Multilinea = False
        Me.txtObservacionNotaCredito.Name = "txtObservacionNotaCredito"
        Me.txtObservacionNotaCredito.Size = New System.Drawing.Size(595, 21)
        Me.txtObservacionNotaCredito.SoloLectura = True
        Me.txtObservacionNotaCredito.TabIndex = 10
        Me.txtObservacionNotaCredito.TabStop = False
        Me.txtObservacionNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacionNotaCredito.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(2, 61)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Observacion:"
        '
        'lblMonedaNotaCredito
        '
        Me.lblMonedaNotaCredito.AutoSize = True
        Me.lblMonedaNotaCredito.Location = New System.Drawing.Point(471, 34)
        Me.lblMonedaNotaCredito.Name = "lblMonedaNotaCredito"
        Me.lblMonedaNotaCredito.Size = New System.Drawing.Size(49, 13)
        Me.lblMonedaNotaCredito.TabIndex = 6
        Me.lblMonedaNotaCredito.Text = "Moneda:"
        '
        'txtCotizacionNC
        '
        Me.txtCotizacionNC.Color = System.Drawing.Color.Empty
        Me.txtCotizacionNC.Decimales = False
        Me.txtCotizacionNC.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacionNC.Location = New System.Drawing.Point(587, 30)
        Me.txtCotizacionNC.Name = "txtCotizacionNC"
        Me.txtCotizacionNC.Size = New System.Drawing.Size(84, 21)
        Me.txtCotizacionNC.SoloLectura = True
        Me.txtCotizacionNC.TabIndex = 8
        Me.txtCotizacionNC.TabStop = False
        Me.txtCotizacionNC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacionNC.Texto = "0"
        '
        'lblFechaNotaCredito
        '
        Me.lblFechaNotaCredito.AutoSize = True
        Me.lblFechaNotaCredito.Location = New System.Drawing.Point(354, 34)
        Me.lblFechaNotaCredito.Name = "lblFechaNotaCredito"
        Me.lblFechaNotaCredito.Size = New System.Drawing.Size(40, 13)
        Me.lblFechaNotaCredito.TabIndex = 4
        Me.lblFechaNotaCredito.Text = "Fecha:"
        '
        'txtComprobanteNotaCredito
        '
        Me.txtComprobanteNotaCredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobanteNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtComprobanteNotaCredito.Indicaciones = Nothing
        Me.txtComprobanteNotaCredito.Location = New System.Drawing.Point(75, 30)
        Me.txtComprobanteNotaCredito.Multilinea = False
        Me.txtComprobanteNotaCredito.Name = "txtComprobanteNotaCredito"
        Me.txtComprobanteNotaCredito.Size = New System.Drawing.Size(154, 21)
        Me.txtComprobanteNotaCredito.SoloLectura = True
        Me.txtComprobanteNotaCredito.TabIndex = 2
        Me.txtComprobanteNotaCredito.TabStop = False
        Me.txtComprobanteNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobanteNotaCredito.Texto = ""
        '
        'gbxComprobantes
        '
        Me.gbxComprobantes.Controls.Add(Me.dgw)
        Me.gbxComprobantes.Controls.Add(Me.txtTotalVenta)
        Me.gbxComprobantes.Controls.Add(Me.lblTotalVenta)
        Me.gbxComprobantes.Controls.Add(Me.lklEliminarFactura)
        Me.gbxComprobantes.Controls.Add(Me.btnAgregarFactura)
        Me.gbxComprobantes.Location = New System.Drawing.Point(4, 235)
        Me.gbxComprobantes.Name = "gbxComprobantes"
        Me.gbxComprobantes.Size = New System.Drawing.Size(684, 198)
        Me.gbxComprobantes.TabIndex = 2
        Me.gbxComprobantes.TabStop = False
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        Me.dgw.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDtransaccio, Me.colComprobante, Me.colMoneda, Me.colCotizacion, Me.colTotal, Me.colCobrado, Me.colDescontado, Me.colSaldo, Me.colImporte})
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgw.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgw.Location = New System.Drawing.Point(8, 41)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(670, 114)
        Me.dgw.TabIndex = 6
        Me.dgw.TabStop = False
        '
        'colIDtransaccio
        '
        Me.colIDtransaccio.HeaderText = "IDTransaccion"
        Me.colIDtransaccio.Name = "colIDtransaccio"
        Me.colIDtransaccio.ReadOnly = True
        Me.colIDtransaccio.Visible = False
        Me.colIDtransaccio.Width = 5
        '
        'colComprobante
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colComprobante.DefaultCellStyle = DataGridViewCellStyle3
        Me.colComprobante.HeaderText = "Comprobante"
        Me.colComprobante.Name = "colComprobante"
        Me.colComprobante.ReadOnly = True
        Me.colComprobante.Width = 110
        '
        'colMoneda
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colMoneda.DefaultCellStyle = DataGridViewCellStyle4
        Me.colMoneda.HeaderText = "Moneda"
        Me.colMoneda.Name = "colMoneda"
        Me.colMoneda.ReadOnly = True
        Me.colMoneda.Width = 52
        '
        'colCotizacion
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colCotizacion.DefaultCellStyle = DataGridViewCellStyle5
        Me.colCotizacion.HeaderText = "Coti."
        Me.colCotizacion.Name = "colCotizacion"
        Me.colCotizacion.ReadOnly = True
        Me.colCotizacion.Width = 45
        '
        'colTotal
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colTotal.DefaultCellStyle = DataGridViewCellStyle6
        Me.colTotal.HeaderText = "Total"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.ReadOnly = True
        Me.colTotal.Width = 111
        '
        'colCobrado
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.Format = "N0"
        DataGridViewCellStyle7.NullValue = "0"
        Me.colCobrado.DefaultCellStyle = DataGridViewCellStyle7
        Me.colCobrado.HeaderText = "Cobrado"
        Me.colCobrado.Name = "colCobrado"
        Me.colCobrado.ReadOnly = True
        Me.colCobrado.Width = 111
        '
        'colDescontado
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colDescontado.DefaultCellStyle = DataGridViewCellStyle8
        Me.colDescontado.HeaderText = "Descontado"
        Me.colDescontado.Name = "colDescontado"
        Me.colDescontado.ReadOnly = True
        Me.colDescontado.Width = 111
        '
        'colSaldo
        '
        Me.colSaldo.HeaderText = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.ReadOnly = True
        Me.colSaldo.Visible = False
        Me.colSaldo.Width = 111
        '
        'colImporte
        '
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        Me.colImporte.Width = 111
        '
        'txtTotalVenta
        '
        Me.txtTotalVenta.Color = System.Drawing.Color.Empty
        Me.txtTotalVenta.Decimales = True
        Me.txtTotalVenta.Indicaciones = Nothing
        Me.txtTotalVenta.Location = New System.Drawing.Point(575, 162)
        Me.txtTotalVenta.Name = "txtTotalVenta"
        Me.txtTotalVenta.Size = New System.Drawing.Size(92, 22)
        Me.txtTotalVenta.SoloLectura = True
        Me.txtTotalVenta.TabIndex = 5
        Me.txtTotalVenta.TabStop = False
        Me.txtTotalVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalVenta.Texto = "0"
        '
        'lblTotalVenta
        '
        Me.lblTotalVenta.AutoSize = True
        Me.lblTotalVenta.Location = New System.Drawing.Point(536, 167)
        Me.lblTotalVenta.Name = "lblTotalVenta"
        Me.lblTotalVenta.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalVenta.TabIndex = 4
        Me.lblTotalVenta.Text = "Total:"
        '
        'lklEliminarFactura
        '
        Me.lklEliminarFactura.AutoSize = True
        Me.lklEliminarFactura.Location = New System.Drawing.Point(5, 171)
        Me.lklEliminarFactura.Name = "lklEliminarFactura"
        Me.lklEliminarFactura.Size = New System.Drawing.Size(108, 13)
        Me.lklEliminarFactura.TabIndex = 3
        Me.lklEliminarFactura.TabStop = True
        Me.lklEliminarFactura.Text = "Eliminar comprobante"
        '
        'btnAgregarFactura
        '
        Me.btnAgregarFactura.Location = New System.Drawing.Point(11, 12)
        Me.btnAgregarFactura.Name = "btnAgregarFactura"
        Me.btnAgregarFactura.Size = New System.Drawing.Size(146, 24)
        Me.btnAgregarFactura.TabIndex = 1
        Me.btnAgregarFactura.Text = "Agregar Comprobantes"
        Me.btnAgregarFactura.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(4, 470)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 7
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(85, 470)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 8
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(278, 470)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 10
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(166, 470)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(75, 23)
        Me.btnBusquedaAvanzada.TabIndex = 9
        Me.btnBusquedaAvanzada.Text = "&Busqueda"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(613, 470)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 14
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(521, 470)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 13
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(440, 470)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 12
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(359, 470)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 11
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 506)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(696, 22)
        Me.StatusStrip1.TabIndex = 15
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'lblSaldoTotal
        '
        Me.lblSaldoTotal.AutoSize = True
        Me.lblSaldoTotal.Location = New System.Drawing.Point(550, 447)
        Me.lblSaldoTotal.Name = "lblSaldoTotal"
        Me.lblSaldoTotal.Size = New System.Drawing.Size(37, 13)
        Me.lblSaldoTotal.TabIndex = 4
        Me.lblSaldoTotal.Text = "Saldo:"
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(272, 442)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(257, 20)
        Me.flpAnuladoPor.TabIndex = 6
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(4, 442)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(262, 20)
        Me.flpRegistradoPor.TabIndex = 3
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(4, 470)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 16
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        Me.btnEliminar.Visible = False
        '
        'txtSaldoTotal
        '
        Me.txtSaldoTotal.Color = System.Drawing.Color.Empty
        Me.txtSaldoTotal.Decimales = True
        Me.txtSaldoTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldoTotal.Indicaciones = Nothing
        Me.txtSaldoTotal.Location = New System.Drawing.Point(591, 442)
        Me.txtSaldoTotal.Name = "txtSaldoTotal"
        Me.txtSaldoTotal.Size = New System.Drawing.Size(84, 22)
        Me.txtSaldoTotal.SoloLectura = True
        Me.txtSaldoTotal.TabIndex = 5
        Me.txtSaldoTotal.TabStop = False
        Me.txtSaldoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoTotal.Texto = "0"
        '
        'frmAplicacionNotaCreditoCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(696, 528)
        Me.Controls.Add(Me.flpAnuladoPor)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.txtSaldoTotal)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.lblSaldoTotal)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.gbxComprobantes)
        Me.Controls.Add(Me.gbxNotaCreditoAplicar)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Controls.Add(Me.btnEliminar)
        Me.KeyPreview = True
        Me.Name = "frmAplicacionNotaCreditoCliente"
        Me.Text = "frmAplicacionNotaCredito"
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.gbxNotaCreditoAplicar.ResumeLayout(False)
        Me.gbxNotaCreditoAplicar.PerformLayout()
        Me.gbxComprobantes.ResumeLayout(False)
        Me.gbxComprobantes.PerformLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents gbxNotaCreditoAplicar As System.Windows.Forms.GroupBox
    Friend WithEvents lblDebito As System.Windows.Forms.Label
    Friend WithEvents LblSaldo As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtObservacionNotaCredito As ERP.ocxTXTString
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblMonedaNotaCredito As System.Windows.Forms.Label
    Friend WithEvents txtCotizacionNC As ERP.ocxTXTNumeric
    Friend WithEvents lblFechaNotaCredito As System.Windows.Forms.Label
    Friend WithEvents txtComprobanteNotaCredito As ERP.ocxTXTString
    Friend WithEvents gbxComprobantes As System.Windows.Forms.GroupBox
    Friend WithEvents txtTotalVenta As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalVenta As System.Windows.Forms.Label
    Friend WithEvents lklEliminarFactura As System.Windows.Forms.LinkLabel
    Friend WithEvents btnAgregarFactura As System.Windows.Forms.Button
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents bntAgregarNotacredito As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents txtFechaNotaCredito As ERP.ocxTXTString
    Friend WithEvents txtMonedaNotaCredito As ERP.ocxTXTString
    Friend WithEvents txtDebitoNotaCredito As ERP.ocxTXTNumeric
    Friend WithEvents txtSaldoNotaCredito As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalNotaCredito As ERP.ocxTXTNumeric
    Friend WithEvents txtSaldoTotal As ERP.ocxTXTNumeric
    Friend WithEvents lblSaldoTotal As System.Windows.Forms.Label
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents colIDtransaccio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colComprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colMoneda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCotizacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCobrado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDescontado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSaldo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents OcxCotizacion1 As ERP.ocxCotizacion
End Class

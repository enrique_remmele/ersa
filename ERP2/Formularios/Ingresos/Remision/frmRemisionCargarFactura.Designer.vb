﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRemisionCargarFactura
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCargarMercaderias = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblNroComprobante = New System.Windows.Forms.Label()
        Me.txtNroComprobante = New ERP.ocxTXTString()
        Me.lklComprobantesMes = New System.Windows.Forms.LinkLabel()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.btnCargarComprobante = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lklQuitarSeleccionados = New System.Windows.Forms.LinkLabel()
        Me.lblCantidadDetalle = New System.Windows.Forms.Label()
        Me.txtCantidadDetalle = New ERP.ocxTXTString()
        Me.dgvComprobante = New System.Windows.Forms.DataGridView()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lklSeleccionarTodo = New System.Windows.Forms.LinkLabel()
        Me.lblCantidadComprobante = New System.Windows.Forms.Label()
        Me.txtCantidadComprobante = New ERP.ocxTXTString()
        Me.IDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Sucursal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Comprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Direccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.dgvComprobante, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.02041!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.97959!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel5, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvDetalle, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvComprobante, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(644, 554)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.Controls.Add(Me.btnCargarMercaderias)
        Me.FlowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel5.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(331, 261)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(310, 28)
        Me.FlowLayoutPanel5.TabIndex = 3
        '
        'btnCargarMercaderias
        '
        Me.btnCargarMercaderias.Location = New System.Drawing.Point(9, 3)
        Me.btnCargarMercaderias.Name = "btnCargarMercaderias"
        Me.btnCargarMercaderias.Size = New System.Drawing.Size(298, 23)
        Me.btnCargarMercaderias.TabIndex = 0
        Me.btnCargarMercaderias.Text = "Cargar productos de las facturas seleccionadas"
        Me.btnCargarMercaderias.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel4.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(331, 519)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(310, 32)
        Me.FlowLayoutPanel4.TabIndex = 5
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(232, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(9, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(217, 23)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.AllowUserToOrderColumns = True
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.TableLayoutPanel1.SetColumnSpan(Me.dgvDetalle, 2)
        Me.dgvDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDetalle.Location = New System.Drawing.Point(3, 295)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.ReadOnly = True
        Me.dgvDetalle.Size = New System.Drawing.Size(638, 218)
        Me.dgvDetalle.TabIndex = 4
        '
        'FlowLayoutPanel1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.FlowLayoutPanel1, 2)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblNroComprobante)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtNroComprobante)
        Me.FlowLayoutPanel1.Controls.Add(Me.lklComprobantesMes)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblFecha)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtDesde)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtHasta)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCargarComprobante)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(638, 28)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'lblNroComprobante
        '
        Me.lblNroComprobante.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNroComprobante.Location = New System.Drawing.Point(3, 0)
        Me.lblNroComprobante.Name = "lblNroComprobante"
        Me.lblNroComprobante.Size = New System.Drawing.Size(75, 28)
        Me.lblNroComprobante.TabIndex = 0
        Me.lblNroComprobante.Text = "Comprobante:"
        Me.lblNroComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNroComprobante
        '
        Me.txtNroComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroComprobante.Color = System.Drawing.Color.Empty
        Me.txtNroComprobante.Indicaciones = Nothing
        Me.txtNroComprobante.Location = New System.Drawing.Point(84, 5)
        Me.txtNroComprobante.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtNroComprobante.Multilinea = False
        Me.txtNroComprobante.Name = "txtNroComprobante"
        Me.txtNroComprobante.Size = New System.Drawing.Size(99, 25)
        Me.txtNroComprobante.SoloLectura = False
        Me.txtNroComprobante.TabIndex = 1
        Me.txtNroComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroComprobante.Texto = ""
        '
        'lklComprobantesMes
        '
        Me.lklComprobantesMes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklComprobantesMes.Location = New System.Drawing.Point(189, 3)
        Me.lklComprobantesMes.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lklComprobantesMes.Name = "lklComprobantesMes"
        Me.lklComprobantesMes.Size = New System.Drawing.Size(138, 23)
        Me.lklComprobantesMes.TabIndex = 2
        Me.lklComprobantesMes.TabStop = True
        Me.lklComprobantesMes.Text = "Comprobantes del Mes"
        Me.lklComprobantesMes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFecha
        '
        Me.lblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(333, 0)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(41, 28)
        Me.lblFecha.TabIndex = 3
        Me.lblFecha.Text = "Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(CType(0, Long))
        Me.txtDesde.Location = New System.Drawing.Point(380, 3)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(80, 21)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 4
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(CType(0, Long))
        Me.txtHasta.Location = New System.Drawing.Point(466, 3)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(80, 21)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 5
        '
        'btnCargarComprobante
        '
        Me.btnCargarComprobante.Location = New System.Drawing.Point(552, 3)
        Me.btnCargarComprobante.Name = "btnCargarComprobante"
        Me.btnCargarComprobante.Size = New System.Drawing.Size(75, 23)
        Me.btnCargarComprobante.TabIndex = 6
        Me.btnCargarComprobante.Text = "Listar"
        Me.btnCargarComprobante.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.lklQuitarSeleccionados)
        Me.FlowLayoutPanel2.Controls.Add(Me.lblCantidadDetalle)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtCantidadDetalle)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 519)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(322, 32)
        Me.FlowLayoutPanel2.TabIndex = 6
        '
        'lklQuitarSeleccionados
        '
        Me.lklQuitarSeleccionados.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklQuitarSeleccionados.Location = New System.Drawing.Point(3, 0)
        Me.lklQuitarSeleccionados.Name = "lklQuitarSeleccionados"
        Me.lklQuitarSeleccionados.Size = New System.Drawing.Size(160, 25)
        Me.lklQuitarSeleccionados.TabIndex = 0
        Me.lklQuitarSeleccionados.TabStop = True
        Me.lklQuitarSeleccionados.Text = "Quitar seleccionados"
        Me.lklQuitarSeleccionados.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCantidadDetalle
        '
        Me.lblCantidadDetalle.Location = New System.Drawing.Point(169, 0)
        Me.lblCantidadDetalle.Name = "lblCantidadDetalle"
        Me.lblCantidadDetalle.Size = New System.Drawing.Size(58, 28)
        Me.lblCantidadDetalle.TabIndex = 1
        Me.lblCantidadDetalle.Text = "Cantidad:"
        Me.lblCantidadDetalle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCantidadDetalle
        '
        Me.txtCantidadDetalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCantidadDetalle.Color = System.Drawing.Color.Empty
        Me.txtCantidadDetalle.Indicaciones = Nothing
        Me.txtCantidadDetalle.Location = New System.Drawing.Point(233, 5)
        Me.txtCantidadDetalle.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtCantidadDetalle.Multilinea = False
        Me.txtCantidadDetalle.Name = "txtCantidadDetalle"
        Me.txtCantidadDetalle.Size = New System.Drawing.Size(78, 25)
        Me.txtCantidadDetalle.SoloLectura = True
        Me.txtCantidadDetalle.TabIndex = 2
        Me.txtCantidadDetalle.TabStop = False
        Me.txtCantidadDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCantidadDetalle.Texto = ""
        '
        'dgvComprobante
        '
        Me.dgvComprobante.AllowUserToAddRows = False
        Me.dgvComprobante.AllowUserToDeleteRows = False
        Me.dgvComprobante.AllowUserToOrderColumns = True
        Me.dgvComprobante.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvComprobante.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDTransaccion, Me.Sel, Me.Sucursal, Me.Codigo, Me.Comprobante, Me.Fecha, Me.Direccion})
        Me.TableLayoutPanel1.SetColumnSpan(Me.dgvComprobante, 2)
        Me.dgvComprobante.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvComprobante.Location = New System.Drawing.Point(3, 37)
        Me.dgvComprobante.Name = "dgvComprobante"
        Me.dgvComprobante.ReadOnly = True
        Me.dgvComprobante.RowHeadersVisible = False
        Me.dgvComprobante.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvComprobante.Size = New System.Drawing.Size(638, 218)
        Me.dgvComprobante.StandardTab = True
        Me.dgvComprobante.TabIndex = 1
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.lklSeleccionarTodo)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblCantidadComprobante)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtCantidadComprobante)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 261)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(322, 28)
        Me.FlowLayoutPanel3.TabIndex = 2
        '
        'lklSeleccionarTodo
        '
        Me.lklSeleccionarTodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklSeleccionarTodo.Location = New System.Drawing.Point(3, 0)
        Me.lklSeleccionarTodo.Name = "lklSeleccionarTodo"
        Me.lklSeleccionarTodo.Size = New System.Drawing.Size(120, 25)
        Me.lklSeleccionarTodo.TabIndex = 0
        Me.lklSeleccionarTodo.TabStop = True
        Me.lklSeleccionarTodo.Text = "Seleccionar todo"
        Me.lklSeleccionarTodo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCantidadComprobante
        '
        Me.lblCantidadComprobante.Location = New System.Drawing.Point(129, 0)
        Me.lblCantidadComprobante.Name = "lblCantidadComprobante"
        Me.lblCantidadComprobante.Size = New System.Drawing.Size(98, 28)
        Me.lblCantidadComprobante.TabIndex = 1
        Me.lblCantidadComprobante.Text = "Cantidad:"
        Me.lblCantidadComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCantidadComprobante
        '
        Me.txtCantidadComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCantidadComprobante.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobante.Indicaciones = Nothing
        Me.txtCantidadComprobante.Location = New System.Drawing.Point(233, 5)
        Me.txtCantidadComprobante.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtCantidadComprobante.Multilinea = False
        Me.txtCantidadComprobante.Name = "txtCantidadComprobante"
        Me.txtCantidadComprobante.Size = New System.Drawing.Size(78, 25)
        Me.txtCantidadComprobante.SoloLectura = True
        Me.txtCantidadComprobante.TabIndex = 2
        Me.txtCantidadComprobante.TabStop = False
        Me.txtCantidadComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCantidadComprobante.Texto = ""
        '
        'IDTransaccion
        '
        Me.IDTransaccion.HeaderText = "IDTransaccion"
        Me.IDTransaccion.Name = "IDTransaccion"
        Me.IDTransaccion.ReadOnly = True
        Me.IDTransaccion.Visible = False
        '
        'Sel
        '
        Me.Sel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Sel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Sel.HeaderText = "Sel"
        Me.Sel.Name = "Sel"
        Me.Sel.ReadOnly = True
        Me.Sel.Width = 28
        '
        'Sucursal
        '
        Me.Sucursal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Sucursal.HeaderText = "Sucursal"
        Me.Sucursal.Name = "Sucursal"
        Me.Sucursal.ReadOnly = True
        Me.Sucursal.Width = 73
        '
        'Codigo
        '
        Me.Codigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Width = 65
        '
        'Comprobante
        '
        Me.Comprobante.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Comprobante.HeaderText = "Comprobante"
        Me.Comprobante.Name = "Comprobante"
        Me.Comprobante.ReadOnly = True
        Me.Comprobante.Width = 95
        '
        'Fecha
        '
        Me.Fecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        Me.Fecha.Width = 62
        '
        'Direccion
        '
        Me.Direccion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Direccion.HeaderText = "Direccion"
        Me.Direccion.Name = "Direccion"
        Me.Direccion.ReadOnly = True
        '
        'frmRemisionCargarFactura
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(644, 554)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmRemisionCargarFactura"
        Me.Text = "frmRemisionCargarFactura"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        CType(Me.dgvComprobante, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents dgvComprobante As System.Windows.Forms.DataGridView
    Friend WithEvents lklComprobantesMes As System.Windows.Forms.LinkLabel
    Friend WithEvents lblNroComprobante As System.Windows.Forms.Label
    Friend WithEvents txtNroComprobante As ERP.ocxTXTString
    Friend WithEvents btnCargarComprobante As System.Windows.Forms.Button
    Friend WithEvents FlowLayoutPanel5 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lklSeleccionarTodo As System.Windows.Forms.LinkLabel
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnCargarMercaderias As System.Windows.Forms.Button
    Friend WithEvents lklQuitarSeleccionados As System.Windows.Forms.LinkLabel
    Friend WithEvents lblCantidadDetalle As System.Windows.Forms.Label
    Friend WithEvents txtCantidadDetalle As ERP.ocxTXTString
    Friend WithEvents lblCantidadComprobante As System.Windows.Forms.Label
    Friend WithEvents txtCantidadComprobante As ERP.ocxTXTString
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents IDTransaccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sel As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Sucursal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Comprobante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Direccion As System.Windows.Forms.DataGridViewTextBoxColumn

End Class

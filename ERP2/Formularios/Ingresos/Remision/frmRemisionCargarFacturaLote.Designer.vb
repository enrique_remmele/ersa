﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRemisionCargarFacturaLote
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCargarMercaderias = New System.Windows.Forms.Button()
        Me.lklSeleccionarTodo = New System.Windows.Forms.LinkLabel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.lklQuitarSeleccionados = New System.Windows.Forms.LinkLabel()
        Me.lblCantidadDetalle = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.dgvComprobante = New System.Windows.Forms.DataGridView()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblCantidadComprobante = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtCantidadDetalle = New ERP.ocxTXTString()
        Me.txtCantidadComprobante = New ERP.ocxTXTString()
        Me.IDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Sucursal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Comprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Direccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDSucursalCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.dgvComprobante, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.Controls.Add(Me.btnCargarMercaderias)
        Me.FlowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel5.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(331, 244)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(310, 28)
        Me.FlowLayoutPanel5.TabIndex = 3
        '
        'btnCargarMercaderias
        '
        Me.btnCargarMercaderias.Location = New System.Drawing.Point(9, 3)
        Me.btnCargarMercaderias.Name = "btnCargarMercaderias"
        Me.btnCargarMercaderias.Size = New System.Drawing.Size(298, 23)
        Me.btnCargarMercaderias.TabIndex = 0
        Me.btnCargarMercaderias.Text = "Cargar productos de las facturas seleccionadas"
        Me.btnCargarMercaderias.UseVisualStyleBackColor = True
        '
        'lklSeleccionarTodo
        '
        Me.lklSeleccionarTodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklSeleccionarTodo.Location = New System.Drawing.Point(3, 0)
        Me.lklSeleccionarTodo.Name = "lklSeleccionarTodo"
        Me.lklSeleccionarTodo.Size = New System.Drawing.Size(120, 25)
        Me.lklSeleccionarTodo.TabIndex = 0
        Me.lklSeleccionarTodo.TabStop = True
        Me.lklSeleccionarTodo.Text = "Seleccionar todo"
        Me.lklSeleccionarTodo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel4.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(331, 519)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(310, 32)
        Me.FlowLayoutPanel4.TabIndex = 5
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(232, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(9, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(217, 23)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "Aceptar y Guardar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.AllowUserToOrderColumns = True
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.TableLayoutPanel1.SetColumnSpan(Me.dgvDetalle, 2)
        Me.dgvDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDetalle.Location = New System.Drawing.Point(3, 278)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.ReadOnly = True
        Me.dgvDetalle.Size = New System.Drawing.Size(638, 235)
        Me.dgvDetalle.TabIndex = 4
        '
        'lklQuitarSeleccionados
        '
        Me.lklQuitarSeleccionados.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklQuitarSeleccionados.Location = New System.Drawing.Point(3, 0)
        Me.lklQuitarSeleccionados.Name = "lklQuitarSeleccionados"
        Me.lklQuitarSeleccionados.Size = New System.Drawing.Size(160, 25)
        Me.lklQuitarSeleccionados.TabIndex = 0
        Me.lklQuitarSeleccionados.TabStop = True
        Me.lklQuitarSeleccionados.Text = "Quitar seleccionados"
        Me.lklQuitarSeleccionados.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCantidadDetalle
        '
        Me.lblCantidadDetalle.Location = New System.Drawing.Point(169, 0)
        Me.lblCantidadDetalle.Name = "lblCantidadDetalle"
        Me.lblCantidadDetalle.Size = New System.Drawing.Size(58, 28)
        Me.lblCantidadDetalle.TabIndex = 1
        Me.lblCantidadDetalle.Text = "Cantidad:"
        Me.lblCantidadDetalle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.lklQuitarSeleccionados)
        Me.FlowLayoutPanel2.Controls.Add(Me.lblCantidadDetalle)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtCantidadDetalle)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 519)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(322, 32)
        Me.FlowLayoutPanel2.TabIndex = 6
        '
        'dgvComprobante
        '
        Me.dgvComprobante.AllowUserToAddRows = False
        Me.dgvComprobante.AllowUserToDeleteRows = False
        Me.dgvComprobante.AllowUserToOrderColumns = True
        Me.dgvComprobante.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvComprobante.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDTransaccion, Me.Sel, Me.Sucursal, Me.Codigo, Me.Comprobante, Me.Fecha, Me.Direccion, Me.Cliente, Me.IDCliente, Me.IDSucursalCliente})
        Me.TableLayoutPanel1.SetColumnSpan(Me.dgvComprobante, 2)
        Me.dgvComprobante.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvComprobante.Location = New System.Drawing.Point(3, 3)
        Me.dgvComprobante.Name = "dgvComprobante"
        Me.dgvComprobante.ReadOnly = True
        Me.dgvComprobante.RowHeadersVisible = False
        Me.dgvComprobante.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvComprobante.Size = New System.Drawing.Size(638, 235)
        Me.dgvComprobante.StandardTab = True
        Me.dgvComprobante.TabIndex = 1
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.lklSeleccionarTodo)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblCantidadComprobante)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtCantidadComprobante)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 244)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(322, 28)
        Me.FlowLayoutPanel3.TabIndex = 2
        '
        'lblCantidadComprobante
        '
        Me.lblCantidadComprobante.Location = New System.Drawing.Point(129, 0)
        Me.lblCantidadComprobante.Name = "lblCantidadComprobante"
        Me.lblCantidadComprobante.Size = New System.Drawing.Size(98, 28)
        Me.lblCantidadComprobante.TabIndex = 1
        Me.lblCantidadComprobante.Text = "Cantidad:"
        Me.lblCantidadComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.02041!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.97959!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel5, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvDetalle, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvComprobante, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(644, 554)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'txtCantidadDetalle
        '
        Me.txtCantidadDetalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCantidadDetalle.Color = System.Drawing.Color.Empty
        Me.txtCantidadDetalle.Indicaciones = Nothing
        Me.txtCantidadDetalle.Location = New System.Drawing.Point(233, 5)
        Me.txtCantidadDetalle.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtCantidadDetalle.Multilinea = False
        Me.txtCantidadDetalle.Name = "txtCantidadDetalle"
        Me.txtCantidadDetalle.Size = New System.Drawing.Size(78, 25)
        Me.txtCantidadDetalle.SoloLectura = True
        Me.txtCantidadDetalle.TabIndex = 2
        Me.txtCantidadDetalle.TabStop = False
        Me.txtCantidadDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCantidadDetalle.Texto = ""
        '
        'txtCantidadComprobante
        '
        Me.txtCantidadComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCantidadComprobante.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobante.Indicaciones = Nothing
        Me.txtCantidadComprobante.Location = New System.Drawing.Point(233, 5)
        Me.txtCantidadComprobante.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtCantidadComprobante.Multilinea = False
        Me.txtCantidadComprobante.Name = "txtCantidadComprobante"
        Me.txtCantidadComprobante.Size = New System.Drawing.Size(78, 25)
        Me.txtCantidadComprobante.SoloLectura = True
        Me.txtCantidadComprobante.TabIndex = 2
        Me.txtCantidadComprobante.TabStop = False
        Me.txtCantidadComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCantidadComprobante.Texto = ""
        '
        'IDTransaccion
        '
        Me.IDTransaccion.HeaderText = "IDTransaccion"
        Me.IDTransaccion.Name = "IDTransaccion"
        Me.IDTransaccion.ReadOnly = True
        Me.IDTransaccion.Visible = False
        '
        'Sel
        '
        Me.Sel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Sel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Sel.HeaderText = "Sel"
        Me.Sel.Name = "Sel"
        Me.Sel.ReadOnly = True
        Me.Sel.Width = 28
        '
        'Sucursal
        '
        Me.Sucursal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Sucursal.HeaderText = "Sucursal"
        Me.Sucursal.Name = "Sucursal"
        Me.Sucursal.ReadOnly = True
        Me.Sucursal.Width = 73
        '
        'Codigo
        '
        Me.Codigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Width = 65
        '
        'Comprobante
        '
        Me.Comprobante.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Comprobante.HeaderText = "Comprobante"
        Me.Comprobante.Name = "Comprobante"
        Me.Comprobante.ReadOnly = True
        Me.Comprobante.Width = 95
        '
        'Fecha
        '
        Me.Fecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        Me.Fecha.Width = 62
        '
        'Direccion
        '
        Me.Direccion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Direccion.HeaderText = "Direccion"
        Me.Direccion.Name = "Direccion"
        Me.Direccion.ReadOnly = True
        '
        'Cliente
        '
        Me.Cliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Cliente.HeaderText = "Cliente"
        Me.Cliente.Name = "Cliente"
        Me.Cliente.ReadOnly = True
        Me.Cliente.Visible = False
        '
        'IDCliente
        '
        Me.IDCliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.IDCliente.HeaderText = "IDCliente"
        Me.IDCliente.Name = "IDCliente"
        Me.IDCliente.ReadOnly = True
        Me.IDCliente.Visible = False
        '
        'IDSucursalCliente
        '
        Me.IDSucursalCliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.IDSucursalCliente.HeaderText = "IDSucursalCliente"
        Me.IDSucursalCliente.Name = "IDSucursalCliente"
        Me.IDSucursalCliente.ReadOnly = True
        Me.IDSucursalCliente.Visible = False
        '
        'frmRemisionCargarFacturaLote
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(644, 554)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmRemisionCargarFacturaLote"
        Me.Text = "frmRemisionCargarFacturaLote"
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        CType(Me.dgvComprobante, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents txtCantidadComprobante As ocxTXTString
    Friend WithEvents FlowLayoutPanel5 As FlowLayoutPanel
    Friend WithEvents btnCargarMercaderias As Button
    Friend WithEvents lklSeleccionarTodo As LinkLabel
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnAceptar As Button
    Friend WithEvents dgvDetalle As DataGridView
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents lklQuitarSeleccionados As LinkLabel
    Friend WithEvents lblCantidadDetalle As Label
    Friend WithEvents txtCantidadDetalle As ocxTXTString
    Friend WithEvents dgvComprobante As DataGridView
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents lblCantidadComprobante As Label
    Friend WithEvents IDTransaccion As DataGridViewTextBoxColumn
    Friend WithEvents Sel As DataGridViewCheckBoxColumn
    Friend WithEvents Sucursal As DataGridViewTextBoxColumn
    Friend WithEvents Codigo As DataGridViewTextBoxColumn
    Friend WithEvents Comprobante As DataGridViewTextBoxColumn
    Friend WithEvents Fecha As DataGridViewTextBoxColumn
    Friend WithEvents Direccion As DataGridViewTextBoxColumn
    Friend WithEvents Cliente As DataGridViewTextBoxColumn
    Friend WithEvents IDCliente As DataGridViewTextBoxColumn
    Friend WithEvents IDSucursalCliente As DataGridViewTextBoxColumn
End Class

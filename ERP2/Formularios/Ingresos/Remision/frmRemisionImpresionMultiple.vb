﻿
Public Class frmRemisionImpresionMultiple

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CReporteCheque As New Reporte.CReporteCheque

    'VARIABLES
    Dim vdtRemision As DataTable

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        CSistema.InicializaControles(Me)

        'Botones inhabilitados
        btnDetalle.Enabled = False
        btnImprimir.Enabled = False

        CargarInformacion()

        DesplegarImpresoras()

    End Sub

    Sub CargarInformacion()


    End Sub

    Sub Cargar()

        btnDetalle.Enabled = False
        btnImprimir.Enabled = False

        'vdtOrdenPago = CSistema.ExecuteToDataTable("Select * From VOrdenPago Where Numero Between " & txtOPInicial.ObtenerValor & " And " & txtOPFinal.ObtenerValor & " And Cheque = 'True' And NroCheque = '' And Anulado='False' And IDSucursal=" & cbxSucursal.GetValue()).Copy
        'vdtOrdenPago = CSistema.ExecuteToDataTable("Select * From VOrdenPago Where Numero Between " & txtOPInicial.ObtenerValor & " And " & txtOPFinal.ObtenerValor & " And Cheque = 'True' And IDSucursal=" & cbxSucursal.GetValue()).Copy

        If vdtRemision Is Nothing Then
            Exit Sub
        End If

        If vdtRemision.Rows.Count = 0 Then
            Exit Sub
        End If

        'Habilitar controles
        btnDetalle.Enabled = True
        btnImprimir.Enabled = True

    End Sub

    Sub Procesar()

        'Validar
        'Si es que MAX y MIN OP no sean 0
        'If txtOPInicial.ObtenerValor = 0 Or txtOPFinal.ObtenerValor = 0 Then
        '    CSistema.MostrarError("Los numeros de OP no concuerdan", ctrError, btnImprimir, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Sub
        'End If

        ' ''MIN no sea menor a MAX
        'If Val(txtOPInicial.ObtenerValor) > Val(txtOPFinal.ObtenerValor) Then
        '    CSistema.MostrarError("La OPInicial no puede ser mayor a la OPFinal", ctrError, btnImprimir, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Sub
        'End If

        ''El nro de cheque no sea 0
        'If txtPrimerCheque.ObtenerValor = 0 Then
        '    CSistema.MostrarError("El Primer Cheque no puede ser igual a cero", ctrError, btnImprimir, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Sub
        'End If

        ''Elegir un formulario
        'If cbxFormulario.cbx.Text = "" Then
        '    CSistema.MostrarError("Elegir algún formulario", ctrError, btnImprimir, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Sub
        'End If

        ''Elegir una impresora
        'If cbxImpresora.cbx.Text = "" Then
        '    CSistema.MostrarError("Debe elegir una impresora", ctrError, btnImprimir, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Sub
        'End If

        ''Variables
        'Dim ImporteLetras As String
        'Dim OP As String

        ''El Maximo depende del cheque, este valor debe ser configurado por cheque
        ''80 es para ITAU, establecer en CuentaBancaria
        'Dim MaximoPrimeraLinea As Integer = 81

        'For Each oRow As DataRow In vdtRemision.Rows

        '    IDTransaccion = (oRow("IDTransaccion"))
        '    ImporteLetras = Numalet.Convertir(CInt(oRow("Importe").ToString), False)

        '    OP = (oRow("Numero").ToString)
        '    OP = "OP: " & OP

        '    'Impimir
        '    Imprimir(ImporteLetras, OP, MaximoPrimeraLinea)

        '    'Mensaje
        '    Dim msg As DialogResult = MessageBox.Show("Se imprimió correctamente el cheque?", "Cheque", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

        '    Select Case msg

        '        Case Windows.Forms.DialogResult.Yes

        '            If ActualizarOrdenPago(oRow("IDTransaccion"), txtPrimerCheque.ObtenerValor) = False Then
        '                Inicializar()
        '                Exit Sub
        '            End If

        '        Case Windows.Forms.DialogResult.No
        '            Inicializar()
        '            Exit Sub
        '        Case Windows.Forms.DialogResult.Cancel
        '            Inicializar()
        '            Exit Sub
        '    End Select

        '    Dim ProximoNumero As Integer
        '    ProximoNumero = txtPrimerCheque.ObtenerValor + 1
        '    txtPrimerCheque.SetValue(ProximoNumero)

        'Next

        CargarInformacion()

    End Sub

    Sub DesplegarImpresoras()

        cbxImpresora.cbx.Items.Clear()

        For Each strPrinter As String In System.Drawing.Printing.PrinterSettings.InstalledPrinters
            cbxImpresora.cbx.Items.Add(strPrinter)
        Next

    End Sub

    Sub Imprimir(ByVal ImporteLetras As String, ByVal OP As String, ByVal MaximoPrimeraLinea As Integer)

        'Dim frm As New frmReporte
        'Dim Diferido As Boolean = False
        'Dim Cadena1 As String = ""
        'Dim Cadena2 As String = ""

        'Cadena1 = RetornarTexto(True, ImporteLetras, MaximoPrimeraLinea)
        'Cadena2 = RetornarTexto(False, ImporteLetras, MaximoPrimeraLinea)

        'CReporteCheque.ImprimirCheque(frm, IDTransaccion, cbxCuenta.GetValue, Diferido, cbxImpresora.cbx.Text, Cadena1, Cadena2, OP)

    End Sub

    Sub Detalle()

        Dim frm As New frmOrdenPagoListaImpresion
        frm.dt = vdtRemision
        FGMostrarFormulario(Me, frm, "Lista de Cheques a imprimir", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True)

    End Sub

    Private Sub frmImpresionCheque_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmImpresionCheque_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Inicializar()
    End Sub

    Private Sub btnCargar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Cargar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Procesar()
    End Sub

    Private Sub btnDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Detalle()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

End Class
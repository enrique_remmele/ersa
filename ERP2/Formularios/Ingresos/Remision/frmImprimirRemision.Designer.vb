﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImprimirRemisionPorDeposito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.txtKilometrajeEstimativo = New ERP.ocxTXTString()
        Me.lblKilometrajeEstimativo = New System.Windows.Forms.Label()
        Me.lblDomicilioChofer = New System.Windows.Forms.Label()
        Me.txtDomicilioChofer = New ERP.ocxTXTString()
        Me.lblRUCChofer = New System.Windows.Forms.Label()
        Me.txtRUCChofer = New ERP.ocxTXTString()
        Me.lblPatenteVehiculo = New System.Windows.Forms.Label()
        Me.txtPatenteVehiculo = New ERP.ocxTXTString()
        Me.txtDepartamentoLlegada = New ERP.ocxTXTString()
        Me.lblDepartamentoLlegada = New System.Windows.Forms.Label()
        Me.txtCiudadLlegada = New ERP.ocxTXTString()
        Me.lblCiudadLlegada = New System.Windows.Forms.Label()
        Me.txtDepartamentoPartida = New ERP.ocxTXTString()
        Me.lblDepartamentoPartida = New System.Windows.Forms.Label()
        Me.txtCiudadPartida = New ERP.ocxTXTString()
        Me.lblCiudadPartida = New System.Windows.Forms.Label()
        Me.txtFechaExpedicionVenta = New ERP.ocxTXTDate()
        Me.lblFechaExpedicionVenta = New System.Windows.Forms.Label()
        Me.txtTimbradoVenta = New ERP.ocxTXTString()
        Me.lblTimbradoVenta = New System.Windows.Forms.Label()
        Me.txtComprobanteVentaTipo = New ERP.ocxTXTString()
        Me.lblComprobanteVentaTipo = New System.Windows.Forms.Label()
        Me.txtDomicilioCliente = New ERP.ocxTXTString()
        Me.lblDomicilioCliente = New System.Windows.Forms.Label()
        Me.cbxTransporte = New ERP.ocxCBX()
        Me.txtMotivoTranslado = New ERP.ocxTXTString()
        Me.lblMotivoTranslado = New System.Windows.Forms.Label()
        Me.cbxChofer = New ERP.ocxCBX()
        Me.lblChofer = New System.Windows.Forms.Label()
        Me.cbxVehiculo = New ERP.ocxCBX()
        Me.lblVehiculo = New System.Windows.Forms.Label()
        Me.txtComprobanteVenta = New ERP.ocxTXTString()
        Me.lblComprobanteVenta = New System.Windows.Forms.Label()
        Me.cbxSucursalOperacion = New ERP.ocxCBX()
        Me.lblSucursalOperacion = New System.Windows.Forms.Label()
        Me.txtDireccionLlegada = New ERP.ocxTXTString()
        Me.txtFechaFin = New ERP.ocxTXTDate()
        Me.cbxDepositoOperacion = New ERP.ocxCBX()
        Me.lblDepositoOperacion = New System.Windows.Forms.Label()
        Me.txtFechaInicio = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.txtDireccionPartida = New ERP.ocxTXTString()
        Me.lblDireccionPartida = New System.Windows.Forms.Label()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.lblTransporte = New System.Windows.Forms.Label()
        Me.lblDireccionLlegada = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnPreparar = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.txtCantidadMaximaProductos = New ERP.ocxTXTNumeric()
        Me.lblCantidadDetalle = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.gbxCabecera.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxCabecera
        '
        Me.gbxCabecera.BackColor = System.Drawing.Color.Transparent
        Me.gbxCabecera.Controls.Add(Me.txtCliente)
        Me.gbxCabecera.Controls.Add(Me.txtKilometrajeEstimativo)
        Me.gbxCabecera.Controls.Add(Me.lblKilometrajeEstimativo)
        Me.gbxCabecera.Controls.Add(Me.lblDomicilioChofer)
        Me.gbxCabecera.Controls.Add(Me.txtDomicilioChofer)
        Me.gbxCabecera.Controls.Add(Me.lblRUCChofer)
        Me.gbxCabecera.Controls.Add(Me.txtRUCChofer)
        Me.gbxCabecera.Controls.Add(Me.lblPatenteVehiculo)
        Me.gbxCabecera.Controls.Add(Me.txtPatenteVehiculo)
        Me.gbxCabecera.Controls.Add(Me.txtDepartamentoLlegada)
        Me.gbxCabecera.Controls.Add(Me.lblDepartamentoLlegada)
        Me.gbxCabecera.Controls.Add(Me.txtCiudadLlegada)
        Me.gbxCabecera.Controls.Add(Me.lblCiudadLlegada)
        Me.gbxCabecera.Controls.Add(Me.txtDepartamentoPartida)
        Me.gbxCabecera.Controls.Add(Me.lblDepartamentoPartida)
        Me.gbxCabecera.Controls.Add(Me.txtCiudadPartida)
        Me.gbxCabecera.Controls.Add(Me.lblCiudadPartida)
        Me.gbxCabecera.Controls.Add(Me.txtFechaExpedicionVenta)
        Me.gbxCabecera.Controls.Add(Me.lblFechaExpedicionVenta)
        Me.gbxCabecera.Controls.Add(Me.txtTimbradoVenta)
        Me.gbxCabecera.Controls.Add(Me.lblTimbradoVenta)
        Me.gbxCabecera.Controls.Add(Me.txtComprobanteVentaTipo)
        Me.gbxCabecera.Controls.Add(Me.lblComprobanteVentaTipo)
        Me.gbxCabecera.Controls.Add(Me.txtDomicilioCliente)
        Me.gbxCabecera.Controls.Add(Me.lblDomicilioCliente)
        Me.gbxCabecera.Controls.Add(Me.cbxTransporte)
        Me.gbxCabecera.Controls.Add(Me.txtMotivoTranslado)
        Me.gbxCabecera.Controls.Add(Me.lblMotivoTranslado)
        Me.gbxCabecera.Controls.Add(Me.cbxChofer)
        Me.gbxCabecera.Controls.Add(Me.lblChofer)
        Me.gbxCabecera.Controls.Add(Me.cbxVehiculo)
        Me.gbxCabecera.Controls.Add(Me.lblVehiculo)
        Me.gbxCabecera.Controls.Add(Me.txtComprobanteVenta)
        Me.gbxCabecera.Controls.Add(Me.lblComprobanteVenta)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursalOperacion)
        Me.gbxCabecera.Controls.Add(Me.lblSucursalOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtDireccionLlegada)
        Me.gbxCabecera.Controls.Add(Me.txtFechaFin)
        Me.gbxCabecera.Controls.Add(Me.cbxDepositoOperacion)
        Me.gbxCabecera.Controls.Add(Me.lblDepositoOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtFechaInicio)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.txtDireccionPartida)
        Me.gbxCabecera.Controls.Add(Me.lblDireccionPartida)
        Me.gbxCabecera.Controls.Add(Me.lblCliente)
        Me.gbxCabecera.Controls.Add(Me.lblTransporte)
        Me.gbxCabecera.Controls.Add(Me.lblDireccionLlegada)
        Me.gbxCabecera.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbxCabecera.Location = New System.Drawing.Point(12, 12)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(760, 246)
        Me.gbxCabecera.TabIndex = 0
        Me.gbxCabecera.TabStop = False
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 185
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(91, 47)
        Me.txtCliente.MostrarSucursal = True
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(378, 24)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 5
        '
        'txtKilometrajeEstimativo
        '
        Me.txtKilometrajeEstimativo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKilometrajeEstimativo.Color = System.Drawing.Color.Beige
        Me.txtKilometrajeEstimativo.Indicaciones = Nothing
        Me.txtKilometrajeEstimativo.Location = New System.Drawing.Point(91, 184)
        Me.txtKilometrajeEstimativo.Multilinea = False
        Me.txtKilometrajeEstimativo.Name = "txtKilometrajeEstimativo"
        Me.txtKilometrajeEstimativo.Size = New System.Drawing.Size(50, 21)
        Me.txtKilometrajeEstimativo.SoloLectura = False
        Me.txtKilometrajeEstimativo.TabIndex = 34
        Me.txtKilometrajeEstimativo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtKilometrajeEstimativo.Texto = ""
        '
        'lblKilometrajeEstimativo
        '
        Me.lblKilometrajeEstimativo.AutoSize = True
        Me.lblKilometrajeEstimativo.Location = New System.Drawing.Point(14, 188)
        Me.lblKilometrajeEstimativo.Name = "lblKilometrajeEstimativo"
        Me.lblKilometrajeEstimativo.Size = New System.Drawing.Size(74, 13)
        Me.lblKilometrajeEstimativo.TabIndex = 33
        Me.lblKilometrajeEstimativo.Text = "Km. Estimado:"
        '
        'lblDomicilioChofer
        '
        Me.lblDomicilioChofer.AutoSize = True
        Me.lblDomicilioChofer.Location = New System.Drawing.Point(471, 215)
        Me.lblDomicilioChofer.Name = "lblDomicilioChofer"
        Me.lblDomicilioChofer.Size = New System.Drawing.Size(52, 13)
        Me.lblDomicilioChofer.TabIndex = 45
        Me.lblDomicilioChofer.Text = "Domicilio:"
        '
        'txtDomicilioChofer
        '
        Me.txtDomicilioChofer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDomicilioChofer.Color = System.Drawing.Color.Beige
        Me.txtDomicilioChofer.Indicaciones = Nothing
        Me.txtDomicilioChofer.Location = New System.Drawing.Point(524, 211)
        Me.txtDomicilioChofer.Multilinea = False
        Me.txtDomicilioChofer.Name = "txtDomicilioChofer"
        Me.txtDomicilioChofer.Size = New System.Drawing.Size(222, 21)
        Me.txtDomicilioChofer.SoloLectura = True
        Me.txtDomicilioChofer.TabIndex = 46
        Me.txtDomicilioChofer.TabStop = False
        Me.txtDomicilioChofer.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDomicilioChofer.Texto = ""
        '
        'lblRUCChofer
        '
        Me.lblRUCChofer.AutoSize = True
        Me.lblRUCChofer.Location = New System.Drawing.Point(306, 215)
        Me.lblRUCChofer.Name = "lblRUCChofer"
        Me.lblRUCChofer.Size = New System.Drawing.Size(48, 13)
        Me.lblRUCChofer.TabIndex = 43
        Me.lblRUCChofer.Text = "RUC/CI:"
        '
        'txtRUCChofer
        '
        Me.txtRUCChofer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRUCChofer.Color = System.Drawing.Color.Beige
        Me.txtRUCChofer.Indicaciones = Nothing
        Me.txtRUCChofer.Location = New System.Drawing.Point(360, 211)
        Me.txtRUCChofer.Multilinea = False
        Me.txtRUCChofer.Name = "txtRUCChofer"
        Me.txtRUCChofer.Size = New System.Drawing.Size(106, 21)
        Me.txtRUCChofer.SoloLectura = True
        Me.txtRUCChofer.TabIndex = 44
        Me.txtRUCChofer.TabStop = False
        Me.txtRUCChofer.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRUCChofer.Texto = ""
        '
        'lblPatenteVehiculo
        '
        Me.lblPatenteVehiculo.AutoSize = True
        Me.lblPatenteVehiculo.Location = New System.Drawing.Point(593, 188)
        Me.lblPatenteVehiculo.Name = "lblPatenteVehiculo"
        Me.lblPatenteVehiculo.Size = New System.Drawing.Size(47, 13)
        Me.lblPatenteVehiculo.TabIndex = 39
        Me.lblPatenteVehiculo.Text = "Patente:"
        '
        'txtPatenteVehiculo
        '
        Me.txtPatenteVehiculo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPatenteVehiculo.Color = System.Drawing.Color.Beige
        Me.txtPatenteVehiculo.Indicaciones = Nothing
        Me.txtPatenteVehiculo.Location = New System.Drawing.Point(640, 184)
        Me.txtPatenteVehiculo.Multilinea = False
        Me.txtPatenteVehiculo.Name = "txtPatenteVehiculo"
        Me.txtPatenteVehiculo.Size = New System.Drawing.Size(106, 21)
        Me.txtPatenteVehiculo.SoloLectura = True
        Me.txtPatenteVehiculo.TabIndex = 40
        Me.txtPatenteVehiculo.TabStop = False
        Me.txtPatenteVehiculo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPatenteVehiculo.Texto = ""
        '
        'txtDepartamentoLlegada
        '
        Me.txtDepartamentoLlegada.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDepartamentoLlegada.Color = System.Drawing.Color.Beige
        Me.txtDepartamentoLlegada.Indicaciones = Nothing
        Me.txtDepartamentoLlegada.Location = New System.Drawing.Point(616, 157)
        Me.txtDepartamentoLlegada.Multilinea = False
        Me.txtDepartamentoLlegada.Name = "txtDepartamentoLlegada"
        Me.txtDepartamentoLlegada.Size = New System.Drawing.Size(130, 21)
        Me.txtDepartamentoLlegada.SoloLectura = False
        Me.txtDepartamentoLlegada.TabIndex = 32
        Me.txtDepartamentoLlegada.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDepartamentoLlegada.Texto = ""
        '
        'lblDepartamentoLlegada
        '
        Me.lblDepartamentoLlegada.AutoSize = True
        Me.lblDepartamentoLlegada.Location = New System.Drawing.Point(539, 161)
        Me.lblDepartamentoLlegada.Name = "lblDepartamentoLlegada"
        Me.lblDepartamentoLlegada.Size = New System.Drawing.Size(77, 13)
        Me.lblDepartamentoLlegada.TabIndex = 31
        Me.lblDepartamentoLlegada.Text = "Departamento:"
        '
        'txtCiudadLlegada
        '
        Me.txtCiudadLlegada.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCiudadLlegada.Color = System.Drawing.Color.Beige
        Me.txtCiudadLlegada.Indicaciones = Nothing
        Me.txtCiudadLlegada.Location = New System.Drawing.Point(427, 157)
        Me.txtCiudadLlegada.Multilinea = False
        Me.txtCiudadLlegada.Name = "txtCiudadLlegada"
        Me.txtCiudadLlegada.Size = New System.Drawing.Size(106, 21)
        Me.txtCiudadLlegada.SoloLectura = False
        Me.txtCiudadLlegada.TabIndex = 30
        Me.txtCiudadLlegada.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCiudadLlegada.Texto = ""
        '
        'lblCiudadLlegada
        '
        Me.lblCiudadLlegada.AutoSize = True
        Me.lblCiudadLlegada.Location = New System.Drawing.Point(381, 161)
        Me.lblCiudadLlegada.Name = "lblCiudadLlegada"
        Me.lblCiudadLlegada.Size = New System.Drawing.Size(43, 13)
        Me.lblCiudadLlegada.TabIndex = 29
        Me.lblCiudadLlegada.Text = "Ciudad:"
        '
        'txtDepartamentoPartida
        '
        Me.txtDepartamentoPartida.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDepartamentoPartida.Color = System.Drawing.Color.Beige
        Me.txtDepartamentoPartida.Indicaciones = Nothing
        Me.txtDepartamentoPartida.Location = New System.Drawing.Point(616, 130)
        Me.txtDepartamentoPartida.Multilinea = False
        Me.txtDepartamentoPartida.Name = "txtDepartamentoPartida"
        Me.txtDepartamentoPartida.Size = New System.Drawing.Size(130, 21)
        Me.txtDepartamentoPartida.SoloLectura = False
        Me.txtDepartamentoPartida.TabIndex = 26
        Me.txtDepartamentoPartida.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDepartamentoPartida.Texto = ""
        '
        'lblDepartamentoPartida
        '
        Me.lblDepartamentoPartida.AutoSize = True
        Me.lblDepartamentoPartida.Location = New System.Drawing.Point(539, 134)
        Me.lblDepartamentoPartida.Name = "lblDepartamentoPartida"
        Me.lblDepartamentoPartida.Size = New System.Drawing.Size(77, 13)
        Me.lblDepartamentoPartida.TabIndex = 25
        Me.lblDepartamentoPartida.Text = "Departamento:"
        '
        'txtCiudadPartida
        '
        Me.txtCiudadPartida.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCiudadPartida.Color = System.Drawing.Color.Beige
        Me.txtCiudadPartida.Indicaciones = Nothing
        Me.txtCiudadPartida.Location = New System.Drawing.Point(427, 130)
        Me.txtCiudadPartida.Multilinea = False
        Me.txtCiudadPartida.Name = "txtCiudadPartida"
        Me.txtCiudadPartida.Size = New System.Drawing.Size(106, 21)
        Me.txtCiudadPartida.SoloLectura = False
        Me.txtCiudadPartida.TabIndex = 24
        Me.txtCiudadPartida.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCiudadPartida.Texto = ""
        '
        'lblCiudadPartida
        '
        Me.lblCiudadPartida.AutoSize = True
        Me.lblCiudadPartida.Location = New System.Drawing.Point(381, 134)
        Me.lblCiudadPartida.Name = "lblCiudadPartida"
        Me.lblCiudadPartida.Size = New System.Drawing.Size(43, 13)
        Me.lblCiudadPartida.TabIndex = 23
        Me.lblCiudadPartida.Text = "Ciudad:"
        '
        'txtFechaExpedicionVenta
        '
        Me.txtFechaExpedicionVenta.Color = System.Drawing.Color.Empty
        Me.txtFechaExpedicionVenta.Fecha = New Date(CType(0, Long))
        Me.txtFechaExpedicionVenta.Location = New System.Drawing.Point(351, 104)
        Me.txtFechaExpedicionVenta.Name = "txtFechaExpedicionVenta"
        Me.txtFechaExpedicionVenta.PermitirNulo = False
        Me.txtFechaExpedicionVenta.Size = New System.Drawing.Size(75, 20)
        Me.txtFechaExpedicionVenta.SoloLectura = False
        Me.txtFechaExpedicionVenta.TabIndex = 17
        '
        'lblFechaExpedicionVenta
        '
        Me.lblFechaExpedicionVenta.AutoSize = True
        Me.lblFechaExpedicionVenta.Location = New System.Drawing.Point(241, 108)
        Me.lblFechaExpedicionVenta.Name = "lblFechaExpedicionVenta"
        Me.lblFechaExpedicionVenta.Size = New System.Drawing.Size(110, 13)
        Me.lblFechaExpedicionVenta.TabIndex = 16
        Me.lblFechaExpedicionVenta.Text = "Fecha de Expedicion:"
        '
        'txtTimbradoVenta
        '
        Me.txtTimbradoVenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTimbradoVenta.Color = System.Drawing.Color.Beige
        Me.txtTimbradoVenta.Indicaciones = Nothing
        Me.txtTimbradoVenta.Location = New System.Drawing.Point(91, 104)
        Me.txtTimbradoVenta.Multilinea = False
        Me.txtTimbradoVenta.Name = "txtTimbradoVenta"
        Me.txtTimbradoVenta.Size = New System.Drawing.Size(150, 21)
        Me.txtTimbradoVenta.SoloLectura = False
        Me.txtTimbradoVenta.TabIndex = 15
        Me.txtTimbradoVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTimbradoVenta.Texto = ""
        '
        'lblTimbradoVenta
        '
        Me.lblTimbradoVenta.AutoSize = True
        Me.lblTimbradoVenta.Location = New System.Drawing.Point(11, 108)
        Me.lblTimbradoVenta.Name = "lblTimbradoVenta"
        Me.lblTimbradoVenta.Size = New System.Drawing.Size(77, 13)
        Me.lblTimbradoVenta.TabIndex = 14
        Me.lblTimbradoVenta.Text = "Nro. Timbrado:"
        '
        'txtComprobanteVentaTipo
        '
        Me.txtComprobanteVentaTipo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobanteVentaTipo.Color = System.Drawing.Color.Beige
        Me.txtComprobanteVentaTipo.Indicaciones = Nothing
        Me.txtComprobanteVentaTipo.Location = New System.Drawing.Point(345, 77)
        Me.txtComprobanteVentaTipo.Multilinea = False
        Me.txtComprobanteVentaTipo.Name = "txtComprobanteVentaTipo"
        Me.txtComprobanteVentaTipo.Size = New System.Drawing.Size(124, 21)
        Me.txtComprobanteVentaTipo.SoloLectura = False
        Me.txtComprobanteVentaTipo.TabIndex = 11
        Me.txtComprobanteVentaTipo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobanteVentaTipo.Texto = ""
        '
        'lblComprobanteVentaTipo
        '
        Me.lblComprobanteVentaTipo.AutoSize = True
        Me.lblComprobanteVentaTipo.Location = New System.Drawing.Point(241, 81)
        Me.lblComprobanteVentaTipo.Name = "lblComprobanteVentaTipo"
        Me.lblComprobanteVentaTipo.Size = New System.Drawing.Size(104, 13)
        Me.lblComprobanteVentaTipo.TabIndex = 10
        Me.lblComprobanteVentaTipo.Text = "Comprobante Venta:"
        '
        'txtDomicilioCliente
        '
        Me.txtDomicilioCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDomicilioCliente.Color = System.Drawing.Color.Beige
        Me.txtDomicilioCliente.Indicaciones = Nothing
        Me.txtDomicilioCliente.Location = New System.Drawing.Point(521, 49)
        Me.txtDomicilioCliente.Multilinea = False
        Me.txtDomicilioCliente.Name = "txtDomicilioCliente"
        Me.txtDomicilioCliente.Size = New System.Drawing.Size(225, 21)
        Me.txtDomicilioCliente.SoloLectura = False
        Me.txtDomicilioCliente.TabIndex = 7
        Me.txtDomicilioCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDomicilioCliente.Texto = ""
        '
        'lblDomicilioCliente
        '
        Me.lblDomicilioCliente.AutoSize = True
        Me.lblDomicilioCliente.Location = New System.Drawing.Point(469, 53)
        Me.lblDomicilioCliente.Name = "lblDomicilioCliente"
        Me.lblDomicilioCliente.Size = New System.Drawing.Size(52, 13)
        Me.lblDomicilioCliente.TabIndex = 6
        Me.lblDomicilioCliente.Text = "Domicilio:"
        '
        'cbxTransporte
        '
        Me.cbxTransporte.CampoWhere = Nothing
        Me.cbxTransporte.CargarUnaSolaVez = False
        Me.cbxTransporte.DataDisplayMember = "Descripcion"
        Me.cbxTransporte.DataFilter = Nothing
        Me.cbxTransporte.DataOrderBy = "Descripcion"
        Me.cbxTransporte.DataSource = "VTransporte"
        Me.cbxTransporte.DataValueMember = "ID"
        Me.cbxTransporte.FormABM = Nothing
        Me.cbxTransporte.Indicaciones = Nothing
        Me.cbxTransporte.Location = New System.Drawing.Point(208, 184)
        Me.cbxTransporte.Name = "cbxTransporte"
        Me.cbxTransporte.SeleccionObligatoria = True
        Me.cbxTransporte.Size = New System.Drawing.Size(167, 21)
        Me.cbxTransporte.SoloLectura = False
        Me.cbxTransporte.TabIndex = 36
        Me.cbxTransporte.Texto = ""
        '
        'txtMotivoTranslado
        '
        Me.txtMotivoTranslado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMotivoTranslado.Color = System.Drawing.Color.Beige
        Me.txtMotivoTranslado.Indicaciones = Nothing
        Me.txtMotivoTranslado.Location = New System.Drawing.Point(91, 77)
        Me.txtMotivoTranslado.Multilinea = False
        Me.txtMotivoTranslado.Name = "txtMotivoTranslado"
        Me.txtMotivoTranslado.Size = New System.Drawing.Size(150, 21)
        Me.txtMotivoTranslado.SoloLectura = False
        Me.txtMotivoTranslado.TabIndex = 9
        Me.txtMotivoTranslado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMotivoTranslado.Texto = ""
        '
        'lblMotivoTranslado
        '
        Me.lblMotivoTranslado.AutoSize = True
        Me.lblMotivoTranslado.Location = New System.Drawing.Point(46, 81)
        Me.lblMotivoTranslado.Name = "lblMotivoTranslado"
        Me.lblMotivoTranslado.Size = New System.Drawing.Size(42, 13)
        Me.lblMotivoTranslado.TabIndex = 8
        Me.lblMotivoTranslado.Text = "Motivo:"
        '
        'cbxChofer
        '
        Me.cbxChofer.CampoWhere = Nothing
        Me.cbxChofer.CargarUnaSolaVez = False
        Me.cbxChofer.DataDisplayMember = "Nombres"
        Me.cbxChofer.DataFilter = Nothing
        Me.cbxChofer.DataOrderBy = "Nombres"
        Me.cbxChofer.DataSource = "VChofer"
        Me.cbxChofer.DataValueMember = "ID"
        Me.cbxChofer.FormABM = Nothing
        Me.cbxChofer.Indicaciones = Nothing
        Me.cbxChofer.Location = New System.Drawing.Point(91, 211)
        Me.cbxChofer.Name = "cbxChofer"
        Me.cbxChofer.SeleccionObligatoria = True
        Me.cbxChofer.Size = New System.Drawing.Size(210, 21)
        Me.cbxChofer.SoloLectura = False
        Me.cbxChofer.TabIndex = 42
        Me.cbxChofer.Texto = ""
        '
        'lblChofer
        '
        Me.lblChofer.AutoSize = True
        Me.lblChofer.Location = New System.Drawing.Point(47, 215)
        Me.lblChofer.Name = "lblChofer"
        Me.lblChofer.Size = New System.Drawing.Size(41, 13)
        Me.lblChofer.TabIndex = 41
        Me.lblChofer.Text = "Chofer:"
        '
        'cbxVehiculo
        '
        Me.cbxVehiculo.CampoWhere = Nothing
        Me.cbxVehiculo.CargarUnaSolaVez = False
        Me.cbxVehiculo.DataDisplayMember = "Descripcion"
        Me.cbxVehiculo.DataFilter = Nothing
        Me.cbxVehiculo.DataOrderBy = Nothing
        Me.cbxVehiculo.DataSource = "VCamion"
        Me.cbxVehiculo.DataValueMember = "ID"
        Me.cbxVehiculo.FormABM = Nothing
        Me.cbxVehiculo.Indicaciones = Nothing
        Me.cbxVehiculo.Location = New System.Drawing.Point(427, 184)
        Me.cbxVehiculo.Name = "cbxVehiculo"
        Me.cbxVehiculo.SeleccionObligatoria = True
        Me.cbxVehiculo.Size = New System.Drawing.Size(154, 21)
        Me.cbxVehiculo.SoloLectura = False
        Me.cbxVehiculo.TabIndex = 38
        Me.cbxVehiculo.Texto = ""
        '
        'lblVehiculo
        '
        Me.lblVehiculo.AutoSize = True
        Me.lblVehiculo.Location = New System.Drawing.Point(373, 188)
        Me.lblVehiculo.Name = "lblVehiculo"
        Me.lblVehiculo.Size = New System.Drawing.Size(51, 13)
        Me.lblVehiculo.TabIndex = 37
        Me.lblVehiculo.Text = "Vehiculo:"
        '
        'txtComprobanteVenta
        '
        Me.txtComprobanteVenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobanteVenta.Color = System.Drawing.Color.Beige
        Me.txtComprobanteVenta.Indicaciones = Nothing
        Me.txtComprobanteVenta.Location = New System.Drawing.Point(594, 77)
        Me.txtComprobanteVenta.Multilinea = False
        Me.txtComprobanteVenta.Name = "txtComprobanteVenta"
        Me.txtComprobanteVenta.Size = New System.Drawing.Size(152, 21)
        Me.txtComprobanteVenta.SoloLectura = False
        Me.txtComprobanteVenta.TabIndex = 13
        Me.txtComprobanteVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobanteVenta.Texto = ""
        '
        'lblComprobanteVenta
        '
        Me.lblComprobanteVenta.AutoSize = True
        Me.lblComprobanteVenta.Location = New System.Drawing.Point(469, 81)
        Me.lblComprobanteVenta.Name = "lblComprobanteVenta"
        Me.lblComprobanteVenta.Size = New System.Drawing.Size(127, 13)
        Me.lblComprobanteVenta.TabIndex = 12
        Me.lblComprobanteVenta.Text = "Comprobante Venta Nro.:"
        '
        'cbxSucursalOperacion
        '
        Me.cbxSucursalOperacion.CampoWhere = Nothing
        Me.cbxSucursalOperacion.CargarUnaSolaVez = False
        Me.cbxSucursalOperacion.DataDisplayMember = "Codigo"
        Me.cbxSucursalOperacion.DataFilter = Nothing
        Me.cbxSucursalOperacion.DataOrderBy = "Codigo"
        Me.cbxSucursalOperacion.DataSource = "VSucursal"
        Me.cbxSucursalOperacion.DataValueMember = "ID"
        Me.cbxSucursalOperacion.FormABM = Nothing
        Me.cbxSucursalOperacion.Indicaciones = Nothing
        Me.cbxSucursalOperacion.Location = New System.Drawing.Point(91, 19)
        Me.cbxSucursalOperacion.Name = "cbxSucursalOperacion"
        Me.cbxSucursalOperacion.SeleccionObligatoria = True
        Me.cbxSucursalOperacion.Size = New System.Drawing.Size(57, 21)
        Me.cbxSucursalOperacion.SoloLectura = False
        Me.cbxSucursalOperacion.TabIndex = 1
        Me.cbxSucursalOperacion.Texto = ""
        '
        'lblSucursalOperacion
        '
        Me.lblSucursalOperacion.AutoSize = True
        Me.lblSucursalOperacion.Location = New System.Drawing.Point(37, 23)
        Me.lblSucursalOperacion.Name = "lblSucursalOperacion"
        Me.lblSucursalOperacion.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursalOperacion.TabIndex = 0
        Me.lblSucursalOperacion.Text = "Sucursal:"
        '
        'txtDireccionLlegada
        '
        Me.txtDireccionLlegada.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccionLlegada.Color = System.Drawing.Color.Beige
        Me.txtDireccionLlegada.Indicaciones = Nothing
        Me.txtDireccionLlegada.Location = New System.Drawing.Point(91, 157)
        Me.txtDireccionLlegada.Multilinea = False
        Me.txtDireccionLlegada.Name = "txtDireccionLlegada"
        Me.txtDireccionLlegada.Size = New System.Drawing.Size(284, 21)
        Me.txtDireccionLlegada.SoloLectura = False
        Me.txtDireccionLlegada.TabIndex = 28
        Me.txtDireccionLlegada.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccionLlegada.Texto = ""
        '
        'txtFechaFin
        '
        Me.txtFechaFin.Color = System.Drawing.Color.Empty
        Me.txtFechaFin.Fecha = New Date(CType(0, Long))
        Me.txtFechaFin.Location = New System.Drawing.Point(673, 104)
        Me.txtFechaFin.Name = "txtFechaFin"
        Me.txtFechaFin.PermitirNulo = False
        Me.txtFechaFin.Size = New System.Drawing.Size(73, 20)
        Me.txtFechaFin.SoloLectura = False
        Me.txtFechaFin.TabIndex = 20
        '
        'cbxDepositoOperacion
        '
        Me.cbxDepositoOperacion.CampoWhere = Nothing
        Me.cbxDepositoOperacion.CargarUnaSolaVez = False
        Me.cbxDepositoOperacion.DataDisplayMember = "Deposito"
        Me.cbxDepositoOperacion.DataFilter = Nothing
        Me.cbxDepositoOperacion.DataOrderBy = Nothing
        Me.cbxDepositoOperacion.DataSource = "VDeposito"
        Me.cbxDepositoOperacion.DataValueMember = "ID"
        Me.cbxDepositoOperacion.FormABM = Nothing
        Me.cbxDepositoOperacion.Indicaciones = Nothing
        Me.cbxDepositoOperacion.Location = New System.Drawing.Point(200, 19)
        Me.cbxDepositoOperacion.Name = "cbxDepositoOperacion"
        Me.cbxDepositoOperacion.SeleccionObligatoria = False
        Me.cbxDepositoOperacion.Size = New System.Drawing.Size(269, 21)
        Me.cbxDepositoOperacion.SoloLectura = False
        Me.cbxDepositoOperacion.TabIndex = 3
        Me.cbxDepositoOperacion.Texto = ""
        '
        'lblDepositoOperacion
        '
        Me.lblDepositoOperacion.AutoSize = True
        Me.lblDepositoOperacion.Location = New System.Drawing.Point(148, 23)
        Me.lblDepositoOperacion.Name = "lblDepositoOperacion"
        Me.lblDepositoOperacion.Size = New System.Drawing.Size(52, 13)
        Me.lblDepositoOperacion.TabIndex = 2
        Me.lblDepositoOperacion.Text = "Deposito:"
        '
        'txtFechaInicio
        '
        Me.txtFechaInicio.Color = System.Drawing.Color.Empty
        Me.txtFechaInicio.Fecha = New Date(CType(0, Long))
        Me.txtFechaInicio.Location = New System.Drawing.Point(595, 104)
        Me.txtFechaInicio.Name = "txtFechaInicio"
        Me.txtFechaInicio.PermitirNulo = False
        Me.txtFechaInicio.Size = New System.Drawing.Size(67, 20)
        Me.txtFechaInicio.SoloLectura = False
        Me.txtFechaInicio.TabIndex = 19
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(426, 108)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(170, 13)
        Me.lblFecha.TabIndex = 18
        Me.lblFecha.Text = "Fecha de Inicio/ Fin de Translado:"
        '
        'txtDireccionPartida
        '
        Me.txtDireccionPartida.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccionPartida.Color = System.Drawing.Color.Beige
        Me.txtDireccionPartida.Indicaciones = Nothing
        Me.txtDireccionPartida.Location = New System.Drawing.Point(91, 130)
        Me.txtDireccionPartida.Multilinea = False
        Me.txtDireccionPartida.Name = "txtDireccionPartida"
        Me.txtDireccionPartida.Size = New System.Drawing.Size(284, 21)
        Me.txtDireccionPartida.SoloLectura = False
        Me.txtDireccionPartida.TabIndex = 22
        Me.txtDireccionPartida.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccionPartida.Texto = ""
        '
        'lblDireccionPartida
        '
        Me.lblDireccionPartida.AutoSize = True
        Me.lblDireccionPartida.Location = New System.Drawing.Point(11, 134)
        Me.lblDireccionPartida.Name = "lblDireccionPartida"
        Me.lblDireccionPartida.Size = New System.Drawing.Size(77, 13)
        Me.lblDireccionPartida.TabIndex = 21
        Me.lblDireccionPartida.Text = "Dir. de Partida:"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(46, 53)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(42, 13)
        Me.lblCliente.TabIndex = 4
        Me.lblCliente.Text = "Cliente:"
        '
        'lblTransporte
        '
        Me.lblTransporte.AutoSize = True
        Me.lblTransporte.Location = New System.Drawing.Point(147, 188)
        Me.lblTransporte.Name = "lblTransporte"
        Me.lblTransporte.Size = New System.Drawing.Size(61, 13)
        Me.lblTransporte.TabIndex = 35
        Me.lblTransporte.Text = "Transporte:"
        '
        'lblDireccionLlegada
        '
        Me.lblDireccionLlegada.AutoSize = True
        Me.lblDireccionLlegada.Location = New System.Drawing.Point(6, 161)
        Me.lblDireccionLlegada.Name = "lblDireccionLlegada"
        Me.lblDireccionLlegada.Size = New System.Drawing.Size(82, 13)
        Me.lblDireccionLlegada.TabIndex = 27
        Me.lblDireccionLlegada.Text = "Dir. de Llegada:"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnPreparar)
        Me.Panel2.Controls.Add(Me.btnImprimir)
        Me.Panel2.Controls.Add(Me.btnSalir)
        Me.Panel2.Controls.Add(Me.txtCantidadMaximaProductos)
        Me.Panel2.Controls.Add(Me.lblCantidadDetalle)
        Me.Panel2.Controls.Add(Me.btnCancelar)
        Me.Panel2.Location = New System.Drawing.Point(10, 264)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(762, 34)
        Me.Panel2.TabIndex = 1
        '
        'btnPreparar
        '
        Me.btnPreparar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreparar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnPreparar.Location = New System.Drawing.Point(395, 6)
        Me.btnPreparar.Name = "btnPreparar"
        Me.btnPreparar.Size = New System.Drawing.Size(75, 23)
        Me.btnPreparar.TabIndex = 2
        Me.btnPreparar.Text = "Preparar"
        Me.btnPreparar.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnImprimir.Location = New System.Drawing.Point(476, 6)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 3
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnSalir.Location = New System.Drawing.Point(672, 6)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 5
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'txtCantidadMaximaProductos
        '
        Me.txtCantidadMaximaProductos.Color = System.Drawing.Color.Empty
        Me.txtCantidadMaximaProductos.Decimales = True
        Me.txtCantidadMaximaProductos.Indicaciones = Nothing
        Me.txtCantidadMaximaProductos.Location = New System.Drawing.Point(275, 7)
        Me.txtCantidadMaximaProductos.Name = "txtCantidadMaximaProductos"
        Me.txtCantidadMaximaProductos.Size = New System.Drawing.Size(56, 21)
        Me.txtCantidadMaximaProductos.SoloLectura = False
        Me.txtCantidadMaximaProductos.TabIndex = 1
        Me.txtCantidadMaximaProductos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadMaximaProductos.Texto = "0"
        '
        'lblCantidadDetalle
        '
        Me.lblCantidadDetalle.Location = New System.Drawing.Point(108, 4)
        Me.lblCantidadDetalle.Name = "lblCantidadDetalle"
        Me.lblCantidadDetalle.Size = New System.Drawing.Size(161, 26)
        Me.lblCantidadDetalle.TabIndex = 0
        Me.lblCantidadDetalle.Text = "Cantidad maxima de productos:"
        Me.lblCantidadDetalle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnCancelar.Location = New System.Drawing.Point(555, 6)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 4
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'frmImprimirRemisionPorDeposito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(779, 311)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Name = "frmImprimirRemisionPorDeposito"
        Me.Text = "frmImprimirRemision"
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents cbxTransporte As ERP.ocxCBX
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents txtMotivoTranslado As ERP.ocxTXTString
    Friend WithEvents lblMotivoTranslado As System.Windows.Forms.Label
    Friend WithEvents cbxChofer As ERP.ocxCBX
    Friend WithEvents lblChofer As System.Windows.Forms.Label
    Friend WithEvents cbxVehiculo As ERP.ocxCBX
    Friend WithEvents lblVehiculo As System.Windows.Forms.Label
    Friend WithEvents txtComprobanteVenta As ERP.ocxTXTString
    Friend WithEvents lblComprobanteVenta As System.Windows.Forms.Label
    Friend WithEvents cbxSucursalOperacion As ERP.ocxCBX
    Friend WithEvents lblSucursalOperacion As System.Windows.Forms.Label
    Friend WithEvents txtDireccionLlegada As ERP.ocxTXTString
    Friend WithEvents txtFechaFin As ERP.ocxTXTDate
    Friend WithEvents cbxDepositoOperacion As ERP.ocxCBX
    Friend WithEvents lblDepositoOperacion As System.Windows.Forms.Label
    Friend WithEvents txtFechaInicio As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents txtDireccionPartida As ERP.ocxTXTString
    Friend WithEvents lblDireccionPartida As System.Windows.Forms.Label
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lblTransporte As System.Windows.Forms.Label
    Friend WithEvents lblDireccionLlegada As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents txtCantidadMaximaProductos As ERP.ocxTXTNumeric
    Friend WithEvents lblCantidadDetalle As System.Windows.Forms.Label
    Friend WithEvents btnPreparar As System.Windows.Forms.Button
    Friend WithEvents txtDomicilioCliente As ERP.ocxTXTString
    Friend WithEvents lblDomicilioCliente As System.Windows.Forms.Label
    Friend WithEvents txtComprobanteVentaTipo As ERP.ocxTXTString
    Friend WithEvents lblComprobanteVentaTipo As System.Windows.Forms.Label
    Friend WithEvents txtDepartamentoLlegada As ERP.ocxTXTString
    Friend WithEvents lblDepartamentoLlegada As System.Windows.Forms.Label
    Friend WithEvents txtCiudadLlegada As ERP.ocxTXTString
    Friend WithEvents lblCiudadLlegada As System.Windows.Forms.Label
    Friend WithEvents txtDepartamentoPartida As ERP.ocxTXTString
    Friend WithEvents lblDepartamentoPartida As System.Windows.Forms.Label
    Friend WithEvents txtCiudadPartida As ERP.ocxTXTString
    Friend WithEvents lblCiudadPartida As System.Windows.Forms.Label
    Friend WithEvents txtFechaExpedicionVenta As ERP.ocxTXTDate
    Friend WithEvents lblFechaExpedicionVenta As System.Windows.Forms.Label
    Friend WithEvents txtTimbradoVenta As ERP.ocxTXTString
    Friend WithEvents lblTimbradoVenta As System.Windows.Forms.Label
    Friend WithEvents lblDomicilioChofer As System.Windows.Forms.Label
    Friend WithEvents txtDomicilioChofer As ERP.ocxTXTString
    Friend WithEvents lblRUCChofer As System.Windows.Forms.Label
    Friend WithEvents txtRUCChofer As ERP.ocxTXTString
    Friend WithEvents lblPatenteVehiculo As System.Windows.Forms.Label
    Friend WithEvents txtPatenteVehiculo As ERP.ocxTXTString
    Friend WithEvents txtKilometrajeEstimativo As ERP.ocxTXTString
    Friend WithEvents lblKilometrajeEstimativo As System.Windows.Forms.Label
End Class

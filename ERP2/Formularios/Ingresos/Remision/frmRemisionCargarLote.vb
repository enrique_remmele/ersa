﻿Public Class frmRemisionCargarLote

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set

    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set

    End Property

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Foco
        Me.ActiveControl = txtNroComprobante
        chkVerCobrados.Valor = False
    End Sub

    Sub CargarComprobante(Optional Where As String = "")
        Dim vCobrados As String = ""
        If chkVerCobrados.Valor = False Then
            vCobrados = " and Idtransaccion not in (select IdTransaccionLote from CobranzaContado)"
        End If

        dgvComprobante.Rows.Clear()

        Dim SQL As String = "Select IDTransaccion, Sucursal, [Cod.], Comprobante, Fecha, Direccion From VLoteDistribucion Where Anulado='False' "

        If Where <> "" Then
            SQL = SQL & " And " & Where
        End If

        If vCobrados <> "" Then
            SQL = SQL & vCobrados
        End If

        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        For Each oRow As DataRow In dt.Rows
            Dim Registro(dgvComprobante.Columns.Count - 1) As String
            Registro(0) = oRow("IDTransaccion")
            Registro(1) = False
            Registro(2) = oRow("Sucursal")
            Registro(3) = oRow("Cod.")
            Registro(4) = oRow("Comprobante")
            Registro(5) = oRow("Fecha")
            Registro(6) = oRow("Direccion")

            dgvComprobante.Rows.Add(Registro)

        Next

        'Formato
        dgvComprobante.Columns("Sel").ReadOnly = False
        dgvComprobante.Columns("IDTransaccion").Visible = False
        dgvComprobante.Columns("Direccion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        'Totales
        txtCantidadComprobante.SetValue(dgvComprobante.RowCount)

        If dgvComprobante.Rows.Count > 0 Then
            Me.ActiveControl = dgvComprobante
        End If

    End Sub

    Sub CargarProductos()


        Dim SQL As String = "Select IDProducto, Referencia, Producto, CodigoBarra, 'Cantidad'=SUM(Cantidad) From VDetalleLoteDistribucion "
        Dim Where As String = ""
        Dim GroupBy As String = " Group By IDProducto, Referencia, Producto, CodigoBarra "

        For Each oRow As DataGridViewRow In dgvComprobante.Rows

            If oRow.Cells("Sel").Value = True Then
                If Where = "" Then
                    Where = " Where (IDTransaccion=" & oRow.Cells("IDTransaccion").Value & ") "
                Else
                    Where = Where & " Or (IDTransaccion=" & oRow.Cells("IDTransaccion").Value & ") "
                End If
            End If
        Next

        If Where = "" Then
            dt.Rows.Clear()
        Else
            dt = CSistema.ExecuteToDataTable(SQL & Where & GroupBy)
        End If

        CSistema.dtToGrid(dgvDetalle, dt)

        'Formato
        dgvDetalle.Columns("IDProducto").Visible = False
        dgvDetalle.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvDetalle.Columns("Cantidad").DefaultCellStyle.Format = "N2"
        dgvDetalle.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        If dgvDetalle.Rows.Count > 0 Then
            Me.ActiveControl = dgvComprobante
        End If

        'Totales
        txtCantidadDetalle.SetValue(dgvDetalle.RowCount)

    End Sub

    Sub Aceptar()

        Procesado = True
        Me.Close()

    End Sub

    Sub Cancelar()

        Procesado = False
        Me.Close()

    End Sub

    Private Sub frmRemisionCargarFactura_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If Me.ActiveControl.Name = txtNroComprobante.Name Then
            Exit Sub
        End If

        If Me.ActiveControl.Name = dgvComprobante.Name Then
            Exit Sub
        End If

        If Me.ActiveControl.Name = dgvDetalle.Name Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmRemisionCargarFactura_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCargarComprobante_Click(sender As System.Object, e As System.EventArgs) Handles btnCargarComprobante.Click
        CargarComprobante(" Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' and IDSucursal = " & IDSucursal)
    End Sub

    Private Sub lklComprobantesMes_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklComprobantesMes.LinkClicked
        CargarComprobante(" Month(Fecha)=" & Date.Now.Month & " And Year(Fecha)=" & Date.Now.Year & " ")
    End Sub

    Private Sub lklSeleccionarTodo_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklSeleccionarTodo.LinkClicked

        If lklSeleccionarTodo.Text = "Seleccionar todo" Then
            For Each oRow As DataGridViewRow In dgvComprobante.Rows
                oRow.Cells("Sel").Value = True
            Next
            lklSeleccionarTodo.Text = "Quitar todo"
            CargarProductos()
            Exit Sub
        End If

        If lklSeleccionarTodo.Text = "Quitar todo" Then
            For Each oRow As DataGridViewRow In dgvComprobante.Rows
                oRow.Cells("Sel").Value = False
            Next
            lklSeleccionarTodo.Text = "Seleccionar todo"
            CargarProductos()
            Exit Sub
        End If

    End Sub

    Private Sub btnCargarMercaderias_Click(sender As System.Object, e As System.EventArgs) Handles btnCargarMercaderias.Click
        CargarProductos()
    End Sub

    Private Sub lklQuitarSeleccionados_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklQuitarSeleccionados.LinkClicked

    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Aceptar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then

            If txtNroComprobante.GetValue = "" Then
                Exit Sub
            End If

            CargarComprobante(" Comprobante Like '%" & txtNroComprobante.GetValue & "%'")
        End If
    End Sub

    Private Sub dgvComprobante_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvComprobante.CellClick

    End Sub

    Private Sub dgvComprobante_Click(sender As Object, e As System.EventArgs) Handles dgvComprobante.Click
        dgvComprobante.SelectedRows(0).Cells("Sel").Value = CStr(Not CBool(dgvComprobante.SelectedRows(0).Cells("Sel").Value))
        CargarProductos()
    End Sub

    Private Sub dgvComprobante_DoubleClick(sender As Object, e As System.EventArgs) Handles dgvComprobante.DoubleClick

    End Sub

    Private Sub dgvComprobante_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvComprobante.KeyDown

        If e.KeyCode = Keys.Enter Then

            'MArcar
            If dgvComprobante.SelectedRows.Count > 0 Then

                dgvComprobante.SelectedRows(0).Cells("Sel").Value = CStr(Not CBool(dgvComprobante.SelectedRows(0).Cells("Sel").Value))

                If dgvComprobante.SelectedRows(0).Index <> dgvComprobante.Rows.Count - 1 Then
                    dgvComprobante.CurrentCell = dgvComprobante.SelectedRows(0).Cells("Sel")
                End If

                CargarProductos()
            End If
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub dgvDetalle_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvDetalle.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If
    End Sub

    Private Sub dgvComprobante_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvComprobante.KeyUp

    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmRemisionCargarLote_Activate()
        Me.Refresh()
    End Sub
End Class
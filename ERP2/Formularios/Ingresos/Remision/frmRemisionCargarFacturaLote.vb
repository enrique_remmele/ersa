﻿Public Class frmRemisionCargarFacturaLote

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Private LoteValue As Integer
    Public Property Lote() As Integer
        Get
            Return LoteValue
        End Get
        Set(ByVal value As Integer)
            LoteValue = value
        End Set

    End Property

    Dim dtLoteDistribucion As DataTable
    Dim dtVentaLote As DataTable
    Dim dtDetalleLoteDistribucion As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarComprobante()
    End Sub

    Sub CargarComprobante(Optional Where As String = "")

        dgvComprobante.Rows.Clear()

        'Dim SQL As String = "Select V.IDTransaccion, V.Sucursal, V.[Cod.], V.Comprobante, V.Fecha, V.Direccion From VVenta V Left Outer Join VentaLoteDistribucion VLD on VLD.IDTransaccionVenta = V.IDTransaccion Join LoteDistribucion LD ON LD.IDTransaccion = VLD.IDTransaccionLote Where LD.Numero =" & Lote & " and V.Anulado='False' and Procesado='True'"
        Dim SQL As String = "Select v.* From VVenta V Left Outer Join VentaLoteDistribucion VLD on VLD.IDTransaccionVenta = V.IDTransaccion Join LoteDistribucion LD ON LD.IDTransaccion = VLD.IDTransaccionLote Where LD.Numero =" & Lote & " and V.Anulado='False' and Procesado='True'"

        If Where <> "" Then
            SQL = SQL & " And " & Where
        End If

        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        For Each oRow As DataRow In dt.Rows
            Dim Registro(dgvComprobante.Columns.Count - 1) As String
            Registro(0) = oRow("IDTransaccion")
            Registro(1) = False
            Registro(2) = oRow("Sucursal")
            Registro(3) = oRow("Cod.")
            Registro(4) = oRow("Comprobante")
            Registro(5) = oRow("Fecha")
            Registro(6) = oRow("Direccion")
            Registro(7) = oRow("Cliente")
            Registro(8) = oRow("IDCliente")
            Registro(9) = oRow("IDSucursalCliente")

            dgvComprobante.Rows.Add(Registro)

        Next

        'Formato
        'dgvComprobante.Columns("Sucursal").Visible = True
        'dgvComprobante.Columns("Cod.").Visible = True
        'dgvComprobante.Columns("Comprobante").Visible = True
        'dgvComprobante.Columns("Fecha").Visible = True
        'dgvComprobante.Columns("Direccion").Visible = True

        dgvComprobante.Columns("Sel").ReadOnly = False
        dgvComprobante.Columns("IDTransaccion").Visible = False
        dgvComprobante.Columns("Direccion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        dgvComprobante.Columns("Cliente").Visible = False
        dgvComprobante.Columns("IDCliente").Visible = False
        dgvComprobante.Columns("IDSucursalCliente").Visible = False

        'Totales
        txtCantidadComprobante.SetValue(dgvComprobante.RowCount)

        If dgvComprobante.Rows.Count > 0 Then
            Me.ActiveControl = dgvComprobante
        End If

    End Sub

    Sub CargarProductos()

        Dim SQL As String = "Select IDProducto, Referencia, Producto, CodigoBarra, 'Cantidad'=SUM(Cantidad),IDTransaccion From VDetalleVenta "
        Dim Where As String = ""
        Dim GroupBy As String = " Group By IDProducto, Referencia, Producto, CodigoBarra,IDTransaccion "

        For Each oRow As DataGridViewRow In dgvComprobante.Rows

            If oRow.Cells("Sel").Value = True Then
                If Where = "" Then
                    Where = " Where (IDTransaccion=" & oRow.Cells("IDTransaccion").Value & ") "
                Else
                    Where = Where & " Or (IDTransaccion=" & oRow.Cells("IDTransaccion").Value & ") "
                End If
            End If
        Next

        If Where = "" Then
            dt.Rows.Clear()
        Else
            dt = CSistema.ExecuteToDataTable(SQL & Where & GroupBy)
        End If

        CSistema.dtToGrid(dgvDetalle, dt)

        'Formato
        dgvDetalle.Columns("IDProducto").Visible = False
        dgvDetalle.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvDetalle.Columns("Cantidad").DefaultCellStyle.Format = "N2"
        dgvDetalle.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        If dgvDetalle.Rows.Count > 0 Then
            Me.ActiveControl = dgvComprobante
        End If

        'Totales
        txtCantidadDetalle.SetValue(dgvDetalle.RowCount)

    End Sub

    Sub Aceptar()

        Procesado = True
        Me.Close()

    End Sub

    Sub Cancelar()

        Procesado = False
        Me.Close()

    End Sub

    Private Sub lklSeleccionarTodo_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklSeleccionarTodo.LinkClicked

        If lklSeleccionarTodo.Text = "Seleccionar todo" Then
            For Each oRow As DataGridViewRow In dgvComprobante.Rows
                oRow.Cells("Sel").Value = True
            Next
            lklSeleccionarTodo.Text = "Quitar todo"
            CargarProductos()
            Exit Sub
        End If

        If lklSeleccionarTodo.Text = "Quitar todo" Then
            For Each oRow As DataGridViewRow In dgvComprobante.Rows
                oRow.Cells("Sel").Value = False
            Next
            lklSeleccionarTodo.Text = "Seleccionar todo"
            CargarProductos()
            Exit Sub
        End If

    End Sub

    Private Sub btnCargarMercaderias_Click(sender As System.Object, e As System.EventArgs) Handles btnCargarMercaderias.Click
        CargarProductos()
    End Sub


    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Aceptar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub dgvComprobante_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvComprobante.CellClick

    End Sub

    Private Sub dgvComprobante_Click(sender As Object, e As System.EventArgs) Handles dgvComprobante.Click
        dgvComprobante.SelectedRows(0).Cells("Sel").Value = CStr(Not CBool(dgvComprobante.SelectedRows(0).Cells("Sel").Value))
        CargarProductos()
    End Sub

    Private Sub dgvComprobante_DoubleClick(sender As Object, e As System.EventArgs) Handles dgvComprobante.DoubleClick

    End Sub

    Private Sub dgvComprobante_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvComprobante.KeyDown

        If e.KeyCode = Keys.Enter Then

            'MArcar
            If dgvComprobante.SelectedRows.Count > 0 Then

                dgvComprobante.SelectedRows(0).Cells("Sel").Value = CStr(Not CBool(dgvComprobante.SelectedRows(0).Cells("Sel").Value))

                If dgvComprobante.SelectedRows(0).Index <> dgvComprobante.Rows.Count - 1 Then
                    dgvComprobante.CurrentCell = dgvComprobante.SelectedRows(0).Cells("Sel")
                End If

                CargarProductos()
            End If
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub dgvDetalle_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvDetalle.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If
    End Sub

    Private Sub frmRemisionCargarFacturaLote_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyUp

        If Me.ActiveControl.Name = dgvComprobante.Name Then
            Exit Sub
        End If

        If Me.ActiveControl.Name = dgvDetalle.Name Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmRemisionCargarFacturaLote_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Me.Refresh()
    End Sub

    Private Sub frmRemisionCargarFacturaLote_Load(sender As Object, e As EventArgs) Handles Me.Load
        Inicializar()
    End Sub
End Class
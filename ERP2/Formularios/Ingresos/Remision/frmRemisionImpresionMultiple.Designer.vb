﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRemisionImpresionMultiple
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxImpresora = New ERP.ocxCBX()
        Me.btnCargar = New System.Windows.Forms.Button()
        Me.txtOPFinal = New ERP.ocxTXTNumeric()
        Me.txtDesde = New ERP.ocxTXTNumeric()
        Me.lblDesdeHasta = New System.Windows.Forms.Label()
        Me.btnDetalle = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.OcxCHK1 = New ERP.ocxCHK()
        Me.OcxConfiguracionCobranzaCredito1 = New ERP.ocxConfiguracionCobranzaCredito()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(44, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 44
        Me.Label2.Text = "Impresora:"
        '
        'cbxImpresora
        '
        Me.cbxImpresora.CampoWhere = Nothing
        Me.cbxImpresora.CargarUnaSolaVez = False
        Me.cbxImpresora.DataDisplayMember = Nothing
        Me.cbxImpresora.DataFilter = Nothing
        Me.cbxImpresora.DataOrderBy = Nothing
        Me.cbxImpresora.DataSource = Nothing
        Me.cbxImpresora.DataValueMember = Nothing
        Me.cbxImpresora.FormABM = Nothing
        Me.cbxImpresora.Indicaciones = Nothing
        Me.cbxImpresora.Location = New System.Drawing.Point(107, 40)
        Me.cbxImpresora.Name = "cbxImpresora"
        Me.cbxImpresora.SeleccionObligatoria = True
        Me.cbxImpresora.Size = New System.Drawing.Size(213, 21)
        Me.cbxImpresora.SoloLectura = False
        Me.cbxImpresora.TabIndex = 45
        Me.cbxImpresora.Texto = ""
        '
        'btnCargar
        '
        Me.btnCargar.Location = New System.Drawing.Point(7, 94)
        Me.btnCargar.Name = "btnCargar"
        Me.btnCargar.Size = New System.Drawing.Size(75, 23)
        Me.btnCargar.TabIndex = 46
        Me.btnCargar.Text = "&Cargar"
        Me.btnCargar.UseVisualStyleBackColor = True
        '
        'txtOPFinal
        '
        Me.txtOPFinal.Color = System.Drawing.Color.Empty
        Me.txtOPFinal.Decimales = True
        Me.txtOPFinal.Indicaciones = Nothing
        Me.txtOPFinal.Location = New System.Drawing.Point(217, 12)
        Me.txtOPFinal.Name = "txtOPFinal"
        Me.txtOPFinal.Size = New System.Drawing.Size(103, 22)
        Me.txtOPFinal.SoloLectura = False
        Me.txtOPFinal.TabIndex = 33
        Me.txtOPFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOPFinal.Texto = "0"
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Decimales = True
        Me.txtDesde.Indicaciones = Nothing
        Me.txtDesde.Location = New System.Drawing.Point(108, 12)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.Size = New System.Drawing.Size(103, 22)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 32
        Me.txtDesde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDesde.Texto = "0"
        '
        'lblDesdeHasta
        '
        Me.lblDesdeHasta.AutoSize = True
        Me.lblDesdeHasta.Location = New System.Drawing.Point(31, 14)
        Me.lblDesdeHasta.Name = "lblDesdeHasta"
        Me.lblDesdeHasta.Size = New System.Drawing.Size(70, 13)
        Me.lblDesdeHasta.TabIndex = 31
        Me.lblDesdeHasta.Text = "Inicial / Final:"
        '
        'btnDetalle
        '
        Me.btnDetalle.Location = New System.Drawing.Point(88, 94)
        Me.btnDetalle.Name = "btnDetalle"
        Me.btnDetalle.Size = New System.Drawing.Size(75, 23)
        Me.btnDetalle.TabIndex = 47
        Me.btnDetalle.Text = "&Detalle"
        Me.btnDetalle.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(169, 94)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 48
        Me.btnImprimir.Text = "&Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(250, 94)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 49
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'OcxCHK1
        '
        Me.OcxCHK1.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK1.Color = System.Drawing.Color.Empty
        Me.OcxCHK1.Location = New System.Drawing.Point(107, 67)
        Me.OcxCHK1.Name = "OcxCHK1"
        Me.OcxCHK1.Size = New System.Drawing.Size(212, 21)
        Me.OcxCHK1.SoloLectura = False
        Me.OcxCHK1.TabIndex = 50
        Me.OcxCHK1.Texto = "Imprimir Nro de Comprobante"
        Me.OcxCHK1.Valor = False
        '
        'OcxConfiguracionCobranzaCredito1
        '
        Me.OcxConfiguracionCobranzaCredito1.Location = New System.Drawing.Point(0, 0)
        Me.OcxConfiguracionCobranzaCredito1.Name = "OcxConfiguracionCobranzaCredito1"
        Me.OcxConfiguracionCobranzaCredito1.Size = New System.Drawing.Size(400, 300)
        Me.OcxConfiguracionCobranzaCredito1.TabIndex = 51
        '
        'frmRemisionImpresionMultiple
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(340, 129)
        Me.Controls.Add(Me.OcxConfiguracionCobranzaCredito1)
        Me.Controls.Add(Me.OcxCHK1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbxImpresora)
        Me.Controls.Add(Me.btnCargar)
        Me.Controls.Add(Me.txtOPFinal)
        Me.Controls.Add(Me.txtDesde)
        Me.Controls.Add(Me.lblDesdeHasta)
        Me.Controls.Add(Me.btnDetalle)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnSalir)
        Me.Name = "frmRemisionImpresionMultiple"
        Me.Text = "frmRemisionImpresionMultiple"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxImpresora As ERP.ocxCBX
    Friend WithEvents btnCargar As System.Windows.Forms.Button
    Friend WithEvents txtOPFinal As ERP.ocxTXTNumeric
    Friend WithEvents txtDesde As ERP.ocxTXTNumeric
    Friend WithEvents lblDesdeHasta As System.Windows.Forms.Label
    Friend WithEvents btnDetalle As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents OcxCHK1 As ERP.ocxCHK
    Friend WithEvents OcxConfiguracionCobranzaCredito1 As ERP.ocxConfiguracionCobranzaCredito
End Class

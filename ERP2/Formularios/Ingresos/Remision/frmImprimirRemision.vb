﻿Imports ERP.Reporte

Public Class frmImprimirRemisionPorDeposito

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim CReporte As New CReporteRemision

    'PROPIEDADES
    Public Property dt As DataTable
    Private dtDetalleValue As DataTable
    Public Property dtDetalle() As DataTable
        Get
            Return dtDetalleValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleValue = value
        End Set
    End Property

    'VARIABLES
    Dim vControles() As Control
    Dim vdtCamion As DataTable
    Dim vdtDistribuidor As DataTable
    Dim vdtChofer As DataTable

    Sub Inicializar()

        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Controles
        txtCliente.Conectar()
        txtCliente.frm = Me

        'Otros

        'Propiedades

        'Funciones
        CargarInformacion()

        'Clases

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'CONFIGURACIONES
        txtFechaInicio.SetValue(VGFechaHoraSistema)
        If IsNumeric(vgConfiguraciones("RemisionCantidadLineas")) = False Then
            vgConfiguraciones("RemisionCantidadLineas") = 20
        End If

        txtCantidadMaximaProductos.SetValue(vgConfiguraciones("RemisionCantidadLineas").ToString)
        txtDireccionPartida.SetValue(vgConfiguraciones("RemisionDireccionSalida").ToString)

        'Foco
        If txtCliente.Seleccionado = False Then
            Me.ActiveControl = txtCliente
            txtCliente.Focus()
        Else
            Me.ActiveControl = txtDomicilioCliente
            txtDomicilioCliente.txt.Focus()
        End If
        

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'Cabecera
        CSistema.CargaControl(vControles, cbxSucursalOperacion)
        CSistema.CargaControl(vControles, cbxDepositoOperacion)

        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, txtDomicilioCliente)
        CSistema.CargaControl(vControles, txtMotivoTranslado)
        CSistema.CargaControl(vControles, txtComprobanteVentaTipo)
        CSistema.CargaControl(vControles, txtComprobanteVenta)
        CSistema.CargaControl(vControles, txtTimbradoVenta)
        CSistema.CargaControl(vControles, txtFechaExpedicionVenta)
        CSistema.CargaControl(vControles, txtFechaInicio)
        CSistema.CargaControl(vControles, txtFechaFin)

        CSistema.CargaControl(vControles, txtDireccionPartida)
        CSistema.CargaControl(vControles, txtCiudadPartida)
        CSistema.CargaControl(vControles, txtDepartamentoPartida)
        CSistema.CargaControl(vControles, txtDireccionLlegada)
        CSistema.CargaControl(vControles, txtCiudadLlegada)
        CSistema.CargaControl(vControles, txtDepartamentoLlegada)

        CSistema.CargaControl(vControles, txtKilometrajeEstimativo)
        CSistema.CargaControl(vControles, cbxTransporte)
        CSistema.CargaControl(vControles, cbxVehiculo)
        CSistema.CargaControl(vControles, cbxChofer)

        dtDetalle = CData.GetStructure("VDetalleRemision", "Select Top(0) * From VDetalleRemision")

        vdtChofer = CData.GetTable("VChofer")
        vdtDistribuidor = CData.GetTable("VDistribuidor")
        vdtCamion = CData.GetTable("VCamion")

        'Fecha de Venta
        txtFechaInicio.SetValue(Now)

        'Cliente
        txtCliente.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CLIENTE", ""), CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "RAZON SOCIAL", ""), CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "RUC", ""))

        txtDomicilioCliente.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DOMICILIO CLIENTE", ""))

        'Motivo
        txtMotivoTranslado.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MOTIVO", ""))

        'Comprobante Venta Tipo
        txtComprobanteVentaTipo.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "COMPROBANTE VENTA TIPO", ""))

        'Direccion de Llegada
        txtDireccionPartida.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DIRECCION DE PARTIDA", ""))
        txtCiudadPartida.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DIRECCION DE PARTIDA CIUDAD", ""))
        txtDepartamentoPartida.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DIRECCION DE PARTIDA DEPARTAMENTO", ""))

        'Direccion de Llegada
        txtDireccionLlegada.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DIRECCION DE LLEGADA", ""))
        txtCiudadLlegada.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DIRECCION DE LLEGADA CIUDAD", ""))
        txtDepartamentoLlegada.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DIRECCION DE LLEGADA DEPARTAMENTO", ""))

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)
        CSistema.ControlBotonesRegistro(Operacion, New Button, New Button, btnCancelar, New Button, btnImprimir, New Button, New Button, vControles)
    End Sub

    Sub GuardarInformacion()

        'Cliente
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CLIENTE", txtCliente.Registro("Referencia").ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "RAZON SOCIAL", txtCliente.Registro("RazonSocial").ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "RUC", txtCliente.Registro("RUC").ToString)

        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DOMICILIO CLIENTE", txtDomicilioCliente.GetValue)

        'Motivo
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MOTIVO", txtMotivoTranslado.GetValue)

        'Comprobante Venta Tipo
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "COMPROBANTE VENTA TIPO", txtComprobanteVentaTipo.GetValue)

        'Direccion de Partida
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DIRECCION DE PARTIDA", txtDireccionPartida.GetValue)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DIRECCION DE PARTIDA CIUDAD", txtCiudadPartida.GetValue)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DIRECCION DE PARTIDA DEPARTAMENTO", txtDepartamentoPartida.GetValue)

        'Direccion de Llegada
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DIRECCION DE LLEGADA", txtDireccionLlegada.GetValue)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DIRECCION DE LLEGADA CIUDAD", txtCiudadLlegada.GetValue)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DIRECCION DE LLEGADA DEPARTAMENTO", txtDepartamentoLlegada.GetValue)

    End Sub

    Sub Cancelar()

        Me.Enabled = True

        btnImprimir.Enabled = False
        btnCancelar.Enabled = False
        btnPreparar.Enabled = True

        txtCliente.txtReferencia.Focus()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Seleccion de Cliente
        If txtCliente.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Function
        End If

        If txtCliente.Registro Is Nothing Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Function
        End If

        If IsNumeric(txtCliente.Registro("ID").ToString) = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Function
        End If

        'Seleccion de Sucursal
        If IsNumeric(cbxSucursalOperacion.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Function
        End If

        'Seleccion de Deposito
        If IsNumeric(cbxDepositoOperacion.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Function
        End If

        ValidarDocumento = True

    End Function

    Sub ObtenerDeposito()

        If cbxSucursalOperacion.cbx.SelectedValue Is Nothing Then
            Exit Sub
        End If

        Dim dttemp As DataTable = CData.GetTable("VDeposito", "IDSucursal=" & cbxSucursalOperacion.cbx.SelectedValue).Copy

        CSistema.SqlToComboBox(cbxDepositoOperacion.cbx, dttemp)

        Dim DepositoRow As DataRow = CData.GetRow("IDSucursal=" & cbxSucursalOperacion.cbx.SelectedValue, "VDeposito")

        cbxDepositoOperacion.cbx.Text = DepositoRow("Deposito").ToString
        cbxDepositoOperacion.cbx.Text = vgDeposito


    End Sub

    Sub ObtenerInformacionClientes()

        'Dim oRow As DataRow = txtCliente.Registro
        'Dim oRowSucursal As DataRow = txtCliente.Sucursal

        ''Datos Comunes
        'If txtCliente.SucursalSeleccionada = False Then
        '    txtDomicilioCliente.txt.Text = oRow("Direccion").ToString
        'Else
        '    txtDomicilioCliente.txt.Text = oRowSucursal("Direccion").ToString
        'End If

        'txtDomicilioCliente.txt.Focus()

    End Sub

    Sub Imprimir()

        Me.Enabled = False

        For vIDTransaccion As Integer = 0 To dt.Rows.Count - 1
            Dim vdt As DataTable = CData.FiltrarDataTable(dt, "IDTransaccion=" & vIDTransaccion)
            Dim vdtDetalle As DataTable = CData.FiltrarDataTable(dtDetalle, "IDTransaccion=" & vIDTransaccion)

            If CReporte.Imprimir(vdt, vdtDetalle) = False Then
                Cancelar()
                Exit Sub
            End If
        Next

        Cancelar()

    End Sub

    Sub SeleccionarTransporte()

        If IsNumeric(cbxTransporte.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        cbxVehiculo.cbx.Text = ""
        txtPatenteVehiculo.txt.Text = ""

        cbxChofer.cbx.Text = ""
        txtRUCChofer.txt.Text = ""
        txtDomicilioChofer.txt.Text = ""

        For Each oRow As DataRow In CData.GetTable("VTransporte").Select(" ID = " & cbxTransporte.cbx.SelectedValue)
            cbxVehiculo.cbx.Text = oRow("Camion").ToString
            txtPatenteVehiculo.txt.Text = oRow("Patente").ToString

            cbxChofer.cbx.Text = oRow("Chofer").ToString
            txtRUCChofer.txt.Text = oRow("ChoferNroDocumento").ToString
            txtDomicilioChofer.txt.Text = oRow("ChoferDireccion").ToString

        Next

    End Sub

    Sub Preparar()

        dtDetalle = CData.GetStructure("VDetalleRemision", "Select Top(0) * From VDetalleRemision")

        dt = CData.GetStructure("VRemision", "Select * From VRemision")
        Dim vdtDetalleDeposito As DataTable = CSistema.ExecuteToDataTable("Select * From VExistenciaDeposito Where Existencia>0 And IDSucursal=" & cbxSucursalOperacion.GetValue & " And IDDeposito=" & cbxDepositoOperacion.GetValue & " Order By Referencia")
        Dim IDTransaccion As Integer = 0
        Dim Cantidad As Integer = 0
        Dim indice As Integer = 1

        For Each oRow As DataRow In vdtDetalleDeposito.Rows

            'Detalle
            Dim NewRow As DataRow = dtDetalle.NewRow
            NewRow("IDTransaccion") = IDTransaccion
            NewRow("Referencia") = oRow("Referencia")
            NewRow("IDProducto") = oRow("IDProducto")
            NewRow("Producto") = oRow("Producto")
            NewRow("Cantidad") = oRow("Existencia")
            NewRow("Descripcion") = oRow("Producto")

            dtDetalle.Rows.Add(NewRow)

            indice = indice + 1

            If indice > txtCantidadMaximaProductos.ObtenerValor Then
                indice = 1
                IDTransaccion = IDTransaccion + 1
            End If

        Next

        Cantidad = IDTransaccion

        For i As Integer = 0 To Cantidad

            IDTransaccion = i
            Dim NewRowCabecera As DataRow = dt.NewRow
            NewRowCabecera("IDTransaccion") = IDTransaccion
            NewRowCabecera("Comprobante") = ""
            NewRowCabecera("Cliente") = txtCliente.Registro("RazonSocial").ToString
            NewRowCabecera("RUC") = txtCliente.Registro("RUC").ToString
            NewRowCabecera("DomicilioCliente") = txtDomicilioCliente.GetValue

            NewRowCabecera("MotivoObservacion") = txtMotivoTranslado.GetValue
            NewRowCabecera("ComprobanteVentaTipo") = txtComprobanteVentaTipo.GetValue
            NewRowCabecera("ComprobanteVenta") = txtComprobanteVenta.GetValue
            NewRowCabecera("TimbradoVenta") = txtTimbradoVenta.GetValue
            NewRowCabecera("FechaExpedicionVenta") = txtFechaExpedicionVenta.GetValue
            NewRowCabecera("Fec Ini") = txtFechaInicio.GetValueString
            NewRowCabecera("fec Fin") = txtFechaFin.GetValueString

            NewRowCabecera("DireccionPartida") = txtDireccionPartida.GetValue
            NewRowCabecera("CiudadPartida") = txtCiudadPartida.GetValue
            NewRowCabecera("DepartamentoPartida") = txtDepartamentoPartida.GetValue
            NewRowCabecera("DireccionLlegada") = txtDireccionLlegada.GetValue
            NewRowCabecera("CiudadLlegada") = txtCiudadLlegada.GetValue
            NewRowCabecera("DepartamentoLlegada") = txtDepartamentoLlegada.GetValue
            NewRowCabecera("KilometrajeEstimado") = txtKilometrajeEstimativo.GetValue

            NewRowCabecera("Camion") = cbxVehiculo.Texto
            NewRowCabecera("CamionPatente") = txtPatenteVehiculo.GetValue

            NewRowCabecera("Chofer") = cbxChofer.Texto
            NewRowCabecera("ChoferDocumento") = txtRUCChofer.GetValue
            NewRowCabecera("ChoferDireccion") = txtDomicilioChofer.GetValue

            dt.Rows.Add(NewRowCabecera)

        Next

        btnImprimir.Enabled = True
        btnCancelar.Enabled = True
        btnPreparar.Enabled = False

    End Sub

    Private Sub frmImprimirRemisionPorDeposito_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmImprimirRemisionPorDeposito_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmImprimirRemisionPorDeposito_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnImprimir_Click(sender As System.Object, e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub cbxSucursalOperacion_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxSucursalOperacion.PropertyChanged

        cbxDepositoOperacion.DataSource = Nothing
        cbxDepositoOperacion.cbx.Text = ""

        Try
            CSistema.SqlToComboBox(cbxDepositoOperacion.cbx, CData.GetTable("VDeposito", "IDSucursal=" & cbxSucursalOperacion.GetValue))

        Catch ex As Exception

        End Try

    End Sub

    Private Sub cbxTransporte_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTransporte.PropertyChanged
        SeleccionarTransporte()
    End Sub

    Private Sub btnPreparar_Click(sender As System.Object, e As System.EventArgs) Handles btnPreparar.Click
        Preparar()
    End Sub

    Private Sub txtCliente_ItemSeleccionado(sender As Object, e As System.EventArgs) Handles txtCliente.ItemSeleccionado
        ObtenerInformacionClientes()
    End Sub

    Private Sub cbxVehiculo_Leave(sender As System.Object, e As System.EventArgs) Handles cbxVehiculo.Leave

        txtPatenteVehiculo.SetValue("")

        If cbxVehiculo.GetValue = 0 Then
            Exit Sub
        End If

        Try
            txtPatenteVehiculo.SetValue(CData.GetTable("VCamion", " ID=" & cbxVehiculo.GetValue)(0)("Patente").ToString)

        Catch ex As Exception

        End Try
    End Sub

    Private Sub cbxChofer_Leave(sender As System.Object, e As System.EventArgs) Handles cbxChofer.Leave

        txtDomicilioChofer.SetValue("")
        txtRUCChofer.SetValue("")

        If cbxChofer.GetValue = 0 Then
            Exit Sub
        End If

        Try
            txtDomicilioChofer.SetValue(CData.GetTable("VChofer", " ID=" & cbxChofer.GetValue)(0)("Direccion").ToString)
            txtRUCChofer.SetValue(CData.GetTable("VChofer", " ID=" & cbxChofer.GetValue)(0)("NroDocumento").ToString)

        Catch ex As Exception

        End Try
    End Sub
End Class
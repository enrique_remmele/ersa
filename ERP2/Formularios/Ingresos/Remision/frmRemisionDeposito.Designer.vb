﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRemisionCargarDeposito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtDeposito = New ERP.ocxTXTString()
        Me.lblDeposito = New System.Windows.Forms.Label()
        Me.gbx = New System.Windows.Forms.GroupBox()
        Me.lklEmliminar = New System.Windows.Forms.LinkLabel()
        Me.lklHoy = New System.Windows.Forms.LinkLabel()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.txtBuscar = New ERP.ocxTXTString()
        Me.btnGenerar = New System.Windows.Forms.Button()
        Me.lklPendientes = New System.Windows.Forms.LinkLabel()
        Me.txtCantidadMAxima = New ERP.ocxTXTString()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dgvPlanilla = New System.Windows.Forms.DataGridView()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.dgvPartes = New System.Windows.Forms.DataGridView()
        Me.gbx.SuspendLayout()
        CType(Me.dgvPlanilla, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPartes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtDeposito
        '
        Me.txtDeposito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDeposito.Color = System.Drawing.Color.Empty
        Me.txtDeposito.Indicaciones = Nothing
        Me.txtDeposito.Location = New System.Drawing.Point(9, 38)
        Me.txtDeposito.Multilinea = False
        Me.txtDeposito.Name = "txtDeposito"
        Me.txtDeposito.Size = New System.Drawing.Size(294, 22)
        Me.txtDeposito.SoloLectura = True
        Me.txtDeposito.TabIndex = 1
        Me.txtDeposito.TabStop = False
        Me.txtDeposito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDeposito.Texto = ""
        '
        'lblDeposito
        '
        Me.lblDeposito.AutoSize = True
        Me.lblDeposito.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeposito.Location = New System.Drawing.Point(6, 22)
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(52, 13)
        Me.lblDeposito.TabIndex = 0
        Me.lblDeposito.Text = "Deposito:"
        '
        'gbx
        '
        Me.gbx.Controls.Add(Me.dgvPartes)
        Me.gbx.Controls.Add(Me.btnGenerar)
        Me.gbx.Controls.Add(Me.lklEmliminar)
        Me.gbx.Controls.Add(Me.lklHoy)
        Me.gbx.Controls.Add(Me.btnBuscar)
        Me.gbx.Controls.Add(Me.txtBuscar)
        Me.gbx.Controls.Add(Me.lklPendientes)
        Me.gbx.Controls.Add(Me.txtCantidadMAxima)
        Me.gbx.Controls.Add(Me.Label2)
        Me.gbx.Controls.Add(Me.txtDeposito)
        Me.gbx.Controls.Add(Me.lblDeposito)
        Me.gbx.Controls.Add(Me.dgvPlanilla)
        Me.gbx.Location = New System.Drawing.Point(12, 12)
        Me.gbx.Name = "gbx"
        Me.gbx.Size = New System.Drawing.Size(429, 383)
        Me.gbx.TabIndex = 0
        Me.gbx.TabStop = False
        Me.gbx.Text = "Cree o seleccione una planilla de carga"
        '
        'lklEmliminar
        '
        Me.lklEmliminar.AutoSize = True
        Me.lklEmliminar.Location = New System.Drawing.Point(176, 213)
        Me.lklEmliminar.Name = "lklEmliminar"
        Me.lklEmliminar.Size = New System.Drawing.Size(43, 13)
        Me.lklEmliminar.TabIndex = 10
        Me.lklEmliminar.TabStop = True
        Me.lklEmliminar.Text = "Eliminar"
        '
        'lklHoy
        '
        Me.lklHoy.AutoSize = True
        Me.lklHoy.Location = New System.Drawing.Point(84, 213)
        Me.lklHoy.Name = "lklHoy"
        Me.lklHoy.Size = New System.Drawing.Size(86, 13)
        Me.lklHoy.TabIndex = 9
        Me.lklHoy.TabStop = True
        Me.lklHoy.Text = "Registros de hoy"
        '
        'btnBuscar
        '
        Me.btnBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnBuscar.Location = New System.Drawing.Point(362, 65)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(53, 22)
        Me.btnBuscar.TabIndex = 6
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'txtBuscar
        '
        Me.txtBuscar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBuscar.Color = System.Drawing.Color.Empty
        Me.txtBuscar.Indicaciones = Nothing
        Me.txtBuscar.Location = New System.Drawing.Point(309, 66)
        Me.txtBuscar.Multilinea = False
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(53, 20)
        Me.txtBuscar.SoloLectura = False
        Me.txtBuscar.TabIndex = 5
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBuscar.Texto = ""
        '
        'btnGenerar
        '
        Me.btnGenerar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnGenerar.Location = New System.Drawing.Point(9, 65)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(167, 22)
        Me.btnGenerar.TabIndex = 4
        Me.btnGenerar.Text = "Generar una nueva planilla (+)"
        Me.btnGenerar.UseVisualStyleBackColor = True
        '
        'lklPendientes
        '
        Me.lklPendientes.AutoSize = True
        Me.lklPendientes.Location = New System.Drawing.Point(18, 213)
        Me.lklPendientes.Name = "lklPendientes"
        Me.lklPendientes.Size = New System.Drawing.Size(60, 13)
        Me.lklPendientes.TabIndex = 8
        Me.lklPendientes.TabStop = True
        Me.lklPendientes.Text = "Pendientes"
        '
        'txtCantidadMAxima
        '
        Me.txtCantidadMAxima.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCantidadMAxima.Color = System.Drawing.Color.Empty
        Me.txtCantidadMAxima.Indicaciones = Nothing
        Me.txtCantidadMAxima.Location = New System.Drawing.Point(309, 38)
        Me.txtCantidadMAxima.Multilinea = False
        Me.txtCantidadMAxima.Name = "txtCantidadMAxima"
        Me.txtCantidadMAxima.Size = New System.Drawing.Size(106, 22)
        Me.txtCantidadMAxima.SoloLectura = True
        Me.txtCantidadMAxima.TabIndex = 3
        Me.txtCantidadMAxima.TabStop = False
        Me.txtCantidadMAxima.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCantidadMAxima.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(329, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Cantidad x Doc.:"
        '
        'dgvPlanilla
        '
        Me.dgvPlanilla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPlanilla.Location = New System.Drawing.Point(9, 94)
        Me.dgvPlanilla.Name = "dgvPlanilla"
        Me.dgvPlanilla.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPlanilla.Size = New System.Drawing.Size(406, 116)
        Me.dgvPlanilla.StandardTab = True
        Me.dgvPlanilla.TabIndex = 7
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnCancelar.Location = New System.Drawing.Point(352, 401)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnAceptar.Location = New System.Drawing.Point(271, 401)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 1
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'dgvPartes
        '
        Me.dgvPartes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPartes.Location = New System.Drawing.Point(9, 231)
        Me.dgvPartes.Name = "dgvPartes"
        Me.dgvPartes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPartes.Size = New System.Drawing.Size(406, 136)
        Me.dgvPartes.StandardTab = True
        Me.dgvPartes.TabIndex = 11
        '
        'frmRemisionCargarDeposito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(450, 436)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.gbx)
        Me.Name = "frmRemisionCargarDeposito"
        Me.Text = "frmRemisionDeposito"
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        CType(Me.dgvPlanilla, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPartes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtDeposito As ERP.ocxTXTString
    Friend WithEvents lblDeposito As System.Windows.Forms.Label
    Friend WithEvents gbx As System.Windows.Forms.GroupBox
    Friend WithEvents dgvPlanilla As System.Windows.Forms.DataGridView
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents txtBuscar As ERP.ocxTXTString
    Friend WithEvents btnGenerar As System.Windows.Forms.Button
    Friend WithEvents lklPendientes As System.Windows.Forms.LinkLabel
    Friend WithEvents txtCantidadMAxima As ERP.ocxTXTString
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lklEmliminar As System.Windows.Forms.LinkLabel
    Friend WithEvents lklHoy As System.Windows.Forms.LinkLabel
    Friend WithEvents dgvPartes As System.Windows.Forms.DataGridView
End Class

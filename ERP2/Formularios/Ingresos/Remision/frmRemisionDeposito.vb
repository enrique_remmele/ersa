﻿Public Class frmRemisionCargarDeposito

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDDepositoValue As Integer
    Public Property IDDeposito() As Integer
        Get
            Return IDDepositoValue
        End Get
        Set(ByVal value As Integer)
            IDDepositoValue = value
        End Set
    End Property

    Private DepositoValue As String
    Public Property Deposito() As String
        Get
            Return DepositoValue
        End Get
        Set(ByVal value As String)
            DepositoValue = value
        End Set
    End Property

    Private CantidadMaximaValue As String
    Public Property CantidadMaxima() As String
        Get
            Return CantidadMaximaValue
        End Get
        Set(ByVal value As String)
            CantidadMaximaValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        txtDeposito.SetValue(Deposito)
        txtCantidadMAxima.SetValue(CantidadMaxima)

        'Funciones
        Listar(" Where Pendiente > 0" & " And IDDeposito=" & IDDeposito)

        'Foco
        Me.ActiveControl = dgvPlanilla

    End Sub

    Sub NuevaPlanilla()

        Dim Numero As Integer = CSistema.ExecuteScalar("Select IsNull(Max(Numero) + 1, 1) From VRemisionPlanillaDeposito Where IDDeposito = " & IDDeposito)
        Dim dtProductos As DataTable = CSistema.ExecuteToDataTable("Select IDProducto, Existencia From VExistenciaDeposito Where Existencia > 0 And IDDeposito=" & IDDeposito & " ")
        Dim SQL As String = ""
        Dim indice As Integer = 0
        Dim Medidor As Integer = 1
        Dim Parte As Integer = 1

        For Each oRow As DataRow In dtProductos.Rows
            SQL = SQL & "INSERT INTO RemisionPlanillaDeposito(IDDeposito, Numero, ID, Creado, IDProducto, Cantidad, Cargado, Parte) Values(" & IDDeposito & ", " & Numero & ", " & indice & ", GetDate(), " & oRow("IDProducto") & ", " & CSistema.FormatoNumeroBaseDatos(oRow("Existencia"), True) & ", 'False', " & Parte & ") " & vbCrLf
            indice = indice + 1

            If Medidor = CantidadMaxima Then
                Medidor = 0
                Parte = Parte + 1
            End If

            Medidor = Medidor + 1

        Next

        Dim Ejecutados As Integer = CSistema.ExecuteNonQuery(SQL)

        If Ejecutados = 0 Then
            Exit Sub
        End If

        Listar(" Where Pendiente > 0" & " And IDDeposito=" & IDDeposito)

    End Sub

    Sub Listar(Optional where As String = "")

        Dim SQL As String = "Select Numero, Creado, Total, Cargados, Pendiente From VRemisionPlanillaDeposito "
        Dim OrderBy As String = " Order By Numero Desc"

        CSistema.SqlToDataGrid(dgvPlanilla, SQL & where & OrderBy)

        'Formato
        dgvPlanilla.Columns("Numero").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvPlanilla.Columns("Total").DefaultCellStyle.Format = "N0"
        dgvPlanilla.Columns("Cargados").DefaultCellStyle.Format = "N0"
        dgvPlanilla.Columns("Pendiente").DefaultCellStyle.Format = "N0"
        dgvPlanilla.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvPlanilla.Columns("Cargados").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvPlanilla.Columns("Pendiente").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

    End Sub

    Sub ListarPartes()

        dgvPartes.DataSource = Nothing

        'Validar
        If dgvPlanilla.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim Numero As Integer = dgvPlanilla.SelectedRows(0).Cells("Numero").Value

        Dim SQL As String = "Select Parte, 'Desde'=MIN(ID), 'Hasta'=MAX(ID), 'Procesado'=(Case When Sum(Convert(int, IsNull(Cargado, 'False'))) = 0 Then '-' Else 'OK' End) From RemisionPlanillaDeposito Where Numero = " & Numero & " And IDDeposito=" & IDDeposito & " Group By Parte "
        CSistema.SqlToDataGrid(dgvPartes, SQL)

        'Formato
        dgvPartes.Columns("Parte").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub Eliminar()

    End Sub

    Sub Aceptar()

        'Validar
        If dgvPlanilla.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        If dgvPlanilla.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim Numero As Integer = dgvPlanilla.SelectedRows(0).Cells("Numero").Value
        Dim Parte As Integer = dgvPartes.SelectedRows(0).Cells("Parte").Value

        Dim SQL As String = "Select R.IDProducto, P.Referencia, 'Producto'=P.Descripcion, P.CodigoBarra, R.Cantidad From RemisionPlanillaDeposito R JOin Producto P On R.IDProducto=P.ID Where Numero = " & Numero & " And R.IDDeposito=" & IDDeposito & "  And R.Parte = " & Parte & " "

        dt = CSistema.ExecuteToDataTable(SQL)

        If dt Is Nothing Then
            Exit Sub
        End If

        Procesado = True

        'Actualizar
        SQL = "Update RemisionPlanillaDeposito Set Cargado='True' Where Numero=" & Numero & " And IDDeposito=" & IDDeposito & " And Parte=" & Parte
        CSistema.ExecuteNonQuery(SQL)

        Me.Close()

    End Sub

    Sub Cancelar()

        Procesado = False
        Me.Close()

    End Sub

    Private Sub frmRemisionCargarDeposito_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub txtNuevo_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            NuevaPlanilla()
        End If
    End Sub

    Private Sub btnGenerar_Click(sender As System.Object, e As System.EventArgs) Handles btnGenerar.Click
        NuevaPlanilla()
    End Sub

    Private Sub txtBuscar_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtBuscar.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            Listar(" Where Numero=" & txtBuscar.GetValue & " And IDDeposito=" & IDDeposito)
        End If
    End Sub

    Private Sub btnBuscar_Click(sender As System.Object, e As System.EventArgs) Handles btnBuscar.Click
        Listar(" Where Numero=" & txtBuscar.GetValue & " And IDDeposito=" & IDDeposito)
    End Sub

    Private Sub dgvPlanilla_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvPlanilla.KeyDown

        If e.KeyCode = Keys.Enter Then

            e.SuppressKeyPress = True

        End If

    End Sub

    Private Sub lklPendientes_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklPendientes.LinkClicked
        Listar(" Where Pendiente > 0" & " And IDDeposito=" & IDDeposito)
    End Sub

    Private Sub lklHoy_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklHoy.LinkClicked
        Listar(" Where Year(Creado) = " & Now.Year & " And Month(Creado) = " & Now.Month & " And Day(Creado) = " & Now.Day & " " & " And IDDeposito=" & IDDeposito)
    End Sub

    Private Sub lklEmliminar_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEmliminar.LinkClicked
        Eliminar()
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Aceptar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub dgvPlanilla_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles dgvPlanilla.SelectionChanged
        ListarPartes()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmRemisionDeposito_Activate()
        Me.Refresh()
    End Sub
End Class
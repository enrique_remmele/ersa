﻿Public Class frmActividad

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'EVENTOS

    'PROPIEDADES
    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private NuevoValue As Boolean
    Public Property Nuevo() As Boolean
        Get
            Return NuevoValue
        End Get
        Set(ByVal value As Boolean)
            NuevoValue = value
        End Set
    End Property

    Private NuevaPlanillaValue As Boolean
    Public Property NuevoNuevaPlanilla() As Boolean
        Get
            Return NuevaPlanillaValue
        End Get
        Set(ByVal value As Boolean)
            NuevaPlanillaValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtProductos As DataTable
    Dim i As Integer = 0

    'FUNCIONES
    Sub Inicializar()

        ocxClasificacion.Nivel = 5
        ocxClasificacion.CheckBox2 = True
        ocxClasificacion.Seleccion = True
        ocxClasificacion.Inicialiar()

        dtProductos = CData.GetStructure("VProducto", "")

        'ID
        If Nuevo = True Then
            txtID.SetValue(CSistema.ExecuteScalar("Select ISNULL((Select MAX(ID)+1 From Actividad), 1)"))
        Else
            txtID.SetValue(ID)
            CargarOperacion()
        End If

        'Producto
        txtProducto.Conectar()

        cbxSucursal.SelectedValue(IDSucursal)



    End Sub

    'Listar Productos
    Sub ListarProductos()

        'lbxProductos.DataSource = Nothing

        If dtProductos IsNot Nothing Then
            dtProductos.Rows.Clear()
        End If

        For Each nodo As TreeNode In ocxClasificacion.tv.Nodes

            If nodo.Nodes.Count > 0 Then
                Listar(nodo.Nodes)
            Else
                AgregarProducto(nodo)
            End If
        Next

        listarProducto2(dtProductos)

        'lbxProductos.DataSource = dtProductos
        ' lbxProductos.DisplayMember = "Descripcion"

    End Sub

    Sub listarProducto2(ByVal dt As DataTable)

        lvLista.Items.Clear()

        For Each oRow As DataRow In dt.Rows
            Dim item As ListViewItem = lvLista.Items.Add(oRow("Ref").ToString)
            item.SubItems.Add(oRow("Descripcion").ToString)
        Next

    End Sub

    Sub Listar(ByVal nodos As TreeNodeCollection)

        For Each nodo As TreeNode In nodos
            If nodo.Nodes.Count > 0 Then
                Listar(nodo.Nodes)
            Else
                AgregarProducto(nodo)
            End If
        Next
    End Sub

    Sub AgregarProducto(ByVal nodo As TreeNode)

        ' If Nuevo = True Then
        If nodo.Checked = False Then
            Exit Sub
        End If
        'End If

        Dim rows As DataRowCollection = Nothing

        Select Case nodo.Level
            Case 0
                rows = CData.GetTable("VProducto", " Estado = 'True' And IDTipoProducto=" & nodo.Name).Copy.Rows
            Case 1
                rows = CData.GetTable("VProducto", " Estado = 'True' And IDLinea=" & nodo.Name & " And IDTipoProducto=" & nodo.Parent.Name).Copy.Rows
            Case 2
                rows = CData.GetTable("VProducto", " Estado = 'True' And IDSubLinea=" & nodo.Name & " And IDLinea=" & nodo.Parent.Name & " And IDTipoProducto=" & nodo.Parent.Parent.Name).Copy.Rows
            Case 3
                rows = CData.GetTable("VProducto", " Estado = 'True' And IDSubLinea2=" & nodo.Name & " And IDSubLinea=" & nodo.Parent.Name & " And IDLinea=" & nodo.Parent.Parent.Name & " And IDTipoProducto=" & nodo.Parent.Parent.Parent.Name).Copy.Rows
            Case 4
                rows = CData.GetTable("VProducto", " Estado = 'True' And IDMarca=" & nodo.Name & " And IDSubLinea2=" & nodo.Parent.Name & " And IDSubLinea=" & nodo.Parent.Parent.Name & " And IDLinea=" & nodo.Parent.Parent.Parent.Name & " And IDTipoProducto=" & nodo.Parent.Parent.Parent.Parent.Name).Copy.Rows
            Case 5
                'If Nuevo = True Then
                rows = CData.GetTable("VProducto", " Estado = 'True' And IDPresentacion=" & nodo.Name & " And IDMarca=" & nodo.Parent.Name & " And IDSubLinea2=" & nodo.Parent.Parent.Name & " And IDSubLinea=" & nodo.Parent.Parent.Parent.Name & " And IDLinea=" & nodo.Parent.Parent.Parent.Parent.Name & " And IDTipoProducto=" & nodo.Parent.Parent.Parent.Parent.Parent.Name).Copy.Rows
                'Else
                'rows = CData.GetTable("VDetalleActividad", " Estado = 'True' And IDPresentacion=" & nodo.Name & " And IDMarca=" & nodo.Parent.Name & " And IDSubLinea2=" & nodo.Parent.Parent.Name & " And IDSubLinea=" & nodo.Parent.Parent.Parent.Name & " And IDLinea=" & nodo.Parent.Parent.Parent.Parent.Name & " And IDTipoProducto=" & nodo.Parent.Parent.Parent.Parent.Parent.Name).Copy.Rows
                'End If
            Case Else
                rows = Nothing
        End Select

        If rows Is Nothing Then
            Exit Sub
        End If

        'Recorremos la lista
        For Each oRow As DataRow In rows

            'Verificamos que no exista en el datatable
            If dtProductos.Select(" ID = " & oRow("ID")).Count = 0 Then

                'Agregamos
                Dim NewRow As DataRow = dtProductos.NewRow
                NewRow("ID") = oRow("ID")
                NewRow("Ref") = oRow("Ref")
                NewRow("Descripcion") = oRow("Descripcion")

                dtProductos.Rows.Add(NewRow)

            End If
        Next

    End Sub

    Sub CargarProductos()

        Dim Clasificacion1 As String = ""
        Dim Clasificacion2 As String = ""
        Dim Clasificacion3 As String = ""
        Dim Clasificacion4 As String = ""
        Dim Clasificacion5 As String = ""
        Dim Clasificacion6 As String = ""

        'Clasificaion 1
        For Each nodo1 As TreeNode In ocxClasificacion.tv.Nodes

            If nodo1.Checked = True Then
                If Clasificacion1 = "" Then
                    Clasificacion1 = " (TipoProducto = '" & nodo1.Text & "' "
                Else
                    Clasificacion1 = Clasificacion1 & " Or TipoProducto = '" & nodo1.Text & "' "
                End If
            End If

            If nodo1.Nodes.Count > 0 Then

                'Clasificaion 2
                For Each nodo2 As TreeNode In nodo1.Nodes

                    If nodo2.Checked = True Then
                        If Clasificacion2 = "" Then
                            Clasificacion2 = " (Linea = '" & nodo2.Text & "' "
                        Else
                            Clasificacion2 = Clasificacion2 & " Or Linea = '" & nodo2.Text & "' "
                        End If
                    End If

                    If nodo2.Nodes.Count > 0 Then

                    End If

                    If Clasificacion2.Length > 0 Then
                        Clasificacion2 = Clasificacion2 & ") "
                    End If

                Next
            End If

            If Clasificacion1.Length > 0 Then
                Clasificacion1 = Clasificacion1 & ") "
            End If

        Next

        Debug.Print(Clasificacion1)
        Debug.Print(Clasificacion2)

    End Sub

    Sub CargarProducto()

        If txtProducto.Seleccionado = False Then
            Exit Sub
        End If

        Dim IDProducto As Integer
        Dim Newdtproducto As DataTable

        IDProducto = txtProducto.Registro("ID").ToString

        If dtProductos.Rows.Count = 0 Then
            dtProductos = CData.GetTable("VProducto", " Estado = 'True' And ID=" & IDProducto)
            listarProducto2(dtProductos)
            Exit Sub
        End If

        Newdtproducto = CData.GetTable("VProducto", " Estado = 'True' And ID=" & IDProducto)

        For Each oRow As DataRow In Newdtproducto.Rows
            'Verificamos q no existsta
            If dtProductos.Select("ID=" & IDProducto).Count = 0 Then

                'Agregamos a dtProductos
                Dim NewRow As DataRow = dtProductos.NewRow
                NewRow("ID") = oRow("ID")
                NewRow("Ref") = oRow("Ref")
                NewRow("Descripcion") = oRow("Descripcion")

                dtProductos.Rows.Add(NewRow)

            End If
        Next

        listarProducto2(dtProductos)

    End Sub


    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        If txtActividad.txt.Text.Trim = "" Then
            CSistema.MostrarError("Introduzca una descripcion de la actividad!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'If txtAsignado.ObtenerValor = 0 Then
        '    CSistema.MostrarError("El monto asignado no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Function
        'End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        ID = txtID.ObtenerValor

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", IDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProveedor", IDProveedor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Codigo", txtActividad.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Mecanica", txtMecanica.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ImporteAsignado", txtAsignado.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DescuentoMaximo", CSistema.FormatoNumeroBaseDatos(txtDescuento.ObtenerValor, True), ParameterDirection.Input)
        
        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpActividad", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select ID From Actividad Where ID=" & ID & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpActividad", False, False, MensajeRetorno)
            End If

            Exit Sub

        End If

        'Insertar el Detalle de Productos
        InsertarDetalle()

        'Insertar en DetallePlanillaActividad si operacion = UPD y vNuevo False
        If Operacion.ToString = "INS" And NuevoNuevaPlanilla = False Then

            Dim SQL As String = ""
            SQL = SQL & "Insert Into DetallePlanillaDescuentoTactico(IDTransaccion, IDActividad) Values(" & IDTransaccion & ", " & ID & ")" & vbCrLf
            If CSistema.ExecuteNonQuery(SQL) = 0 Then
                Exit Sub
            End If

            Me.Close()

        End If


    End Sub

    Sub EliminarSelecionado()
        For Each item As ListViewItem In lvLista.CheckedItems

            Dim Referencia As String = item.Text
            For i As Integer = dtProductos.Rows.Count - 1 To 0 Step -1
                Dim oRow As DataRow = dtProductos.Rows(i)
                If oRow("Ref") = Referencia Then
                    dtProductos.Rows.RemoveAt(i)
                End If
            Next
        Next

        listarProducto2(dtProductos)

    End Sub

    Sub InsertarDetalle()

        Dim SQL As String

        'Eliminar todo el detalle
        SQL = "Delete From DetalleActividad Where IDActividad=" & txtID.ObtenerValor & vbCrLf

        'Cargamos los Productos
        For i As Integer = 0 To lvLista.Items.Count - 1
            Dim Producto As String = lvLista.Items(i).SubItems(0).Text
            Dim IDProducto As String = dtProductos.Select(" Ref = '" & Producto & "' ")(0)("ID")

            SQL = SQL & "Insert Into DetalleActividad(IDActividad, ID, IDProducto) Values(" & txtID.ObtenerValor & ", " & i & ", " & IDProducto & ")" & vbCrLf
        Next

        If CSistema.ExecuteNonQuery(SQL) > 0 Then
            Me.Close()
        End If

    End Sub

    Sub CargarOperacion()

        If ID = 0 Then
            Exit Sub
        End If

        Dim dtTemp As DataTable = CSistema.ExecuteToDataTable("Select * From VActividad Where ID=" & ID).Copy
        For Each oRow As DataRow In dtTemp.Rows
            cbxSucursal.SelectedValue(oRow("IDSucursal"))
            txtActividad.txt.Text = oRow("Codigo")
            txtAsignado.SetValue(oRow("ImporteAsignado"))
            txtDescuento.SetValue(oRow("DescuentoMaximo"))
            txtMecanica.txt.Text = oRow("Mecanica")

        Next

        'Cargamos los productos
        dtProductos = CSistema.ExecuteToDataTable("Select * From VDetalleActividad Where IDActividad = " & ID)

        'lbxProductos.DataSource = dttemp2
        'lbxProductos.DisplayMember = "Descripcion"
        'lbxProductos.ValueMember = "ID"
        listarProducto2(dtProductos)
        ListarProductos2(dtProductos)

    End Sub

    Sub ListarProductos2(ByVal dt As DataTable)

        ocxClasificacion.Bloquear = True

        For Each orow As DataRow In dt.Rows

            If IsNumeric(orow("IDTipoProducto")) = True Then
                ocxClasificacion.MarcarNodo(orow("IDTipoProducto"), 0, True)
                ocxClasificacion.MarcarNodo(orow("IDLinea"), 1, True)
                ocxClasificacion.MarcarNodo(orow("IDSubLinea"), 2, True)
                ocxClasificacion.MarcarNodo(orow("IDSubLinea2"), 3, True)
                ocxClasificacion.MarcarNodo(orow("IDMarca"), 4, True)
                ocxClasificacion.MarcarNodo(orow("IDPresentacion").ToString, 5, True)
            End If

        Next

        ocxClasificacion.Bloquear = False

    End Sub

    Private Sub frmActividad_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmActividad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub ocxClasificacion_CheckedItem() Handles ocxClasificacion.CheckedItem
        'ListarProductos()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        i = 0
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If Nuevo = True Then
            Guardar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Guardar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub ocxClasificacion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ocxClasificacion.Click

    End Sub

    Private Sub ocxClasificacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocxClasificacion.Load

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        If Nuevo = True Then
            Me.Close()
        Else
            CargarOperacion()
        End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ListarProductos()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        EliminarSelecionado()
    End Sub

    Private Sub txtProducto_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then
            btnAgregar.Focus()
        End If
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        CargarProducto()
        txtProducto.txt.txt.Clear()
        txtProducto.txt.txt.Focus()

    End Sub

    '10-06-2021 - SC - Actualiza datos
    Sub frmActividad_Activate()
        Me.Refresh()
    End Sub
End Class
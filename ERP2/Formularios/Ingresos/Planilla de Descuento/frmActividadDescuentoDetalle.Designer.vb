﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmActividadDescuentoDetalle
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.dgvLista = New System.Windows.Forms.DataGridView()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.cbxProducto = New ERP.ocxCBX()
        Me.cbxCliente = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.chkProducto = New ERP.ocxCHK()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotal = New ERP.ocxTXTNumeric()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFiltro.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.dgvLista, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxFiltro, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 108.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(893, 502)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLista.Location = New System.Drawing.Point(3, 111)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.Size = New System.Drawing.Size(887, 357)
        Me.dgvLista.TabIndex = 1
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.lblFecha)
        Me.gbxFiltro.Controls.Add(Me.btnListar)
        Me.gbxFiltro.Controls.Add(Me.txtHasta)
        Me.gbxFiltro.Controls.Add(Me.txtDesde)
        Me.gbxFiltro.Controls.Add(Me.cbxVendedor)
        Me.gbxFiltro.Controls.Add(Me.chkVendedor)
        Me.gbxFiltro.Controls.Add(Me.cbxProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxCliente)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(887, 102)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(346, 24)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 6
        Me.lblFecha.Text = "Fecha:"
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(407, 43)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(162, 44)
        Me.btnListar.TabIndex = 9
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(CType(0, Long))
        Me.txtHasta.Location = New System.Drawing.Point(491, 20)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(78, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 8
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(CType(0, Long))
        Me.txtDesde.Location = New System.Drawing.Point(407, 20)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(78, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 7
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = Nothing
        Me.cbxVendedor.DataDisplayMember = Nothing
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = Nothing
        Me.cbxVendedor.DataSource = Nothing
        Me.cbxVendedor.DataValueMember = Nothing
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = Nothing
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(78, 65)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(262, 23)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 5
        Me.cbxVendedor.Texto = ""
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(9, 66)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(86, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 4
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'cbxProducto
        '
        Me.cbxProducto.CampoWhere = Nothing
        Me.cbxProducto.DataDisplayMember = Nothing
        Me.cbxProducto.DataFilter = Nothing
        Me.cbxProducto.DataOrderBy = Nothing
        Me.cbxProducto.DataSource = Nothing
        Me.cbxProducto.DataValueMember = Nothing
        Me.cbxProducto.Enabled = False
        Me.cbxProducto.FormABM = Nothing
        Me.cbxProducto.Indicaciones = Nothing
        Me.cbxProducto.Location = New System.Drawing.Point(78, 19)
        Me.cbxProducto.Name = "cbxProducto"
        Me.cbxProducto.SeleccionObligatoria = False
        Me.cbxProducto.Size = New System.Drawing.Size(262, 23)
        Me.cbxProducto.SoloLectura = False
        Me.cbxProducto.TabIndex = 1
        Me.cbxProducto.Texto = ""
        '
        'cbxCliente
        '
        Me.cbxCliente.CampoWhere = Nothing
        Me.cbxCliente.DataDisplayMember = Nothing
        Me.cbxCliente.DataFilter = Nothing
        Me.cbxCliente.DataOrderBy = Nothing
        Me.cbxCliente.DataSource = Nothing
        Me.cbxCliente.DataValueMember = Nothing
        Me.cbxCliente.Enabled = False
        Me.cbxCliente.FormABM = Nothing
        Me.cbxCliente.Indicaciones = Nothing
        Me.cbxCliente.Location = New System.Drawing.Point(78, 42)
        Me.cbxCliente.Name = "cbxCliente"
        Me.cbxCliente.SeleccionObligatoria = False
        Me.cbxCliente.Size = New System.Drawing.Size(262, 23)
        Me.cbxCliente.SoloLectura = False
        Me.cbxCliente.TabIndex = 3
        Me.cbxCliente.Texto = ""
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(9, 43)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(86, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 2
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(9, 20)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(86, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 0
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotal)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblTotal)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtCantidad)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblCantidad)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 474)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(887, 25)
        Me.FlowLayoutPanel1.TabIndex = 2
        '
        'txtTotal
        '
        Me.txtTotal.Color = System.Drawing.Color.Empty
        Me.txtTotal.Decimales = True
        Me.txtTotal.Indicaciones = Nothing
        Me.txtTotal.Location = New System.Drawing.Point(762, 3)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(122, 20)
        Me.txtTotal.SoloLectura = False
        Me.txtTotal.TabIndex = 3
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Texto = "0"
        '
        'lblTotal
        '
        Me.lblTotal.Location = New System.Drawing.Point(704, 0)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(52, 23)
        Me.lblTotal.TabIndex = 2
        Me.lblTotal.Text = "Total:"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = Nothing
        Me.txtCantidad.Location = New System.Drawing.Point(655, 3)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(43, 20)
        Me.txtCantidad.SoloLectura = False
        Me.txtCantidad.TabIndex = 1
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'lblCantidad
        '
        Me.lblCantidad.Location = New System.Drawing.Point(588, 0)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(61, 23)
        Me.lblCantidad.TabIndex = 0
        Me.lblCantidad.Text = "Cantidad:"
        Me.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmActividadDescuentoDetalle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(893, 502)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmActividadDescuentoDetalle"
        Me.Text = "frmActividadDescuentoDetalle"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents dgvLista As System.Windows.Forms.DataGridView
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents btnListar As System.Windows.Forms.Button
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents cbxProducto As ERP.ocxCBX
    Friend WithEvents cbxCliente As ERP.ocxCBX
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotal As ERP.ocxTXTNumeric
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As ERP.ocxTXTNumeric
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
End Class

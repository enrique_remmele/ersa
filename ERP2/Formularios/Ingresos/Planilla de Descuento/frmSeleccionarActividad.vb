﻿Public Class frmSeleccionarActividad

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private dtActividadesValue As DataTable
    Public Property dtActividades() As DataTable

        Get
            Return dtActividadesValue
        End Get
        Set(ByVal value As DataTable)
            dtActividadesValue = value
        End Set
    End Property

    Private dtProductosValue As DataTable
    Public Property dtProductos() As DataTable
        Get
            Return dtProductosValue
        End Get
        Set(ByVal value As DataTable)
            dtProductosValue = value
        End Set
    End Property

    'EVENTOS

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()


        dtActividades = CSistema.ExecuteToDataTable("Select * From VActividad Where Relacionado='False' ").Copy
        dtProductos = CSistema.ExecuteToDataTable("Select * From VDetalleActividad Where Relacionado='False' ").Copy

        dtActividades.Columns.Add("Seleccionado")

        CargarActividades()

    End Sub

    Sub CargarActividades()

        lvActividades.Items.Clear()

        If dtActividades Is Nothing Then
            Exit Sub
        End If

        For Each oRow As DataRow In dtActividades.Rows
            Dim item As New ListViewItem

            item.Text = oRow("ID").ToString
            item.SubItems.Add(oRow("Codigo").ToString)
            item.SubItems.Add(oRow("Mecanica").ToString)
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("Asignado").ToString))
            item.SubItems.Add(CSistema.FormatoNumero(oRow("DescuentoMaximo").ToString))

            lvActividades.Items.Add(item)

        Next

    End Sub

    Sub CargarProductos()

        lbxProductos.Items.Clear()

        If lvActividades.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim ID As Integer = lvActividades.SelectedItems(0).Text

        For Each oRow As DataRow In dtProductos.Select(" IDActividad = " & ID)
            lbxProductos.Items.Add(oRow("Producto"))
        Next

    End Sub

    Sub Seleccionar()

        For Each item As ListViewItem In lvActividades.CheckedItems

            For Each oRow As DataRow In dtActividades.Select(" ID = " & item.Text)
                oRow("Seleccionado") = True
            Next

        Next

        Me.Close()

    End Sub

    Sub Eliminar()

        'Validar
        If lvActividades.CheckedItems.Count = 0 Then
            CSistema.MostrarError("Seleccione un registro para continuar!", ctrError, btnEliminar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If


        For Each item As ListViewItem In lvActividades.CheckedItems

            Dim ID As Integer
            Dim param(-1) As SqlClient.SqlParameter
            Dim IndiceOperacion As Integer

            ID = item.Text


            'SetSQLParameter, ayuda a generar y configurar los parametros.
            'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
            CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)

            'Operacion
            CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesABM.DEL.ToString, ParameterDirection.Input)
            IndiceOperacion = param.GetLength(0) - 1

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpActividad", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

                'Eliminar el Registro si es que se registro
                If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select ID From Actividad Where ID=" & ID & ") Is Null Then 'False' Else 'True' End)")) = True Then
                    param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                    CSistema.ExecuteStoreProcedure(param, "SpActividad", False, False, MensajeRetorno)
                End If

                Exit Sub

            End If

        Next

        Inicializar()

    End Sub

    Sub Cancelar()

    End Sub

    Private Sub frmSeleccionarActividad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub lvActividades_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvActividades.SelectedIndexChanged
        CargarProductos()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

    '10-06-2021 - SC - Actualiza datos
    Sub frmSeleccionarActividad_Activate()
        Me.Refresh()
    End Sub

End Class
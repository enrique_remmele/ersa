﻿Public Class frmPlanillaDescuentoTactico

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtActividades As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "PLANILLA DE DESCUENTOS TACTICOS", "PLA")
        vNuevo = True

        'Funciones
        CargarInformacion()

        'Clases

        'Controles
        txtProveedor.Conectar()

        'Otros
        vNuevo = False
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

       

        'Botones
        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)

        'Foco
        txtID.txt.Focus()
        txtID.txt.SelectAll()

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'Cabecera
        CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtProveedor)
        CSistema.CargaControl(vControles, txtDesde)
        CSistema.CargaControl(vControles, txtHasta)
        CSistema.CargaControl(vControles, txtPresupuesto)
        CSistema.CargaControl(vControles, txtProveedor)
        CSistema.CargaControl(vControles, txtObservacion)

        'Detalle
        CSistema.CargaControl(vControles, lklEliminar)

        'ESTRUCTURA
        dtActividades = CData.GetStructure("VActividad", "Select * From VActividad")
        dtActividades.Columns.Add("Seleccionado")

        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, CData.CreateTable("VSucursalCiudad", "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2"), "IDCiudad", "CodigoCiudad")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, CData.GetTable("VTipoComprobante", "IDOperacion=" & IDOperacion), "ID", "Codigo")

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudades
        CSistema.SqlToComboBox(cbxCiudad.cbx, CData.GetTable("VSucursal", " ID = " & vgIDSucursal), "IDCiudad", "CodigoCiudad")

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal", " ID = " & vgIDSucursal), "ID", "Descripcion")

          'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

     

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

    End Sub

    Sub CargarActividad()

        'Validar
        'Proveedor
        If txtProveedor.Seleccionado = False Then
            CSistema.MostrarError("Seleccione un proveedor para continuar!", ctrError, btnAgregar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        Dim frm As New frmActividad
        frm.IDProveedor = txtProveedor.Registro("ID").ToString
        frm.IDSucursal = cbxSucursal.GetValue
        frm.Nuevo = True
        frm.NuevoNuevaPlanilla = vNuevo
        frm.IDTransaccion = IDTransaccion
        FGMostrarFormulario(Me, frm, "Actividades", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        Dim dtTemp As DataTable = CSistema.ExecuteToDataTable("Select * From VActividad Where ID=" & frm.ID)

        For Each orow As DataRow In dtTemp.Rows
            Dim NewRow As DataRow = dtActividades.NewRow

            NewRow("ID") = orow("ID").ToString
            NewRow("Codigo") = orow("Codigo").ToString
            NewRow("Mecanica") = orow("Mecanica").ToString
            NewRow("Asignado") = CSistema.FormatoMoneda(orow("Asignado").ToString)
            NewRow("Entrada") = CSistema.FormatoMoneda(orow("Entrada").ToString)
            NewRow("Salida") = CSistema.FormatoMoneda(orow("Salida").ToString)
            NewRow("DescuentoMaximo") = CSistema.FormatoMoneda(orow("DescuentoMaximo").ToString)
            NewRow("Tactico") = CSistema.FormatoMoneda(orow("Tactico").ToString)
            NewRow("Acuerdo") = CSistema.FormatoMoneda(orow("Acuerdo").ToString)
            NewRow("Total") = CSistema.FormatoMoneda(orow("Total").ToString)
            NewRow("PorcentajeUtilizado") = CSistema.FormatoMoneda(orow("PorcentajeUtilizado").ToString)
            NewRow("Saldo") = CSistema.FormatoMoneda(orow("Saldo").ToString)
            NewRow("Excedente") = CSistema.FormatoMoneda(orow("Excedente").ToString)

            dtActividades.Rows.Add(NewRow)
        Next

        ListarActividades()

        If dgw.SelectedRows.Count > 0 Then
            btnCargarUltimoDetalle.Enabled = False
        End If

    End Sub

    Sub EditarActividad()

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim ID As Integer = dgw.SelectedCells(0).Value

        Dim frm As New frmActividad
        frm.ID = ID
        ' frm.IDProveedor = txtProveedor.Registro("ID").ToString
        frm.IDSucursal = cbxSucursal.GetValue
        frm.Nuevo = False
        FGMostrarFormulario(Me, frm, "Modificacion de Actividades", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        Dim dtTemp As DataTable = CSistema.ExecuteToDataTable("Select * From VActividad Where ID=" & ID)

        For Each orow As DataRow In dtActividades.Select(" ID = " & ID)


            orow("ID") = dtTemp.Rows(0)("ID").ToString
            orow("Codigo") = dtTemp.Rows(0)("Codigo").ToString
            orow("Mecanica") = dtTemp.Rows(0)("Mecanica").ToString
            orow("Asignado") = CSistema.FormatoMoneda(dtTemp.Rows(0)("Asignado").ToString)
            orow("Entrada") = CSistema.FormatoMoneda(dtTemp.Rows(0)("Entrada").ToString)
            orow("Salida") = CSistema.FormatoMoneda(dtTemp.Rows(0)("Salida").ToString)
            orow("DescuentoMaximo") = CSistema.FormatoMoneda(orow("DescuentoMaximo").ToString)
            orow("Tactico") = CSistema.FormatoMoneda(dtTemp.Rows(0)("Tactico").ToString)
            orow("Acuerdo") = CSistema.FormatoMoneda(dtTemp.Rows(0)("Acuerdo").ToString)
            orow("Total") = CSistema.FormatoMoneda(dtTemp.Rows(0)("Total").ToString)
            orow("PorcentajeUtilizado") = dtTemp.Rows(0)("PorcentajeUtilizado").ToString
            orow("Saldo") = CSistema.FormatoMoneda(dtTemp.Rows(0)("Saldo").ToString)
            orow("Excedente") = CSistema.FormatoMoneda(dtTemp.Rows(0)("Excedente").ToString)

        Next

        ListarActividades()


    End Sub

    Sub SeleccionarActividad()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Proveedor
        If txtProveedor.Seleccionado = False Then
            CSistema.MostrarError("Seleccione un proveedor para continuar!", ctrError, Button1, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        Dim frm As New frmSeleccionarActividad
        FGMostrarFormulario(Me, frm, "Seleccionar actividades no relacionadas", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        For Each orow As DataRow In frm.dtActividades.Select("Seleccionado='True'")

            'Verificar que no exista
            If dtActividades.Select(" ID = " & orow("ID")).Count = 0 Then
                Dim NewRow As DataRow = dtActividades.NewRow

                NewRow("ID") = orow("ID").ToString
                NewRow("Codigo") = orow("Codigo").ToString
                NewRow("Mecanica") = orow("Mecanica").ToString
                NewRow("Asignado") = CSistema.FormatoMoneda(orow("Asignado").ToString)
                NewRow("Entrada") = CSistema.FormatoMoneda(orow("Entrada").ToString)
                NewRow("Salida") = CSistema.FormatoMoneda(orow("Salida").ToString)
                NewRow("DescuentoMaximo") = CSistema.FormatoMoneda(orow("DescuentoMaximo").ToString)
                NewRow("Tactico") = CSistema.FormatoMoneda(orow("Tactico").ToString)
                NewRow("Acuerdo") = CSistema.FormatoMoneda(orow("Acuerdo").ToString)
                NewRow("Total") = CSistema.FormatoMoneda(orow("Total").ToString)
                NewRow("PorcentajeUtilizado") = CSistema.FormatoMoneda(orow("PorcentajeUtilizado").ToString)
                NewRow("Saldo") = CSistema.FormatoMoneda(orow("Saldo").ToString)
                NewRow("Excedente") = CSistema.FormatoMoneda(orow("Excedente").ToString)

                dtActividades.Rows.Add(NewRow)

            End If

        Next

        ListarActividades()

    End Sub

    Sub Nuevo()

        'Configurar botones
        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)

        'Limpiar detalle
        dtActividades.Rows.Clear()
        ListarActividades()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True
        btnRecalcularSaldos.Enabled = False
        btnCargarUltimoDetalle.Enabled = True
        Button1.Enabled = False

        'Controles
        'dtActividades = CSistema.ExecuteToDataTable("Select * From VActividad Where Pendiente = 'True' ").Copy

        'Cabecera
        txtProveedor.Clear()
        txtDesde.txt.Clear()
        txtHasta.txt.Clear()
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()
        txtPresupuesto.SetValue(0)
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From PlanillaDescuentoTactico Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)
        'txtComprobante.txt.Text = CSistema.ObtenerProximoNroComprobante(cbxTipoComprobante.cbx.SelectedValue, "PlanillaDescuentoTactico", "NroComprobante", cbxSucursal.cbx.SelectedValue)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Poner el foco
        If txtComprobante.txt.Text = "" Then
            txtComprobante.txt.Focus()
        Else
            txtProveedor.txtReferencia.Focus()
        End If

        'CargarUltimoDetalle()
        btnCargarUltimoDetalle.Enabled = False

        txtDesde.SetValue(MFuncionesGlobales.FirstDayOfMonth(Date.Today))
        txtHasta.SetValue(MFuncionesGlobales.LastDayOfMonth(Date.Today))

    End Sub

    Sub CargarUltimoDetalle(ByVal IDProveedor As Integer)

        dtActividades = CSistema.ExecuteToDataTable("Execute SpCargarUltimoDetallePlanillaRegistro " & IDProveedor & "," & vgIDSucursal & "").Copy

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub Cancelar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)
        vNuevo = False
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Validar
        'Ciudad
        If cbxCiudad.Validar("Seleccione correctamente la ciudad!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Tipo Comprobante
        If cbxCiudad.Validar("Seleccione correctamente el tipo de comprobante!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
            Exit Function
        End If

        'Sucursal
        If cbxSucursal.Validar("Seleccione correctamente el la sucursal!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        

        'Detalle
        'If dgw.SelectedRows.Count = 0 Then
        '    Dim mensaje As String = "No se ha seleccionado ningun registro!"
        '    CSistema.MostrarError(mensaje, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
        '    Exit Function
        'End If

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If

        End If

        Return True


    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Desde", txtDesde.GetValueString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Hasta", txtHasta.GetValueString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalPresupuesto", txtPresupuesto.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpPlanillaDescuentoTactico", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From PlanillaDescuentoTactico Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpPlanillaDescuentoTactico", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        Dim Procesar As Boolean = True


        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)
            CargarOperacion(IDTransaccion)
            txtID.SoloLectura = False
        End If



        If IDTransaccion > 0 Then

            'Cargamos el Detalle de Egresos
            Procesar = InsertarDetalle(IDTransaccion)

        End If


        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)
        CargarOperacion(IDTransaccion)

        If dgw.Rows.Count = 0 Then
            btnCargarUltimoDetalle.Enabled = True
        End If
        txtID.SoloLectura = False

    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalle = False

        If dtActividades.Rows.Count = 0 Then
            Return True
        End If

        Dim SQL As String = ""

        For Each oRow As DataRow In dtActividades.Rows
            SQL = SQL & "Insert Into DetallePlanillaDescuentoTactico(IDTransaccion, IDActividad) Values(" & IDTransaccion & ", " & oRow("ID") & ")" & vbCrLf
        Next

        If CSistema.ExecuteNonQuery(SQL) > 0 Then
            Return True
        End If

    End Function

    Sub ListarActividades()

        CSistema.dtToGrid(dgw, dtActividades)

        'Ocultar todas las columnas
        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).Visible = False
            Debug.Print(dgw.Columns(i).Name)
        Next

        'Mostrar algunas columnas
        dgw.Columns("Codigo").Visible = True
        dgw.Columns("Asignado").Visible = True
        dgw.Columns("Entrada").Visible = True
        dgw.Columns("Salida").Visible = True
        dgw.Columns("DescuentoMaximo").Visible = True
        dgw.Columns("Tactico").Visible = True
        dgw.Columns("Acuerdo").Visible = True
        dgw.Columns("Total").Visible = True
        dgw.Columns("PorcentajeUtilizado").Visible = True
        dgw.Columns("Saldo").Visible = True
        dgw.Columns("Excedente").Visible = True

        'Formato moneda
        dgw.Columns("Asignado").DefaultCellStyle.Format = "N0"
        dgw.Columns("Entrada").DefaultCellStyle.Format = "N0"
        dgw.Columns("Salida").DefaultCellStyle.Format = "N0"
        dgw.Columns("DescuentoMaximo").DefaultCellStyle.Format = "N0"
        dgw.Columns("Tactico").DefaultCellStyle.Format = "N0"
        dgw.Columns("Acuerdo").DefaultCellStyle.Format = "N0"
        dgw.Columns("Total").DefaultCellStyle.Format = "N0"
        dgw.Columns("PorcentajeUtilizado").DefaultCellStyle.Format = "N0"
        dgw.Columns("Saldo").DefaultCellStyle.Format = "N0"
        dgw.Columns("Excedente").DefaultCellStyle.Format = "N0"

        'Alineacion
        dgw.Columns("Codigo").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns("Asignado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Entrada").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Salida").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("DescuentoMaximo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Tactico").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Acuerdo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PorcentajeUtilizado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Saldo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Excedente").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        'Cambiar nombre de la columna
        dgw.Columns("DescuentoMaximo").HeaderText = "%Max"
        dgw.Columns("PorcentajeUtilizado").HeaderText = "%Utilizado"



        CalcularTotales()

    End Sub

    Sub CargarActividades(ByVal IDTransaccion As Integer)

        dtActividades = CSistema.ExecuteToDataTable("Select A.*, Seleccionado='False' From VDetallePlanillaDescuentoTactico D Join VActividad A On D.ID=A.ID Where D.IDTransaccion=" & IDTransaccion).Copy
        ListarActividades()

    End Sub

    Sub EliminarActividad()

        For Each item As DataGridViewRow In dgw.SelectedRows
            dtActividades.Rows.RemoveAt(item.Index)
            Exit For
        Next

      

        'For Each item As ListViewItem In lvDetalle.SelectedItems

        '    For Each oRow As DataRow In dtActividades.Select(" ID = " & item.Text & " ")
        '        oRow.Delete()
        '        Exit For
        '    Next

        'Next

        ListarActividades()

    End Sub

    Sub CalcularTotales()

        txtTotalUtilizado.txt.Text = CSistema.TotalesDataTable(dtActividades, "Total", True)
        txtTotalSaldo.txt.Text = CSistema.TotalesDataTable(dtActividades, "Saldo", True)
        txtTotalExcedente.txt.Text = CSistema.TotalesDataTable(dtActividades, "Excedente", True)
        'CSistema.TotalesLv(lvDetalle, txtTotalExcedente.txt, 12, True)
        'CSistema.TotalesLv(lvDetalle, txtTotalUtilizado.txt, 9, True)

    End Sub

    Sub Imprimir()

        Dim frm As New frmInformePlanillaDescuento
        frm.Numero = txtID.ObtenerValor
        frm.IDSucursal = cbxSucursal.GetValue
        FGMostrarFormulario(Me, frm, "Descuento Tactico", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Sub Buscar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, New Button, vControles)

        'Otros

        Dim frm As New frmConsultaPlanillaDescuentoTactico
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog(Me)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From PlanillaDescuentoTactico Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VPlanillaDescuentoTactico Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtProveedor.SetValue(oRow("IDProveedor"))
        txtDesde.SetValue(oRow("Desde"))
        txtHasta.SetValue(oRow("Hasta"))
        txtPresupuesto.SetValue(oRow("TotalPresupuesto"))
        txtObservacion.txt.Text = oRow("Observacion").ToString
        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos el detalle de Egresos
        CargarActividades(IDTransaccion)

        If dgw.RowCount = 0 And btnGuardar.Enabled = False Then
            btnCargarUltimoDetalle.Enabled = True
        Else
            btnCargarUltimoDetalle.Enabled = False
        End If

        'btnCargarUltimoDetalle.Enabled = True
        btnRecalcularSaldos.Enabled = True
        Button1.Enabled = True

        'Calcular Totales
        'CalcularTotales()

    End Sub

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpPlanillaDescuentoTactico", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From PlanillaDescuentoTactico Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpPlanillaDescuentoTactico", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)
        CargarOperacion(IDTransaccion)



    End Sub

    Sub ManejarTecla(ByRef e As System.Windows.Forms.KeyEventArgs)

        If vNuevo Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.ObtenerValor
            ID = CInt(ID) + 1
            txtID.SetValue(ID)
            txtID.txt.SelectAll()
            CargarOperacion()
            Exit Sub
        End If



        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.ObtenerValor

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.SetValue(ID)
            txtID.txt.SelectAll()
            CargarOperacion()
            Exit Sub

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VPlanillaDescuentoTactico Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

            Exit Sub

        End If

        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VPlanillaDescuentoTactico Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

            Exit Sub

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub RecalcularSaldos()

        'Consulta
        If MessageBox.Show("Realmente desea recalcular los saldos? Esto puede tardar varios minutos.", "Recalcular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
            Exit Sub
        End If


        Dim SQL As String = "Exec SpRecalcularSaldoTactico "
        CSistema.ConcatenarParametro(SQL, "@IDTransaccion", IDTransaccion)

        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        If dt Is Nothing Then
            MessageBox.Show("Se produjo un error.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            MessageBox.Show("Se produjo un error.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        If oRow("Procesado") = True Then
            MessageBox.Show("Registros procesados: " & oRow("Registros"), "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)
            CargarOperacion(IDTransaccion)
            Exit Sub
        Else
            MessageBox.Show(oRow("Mensaje"), "Informe", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

    End Sub

    Sub VerDetalle()

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim IDActividad As Integer = dgw.SelectedCells(0).Value

        Dim frm As New frmActividadDescuentoDetalle
        frm.IDActividad = IDActividad
        frm.FechaDesde = txtDesde.GetValue
        frm.FechaHasta = txtHasta.GetValue
        frm.WindowState = FormWindowState.Maximized
        FGMostrarFormulario(Me, frm, "Detalle de Actividades", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

    End Sub

    Private Sub frmPlanillaDescuentoTactico_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmPlanillaDescuentoTactico_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Enter Then

            'If txtProveedor.Focus = True Then
            '    Exit Sub
            'End If


        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub frmPlanillaDescuentoTactico_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()

    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal", "IDCiudad=" & cbxCiudad.cbx.SelectedValue).Copy, "ID", "Descripcion")


    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        CargarActividad()
    End Sub

    Private Sub txtProveedor_ItemSeleccionado(ByVal sender As Object, ByVal e As System.EventArgs)
        txtDesde.txt.Focus()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If txtProveedor.Seleccionado = False Then
            Exit Sub
        End If

        SeleccionarActividad()

    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        EditarActividad()
    End Sub

    Private Sub lklEliminar_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminar.LinkClicked
        EliminarActividad()
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub txtProveedor_ItemSeleccionado_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProveedor.ItemSeleccionado
        'If vNuevo = False Then
        '    Exit Sub
        'End If

        'CargarUltimoDetalle(txtProveedor.Registro("ID").ToString)

        'txtDesde.Focus()
    End Sub

    Private Sub txtID_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtID.Leave

    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked

    End Sub

    Private Sub btnCargarUltimoDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'CargarUltimoDetalle()
    End Sub

    Private Sub btnRecalcularSaldos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRecalcularSaldos.Click
        RecalcularSaldos()
    End Sub

    Private Sub VerDetalleToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VerDetalleToolStripMenuItem.Click
        VerDetalle()
    End Sub

    Private Sub dgw_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgw.MouseMove

        

    End Sub

    Private Sub dgw_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgw.SelectionChanged

        txtMecanica.txt.Text = ""

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        txtMecanica.txt.Text = dgw.SelectedRows(0).Cells("Mecanica").Value

    End Sub

    Private Sub btnCargarUltimoDetalle_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCargarUltimoDetalle.Click
        Try
            ''JGR 20140502
            ''IMPORTANTE: SOlo cargar Ultimo detalle si NO existen registros, caso contrario entra
            '' en un bucle que llena las tablas Actividad, DetalleActividad y DetallePlanillaDescuentoTactico infinitamente
            ''TODO: Falta un proceso de eliminar detalles caso ya haya cargado
            If dgw.RowCount = 0 And btnGuardar.Enabled = False Then
                CargarUltimoDetalle(txtProveedor.txtID.ObtenerValor)
            Else
                MessageBox.Show("Para cargar los últimos detalles la lista de detalles debe estar vacía.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmPlanillaDescuentoTactico_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmInformePlanillaDescuento

    'CLASES
    Dim CSistema As New CSistema
    Dim CReporte As New Reporte.CReporteDescuentoTactico

    'PROPIEDADES
    Private NumeroValue As Integer
    Public Property Numero() As Integer
        Get
            Return NumeroValue
        End Get
        Set(ByVal value As Integer)
            NumeroValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    'VARIABLES
    Dim vIDTransaccion As Integer

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.Text = "Planilla de Tacticos"
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'IDTransaccion
        vIDTransaccion = CSistema.ExecuteScalar("Select IDTransaccion From PlanillaDescuentoTactico Where IDSucursal=" & IDSucursal & " And Numero=" & Numero & " ")


    End Sub

    Sub Listar()

        If rdbListaProducto.Checked = True Then
            CReporte.ListadoProducto(True, vIDTransaccion)
        End If

        If rdbListaProductoDetallado.Checked = True Then
            CReporte.ListadoProductoDetallado(True, vIDTransaccion)
        End If

        If rdbListadoPosmorten.Checked = True Then
            CReporte.ListadoPosmorten(True, vIDTransaccion)
        End If

    End Sub

    Private Sub frmInformePlanillaDescuento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmInformePlanillaDescuento_Activate()
        Me.Refresh()
    End Sub
End Class
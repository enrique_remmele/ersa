﻿Public Class frmActividadDescuentoDetalle

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private IDActividadValue As Integer
    Public Property IDActividad() As Integer
        Get
            Return IDActividadValue
        End Get
        Set(ByVal value As Integer)
            IDActividadValue = value
        End Set
    End Property

    Private CodigoActividadValue As String
    Public Property CodigoActividad() As String
        Get
            Return CodigoActividadValue
        End Get
        Set(ByVal value As String)
            CodigoActividadValue = value
        End Set
    End Property

    Private ActividadValue As String
    Public Property Actividad() As String
        Get
            Return ActividadValue
        End Get
        Set(ByVal value As String)
            ActividadValue = value
        End Set
    End Property

    Private FechaDesdeValue As Date
    Public Property FechaDesde() As Date
        Get
            Return FechaDesdeValue
        End Get
        Set(ByVal value As Date)
            FechaDesdeValue = value
        End Set
    End Property

    Private FechaHastaValue As Date
    Public Property FechaHasta() As Date
        Get
            Return FechaHastaValue
        End Get
        Set(ByVal value As Date)
            FechaHastaValue = value
        End Set
    End Property

    'VARIABLES
    Dim dt As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Fecha
        txtDesde.SetValue(FechaDesde)
        txtHasta.SetValue(FechaHasta)

        'Funciones
        CargarInformacion()

        'Foco
        chkProducto.Focus()

    End Sub

    Sub CargarInformacion()

        'Producto
        CSistema.SqlToComboBox(cbxProducto.cbx, CData.GetTable("VProducto").Copy, "ID", "Descripcion")

        'Cliente
        CSistema.SqlToComboBox(cbxCliente.cbx, CData.GetTable("VCliente").Copy, "ID", "RazonSocial")

        'Vendedor
        CSistema.SqlToComboBox(cbxVendedor.cbx, CData.GetTable("VVendedor").Copy, "ID", "Nombres")

        Dim SQL As String = "Select Referencia, Producto, Vendedor, Cliente, Venta, FechaEmision, [Desc %], Cantidad, Descuento, IDProducto, IDCliente, IDVendedor From VDetalleActividadDescuento "
        Dim Where As String = " Where IDActividad = " & IDActividad & " And (FechaEmision Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "') And (Desde >= '" & txtDesde.GetValueString & "' And Hasta <='" & txtHasta.GetValueString & "') "
        Dim OrderBy As String = " Order By Venta"
        dt = CSistema.ExecuteToDataTable(SQL & Where & OrderBy)

        Listar()

    End Sub

    Sub Listar(Optional ByVal Condicion As String = "")

        If dt Is Nothing Then
            Exit Sub
        End If

        Dim dttemp As DataTable = dt.Copy
        Dim SQL As String = ""

        SQL = " FechaEmision >= '" & txtDesde.GetValueString & "' And FechaEmision <= '" & txtHasta.GetValueString & "' "

        If cbxProducto.Enabled = True Then
            SQL = SQL & " And IDProducto = " & cbxProducto.GetValue
        End If

        If cbxCliente.Enabled = True Then
            SQL = SQL & " And IDCliente = " & cbxCliente.GetValue
        End If

        If cbxVendedor.Enabled = True Then
            SQL = SQL & " And IDVendedor= " & cbxVendedor.GetValue
        End If

        dttemp = CData.FiltrarDataTable(dttemp, SQL)

        CSistema.dtToGrid(dgvLista, dttemp)

        If dgvLista.Columns.Count = 0 Then
            Exit Sub
        End If

        'Fotmato
        'Esconder columnas
        dgvLista.Columns("IDProducto").Visible = False
        dgvLista.Columns("IDCliente").Visible = False
        dgvLista.Columns("IDVendedor").Visible = False

        'Alineamento
        dgvLista.Columns("Descuento").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Desc %").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        'Numerico
        dgvLista.Columns("Descuento").DefaultCellStyle.Format = "N0"
        dgvLista.Columns("Desc %").DefaultCellStyle.Format = "N2"
        dgvLista.Columns("Cantidad").DefaultCellStyle.Format = "N0"

        'Fill
        dgvLista.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        'Resumen
        txtCantidad.SetValue(dgvLista.RowCount)
        txtTotal.SetValue(CSistema.gridSumColumn(dgvLista, "Descuento"))

    End Sub

    Private Sub frmActividadDescuentoDetalle_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmActividadDescuentoDetalle_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub chkProducto_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkProducto.PropertyChanged
        cbxProducto.Enabled = chkProducto.Valor
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCliente.PropertyChanged
        cbxCliente.Enabled = chkCliente.Valor
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = chkVendedor.Valor
    End Sub

    Private Sub cbxProducto_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxProducto.PropertyChanged
        If cbxProducto.Enabled = False Then
            Exit Sub
        End If

        Listar(" IDProducto = " & cbxProducto.GetValue)

    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmActividadDescuentoDetalle_Activate()
        Me.Refresh()
    End Sub
End Class
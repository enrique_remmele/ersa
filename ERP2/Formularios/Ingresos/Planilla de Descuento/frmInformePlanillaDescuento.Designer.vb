﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInformePlanillaDescuento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.rdbListaProducto = New System.Windows.Forms.RadioButton()
        Me.rdbListaProductoDetallado = New System.Windows.Forms.RadioButton()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.gbx = New System.Windows.Forms.GroupBox()
        Me.rdbListadoPosmorten = New System.Windows.Forms.RadioButton()
        Me.gbx.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(40, 115)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(75, 23)
        Me.btnListar.TabIndex = 1
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'rdbListaProducto
        '
        Me.rdbListaProducto.AutoSize = True
        Me.rdbListaProducto.Checked = True
        Me.rdbListaProducto.Location = New System.Drawing.Point(19, 28)
        Me.rdbListaProducto.Name = "rdbListaProducto"
        Me.rdbListaProducto.Size = New System.Drawing.Size(113, 17)
        Me.rdbListaProducto.TabIndex = 0
        Me.rdbListaProducto.TabStop = True
        Me.rdbListaProducto.Text = "Lista de Productos"
        Me.rdbListaProducto.UseVisualStyleBackColor = True
        '
        'rdbListaProductoDetallado
        '
        Me.rdbListaProductoDetallado.AutoSize = True
        Me.rdbListaProductoDetallado.Location = New System.Drawing.Point(19, 51)
        Me.rdbListaProductoDetallado.Name = "rdbListaProductoDetallado"
        Me.rdbListaProductoDetallado.Size = New System.Drawing.Size(161, 17)
        Me.rdbListaProductoDetallado.TabIndex = 1
        Me.rdbListaProductoDetallado.Text = "Lista de Productos Detallado"
        Me.rdbListaProductoDetallado.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(121, 115)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(75, 23)
        Me.btnCerrar.TabIndex = 2
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'gbx
        '
        Me.gbx.Controls.Add(Me.rdbListadoPosmorten)
        Me.gbx.Controls.Add(Me.rdbListaProducto)
        Me.gbx.Controls.Add(Me.rdbListaProductoDetallado)
        Me.gbx.Location = New System.Drawing.Point(12, 8)
        Me.gbx.Name = "gbx"
        Me.gbx.Size = New System.Drawing.Size(187, 101)
        Me.gbx.TabIndex = 0
        Me.gbx.TabStop = False
        Me.gbx.Text = "- Seleccione el tipo de Informe:"
        '
        'rdbListadoPosmorten
        '
        Me.rdbListadoPosmorten.AutoSize = True
        Me.rdbListadoPosmorten.Location = New System.Drawing.Point(19, 74)
        Me.rdbListadoPosmorten.Name = "rdbListadoPosmorten"
        Me.rdbListadoPosmorten.Size = New System.Drawing.Size(112, 17)
        Me.rdbListadoPosmorten.TabIndex = 2
        Me.rdbListadoPosmorten.Text = "Listado Posmorten"
        Me.rdbListadoPosmorten.UseVisualStyleBackColor = True
        '
        'frmInformePlanillaDescuento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(211, 145)
        Me.Controls.Add(Me.gbx)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnListar)
        Me.Name = "frmInformePlanillaDescuento"
        Me.Text = "frmInformePlanillaDescuento"
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnListar As System.Windows.Forms.Button
    Friend WithEvents rdbListaProducto As System.Windows.Forms.RadioButton
    Friend WithEvents rdbListaProductoDetallado As System.Windows.Forms.RadioButton
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents gbx As System.Windows.Forms.GroupBox
    Friend WithEvents rdbListadoPosmorten As System.Windows.Forms.RadioButton
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPlanillaDescuentoTactico
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.gbxDetalle = New System.Windows.Forms.GroupBox()
        Me.lblMecanica = New System.Windows.Forms.Label()
        Me.txtMecanica = New ERP.ocxTXTString()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.VerDetalleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnRecalcularSaldos = New System.Windows.Forms.Button()
        Me.btnCargarUltimoDetalle = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.txtTotalExcedente = New ERP.ocxTXTNumeric()
        Me.lblTotalExcedente = New System.Windows.Forms.Label()
        Me.txtTotalSaldo = New ERP.ocxTXTNumeric()
        Me.lblTotalSaldo = New System.Windows.Forms.Label()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.txtTotalUtilizado = New ERP.ocxTXTNumeric()
        Me.lblTotalUtilizado = New System.Windows.Forms.Label()
        Me.lklEliminar = New System.Windows.Forms.LinkLabel()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtPresupuesto = New ERP.ocxTXTNumeric()
        Me.lblPresupuesto = New System.Windows.Forms.Label()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.lblHasta = New System.Windows.Forms.Label()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.lblDesde = New System.Windows.Forms.Label()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.ToolTip2 = New System.Windows.Forms.ToolTip(Me.components)
        Me.gbxDetalle.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxDetalle
        '
        Me.gbxDetalle.Controls.Add(Me.lblMecanica)
        Me.gbxDetalle.Controls.Add(Me.txtMecanica)
        Me.gbxDetalle.Controls.Add(Me.dgw)
        Me.gbxDetalle.Controls.Add(Me.btnRecalcularSaldos)
        Me.gbxDetalle.Controls.Add(Me.btnCargarUltimoDetalle)
        Me.gbxDetalle.Controls.Add(Me.Button1)
        Me.gbxDetalle.Controls.Add(Me.flpAnuladoPor)
        Me.gbxDetalle.Controls.Add(Me.flpRegistradoPor)
        Me.gbxDetalle.Controls.Add(Me.LinkLabel2)
        Me.gbxDetalle.Controls.Add(Me.LinkLabel1)
        Me.gbxDetalle.Controls.Add(Me.txtTotalExcedente)
        Me.gbxDetalle.Controls.Add(Me.lblTotalExcedente)
        Me.gbxDetalle.Controls.Add(Me.txtTotalSaldo)
        Me.gbxDetalle.Controls.Add(Me.lblTotalSaldo)
        Me.gbxDetalle.Controls.Add(Me.txtCantidad)
        Me.gbxDetalle.Controls.Add(Me.txtTotalUtilizado)
        Me.gbxDetalle.Controls.Add(Me.lblTotalUtilizado)
        Me.gbxDetalle.Controls.Add(Me.lklEliminar)
        Me.gbxDetalle.Controls.Add(Me.btnAgregar)
        Me.gbxDetalle.Location = New System.Drawing.Point(12, 91)
        Me.gbxDetalle.Name = "gbxDetalle"
        Me.gbxDetalle.Size = New System.Drawing.Size(978, 396)
        Me.gbxDetalle.TabIndex = 1
        Me.gbxDetalle.TabStop = False
        '
        'lblMecanica
        '
        Me.lblMecanica.AutoSize = True
        Me.lblMecanica.Location = New System.Drawing.Point(194, 322)
        Me.lblMecanica.Name = "lblMecanica"
        Me.lblMecanica.Size = New System.Drawing.Size(57, 13)
        Me.lblMecanica.TabIndex = 23
        Me.lblMecanica.Text = "Mecanica:"
        '
        'txtMecanica
        '
        Me.txtMecanica.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMecanica.Color = System.Drawing.Color.Empty
        Me.txtMecanica.Indicaciones = Nothing
        Me.txtMecanica.Location = New System.Drawing.Point(253, 318)
        Me.txtMecanica.Multilinea = False
        Me.txtMecanica.Name = "txtMecanica"
        Me.txtMecanica.Size = New System.Drawing.Size(436, 21)
        Me.txtMecanica.SoloLectura = False
        Me.txtMecanica.TabIndex = 22
        Me.txtMecanica.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMecanica.Texto = ""
        '
        'dgw
        '
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.ContextMenuStrip = Me.ContextMenuStrip1
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgw.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgw.Location = New System.Drawing.Point(3, 39)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgw.Size = New System.Drawing.Size(970, 276)
        Me.dgw.TabIndex = 21
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VerDetalleToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(130, 26)
        '
        'VerDetalleToolStripMenuItem
        '
        Me.VerDetalleToolStripMenuItem.Name = "VerDetalleToolStripMenuItem"
        Me.VerDetalleToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
        Me.VerDetalleToolStripMenuItem.Text = "Ver detalle"
        '
        'btnRecalcularSaldos
        '
        Me.btnRecalcularSaldos.Location = New System.Drawing.Point(835, 12)
        Me.btnRecalcularSaldos.Name = "btnRecalcularSaldos"
        Me.btnRecalcularSaldos.Size = New System.Drawing.Size(130, 21)
        Me.btnRecalcularSaldos.TabIndex = 2
        Me.btnRecalcularSaldos.Text = "Recalcular Saldos"
        Me.btnRecalcularSaldos.UseVisualStyleBackColor = True
        '
        'btnCargarUltimoDetalle
        '
        Me.btnCargarUltimoDetalle.Location = New System.Drawing.Point(281, 12)
        Me.btnCargarUltimoDetalle.Name = "btnCargarUltimoDetalle"
        Me.btnCargarUltimoDetalle.Size = New System.Drawing.Size(130, 21)
        Me.btnCargarUltimoDetalle.TabIndex = 1
        Me.btnCargarUltimoDetalle.Text = "Cargar Ultimo Detalle"
        Me.btnCargarUltimoDetalle.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(145, 12)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(130, 21)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Seleccionar actividad"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(254, 370)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(247, 20)
        Me.flpAnuladoPor.TabIndex = 14
        Me.flpAnuladoPor.Visible = False
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(6, 370)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(239, 20)
        Me.flpRegistradoPor.TabIndex = 13
        Me.flpRegistradoPor.Visible = False
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'LinkLabel2
        '
        Me.LinkLabel2.AutoSize = True
        Me.LinkLabel2.Location = New System.Drawing.Point(120, 322)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(51, 13)
        Me.LinkLabel2.TabIndex = 6
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "Transferir"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(80, 322)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(34, 13)
        Me.LinkLabel1.TabIndex = 5
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Editar"
        '
        'txtTotalExcedente
        '
        Me.txtTotalExcedente.Color = System.Drawing.Color.Empty
        Me.txtTotalExcedente.Decimales = True
        Me.txtTotalExcedente.Indicaciones = Nothing
        Me.txtTotalExcedente.Location = New System.Drawing.Point(784, 361)
        Me.txtTotalExcedente.Name = "txtTotalExcedente"
        Me.txtTotalExcedente.Size = New System.Drawing.Size(137, 22)
        Me.txtTotalExcedente.SoloLectura = True
        Me.txtTotalExcedente.TabIndex = 12
        Me.txtTotalExcedente.TabStop = False
        Me.txtTotalExcedente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalExcedente.Texto = "0"
        '
        'lblTotalExcedente
        '
        Me.lblTotalExcedente.AutoSize = True
        Me.lblTotalExcedente.Location = New System.Drawing.Point(690, 364)
        Me.lblTotalExcedente.Name = "lblTotalExcedente"
        Me.lblTotalExcedente.Size = New System.Drawing.Size(88, 13)
        Me.lblTotalExcedente.TabIndex = 11
        Me.lblTotalExcedente.Text = "Total Excedente:"
        '
        'txtTotalSaldo
        '
        Me.txtTotalSaldo.Color = System.Drawing.Color.Empty
        Me.txtTotalSaldo.Decimales = True
        Me.txtTotalSaldo.Indicaciones = Nothing
        Me.txtTotalSaldo.Location = New System.Drawing.Point(784, 341)
        Me.txtTotalSaldo.Name = "txtTotalSaldo"
        Me.txtTotalSaldo.Size = New System.Drawing.Size(137, 22)
        Me.txtTotalSaldo.SoloLectura = True
        Me.txtTotalSaldo.TabIndex = 10
        Me.txtTotalSaldo.TabStop = False
        Me.txtTotalSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalSaldo.Texto = "0"
        '
        'lblTotalSaldo
        '
        Me.lblTotalSaldo.AutoSize = True
        Me.lblTotalSaldo.Location = New System.Drawing.Point(690, 344)
        Me.lblTotalSaldo.Name = "lblTotalSaldo"
        Me.lblTotalSaldo.Size = New System.Drawing.Size(64, 13)
        Me.lblTotalSaldo.TabIndex = 9
        Me.lblTotalSaldo.Text = "Total Saldo:"
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = Nothing
        Me.txtCantidad.Location = New System.Drawing.Point(923, 321)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidad.SoloLectura = False
        Me.txtCantidad.TabIndex = 10
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'txtTotalUtilizado
        '
        Me.txtTotalUtilizado.Color = System.Drawing.Color.Empty
        Me.txtTotalUtilizado.Decimales = True
        Me.txtTotalUtilizado.Indicaciones = Nothing
        Me.txtTotalUtilizado.Location = New System.Drawing.Point(784, 321)
        Me.txtTotalUtilizado.Name = "txtTotalUtilizado"
        Me.txtTotalUtilizado.Size = New System.Drawing.Size(137, 22)
        Me.txtTotalUtilizado.SoloLectura = True
        Me.txtTotalUtilizado.TabIndex = 8
        Me.txtTotalUtilizado.TabStop = False
        Me.txtTotalUtilizado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalUtilizado.Texto = "0"
        '
        'lblTotalUtilizado
        '
        Me.lblTotalUtilizado.AutoSize = True
        Me.lblTotalUtilizado.Location = New System.Drawing.Point(690, 324)
        Me.lblTotalUtilizado.Name = "lblTotalUtilizado"
        Me.lblTotalUtilizado.Size = New System.Drawing.Size(77, 13)
        Me.lblTotalUtilizado.TabIndex = 7
        Me.lblTotalUtilizado.Text = "Total Utilizado:"
        '
        'lklEliminar
        '
        Me.lklEliminar.AutoSize = True
        Me.lklEliminar.Location = New System.Drawing.Point(20, 322)
        Me.lklEliminar.Name = "lklEliminar"
        Me.lklEliminar.Size = New System.Drawing.Size(43, 13)
        Me.lklEliminar.TabIndex = 4
        Me.lklEliminar.TabStop = True
        Me.lklEliminar.Text = "Eliminar"
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(9, 12)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(130, 21)
        Me.btnAgregar.TabIndex = 0
        Me.btnAgregar.Text = "Nueva actividad"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(15, 499)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 2
        Me.btnAnular.Text = "&Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(95, 499)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 3
        Me.btnImprimir.Text = "&Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(176, 499)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(75, 23)
        Me.btnBusquedaAvanzada.TabIndex = 4
        Me.btnBusquedaAvanzada.Text = "&Busqueda"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(907, 499)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 8
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(626, 499)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 7
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(464, 499)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 5
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(545, 499)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 6
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 533)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(996, 22)
        Me.StatusStrip1.TabIndex = 9
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtID)
        Me.GroupBox1.Controls.Add(Me.txtPresupuesto)
        Me.GroupBox1.Controls.Add(Me.lblPresupuesto)
        Me.GroupBox1.Controls.Add(Me.txtDesde)
        Me.GroupBox1.Controls.Add(Me.lblHasta)
        Me.GroupBox1.Controls.Add(Me.txtProveedor)
        Me.GroupBox1.Controls.Add(Me.txtObservacion)
        Me.GroupBox1.Controls.Add(Me.cbxSucursal)
        Me.GroupBox1.Controls.Add(Me.txtHasta)
        Me.GroupBox1.Controls.Add(Me.lblDesde)
        Me.GroupBox1.Controls.Add(Me.cbxTipoComprobante)
        Me.GroupBox1.Controls.Add(Me.txtComprobante)
        Me.GroupBox1.Controls.Add(Me.lblProveedor)
        Me.GroupBox1.Controls.Add(Me.lblObservacion)
        Me.GroupBox1.Controls.Add(Me.cbxCiudad)
        Me.GroupBox1.Controls.Add(Me.lblSucursal)
        Me.GroupBox1.Controls.Add(Me.lblComprobante)
        Me.GroupBox1.Controls.Add(Me.lblOperacion)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(973, 82)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(134, 20)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(39, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtPresupuesto
        '
        Me.txtPresupuesto.Color = System.Drawing.Color.Empty
        Me.txtPresupuesto.Decimales = True
        Me.txtPresupuesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPresupuesto.Indicaciones = Nothing
        Me.txtPresupuesto.Location = New System.Drawing.Point(334, 47)
        Me.txtPresupuesto.Name = "txtPresupuesto"
        Me.txtPresupuesto.Size = New System.Drawing.Size(118, 22)
        Me.txtPresupuesto.SoloLectura = False
        Me.txtPresupuesto.TabIndex = 15
        Me.txtPresupuesto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtPresupuesto.Texto = "0"
        '
        'lblPresupuesto
        '
        Me.lblPresupuesto.AutoSize = True
        Me.lblPresupuesto.Location = New System.Drawing.Point(259, 51)
        Me.lblPresupuesto.Name = "lblPresupuesto"
        Me.lblPresupuesto.Size = New System.Drawing.Size(69, 13)
        Me.lblPresupuesto.TabIndex = 14
        Me.lblPresupuesto.Text = "Presupuesto:"
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 19, 14, 37, 27, 960)
        Me.txtDesde.Location = New System.Drawing.Point(73, 47)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(71, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 11
        '
        'lblHasta
        '
        Me.lblHasta.AutoSize = True
        Me.lblHasta.Location = New System.Drawing.Point(144, 51)
        Me.lblHasta.Name = "lblHasta"
        Me.lblHasta.Size = New System.Drawing.Size(38, 13)
        Me.lblHasta.TabIndex = 12
        Me.lblHasta.Text = "Hasta:"
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 72
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(587, 19)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(378, 23)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 9
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(529, 49)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(436, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 17
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(231, 20)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(85, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 19, 14, 37, 28, 120)
        Me.txtHasta.Location = New System.Drawing.Point(182, 47)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(71, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 13
        '
        'lblDesde
        '
        Me.lblDesde.AutoSize = True
        Me.lblDesde.Location = New System.Drawing.Point(33, 51)
        Me.lblDesde.Name = "lblDesde"
        Me.lblDesde.Size = New System.Drawing.Size(41, 13)
        Me.lblDesde.TabIndex = 10
        Me.lblDesde.Text = "Desde:"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(389, 20)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(63, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 6
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(452, 20)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(76, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 7
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(528, 24)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(59, 13)
        Me.lblProveedor.TabIndex = 8
        Me.lblProveedor.Text = "Proveedor:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(458, 51)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 16
        Me.lblObservacion.Text = "Observacion:"
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = ""
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(73, 20)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(63, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(180, 24)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(316, 24)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 5
        Me.lblComprobante.Text = "Comprobante:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(14, 24)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'frmPlanillaDescuentoTactico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(996, 555)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.gbxDetalle)
        Me.KeyPreview = True
        Me.Name = "frmPlanillaDescuentoTactico"
        Me.Tag = "PLANILLA DE DESCUENTO TACTICOS"
        Me.Text = "frmPlanillaDescuentoTactico"
        Me.gbxDetalle.ResumeLayout(False)
        Me.gbxDetalle.PerformLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxDetalle As System.Windows.Forms.GroupBox
    Friend WithEvents txtCantidad As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalUtilizado As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalUtilizado As System.Windows.Forms.Label
    Friend WithEvents lklEliminar As System.Windows.Forms.LinkLabel
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents txtTotalExcedente As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalExcedente As System.Windows.Forms.Label
    Friend WithEvents txtTotalSaldo As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalSaldo As System.Windows.Forms.Label
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents LinkLabel2 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPresupuesto As ERP.ocxTXTNumeric
    Friend WithEvents lblPresupuesto As System.Windows.Forms.Label
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents lblHasta As System.Windows.Forms.Label
    Friend WithEvents txtProveedor As ERP.ocxTXTProveedor
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents lblDesde As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents btnRecalcularSaldos As System.Windows.Forms.Button
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents VerDetalleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents lblMecanica As System.Windows.Forms.Label
    Friend WithEvents txtMecanica As ERP.ocxTXTString
    Friend WithEvents ToolTip2 As System.Windows.Forms.ToolTip
    Friend WithEvents btnCargarUltimoDetalle As System.Windows.Forms.Button
End Class
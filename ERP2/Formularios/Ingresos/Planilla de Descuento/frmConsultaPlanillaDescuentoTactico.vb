﻿Public Class frmConsultaPlanillaDescuentoTactico

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CData As New CData

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar

    Sub Inicializar()

        'Form

        'TextBox
        'txtCantidadDetalle.txt.ResetText()
        txtCantidadOperacion.txt.ResetText()
        txtComprobante.ResetText()
        'txtTotalDetalle.txt.ResetText()
        txtTotalOperacion.txt.ResetText()

        'CheckBox
        chkComprobante.Checked = False

        chkUsuario.Checked = False
        chkSucursal.Checked = False

        'ComboBox
        cbxComprobante.Enabled = False

        cbxUsuario.Enabled = False
        cbxSucursal.Enabled = False

        'RadioButton

        'ListView
        ' lvDetalle.Items.Clear()
        lvOperacion.Items.Clear()

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        CargarInformacion()

        'Foco

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        
        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal, CData.GetTable("VSucursal", " ID = " & vgIDSucursal), "ID", "Descripcion")


        ''Condicion
        'cbxCondicion.Items.Add("CONT")
        'cbxCondicion.Items.Add("CRED")
        'cbxCondicion.DropDownStyle = ComboBoxStyle.DropDownList

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxComprobante, "Select ID, Codigo From TipoComprobante Where IDOperacion=28")

        'Usuarios
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxUsuario, "Select ID, Usuario From Usuario Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

        'Condicion


        'Comprobante
        chkComprobante.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "COMPROBANTE ACTIVO", "False")
        cbxComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "COMPROBANTE", "")
        txtComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "NROCOMPROBANTE", "")

        'Usuario
        chkUsuario.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "USUARIO ACTIVO", "False")
        cbxUsuario.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "USUARIO", "")


    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCRUSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Condicion


        'Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "COMPROBANTE ACTIVO", chkComprobante.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "COMPROBANTE", cbxComprobante.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "NROCOMPROBANTE", txtComprobante.Text)

        'Usuario
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "USUARIO ACTIVO", chkUsuario.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "USUARIO", cbxUsuario.Text)

    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Operaciones
    Sub ListarOperaciones(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "")

        ctrError.Clear()

        Consulta = "Select Sucursal, 'TipoComprobante'=DescripcionTipoComprobante , [Cod.] , Comprobante, Proveedor, [Fec. Desde], [Fec. Hasta], FechaTransaccion, TotalPresupuesto, IDTransaccion  from VPlanillaDescuentoTactico "

        Where = Condicion

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la Sucursal!") = False Then
            Exit Sub
        End If

        'Condicion


        'Comprobante
        If chkComprobante.Checked = True Then

            If EstablecerCondicion(cbxComprobante, chkComprobante, "IDTipoComprobante", "Seleccione correctamente el tipo de comprobante!") = False Then
                Exit Sub
            Else
                If Where = "" Then
                    Where = " Where (Comprobante Like '%" & txtComprobante.Text.Trim & "%') "
                Else
                    Where = Where & " And (Comprobante Like '%" & txtComprobante.Text.Trim & "%') "
                End If
            End If

        End If

        'Usuario
        If EstablecerCondicion(cbxUsuario, chkUsuario, "IDUsuario", "Seleccione correctamente el usuario!") = False Then
            Exit Sub
        End If

        lvOperacion.Items.Clear()
        'lvDetalle.Items.Clear()

        CSistema.SqlToLv(lvOperacion, Consulta & " " & Where & " Order By FechaTransaccion")

        'Formato
        lvOperacion.Columns(7).Width = 0
        lvOperacion.Columns(9).Width = 0

        'Totales
        CSistema.FormatoMoneda(lvOperacion, 8)
        CSistema.TotalesLv(lvOperacion, txtTotalOperacion.txt, 8)
        txtCantidadOperacion.txt.Text = lvOperacion.Items.Count


    End Sub

    'Listar Detalle
    'Sub ListarDetalle()

    '    ctrError.Clear()

    '    'Validar
    '    If lvOperacion.SelectedItems.Count = 0 Then
    '        Dim Mensaje As String = "Seleccione correctamente un registro!"
    '        ctrError.SetError(lvOperacion, Mensaje)
    '        ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
    '        Exit Sub
    '    End If

    '    If IsNumeric(lvOperacion.SelectedItems(0).SubItems(10).Text) = False Then
    '        Dim Mensaje As String = "Seleccione correctamente un registro!"
    '        ctrError.SetError(lvOperacion, Mensaje)
    '        ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
    '        Exit Sub
    '    End If

    '    'Limpiar ListView
    '    lvDetalle.Items.Clear()

    '    'Obtener el IDTransaccion
    '    IDTransaccion = CInt(lvOperacion.SelectedItems(0).SubItems(10).Text)
    '    Dim Consulta As String = "Select 'ID'=IDProducto, Descripcion, Deposito, [Pre. Uni.], Total  from VDetalleNotaCreditoProveedor  Where IDTransaccion=" & IDTransaccion & " Order By ID"
    '    CSistema.SqlToLv(lvDetalle, Consulta)
    '    CSistema.FormatoMoneda(lvDetalle, 2)
    '    CSistema.FormatoMoneda(lvDetalle, 3)
    '    CSistema.FormatoMoneda(lvDetalle, 4)

    '    txtCantidadDetalle.txt.Text = lvDetalle.Items.Count
    '    CSistema.TotalesLv(lvDetalle, txtTotalDetalle.txt, 4)


    'End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        If lvOperacion.SelectedItems.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(lvOperacion.SelectedItems(0).SubItems(13).Text) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = CInt(lvOperacion.SelectedItems(0).SubItems(13).Text)

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub btnRegistrosDelDia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosDelDia.Click
        Where = " Where (DateDiff(dd, FechaTransaccion, GetDate())=0) "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub frmConsultaCompra_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaCompra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub lvOperacion_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvOperacion.DoubleClick
        SeleccionarRegistro()
    End Sub

    Private Sub lvOperacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvOperacion.SelectedIndexChanged
        'ListarDetalle()
    End Sub

    Private Sub chkComprobante_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkComprobante.CheckedChanged
        If chkComprobante.Checked = True Then
            cbxComprobante.Enabled = True
            txtComprobante.Enabled = True
        Else
            cbxComprobante.Enabled = False
            txtComprobante.Enabled = False
        End If

    End Sub

    Private Sub chkUsuario_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUsuario.CheckedChanged
        HabilitarControles(chkUsuario, cbxUsuario)
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub

    Private Sub btnRegistrosDelMes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosDelMes.Click
        Where = " Where (DateDiff(MM, FechaTransaccion, GetDate())=0) "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub btnRegistrosGenerales_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosGenerales.Click
        ListarOperaciones()
    End Sub

    Private Sub dtbRegistrosPorFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtbRegistrosPorFecha.Click
        Where = " Where (Desde ='" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And  Hasta='" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "') "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub lvOperacion_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvOperacion.KeyUp
        If e.KeyCode = Keys.Enter Then
            SeleccionarRegistro()
        End If

    End Sub

    Private Sub frmConsultaMovimiento_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmConsultaPlanillaDescuentoTactico_Activate()
        Me.Refresh()
    End Sub
End Class




﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAsignarLimiteDescuento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.lblPorcentaje = New System.Windows.Forms.Label()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.cbxListaPrecio = New ERP.ocxCBX()
        Me.txtPorcentaje = New ERP.ocxTXTNumeric()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 435)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(505, 22)
        Me.StatusStrip1.TabIndex = 124
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(418, 403)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 123
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(418, 131)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 120
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(337, 131)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 121
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(175, 131)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 119
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'lblPorcentaje
        '
        Me.lblPorcentaje.AutoSize = True
        Me.lblPorcentaje.Location = New System.Drawing.Point(31, 97)
        Me.lblPorcentaje.Name = "lblPorcentaje"
        Me.lblPorcentaje.Size = New System.Drawing.Size(61, 13)
        Me.lblPorcentaje.TabIndex = 100
        Me.lblPorcentaje.Text = "Porcentaje:"
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(256, 131)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 118
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(12, 59)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(80, 13)
        Me.lblSucursal.TabIndex = 125
        Me.lblSucursal.Text = "Lista de Precio:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(46, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 127
        Me.Label1.Text = "Usuario:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(168, 97)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(15, 13)
        Me.Label2.TabIndex = 129
        Me.Label2.Text = "%"
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(98, 131)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 130
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.BackgroundColor = System.Drawing.Color.White
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.GridColor = System.Drawing.Color.White
        Me.dgv.Location = New System.Drawing.Point(98, 160)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.Size = New System.Drawing.Size(395, 224)
        Me.dgv.TabIndex = 131
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = Nothing
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = "Nombre"
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = "ID"
        Me.cbxUsuario.DataSource = ""
        Me.cbxUsuario.DataValueMember = "ID"
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(98, 18)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(237, 21)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 128
        Me.cbxUsuario.Texto = ""
        '
        'cbxListaPrecio
        '
        Me.cbxListaPrecio.CampoWhere = Nothing
        Me.cbxListaPrecio.CargarUnaSolaVez = False
        Me.cbxListaPrecio.DataDisplayMember = "Lista"
        Me.cbxListaPrecio.DataFilter = Nothing
        Me.cbxListaPrecio.DataOrderBy = "ID"
        Me.cbxListaPrecio.DataSource = ""
        Me.cbxListaPrecio.DataValueMember = "ID"
        Me.cbxListaPrecio.FormABM = Nothing
        Me.cbxListaPrecio.Indicaciones = Nothing
        Me.cbxListaPrecio.Location = New System.Drawing.Point(98, 55)
        Me.cbxListaPrecio.Name = "cbxListaPrecio"
        Me.cbxListaPrecio.SeleccionObligatoria = False
        Me.cbxListaPrecio.Size = New System.Drawing.Size(237, 21)
        Me.cbxListaPrecio.SoloLectura = False
        Me.cbxListaPrecio.TabIndex = 126
        Me.cbxListaPrecio.Texto = ""
        '
        'txtPorcentaje
        '
        Me.txtPorcentaje.Color = System.Drawing.Color.Empty
        Me.txtPorcentaje.Decimales = True
        Me.txtPorcentaje.Indicaciones = Nothing
        Me.txtPorcentaje.Location = New System.Drawing.Point(98, 92)
        Me.txtPorcentaje.Name = "txtPorcentaje"
        Me.txtPorcentaje.Size = New System.Drawing.Size(63, 22)
        Me.txtPorcentaje.SoloLectura = False
        Me.txtPorcentaje.TabIndex = 101
        Me.txtPorcentaje.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentaje.Texto = "0"
        '
        'frmAsignarLimiteDescuento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(505, 457)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbxUsuario)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbxListaPrecio)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.txtPorcentaje)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.lblPorcentaje)
        Me.Name = "frmAsignarLimiteDescuento"
        Me.Text = "frmAsignarLimiteDescuento"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtPorcentaje As ERP.ocxTXTNumeric
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents lblPorcentaje As System.Windows.Forms.Label
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents cbxUsuario As ERP.ocxCBX
    Friend WithEvents cbxListaPrecio As ERP.ocxCBX
End Class

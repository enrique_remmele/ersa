﻿Imports System.Threading
Public Class frmSolicitudExcepcion

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim PorcentajeAsignado As Decimal = 0
    Dim ImporteAsignado As Decimal = 0

    'PROPIEDADES

    'VARIABLES
    Dim tProcesar As Thread
    Dim vProcesar As Boolean = False
    Dim dtPedidos As DataTable
    Dim dtAprobador As DataTable
    Dim dtSolicitante As DataTable
    Dim dtPorcentajes As DataTable
    Dim Listando As Boolean = False

    Enum ENUMOperacionPedido
        APROBAR = 1
        RECHAZAR = 2
        RECUPERAR = 3
        ELIMINAR = 4
    End Enum


    'FUNCIONES
    Sub Inicializar()

        Me.KeyPreview = True

        'Varios
        CheckForIllegalCrossThreadCalls = False

        cbxGrupos.SelectedIndex = 0
        cbxOrden.SelectedIndex = 0

        'Funciones
        ListarGrupos()

        'Fecha facturacion
        If IsDate(VGFechaFacturacion) = False Then
            VGFechaFacturacion = VGFechaHoraSistema
        End If

        txtFechaDesde.Text = Today
        txtFechaHasta.Text = Today.AddDays(7)


        CargarPorcentaje()
        ListarPedidos()
    End Sub

    Sub CargarInformacion()

    End Sub

    Sub GuardarInformacion()

    End Sub
    Sub ListarGrupos()

        lvGrupo.Items.Clear()
        lvPedidos.Items.Clear()

        Dim SQL As String = ""
        Dim INDEX As Integer = cbxGrupos.SelectedIndex

        Dim dtGrupo As New DataTable("Grupos")

        dtGrupo.Columns.Add("Grupo", Type.GetType("System.String"))

        Dim newRow As DataRow = dtGrupo.NewRow()
        newRow(0) = "PENDIENTES"
        dtGrupo.Rows.Add(newRow)

        Dim newRowc As DataRow = dtGrupo.NewRow()
        newRowc(0) = "APROBADOS"
        dtGrupo.Rows.Add(newRowc)

        Dim newRowd As DataRow = dtGrupo.NewRow()
        newRowd(0) = "RECHAZADOS"
        dtGrupo.Rows.Add(newRowd)

        CSistema.dtToLv(lvGrupo, dtGrupo)


        For i = 0 To lvGrupo.Columns.Count - 1
            lvGrupo.Columns(i).AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize)
        Next



    End Sub

    Sub SeleccionarGrupo()

        ListarPedidos()

    End Sub

    Sub ListarPedidos()

        If lvGrupo.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim OrderBy As String = ""
        Dim Campo As String = ""


        If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        End If
        Select Case lvGrupo.SelectedItems.Item(0).Text
            Case "PENDIENTES"
                SQL = "Select * From vSolicitudProductoListaPrecioExcepciones Where Estado = 'PENDIENTE'"
                OrderBy = " Order By " & cbxOrden.Text
                btnAprobar.Enabled = True
                btnRechazar.Enabled = True
                btnEliminar.Enabled = True
                btnAprobar.Text = "Aprobar"
                btnRechazar.Text = "Rechazar"
                txtFechaDesde.Visible = False
                txtFechaHasta.Visible = False

            Case "APROBADOS"
                SQL = "Select * From vSolicitudProductoListaPrecioExcepciones Where Estado = 'APROBADO' and cast(FechaAprobado as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.Text, True, False) & "'"
                OrderBy = " Order By " & cbxOrden.Text
                btnAprobar.Enabled = True
                btnRechazar.Enabled = False
                btnEliminar.Enabled = False
                btnAprobar.Text = "Recuperar"
                txtFechaDesde.Visible = True
                txtFechaHasta.Visible = True


            Case "RECHAZADOS"
                SQL = "Select * From vSolicitudProductoListaPrecioExcepciones Where Estado = 'RECHAZADO' and cast(FechaAprobado as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.Text, True, False) & "'"
                OrderBy = " Order By " & cbxOrden.Text
                btnAprobar.Enabled = True
                btnRechazar.Enabled = False
                btnEliminar.Enabled = False
                btnAprobar.Text = "Recuperar"
                txtFechaDesde.Visible = True
                txtFechaHasta.Visible = True

        End Select

        If Listando Then
            Exit Sub
        End If
        Listar(SQL, OrderBy)
        'Filtrar()

    End Sub

    Sub Listar(ByVal SQL As String, Optional ByVal OrderBy As String = "")

        'CSistema.SqlToLv(lvPedidos, SQL)
        Listando = True
        dtPedidos = CSistema.ExecuteToDataTable(SQL & OrderBy)
        CSistema.dtToLv(lvPedidos, dtPedidos)
        Dim dtAprobado As DataTable = CSistema.ExecuteToDataTable(SQL.Replace("*", " Distinct IDUsuarioAprobado, [Aprobado/Rechazado]"))
        Dim dtSolicitante As DataTable = CSistema.ExecuteToDataTable(SQL.Replace("*", " Distinct IDUsuarioSolicitud, Solicitante"))
        Dim dtCliente As DataTable = CSistema.ExecuteToDataTable(SQL.Replace("*", " Distinct IDCliente, Cliente"))
        Dim dtProducto As DataTable = CSistema.ExecuteToDataTable(SQL.Replace("*", " Distinct IDProducto, Producto"))

        dtAprobado.Rows.Add("0", "TODOS")
        dtAprobado.DefaultView.Sort = "IDUsuarioAprobado"
        CSistema.SqlToComboBox(cbxAprobador, dtAprobado)
        cbxAprobador.Text = "TODOS"

        dtSolicitante.Rows.Add("0", "TODOS")
        dtSolicitante.DefaultView.Sort = "IDUsuarioSolicitud"
        CSistema.SqlToComboBox(cbxSolicitante, dtSolicitante)
        cbxSolicitante.Text = "TODOS"

        dtCliente.Rows.Add("0", "TODOS")
        dtCliente.DefaultView.Sort = "IDCliente"
        CSistema.SqlToComboBox(cbxCliente, dtCliente)
        cbxCliente.Text = "TODOS"

        dtProducto.Rows.Add("0", "TODOS")
        dtProducto.DefaultView.Sort = "IDProducto"
        CSistema.SqlToComboBox(cbxProducto, dtProducto)
        cbxProducto.Text = "TODOS"

        txtPorcentajeDesde.SetValue(0)
        txtPorcentajeHasta.SetValue(99)


        lvPedidos.Columns(1).Width = 10
        lvPedidos.Columns(3).Width = 0
        lvPedidos.Columns(5).Width = 0
        lvPedidos.Columns(8).Width = 0
        lvPedidos.Columns(10).Width = 0




        Listando = False
    End Sub

    Sub Detener()
        Try
            If vProcesar = False Then
                Exit Sub
            End If

            tProcesar.Abort()
            vProcesar = False
        Catch ex As Exception

        End Try

        Thread.Sleep(1000)

    End Sub

    Sub AprobarRechazarPedido(ByVal Operacion As ENUMOperacionPedido)

        lvGrupo.Enabled = False
        lvPedidos.Enabled = False

        'Si no esta seleccionado ningun grupo, seleccionamos el primero
        If lvGrupo.Items.Count = 0 Then
            GoTo terminar
        End If

        If lvGrupo.SelectedItems.Count = 0 Then
            'GoTo terminar
        End If

        For Each item As ListViewItem In lvPedidos.Items
            'For Each item As ListViewItem In lvPedidos.CheckedItems
            If item.Checked = False Then
                GoTo siguiente
            End If

            item.ImageIndex = 1

            'Dim ID As Integer = item.Text
            Dim IDProducto As Integer = item.SubItems(1).Text
            Dim IDCliente As Integer = item.SubItems(5).Text
            Dim IDTipoDescuento As Integer = item.SubItems(8).Text
            Dim IDListaPrecio As Integer = item.SubItems(3).Text
            Dim Desde As Date = item.SubItems(17).Text
            Dim Hasta As Date = item.SubItems(18).Text
            Dim Porcentaje As Decimal = item.SubItems(16).Text
            Dim Descuento As Decimal = item.SubItems(13).Text
            'Dim Comprobante As String = item.SubItems(2).Text

            'Actualizar el item
            If ControlarPorcentaje(Porcentaje, Descuento) = True Then
                Select Case Operacion
                    Case ENUMOperacionPedido.APROBAR
                        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionExcepcion @IDProducto = " & IDProducto & ", @IDCliente = " & IDCliente & ", @IDTipoDescuento = " & IDTipoDescuento & ", @IDListaPrecio = " & IDListaPrecio & ", @Desde = '" & CSistema.FormatoFechaBaseDatos(Desde, True, False) & "', @Hasta = '" & CSistema.FormatoFechaBaseDatos(Hasta, True, False) & "', @valor='APROBAR', @IDUsuario = " & vgIDUsuario & ", @IDTerminal = " & vgIDTerminal & "").Copy
                    Case ENUMOperacionPedido.RECHAZAR
                        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionExcepcion @IDProducto = " & IDProducto & ", @IDCliente = " & IDCliente & ", @IDTipoDescuento = " & IDTipoDescuento & ", @IDListaPrecio = " & IDListaPrecio & ", @Desde = '" & CSistema.FormatoFechaBaseDatos(Desde, True, False) & "', @Hasta = '" & CSistema.FormatoFechaBaseDatos(Hasta, True, False) & "', @valor='RECHAZAR', @IDUsuario = " & vgIDUsuario & ", @IDTerminal = " & vgIDTerminal & "").Copy
                    Case ENUMOperacionPedido.RECUPERAR
                        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionExcepcion @IDProducto = " & IDProducto & ", @IDCliente = " & IDCliente & ", @IDTipoDescuento = " & IDTipoDescuento & ", @IDListaPrecio = " & IDListaPrecio & ", @Desde = '" & CSistema.FormatoFechaBaseDatos(Desde, True, False) & "', @Hasta = '" & CSistema.FormatoFechaBaseDatos(Hasta, True, False) & "', @valor='RECUPERAR', @IDUsuario = " & vgIDUsuario & ", @IDTerminal = " & vgIDTerminal & "").Copy
                End Select

            End If

siguiente:

        Next

terminar:
        ListarPedidos()
        lvGrupo.Enabled = True
        lvPedidos.Enabled = True
        vProcesar = False

    End Sub

    Sub CargarSolicitud()

        Dim frm As New frmCargarSolicitud
        frm.ShowDialog()
        ListarPedidos()

    End Sub

    Sub CargarPorcentaje()
        PorcentajeAsignado = CType(CSistema.ExecuteScalar("Select IsNull(porcentaje,0) from PorcentajeExcepcion where IDUsuario=" & vgIDUsuario), Decimal)
        ImporteAsignado = CType(CSistema.ExecuteScalar("Select IsNull(Importe,0) from PorcentajeExcepcion where IDUsuario=" & vgIDUsuario), Decimal)
    End Sub

    Sub Exportar()
        CSistema.LvToExcel(lvPedidos)
    End Sub

    Sub Filtrar()

        Dim Filtro As Boolean = False
        Dim dtAux As DataTable = dtPedidos.Clone
        Dim dtAux1 As DataTable = dtPedidos.Clone


        If cbxAprobador.Text <> "TODOS" And cbxAprobador.Text <> "" Then
            For Each oRow As DataRow In dtPedidos.Select("[Aprobado/Rechazado] = '" & cbxAprobador.Text & "'")
                dtAux.ImportRow(oRow)
            Next
            Filtro = True
        End If

        If cbxSolicitante.Text <> "TODOS" And cbxSolicitante.Text <> "" Then
            If Filtro Then
                For Each oRow As DataRow In dtAux.Select("Solicitante = '" & cbxSolicitante.Text & "'")
                    dtAux1.ImportRow(oRow)
                Next
                dtAux = dtAux1.Copy()
                dtAux1.Clear()
            Else

                For Each oRow As DataRow In dtPedidos.Select("Solicitante = '" & cbxSolicitante.Text & "'")
                    dtAux.ImportRow(oRow)
                Next
            End If

            Filtro = True
        End If

        If cbxCliente.Text <> "TODOS" And cbxCliente.Text <> "" Then
            If Filtro Then
                For Each oRow As DataRow In dtAux.Select("Cliente = '" & cbxCliente.Text & "'")
                    dtAux1.ImportRow(oRow)
                Next
                dtAux = dtAux1.Copy()
                dtAux1.Clear()
            Else

                For Each oRow As DataRow In dtPedidos.Select("Cliente = '" & cbxCliente.Text & "'")
                    dtAux.ImportRow(oRow)
                Next
            End If

            Filtro = True
        End If

        If cbxProducto.Text <> "TODOS" And cbxProducto.Text <> "" Then
            If Filtro Then
                For Each oRow As DataRow In dtAux.Select("Producto = '" & cbxProducto.Text & "'")
                    dtAux1.ImportRow(oRow)
                Next
                dtAux = dtAux1.Copy()
                dtAux1.Clear()
            Else
                For Each oRow As DataRow In dtPedidos.Select("Producto = '" & cbxProducto.Text & "'")
                    dtAux.ImportRow(oRow)
                Next

            End If

            Filtro = True
        End If

        If txtPorcentajeDesde.ObtenerValor.ToString <> "0" Or txtPorcentajeHasta.ObtenerValor.ToString <> "99" Then

            If txtPorcentajeDesde.ObtenerValor.ToString <> "" And txtPorcentajeHasta.ObtenerValor.ToString <> "" Then
                If Filtro Then
                    For Each oRow As DataRow In dtAux.Select("Porcentaje > " & txtPorcentajeDesde.ObtenerValor.ToString & " and Porcentaje < '" & txtPorcentajeHasta.ObtenerValor.ToString & "'")
                        dtAux1.ImportRow(oRow)
                    Next
                    dtAux = dtAux1.Copy()
                    dtAux1.Clear()
                Else

                    For Each oRow As DataRow In dtPedidos.Select("Porcentaje > " & txtPorcentajeDesde.ObtenerValor.ToString & " and Porcentaje < '" & txtPorcentajeHasta.ObtenerValor.ToString & "'")
                        dtAux.ImportRow(oRow)
                    Next
                End If

                Filtro = True
            End If
        End If

        If Filtro = True Then
            CSistema.dtToLv(lvPedidos, dtAux)
            txtCantidadPedidos.txt.Text = dtAux.Rows.Count
        Else
            If dtAux.Rows.Count <> dtPedidos.Rows.Count And dtAux.Rows.Count > 0 Then
                CSistema.dtToLv(lvPedidos, dtPedidos)
            End If
            txtCantidadPedidos.txt.Text = dtPedidos.Rows.Count
        End If

        lvPedidos.Columns(1).Width = 10
        lvPedidos.Columns(3).Width = 0
        lvPedidos.Columns(5).Width = 0
        lvPedidos.Columns(8).Width = 0
        lvPedidos.Columns(10).Width = 0

    End Sub

    Sub ELiminarSolicitud()
        lvGrupo.Enabled = False
        lvPedidos.Enabled = False

        'Si no esta seleccionado ningun grupo, seleccionamos el primero
        If lvGrupo.Items.Count = 0 Then
            GoTo terminar
        End If

        If lvGrupo.SelectedItems.Count = 0 Then
            'GoTo terminar
        End If

        For Each item As ListViewItem In lvPedidos.Items
            'For Each item As ListViewItem In lvPedidos.CheckedItems
            If item.Checked = False Then
                GoTo siguiente
            End If

            item.ImageIndex = 1

            'Dim ID As Integer = item.Text
            Dim IDProducto As Integer = item.SubItems(1).Text
            Dim IDCliente As Integer = item.SubItems(5).Text
            Dim IDTipoDescuento As Integer = item.SubItems(8).Text
            Dim IDListaPrecio As Integer = item.SubItems(3).Text
            Dim Desde As Date = item.SubItems(17).Text
            Dim Hasta As Date = item.SubItems(18).Text
            Dim Porcentaje As Decimal = item.SubItems(16).Text
            Dim Descuento As Decimal = item.SubItems(13).Text

            Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionExcepcion @IDProducto = " & IDProducto & ", @IDCliente = " & IDCliente & ", @IDTipoDescuento = " & IDTipoDescuento & ", @IDListaPrecio = " & IDListaPrecio & ", @Desde = '" & CSistema.FormatoFechaBaseDatos(Desde, True, False) & "', @Hasta = '" & CSistema.FormatoFechaBaseDatos(Hasta, True, False) & "', @valor='ELIMINAR', @IDUsuario = " & vgIDUsuario & "").Copy


siguiente:

        Next

terminar:
        ListarPedidos()
        lvGrupo.Enabled = True
        lvPedidos.Enabled = True
        vProcesar = False






    End Sub


    Function ControlarPorcentaje(ByVal Porcentaje As Decimal, ByVal Importe As Decimal) As Boolean
        ControlarPorcentaje = True
        If PorcentajeAsignado = 0 And ImporteAsignado = 0 Then
            Return False
        End If
        If (Porcentaje > PorcentajeAsignado) Or (Importe > ImporteAsignado) Then
            Return False
        End If
    End Function

    Sub SeleccionarTodo()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = True
        Next
    End Sub

    Sub QuitarSeleccion()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = False
        Next
    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        Select Case e.KeyCode
            Case Keys.Enter
                CSistema.SelectNextControl(Me, e.KeyCode)
        End Select

    End Sub

    Sub EliminarPedido()

        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        lvPedidos.SelectedItems(0).Remove()

    End Sub

    Private Sub frmAprobarPedido_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Detener()
        GuardarInformacion()
        'FA 20/06/2023
        LiberarMemoria()
    End Sub

    Private Sub frmAprobarPedido_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        ManejarTecla(e)
    End Sub

    Private Sub frmAprobarPedido_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnActualizarGrupo_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarGrupo.Click
        ListarGrupos()
    End Sub

    Private Sub cbxGrupos_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxGrupos.SelectedIndexChanged
        ListarGrupos()
    End Sub

    Private Sub lvGrupo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lvGrupo.SelectedIndexChanged
        SeleccionarGrupo()
    End Sub

    Private Sub btnProcesar_Click(sender As System.Object, e As System.EventArgs) Handles btnAprobar.Click
        If lvGrupo.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Select Case lvGrupo.SelectedItems.Item(0).Text
            Case "RECHAZADOS"
                AprobarRechazarPedido(ENUMOperacionPedido.RECUPERAR)
            Case "APROBADOS"
                AprobarRechazarPedido(ENUMOperacionPedido.RECUPERAR)
            Case Else
                AprobarRechazarPedido(ENUMOperacionPedido.APROBAR)
        End Select

    End Sub

    Private Sub btnSeleccionarTodos_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionarTodos.Click
        SeleccionarTodo()
    End Sub

    Private Sub ToolStripButton1_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton1.Click
        QuitarSeleccion()
    End Sub

    Private Sub btnActualizarPedidos_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarPedidos.Click
        ListarPedidos()
    End Sub

    Private Sub EliminarDeLaListaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        EliminarPedido()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnV_Click(sender As System.Object, e As System.EventArgs) Handles btnCargarSolicitud.Click
        CargarSolicitud()
    End Sub

    Private Sub ToolStripButton2_Click(sender As System.Object, e As System.EventArgs) Handles btnRechazar.Click
        'Select Case lvGrupo.SelectedItems.Item(0).Text
        '    Case "PENDIENTE"
        '        AprobarRechazarPedido(ENUMOperacionPedido.RECHAZAR)
        '    Case "RECHAZADO"
        '        AprobarRechazarPedido(ENUMOperacionPedido.ANULAR)
        'End Select

        AprobarRechazarPedido(ENUMOperacionPedido.RECHAZAR)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Exportar()
    End Sub

    Private Sub cbxAprobador_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxAprobador.SelectedValueChanged
        Filtrar()
    End Sub

    Private Sub cbxSolicitante_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSolicitante.SelectedValueChanged
        Filtrar()
    End Sub

    Private Sub cbxCliente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxCliente.SelectedValueChanged
        Filtrar()
    End Sub

    Private Sub cbxProducto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxProducto.SelectedValueChanged
        Filtrar()
    End Sub

    Private Sub txtPorcentajeDesde_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtPorcentajeDesde.TeclaPrecionada
        Filtrar()
    End Sub

    Private Sub txtPorcentajeHasta_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtPorcentajeHasta.TeclaPrecionada
        Filtrar()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If MessageBox.Show("Esto eliminara la solicitud permanentemente, Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.No Then
            Exit Sub
        End If
        EliminarSolicitud()

    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmSolicitudExcepcion_Activate()
        Me.Refresh()
    End Sub
End Class
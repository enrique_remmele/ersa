﻿Public Class frmCargarSolicitudProductoPrecio
    Dim CSistema As New CSistema

    Dim dt As New DataTable
    Dim dtRetorno As New DataTable
    Dim SQL As String

    Sub Inicializar()

        SQL = "Select 'Producto'='41050', 'Cliente'='01SST', 'Tipo'='FIJO/UNICO', 'PrecioFinal' = 130000 , 'Desde' ='01/12/1900','Hasta' ='01/12/1900', 'CantidadMinimaPrecioUnico'=1000"
        dt = CSistema.ExecuteToDataTable(SQL & ", 'Mensaje' = ''").Clone()

        dgv.DataSource = dt

    End Sub

    Sub GenerarArchivoVacio()

        Dim txt As New SaveFileDialog
        txt.Filter = "Archivo de texto separado por tabulacion | *.txt"
        If txt.ShowDialog <> Windows.Forms.DialogResult.OK Then
            Exit Sub
        End If


        dt = CSistema.ExecuteToDataTable(SQL)

        If dt Is Nothing Then
            Exit Sub
        End If

        If CSistema.DataTableToTXT(dt, txt.FileName) = False Then
            Exit Sub
        End If

    End Sub

    Sub CargarArchivo()

        Dim txt As New OpenFileDialog
        txt.Filter = "Archivo de texto separado por tabulacion | *.txt"
        If txt.ShowDialog <> Windows.Forms.DialogResult.OK Then
            Exit Sub
        End If

        dt = CSistema.ExecuteToDataTableTXT(txt.FileName)


        dtRetorno = dt.Clone()
        dtRetorno.Columns.Add("Mensaje", GetType(String))

        If ValidarArchivo() = False Then
            Exit Sub
        End If

        For Each oRow As DataRow In dt.Rows

            Dim param(-1) As SqlClient.SqlParameter

            'Dim IndiceOperacion As Integer
            ''Si fue seleccionado una sucursal del cliente
            'CSistema.SetSQLParameter(param, "@Producto", oRow("Producto").ToString.Trim, ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@ListaPrecio", oRow("ListaPrecio").ToString.Trim, ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@Cliente", oRow("Cliente").ToString.Trim.Replace("�", "Ñ"), ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@TipoDescuento", oRow("TipoDescuento").ToString.Trim, ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@Descuento", oRow("Descuento").ToString.Replace(".", "").Trim, ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@Moneda", oRow("Moneda").ToString.Trim, ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@Desde", CSistema.FormatoFechaBaseDatos(oRow("Desde").ToString, True, False), ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@Hasta", CSistema.FormatoFechaBaseDatos(oRow("Hasta").ToString, True, False), ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@CantidadLimite", oRow("CantidadLimite").ToString.Replace(".", "").Trim, ParameterDirection.Input)


            CSistema.SetSQLParameter(param, "@Producto", oRow("Producto").ToString.Trim, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cliente", oRow("Cliente").ToString.Trim.Replace("�", "Ñ"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Moneda", "GS", ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TipoPrecio", oRow("Tipo").ToString.Trim, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Precio", oRow("PrecioFinal").ToString.Replace(".", "").Trim, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Desde", CSistema.FormatoFechaBaseDatos(oRow("Desde").ToString, True, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Hasta", CSistema.FormatoFechaBaseDatos(oRow("Hasta").ToString, True, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@CantidadMinimaPrecioUnico", oRow("CantidadMinimaPrecioUnico").ToString.Replace(".", "").Trim, ParameterDirection.Input)

            'Transaccion
            CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpSolicitudProductoPrecio", False, False, MensajeRetorno) = False Then
                '    dtRetorno.Rows.Add(oRow("Producto").ToString, oRow("Cliente").ToString, oRow("ListaPrecio").ToString, oRow("Moneda").ToString, oRow("TipoDescuento").ToString, oRow("Descuento").ToString, oRow("Desde").ToString, oRow("Hasta").ToString, oRow("CantidadLimite").ToString, MensajeRetorno)
                '    CSistema.dtToGrid(dgv, dtRetorno)
                'Else
                '    MessageBox.Show(MensajeRetorno)
            End If

            dtRetorno.Rows.Add(oRow("Producto").ToString, oRow("Cliente").ToString, oRow("Tipo").ToString, oRow("PrecioFinal").ToString, oRow("Desde").ToString, oRow("Hasta").ToString, oRow("CantidadMinimaPrecioUnico").ToString, MensajeRetorno)
            CSistema.dtToGrid(dgv, dtRetorno)

        Next


    End Sub

    Function ValidarArchivo() As Boolean

        ValidarArchivo = False

        If dt Is Nothing Then
            Return False
        End If

        'Obtener muestra
        Dim vdt As DataTable = CSistema.ExecuteToDataTable("Select 'Producto'='41050', 'Cliente'='01SST', 'Tipo'='FIJO/UNICO', 'PrecioFinal' = 130000 , 'Desde' ='01/12/1900','Hasta' ='01/12/1900', 'CantidadMinimaPrecioUnico'=1000")
        Dim NoEncontrado As Boolean = True

        'Controlar las columnas
        For c1 As Integer = 0 To dt.Columns.Count - 1
            NoEncontrado = True
            For c2 As Integer = 0 To vdt.Columns.Count - 1
                If dt.Columns(c1).ColumnName = vdt.Columns(c2).ColumnName Then
                    NoEncontrado = False
                    GoTo seguir
                End If
            Next

seguir:
            If NoEncontrado = True Then
                MessageBox.Show("El formato del archivo no es correcto! No fue encontrado la columna: " & dt.Columns(c1).ColumnName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Return False
            End If
        Next

        Return True

    End Function

    Sub Exportar()
        If dt Is Nothing Then
            Exit Sub
        End If
        CSistema.dtToExcel2(dtRetorno, "nombre")

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnGenerar.Click
        GenerarArchivoVacio()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnCargarArchivo.Click
        CargarArchivo()
    End Sub

    Private Sub frmCargarSolicitud_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles btnExportarExcel.Click
        Exportar()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmCargarSolicitudProductoPrecio_Activate()
        Me.Refresh()
    End Sub
End Class
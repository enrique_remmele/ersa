﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDescuentos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ALOE")
        Dim TreeNode2 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("400", New System.Windows.Forms.TreeNode() {TreeNode1})
        Dim TreeNode3 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ALOE")
        Dim TreeNode4 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("200", New System.Windows.Forms.TreeNode() {TreeNode3})
        Dim TreeNode5 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ALOE")
        Dim TreeNode6 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("1000", New System.Windows.Forms.TreeNode() {TreeNode5})
        Dim TreeNode7 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("PRESENTACION", New System.Windows.Forms.TreeNode() {TreeNode2, TreeNode4, TreeNode6})
        Dim TreeNode8 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("OMO", New System.Windows.Forms.TreeNode() {TreeNode7})
        Dim TreeNode9 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("KUÑATAI")
        Dim TreeNode10 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("BRILLANTE")
        Dim TreeNode11 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("MARCA", New System.Windows.Forms.TreeNode() {TreeNode8, TreeNode9, TreeNode10})
        Dim TreeNode12 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("HC", New System.Windows.Forms.TreeNode() {TreeNode11})
        Dim TreeNode13 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ALOE")
        Dim TreeNode14 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("400", New System.Windows.Forms.TreeNode() {TreeNode13})
        Dim TreeNode15 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ALOE")
        Dim TreeNode16 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("200", New System.Windows.Forms.TreeNode() {TreeNode15})
        Dim TreeNode17 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ALOE")
        Dim TreeNode18 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("1000", New System.Windows.Forms.TreeNode() {TreeNode17})
        Dim TreeNode19 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("PRESENTACION", New System.Windows.Forms.TreeNode() {TreeNode14, TreeNode16, TreeNode18})
        Dim TreeNode20 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("OMO", New System.Windows.Forms.TreeNode() {TreeNode19})
        Dim TreeNode21 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("KUÑATAI")
        Dim TreeNode22 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("BRILLANTE")
        Dim TreeNode23 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("MARCA", New System.Windows.Forms.TreeNode() {TreeNode20, TreeNode21, TreeNode22})
        Dim TreeNode24 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("HC", New System.Windows.Forms.TreeNode() {TreeNode23})
        Me.lblID = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.OcxTXTDate1 = New ERP.ocxTXTDate()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.OcxTXTDate2 = New ERP.ocxTXTDate()
        Me.TreeView2 = New System.Windows.Forms.TreeView()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.OcxTXTNumeric1 = New ERP.ocxTXTNumeric()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.OcxTXTNumeric2 = New ERP.ocxTXTNumeric()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.OcxTXTNumeric3 = New ERP.ocxTXTNumeric()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.OcxTXTNumeric4 = New ERP.ocxTXTNumeric()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(10, 18)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 2
        Me.lblID.Text = "ID:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(96, 12)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 3
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(96, 40)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(184, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 17
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(10, 45)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(54, 13)
        Me.lblDescripcion.TabIndex = 16
        Me.lblDescripcion.Text = "Actividad:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 138)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Principal"
        '
        'TreeView1
        '
        Me.TreeView1.Location = New System.Drawing.Point(96, 138)
        Me.TreeView1.Name = "TreeView1"
        TreeNode1.Name = "Nodo11"
        TreeNode1.Text = "ALOE"
        TreeNode2.Name = "Nodo8"
        TreeNode2.Text = "400"
        TreeNode3.Name = "Nodo12"
        TreeNode3.Text = "ALOE"
        TreeNode4.Name = "Nodo7"
        TreeNode4.Text = "200"
        TreeNode5.Name = "Nodo13"
        TreeNode5.Text = "ALOE"
        TreeNode6.Name = "Nodo10"
        TreeNode6.Text = "1000"
        TreeNode7.Name = "Nodo9"
        TreeNode7.Text = "PRESENTACION"
        TreeNode8.Name = "Nodo2"
        TreeNode8.Text = "OMO"
        TreeNode9.Name = "Nodo5"
        TreeNode9.Text = "KUÑATAI"
        TreeNode10.Name = "Nodo6"
        TreeNode10.Text = "BRILLANTE"
        TreeNode11.Name = "Nodo1"
        TreeNode11.Text = "MARCA"
        TreeNode12.Name = "Nodo0"
        TreeNode12.Text = "HC"
        Me.TreeView1.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode12})
        Me.TreeView1.Size = New System.Drawing.Size(289, 97)
        Me.TreeView1.TabIndex = 19
        '
        'OcxTXTDate1
        '
        Me.OcxTXTDate1.Color = System.Drawing.Color.Empty
        Me.OcxTXTDate1.Fecha = New Date(2012, 11, 13, 14, 37, 42, 109)
        Me.OcxTXTDate1.Location = New System.Drawing.Point(96, 67)
        Me.OcxTXTDate1.Name = "OcxTXTDate1"
        Me.OcxTXTDate1.PermitirNulo = False
        Me.OcxTXTDate1.Size = New System.Drawing.Size(74, 20)
        Me.OcxTXTDate1.SoloLectura = False
        Me.OcxTXTDate1.TabIndex = 22
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 71)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 13)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "Inicio:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(176, 71)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(24, 13)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "Fin:"
        '
        'OcxTXTDate2
        '
        Me.OcxTXTDate2.Color = System.Drawing.Color.Empty
        Me.OcxTXTDate2.Fecha = New Date(2012, 11, 13, 14, 37, 42, 109)
        Me.OcxTXTDate2.Location = New System.Drawing.Point(206, 67)
        Me.OcxTXTDate2.Name = "OcxTXTDate2"
        Me.OcxTXTDate2.PermitirNulo = False
        Me.OcxTXTDate2.Size = New System.Drawing.Size(74, 20)
        Me.OcxTXTDate2.SoloLectura = False
        Me.OcxTXTDate2.TabIndex = 24
        '
        'TreeView2
        '
        Me.TreeView2.Location = New System.Drawing.Point(488, 138)
        Me.TreeView2.Name = "TreeView2"
        TreeNode13.Name = "Nodo11"
        TreeNode13.Text = "ALOE"
        TreeNode14.Name = "Nodo8"
        TreeNode14.Text = "400"
        TreeNode15.Name = "Nodo12"
        TreeNode15.Text = "ALOE"
        TreeNode16.Name = "Nodo7"
        TreeNode16.Text = "200"
        TreeNode17.Name = "Nodo13"
        TreeNode17.Text = "ALOE"
        TreeNode18.Name = "Nodo10"
        TreeNode18.Text = "1000"
        TreeNode19.Name = "Nodo9"
        TreeNode19.Text = "PRESENTACION"
        TreeNode20.Name = "Nodo2"
        TreeNode20.Text = "OMO"
        TreeNode21.Name = "Nodo5"
        TreeNode21.Text = "KUÑATAI"
        TreeNode22.Name = "Nodo6"
        TreeNode22.Text = "BRILLANTE"
        TreeNode23.Name = "Nodo1"
        TreeNode23.Text = "MARCA"
        TreeNode24.Name = "Nodo0"
        TreeNode24.Text = "HC"
        Me.TreeView2.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode24})
        Me.TreeView2.Size = New System.Drawing.Size(289, 97)
        Me.TreeView2.TabIndex = 27
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(418, 138)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 13)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Secundario:"
        '
        'OcxTXTNumeric1
        '
        Me.OcxTXTNumeric1.Color = System.Drawing.Color.Empty
        Me.OcxTXTNumeric1.Decimales = True
        Me.OcxTXTNumeric1.Enabled = False
        Me.OcxTXTNumeric1.Indicaciones = Nothing
        Me.OcxTXTNumeric1.Location = New System.Drawing.Point(96, 241)
        Me.OcxTXTNumeric1.Name = "OcxTXTNumeric1"
        Me.OcxTXTNumeric1.Size = New System.Drawing.Size(39, 22)
        Me.OcxTXTNumeric1.SoloLectura = False
        Me.OcxTXTNumeric1.TabIndex = 21
        Me.OcxTXTNumeric1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.OcxTXTNumeric1.Texto = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 246)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 13)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Cantidad:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(418, 247)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 13)
        Me.Label6.TabIndex = 28
        Me.Label6.Text = "Cantidad:"
        '
        'OcxTXTNumeric2
        '
        Me.OcxTXTNumeric2.Color = System.Drawing.Color.Empty
        Me.OcxTXTNumeric2.Decimales = True
        Me.OcxTXTNumeric2.Enabled = False
        Me.OcxTXTNumeric2.Indicaciones = Nothing
        Me.OcxTXTNumeric2.Location = New System.Drawing.Point(488, 241)
        Me.OcxTXTNumeric2.Name = "OcxTXTNumeric2"
        Me.OcxTXTNumeric2.Size = New System.Drawing.Size(63, 22)
        Me.OcxTXTNumeric2.SoloLectura = False
        Me.OcxTXTNumeric2.TabIndex = 29
        Me.OcxTXTNumeric2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.OcxTXTNumeric2.Texto = "0"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(418, 45)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(57, 13)
        Me.Label7.TabIndex = 30
        Me.Label7.Text = "Mecanica:"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(488, 45)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(289, 42)
        Me.TextBox1.TabIndex = 31
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(162, 246)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(62, 13)
        Me.Label8.TabIndex = 32
        Me.Label8.Text = "Descuento:"
        '
        'OcxTXTNumeric3
        '
        Me.OcxTXTNumeric3.Color = System.Drawing.Color.Empty
        Me.OcxTXTNumeric3.Decimales = True
        Me.OcxTXTNumeric3.Enabled = False
        Me.OcxTXTNumeric3.Indicaciones = Nothing
        Me.OcxTXTNumeric3.Location = New System.Drawing.Point(224, 241)
        Me.OcxTXTNumeric3.Name = "OcxTXTNumeric3"
        Me.OcxTXTNumeric3.Size = New System.Drawing.Size(24, 22)
        Me.OcxTXTNumeric3.SoloLectura = False
        Me.OcxTXTNumeric3.TabIndex = 33
        Me.OcxTXTNumeric3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.OcxTXTNumeric3.Texto = "0"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(141, 245)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox1.TabIndex = 34
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'OcxTXTNumeric4
        '
        Me.OcxTXTNumeric4.Color = System.Drawing.Color.Empty
        Me.OcxTXTNumeric4.Decimales = True
        Me.OcxTXTNumeric4.Enabled = False
        Me.OcxTXTNumeric4.Indicaciones = Nothing
        Me.OcxTXTNumeric4.Location = New System.Drawing.Point(305, 241)
        Me.OcxTXTNumeric4.Name = "OcxTXTNumeric4"
        Me.OcxTXTNumeric4.Size = New System.Drawing.Size(80, 22)
        Me.OcxTXTNumeric4.SoloLectura = False
        Me.OcxTXTNumeric4.TabIndex = 36
        Me.OcxTXTNumeric4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.OcxTXTNumeric4.Texto = "0"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(254, 245)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(45, 13)
        Me.Label9.TabIndex = 37
        Me.Label9.Text = "Liminite:"
        '
        'frmDescuentos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(812, 311)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.OcxTXTNumeric4)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.OcxTXTNumeric3)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.OcxTXTNumeric2)
        Me.Controls.Add(Me.TreeView2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.OcxTXTDate2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.OcxTXTDate1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.OcxTXTNumeric1)
        Me.Controls.Add(Me.TreeView1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.txtID)
        Me.Name = "frmDescuentos"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents OcxTXTDate1 As ERP.ocxTXTDate
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents OcxTXTDate2 As ERP.ocxTXTDate
    Friend WithEvents TreeView2 As System.Windows.Forms.TreeView
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents OcxTXTNumeric1 As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents OcxTXTNumeric2 As ERP.ocxTXTNumeric
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents OcxTXTNumeric3 As ERP.ocxTXTNumeric
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents OcxTXTNumeric4 As ERP.ocxTXTNumeric
    Friend WithEvents Label9 As System.Windows.Forms.Label
End Class

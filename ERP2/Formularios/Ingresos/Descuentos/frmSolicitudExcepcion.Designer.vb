﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSolicitudExcepcion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSolicitudExcepcion))
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtCantidadPedidos = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lvGrupo = New System.Windows.Forms.ListView()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.gbxGrupo = New System.Windows.Forms.GroupBox()
        Me.ToolStrip2 = New System.Windows.Forms.ToolStrip()
        Me.lblListadoPedidos = New System.Windows.Forms.ToolStripLabel()
        Me.cbxGrupos = New System.Windows.Forms.ToolStripComboBox()
        Me.btnActualizarGrupo = New System.Windows.Forms.ToolStripButton()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.lvPedidos = New System.Windows.Forms.ListView()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtFechaDesde = New System.Windows.Forms.DateTimePicker()
        Me.txtFechaHasta = New System.Windows.Forms.DateTimePicker()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel2 = New System.Windows.Forms.ToolStripLabel()
        Me.cbxOrden = New System.Windows.Forms.ToolStripComboBox()
        Me.btnActualizarPedidos = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnAprobar = New System.Windows.Forms.ToolStripButton()
        Me.btnSeleccionarTodos = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.btnRechazar = New System.Windows.Forms.ToolStripButton()
        Me.btnEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnCargarSolicitud = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbxAprobador = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbxSolicitante = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPorcentajeDesde = New ERP.ocxTXTNumeric()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtPorcentajeHasta = New ERP.ocxTXTNumeric()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cbxCliente = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cbxProducto = New System.Windows.Forms.ComboBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.gbxGrupo.SuspendLayout()
        Me.ToolStrip2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtCantidadPedidos)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(594, 0)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(595, 29)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'txtCantidadPedidos
        '
        Me.txtCantidadPedidos.Color = System.Drawing.Color.Empty
        Me.txtCantidadPedidos.Decimales = True
        Me.txtCantidadPedidos.Indicaciones = Nothing
        Me.txtCantidadPedidos.Location = New System.Drawing.Point(526, 3)
        Me.txtCantidadPedidos.Name = "txtCantidadPedidos"
        Me.txtCantidadPedidos.Size = New System.Drawing.Size(66, 22)
        Me.txtCantidadPedidos.SoloLectura = True
        Me.txtCantidadPedidos.TabIndex = 1
        Me.txtCantidadPedidos.TabStop = False
        Me.txtCantidadPedidos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadPedidos.Texto = "0"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(457, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Cantidad:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel2, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1, 1, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 483)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1189, 29)
        Me.TableLayoutPanel2.TabIndex = 4
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.Button1)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(594, 29)
        Me.FlowLayoutPanel2.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(3, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(166, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Exportar a Excel"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(1292, 3)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'lvGrupo
        '
        Me.lvGrupo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvGrupo.Location = New System.Drawing.Point(3, 43)
        Me.lvGrupo.Name = "lvGrupo"
        Me.lvGrupo.Size = New System.Drawing.Size(151, 488)
        Me.lvGrupo.SmallImageList = Me.ImageList1
        Me.lvGrupo.TabIndex = 1
        Me.lvGrupo.UseCompatibleStateImageBehavior = False
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "ROJO.JPG")
        Me.ImageList1.Images.SetKeyName(1, "AMARILLO.JPG")
        Me.ImageList1.Images.SetKeyName(2, "VERDE.JPG")
        '
        'gbxGrupo
        '
        Me.gbxGrupo.Controls.Add(Me.lvGrupo)
        Me.gbxGrupo.Controls.Add(Me.ToolStrip2)
        Me.gbxGrupo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxGrupo.Location = New System.Drawing.Point(3, 3)
        Me.gbxGrupo.Name = "gbxGrupo"
        Me.gbxGrupo.Size = New System.Drawing.Size(157, 534)
        Me.gbxGrupo.TabIndex = 0
        Me.gbxGrupo.TabStop = False
        Me.gbxGrupo.Text = "Grupos"
        '
        'ToolStrip2
        '
        Me.ToolStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblListadoPedidos, Me.cbxGrupos, Me.btnActualizarGrupo})
        Me.ToolStrip2.Location = New System.Drawing.Point(3, 16)
        Me.ToolStrip2.Name = "ToolStrip2"
        Me.ToolStrip2.Size = New System.Drawing.Size(151, 27)
        Me.ToolStrip2.TabIndex = 0
        Me.ToolStrip2.TabStop = True
        Me.ToolStrip2.Text = "ToolStrip2"
        '
        'lblListadoPedidos
        '
        Me.lblListadoPedidos.Name = "lblListadoPedidos"
        Me.lblListadoPedidos.Size = New System.Drawing.Size(50, 24)
        Me.lblListadoPedidos.Text = "Lista de:"
        '
        'cbxGrupos
        '
        Me.cbxGrupos.Items.AddRange(New Object() {"TODOS"})
        Me.cbxGrupos.Name = "cbxGrupos"
        Me.cbxGrupos.Size = New System.Drawing.Size(121, 23)
        '
        'btnActualizarGrupo
        '
        Me.btnActualizarGrupo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnActualizarGrupo.Image = CType(resources.GetObject("btnActualizarGrupo.Image"), System.Drawing.Image)
        Me.btnActualizarGrupo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnActualizarGrupo.Name = "btnActualizarGrupo"
        Me.btnActualizarGrupo.Size = New System.Drawing.Size(23, 20)
        Me.btnActualizarGrupo.Text = "ToolStripButton1"
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 163.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.FlowLayoutPanel3, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.gbxGrupo, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.GroupBox2, 1, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(1370, 573)
        Me.TableLayoutPanel3.TabIndex = 3
        '
        'FlowLayoutPanel3
        '
        Me.TableLayoutPanel3.SetColumnSpan(Me.FlowLayoutPanel3, 2)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnSalir)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(0, 540)
        Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(1370, 33)
        Me.FlowLayoutPanel3.TabIndex = 2
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Location = New System.Drawing.Point(166, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1201, 534)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Lista de Facturas"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lvPedidos, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel5, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 16)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1195, 515)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'lvPedidos
        '
        Me.lvPedidos.CheckBoxes = True
        Me.lvPedidos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvPedidos.Location = New System.Drawing.Point(3, 67)
        Me.lvPedidos.Name = "lvPedidos"
        Me.lvPedidos.Size = New System.Drawing.Size(1189, 410)
        Me.lvPedidos.SmallImageList = Me.ImageList1
        Me.lvPedidos.TabIndex = 1
        Me.lvPedidos.UseCompatibleStateImageBehavior = False
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.Label2)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtFechaDesde)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtFechaHasta)
        Me.FlowLayoutPanel4.Controls.Add(Me.ToolStrip1)
        Me.FlowLayoutPanel4.Controls.Add(Me.btnCargarSolicitud)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(1189, 26)
        Me.FlowLayoutPanel4.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 23)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Fecha:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFechaDesde
        '
        Me.txtFechaDesde.CustomFormat = ""
        Me.txtFechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtFechaDesde.Location = New System.Drawing.Point(53, 3)
        Me.txtFechaDesde.Name = "txtFechaDesde"
        Me.txtFechaDesde.Size = New System.Drawing.Size(96, 20)
        Me.txtFechaDesde.TabIndex = 8
        '
        'txtFechaHasta
        '
        Me.txtFechaHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtFechaHasta.Location = New System.Drawing.Point(155, 3)
        Me.txtFechaHasta.Name = "txtFechaHasta"
        Me.txtFechaHasta.Size = New System.Drawing.Size(95, 20)
        Me.txtFechaHasta.TabIndex = 9
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel2, Me.cbxOrden, Me.btnActualizarPedidos, Me.ToolStripSeparator1, Me.ToolStripSeparator2, Me.btnAprobar, Me.btnSeleccionarTodos, Me.ToolStripButton1, Me.btnRechazar, Me.btnEliminar})
        Me.ToolStrip1.Location = New System.Drawing.Point(253, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(680, 25)
        Me.ToolStrip1.TabIndex = 3
        Me.ToolStrip1.TabStop = True
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripLabel2
        '
        Me.ToolStripLabel2.Name = "ToolStripLabel2"
        Me.ToolStripLabel2.Size = New System.Drawing.Size(84, 22)
        Me.ToolStripLabel2.Text = "Ordenado por:"
        '
        'cbxOrden
        '
        Me.cbxOrden.Items.AddRange(New Object() {"CLIENTE", "PRODUCTO", "PORCENTAJE"})
        Me.cbxOrden.Name = "cbxOrden"
        Me.cbxOrden.Size = New System.Drawing.Size(90, 25)
        '
        'btnActualizarPedidos
        '
        Me.btnActualizarPedidos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnActualizarPedidos.Image = CType(resources.GetObject("btnActualizarPedidos.Image"), System.Drawing.Image)
        Me.btnActualizarPedidos.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnActualizarPedidos.Name = "btnActualizarPedidos"
        Me.btnActualizarPedidos.Size = New System.Drawing.Size(23, 22)
        Me.btnActualizarPedidos.Text = "ToolStripButton1"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'btnAprobar
        '
        Me.btnAprobar.Image = CType(resources.GetObject("btnAprobar.Image"), System.Drawing.Image)
        Me.btnAprobar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnAprobar.Name = "btnAprobar"
        Me.btnAprobar.Size = New System.Drawing.Size(70, 22)
        Me.btnAprobar.Text = "Aprobar"
        '
        'btnSeleccionarTodos
        '
        Me.btnSeleccionarTodos.Image = CType(resources.GetObject("btnSeleccionarTodos.Image"), System.Drawing.Image)
        Me.btnSeleccionarTodos.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSeleccionarTodos.Name = "btnSeleccionarTodos"
        Me.btnSeleccionarTodos.Size = New System.Drawing.Size(115, 22)
        Me.btnSeleccionarTodos.Text = "Seleccionar todo"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(113, 22)
        Me.ToolStripButton1.Text = "Quitar Seleccion"
        '
        'btnRechazar
        '
        Me.btnRechazar.Image = Global.ERP.My.Resources.Resources.icons8_eliminar_16
        Me.btnRechazar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnRechazar.Name = "btnRechazar"
        Me.btnRechazar.Size = New System.Drawing.Size(74, 22)
        Me.btnRechazar.Text = "Rechazar"
        '
        'btnEliminar
        '
        Me.btnEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.btnEliminar.Image = CType(resources.GetObject("btnEliminar.Image"), System.Drawing.Image)
        Me.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(54, 22)
        Me.btnEliminar.Text = "Eliminar"
        '
        'btnCargarSolicitud
        '
        Me.btnCargarSolicitud.Location = New System.Drawing.Point(936, 3)
        Me.btnCargarSolicitud.Name = "btnCargarSolicitud"
        Me.btnCargarSolicitud.Size = New System.Drawing.Size(102, 23)
        Me.btnCargarSolicitud.TabIndex = 7
        Me.btnCargarSolicitud.Text = "Cargar Solicitud"
        Me.btnCargarSolicitud.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.Controls.Add(Me.Label3)
        Me.FlowLayoutPanel5.Controls.Add(Me.cbxAprobador)
        Me.FlowLayoutPanel5.Controls.Add(Me.Label4)
        Me.FlowLayoutPanel5.Controls.Add(Me.cbxSolicitante)
        Me.FlowLayoutPanel5.Controls.Add(Me.Label5)
        Me.FlowLayoutPanel5.Controls.Add(Me.txtPorcentajeDesde)
        Me.FlowLayoutPanel5.Controls.Add(Me.Label8)
        Me.FlowLayoutPanel5.Controls.Add(Me.txtPorcentajeHasta)
        Me.FlowLayoutPanel5.Controls.Add(Me.Label6)
        Me.FlowLayoutPanel5.Controls.Add(Me.cbxCliente)
        Me.FlowLayoutPanel5.Controls.Add(Me.Label7)
        Me.FlowLayoutPanel5.Controls.Add(Me.cbxProducto)
        Me.FlowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(3, 35)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(1189, 26)
        Me.FlowLayoutPanel5.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(3, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 23)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Aprobador:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbxAprobador
        '
        Me.cbxAprobador.FormattingEnabled = True
        Me.cbxAprobador.Location = New System.Drawing.Point(72, 3)
        Me.cbxAprobador.Name = "cbxAprobador"
        Me.cbxAprobador.Size = New System.Drawing.Size(178, 21)
        Me.cbxAprobador.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(256, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 23)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Solicitante:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbxSolicitante
        '
        Me.cbxSolicitante.FormattingEnabled = True
        Me.cbxSolicitante.Location = New System.Drawing.Point(325, 3)
        Me.cbxSolicitante.Name = "cbxSolicitante"
        Me.cbxSolicitante.Size = New System.Drawing.Size(178, 21)
        Me.cbxSolicitante.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(509, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 23)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Porcentaje:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPorcentajeDesde
        '
        Me.txtPorcentajeDesde.Color = System.Drawing.Color.Empty
        Me.txtPorcentajeDesde.Decimales = True
        Me.txtPorcentajeDesde.Indicaciones = Nothing
        Me.txtPorcentajeDesde.Location = New System.Drawing.Point(578, 3)
        Me.txtPorcentajeDesde.Name = "txtPorcentajeDesde"
        Me.txtPorcentajeDesde.Size = New System.Drawing.Size(17, 21)
        Me.txtPorcentajeDesde.SoloLectura = False
        Me.txtPorcentajeDesde.TabIndex = 13
        Me.txtPorcentajeDesde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajeDesde.Texto = "0"
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(601, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(10, 23)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "_"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtPorcentajeHasta
        '
        Me.txtPorcentajeHasta.Color = System.Drawing.Color.Empty
        Me.txtPorcentajeHasta.Decimales = True
        Me.txtPorcentajeHasta.Indicaciones = Nothing
        Me.txtPorcentajeHasta.Location = New System.Drawing.Point(617, 3)
        Me.txtPorcentajeHasta.Name = "txtPorcentajeHasta"
        Me.txtPorcentajeHasta.Size = New System.Drawing.Size(18, 21)
        Me.txtPorcentajeHasta.SoloLectura = False
        Me.txtPorcentajeHasta.TabIndex = 14
        Me.txtPorcentajeHasta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajeHasta.Texto = "0"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(641, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(63, 23)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Cliente:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbxCliente
        '
        Me.cbxCliente.FormattingEnabled = True
        Me.cbxCliente.Location = New System.Drawing.Point(710, 3)
        Me.cbxCliente.Name = "cbxCliente"
        Me.cbxCliente.Size = New System.Drawing.Size(178, 21)
        Me.cbxCliente.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(894, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(63, 23)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Producto:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbxProducto
        '
        Me.cbxProducto.FormattingEnabled = True
        Me.cbxProducto.Location = New System.Drawing.Point(963, 3)
        Me.cbxProducto.Name = "cbxProducto"
        Me.cbxProducto.Size = New System.Drawing.Size(178, 21)
        Me.cbxProducto.TabIndex = 8
        '
        'frmSolicitudExcepcion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1370, 573)
        Me.Controls.Add(Me.TableLayoutPanel3)
        Me.Name = "frmSolicitudExcepcion"
        Me.Text = "frmSolicitudExcepcion"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.gbxGrupo.ResumeLayout(False)
        Me.gbxGrupo.PerformLayout()
        Me.ToolStrip2.ResumeLayout(False)
        Me.ToolStrip2.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel4.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents txtCantidadPedidos As ocxTXTNumeric
    Friend WithEvents Label1 As Label
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents btnSalir As Button
    Friend WithEvents lvGrupo As ListView
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents gbxGrupo As GroupBox
    Friend WithEvents ToolStrip2 As ToolStrip
    Friend WithEvents lblListadoPedidos As ToolStripLabel
    Friend WithEvents cbxGrupos As ToolStripComboBox
    Friend WithEvents btnActualizarGrupo As ToolStripButton
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents lvPedidos As ListView
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents Label2 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents btnActualizarPedidos As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents btnAprobar As ToolStripButton
    Friend WithEvents btnSeleccionarTodos As ToolStripButton
    Friend WithEvents ToolStripButton1 As ToolStripButton
    Friend WithEvents btnRechazar As ToolStripButton
    Friend WithEvents btnCargarSolicitud As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents txtFechaDesde As DateTimePicker
    Friend WithEvents txtFechaHasta As DateTimePicker
    Friend WithEvents FlowLayoutPanel5 As FlowLayoutPanel
    Friend WithEvents ToolStripLabel2 As ToolStripLabel
    Friend WithEvents cbxOrden As ToolStripComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents cbxSolicitante As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents cbxCliente As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents cbxProducto As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents cbxAprobador As ComboBox
    Friend WithEvents txtPorcentajeDesde As ocxTXTNumeric
    Friend WithEvents txtPorcentajeHasta As ocxTXTNumeric
    Friend WithEvents btnEliminar As ToolStripButton
End Class

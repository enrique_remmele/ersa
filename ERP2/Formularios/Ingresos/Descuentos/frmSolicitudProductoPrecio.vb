﻿Imports System.Threading
Imports Spire.Pdf.Exporting.XPS.Schema

Public Class frmSolicitudProductoPrecio

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim PorcentajeAsignado As Decimal = 0
    Dim ImporteAsignado As Decimal = 0

    'PROPIEDADES

    'VARIABLES
    Dim tProcesar As Thread
    Dim vProcesar As Boolean = False
    Dim dtPedidos As DataTable
    Dim dtAprobador As DataTable
    Dim dtSolicitante As DataTable
    Dim dtPorcentajes As DataTable
    Dim Listando As Boolean = False

    Enum ENUMOperacionPedido
        APROBAR = 1
        RECHAZAR = 2
        RECUPERAR = 3
        ELIMINAR = 4
    End Enum


    'FUNCIONES
    Sub Inicializar()

        Me.KeyPreview = True

        'Varios
        CheckForIllegalCrossThreadCalls = False

        cbxGrupos.SelectedIndex = 0
        cbxOrden.SelectedIndex = 0

        'Funciones
        ListarGrupos()

        'Fecha facturacion
        If IsDate(VGFechaFacturacion) = False Then
            VGFechaFacturacion = VGFechaHoraSistema
        End If

        txtFechaDesde.Text = Today
        txtFechaHasta.Text = Today.AddDays(7)


        ListarPedidos()
    End Sub

    Sub ListarGrupos()

        lvGrupo.Items.Clear()
        lvPedidos.Items.Clear()

        Dim SQL As String = ""
        Dim INDEX As Integer = cbxGrupos.SelectedIndex

        Dim dtGrupo As New DataTable("Grupos")

        dtGrupo.Columns.Add("Grupo", Type.GetType("System.String"))

        Dim newRow As DataRow = dtGrupo.NewRow()
        newRow(0) = "PENDIENTES"
        dtGrupo.Rows.Add(newRow)

        Dim newRowc As DataRow = dtGrupo.NewRow()
        newRowc(0) = "APROBADOS"
        dtGrupo.Rows.Add(newRowc)

        Dim newRowd As DataRow = dtGrupo.NewRow()
        newRowd(0) = "RECHAZADOS"
        dtGrupo.Rows.Add(newRowd)

        CSistema.dtToLv(lvGrupo, dtGrupo)

        For i = 0 To lvGrupo.Columns.Count - 1
            lvGrupo.Columns(i).AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize)
        Next


    End Sub

    Sub SeleccionarGrupo()

        ListarPedidos()

    End Sub

    Sub ListarPedidos()

        If lvGrupo.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim OrderBy As String = ""
        Dim Campo As String = ""


        If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        End If
        Select Case lvGrupo.SelectedItems.Item(0).Text
            Case "PENDIENTES"
                SQL = "Select * From vSolicitudProductoPrecio Where Estado = 'PENDIENTE'"
                OrderBy = " Order By " & cbxOrden.Text
                btnAprobar.Enabled = True
                btnRechazar.Enabled = True
                btnEliminar.Enabled = True
                btnAprobar.Text = "Aprobar"
                btnRechazar.Text = "Rechazar"
                txtFechaDesde.Visible = False
                txtFechaHasta.Visible = False

            Case "APROBADOS"
                SQL = "Select * From vSolicitudProductoPrecio Where Estado = 'APROBADO' and cast(FechaAprobado as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.Text, True, False) & "'"
                OrderBy = " Order By " & cbxOrden.Text
                btnAprobar.Enabled = True
                btnRechazar.Enabled = False
                btnEliminar.Enabled = True
                btnAprobar.Text = "Recuperar"
                txtFechaDesde.Visible = True
                txtFechaHasta.Visible = True


            Case "RECHAZADOS"
                SQL = "Select * From vSolicitudProductoPrecio Where Estado = 'RECHAZADO' and cast(FechaAprobado as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.Text, True, False) & "'"
                OrderBy = " Order By " & cbxOrden.Text
                btnAprobar.Enabled = True
                btnRechazar.Enabled = False
                btnEliminar.Enabled = False
                btnAprobar.Text = "Recuperar"
                txtFechaDesde.Visible = True
                txtFechaHasta.Visible = True

        End Select

        If Listando Then
            Exit Sub
        End If
        Listar(SQL, OrderBy)
        'Filtrar()

    End Sub

    Sub Listar(ByVal SQL As String, Optional ByVal OrderBy As String = "")

        'CSistema.SqlToLv(lvPedidos, SQL)
        Listando = True
        dtPedidos = CSistema.ExecuteToDataTable(SQL & OrderBy)


        Dim dtTipoProducto As DataTable = CSistema.ExecuteToDataTable(SQL.Replace("*", " Distinct IDTipoProducto, TipoProducto"))
        Dim dtSolicitante As DataTable = CSistema.ExecuteToDataTable(SQL.Replace("*", " Distinct IDUsuarioSolicitud, Solicitante"))
        Dim dtCliente As DataTable = CSistema.ExecuteToDataTable(SQL.Replace("*", " Distinct IDCliente, Cliente"))
        Dim dtProducto As DataTable = CSistema.ExecuteToDataTable(SQL.Replace("*", " Distinct IDProducto, Producto"))

        dtTipoProducto.Rows.Add("0", "TODOS")
        'dtAprobado.DefaultView.Sort = "IDUsuarioAprobado"
        CSistema.SqlToComboBox(cbxTipoProducto, dtTipoProducto)
        cbxTipoProducto.Text = "TODOS"

        dtSolicitante.Rows.Add("0", "TODOS")
        'dtSolicitante.DefaultView.Sort = "IDUsuarioSolicitud"
        CSistema.SqlToComboBox(cbxSolicitante, dtSolicitante)
        cbxSolicitante.Text = "TODOS"

        dtCliente.Rows.Add("0", "TODOS")
        'dtCliente.DefaultView.Sort = "IDCliente"
        CSistema.SqlToComboBox(cbxCliente, dtCliente)
        cbxCliente.Text = "TODOS"

        dtProducto.Rows.Add("0", "TODOS")
        'dtProducto.DefaultView.Sort = "IDProducto"
        CSistema.SqlToComboBox(cbxProducto, dtProducto)
        cbxProducto.Text = "TODOS"

        CSistema.dtToLv(lvPedidos, dtPedidos)
        lvPedidos.Columns("IDPresentacion").Width = 1
        lvPedidos.Columns("IDProducto").Width = 0
        lvPedidos.Columns("IDCliente").Width = 0
        lvPedidos.Columns("IDMoneda").Width = 0
        lvPedidos.Columns("IDUsuarioSolicitud").Width = 0
        lvPedidos.Columns("IDUsuarioAprobado").Width = 0
        lvPedidos.Columns("PrecioUnico").Width = 0
        txtCantidadPedidos.SetValue(dtPedidos.Rows.Count)
        Listando = False
    End Sub

    Sub Detener()
        Try
            If vProcesar = False Then
                Exit Sub
            End If

            tProcesar.Abort()
            vProcesar = False
        Catch ex As Exception

        End Try

        Thread.Sleep(1000)

    End Sub

    Sub AprobarRechazarPedido(ByVal Operacion As ENUMOperacionPedido)

        lvGrupo.Enabled = False
        lvPedidos.Enabled = False

        'Si no esta seleccionado ningun grupo, seleccionamos el primero
        If lvGrupo.Items.Count = 0 Then
            GoTo terminar
        End If

        If lvGrupo.SelectedItems.Count = 0 Then
            'GoTo terminar
        End If

        For Each item As ListViewItem In lvPedidos.Items
            'For Each item As ListViewItem In lvPedidos.CheckedItems
            If item.Checked = False Then
                GoTo siguiente
            End If

            item.ImageIndex = 1

            'Dim ID As Integer = item.Text
            Dim IDProducto As Integer = item.SubItems("IDProducto").Text
            Dim IDCliente As Integer = item.SubItems("IDCliente").Text
            Dim IDMoneda As Integer = item.SubItems("IDMoneda").Text
            Dim PrecioUnico As Boolean = item.SubItems("PrecioUnico").Text
            Dim Desde As Date = item.SubItems("Desde").Text
            Dim Hasta As Date = item.SubItems("Hasta").Text
            Dim Precio As Decimal = item.SubItems("PrecioConDescuento").Text
            Dim Porcentaje As Decimal = item.SubItems("Porcentaje").Text
            Dim Descuento As Decimal = item.SubItems("Descuento").Text
            Dim CantidadMinimaPrecioUnico As Integer = item.SubItems("CantidadMinimaPrecioUnico").Text
            Dim IDPresentacion As Integer = item.SubItems("IDPresentacion").Text
            'Dim Comprobante As String = item.SubItems(2).Text

            'Actualizar el item
            If ControlarPorcentaje(Porcentaje, Descuento, IDPresentacion) = True Then
                Select Case Operacion
                    Case ENUMOperacionPedido.APROBAR
                        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionProductoPrecio @IDProducto = " & IDProducto & ", @IDCliente = " & IDCliente & ", @IDMoneda = " & IDMoneda & ", @PrecioUnico = '" & PrecioUnico & "', @Precio = " & Precio & ", @Desde = '" & CSistema.FormatoFechaBaseDatos(Desde, True, False) & "', @Hasta = '" & CSistema.FormatoFechaBaseDatos(Hasta, True, False) & "', @CantidadMinimaPrecioUnico = '" & CSistema.FormatoNumeroBaseDatos(CantidadMinimaPrecioUnico, True) & "', @Operacion='APROBAR', @IDUsuario = " & vgIDUsuario & ", @IDTerminal = " & vgIDTerminal & "").Copy
                    Case ENUMOperacionPedido.RECHAZAR
                        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionProductoPrecio @IDProducto = " & IDProducto & ", @IDCliente = " & IDCliente & ", @IDMoneda = " & IDMoneda & ", @PrecioUnico = '" & PrecioUnico & "', @Precio = " & Precio & ", @Desde = '" & CSistema.FormatoFechaBaseDatos(Desde, True, False) & "', @Hasta = '" & CSistema.FormatoFechaBaseDatos(Hasta, True, False) & "', @CantidadMinimaPrecioUnico = '" & CSistema.FormatoNumeroBaseDatos(CantidadMinimaPrecioUnico, True) & "', @Operacion='RECHAZAR', @IDUsuario = " & vgIDUsuario & ", @IDTerminal = " & vgIDTerminal & "").Copy
                    Case ENUMOperacionPedido.RECUPERAR
                        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionProductoPrecio @IDProducto = " & IDProducto & ", @IDCliente = " & IDCliente & ", @IDMoneda = " & IDMoneda & ", @PrecioUnico = '" & PrecioUnico & "', @Precio = " & Precio & ", @Desde = '" & CSistema.FormatoFechaBaseDatos(Desde, True, False) & "', @Hasta = '" & CSistema.FormatoFechaBaseDatos(Hasta, True, False) & "', @CantidadMinimaPrecioUnico = '" & CSistema.FormatoNumeroBaseDatos(CantidadMinimaPrecioUnico, True) & "', @Operacion='RECUPERAR', @IDUsuario = " & vgIDUsuario & ", @IDTerminal = " & vgIDTerminal & "").Copy
                End Select

            End If

siguiente:

        Next

terminar:
        ListarPedidos()
        lvGrupo.Enabled = True
        lvPedidos.Enabled = True
        vProcesar = False

    End Sub

    Sub CargarSolicitud()

        Dim frm As New frmCargarSolicitudProductoPrecio
        frm.ShowDialog()
        ListarPedidos()

    End Sub

    'Sub CargarPorcentaje()
    '    PorcentajeAsignado = CType(CSistema.ExecuteScalar("Select IsNull(porcentaje,0) from PorcentajeExcepcion where IDUsuario=" & vgIDUsuario), Decimal)
    '    ImporteAsignado = CType(CSistema.ExecuteScalar("Select IsNull(Importe,0) from PorcentajeExcepcion where IDUsuario=" & vgIDUsuario), Decimal)
    'End Sub

    Sub Exportar()
        CSistema.LvToExcel(lvPedidos)
    End Sub

    Sub Filtrar()

        Dim Filtro As Boolean = False
        Dim dtAux As DataTable = dtPedidos.Clone
        Dim dtAux1 As DataTable = dtPedidos.Clone

        If Listando Then
            Exit Sub
        End If

        If cbxTipoProducto.Text <> "TODOS" And cbxTipoProducto.Text <> "" Then
            For Each oRow As DataRow In dtPedidos.Select("TipoProducto = '" & cbxTipoProducto.Text & "'")
                dtAux.ImportRow(oRow)
            Next
            Filtro = True
        End If

        If cbxSolicitante.Text <> "TODOS" And cbxSolicitante.Text <> "" Then
            If Filtro Then
                For Each oRow As DataRow In dtAux.Select("Solicitante = '" & cbxSolicitante.Text & "'")
                    dtAux1.ImportRow(oRow)
                Next
                dtAux = dtAux1.Copy()
                dtAux1.Clear()
            Else

                For Each oRow As DataRow In dtPedidos.Select("Solicitante = '" & cbxSolicitante.Text & "'")
                    dtAux.ImportRow(oRow)
                Next
            End If

            Filtro = True
        End If

        If cbxCliente.Text <> "TODOS" And cbxCliente.Text <> "" Then
            If Filtro Then
                For Each oRow As DataRow In dtAux.Select("Cliente = '" & cbxCliente.Text & "'")
                    dtAux1.ImportRow(oRow)
                Next
                dtAux = dtAux1.Copy()
                dtAux1.Clear()
            Else

                For Each oRow As DataRow In dtPedidos.Select("Cliente = '" & cbxCliente.Text & "'")
                    dtAux.ImportRow(oRow)
                Next
            End If

            Filtro = True
        End If

        If cbxProducto.Text <> "TODOS" And cbxProducto.Text <> "" Then
            If Filtro Then
                For Each oRow As DataRow In dtAux.Select("Producto = '" & cbxProducto.Text & "'")
                    dtAux1.ImportRow(oRow)
                Next
                dtAux = dtAux1.Copy()
                dtAux1.Clear()
            Else
                For Each oRow As DataRow In dtPedidos.Select("Producto = '" & cbxProducto.Text & "'")
                    dtAux.ImportRow(oRow)
                Next

            End If

            Filtro = True
        End If

        If Filtro = True Then
            CSistema.dtToLv(lvPedidos, dtAux)
            txtCantidadPedidos.txt.Text = dtAux.Rows.Count
        Else
            'If dtAux.Rows.Count <> dtPedidos.Rows.Count And dtAux.Rows.Count > 0 Then
            CSistema.dtToLv(lvPedidos, dtPedidos)
            'End If
            txtCantidadPedidos.txt.Text = dtPedidos.Rows.Count
        End If

        lvPedidos.Columns(1).Width = 10
        lvPedidos.Columns(3).Width = 0
        lvPedidos.Columns(5).Width = 0
        lvPedidos.Columns(8).Width = 0
        lvPedidos.Columns(10).Width = 0
        txtCantidadPedidos.SetValue(dtPedidos.Rows.Count)
    End Sub

    Sub ELiminarSolicitud()
        lvGrupo.Enabled = False
        lvPedidos.Enabled = False

        'Si no esta seleccionado ningun grupo, seleccionamos el primero
        If lvGrupo.Items.Count = 0 Then
            GoTo terminar
        End If

        For Each item As ListViewItem In lvPedidos.Items
            'For Each item As ListViewItem In lvPedidos.CheckedItems
            If item.Checked = False Then
                GoTo siguiente
            End If

            item.ImageIndex = 1

            Dim IDProducto As Integer = item.SubItems("IDProducto").Text
            Dim IDCliente As Integer = item.SubItems("IDCliente").Text
            Dim IDMoneda As Integer = item.SubItems("IDMoneda").Text
            Dim PrecioUnico As Boolean = item.SubItems("PrecioUnico").Text
            Dim Desde As Date = item.SubItems("Desde").Text
            Dim Hasta As Date = item.SubItems("Hasta").Text
            Dim Precio As Decimal = item.SubItems("PrecioConDescuento").Text
            Dim Porcentaje As Decimal = item.SubItems("Porcentaje").Text
            Dim Descuento As Decimal = item.SubItems("Descuento").Text
            Dim CantidadMinimaPrecioUnico As Integer = item.SubItems("CantidadMinimaPrecioUnico").Text
            Dim IDPresentacion As Integer = item.SubItems("IDPresentacion").Text
            'Dim Comprobante As String = item.SubItems(2).Text

            Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionProductoPrecio @IDProducto = " & IDProducto & ", @IDCliente = " & IDCliente & ", @IDMoneda = " & IDMoneda & ", @PrecioUnico = '" & PrecioUnico & "', @Precio = " & Precio & ", @Desde = '" & CSistema.FormatoFechaBaseDatos(Desde, True, False) & "', @Hasta = '" & CSistema.FormatoFechaBaseDatos(Hasta, True, False) & "', @CantidadMinimaPrecioUnico = '" & CSistema.FormatoNumeroBaseDatos(CantidadMinimaPrecioUnico, True) & "', @Operacion='ELIMINAR', @IDUsuario = " & vgIDUsuario & ", @IDTerminal = " & vgIDTerminal & "").Copy

siguiente:

        Next

terminar:
        ListarPedidos()
        lvGrupo.Enabled = True
        lvPedidos.Enabled = True
        vProcesar = False

    End Sub

    Function ControlarPorcentaje(ByVal Porcentaje As Decimal, ByVal Importe As Decimal, ByVal IDPresentacion As Integer) As Boolean
        ControlarPorcentaje = True
        Dim RowPorcentaje As DataRow = CSistema.ExecuteToDataTable("Select * from PorcentajeProductoPrecio where IDUsuario=" & vgIDUsuario & " And IDPresentacion = " & IDPresentacion)(0)
        If RowPorcentaje Is Nothing Then
            Return False
        End If
        PorcentajeAsignado = CType(RowPorcentaje("Porcentaje"), Decimal)
        ImporteAsignado = CType(RowPorcentaje("Importe"), Decimal)

        If PorcentajeAsignado = 0 And ImporteAsignado = 0 Then
            Return False
        End If
        If (Porcentaje > PorcentajeAsignado) Or (Importe > ImporteAsignado) Then
            Return False
        End If
    End Function

    Sub SeleccionarTodo()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = True
        Next
    End Sub

    Sub QuitarSeleccion()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = False
        Next
    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        Select Case e.KeyCode
            Case Keys.Enter
                CSistema.SelectNextControl(Me, e.KeyCode)
        End Select

    End Sub

    Sub EliminarPedido()

        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        lvPedidos.SelectedItems(0).Remove()

    End Sub

    Private Sub frmAprobarPedido_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Detener()
        'FA 20/06/2023
        LiberarMemoria()
    End Sub

    Private Sub frmAprobarPedido_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        ManejarTecla(e)
    End Sub

    Private Sub frmAprobarPedido_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnActualizarGrupo_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarGrupo.Click
        ListarGrupos()
    End Sub

    Private Sub cbxGrupos_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxGrupos.SelectedIndexChanged
        ListarGrupos()
    End Sub

    Private Sub lvGrupo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lvGrupo.SelectedIndexChanged
        SeleccionarGrupo()
    End Sub

    Private Sub btnProcesar_Click(sender As System.Object, e As System.EventArgs) Handles btnAprobar.Click
        If lvGrupo.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Select Case lvGrupo.SelectedItems.Item(0).Text
            Case "RECHAZADOS"
                AprobarRechazarPedido(ENUMOperacionPedido.RECUPERAR)
            Case "APROBADOS"
                AprobarRechazarPedido(ENUMOperacionPedido.RECUPERAR)
            Case Else
                AprobarRechazarPedido(ENUMOperacionPedido.APROBAR)
        End Select

    End Sub

    Private Sub btnSeleccionarTodos_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionarTodos.Click
        SeleccionarTodo()
    End Sub

    Private Sub ToolStripButton1_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton1.Click
        QuitarSeleccion()
    End Sub

    Private Sub btnActualizarPedidos_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarPedidos.Click
        ListarPedidos()
    End Sub

    Private Sub EliminarDeLaListaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        EliminarPedido()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnV_Click(sender As System.Object, e As System.EventArgs) Handles btnCargarSolicitud.Click
        CargarSolicitud()
        'MessageBox.Show("Modulo pendiente de habilitacion, favor cargue las solicitudes en Entidades/Clientes/Precio", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Hand)
    End Sub

    Private Sub ToolStripButton2_Click(sender As System.Object, e As System.EventArgs) Handles btnRechazar.Click
        'Select Case lvGrupo.SelectedItems.Item(0).Text
        '    Case "PENDIENTE"
        '        AprobarRechazarPedido(ENUMOperacionPedido.RECHAZAR)
        '    Case "RECHAZADO"
        '        AprobarRechazarPedido(ENUMOperacionPedido.ANULAR)
        'End Select

        AprobarRechazarPedido(ENUMOperacionPedido.RECHAZAR)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Exportar()
    End Sub

    Private Sub cbxAprobador_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxTipoProducto.SelectedValueChanged
        Filtrar()
    End Sub

    Private Sub cbxSolicitante_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSolicitante.SelectedValueChanged
        Filtrar()
    End Sub

    Private Sub cbxCliente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxCliente.SelectedValueChanged
        Filtrar()
    End Sub

    Private Sub cbxProducto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxProducto.SelectedValueChanged
        Filtrar()
    End Sub

    Private Sub txtPorcentajeDesde_TeclaPrecionada(sender As Object, e As KeyEventArgs)
        Filtrar()
    End Sub

    Private Sub txtPorcentajeHasta_TeclaPrecionada(sender As Object, e As KeyEventArgs)
        Filtrar()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If MessageBox.Show("Esto eliminara la solicitud permanentemente, Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.No Then
            Exit Sub
        End If
        ELiminarSolicitud()

    End Sub

    Private Sub btnRegistroPrecio_Click(sender As Object, e As EventArgs) Handles btnRegistroPrecio.Click
        Dim item As ListViewItem = lvPedidos.SelectedItems.Item(0)

        Dim frm As New frmRegistroPrecioCliente
        frm.IDClientePrecargar = item.SubItems("IDCliente").Text
        frm.IDProductoPrecargar = item.SubItems("IDProducto").Text
        frm.Precargar = True
        frm.ShowDialog()

    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmSolicitudProductoPrecio_Activate()
        Me.Refresh()
    End Sub
End Class
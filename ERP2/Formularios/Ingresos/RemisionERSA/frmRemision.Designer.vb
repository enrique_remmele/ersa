﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRemision
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtIDTimbrado = New ERP.ocxTXTString()
        Me.txtTimbrado = New ERP.ocxTXTString()
        Me.lklTimbrado = New System.Windows.Forms.LinkLabel()
        Me.txtRestoTimbrado = New ERP.ocxTXTNumeric()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.lblRestoTimbrado = New System.Windows.Forms.Label()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lblCiudad = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.txtVencimientoTimbrado = New ERP.ocxTXTDate()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.txtOtros = New ERP.ocxTXTString()
        Me.lblOtros = New System.Windows.Forms.Label()
        Me.cbxMotivo = New ERP.ocxCBX()
        Me.lblMotivo = New System.Windows.Forms.Label()
        Me.cbxChofer = New ERP.ocxCBX()
        Me.lblChofer = New System.Windows.Forms.Label()
        Me.cbxVehiculo = New ERP.ocxCBX()
        Me.lblVehiculo = New System.Windows.Forms.Label()
        Me.cbxDistribuidor = New ERP.ocxCBX()
        Me.lblDistribuidor = New System.Windows.Forms.Label()
        Me.txtComprobanteVenta = New ERP.ocxTXTString()
        Me.lblComprobanteVenta = New System.Windows.Forms.Label()
        Me.cbxSucursalOperacion = New ERP.ocxCBX()
        Me.lblSucursalOperacion = New System.Windows.Forms.Label()
        Me.txtDireccionDestino = New ERP.ocxTXTString()
        Me.lblDireccionDestino = New System.Windows.Forms.Label()
        Me.txtFechaFin = New ERP.ocxTXTDate()
        Me.txtCliente = New ERP.ocxTXTClienteVenta()
        Me.cbxDepositoOperacion = New ERP.ocxCBX()
        Me.txtDetalle = New ERP.ocxTXTString()
        Me.lblDetalle = New System.Windows.Forms.Label()
        Me.lblDepositoOperacion = New System.Windows.Forms.Label()
        Me.txtFechaInicio = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.txtDireccionPartida = New ERP.ocxTXTString()
        Me.lblDireccionPartida = New System.Windows.Forms.Label()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.lblVenimientoTimbrado = New System.Windows.Forms.Label()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.txtNroComprobante = New ERP.ocxTXTString()
        Me.txtTalonario = New ERP.ocxTXTString()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.lblTalonario = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.txtReferenciaSucursal = New ERP.ocxTXTString()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.gbxComprobante = New System.Windows.Forms.GroupBox()
        Me.txtReferenciaTerminal = New ERP.ocxTXTString()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblProducto = New System.Windows.Forms.Label()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.btnCargar = New System.Windows.Forms.Button()
        Me.lblAgregarPor = New System.Windows.Forms.Label()
        Me.btnFacturas = New System.Windows.Forms.Button()
        Me.btnLotes = New System.Windows.Forms.Button()
        Me.btnDeposito = New System.Windows.Forms.Button()
        Me.btnMovimiento = New System.Windows.Forms.Button()
        Me.dgvLista = New System.Windows.Forms.DataGridView()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtCantidadComprobante = New ERP.ocxTXTNumeric()
        Me.lblCantidadComprobante = New System.Windows.Forms.Label()
        Me.txtCantidadMaximaProductos = New ERP.ocxTXTNumeric()
        Me.lblCantidadDetalle = New System.Windows.Forms.Label()
        Me.gbxCabecera.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.gbxComprobante.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtIDTimbrado
        '
        Me.txtIDTimbrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIDTimbrado.Color = System.Drawing.Color.Beige
        Me.txtIDTimbrado.Indicaciones = Nothing
        Me.txtIDTimbrado.Location = New System.Drawing.Point(68, 11)
        Me.txtIDTimbrado.Multilinea = False
        Me.txtIDTimbrado.Name = "txtIDTimbrado"
        Me.txtIDTimbrado.Size = New System.Drawing.Size(36, 21)
        Me.txtIDTimbrado.SoloLectura = True
        Me.txtIDTimbrado.TabIndex = 1
        Me.txtIDTimbrado.TabStop = False
        Me.txtIDTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtIDTimbrado.Texto = ""
        Me.txtIDTimbrado.Visible = False
        '
        'txtTimbrado
        '
        Me.txtTimbrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTimbrado.Color = System.Drawing.Color.Beige
        Me.txtTimbrado.Indicaciones = Nothing
        Me.txtTimbrado.Location = New System.Drawing.Point(69, 11)
        Me.txtTimbrado.Multilinea = False
        Me.txtTimbrado.Name = "txtTimbrado"
        Me.txtTimbrado.Size = New System.Drawing.Size(176, 21)
        Me.txtTimbrado.SoloLectura = True
        Me.txtTimbrado.TabIndex = 2
        Me.txtTimbrado.TabStop = False
        Me.txtTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTimbrado.Texto = ""
        '
        'lklTimbrado
        '
        Me.lklTimbrado.AutoSize = True
        Me.lklTimbrado.Location = New System.Drawing.Point(9, 15)
        Me.lklTimbrado.Name = "lklTimbrado"
        Me.lklTimbrado.Size = New System.Drawing.Size(54, 13)
        Me.lklTimbrado.TabIndex = 0
        Me.lklTimbrado.TabStop = True
        Me.lklTimbrado.Text = "Timbrado:"
        '
        'txtRestoTimbrado
        '
        Me.txtRestoTimbrado.Color = System.Drawing.Color.Beige
        Me.txtRestoTimbrado.Decimales = True
        Me.txtRestoTimbrado.Indicaciones = Nothing
        Me.txtRestoTimbrado.Location = New System.Drawing.Point(389, 34)
        Me.txtRestoTimbrado.Name = "txtRestoTimbrado"
        Me.txtRestoTimbrado.Size = New System.Drawing.Size(117, 22)
        Me.txtRestoTimbrado.SoloLectura = True
        Me.txtRestoTimbrado.TabIndex = 17
        Me.txtRestoTimbrado.TabStop = False
        Me.txtRestoTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRestoTimbrado.Texto = "0"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(389, 11)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(117, 21)
        Me.cbxSucursal.SoloLectura = True
        Me.cbxSucursal.TabIndex = 6
        Me.cbxSucursal.TabStop = False
        Me.cbxSucursal.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = "Codigo"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = "VCiudad"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(284, 11)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(73, 21)
        Me.cbxCiudad.SoloLectura = True
        Me.cbxCiudad.TabIndex = 4
        Me.cbxCiudad.TabStop = False
        Me.cbxCiudad.Texto = ""
        '
        'lblRestoTimbrado
        '
        Me.lblRestoTimbrado.AutoSize = True
        Me.lblRestoTimbrado.Location = New System.Drawing.Point(251, 39)
        Me.lblRestoTimbrado.Name = "lblRestoTimbrado"
        Me.lblRestoTimbrado.Size = New System.Drawing.Size(138, 13)
        Me.lblRestoTimbrado.TabIndex = 16
        Me.lblRestoTimbrado.Text = "Nro. restantes en Talonario:"
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBusquedaAvanzada.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(0, 3)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(126, 23)
        Me.btnBusquedaAvanzada.TabIndex = 0
        Me.btnBusquedaAvanzada.Text = "&Busqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnSalir.Location = New System.Drawing.Point(672, 3)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 5
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnGuardar.Location = New System.Drawing.Point(474, 3)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 3
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnNuevo.Location = New System.Drawing.Point(393, 3)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 2
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(251, 15)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(28, 13)
        Me.lblCiudad.TabIndex = 3
        Me.lblCiudad.Text = "Ciu.:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(357, 15)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(32, 13)
        Me.lblSucursal.TabIndex = 5
        Me.lblSucursal.Text = "Suc.:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnCancelar.Location = New System.Drawing.Point(555, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 4
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'txtVencimientoTimbrado
        '
        Me.txtVencimientoTimbrado.Color = System.Drawing.Color.Beige
        Me.txtVencimientoTimbrado.Fecha = New Date(CType(0, Long))
        Me.txtVencimientoTimbrado.Location = New System.Drawing.Point(172, 35)
        Me.txtVencimientoTimbrado.Name = "txtVencimientoTimbrado"
        Me.txtVencimientoTimbrado.PermitirNulo = False
        Me.txtVencimientoTimbrado.Size = New System.Drawing.Size(74, 20)
        Me.txtVencimientoTimbrado.SoloLectura = True
        Me.txtVencimientoTimbrado.TabIndex = 15
        Me.txtVencimientoTimbrado.TabStop = False
        '
        'gbxCabecera
        '
        Me.gbxCabecera.BackColor = System.Drawing.Color.Transparent
        Me.gbxCabecera.Controls.Add(Me.txtOtros)
        Me.gbxCabecera.Controls.Add(Me.lblOtros)
        Me.gbxCabecera.Controls.Add(Me.cbxMotivo)
        Me.gbxCabecera.Controls.Add(Me.lblMotivo)
        Me.gbxCabecera.Controls.Add(Me.cbxChofer)
        Me.gbxCabecera.Controls.Add(Me.lblChofer)
        Me.gbxCabecera.Controls.Add(Me.cbxVehiculo)
        Me.gbxCabecera.Controls.Add(Me.lblVehiculo)
        Me.gbxCabecera.Controls.Add(Me.cbxDistribuidor)
        Me.gbxCabecera.Controls.Add(Me.lblDistribuidor)
        Me.gbxCabecera.Controls.Add(Me.txtComprobanteVenta)
        Me.gbxCabecera.Controls.Add(Me.lblComprobanteVenta)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursalOperacion)
        Me.gbxCabecera.Controls.Add(Me.lblSucursalOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtDireccionDestino)
        Me.gbxCabecera.Controls.Add(Me.lblDireccionDestino)
        Me.gbxCabecera.Controls.Add(Me.txtFechaFin)
        Me.gbxCabecera.Controls.Add(Me.txtCliente)
        Me.gbxCabecera.Controls.Add(Me.cbxDepositoOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtDetalle)
        Me.gbxCabecera.Controls.Add(Me.lblDetalle)
        Me.gbxCabecera.Controls.Add(Me.lblDepositoOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtFechaInicio)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.txtDireccionPartida)
        Me.gbxCabecera.Controls.Add(Me.lblDireccionPartida)
        Me.gbxCabecera.Controls.Add(Me.lblCliente)
        Me.gbxCabecera.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxCabecera.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbxCabecera.Location = New System.Drawing.Point(3, 68)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(748, 172)
        Me.gbxCabecera.TabIndex = 1
        Me.gbxCabecera.TabStop = False
        '
        'txtOtros
        '
        Me.txtOtros.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOtros.Color = System.Drawing.Color.Beige
        Me.txtOtros.Indicaciones = Nothing
        Me.txtOtros.Location = New System.Drawing.Point(362, 121)
        Me.txtOtros.Multilinea = False
        Me.txtOtros.Name = "txtOtros"
        Me.txtOtros.Size = New System.Drawing.Size(380, 21)
        Me.txtOtros.SoloLectura = False
        Me.txtOtros.TabIndex = 22
        Me.txtOtros.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtOtros.Texto = ""
        '
        'lblOtros
        '
        Me.lblOtros.AutoSize = True
        Me.lblOtros.Location = New System.Drawing.Point(327, 125)
        Me.lblOtros.Name = "lblOtros"
        Me.lblOtros.Size = New System.Drawing.Size(35, 13)
        Me.lblOtros.TabIndex = 21
        Me.lblOtros.Text = "Otros:"
        '
        'cbxMotivo
        '
        Me.cbxMotivo.CampoWhere = Nothing
        Me.cbxMotivo.CargarUnaSolaVez = False
        Me.cbxMotivo.DataDisplayMember = Nothing
        Me.cbxMotivo.DataFilter = Nothing
        Me.cbxMotivo.DataOrderBy = Nothing
        Me.cbxMotivo.DataSource = Nothing
        Me.cbxMotivo.DataValueMember = Nothing
        Me.cbxMotivo.FormABM = Nothing
        Me.cbxMotivo.Indicaciones = Nothing
        Me.cbxMotivo.Location = New System.Drawing.Point(69, 121)
        Me.cbxMotivo.Name = "cbxMotivo"
        Me.cbxMotivo.SeleccionObligatoria = True
        Me.cbxMotivo.Size = New System.Drawing.Size(236, 21)
        Me.cbxMotivo.SoloLectura = False
        Me.cbxMotivo.TabIndex = 20
        Me.cbxMotivo.Texto = ""
        '
        'lblMotivo
        '
        Me.lblMotivo.AutoSize = True
        Me.lblMotivo.Location = New System.Drawing.Point(24, 125)
        Me.lblMotivo.Name = "lblMotivo"
        Me.lblMotivo.Size = New System.Drawing.Size(42, 13)
        Me.lblMotivo.TabIndex = 19
        Me.lblMotivo.Text = "Motivo:"
        '
        'cbxChofer
        '
        Me.cbxChofer.CampoWhere = Nothing
        Me.cbxChofer.CargarUnaSolaVez = False
        Me.cbxChofer.DataDisplayMember = "Nombres"
        Me.cbxChofer.DataFilter = Nothing
        Me.cbxChofer.DataOrderBy = "Nombres"
        Me.cbxChofer.DataSource = "VChofer"
        Me.cbxChofer.DataValueMember = "ID"
        Me.cbxChofer.FormABM = Nothing
        Me.cbxChofer.Indicaciones = Nothing
        Me.cbxChofer.Location = New System.Drawing.Point(548, 94)
        Me.cbxChofer.Name = "cbxChofer"
        Me.cbxChofer.SeleccionObligatoria = True
        Me.cbxChofer.Size = New System.Drawing.Size(195, 21)
        Me.cbxChofer.SoloLectura = False
        Me.cbxChofer.TabIndex = 18
        Me.cbxChofer.Texto = ""
        '
        'lblChofer
        '
        Me.lblChofer.AutoSize = True
        Me.lblChofer.Location = New System.Drawing.Point(505, 98)
        Me.lblChofer.Name = "lblChofer"
        Me.lblChofer.Size = New System.Drawing.Size(41, 13)
        Me.lblChofer.TabIndex = 17
        Me.lblChofer.Text = "Chofer:"
        '
        'cbxVehiculo
        '
        Me.cbxVehiculo.CampoWhere = Nothing
        Me.cbxVehiculo.CargarUnaSolaVez = False
        Me.cbxVehiculo.DataDisplayMember = "Descripcion"
        Me.cbxVehiculo.DataFilter = Nothing
        Me.cbxVehiculo.DataOrderBy = Nothing
        Me.cbxVehiculo.DataSource = "VCamion"
        Me.cbxVehiculo.DataValueMember = "ID"
        Me.cbxVehiculo.FormABM = Nothing
        Me.cbxVehiculo.Indicaciones = Nothing
        Me.cbxVehiculo.Location = New System.Drawing.Point(362, 94)
        Me.cbxVehiculo.Name = "cbxVehiculo"
        Me.cbxVehiculo.SeleccionObligatoria = True
        Me.cbxVehiculo.Size = New System.Drawing.Size(139, 21)
        Me.cbxVehiculo.SoloLectura = False
        Me.cbxVehiculo.TabIndex = 16
        Me.cbxVehiculo.Texto = ""
        '
        'lblVehiculo
        '
        Me.lblVehiculo.AutoSize = True
        Me.lblVehiculo.Location = New System.Drawing.Point(311, 98)
        Me.lblVehiculo.Name = "lblVehiculo"
        Me.lblVehiculo.Size = New System.Drawing.Size(51, 13)
        Me.lblVehiculo.TabIndex = 15
        Me.lblVehiculo.Text = "Vehiculo:"
        '
        'cbxDistribuidor
        '
        Me.cbxDistribuidor.CampoWhere = Nothing
        Me.cbxDistribuidor.CargarUnaSolaVez = False
        Me.cbxDistribuidor.DataDisplayMember = "Nombres"
        Me.cbxDistribuidor.DataFilter = Nothing
        Me.cbxDistribuidor.DataOrderBy = "Nombres"
        Me.cbxDistribuidor.DataSource = "VDistribuidor"
        Me.cbxDistribuidor.DataValueMember = "ID"
        Me.cbxDistribuidor.FormABM = Nothing
        Me.cbxDistribuidor.Indicaciones = Nothing
        Me.cbxDistribuidor.Location = New System.Drawing.Point(69, 94)
        Me.cbxDistribuidor.Name = "cbxDistribuidor"
        Me.cbxDistribuidor.SeleccionObligatoria = True
        Me.cbxDistribuidor.Size = New System.Drawing.Size(236, 21)
        Me.cbxDistribuidor.SoloLectura = False
        Me.cbxDistribuidor.TabIndex = 14
        Me.cbxDistribuidor.Texto = ""
        '
        'lblDistribuidor
        '
        Me.lblDistribuidor.AutoSize = True
        Me.lblDistribuidor.Location = New System.Drawing.Point(4, 98)
        Me.lblDistribuidor.Name = "lblDistribuidor"
        Me.lblDistribuidor.Size = New System.Drawing.Size(62, 13)
        Me.lblDistribuidor.TabIndex = 13
        Me.lblDistribuidor.Text = "Distribuidor:"
        '
        'txtComprobanteVenta
        '
        Me.txtComprobanteVenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobanteVenta.Color = System.Drawing.Color.Beige
        Me.txtComprobanteVenta.Indicaciones = Nothing
        Me.txtComprobanteVenta.Location = New System.Drawing.Point(629, 148)
        Me.txtComprobanteVenta.Multilinea = False
        Me.txtComprobanteVenta.Name = "txtComprobanteVenta"
        Me.txtComprobanteVenta.Size = New System.Drawing.Size(113, 21)
        Me.txtComprobanteVenta.SoloLectura = False
        Me.txtComprobanteVenta.TabIndex = 26
        Me.txtComprobanteVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobanteVenta.Texto = ""
        '
        'lblComprobanteVenta
        '
        Me.lblComprobanteVenta.AutoSize = True
        Me.lblComprobanteVenta.Location = New System.Drawing.Point(524, 152)
        Me.lblComprobanteVenta.Name = "lblComprobanteVenta"
        Me.lblComprobanteVenta.Size = New System.Drawing.Size(104, 13)
        Me.lblComprobanteVenta.TabIndex = 25
        Me.lblComprobanteVenta.Text = "Comprobante Venta:"
        '
        'cbxSucursalOperacion
        '
        Me.cbxSucursalOperacion.CampoWhere = Nothing
        Me.cbxSucursalOperacion.CargarUnaSolaVez = False
        Me.cbxSucursalOperacion.DataDisplayMember = "Descripcion"
        Me.cbxSucursalOperacion.DataFilter = Nothing
        Me.cbxSucursalOperacion.DataOrderBy = Nothing
        Me.cbxSucursalOperacion.DataSource = "VSucursal"
        Me.cbxSucursalOperacion.DataValueMember = "ID"
        Me.cbxSucursalOperacion.FormABM = Nothing
        Me.cbxSucursalOperacion.Indicaciones = Nothing
        Me.cbxSucursalOperacion.Location = New System.Drawing.Point(69, 67)
        Me.cbxSucursalOperacion.Name = "cbxSucursalOperacion"
        Me.cbxSucursalOperacion.SeleccionObligatoria = True
        Me.cbxSucursalOperacion.Size = New System.Drawing.Size(73, 21)
        Me.cbxSucursalOperacion.SoloLectura = False
        Me.cbxSucursalOperacion.TabIndex = 10
        Me.cbxSucursalOperacion.Texto = ""
        '
        'lblSucursalOperacion
        '
        Me.lblSucursalOperacion.AutoSize = True
        Me.lblSucursalOperacion.Location = New System.Drawing.Point(15, 71)
        Me.lblSucursalOperacion.Name = "lblSucursalOperacion"
        Me.lblSucursalOperacion.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursalOperacion.TabIndex = 9
        Me.lblSucursalOperacion.Text = "Sucursal:"
        '
        'txtDireccionDestino
        '
        Me.txtDireccionDestino.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccionDestino.Color = System.Drawing.Color.Beige
        Me.txtDireccionDestino.Indicaciones = Nothing
        Me.txtDireccionDestino.Location = New System.Drawing.Point(439, 40)
        Me.txtDireccionDestino.Multilinea = False
        Me.txtDireccionDestino.Name = "txtDireccionDestino"
        Me.txtDireccionDestino.Size = New System.Drawing.Size(304, 21)
        Me.txtDireccionDestino.SoloLectura = False
        Me.txtDireccionDestino.TabIndex = 8
        Me.txtDireccionDestino.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccionDestino.Texto = ""
        '
        'lblDireccionDestino
        '
        Me.lblDireccionDestino.AutoSize = True
        Me.lblDireccionDestino.Location = New System.Drawing.Point(387, 44)
        Me.lblDireccionDestino.Name = "lblDireccionDestino"
        Me.lblDireccionDestino.Size = New System.Drawing.Size(46, 13)
        Me.lblDireccionDestino.TabIndex = 7
        Me.lblDireccionDestino.Text = "Destino:"
        '
        'txtFechaFin
        '
        Me.txtFechaFin.Color = System.Drawing.Color.Empty
        Me.txtFechaFin.Fecha = New Date(CType(0, Long))
        Me.txtFechaFin.Location = New System.Drawing.Point(660, 12)
        Me.txtFechaFin.Name = "txtFechaFin"
        Me.txtFechaFin.PermitirNulo = False
        Me.txtFechaFin.Size = New System.Drawing.Size(83, 20)
        Me.txtFechaFin.SoloLectura = False
        Me.txtFechaFin.TabIndex = 4
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 117
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(69, 10)
        Me.txtCliente.MostrarSucursal = True
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(442, 24)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 1
        '
        'cbxDepositoOperacion
        '
        Me.cbxDepositoOperacion.CampoWhere = Nothing
        Me.cbxDepositoOperacion.CargarUnaSolaVez = False
        Me.cbxDepositoOperacion.DataDisplayMember = "Deposito"
        Me.cbxDepositoOperacion.DataFilter = Nothing
        Me.cbxDepositoOperacion.DataOrderBy = Nothing
        Me.cbxDepositoOperacion.DataSource = "VDeposito"
        Me.cbxDepositoOperacion.DataValueMember = "ID"
        Me.cbxDepositoOperacion.FormABM = Nothing
        Me.cbxDepositoOperacion.Indicaciones = Nothing
        Me.cbxDepositoOperacion.Location = New System.Drawing.Point(204, 67)
        Me.cbxDepositoOperacion.Name = "cbxDepositoOperacion"
        Me.cbxDepositoOperacion.SeleccionObligatoria = False
        Me.cbxDepositoOperacion.Size = New System.Drawing.Size(169, 21)
        Me.cbxDepositoOperacion.SoloLectura = False
        Me.cbxDepositoOperacion.TabIndex = 12
        Me.cbxDepositoOperacion.Texto = ""
        '
        'txtDetalle
        '
        Me.txtDetalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDetalle.Color = System.Drawing.Color.Empty
        Me.txtDetalle.Indicaciones = Nothing
        Me.txtDetalle.Location = New System.Drawing.Point(69, 148)
        Me.txtDetalle.Multilinea = False
        Me.txtDetalle.Name = "txtDetalle"
        Me.txtDetalle.Size = New System.Drawing.Size(449, 21)
        Me.txtDetalle.SoloLectura = False
        Me.txtDetalle.TabIndex = 24
        Me.txtDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDetalle.Texto = ""
        '
        'lblDetalle
        '
        Me.lblDetalle.AutoSize = True
        Me.lblDetalle.Location = New System.Drawing.Point(23, 152)
        Me.lblDetalle.Name = "lblDetalle"
        Me.lblDetalle.Size = New System.Drawing.Size(43, 13)
        Me.lblDetalle.TabIndex = 23
        Me.lblDetalle.Text = "Detalle:"
        '
        'lblDepositoOperacion
        '
        Me.lblDepositoOperacion.AutoSize = True
        Me.lblDepositoOperacion.Location = New System.Drawing.Point(145, 71)
        Me.lblDepositoOperacion.Name = "lblDepositoOperacion"
        Me.lblDepositoOperacion.Size = New System.Drawing.Size(52, 13)
        Me.lblDepositoOperacion.TabIndex = 11
        Me.lblDepositoOperacion.Text = "Deposito:"
        '
        'txtFechaInicio
        '
        Me.txtFechaInicio.Color = System.Drawing.Color.Empty
        Me.txtFechaInicio.Fecha = New Date(CType(0, Long))
        Me.txtFechaInicio.Location = New System.Drawing.Point(574, 12)
        Me.txtFechaInicio.Name = "txtFechaInicio"
        Me.txtFechaInicio.PermitirNulo = False
        Me.txtFechaInicio.Size = New System.Drawing.Size(77, 20)
        Me.txtFechaInicio.SoloLectura = False
        Me.txtFechaInicio.TabIndex = 3
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(515, 16)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(58, 13)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Text = "Inicio - Fin:"
        '
        'txtDireccionPartida
        '
        Me.txtDireccionPartida.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccionPartida.Color = System.Drawing.Color.Beige
        Me.txtDireccionPartida.Indicaciones = Nothing
        Me.txtDireccionPartida.Location = New System.Drawing.Point(69, 40)
        Me.txtDireccionPartida.Multilinea = False
        Me.txtDireccionPartida.Name = "txtDireccionPartida"
        Me.txtDireccionPartida.Size = New System.Drawing.Size(304, 21)
        Me.txtDireccionPartida.SoloLectura = False
        Me.txtDireccionPartida.TabIndex = 6
        Me.txtDireccionPartida.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccionPartida.Texto = ""
        '
        'lblDireccionPartida
        '
        Me.lblDireccionPartida.AutoSize = True
        Me.lblDireccionPartida.Location = New System.Drawing.Point(23, 44)
        Me.lblDireccionPartida.Name = "lblDireccionPartida"
        Me.lblDireccionPartida.Size = New System.Drawing.Size(43, 13)
        Me.lblDireccionPartida.TabIndex = 5
        Me.lblDireccionPartida.Text = "Partida:"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(24, 16)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(42, 13)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Text = "Cliente:"
        '
        'lblVenimientoTimbrado
        '
        Me.lblVenimientoTimbrado.AutoSize = True
        Me.lblVenimientoTimbrado.Location = New System.Drawing.Point(104, 39)
        Me.lblVenimientoTimbrado.Name = "lblVenimientoTimbrado"
        Me.lblVenimientoTimbrado.Size = New System.Drawing.Size(68, 13)
        Me.lblVenimientoTimbrado.TabIndex = 14
        Me.lblVenimientoTimbrado.Text = "Vencimiento:"
        '
        'btnImprimir
        '
        Me.btnImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnImprimir.Location = New System.Drawing.Point(132, 3)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 1
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(512, 15)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(55, 13)
        Me.lblComprobante.TabIndex = 7
        Me.lblComprobante.Text = "Comprob.:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 581)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(754, 20)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 15)
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 3)
        Me.lblAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.ForeColor = System.Drawing.SystemColors.ControlText
        Me.flpAnuladoPor.Location = New System.Drawing.Point(364, 3)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(355, 20)
        Me.flpAnuladoPor.TabIndex = 0
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 3)
        Me.lblUsuarioAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 3)
        Me.lblFechaAnulado.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'txtNroComprobante
        '
        Me.txtNroComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroComprobante.Color = System.Drawing.Color.Empty
        Me.txtNroComprobante.Indicaciones = Nothing
        Me.txtNroComprobante.Location = New System.Drawing.Point(680, 11)
        Me.txtNroComprobante.Multilinea = False
        Me.txtNroComprobante.Name = "txtNroComprobante"
        Me.txtNroComprobante.Size = New System.Drawing.Size(68, 21)
        Me.txtNroComprobante.SoloLectura = False
        Me.txtNroComprobante.TabIndex = 11
        Me.txtNroComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNroComprobante.Texto = ""
        '
        'txtTalonario
        '
        Me.txtTalonario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTalonario.Color = System.Drawing.Color.Beige
        Me.txtTalonario.Indicaciones = Nothing
        Me.txtTalonario.Location = New System.Drawing.Point(68, 35)
        Me.txtTalonario.Multilinea = False
        Me.txtTalonario.Name = "txtTalonario"
        Me.txtTalonario.Size = New System.Drawing.Size(36, 21)
        Me.txtTalonario.SoloLectura = True
        Me.txtTalonario.TabIndex = 13
        Me.txtTalonario.TabStop = False
        Me.txtTalonario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTalonario.Texto = ""
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Codigo"
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = ""
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(569, 11)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(55, 21)
        Me.cbxTipoComprobante.SoloLectura = True
        Me.cbxTipoComprobante.TabIndex = 8
        Me.cbxTipoComprobante.TabStop = False
        Me.cbxTipoComprobante.Texto = ""
        '
        'lblTalonario
        '
        Me.lblTalonario.AutoSize = True
        Me.lblTalonario.Location = New System.Drawing.Point(9, 39)
        Me.lblTalonario.Name = "lblTalonario"
        Me.lblTalonario.Size = New System.Drawing.Size(54, 13)
        Me.lblTalonario.TabIndex = 12
        Me.lblTalonario.Text = "Talonario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 3)
        Me.lblFechaRegistro.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'txtReferenciaSucursal
        '
        Me.txtReferenciaSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferenciaSucursal.Color = System.Drawing.Color.Empty
        Me.txtReferenciaSucursal.Indicaciones = Nothing
        Me.txtReferenciaSucursal.Location = New System.Drawing.Point(624, 11)
        Me.txtReferenciaSucursal.Multilinea = False
        Me.txtReferenciaSucursal.Name = "txtReferenciaSucursal"
        Me.txtReferenciaSucursal.Size = New System.Drawing.Size(28, 21)
        Me.txtReferenciaSucursal.SoloLectura = True
        Me.txtReferenciaSucursal.TabIndex = 9
        Me.txtReferenciaSucursal.TabStop = False
        Me.txtReferenciaSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferenciaSucursal.Texto = ""
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 3)
        Me.lblUsuarioRegistro.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'gbxComprobante
        '
        Me.gbxComprobante.BackColor = System.Drawing.Color.Transparent
        Me.gbxComprobante.Controls.Add(Me.txtIDTimbrado)
        Me.gbxComprobante.Controls.Add(Me.txtTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lklTimbrado)
        Me.gbxComprobante.Controls.Add(Me.txtRestoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.cbxSucursal)
        Me.gbxComprobante.Controls.Add(Me.cbxCiudad)
        Me.gbxComprobante.Controls.Add(Me.lblRestoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lblCiudad)
        Me.gbxComprobante.Controls.Add(Me.lblSucursal)
        Me.gbxComprobante.Controls.Add(Me.txtVencimientoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lblVenimientoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lblComprobante)
        Me.gbxComprobante.Controls.Add(Me.txtNroComprobante)
        Me.gbxComprobante.Controls.Add(Me.txtTalonario)
        Me.gbxComprobante.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxComprobante.Controls.Add(Me.lblTalonario)
        Me.gbxComprobante.Controls.Add(Me.txtReferenciaSucursal)
        Me.gbxComprobante.Controls.Add(Me.txtReferenciaTerminal)
        Me.gbxComprobante.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxComprobante.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbxComprobante.Location = New System.Drawing.Point(3, 3)
        Me.gbxComprobante.Name = "gbxComprobante"
        Me.gbxComprobante.Size = New System.Drawing.Size(748, 59)
        Me.gbxComprobante.TabIndex = 0
        Me.gbxComprobante.TabStop = False
        '
        'txtReferenciaTerminal
        '
        Me.txtReferenciaTerminal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferenciaTerminal.Color = System.Drawing.Color.Empty
        Me.txtReferenciaTerminal.Indicaciones = Nothing
        Me.txtReferenciaTerminal.Location = New System.Drawing.Point(652, 11)
        Me.txtReferenciaTerminal.Multilinea = False
        Me.txtReferenciaTerminal.Name = "txtReferenciaTerminal"
        Me.txtReferenciaTerminal.Size = New System.Drawing.Size(28, 21)
        Me.txtReferenciaTerminal.SoloLectura = True
        Me.txtReferenciaTerminal.TabIndex = 10
        Me.txtReferenciaTerminal.TabStop = False
        Me.txtReferenciaTerminal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferenciaTerminal.Texto = ""
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 3)
        Me.lblRegistradoPor.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.ForeColor = System.Drawing.SystemColors.ControlText
        Me.flpRegistradoPor.Location = New System.Drawing.Point(3, 3)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(355, 20)
        Me.flpRegistradoPor.TabIndex = 1
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.StatusStrip1, 0, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxComprobante, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxCabecera, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvLista, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 4)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 8
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 65.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 178.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(754, 601)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Panel2.Controls.Add(Me.btnImprimir)
        Me.Panel2.Controls.Add(Me.btnSalir)
        Me.Panel2.Controls.Add(Me.btnCancelar)
        Me.Panel2.Controls.Add(Me.btnGuardar)
        Me.Panel2.Controls.Add(Me.btnNuevo)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 548)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(748, 30)
        Me.Panel2.TabIndex = 4
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.flpRegistradoPor)
        Me.FlowLayoutPanel1.Controls.Add(Me.flpAnuladoPor)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 518)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(748, 24)
        Me.FlowLayoutPanel1.TabIndex = 3
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.lblProducto)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtProducto)
        Me.FlowLayoutPanel2.Controls.Add(Me.lblCantidad)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtCantidad)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnCargar)
        Me.FlowLayoutPanel2.Controls.Add(Me.lblAgregarPor)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnFacturas)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnLotes)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnDeposito)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnMovimiento)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 246)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(748, 60)
        Me.FlowLayoutPanel2.TabIndex = 2
        '
        'lblProducto
        '
        Me.lblProducto.Location = New System.Drawing.Point(3, 0)
        Me.lblProducto.Name = "lblProducto"
        Me.lblProducto.Size = New System.Drawing.Size(60, 26)
        Me.lblProducto.TabIndex = 0
        Me.lblProducto.Text = "Producto:"
        Me.lblProducto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 0
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(69, 3)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.Size = New System.Drawing.Size(454, 23)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 1
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'lblCantidad
        '
        Me.lblCantidad.Location = New System.Drawing.Point(529, 0)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(60, 26)
        Me.lblCantidad.TabIndex = 2
        Me.lblCantidad.Text = "Cantidad:"
        Me.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = Nothing
        Me.txtCantidad.Location = New System.Drawing.Point(595, 3)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(56, 21)
        Me.txtCantidad.SoloLectura = False
        Me.txtCantidad.TabIndex = 3
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'btnCargar
        '
        Me.btnCargar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCargar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnCargar.Location = New System.Drawing.Point(657, 1)
        Me.btnCargar.Margin = New System.Windows.Forms.Padding(3, 1, 3, 3)
        Me.btnCargar.Name = "btnCargar"
        Me.btnCargar.Size = New System.Drawing.Size(78, 23)
        Me.btnCargar.TabIndex = 4
        Me.btnCargar.Text = "Cargar"
        Me.btnCargar.UseVisualStyleBackColor = True
        '
        'lblAgregarPor
        '
        Me.lblAgregarPor.Location = New System.Drawing.Point(3, 29)
        Me.lblAgregarPor.Name = "lblAgregarPor"
        Me.lblAgregarPor.Size = New System.Drawing.Size(73, 26)
        Me.lblAgregarPor.TabIndex = 5
        Me.lblAgregarPor.Text = "Agregar por:"
        Me.lblAgregarPor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnFacturas
        '
        Me.btnFacturas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFacturas.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnFacturas.Location = New System.Drawing.Point(82, 32)
        Me.btnFacturas.Name = "btnFacturas"
        Me.btnFacturas.Size = New System.Drawing.Size(75, 23)
        Me.btnFacturas.TabIndex = 6
        Me.btnFacturas.Text = "Facturas"
        Me.btnFacturas.UseVisualStyleBackColor = True
        '
        'btnLotes
        '
        Me.btnLotes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLotes.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnLotes.Location = New System.Drawing.Point(163, 32)
        Me.btnLotes.Name = "btnLotes"
        Me.btnLotes.Size = New System.Drawing.Size(75, 23)
        Me.btnLotes.TabIndex = 7
        Me.btnLotes.Text = "Lotes"
        Me.btnLotes.UseVisualStyleBackColor = True
        '
        'btnDeposito
        '
        Me.btnDeposito.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeposito.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnDeposito.Location = New System.Drawing.Point(244, 32)
        Me.btnDeposito.Name = "btnDeposito"
        Me.btnDeposito.Size = New System.Drawing.Size(75, 23)
        Me.btnDeposito.TabIndex = 8
        Me.btnDeposito.Text = "Deposito"
        Me.btnDeposito.UseVisualStyleBackColor = True
        '
        'btnMovimiento
        '
        Me.btnMovimiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMovimiento.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnMovimiento.Location = New System.Drawing.Point(325, 32)
        Me.btnMovimiento.Name = "btnMovimiento"
        Me.btnMovimiento.Size = New System.Drawing.Size(75, 23)
        Me.btnMovimiento.TabIndex = 9
        Me.btnMovimiento.Text = "Movimientos"
        Me.btnMovimiento.UseVisualStyleBackColor = True
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLista.Location = New System.Drawing.Point(3, 312)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.Size = New System.Drawing.Size(748, 170)
        Me.dgvLista.TabIndex = 6
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.txtCantidadComprobante)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblCantidadComprobante)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtCantidadMaximaProductos)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblCantidadDetalle)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 488)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(748, 24)
        Me.FlowLayoutPanel3.TabIndex = 7
        '
        'txtCantidadComprobante
        '
        Me.txtCantidadComprobante.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobante.Decimales = True
        Me.txtCantidadComprobante.Indicaciones = Nothing
        Me.txtCantidadComprobante.Location = New System.Drawing.Point(689, 3)
        Me.txtCantidadComprobante.Name = "txtCantidadComprobante"
        Me.txtCantidadComprobante.Size = New System.Drawing.Size(56, 21)
        Me.txtCantidadComprobante.SoloLectura = True
        Me.txtCantidadComprobante.TabIndex = 5
        Me.txtCantidadComprobante.TabStop = False
        Me.txtCantidadComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobante.Texto = "0"
        '
        'lblCantidadComprobante
        '
        Me.lblCantidadComprobante.Location = New System.Drawing.Point(548, 0)
        Me.lblCantidadComprobante.Name = "lblCantidadComprobante"
        Me.lblCantidadComprobante.Size = New System.Drawing.Size(135, 26)
        Me.lblCantidadComprobante.TabIndex = 4
        Me.lblCantidadComprobante.Text = "Cantidad:"
        Me.lblCantidadComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCantidadMaximaProductos
        '
        Me.txtCantidadMaximaProductos.Color = System.Drawing.Color.Empty
        Me.txtCantidadMaximaProductos.Decimales = True
        Me.txtCantidadMaximaProductos.Indicaciones = Nothing
        Me.txtCantidadMaximaProductos.Location = New System.Drawing.Point(486, 3)
        Me.txtCantidadMaximaProductos.Name = "txtCantidadMaximaProductos"
        Me.txtCantidadMaximaProductos.Size = New System.Drawing.Size(56, 21)
        Me.txtCantidadMaximaProductos.SoloLectura = True
        Me.txtCantidadMaximaProductos.TabIndex = 7
        Me.txtCantidadMaximaProductos.TabStop = False
        Me.txtCantidadMaximaProductos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadMaximaProductos.Texto = "0"
        '
        'lblCantidadDetalle
        '
        Me.lblCantidadDetalle.Location = New System.Drawing.Point(297, 0)
        Me.lblCantidadDetalle.Name = "lblCantidadDetalle"
        Me.lblCantidadDetalle.Size = New System.Drawing.Size(183, 26)
        Me.lblCantidadDetalle.TabIndex = 6
        Me.lblCantidadDetalle.Text = "Cantidad maxima de productos:"
        Me.lblCantidadDetalle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmRemision
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(754, 601)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmRemision"
        Me.Text = "frmRemision"
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.gbxComprobante.ResumeLayout(False)
        Me.gbxComprobante.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtIDTimbrado As ERP.ocxTXTString
    Friend WithEvents txtTimbrado As ERP.ocxTXTString
    Friend WithEvents lklTimbrado As System.Windows.Forms.LinkLabel
    Friend WithEvents txtRestoTimbrado As ERP.ocxTXTNumeric
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents lblRestoTimbrado As System.Windows.Forms.Label
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents txtVencimientoTimbrado As ERP.ocxTXTDate
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents txtCliente As ERP.ocxTXTClienteVenta
    Friend WithEvents cbxDepositoOperacion As ERP.ocxCBX
    Friend WithEvents txtDetalle As ERP.ocxTXTString
    Friend WithEvents lblDetalle As System.Windows.Forms.Label
    Friend WithEvents lblDepositoOperacion As System.Windows.Forms.Label
    Friend WithEvents txtFechaInicio As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents txtDireccionPartida As ERP.ocxTXTString
    Friend WithEvents lblDireccionPartida As System.Windows.Forms.Label
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents lblVenimientoTimbrado As System.Windows.Forms.Label
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents gbxComprobante As System.Windows.Forms.GroupBox
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents txtNroComprobante As ERP.ocxTXTString
    Friend WithEvents txtTalonario As ERP.ocxTXTString
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents lblTalonario As System.Windows.Forms.Label
    Friend WithEvents txtReferenciaSucursal As ERP.ocxTXTString
    Friend WithEvents txtReferenciaTerminal As ERP.ocxTXTString
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtFechaFin As ERP.ocxTXTDate
    Friend WithEvents txtDireccionDestino As ERP.ocxTXTString
    Friend WithEvents lblDireccionDestino As System.Windows.Forms.Label
    Friend WithEvents cbxSucursalOperacion As ERP.ocxCBX
    Friend WithEvents lblSucursalOperacion As System.Windows.Forms.Label
    Friend WithEvents txtComprobanteVenta As ERP.ocxTXTString
    Friend WithEvents lblComprobanteVenta As System.Windows.Forms.Label
    Friend WithEvents txtOtros As ERP.ocxTXTString
    Friend WithEvents lblOtros As System.Windows.Forms.Label
    Friend WithEvents cbxMotivo As ERP.ocxCBX
    Friend WithEvents lblMotivo As System.Windows.Forms.Label
    Friend WithEvents cbxChofer As ERP.ocxCBX
    Friend WithEvents lblChofer As System.Windows.Forms.Label
    Friend WithEvents cbxVehiculo As ERP.ocxCBX
    Friend WithEvents lblVehiculo As System.Windows.Forms.Label
    Friend WithEvents cbxDistribuidor As ERP.ocxCBX
    Friend WithEvents lblDistribuidor As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnFacturas As System.Windows.Forms.Button
    Friend WithEvents btnLotes As System.Windows.Forms.Button
    Friend WithEvents btnDeposito As System.Windows.Forms.Button
    Friend WithEvents lblProducto As System.Windows.Forms.Label
    Friend WithEvents lblAgregarPor As System.Windows.Forms.Label
    Friend WithEvents btnMovimiento As System.Windows.Forms.Button
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents dgvLista As System.Windows.Forms.DataGridView
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As ERP.ocxTXTNumeric
    Friend WithEvents btnCargar As System.Windows.Forms.Button
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtCantidadComprobante As ERP.ocxTXTNumeric
    Friend WithEvents lblCantidadComprobante As System.Windows.Forms.Label
    Friend WithEvents txtCantidadMaximaProductos As ERP.ocxTXTNumeric
    Friend WithEvents lblCantidadDetalle As System.Windows.Forms.Label
End Class

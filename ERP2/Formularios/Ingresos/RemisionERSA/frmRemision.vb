﻿Imports ERP.Reporte

Public Class frmRemision

    'CLASES
    Public CSistema As New CSistema
    Public CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim CReporte As New CReporteVentas

    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private NuevoIDTransaccionValue As Integer
    Public Property NuevoIDTransaccion() As Integer
        Get
            Return NuevoIDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            NuevoIDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private dtDetalleValue As DataTable
    Public Property dtDetalle() As DataTable
        Get
            Return dtDetalleValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleValue = value
        End Set
    End Property

    'VARIABLES
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Public dtPuntoExpedicion As DataTable

    Sub Inicializar()

        Me.AcceptButton = New Button

        'Controles
        txtProducto.Venta = False
        txtProducto.Conectar()
        txtCliente.Conectar()
        txtCliente.frm = Me

        'Otros
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        'Propiedades
        IDTransaccion = 0
        vNuevo = False

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion("frmRemision", "REMISION EMITIDA", "REM", CadenaConexion)
        CargarInformacion()

        'Clases

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        'Foco
        txtNroComprobante.Focus()
        txtNroComprobante.txt.Focus()
        txtNroComprobante.txt.SelectAll()

        'CONFIGURACIONES
        txtFechaInicio.SetValue(VGFechaFacturacion)

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'Cabecera
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, txtFechaInicio)
        CSistema.CargaControl(vControles, txtFechaFin)
        CSistema.CargaControl(vControles, txtDireccionPartida)
        CSistema.CargaControl(vControles, txtDireccionDestino)
        CSistema.CargaControl(vControles, cbxSucursalOperacion)
        CSistema.CargaControl(vControles, cbxDepositoOperacion)
        CSistema.CargaControl(vControles, cbxDistribuidor)
        CSistema.CargaControl(vControles, cbxVehiculo)
        CSistema.CargaControl(vControles, cbxChofer)
        CSistema.CargaControl(vControles, cbxMotivo)
        CSistema.CargaControl(vControles, txtOtros)
        CSistema.CargaControl(vControles, txtDetalle)
        CSistema.CargaControl(vControles, txtComprobanteVenta)

        'Detalle
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, txtCantidad)
        CSistema.CargaControl(vControles, btnFacturas)
        CSistema.CargaControl(vControles, btnDeposito)
        CSistema.CargaControl(vControles, btnLotes)
        CSistema.CargaControl(vControles, btnMovimiento)
        CSistema.CargaControl(vControles, dgvLista)

        dtDetalle = CData.GetStructure("VDetalleVenta", "Select Top(0) IDTransaccion, IDProducto, ID, Referencia, Producto, CodigoBarra, Cantidad, Deposito From VDetalleRemision")

        'CARGAR CONTROLES
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, CData.GetTable("VTipoComprobante", " IDOperacion = " & IDOperacion), "ID", "Codigo")

        'Ciudad
        cbxCiudad.Conectar()

        'Sucursal
        cbxSucursal.Conectar()

        'Puntos de Expediciones
        dtPuntoExpedicion = CData.GetTable("VPuntoExpedicion").Copy
        dtPuntoExpedicion = CData.FiltrarDataTable(dtPuntoExpedicion, " IDOperacion = " & IDOperacion & " And Estado='True' ")
        txtIDTimbrado.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIMBRADO", 0))
        ObtenerInformacionPuntoVenta()

        'Fecha de Venta
        txtFechaInicio.SetValue(VGFechaFacturacion)

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)
        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, New Button, vControles)
    End Sub

    Sub GuardarInformacion()

        'TIMBRADO
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIMBRADO", txtIDTimbrado.GetValue)

    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Limpiar cabecera
        txtCliente.SetValue(Nothing)

        'Limpiar detalle
        dtDetalle.Rows.Clear()
        ListarDetalle()

        'Recargar el dtTerminalPuntoExpedicion
        CData.ResetTable("VPuntoExpedicion")
        dtPuntoExpedicion = CData.GetTable("VPuntoExpedicion", " IDOperacion = " & IDOperacion & " And Estado='True' ")
        ObtenerInformacionPuntoVenta()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        
        vNuevo = True

        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        ObtenerDeposito()

        'Poner el foco en el proveedor
        txtNroComprobante.txt.SelectAll()
        txtNroComprobante.txt.Focus()

    End Sub

    Sub Cancelar()

        vNuevo = False

        Dim e As New KeyEventArgs(Keys.End)
        txtNroComprobante_TeclaPrecionada(New Object, e)

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        txtNroComprobante.txt.Focus()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Seleccion de Punto de Expedicion
        If IsNumeric(txtIDTimbrado.GetValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el punto de venta!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Nro de Comprobante, mayor a 0
        If IsNumeric(txtNroComprobante.txt.Text) = False Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Asignamos entero al nro de comprobante
        txtNroComprobante.txt.Text = CInt(txtNroComprobante.txt.Text)

        If CInt(txtNroComprobante.txt.Text) = 0 Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Cliente
        If txtCliente.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If txtCliente.Registro Is Nothing Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If IsNumeric(txtCliente.Registro("ID").ToString) = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Sucursal
        If IsNumeric(cbxSucursalOperacion.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente la sucursal!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Deposito
        If IsNumeric(cbxDepositoOperacion.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Existencia en Detalle
        If dtDetalle.Rows.Count = 0 Then
            Dim mensaje As String = "El documento debe tener por lo menos 1 detalle!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        ValidarDocumento = True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Exit Sub

        Dim param(-1) As SqlClient.SqlParameter
        Dim IDTransaccion As Integer
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        'Punto de Expedicion
        Dim PuntoExpedicionRow As DataRow = CData.GetTable("VTerminalPuntoExpedicion", " IDTerminal = " & vgIDTerminal).Select("IDPuntoExpedicion=" & txtIDTimbrado.GetValue)(0)

        CSistema.SetSQLParameter(param, "@IDPuntoExpedicion", txtIDTimbrado.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", PuntoExpedicionRow("IDTipoComprobante").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtNroComprobante.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Comprobante", PuntoExpedicionRow("ReferenciaSucursal").ToString & "-" & PuntoExpedicionRow("ReferenciaPunto").ToString & "-" & txtNroComprobante.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DireccionPartida", txtDireccionPartida.txt.Text, ParameterDirection.Input, 100)
        CSistema.SetSQLParameter(param, "@DireccionLlegada", txtDireccionDestino.txt.Text, ParameterDirection.Input, 100)

        CSistema.SetSQLParameter(param, "@FechaInicio", CSistema.FormatoFechaBaseDatos(txtFechaInicio.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaFin", CSistema.FormatoFechaBaseDatos(txtFechaFin.GetValue.ToShortDateString, True, False), ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursalOperacion.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoOperacion", cbxDepositoOperacion.cbx.SelectedValue, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@IDDistribuidor", cbxDistribuidor.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCamion", cbxVehiculo.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDChofer", cbxChofer.cbx.SelectedValue, ParameterDirection.Input)

        'Si es por sucursal
        If txtCliente.SucursalSeleccionada = True Then
            CSistema.SetSQLParameter(param, "@IDSucursalCliente", txtCliente.Sucursal("ID").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@EsVentaSucursal", "True", ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        'Capturamos el index de la Operacion para un posible proceso posterior
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpVenta", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From Venta Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpVenta", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        If IDTransaccion > 0 Then

            'Insertamos el Detalle
            'If InsertarDetalle(IDTransaccion, ERP.CSistema.NUMOperacionesRegistro.INS) = False Then

            '    'Eliminar el Registro si es que se registro
            '    If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From Compra Where IDTransaccion=1) Is Null Then 'False' Else 'True' End)")) = True Then
            '        param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
            '        CSistema.ExecuteStoreProcedure(param, "SpVenta", False, False, MensajeRetorno, IDTransaccion)
            '    End If

            '    Exit Sub

            'End If

            'Si es nuevo
            If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then

                'Imprimimos
                Imprimir()

            End If

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtNroComprobante.SoloLectura = False

    End Sub

    Function InsertarDetalle() As String

        InsertarDetalle = ""

        For Each oRow As DataRow In dtDetalle.Rows

            Dim param As New DataTable

            CSistema.SetSQLParameter(param, "IDTransaccion", NuevoIDTransaccion)
            CSistema.SetSQLParameter(param, "IDProducto", oRow("IDProducto").ToString)
            CSistema.SetSQLParameter(param, "ID", oRow("ID").ToString)
            CSistema.SetSQLParameter(param, "IDDeposito", oRow("IDDeposito").ToString)
            CSistema.SetSQLParameter(param, "Observacion", oRow("Observacion").ToString)
            CSistema.SetSQLParameter(param, "IDImpuesto", oRow("IDImpuesto").ToString)
            CSistema.SetSQLParameter(param, "Cantidad", CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString))

            InsertarDetalle = InsertarDetalle & CSistema.InsertSQL(param, "DetalleRemision") & vbCrLf

        Next

    End Function

    Sub CargarProducto()

        'Validar
        If ValidarProducto() = False Then
            Exit Sub
        End If

        'Cargamos el registro en el detalle
        Dim dRow As DataRow = dtDetalle.NewRow()

        'Obtener Valores
        'Productos
        dRow("IDTransaccion") = NuevoIDTransaccion
        dRow("IDProducto") = txtProducto.Registro("ID").ToString
        dRow("ID") = dtDetalle.Rows.Count
        dRow("Referencia") = txtProducto.Registro("Ref").ToString
        dRow("Producto") = txtProducto.Registro("Descripcion").ToString
        dRow("CodigoBarra") = txtProducto.Registro("CodigoBarra").ToString
        dRow("Cantidad") = txtCantidad.ObtenerValor

        'Deposito
        dRow("Deposito") = cbxDepositoOperacion.cbx.Text

        'Agregamos al detalle
        dtDetalle.Rows.Add(dRow)

        ListarDetalle()

        'Inicializamos los valores
        txtCantidad.txt.Text = "0"
        txtProducto.txt.txt.Clear()

        'Retorno de Foco
        txtProducto.txt.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Function ValidarProducto() As Boolean

        ValidarProducto = False

        'Seleccion de Producto
        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Cantidad
        If IsNumeric(txtCantidad.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If CDec(txtCantidad.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Limite de lineas en el detalle
        If dgvLista.Rows.Count >= txtCantidadMaximaProductos.ObtenerValor Then
            MessageBox.Show("Se ha llegado al limite de lineas del detalle del comprobante cierre esta operacion.!", "Limite de lineas.!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Function
        End If

        ValidarProducto = True

    End Function

    Sub EliminarProducto()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If dgvLista.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgvLista, mensaje)
            ctrError.SetIconAlignment(dgvLista, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        For Each item As ListViewItem In dgvLista.SelectedRows
            dtDetalle.Rows.RemoveAt(item.Index)
            Exit For
        Next

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtDetalle.Rows
            If Not oRow.RowState = DataRowState.Deleted Then
                oRow("ID") = Index
                Index = Index + 1
            End If
        Next

        ListarDetalle()

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        CSistema.dtToGrid(dgvLista, dtDetalle)

        'Formato
        dgvLista.Columns("IDTransaccion").Visible = False
        dgvLista.Columns("ID").Visible = False
        dgvLista.Columns("IDProducto").Visible = False
        dgvLista.Columns("Deposito").Visible = False

        dgvLista.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvLista.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Cantidad").DefaultCellStyle.Format = "N2"

        'Bloqueamos la cabecera si corresponde
        If dtDetalle.Rows.Count > 0 Then
            cbxDepositoOperacion.SoloLectura = True
            txtCliente.SoloLectura = True
        Else
            cbxDepositoOperacion.SoloLectura = False
            txtCliente.SoloLectura = False
        End If

        
    End Sub

    Function ObtenerNroComprobante() As String

        Dim dt As DataTable = CData.GetTable("VPuntoExpedicion", " IDOperacion = " & IDOperacion)

        If dt Is Nothing AndAlso dt.Rows.Count = 0 Then
            Return "0"
        End If

        If IsNumeric(txtIDTimbrado.GetValue) = False Then
            Return "0"
        End If

        If dt.Select("ID=" & txtIDTimbrado.GetValue).Count = 0 Then
            Return "0"
        End If

        Dim PuntoExpedicionRow As DataRow = dt.Select("ID=" & txtIDTimbrado.GetValue)(0)
        ObtenerNroComprobante = PuntoExpedicionRow("ReferenciaSucursal").ToString & "-" & PuntoExpedicionRow("ReferenciaPunto").ToString & "-" & txtNroComprobante.GetValue

    End Function

    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'Otros
        Dim frm As New frmConsultaRemision
        frm.SoloConsulta = False
        frm.IDOperacion = IDOperacion
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.Size = New Size(frmPrincipal2.ClientSize.Width - 75, frmPrincipal2.ClientSize.Height - 75)
        frm.StartPosition = FormStartPosition.CenterScreen
        FGMostrarFormulario(Me, frm, "Seleccion de Comprobante", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtNroComprobante.txt.Focus()
        txtNroComprobante.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select Top(1) IDTransaccion From VVenta Where Comprobante = '" & ObtenerNroComprobante() & "' And IDPuntoExpedicion=" & txtIDTimbrado.GetValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtNroComprobante, mensaje)
            ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select Top(1) * From VRemision Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select * From VDetalleRemision Where IDTransaccion=" & IDTransaccion & " Order By ID").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtNroComprobante, mensaje)
            ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        'Punto de Expedicion
        txtTimbrado.SetValue(oRow("Timbrado").ToString)

        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.txt.Text = oRow("TipoComprobante").ToString
        txtReferenciaSucursal.txt.Text = oRow("ReferenciaSucursal").ToString
        txtReferenciaSucursal.txt.Text = oRow("ReferenciaSucursal").ToString
        txtNroComprobante.txt.Text = oRow("NroComprobante").ToString


        txtCliente.SetValue(oRow("IDCliente").ToString)
        txtFechaInicio.SetValueFromString(CDate(oRow("FechaInicio").ToString))
        txtFechaFin.SetValueFromString(CDate(oRow("FechaFin").ToString))
        txtDireccionPartida.txt.Text = oRow("DireccionPartida").ToString
        txtDireccionDestino.txt.Text = oRow("DireccionLlegada").ToString

        cbxSucursalOperacion.txt.Text = oRow("SucursalOperacion").ToString
        cbxDepositoOperacion.txt.Text = oRow("DepositoOperacion").ToString

        cbxDistribuidor.txt.Text = oRow("Distribuidor").ToString
        cbxChofer.txt.Text = oRow("Chofer").ToString
        cbxVehiculo.txt.Text = oRow("Camion").ToString

        'cbxMotivo.txt.Text = oRow("Motivo").ToString
        txtOtros.txt.Text = oRow("MotivoObservacion").ToString

        txtDetalle.txt.Text = oRow("Observacion").ToString
        txtComprobanteVenta.txt.Text = oRow("ComprobanteVenta").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            btnImprimir.Enabled = False
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos el detalle
        ListarDetalle()
        
    End Sub

    Sub ListarSucursalesClientes()

    End Sub

    Sub ObtenerInformacionPuntoVenta()

        txtTalonario.txt.Text = "0"
        txtVencimientoTimbrado.txt.Text = ""
        txtRestoTimbrado.txt.Text = "0"
        cbxTipoComprobante.cbx.Text = ""
        txtReferenciaSucursal.txt.Text = "000"
        txtReferenciaTerminal.txt.Text = "000"
        txtNroComprobante.txt.Text = "000"
        cbxCiudad.cbx.Text = ""
        cbxSucursal.cbx.Text = ""
        txtProducto.IDSucursal = vgIDSucursal

        If IsNumeric(txtIDTimbrado.GetValue) = False Then
            Exit Sub
        End If

        If dtPuntoExpedicion Is Nothing Then
            Exit Sub
        End If

        For Each oRow As DataRow In dtPuntoExpedicion.Select(" ID=" & txtIDTimbrado.GetValue)
            txtTimbrado.txt.Text = oRow("Timbrado").ToString
            txtTalonario.txt.Text = oRow("TalonarioActual").ToString
            txtVencimientoTimbrado.txt.Text = oRow("Vencimiento").ToString
            txtRestoTimbrado.txt.Text = oRow("Saldo").ToString
            cbxTipoComprobante.SelectedValue(oRow("IDTipoComprobante").ToString)
            txtReferenciaSucursal.txt.Text = oRow("ReferenciaSucursal").ToString
            txtReferenciaTerminal.txt.Text = oRow("ReferenciaPunto").ToString
            txtNroComprobante.txt.Text = oRow("ProximoNumero").ToString
            cbxCiudad.SelectedValue(oRow("IDCiudad").ToString)
            cbxSucursal.SelectedValue(oRow("IDSucursal").ToString)

            ' cbxDeposito.cbx.Text = 
            txtProducto.IDSucursal = oRow("IDSucursal").ToString
        Next

        Debug.Print(cbxTipoComprobante.cbx.Text)
        Debug.Print(cbxTipoComprobante.txt.Text)
        Debug.Print(cbxTipoComprobante.Texto)
        Debug.Print(txtFechaInicio.GetValueString)

    End Sub

    Sub ObtenerDeposito()

        If cbxSucursal.cbx.SelectedValue Is Nothing Then
            Exit Sub
        End If

        Dim dttemp As DataTable = CData.GetTable("VDeposito", "IDSucursal=" & cbxSucursal.cbx.SelectedValue).Copy

        CSistema.SqlToComboBox(cbxDepositoOperacion.cbx, dttemp)

        Dim DepositoRow As DataRow = CData.GetRow("IDSucursal=" & cbxSucursal.cbx.SelectedValue, "VDeposito")

        cbxDepositoOperacion.cbx.Text = DepositoRow("Deposito").ToString
        cbxDepositoOperacion.cbx.Text = vgDeposito


    End Sub

    Sub ObtenerInformacionClientes()

        Dim oRow As DataRow = txtCliente.Registro
        Dim oRowSucursal As DataRow = txtCliente.Sucursal

        'Datos Comunes
        txtProducto.IDCliente = oRow("ID").ToString

        If txtCliente.SucursalSeleccionada = False Then
            txtDireccionDestino.txt.Text = oRow("Direccion").ToString
        Else
            txtDireccionDestino.txt.Text = oRowSucursal("Direccion").ToString
        End If

    End Sub

    Sub Imprimir(Optional ByVal ImprimirDirecto As Boolean = False)

        If ImprimirDirecto = False Then
            If CBool(vgConfiguraciones("RemisionImprimirDocumento").ToString) = False Then
                Exit Sub
            End If
        End If

        Dim Where As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar IDTransaccion
        Where = " Where IDTransaccion =" & IDTransaccion

        Dim Path As String = vgImpresionRemisionPath

        'CReporte.ImprimirFactura(frm, Where, Path, vgImpresionFacturaImpresora, NumeroALetra, CSistema.FormatoMoneda(Total))

    End Sub

    Sub SeleccionarProducto()

        txtCantidad.Focus()

    End Sub

    Sub LimpiarControles()

        txtCantidad.txt.Text = "0"
        txtProducto.txt.txt.Clear()

    End Sub

    Sub ManejarTecla(ByRef e As System.Windows.Forms.KeyEventArgs)

        If vNuevo Then
            If e.KeyCode = vgKeyNuevoRegistro Then
                Nuevo()
            End If
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtNroComprobante.txt.Text
            ID = CInt(ID) + 1
            txtNroComprobante.SetValue(ID)
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtNroComprobante.txt.Text

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtNroComprobante.SetValue(ID)
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(NroComprobante), 1) From VRemision Where IDPuntoExpedicion=" & txtIDTimbrado.GetValue & " "), Integer)

            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = vgKeyConsultar Then

            'Si no esta en el cliente
            If txtCliente.IsFocus = True Then
                Exit Sub
            End If

            'Si esta en el producto
            If txtProducto.Focus = True Then
                Dim IDCliente As String
                If txtCliente.Registro Is Nothing Then
                    Exit Sub
                End If

                IDCliente = txtCliente.Registro("ID").ToString

                Exit Sub
            End If

            Buscar()
        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(NroComprobante), 1) From VRemision Where IDPuntoExpedicion=" & txtIDTimbrado.GetValue & " "), Integer)

            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub SeleccionarTimbrado()

        Dim frm As New frmSeleccionarTimbrado
        If IsNumeric(txtIDTimbrado.GetValue) = True Then
            frm.ID = txtIDTimbrado.GetValue
        End If
        frm.IDOperacion = IDOperacion
        frm.IDSucursal = vgIDSucursal

        FGMostrarFormulario(Me, frm, "Timbrados", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Seleccionado = False Then
            Exit Sub
        End If

        txtIDTimbrado.SetValue(frm.ID)
        ObtenerInformacionPuntoVenta()

    End Sub

    Private Sub frm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub frm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frm_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Down Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Or e.KeyCode = Keys.Up Then

            'Solo ejecutar fuera de ciertos controles
            If txtCantidad.txt.Focused = True Then
                Exit Sub
            End If

            If dgvLista.Focused = True Then
                Exit Sub
            End If

        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
        LimpiarControles()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvLista.KeyUp

        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If

    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub txtCliente_ItemMalSeleccionado(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCliente.ItemMalSeleccionado

        'Limpiar los otros controles
        txtDireccionDestino.txt.Clear()

        'Datos Comunes
        txtProducto.IDCliente = Nothing

    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado
        ObtenerInformacionClientes()
        If vNuevo = True Then
            txtFechaInicio.txt.Focus()
        End If
    End Sub

    Private Sub txtProducto_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then
            SeleccionarProducto()
        End If
    End Sub

    Private Sub txtCantidad_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.TeclaPrecionada

        If txtCantidad.txt.Focused = False Then
            Exit Sub
        End If

        'Validar cantidad
        If txtCantidad.ObtenerValor = 0 Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If

    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir(True)
    End Sub

    Private Sub lklTimbrado_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklTimbrado.LinkClicked
        SeleccionarTimbrado()
    End Sub

    Private Sub cbxDepositoOperacion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxDepositoOperacion.PropertyChanged
        txtProducto.IDDeposito = cbxDepositoOperacion.GetValue
    End Sub

End Class
﻿Public Class frmTotalGeneral
    'CLASES
    Dim CSistemas As New CSistema
    Dim CReporte As New Reporte.CReporteCobranza

    'VARIABLES
    Dim vdt As DataTable
    Dim vdtCobranzas As DataTable
    Dim vdtCheques As DataTable
    Dim vdtChequesContado As DataTable
    Dim vdtDocumentos As DataTable
    Dim vdtOrdenPago As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'TextBox
        txtFecha.SetValue(Date.Now)

        'Funciones
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Sucursal
        CSistemas.SqlToComboBox(cbxSucursal, "Select ID, Codigo From Sucursal Order By 2")

        'Vendedor
        CSistemas.SqlToComboBox(cbxVendedor, "Select ID, Nombres From Vendedor Order By 2")

        'Tipo Producto
        CSistemas.SqlToComboBox(cbxTipoProducto, "Select ID, Descripcion From TipoProducto Order By 2")

        'Unidad  Medida
        CSistemas.SqlToComboBox(cbxUnidadMedida, "Select ID, Descripcion From UnidadMedida Order By 2")

        'Producto
        CSistemas.SqlToComboBox(cbxProducto, "Select ID, Concat(Referencia,'-',Descripcion) as Descripcion From Producto where ID = " & cbxTipoProducto.GetValue & " Order by 2")

        'Lista Precio
        CSistemas.SqlToComboBox(cbxListaPrecio, "Select ID, Lista From vListaPrecio Order By 2")
    End Sub

    Sub Listar()
        Dim vMes As Integer = txtFecha.MesFecha
        Dim vAño As Integer = txtFecha.AñoFecha
        Dim vFiltro3Meses As String = ""
        Dim vFiltrMesAnterior As String = ""
        Dim VFiltroMMAA As String = " and Mes = " & vMes & " and año = " & vAño - 1

        'Si es mes de Enero - Mes anterior
        If vMes = 1 Then
            vFiltrMesAnterior = " and Mes = 12 and Año = " & vAño - 1
        Else
            vFiltrMesAnterior = " and Mes = " & vMes - 1 & " and año = " & vAño
        End If

        'Promedio 3 Meses
        If vMes = 1 Then 'Enero
            vFiltro3Meses = " and Mes in (10,11,12) and Mes = " & vAño - 1
        ElseIf vMes = 2 Then 'Febrero
            vFiltro3Meses = " and ((Mes in (11,12) and Año = " & vAño - 1 & ") or (Mes = " & vMes - 1 & "))"
        ElseIf vMes = 3 Then
            vFiltro3Meses = " and ((Mes in (1,2) and Año = " & vAño & ") or (Mes = 12 and Año = " & vAño - 1 & "))"
        ElseIf vMes > 3 Then
            vFiltro3Meses = " and Mes in (" & vMes - 1 & ", " & vMes - 2 & ", " & vMes - 3 & ") and año = " & vAño
        End If



        Dim vFiltro As String = ""
        If chkSucursal.Valor = True Then
            vFiltro = vFiltro & " and IDSucursal = " & cbxSucursal.GetValue
        End If
        If chkVendedor.Valor = True Then
            vFiltro = vFiltro & " and IDVendedor = " & cbxVendedor.GetValue
        End If
        If chkTipoProducto.Valor = True Then
            vFiltro = vFiltro & " and IDTipoProducto = " & cbxTipoProducto.GetValue
        End If
        If chkUnidadMedida.Valor = True Then
            vFiltro = vFiltro & " and IDUnidadMedida = " & cbxUnidadMedida.GetValue
        End If
        If chkProducto.Valor = True Then
            vFiltro = vFiltro & " and IDProducto = " & cbxProducto.GetValue
        End If
        If chkListaPrecio.Valor = True Then
            vFiltro = vFiltro & " and IDListaPrecio = " & cbxListaPrecio.GetValue
        End If

        vdt = CSistemas.ExecuteToDataTable("select IDTransaccion,Mes, Año, Sucursal, Vendedor, TipoProducto,UnidadMedida,Producto,ListaPrecio, Cantidad, Importe,Peso from VInformeGerencialVenta where Cast(Fecha as date) <=  '" & CSistemas.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' and Mes = Month('" & CSistemas.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "') and Año = Year('" & CSistemas.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "')" & vFiltro)

        'Listar en la Grilla
        CSistemas.dtToGrid(dgvLista, vdt)


        txtDiasHabiles.txt.Text = CType(CSistemas.ExecuteScalar("select count(*) from Calendario where MONTH(fecha) = MONTH('" & CSistemas.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "') and YEAR(fecha) = YEAR('" & CSistemas.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "') and Habil = 'True'"), String)
        txtDiasTranscurridos.txt.Text = CType(CSistemas.ExecuteScalar("select count(*) from Calendario where MONTH(fecha) = MONTH('" & CSistemas.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "') and YEAR(fecha) = YEAR('" & CSistemas.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "') and Habil = 'True' and Fecha < '" & CSistemas.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "'"), String)
        txtRestan.txt.Text = txtDiasHabiles.txt.Text - txtDiasTranscurridos.txt.Text
        If txtDiasTranscurridos.txt.Text > 0 And txtDiasHabiles.txt.Text > 0 Then
            txtPorcentajeDias.txt.Text = CSistemas.FormatoNumero((txtDiasTranscurridos.txt.Text * 100) / txtDiasHabiles.txt.Text, True)
        Else
            txtPorcentajeDias.txt.Text = "0"
        End If
        txtMetaKg.txt.Text = CType(CSistemas.ExecuteScalar("select IsNull(Sum(MetaKg),0) from Meta where Mes = MONTH('" & CSistemas.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "') and año = YEAR('" & CSistemas.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' ) " & vFiltro), String)

        txtVentaMesAnterior.txt.Text = CType(CSistemas.ExecuteScalar("select IsNull(Sum(Peso),0) from VInformeGerencialVenta where 1 = 1 " & vFiltrMesAnterior & vFiltro), String)
        Dim MMetaMesAnterior As Decimal = CType(CSistemas.ExecuteScalar("select IsNull(Sum(MetaKg),0) from Meta where 1 = 1 " & vFiltrMesAnterior & vFiltro), String)
        If txtVentaMesAnterior.txt.Text > 0 And MMetaMesAnterior > 0 Then
            txtMesAnterior.txt.Text = CSistemas.FormatoNumero((txtVentaMesAnterior.txt.Text * 100) / MMetaMesAnterior, True)
        Else
            txtMesAnterior.txt.Text = "0"
        End If

        txtMMAA.txt.Text = CType(CSistemas.ExecuteScalar("select IsNull(Sum(Peso),0) from VInformeGerencialVenta where 1 = 1 " & VFiltroMMAA & vFiltro), String)
        Dim MMetaMMAA As Decimal = CType(CSistemas.ExecuteScalar("select IsNull(Sum(MetaKg),0) from Meta where 1 = 1 " & VFiltroMMAA & vFiltro), String)
        If txtMMAA.txt.Text > 0 And MMetaMMAA > 0 Then
            txtProcentajeMMAA.txt.Text = CSistemas.FormatoNumero((txtMMAA.txt.Text * 100) / MMetaMMAA, True)
        Else
            txtProcentajeMMAA.txt.Text = "0"
        End If

        txtVentasPromedio3M.txt.Text = CType(CSistemas.ExecuteScalar("select IsNull(Sum(Peso),0) from VInformeGerencialVenta where 1 = 1 " & vFiltro3Meses & vFiltro), String)
        Dim MMeta3M As Decimal = CType(CSistemas.ExecuteScalar("select IsNull(Sum(MetaKg),0) from Meta where 1 = 1 " & vFiltro3Meses & vFiltro), String)
        If txtVentasPromedio3M.txt.Text > 0 And MMeta3M > 0 Then
            txtPorcentajePromedio3M.txt.Text = CSistemas.FormatoNumero((txtVentasPromedio3M.txt.Text * 100) / MMeta3M, True)
        Else
            txtPorcentajePromedio3M.txt.Text = "0"
        End If

        'Totalizador
        If vdt Is Nothing Then
            Exit Sub
        End If

        'If Nuevo = False Then

        '    'Quitamos el total
        '    For Each oRow As DataRow In vdt.Select("Lote='" & NroPlanilla & "'")
        '        vdt.Rows.Remove(oRow)
        '        Exit For
        '    Next

        'End If

        Dim NewRow As DataRow = vdt.NewRow
        'NewRow("Lote") = ""
        'For c As Integer = 1 To vdt.Columns.Count - 1
        '    NewRow(c) = CSistemas.dtSumColumn(vdt, vdt.Columns(c).ColumnName)
        'Next

        vdt.Rows.Add(NewRow)

        'Formato
        dgvLista.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        For c As Integer = 1 To dgvLista.ColumnCount - 1
            dgvLista.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            dgvLista.Columns(c).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLista.Columns(c).DefaultCellStyle.Format = "N0"
        Next

        dgvLista.Rows(dgvLista.Rows.Count - 1).DefaultCellStyle.Font = New Font(Me.Font.FontFamily.Name, 8, FontStyle.Bold)
        CalcularTotales()
    End Sub

    Sub CalcularTotales()

        Dim TotalComprobantes As Decimal = 0
        Dim Saldo As Decimal = 0
        Dim KilosVendidos As Decimal = 0
        Dim CantidadComprobantes As Decimal = 0

        For Each item As DataGridViewRow In dgvLista.Rows
            'TotalComprobantes = TotalComprobantes + CDbl(item.Cells("colTotal").Value)
            'CantidadComprobantes = CantidadComprobantes + 1
            'KilosVendidos = KilosVendidos + CDbl(dgvLista.CurrentRow.Cells("Peso").Value)
            If Not (item.Cells("IDTransaccion").Value Is DBNull.Value) Then
                KilosVendidos = KilosVendidos + CDbl(item.Cells("Peso").Value)
            End If
        Next
        txtKilosVendidos.SetValue(KilosVendidos)
        If KilosVendidos > 0 And txtMetaKg.txt.Text > 0 Then
            txtPorcentajeMeta.txt.Text = CSistemas.FormatoNumero((KilosVendidos * 100) / txtMetaKg.txt.Text, True)
        Else
            txtPorcentajeMeta.txt.Text = "0"
        End If
        txtVentaIdeal.txt.Text = CSistemas.FormatoNumero((txtMetaKg.txt.Text / txtDiasHabiles.txt.Text) * txtDiasTranscurridos.txt.Text, False)
        txtPorcentajeIdeal.txt.Text = txtPorcentajeDias.txt.Text
        txtKilosProyectados.txt.Text = CSistemas.FormatoNumero((KilosVendidos / txtDiasTranscurridos.txt.Text) * txtDiasHabiles.txt.Text, False)
        If txtKilosProyectados.txt.Text > 0 And txtMetaKg.txt.Text > 0 Then
            txtPorcentajePromedioProyectado.txt.Text = CSistemas.FormatoNumero((txtKilosProyectados.txt.Text * 100) / txtMetaKg.txt.Text, True)
        Else
            txtPorcentajePromedioProyectado.txt.Text = "0"
        End If

    End Sub

    Private Sub frmTotalGeneral_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkVendedor_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkUnidadMedida_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkUnidadMedida.PropertyChanged
        cbxUnidadMedida.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkProducto.PropertyChanged
        cbxProducto.Enabled = value
    End Sub

    Private Sub chkListaPrecio_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkListaPrecio.PropertyChanged
        cbxListaPrecio.Enabled = value
    End Sub

    Private Sub btnListar_Click(sender As System.Object, e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmTotalGeneral_Activate()
        Me.Refresh()
    End Sub
End Class
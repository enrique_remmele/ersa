﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTotalGeneral
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkUnidadMedida = New ERP.ocxCHK()
        Me.cbxUnidadMedida = New ERP.ocxCBX()
        Me.chkProducto = New ERP.ocxCHK()
        Me.cbxProducto = New ERP.ocxCBX()
        Me.chkListaPrecio = New ERP.ocxCHK()
        Me.cbxListaPrecio = New ERP.ocxCBX()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dgvLista = New System.Windows.Forms.DataGridView()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtSobreAfrecho = New ERP.ocxTXTNumeric()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtPrecioPromedio = New ERP.ocxTXTNumeric()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtPorcentajePromedio3M = New ERP.ocxTXTNumeric()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtVentasPromedio3M = New ERP.ocxTXTNumeric()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtProcentajeMMAA = New ERP.ocxTXTNumeric()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtMMAA = New ERP.ocxTXTNumeric()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtMesAnterior = New ERP.ocxTXTNumeric()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtVentaMesAnterior = New ERP.ocxTXTNumeric()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtPorcentajePromedioProyectado = New ERP.ocxTXTNumeric()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtKilosProyectados = New ERP.ocxTXTNumeric()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtVentaIdeal = New ERP.ocxTXTNumeric()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtPorcentajeIdeal = New ERP.ocxTXTNumeric()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPorcentajeMeta = New ERP.ocxTXTNumeric()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtMetaKg = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtKilosVendidos = New ERP.ocxTXTNumeric()
        Me.lblTotalVenta = New System.Windows.Forms.Label()
        Me.txtDiasHabiles = New ERP.ocxTXTNumeric()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtDiasTranscurridos = New ERP.ocxTXTNumeric()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtRestan = New ERP.ocxTXTNumeric()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtPorcentajeDias = New ERP.ocxTXTNumeric()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 2)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(1, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.70968!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 268.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 106.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1030, 441)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.lblFecha)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtFecha)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkSucursal)
        Me.FlowLayoutPanel1.Controls.Add(Me.cbxSucursal)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkVendedor)
        Me.FlowLayoutPanel1.Controls.Add(Me.cbxVendedor)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkTipoProducto)
        Me.FlowLayoutPanel1.Controls.Add(Me.cbxTipoProducto)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkUnidadMedida)
        Me.FlowLayoutPanel1.Controls.Add(Me.cbxUnidadMedida)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkProducto)
        Me.FlowLayoutPanel1.Controls.Add(Me.cbxProducto)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkListaPrecio)
        Me.FlowLayoutPanel1.Controls.Add(Me.cbxListaPrecio)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnListar)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1024, 61)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'lblFecha
        '
        Me.lblFecha.Location = New System.Drawing.Point(3, 0)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(43, 29)
        Me.lblFecha.TabIndex = 15
        Me.lblFecha.Text = "Fecha:"
        Me.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 7, 18, 9, 5, 29, 681)
        Me.txtFecha.Location = New System.Drawing.Point(52, 3)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 23)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 16
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(132, 3)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(71, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 4
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(209, 3)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(78, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 3
        Me.cbxSucursal.Texto = ""
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(293, 3)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(76, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 6
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = Nothing
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = Nothing
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = Nothing
        Me.cbxVendedor.DataSource = Nothing
        Me.cbxVendedor.DataValueMember = Nothing
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = Nothing
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(375, 3)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = True
        Me.cbxVendedor.Size = New System.Drawing.Size(227, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 5
        Me.cbxVendedor.Texto = ""
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(608, 3)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(99, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 8
        Me.chkTipoProducto.Texto = "Tipo.Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = Nothing
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = Nothing
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = Nothing
        Me.cbxTipoProducto.DataSource = Nothing
        Me.cbxTipoProducto.DataValueMember = Nothing
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(713, 3)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = True
        Me.cbxTipoProducto.Size = New System.Drawing.Size(227, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 7
        Me.cbxTipoProducto.Texto = ""
        '
        'chkUnidadMedida
        '
        Me.chkUnidadMedida.BackColor = System.Drawing.Color.Transparent
        Me.chkUnidadMedida.Color = System.Drawing.Color.Empty
        Me.chkUnidadMedida.Location = New System.Drawing.Point(3, 32)
        Me.chkUnidadMedida.Name = "chkUnidadMedida"
        Me.chkUnidadMedida.Size = New System.Drawing.Size(82, 21)
        Me.chkUnidadMedida.SoloLectura = False
        Me.chkUnidadMedida.TabIndex = 10
        Me.chkUnidadMedida.Texto = "Unid.Med:"
        Me.chkUnidadMedida.Valor = False
        '
        'cbxUnidadMedida
        '
        Me.cbxUnidadMedida.CampoWhere = Nothing
        Me.cbxUnidadMedida.CargarUnaSolaVez = False
        Me.cbxUnidadMedida.DataDisplayMember = Nothing
        Me.cbxUnidadMedida.DataFilter = Nothing
        Me.cbxUnidadMedida.DataOrderBy = Nothing
        Me.cbxUnidadMedida.DataSource = Nothing
        Me.cbxUnidadMedida.DataValueMember = Nothing
        Me.cbxUnidadMedida.dtSeleccionado = Nothing
        Me.cbxUnidadMedida.Enabled = False
        Me.cbxUnidadMedida.FormABM = Nothing
        Me.cbxUnidadMedida.Indicaciones = Nothing
        Me.cbxUnidadMedida.Location = New System.Drawing.Point(91, 32)
        Me.cbxUnidadMedida.Name = "cbxUnidadMedida"
        Me.cbxUnidadMedida.SeleccionMultiple = False
        Me.cbxUnidadMedida.SeleccionObligatoria = True
        Me.cbxUnidadMedida.Size = New System.Drawing.Size(69, 21)
        Me.cbxUnidadMedida.SoloLectura = False
        Me.cbxUnidadMedida.TabIndex = 9
        Me.cbxUnidadMedida.Texto = ""
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(166, 32)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(76, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 12
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'cbxProducto
        '
        Me.cbxProducto.CampoWhere = Nothing
        Me.cbxProducto.CargarUnaSolaVez = False
        Me.cbxProducto.DataDisplayMember = Nothing
        Me.cbxProducto.DataFilter = Nothing
        Me.cbxProducto.DataOrderBy = Nothing
        Me.cbxProducto.DataSource = Nothing
        Me.cbxProducto.DataValueMember = Nothing
        Me.cbxProducto.dtSeleccionado = Nothing
        Me.cbxProducto.Enabled = False
        Me.cbxProducto.FormABM = Nothing
        Me.cbxProducto.Indicaciones = Nothing
        Me.cbxProducto.Location = New System.Drawing.Point(248, 32)
        Me.cbxProducto.Name = "cbxProducto"
        Me.cbxProducto.SeleccionMultiple = False
        Me.cbxProducto.SeleccionObligatoria = True
        Me.cbxProducto.Size = New System.Drawing.Size(299, 21)
        Me.cbxProducto.SoloLectura = False
        Me.cbxProducto.TabIndex = 11
        Me.cbxProducto.Texto = ""
        '
        'chkListaPrecio
        '
        Me.chkListaPrecio.BackColor = System.Drawing.Color.Transparent
        Me.chkListaPrecio.Color = System.Drawing.Color.Empty
        Me.chkListaPrecio.Location = New System.Drawing.Point(553, 32)
        Me.chkListaPrecio.Name = "chkListaPrecio"
        Me.chkListaPrecio.Size = New System.Drawing.Size(99, 21)
        Me.chkListaPrecio.SoloLectura = False
        Me.chkListaPrecio.TabIndex = 14
        Me.chkListaPrecio.Texto = "Lista.Precio:"
        Me.chkListaPrecio.Valor = False
        '
        'cbxListaPrecio
        '
        Me.cbxListaPrecio.CampoWhere = Nothing
        Me.cbxListaPrecio.CargarUnaSolaVez = False
        Me.cbxListaPrecio.DataDisplayMember = Nothing
        Me.cbxListaPrecio.DataFilter = Nothing
        Me.cbxListaPrecio.DataOrderBy = Nothing
        Me.cbxListaPrecio.DataSource = Nothing
        Me.cbxListaPrecio.DataValueMember = Nothing
        Me.cbxListaPrecio.dtSeleccionado = Nothing
        Me.cbxListaPrecio.Enabled = False
        Me.cbxListaPrecio.FormABM = Nothing
        Me.cbxListaPrecio.Indicaciones = Nothing
        Me.cbxListaPrecio.Location = New System.Drawing.Point(658, 32)
        Me.cbxListaPrecio.Name = "cbxListaPrecio"
        Me.cbxListaPrecio.SeleccionMultiple = False
        Me.cbxListaPrecio.SeleccionObligatoria = True
        Me.cbxListaPrecio.Size = New System.Drawing.Size(203, 21)
        Me.cbxListaPrecio.SoloLectura = False
        Me.cbxListaPrecio.TabIndex = 13
        Me.cbxListaPrecio.Texto = ""
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(867, 32)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(75, 23)
        Me.btnListar.TabIndex = 17
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvLista)
        Me.Panel1.Location = New System.Drawing.Point(3, 70)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1024, 259)
        Me.Panel1.TabIndex = 1
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Location = New System.Drawing.Point(-3, -3)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.Size = New System.Drawing.Size(1030, 262)
        Me.dgvLista.TabIndex = 3
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txtPorcentajeDias)
        Me.Panel2.Controls.Add(Me.Label24)
        Me.Panel2.Controls.Add(Me.txtRestan)
        Me.Panel2.Controls.Add(Me.Label23)
        Me.Panel2.Controls.Add(Me.txtDiasTranscurridos)
        Me.Panel2.Controls.Add(Me.Label22)
        Me.Panel2.Controls.Add(Me.txtDiasHabiles)
        Me.Panel2.Controls.Add(Me.Label21)
        Me.Panel2.Controls.Add(Me.txtSobreAfrecho)
        Me.Panel2.Controls.Add(Me.Label19)
        Me.Panel2.Controls.Add(Me.Label20)
        Me.Panel2.Controls.Add(Me.txtPrecioPromedio)
        Me.Panel2.Controls.Add(Me.Label17)
        Me.Panel2.Controls.Add(Me.Label18)
        Me.Panel2.Controls.Add(Me.txtPorcentajePromedio3M)
        Me.Panel2.Controls.Add(Me.Label15)
        Me.Panel2.Controls.Add(Me.Label16)
        Me.Panel2.Controls.Add(Me.txtVentasPromedio3M)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.Label14)
        Me.Panel2.Controls.Add(Me.txtProcentajeMMAA)
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Controls.Add(Me.txtMMAA)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.txtMesAnterior)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.txtVentaMesAnterior)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.txtPorcentajePromedioProyectado)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.txtKilosProyectados)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.txtVentaIdeal)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.txtPorcentajeIdeal)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.txtPorcentajeMeta)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.txtMetaKg)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.txtKilosVendidos)
        Me.Panel2.Controls.Add(Me.lblTotalVenta)
        Me.Panel2.Location = New System.Drawing.Point(3, 338)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1024, 100)
        Me.Panel2.TabIndex = 2
        '
        'txtSobreAfrecho
        '
        Me.txtSobreAfrecho.Color = System.Drawing.Color.Empty
        Me.txtSobreAfrecho.Decimales = True
        Me.txtSobreAfrecho.Indicaciones = Nothing
        Me.txtSobreAfrecho.Location = New System.Drawing.Point(950, 75)
        Me.txtSobreAfrecho.Name = "txtSobreAfrecho"
        Me.txtSobreAfrecho.Size = New System.Drawing.Size(71, 22)
        Me.txtSobreAfrecho.SoloLectura = True
        Me.txtSobreAfrecho.TabIndex = 37
        Me.txtSobreAfrecho.TabStop = False
        Me.txtSobreAfrecho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSobreAfrecho.Texto = "0"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(888, 84)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(57, 13)
        Me.Label19.TabIndex = 36
        Me.Label19.Text = "s/ Afrecho"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(888, 68)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(37, 13)
        Me.Label20.TabIndex = 35
        Me.Label20.Text = "Precio"
        '
        'txtPrecioPromedio
        '
        Me.txtPrecioPromedio.Color = System.Drawing.Color.Empty
        Me.txtPrecioPromedio.Decimales = True
        Me.txtPrecioPromedio.Indicaciones = Nothing
        Me.txtPrecioPromedio.Location = New System.Drawing.Point(818, 75)
        Me.txtPrecioPromedio.Name = "txtPrecioPromedio"
        Me.txtPrecioPromedio.Size = New System.Drawing.Size(64, 22)
        Me.txtPrecioPromedio.SoloLectura = True
        Me.txtPrecioPromedio.TabIndex = 34
        Me.txtPrecioPromedio.TabStop = False
        Me.txtPrecioPromedio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrecioPromedio.Texto = "0"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(779, 84)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(34, 13)
        Me.Label17.TabIndex = 33
        Me.Label17.Text = "Prom."
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(779, 68)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(37, 13)
        Me.Label18.TabIndex = 32
        Me.Label18.Text = "Precio"
        '
        'txtPorcentajePromedio3M
        '
        Me.txtPorcentajePromedio3M.Color = System.Drawing.Color.Empty
        Me.txtPorcentajePromedio3M.Decimales = True
        Me.txtPorcentajePromedio3M.Indicaciones = Nothing
        Me.txtPorcentajePromedio3M.Location = New System.Drawing.Point(732, 75)
        Me.txtPorcentajePromedio3M.Name = "txtPorcentajePromedio3M"
        Me.txtPorcentajePromedio3M.Size = New System.Drawing.Size(42, 22)
        Me.txtPorcentajePromedio3M.SoloLectura = True
        Me.txtPorcentajePromedio3M.TabIndex = 31
        Me.txtPorcentajePromedio3M.TabStop = False
        Me.txtPorcentajePromedio3M.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajePromedio3M.Texto = "0"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(688, 84)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(28, 13)
        Me.Label15.TabIndex = 30
        Me.Label15.Text = "3 M."
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(688, 68)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(45, 13)
        Me.Label16.TabIndex = 29
        Me.Label16.Text = "% Prom."
        '
        'txtVentasPromedio3M
        '
        Me.txtVentasPromedio3M.Color = System.Drawing.Color.Empty
        Me.txtVentasPromedio3M.Decimales = True
        Me.txtVentasPromedio3M.Indicaciones = Nothing
        Me.txtVentasPromedio3M.Location = New System.Drawing.Point(594, 75)
        Me.txtVentasPromedio3M.Name = "txtVentasPromedio3M"
        Me.txtVentasPromedio3M.Size = New System.Drawing.Size(90, 22)
        Me.txtVentasPromedio3M.SoloLectura = True
        Me.txtVentasPromedio3M.TabIndex = 28
        Me.txtVentasPromedio3M.TabStop = False
        Me.txtVentasPromedio3M.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtVentasPromedio3M.Texto = "0"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(539, 84)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(52, 13)
        Me.Label13.TabIndex = 27
        Me.Label13.Text = "Prom. 3M"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(539, 68)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(40, 13)
        Me.Label14.TabIndex = 26
        Me.Label14.Text = "Ventas"
        '
        'txtProcentajeMMAA
        '
        Me.txtProcentajeMMAA.Color = System.Drawing.Color.Empty
        Me.txtProcentajeMMAA.Decimales = True
        Me.txtProcentajeMMAA.Indicaciones = Nothing
        Me.txtProcentajeMMAA.Location = New System.Drawing.Point(495, 75)
        Me.txtProcentajeMMAA.Name = "txtProcentajeMMAA"
        Me.txtProcentajeMMAA.Size = New System.Drawing.Size(42, 22)
        Me.txtProcentajeMMAA.SoloLectura = True
        Me.txtProcentajeMMAA.TabIndex = 25
        Me.txtProcentajeMMAA.TabStop = False
        Me.txtProcentajeMMAA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtProcentajeMMAA.Texto = "0"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(446, 79)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(47, 13)
        Me.Label12.TabIndex = 24
        Me.Label12.Text = "%MMAA"
        '
        'txtMMAA
        '
        Me.txtMMAA.Color = System.Drawing.Color.Empty
        Me.txtMMAA.Decimales = True
        Me.txtMMAA.Indicaciones = Nothing
        Me.txtMMAA.Location = New System.Drawing.Point(338, 75)
        Me.txtMMAA.Name = "txtMMAA"
        Me.txtMMAA.Size = New System.Drawing.Size(102, 22)
        Me.txtMMAA.SoloLectura = True
        Me.txtMMAA.TabIndex = 23
        Me.txtMMAA.TabStop = False
        Me.txtMMAA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMMAA.Texto = "0"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(297, 79)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(39, 13)
        Me.Label11.TabIndex = 22
        Me.Label11.Text = "MMAA"
        '
        'txtMesAnterior
        '
        Me.txtMesAnterior.Color = System.Drawing.Color.Empty
        Me.txtMesAnterior.Decimales = True
        Me.txtMesAnterior.Indicaciones = Nothing
        Me.txtMesAnterior.Location = New System.Drawing.Point(248, 75)
        Me.txtMesAnterior.Name = "txtMesAnterior"
        Me.txtMesAnterior.Size = New System.Drawing.Size(42, 22)
        Me.txtMesAnterior.SoloLectura = True
        Me.txtMesAnterior.TabIndex = 21
        Me.txtMesAnterior.TabStop = False
        Me.txtMesAnterior.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMesAnterior.Texto = "0"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(179, 84)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(65, 13)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "mes Anterior"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(179, 68)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(58, 13)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Porcentaje"
        '
        'txtVentaMesAnterior
        '
        Me.txtVentaMesAnterior.Color = System.Drawing.Color.Empty
        Me.txtVentaMesAnterior.Decimales = True
        Me.txtVentaMesAnterior.Indicaciones = Nothing
        Me.txtVentaMesAnterior.Location = New System.Drawing.Point(72, 75)
        Me.txtVentaMesAnterior.Name = "txtVentaMesAnterior"
        Me.txtVentaMesAnterior.Size = New System.Drawing.Size(101, 22)
        Me.txtVentaMesAnterior.SoloLectura = True
        Me.txtVentaMesAnterior.TabIndex = 18
        Me.txtVentaMesAnterior.TabStop = False
        Me.txtVentaMesAnterior.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtVentaMesAnterior.Texto = "0"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 84)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(43, 13)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Anterior"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 68)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 13)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Venta Mes"
        '
        'txtPorcentajePromedioProyectado
        '
        Me.txtPorcentajePromedioProyectado.Color = System.Drawing.Color.Empty
        Me.txtPorcentajePromedioProyectado.Decimales = True
        Me.txtPorcentajePromedioProyectado.Indicaciones = Nothing
        Me.txtPorcentajePromedioProyectado.Location = New System.Drawing.Point(980, 35)
        Me.txtPorcentajePromedioProyectado.Name = "txtPorcentajePromedioProyectado"
        Me.txtPorcentajePromedioProyectado.Size = New System.Drawing.Size(41, 22)
        Me.txtPorcentajePromedioProyectado.SoloLectura = True
        Me.txtPorcentajePromedioProyectado.TabIndex = 15
        Me.txtPorcentajePromedioProyectado.TabStop = False
        Me.txtPorcentajePromedioProyectado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajePromedioProyectado.Texto = "0"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(899, 38)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 13)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "% Proyectado:"
        '
        'txtKilosProyectados
        '
        Me.txtKilosProyectados.Color = System.Drawing.Color.Empty
        Me.txtKilosProyectados.Decimales = True
        Me.txtKilosProyectados.Indicaciones = Nothing
        Me.txtKilosProyectados.Location = New System.Drawing.Point(796, 35)
        Me.txtKilosProyectados.Name = "txtKilosProyectados"
        Me.txtKilosProyectados.Size = New System.Drawing.Size(101, 22)
        Me.txtKilosProyectados.SoloLectura = True
        Me.txtKilosProyectados.TabIndex = 13
        Me.txtKilosProyectados.TabStop = False
        Me.txtKilosProyectados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtKilosProyectados.Texto = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(705, 38)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(90, 13)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Kgs Proyectados:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'txtVentaIdeal
        '
        Me.txtVentaIdeal.Color = System.Drawing.Color.Empty
        Me.txtVentaIdeal.Decimales = True
        Me.txtVentaIdeal.Indicaciones = Nothing
        Me.txtVentaIdeal.Location = New System.Drawing.Point(508, 35)
        Me.txtVentaIdeal.Name = "txtVentaIdeal"
        Me.txtVentaIdeal.Size = New System.Drawing.Size(101, 22)
        Me.txtVentaIdeal.SoloLectura = True
        Me.txtVentaIdeal.TabIndex = 11
        Me.txtVentaIdeal.TabStop = False
        Me.txtVentaIdeal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtVentaIdeal.Texto = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(444, 38)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Venta Ideal:"
        '
        'txtPorcentajeIdeal
        '
        Me.txtPorcentajeIdeal.Color = System.Drawing.Color.Empty
        Me.txtPorcentajeIdeal.Decimales = True
        Me.txtPorcentajeIdeal.Indicaciones = Nothing
        Me.txtPorcentajeIdeal.Location = New System.Drawing.Point(658, 35)
        Me.txtPorcentajeIdeal.Name = "txtPorcentajeIdeal"
        Me.txtPorcentajeIdeal.Size = New System.Drawing.Size(42, 22)
        Me.txtPorcentajeIdeal.SoloLectura = True
        Me.txtPorcentajeIdeal.TabIndex = 9
        Me.txtPorcentajeIdeal.TabStop = False
        Me.txtPorcentajeIdeal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajeIdeal.Texto = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(613, 38)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "% Ideal:"
        '
        'txtPorcentajeMeta
        '
        Me.txtPorcentajeMeta.Color = System.Drawing.Color.Empty
        Me.txtPorcentajeMeta.Decimales = True
        Me.txtPorcentajeMeta.Indicaciones = Nothing
        Me.txtPorcentajeMeta.Location = New System.Drawing.Point(398, 35)
        Me.txtPorcentajeMeta.Name = "txtPorcentajeMeta"
        Me.txtPorcentajeMeta.Size = New System.Drawing.Size(42, 22)
        Me.txtPorcentajeMeta.SoloLectura = True
        Me.txtPorcentajeMeta.TabIndex = 7
        Me.txtPorcentajeMeta.TabStop = False
        Me.txtPorcentajeMeta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajeMeta.Texto = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(342, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "% s/Meta:"
        '
        'txtMetaKg
        '
        Me.txtMetaKg.Color = System.Drawing.Color.Empty
        Me.txtMetaKg.Decimales = True
        Me.txtMetaKg.Indicaciones = Nothing
        Me.txtMetaKg.Location = New System.Drawing.Point(59, 35)
        Me.txtMetaKg.Name = "txtMetaKg"
        Me.txtMetaKg.Size = New System.Drawing.Size(101, 22)
        Me.txtMetaKg.SoloLectura = True
        Me.txtMetaKg.TabIndex = 5
        Me.txtMetaKg.TabStop = False
        Me.txtMetaKg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMetaKg.Texto = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Meta Kg:"
        '
        'txtKilosVendidos
        '
        Me.txtKilosVendidos.Color = System.Drawing.Color.Empty
        Me.txtKilosVendidos.Decimales = True
        Me.txtKilosVendidos.Indicaciones = Nothing
        Me.txtKilosVendidos.Location = New System.Drawing.Point(237, 35)
        Me.txtKilosVendidos.Name = "txtKilosVendidos"
        Me.txtKilosVendidos.Size = New System.Drawing.Size(101, 22)
        Me.txtKilosVendidos.SoloLectura = True
        Me.txtKilosVendidos.TabIndex = 3
        Me.txtKilosVendidos.TabStop = False
        Me.txtKilosVendidos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtKilosVendidos.Texto = "0"
        '
        'lblTotalVenta
        '
        Me.lblTotalVenta.AutoSize = True
        Me.lblTotalVenta.Location = New System.Drawing.Point(163, 38)
        Me.lblTotalVenta.Name = "lblTotalVenta"
        Me.lblTotalVenta.Size = New System.Drawing.Size(75, 13)
        Me.lblTotalVenta.TabIndex = 2
        Me.lblTotalVenta.Text = "Kgs Vendidos:"
        '
        'txtDiasHabiles
        '
        Me.txtDiasHabiles.Color = System.Drawing.Color.Empty
        Me.txtDiasHabiles.Decimales = True
        Me.txtDiasHabiles.Indicaciones = Nothing
        Me.txtDiasHabiles.Location = New System.Drawing.Point(80, 7)
        Me.txtDiasHabiles.Name = "txtDiasHabiles"
        Me.txtDiasHabiles.Size = New System.Drawing.Size(32, 22)
        Me.txtDiasHabiles.SoloLectura = True
        Me.txtDiasHabiles.TabIndex = 39
        Me.txtDiasHabiles.TabStop = False
        Me.txtDiasHabiles.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDiasHabiles.Texto = "0"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(5, 10)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(71, 13)
        Me.Label21.TabIndex = 38
        Me.Label21.Text = "Dias Habilies:"
        '
        'txtDiasTranscurridos
        '
        Me.txtDiasTranscurridos.Color = System.Drawing.Color.Empty
        Me.txtDiasTranscurridos.Decimales = True
        Me.txtDiasTranscurridos.Indicaciones = Nothing
        Me.txtDiasTranscurridos.Location = New System.Drawing.Point(216, 7)
        Me.txtDiasTranscurridos.Name = "txtDiasTranscurridos"
        Me.txtDiasTranscurridos.Size = New System.Drawing.Size(32, 22)
        Me.txtDiasTranscurridos.SoloLectura = True
        Me.txtDiasTranscurridos.TabIndex = 41
        Me.txtDiasTranscurridos.TabStop = False
        Me.txtDiasTranscurridos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDiasTranscurridos.Texto = "0"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(115, 10)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(98, 13)
        Me.Label22.TabIndex = 40
        Me.Label22.Text = "Dias Transcurridos:"
        '
        'txtRestan
        '
        Me.txtRestan.Color = System.Drawing.Color.Empty
        Me.txtRestan.Decimales = True
        Me.txtRestan.Indicaciones = Nothing
        Me.txtRestan.Location = New System.Drawing.Point(308, 7)
        Me.txtRestan.Name = "txtRestan"
        Me.txtRestan.Size = New System.Drawing.Size(32, 22)
        Me.txtRestan.SoloLectura = True
        Me.txtRestan.TabIndex = 43
        Me.txtRestan.TabStop = False
        Me.txtRestan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRestan.Texto = "0"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(263, 10)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(44, 13)
        Me.Label23.TabIndex = 42
        Me.Label23.Text = "Restan:"
        '
        'txtPorcentajeDias
        '
        Me.txtPorcentajeDias.Color = System.Drawing.Color.Empty
        Me.txtPorcentajeDias.Decimales = True
        Me.txtPorcentajeDias.Indicaciones = Nothing
        Me.txtPorcentajeDias.Location = New System.Drawing.Point(396, 7)
        Me.txtPorcentajeDias.Name = "txtPorcentajeDias"
        Me.txtPorcentajeDias.Size = New System.Drawing.Size(44, 22)
        Me.txtPorcentajeDias.SoloLectura = True
        Me.txtPorcentajeDias.TabIndex = 45
        Me.txtPorcentajeDias.TabStop = False
        Me.txtPorcentajeDias.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajeDias.Texto = "0"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(350, 10)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(42, 13)
        Me.Label24.TabIndex = 44
        Me.Label24.Text = "% Dias:"
        '
        'frmTotalGeneral
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1031, 453)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmTotalGeneral"
        Me.Text = "frmTotalGeneral"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents chkUnidadMedida As ERP.ocxCHK
    Friend WithEvents cbxUnidadMedida As ERP.ocxCBX
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents cbxProducto As ERP.ocxCBX
    Friend WithEvents chkListaPrecio As ERP.ocxCHK
    Friend WithEvents cbxListaPrecio As ERP.ocxCBX
    Friend WithEvents btnListar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvLista As System.Windows.Forms.DataGridView
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtKilosProyectados As ERP.ocxTXTNumeric
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtVentaIdeal As ERP.ocxTXTNumeric
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtPorcentajeIdeal As ERP.ocxTXTNumeric
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtPorcentajeMeta As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtMetaKg As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtKilosVendidos As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalVenta As System.Windows.Forms.Label
    Friend WithEvents txtPorcentajePromedioProyectado As ERP.ocxTXTNumeric
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtMesAnterior As ERP.ocxTXTNumeric
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtVentaMesAnterior As ERP.ocxTXTNumeric
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtSobreAfrecho As ERP.ocxTXTNumeric
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtPrecioPromedio As ERP.ocxTXTNumeric
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtPorcentajePromedio3M As ERP.ocxTXTNumeric
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtVentasPromedio3M As ERP.ocxTXTNumeric
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtProcentajeMMAA As ERP.ocxTXTNumeric
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtMMAA As ERP.ocxTXTNumeric
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtPorcentajeDias As ERP.ocxTXTNumeric
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtRestan As ERP.ocxTXTNumeric
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtDiasTranscurridos As ERP.ocxTXTNumeric
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtDiasHabiles As ERP.ocxTXTNumeric
    Friend WithEvents Label21 As System.Windows.Forms.Label
End Class

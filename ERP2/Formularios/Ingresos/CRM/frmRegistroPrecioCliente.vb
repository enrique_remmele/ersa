﻿Public Class frmRegistroPrecioCliente

    Dim CSistema As New CSistema
    Dim dtRegistroPrecioCliente As DataTable
    Dim dtExtras As DataTable
    Dim CodigoClienteSeleccionado As String
    Dim PrecioUnitario As String
    Dim Listando As Boolean = False
    Public Property Precargar As Boolean = False
    Public Property IDProductoPrecargar As Integer = 0
    Public Property IDClientePrecargar As Integer = 0

    Sub Inicializar()
        txtProducto.Conectar()
        If Precargar Then
            Consultar()
        End If
    End Sub

    Sub Consultar()
        Dim IDTipoProducto As Integer = 0
        Dim IDProducto As Integer = 0
        Dim IDCliente As Integer = 0
        Dim IDTipoCliente As Integer = 0

        If Precargar Then
            IDProducto = IDProductoPrecargar
            IDCliente = IDClientePrecargar
        Else


            IDTipoProducto = IIf(chkTipoProducto.chk.Checked = True, cbxTipoProducto.cbx.SelectedValue, 0)

            If chkProducto.chk.Checked Then
                If txtProducto.Seleccionado Then
                    IDproducto = txtProducto.Registro("ID")
                End If
            End If


            IDCliente = IIf(chkCliente.chk.Checked = True, cbxCliente.cbx.SelectedValue, 0)
            IDTipoCliente = IIf(chkTipoCliente.chk.Checked = True, cbxTipoCliente.cbx.SelectedValue, 0)
        End If

        Dim SQL As String = "Exec SpViewRegistroPrecioCliente "

        CSistema.ConcatenarParametro(SQL, "@Año", txtAño.Value)
        CSistema.ConcatenarParametro(SQL, "@IDTipoProducto", IDTipoProducto)
        CSistema.ConcatenarParametro(SQL, "@IDProducto", IDproducto)
        CSistema.ConcatenarParametro(SQL, "@IDTipoCliente", IDTipoCliente)
        CSistema.ConcatenarParametro(SQL, "@IDCliente", IDCliente)

        Dim frm As New frmLoadingForm
        frm.Operacion = "SQLtoDataTable"
        frm.Consulta = SQL
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()
        dtRegistroPrecioCliente = frm.dtRetorno

        Listar()

    End Sub

    Sub ConsultarExtras(ByVal CodigoSeleccionado As String)

        If CodigoClienteSeleccionado = CodigoSeleccionado Then
            Exit Sub
        End If
        CodigoClienteSeleccionado = CodigoSeleccionado

        Dim SQL As String = "Exec SpViewRegistroPrecioClienteDatosExtra "

        CSistema.ConcatenarParametro(SQL, "@Año", txtAño.Value)
        CSistema.ConcatenarParametro(SQL, "@CodigoCliente", CodigoClienteSeleccionado)

        Dim frm As New frmLoadingForm
        frm.Operacion = "SQLtoDataTable"
        frm.Consulta = SQL
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()
        dtExtras = frm.dtRetorno

        ListarDatosExtras()

    End Sub

    Sub Listar()

        If chkIvaIncluido.chk.Checked Then
            PrecioUnitario = "PrecioUnitario"
        Else
            PrecioUnitario = "PrecioUnitarioDiscriminado"
        End If

        Listando = True

        Dim dtMostrar As New DataTable

        dtMostrar.Columns.Add("CodigoCliente", GetType(String))
        dtMostrar.Columns.Add("Cliente", GetType(String))
        dtMostrar.Columns.Add("CodigoProducto", GetType(String))
        dtMostrar.Columns.Add("Producto", GetType(String))
        'dtMostrar.Columns.Add("CodigoCliente", GetType(String))

        dtMostrar.Columns.Add("Precio1", GetType(Integer))
        dtMostrar.Columns.Add("Cantidad1", GetType(Integer))
        dtMostrar.Columns.Add("Devolucion1", GetType(Integer))

        dtMostrar.Columns.Add("Precio2", GetType(Integer))
        dtMostrar.Columns.Add("Cantidad2", GetType(Integer))
        dtMostrar.Columns.Add("Devolucion2", GetType(Integer))

        dtMostrar.Columns.Add("Precio3", GetType(Integer))
        dtMostrar.Columns.Add("Cantidad3", GetType(Integer))
        dtMostrar.Columns.Add("Devolucion3", GetType(Integer))

        dtMostrar.Columns.Add("Precio4", GetType(Integer))
        dtMostrar.Columns.Add("Cantidad4", GetType(Integer))
        dtMostrar.Columns.Add("Devolucion4", GetType(Integer))

        dtMostrar.Columns.Add("Precio5", GetType(Integer))
        dtMostrar.Columns.Add("Cantidad5", GetType(Integer))
        dtMostrar.Columns.Add("Devolucion5", GetType(Integer))

        dtMostrar.Columns.Add("Precio6", GetType(Integer))
        dtMostrar.Columns.Add("Cantidad6", GetType(Integer))
        dtMostrar.Columns.Add("Devolucion6", GetType(Integer))

        dtMostrar.Columns.Add("Precio7", GetType(Integer))
        dtMostrar.Columns.Add("Cantidad7", GetType(Integer))
        dtMostrar.Columns.Add("Devolucion7", GetType(Integer))

        dtMostrar.Columns.Add("Precio8", GetType(Integer))
        dtMostrar.Columns.Add("Cantidad8", GetType(Integer))
        dtMostrar.Columns.Add("Devolucion8", GetType(Integer))

        dtMostrar.Columns.Add("Precio9", GetType(Integer))
        dtMostrar.Columns.Add("Cantidad9", GetType(Integer))
        dtMostrar.Columns.Add("Devolucion9", GetType(Integer))

        dtMostrar.Columns.Add("Precio10", GetType(Integer))
        dtMostrar.Columns.Add("Cantidad10", GetType(Integer))
        dtMostrar.Columns.Add("Devolucion10", GetType(Integer))

        dtMostrar.Columns.Add("Precio11", GetType(Integer))
        dtMostrar.Columns.Add("Cantidad11", GetType(Integer))
        dtMostrar.Columns.Add("Devolucion11", GetType(Integer))

        dtMostrar.Columns.Add("Precio12", GetType(Integer))
        dtMostrar.Columns.Add("Cantidad12", GetType(Integer))
        dtMostrar.Columns.Add("Devolucion12", GetType(Integer))

        Dim MesAnterior As Integer = 0
        Dim CodigoClienteAnterior As String = ""
        Dim CodigoProductoAnterior As String = ""
        Dim NuevaFila As Boolean = False

        For Each oRow As DataRow In dtRegistroPrecioCliente.Rows

            If dtMostrar.Rows.Count = 0 Then
                AgregarFila(dtMostrar, oRow)
                MesAnterior = oRow("Mes")
                CodigoClienteAnterior = oRow("CodigoCliente")
                CodigoProductoAnterior = oRow("CodigoProducto")
                GoTo seguir
            End If

            If oRow("Devoluciones") > 0 Then
                Dim a As String = "1"
            End If

            If CodigoClienteAnterior <> oRow("CodigoCliente") Or CodigoProductoAnterior <> oRow("CodigoProducto") Then
                NuevaFila = True
            Else
                If MesAnterior = oRow("Mes") Then
                    If dtMostrar.Select("CodigoCliente = '" & oRow("CodigoCliente") & "' and CodigoProducto = '" & oRow("CodigoProducto") & "' and Precio" & oRow("Mes") & " is NULL").Count = 0 Then
                        NuevaFila = True
                    End If

                End If
            End If

            If NuevaFila Then
                AgregarFila(dtMostrar, oRow)
            Else
                ActualizarFila(dtMostrar, oRow)
            End If
            MesAnterior = oRow("Mes")
            CodigoClienteAnterior = oRow("CodigoCliente")
            CodigoProductoAnterior = oRow("CodigoProducto")
            NuevaFila = False

seguir:

        Next
        CSistema.dtToGrid(dgvLista, dtMostrar)

        For Each Col As DataGridViewColumn In dgvLista.Columns

            'Col.DefaultCellStyle.BackColor = Color.White
            If Col.Index < 4 Then
                Col.DefaultCellStyle.BackColor = Color.White
            End If

            'Enero
            If Col.Index >= 4 And Col.Index < 7 Then
                Col.DefaultCellStyle.BackColor = Color.LightGray
            End If

            'Febrero
            If Col.Index >= 7 And Col.Index < 10 Then
                Col.DefaultCellStyle.BackColor = Color.GhostWhite
            End If

            'Marzo
            If Col.Index >= 10 And Col.Index < 13 Then
                Col.DefaultCellStyle.BackColor = Color.LightGray
            End If

            'Abril
            If Col.Index >= 13 And Col.Index < 16 Then
                Col.DefaultCellStyle.BackColor = Color.GhostWhite
            End If

            'Mayo
            If Col.Index >= 16 And Col.Index < 19 Then
                Col.DefaultCellStyle.BackColor = Color.LightGray
            End If

            'Junio
            If Col.Index >= 19 And Col.Index < 22 Then
                Col.DefaultCellStyle.BackColor = Color.GhostWhite
            End If

            'Julio
            If Col.Index >= 22 And Col.Index < 25 Then
                Col.DefaultCellStyle.BackColor = Color.LightGray
            End If

            'Agosto
            If Col.Index >= 25 And Col.Index < 28 Then
                Col.DefaultCellStyle.BackColor = Color.GhostWhite
            End If

            'Septiembre
            If Col.Index >= 28 And Col.Index < 31 Then
                Col.DefaultCellStyle.BackColor = Color.LightGray
            End If

            'Octubre
            If Col.Index >= 31 And Col.Index < 34 Then
                Col.DefaultCellStyle.BackColor = Color.GhostWhite
            End If

            'Noviembre
            If Col.Index >= 34 And Col.Index < 37 Then
                Col.DefaultCellStyle.BackColor = Color.LightGray
            End If

            'Diciembre
            If Col.Index >= 37 And Col.Index < 40 Then
                Col.DefaultCellStyle.BackColor = Color.GhostWhite
            End If

        Next

        For i = 4 To dgvLista.ColumnCount - 1
            dgvLista.Columns(i).DefaultCellStyle.Format = "N0"
        Next

        dgvLista.Columns("Precio1").HeaderText = "Precio Enero"
        dgvLista.Columns("Cantidad1").HeaderText = "Cantidad Enero"
        dgvLista.Columns("Devolucion1").HeaderText = "Devolucion Enero"

        dgvLista.Columns("Precio2").HeaderText = "Precio Febrero"
        dgvLista.Columns("Cantidad2").HeaderText = "Cantidad Febrero"
        dgvLista.Columns("Devolucion2").HeaderText = "Devolucion Febrero"

        dgvLista.Columns("Precio3").HeaderText = "Precio Marzo"
        dgvLista.Columns("Cantidad3").HeaderText = "Cantidad Marzo"
        dgvLista.Columns("Devolucion3").HeaderText = "Devolucion Marzo"

        dgvLista.Columns("Precio4").HeaderText = "Precio Abril"
        dgvLista.Columns("Cantidad4").HeaderText = "Cantidad Abril"
        dgvLista.Columns("Devolucion4").HeaderText = "Devolucion Abril"

        dgvLista.Columns("Precio5").HeaderText = "Precio Mayo"
        dgvLista.Columns("Cantidad5").HeaderText = "Cantidad Mayo"
        dgvLista.Columns("Devolucion5").HeaderText = "Devolucion Mayo"

        dgvLista.Columns("Precio6").HeaderText = "Precio Junio"
        dgvLista.Columns("Cantidad6").HeaderText = "Cantidad Junio"
        dgvLista.Columns("Devolucion6").HeaderText = "Devolucion Junio"

        dgvLista.Columns("Precio7").HeaderText = "Precio Julio"
        dgvLista.Columns("Cantidad7").HeaderText = "Cantidad Julio"
        dgvLista.Columns("Devolucion7").HeaderText = "Devolucion Julio"

        dgvLista.Columns("Precio8").HeaderText = "Precio Agosto"
        dgvLista.Columns("Cantidad8").HeaderText = "Cantidad Agosto"
        dgvLista.Columns("Devolucion8").HeaderText = "Devolucion Agosto"

        dgvLista.Columns("Precio9").HeaderText = "Precio Septiembre"
        dgvLista.Columns("Cantidad9").HeaderText = "Cantidad Septiembre"
        dgvLista.Columns("Devolucion9").HeaderText = "Devolucion Septiembre"

        dgvLista.Columns("Precio10").HeaderText = "Precio Octubre"
        dgvLista.Columns("Cantidad10").HeaderText = "Cantidad Octubre"
        dgvLista.Columns("Devolucion10").HeaderText = "Devolucion Octubre"

        dgvLista.Columns("Precio11").HeaderText = "Precio Noviembre"
        dgvLista.Columns("Cantidad11").HeaderText = "Cantidad Noviembre"
        dgvLista.Columns("Devolucion11").HeaderText = "Devolucion Noviembre"

        dgvLista.Columns("Precio12").HeaderText = "Precio Diciembre"
        dgvLista.Columns("Cantidad12").HeaderText = "Cantidad Diciembre"
        dgvLista.Columns("Devolucion12").HeaderText = "Devolucion Diciembre"

        Listando = False

    End Sub

    Sub ActualizarFila(ByVal dt As DataTable, ByVal oRow As DataRow)

        Dim Precio As Integer = 0
        Dim Cantidad As Integer = 0
        Dim Devoluciones As Integer = 0

        Precio = oRow(PrecioUnitario)
        Cantidad = oRow("Cantidad")
        Devoluciones = oRow("Devoluciones")

        Dim nRow As DataRow = dt.Select("CodigoCliente = '" & oRow("CodigoCliente") & "' and CodigoProducto = '" & oRow("CodigoProducto") & "' and [Precio" & oRow("Mes") & "] is NULL")(0)

        Select Case oRow("Mes")
            Case 1
                nRow("Cantidad1") = Cantidad
                nRow("Devolucion1") = Devoluciones
                nRow("Precio1") = Precio
            Case 2
                nRow("Cantidad2") = Cantidad
                nRow("Devolucion2") = Devoluciones
                nRow("Precio2") = Precio
            Case 3
                nRow("Cantidad3") = Cantidad
                nRow("Devolucion3") = Devoluciones
                nRow("Precio3") = Precio
            Case 4
                nRow("Cantidad4") = Cantidad
                nRow("Devolucion4") = Devoluciones
                nRow("Precio4") = Precio
            Case 5
                nRow("Cantidad5") = Cantidad
                nRow("Devolucion5") = Devoluciones
                nRow("Precio5") = Precio
            Case 6
                nRow("Cantidad6") = Cantidad
                nRow("Devolucion6") = Devoluciones
                nRow("Precio6") = Precio
            Case 7
                nRow("Cantidad7") = Cantidad
                nRow("Devolucion7") = Devoluciones
                nRow("Precio7") = Precio
            Case 8
                nRow("Cantidad8") = Cantidad
                nRow("Devolucion8") = Devoluciones
                nRow("Precio8") = Precio
            Case 9
                nRow("Cantidad9") = Cantidad
                nRow("Devolucion9") = Devoluciones
                nRow("Precio9") = Precio
            Case 10
                nRow("Cantidad10") = Cantidad
                nRow("Devolucion10") = Devoluciones
                nRow("Precio10") = Precio
            Case 11
                nRow("Cantidad11") = Cantidad
                nRow("Devolucion11") = Devoluciones
                nRow("Precio11") = Precio
            Case 12
                nRow("Cantidad12") = Cantidad
                nRow("Devolucion12") = Devoluciones
                nRow("Precio12") = Precio
        End Select

    End Sub

    Sub AgregarFila(ByVal dt As DataTable, ByVal oRow As DataRow)

        Dim nRow As DataRow = dt.NewRow

        Dim Precio As Integer = 0
        Dim Cantidad As Integer = 0
        Dim Devoluciones As Integer = 0

        nRow("CodigoCliente") = oRow("CodigoCliente")
        nRow("Cliente") = oRow("Cliente")
        nRow("CodigoProducto") = oRow("CodigoProducto")
        nRow("Producto") = oRow("Producto")

        Precio = oRow(PrecioUnitario)
        Cantidad = oRow("Cantidad")
        Devoluciones = oRow("Devoluciones")

        nRow("Precio1") = IIf(oRow("Mes") = 1, Precio, DBNull.Value)
        nRow("Cantidad1") = IIf(oRow("Mes") = 1, Cantidad, DBNull.Value)
        nRow("Devolucion1") = IIf(oRow("Mes") = 1, Devoluciones, DBNull.Value)

        nRow("Precio2") = IIf(oRow("Mes") = 2, Precio, DBNull.Value)
        nRow("Cantidad2") = IIf(oRow("Mes") = 2, Cantidad, DBNull.Value)
        nRow("Devolucion2") = IIf(oRow("Mes") = 2, Devoluciones, DBNull.Value)

        nRow("Precio3") = IIf(oRow("Mes") = 3, Precio, DBNull.Value)
        nRow("Cantidad3") = IIf(oRow("Mes") = 3, Cantidad, DBNull.Value)
        nRow("Devolucion3") = IIf(oRow("Mes") = 3, Devoluciones, DBNull.Value)

        nRow("Precio4") = IIf(oRow("Mes") = 4, Precio, DBNull.Value)
        nRow("Cantidad4") = IIf(oRow("Mes") = 4, Cantidad, DBNull.Value)
        nRow("Devolucion4") = IIf(oRow("Mes") = 4, Devoluciones, DBNull.Value)

        nRow("Precio5") = IIf(oRow("Mes") = 5, Precio, DBNull.Value)
        nRow("Cantidad5") = IIf(oRow("Mes") = 5, Cantidad, DBNull.Value)
        nRow("Devolucion5") = IIf(oRow("Mes") = 5, Devoluciones, DBNull.Value)

        nRow("Precio6") = IIf(oRow("Mes") = 6, Precio, DBNull.Value)
        nRow("Cantidad6") = IIf(oRow("Mes") = 6, Cantidad, DBNull.Value)
        nRow("Devolucion6") = IIf(oRow("Mes") = 6, Devoluciones, DBNull.Value)

        nRow("Precio7") = IIf(oRow("Mes") = 7, Precio, DBNull.Value)
        nRow("Cantidad7") = IIf(oRow("Mes") = 7, Cantidad, DBNull.Value)
        nRow("Devolucion7") = IIf(oRow("Mes") = 7, Devoluciones, DBNull.Value)

        nRow("Precio8") = IIf(oRow("Mes") = 8, Precio, DBNull.Value)
        nRow("Cantidad8") = IIf(oRow("Mes") = 8, Cantidad, DBNull.Value)
        nRow("Devolucion8") = IIf(oRow("Mes") = 8, Devoluciones, DBNull.Value)

        nRow("Precio9") = IIf(oRow("Mes") = 9, Precio, DBNull.Value)
        nRow("Cantidad9") = IIf(oRow("Mes") = 9, Cantidad, DBNull.Value)
        nRow("Devolucion9") = IIf(oRow("Mes") = 9, Devoluciones, DBNull.Value)

        nRow("Precio10") = IIf(oRow("Mes") = 10, Precio, DBNull.Value)
        nRow("Cantidad10") = IIf(oRow("Mes") = 10, Cantidad, DBNull.Value)
        nRow("Devolucion10") = IIf(oRow("Mes") = 10, Devoluciones, DBNull.Value)

        nRow("Precio11") = IIf(oRow("Mes") = 11, Precio, DBNull.Value)
        nRow("Cantidad11") = IIf(oRow("Mes") = 11, Cantidad, DBNull.Value)
        nRow("Devolucion11") = IIf(oRow("Mes") = 11, Devoluciones, DBNull.Value)

        nRow("Precio12") = IIf(oRow("Mes") = 12, Precio, DBNull.Value)
        nRow("Cantidad12") = IIf(oRow("Mes") = 12, Cantidad, DBNull.Value)
        nRow("Devolucion12") = IIf(oRow("Mes") = 12, Devoluciones, DBNull.Value)

        dt.Rows.Add(nRow)

    End Sub

    Sub ListarDatosExtras()

        Dim dtMostrarExtra As New DataTable

        dtMostrarExtra.Columns.Add("Dato", GetType(String))

        dtMostrarExtra.Columns.Add("1", GetType(String))

        dtMostrarExtra.Columns.Add("2", GetType(String))

        dtMostrarExtra.Columns.Add("3", GetType(String))

        dtMostrarExtra.Columns.Add("4", GetType(String))

        dtMostrarExtra.Columns.Add("5", GetType(String))

        dtMostrarExtra.Columns.Add("6", GetType(String))

        dtMostrarExtra.Columns.Add("7", GetType(String))

        dtMostrarExtra.Columns.Add("8", GetType(String))

        dtMostrarExtra.Columns.Add("9", GetType(String))

        dtMostrarExtra.Columns.Add("10", GetType(String))

        dtMostrarExtra.Columns.Add("11", GetType(String))

        dtMostrarExtra.Columns.Add("12", GetType(String))

        For Each col As DataColumn In dtExtras.Columns
            If col.ColumnName.ToString <> "Mes" Then
                Dim nRow As DataRow = dtMostrarExtra.NewRow
                nRow("Dato") = col.ColumnName.ToString
                dtMostrarExtra.Rows.Add(nRow)
            End If
        Next

        For Each Row As DataRow In dtMostrarExtra.Rows
            For Each dRow As DataRow In dtExtras.Select("[" & Row(0).ToString & "] > 0")
                Row(dRow("Mes")) = dRow(Row(0))
            Next
        Next

        CSistema.dtToGrid(dgvExtras, dtMostrarExtra)

        dgvExtras.Columns("1").HeaderText = "Enero"
        dgvExtras.Columns("2").HeaderText = "Febrero"
        dgvExtras.Columns("3").HeaderText = "Marzo"
        dgvExtras.Columns("4").HeaderText = "Abril"
        dgvExtras.Columns("5").HeaderText = "Mayo"
        dgvExtras.Columns("6").HeaderText = "Junio"
        dgvExtras.Columns("7").HeaderText = "Julio"
        dgvExtras.Columns("8").HeaderText = "Agosto"
        dgvExtras.Columns("9").HeaderText = "Septiembre"
        dgvExtras.Columns("10").HeaderText = "Octubre"
        dgvExtras.Columns("11").HeaderText = "Noviembre"
        dgvExtras.Columns("12").HeaderText = "Diciembre"

    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs) Handles btnListar.Click
        If chkCliente.chk.Checked = False And chkProducto.chk.Checked = False Then
            MessageBox.Show("Es necesario seleccionar un cliente o un producto", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Exit Sub
        End If

        Consultar()

    End Sub

    Private Sub dgvLista_SelectionChanged(sender As Object, e As EventArgs) Handles dgvLista.SelectionChanged
        If dgvLista.SelectedRows.Count = 0 Then
            Exit Sub
        End If
        If Listando Then
            Exit Sub
        End If

        ConsultarExtras(dgvLista.Rows(dgvLista.SelectedRows(0).Index).Cells("CodigoCliente").Value.ToString)
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.SoloLectura = Not value
    End Sub

    Private Sub chkProducto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkProducto.PropertyChanged
        txtProducto.SoloLectura = Not value
    End Sub

    Private Sub chkTipoCliente_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTipoCliente.PropertyChanged
        cbxTipoCliente.SoloLectura = Not value
    End Sub

    Private Sub chkCliente_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkCliente.PropertyChanged
        cbxCliente.SoloLectura = Not value
    End Sub

    Private Sub frmRegistroPrecioCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()

    End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click

    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmRegistroPrecioCliente_Activate()
        Me.Refresh()
    End Sub
End Class
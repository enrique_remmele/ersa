﻿Public Class FrmTeleventas
    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim gCliente As Integer
    Dim time As Integer = 10

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property
    'PROPIEDADES
    Private dtDocumentosValue As DataTable
    Public Property dtDocumentos() As DataTable
        Get
            Return dtDocumentosValue
        End Get
        Set(ByVal value As DataTable)
            dtDocumentosValue = value
        End Set
    End Property

    Sub Inicializar()
        Me.AcceptButton = New Button
        Focus()

        txtCliente.Focus()

        'Propiedades
        IDTransaccion = 0
        'vNuevo = True

        'Funciones
        CargarInformacion()
        'Fecha
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()
        'txtLlamarEl.Hoy()
        txtCliente.Enabled = False
        txtProducto.Enabled = False

        OcxLlamadaTeleventa1.CargarInformacion()
        OcxVisitaTeleventa1.CargarInformacion()



    End Sub


    Sub CargarInformacion()
        'Cliente
        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito  From VCliente "
        txtCliente.frm = Me

        'Producto
        txtProducto.Conectar()

        ReDim vControles(-1)
        'Cabecera
        ' CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, txtDesde)
        CSistema.CargaControl(vControles, txtHasta)
        CSistema.CargaControl(vControles, TabControl)
        CSistema.CargaControl(vControles, txtProducto)
    End Sub

    Private Sub FrmTeleventas_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub


    'Producto
    'If chkProducto.Valor = True Then
    ' If txtProducto.Seleccionado = False Then
    '     Exit Sub
    ' End If

    ' WhereDetalle = WhereDetalle & " And IDProducto=" & txtProducto.Registro("ID") & " "
    ' WhereDetalleNCR = WhereDetalleNCR & " And IDProducto=" & txtProducto.Registro("ID") & " "
    ' End If


    Private Sub TabControl_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Sub ListarProductos(ByRef Cliente As Integer, ByRef FechaDesde As String, ByRef FechaHasta As String, Optional ByRef Producto As Integer = 0)
        Dim Consulta As String = "select * from vTeleventaProducto"
        Dim Where As String = ""
        Dim OrderBy As String = " Order By UltimaCompra "

        'Si es por comprobante
        If Cliente <> 0 Then
            Where = Where & " Where IDCliente=" & Cliente
        End If

        If FechaDesde <> "" And FechaHasta <> "" Then
            Where = Where & " and UltimaCompra between " & "'" & CSistema.FormatoFechaBaseDatos(FechaDesde, True, False) & "'" & " And " & "'" & CSistema.FormatoFechaBaseDatos(FechaHasta, True, False) & "'"
        End If

        'If Producto <> 0 Then
        'Where = Where & " and IdProducto = " & Producto
        'End If

        'Listar 
        'dtDocumentos = CSistema.ExecuteToDataTable(Consulta & " " & Where & " " & OrderBy)
        'DataGridView1.DataSource = dtDocumentos

        CSistema.SqlToDataGrid(DataGridView1, Consulta & " " & Where & " " & OrderBy)

        If DataGridView1.ColumnCount = 0 Then
            Exit Sub
        End If


        'Dim ColumnasNumericas() As String = {"UltimoPrecio", "UltimaCantidad", "Total", "Maximo", "Promedio", "Veces", "Plazo"}
        CSistema.DataGridColumnasNumericas(DataGridView1, {"UltimoPrecio", "UltimaCantidad", "Total", "Maximo", "Promedio", "Veces", "Plazo"})

        ' For c As Integer = 0 To ColumnasNumericas.Length - 1
        'DataGridView1.Columns(ColumnasNumericas(c)).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        ' DataGridView1.Columns(ColumnasNumericas(c)).DefaultCellStyle.Format = "N0"
        'Next

        DataGridView1.Columns("IdProducto").Visible = False
        DataGridView1.Columns("IdCliente").Visible = False
        DataGridView1.Columns("IdVendedor").Visible = False

    End Sub

    Private Sub btnVentasErsa_Click(sender As System.Object, e As System.EventArgs) Handles btnVentasErsa.Click
        If chkCliente.Valor = False And chkProducto.Valor = False Then
            MessageBox.Show("Seleccione un cliente o producto para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        'Cliente
        If chkCliente.Valor = True And txtCliente.Seleccionado = False Then
            MessageBox.Show("Seleccione un cliente para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim vProducto As Integer = 0
        If chkProducto.Valor = True And txtProducto.txt.Texto = "" Then
            MessageBox.Show("Seleccione un producto para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If chkProducto.Valor = True And txtProducto.txt.Texto <> "" Then
            vProducto = txtProducto.Registro("ID")
        End If

        If chkCliente.Valor = True And txtCliente.Seleccionado = True Then
            ListarProductos(txtCliente.Registro("ID"), txtDesde.txt.Text, txtHasta.txt.Text, vProducto)
            ListarCompras(txtCliente.Registro("ID"))
            ListarPendientes(txtCliente.Registro("ID"))
            OcxClienteTeleventa1.IDCliente = txtCliente.Registro("ID")
            OcxClienteTeleventa1.ListarCliente()
            OcxLlamadaTeleventa1.IDCliente = txtCliente.Registro("ID")
            OcxVisitaTeleventa1.IDCliente = txtCliente.Registro("ID")
            OcxLlamadaTeleventa1.Inicializar()
            OcxVisitaTeleventa1.Inicializar()
            CargarInformacionLlamadas()
            CargarInformacionVisitas()
            TabControl.SelectedIndex = 1
        End If
        If chkProducto.Valor = True Then
            If txtProducto.Seleccionado = True Then
                ListarListadoCliente()
                TabControl.SelectedIndex = 0
                Exit Sub
            End If
        End If
        'ListarListadoCliente()


        'cbxTipoLlamada.Enabled = True
        'cbxLlamador.Enabled = True
        'cbxResultado.Enabled = True 


    End Sub

    Sub ListarCompras(ByRef Cliente As Integer)
        Dim Consulta As String = "select * from vTeleventaCompra"
        Dim Where As String = ""
        Dim OrderBy As String = " Order By FechaEmision desc "

        'Si es por comprobante
        If Cliente <> 0 Then
            Where = " Where IDCliente=" & Cliente
        End If

        If txtDesde.txt.Text <> "" And txtHasta.txt.Text <> "" Then
            Where = Where & " and FechaEmision between " & "'" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "'" & " And " & "'" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "'"
        End If

        'Listar 
        dtDocumentos = CSistema.ExecuteToDataTable(Consulta & " " & Where & " " & OrderBy)
        DataGridView2.DataSource = dtDocumentos

        If DataGridView2.ColumnCount = 0 Then
            Exit Sub
        End If


        For c As Integer = 0 To DataGridView2.ColumnCount - 1
            DataGridView2.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Next
        DataGridView2.Columns("IdTransaccion").Visible = False
        DataGridView2.Columns("IdCliente").Visible = False
        DataGridView2.Columns("FechaEmision").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView2.Columns("TipoComprobante").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView2.Columns("Comprobante").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView2.Columns("Condicion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView2.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView2.Columns("PlazoCredito").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView2.Columns("FechaVencimiento").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView2.Columns("Reparto").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView2.Columns("Camion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView2.Columns("HoraSalida").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        CSistema.DataGridColumnasNumericas(DataGridView2, {"Total", "PlazoCredito"})
    End Sub

    Sub ListarDetalleCompras()
        If DataGridView2.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim Consulta As String = "select ID,IdProducto,Producto,Cantidad,PrecioUnitario,Total,Idtransaccion from vDetalleVenta"
        Dim Where As String = ""
        Dim OrderBy As String = " Order By ID desc "

        'Si es por comprobante
        If DataGridView2.SelectedRows(0).Cells("IDTransaccion").Value <> 0 Then
            Where = " Where IdTransaccion=" & DataGridView2.SelectedRows(0).Cells("IDTransaccion").Value
        End If

        'Listar 
        dtDocumentos = CSistema.ExecuteToDataTable(Consulta & " " & Where & " " & OrderBy)
        DataGridView3.DataSource = dtDocumentos

        If DataGridView3.ColumnCount = 0 Then
            Exit Sub
        End If


        For c As Integer = 0 To DataGridView3.ColumnCount - 1
            DataGridView3.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Next

        DataGridView3.Columns("ID").Visible = False
        DataGridView3.Columns("IdProducto").Visible = False
        DataGridView3.Columns("Producto").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView3.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView3.Columns("PrecioUnitario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView3.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView3.Columns("Idtransaccion").Visible = False

        DataGridView3.Columns("Cantidad").DefaultCellStyle.Format = 0
        DataGridView3.Columns("PrecioUnitario").DefaultCellStyle.Format = 0
        DataGridView3.Columns("Total").DefaultCellStyle.Format = 0
        CSistema.DataGridColumnasNumericas(DataGridView3, {"Cantidad", "PrecioUnitario", "Total"})
    End Sub

    Private Sub DataGridView2_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles DataGridView2.SelectionChanged
        ListarDetalleCompras()
    End Sub

    Private Sub chkProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean)
        txtProducto.Enabled = value
    End Sub

    Private Sub txtCliente_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs)
        'cbxTipoLlamada.Enabled = False
        'cbxLlamador.Enabled = False
        '.Enabled = False
    End Sub

    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs)
        NuevaLLamada()
    End Sub
    Sub NuevaLLamada()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO)
        vNuevo = True
        InicializarControles()
    End Sub
    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesABM)
        'CSistema.ControlBotonesABM(Operacion, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
    End Sub
    Sub InicializarControles()

        'TextBox
        'txtObservaciones.txt.Clear()
        'txtLlamarEl.Hoy()
        'txtAtendidaPor.txt.Clear()

        'Funciones
        If vNuevo = True Then
            'txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Llamadas"), Integer)
        Else
            'Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Llamadas"), Integer))
        End If

        'Error
        'ctrError.Clear()

        'Foco
        'txtDescripcion.Focus()


    End Sub
    Sub CargarInformacionLlamadas()
        'Cliente
        If txtCliente.Seleccionado = False And chkCliente.Valor = True Then
            MessageBox.Show("Seleccione un cliente para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If
        If OcxLlamadaTeleventa1.Iniciado = False Then
            OcxLlamadaTeleventa1.Inicializar()
        End If

    End Sub

    Sub CargarInformacionVisitas()
        'Cliente
        If txtCliente.Seleccionado = False And chkCliente.Valor = True Then
            MessageBox.Show("Seleccione un cliente para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If
        If OcxVisitaTeleventa1.Iniciado = False Then
            OcxVisitaTeleventa1.Inicializar()
        End If

    End Sub

    Sub ObtenerInformacionLlamadas(ByVal ID As Integer)
        'Cliente
        If txtCliente.Seleccionado = False Then
            MessageBox.Show("Seleccione un cliente para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If
        If TabControl.SelectedTab.Name = "tabLlamadas" Then
            OcxLlamadaTeleventa1.IDCliente = ID

            OcxLlamadaTeleventa1.InicializarControles()
            OcxLlamadaTeleventa1.Listar()

        End If


    End Sub

    Sub ObtenerInformacionVisita(ByVal ID As Integer)
        'Cliente
        If txtCliente.Seleccionado = False Then
            MessageBox.Show("Seleccione un cliente para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If
        If TabControl.SelectedTab.Name = "tabLlamadas" Then
            OcxVisitaTeleventa1.IDCliente = ID

            OcxVisitaTeleventa1.InicializarControles()
            OcxVisitaTeleventa1.Listar()

        End If


    End Sub

    Private Sub TabControl_Selected(sender As System.Object, e As System.Windows.Forms.TabControlEventArgs) Handles TabControl.Selected
        If (TabControl.SelectedIndex = 5 Or TabControl.SelectedIndex = 7) Then
            If chkCliente.Valor = True And txtCliente.Seleccionado = False Then
                    MessageBox.Show("Seleccione un cliente para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    TabControl.SelectedIndex = 0
            End If
            
        End If

        If e.TabPageIndex = 4 And chkCliente.Valor = True Then
            'Cliente
            If txtCliente.Seleccionado = False Then
                MessageBox.Show("Seleccione un cliente para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                TabControl.SelectedIndex = 0
                txtCliente.Focus()
                Exit Sub
            End If
            OcxLlamadaTeleventa1.IDCliente = txtCliente.Registro("ID")
            OcxLlamadaTeleventa1.Inicializar()
            OcxLlamadaTeleventa1.Listar()
            OcxLlamadaTeleventa1.Refresh()
            TabControl.TabPages(e.TabPageIndex).Refresh()
        End If

        If e.TabPageIndex = 6 Then
            'Cliente
            If txtCliente.Seleccionado = False And chkCliente.Valor = True Then
                MessageBox.Show("Seleccione un cliente para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                TabControl.SelectedIndex = 0
                txtCliente.Focus()
                Exit Sub
            End If
            If chkCliente.Valor = True And txtCliente.Seleccionado = True Then
                OcxVisitaTeleventa1.IDCliente = txtCliente.Registro("ID")
                OcxVisitaTeleventa1.Inicializar()
                OcxVisitaTeleventa1.Listar()
                OcxVisitaTeleventa1.Refresh()
                TabControl.TabPages(e.TabPageIndex).Refresh()
            End If
        End If

        If e.TabPageIndex = 2 Then
            'Cliente
            If txtCliente.Seleccionado = False And chkCliente.Valor = True Then
                MessageBox.Show("Seleccione un cliente para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                TabControl.SelectedIndex = 0
                txtCliente.Focus()
                Exit Sub
            End If
            If chkCliente.Valor = True And txtCliente.Seleccionado = True Then
                OcxClienteTeleventa1.ListarCliente()
            End If
        End If
    End Sub

    Sub ListarPendientes(Optional ByRef Cliente As Integer = 0)
        Dim Consulta As String = "select * from vPendienteTeleventa"
        Dim Where As String = "where 1 = 1 "
        Dim OrderBy As String = " Order By FechaEmision "

        'Si es por comprobante
        If Cliente <> 0 Then
            Where = Where & " and IDCliente=" & Cliente
        End If

        If txtDesde.txt.Text <> "" And txtHasta.txt.Text <> "" Then
            'Where = Where & " and FechaEmision between " & "'" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "'" & " And " & "'" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "'"
        End If


        'Listar 
        dtDocumentos = CSistema.ExecuteToDataTable(Consulta & " " & Where & " " & OrderBy)
        DataGridView4.DataSource = dtDocumentos

        If DataGridView4.ColumnCount = 0 Then
            Exit Sub
        End If


        For c As Integer = 0 To DataGridView4.ColumnCount - 1
            DataGridView4.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Next


        DataGridView4.Columns("IdCliente").Visible = False
        DataGridView4.Columns("Operacion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView4.Columns("SucursalCliente").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView4.Columns("FechaEmision").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView4.Columns("FechaVencimiento").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView4.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView4.Columns("Cobrado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView4.Columns("Saldo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        CSistema.DataGridColumnasNumericas(DataGridView4, {"Total", "Cobrado", "Saldo"})
    End Sub

    Sub ListarListadoCliente()
        Dim Consulta As String = "select * from vTeleventaListadoCliente"
        Dim Where As String = "where 1 = 1 "
        Dim OrderBy As String = " Order By UltimaCompra "

        'Si es por comprobante
        If chkCliente.Valor = True And txtCliente.Seleccionado = True Then
            Where = Where & " and IDCliente=" & txtCliente.Registro("ID")
        End If

        If txtDesde.txt.Text <> "" And txtHasta.txt.Text <> "" Then
            Where = Where & " and UltimaCompra between " & "'" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "'" & " And " & "'" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "'"
        End If

        If chkProducto.Valor = True And txtProducto.Seleccionado = True Then
                Where = Where & " and IdProducto = " & txtProducto.Registro("ID")

        End If

        'Listar 
        dtDocumentos = CSistema.ExecuteToDataTable(Consulta & " " & Where & " " & OrderBy)
        dgvClientes.DataSource = dtDocumentos

        If dgvClientes.ColumnCount = 0 Then
            Exit Sub
        End If


        For c As Integer = 0 To dgvClientes.ColumnCount - 1
            dgvClientes.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Next


        dgvClientes.Columns("IdProducto").Visible = False

        dgvClientes.Columns("Cliente").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvClientes.Columns("RazonSocial").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvClientes.Columns("IdCliente").Visible = False
        dgvClientes.Columns("UltimaCompra").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvClientes.Columns("UltimoPrecio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvClientes.Columns("UltimaCantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvClientes.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvClientes.Columns("Maximo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvClientes.Columns("Promedio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvClientes.Columns("Veces").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvClientes.Columns("IdVendedor").Visible = False
        dgvClientes.Columns("Vendedor").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvClientes.Columns("Plazo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

    End Sub

    Private Sub chkProducto_PropertyChanged_1(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub dgvClientes_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles dgvClientes.SelectionChanged
        If dgvClientes.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        If dgvClientes.RowCount > 0 Then
            If dgvClientes.SelectedRows(0).Cells("IdCliente").Value <> 0 Then
                ListarProductos(dgvClientes.SelectedRows(0).Cells("IdCliente").Value, txtDesde.txt.Text, txtHasta.txt.Text, 0)
                ListarCompras(dgvClientes.SelectedRows(0).Cells("IdCliente").Value)
                ListarPendientes(dgvClientes.SelectedRows(0).Cells("IdCliente").Value)
                OcxClienteTeleventa1.IDCliente = dgvClientes.SelectedRows(0).Cells("IdCliente").Value
                OcxClienteTeleventa1.ListarCliente()
                OcxLlamadaTeleventa1.IDCliente = dgvClientes.SelectedRows(0).Cells("IdCliente").Value
                OcxVisitaTeleventa1.IDCliente = dgvClientes.SelectedRows(0).Cells("IdCliente").Value
                OcxLlamadaTeleventa1.Inicializar()
                OcxVisitaTeleventa1.Inicializar()
                CargarInformacionLlamadas()
                CargarInformacionVisitas()
                TabControl.SelectedIndex = 1
                txtCliente.SetValue(dgvClientes.SelectedRows(0).Cells("IdCliente").Value)
            End If
        End If
    End Sub

    Private Sub btnPedido_Click(sender As System.Object, e As System.EventArgs) Handles btnPedido.Click
        Dim frm As New frmPedido
        If txtCliente.txtID.txt.Text <> "" Then
            frm.Cliente = txtCliente.txtReferencia.txt.Text
            frm.IDCliente = CInt(txtCliente.txtID.txt.Text)
        End If
        frm.Show()
    End Sub

    Private Sub txtCliente_ItemMalSeleccionado(sender As System.Object, e As System.EventArgs) Handles txtCliente.ItemMalSeleccionado
        If txtCliente.txtRazonSocial.txt.Text <> "" Then
            chkProducto.Focus()
        End If

    End Sub

    Private Sub FrmTeleventas_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs)
        'If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Down Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Or e.KeyCode = Keys.Up Then

        'End If

        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub FrmTeleventas_Activate()
        Me.Refresh()
    End Sub
End Class


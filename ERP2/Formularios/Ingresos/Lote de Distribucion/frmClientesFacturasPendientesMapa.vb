﻿Imports System.IO

Public Class frmClientesFacturasPendientesMapa

    Sub Inicializar()

        Dim Archivo As String = VGCarpetaAplicacion & "\Mapa.HTM"
        Dim oSW As New StreamWriter(Archivo)
        oSW.Write(My.Resources.Mapa)
        oSW.Flush()
        WebBrowser1.Navigate(Archivo)

    End Sub

    Private Sub frmClientesFacturasPendientesMapa_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmClientesFacturasPendientesMapa_Activate()
        Me.Refresh()
    End Sub
End Class
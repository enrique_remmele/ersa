﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaLoteDistribucion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cbxSucursal = New System.Windows.Forms.ComboBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.txtCantidadDetalle = New ERP.ocxTXTNumeric()
        Me.lblCantidadDetalle = New System.Windows.Forms.Label()
        Me.chkSucursal = New System.Windows.Forms.CheckBox()
        Me.lvDetalle = New System.Windows.Forms.ListView()
        Me.lblTotalDetalle = New System.Windows.Forms.Label()
        Me.chkUsuario = New System.Windows.Forms.CheckBox()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.txtTotalDetalle = New ERP.ocxTXTNumeric()
        Me.cbxUsuario = New System.Windows.Forms.ComboBox()
        Me.txtComprobante = New System.Windows.Forms.TextBox()
        Me.chkComprobante = New System.Windows.Forms.CheckBox()
        Me.lvOperacion = New System.Windows.Forms.ListView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblTotalOperacion = New System.Windows.Forms.Label()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtTotalOperacion = New ERP.ocxTXTNumeric()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.cbxComprobante = New System.Windows.Forms.ComboBox()
        Me.btnEmitirInforme = New System.Windows.Forms.Button()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtCantidadOperacion = New ERP.ocxTXTNumeric()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkFecha = New System.Windows.Forms.CheckBox()
        Me.chkDistribuidor = New System.Windows.Forms.CheckBox()
        Me.cbxDistribuidor = New System.Windows.Forms.ComboBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblCantidadOperacion = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Panel6.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cbxSucursal
        '
        Me.cbxSucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormattingEnabled = True
        Me.cbxSucursal.Location = New System.Drawing.Point(6, 39)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.Size = New System.Drawing.Size(171, 21)
        Me.cbxSucursal.TabIndex = 1
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.txtCantidadDetalle)
        Me.Panel6.Controls.Add(Me.lblCantidadDetalle)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(3, 3)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(242, 25)
        Me.Panel6.TabIndex = 0
        '
        'txtCantidadDetalle
        '
        Me.txtCantidadDetalle.Color = System.Drawing.Color.Empty
        Me.txtCantidadDetalle.Decimales = True
        Me.txtCantidadDetalle.Indicaciones = Nothing
        Me.txtCantidadDetalle.Location = New System.Drawing.Point(61, 2)
        Me.txtCantidadDetalle.Name = "txtCantidadDetalle"
        Me.txtCantidadDetalle.Size = New System.Drawing.Size(48, 22)
        Me.txtCantidadDetalle.SoloLectura = False
        Me.txtCantidadDetalle.TabIndex = 4
        Me.txtCantidadDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadDetalle.Texto = "0"
        '
        'lblCantidadDetalle
        '
        Me.lblCantidadDetalle.AutoSize = True
        Me.lblCantidadDetalle.Location = New System.Drawing.Point(3, 7)
        Me.lblCantidadDetalle.Name = "lblCantidadDetalle"
        Me.lblCantidadDetalle.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadDetalle.TabIndex = 0
        Me.lblCantidadDetalle.Text = "Cantidad:"
        '
        'chkSucursal
        '
        Me.chkSucursal.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkSucursal.Location = New System.Drawing.Point(6, 13)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(171, 21)
        Me.chkSucursal.TabIndex = 0
        Me.chkSucursal.Text = "Sucursal:"
        Me.chkSucursal.UseVisualStyleBackColor = True
        '
        'lvDetalle
        '
        Me.lvDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvDetalle.Location = New System.Drawing.Point(3, 252)
        Me.lvDetalle.Name = "lvDetalle"
        Me.lvDetalle.Size = New System.Drawing.Size(648, 171)
        Me.lvDetalle.TabIndex = 3
        Me.lvDetalle.UseCompatibleStateImageBehavior = False
        '
        'lblTotalDetalle
        '
        Me.lblTotalDetalle.AutoSize = True
        Me.lblTotalDetalle.Location = New System.Drawing.Point(4, 7)
        Me.lblTotalDetalle.Name = "lblTotalDetalle"
        Me.lblTotalDetalle.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalDetalle.TabIndex = 0
        Me.lblTotalDetalle.Text = "Total:"
        '
        'chkUsuario
        '
        Me.chkUsuario.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkUsuario.Location = New System.Drawing.Point(6, 120)
        Me.chkUsuario.Name = "chkUsuario"
        Me.chkUsuario.Size = New System.Drawing.Size(171, 21)
        Me.chkUsuario.TabIndex = 7
        Me.chkUsuario.Text = "Usuario:"
        Me.chkUsuario.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 4
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 49.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 103.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Panel6, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel5, 2, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.txtTotalDetalle, 3, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 429)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(648, 31)
        Me.TableLayoutPanel4.TabIndex = 2
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.lblTotalDetalle)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(499, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(43, 25)
        Me.Panel5.TabIndex = 1
        '
        'txtTotalDetalle
        '
        Me.txtTotalDetalle.Color = System.Drawing.Color.Empty
        Me.txtTotalDetalle.Decimales = True
        Me.txtTotalDetalle.Indicaciones = Nothing
        Me.txtTotalDetalle.Location = New System.Drawing.Point(548, 3)
        Me.txtTotalDetalle.Name = "txtTotalDetalle"
        Me.txtTotalDetalle.Size = New System.Drawing.Size(95, 22)
        Me.txtTotalDetalle.SoloLectura = False
        Me.txtTotalDetalle.TabIndex = 4
        Me.txtTotalDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDetalle.Texto = "0"
        '
        'cbxUsuario
        '
        Me.cbxUsuario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxUsuario.Enabled = False
        Me.cbxUsuario.FormattingEnabled = True
        Me.cbxUsuario.Location = New System.Drawing.Point(6, 147)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.Size = New System.Drawing.Size(171, 21)
        Me.cbxUsuario.TabIndex = 8
        '
        'txtComprobante
        '
        Me.txtComprobante.Enabled = False
        Me.txtComprobante.Location = New System.Drawing.Point(92, 94)
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(85, 20)
        Me.txtComprobante.TabIndex = 6
        '
        'chkComprobante
        '
        Me.chkComprobante.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkComprobante.Location = New System.Drawing.Point(6, 66)
        Me.chkComprobante.Name = "chkComprobante"
        Me.chkComprobante.Size = New System.Drawing.Size(171, 21)
        Me.chkComprobante.TabIndex = 4
        Me.chkComprobante.Text = "Comprobante:"
        Me.chkComprobante.UseVisualStyleBackColor = True
        '
        'lvOperacion
        '
        Me.lvOperacion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvOperacion.Location = New System.Drawing.Point(3, 39)
        Me.lvOperacion.Name = "lvOperacion"
        Me.lvOperacion.Size = New System.Drawing.Size(648, 171)
        Me.lvOperacion.TabIndex = 1
        Me.lvOperacion.UseCompatibleStateImageBehavior = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblTotalOperacion)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(499, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(43, 24)
        Me.Panel1.TabIndex = 0
        '
        'lblTotalOperacion
        '
        Me.lblTotalOperacion.AutoSize = True
        Me.lblTotalOperacion.Location = New System.Drawing.Point(4, 4)
        Me.lblTotalOperacion.Name = "lblTotalOperacion"
        Me.lblTotalOperacion.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalOperacion.TabIndex = 0
        Me.lblTotalOperacion.Text = "Total:"
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 4
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 49.0!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 103.0!))
        Me.TableLayoutPanel5.Controls.Add(Me.Panel1, 2, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.txtTotalOperacion, 3, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.LinkLabel1, 0, 0)
        Me.TableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(3, 216)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(648, 30)
        Me.TableLayoutPanel5.TabIndex = 2
        '
        'txtTotalOperacion
        '
        Me.txtTotalOperacion.Color = System.Drawing.Color.Empty
        Me.txtTotalOperacion.Decimales = True
        Me.txtTotalOperacion.Indicaciones = Nothing
        Me.txtTotalOperacion.Location = New System.Drawing.Point(548, 3)
        Me.txtTotalOperacion.Name = "txtTotalOperacion"
        Me.txtTotalOperacion.Size = New System.Drawing.Size(95, 22)
        Me.txtTotalOperacion.SoloLectura = False
        Me.txtTotalOperacion.TabIndex = 4
        Me.txtTotalOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalOperacion.Texto = "0"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(3, 0)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(67, 13)
        Me.LinkLabel1.TabIndex = 5
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Ver en mapa"
        '
        'cbxComprobante
        '
        Me.cbxComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxComprobante.Enabled = False
        Me.cbxComprobante.FormattingEnabled = True
        Me.cbxComprobante.Location = New System.Drawing.Point(6, 93)
        Me.cbxComprobante.Name = "cbxComprobante"
        Me.cbxComprobante.Size = New System.Drawing.Size(85, 21)
        Me.cbxComprobante.TabIndex = 5
        '
        'btnEmitirInforme
        '
        Me.btnEmitirInforme.Location = New System.Drawing.Point(6, 307)
        Me.btnEmitirInforme.Name = "btnEmitirInforme"
        Me.btnEmitirInforme.Size = New System.Drawing.Size(171, 33)
        Me.btnEmitirInforme.TabIndex = 15
        Me.btnEmitirInforme.Text = "Emitir Informe"
        Me.btnEmitirInforme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEmitirInforme.UseVisualStyleBackColor = True
        '
        'dtpHasta
        '
        Me.dtpHasta.Enabled = False
        Me.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHasta.Location = New System.Drawing.Point(92, 255)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(85, 20)
        Me.dtpHasta.TabIndex = 14
        '
        'dtpDesde
        '
        Me.dtpDesde.Enabled = False
        Me.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDesde.Location = New System.Drawing.Point(6, 255)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(85, 20)
        Me.dtpDesde.TabIndex = 13
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.txtCantidadOperacion)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(591, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(54, 24)
        Me.Panel4.TabIndex = 2
        '
        'txtCantidadOperacion
        '
        Me.txtCantidadOperacion.Color = System.Drawing.Color.Empty
        Me.txtCantidadOperacion.Decimales = True
        Me.txtCantidadOperacion.Indicaciones = Nothing
        Me.txtCantidadOperacion.Location = New System.Drawing.Point(3, 1)
        Me.txtCantidadOperacion.Name = "txtCantidadOperacion"
        Me.txtCantidadOperacion.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadOperacion.SoloLectura = False
        Me.txtCantidadOperacion.TabIndex = 4
        Me.txtCantidadOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadOperacion.Texto = "0"
        '
        'Panel3
        '
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(502, 24)
        Me.Panel3.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkFecha)
        Me.GroupBox1.Controls.Add(Me.chkDistribuidor)
        Me.GroupBox1.Controls.Add(Me.cbxDistribuidor)
        Me.GroupBox1.Controls.Add(Me.chkSucursal)
        Me.GroupBox1.Controls.Add(Me.cbxSucursal)
        Me.GroupBox1.Controls.Add(Me.chkUsuario)
        Me.GroupBox1.Controls.Add(Me.cbxUsuario)
        Me.GroupBox1.Controls.Add(Me.txtComprobante)
        Me.GroupBox1.Controls.Add(Me.chkComprobante)
        Me.GroupBox1.Controls.Add(Me.cbxComprobante)
        Me.GroupBox1.Controls.Add(Me.btnEmitirInforme)
        Me.GroupBox1.Controls.Add(Me.dtpHasta)
        Me.GroupBox1.Controls.Add(Me.dtpDesde)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(663, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(183, 463)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'chkFecha
        '
        Me.chkFecha.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkFecha.Location = New System.Drawing.Point(6, 228)
        Me.chkFecha.Name = "chkFecha"
        Me.chkFecha.Size = New System.Drawing.Size(171, 21)
        Me.chkFecha.TabIndex = 18
        Me.chkFecha.Text = "Fecha"
        Me.chkFecha.UseVisualStyleBackColor = True
        '
        'chkDistribuidor
        '
        Me.chkDistribuidor.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkDistribuidor.Location = New System.Drawing.Point(6, 174)
        Me.chkDistribuidor.Name = "chkDistribuidor"
        Me.chkDistribuidor.Size = New System.Drawing.Size(171, 21)
        Me.chkDistribuidor.TabIndex = 16
        Me.chkDistribuidor.Text = "Distribuidor"
        Me.chkDistribuidor.UseVisualStyleBackColor = True
        '
        'cbxDistribuidor
        '
        Me.cbxDistribuidor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxDistribuidor.Enabled = False
        Me.cbxDistribuidor.FormattingEnabled = True
        Me.cbxDistribuidor.Location = New System.Drawing.Point(6, 201)
        Me.cbxDistribuidor.Name = "cbxDistribuidor"
        Me.cbxDistribuidor.Size = New System.Drawing.Size(171, 21)
        Me.cbxDistribuidor.TabIndex = 17
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 189.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(849, 469)
        Me.TableLayoutPanel1.TabIndex = 3
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel4, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.lvDetalle, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.lvOperacion, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel5, 0, 2)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 5
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(654, 463)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 4
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 97.27047!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.729528!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 59.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Panel2, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel3, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel4, 3, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(648, 30)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblCantidadOperacion)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(525, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(60, 24)
        Me.Panel2.TabIndex = 1
        '
        'lblCantidadOperacion
        '
        Me.lblCantidadOperacion.AutoSize = True
        Me.lblCantidadOperacion.Location = New System.Drawing.Point(3, 8)
        Me.lblCantidadOperacion.Name = "lblCantidadOperacion"
        Me.lblCantidadOperacion.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadOperacion.TabIndex = 0
        Me.lblCantidadOperacion.Text = "Cantidad:"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmConsultaLoteDistribucion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(849, 469)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmConsultaLoteDistribucion"
        Me.Tag = "CONSULTA DE LOTES DE DISTRIBUCION"
        Me.Text = "frmConsultaLoteDistribucion"
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.TableLayoutPanel5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cbxSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidadDetalle As ERP.ocxTXTNumeric
    Friend WithEvents lblCantidadDetalle As System.Windows.Forms.Label
    Friend WithEvents chkSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents lvDetalle As System.Windows.Forms.ListView
    Friend WithEvents txtTotalDetalle As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalDetalle As System.Windows.Forms.Label
    Friend WithEvents chkUsuario As System.Windows.Forms.CheckBox
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents cbxUsuario As System.Windows.Forms.ComboBox
    Friend WithEvents txtComprobante As System.Windows.Forms.TextBox
    Friend WithEvents chkComprobante As System.Windows.Forms.CheckBox
    Friend WithEvents lvOperacion As System.Windows.Forms.ListView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblTotalOperacion As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtTotalOperacion As ERP.ocxTXTNumeric
    Friend WithEvents cbxComprobante As System.Windows.Forms.ComboBox
    Friend WithEvents txtCantidadOperacion As ERP.ocxTXTNumeric
    Friend WithEvents btnEmitirInforme As System.Windows.Forms.Button
    Friend WithEvents dtpHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidadOperacion As System.Windows.Forms.Label
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents chkDistribuidor As System.Windows.Forms.CheckBox
    Friend WithEvents cbxDistribuidor As System.Windows.Forms.ComboBox
    Friend WithEvents chkFecha As System.Windows.Forms.CheckBox
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
End Class

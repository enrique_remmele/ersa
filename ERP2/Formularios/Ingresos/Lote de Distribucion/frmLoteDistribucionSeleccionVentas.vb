﻿Public Class frmLoteDistribucionSeleccionVentas

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim totalPeso As Double = 0

    'PORPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private SucursalValue As String
    Public Property Sucursal() As String
        Get
            Return SucursalValue
        End Get
        Set(ByVal value As String)
            SucursalValue = value
        End Set
    End Property

    Private ZonaValue As String
    Public Property Zona() As String
        Get
            Return ZonaValue
        End Get
        Set(ByVal value As String)
            ZonaValue = value
        End Set
    End Property

    Private ValidoValue As String
    Public Property Valido() As String
        Get
            Return ValidoValue
        End Get
        Set(ByVal value As String)
            ValidoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        txtSucursal.txt.Text = Sucursal
        txtZona.txt.Text = Zona

        Me.Text = "Seleccion de comprobantes pendientes de entrega"
        Valido = False

        ListarComprobantes()
        dgw.Focus()

    End Sub

    'Listar Comprobantes
    Sub ListarComprobantes()

        

        CSistema.dtToGrid(dgw, dt)
        dgw.AlternatingRowsDefaultCellStyle.BackColor = Color.White
        dgw.ReadOnly = True
        dgw.Columns(0).ReadOnly = False

        'For Each oRow As DataGridViewRow In dgw.Rows
        '    If oRow.Cells("").Value = Zona Then
        '        oRow.Cells("Seleccionar").Value = True
        '    Else
        '        oRow.Cells("Seleccionar").Value = False
        '    End If
        'Next

        For c As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(c).Visible = False
        Next

        dgw.Columns("Seleccionar").Visible = True
        dgw.Columns("TipoComprobante").Visible = True
        dgw.Columns("Comprobante").Visible = True
        dgw.Columns("FechaFact").Visible = True
        dgw.Columns("Condicion").Visible = True
        dgw.Columns("Moneda").Visible = True
        dgw.Columns("Total").Visible = True
        dgw.Columns("Cliente").Visible = True
        dgw.Columns("Suc. Cliente").Visible = True
        dgw.Columns("Vendedor").Visible = True
        dgw.Columns("ZonaVenta").Visible = True
        dgw.Columns("Ciudad").Visible = True
        dgw.Columns("Barrio").Visible = True
        dgw.Columns("Direccion").Visible = True
        dgw.Columns("Peso").Visible = True

        dgw.Columns("Total").DisplayIndex = 8




        'Formato de Columnas
        'dgw.Columns(6).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        'dgw.Columns(6).DefaultCellStyle.Format = "N0"
        dgw.Columns(23).DefaultCellStyle.Format = "N0"
        dgw.Columns(24).DefaultCellStyle.Format = "N0"
        dgw.Columns(25).DefaultCellStyle.Format = "N0"
        dgw.Columns(26).DefaultCellStyle.Format = "N0"
        dgw.Columns(27).DefaultCellStyle.Format = "N0"

        dgw.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dgw.Columns(23).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dgw.Columns(24).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dgw.Columns(25).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dgw.Columns(26).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dgw.Columns(27).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        ' 28 es Peso
        PintarCelda()
        'prueba peso
        Dim Total As Decimal = 0

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(0).Value = True Then
                Total = Total + Row.Cells("Peso").Value
            ElseIf Total <> 0 Then
                'Total = Total - Row.Cells("Peso").Value
            End If
        Next

        TxtPesoTotal.Text = Total
    End Sub

    Sub PintarCelda()
        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(0).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.FromArgb(vgColorSeleccionado)
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next
    End Sub

    Sub Seleccionar()
        Valido = True
        Me.Close()
    End Sub

    Private Sub frmSeleccionSucursalCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Valido = False
        Me.Close()
    End Sub

    Private Sub lvComprobante_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub lvComprobante_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)

        If e.Item.Checked = True Then
            e.Item.BackColor = Color.FromArgb(vgColorSeleccionado)
        Else
            e.Item.BackColor = Color.White

        End If

        For Each oRow As DataRow In dt.Select(" Comprobante = '" & e.Item.SubItems(0).Text & "' ")
            oRow("Seleccionado") = e.Item.Checked
        Next

    End Sub

    Private Sub cbxOrdenar_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ListarComprobantes()
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        
    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If dgw.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = True Then
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.FromArgb(vgColorSeleccionado)
        Else
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
        End If
    End Sub

    Private Sub dgw_ColumnHeaderMouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgw.ColumnHeaderMouseClick
        PintarCelda()
    End Sub

    Private Sub dgw_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Space Then

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

            For Each oRow As DataGridViewRow In dgw.Rows
                If oRow.Index = RowIndex Then
                    If oRow.Cells("Seleccionar").Value = False Then
                        oRow.Cells("Seleccionar").Value = True
                        PintarCelda()
                    Else
                        oRow.Cells("Seleccionar").Value = False
                        PintarCelda()
                    End If
                End If

            Next

            If e.KeyCode = Keys.Space Then


                If dgw.SelectedRows(0).Index = dgw.Rows.Count - 1 Then
                    Exit Sub
                End If

                dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index + 1).Cells(0)

            End If
        End If

        'prueba peso
        Dim Total As Decimal = 0

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(0).Value = True Then
                If IsNumeric(Row.Cells("Peso").Value.ToString) Then
                    Total = Total + Row.Cells("Peso").Value
                End If
            ElseIf Total <> 0 Then
                'Total = Total - Row.Cells("Peso").Value
            End If
        Next

        TxtPesoTotal.Text = Total
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmLoteDistribucionSeleccionVentas_Activate()
        Me.Refresh()
    End Sub
End Class
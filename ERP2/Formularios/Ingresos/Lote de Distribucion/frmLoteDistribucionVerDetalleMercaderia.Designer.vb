﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoteDistribucionVerDetalleMercaderia
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtPesoTotal = New System.Windows.Forms.TextBox()
        Me.lblTotalComprobantes = New System.Windows.Forms.Label()
        Me.txtTotalComprobantes = New ERP.ocxTXTNumeric()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.ListView1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(620, 451)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'ListView1
        '
        Me.ListView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListView1.Location = New System.Drawing.Point(3, 3)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(614, 415)
        Me.ListView1.TabIndex = 0
        Me.ListView1.UseCompatibleStateImageBehavior = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.TxtPesoTotal)
        Me.Panel1.Controls.Add(Me.lblTotalComprobantes)
        Me.Panel1.Controls.Add(Me.txtTotalComprobantes)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 424)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(614, 24)
        Me.Panel1.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(300, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 13)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Total de Peso:"
        '
        'TxtPesoTotal
        '
        Me.TxtPesoTotal.BackColor = System.Drawing.SystemColors.Window
        Me.TxtPesoTotal.Location = New System.Drawing.Point(376, 3)
        Me.TxtPesoTotal.Name = "TxtPesoTotal"
        Me.TxtPesoTotal.ReadOnly = True
        Me.TxtPesoTotal.Size = New System.Drawing.Size(100, 20)
        Me.TxtPesoTotal.TabIndex = 10
        Me.TxtPesoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotalComprobantes
        '
        Me.lblTotalComprobantes.AutoSize = True
        Me.lblTotalComprobantes.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblTotalComprobantes.Location = New System.Drawing.Point(477, 0)
        Me.lblTotalComprobantes.Name = "lblTotalComprobantes"
        Me.lblTotalComprobantes.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalComprobantes.TabIndex = 9
        Me.lblTotalComprobantes.Text = "Total:"
        '
        'txtTotalComprobantes
        '
        Me.txtTotalComprobantes.Color = System.Drawing.Color.Empty
        Me.txtTotalComprobantes.Decimales = True
        Me.txtTotalComprobantes.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtTotalComprobantes.Indicaciones = Nothing
        Me.txtTotalComprobantes.Location = New System.Drawing.Point(511, 0)
        Me.txtTotalComprobantes.Name = "txtTotalComprobantes"
        Me.txtTotalComprobantes.Size = New System.Drawing.Size(103, 24)
        Me.txtTotalComprobantes.SoloLectura = True
        Me.txtTotalComprobantes.TabIndex = 8
        Me.txtTotalComprobantes.TabStop = False
        Me.txtTotalComprobantes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalComprobantes.Texto = "0"
        '
        'frmLoteDistribucionVerDetalleMercaderia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(620, 451)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmLoteDistribucionVerDetalleMercaderia"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Detalle de Mercaderias"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblTotalComprobantes As System.Windows.Forms.Label
    Friend WithEvents txtTotalComprobantes As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtPesoTotal As System.Windows.Forms.TextBox
End Class

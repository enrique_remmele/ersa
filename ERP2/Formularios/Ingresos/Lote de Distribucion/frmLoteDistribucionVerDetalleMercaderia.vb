﻿Public Class frmLoteDistribucionVerDetalleMercaderia

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private ComprobanteValue As String
    Public Property Comprobante() As String
        Get
            Return ComprobanteValue
        End Get
        Set(ByVal value As String)
            ComprobanteValue = value
        End Set
    End Property

    Private dtVentaValue As DataTable
    Public Property dtVenta() As DataTable
        Get
            Return dtVentaValue
        End Get
        Set(ByVal value As DataTable)
            dtVentaValue = value
        End Set
    End Property

    Private ListarPorVentaValue As Boolean
    Public Property ListarPorVenta() As Boolean
        Get
            Return ListarPorVentaValue
        End Get
        Set(ByVal value As Boolean)
            ListarPorVentaValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.Text = "Detalle de Mercaderias para el Lote: " & Comprobante
        Listar()

        TxtPesoTotal.BackColor = Color.FromArgb(MVariablesGlobales.vgColorSoloLectura)


    End Sub

    Sub Listar()

        If ListarPorVenta = False Then
            CargarSegunLote()
        Else
            CargarSegunVenta()
        End If

        CSistema.FormatoMoneda(ListView1, 7)
        CSistema.FormatoNumero(ListView1, 4, True)
        'CSistema.FormatoNumero(ListView1, 5, True)
        CSistema.TotalesLv(ListView1, txtTotalComprobantes.txt, 7, True)
        ListView1.Columns(0).Width = 0

        Dim Total As Decimal = 0
        For Each item As ListViewItem In ListView1.Items
            If IsNumeric(item.SubItems(6).Text) = False Then
                Exit For
            End If

            Dim Col As String
            Col = item.SubItems(6).Text
            Total = Total + Col

        Next
       
        TxtPesoTotal.Text = Total


    End Sub

    Private Sub CargarSegunLote()

        Dim sql As String = "Select ID, Ref, Producto, CodigoBarra, Cantidad, Cajas, Peso, Total From VDetalleLoteDistribucion Where IDTransaccion=" & IDTransaccion & " Order By Producto"
        CSistema.SqlToLv(ListView1, sql)

    End Sub

    Private Sub CargarSegunVenta()

        Dim Where As String = ""

        If dtVenta.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each oRow As DataRow In dtVenta.Rows
            If Where = "" Then
                Where = " Where (IDTransaccion=" & oRow("IDTransaccion") & ") "
            Else
                Where = Where & " or (IDTransaccion=" & oRow("IDTransaccion") & ") "
            End If
        Next

        ' Dim dtTemp As DataTable = CSistema.ExecuteToDataTable("Select 'ID'=IDProducto, Referencia, Producto, CodigoBarra, 'Cantidad'=SUM(Cantidad), 'CantidadCaja'=SUM(CantidadCaja), 'Peso'=Sum(Peso),  'Total'=SUM(Total) From VDetalleVenta " & Where & " Group By IDProducto, Referencia, Producto, CodigoBarra ").Copy
        Dim dtTemp As DataTable = CSistema.ExecuteToDataTable("Select 'ID'=IDProducto, Referencia, Producto, CodigoBarra, 'Cantidad'=SUM(Cantidad), 'CantidadCaja'=SUM(CantidadCaja), 'Peso'=Sum(Peso),  'Total'=SUM(Total) From VVentasRemisionParaLote " & Where & " Group By IDProducto, Referencia, Producto, CodigoBarra ").Copy

        CSistema.dtToLv(ListView1, dtTemp)

    End Sub

    Private Sub frmLoteDistribucionVerDetalleMercaderia_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmLoteDistribucionVerDetalleMercaderia_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmAgregarMotivoAnulacionPedido

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Public Property Procesado As Boolean

    'CLASES
    Dim CSistema As New CSistema

    Sub Inicilizar()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select ID, Descripcion,MantenerPedidoVigente from MotivoAnulacionVenta where estado = 1 and Pedido = 1")
        cbxMotivo.DataSource = dt
        cbxMotivo.ValueMember = "ID"
        cbxMotivo.DisplayMember = "Descripcion"

        Procesado = False

    End Sub

    Sub Anular()

        If cbxMotivo.SelectedValue Is Nothing Then
            tsslEstado.Text = "Atencion: Motivo invalido"
            ctrError.SetError(cbxMotivo, "Atencion: Motivo invalido")
            ctrError.SetIconAlignment(cbxMotivo, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'Consultar
        If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        'Anulamos
        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMotivoAnulacion", cbxMotivo.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", ERP.CSistema.NUMOperacionesRegistro.ANULAR.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Anular Registro
        If CSistema.ExecuteStoreProcedure(param, "SpPedido", False, False, MensajeRetorno) = False Then
            MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            'tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            'ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            'ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)
        End If

        Procesado = True
        Me.Close()

    End Sub

    Private Sub frmAgregarMotivoAnulacionPedido_Load(sender As Object, e As EventArgs) Handles Me.Load
        Inicilizar()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Anular()
    End Sub

    Private Sub frmAgregarMotivoAnulacionPedido_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Me.Refresh()
    End Sub
End Class
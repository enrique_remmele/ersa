﻿Public Class frmAgregarMotivoReproceso
    Public Property Procesado As Boolean

    Private MotivoReprocesoValue As Integer
    Public Property MotivoReproceso() As Integer
        Get
            Return MotivoReprocesoValue
        End Get
        Set(ByVal value As Integer)
            MotivoReprocesoValue = value
        End Set
    End Property

    'CLASES
    Dim CSistema As New CSistema

    Sub Inicilizar()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select ID, Descripcion From MotivoReproceso where estado = 1 ")
        cbxMotivo.DataSource = dt
        cbxMotivo.ValueMember = "ID"
        cbxMotivo.DisplayMember = "Descripcion"

        Procesado = False

    End Sub

    Sub Aceptar()
        MotivoReproceso = cbxMotivo.SelectedValue

        Procesado = True
        Me.Close()

    End Sub

    Private Sub frmAgregarMotivoReproceso_Load(sender As Object, e As EventArgs) Handles Me.Load
        Inicilizar()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Aceptar()
    End Sub
End Class
﻿Public Class frmVentaPedido

    'CLASES
    Dim CAsiento As New CAsientoVenta
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDPedidoValue As Integer
    Public VentaPedidoAbierto As Integer

    Public Property IDPedido() As Integer
        Get
            Return IDPedidoValue
        End Get
        Set(ByVal value As Integer)
            IDPedidoValue = value
        End Set
    End Property

    Private ComprobanteValue As String
    Public Property Comprobante() As String
        Get
            Return ComprobanteValue
        End Get
        Set(ByVal value As String)
            ComprobanteValue = value
        End Set
    End Property

    Private IDTransaccionPedidoValue As Integer
    Public Property IDTransaccionPedido() As Integer
        Get
            Return IDTransaccionPedidoValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionPedidoValue = value
        End Set
    End Property


    'VARIABLES
    Dim dtPedido As DataTable
    Dim vControles() As Control

    'FUNCIONES
    Overloads Sub Inicializar()
        nmModuloActivo = "frmVentaPedido"
        mensajeError = ""


        VentaPedidoAbierto = 0
        Me.AcceptButton = New Button

        'Controles
        txtProducto.Venta = True
        'cb
        ' txtProducto.ConectarVenta()
        txtProducto.PVtaConectarVenta()
        'txtCliente.Conectar()
        txtCliente.PVtaConectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Condicion, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito  From VCliente Where Estado='ACTIVA' "
        txtCliente.frm = Me

        'Otros
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False
        txtVencimiento.Enabled = False
        txtPlazo.Enabled = False
        txtProducto.Enabled = False
        lvLista.Enabled = False
        OcxImpuesto1.Inicializar()
        OcxImpuesto1.dg.ReadOnly = True

        'Propiedades
        IDTransaccion = 0

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion(frmVenta.Name.ToString, "VENTAS CLIENTES", "FATCLI")
        CargarInformacion()

        'No se puede utitilzar este formulario para nada que no se directamente facturar, inhabilitar busquedas, navegacion, asiento, etc
        btnBusquedaAvanzada.Visible = False
        btnAsiento.Visible = False
        btnCancelar.Visible = False
        btnImprimir.Visible = False
        btnNuevo.Visible = False

        'Clases
        CAsiento.InicializarAsiento()

        'Foco
        txtNroComprobante.Focus()
        txtNroComprobante.txt.Focus()
        txtNroComprobante.txt.SelectAll()

        btnHabilitar.Visible = False
        btnImprimirRecibo.Visible = False
        btnAnularRecibo.Visible = False
    End Sub

    Sub ObtenerInformacionCliente()

        Dim oRow As DataRow = txtCliente.Registro
        Dim oRowSucursal As DataRow = txtCliente.Sucursal

        ''Datos Comunes
        'txtDescuento.txt.Text = oRow("Descuento").ToString
        'txtCreditoSaldo.txt.Text = oRow("Deuda").ToString
        txtPlazo.txt.Text = oRow("PlazoCredito").ToString
        CalcularPlazo(oRow("PlazoCredito"))
        'Dim Plazo As Integer = oRow("PlazoCredito").ToString
        'txtVencimiento.SetValue(DateAdd(DateInterval.Day, Plazo, txtFechaVenta.GetValue))
        'cbxPromotor.txt.Text = oRow("Promotor")


        txtProducto.IDCliente = oRow("ID").ToString
        txtProducto.IDListaPrecio = cbxListaPrecio2.GetValue


        If txtCliente.SucursalSeleccionada = False Then
            If txtDireccion.txt.Text = "" Then
                txtDireccion.txt.Text = oRow("Direccion").ToString
            End If
            If txtTelefono.txt.Text = "" Then
                txtTelefono.txt.Text = oRow("Telefono").ToString
            End If

        Else
            If txtDireccion.txt.Text = "" Then
                txtDireccion.txt.Text = oRowSucursal("Direccion").ToString
            End If
            If txtTelefono.txt.Text = "" Then
                txtTelefono.txt.Text = oRowSucursal("Telefono").ToString
            End If
            txtProducto.IDListaPrecio = oRowSucursal("IDListaPrecio").ToString
        End If


    End Sub

    Overloads Sub CargarInformacion()

        ReDim vControles(-1)

        'Cabecera
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, txtFechaVenta)
        CSistema.CargaControl(vControles, cbxCondicion)
        CSistema.CargaControl(vControles, txtVencimiento)
        CSistema.CargaControl(vControles, txtPlazo)
        CSistema.CargaControl(vControles, cbxDeposito2)
        CSistema.CargaControl(vControles, txtDetalle)
        CSistema.CargaControl(vControles, txtTipoEntrega)

        'Detalle
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, txtObservacionProducto)
        CSistema.CargaControl(vControles, cbxUnidad)
        CSistema.CargaControl(vControles, txtCantidad)
        CSistema.CargaControl(vControles, txtPrecioUnitario)
        CSistema.CargaControl(vControles, txtImporte)
        CSistema.CargaControl(vControles, lvLista)
        CSistema.CargaControl(vControles, btnHabilitar)

        dtDetalle = CData.GetStructure("VDetalleVenta", "Select Top(0) * From VDetalleVenta")
        dtDescuento = CData.GetStructure("VDescuento", "Select Top(0) * From VDescuento")

        dtPreciosProducto = New DataTable
        dtPreciosProducto.Columns.Add("Precio")
        dtPreciosProducto.Columns.Add("Cantidad")
        dtPreciosProducto.Columns.Add("PorcentajeDescuento")
        dtPreciosProducto.Columns.Add("DescuentoUnitario")
        dtPreciosProducto.Columns.Add("TotalDescuento")
        dtPreciosProducto.Columns.Add("Total")
        dtPreciosProducto.Columns.Add("TotalSinDescuento")
        dtPreciosProducto.Columns.Add("DescuentoUnitarioDiscriminado")
        dtPreciosProducto.Columns.Add("TotalDescuentoDiscriminado")

        'Descuentos
        dtPreciosProducto.Columns.Add("TPR")
        dtPreciosProducto.Columns.Add("Tactico")
        dtPreciosProducto.Columns.Add("Express")
        dtPreciosProducto.Columns.Add("Acuerdo")
        dtPreciosProducto.Columns.Add("TacticoPlanilla")

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CONDICION
        cbxCondicion.cbx.Items.Add("CONTADO")
        cbxCondicion.cbx.Items.Add("CREDITO")
        cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR UNIDAD
        cbxUnidad.Items.Add("UNIDAD")
        cbxUnidad.Items.Add("CAJA")
        cbxUnidad.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR CONTROLES
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, CData.GetTable("VTipoComprobante", " IDOperacion = " & IDOperacion), "ID", "Codigo")

        'Ciudad
        cbxCiudad.Conectar()

        'Sucursal
        cbxSucursal.Conectar()

        'Depositos
        cbxDeposito2.Conectar()
        cbxDeposito2.cbx.Text = vgDeposito

        'Puntos de Expediciones
        dtPuntoExpedicion = CData.GetTable("VPuntoExpedicion").Copy
        dtPuntoExpedicion = CData.FiltrarDataTable(dtPuntoExpedicion, " IDOperacion = " & IDOperacion)
        txtIDTimbrado.SetValue(CArchivoInicio.IniGet(VGArchivoINI, frmVenta.Name.ToString, "TIMBRADO", 0))
        ObtenerInformacionPuntoVenta()

        'Vendedor
        cbxVendedor.Conectar()

        cbxFormaPagoFactura.Conectar()

        'Promotor
        cbxPromotor.Conectar()

        'Listas de Precio
        CSistema.SqlToComboBox(cbxListaPrecio2.cbx, CData.GetTable("VListaPrecio"), "ID", "Descripcion")


        'CONFIGURACIONES
        If IsDate(VGFechaFacturacion) = False Then
            VGFechaFacturacion = VGFechaHoraSistema
        End If

        If CBool(vgConfiguraciones("VentaBloquearFecha").ToString) = True Then
            txtFechaVenta.Enabled = False
        Else
            txtFechaVenta.Enabled = True
        End If
        VGFechaFacturacion = CSistema.ExecuteScalar("Select TOP 1 Fecha From FechaFacturacion Where IDSucursal=" & vgIDSucursal)

        txtFechaVenta.SetValue(VGFechaFacturacion)

    End Sub

    Overloads Sub GuardarInformacion()

        'TIMBRADO
        CArchivoInicio.IniWrite(VGArchivoINI, frmVenta.Name, "TIMBRADO", txtIDTimbrado.GetValue)

    End Sub

    Sub CargarPedido(ByVal IDSucursal As Integer)
        Try
            'Obtener la Transaccion del pedido
            'Esta opcion es configurable en el menu de configuracion de pedido
            'chkImprimir2.Valor = CBool(vgConfiguraciones("VentaImprimirSinRpt").ToString)
            ctrError.Clear()
            tsslEstado.Text = ""

            txtNroComprobante.txt.Focus()
            txtNroComprobante.txt.SelectAll()

            If IDTransaccionPedido = 0 Then
                Dim mensaje As String = "El sistema no encuentra el registro!"
                ctrError.SetError(txtNroComprobante, mensaje)
                ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'Verificamos si el pedido sigue vigente o esta anulado
            Dim Anulado As Integer
            Anulado = CSistema.ExecuteScalar("Select Anulado From Pedido Where IDTransaccion=" & IDTransaccionPedido & " ")

            If Anulado = 1 Then
                Dim mensaje As String = "El pedido esta anulado no se puede facturar!"
                ctrError.SetError(txtNroComprobante, mensaje)
                ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If


            dtDescuento.Clear()
            dtPreciosProducto.Clear()
            dtDetalle.Clear()

            lblIDTransaccionPedido.Text = IDTransaccionPedido.ToString

            dtPedido = CSistema.ExecuteToDataTable("Select * From VPedido Where IDTransaccion=" & IDTransaccionPedido).Copy
            dtDetalle = CSistema.ExecuteToDataTable("Select * From VDetallePedido Where IDTransaccion=" & IDTransaccionPedido & " Order By ID").Copy
            dtDescuento = CSistema.ExecuteToDataTable("Select * From VDescuentoPedido Where IDTransaccion=" & IDTransaccionPedido & " Order By ID").Copy

            'Validar
            If dtPedido Is Nothing Then
                MessageBox.Show("El sistema no puede encontrar el registro solicitado! Comuniquese con sistemas.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

            If dtPedido.Rows.Count = 0 Then
                MessageBox.Show("El sistema no puede encontrar el registro solicitado! Comuniquese con sistemas.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

            'Actualizar datos
            ActualizarDatos()

            'Cargamos la cabecera
            If dtPedido Is Nothing Then
                Dim mensaje As String = "Error en la consulta! Problemas tecnico."
                ctrError.SetError(txtNroComprobante, mensaje)
                ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If


            If dtPedido.Rows.Count = 0 Then
                Exit Sub
            End If

            Dim oRow As DataRow = dtPedido.Rows(0)

            'Cliente
            txtCliente.SetValue(oRow("IDCliente").ToString)
            txtCliente.txtRazonSocial.txt.Text = oRow("Cliente").ToString
            txtCliente.txtRUC.txt.Text = oRow("RUC").ToString
            txtDireccion.txt.Text = oRow("Direccion").ToString
            txtTelefono.txt.Text = oRow("Telefono").ToString
            cbxVendedor.cbx.Text = oRow("Vendedor").ToString
            'SC: 17/12/2021 - Nueva asignacion para TipoEntrega
            txtTipoEntrega.txt.Text = oRow("TipoEntrega").ToString

            Dim IDFormaPagoFactura As Integer = CSistema.RetornarValorInteger(oRow("IDFormaPagoFactura").ToString)
            cbxFormaPagoFactura.SelectedValue(IDFormaPagoFactura)

            If oRow("EsVentaSucursal") = True Then
                txtCliente.SucursalSeleccionada = True
                Dim dt As DataTable = CSistema.ExecuteToDataTable("Select ID, Sucursal, IDVendedor, Vendedor, Direccion, Condicion, Telefono, Ciudad, Barrio, ZonaVenta, IDListaPrecio From VClienteSucursal Where IDCliente=" & oRow("IDCliente").ToString & " And ID=" & oRow("IDSucursalCliente").ToString)
                txtCliente.Sucursal = dt.Rows(0)
                'oRow("IDVendedor") = txtCliente.Sucursal("IDVendedor")
                'oRow("Vendedor") = txtCliente.Sucursal("Vendedor")
            End If

            txtFechaVenta.SetValueFromString(VGFechaFacturacion)

            cbxDeposito2.cbx.Text = oRow("Deposito").ToString

            cbxVendedor.cbx.Text = oRow("Vendedor").ToString

            'cbxListaPrecio2.txt.Text = oRow("Lista de Precio").ToString
            cbxListaPrecio2.SelectedValue(CSistema.RetornarValorInteger(oRow("IDListaPrecio").ToString))
            Dim Cotizacion As Decimal = 1
            If oRow("IDMoneda") > 1 Then
                Cotizacion = CSistema.ExecuteScalar("select dbo.FCotizacionAlDiafecha(" & oRow("IDMoneda") & ",0,'" & CSistema.FormatoFechaBaseDatos(VGFechaFacturacion, True, False) & "')")
            End If

            OcxCotizacion1.cbxMoneda.cbx.SelectedValue = oRow("IDMoneda")
            OcxCotizacion1.SetValue(oRow("Moneda").ToString, Cotizacion)

            'OcxCotizacion1.Registro("IDMoneda") = oRow("IDMoneda")
            txtDetalle.txt.Text = oRow("Observacion").ToString
            ObtenerInformacionCliente()
            txtPlazo.txt.Text = oRow("PlazoCredito").ToString
            PlazoCliente = oRow("PlazoCredito").ToString
            CalcularPlazo(oRow("PlazoCredito"))

            'Ponemos el numero de comprobante si es que tiene
            If IsNumeric(Comprobante) Then
                txtNroComprobante.SetValue(Comprobante)
            End If

            If CBool(oRow("Credito").ToString) = True Then
                cbxCondicion.cbx.Text = "CREDITO"
            Else
                cbxCondicion.cbx.Text = "CONTADO"
            End If

            'Cargamos el detalle
            ListarDetalle()
            CDetalleImpuesto.Decimales = vDecimalOperacion
            CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, True, vDecimalOperacion)
            OcxImpuesto1.CargarValores(CDetalleImpuesto.dt)

            'Inicializamos el Asiento
            GenerarAsiento()

            Debug.Print(txtFechaVenta.GetValueString)

            'Bloquar 
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
       
    End Sub

    Sub ActualizarDatos()

        'Clientes
        Dim IDCliente As Integer = dtPedido.Rows(0)("IDCliente")
        ' cb
        'If CData.GetTable("VCliente", " ID = " & IDCliente).Rows.Count = 0 Then
        'CData.Actualizar(IDCliente, "VCliente")
        'End If

        If CData.GetTable("ClienteUnico", IDCliente.ToString).Rows.Count = 0 Then
            CData.Actualizar(IDCliente, "ClienteUnico")
        End If

        ' cb
        'For Each oRow As DataRow In dtDetalle.Rows
        'If CData.GetTable("VProducto", " ID = " & oRow("IDProducto")).Rows.Count = 0 Then
        'CData.Actualizar(oRow("IDProducto"), "VProducto")
        'End If
        'Next

        For Each oRow As DataRow In dtDetalle.Rows
            If CData.GetTable("ProductoUnico", oRow("IDProducto")).Rows.Count = 0 Then
                CData.Actualizar(oRow("IDProducto"), "ProductoUnico")
            End If
        Next

    End Sub

    Private Sub frmVentaPedido_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
        LiberarMemoria()
    End Sub

    Private Sub frmVentaPedido_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmVentaPedido_Activate()
        Me.Refresh()
    End Sub
    ''facturacion asistida 
    Private Sub frmVentaPedido_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        If cbxFormaPagoFactura.GetValue <> 7 Then
            If pFacturacionAsistida Then
                Guardar2()
                If pFacturacionAsistida = True Then
                    Me.Close()
                End If
            End If
        Else
            ''se agrega para poder facturar MANUAL los que son forma de pago NC 
            If pFacturacionAsistida Then
                Me.Close()
            End If

        End If

    End Sub
End Class

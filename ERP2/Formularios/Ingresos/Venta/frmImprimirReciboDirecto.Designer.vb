﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImprimirReciboDirecto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxImpresora = New ERP.ocxCBX()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(419, 29)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Seleccione la Impresora para Imprimir el Recibo:"
        '
        'cbxImpresora
        '
        Me.cbxImpresora.CampoWhere = Nothing
        Me.cbxImpresora.CargarUnaSolaVez = False
        Me.cbxImpresora.DataDisplayMember = Nothing
        Me.cbxImpresora.DataFilter = Nothing
        Me.cbxImpresora.DataOrderBy = Nothing
        Me.cbxImpresora.DataSource = Nothing
        Me.cbxImpresora.DataValueMember = Nothing
        Me.cbxImpresora.dtSeleccionado = Nothing
        Me.cbxImpresora.FormABM = Nothing
        Me.cbxImpresora.Indicaciones = Nothing
        Me.cbxImpresora.Location = New System.Drawing.Point(14, 57)
        Me.cbxImpresora.Name = "cbxImpresora"
        Me.cbxImpresora.SeleccionMultiple = False
        Me.cbxImpresora.SeleccionObligatoria = True
        Me.cbxImpresora.Size = New System.Drawing.Size(415, 33)
        Me.cbxImpresora.SoloLectura = False
        Me.cbxImpresora.TabIndex = 12
        Me.cbxImpresora.Texto = ""
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Me.Button1.Location = New System.Drawing.Point(116, 108)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(210, 33)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "IMPRIMIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmImprimirReciboDirecto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(449, 160)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.cbxImpresora)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmImprimirReciboDirecto"
        Me.Text = "frmImprimirReciboDirecto"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents cbxImpresora As ocxCBX
    Friend WithEvents Button1 As Button
End Class

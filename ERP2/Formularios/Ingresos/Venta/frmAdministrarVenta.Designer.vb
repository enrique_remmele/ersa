﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdministrarVenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtCantidadOperacion = New ERP.ocxTXTNumeric()
        Me.lblCantidadOperacion = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ExportarAPlanillaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.chkInlcuirAnuladosGeneral = New System.Windows.Forms.CheckBox()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpHastaGeneral = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtpDesdeGeneral = New System.Windows.Forms.DateTimePicker()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.btnGeneralPorDia = New System.Windows.Forms.Button()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.chkInlcuirAnuladosSucursal = New System.Windows.Forms.CheckBox()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpHastaSucursal = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpDesdeSucursal = New System.Windows.Forms.DateTimePicker()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dtpHastaReporte = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dtpDesdeReporte = New System.Windows.Forms.DateTimePicker()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cbxSucursalOperacion = New ERP.ocxCBX()
        Me.cbxComprobante = New System.Windows.Forms.ComboBox()
        Me.txtComprobante = New System.Windows.Forms.TextBox()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnVerDetalle = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnAgregarComprobantes = New System.Windows.Forms.Button()
        Me.btnVerPedido = New System.Windows.Forms.Button()
        Me.btnVerLote = New System.Windows.Forms.Button()
        Me.OcxDocumentosVentas1 = New ERP.ocxDocumentosVentas()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnReparar = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TableLayoutPanel1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 224.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 195.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.StatusStrip1, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TabControl1, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.OcxDocumentosVentas1, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 1, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 228.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1030, 513)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'StatusStrip1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.StatusStrip1, 2)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 493)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(835, 20)
        Me.StatusStrip1.TabIndex = 15
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 15)
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.txtCantidadOperacion)
        Me.FlowLayoutPanel2.Controls.Add(Me.lblCantidadOperacion)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(614, 3)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(218, 25)
        Me.FlowLayoutPanel2.TabIndex = 1
        '
        'txtCantidadOperacion
        '
        Me.txtCantidadOperacion.Color = System.Drawing.Color.Empty
        Me.txtCantidadOperacion.Decimales = True
        Me.txtCantidadOperacion.Indicaciones = Nothing
        Me.txtCantidadOperacion.Location = New System.Drawing.Point(173, 3)
        Me.txtCantidadOperacion.Name = "txtCantidadOperacion"
        Me.txtCantidadOperacion.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadOperacion.SoloLectura = False
        Me.txtCantidadOperacion.TabIndex = 1
        Me.txtCantidadOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadOperacion.Texto = "0"
        '
        'lblCantidadOperacion
        '
        Me.lblCantidadOperacion.AutoSize = True
        Me.lblCantidadOperacion.Location = New System.Drawing.Point(115, 0)
        Me.lblCantidadOperacion.Name = "lblCantidadOperacion"
        Me.lblCantidadOperacion.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadOperacion.TabIndex = 0
        Me.lblCantidadOperacion.Text = "Cantidad:"
        '
        'Panel2
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.Panel2, 2)
        Me.Panel2.Controls.Add(Me.DataGridView1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 34)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(829, 192)
        Me.Panel2.TabIndex = 1
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.ContextMenuStrip = Me.ContextMenuStrip1
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(829, 192)
        Me.DataGridView1.StandardTab = True
        Me.DataGridView1.TabIndex = 0
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExportarAPlanillaToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(169, 48)
        '
        'ExportarAPlanillaToolStripMenuItem
        '
        Me.ExportarAPlanillaToolStripMenuItem.Name = "ExportarAPlanillaToolStripMenuItem"
        Me.ExportarAPlanillaToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ExportarAPlanillaToolStripMenuItem.Text = "Exportar a Planilla"
        '
        'TabControl1
        '
        Me.TabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(838, 3)
        Me.TabControl1.Name = "TabControl1"
        Me.TableLayoutPanel1.SetRowSpan(Me.TabControl1, 5)
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(189, 507)
        Me.TabControl1.TabIndex = 5
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.chkInlcuirAnuladosGeneral)
        Me.TabPage1.Controls.Add(Me.Button9)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.dtpHastaGeneral)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.dtpDesdeGeneral)
        Me.TabPage1.Controls.Add(Me.Button8)
        Me.TabPage1.Controls.Add(Me.Button7)
        Me.TabPage1.Controls.Add(Me.Button6)
        Me.TabPage1.Controls.Add(Me.Button5)
        Me.TabPage1.Controls.Add(Me.btnGeneralPorDia)
        Me.TabPage1.Location = New System.Drawing.Point(4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(181, 481)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'chkInlcuirAnuladosGeneral
        '
        Me.chkInlcuirAnuladosGeneral.AutoSize = True
        Me.chkInlcuirAnuladosGeneral.Checked = True
        Me.chkInlcuirAnuladosGeneral.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkInlcuirAnuladosGeneral.Location = New System.Drawing.Point(6, 286)
        Me.chkInlcuirAnuladosGeneral.Name = "chkInlcuirAnuladosGeneral"
        Me.chkInlcuirAnuladosGeneral.Size = New System.Drawing.Size(101, 17)
        Me.chkInlcuirAnuladosGeneral.TabIndex = 10
        Me.chkInlcuirAnuladosGeneral.Text = "Incluir Anulados"
        Me.chkInlcuirAnuladosGeneral.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(6, 239)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(167, 23)
        Me.Button9.TabIndex = 9
        Me.Button9.Text = "Comprobantes por Fecha"
        Me.Button9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(89, 197)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Hasta:"
        '
        'dtpHastaGeneral
        '
        Me.dtpHastaGeneral.CustomFormat = "dd-MM-yyyy"
        Me.dtpHastaGeneral.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpHastaGeneral.Location = New System.Drawing.Point(92, 213)
        Me.dtpHastaGeneral.Name = "dtpHastaGeneral"
        Me.dtpHastaGeneral.Size = New System.Drawing.Size(81, 20)
        Me.dtpHastaGeneral.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 197)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Desde:"
        '
        'dtpDesdeGeneral
        '
        Me.dtpDesdeGeneral.CustomFormat = "dd-MM-yyyy"
        Me.dtpDesdeGeneral.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDesdeGeneral.Location = New System.Drawing.Point(9, 213)
        Me.dtpDesdeGeneral.Name = "dtpDesdeGeneral"
        Me.dtpDesdeGeneral.Size = New System.Drawing.Size(81, 20)
        Me.dtpDesdeGeneral.TabIndex = 6
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(6, 143)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(167, 23)
        Me.Button8.TabIndex = 4
        Me.Button8.Text = "Comprobantes del mes anterior"
        Me.Button8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(6, 56)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(167, 23)
        Me.Button7.TabIndex = 1
        Me.Button7.Text = "Comprobantes de Ayer"
        Me.Button7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(6, 114)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(167, 23)
        Me.Button6.TabIndex = 3
        Me.Button6.Text = "Comprobantes del Mes"
        Me.Button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(6, 85)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(167, 23)
        Me.Button5.TabIndex = 2
        Me.Button5.Text = "Comprobantes de la Semana"
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button5.UseVisualStyleBackColor = True
        '
        'btnGeneralPorDia
        '
        Me.btnGeneralPorDia.Location = New System.Drawing.Point(6, 27)
        Me.btnGeneralPorDia.Name = "btnGeneralPorDia"
        Me.btnGeneralPorDia.Size = New System.Drawing.Size(167, 23)
        Me.btnGeneralPorDia.TabIndex = 0
        Me.btnGeneralPorDia.Text = "Comprobantes del Dia"
        Me.btnGeneralPorDia.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGeneralPorDia.UseVisualStyleBackColor = True
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.chkInlcuirAnuladosSucursal)
        Me.TabPage6.Controls.Add(Me.Button11)
        Me.TabPage6.Controls.Add(Me.Label1)
        Me.TabPage6.Controls.Add(Me.dtpHastaSucursal)
        Me.TabPage6.Controls.Add(Me.Label4)
        Me.TabPage6.Controls.Add(Me.dtpDesdeSucursal)
        Me.TabPage6.Controls.Add(Me.Button12)
        Me.TabPage6.Controls.Add(Me.Button13)
        Me.TabPage6.Controls.Add(Me.Button14)
        Me.TabPage6.Controls.Add(Me.Button15)
        Me.TabPage6.Controls.Add(Me.Button16)
        Me.TabPage6.Location = New System.Drawing.Point(4, 4)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(181, 481)
        Me.TabPage6.TabIndex = 2
        Me.TabPage6.Text = "Sucursal"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'chkInlcuirAnuladosSucursal
        '
        Me.chkInlcuirAnuladosSucursal.AutoSize = True
        Me.chkInlcuirAnuladosSucursal.Checked = True
        Me.chkInlcuirAnuladosSucursal.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkInlcuirAnuladosSucursal.Location = New System.Drawing.Point(7, 286)
        Me.chkInlcuirAnuladosSucursal.Name = "chkInlcuirAnuladosSucursal"
        Me.chkInlcuirAnuladosSucursal.Size = New System.Drawing.Size(101, 17)
        Me.chkInlcuirAnuladosSucursal.TabIndex = 11
        Me.chkInlcuirAnuladosSucursal.Text = "Incluir Anulados"
        Me.chkInlcuirAnuladosSucursal.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(7, 239)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(167, 23)
        Me.Button11.TabIndex = 10
        Me.Button11.Text = "Comprobantes por Fecha"
        Me.Button11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(90, 197)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Hasta:"
        '
        'dtpHastaSucursal
        '
        Me.dtpHastaSucursal.CustomFormat = "dd-mm-yyyy"
        Me.dtpHastaSucursal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpHastaSucursal.Location = New System.Drawing.Point(93, 213)
        Me.dtpHastaSucursal.Name = "dtpHastaSucursal"
        Me.dtpHastaSucursal.Size = New System.Drawing.Size(81, 20)
        Me.dtpHastaSucursal.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 197)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Desde:"
        '
        'dtpDesdeSucursal
        '
        Me.dtpDesdeSucursal.CustomFormat = "dd-mm-yyyy"
        Me.dtpDesdeSucursal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDesdeSucursal.Location = New System.Drawing.Point(10, 213)
        Me.dtpDesdeSucursal.Name = "dtpDesdeSucursal"
        Me.dtpDesdeSucursal.Size = New System.Drawing.Size(81, 20)
        Me.dtpDesdeSucursal.TabIndex = 7
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(7, 143)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(167, 23)
        Me.Button12.TabIndex = 5
        Me.Button12.Text = "Comprobantes del mes anterior"
        Me.Button12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(7, 56)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(167, 23)
        Me.Button13.TabIndex = 2
        Me.Button13.Text = "Comprobantes de Ayer"
        Me.Button13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(7, 114)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(167, 23)
        Me.Button14.TabIndex = 4
        Me.Button14.Text = "Comprobantes del Mes"
        Me.Button14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(7, 85)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(167, 23)
        Me.Button15.TabIndex = 3
        Me.Button15.Text = "Comprobantes de la Semana"
        Me.Button15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(7, 27)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(167, 23)
        Me.Button16.TabIndex = 1
        Me.Button16.Text = "Comprobantes del Dia"
        Me.Button16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button16.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Button2)
        Me.TabPage2.Controls.Add(Me.Button1)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.dtpHastaReporte)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.dtpDesdeReporte)
        Me.TabPage2.Location = New System.Drawing.Point(4, 4)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(181, 481)
        Me.TabPage2.TabIndex = 3
        Me.TabPage2.Text = "Reporte"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(3, 79)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(167, 23)
        Me.Button2.TabIndex = 21
        Me.Button2.Text = "Saldos incorrectos"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(3, 50)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(167, 23)
        Me.Button1.TabIndex = 20
        Me.Button1.Text = "Diferencias entre detalle y total"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(86, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(38, 13)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "Hasta:"
        '
        'dtpHastaReporte
        '
        Me.dtpHastaReporte.CustomFormat = "dd-MM-yyyy"
        Me.dtpHastaReporte.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpHastaReporte.Location = New System.Drawing.Point(89, 24)
        Me.dtpHastaReporte.Name = "dtpHastaReporte"
        Me.dtpHastaReporte.Size = New System.Drawing.Size(81, 20)
        Me.dtpHastaReporte.TabIndex = 19
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(41, 13)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "Desde:"
        '
        'dtpDesdeReporte
        '
        Me.dtpDesdeReporte.CustomFormat = "dd-MM-yyyy"
        Me.dtpDesdeReporte.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDesdeReporte.Location = New System.Drawing.Point(6, 24)
        Me.dtpDesdeReporte.Name = "dtpDesdeReporte"
        Me.dtpDesdeReporte.Size = New System.Drawing.Size(81, 20)
        Me.dtpDesdeReporte.TabIndex = 17
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cbxSucursalOperacion)
        Me.FlowLayoutPanel1.Controls.Add(Me.cbxComprobante)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtComprobante)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(605, 25)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'cbxSucursalOperacion
        '
        Me.cbxSucursalOperacion.CampoWhere = Nothing
        Me.cbxSucursalOperacion.CargarUnaSolaVez = False
        Me.cbxSucursalOperacion.DataDisplayMember = "Codigo"
        Me.cbxSucursalOperacion.DataFilter = Nothing
        Me.cbxSucursalOperacion.DataOrderBy = Nothing
        Me.cbxSucursalOperacion.DataSource = "VSucursal"
        Me.cbxSucursalOperacion.DataValueMember = "ID"
        Me.cbxSucursalOperacion.dtSeleccionado = Nothing
        Me.cbxSucursalOperacion.FormABM = Nothing
        Me.cbxSucursalOperacion.Indicaciones = Nothing
        Me.cbxSucursalOperacion.Location = New System.Drawing.Point(3, 3)
        Me.cbxSucursalOperacion.Name = "cbxSucursalOperacion"
        Me.cbxSucursalOperacion.SeleccionMultiple = False
        Me.cbxSucursalOperacion.SeleccionObligatoria = True
        Me.cbxSucursalOperacion.Size = New System.Drawing.Size(75, 21)
        Me.cbxSucursalOperacion.SoloLectura = False
        Me.cbxSucursalOperacion.TabIndex = 0
        Me.cbxSucursalOperacion.Texto = ""
        '
        'cbxComprobante
        '
        Me.cbxComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxComprobante.FormattingEnabled = True
        Me.cbxComprobante.Location = New System.Drawing.Point(84, 3)
        Me.cbxComprobante.Name = "cbxComprobante"
        Me.cbxComprobante.Size = New System.Drawing.Size(75, 21)
        Me.cbxComprobante.TabIndex = 1
        '
        'txtComprobante
        '
        Me.txtComprobante.Location = New System.Drawing.Point(165, 3)
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(155, 20)
        Me.txtComprobante.TabIndex = 2
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.btnVerDetalle)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnAsiento)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnImprimir)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnAnular)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnAgregarComprobantes)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnVerPedido)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnVerLote)
        Me.FlowLayoutPanel3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 232)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(603, 30)
        Me.FlowLayoutPanel3.TabIndex = 2
        '
        'btnVerDetalle
        '
        Me.btnVerDetalle.Location = New System.Drawing.Point(3, 3)
        Me.btnVerDetalle.Name = "btnVerDetalle"
        Me.btnVerDetalle.Size = New System.Drawing.Size(75, 23)
        Me.btnVerDetalle.TabIndex = 0
        Me.btnVerDetalle.Text = "Ver Detalle"
        Me.btnVerDetalle.UseVisualStyleBackColor = True
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(84, 3)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 3
        Me.btnAsiento.Text = "Ver Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(165, 3)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 1
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(246, 3)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 2
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnAgregarComprobantes
        '
        Me.btnAgregarComprobantes.Location = New System.Drawing.Point(327, 3)
        Me.btnAgregarComprobantes.Name = "btnAgregarComprobantes"
        Me.btnAgregarComprobantes.Size = New System.Drawing.Size(114, 23)
        Me.btnAgregarComprobantes.TabIndex = 11
        Me.btnAgregarComprobantes.Text = "Agregar Anulado"
        Me.btnAgregarComprobantes.UseVisualStyleBackColor = True
        '
        'btnVerPedido
        '
        Me.btnVerPedido.Location = New System.Drawing.Point(447, 3)
        Me.btnVerPedido.Name = "btnVerPedido"
        Me.btnVerPedido.Size = New System.Drawing.Size(72, 23)
        Me.btnVerPedido.TabIndex = 12
        Me.btnVerPedido.Text = "Ver Pedido"
        Me.btnVerPedido.UseVisualStyleBackColor = True
        '
        'btnVerLote
        '
        Me.btnVerLote.Location = New System.Drawing.Point(525, 3)
        Me.btnVerLote.Name = "btnVerLote"
        Me.btnVerLote.Size = New System.Drawing.Size(72, 23)
        Me.btnVerLote.TabIndex = 13
        Me.btnVerLote.Text = "Ver Lote"
        Me.btnVerLote.UseVisualStyleBackColor = True
        '
        'OcxDocumentosVentas1
        '
        Me.OcxDocumentosVentas1.CancelarAutomatico = False
        Me.TableLayoutPanel1.SetColumnSpan(Me.OcxDocumentosVentas1, 2)
        Me.OcxDocumentosVentas1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxDocumentosVentas1.IDTransaccion = Nothing
        Me.OcxDocumentosVentas1.Location = New System.Drawing.Point(3, 268)
        Me.OcxDocumentosVentas1.Name = "OcxDocumentosVentas1"
        Me.OcxDocumentosVentas1.Size = New System.Drawing.Size(829, 222)
        Me.OcxDocumentosVentas1.TabIndex = 16
        Me.OcxDocumentosVentas1.Tag = "frmAdministrarVenta"
        Me.OcxDocumentosVentas1.Total = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlowLayoutPanel4.AutoSize = True
        Me.FlowLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel4.Controls.Add(Me.btnEliminar)
        Me.FlowLayoutPanel4.Controls.Add(Me.btnReparar)
        Me.FlowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(615, 232)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(217, 29)
        Me.FlowLayoutPanel4.TabIndex = 3
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(139, 3)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 0
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnReparar
        '
        Me.btnReparar.Location = New System.Drawing.Point(3, 3)
        Me.btnReparar.Margin = New System.Windows.Forms.Padding(3, 3, 5, 3)
        Me.btnReparar.Name = "btnReparar"
        Me.btnReparar.Size = New System.Drawing.Size(128, 23)
        Me.btnReparar.TabIndex = 1
        Me.btnReparar.Text = "Reparar Venta"
        Me.btnReparar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmAdministrarVenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1030, 513)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.KeyPreview = True
        Me.Name = "frmAdministrarVenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "ADMINISTRAR VENTAS"
        Me.Text = "frmAdministrarVenta"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtComprobante As System.Windows.Forms.TextBox
    Friend WithEvents cbxComprobante As System.Windows.Forms.ComboBox
    Friend WithEvents txtCantidadOperacion As ERP.ocxTXTNumeric
    Friend WithEvents lblCantidadOperacion As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents btnGeneralPorDia As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents dtpDesdeGeneral As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpHastaGeneral As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents chkInlcuirAnuladosGeneral As System.Windows.Forms.CheckBox
    Friend WithEvents btnVerDetalle As System.Windows.Forms.Button
    Friend WithEvents chkInlcuirAnuladosSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpHastaSucursal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtpDesdeSucursal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnAgregarComprobantes As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents OcxDocumentosVentas1 As ERP.ocxDocumentosVentas
    Friend WithEvents btnReparar As System.Windows.Forms.Button
    Friend WithEvents btnVerPedido As System.Windows.Forms.Button
    Friend WithEvents cbxSucursalOperacion As ERP.ocxCBX
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dtpHastaReporte As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dtpDesdeReporte As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnVerLote As System.Windows.Forms.Button
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ExportarAPlanillaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class

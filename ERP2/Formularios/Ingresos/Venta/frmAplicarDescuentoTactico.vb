﻿Public Class frmAplicarDescuentoTactico

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDProductoValue As Integer
    Public Property IDProducto() As Integer
        Get
            Return IDProductoValue
        End Get
        Set(ByVal value As Integer)
            IDProductoValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Private PrecioValue As Decimal
    Public Property Precio() As Decimal
        Get
            Return PrecioValue
        End Get
        Set(ByVal value As Decimal)
            PrecioValue = value
        End Set
    End Property

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button
        Me.Text = "Descuentos"

        CargarDatos()

        dgvDescuentos.Focus()

    End Sub

    Sub CargarDatos()

        Dim dtDescuentos As DataTable = CData.GetTable("VTipoDescuento").Copy

        dtDescuentos.Columns.Add("PrecioUnitario")
        dtDescuentos.Columns.Add("Porcentaje")
        dtDescuentos.Columns.Add("Descuento")

        CSistema.dtToGrid(dgvDescuentos, dtDescuentos)

        For Each oRow As DataRow In dtDescuentos.Rows

            oRow("PrecioUnitario") = Precio
            oRow("Porcentaje") = 0
            oRow("Descuento") = 0

            For Each DescuentoRow As DataRow In dt.Select(" TipoDescuento = '" & oRow("Codigo").ToString & "' ")
                oRow("Porcentaje") = DescuentoRow("Porcentaje")
                oRow("Descuento") = DescuentoRow("Importe")
            Next
        Next

        'Formato
        For c As Integer = 0 To dgvDescuentos.ColumnCount - 1
            dgvDescuentos.Columns(c).Visible = False
        Next

        dgvDescuentos.Columns("Descripcion").Visible = True
        dgvDescuentos.Columns("PrecioUnitario").Visible = True
        dgvDescuentos.Columns("Porcentaje").Visible = True
        dgvDescuentos.Columns("Descuento").Visible = True

        dgvDescuentos.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        dgvDescuentos.Columns("PrecioUnitario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDescuentos.Columns("Porcentaje").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDescuentos.Columns("Descuento").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgvDescuentos.Columns("PrecioUnitario").DefaultCellStyle.Format = "N0"
        dgvDescuentos.Columns("Porcentaje").DefaultCellStyle.Format = "N2"
        dgvDescuentos.Columns("Descuento").DefaultCellStyle.Format = "N2"


    End Sub

    Sub Seleccionar()

        txtDescuento.SetValue(0)

        If dgvDescuentos.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        If IsNumeric(dgvDescuentos.SelectedRows(0).Cells("Porcentaje").Value) = False Then
            Exit Sub
        End If

        txtDescuento.SetValue(dgvDescuentos.SelectedRows(0).Cells("Porcentaje").Value)

    End Sub

    Sub Aplicar()

        'Validar
        'Que el tipo de Descuento permita
        If dgvDescuentos.SelectedRows(0).Cells("Editable").Value = False Then
            MessageBox.Show("Este tipo de descuento no esta permitido la edicion!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        'Porcentaje
        If txtDescuento.ObtenerValor > 100 Then
            MessageBox.Show("El porcentaje de descuento no puede ser mayor a 100!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        'Contraseña

        'Porcentaje acumulado

        'Aplicar
        'Si es que no existe, agregar
        If dt.Select(" Tipo  = '" & dgvDescuentos.SelectedRows(0).Cells("Descripcion").Value & "' ").Count = 0 Then

            
            'Vemos si es que esta en la planilla
            Dim IDActividad As Integer = CSistema.ExecuteScalar("Select IsNull((Select  DA.IDActividad From DetalleActividad DA Join DetallePlanillaDescuentoTactico DPDT On DA.IDActividad=DPDT.IDActividad Join PlanillaDescuentoTactico PT On DPDT.IDTransaccion=PT.IDTransaccion Where DA.IDProducto=" & IDProducto & " And '" & VGFechaFacturacion & "' Between PT.Desde And PT.Hasta And PT.IDSucursal=" & vgIDSucursal & " ),0)")

            If IDActividad = 0 Then
                MessageBox.Show("No se encuentra una planilla de descuentos tacticos asociada!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If

            Dim NewRow As DataRow = dt.NewRow
            NewRow("IDTipo") = dgvDescuentos.SelectedRows(0).Cells("ID").Value
            NewRow("Tipo") = dgvDescuentos.SelectedRows(0).Cells("Descripcion").Value
            NewRow("TipoDescuento") = dgvDescuentos.SelectedRows(0).Cells("Codigo").Value
            NewRow("IDActividad") = IDActividad
            NewRow("Importe") = CSistema.CalcularPorcentaje(Precio, txtDescuento.ObtenerValor, True, False)
            NewRow("Porcentaje") = txtDescuento.ObtenerValor
            NewRow("Existencia") = 0

            dt.Rows.Add(NewRow)

        End If

        For Each DescuentoRow As DataRow In dt.Select(" Tipo  = '" & dgvDescuentos.SelectedRows(0).Cells("Descripcion").Value & "' ")
            DescuentoRow("Porcentaje") = txtDescuento.ObtenerValor
            DescuentoRow("Importe") = CSistema.CalcularPorcentaje(Precio, txtDescuento.ObtenerValor, True, False)
        Next

        CargarDatos()

    End Sub

    Sub Cerrar()

        Procesado = False
        Me.Close()


    End Sub

    Private Sub frmAplicarDescuentoTactico_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmAplicarDescuentoTactico_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub lklAplicar_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklAplicar.LinkClicked
        Aplicar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cerrar()
    End Sub

    Private Sub dgvDescuentos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDescuentos.KeyDown
        If e.KeyCode = Keys.Enter Then

            ' Your code here
            e.SuppressKeyPress = True

        End If
    End Sub

    Private Sub dgvDescuentos_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvDescuentos.SelectionChanged
        Seleccionar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Procesado = True
        Me.Close()
    End Sub

    Private Sub dgvDescuentos_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDescuentos.KeyUp
        e.SuppressKeyPress = True
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmAplicarDescuentoTactico_Activate()
        Me.Refresh()
    End Sub
End Class
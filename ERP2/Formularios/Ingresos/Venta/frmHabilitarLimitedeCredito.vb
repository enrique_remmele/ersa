﻿Public Class frmHabilitarLimitedeCredito
    'CLASES
    Public CSistema As New CSistema

    'PROPIEDADES
    Private AprobadoValue As Boolean
    Public Property Aprobado() As Boolean
        Get
            Return AprobadoValue
        End Get
        Set(ByVal value As Boolean)
            AprobadoValue = value
        End Set
    End Property

    Private NombreFrmValue As String
    Public Property NombreFrm() As String
        Get
            Return NombreFrmValue
        End Get
        Set(ByVal value As String)
            NombreFrmValue = value
        End Set
    End Property

    Sub Inicializar()

        'Formulario
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'ocultar contraseña
        txtContraseña.txt.UseSystemPasswordChar = True

        'Foco
        txtUsuario.txt.Focus()

    End Sub


    Sub Procesar()

        tsslEstado.Text = ""
        ctrError.Clear()

        Dim SQL As String = "Exec SpHabilitarLimiteCredito "
        CSistema.ConcatenarParametro(SQL, "@Usuario", txtUsuario.txt.Text)
        CSistema.ConcatenarParametro(SQL, "@Password", txtContraseña.txt.Text)
        CSistema.ConcatenarParametro(SQL, "@IDPerfil", vgIDPerfil)
        CSistema.ConcatenarParametro(SQL, "@IDTerminal", vgIDTerminal)
        CSistema.ConcatenarParametro(SQL, "@NombreFormulario", NombreFrm)
        CSistema.ConcatenarParametro(SQL, "@NombreFuncion", "HabilitarLineaCredito")

        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)
        Dim Mensaje As String = "Error al generar consulta"

        If dt Is Nothing AndAlso dt.Rows.Count = 0 Then
            tsslEstado.Text = "Atencion: " & Mensaje
            ctrError.SetError(btnAceptar, "Atencion: " & Mensaje)
            ctrError.SetIconAlignment(btnAceptar, ErrorIconAlignment.TopRight)
        End If

        Dim Resultado As DataRow = dt.Rows(0)
        Dim MensajeRetorno As String = ""
        MensajeRetorno = Resultado("Mensaje")

        If Resultado("Procesado") = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnAceptar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAceptar, ErrorIconAlignment.TopRight)
        End If


        'si procesado = true, habilitar
        If Resultado("Procesado") = True Then
            Aprobado = True
            Me.Close()
        End If

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Procesar()
    End Sub

    Private Sub frmHabilitarLimitedeCredito_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub frmHabilitarLimitedeCredito_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmHabilitarLimitedeCredito_Activate()
        Me.Refresh()
    End Sub
End Class
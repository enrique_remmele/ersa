﻿Public Class frmConfigurarDescuentosDeActividades

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PORPIEDADES
    Private dtActividadValue As DataTable
    Public Property dtActividad() As DataTable
        Get
            Return dtActividadValue
        End Get
        Set(ByVal value As DataTable)
            dtActividadValue = value
        End Set
    End Property

    Private rowDescuentoValue As DataRow
    Public Property rowDescuento() As DataRow
        Get
            Return rowDescuentoValue
        End Get
        Set(ByVal value As DataRow)
            rowDescuentoValue = value
        End Set
    End Property

    Private IDProductoValue As Integer
    Public Property IDProducto() As Integer
        Get
            Return IDProductoValue
        End Get
        Set(ByVal value As Integer)
            IDProductoValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDDepositoValue As Integer
    Public Property IDDeposito() As Integer
        Get
            Return IDDepositoValue
        End Get
        Set(ByVal value As Integer)
            IDDepositoValue = value
        End Set
    End Property

    Private SeleccionadoValue As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionadoValue = value
        End Set
    End Property

    Private TipoDescuentoValue As String
    Public Property TipoDescuento() As String
        Get
            Return TipoDescuentoValue
        End Get
        Set(ByVal value As String)
            TipoDescuentoValue = value
        End Set
    End Property

    'Variables
    Dim Precio As Decimal

    'FUNCIONES
    Sub Inicializar()

        'Variables
        Seleccionado = False

        'Producto
        Dim Producto As DataRow = CData.GetRow(" ID=" & IDProducto, "VProducto")
        If Producto IsNot Nothing Then
            txtProducto.txt.Text = Producto("Descripcion")
        End If

        Try
            txtTotalDescuento.SetValue(rowDescuento(TipoDescuento))
            Precio = rowDescuento("Precio")
        Catch ex As Exception

        End Try

        txtTotal.SetValue(CSistema.CalcularPorcentaje(Precio, txtTotalDescuento.ObtenerValor, False, True))
        txtPrecio.SetValue(Precio)

        'Funciones
        Listar()

    End Sub

    Sub Listar()

        dgw.Rows.Clear()
        Dim TotalDeuda As Decimal = 0

        Dim SQL As String = "Exec SpViewDetalleActividadConProducto @IDUsuario = " & vgIDUsuario & ", @IDSucursal = " & IDSucursal & ", @IDDeposito = " & IDDeposito & ", @IDTerminal = " & vgIDTerminal & ", @IDProducto = " & IDProducto
        Dim dttemp As DataTable = CSistema.ExecuteToDataTable(SQL)

        For Each oRow As DataRow In dttemp.Rows
            Dim Registro(11) As String
            Registro(0) = oRow("IDActividad").ToString
            Registro(1) = oRow("ID").ToString
            Registro(2) = oRow("IDProducto").ToString
            Registro(3) = False
            Registro(4) = oRow("Codigo").ToString
            Registro(5) = CSistema.FormatoMoneda(oRow("Asignado").ToString)
            Registro(6) = CSistema.FormatoNumero(oRow("DescuentoMaximo").ToString)
            Registro(7) = CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(8) = CSistema.FormatoMoneda(oRow("SaldoReal").ToString)
            Registro(9) = CSistema.FormatoNumero(oRow("DescuentoMaximo").ToString)
            Dim temp As Decimal = CSistema.CalcularPorcentaje(Precio, oRow("DescuentoMaximo"), False, True)
            Registro(10) = temp
            Registro(11) = CSistema.FormatoMoneda(oRow("Excedente").ToString)

            dgw.Rows.Add(Registro)

        Next

        CalcularTotales()

    End Sub

    Sub Cargar()

        For Each oRow As DataRow In dtactividad.Rows
            For Each oRow1 As DataGridViewRow In dgw.Rows
                If oRow("IDTransaccion") = oRow1.Cells(0).Value Then
                    oRow1.Cells(1).Value = CBool(oRow("Sel").ToString)
                    oRow1.Cells(10).Value = CSistema.FormatoMoneda(oRow("Importe").ToString)
                    oRow1.Cells(11).Value = CBool(oRow("Cancelar").ToString)
                End If
            Next
        Next

        'PintarCelda()
        CalcularTotales()

    End Sub

    Sub Seleccionar()

        If dtActividad.Columns.IndexOf("Sel") < 0 Then
            dtActividad.Columns.Add("Sel")
        End If

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDActividad As Integer = oRow.Cells("colIDActividad").Value
            For Each oRow1 As DataRow In dtActividad.Select("IDActividad=" & IDActividad)
                oRow1("Sel") = oRow.Cells(3).Value
                oRow1("DescuentoMaximo") = oRow.Cells(9).Value
            Next

        Next

        'Validar
        If txtSaldo.ObtenerValor <> 0 Then
            CSistema.MostrarError("El saldo tiene que ser 0!", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        Me.Seleccionado = True

        Me.Close()

    End Sub

    Sub CalcularTotales()

        Dim Contador As Decimal = 0
        Dim TotalDescuento As Decimal = txtTotal.ObtenerValor
        Dim Saldo As Decimal = 0

        For Each orow As DataGridViewRow In dgw.Rows
            If orow.Cells("colSel").Value = True Then
                Contador = Contador + orow.Cells("colDescuento").Value
            End If
        Next

        Saldo = TotalDescuento - Contador

        txtSaldo.SetValue(Saldo)

    End Sub

    Private Sub frmConfigurarDescuentosDeActividades_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub dgw_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellClick

        If e.ColumnIndex = 1 Then

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(10).ReadOnly = False
                        oRow.Cells(11).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(10)

                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(10).ReadOnly = True
                        oRow.Cells(11).ReadOnly = True
                        oRow.Cells(10).Value = oRow.Cells(9).Value
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

        End If

    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If e.ColumnIndex = 9 Then

            If IsNumeric(dgw.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgw, mensaje)
                ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgw.CurrentCell.Value = dgw.CurrentRow.Cells(6).Value

            Else
                dgw.CurrentCell.Value = CSistema.FormatoMoneda(dgw.CurrentRow.Cells(9).Value)
                dgw.CurrentRow.Cells("colDescuento").Value = CSistema.CalcularPorcentaje(Precio, dgw.CurrentRow.Cells(9).Value, False, True)
            End If

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown
        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(3).Value = False Then
                        oRow.Cells(3).Value = True
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(9)
                    Else
                        oRow.Cells(3).Value = False
                        oRow.Cells(9).Value = oRow.Cells(6).Value
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If
    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Index = e.RowIndex Then
                If oRow.Cells(3).Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                End If

                oRow.DefaultCellStyle.Font = f1

                oRow.Cells(9).Selected = True

            Else

                oRow.DefaultCellStyle.Font = f2

                If oRow.Cells(3).Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.White
                Else
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                End If
            End If
        Next
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Tab Then



            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells(9)
        End If

    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmConfigurarDescuentosDeActividades_Activate()
        Me.Refresh()
    End Sub
End Class
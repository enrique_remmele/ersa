﻿Public Class frmHabilitarProceso

    'CLASES
    Dim CSistema As New CSistema

    'EVENTOS

    'PROPIEDADES
    Private MensajeValue As String
    Public Property Mensaje() As String
        Get
            Return MensajeValue
        End Get
        Set(ByVal value As String)
            MensajeValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private FuncionValue As String
    Public Property Funcion() As String
        Get
            Return FuncionValue
        End Get
        Set(ByVal value As String)
            FuncionValue = value
        End Set
    End Property

    Private FormularioValue As String
    Public Property Formulario() As String
        Get
            Return FormularioValue
        End Get
        Set(ByVal value As String)
            FormularioValue = value
        End Set
    End Property

    Private AprobadoValue As Boolean
    Public Property Aprobado() As Boolean
        Get
            Return AprobadoValue
        End Get
        Set(ByVal value As Boolean)
            AprobadoValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Private OperacionValue As String
    Public Property Operacion() As String
        Get
            Return OperacionValue
        End Get
        Set(ByVal value As String)
            OperacionValue = value
        End Set
    End Property

    Private TablaValue As String
    Public Property Tabla() As String
        Get
            Return TablaValue
        End Get
        Set(ByVal value As String)
            TablaValue = value
        End Set
    End Property

    Private IDUsuarioValue As Integer
    Public Property IDUsuario() As Integer
        Get
            Return IDUsuarioValue
        End Get
        Set(ByVal value As Integer)
            IDUsuarioValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private ComprobanteValue As String
    Public Property Comprobante() As String
        Get
            Return ComprobanteValue
        End Get
        Set(ByVal value As String)
            ComprobanteValue = value
        End Set
    End Property

    Private IDTerminalValue As Integer
    Public Property IDTerminal() As Integer
        Get
            Return IDTerminalValue
        End Get
        Set(ByVal value As Integer)
            IDTerminalValue = value
        End Set
    End Property

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        Me.AcceptButton = New Button

        'Mensaje
        lblMensaje.Text = Mensaje

        'Contraseña
        txtContraseña.txt.UseSystemPasswordChar = True

        Aprobado = False

        'Foco
        txtUsuario.txt.Focus()

    End Sub

    Public Sub Validar()

        Dim SQL As String

        SQL = "Select " & vgOwnerDBFunction & ".FCredenciales('" & txtUsuario.txt.Text & "','" & txtContraseña.txt.Text & "'," & IDOperacion & ",'" & Formulario & "','" & Funcion & "')"

        Aprobado = CType(CSistema.ExecuteScalar(SQL), Boolean)

        If Aprobado = True Then
            Aprobado = True
            Me.Close()
        Else
            CSistema.MostrarError("Credenciales no validas!", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
            txtUsuario.txt.SelectAll()
            txtUsuario.txt.Focus()
        End If

    End Sub

    Sub Guardar()

        Dim param(-1) As SqlClient.SqlParameter

        'Transaccion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Tabla", Tabla, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", IDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Comprobante", Comprobante, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", IDTerminal, ParameterDirection.Input)
       

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpLogSuceso", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            ctrError.Clear()
        Else
            Procesado = True
        End If

    End Sub

    Private Sub frmHabilitarProceso_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmHabilitarProceso_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Validar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmHabilitarProceso_Activate()
        Me.Refresh()
    End Sub
End Class
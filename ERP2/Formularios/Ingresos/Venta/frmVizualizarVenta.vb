﻿Public Class frmVizualizarVenta

    'CLASES
    Dim CSistema As New CSistema
    Dim CDetalleImpuesto As New CDetalleImpuesto

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim dtTerminalPuntoExpedicion As DataTable

    Sub Inicializar()

        'Controles
        txtCliente.Conectar()

        'Otros
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False
        txtVencimiento.Enabled = False
        txtPlazo.Enabled = False
        OcxImpuesto1.Inicializar()
        OcxImpuesto1.dg.ReadOnly = True

        'Propiedades
        IDTransaccion = 0
        vNuevo = True

        'Funciones
        CargarInformacion()

        'Foco
        txtNroComprobante.Focus()
        txtNroComprobante.txt.Focus()
        txtNroComprobante.txt.SelectAll()

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'Combrobante
        CSistema.CargaControl(vControles, cbxPuntoExpedicion)

        'Cabecera
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, cbxCondicion)
        CSistema.CargaControl(vControles, txtVencimiento)
        CSistema.CargaControl(vControles, txtPlazo)
        CSistema.CargaControl(vControles, cbxDeposito)
        CSistema.CargaControl(vControles, txtDetalle)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, txtCotizacion)
        CSistema.CargaControl(vControles, txtRemision)

        'Detalle
        CSistema.CargaControl(vControles, lvLista)

        dtDetalle = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleVenta").Clone

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CONDICION
        cbxCondicion.cbx.Items.Add("CONTADO")
        cbxCondicion.cbx.Items.Add("CREDITO")
        cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR CONTROLES
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=1")

        'Puntos de Expediciones
        dtTerminalPuntoExpedicion = CSistema.ExecuteToDataTable("Select * From VTerminalPuntoExpedicion Where IDTerminal=" & vgIDTerminal)
        CSistema.SqlToComboBox(cbxPuntoExpedicion.cbx, dtTerminalPuntoExpedicion, "IDPuntoExpedicion", "PE")

        'Ciudades
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Descripcion From VSucursal Order By 2")

        'Depositos
        CSistema.SqlToComboBox(cbxDeposito.cbx, "Select ID, Descripcion From Deposito Order By 2")
        cbxDeposito.cbx.Text = vgDeposito

        'Vendedor
        CSistema.SqlToComboBox(cbxVendedor.cbx, "Select ID, Nombres From Vendedor Order By 2")

        'Promotor
        CSistema.SqlToComboBox(cbxPromotor.cbx, "Select ID, Nombres From Promotor Order By 2")

        'Listas de Precio
        CSistema.SqlToComboBox(cbxListaPrecio.cbx, "Select ID, Descripcion From ListaPrecio Where Estado='True' Order By 2")

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Referencia From Moneda")
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtNroComprobante.txt.Focus()
        txtNroComprobante.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From Venta Where Comprobante Like '%" & txtNroComprobante.txt.Text & "%' And IDTipoComprobante=" & cbxTipoComprobante.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtNroComprobante, mensaje)
            ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VVenta Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select * From VDetalleVenta Where IDTransaccion=" & IDTransaccion & " Order By ID").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtNroComprobante, mensaje)
            ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        'Punto de Expedicion
        cbxPuntoExpedicion.cbx.Text = oRow("PE").ToString
        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.txt.Text = oRow("TipoComprobante").ToString
        txtReferenciaSucursal.txt.Text = oRow("ReferenciaSucursal").ToString
        txtReferenciaSucursal.txt.Text = oRow("ReferenciaSucursal").ToString
        txtNroComprobante.txt.Text = oRow("NroComprobante").ToString

        txtCliente.SetValue(oRow("IDCliente").ToString)
        txtDireccion.txt.Text = oRow("Direccion").ToString
        txtTelefono.txt.Text = oRow("Telefono").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        cbxCondicion.cbx.Text = oRow("Condicion").ToString

        'Condicion
        If CBool(oRow("Credito").ToString) = False Then
            txtVencimiento.txt.Clear()
            txtPlazo.txt.Clear()
            'cbxCondicion.cbx.Text  = "CREDITO"
        Else
            'txtVencimiento.SetValue(CDate(oRow("FechaVencimiento").ToString))
            'calcular plazo
            'cbxCondicion.cbx.SelectedIndex = 1
        End If

        cbxDeposito.txt.Text = oRow("Deposito").ToString
        cbxVendedor.txt.Text = oRow("Vendedor").ToString
        txtDetalle.txt.Text = oRow("Observacion").ToString
        cbxListaPrecio.txt.Text = oRow("Lista de Precio").ToString
        txtDescuento.txt.Text = oRow("Descuento").ToString
        cbxMoneda.txt.Text = oRow("Moneda").ToString
        txtCotizacion.txt.Text = oRow("Cotizacion").ToString
        cbxPromotor.txt.Text = oRow("Promotor").ToString
        txtRemision.txt.Text = oRow("Remision").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos el detalle
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)
        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        OcxImpuesto1.CargarValores(CDetalleImpuesto.dt)

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        lvLista.Items.Clear()

        'Variables
        Dim Total As Decimal = 0

        'Cargamos registro por registro
        For Each oRow As DataRow In dtDetalle.Rows

            Dim item As ListViewItem = lvLista.Items.Add(oRow("IDProducto").ToString)

            Dim Descripcion As String
            If oRow("Observacion").ToString.Trim.Length > 0 Then
                Descripcion = oRow("Descripcion").ToString.Trim & " - " & oRow("Observacion").ToString.Trim
            Else
                Descripcion = oRow("Descripcion").ToString.Trim
            End If

            item.SubItems.Add(Descripcion)
            item.SubItems.Add(oRow("Ref Imp").ToString)
            item.SubItems.Add(CSistema.FormatoNumero(oRow("Cantidad").ToString))
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("PrecioUnitario").ToString))
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("DescuentoUnitario").ToString))
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("PorcentajeDescuento").ToString))
            If CBool(oRow("Exento").ToString) = True Then
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Exento").ToString))
                item.SubItems.Add(CSistema.FormatoMoneda(0))
            Else
                item.SubItems.Add(CSistema.FormatoMoneda(0))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Total").ToString))
            End If

        Next

        'Bloqueamos la cabecera si corresponde
        If dtDetalle.Rows.Count > 0 Then
            cbxDeposito.SoloLectura = True
            txtCliente.SoloLectura = True
        Else
            cbxDeposito.SoloLectura = False
            txtCliente.SoloLectura = False
        End If

    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmVizualizarVenta_Activate()
        Me.Refresh()
    End Sub

End Class
﻿Public Class frmAprobacionPedido
    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim vControles() As Control
    Dim dtPedidos As New DataTable

    'FUNCIONES
    Sub Inicializar()
        CargarInformacion()
        'txtDesde.PrimerDiaMes()
        txtDesde.Hoy()
        txtHasta.Hoy()
    End Sub
    Sub CargarInformacion()
        'Cliente
        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito  From VCliente "
        txtCliente.frm = Me

        'Producto
        txtProducto.Conectar()

        ReDim vControles(-1)
        'Cabecera
        'CSistema.CargaControl(vControles, cbxDeposito)
        'CSistema.CargaControl(vControles, cbxVendedor)
        'CSistema.CargaControl(vControles, cbxZonaVenta)
        'CSistema.CargaControl(vControles, cbxGrupo)
        'CSistema.CargaControl(vControles, cbxOrdenadoPor)
        'CSistema.CargaControl(vControles, txtCliente)
        'CSistema.CargaControl(vControles, txtProducto)
        'CSistema.CargaControl(vControles, cbxResumen)
    End Sub

    Private Sub chkDeposito_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = chkProducto.Valor
    End Sub

    Private Sub chkVendedor_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = chkVendedor.Valor
    End Sub

    Private Sub chkZonaVenta_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkZonaVenta.PropertyChanged
        cbxZonaVenta.Enabled = chkZonaVenta.Valor
    End Sub

    Private Sub chkGrupo_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkGrupo.PropertyChanged
        cbxGrupo.Enabled = chkGrupo.Valor
    End Sub

    Private Sub chkDetallePor_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkDetallePor.PropertyChanged
        cbxOrdenadoPor.Enabled = chkDetallePor.Valor
    End Sub

    Private Sub chkCliente_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = chkCliente.Valor
    End Sub

    Private Sub frmAprobacionPedido_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub lblListar_Click(sender As System.Object, e As System.EventArgs) Handles lblListar.Click
        ListarPedidos()
    End Sub
    Private Sub ListarPedidos()
        Dim Str As String
        Str = "Select * from vPedido where "

        'CARGAR ESTRUCTURA DEL DETALLE
        'dtPedidos = CSistema.ExecuteToDataTable("Select * From VVentasParaLote Where IDSucursal=" & cbxSucursal.GetValue).Copy
    End Sub

    Private Sub btnEsteMes_Click(sender As System.Object, e As System.EventArgs) Handles btnEsteMes.Click
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()
    End Sub

    Private Sub btnAyer_Click(sender As System.Object, e As System.EventArgs) Handles btnAyer.Click
        txtDesde.Ayer()
        txtHasta.Ayer()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmAprobacionPedido_Activate()
        Me.Refresh()
    End Sub
End Class
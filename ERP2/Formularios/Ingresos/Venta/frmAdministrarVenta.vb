﻿Public Class frmAdministrarVenta

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private dtDocumentosValue As DataTable
    Public Property dtDocumentos() As DataTable
        Get
            Return dtDocumentosValue
        End Get
        Set(ByVal value As DataTable)
            dtDocumentosValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim vInformeErrores As Boolean

    'FUNCIONES
    Sub Inicializar()

        IDOperacion = CSistema.ObtenerIDOperacion(frmVenta.Name, "VENTAS CLIENTES", "FATCLI")

        'Funciones
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Tipos de Comprobantes
        CSistema.SqlToComboBox(cbxComprobante, "Select ID, Codigo From VTipoComprobante Where IDOperacion=" & IDOperacion)

        'Sucursales
       ' CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion   From Sucursal Order By 2")

        Try
            cbxSucursalOperacion.SelectedValue(vgIDSucursal)

        Catch ex As Exception

        End Try

    End Sub

    Sub GuardarInformacion()

    End Sub

    Sub ListarComprobantes(Optional ByVal Comprobante As String = "", Optional ByVal IDTipoComprobante As Integer = 0, Optional ByVal IDSucursal As Integer = 0)

        vInformeErrores = False

        Dim Consulta As String = "Select 'ID'=IDTransaccion, 'Suc.'=Sucursal, 'Comprobante'=[Cod.] + ' ' + Comprobante, 'Ref'=Referencia, Cliente, RUC, Fec, Condicion, 'F. Pag.'=FormaPago, 'NumeroPedido'=(Select Top(1) Ped.Numero From Pedido Ped  join pedidoventa pv on ped.IDTransaccion = pv.IDTransaccionPedido Where pv.IDTransaccionVenta=V.IDTransaccion), [Fec. Venc.], Total, Saldo, 'Estado'=EstadoVenta, Anulado,  'Usuario'=UsuarioIdentificador, 'NroLote'=(Select Min(VD.Numero) From VentaLoteDistribucion VDL join LoteDistribucion VD on VDL.IDTransaccionLote = VD.IDTransaccion Where VDL.IDTransaccionVenta=V.IDTransaccion), 'Distribuidor'=(Select Top(1) D.Nombres From VentaLoteDistribucion VDL join LoteDistribucion VD on VDL.IDTransaccionLote = VD.IDTransaccion join Distribuidor D on VD.IDDistribuidor = D.ID Where VDL.IDTransaccionVenta=V.IDTransaccion), CancelarAutomatico From VVenta V  "
        Dim Where As String = ""
        Dim OrderBy As String = " Order By Comprobante "

        'Si es por comprobante
        If Comprobante.Length > 0 Then

            Where = " Where IDSucursal=" & cbxSucursalOperacion.GetValue & " And IDTipoComprobante=" & IDTipoComprobante & " And Comprobante Like '%" & Comprobante.Trim & "%' "

        End If

        ''Si es por Sucursal
        'If IDSucursal > 0 Then
        '    Where = " Where Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesdeSucursal, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHastaSucursal, True, False) & "' " & " and IDSucursal = " & cbxSucursal.SelectedValue
        '    If chkInlcuirAnuladosSucursal.Checked = False Then
        '        Where = Where & " And Anulado='False' "
        '    End If
        'End If

        'Si es normal
        If Where = "" Then

            Where = " Where IDSucursal=" & cbxSucursalOperacion.GetValue & " And Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesdeGeneral, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHastaGeneral, True, False) & "' "
            If chkInlcuirAnuladosGeneral.Checked = False Then
                Where = Where & " And Anulado='False' "
            End If

        End If

        'Listar 
        dtDocumentos = CSistema.ExecuteToDataTable(Consulta & " " & Where & " " & OrderBy)
        DataGridView1.DataSource = dtDocumentos

        'For Each oRow As DataGridViewRow In DataGridView1.Rows
        '    'If oRow.Cells(11).Value = True Then
        '    '    oRow.Cells(3).Value = "---"
        '    '    oRow.Cells(4).Value = "---"
        '    '    oRow.Cells(5).Value = "---"
        '    '    oRow.Cells(6).Value = "---"
        '    '    oRow.Cells(7).Value = "---"
        '    '    oRow.Cells(8).Value = 0
        '    '    oRow.Cells(9).Value = 0
        '    '    oRow.DefaultCellStyle.ForeColor = Color.Red
        '    'End If

        'Next

        If DataGridView1.ColumnCount = 0 Then
            Exit Sub
        End If

        'Formato
        DataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(vgColorDataGridAlternancia)
        DataGridView1.Columns("Anulado").Visible = False
        DataGridView1.Columns("ID").Visible = False


        For c As Integer = 0 To DataGridView1.ColumnCount - 1
            DataGridView1.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Next

        DataGridView1.Columns("CancelarAutomatico").Visible = False
        DataGridView1.Columns("Suc.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns("Fec").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns("NumeroPedido").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns("Condicion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns("Fec. Venc.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        DataGridView1.Columns("Saldo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        DataGridView1.Columns("Total").DefaultCellStyle.Format = "N0"
        DataGridView1.Columns("Saldo").DefaultCellStyle.Format = "N0"

        txtCantidadOperacion.SetValue(DataGridView1.Rows.Count)

    End Sub

    Sub ListarComprobantesErrores(Where As String)

        vInformeErrores = True

        Dim Consulta As String = "Select 'ID'=IDTransaccion, 'Suc.'=Sucursal, 'Comprobante'=[Cod.] + ' ' + Comprobante, 'Ref'=Referencia, Cliente, RUC, Fec, Condicion, [Fec. Venc.], Total, Saldo, 'F. Pag.'=FormaPago, 'Estado'=dbo.FValidarVenta(IDTransaccion), Anulado, 'Usuario'=UsuarioIdentificador, CancelarAutomatico From VVenta V "
        Dim OrderBy As String = " Order By FechaEmision "

        'Listar 
        dtDocumentos = CSistema.ExecuteToDataTable(Consulta & Where & OrderBy)
        DataGridView1.DataSource = dtDocumentos

        If DataGridView1.ColumnCount = 0 Then
            Exit Sub
        End If

        'Formato
        DataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(vgColorDataGridAlternancia)
        DataGridView1.Columns("Anulado").Visible = False
        DataGridView1.Columns("ID").Visible = False

        For c As Integer = 0 To DataGridView1.ColumnCount - 1
            DataGridView1.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Next

        DataGridView1.Columns("CancelarAutomatico").Visible = False
        DataGridView1.Columns("Suc.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns("Fec").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns("Condicion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns("Fec. Venc.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        DataGridView1.Columns("Saldo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        DataGridView1.Columns("Total").DefaultCellStyle.Format = "N0"
        DataGridView1.Columns("Saldo").DefaultCellStyle.Format = "N0"

        txtCantidadOperacion.SetValue(DataGridView1.Rows.Count)

    End Sub

    Sub VerDetalle()

        If DataGridView1.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione un registro!"
            ctrError.SetError(btnVerDetalle, mensaje)
            ctrError.SetIconAlignment(btnVerDetalle, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim IDTransaccion As Integer
        IDTransaccion = DataGridView1.SelectedRows(0).Cells(0).Value

        'Dim frm As New frmVizualizarVenta
        'frm.Inicializar()
        'frm.CargarOperacion(IDTransaccion)
        'frm.ShowDialog()

        Dim frm As New frmVenta
        frm.IDTransaccion = IDTransaccion
        frm.Inicializar()
        frm.CargarOperacion(IDTransaccion)
        frm.btnNuevo.Enabled = False
        frm.btnAsiento.Enabled = False
        frm.ShowDialog()



    End Sub

    Sub VerPedido()
        Try
            If DataGridView1.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(btnVerPedido, mensaje)
                ctrError.SetIconAlignment(btnVerPedido, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim IDTransaccion As Integer
            Dim IDTransaccionPedido As Integer
            IDTransaccion = DataGridView1.SelectedRows(0).Cells(0).Value
            IDTransaccionPedido = CSistema.ExecuteScalar("Select IDTransaccionPedido From PedidoVenta WHERE IDTransaccionVenta=" & IDTransaccion )

            Dim frm As New frmPedido
            frm.Show()
            frm.CargarOperacion(IDTransaccionPedido)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Sub VerLote()

        Try
            If DataGridView1.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(btnVerLote, mensaje)
                ctrError.SetIconAlignment(btnVerLote, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim IDTransaccion As Integer
            Dim IDTransaccionLote As Integer
            IDTransaccion = DataGridView1.SelectedRows(0).Cells(0).Value
            IDTransaccionLote = CSistema.ExecuteScalar("Select IDTransaccion From LoteDistribucion where IDTransaccion=" & IDTransaccion)

            Dim frm As New frmLoteDistribucion
            frm.Inicializar()
            frm.CargarOperacion(IDTransaccion)
            FGMostrarFormulario(Me, frm, "Lote de Distribucion", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Sub VerAsiento()

        If DataGridView1.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione un registro!"
            ctrError.SetError(btnAsiento, mensaje)
            ctrError.SetIconAlignment(btnAsiento, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim Comprobante As String
        Dim Cliente As String
        Dim IDTransaccion As Integer

        IDTransaccion = DataGridView1.SelectedRows(0).Cells(0).Value
        Comprobante = DataGridView1.SelectedRows(0).Cells(2).Value
        Cliente = DataGridView1.SelectedRows(0).Cells(3).Value

        Dim frm As New frmVisualizarAsiento
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.Text = "Venta: " & Comprobante & "  -  " & Cliente
        frm.IDTransaccion = IDTransaccion

        'Mostramos
        frm.ShowDialog(Me)

    End Sub

    Sub Imprimir()

        If DataGridView1.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim CReporte As New ERP.Reporte.CReporteVentas
        Dim Total As Double = DataGridView1.SelectedRows(0).Cells("Total").Value
        Dim NumeroALetra As String = CSistema.NumeroALetra(Total)

        Dim Where As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar IDTransaccion
        Where = " Where IDTransaccion =" & DataGridView1.SelectedRows(0).Cells(0).Value

        Dim Path As String = vgImpresionFacturaPath

        CReporte.ImprimirFactura(frm, Where, Path, vgImpresionFacturaImpresora, NumeroALetra, CSistema.FormatoMoneda(Total))

    End Sub

    Sub Anular()

        'Seleccion
        If DataGridView1.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione un registro!"
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim IDTransaccion As Integer
        IDTransaccion = DataGridView1.SelectedRows(0).Cells(0).Value

        Dim frm As New frmAgregarMotivoAnulacion
        frm.IDTransaccion = IDTransaccion
        frm.Text = " Motivo Anulacion "
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.ShowDialog(Me)

        If frm.Procesado = False Then
            Exit Sub
        End If

        ListarComprobantes()

    End Sub

    Sub Eliminar()

        'Seleccion
        If DataGridView1.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione un registro!"
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim IDTransaccion As Integer
        IDTransaccion = DataGridView1.SelectedRows(0).Cells(0).Value

        'Verificar que el documento no esta ya anulado
        Dim Anulado As Boolean
        Anulado = CType(CSistema.ExecuteScalar("Select IsNull((Select Anulado From Venta Where IDTransaccion=" & IDTransaccion & "), 'False')"), Boolean)
        If Anulado = False Then
            Dim mensaje As String = "El documento debe estar anulado para poder eliminarlo!"
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consultar
        If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        'Anulamos
        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", ERP.CSistema.NUMOperacionesRegistro.DEL.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Anular Registro
        If CSistema.ExecuteStoreProcedure(param, "SpVenta", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub ListarDocumentosAsociados()

        If DataGridView1.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Try

            Dim IDTransaccion As Integer
            Dim Total As Decimal
            Dim CancelarAutomatico As Boolean

            IDTransaccion = DataGridView1.SelectedRows(0).Cells("ID").Value
            Total = DataGridView1.SelectedRows(0).Cells("Total").Value
            CancelarAutomatico = DataGridView1.SelectedRows(0).Cells("CancelarAutomatico").Value
            OcxDocumentosVentas1.IDTransaccion = IDTransaccion
            OcxDocumentosVentas1.Total = Total
            OcxDocumentosVentas1.CancelarAutomatico = CancelarAutomatico
            OcxDocumentosVentas1.CargarDocumentos()

        Catch ex As Exception

        End Try

    End Sub

    Sub RepararVenta()

        'Validar
        If DataGridView1.SelectedRows.Count = 0 Then
            Exit Sub
        End If


        'Hayar valores
        Dim IDTransaccion As Integer

        IDTransaccion = DataGridView1.SelectedRows(0).Cells(0).Value

        Dim SQL As String
        SQL = "Exec SpRepararVenta @IDTransaccion=" & IDTransaccion

        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        If dt Is Nothing Then
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim Resultado As DataRow = dt.Rows(0)

        If Resultado("Procesado") = True Then
            MessageBox.Show(Resultado("Mensaje").ToString, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information)

            'Actualizar la lista
            Dim dtVenta As DataTable = CSistema.ExecuteToDataTable("Select 'ID'=IDTransaccion, 'Suc.'=Sucursal, 'Comprobante'=[Cod.] + ' ' + Comprobante, 'Ref'=Referencia, Cliente, RUC, Fec, Condicion,'NumeroPedido'=(Select Top(1) VDL.NumeroPedido From VVentaLoteDistribucion VDL Where VDL.IDTransaccionVenta=V.IDTransaccion), [Fec. Venc.], Total, Saldo, 'Estado'=EstadoVenta, Anulado,  'Usuario'=UsuarioIdentificador, 'NroLote'=(Select Min(VDL.Numero) From VVentaLoteDistribucion VDL Where VDL.IDTransaccionVenta=V.IDTransaccion), 'Distribuidor'=(Select Top(1) VDL.Distribuidor From VVentaLoteDistribucion VDL Where VDL.IDTransaccionVenta=V.IDTransaccion) From VVenta V Where IDTransaccion=" & IDTransaccion)
            Dim Venta As DataRow = dtVenta.Rows(0)
            Dim Indice As Integer = DataGridView1.SelectedRows(0).Index

            DataGridView1.Rows(Indice).Cells("Saldo").Value = Venta("Saldo")
            DataGridView1.Rows(Indice).Cells("Estado").Value = Venta("Estado")

            'For c As Integer = 0 To DataGridView1.ColumnCount - 1
            '    If DataGridView1.Columns(c).Name = "Saldo" Or DataGridView1.Columns(c).Name = "Estado" Then
            '        Try
            '            DataGridView1.Rows(Indice).Cells(c).Value = Venta(c)
            '        Catch ex As Exception

            '        End Try
            '    End If

            'Next

            OcxDocumentosVentas1.CargarDocumentos()

        Else
            MessageBox.Show(Resultado("Mensaje").ToString, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

    End Sub

    Sub RepararLote()

        If DataGridView1.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        'Hayar valores
        For Each oRow As DataGridViewRow In DataGridView1.Rows
            Dim IDTransaccion As Integer

            IDTransaccion = oRow.Cells("ID").Value

            Dim SQL As String
            SQL = "Exec SpRepararVenta @IDTransaccion=" & IDTransaccion

            Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

            If dt Is Nothing Then
                GoTo seguir
            End If

            If dt.Rows.Count = 0 Then
                GoTo seguir
            End If

            Dim Resultado As DataRow = dt.Rows(0)

            If Resultado("Procesado") = False Then
                MessageBox.Show(Resultado("Mensaje").ToString, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

seguir:
        Next

        MessageBox.Show("Registros procesados!", "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)
        ListarComprobantesErrores(" Where V.Anulado=0 And V.Saldo <>((V.Total	+ IsNull((Select Sum(Importe) From VNotaDebitoVenta Where IDTransaccionVenta=V.IDTransaccion),0)) - (IsNull((Select Sum(Importe) From VVentaDetalleCobranza Where IDTransaccion=V.IDTransaccion And Anulado='False'),0) + IsNull((Select Sum(NC.Importe) From vNotaCreditoVenta NC Where NC.IDTransaccionVenta=V.IDTransaccion And NC.Anulado='False'),0))) And V.Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesdeReporte, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHastaReporte, True, False) & "'")

    End Sub

    Sub Reparar()

        If vInformeErrores = False Then
            RepararVenta()
        Else
            'Consultar
            If MessageBox.Show("Desea reparar todos los registros mostrados en la lista? Esto puede demorar unos minutos", "Reparar", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If

            RepararLote()
        End If

    End Sub

    Private Sub frmAdministrarVenta_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
        LiberarMemoria()
    End Sub

    Private Sub frmAdministrarVenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnVerDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerDetalle.Click
        VerDetalle()
    End Sub

    Private Sub txtComprobante_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtComprobante.KeyUp

        If e.KeyCode = Keys.Enter Then

            If IsNumeric(cbxComprobante.SelectedValue) = False Then
                Exit Sub
            End If

            If CInt(cbxComprobante.SelectedValue) = 0 Then
                Exit Sub
            End If

            ListarComprobantes(txtComprobante.Text.Trim, cbxComprobante.SelectedValue, 0)

        End If

    End Sub

    Private Sub btnGeneralPorDia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGeneralPorDia.Click

        dtpDesdeGeneral.Value = Date.Now
        dtpHastaGeneral.Value = Date.Now
        ListarComprobantes()

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click

        dtpDesdeGeneral.Value = Date.Now.AddDays(-1)
        dtpHastaGeneral.Value = Date.Now.AddDays(-1)
        ListarComprobantes()

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Dim DiaHoy As Integer
        Dim Desde As Date
        Dim Hasta As Date = Date.Now

        'Vemos que dia es hoy
        DiaHoy = (Date.Now.DayOfWeek - 1)

        If DiaHoy > 1 Then
            Desde = Date.Now.AddDays(-DiaHoy)
        Else
            Desde = Date.Now
        End If

        dtpDesdeGeneral.Value = Desde
        dtpHastaGeneral.Value = Hasta

        ListarComprobantes()

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click

        Dim Desde As Date = Date.Now
        Dim Hasta As Date = Date.Now
        Dim PrimerDia As Integer
        Dim UltimoDia As Integer
        Dim DiaActual As Integer

        PrimerDia = Date.Now.Day - 1
        UltimoDia = Date.DaysInMonth(Date.Now.Year, Date.Now.Month)
        DiaActual = Date.Now.Day

        UltimoDia = UltimoDia - DiaActual

        Desde = Desde.AddDays(-PrimerDia)
        Hasta = Hasta.AddDays(UltimoDia)

        dtpDesdeGeneral.Value = Desde
        dtpHastaGeneral.Value = Hasta

        ListarComprobantes()

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click

        Dim Desde As Date = Date.Now.AddMonths(-1)
        Dim Hasta As Date = Date.Now.AddMonths(-1)
        Dim PrimerDia As Integer
        Dim UltimoDia As Integer
        Dim DiaActual As Integer

        PrimerDia = Date.Now.Day - 1
        UltimoDia = Date.DaysInMonth(Hasta.Year, Hasta.Month)
        DiaActual = Date.Now.Day

        UltimoDia = UltimoDia - DiaActual

        Desde = Desde.AddDays(-PrimerDia)
        Hasta = Hasta.AddDays(UltimoDia)

        dtpDesdeGeneral.Value = Desde
        dtpHastaGeneral.Value = Hasta

        ListarComprobantes()

    End Sub

    Sub AgregarComprobantes()

        Dim frm As New frmAgregarComprobantesAnulado
        frm.Text = " Agregar comprobantes "
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        FGMostrarFormulario(Me, frm, "Agregar comprobante anulado", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        ListarComprobantes()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VerAsiento()
    End Sub

    Private Sub btnAgregarComprobantes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarComprobantes.Click
        AgregarComprobantes()
    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles DataGridView1.SelectionChanged
        ListarDocumentosAsociados()
    End Sub

    Private Sub btnReparar_Click(sender As System.Object, e As System.EventArgs) Handles btnReparar.Click
        Reparar()
    End Sub

    Private Sub btnVerPedido_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerPedido.Click
        VerPedido()
    End Sub

    Private Sub Button16_Click(sender As System.Object, e As System.EventArgs) Handles Button16.Click
        dtpDesdeSucursal.Value = Date.Now
        dtpHastaSucursal.Value = Date.Now
        ListarComprobantes("", 0, cbxSucursalOperacion.GetValue)
    End Sub

    Private Sub Button13_Click(sender As System.Object, e As System.EventArgs) Handles Button13.Click
        dtpDesdeSucursal.Value = Date.Now.AddDays(-1)
        dtpHastaSucursal.Value = Date.Now.AddDays(-1)
        ListarComprobantes("", 0, cbxSucursalOperacion.GetValue)
    End Sub

    Private Sub Button15_Click(sender As System.Object, e As System.EventArgs) Handles Button15.Click
        Dim DiaHoy As Integer
        Dim Desde As Date
        Dim Hasta As Date = Date.Now

        'Vemos que dia es hoy
        DiaHoy = (Date.Now.DayOfWeek - 1)

        If DiaHoy > 1 Then
            Desde = Date.Now.AddDays(-DiaHoy)
        Else
            Desde = Date.Now
        End If

        dtpDesdeSucursal.Value = Desde
        dtpHastaSucursal.Value = Hasta

        ListarComprobantes("", 0, cbxSucursalOperacion.GetValue)
    End Sub

    Private Sub Button14_Click(sender As System.Object, e As System.EventArgs) Handles Button14.Click
        Dim Desde As Date = Date.Now
        Dim Hasta As Date = Date.Now
        Dim PrimerDia As Integer
        Dim UltimoDia As Integer
        Dim DiaActual As Integer

        PrimerDia = Date.Now.Day - 1
        UltimoDia = Date.DaysInMonth(Date.Now.Year, Date.Now.Month)
        DiaActual = Date.Now.Day

        UltimoDia = UltimoDia - DiaActual

        Desde = Desde.AddDays(-PrimerDia)
        Hasta = Hasta.AddDays(UltimoDia)

        dtpDesdeSucursal.Value = Desde
        dtpHastaSucursal.Value = Hasta

        ListarComprobantes("", 0, cbxSucursalOperacion.GetValue)
    End Sub

    Private Sub Button12_Click(sender As System.Object, e As System.EventArgs) Handles Button12.Click
        Dim Desde As Date = Date.Now.AddMonths(-1)
        Dim Hasta As Date = Date.Now.AddMonths(-1)
        Dim PrimerDia As Integer
        Dim UltimoDia As Integer
        Dim DiaActual As Integer

        PrimerDia = Date.Now.Day - 1
        UltimoDia = Date.DaysInMonth(Hasta.Year, Hasta.Month)
        DiaActual = Date.Now.Day

        UltimoDia = UltimoDia - DiaActual

        Desde = Desde.AddDays(-PrimerDia)
        Hasta = Hasta.AddDays(UltimoDia)

        dtpDesdeSucursal.Value = Desde
        dtpHastaSucursal.Value = Hasta

        ListarComprobantes("", 0, cbxSucursalOperacion.GetValue)
    End Sub

    Private Sub Button11_Click(sender As System.Object, e As System.EventArgs) Handles Button11.Click
        ListarComprobantes("", 0, cbxSucursalOperacion.GetValue)
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        ListarComprobantesErrores(" Where V.Total<>IsNull((Select Sum(DV.Total) From DetalleVenta DV Where DV.IDTransaccion=V.IDTransaccion), 0) And V.Anulado=0 And V.Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesdeReporte, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHastaReporte, True, False) & "'")
    End Sub

    Private Sub ExportarAPlanillaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExportarAPlanillaToolStripMenuItem.Click
        CSistema.dtToExcel(DataGridView1.DataSource)
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        ListarComprobantesErrores(" Where V.Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesdeReporte, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHastaReporte, True, False) & "' And dbo.FValidarVenta(V.IDTransaccion)!='---' ")
    End Sub

    Private Sub btnVerLote_Click(sender As System.Object, e As System.EventArgs) Handles btnVerLote.Click
        VerLote()
    End Sub

    '10-06-2021 - SC - Actualiza datos
    Sub frmAdministrarVenta_Activate()
        Me.Refresh()
    End Sub
End Class
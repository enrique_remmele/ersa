﻿Public Class frmReenvioComprobantesElectronicos

    Public CSistema As New CSistema
    Public CData As New CData

    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    Dim vControles() As Control
    Sub CargarInformacion()
        ReDim vControles(-1)

        CSistema.CargaControl(vControles, txtNroComprobante)
        CSistema.CargaControl(vControles, cbxEstado)
        CSistema.CargaControl(vControles, txtFechaDesde)
        CSistema.CargaControl(vControles, txtFechaHasta)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtRespuestaLote)

        'Estado
        cbxEstado.Items.Add("Rechazado")
        cbxEstado.Items.Add("Error")
        cbxEstado.Text = ""

        'TipoComprobante
        CSistema.SqlToComboBox(cbxTipoComprobante, CData.GetTable("TipoComprobanteElectronico", ""), "Codigo", "Descripcion")
        cbxTipoComprobante.Text = ""

    End Sub

    Sub Limpiar()
        txtNroComprobante.Text = ""
        cbxEstado.Text = ""
        txtFechaDesde.txt.Text = ""
        txtFechaHasta.txt.Text = ""
        cbxTipoComprobante.Text = ""
        txtRespuestaLote.Text = ""
    End Sub

    Sub Reenviar()
        Dim SQL As String
        Dim Where As String = "Where 0=0 "

        If txtNroComprobante.Text <> "" Then
            Where = Where & " And Nro_Comprobante Like'%" & txtNroComprobante.Text & "%'"
        End If

        If cbxEstado.Text <> "" Then
            Where = Where & " And Estado = '" & cbxEstado.Text & "'"
        Else
            MessageBox.Show("Seleccione un Estado", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If txtFechaDesde.txt.Text <> "  /  /" Then
            If txtFechaHasta.txt.Text = "  /  /" Then
                Where = Where & " And Fecha >= '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "'"
            Else
                Where = Where & " And Fecha Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
            End If
        End If

        If txtFechaHasta.txt.Text <> "  /  /" Then
            If txtFechaDesde.txt.Text = "  /  /" Then
                Where = Where & " And Fecha <= '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
            End If
        End If

        If cbxTipoComprobante.Text <> "" Then
            Where = Where & " And ID_Comprobante = " & cbxTipoComprobante.SelectedValue
        End If

        If txtRespuestaLote.Text <> "" Then
            Where = Where & " And observacion like'%" & txtRespuestaLote.Text & "%'"
        End If

        SQL = "Update dpy_enviados set IND_REENVIAR = 'S' " & Where
        CSistema.ExecuteNonQuery(SQL, CadenaConexion)
        MessageBox.Show("Comprobante(s) Re-enviado(s)!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        Limpiar()
    End Sub

    Private Sub frmReenvioComprobantesElectronicos_Load(sender As Object, e As EventArgs) Handles Me.Load
        CargarInformacion()
    End Sub

    Private Sub btnReenviar_Click(sender As Object, e As EventArgs) Handles btnReenviar.Click
        Reenviar()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAplicarDescuentoTactico
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvDescuentos = New System.Windows.Forms.DataGridView()
        Me.lklAplicar = New System.Windows.Forms.LinkLabel()
        Me.lblModificar = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.txtDescuento = New ERP.ocxTXTNumeric()
        CType(Me.dgvDescuentos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Descuentos:"
        '
        'dgvDescuentos
        '
        Me.dgvDescuentos.AllowUserToAddRows = False
        Me.dgvDescuentos.AllowUserToDeleteRows = False
        Me.dgvDescuentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDescuentos.Location = New System.Drawing.Point(12, 25)
        Me.dgvDescuentos.Name = "dgvDescuentos"
        Me.dgvDescuentos.ReadOnly = True
        Me.dgvDescuentos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDescuentos.Size = New System.Drawing.Size(351, 150)
        Me.dgvDescuentos.StandardTab = True
        Me.dgvDescuentos.TabIndex = 1
        '
        'lklAplicar
        '
        Me.lklAplicar.AutoSize = True
        Me.lklAplicar.Location = New System.Drawing.Point(324, 188)
        Me.lklAplicar.Name = "lklAplicar"
        Me.lklAplicar.Size = New System.Drawing.Size(39, 13)
        Me.lklAplicar.TabIndex = 4
        Me.lklAplicar.TabStop = True
        Me.lklAplicar.Text = "Aplicar"
        '
        'lblModificar
        '
        Me.lblModificar.AutoSize = True
        Me.lblModificar.Location = New System.Drawing.Point(86, 188)
        Me.lblModificar.Name = "lblModificar"
        Me.lblModificar.Size = New System.Drawing.Size(180, 13)
        Me.lblModificar.TabIndex = 2
        Me.lblModificar.Text = "- Modificar porcentaje de descuento:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(288, 226)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 6
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(207, 226)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 5
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'txtDescuento
        '
        Me.txtDescuento.Color = System.Drawing.Color.Empty
        Me.txtDescuento.Decimales = True
        Me.txtDescuento.Indicaciones = Nothing
        Me.txtDescuento.Location = New System.Drawing.Point(272, 184)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(50, 21)
        Me.txtDescuento.SoloLectura = False
        Me.txtDescuento.TabIndex = 3
        Me.txtDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDescuento.Texto = "0"
        '
        'frmAplicarDescuentoTactico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(375, 261)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.txtDescuento)
        Me.Controls.Add(Me.lblModificar)
        Me.Controls.Add(Me.lklAplicar)
        Me.Controls.Add(Me.dgvDescuentos)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmAplicarDescuentoTactico"
        Me.Text = "frmAplicarDescuentoTactico"
        CType(Me.dgvDescuentos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvDescuentos As System.Windows.Forms.DataGridView
    Friend WithEvents lklAplicar As System.Windows.Forms.LinkLabel
    Friend WithEvents txtDescuento As ERP.ocxTXTNumeric
    Friend WithEvents lblModificar As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
End Class

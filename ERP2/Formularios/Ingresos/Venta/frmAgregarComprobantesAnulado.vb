﻿Public Class frmAgregarComprobantesAnulado
    'CLASES
    Public CSistema As New CSistema
    Public CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property
    'VARIABLES
    Dim dtPuntoExpedicion As DataTable

    Sub Inicializar()

        
        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion("frmVenta", "VENTAS CLIENTES", "FATCLI")

        CargarInformacion()


    End Sub

    Sub CargarInformacion()
        'Puntos de Expediciones
        dtPuntoExpedicion = CData.GetTable("VPuntoExpedicion").Copy
        dtPuntoExpedicion = CData.FiltrarDataTable(dtPuntoExpedicion, " IDOperacion = " & IDOperacion & " And Estado='True' ")
        txtIDTimbrado.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIMBRADO", 0))
        ObtenerInformacionPuntoVenta()

        'Clientes
        txtCliente.Conectar()

        'Ciudades
        cbxCiudad.Conectar("", "Descripcion")

        'Sucursal
        cbxSucursal.Conectar()

        'Tipo de Comprobante
        cbxTipoComprobante.Conectar(" IDOperacion = " & IDOperacion)

        ''Depositos
        cbxDeposito.Conectar()

        ' Motivo
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion from MotivoAnulacionVenta where estado = 1")

        ''Obtener deposito
        ObtenerInformacionPuntoVenta()
        ObtenerDeposito()

    End Sub

    Sub GuardarInformacion()

        'TIMBRADO
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIMBRADO", txtIDTimbrado.GetValue)

    End Sub

    Sub ObtenerDeposito()

        If cbxSucursal.GetValue = 0 Then
            Exit Sub
        End If

        Dim dttemp As DataTable = CData.GetTable("VDeposito", "IDSucursal=" & cbxSucursal.GetValue).Copy

        CSistema.SqlToComboBox(cbxDeposito.cbx, dttemp)

    End Sub

    Sub SeleccionarTimbrado()

        Dim frm As New frmSeleccionarTimbrado
        frm.ID = txtIDTimbrado.GetValue
        frm.IDOperacion = IDOperacion
        frm.IDSucursal = vgIDSucursal

        FGMostrarFormulario(Me, frm, "Timbrados", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Seleccionado = False Then
            Exit Sub
        End If

        txtIDTimbrado.SetValue(frm.ID)
        ObtenerInformacionPuntoVenta()
        ObtenerDeposito()

    End Sub

    Sub ObtenerInformacionPuntoVenta()


       
        txtTalonario.txt.Text = "0"
        txtVencimientoTimbrado.txt.Text = ""
        txtRestoTimbrado.txt.Text = "0"
        cbxTipoComprobante.cbx.Text = ""
        txtReferenciaSucursal.txt.Text = "000"
        txtReferenciaTerminal.txt.Text = "000"
        txtNroComprobante.txt.Text = "000"
        cbxCiudad.cbx.Text = ""
        cbxSucursal.cbx.Text = ""


        If IsNumeric(txtIDTimbrado.GetValue) = False Then
            Exit Sub
        End If

        If dtPuntoExpedicion Is Nothing Then
            Exit Sub
        End If

        For Each oRow As DataRow In dtPuntoExpedicion.Select(" ID=" & txtIDTimbrado.GetValue)
            txtTimbrado.txt.Text = oRow("Timbrado").ToString
            txtTalonario.txt.Text = oRow("TalonarioActual").ToString
            txtVencimientoTimbrado.txt.Text = oRow("Vencimiento").ToString
            txtRestoTimbrado.txt.Text = oRow("Saldo").ToString
            cbxTipoComprobante.SelectedValue(oRow("IDTipoComprobante").ToString)
            txtReferenciaSucursal.txt.Text = oRow("ReferenciaSucursal").ToString
            txtReferenciaTerminal.txt.Text = oRow("ReferenciaPunto").ToString
            txtNroComprobante.txt.Text = oRow("ProximoNumero").ToString
            cbxCiudad.SelectedValue(oRow("IDCiudad").ToString)
            cbxSucursal.SelectedValue(oRow("IDSucursal").ToString)

         

        Next

    End Sub

    Sub Insertar()

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento() = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        Dim PuntoExpedicionRow As DataRow = CData.GetTable("VPuntoExpedicion", " IDOperacion = " & IDOperacion).Select("ID=" & txtIDTimbrado.GetValue)(0)

        CSistema.SetSQLParameter(param, "@IDPuntoExpedicion", txtIDTimbrado.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", PuntoExpedicionRow("IDTipoComprobante").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtNroComprobante.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Comprobante", PuntoExpedicionRow("ReferenciaSucursal").ToString & "-" & PuntoExpedicionRow("ReferenciaPunto").ToString & "-" & txtNroComprobante.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", PuntoExpedicionRow("IDSucursal").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoOperacion", cbxDeposito.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DescripcionAnulacion", cbxMotivo.cbx.Text, ParameterDirection.Input)
       'Otros
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "INS".ToString, ParameterDirection.Input)

        'Capturamos el index de la Operacion para un posible proceso posterior
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAgregarComprobantesAnular", False, False, MensajeRetorno, IDTransaccion) = False Then
            CSistema.MostrarError("Atencion: " & MensajeRetorno, ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.MiddleLeft)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From Venta Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpVenta", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub
        Else
            MessageBox.Show("Registro guargado", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()
        End If


    End Sub

    Function ValidarDocumento() As Boolean

        ValidarDocumento = False

        'Nro de Comprobante
        If IsNumeric(txtNroComprobante.GetValue) = False Then
            CSistema.MostrarError("Nro de coprobante no es correcto!", ctrError, btnAceptar, tsslEstado)
            Exit Function
        End If

        ''Observacion
        If txtCliente.Seleccionado = False Then
            CSistema.MostrarError("Selecione un Cliente!", ctrError, btnAceptar, tsslEstado)
            Exit Function
        End If

        If cbxMotivo.cbx.SelectedValue Is Nothing Then
            CSistema.MostrarError("Motivo invalido!", ctrError, cbxMotivo, tsslEstado)
            Exit Function
        End If

        Return True

    End Function

    Sub InsertarMotivo()

        Dim ID As Integer = 0
        Dim dt As DataTable = CData.GetTable("MotivoAnulacionVenta")
        Dim sql As String = ""

        ID = CType(CSistema.ExecuteScalar("Select MAX (ID) From MotivoAnulacionVenta"), Integer) + 1


        For Each oRow As DataRow In dt.Select("Descripcion =" & cbxMotivo.cbx.SelectedValue)
            sql = "Insert MotivoAnulacionVenta (ID,Descripcion) Values (" & ID & "," & cbxMotivo.cbx.SelectedValue & ")"
        Next
        ' CSistema.ExecuteNonQuery(sql)

    End Sub

    Private Sub frmAgregarComprobantesAnulado_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmAgregarComprobantes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Insertar()
    End Sub

    Private Sub btnCancalar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancalar.Click
        Me.Close()
    End Sub

    Private Sub lklTimbrado_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklTimbrado.LinkClicked
        SeleccionarTimbrado()
    End Sub

    Private Sub cbxSucursal_PropertyChanged(sender As Object, e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        ObtenerDeposito()
    End Sub

End Class
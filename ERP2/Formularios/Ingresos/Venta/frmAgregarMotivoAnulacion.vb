﻿Public Class frmAgregarMotivoAnulacion

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Public Property Procesado As Boolean

    'CLASES
    Dim CSistema As New CSistema

    Sub Inicilizar()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select ID, Descripcion,MantenerPedidoVigente from MotivoAnulacionVenta where estado = 1")
        cbxMotivo.DataSource = dt
        cbxMotivo.ValueMember = "ID"
        cbxMotivo.DisplayMember = "Descripcion"

        Procesado = False

    End Sub

    Sub Anular()

        If cbxMotivo.SelectedValue Is Nothing Then
            tsslEstado.Text = "Atencion: Motivo invalido"
            ctrError.SetError(cbxMotivo, "Atencion: Motivo invalido")
            ctrError.SetIconAlignment(cbxMotivo, ErrorIconAlignment.TopRight)
            Exit Sub
        End If
        Dim Mantener As Boolean = cbxMotivo.Items(cbxMotivo.SelectedIndex)("MantenerPedidoVigente")

        If Not Mantener Then
            If MessageBox.Show("Atencion! Esto anulara tambien el PEDIDO DE VENTA. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.No Then
                Exit Sub
            End If
        End If

        'Consultar
        If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        'Anulamos
        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "ANULAR", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMotivo", cbxMotivo.SelectedValue, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Anular Registro
        If CSistema.ExecuteStoreProcedure(param, "SpVenta", False, False, MensajeRetorno, IDTransaccion, False, "", 1000) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAceptar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAceptar, ErrorIconAlignment.TopRight)

            MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)

            If MensajeRetorno = "No se puede anular con fecha distinta a hoy" Then

                If MessageBox.Show("Desea solicitar anulacion por autorizacion?", "Cargar solicitud de anulacion", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.Yes Then
                    Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionAnulacion @IDTransaccion = " & IDTransaccion & ",@Operacion='SOLICITAR',@IDUsuario = " & vgIDUsuario & ",@IDMotivo = " & cbxMotivo.SelectedValue & ",@IDSucursal =" & vgIDSucursal & ",@IDDeposito =" & vgIDDeposito & ",@IDTerminal =" & vgIDTerminal).Copy
                    If Not dttemp Is Nothing Then
                        If dttemp.Rows.Count > 0 Then
                            MessageBox.Show(dttemp.Rows(0)("Mensaje").ToString, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    End If
                End If
            End If


            Procesado = False

        End If

        Procesado = True
        Me.Close()

    End Sub

    Private Sub frmAgregarMotivoAlulacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicilizar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Anular()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmAgregarComprobantesAnulado_Activate()
        Me.Refresh()
    End Sub
End Class
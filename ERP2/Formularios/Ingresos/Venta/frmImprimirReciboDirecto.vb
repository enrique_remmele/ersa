﻿Public Class frmImprimirReciboDirecto
    Public Property Impresora As String = ""

    Private Sub frmImprimirReciboDirecto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbxImpresora.cbx.Items.Clear()

        For Each strPrinter As String In System.Drawing.Printing.PrinterSettings.InstalledPrinters
            cbxImpresora.cbx.Items.Add(strPrinter)
        Next
        cbxImpresora.cbx.SelectedIndex = 0

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Impresora = cbxImpresora.cbx.Text
        Me.Close()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmImprimirReciboDirecto_Activate()
        Me.Refresh()
    End Sub
End Class
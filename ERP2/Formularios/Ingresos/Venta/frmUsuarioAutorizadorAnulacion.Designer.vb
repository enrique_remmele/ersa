﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmUsuarioAutorizadorAnulacion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lblMotivo = New System.Windows.Forms.Label()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.dgvUsuario = New System.Windows.Forms.DataGridView()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblUsuario1 = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.gbxDato = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbFueraDelMes = New System.Windows.Forms.RadioButton()
        Me.rbDentroDelMes = New System.Windows.Forms.RadioButton()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDato.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(318, 126)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(64, 23)
        Me.btnCancelar.TabIndex = 58
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(382, 126)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(64, 23)
        Me.btnEliminar.TabIndex = 59
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(254, 126)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(64, 23)
        Me.btnGuardar.TabIndex = 55
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(190, 126)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(64, 23)
        Me.btnEditar.TabIndex = 57
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(125, 126)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(64, 23)
        Me.btnNuevo.TabIndex = 56
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lblMotivo
        '
        Me.lblMotivo.AutoSize = True
        Me.lblMotivo.Location = New System.Drawing.Point(9, 73)
        Me.lblMotivo.Name = "lblMotivo"
        Me.lblMotivo.Size = New System.Drawing.Size(46, 13)
        Me.lblMotivo.TabIndex = 51
        Me.lblMotivo.Text = "Periodo:"
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = Nothing
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = Nothing
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = Nothing
        Me.cbxUsuario.DataSource = Nothing
        Me.cbxUsuario.DataValueMember = Nothing
        Me.cbxUsuario.dtSeleccionado = Nothing
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(87, 38)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionMultiple = False
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(262, 23)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 50
        Me.cbxUsuario.Texto = ""
        '
        'dgvUsuario
        '
        Me.dgvUsuario.AllowUserToAddRows = False
        Me.dgvUsuario.AllowUserToDeleteRows = False
        Me.dgvUsuario.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvUsuario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvUsuario.Location = New System.Drawing.Point(9, 155)
        Me.dgvUsuario.Name = "dgvUsuario"
        Me.dgvUsuario.ReadOnly = True
        Me.dgvUsuario.Size = New System.Drawing.Size(437, 304)
        Me.dgvUsuario.TabIndex = 49
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(9, 18)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 41
        Me.lblID.Text = "ID:"
        '
        'lblUsuario1
        '
        Me.lblUsuario1.AutoSize = True
        Me.lblUsuario1.Location = New System.Drawing.Point(9, 44)
        Me.lblUsuario1.Name = "lblUsuario1"
        Me.lblUsuario1.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuario1.TabIndex = 43
        Me.lblUsuario1.Text = "Usuario:"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(9, 100)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 45
        Me.lblEstado.Text = "Estado:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(87, 13)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 42
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'gbxDato
        '
        Me.gbxDato.Controls.Add(Me.Button1)
        Me.gbxDato.Controls.Add(Me.GroupBox2)
        Me.gbxDato.Controls.Add(Me.GroupBox1)
        Me.gbxDato.Controls.Add(Me.btnCancelar)
        Me.gbxDato.Controls.Add(Me.btnEliminar)
        Me.gbxDato.Controls.Add(Me.btnGuardar)
        Me.gbxDato.Controls.Add(Me.btnEditar)
        Me.gbxDato.Controls.Add(Me.btnNuevo)
        Me.gbxDato.Controls.Add(Me.lblMotivo)
        Me.gbxDato.Controls.Add(Me.cbxUsuario)
        Me.gbxDato.Controls.Add(Me.dgvUsuario)
        Me.gbxDato.Controls.Add(Me.lblID)
        Me.gbxDato.Controls.Add(Me.lblUsuario1)
        Me.gbxDato.Controls.Add(Me.lblEstado)
        Me.gbxDato.Controls.Add(Me.txtID)
        Me.gbxDato.Location = New System.Drawing.Point(3, 2)
        Me.gbxDato.Name = "gbxDato"
        Me.gbxDato.Size = New System.Drawing.Size(454, 504)
        Me.gbxDato.TabIndex = 1
        Me.gbxDato.TabStop = False
        Me.gbxDato.Text = "Datos"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(382, 465)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(64, 23)
        Me.Button1.TabIndex = 62
        Me.Button1.Text = "&Salir"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.Control
        Me.GroupBox2.Controls.Add(Me.rdbDesactivado)
        Me.GroupBox2.Controls.Add(Me.rdbActivo)
        Me.GroupBox2.Location = New System.Drawing.Point(61, 100)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(0)
        Me.GroupBox2.Size = New System.Drawing.Size(200, 20)
        Me.GroupBox2.TabIndex = 61
        Me.GroupBox2.TabStop = False
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(104, 3)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(142, 17)
        Me.rdbDesactivado.TabIndex = 63
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado                   "
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(0, 3)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(133, 17)
        Me.rdbActivo.TabIndex = 62
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo                          "
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbFueraDelMes)
        Me.GroupBox1.Controls.Add(Me.rbDentroDelMes)
        Me.GroupBox1.Location = New System.Drawing.Point(61, 67)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(0)
        Me.GroupBox1.Size = New System.Drawing.Size(200, 19)
        Me.GroupBox1.TabIndex = 60
        Me.GroupBox1.TabStop = False
        '
        'rbFueraDelMes
        '
        Me.rbFueraDelMes.AutoSize = True
        Me.rbFueraDelMes.Location = New System.Drawing.Point(104, 3)
        Me.rbFueraDelMes.Name = "rbFueraDelMes"
        Me.rbFueraDelMes.Size = New System.Drawing.Size(128, 17)
        Me.rbFueraDelMes.TabIndex = 65
        Me.rbFueraDelMes.TabStop = True
        Me.rbFueraDelMes.Text = "Fuera del Mes            "
        Me.rbFueraDelMes.UseVisualStyleBackColor = True
        '
        'rbDentroDelMes
        '
        Me.rbDentroDelMes.AutoSize = True
        Me.rbDentroDelMes.Location = New System.Drawing.Point(1, 3)
        Me.rbDentroDelMes.Name = "rbDentroDelMes"
        Me.rbDentroDelMes.Size = New System.Drawing.Size(136, 17)
        Me.rbDentroDelMes.TabIndex = 64
        Me.rbDentroDelMes.TabStop = True
        Me.rbDentroDelMes.Text = "Dentro del Mes             "
        Me.rbDentroDelMes.UseVisualStyleBackColor = True
        '
        'frmUsuarioAutorizadorAnulacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(461, 508)
        Me.Controls.Add(Me.gbxDato)
        Me.Name = "frmUsuarioAutorizadorAnulacion"
        Me.Text = "frmUsuarioAutorizadorAnulacion"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDato.ResumeLayout(False)
        Me.gbxDato.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents gbxDato As GroupBox
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnEliminar As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnEditar As Button
    Friend WithEvents btnNuevo As Button
    Friend WithEvents lblMotivo As Label
    Friend WithEvents cbxUsuario As ocxCBX
    Friend WithEvents dgvUsuario As DataGridView
    Friend WithEvents lblID As Label
    Friend WithEvents lblUsuario1 As Label
    Friend WithEvents lblEstado As Label
    Friend WithEvents txtID As ocxTXTNumeric
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents rdbDesactivado As RadioButton
    Friend WithEvents rdbActivo As RadioButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents rbFueraDelMes As RadioButton
    Friend WithEvents rbDentroDelMes As RadioButton
    Friend WithEvents Button1 As Button
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVenta2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblIDTransaccionPedido = New System.Windows.Forms.Label()
        Me.lblCiudad = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblTalonario = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lklDescuento = New System.Windows.Forms.LinkLabel()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.InformacionDelProductoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblObservacionProducto = New System.Windows.Forms.Label()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.cbxUnidad = New System.Windows.Forms.ComboBox()
        Me.lblUnidad = New System.Windows.Forms.Label()
        Me.lblProducto = New System.Windows.Forms.Label()
        Me.ExportarAExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VerDescuentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.chkImprimirSoloObservacion = New ERP.ocxCHK()
        Me.btnAnularRecibo = New System.Windows.Forms.Button()
        Me.btnImprimirRecibo = New System.Windows.Forms.Button()
        Me.btnHabilitar = New System.Windows.Forms.Button()
        Me.chkConversion = New ERP.ocxCHK()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.gbxComprobante = New System.Windows.Forms.GroupBox()
        Me.txtIDTimbrado = New ERP.ocxTXTString()
        Me.txtTimbrado = New ERP.ocxTXTString()
        Me.lklTimbrado = New System.Windows.Forms.LinkLabel()
        Me.txtRestoTimbrado = New ERP.ocxTXTNumeric()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.lblRestoTimbrado = New System.Windows.Forms.Label()
        Me.txtVencimientoTimbrado = New ERP.ocxTXTDate()
        Me.lblVenimientoTimbrado = New System.Windows.Forms.Label()
        Me.txtNroComprobante = New ERP.ocxTXTString()
        Me.txtTalonario = New ERP.ocxTXTString()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtReferenciaSucursal = New ERP.ocxTXTString()
        Me.txtReferenciaTerminal = New ERP.ocxTXTString()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.OcxImpuesto1 = New ERP.ocxImpuesto()
        Me.ModificarDescToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.txtTipoEntrega = New ERP.ocxTXTString()
        Me.OcxCotizacion1 = New ERP.ocxCotizacion()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.cbxDeposito2 = New ERP.ocxCBX()
        Me.cbxListaPrecio2 = New ERP.ocxCBX()
        Me.txtPlazo = New ERP.ocxTXTNumeric()
        Me.txtTelefono = New ERP.ocxTXTString()
        Me.lblTelefono = New System.Windows.Forms.Label()
        Me.lblTipoEntrega = New System.Windows.Forms.Label()
        Me.txtDetalle = New ERP.ocxTXTString()
        Me.lblDetalle = New System.Windows.Forms.Label()
        Me.txtCreditoSaldo = New ERP.ocxTXTNumeric()
        Me.lblCreditoSaldo = New System.Windows.Forms.Label()
        Me.txtDescuento = New ERP.ocxTXTNumeric()
        Me.lblDescuento = New System.Windows.Forms.Label()
        Me.lblDeposito = New System.Windows.Forms.Label()
        Me.lblListaPrecio = New System.Windows.Forms.Label()
        Me.cbxPromotor = New ERP.ocxCBX()
        Me.lblPromotor = New System.Windows.Forms.Label()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.lblVendedor = New System.Windows.Forms.Label()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblPlazo = New System.Windows.Forms.Label()
        Me.txtVencimiento = New ERP.ocxTXTDate()
        Me.cbxCondicion = New ERP.ocxCBX()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.txtFechaVenta = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.txtDireccion = New ERP.ocxTXTString()
        Me.lblDireccion = New System.Windows.Forms.Label()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.lblPorcentaje = New System.Windows.Forms.Label()
        Me.lblVencimiento = New System.Windows.Forms.Label()
        Me.lblCondicion = New System.Windows.Forms.Label()
        Me.cbxFormaPagoFactura = New ERP.ocxCBX()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.colGravado = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colExento = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colDescuentoPocentaje = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colDescuentoUnitario = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colPrecioUnitario = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColUnidadMedida = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colCantidad = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.txtDescuentoProducto = New ERP.ocxTXTNumeric()
        Me.txtPrecioUnitario = New ERP.ocxTXTNumeric()
        Me.lblPrecioUnitario = New System.Windows.Forms.Label()
        Me.colIVA = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colDescripcion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colReferencia = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txtObservacionProducto = New ERP.ocxTXTString()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.colID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.gbxDetalle = New System.Windows.Forms.GroupBox()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.flpAnuladoPor.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.gbxComprobante.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.gbxCabecera.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.gbxDetalle.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblIDTransaccionPedido
        '
        Me.lblIDTransaccionPedido.AutoSize = True
        Me.lblIDTransaccionPedido.Location = New System.Drawing.Point(585, 41)
        Me.lblIDTransaccionPedido.Name = "lblIDTransaccionPedido"
        Me.lblIDTransaccionPedido.Size = New System.Drawing.Size(13, 13)
        Me.lblIDTransaccionPedido.TabIndex = 17
        Me.lblIDTransaccionPedido.Text = "0"
        Me.lblIDTransaccionPedido.Visible = False
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Location = New System.Drawing.Point(251, 15)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(28, 13)
        Me.lblCiudad.TabIndex = 2
        Me.lblCiudad.Text = "Ciu.:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(357, 15)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(32, 13)
        Me.lblSucursal.TabIndex = 4
        Me.lblSucursal.Text = "Suc.:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(560, 15)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(55, 13)
        Me.lblComprobante.TabIndex = 6
        Me.lblComprobante.Text = "Comprob.:"
        '
        'lblTalonario
        '
        Me.lblTalonario.AutoSize = True
        Me.lblTalonario.Location = New System.Drawing.Point(9, 39)
        Me.lblTalonario.Name = "lblTalonario"
        Me.lblTalonario.Size = New System.Drawing.Size(54, 13)
        Me.lblTalonario.TabIndex = 11
        Me.lblTalonario.Text = "Talonario:"
        '
        'lklDescuento
        '
        Me.lklDescuento.AutoSize = True
        Me.lklDescuento.Location = New System.Drawing.Point(686, 16)
        Me.lklDescuento.Name = "lklDescuento"
        Me.lklDescuento.Size = New System.Drawing.Size(38, 13)
        Me.lklDescuento.TabIndex = 15
        Me.lklDescuento.TabStop = True
        Me.lklDescuento.Text = "Desc.:"
        Me.ToolTip1.SetToolTip(Me.lklDescuento, "Aplicar descuentos")
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'InformacionDelProductoToolStripMenuItem
        '
        Me.InformacionDelProductoToolStripMenuItem.Name = "InformacionDelProductoToolStripMenuItem"
        Me.InformacionDelProductoToolStripMenuItem.Size = New System.Drawing.Size(210, 22)
        Me.InformacionDelProductoToolStripMenuItem.Text = "Informacion del Producto"
        '
        'lblObservacionProducto
        '
        Me.lblObservacionProducto.AutoSize = True
        Me.lblObservacionProducto.Location = New System.Drawing.Point(316, 16)
        Me.lblObservacionProducto.Name = "lblObservacionProducto"
        Me.lblObservacionProducto.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacionProducto.TabIndex = 2
        Me.lblObservacionProducto.Text = "Observacion:"
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = False
        Me.txtImporte.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporte.Location = New System.Drawing.Point(723, 32)
        Me.txtImporte.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(81, 21)
        Me.txtImporte.SoloLectura = True
        Me.txtImporte.TabIndex = 13
        Me.txtImporte.TabStop = False
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(756, 16)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(45, 13)
        Me.lblImporte.TabIndex = 12
        Me.lblImporte.Text = "Importe:"
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCantidad.Location = New System.Drawing.Point(578, 32)
        Me.txtCantidad.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(49, 21)
        Me.txtCantidad.SoloLectura = False
        Me.txtCantidad.TabIndex = 7
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.ForeColor = System.Drawing.SystemColors.ControlText
        Me.flpAnuladoPor.Location = New System.Drawing.Point(0, 453)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(300, 20)
        Me.flpAnuladoPor.TabIndex = 41
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 13
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 15
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 17
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(575, 16)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidad.TabIndex = 6
        Me.lblCantidad.Text = "Cantidad:"
        '
        'cbxUnidad
        '
        Me.cbxUnidad.Enabled = False
        Me.cbxUnidad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbxUnidad.FormattingEnabled = True
        Me.cbxUnidad.Location = New System.Drawing.Point(507, 31)
        Me.cbxUnidad.Name = "cbxUnidad"
        Me.cbxUnidad.Size = New System.Drawing.Size(71, 21)
        Me.cbxUnidad.TabIndex = 5
        '
        'lblUnidad
        '
        Me.lblUnidad.AutoSize = True
        Me.lblUnidad.Location = New System.Drawing.Point(518, 16)
        Me.lblUnidad.Name = "lblUnidad"
        Me.lblUnidad.Size = New System.Drawing.Size(44, 13)
        Me.lblUnidad.TabIndex = 4
        Me.lblUnidad.Text = "Unidad:"
        '
        'lblProducto
        '
        Me.lblProducto.AutoSize = True
        Me.lblProducto.Location = New System.Drawing.Point(6, 16)
        Me.lblProducto.Name = "lblProducto"
        Me.lblProducto.Size = New System.Drawing.Size(53, 13)
        Me.lblProducto.TabIndex = 0
        Me.lblProducto.Text = "Producto:"
        '
        'ExportarAExcelToolStripMenuItem
        '
        Me.ExportarAExcelToolStripMenuItem.Name = "ExportarAExcelToolStripMenuItem"
        Me.ExportarAExcelToolStripMenuItem.Size = New System.Drawing.Size(210, 22)
        Me.ExportarAExcelToolStripMenuItem.Text = "Exportar a Excel"
        '
        'VerDescuentosToolStripMenuItem
        '
        Me.VerDescuentosToolStripMenuItem.Name = "VerDescuentosToolStripMenuItem"
        Me.VerDescuentosToolStripMenuItem.Size = New System.Drawing.Size(210, 22)
        Me.VerDescuentosToolStripMenuItem.Text = "Ver descuentos"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(207, 6)
        '
        'chkImprimirSoloObservacion
        '
        Me.chkImprimirSoloObservacion.BackColor = System.Drawing.Color.Transparent
        Me.chkImprimirSoloObservacion.Color = System.Drawing.Color.Empty
        Me.chkImprimirSoloObservacion.Location = New System.Drawing.Point(174, 397)
        Me.chkImprimirSoloObservacion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.chkImprimirSoloObservacion.Name = "chkImprimirSoloObservacion"
        Me.chkImprimirSoloObservacion.Size = New System.Drawing.Size(126, 21)
        Me.chkImprimirSoloObservacion.SoloLectura = False
        Me.chkImprimirSoloObservacion.TabIndex = 46
        Me.chkImprimirSoloObservacion.Texto = "Imp. solo Obs."
        Me.chkImprimirSoloObservacion.Valor = False
        '
        'btnAnularRecibo
        '
        Me.btnAnularRecibo.Location = New System.Drawing.Point(136, 508)
        Me.btnAnularRecibo.Name = "btnAnularRecibo"
        Me.btnAnularRecibo.Size = New System.Drawing.Size(128, 23)
        Me.btnAnularRecibo.TabIndex = 45
        Me.btnAnularRecibo.Text = "Anular Recibo"
        Me.btnAnularRecibo.UseVisualStyleBackColor = True
        '
        'btnImprimirRecibo
        '
        Me.btnImprimirRecibo.Location = New System.Drawing.Point(136, 481)
        Me.btnImprimirRecibo.Name = "btnImprimirRecibo"
        Me.btnImprimirRecibo.Size = New System.Drawing.Size(128, 23)
        Me.btnImprimirRecibo.TabIndex = 44
        Me.btnImprimirRecibo.Text = "Imprimir Recibo"
        Me.btnImprimirRecibo.UseVisualStyleBackColor = True
        '
        'btnHabilitar
        '
        Me.btnHabilitar.Location = New System.Drawing.Point(225, 538)
        Me.btnHabilitar.Name = "btnHabilitar"
        Me.btnHabilitar.Size = New System.Drawing.Size(75, 23)
        Me.btnHabilitar.TabIndex = 43
        Me.btnHabilitar.Text = "Habilitar"
        Me.btnHabilitar.UseVisualStyleBackColor = True
        '
        'chkConversion
        '
        Me.chkConversion.BackColor = System.Drawing.Color.Transparent
        Me.chkConversion.Color = System.Drawing.Color.Empty
        Me.chkConversion.Location = New System.Drawing.Point(16, 397)
        Me.chkConversion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.chkConversion.Name = "chkConversion"
        Me.chkConversion.Size = New System.Drawing.Size(152, 21)
        Me.chkConversion.SoloLectura = False
        Me.chkConversion.TabIndex = 42
        Me.chkConversion.Texto = "Convertir Unidad de Medida"
        Me.chkConversion.Valor = False
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.ForeColor = System.Drawing.SystemColors.ControlText
        Me.flpRegistradoPor.Location = New System.Drawing.Point(2, 427)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(298, 20)
        Me.flpRegistradoPor.TabIndex = 31
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'gbxComprobante
        '
        Me.gbxComprobante.BackColor = System.Drawing.Color.Transparent
        Me.gbxComprobante.Controls.Add(Me.lblIDTransaccionPedido)
        Me.gbxComprobante.Controls.Add(Me.txtIDTimbrado)
        Me.gbxComprobante.Controls.Add(Me.txtTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lklTimbrado)
        Me.gbxComprobante.Controls.Add(Me.txtRestoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.cbxSucursal)
        Me.gbxComprobante.Controls.Add(Me.cbxCiudad)
        Me.gbxComprobante.Controls.Add(Me.lblRestoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lblCiudad)
        Me.gbxComprobante.Controls.Add(Me.lblSucursal)
        Me.gbxComprobante.Controls.Add(Me.txtVencimientoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lblVenimientoTimbrado)
        Me.gbxComprobante.Controls.Add(Me.lblComprobante)
        Me.gbxComprobante.Controls.Add(Me.txtNroComprobante)
        Me.gbxComprobante.Controls.Add(Me.txtTalonario)
        Me.gbxComprobante.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxComprobante.Controls.Add(Me.lblTalonario)
        Me.gbxComprobante.Controls.Add(Me.txtReferenciaSucursal)
        Me.gbxComprobante.Controls.Add(Me.txtReferenciaTerminal)
        Me.gbxComprobante.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbxComprobante.Location = New System.Drawing.Point(4, 1)
        Me.gbxComprobante.Name = "gbxComprobante"
        Me.gbxComprobante.Size = New System.Drawing.Size(810, 62)
        Me.gbxComprobante.TabIndex = 28
        Me.gbxComprobante.TabStop = False
        '
        'txtIDTimbrado
        '
        Me.txtIDTimbrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIDTimbrado.Color = System.Drawing.Color.Beige
        Me.txtIDTimbrado.Indicaciones = Nothing
        Me.txtIDTimbrado.Location = New System.Drawing.Point(68, 11)
        Me.txtIDTimbrado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtIDTimbrado.Multilinea = False
        Me.txtIDTimbrado.Name = "txtIDTimbrado"
        Me.txtIDTimbrado.Size = New System.Drawing.Size(36, 21)
        Me.txtIDTimbrado.SoloLectura = True
        Me.txtIDTimbrado.TabIndex = 1
        Me.txtIDTimbrado.TabStop = False
        Me.txtIDTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtIDTimbrado.Texto = ""
        Me.txtIDTimbrado.Visible = False
        '
        'txtTimbrado
        '
        Me.txtTimbrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTimbrado.Color = System.Drawing.Color.Beige
        Me.txtTimbrado.Indicaciones = Nothing
        Me.txtTimbrado.Location = New System.Drawing.Point(69, 11)
        Me.txtTimbrado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtTimbrado.Multilinea = False
        Me.txtTimbrado.Name = "txtTimbrado"
        Me.txtTimbrado.Size = New System.Drawing.Size(176, 21)
        Me.txtTimbrado.SoloLectura = True
        Me.txtTimbrado.TabIndex = 2
        Me.txtTimbrado.TabStop = False
        Me.txtTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTimbrado.Texto = ""
        '
        'lklTimbrado
        '
        Me.lklTimbrado.AutoSize = True
        Me.lklTimbrado.Location = New System.Drawing.Point(9, 15)
        Me.lklTimbrado.Name = "lklTimbrado"
        Me.lklTimbrado.Size = New System.Drawing.Size(54, 13)
        Me.lklTimbrado.TabIndex = 0
        Me.lklTimbrado.TabStop = True
        Me.lklTimbrado.Text = "Timbrado:"
        '
        'txtRestoTimbrado
        '
        Me.txtRestoTimbrado.Color = System.Drawing.Color.Beige
        Me.txtRestoTimbrado.Decimales = True
        Me.txtRestoTimbrado.Indicaciones = Nothing
        Me.txtRestoTimbrado.Location = New System.Drawing.Point(389, 34)
        Me.txtRestoTimbrado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtRestoTimbrado.Name = "txtRestoTimbrado"
        Me.txtRestoTimbrado.Size = New System.Drawing.Size(117, 22)
        Me.txtRestoTimbrado.SoloLectura = True
        Me.txtRestoTimbrado.TabIndex = 16
        Me.txtRestoTimbrado.TabStop = False
        Me.txtRestoTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRestoTimbrado.Texto = "0"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(389, 11)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(117, 21)
        Me.cbxSucursal.SoloLectura = True
        Me.cbxSucursal.TabIndex = 5
        Me.cbxSucursal.TabStop = False
        Me.cbxSucursal.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = "Codigo"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = "VCiudad"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(284, 11)
        Me.cbxCiudad.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(73, 21)
        Me.cbxCiudad.SoloLectura = True
        Me.cbxCiudad.TabIndex = 3
        Me.cbxCiudad.TabStop = False
        Me.cbxCiudad.Texto = ""
        '
        'lblRestoTimbrado
        '
        Me.lblRestoTimbrado.AutoSize = True
        Me.lblRestoTimbrado.Location = New System.Drawing.Point(251, 39)
        Me.lblRestoTimbrado.Name = "lblRestoTimbrado"
        Me.lblRestoTimbrado.Size = New System.Drawing.Size(138, 13)
        Me.lblRestoTimbrado.TabIndex = 15
        Me.lblRestoTimbrado.Text = "Nro. restantes en Talonario:"
        '
        'txtVencimientoTimbrado
        '
        Me.txtVencimientoTimbrado.AñoFecha = 0
        Me.txtVencimientoTimbrado.Color = System.Drawing.Color.Beige
        Me.txtVencimientoTimbrado.Fecha = New Date(CType(0, Long))
        Me.txtVencimientoTimbrado.Location = New System.Drawing.Point(172, 35)
        Me.txtVencimientoTimbrado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtVencimientoTimbrado.MesFecha = 0
        Me.txtVencimientoTimbrado.Name = "txtVencimientoTimbrado"
        Me.txtVencimientoTimbrado.PermitirNulo = False
        Me.txtVencimientoTimbrado.Size = New System.Drawing.Size(74, 20)
        Me.txtVencimientoTimbrado.SoloLectura = True
        Me.txtVencimientoTimbrado.TabIndex = 14
        Me.txtVencimientoTimbrado.TabStop = False
        '
        'lblVenimientoTimbrado
        '
        Me.lblVenimientoTimbrado.AutoSize = True
        Me.lblVenimientoTimbrado.Location = New System.Drawing.Point(104, 39)
        Me.lblVenimientoTimbrado.Name = "lblVenimientoTimbrado"
        Me.lblVenimientoTimbrado.Size = New System.Drawing.Size(68, 13)
        Me.lblVenimientoTimbrado.TabIndex = 13
        Me.lblVenimientoTimbrado.Text = "Vencimiento:"
        '
        'txtNroComprobante
        '
        Me.txtNroComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroComprobante.Color = System.Drawing.Color.Empty
        Me.txtNroComprobante.Indicaciones = Nothing
        Me.txtNroComprobante.Location = New System.Drawing.Point(728, 11)
        Me.txtNroComprobante.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtNroComprobante.Multilinea = False
        Me.txtNroComprobante.Name = "txtNroComprobante"
        Me.txtNroComprobante.Size = New System.Drawing.Size(76, 21)
        Me.txtNroComprobante.SoloLectura = False
        Me.txtNroComprobante.TabIndex = 10
        Me.txtNroComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNroComprobante.Texto = ""
        '
        'txtTalonario
        '
        Me.txtTalonario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTalonario.Color = System.Drawing.Color.Beige
        Me.txtTalonario.Indicaciones = Nothing
        Me.txtTalonario.Location = New System.Drawing.Point(68, 35)
        Me.txtTalonario.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtTalonario.Multilinea = False
        Me.txtTalonario.Name = "txtTalonario"
        Me.txtTalonario.Size = New System.Drawing.Size(36, 21)
        Me.txtTalonario.SoloLectura = True
        Me.txtTalonario.TabIndex = 12
        Me.txtTalonario.TabStop = False
        Me.txtTalonario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTalonario.Texto = ""
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Codigo"
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = ""
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(617, 11)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(55, 21)
        Me.cbxTipoComprobante.SoloLectura = True
        Me.cbxTipoComprobante.TabIndex = 7
        Me.cbxTipoComprobante.TabStop = False
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtReferenciaSucursal
        '
        Me.txtReferenciaSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferenciaSucursal.Color = System.Drawing.Color.Empty
        Me.txtReferenciaSucursal.Indicaciones = Nothing
        Me.txtReferenciaSucursal.Location = New System.Drawing.Point(672, 11)
        Me.txtReferenciaSucursal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtReferenciaSucursal.Multilinea = False
        Me.txtReferenciaSucursal.Name = "txtReferenciaSucursal"
        Me.txtReferenciaSucursal.Size = New System.Drawing.Size(28, 21)
        Me.txtReferenciaSucursal.SoloLectura = True
        Me.txtReferenciaSucursal.TabIndex = 8
        Me.txtReferenciaSucursal.TabStop = False
        Me.txtReferenciaSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferenciaSucursal.Texto = ""
        '
        'txtReferenciaTerminal
        '
        Me.txtReferenciaTerminal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferenciaTerminal.Color = System.Drawing.Color.Empty
        Me.txtReferenciaTerminal.Indicaciones = Nothing
        Me.txtReferenciaTerminal.Location = New System.Drawing.Point(700, 11)
        Me.txtReferenciaTerminal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtReferenciaTerminal.Multilinea = False
        Me.txtReferenciaTerminal.Name = "txtReferenciaTerminal"
        Me.txtReferenciaTerminal.Size = New System.Drawing.Size(28, 21)
        Me.txtReferenciaTerminal.SoloLectura = True
        Me.txtReferenciaTerminal.TabIndex = 9
        Me.txtReferenciaTerminal.TabStop = False
        Me.txtReferenciaTerminal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtReferenciaTerminal.Texto = ""
        '
        'btnImprimir
        '
        Me.btnImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnImprimir.Location = New System.Drawing.Point(136, 538)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 34
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBusquedaAvanzada.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(4, 538)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(126, 23)
        Me.btnBusquedaAvanzada.TabIndex = 33
        Me.btnBusquedaAvanzada.Text = "&Busqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnSalir.Location = New System.Drawing.Point(734, 538)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 39
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnCancelar.Location = New System.Drawing.Point(617, 538)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 38
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnGuardar.Location = New System.Drawing.Point(536, 538)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 37
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnNuevo.Location = New System.Drawing.Point(455, 538)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 36
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 573)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(826, 22)
        Me.StatusStrip1.TabIndex = 40
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'btnAsiento
        '
        Me.btnAsiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAsiento.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnAsiento.Location = New System.Drawing.Point(374, 538)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 35
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'OcxImpuesto1
        '
        Me.OcxImpuesto1.BackColor = System.Drawing.Color.Transparent
        Me.OcxImpuesto1.Decimales = False
        Me.OcxImpuesto1.dtImpuesto = Nothing
        Me.OcxImpuesto1.IDMoneda = 0
        Me.OcxImpuesto1.Location = New System.Drawing.Point(328, 397)
        Me.OcxImpuesto1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.OcxImpuesto1.Name = "OcxImpuesto1"
        Me.OcxImpuesto1.Size = New System.Drawing.Size(484, 134)
        Me.OcxImpuesto1.TabIndex = 32
        Me.OcxImpuesto1.TotalRetencionIVA = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'ModificarDescToolStripMenuItem
        '
        Me.ModificarDescToolStripMenuItem.Name = "ModificarDescToolStripMenuItem"
        Me.ModificarDescToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.ModificarDescToolStripMenuItem.Size = New System.Drawing.Size(210, 22)
        Me.ModificarDescToolStripMenuItem.Text = "Modificar Desc."
        '
        'EliminarToolStripMenuItem
        '
        Me.EliminarToolStripMenuItem.Name = "EliminarToolStripMenuItem"
        Me.EliminarToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete
        Me.EliminarToolStripMenuItem.Size = New System.Drawing.Size(210, 22)
        Me.EliminarToolStripMenuItem.Text = "Eliminar"
        '
        'txtTipoEntrega
        '
        Me.txtTipoEntrega.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoEntrega.Color = System.Drawing.Color.Beige
        Me.txtTipoEntrega.Indicaciones = Nothing
        Me.txtTipoEntrega.Location = New System.Drawing.Point(590, 108)
        Me.txtTipoEntrega.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtTipoEntrega.Multilinea = False
        Me.txtTipoEntrega.Name = "txtTipoEntrega"
        Me.txtTipoEntrega.Size = New System.Drawing.Size(211, 21)
        Me.txtTipoEntrega.SoloLectura = True
        Me.txtTipoEntrega.TabIndex = 35
        Me.txtTipoEntrega.TabStop = False
        Me.txtTipoEntrega.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTipoEntrega.Texto = ""
        '
        'OcxCotizacion1
        '
        Me.OcxCotizacion1.dt = Nothing
        Me.OcxCotizacion1.FiltroFecha = Nothing
        Me.OcxCotizacion1.Location = New System.Drawing.Point(590, 56)
        Me.OcxCotizacion1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.OcxCotizacion1.Name = "OcxCotizacion1"
        Me.OcxCotizacion1.Registro = Nothing
        Me.OcxCotizacion1.Saltar = False
        Me.OcxCotizacion1.Seleccionado = True
        Me.OcxCotizacion1.Size = New System.Drawing.Size(148, 28)
        Me.OcxCotizacion1.SoloLectura = False
        Me.OcxCotizacion1.TabIndex = 22
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(2, 88)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "Form.Pago:"
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 117
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.ControlCorto = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(64, 10)
        Me.txtCliente.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtCliente.MostrarSucursal = True
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(445, 24)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 1
        '
        'cbxDeposito2
        '
        Me.cbxDeposito2.CampoWhere = Nothing
        Me.cbxDeposito2.CargarUnaSolaVez = False
        Me.cbxDeposito2.DataDisplayMember = "Deposito"
        Me.cbxDeposito2.DataFilter = Nothing
        Me.cbxDeposito2.DataOrderBy = Nothing
        Me.cbxDeposito2.DataSource = Nothing
        Me.cbxDeposito2.DataValueMember = "ID"
        Me.cbxDeposito2.dtSeleccionado = Nothing
        Me.cbxDeposito2.FormABM = Nothing
        Me.cbxDeposito2.Indicaciones = Nothing
        Me.cbxDeposito2.Location = New System.Drawing.Point(64, 109)
        Me.cbxDeposito2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbxDeposito2.Name = "cbxDeposito2"
        Me.cbxDeposito2.SeleccionMultiple = False
        Me.cbxDeposito2.SeleccionObligatoria = False
        Me.cbxDeposito2.Size = New System.Drawing.Size(198, 21)
        Me.cbxDeposito2.SoloLectura = True
        Me.cbxDeposito2.TabIndex = 31
        Me.cbxDeposito2.TabStop = False
        Me.cbxDeposito2.Texto = ""
        '
        'cbxListaPrecio2
        '
        Me.cbxListaPrecio2.CampoWhere = Nothing
        Me.cbxListaPrecio2.CargarUnaSolaVez = False
        Me.cbxListaPrecio2.DataDisplayMember = Nothing
        Me.cbxListaPrecio2.DataFilter = Nothing
        Me.cbxListaPrecio2.DataOrderBy = Nothing
        Me.cbxListaPrecio2.DataSource = Nothing
        Me.cbxListaPrecio2.DataValueMember = Nothing
        Me.cbxListaPrecio2.dtSeleccionado = Nothing
        Me.cbxListaPrecio2.FormABM = Nothing
        Me.cbxListaPrecio2.Indicaciones = Nothing
        Me.cbxListaPrecio2.Location = New System.Drawing.Point(590, 10)
        Me.cbxListaPrecio2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbxListaPrecio2.Name = "cbxListaPrecio2"
        Me.cbxListaPrecio2.SeleccionMultiple = False
        Me.cbxListaPrecio2.SeleccionObligatoria = False
        Me.cbxListaPrecio2.Size = New System.Drawing.Size(211, 21)
        Me.cbxListaPrecio2.SoloLectura = True
        Me.cbxListaPrecio2.TabIndex = 3
        Me.cbxListaPrecio2.TabStop = False
        Me.cbxListaPrecio2.Texto = ""
        '
        'txtPlazo
        '
        Me.txtPlazo.Color = System.Drawing.Color.Empty
        Me.txtPlazo.Decimales = False
        Me.txtPlazo.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtPlazo.Location = New System.Drawing.Point(432, 59)
        Me.txtPlazo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtPlazo.Name = "txtPlazo"
        Me.txtPlazo.Size = New System.Drawing.Size(74, 21)
        Me.txtPlazo.SoloLectura = True
        Me.txtPlazo.TabIndex = 20
        Me.txtPlazo.TabStop = False
        Me.txtPlazo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPlazo.Texto = "0"
        '
        'txtTelefono
        '
        Me.txtTelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefono.Color = System.Drawing.Color.Beige
        Me.txtTelefono.Indicaciones = Nothing
        Me.txtTelefono.Location = New System.Drawing.Point(412, 36)
        Me.txtTelefono.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtTelefono.Multilinea = False
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(94, 21)
        Me.txtTelefono.SoloLectura = True
        Me.txtTelefono.TabIndex = 7
        Me.txtTelefono.TabStop = False
        Me.txtTelefono.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTelefono.Texto = ""
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Location = New System.Drawing.Point(389, 40)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(25, 13)
        Me.lblTelefono.TabIndex = 6
        Me.lblTelefono.Text = "Tel:"
        '
        'lblTipoEntrega
        '
        Me.lblTipoEntrega.AutoSize = True
        Me.lblTipoEntrega.Location = New System.Drawing.Point(516, 113)
        Me.lblTipoEntrega.Name = "lblTipoEntrega"
        Me.lblTipoEntrega.Size = New System.Drawing.Size(68, 13)
        Me.lblTipoEntrega.TabIndex = 34
        Me.lblTipoEntrega.Text = "TipoEntrega:"
        '
        'txtDetalle
        '
        Me.txtDetalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDetalle.Color = System.Drawing.Color.Empty
        Me.txtDetalle.Indicaciones = Nothing
        Me.txtDetalle.Location = New System.Drawing.Point(317, 109)
        Me.txtDetalle.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtDetalle.Multilinea = False
        Me.txtDetalle.Name = "txtDetalle"
        Me.txtDetalle.Size = New System.Drawing.Size(189, 21)
        Me.txtDetalle.SoloLectura = False
        Me.txtDetalle.TabIndex = 33
        Me.txtDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDetalle.Texto = ""
        '
        'lblDetalle
        '
        Me.lblDetalle.AutoSize = True
        Me.lblDetalle.Location = New System.Drawing.Point(273, 114)
        Me.lblDetalle.Name = "lblDetalle"
        Me.lblDetalle.Size = New System.Drawing.Size(43, 13)
        Me.lblDetalle.TabIndex = 32
        Me.lblDetalle.Text = "Detalle:"
        '
        'txtCreditoSaldo
        '
        Me.txtCreditoSaldo.Color = System.Drawing.Color.Beige
        Me.txtCreditoSaldo.Decimales = False
        Me.txtCreditoSaldo.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCreditoSaldo.Location = New System.Drawing.Point(704, 36)
        Me.txtCreditoSaldo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtCreditoSaldo.Name = "txtCreditoSaldo"
        Me.txtCreditoSaldo.Size = New System.Drawing.Size(97, 21)
        Me.txtCreditoSaldo.SoloLectura = True
        Me.txtCreditoSaldo.TabIndex = 12
        Me.txtCreditoSaldo.TabStop = False
        Me.txtCreditoSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCreditoSaldo.Texto = "0"
        '
        'lblCreditoSaldo
        '
        Me.lblCreditoSaldo.AutoSize = True
        Me.lblCreditoSaldo.Location = New System.Drawing.Point(660, 40)
        Me.lblCreditoSaldo.Name = "lblCreditoSaldo"
        Me.lblCreditoSaldo.Size = New System.Drawing.Size(43, 13)
        Me.lblCreditoSaldo.TabIndex = 11
        Me.lblCreditoSaldo.Text = "Credito:"
        '
        'txtDescuento
        '
        Me.txtDescuento.Color = System.Drawing.Color.Beige
        Me.txtDescuento.Decimales = False
        Me.txtDescuento.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtDescuento.Location = New System.Drawing.Point(621, 36)
        Me.txtDescuento.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(24, 21)
        Me.txtDescuento.SoloLectura = True
        Me.txtDescuento.TabIndex = 9
        Me.txtDescuento.TabStop = False
        Me.txtDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDescuento.Texto = "0"
        '
        'lblDescuento
        '
        Me.lblDescuento.AutoSize = True
        Me.lblDescuento.Location = New System.Drawing.Point(534, 40)
        Me.lblDescuento.Name = "lblDescuento"
        Me.lblDescuento.Size = New System.Drawing.Size(35, 13)
        Me.lblDescuento.TabIndex = 8
        Me.lblDescuento.Text = "Desc:"
        '
        'lblDeposito
        '
        Me.lblDeposito.AutoSize = True
        Me.lblDeposito.Location = New System.Drawing.Point(2, 113)
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(52, 13)
        Me.lblDeposito.TabIndex = 30
        Me.lblDeposito.Text = "Deposito:"
        '
        'lblListaPrecio
        '
        Me.lblListaPrecio.AutoSize = True
        Me.lblListaPrecio.Location = New System.Drawing.Point(517, 16)
        Me.lblListaPrecio.Name = "lblListaPrecio"
        Me.lblListaPrecio.Size = New System.Drawing.Size(52, 13)
        Me.lblListaPrecio.TabIndex = 2
        Me.lblListaPrecio.Text = "L. Precio:"
        '
        'cbxPromotor
        '
        Me.cbxPromotor.CampoWhere = Nothing
        Me.cbxPromotor.CargarUnaSolaVez = False
        Me.cbxPromotor.DataDisplayMember = "Nombres"
        Me.cbxPromotor.DataFilter = Nothing
        Me.cbxPromotor.DataOrderBy = Nothing
        Me.cbxPromotor.DataSource = "VPromotor"
        Me.cbxPromotor.DataValueMember = "ID"
        Me.cbxPromotor.dtSeleccionado = Nothing
        Me.cbxPromotor.FormABM = Nothing
        Me.cbxPromotor.Indicaciones = Nothing
        Me.cbxPromotor.Location = New System.Drawing.Point(590, 85)
        Me.cbxPromotor.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbxPromotor.Name = "cbxPromotor"
        Me.cbxPromotor.SeleccionMultiple = False
        Me.cbxPromotor.SeleccionObligatoria = True
        Me.cbxPromotor.Size = New System.Drawing.Size(211, 21)
        Me.cbxPromotor.SoloLectura = True
        Me.cbxPromotor.TabIndex = 29
        Me.cbxPromotor.TabStop = False
        Me.cbxPromotor.Texto = ""
        '
        'lblPromotor
        '
        Me.lblPromotor.AutoSize = True
        Me.lblPromotor.Location = New System.Drawing.Point(517, 89)
        Me.lblPromotor.Name = "lblPromotor"
        Me.lblPromotor.Size = New System.Drawing.Size(52, 13)
        Me.lblPromotor.TabIndex = 28
        Me.lblPromotor.Text = "Promotor:"
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = Nothing
        Me.cbxVendedor.CargarUnaSolaVez = True
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "VVendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.FormABM = Nothing
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(317, 85)
        Me.cbxVendedor.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = True
        Me.cbxVendedor.Size = New System.Drawing.Size(190, 21)
        Me.cbxVendedor.SoloLectura = True
        Me.cbxVendedor.TabIndex = 27
        Me.cbxVendedor.TabStop = False
        Me.cbxVendedor.Texto = ""
        '
        'lblVendedor
        '
        Me.lblVendedor.AutoSize = True
        Me.lblVendedor.Location = New System.Drawing.Point(263, 89)
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Size = New System.Drawing.Size(56, 13)
        Me.lblVendedor.TabIndex = 26
        Me.lblVendedor.Text = "Vendedor:"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(520, 63)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 21
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblPlazo
        '
        Me.lblPlazo.AutoSize = True
        Me.lblPlazo.Location = New System.Drawing.Point(400, 63)
        Me.lblPlazo.Name = "lblPlazo"
        Me.lblPlazo.Size = New System.Drawing.Size(36, 13)
        Me.lblPlazo.TabIndex = 19
        Me.lblPlazo.Text = "Plazo:"
        '
        'txtVencimiento
        '
        Me.txtVencimiento.AñoFecha = 0
        Me.txtVencimiento.Color = System.Drawing.Color.Empty
        Me.txtVencimiento.Fecha = New Date(CType(0, Long))
        Me.txtVencimiento.Location = New System.Drawing.Point(250, 59)
        Me.txtVencimiento.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtVencimiento.MesFecha = 0
        Me.txtVencimiento.Name = "txtVencimiento"
        Me.txtVencimiento.PermitirNulo = False
        Me.txtVencimiento.Size = New System.Drawing.Size(83, 20)
        Me.txtVencimiento.SoloLectura = True
        Me.txtVencimiento.TabIndex = 18
        Me.txtVencimiento.TabStop = False
        '
        'cbxCondicion
        '
        Me.cbxCondicion.CampoWhere = Nothing
        Me.cbxCondicion.CargarUnaSolaVez = False
        Me.cbxCondicion.DataDisplayMember = Nothing
        Me.cbxCondicion.DataFilter = Nothing
        Me.cbxCondicion.DataOrderBy = Nothing
        Me.cbxCondicion.DataSource = Nothing
        Me.cbxCondicion.DataValueMember = Nothing
        Me.cbxCondicion.dtSeleccionado = Nothing
        Me.cbxCondicion.FormABM = Nothing
        Me.cbxCondicion.Indicaciones = Nothing
        Me.cbxCondicion.Location = New System.Drawing.Point(173, 59)
        Me.cbxCondicion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.SeleccionMultiple = False
        Me.cbxCondicion.SeleccionObligatoria = True
        Me.cbxCondicion.Size = New System.Drawing.Size(89, 21)
        Me.cbxCondicion.SoloLectura = True
        Me.cbxCondicion.TabIndex = 16
        Me.cbxCondicion.TabStop = False
        Me.cbxCondicion.Texto = ""
        '
        'gbxCabecera
        '
        Me.gbxCabecera.BackColor = System.Drawing.Color.Transparent
        Me.gbxCabecera.Controls.Add(Me.txtTipoEntrega)
        Me.gbxCabecera.Controls.Add(Me.OcxCotizacion1)
        Me.gbxCabecera.Controls.Add(Me.Label1)
        Me.gbxCabecera.Controls.Add(Me.txtCliente)
        Me.gbxCabecera.Controls.Add(Me.cbxDeposito2)
        Me.gbxCabecera.Controls.Add(Me.cbxListaPrecio2)
        Me.gbxCabecera.Controls.Add(Me.txtPlazo)
        Me.gbxCabecera.Controls.Add(Me.txtTelefono)
        Me.gbxCabecera.Controls.Add(Me.lblTelefono)
        Me.gbxCabecera.Controls.Add(Me.lblTipoEntrega)
        Me.gbxCabecera.Controls.Add(Me.txtDetalle)
        Me.gbxCabecera.Controls.Add(Me.lblDetalle)
        Me.gbxCabecera.Controls.Add(Me.txtCreditoSaldo)
        Me.gbxCabecera.Controls.Add(Me.lblCreditoSaldo)
        Me.gbxCabecera.Controls.Add(Me.txtDescuento)
        Me.gbxCabecera.Controls.Add(Me.lblDescuento)
        Me.gbxCabecera.Controls.Add(Me.lblDeposito)
        Me.gbxCabecera.Controls.Add(Me.lblListaPrecio)
        Me.gbxCabecera.Controls.Add(Me.cbxPromotor)
        Me.gbxCabecera.Controls.Add(Me.lblPromotor)
        Me.gbxCabecera.Controls.Add(Me.cbxVendedor)
        Me.gbxCabecera.Controls.Add(Me.lblVendedor)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblPlazo)
        Me.gbxCabecera.Controls.Add(Me.txtVencimiento)
        Me.gbxCabecera.Controls.Add(Me.cbxCondicion)
        Me.gbxCabecera.Controls.Add(Me.txtFechaVenta)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.txtDireccion)
        Me.gbxCabecera.Controls.Add(Me.lblDireccion)
        Me.gbxCabecera.Controls.Add(Me.lblCliente)
        Me.gbxCabecera.Controls.Add(Me.lblPorcentaje)
        Me.gbxCabecera.Controls.Add(Me.lblVencimiento)
        Me.gbxCabecera.Controls.Add(Me.lblCondicion)
        Me.gbxCabecera.Controls.Add(Me.cbxFormaPagoFactura)
        Me.gbxCabecera.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbxCabecera.Location = New System.Drawing.Point(4, 62)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(810, 135)
        Me.gbxCabecera.TabIndex = 29
        Me.gbxCabecera.TabStop = False
        '
        'txtFechaVenta
        '
        Me.txtFechaVenta.AñoFecha = 0
        Me.txtFechaVenta.Color = System.Drawing.Color.Empty
        Me.txtFechaVenta.Fecha = New Date(CType(0, Long))
        Me.txtFechaVenta.Location = New System.Drawing.Point(64, 59)
        Me.txtFechaVenta.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtFechaVenta.MesFecha = 0
        Me.txtFechaVenta.Name = "txtFechaVenta"
        Me.txtFechaVenta.PermitirNulo = False
        Me.txtFechaVenta.Size = New System.Drawing.Size(67, 20)
        Me.txtFechaVenta.SoloLectura = False
        Me.txtFechaVenta.TabIndex = 14
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(2, 63)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 13
        Me.lblFecha.Text = "Fecha:"
        '
        'txtDireccion
        '
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Color = System.Drawing.Color.Beige
        Me.txtDireccion.Indicaciones = Nothing
        Me.txtDireccion.Location = New System.Drawing.Point(64, 36)
        Me.txtDireccion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtDireccion.Multilinea = False
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(320, 21)
        Me.txtDireccion.SoloLectura = True
        Me.txtDireccion.TabIndex = 5
        Me.txtDireccion.TabStop = False
        Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccion.Texto = ""
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.Location = New System.Drawing.Point(2, 40)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(55, 13)
        Me.lblDireccion.TabIndex = 4
        Me.lblDireccion.Text = "Direccion:"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(2, 16)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(42, 13)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Text = "Cliente:"
        '
        'lblPorcentaje
        '
        Me.lblPorcentaje.AutoSize = True
        Me.lblPorcentaje.Location = New System.Drawing.Point(592, 40)
        Me.lblPorcentaje.Name = "lblPorcentaje"
        Me.lblPorcentaje.Size = New System.Drawing.Size(15, 13)
        Me.lblPorcentaje.TabIndex = 10
        Me.lblPorcentaje.Text = "%"
        '
        'lblVencimiento
        '
        Me.lblVencimiento.AutoSize = True
        Me.lblVencimiento.Location = New System.Drawing.Point(281, 63)
        Me.lblVencimiento.Name = "lblVencimiento"
        Me.lblVencimiento.Size = New System.Drawing.Size(38, 13)
        Me.lblVencimiento.TabIndex = 17
        Me.lblVencimiento.Text = "Venc.:"
        '
        'lblCondicion
        '
        Me.lblCondicion.AutoSize = True
        Me.lblCondicion.Location = New System.Drawing.Point(137, 63)
        Me.lblCondicion.Name = "lblCondicion"
        Me.lblCondicion.Size = New System.Drawing.Size(38, 13)
        Me.lblCondicion.TabIndex = 15
        Me.lblCondicion.Text = "Cond.:"
        '
        'cbxFormaPagoFactura
        '
        Me.cbxFormaPagoFactura.CampoWhere = Nothing
        Me.cbxFormaPagoFactura.CargarUnaSolaVez = True
        Me.cbxFormaPagoFactura.DataDisplayMember = "Referencia"
        Me.cbxFormaPagoFactura.DataFilter = Nothing
        Me.cbxFormaPagoFactura.DataOrderBy = "ID"
        Me.cbxFormaPagoFactura.DataSource = "VFormaPagoFactura"
        Me.cbxFormaPagoFactura.DataValueMember = "ID"
        Me.cbxFormaPagoFactura.dtSeleccionado = Nothing
        Me.cbxFormaPagoFactura.FormABM = Nothing
        Me.cbxFormaPagoFactura.Indicaciones = Nothing
        Me.cbxFormaPagoFactura.Location = New System.Drawing.Point(64, 85)
        Me.cbxFormaPagoFactura.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbxFormaPagoFactura.Name = "cbxFormaPagoFactura"
        Me.cbxFormaPagoFactura.SeleccionMultiple = False
        Me.cbxFormaPagoFactura.SeleccionObligatoria = True
        Me.cbxFormaPagoFactura.Size = New System.Drawing.Size(198, 21)
        Me.cbxFormaPagoFactura.SoloLectura = True
        Me.cbxFormaPagoFactura.TabIndex = 25
        Me.cbxFormaPagoFactura.TabStop = False
        Me.cbxFormaPagoFactura.Texto = ""
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EliminarToolStripMenuItem, Me.ModificarDescToolStripMenuItem, Me.InformacionDelProductoToolStripMenuItem, Me.ToolStripSeparator1, Me.VerDescuentosToolStripMenuItem, Me.ExportarAExcelToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(211, 120)
        '
        'colGravado
        '
        Me.colGravado.Text = "Gravado"
        Me.colGravado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colGravado.Width = 65
        '
        'colExento
        '
        Me.colExento.Text = "Exento"
        Me.colExento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colExento.Width = 51
        '
        'colDescuentoPocentaje
        '
        Me.colDescuentoPocentaje.Text = "Desc. %"
        Me.colDescuentoPocentaje.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'colDescuentoUnitario
        '
        Me.colDescuentoUnitario.Text = "Desc. Uni."
        Me.colDescuentoUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colDescuentoUnitario.Width = 68
        '
        'colPrecioUnitario
        '
        Me.colPrecioUnitario.Text = "Pre. Uni."
        Me.colPrecioUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colPrecioUnitario.Width = 69
        '
        'ColUnidadMedida
        '
        Me.ColUnidadMedida.Text = "U.Med."
        Me.ColUnidadMedida.Width = 51
        '
        'colCantidad
        '
        Me.colCantidad.Text = "Cantidad"
        Me.colCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.colCantidad.Width = 54
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 260
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(6, 31)
        Me.txtProducto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(299, 22)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 1
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'txtDescuentoProducto
        '
        Me.txtDescuentoProducto.Color = System.Drawing.Color.Empty
        Me.txtDescuentoProducto.Decimales = True
        Me.txtDescuentoProducto.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtDescuentoProducto.Location = New System.Drawing.Point(686, 32)
        Me.txtDescuentoProducto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtDescuentoProducto.Name = "txtDescuentoProducto"
        Me.txtDescuentoProducto.Size = New System.Drawing.Size(37, 21)
        Me.txtDescuentoProducto.SoloLectura = True
        Me.txtDescuentoProducto.TabIndex = 11
        Me.txtDescuentoProducto.TabStop = False
        Me.txtDescuentoProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDescuentoProducto.Texto = "0"
        '
        'txtPrecioUnitario
        '
        Me.txtPrecioUnitario.Color = System.Drawing.Color.Empty
        Me.txtPrecioUnitario.Decimales = False
        Me.txtPrecioUnitario.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtPrecioUnitario.Location = New System.Drawing.Point(627, 32)
        Me.txtPrecioUnitario.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtPrecioUnitario.Name = "txtPrecioUnitario"
        Me.txtPrecioUnitario.Size = New System.Drawing.Size(59, 21)
        Me.txtPrecioUnitario.SoloLectura = False
        Me.txtPrecioUnitario.TabIndex = 9
        Me.txtPrecioUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrecioUnitario.Texto = "0"
        '
        'lblPrecioUnitario
        '
        Me.lblPrecioUnitario.AutoSize = True
        Me.lblPrecioUnitario.Location = New System.Drawing.Point(624, 16)
        Me.lblPrecioUnitario.Name = "lblPrecioUnitario"
        Me.lblPrecioUnitario.Size = New System.Drawing.Size(62, 13)
        Me.lblPrecioUnitario.TabIndex = 8
        Me.lblPrecioUnitario.Text = "Precio Uni.:"
        '
        'colIVA
        '
        Me.colIVA.Text = "IVA"
        Me.colIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.colIVA.Width = 50
        '
        'colDescripcion
        '
        Me.colDescripcion.Text = "Descripcion"
        Me.colDescripcion.Width = 210
        '
        'colReferencia
        '
        Me.colReferencia.Text = "Referencia"
        Me.colReferencia.Width = 64
        '
        'txtObservacionProducto
        '
        Me.txtObservacionProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacionProducto.Color = System.Drawing.Color.Empty
        Me.txtObservacionProducto.Indicaciones = Nothing
        Me.txtObservacionProducto.Location = New System.Drawing.Point(311, 31)
        Me.txtObservacionProducto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtObservacionProducto.Multilinea = False
        Me.txtObservacionProducto.Name = "txtObservacionProducto"
        Me.txtObservacionProducto.Size = New System.Drawing.Size(196, 21)
        Me.txtObservacionProducto.SoloLectura = False
        Me.txtObservacionProducto.TabIndex = 3
        Me.txtObservacionProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacionProducto.Texto = ""
        '
        'lvLista
        '
        Me.lvLista.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lvLista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvLista.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colID, Me.colReferencia, Me.colDescripcion, Me.colIVA, Me.colCantidad, Me.ColUnidadMedida, Me.colPrecioUnitario, Me.colDescuentoUnitario, Me.colDescuentoPocentaje, Me.colExento, Me.colGravado})
        Me.lvLista.ContextMenuStrip = Me.ContextMenuStrip1
        Me.lvLista.FullRowSelect = True
        Me.lvLista.GridLines = True
        Me.lvLista.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvLista.HideSelection = False
        Me.lvLista.Location = New System.Drawing.Point(30, 58)
        Me.lvLista.MultiSelect = False
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(745, 124)
        Me.lvLista.TabIndex = 14
        Me.lvLista.UseCompatibleStateImageBehavior = False
        Me.lvLista.View = System.Windows.Forms.View.Details
        '
        'colID
        '
        Me.colID.Text = "ID"
        Me.colID.Width = 0
        '
        'gbxDetalle
        '
        Me.gbxDetalle.Controls.Add(Me.txtProducto)
        Me.gbxDetalle.Controls.Add(Me.lklDescuento)
        Me.gbxDetalle.Controls.Add(Me.txtDescuentoProducto)
        Me.gbxDetalle.Controls.Add(Me.txtPrecioUnitario)
        Me.gbxDetalle.Controls.Add(Me.lblPrecioUnitario)
        Me.gbxDetalle.Controls.Add(Me.txtObservacionProducto)
        Me.gbxDetalle.Controls.Add(Me.lvLista)
        Me.gbxDetalle.Controls.Add(Me.lblObservacionProducto)
        Me.gbxDetalle.Controls.Add(Me.txtImporte)
        Me.gbxDetalle.Controls.Add(Me.lblImporte)
        Me.gbxDetalle.Controls.Add(Me.txtCantidad)
        Me.gbxDetalle.Controls.Add(Me.lblCantidad)
        Me.gbxDetalle.Controls.Add(Me.cbxUnidad)
        Me.gbxDetalle.Controls.Add(Me.lblUnidad)
        Me.gbxDetalle.Controls.Add(Me.lblProducto)
        Me.gbxDetalle.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbxDetalle.Location = New System.Drawing.Point(4, 197)
        Me.gbxDetalle.Name = "gbxDetalle"
        Me.gbxDetalle.Size = New System.Drawing.Size(810, 188)
        Me.gbxDetalle.TabIndex = 30
        Me.gbxDetalle.TabStop = False
        '
        'frmVenta2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(826, 595)
        Me.Controls.Add(Me.flpAnuladoPor)
        Me.Controls.Add(Me.chkImprimirSoloObservacion)
        Me.Controls.Add(Me.btnAnularRecibo)
        Me.Controls.Add(Me.btnImprimirRecibo)
        Me.Controls.Add(Me.btnHabilitar)
        Me.Controls.Add(Me.chkConversion)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.gbxComprobante)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.OcxImpuesto1)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Controls.Add(Me.gbxDetalle)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "frmVenta2"
        Me.Tag = "frmVenta"
        Me.Text = "frmVenta2"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.gbxComprobante.ResumeLayout(False)
        Me.gbxComprobante.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.gbxDetalle.ResumeLayout(False)
        Me.gbxDetalle.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblIDTransaccionPedido As Label
    Friend WithEvents lblCiudad As Label
    Friend WithEvents lblSucursal As Label
    Friend WithEvents lblComprobante As Label
    Friend WithEvents lblTalonario As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents lklDescuento As LinkLabel
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents flpAnuladoPor As FlowLayoutPanel
    Friend WithEvents lblAnulado As Label
    Friend WithEvents lblUsuarioAnulado As Label
    Friend WithEvents lblFechaAnulado As Label
    Friend WithEvents chkImprimirSoloObservacion As ocxCHK
    Friend WithEvents btnAnularRecibo As Button
    Friend WithEvents btnImprimirRecibo As Button
    Friend WithEvents btnHabilitar As Button
    Friend WithEvents chkConversion As ocxCHK
    Friend WithEvents flpRegistradoPor As FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As Label
    Friend WithEvents lblUsuarioRegistro As Label
    Friend WithEvents lblFechaRegistro As Label
    Friend WithEvents gbxComprobante As GroupBox
    Friend WithEvents txtIDTimbrado As ocxTXTString
    Friend WithEvents txtTimbrado As ocxTXTString
    Friend WithEvents lklTimbrado As LinkLabel
    Friend WithEvents txtRestoTimbrado As ocxTXTNumeric
    Friend WithEvents cbxSucursal As ocxCBX
    Friend WithEvents cbxCiudad As ocxCBX
    Friend WithEvents lblRestoTimbrado As Label
    Friend WithEvents txtVencimientoTimbrado As ocxTXTDate
    Friend WithEvents lblVenimientoTimbrado As Label
    Friend WithEvents txtNroComprobante As ocxTXTString
    Friend WithEvents txtTalonario As ocxTXTString
    Friend WithEvents cbxTipoComprobante As ocxCBX
    Friend WithEvents txtReferenciaSucursal As ocxTXTString
    Friend WithEvents txtReferenciaTerminal As ocxTXTString
    Friend WithEvents btnImprimir As Button
    Friend WithEvents btnBusquedaAvanzada As Button
    Friend WithEvents btnSalir As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnNuevo As Button
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents btnAsiento As Button
    Friend WithEvents OcxImpuesto1 As ocxImpuesto
    Friend WithEvents gbxCabecera As GroupBox
    Friend WithEvents txtTipoEntrega As ocxTXTString
    Friend WithEvents OcxCotizacion1 As ocxCotizacion
    Friend WithEvents Label1 As Label
    Friend WithEvents txtCliente As ocxTXTCliente
    Friend WithEvents cbxDeposito2 As ocxCBX
    Friend WithEvents cbxListaPrecio2 As ocxCBX
    Friend WithEvents txtPlazo As ocxTXTNumeric
    Friend WithEvents txtTelefono As ocxTXTString
    Friend WithEvents lblTelefono As Label
    Friend WithEvents lblTipoEntrega As Label
    Friend WithEvents txtDetalle As ocxTXTString
    Friend WithEvents lblDetalle As Label
    Friend WithEvents txtCreditoSaldo As ocxTXTNumeric
    Friend WithEvents lblCreditoSaldo As Label
    Friend WithEvents txtDescuento As ocxTXTNumeric
    Friend WithEvents lblDescuento As Label
    Friend WithEvents lblDeposito As Label
    Friend WithEvents lblListaPrecio As Label
    Friend WithEvents cbxPromotor As ocxCBX
    Friend WithEvents lblPromotor As Label
    Friend WithEvents cbxVendedor As ocxCBX
    Friend WithEvents lblVendedor As Label
    Friend WithEvents lblMoneda As Label
    Friend WithEvents lblPlazo As Label
    Friend WithEvents txtVencimiento As ocxTXTDate
    Friend WithEvents cbxCondicion As ocxCBX
    Friend WithEvents txtFechaVenta As ocxTXTDate
    Friend WithEvents lblFecha As Label
    Friend WithEvents txtDireccion As ocxTXTString
    Friend WithEvents lblDireccion As Label
    Friend WithEvents lblCliente As Label
    Friend WithEvents lblPorcentaje As Label
    Friend WithEvents lblVencimiento As Label
    Friend WithEvents lblCondicion As Label
    Friend WithEvents cbxFormaPagoFactura As ocxCBX
    Friend WithEvents gbxDetalle As GroupBox
    Friend WithEvents txtProducto As ocxTXTProducto
    Friend WithEvents txtDescuentoProducto As ocxTXTNumeric
    Friend WithEvents txtPrecioUnitario As ocxTXTNumeric
    Friend WithEvents lblPrecioUnitario As Label
    Friend WithEvents txtObservacionProducto As ocxTXTString
    Friend WithEvents lvLista As ListView
    Friend WithEvents colID As ColumnHeader
    Friend WithEvents colReferencia As ColumnHeader
    Friend WithEvents colDescripcion As ColumnHeader
    Friend WithEvents colIVA As ColumnHeader
    Friend WithEvents colCantidad As ColumnHeader
    Friend WithEvents ColUnidadMedida As ColumnHeader
    Friend WithEvents colPrecioUnitario As ColumnHeader
    Friend WithEvents colDescuentoUnitario As ColumnHeader
    Friend WithEvents colDescuentoPocentaje As ColumnHeader
    Friend WithEvents colExento As ColumnHeader
    Friend WithEvents colGravado As ColumnHeader
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents EliminarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ModificarDescToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents InformacionDelProductoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents VerDescuentosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExportarAExcelToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents lblObservacionProducto As Label
    Friend WithEvents txtImporte As ocxTXTNumeric
    Friend WithEvents lblImporte As Label
    Friend WithEvents txtCantidad As ocxTXTNumeric
    Friend WithEvents lblCantidad As Label
    Friend WithEvents cbxUnidad As ComboBox
    Friend WithEvents lblUnidad As Label
    Friend WithEvents lblProducto As Label
End Class

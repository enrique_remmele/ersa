﻿Imports System.Threading
Public Class frmSolicitudAnulacion

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim PorcentajeAsignado As Decimal = 0
    Dim ImporteAsignado As Decimal = 0

    'PROPIEDADES

    'VARIABLES
    Dim tProcesar As Thread
    Dim vProcesar As Boolean = False
    Dim SQL As String = ""


    Enum ENUMOperacionPedido
        APROBAR = 1
        RECHAZAR = 2
        RECUPERAR = 3
        ELIMINAR = 4
    End Enum


    'FUNCIONES
    Sub Inicializar()

        Me.KeyPreview = True

        'Varios
        CheckForIllegalCrossThreadCalls = False

        cbxGrupos.SelectedIndex = 0

        CargarInformacion()

        'Funciones
        ListarGrupos()

        'Fecha facturacion
        If IsDate(VGFechaFacturacion) = False Then
            VGFechaFacturacion = VGFechaHoraSistema
        End If

        txtFechaDesde.Hoy()
        txtFechaHasta.UnaSemana()

        ListarPedidos()
    End Sub

    Sub CargarInformacion()
        txtFechaDesde.txt.Text = Today.Date
        txtFechaHasta.txt.Text = Today.Date

    End Sub

    Sub GuardarInformacion()

    End Sub
    Sub ListarGrupos()

        lvGrupo.Items.Clear()
        lvPedidos.Items.Clear()

        Dim SQL As String = ""
        Dim INDEX As Integer = cbxGrupos.SelectedIndex

        Dim dtGrupo As New DataTable("Grupos")

        dtGrupo.Columns.Add("Grupo", Type.GetType("System.String"))

        Dim newRow As DataRow = dtGrupo.NewRow()
        newRow(0) = "VENTA"
        dtGrupo.Rows.Add(newRow)

        Dim newRowb As DataRow = dtGrupo.NewRow()
        newRowb(0) = "NOTA DE CREDITO"
        dtGrupo.Rows.Add(newRowb)

        Dim newRowc As DataRow = dtGrupo.NewRow()
        newRowc(0) = "APROBADOS"
        dtGrupo.Rows.Add(newRowc)

        Dim newRowd As DataRow = dtGrupo.NewRow()
        newRowd(0) = "RECHAZADOS"
        dtGrupo.Rows.Add(newRowd)

        CSistema.dtToLv(lvGrupo, dtGrupo)


        For i = 0 To lvGrupo.Columns.Count - 1
            lvGrupo.Columns(i).AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize)
        Next



    End Sub

    Sub SeleccionarGrupo()

        ListarPedidos()

    End Sub

    Sub ListarPedidos()

        If lvGrupo.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim Campo As String = ""


        If IsDate(vgConfiguraciones("VentaFechaFacturacion").ToString) = False Then
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaHoraSistema
        End If
        Select Case lvGrupo.SelectedItems.Item(0).Text
            Case "VENTA"
                SQL = "Select * From vSolicitudAnulacion Where Estado = 'PENDIENTE' and Operacion = 'VENTA'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = True
                btnAprobar.Text = "Aprobar"
                btnRechazar.Text = "Rechazar"
                txtFechaDesde.Visible = False
                txtFechaHasta.Visible = False
            Case "NOTA DE CREDITO"
                SQL = "Select * From vSolicitudAnulacion Where Estado = 'PENDIENTE' and Operacion = 'NOTA DE CREDITO'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = True
                btnAprobar.Text = "Aprobar"
                btnRechazar.Text = "Rechazar"
                txtFechaDesde.Visible = False
                txtFechaHasta.Visible = False

            Case "APROBADOS"
                SQL = "Select * From vSolicitudAnulacion Where Estado = 'APROBADO' and cast(FechaSolicitud as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = False
                btnRechazar.Enabled = False
                btnAprobar.Text = "Recuperar"
                txtFechaDesde.Visible = True
                txtFechaHasta.Visible = True


            Case "RECHAZADOS"
                SQL = "Select * From vSolicitudAnulacion Where Estado = 'RECHAZADO' and cast(FechaSolicitud as date)  Between '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
                btnAprobar.Enabled = True
                btnRechazar.Enabled = False
                btnAprobar.Text = "Recuperar"
                txtFechaDesde.Visible = True
                txtFechaHasta.Visible = True

        End Select

        Listar()

    End Sub

    Sub Listar()

        CSistema.SqlToLv(lvPedidos, SQL)

    End Sub
    Sub Excel()
        If SQL = "" Then
            Exit Sub
        End If
        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)
        CSistema.dtToExcel2(dt, "SolicitudesDeAnulacion")
    End Sub
    Sub Detener()
        Try
            If vProcesar = False Then
                Exit Sub
            End If

            tProcesar.Abort()
            vProcesar = False
        Catch ex As Exception

        End Try

        Thread.Sleep(1000)

    End Sub

    Sub AprobarRechazarPedido(ByVal Operacion As ENUMOperacionPedido)

        lvGrupo.Enabled = False
        lvPedidos.Enabled = False

        'Si no esta seleccionado ningun grupo, seleccionamos el primero
        If lvGrupo.Items.Count = 0 Then
            GoTo terminar
        End If

        If lvGrupo.SelectedItems.Count = 0 Then
            'GoTo terminar
        End If

        For Each item As ListViewItem In lvPedidos.Items
            'For Each item As ListViewItem In lvPedidos.CheckedItems
            If item.Checked = False Then
                GoTo siguiente
            End If

            item.ImageIndex = 1

            'Dim ID As Integer = item.Text
            Dim IDTransaccion As Integer = item.SubItems(0).Text
            Dim IDMotivo As Integer = item.SubItems(7).Text
            Dim dttemp As DataTable
            'Actualizar el item

            Select Case Operacion
                Case ENUMOperacionPedido.APROBAR
                    dttemp = CSistema.ExecuteToDataTable("Exec SpAutorizacionAnulacion
                                                                            @IDTransaccion = " & IDTransaccion & ",
                                                                            @Operacion='APROBAR',
	                                                                        @IDUsuario = " & vgIDUsuario & ",
	                                                                        @IDMotivo = " & IDMotivo & ",
	                                                                        @IDSucursal =" & vgIDSucursal & ",
                                                                            @IDDeposito =" & vgIDDeposito & ",
                                                                            @IDTerminal =" & vgIDTerminal, "", 1000).Copy
                Case ENUMOperacionPedido.RECHAZAR
                    dttemp = CSistema.ExecuteToDataTable("Exec SpAutorizacionAnulacion 
                                                                            @IDTransaccion = " & IDTransaccion & ", 
                                                                            @Operacion='RECHAZAR', 
                                                                            @IDUsuario = " & vgIDUsuario & ",
	                                                                        @IDMotivo = " & IDMotivo & ",
	                                                                        @IDSucursal =" & vgIDSucursal & ",
                                                                            @IDDeposito =" & vgIDDeposito & ",
                                                                            @IDTerminal =" & vgIDTerminal, "", 1000).Copy
                Case ENUMOperacionPedido.RECUPERAR
                    dttemp = CSistema.ExecuteToDataTable("Exec SpAutorizacionAnulacion
                                                                            @IDTransaccion = " & IDTransaccion & ", 
                                                                            @Operacion='RECUPERAR', 
                                                                            @IDUsuario = " & vgIDUsuario & ",
	                                                                        @IDMotivo = " & IDMotivo & ",
	                                                                        @IDSucursal =" & vgIDSucursal & ",
                                                                            @IDDeposito =" & vgIDDeposito & ",
                                                                            @IDTerminal =" & vgIDTerminal, "", 1000).Copy
            End Select

siguiente:

        Next

terminar:
        ListarPedidos()
        lvGrupo.Enabled = True
        lvPedidos.Enabled = True
        vProcesar = False

    End Sub

    Sub SeleccionarTodo()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = True
        Next
    End Sub

    Sub QuitarSeleccion()
        For Each item As ListViewItem In lvPedidos.Items
            item.Checked = False
        Next
    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        Select Case e.KeyCode
            Case Keys.Enter
                CSistema.SelectNextControl(Me, e.KeyCode)
        End Select

    End Sub

    Sub EliminarPedido()

        If lvPedidos.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        lvPedidos.SelectedItems(0).Remove()

    End Sub

    Private Sub frmAprobarPedido_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Detener()
        GuardarInformacion()
    End Sub

    Private Sub frmAprobarPedido_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        ManejarTecla(e)
    End Sub

    Private Sub frmAprobarPedido_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnActualizarGrupo_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarGrupo.Click
        ListarGrupos()
    End Sub

    Private Sub cbxGrupos_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxGrupos.SelectedIndexChanged
        ListarGrupos()
    End Sub

    Private Sub lvGrupo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lvGrupo.SelectedIndexChanged
        SeleccionarGrupo()
    End Sub

    Private Sub btnProcesar_Click(sender As System.Object, e As System.EventArgs) Handles btnAprobar.Click
        'Procesar()
        Select Case lvGrupo.SelectedItems.Item(0).Text
            Case "RECHAZADOS"
                AprobarRechazarPedido(ENUMOperacionPedido.RECUPERAR)
            Case "APROBADOS"
                AprobarRechazarPedido(ENUMOperacionPedido.RECUPERAR)
            Case Else
                AprobarRechazarPedido(ENUMOperacionPedido.APROBAR)
        End Select

    End Sub

    Private Sub btnSeleccionarTodos_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionarTodos.Click
        SeleccionarTodo()
    End Sub

    Private Sub ToolStripButton1_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton1.Click
        QuitarSeleccion()
    End Sub

    Private Sub btnActualizarPedidos_Click(sender As System.Object, e As System.EventArgs) Handles btnActualizarPedidos.Click
        ListarPedidos()
    End Sub

    Private Sub EliminarDeLaListaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        EliminarPedido()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub ToolStripButton2_Click(sender As System.Object, e As System.EventArgs) Handles btnRechazar.Click
        'Select Case lvGrupo.SelectedItems.Item(0).Text
        '    Case "PENDIENTE"
        '        AprobarRechazarPedido(ENUMOperacionPedido.RECHAZAR)
        '    Case "RECHAZADO"
        '        AprobarRechazarPedido(ENUMOperacionPedido.ANULAR)
        'End Select

        AprobarRechazarPedido(ENUMOperacionPedido.RECHAZAR)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Excel()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmSolicitudAnulacion_Activate()
        Me.Refresh()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAprobacionPedido
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnProcesar = New System.Windows.Forms.Button()
        Me.lblVista = New System.Windows.Forms.Button()
        Me.lblImprimir = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblListar = New System.Windows.Forms.Button()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblZona = New System.Windows.Forms.Label()
        Me.chkResumenPor = New ERP.ocxCHK()
        Me.cbxResumen = New ERP.ocxCBX()
        Me.chkDetallePor = New ERP.ocxCHK()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.chkGrupo = New ERP.ocxCHK()
        Me.cbxGrupo = New ERP.ocxCBX()
        Me.chkZonaVenta = New ERP.ocxCHK()
        Me.cbxZonaVenta = New ERP.ocxCBX()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.chkDeposito = New ERP.ocxCHK()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.chkProducto = New ERP.ocxCHK()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.chkCliente = New ERP.ocxCHK()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.txtSucursal = New ERP.ocxTXTString()
        Me.txtZona = New ERP.ocxTXTString()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.btnAyer = New System.Windows.Forms.Button()
        Me.btnEsteMes = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.TableLayoutPanel1)
        Me.Panel1.Controls.Add(Me.txtSucursal)
        Me.Panel1.Controls.Add(Me.lblSucursal)
        Me.Panel1.Controls.Add(Me.txtZona)
        Me.Panel1.Controls.Add(Me.lblZona)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(853, 471)
        Me.Panel1.TabIndex = 1
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dgw, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 175.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(853, 471)
        Me.TableLayoutPanel1.TabIndex = 4
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnProcesar)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblVista)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblImprimir)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 438)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(847, 30)
        Me.FlowLayoutPanel1.TabIndex = 2
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(769, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnProcesar
        '
        Me.btnProcesar.Location = New System.Drawing.Point(678, 3)
        Me.btnProcesar.Name = "btnProcesar"
        Me.btnProcesar.Size = New System.Drawing.Size(85, 23)
        Me.btnProcesar.TabIndex = 0
        Me.btnProcesar.Text = "&Procesar"
        Me.btnProcesar.UseVisualStyleBackColor = True
        '
        'lblVista
        '
        Me.lblVista.Location = New System.Drawing.Point(587, 3)
        Me.lblVista.Name = "lblVista"
        Me.lblVista.Size = New System.Drawing.Size(85, 23)
        Me.lblVista.TabIndex = 2
        Me.lblVista.Text = "&Vista"
        Me.lblVista.UseVisualStyleBackColor = True
        '
        'lblImprimir
        '
        Me.lblImprimir.Location = New System.Drawing.Point(496, 3)
        Me.lblImprimir.Name = "lblImprimir"
        Me.lblImprimir.Size = New System.Drawing.Size(85, 23)
        Me.lblImprimir.TabIndex = 3
        Me.lblImprimir.Text = "&Imprimir"
        Me.lblImprimir.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnEsteMes)
        Me.Panel2.Controls.Add(Me.btnAyer)
        Me.Panel2.Controls.Add(Me.FlowLayoutPanel4)
        Me.Panel2.Controls.Add(Me.chkResumenPor)
        Me.Panel2.Controls.Add(Me.cbxResumen)
        Me.Panel2.Controls.Add(Me.chkDetallePor)
        Me.Panel2.Controls.Add(Me.cbxOrdenadoPor)
        Me.Panel2.Controls.Add(Me.chkGrupo)
        Me.Panel2.Controls.Add(Me.cbxGrupo)
        Me.Panel2.Controls.Add(Me.chkZonaVenta)
        Me.Panel2.Controls.Add(Me.cbxZonaVenta)
        Me.Panel2.Controls.Add(Me.chkVendedor)
        Me.Panel2.Controls.Add(Me.cbxVendedor)
        Me.Panel2.Controls.Add(Me.chkDeposito)
        Me.Panel2.Controls.Add(Me.cbxDeposito)
        Me.Panel2.Controls.Add(Me.FlowLayoutPanel5)
        Me.Panel2.Controls.Add(Me.FlowLayoutPanel3)
        Me.Panel2.Controls.Add(Me.lblListar)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(847, 169)
        Me.Panel2.TabIndex = 0
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.Controls.Add(Me.chkProducto)
        Me.FlowLayoutPanel5.Controls.Add(Me.txtProducto)
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(355, 55)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(481, 52)
        Me.FlowLayoutPanel5.TabIndex = 50
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.chkCliente)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtCliente)
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(355, 4)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(481, 52)
        Me.FlowLayoutPanel3.TabIndex = 49
        '
        'lblListar
        '
        Me.lblListar.Location = New System.Drawing.Point(587, 141)
        Me.lblListar.Name = "lblListar"
        Me.lblListar.Size = New System.Drawing.Size(85, 23)
        Me.lblListar.TabIndex = 4
        Me.lblListar.Text = "&Listar"
        Me.lblListar.UseVisualStyleBackColor = True
        '
        'dgw
        '
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgw.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgw.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgw.Location = New System.Drawing.Point(3, 178)
        Me.dgw.Name = "dgw"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgw.Size = New System.Drawing.Size(847, 254)
        Me.dgw.TabIndex = 3
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSucursal.Location = New System.Drawing.Point(9, 6)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(60, 13)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblZona
        '
        Me.lblZona.AutoSize = True
        Me.lblZona.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblZona.Location = New System.Drawing.Point(239, 6)
        Me.lblZona.Name = "lblZona"
        Me.lblZona.Size = New System.Drawing.Size(40, 13)
        Me.lblZona.TabIndex = 2
        Me.lblZona.Text = "Zona:"
        '
        'chkResumenPor
        '
        Me.chkResumenPor.BackColor = System.Drawing.Color.Transparent
        Me.chkResumenPor.Color = System.Drawing.Color.Empty
        Me.chkResumenPor.Location = New System.Drawing.Point(3, 143)
        Me.chkResumenPor.Name = "chkResumenPor"
        Me.chkResumenPor.Size = New System.Drawing.Size(114, 21)
        Me.chkResumenPor.SoloLectura = False
        Me.chkResumenPor.TabIndex = 62
        Me.chkResumenPor.Texto = "Resumen por:"
        Me.chkResumenPor.Valor = False
        '
        'cbxResumen
        '
        Me.cbxResumen.CampoWhere = Nothing
        Me.cbxResumen.CargarUnaSolaVez = False
        Me.cbxResumen.DataDisplayMember = Nothing
        Me.cbxResumen.DataFilter = Nothing
        Me.cbxResumen.DataOrderBy = Nothing
        Me.cbxResumen.DataSource = Nothing
        Me.cbxResumen.DataValueMember = Nothing
        Me.cbxResumen.FormABM = Nothing
        Me.cbxResumen.Indicaciones = Nothing
        Me.cbxResumen.Location = New System.Drawing.Point(123, 143)
        Me.cbxResumen.Name = "cbxResumen"
        Me.cbxResumen.SeleccionObligatoria = True
        Me.cbxResumen.Size = New System.Drawing.Size(157, 21)
        Me.cbxResumen.SoloLectura = False
        Me.cbxResumen.TabIndex = 61
        Me.cbxResumen.Texto = ""
        '
        'chkDetallePor
        '
        Me.chkDetallePor.BackColor = System.Drawing.Color.Transparent
        Me.chkDetallePor.Color = System.Drawing.Color.Empty
        Me.chkDetallePor.Location = New System.Drawing.Point(3, 116)
        Me.chkDetallePor.Name = "chkDetallePor"
        Me.chkDetallePor.Size = New System.Drawing.Size(114, 21)
        Me.chkDetallePor.SoloLectura = False
        Me.chkDetallePor.TabIndex = 60
        Me.chkDetallePor.Texto = "Detalle por:"
        Me.chkDetallePor.Valor = False
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(123, 116)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionObligatoria = True
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 59
        Me.cbxOrdenadoPor.Texto = ""
        '
        'chkGrupo
        '
        Me.chkGrupo.BackColor = System.Drawing.Color.Transparent
        Me.chkGrupo.Color = System.Drawing.Color.Empty
        Me.chkGrupo.Location = New System.Drawing.Point(3, 89)
        Me.chkGrupo.Name = "chkGrupo"
        Me.chkGrupo.Size = New System.Drawing.Size(114, 21)
        Me.chkGrupo.SoloLectura = False
        Me.chkGrupo.TabIndex = 57
        Me.chkGrupo.Texto = "Grupo:"
        Me.chkGrupo.Valor = False
        '
        'cbxGrupo
        '
        Me.cbxGrupo.CampoWhere = "IDZonaVenta"
        Me.cbxGrupo.CargarUnaSolaVez = False
        Me.cbxGrupo.DataDisplayMember = "Descripcion"
        Me.cbxGrupo.DataFilter = Nothing
        Me.cbxGrupo.DataOrderBy = Nothing
        Me.cbxGrupo.DataSource = "VZonaVenta"
        Me.cbxGrupo.DataValueMember = "ID"
        Me.cbxGrupo.Enabled = False
        Me.cbxGrupo.FormABM = Nothing
        Me.cbxGrupo.Indicaciones = Nothing
        Me.cbxGrupo.Location = New System.Drawing.Point(123, 89)
        Me.cbxGrupo.Name = "cbxGrupo"
        Me.cbxGrupo.SeleccionObligatoria = False
        Me.cbxGrupo.Size = New System.Drawing.Size(213, 21)
        Me.cbxGrupo.SoloLectura = False
        Me.cbxGrupo.TabIndex = 58
        Me.cbxGrupo.Texto = ""
        '
        'chkZonaVenta
        '
        Me.chkZonaVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkZonaVenta.Color = System.Drawing.Color.Empty
        Me.chkZonaVenta.Location = New System.Drawing.Point(3, 62)
        Me.chkZonaVenta.Name = "chkZonaVenta"
        Me.chkZonaVenta.Size = New System.Drawing.Size(114, 21)
        Me.chkZonaVenta.SoloLectura = False
        Me.chkZonaVenta.TabIndex = 55
        Me.chkZonaVenta.Texto = "Zona de Venta:"
        Me.chkZonaVenta.Valor = False
        '
        'cbxZonaVenta
        '
        Me.cbxZonaVenta.CampoWhere = "IDZonaVenta"
        Me.cbxZonaVenta.CargarUnaSolaVez = False
        Me.cbxZonaVenta.DataDisplayMember = "Descripcion"
        Me.cbxZonaVenta.DataFilter = Nothing
        Me.cbxZonaVenta.DataOrderBy = Nothing
        Me.cbxZonaVenta.DataSource = "VZonaVenta"
        Me.cbxZonaVenta.DataValueMember = "ID"
        Me.cbxZonaVenta.Enabled = False
        Me.cbxZonaVenta.FormABM = Nothing
        Me.cbxZonaVenta.Indicaciones = Nothing
        Me.cbxZonaVenta.Location = New System.Drawing.Point(123, 62)
        Me.cbxZonaVenta.Name = "cbxZonaVenta"
        Me.cbxZonaVenta.SeleccionObligatoria = False
        Me.cbxZonaVenta.Size = New System.Drawing.Size(213, 21)
        Me.cbxZonaVenta.SoloLectura = False
        Me.cbxZonaVenta.TabIndex = 56
        Me.cbxZonaVenta.Texto = ""
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(3, 35)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(114, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 53
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = "IDVendedor"
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "VVendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = "frmVendedor"
        Me.cbxVendedor.Indicaciones = "Seleccion de Vendedor"
        Me.cbxVendedor.Location = New System.Drawing.Point(123, 35)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(213, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 54
        Me.cbxVendedor.Texto = "ALDO GONZALES"
        '
        'chkDeposito
        '
        Me.chkDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkDeposito.Color = System.Drawing.Color.Empty
        Me.chkDeposito.Location = New System.Drawing.Point(3, 7)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(114, 21)
        Me.chkDeposito.SoloLectura = False
        Me.chkDeposito.TabIndex = 51
        Me.chkDeposito.Texto = "Deposito:"
        Me.chkDeposito.Valor = False
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = "IDDeposito"
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "SucDeposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = "SucDeposito"
        Me.cbxDeposito.DataSource = "VDeposito"
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.Enabled = False
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(123, 7)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(213, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 52
        Me.cbxDeposito.Texto = "AVERIADOS"
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(3, 3)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(69, 17)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 44
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 260
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(78, 3)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.Size = New System.Drawing.Size(394, 23)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 45
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(3, 3)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(57, 26)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 45
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 115
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(66, 3)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(406, 26)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 42
        '
        'txtSucursal
        '
        Me.txtSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSucursal.Color = System.Drawing.Color.Empty
        Me.txtSucursal.Indicaciones = Nothing
        Me.txtSucursal.Location = New System.Drawing.Point(75, 2)
        Me.txtSucursal.Multilinea = False
        Me.txtSucursal.Name = "txtSucursal"
        Me.txtSucursal.Size = New System.Drawing.Size(158, 21)
        Me.txtSucursal.SoloLectura = True
        Me.txtSucursal.TabIndex = 1
        Me.txtSucursal.TabStop = False
        Me.txtSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSucursal.Texto = ""
        '
        'txtZona
        '
        Me.txtZona.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtZona.Color = System.Drawing.Color.Empty
        Me.txtZona.Indicaciones = Nothing
        Me.txtZona.Location = New System.Drawing.Point(285, 2)
        Me.txtZona.Multilinea = False
        Me.txtZona.Name = "txtZona"
        Me.txtZona.Size = New System.Drawing.Size(169, 21)
        Me.txtZona.SoloLectura = True
        Me.txtZona.TabIndex = 3
        Me.txtZona.TabStop = False
        Me.txtZona.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtZona.Texto = ""
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.lblPeriodo)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtDesde)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtHasta)
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(356, 112)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(222, 25)
        Me.FlowLayoutPanel4.TabIndex = 63
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(3, 0)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 38
        Me.lblPeriodo.Text = "Periodo:"
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 5, 14, 23, 5, 359)
        Me.txtDesde.Location = New System.Drawing.Point(55, 3)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 39
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 5, 14, 23, 5, 359)
        Me.txtHasta.Location = New System.Drawing.Point(135, 3)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 40
        '
        'btnAyer
        '
        Me.btnAyer.Location = New System.Drawing.Point(355, 141)
        Me.btnAyer.Name = "btnAyer"
        Me.btnAyer.Size = New System.Drawing.Size(85, 23)
        Me.btnAyer.TabIndex = 64
        Me.btnAyer.Text = "&Ayer"
        Me.btnAyer.UseVisualStyleBackColor = True
        '
        'btnEsteMes
        '
        Me.btnEsteMes.Location = New System.Drawing.Point(446, 141)
        Me.btnEsteMes.Name = "btnEsteMes"
        Me.btnEsteMes.Size = New System.Drawing.Size(85, 23)
        Me.btnEsteMes.TabIndex = 65
        Me.btnEsteMes.Text = "&Este mes"
        Me.btnEsteMes.UseVisualStyleBackColor = True
        '
        'frmAprobacionPedido
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(853, 471)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmAprobacionPedido"
        Me.Text = "frmAprobacionPedido"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtSucursal As ERP.ocxTXTString
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents txtZona As ERP.ocxTXTString
    Friend WithEvents lblZona As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnProcesar As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents lblListar As System.Windows.Forms.Button
    Friend WithEvents FlowLayoutPanel5 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents chkDeposito As ERP.ocxCHK
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents chkGrupo As ERP.ocxCHK
    Friend WithEvents cbxGrupo As ERP.ocxCBX
    Friend WithEvents chkZonaVenta As ERP.ocxCHK
    Friend WithEvents cbxZonaVenta As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents chkResumenPor As ERP.ocxCHK
    Friend WithEvents cbxResumen As ERP.ocxCBX
    Friend WithEvents chkDetallePor As ERP.ocxCHK
    Friend WithEvents lblVista As System.Windows.Forms.Button
    Friend WithEvents lblImprimir As System.Windows.Forms.Button
    Friend WithEvents btnEsteMes As System.Windows.Forms.Button
    Friend WithEvents btnAyer As System.Windows.Forms.Button
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents txtHasta As ERP.ocxTXTDate
End Class

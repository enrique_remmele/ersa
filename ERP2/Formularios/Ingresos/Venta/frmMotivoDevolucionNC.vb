﻿Public Class frmMotivoDevolucionNC

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Public Property Procesado As Boolean

    'CLASES
    Dim CSistema As New CSistema
    Private IDMotivoValue As Integer
    Public Property IDMotivo() As Integer
        Get
            Return IDMotivoValue
        End Get
        Set(ByVal value As Integer)
            IDMotivoValue = value
        End Set
    End Property

    Sub Inicilizar()
        IDMotivo = 0
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion from MotivoDevolucionNC where Activo = 1")
        Procesado = False


    End Sub

    Sub Aceptar()

        If cbxMotivo.cbx.SelectedValue Is Nothing Then
            tsslEstado.Text = "Atencion: Motivo invalido"
            ctrError.SetError(cbxMotivo, "Atencion: Motivo invalido")
            ctrError.SetIconAlignment(cbxMotivo, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'Consultar
        ' If MessageBox.Show("Atencion! Esta seguro que es " & cbxMotivo.Texto, "Motivo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
        'Exit Sub
        'End If

        IDMotivo = cbxMotivo.GetValue

        Dim MensajeRetorno As String = ""

        Procesado = True
        Me.Close()

    End Sub

    Private Sub frmAgregarMotivoAlulacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicilizar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Aceptar()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmMotivoDevolucionNC_Activate()
        Me.Refresh()
    End Sub
End Class
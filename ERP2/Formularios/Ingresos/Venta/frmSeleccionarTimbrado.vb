﻿Public Class frmSeleccionarTimbrado

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private TimbradoValue As String
    Public Property Timbrado() As String
        Get
            Return TimbradoValue
        End Get
        Set(ByVal value As String)
            TimbradoValue = value
        End Set
    End Property

    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private SeleccionadoValue As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionadoValue = value
        End Set
    End Property

    Public Property IDtipoComprobante As Integer = -1

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        cbxSucursal.SelectedValue(vgIDSucursal)

        'Funciones
        Listar()

        'Foco
        dgv.Focus()

    End Sub

    Sub Listar()

        'Por Sucursal
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select ID, 'Suc'= ReferenciaSucursal, 'P. Exp.'= ReferenciaPunto, 'T. Comp.'=TipoComprobante, Timbrado, Vencimiento, 'Desde'=NumeracionDesde, 'Hasta'=NumeracionHasta, 'Prox.'=ProximoComprobante  From VPuntoExpedicion Where IDOperacion=" & IDOperacion & " And IDSucursal=" & cbxSucursal.GetValue & " And Estado='" & chkActivos.Valor & "' Order By ReferenciaPunto, TipoComprobante, Vencimiento")
        CSistema.dtToGrid(dgv, dt)

        ''Todos lo puntos de expedición del comprobante
        'If PorSucursal = False Then
        '    Dim dt As DataTable = CSistema.ExecuteToDataTable("Select ID, 'Suc'= ReferenciaSucursal, 'P. Exp.'= ReferenciaPunto, 'T. Comp.'=TipoComprobante, Timbrado, Vencimiento, 'Desde'=NumeracionDesde, 'Hasta'=NumeracionHasta  From VPuntoExpedicion Where IDOperacion=" & IDOperacion & " And Estado='True' ")
        '    CSistema.dtToGrid(dgv, dt)
        'End If


        'Formato
        For c As Integer = 0 To dgv.Columns.Count - 1
            dgv.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Next

        dgv.Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill


    End Sub

    Sub Seleccionar()

        If dgv.SelectedRows.Count = 0 Then
            MessageBox.Show("Seleccione un registro!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        ID = dgv.SelectedRows(0).Cells(0).Value
        Timbrado = dgv.SelectedRows(0).Cells(4).Value
        Seleccionado = True
        Me.Close()

    End Sub

    Sub Salir()

        Seleccionado = False
        Me.Close()

    End Sub

    Private Sub dgv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyDown
        If e.KeyCode = Keys.Enter Then

            ' Your code here
            e.SuppressKeyPress = True

        End If

    End Sub

    Private Sub dgv_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyUp
        If e.KeyCode = Keys.Enter Then
            Seleccionar()
        End If
    End Sub

    Private Sub frmSeleccionarTimbrado_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmSeleccionarTimbrado_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Salir()
    End Sub

    Private Sub btnActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        Listar()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmSeleccionarTimbrado_Activate()
        Me.Refresh()
    End Sub
End Class
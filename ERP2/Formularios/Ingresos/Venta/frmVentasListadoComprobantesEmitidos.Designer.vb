﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVentasListadoComprobantesEmitidos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.OcxTXTDate1 = New ERP.ocxTXTDate()
        Me.OcxCBX1 = New ERP.ocxCBX()
        Me.OcxCHK1 = New ERP.ocxCHK()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(26, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Label1"
        '
        'OcxTXTDate1
        '
        Me.OcxTXTDate1.Color = System.Drawing.Color.Empty
        Me.OcxTXTDate1.Fecha = New Date(CType(0, Long))
        Me.OcxTXTDate1.Location = New System.Drawing.Point(90, 37)
        Me.OcxTXTDate1.Name = "OcxTXTDate1"
        Me.OcxTXTDate1.Size = New System.Drawing.Size(74, 20)
        Me.OcxTXTDate1.SoloLectura = False
        Me.OcxTXTDate1.TabIndex = 1
        '
        'OcxCBX1
        '
        Me.OcxCBX1.DataDisplayMember = Nothing
        Me.OcxCBX1.DataFilter = Nothing
        Me.OcxCBX1.DataSource = Nothing
        Me.OcxCBX1.DataValueMember = Nothing
        Me.OcxCBX1.FormABM = Nothing
        Me.OcxCBX1.Indicaciones = Nothing
        Me.OcxCBX1.Location = New System.Drawing.Point(86, 79)
        Me.OcxCBX1.Name = "OcxCBX1"
        Me.OcxCBX1.SeleccionObligatoria = False
        Me.OcxCBX1.Size = New System.Drawing.Size(189, 21)
        Me.OcxCBX1.SoloLectura = False
        Me.OcxCBX1.TabIndex = 2
        Me.OcxCBX1.Texto = ""
        '
        'OcxCHK1
        '
        Me.OcxCHK1.Color = System.Drawing.Color.Empty
        Me.OcxCHK1.Location = New System.Drawing.Point(49, 124)
        Me.OcxCHK1.Name = "OcxCHK1"
        Me.OcxCHK1.Size = New System.Drawing.Size(74, 15)
        Me.OcxCHK1.SoloLectura = False
        Me.OcxCHK1.TabIndex = 3
        Me.OcxCHK1.Texto = Nothing
        Me.OcxCHK1.Valor = False
        '
        'frmVentasListadoComprobantesEmitidos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(826, 390)
        Me.Controls.Add(Me.OcxCHK1)
        Me.Controls.Add(Me.OcxCBX1)
        Me.Controls.Add(Me.OcxTXTDate1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmVentasListadoComprobantesEmitidos"
        Me.Text = "frmVentasListadoComprobantesEmitidos"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents OcxTXTDate1 As ERP.ocxTXTDate
    Friend WithEvents OcxCBX1 As ERP.ocxCBX
    Friend WithEvents OcxCHK1 As ERP.ocxCHK
End Class
